<?php
    // This is a SPIP language file  --  Ceci est un fichier langue de SPIP

    if (!defined('_ECRIRE_INC_VERSION')) return;

    $GLOBALS[$GLOBALS['idx_lang']] = array(

        // T
        'theme_brownie_nom' => "Th&egrave;me Brownie",
        'theme_brownie_description' => "Brownie est un th&egrave;me responsive pour SPIP 3 qui fonctionne tr&egrave;s bien pour la cr&eacute;ation d'un blog",
        'theme_brownie_slogan' => "Th&egrave;me Responsive pour cr&eacute;er un site de news ou un blog."
    );

?>