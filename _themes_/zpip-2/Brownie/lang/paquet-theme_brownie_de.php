<?php
    // This is a SPIP language file  --  Ceci est un fichier langue de SPIP

    if (!defined('_ECRIRE_INC_VERSION')) return;

    $GLOBALS[$GLOBALS['idx_lang']] = array(

        // T
        'theme_brownie_nom' => "Thema Brownie",
        'theme_brownie_description' => "Brownie ist ein Thema f�r SIP 3, das besonders f�r Blogs geeignet ist.",
        'theme_brownie_slogan' => "Ein Thema mit Techniken des Responsive Web Design f�r News-Sites und Blogs."
    );

?>