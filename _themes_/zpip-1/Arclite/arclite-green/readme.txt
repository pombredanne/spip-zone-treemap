Project page:
http://digitalnature.ro/projects/arclite/
Theme page for spip
http://spip-contrib.net


Licensed under GPL
http://www.opensource.org/licenses/gpl-license.php


Credits:

- Design and coding by digitalnature, http://digitalnature.ro
- "Union" font by the Danish Ministry of Education, http://praegnanz.de/essays/391/union
- "Share" font by the Typo3 design team, http://typo3.org/teams/design/style-guide/downloads/
- lightbox based on FancyBox by Janis Skarnelis
- Adaptation for spip by Denis Chenu, http://www.gsill.net