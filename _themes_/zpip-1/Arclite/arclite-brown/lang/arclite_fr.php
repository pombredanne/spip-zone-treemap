<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
    'cfg'                       =>'Configuration du th&egrave;me "arclite"',
	'cfg_variation'				=>'Variations de Arclite',
	'cfg_color_variation'		=>'Variations de couleurs principale',
	'cfg_color_brown'			=>'Defaut : Brun',
	'cfg_color_green'			=>'Vert',
	'cfg_color_red'				=>'Rouge',
	'cfg_color_blue'			=>'Bleu',
);  

?>
