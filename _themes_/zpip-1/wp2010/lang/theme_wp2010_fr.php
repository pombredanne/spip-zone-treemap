<?php
/** Fichier de langue de SPIP **/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'copyleft'     =>'Thème ZPIP <i>WordPress Twenty Ten</i> 2012 <a href="http://www.maieul.net">Maïeul Rouquette</a> d\'après <a href="http://www.wordpress.com">communauté WordPress</a>, licence <a href="http://www.gnu.org/licenses/gpl.html">GNU/GPL</a>.'
	
);

?>