<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
    'habillage_de' => "Habillage inspir&eacute; de ",
    'theme_multiflex' => "Theme Multiflex ",
    'pour' => "pour ",
    'sous_licence' => "Licence ",
    
    'configurer_multiflex' => "Multiflex",
    'configurer_description_multiflex' => "Couleurs, colonnage et autres babioles... C'est ici pour choisir !",
    
	'label_bordure' => "Bordure",
	'label_bordure_claire' => "Bordure claire",
	'label_bordure_foncee' => "Bordure foncée",
	
	'label_fond_clair' => "Fond clair",
	'label_fond_fonce' => "Fond foncé",
	
	'label_texte' => "Texte",
	'label_texte_entete' => "Entête",
	'label_texte_entete_image' => "Entête si image dessous",
	'label_texte_titre' => "Titre",
	'label_texte_titre_menu' => "Titre des menus",
	'label_lien' => "Liens",
	'label_lien_hover' => "Liens au survol",
	
	'label_texte_codes' => "Texte",
	'label_fond_codes' => "Fond",
	'label_bordure_codes' => "Bordure",
	
	'label_texte_forum' => "Texte",
	'label_fond_forum' => "Fond",
	'label_fond_forum_clair' => "Fond clair",
	'label_bordure_forum' => "Bordure",
	'label_liens_forum' => "Liens",
	
	'label_extra_bordure' => "Bordure @id@",
	'label_extra_fond' => "Fond @id@",
	
);  

?>
