<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
    'cfg'                       =>'Configuration du th&egrave;me "Collezion"',
    'cfg_colorer_fond_texte'    =>'Colorer les fonds de texte (descriptif d\'un objet, introduction &agrave; une cat&eacute;gorie)',
    'cfg_liens_nav'             =>'Couleurs des liens de navigations',
    'cfg_liens_spip_in'         =>'Couleurs des liens SPIP internes',
    'cfg_liens_spip_out'        =>'Couleurs des liens SPIP externes',
    'cfg_liens_spip_glossaire'  =>'Couleurs des liens SPIP vers le glossaire (Wikip&eacute;dia)',
    'cfg_couleur_liens'         =>'Couleur des liens',
    'cfg_pagination'            =>'Nombre maximum d\'objets par page (x3 ; 0 si illimit&eacute;)',
    'cfg_couleur_titres'        =>'Couleur des titres',
    'cfg_fond_sepia'            =>'Couleur du d&eacute;grad&eacute; de fond de page (gauche)',
    'cfg_afficher_hr'           =>'Afficher les filets de s&eacute;paration horizontaux',
    'cfg_couleur_filets'        =>'Couleurs des filets de s&eacute;paration',
    'cfg_presentation'          =>'Configuration de la pr&eacute;sentation',

);  

?>
