<?php

function formulaires_theme_selection_variante_charger_dist($theme='') {
	$variante = '';
	$themes = unserialize($GLOBALS['meta']['themes']);
	if (is_array($themes) and isset($themes['variantes'])
	and $variantes = $themes['variantes']
	and isset($variantes[$theme])
	and $variantes[$theme]) {
		$variante = $variantes[$theme];
	}

	$listes = find_all_in_path('variantes/', '.*\.css');
	$listes = array_keys($listes);
	return array(
		'theme' => $theme,
		'variante' => $variante,
		'variantes' => $listes,
	);
}

function formulaires_theme_selection_variante_verifier_dist($theme='') {
	return array();
}

function formulaires_theme_selection_variante_traiter_dist($theme='') {
	$themes = unserialize($GLOBALS['meta']['themes']);
	if (!is_array($themes)) $themes = array();
	$variante = _request('variante');
	$add = array('variantes'=>array($theme=>''));
	$themes = array_merge_recursive($themes, $add);
	$themes['variantes'][$theme] = $variante;
	ecrire_meta('themes', serialize($themes));
	include_spip('inc/invalideur');
	suivre_invalideur("theme/$theme/$variante");
}

?>
