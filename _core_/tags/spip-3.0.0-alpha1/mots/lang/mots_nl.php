<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'avis_conseil_selection_mot_cle' => '<b>Belangrijke groep:</b> het is erg aan te bevelen een trefwoord uit deze groep te kiezen.',
	'avis_doublon_mot_cle' => 'Een woord bestaat al met deze titel. Ben je zeker dat je wilt dezelfde te maken ?',

	// B
	'bouton_checkbox_qui_attribue_mot_cle_administrateurs' => 'beheerders van de site',
	'bouton_checkbox_qui_attribue_mot_cle_redacteurs' => 'auteurs',
	'bouton_checkbox_qui_attribue_mot_cle_visiteurs' => 'bezoekers van de publieke site die een bericht op het forum posten',

	// C
	'creer_et_associer_un_mot' => 'Créér et associer un mot-clé', # NEW

	// I
	'icone_creation_groupe_mots' => 'Een nieuwe groep van trefwoorden aanmaken',
	'icone_creation_mots_cles' => 'Een nieuw trefwoord aanmaken',
	'icone_modif_groupe_mots' => 'Deze groep van trefwoorden wijzigen',
	'icone_modifier_mot' => 'Dit sleutelwoord wijzigen',
	'icone_mots_cles' => 'Trefwoorden',
	'icone_supprimer_groupe_mots' => 'Deze groep wissen',
	'icone_voir_groupe_mots' => 'Voir ce groupe de mots', # NEW
	'icone_voir_tous_mots_cles' => 'Alle trefwoorden bekijken',
	'info_1_groupe_mots' => '1 groupe de mots', # NEW
	'info_articles_lies_mot' => 'Artikels gekoppeld aan dit trefwoord',
	'info_aucun_groupe_mots' => 'Aucun groupe de mots', # NEW
	'info_aucun_mot_cle' => 'Geen Trefwoord',
	'info_changer_nom_groupe' => 'De naam van deze groep veranderen:',
	'info_creation_mots_cles' => 'Creëer en configureer hier de trefwoorden van de site',
	'info_dans_groupe' => 'In de groep:',
	'info_delet_mots_cles' => 'Je hebt gevraagd het trefwoord 
<b>@titre_mot@</b> (@type_mot@) te verwijderen. Dit trefwoord is gekoppeld aan 
<b>@texte_lie@</b>, je dient je beslissing te bevestigen:',
	'info_groupe_important' => 'Belangrijke groep',
	'info_modifier_mot' => 'Het sleutelwoord wijzigen :',
	'info_mots_cles' => 'Trefwoorden',
	'info_mots_cles_association' => 'Trefwoorden uit deze groep kunnen verbonden worden met:',
	'info_nb_groupe_mots' => '@nb@ groupes de mots', # NEW
	'info_oui_suppression_mot_cle' => 'Ik wil dit trefwoord definitief verwijderen.',
	'info_question_mots_cles' => 'Wil je gebruik maken van trefwoorden op je site?',
	'info_qui_attribue_mot_cle' => 'De woorden uit deze groep kunnen toegekend worden door:',
	'info_remplacer_mot' => 'Remplacer "@titre@"', # NEW
	'info_retirer_mot' => 'Trek dat woord',
	'info_retirer_mots' => 'Alle trefwoorden intrekken',
	'info_rubriques_liees_mot' => 'Rubrieken gekoppeld aan dit trefwoord',
	'info_selection_un_seul_mot_cle' => 'Men kan slechts <b>één enkel trefwoord</b> tegelijk kiezen uit deze groep.',
	'info_supprimer_mot' => 'dit woord verwijderen',
	'info_titre_mot_cle' => 'Name of titel voor slautelwoord',
	'info_un_mot' => 'Een enkel trefwoord',
	'item_ajout_mots_cles' => 'Gebruik van trefwoorden toestaan op de forums',
	'item_non_ajout_mots_cles' => 'Gebruik van trefwoorden in de forums niet toestaan',
	'item_non_utiliser_config_groupe_mots_cles' => 'Geavanceerde configuratie van groepen trefwoorden niet gebruiken',
	'item_non_utiliser_mots_cles' => 'Trefwoorden niet gebruiken',
	'item_utiliser_config_groupe_mots_cles' => 'Geavanceerde configuratie van groepen trefwoorden gebruiken',
	'item_utiliser_mots_cles' => 'Trefwoorden gebruiken',

	// L
	'lien_ajouter_mot' => 'Ajouter ce mot-clé', # NEW
	'logo_groupe' => 'LOGO DE CE GROUPE', # NEW
	'logo_mot_cle' => 'LOGO VAN HET TREFWOORD',

	// T
	'texte_config_groupe_mots_cles' => 'Wilt u de geavanceerde configuratie van de sleutelwoorden activeren,
   door mede te delen bijvoorbeeld dat men een enig woord
 kan selecteren   per groep, dat een groep belangrijk is... ?',
	'texte_mots_cles' => 'Met behulp van trefwoorden kan je thematische verbanden leggen tussen je artikels,
 onafhankelijk van hun plaats in de rubrieken. Zo kan je
 de navigatie op je site verbeteren maar ook de weergave
 van artikels aanpassen aan de inhoud.',
	'texte_mots_cles_dans_forum' => 'Wilt u het gebruik van sleutelwoorden geselecteren door de bezoekers in de forums van de openbare site toelaten? (Aandacht : deze keuze is betrekkelijk ingewikkeld juist te gebruiken.)',
	'texte_nouveau_mot' => 'Nieuw trefwoord',
	'titre_config_groupe_mots_cles' => 'Configuratie van de groepen trefwoorden',
	'titre_gauche_mots_edit' => 'WOORD NUMMER:',
	'titre_groupe_mots' => 'Groupe de mots-cl&eacute;s', # NEW
	'titre_groupe_mots_numero' => 'GROUPE DE MOTS NUMÉRO :', # NEW
	'titre_groupes_mots' => 'Groupes de mots-cl&eacute;s', # NEW
	'titre_mots_cles_dans_forum' => 'Trefwoorden in de forums van de publieke site',
	'titre_mots_tous' => 'Trefwoorden',
	'titre_nouveau_groupe' => 'Nieuwe groep',
	'titre_objets_lies_mot' => 'Liés à ce mot-clé :', # NEW
	'titre_page_mots_tous' => 'Trefwoorden'
);

?>
