<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'info_compresseur_titre' => 'Igües y compresión',
	'info_question_activer_compactage_css' => '¿Quies activar la compautación de les fueyes d\'estilu (CSS)?',
	'info_question_activer_compactage_js' => '¿Quies activar la compautación de los scripts (JavaScript)?',
	'info_question_activer_compresseur' => '¿Quies activar la compresión del tráficu HTTP?',
	'item_compresseur' => 'Activar la compresión',
	'item_compresseur_closure' => 'Utiliser Google Closure Compiler [expérimental]', # NEW

	// T
	'texte_compacter_avertissement' => 'Cuida de nun activar estes opciones durante el desarrollo del sitiu: los elementos compactaos dexen dafechu de ser lleibles.',
	'texte_compacter_script_css' => 'SPIP pue compautar los scripts JavaScript y les fueyes d\'estilu CSS, pa guardalos como archivos estáticos; esto acelera l\'amosamientu del sitiu.',
	'texte_compresseur_page' => 'SPIP pue comprimir automáticamente toles páxines qu\'unvía a los
visitantes del sitiu. Esti axuste permite optimizar l\'anchu de banda (el
sitiu ye más rápidu tando tres d\'un enllaz de baxa capacidá), pero
requier más potencia del sirvidor.',
	'titre_compacter_script_css' => 'Compautación de los scripts y CSS',
	'titre_compresser_flux_http' => 'Compresión del fluxu HTTP'
);

?>
