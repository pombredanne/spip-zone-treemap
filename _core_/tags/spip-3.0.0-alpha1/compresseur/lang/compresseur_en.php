<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'info_compresseur_titre' => 'Optimisation and compression',
	'info_question_activer_compactage_css' => 'Do you wish to activate compression for CSS stylesheets?',
	'info_question_activer_compactage_js' => 'Do you wish to activate compression for Javascript files?',
	'info_question_activer_compresseur' => 'Do you wish to activate compression for HTTP data?',
	'item_compresseur' => 'Activate compression',
	'item_compresseur_closure' => 'Use Google Closure Compiler [experimental]',

	// T
	'texte_compacter_avertissement' => 'Be careful not to activate these options during site development, as compressed elements are difficult to read and debug.',
	'texte_compacter_script_css' => 'SPIP can compact Javascript files and CSS stylesheets and save them as static files. This makes the site display faster on browsers.',
	'texte_compresseur_page' => 'SPIP can automatically compress every page it sends. This option reduces bandwidth, making the site faster for lower speed connections), but it does require more resources from the server.',
	'titre_compacter_script_css' => 'Compression of scripts and CSS',
	'titre_compresser_flux_http' => 'Compression of HTTP data'
);

?>
