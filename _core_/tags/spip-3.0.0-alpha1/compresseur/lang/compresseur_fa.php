<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'info_compresseur_titre' => 'بهينه‌سازي و فشرده سازي',
	'info_question_activer_compactage_css' => 'آيا مايليد شيوه‌‌برگه‌ها را فشرده كنيد؟ ',
	'info_question_activer_compactage_js' => 'آيا مايليد فشرده‌سازي نويسان‌ها (جاوااسكريبت‌) را فعال كنيد؟',
	'info_question_activer_compresseur' => 'آيا مي‌خواهيد فشرده‌سازي جريان اچ.تي.تي.پي را فعال كنيد؟',
	'item_compresseur' => 'فعال سازي فشرده سازي ',
	'item_compresseur_closure' => 'Utiliser Google Closure Compiler [expérimental]', # NEW

	// T
	'texte_compacter_avertissement' => 'در جريان توسعه‌ي سايت خود مراقب باشيد اين گزينه‌ها را فعال نسازيد:‌ خطازدايي از عناصر فشرده شده دشوار مي‌‌شود.',
	'texte_compacter_script_css' => 'اسپيپ مي‌تواند فايل‌هاي جاواسكريپت و شيوه‌برگه‌هاي سي.اس.اس را فشرده كرده و در فال‌هاي استاتيك صبط كند. اين كار نمايش سايت را تسريع خواهد كرد. ',
	'texte_compresseur_page' => 'اسپيپ مي‌تواند خودكار هر صفحه را كه براي بازديدكنندگان مي‌فرستد فشرده كند. اين گزينه پهناي باند مورد استفاه را كم مي‌كند، سرعت سايت را در ارتباطات كم سرعت افزايش مي‌دهد،‌ اما منابع بيشتري را از سرور نياز خواهد داشت. ',
	'titre_compacter_script_css' => 'فشرده‌سازي نويسه‌ها و سي.اس.اس',
	'titre_compresser_flux_http' => 'فشرده‌سازي داده‌هاي HTTP'
);

?>
