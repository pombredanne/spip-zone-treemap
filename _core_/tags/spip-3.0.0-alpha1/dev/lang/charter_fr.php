<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'legend' => 'Legende d\'un fieldset, pour un groupe de saisie',
'label_checkbox_1' => 'Un premier choix',
'label_checkbox_2' => 'Un deuxième choix',
'label_checkbox_long_label_1' => 'Un premier choix',
'label_checkbox_long_label_2' => 'Un deuxième choix',
'label_text' => 'Une saisie en text',
'label_select' => 'Une saisie sous forme de select',
'label_select_oui' => 'C\'est cela, OUI',
'label_select_non' => 'Non merci, pas du tout',
'label_text_obli' => 'Une saisie obligatoire',
'label_textarea' => 'Une saisie en bloc',
'label_textarea_pleine_largeur' => 'Une saisie en bloc en <tt>.pleine_largeur</tt>',
'label_textarea_pleine_largeur_obli' => 'Une saisie obligatoire en bloc en <tt>.pleine_largeur</tt>',
'label_text_long_label' => 'Une saisie avec un tres tres long label qui utilise une class <tt>.long_label</tt>',
'label_radio' => 'Un choix unique',
'label_radio_oui' => 'Oui',
'label_radio_non' => 'Non',
'label_checkbox' => 'Un choix multiple',
'label_checkbox_long_label' => 'Un choix multiple avec un tres tres long label qui utilise une class <tt>.long_label</tt>',

'titre_boites' => 'Boites',
'titre_charte' => 'Charte',
'titre_formulaires' => 'Formulaires',
'titre_icones' => 'Icones',
'titre_listes' => 'Listes',
'titre_typo' => 'Typo',

);

?>
