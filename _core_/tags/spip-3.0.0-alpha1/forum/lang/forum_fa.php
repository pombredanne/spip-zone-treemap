<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_message_forum' => 'Aucun message de forum', # NEW

	// B
	'bouton_radio_articles_futurs' => ' ( منحصرأ براى مقالات آينده (بر روى  داده پى ها بى تأثير ميباشد',
	'bouton_radio_articles_tous' => 'به تمامى مقاله ها، بدون استثناء',
	'bouton_radio_articles_tous_sauf_forum_desactive' => 'به تمامى مقاله ها، بجز آنهائى كه سخنگاهشان غير فعال است. ',
	'bouton_radio_enregistrement_obligatoire' => 'ضبط اجبارى، (كاربران بايد, براى داشتن آدرس الكترونيكى  مشترك شوند تا بتوانند پيامهايشان را پست كنند)      ',
	'bouton_radio_moderation_priori' => 'پيامها پس از تأئيد گردانند گان سايت نمايان ميشوند',
	'bouton_radio_modere_abonnement' => 'براى مشتركين',
	'bouton_radio_modere_posteriori' => 'اعلان پيش از تأئيد',
	'bouton_radio_modere_priori' => 'اعلان پس از تأئيد',
	'bouton_radio_publication_immediate' => 'انتشار فورى پيامها 
 (پيامها بمحض فرستاده شدن نمايان ميشوند سپس گردانند گان سايت ميتوانند آنها را حذف كنند)',

	// D
	'documents_interdits_forum' => 'سندهاي ممنوع در سخناگاه ',

	// E
	'erreur_enregistrement_message' => 'Votre message n\'a pas pu être enregistré en raison d\'un problème technique', # NEW

	// F
	'form_pet_message_commentaire' => 'يك پيام ، يك تفسير ؟',
	'forum' => 'سخنگاه',
	'forum_acces_refuse' => '.شما ديگر اجازه استفاده از اين سخنگاه را نداريد',
	'forum_attention_dix_caracteres' => '</b>! توجه<b>   پيامتان بايد بيش از ده حرف باشد.',
	'forum_attention_trois_caracteres' => '</b>! توجه<b> عنوانتان بايد بيش از سه حرف باشد. ',
	'forum_attention_trop_caracteres' => '<b>توجه!</b> پیامتان بسیار طولانی است     (@compte@کلمه)  و نباید از  @max@ کلمه بیشتر باشد.',
	'forum_avez_selectionne' => ': شما انتخاب كرده ايد',
	'forum_cliquer_retour' => ' </a> اينجا <a href=\'@retour_forum@\'>     را براى ادامه كليك كنيد     ',
	'forum_forum' => 'سخنگاه',
	'forum_info_modere' => 'سخنگاه  : پيامهاى شما پس از تأئيد گردانند گان سايت نمايان خواهند شد',
	'forum_lien_hyper' => ' (اختيارى)  </b>پيوند هايپرتكست<b> ',
	'forum_message_definitif' => 'پيام نهايى : به سايت بفرستيد',
	'forum_message_trop_long' => 'پيامتان بيش از حد بلند است. حداكثر اندازه ٢٠٠٠٠ حرف است',
	'forum_ne_repondez_pas' => 'به اين ايميل مستقيماً پاسخ ندهيد-از سخنگاه وصل‌خط در نشاني زير استفاده كنيد:',
	'forum_page_url' => 'اگر پيامتان راجع به يك مقاله چاپ شده يا به يك صفحه اى كه داراى اطلاعات سودمندى است مربوط ميشود، خواهشمند است عنوان صفحه وآدرس آنرا در زير مشخص كنيد ',
	'forum_poste_par' => 'پيام @parauteur@ بدنبال مقاله « @titre@ »  فرستاده شده. ',
	'forum_qui_etes_vous' => '(اختيارى) <b> كى هستيد؟ </b>                                      ',
	'forum_texte' => ':متن پيامتان',
	'forum_titre' => ':عنوان',
	'forum_url' => 'URL :',
	'forum_valider' => 'اين انتخاب را معتبر كنيد',
	'forum_voir_avant' => 'اين پيام را پيش از فرستادن آن بخوانيد',
	'forum_votre_email' => ':آدرس ايميل تان',
	'forum_votre_nom' => '(نام  يا (نام مستعارتان  ',
	'forum_vous_enregistrer' => ' براى شركت در اين سخنگاه، شما بايد از پيش ثبت نام ميكرديد. خواهشمند است كلمه شناساييتان را كه دريافت كرده ايد در زير مشخص كنيد.اگر ثبت نشديد، بايد',
	'forum_vous_inscrire' => 'ثبت نام كنيد.   ',

	// I
	'icone_bruler_message' => 'Signaler comme Spam', # NEW
	'icone_bruler_messages' => 'Signaler comme Spam', # NEW
	'icone_legitimer_message' => 'Signaler comme licite', # NEW
	'icone_poster_message' => 'يك پيام بفرستيد',
	'icone_suivi_forum' => '                                                                                                  دنباله سخنگاه همگانى : @nb_forums@ پيام                                                       ',
	'icone_suivi_forums' => 'پيگيرى/اداره كردن سخنگاه',
	'icone_supprimer_message' => 'اين پيام را حذف كنيد',
	'icone_supprimer_messages' => 'Supprimer ces messages', # NEW
	'icone_valider_message' => 'اين پيام را معتبر كنيد',
	'icone_valider_messages' => 'Valider ces messages', # NEW
	'icone_valider_repondre_message' => 'Valider &amp; Répondre à ce message', # NEW
	'info_1_message_forum' => '1 message de forum', # NEW
	'info_activer_forum_public' => '<i>براى فعال كردن سخنگاه همگانى، خواهشمند است شيوه اعتبار پس از تأئيد را انتخاب نمائيد</i>',
	'info_appliquer_choix_moderation' => ': اين انتخاب معتدل را بكار بريد',
	'info_config_forums_prive' => 'در قسمت شخصي سايت مي‌توانيد چند نوع سخنگاه را فعال كنيد:',
	'info_config_forums_prive_admin' => 'يك سخنگاه اختصاصي براي مديران سايت:',
	'info_config_forums_prive_global' => 'يك سخنگاه‌ي كلي، گشوده به روي تمام نويسندگان:',
	'info_config_forums_prive_objets' => 'يك سخناگاه براي هر مقاله، خبر و سايت‌ ارجاعي و غيره:',
	'info_desactiver_forum_public' => 'استفاده از سخنگاه همگانى را غير فعال كنيد. استفاده از آن ميتواند مورد به مورد درباره مقالات داده شود، ولى در موارد ديگر ممنوع خواهد بود',
	'info_envoi_forum' => 'ارسال سخنگاه به نويسند گان مقاله ها',
	'info_fonctionnement_forum' => ': عملكرد سخنگاه',
	'info_forums_liees_mot' => 'Les messages de forum liés à ce mot', # NEW
	'info_gauche_suivi_forum_2' => 'صفحه دنباله سخنگاه از ابزار اداره سايتتان ميباشد (و نه مكانى براى بحث يا نگارش) و امكان اعلان تمام پيامها ى سخنگاه همگانى و خصوصى و همچنين اداره پيامها را بشما ميدهد.',
	'info_liens_syndiques_3' => 'سخنگاه',
	'info_liens_syndiques_4' => 'هستند',
	'info_liens_syndiques_5' => 'سخنگاه',
	'info_liens_syndiques_6' => 'هست',
	'info_liens_syndiques_7' => 'در انتظار تأئيد',
	'info_liens_texte' => 'Lien(s) contenu(s) dans le texte du message', # NEW
	'info_liens_titre' => 'Lien(s) contenu(s) dans le titre du message', # NEW
	'info_mode_fonctionnement_defaut_forum_public' => 'طرز عملكرد سخنگاه هاى عمومى',
	'info_nb_messages_forum' => '@nb@ messages de forum', # NEW
	'info_option_email' => 'هنگاميكه يكى از بينند گان سايت پيامى در سخنگاه راجع به مقاله‌اى مي‌فرستد، نويسند گان مقاله  مي‌توانند از آن آگاه شوند. آيا مي‌خواهيد اين گزينه را داشته باشيد؟',
	'info_pas_de_forum' => 'بدون سخنگاه',
	'info_question_visiteur_ajout_document_forum' => 'اگر مايليد به بازديد‌كنندگان اجازه دهيد كه سند‌هايي (تصوير، صدا و . . . ) را به پيام‌هايشان در سخنگاه بچسبانند، فهرست توسعه‌هاي مجاز براي سند‌ها را براي سخنگاه‌ها مشخص كنيد (ex: gif, jpg, png, mp3).',
	'info_question_visiteur_ajout_document_forum_format' => 'اگر مايليد به تمام انواع سند‌هايي كه مورد اعتماد اسپيپ است اجازه بدهيد، يك ستاره بگذاريد. اگر اجازه نمي‌دهيد هيچ كار نكنيد.',
	'interface_formulaire' => 'Interface formulaire', # NEW
	'interface_onglets' => 'Interface avec onglets', # NEW
	'item_activer_forum_administrateur' => 'سخنگاه گردانندگان سايت را فعال كنيد',
	'item_config_forums_prive_global' => 'فعال سازي سخنگاه نويسندگان',
	'item_config_forums_prive_objets' => 'فعال سازي اين سخنگاه‌ها',
	'item_desactiver_forum_administrateur' => 'سخنگاه گردانندگان سايت را غير فعال كنيد',
	'item_non_config_forums_prive_global' => 'غيرفعال سازي سخنگاه نويسندگان',
	'item_non_config_forums_prive_objets' => 'غيرفعال سازي اين سخنگاه‌ها',

	// L
	'lien_reponse_article' => 'پاسخ به مقاله',
	'lien_reponse_breve_2' => 'پاسخ به مقاله كوتاه',
	'lien_reponse_rubrique' => 'پاسخ به بخش',
	'lien_reponse_site_reference' => ':پاسخ به سايت مرجع',

	// M
	'messages_aucun' => 'Aucun', # NEW
	'messages_meme_auteur' => 'Tous les messages de cet auteur', # NEW
	'messages_meme_email' => 'Tous les messages de cet email', # NEW
	'messages_meme_ip' => 'Tous les messages de cette IP', # NEW
	'messages_off' => 'Supprimés', # NEW
	'messages_perso' => 'Personnels', # NEW
	'messages_privadm' => 'Administrateurs', # NEW
	'messages_prive' => 'Privés', # NEW
	'messages_privoff' => 'Supprimés', # NEW
	'messages_privrac' => 'Généraux', # NEW
	'messages_prop' => 'Proposés', # NEW
	'messages_publie' => 'Publiés', # NEW
	'messages_spam' => 'Spam', # NEW
	'messages_tous' => 'Tous', # NEW

	// O
	'onglet_messages_internes' => 'پيام هاى داخلى',
	'onglet_messages_publics' => 'پيام هاى همگانى',
	'onglet_messages_vide' => 'پيام هاى بدون متن',

	// R
	'repondre_message' => 'پاسخ به اين پيام',

	// S
	'statut_off' => 'Supprimé', # NEW
	'statut_original' => 'اصلی',
	'statut_prop' => 'Proposé', # NEW
	'statut_publie' => 'Publié', # NEW
	'statut_spam' => 'Spam', # NEW

	// T
	'text_article_propose_publication_forum' => 'از طريق سخنگاه پيوست در پائين همين صفحه نظرتان را مورد اين مقاله بيان كنيد.',
	'texte_en_cours_validation' => 'Les articles, brèves, forums ci dessous sont proposés à la publication.', # NEW
	'texte_en_cours_validation_forum' => 'مي‌توانيد از طريق سخنگاه‌هاي پيوست شده به‌ آن‌ها نظراتتان را بيان كنيد.',
	'texte_messages_publics' => 'Messages publics sur&nbsp;:', # NEW
	'titre_cadre_forum_administrateur' => 'سخنگاه خصوصى گردانندگان سايت',
	'titre_cadre_forum_interne' => 'سخنگاه داخلى',
	'titre_config_forums_prive' => 'سخنگاه‌هاي قسمت شخصي',
	'titre_forum' => 'سخنگاه',
	'titre_forum_suivi' => 'دنباله سخنگاه ',
	'titre_page_forum_suivi' => 'دنباله سخنگاه',
	'titre_selection_action' => 'Sélection', # NEW
	'tout_voir' => 'Voir tous les messages', # NEW

	// V
	'voir_messages_objet' => 'voir les messages' # NEW
);

?>
