<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_message_forum' => 'Aucun message de forum', # NEW

	// B
	'bouton_radio_articles_futurs' => 'только для новых статей(текущие настройки не изменяться).',
	'bouton_radio_articles_tous' => 'ко всем статьям без исключения.',
	'bouton_radio_articles_tous_sauf_forum_desactive' => 'ко всем статьям, за исключением закрытых  форумов.',
	'bouton_radio_enregistrement_obligatoire' => 'Требуется регистрация (
 пользователи должны подписаться, предоставляя свой электронный адрес, перед 
  возможностью публикации статьи).',
	'bouton_radio_moderation_priori' => 'Пре-модерация(
 статьи будут показываться только после утверждения 
 администраторами).',
	'bouton_radio_modere_abonnement' => 'запрос на регистрацию',
	'bouton_radio_modere_posteriori' => 'пост-модерация',
	'bouton_radio_modere_priori' => 'пре-модерация',
	'bouton_radio_publication_immediate' => 'Немедленная публикация сообщений 
(статьи будут показываться, как только они будут отправлены. Администраторы могут 
 потом удалить их).',

	// D
	'documents_interdits_forum' => 'Запрещенно добавлять документы к сообщениям форума',

	// E
	'erreur_enregistrement_message' => 'Votre message n\'a pas pu être enregistré en raison d\'un problème technique', # NEW

	// F
	'form_pet_message_commentaire' => 'Любое сообщение или комментарий?',
	'forum' => 'Форум',
	'forum_acces_refuse' => 'Вы больше не имеете доступа к этим форумам.',
	'forum_attention_dix_caracteres' => '<b>Внимание!</b> Сообщение должно быть не менее десяти символов.',
	'forum_attention_trois_caracteres' => '<b>Внимание!</b> Заголовок должен быть не менее трех символов.',
	'forum_attention_trop_caracteres' => '<b>Внимание !</b> Ваше сообщение слишком длинно (@compte@ букв) : чтобы было можно его сохранить, сообщение должно состоять не  более чем из @max@ букв.',
	'forum_avez_selectionne' => 'Вы выбрали:',
	'forum_cliquer_retour' => 'Нажмите  <a href=\'@retour_forum@\'>сюда</a> для продолжения.',
	'forum_forum' => 'форум',
	'forum_info_modere' => 'Этот форум модерируется заранее: ваше сообщение появиться после того как его утвердит администратор.',
	'forum_lien_hyper' => '<b>Ссылка</b> (необязательно)',
	'forum_message_definitif' => 'Всё верно! Отправить!',
	'forum_message_trop_long' => 'Ваше сообщение слишком длинно. Максимальная длина 20000 символов.',
	'forum_ne_repondez_pas' => 'Не отвечайте на эту почту  на форуме в следующих адресеах:',
	'forum_page_url' => '(Если ваше сообщение обращено к статьям изданным в сети или к странице, обеспечивающей дальнейшую информацию, пожалуйста введите заголовок страницы и ее URL ниже).',
	'forum_poste_par' => '@parauteur@ прокомментировал вашу статью.',
	'forum_qui_etes_vous' => '<b>Кто вы?</b> (необязательно)',
	'forum_texte' => 'Текст сообщения:',
	'forum_titre' => 'Тема:',
	'forum_url' => 'URL:',
	'forum_valider' => 'Подтвердите выбор',
	'forum_voir_avant' => 'Предварительный просмотр сообщения перед отправлением',
	'forum_votre_email' => 'Ваш e-mail адрес:',
	'forum_votre_nom' => 'Ваше имя (или псевдоним):',
	'forum_vous_enregistrer' => 'Для участия в
  этом форуме, вы должны зарегистрироваться. Введите персоналый  идентификатор,
  высланный вам.',
	'forum_vous_inscrire' => 'регистрация.',

	// I
	'icone_bruler_message' => 'Signaler comme Spam', # NEW
	'icone_bruler_messages' => 'Signaler comme Spam', # NEW
	'icone_legitimer_message' => 'Signaler comme licite', # NEW
	'icone_poster_message' => 'Разместить сообщение',
	'icone_suivi_forum' => 'Продолжение общественного форума: @nb_forums@ статья (статьи)',
	'icone_suivi_forums' => 'Управление форумами',
	'icone_supprimer_message' => 'Удалить это сообщение',
	'icone_supprimer_messages' => 'Supprimer ces messages', # NEW
	'icone_valider_message' => 'Подтвердить сообщение',
	'icone_valider_messages' => 'Valider ces messages', # NEW
	'icone_valider_repondre_message' => 'Valider &amp; Répondre à ce message', # NEW
	'info_1_message_forum' => '1 message de forum', # NEW
	'info_activer_forum_public' => '<i>Для включения публичных форумов, пожалуйста выберите модерацию: </i>',
	'info_appliquer_choix_moderation' => 'Применить этот тип модерации:',
	'info_config_forums_prive' => 'В области редактирования сайта может быть включено несколько видов форума:',
	'info_config_forums_prive_admin' => 'Форум для администраторов сайта:',
	'info_config_forums_prive_global' => 'Общий форум, открыт для всех авторов:',
	'info_config_forums_prive_objets' => 'Форум, прикрепленный к каждой статье, новости, ссылочному сайту, и т. д.:',
	'info_desactiver_forum_public' => '<MODIF>Отключить основные 
 форумы. Основные форумы могут быть разрешены в зависимости от конкретного случая 
  основы статей; они будут запрещены для разделов, новостей, и т.д.',
	'info_envoi_forum' => 'Отправлять сообщения форумов авторам статей',
	'info_fonctionnement_forum' => 'Работа форума:',
	'info_forums_liees_mot' => 'Les messages de forum liés à ce mot', # NEW
	'info_gauche_suivi_forum_2' => '<i>Продолжение форумов </i> страница - инструмент управления Вашего сайта (не обсуждается или редактируется). Она показывает все записи основного форума этой статьи и позволяет Вам управлять этими записями.',
	'info_liens_syndiques_3' => 'форумы',
	'info_liens_syndiques_4' => 'есть',
	'info_liens_syndiques_5' => 'форум',
	'info_liens_syndiques_6' => 'есть',
	'info_liens_syndiques_7' => 'ожидание утверждения.',
	'info_liens_texte' => 'Lien(s) contenu(s) dans le texte du message', # NEW
	'info_liens_titre' => 'Lien(s) contenu(s) dans le titre du message', # NEW
	'info_mode_fonctionnement_defaut_forum_public' => 'Стандартный режим работы основных форумов',
	'info_nb_messages_forum' => '@nb@ messages de forum', # NEW
	'info_option_email' => 'Когда посетитель сайта размещает сообщение на 
 форуме  связанного со статьей, автор статьи может быть 
 проинформирован о нем по электронной почте. Укажите для каждого вида форума, если эта опция должна быть включена.',
	'info_pas_de_forum' => 'нет форума',
	'info_question_visiteur_ajout_document_forum' => 'Если Вы хотите разрешить Вашим посетителям прикреплять документы (изображения, звуковые файлы, ...) к ихним сообщениям форума, укажите ниже список расширения имени файла, которые являются авторизованными(e.g. gif, jpg, png, mp3).',
	'info_question_visiteur_ajout_document_forum_format' => 'Если Вы хотите включить все типы документов, которые позволяет SPIP, тогда поставьте звездочку. Чтобы не разрешить типы файлов - оставьте пустым.',
	'interface_formulaire' => 'Interface formulaire', # NEW
	'interface_onglets' => 'Interface avec onglets', # NEW
	'item_activer_forum_administrateur' => 'Включить форум администраторов',
	'item_config_forums_prive_global' => 'Включить форум авторов',
	'item_config_forums_prive_objets' => 'Включить эти форумы',
	'item_desactiver_forum_administrateur' => 'Отключить форум администраторов',
	'item_non_config_forums_prive_global' => 'Отключить форум авторов',
	'item_non_config_forums_prive_objets' => 'отключить эти форумы',

	// L
	'lien_reponse_article' => 'Ответить на статью',
	'lien_reponse_breve_2' => 'Ответить на новость',
	'lien_reponse_rubrique' => 'Ответить на раздел',
	'lien_reponse_site_reference' => 'Ответить на ссылающийся сайт:',

	// M
	'messages_aucun' => 'Aucun', # NEW
	'messages_meme_auteur' => 'Tous les messages de cet auteur', # NEW
	'messages_meme_email' => 'Tous les messages de cet email', # NEW
	'messages_meme_ip' => 'Tous les messages de cette IP', # NEW
	'messages_off' => 'Supprimés', # NEW
	'messages_perso' => 'Personnels', # NEW
	'messages_privadm' => 'Administrateurs', # NEW
	'messages_prive' => 'Privés', # NEW
	'messages_privoff' => 'Supprimés', # NEW
	'messages_privrac' => 'Généraux', # NEW
	'messages_prop' => 'Proposés', # NEW
	'messages_publie' => 'Publiés', # NEW
	'messages_spam' => 'Spam', # NEW
	'messages_tous' => 'Tous', # NEW

	// O
	'onglet_messages_internes' => 'Внутреняя переписка',
	'onglet_messages_publics' => 'Общественные сообщения',
	'onglet_messages_vide' => 'Сообщения без текста',

	// R
	'repondre_message' => 'Ответить на это сообщение',

	// S
	'statut_off' => 'Supprimé', # NEW
	'statut_original' => 'Исходник',
	'statut_prop' => 'Proposé', # NEW
	'statut_publie' => 'Publié', # NEW
	'statut_spam' => 'Spam', # NEW

	// T
	'text_article_propose_publication_forum' => 'Вы можете прокомментировать эту статью, используя прикрепленный форум (внизу страницы).',
	'texte_en_cours_validation' => 'Les articles, brèves, forums ci dessous sont proposés à la publication.', # NEW
	'texte_en_cours_validation_forum' => 'Вы можете оставлять Ваши комментарии, используя прикрепленные к ним форумы',
	'texte_messages_publics' => 'Messages publics sur&nbsp;:', # NEW
	'titre_cadre_forum_administrateur' => 'Административный форум администраторов',
	'titre_cadre_forum_interne' => 'Внутренний форум',
	'titre_config_forums_prive' => 'Форумы в редакторской части',
	'titre_forum' => 'Форум',
	'titre_forum_suivi' => 'Дополнительные форумы',
	'titre_page_forum_suivi' => 'Дополнительные форумы',
	'titre_selection_action' => 'Sélection', # NEW
	'tout_voir' => 'Voir tous les messages', # NEW

	// V
	'voir_messages_objet' => 'voir les messages' # NEW
);

?>
