<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_message_forum' => 'Aucun message de forum', # NEW

	// B
	'bouton_radio_articles_futurs' => 'len pre budúce články (žiadna zmena databázy).',
	'bouton_radio_articles_tous' => 'pre všetky články bez výnimky.',
	'bouton_radio_articles_tous_sauf_forum_desactive' => 'pre všetky články, okrem tých s deaktivovanými diskusnými fórami.',
	'bouton_radio_enregistrement_obligatoire' => 'Vyžaduje sa registrácia (
  používatelia sa musia zaregistrovať uvedením svojej e-mailovej adresy predtým,
  ako budú môcť posielať príspevky).',
	'bouton_radio_moderation_priori' => 'Kontrola pred zverejnením (
 príspevky sa zobrazia až potom, čo ich
 schvália administrátori).',
	'bouton_radio_modere_abonnement' => 'Vyžaduje sa registrácia',
	'bouton_radio_modere_posteriori' => 'Kontrola po zverejnení',
	'bouton_radio_modere_priori' => 'Kontrola pred zverejnením',
	'bouton_radio_publication_immediate' => 'Okamžité publikovanie príspevkov
 (príspevky sa zobrazia len čo budú odoslané; ak bude treba, administrátori
 ich môžu potom odstrániť).',

	// D
	'documents_interdits_forum' => 'Dokumenty nie sú v tomto diskusnom fóre povolené',

	// E
	'erreur_enregistrement_message' => 'Votre message n\'a pas pu être enregistré en raison d\'un problème technique', # NEW

	// F
	'form_pet_message_commentaire' => 'Nejaký príspevok alebo komentáre?',
	'forum' => 'Diskusné fórum',
	'forum_acces_refuse' => 'K týmto diskusným fóram už viac nemáte prístup.',
	'forum_attention_dix_caracteres' => '<b>Upozornenie!</b> Váš príspevok musí mať aspoň 10 znakok.',
	'forum_attention_trois_caracteres' => '<b>Upozornenie!</b> Názov musí mať aspoň 3 znaky.',
	'forum_attention_trop_caracteres' => '<b>Warning !</b> your message is too long (@compte@ characters) : to be able to save it, the message should not contain more than @max@ characters.',
	'forum_avez_selectionne' => 'Vybrali ste:',
	'forum_cliquer_retour' => 'Kliknite <a href=\'@retour_forum@\'>sem,</a> aby ste mohli pokračovať.',
	'forum_forum' => 'diskusné fórum',
	'forum_info_modere' => 'Toto diskusné fórum sa kontroluje pred zverejnením: váš príspevok sa zobrazí až potom, čo ho schváli administrátor stránky.',
	'forum_lien_hyper' => '<b>Hypertextový odkaz</b> (nepovinné)',
	'forum_message_definitif' => 'Koniec úprav: poslať príspevok',
	'forum_message_trop_long' => 'Vaša správa je pridlhá. Maximálna dĺžka je 20000 znakov.',
	'forum_ne_repondez_pas' => 'Neodpovedajte na tento mejl priamo - prosím, použite diskusné fórum:',
	'forum_page_url' => '(If your message refers to an article published on the web or to a page providing further information, please enter the title of the page and its URL below).',
	'forum_poste_par' => 'Príspevky @parauteur@ k vášmu článku "@titre@".',
	'forum_qui_etes_vous' => '<b>Kto ste?</b> (voliteľné)',
	'forum_texte' => 'Text vášho príspevku:',
	'forum_titre' => 'Predmet:',
	'forum_url' => 'Adresa stránky:',
	'forum_valider' => 'Potvrdiť výber',
	'forum_voir_avant' => 'Zobraziť príspevok pred odoslaním',
	'forum_votre_email' => 'Vaša e-mailová adresa:',
	'forum_votre_nom' => 'Vaše meno (alebo alias):',
	'forum_vous_enregistrer' => 'Na to, aby ste mohli diskutovať v tomto diskusnom fóre, musíte byť zaregistrovaný.
 Prosím, zadajte svoj osobný identifikátor.
  ak ste sa ešte nezaregistrovali, musíte',
	'forum_vous_inscrire' => 'sa zaregistrovať.',

	// I
	'icone_bruler_message' => 'Signaler comme Spam', # NEW
	'icone_bruler_messages' => 'Signaler comme Spam', # NEW
	'icone_legitimer_message' => 'Signaler comme licite', # NEW
	'icone_poster_message' => 'Poslať príspevok',
	'icone_suivi_forum' => 'Sledovanie verejného diskusného fóra: @nb_forums@ príspevok (-kov)',
	'icone_suivi_forums' => 'Riadiť diskusné fóra',
	'icone_supprimer_message' => 'Odstrániť tento príspevok',
	'icone_supprimer_messages' => 'Supprimer ces messages', # NEW
	'icone_valider_message' => 'Schváliť príspevok',
	'icone_valider_messages' => 'Valider ces messages', # NEW
	'icone_valider_repondre_message' => 'Valider &amp; Répondre à ce message', # NEW
	'info_1_message_forum' => '1 message de forum', # NEW
	'info_activer_forum_public' => '<i>Ak chcete povoliť verejné diskusné fóra, prosím, uveďte typ ich riadenia:</i>',
	'info_appliquer_choix_moderation' => 'Použiť tento výber riadenia:',
	'info_config_forums_prive' => 'V súkromnej zóne môžete aktivovať rôzne typy diskusných fór:',
	'info_config_forums_prive_admin' => 'Diskusné fórum vyhradené pre administrátorov:',
	'info_config_forums_prive_global' => 'Globálne diskusné fórum otvorené všetkým autorom:',
	'info_config_forums_prive_objets' => 'Diskusné fórum pripojené ku každému článku, každej novinke, odkazovanej stránke, atď.:',
	'info_desactiver_forum_public' => 'Disable the use of public
	forums. Public forums could be allowed on a case by case
	basis for the articles; they will be forbidden for the sections, news, etc.',
	'info_envoi_forum' => 'Posielať príspevky z diskusného fóra príslušným autorom',
	'info_fonctionnement_forum' => 'Operácia diskusného fóra:',
	'info_forums_liees_mot' => 'Les messages de forum liés à ce mot', # NEW
	'info_gauche_suivi_forum_2' => 'Stránka na <i>sledovanie diskusných fór</i> je nástroj na riadenie vašej stránky (nie diskusií alebo úprav). Zobrazuje všetky príspevky do diskusných fór (tak na verejnej stránke, ako aj v súkromnej zóne) a umožňuje vám rozhodovať o týchto príspevkoch.',
	'info_liens_syndiques_3' => 'diskusné fóra',
	'info_liens_syndiques_4' => 'je/sú',
	'info_liens_syndiques_5' => 'diskusné fórum',
	'info_liens_syndiques_6' => 'je',
	'info_liens_syndiques_7' => 'čaká na schválenie.',
	'info_liens_texte' => 'Lien(s) contenu(s) dans le texte du message', # NEW
	'info_liens_titre' => 'Lien(s) contenu(s) dans le titre du message', # NEW
	'info_mode_fonctionnement_defaut_forum_public' => 'Predvolený režim verejných diskusných fór',
	'info_nb_messages_forum' => '@nb@ messages de forum', # NEW
	'info_option_email' => 'Keď návštevník pošle príspevok do diskusného fóra článku, autor článku môže byť o tom informovaný e-mailom. Pre každý typ diskusného fóra uveďte, či sa má táto možnosť aktivovať.',
	'info_pas_de_forum' => 'žiadne diskusné fórum',
	'info_question_visiteur_ajout_document_forum' => 'Ak chcete povoliť návštevníkom pripájať dokumenty (a obrázky, zvukové súbory,...) k diskusným príspevkom, určte, ktoré prípony súborov budú akceptované (napr. gif, jpg, png, mp3).',
	'info_question_visiteur_ajout_document_forum_format' => 'Ak chcete povoliť dokumenty všetkých typov, ktoré SPIP považuje za bezpečné, použite hviezdičku. Ak chcete všetky zablokovať, nechajte pole prázdne.',
	'interface_formulaire' => 'Interface formulaire', # NEW
	'interface_onglets' => 'Interface avec onglets', # NEW
	'item_activer_forum_administrateur' => 'Povoliť diskusné fórum administrátorov',
	'item_config_forums_prive_global' => 'Aktivovať diskusné fórum autorov',
	'item_config_forums_prive_objets' => 'Aktivovať tieto diskusné fóra',
	'item_desactiver_forum_administrateur' => 'Deaktivovať diskusné fórum administrátorov',
	'item_non_config_forums_prive_global' => 'Deaktivovať diskusné fórum autorov',
	'item_non_config_forums_prive_objets' => 'Deaktivovať tieto diskusné fóra',

	// L
	'lien_reponse_article' => 'Komentovať tento článok',
	'lien_reponse_breve_2' => 'Komentovať túto novinku',
	'lien_reponse_rubrique' => 'Komentovať túto rubriku',
	'lien_reponse_site_reference' => 'Komentovať túto webovú stránku:',

	// M
	'messages_aucun' => 'Aucun', # NEW
	'messages_meme_auteur' => 'Tous les messages de cet auteur', # NEW
	'messages_meme_email' => 'Tous les messages de cet email', # NEW
	'messages_meme_ip' => 'Tous les messages de cette IP', # NEW
	'messages_off' => 'Supprimés', # NEW
	'messages_perso' => 'Personnels', # NEW
	'messages_privadm' => 'Administrateurs', # NEW
	'messages_prive' => 'Privés', # NEW
	'messages_privoff' => 'Supprimés', # NEW
	'messages_privrac' => 'Généraux', # NEW
	'messages_prop' => 'Proposés', # NEW
	'messages_publie' => 'Publiés', # NEW
	'messages_spam' => 'Spam', # NEW
	'messages_tous' => 'Tous', # NEW

	// O
	'onglet_messages_internes' => 'Súkromné správy',
	'onglet_messages_publics' => 'Verejné správy',
	'onglet_messages_vide' => 'Správy bez textu',

	// R
	'repondre_message' => 'Odpovedať na túto správu',

	// S
	'statut_off' => 'Supprimé', # NEW
	'statut_original' => 'pôvodný',
	'statut_prop' => 'Proposé', # NEW
	'statut_publie' => 'Publié', # NEW
	'statut_spam' => 'Spam', # NEW

	// T
	'text_article_propose_publication_forum' => 'K tomuto článku môžete napísať komentár v diskusnom fóre, ktoré sa nachádza pod ním.',
	'texte_en_cours_validation' => 'Les articles, brèves, forums ci dessous sont proposés à la publication.', # NEW
	'texte_en_cours_validation_forum' => 'V pripojených diskusných fórach môžete smelo napísať komentár.',
	'texte_messages_publics' => 'Messages publics sur&nbsp;:', # NEW
	'titre_cadre_forum_administrateur' => 'Súkromné diskusné fórum administrátorov',
	'titre_cadre_forum_interne' => 'Interné diskusné fórum',
	'titre_config_forums_prive' => 'Diskusné fóra v súkromnej zóne',
	'titre_forum' => 'Diskusné fórum',
	'titre_forum_suivi' => 'Sledovanie diskusných fór',
	'titre_page_forum_suivi' => 'Sledovanie diskusných fór',
	'titre_selection_action' => 'Sélection', # NEW
	'tout_voir' => 'Voir tous les messages', # NEW

	// V
	'voir_messages_objet' => 'voir les messages' # NEW
);

?>
