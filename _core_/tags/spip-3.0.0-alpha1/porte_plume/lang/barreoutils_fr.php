<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_core_/plugins/porte_plume/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'barre_a_accent_grave' => 'Ins&eacute;rer un A accent grave majuscule',
	'barre_adresse' => 'Adresse',
	'barre_aide' => 'Utilisez les raccourcis typographiques pour enrichir votre mise en page',
	'barre_alignerdroite' => '[/Aligne &agrave; droite/] le paragraphe',
	'barre_ancres' => 'Gestion des ancres',
	'barre_barre' => 'Barrer le texte',
	'barre_bulle' => 'Bulle d\'aide',
	'barre_c_cedille_maj' => 'Ins&eacute;rer un C c&eacute;dille majuscule',
	'barre_cadre' => 'Placer dans une &lt;cadre&gt;zone de saisie de texte&lt;/cadre&gt;',
	'barre_caracteres' => 'Caract&egrave;res sp&eacute;ciaux',
	'barre_centrer' => '[|Centrer|] le paragraphe',
	'barre_chercher' => 'Chercher Remplacer',
	'barre_clean' => 'Nettoyer le code de toutes les balises HTML',
	'barre_code' => 'Mettre en forme un &lt;code&gt;code informatique&lt;/code&gt;',
	'barre_desindenter' => 'D&eacute;sindenter une liste',
	'barre_e_accent_aigu' => 'Ins&eacute;rer un E accent aigu majuscule',
	'barre_e_accent_grave' => 'Ins&eacute;rer un E majuscule accent grave',
	'barre_ea' => 'Ins&eacute;rer un E dans l\'A',
	'barre_ea_maj' => 'Ins&eacute;rer un E dans l\'A majuscule',
	'barre_encadrer' => '[(Encadrer)] le paragraphe',
	'barre_eo' => 'Ins&eacute;rer un E dans l\'O',
	'barre_eo_maj' => 'Ins&eacute;rer un E dans l\'O majuscule',
	'barre_euro' => 'Ins&eacute;rer le symbole &euro;',
	'barre_exposant' => 'Mettre le texte en &lt;sup&gt;exposant&lt;/sup&gt;',
	'barre_formatages_speciaux' => 'Formatage sp&eacute;ciaux',
	'barre_galerie' => 'Ouvrir la galerie',
	'barre_gestion_anc_bulle' => 'Bulle d\'aide ancre',
	'barre_gestion_anc_caption' => 'Gestion des ancres',
	'barre_gestion_anc_cible' => 'Ancre cible',
	'barre_gestion_anc_inserer' => 'Transformer en ancre',
	'barre_gestion_anc_nom' => 'Nom de l\'ancre',
	'barre_gestion_anc_pointer' => 'Pointer vers une ancre',
	'barre_gestion_caption' => 'Caption et R&eacute;sum&eacute;',
	'barre_gestion_colonne' => 'Nb colonnes',
	'barre_gestion_cr_casse' => 'Respecter la casse',
	'barre_gestion_cr_changercasse' => 'Changer la casse',
	'barre_gestion_cr_changercassemajuscules' => 'Passer en majuscules',
	'barre_gestion_cr_changercasseminuscules' => 'Passer en minuscules',
	'barre_gestion_cr_chercher' => 'Chercher',
	'barre_gestion_cr_entier' => 'Mot entier',
	'barre_gestion_cr_remplacer' => 'Remplacer',
	'barre_gestion_cr_tout' => 'Tout remplacer',
	'barre_gestion_entete' => 'Entete',
	'barre_gestion_ligne' => 'Nb lignes',
	'barre_gestion_taille' => 'Taille fixe',
	'barre_glossaire' => 'Entr&eacute;e du [?glossaire] (Wikipedia)',
	'barre_gras' => 'Mettre en {{gras}}',
	'barre_guillemets' => 'Entourer de &laquo; guillemets &raquo;',
	'barre_guillemets_simples' => 'Entourer de &ldquo;guillemets de second niveau&rdquo;',
	'barre_indenter' => 'Indenter une liste',
	'barre_inserer_caracteres' => 'Ins&eacute;rer des caract&egrave;res sp&eacute;cifiques',
	'barre_intertitre' => 'Transformer en {{{intertitre}}}',
	'barre_italic' => 'Mettre en {italique}',
	'barre_langue' => 'Langue abr&eacute;g&eacute;e',
	'barre_lien' => 'Transformer en [lien hypertexte->http://...]',
	'barre_lien_externe' => 'Lien externe',
	'barre_lien_input' => 'Veuillez indiquer l\'adresse de votre lien (vous pouvez indiquer une adresse Internet sous la forme http://www.monsite.com, une adresse courriel, ou simplement indiquer le num&eacute;ro d\'un article de ce site.',
	'barre_liste_ol' => 'Mettre en liste num&eacute;rot&eacute;e',
	'barre_liste_ul' => 'Mettre en liste',
	'barre_lorem_ipsum' => 'Ins&eacute;rer un paragraphe factice',
	'barre_lorem_ipsum_3' => 'Ins&eacute;rer trois paragraphes factices',
	'barre_miseenevidence' => 'Mettre le texte en [*&eacute;vidence*]',
	'barre_note' => 'Transformer en [[Note de bas de page]]',
	'barre_paragraphe' => 'Cr&eacute;er un paragraphe',
	'barre_petitescapitales' => 'Mettre le texte en &lt;sc&gt;petites capitales&lt;/sc&gt;',
	'barre_poesie' => 'Mettre en forme comme une &lt;poesie&gt;po&eacute;sie&lt;/poesie&gt;',
	'barre_preview' => 'Mode pr&eacute;visualisation',
	'barre_quote' => '<quote>Citer un message</quote>',
	'barre_stats' => 'Afficher les statistiques du texte',
	'barre_tableau' => 'Ins&eacute;rer/modifier (le s&eacute;lectionner avant) un tableau',

	// C
	'config_info_enregistree' => 'Configuration sauvegard&eacute;e',

	// E
	'editer' => '&Eacute;diter',
	'explication_barre_outils_public' => 'Les scripts CSS et Javascript des barre d\'outils
	(extension Porte Plume) sont charg&eacute;s sur l\'espace public
	et permettent d\'utiliser ces barres sur les formulaires de forums,
	les crayons publics ou pour d\'autres plugins, si leurs configurations
	respectives le permettent.',
	'explication_barre_outils_public_2' => 'Vous pouvez choisir de ne pas charger
	ces scripts afin d\'all&eacute;ger le poids des pages publiques.
	D&egrave;s lors quelque soit la configuration des forums, crayons ou plugin,
	aucune barre d\'outils du Porte Plume ne pourra &ecirc;tre
	pr&eacute;sente automatiquement sur l\'espace public.',

	// I
	'info_barre_outils_public' => 'Barre d\'outils publique',
	'info_porte_plume_titre' => 'Configurer les barres d\'outils',

	// L
	'label_barre_outils_public_non' => 'Ne pas charger les scripts de barre d\'outils sur l\'espace public',
	'label_barre_outils_public_oui' => 'Charger les scripts de barre d\'outils sur l\'espace public',

	// V
	'voir' => 'Voir'
);

?>
