<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'barre_a_accent_grave' => 'Inserisci una A con accento grave maiuscolo', # MODIF
	'barre_adresse' => 'Indirizzo',
	'barre_aide' => 'Utilizza le scorciatoie tipografiche per arrichhire la tua impaginazione',
	'barre_alignerdroite' => '[/Allinea a destra/] il paragrafo', # MODIF
	'barre_ancres' => 'Gestione delle ancore',
	'barre_barre' => 'Barra il testo',
	'barre_bulle' => 'Tooltip',
	'barre_c_cedille_maj' => 'Inserisci una C con cediglia maiuscola', # MODIF
	'barre_cadre' => 'Metti dentro una &lt;cadre&gt;zona di inserimento del testo&lt;/cadre&gt;',
	'barre_caracteres' => 'Caratteri speciali', # MODIF
	'barre_centrer' => '[|Centra|] il paragrafo',
	'barre_chercher' => 'Cerca e sostituisci',
	'barre_clean' => 'Elimina dal codice tutti i tag HTML',
	'barre_code' => 'Impagina un &lt;code&gt;codice informatico&lt;/code&gt;',
	'barre_desindenter' => 'Elimina l\'indentazione dela lista', # MODIF
	'barre_e_accent_aigu' => 'Inserisci una E con accento acuto maiuscola', # MODIF
	'barre_e_accent_grave' => 'Inserisci una E maiuscola con accento grave', # MODIF
	'barre_ea' => 'Inserisci una E nella A', # MODIF
	'barre_ea_maj' => 'Inserisci una E nella A maiuscola', # MODIF
	'barre_encadrer' => '[(Riquadra)] il paragrafo',
	'barre_eo' => 'Inserisci una E con legatura alla O minuscola', # MODIF
	'barre_eo_maj' => 'Inserisci una E con legatura alla O maiuscola', # MODIF
	'barre_euro' => 'Inserisci il simbolo &euro;', # MODIF
	'barre_exposant' => 'Metti il testo in &lt;sup&gt;esponente&lt;/sup&gt;',
	'barre_formatages_speciaux' => 'Formattazioni speciali', # MODIF
	'barre_galerie' => 'Apri la galleria',
	'barre_gestion_anc_bulle' => 'Tooltip ancora',
	'barre_gestion_anc_caption' => 'Gestione delle ancore',
	'barre_gestion_anc_cible' => 'Destinazione ancora',
	'barre_gestion_anc_inserer' => 'Trasforma in ancora',
	'barre_gestion_anc_nom' => 'Nome dell\'ancora',
	'barre_gestion_anc_pointer' => 'Punta verso un\'ancora',
	'barre_gestion_caption' => 'Didascalia e riassunto', # MODIF
	'barre_gestion_colonne' => 'Num colonne',
	'barre_gestion_cr_casse' => 'Rispetta le maiuscole/minuscole',
	'barre_gestion_cr_changercasse' => 'Cambia maiuscole/minuscole',
	'barre_gestion_cr_changercassemajuscules' => 'Passa in maiuscole',
	'barre_gestion_cr_changercasseminuscules' => 'Passa in minuscole',
	'barre_gestion_cr_chercher' => 'Cerca',
	'barre_gestion_cr_entier' => 'Parola intera',
	'barre_gestion_cr_remplacer' => 'Sostituisci',
	'barre_gestion_cr_tout' => 'Sostituisci tutto',
	'barre_gestion_entete' => 'Intestazione',
	'barre_gestion_ligne' => 'Num righe',
	'barre_gestion_taille' => 'DImensione fissa',
	'barre_glossaire' => 'Voce di [?glossario] (Wikipedia)', # MODIF
	'barre_gras' => 'Converti in {{grassetto}}',
	'barre_guillemets' => 'Racchiudi tra &laquo; virgolette &raquo;', # MODIF
	'barre_guillemets_simples' => 'Racchiudi tra &ldquo;virgolette di secondo livello&rdquo;', # MODIF
	'barre_indenter' => 'Indenta la lista',
	'barre_inserer_caracteres' => 'Inserisci dei caratteri speciali', # MODIF
	'barre_intertitre' => 'Trasforma in {{{titolo}}}',
	'barre_italic' => 'Converti in {corsivo}',
	'barre_langue' => 'Acronimo lingua', # MODIF
	'barre_lien' => 'Trasforma in [link ipertestuale->http://...]',
	'barre_lien_externe' => 'Link esterno',
	'barre_lien_input' => 'Indica l\'indirizzo del tuo link (puoi indicare un indirizo internet sotto forma di http://www.miosito.com, un indirizzo di posta elettronica, o semplicemente indicare il numero di un articolo di questo sito.', # MODIF
	'barre_liste_ol' => 'Converti in lista numerata', # MODIF
	'barre_liste_ul' => 'Converti in lista',
	'barre_lorem_ipsum' => 'Inserisci un paragrafo di prova (lorem ipsum)', # MODIF
	'barre_lorem_ipsum_3' => 'Inserisci 3 paragrafi di prova (lorem ipsum)', # MODIF
	'barre_miseenevidence' => 'Metti il testo in [*evidenza*]', # MODIF
	'barre_note' => 'Trasforma in [[nota a pi&egrave; pagina]]',
	'barre_paragraphe' => 'Crea un paragrafo', # MODIF
	'barre_petitescapitales' => 'metti il testo in &lt;sc&gt;maiuscoletto&lt;/sc&gt;',
	'barre_poesie' => 'Impagina come una &lt;poesie&gt;poesia&lt;/poesie&gt;', # MODIF
	'barre_preview' => 'Modalit', # MODIF
	'barre_quote' => '<quote>Cita un messaggio</quote>',
	'barre_stats' => 'Mostra le statistiche del testo',
	'barre_tableau' => 'Inserisci/modifica (selezionare prima) una tabella', # MODIF

	// C
	'config_info_enregistree' => 'Configurazione salvata', # MODIF

	// E
	'editer' => 'Modifica', # MODIF
	'explication_barre_outils_public' => 'Gli script CSS e Javascript delle barre di testo
	(estensione Porte Plume) vengono caricate nello spazio pubblico
	e consentono di utilizzare queste barre sui moduli dei forum,
	i pennarelli pubblici o per altri plugin, se le loro rispettive configurazioni
	lo permettono.', # MODIF
	'explication_barre_outils_public_2' => 'Puoi scegliere di non caricare
	questi script al fine di allegerire il peso delle pagine pubbliche.
  Qualunque sia la configurazione dei forum, pennarelli o plugin,
  nessuna barra di testo di Prote Pplume potr&agrave; essere
	inserita automaticamente nello spazio pubblico.', # MODIF

	// I
	'info_barre_outils_public' => 'Barra ti testo pubblica',
	'info_porte_plume_titre' => 'Configura le barre di testo',

	// L
	'label_barre_outils_public_non' => 'Non caricare gli script della barra di testo sullo spazio pubblico',
	'label_barre_outils_public_oui' => 'Caricare gli script della barra di testo sullo spazio pubblico',

	// V
	'voir' => 'Vedi'
);

?>
