<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'barre_a_accent_grave' => 'Insertar &Agrave;',
	'barre_adresse' => 'Adresse', # NEW
	'barre_aide' => 'Puedes enriquecer el formato de tu texto utilizando &quot;atajos tipogr&aacute;ficos&quot;',
	'barre_alignerdroite' => '[/Alinear a derecha/]',
	'barre_ancres' => 'Anclas',
	'barre_barre' => 'Barrer le texte', # NEW
	'barre_bulle' => 'Globo de ayuda',
	'barre_c_cedille_maj' => 'Insertar &Ccedil;',
	'barre_cadre' => 'Mostrar dentro de una  &lt;cadre&gt;caja de texto&lt;/cadre&gt;',
	'barre_caracteres' => 'Caracteres especiales',
	'barre_centrer' => '[|Centrar|] p&aacute;rrafo',
	'barre_chercher' => 'Buscar y reemplazar',
	'barre_clean' => 'Limpiar el c&oacute;digo de todas las etiquetas HTML',
	'barre_code' => 'Mostrar como &lt;code&gt;c&oacute;digo fuente&lt;/code&gt;',
	'barre_desindenter' => 'Deshacer sublista',
	'barre_e_accent_aigu' => 'Insertar &Eacute;',
	'barre_e_accent_grave' => 'Intertar &Egrave;',
	'barre_ea' => 'Insertar ligadura &aelig;',
	'barre_ea_maj' => 'Insertar ligadura &AElig;',
	'barre_encadrer' => '[(P&aacute;rrafo con recuadro)]',
	'barre_eo' => 'Insertar ligadura &oelig;',
	'barre_eo_maj' => 'Insertar  ligadura &OElig',
	'barre_euro' => 'Insertar s&iacute;mbolo &euro; ',
	'barre_exposant' => 'Poner el texto en &lt;sup&gt;super&iacute;ndice&lt;/sup&gt;',
	'barre_formatages_speciaux' => 'Formateado especial',
	'barre_galerie' => 'Abrir la galer&iacute;a',
	'barre_gestion_anc_bulle' => 'Globo de ayuda de anclas',
	'barre_gestion_anc_caption' => 'Gestionar las anclas',
	'barre_gestion_anc_cible' => 'Destino del ancla',
	'barre_gestion_anc_inserer' => 'Convertir a un ancla',
	'barre_gestion_anc_nom' => 'Nombre del ancla',
	'barre_gestion_anc_pointer' => 'Apuntar a un ancla',
	'barre_gestion_caption' => 'Encabezado y resumen',
	'barre_gestion_colonne' => 'N&uacute;m de columnas',
	'barre_gestion_cr_casse' => 'Respetar may&uacute;sculas y min&uacute;sculas',
	'barre_gestion_cr_changercasse' => 'Alternar',
	'barre_gestion_cr_changercassemajuscules' => 'Pasar a may&uacute;sculas',
	'barre_gestion_cr_changercasseminuscules' => 'Pasar a min&uacute;sculas',
	'barre_gestion_cr_chercher' => 'Buscar',
	'barre_gestion_cr_entier' => 'S&oacute;lo palabras completas',
	'barre_gestion_cr_remplacer' => 'Reemplazar',
	'barre_gestion_cr_tout' => 'Reemplazar todo',
	'barre_gestion_entete' => 'Encabezado',
	'barre_gestion_ligne' => 'N&uacute;m de l&iacute;neas',
	'barre_gestion_taille' => 'Tama&ntilde;o fijo',
	'barre_glossaire' => '[?Referencia] (Wikipedia)',
	'barre_gras' => '{{Negrita}}',
	'barre_guillemets' => 'Encerrar entre &laquo;comillas dobles&laquo;',
	'barre_guillemets_simples' => 'Place between &ldquo;single quotes&ldquo;',
	'barre_indenter' => 'Convertir en sublista',
	'barre_inserer_caracteres' => 'Insertar caracteres especiales',
	'barre_intertitre' => 'Convertir en {{{intert&iacute;tulo}}}',
	'barre_italic' => '{cursiva}',
	'barre_langue' => 'Idioma abreviado',
	'barre_lien' => 'Convertir en [hipev&iacute;nculo->http://...]',
	'barre_lien_externe' => 'Enlace externo',
	'barre_lien_input' => 'Por favor ingresa el destino de tu hiperv&iacute;nculo (puede ser una URL de la forma http://www.misitio.com o simplmente el n&uacute;mero de un art&iacute;culo de este sitio).',
	'barre_liste_ol' => 'Convertir en lista ordenada',
	'barre_liste_ul' => 'Convertir en lista',
	'barre_lorem_ipsum' => 'Insertar un p&aacute;rrafo ficticio',
	'barre_lorem_ipsum_3' => 'Insertar tres p&aacute;rrafos ficticios',
	'barre_miseenevidence' => 'Resaltar el [*texto*]',
	'barre_note' => 'Convertir en [[Nota al pie]]',
	'barre_paragraphe' => 'Crear p&aacute;rrafo',
	'barre_petitescapitales' => 'Poner el texto en &lt;sc&gt;may&uacute;sculas pequeñas&lt;/sc&gt;',
	'barre_poesie' => 'Mostrar como &lt;poesie&gt;Poes&iacute;a&lt;/poesie&gt;',
	'barre_preview' => 'Modo previsualizaci&oacute;n',
	'barre_quote' => '<quote>Citar</quote>',
	'barre_stats' => 'Mostrar estad&iacute;stica del texto',
	'barre_tableau' => 'Insertar/modificar una tabla (seleccionala primero)',

	// C
	'config_info_enregistree' => 'Configuration sauvegard&eacute;e', # NEW

	// E
	'editer' => 'Editar',
	'explication_barre_outils_public' => 'Les scripts CSS et Javascript des barre d\'outils
	(extension Porte Plume) sont charg&eacute;s sur l\'espace public
	et permettent d\'utiliser ces barres sur les formulaires de forums,
	les crayons publics ou pour d\'autres plugins, si leurs configurations
	respectives le permettent.', # NEW
	'explication_barre_outils_public_2' => 'Vous pouvez choisir de ne pas charger
	ces scripts afin d\'all&eacute;ger le poids des pages publiques.
	D&egrave;s lors quelque soit la configuration des forums, crayons ou plugin,
	aucune barre d\'outils du Porte Plume ne pourra &ecirc;tre
	pr&eacute;sente automatiquement sur l\'espace public.', # NEW

	// I
	'info_barre_outils_public' => 'Barre d\'outils publique', # NEW
	'info_porte_plume_titre' => 'Configurer les barres d\'outils', # NEW

	// L
	'label_barre_outils_public_non' => 'Ne pas charger les scripts de barre d\'outils sur l\'espace public', # NEW
	'label_barre_outils_public_oui' => 'Charger les scripts de barre d\'outils sur l\'espace public', # NEW

	// V
	'voir' => 'Ver'
);

?>
