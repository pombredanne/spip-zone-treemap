<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'barre_a_accent_grave' => 'Majuskel A mit accent grave einf&uuml;gen',
	'barre_adresse' => 'Adresse',
	'barre_aide' => 'Gestalten sie ihre Seite mit typografischen K&uuml;rzeln',
	'barre_alignerdroite' => 'Absatz [/rechtsb&uuml;ndig/] ausrichten',
	'barre_ancres' => 'Anker verwalten',
	'barre_barre' => 'Text durchstreichen',
	'barre_bulle' => 'Sprechblasenhilfe',
	'barre_c_cedille_maj' => 'Majuskel C c&eacute;dille  einf&uuml;gen',
	'barre_cadre' => 'In &lt;cadre&gt;Textfeld&lt;/cadre&gt; einf&uuml;gen',
	'barre_caracteres' => 'Sonderzeichen',
	'barre_centrer' => 'Absatz [|zentrieren|]',
	'barre_chercher' => 'Suchen / Ersetzen',
	'barre_clean' => 'Code aller HTML-Tags bereinigen',
	'barre_code' => '&lt;code&gt;Programmcode &lt;/code&gt; formatieren',
	'barre_desindenter' => 'Einr&uuml;ckungen aus Liste entfernen',
	'barre_e_accent_aigu' => 'Majuskel E mit accent aigu einf&uuml;gen',
	'barre_e_accent_grave' => 'Ins&eacute;rer un E majuscule accent grave',
	'barre_ea' => 'Ins&eacute;rer un E dans l\'A',
	'barre_ea_maj' => 'Majuskel E im A einf&uuml;gen',
	'barre_encadrer' => 'Absatz mit [(Kasten)]',
	'barre_eo' => 'E im O  einf&uuml;gen',
	'barre_eo_maj' => 'Majuskel E im O  einf&uuml;gen',
	'barre_euro' => '&euro; Symbol  einf&uuml;gen',
	'barre_exposant' => 'Text &lt;sup&gt;hochstellen&lt;/sup&gt;',
	'barre_formatages_speciaux' => 'Spezielle Formatierungen',
	'barre_galerie' => 'Galerie &ouml;ffnen',
	'barre_gestion_anc_bulle' => 'Sprechblasenhilfe zum Anker',
	'barre_gestion_anc_caption' => 'Anker verwalten',
	'barre_gestion_anc_cible' => 'Zielanker',
	'barre_gestion_anc_inserer' => 'In Anker umwandeln',
	'barre_gestion_anc_nom' => 'Name des Ankers',
	'barre_gestion_anc_pointer' => 'Link zu Anker',
	'barre_gestion_caption' => 'Motto und Zusammenfassung',
	'barre_gestion_colonne' => 'Spaltenzahl',
	'barre_gestion_cr_casse' => 'Gross/Klein beachten',
	'barre_gestion_cr_changercasse' => 'Majusker/Gemeine tauschel',
	'barre_gestion_cr_changercassemajuscules' => 'In Majuskel umwandeln',
	'barre_gestion_cr_changercasseminuscules' => 'In Gemeine umwandeln',
	'barre_gestion_cr_chercher' => 'Suchen',
	'barre_gestion_cr_entier' => 'Ganzes Wort',
	'barre_gestion_cr_remplacer' => 'Ersetzen',
	'barre_gestion_cr_tout' => 'Alles ersetzen',
	'barre_gestion_entete' => 'Kopf',
	'barre_gestion_ligne' => 'Zeilenzahl',
	'barre_gestion_taille' => 'Feste Gr&ouml;&szlig;e',
	'barre_glossaire' => 'Eintrag in [?glossaire] (Wikipedia)',
	'barre_gras' => '{{Fett}} setzen',
	'barre_guillemets' => 'In &laquo; Anf&uuml;hrungszeichen &raquo; setzen.',
	'barre_guillemets_simples' => 'in &ldquo; doppelte Anf&uuml;hrungszeichen &rdquo;',
	'barre_indenter' => 'Liste mit Einr&uuml;ckungen',
	'barre_inserer_caracteres' => 'Sonderzeichen einf&uuml;gen',
	'barre_intertitre' => 'In {{{Zwischentitel}}} umwandeln',
	'barre_italic' => 'In {kursiv} umwandeln',
	'barre_langue' => 'Sprache / Kurzform',
	'barre_lien' => 'In [Hyperlink->http://...] umwandeln',
	'barre_lien_externe' => 'Externer Link',
	'barre_lien_input' => 'Bitte geben sie die Adresse ihrer Website an (Sie k&ouml;nnen eine Webadresse im Format http://www.meineseite.de, eine Mailadresse oder die Nummer eines Artikels dieser Website angeben.)',
	'barre_liste_ol' => 'Als nummerierte Liste formatieren',
	'barre_liste_ul' => 'Als Liste formatieren',
	'barre_lorem_ipsum' => 'Einen falschen Absatz einf&uuml;gen',
	'barre_lorem_ipsum_3' => 'Drei falsche Abs&auml;tze einf&uuml;gen',
	'barre_miseenevidence' => '[*hervorheben*]',
	'barre_note' => 'In [[Fu',
	'barre_paragraphe' => 'Absatz einf&uuml;gen',
	'barre_petitescapitales' => 'Text als &lt;sc&gt;Kapit&auml;lchen&lt;/sc&gt;',
	'barre_poesie' => 'Als &lt;Poesie&gt;Po&eacute;sie&lt;/poesie&gt; formatieren',
	'barre_preview' => 'Vorschaumodus',
	'barre_quote' => '<quote>Nachricht zitieren</quote>',
	'barre_stats' => 'Statistiken des Texts anzeigen',
	'barre_tableau' => 'In einer Tabelle (bitte markieren) suchen und ersetzen',

	// C
	'config_info_enregistree' => 'Konfiguration gespeichert',

	// E
	'editer' => 'Bearbeiten',
	'explication_barre_outils_public' => 'Die CSS et Javaskripte der Werkzeugleiste
	(Erweiterung Federhalter) werden in den &ouml;ffentlichen Bereich geladen und
	erm&ouml;glichen diese Werkzeugleiten in den Formularen der Foren, des Stift-Plugins
	und in weiteren Plugins einzusetzen, wenn die jeweilige Konfiguration es erlaubt.',
	'explication_barre_outils_public_2' => 'Sie k&ouml;nnen die Verwendung der Werkzeugleisten verhindern und so
	den Code der  &ouml;ffentlichen Seiten verschlanken. Damit stehen sie weder
	in Foren, noch in den Eingabefeldern von Plugins zur Verf&uuml;gung.',

	// I
	'info_barre_outils_public' => '&Ouml;ffentliche Werkzeugleiste',
	'info_porte_plume_titre' => 'Werkzeugleiste konfigurieren',

	// L
	'label_barre_outils_public_non' => 'Werkzeugleisten nicht im &ouml;ffentlichen Bereich laden.',
	'label_barre_outils_public_oui' => 'Werkzeugleisten und Skripte im &ouml;ffentlichen Bereich laden.',

	// V
	'voir' => 'Anzeigen'
);

?>
