<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_core_/plugins/urls_etendues/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'actualiser_toutes' => 'Mettre &agrave; jour toutes les urls',

	// I
	'icone_controler_urls' => 'URLs signifiantes',

	// L
	'label_tri_date' => 'Date',
	'label_tri_id' => 'ID',
	'label_tri_url' => 'URL',
	'label_url' => 'Nouvelle URL',
	'label_url_permanente' => 'Verrouiller cette URL (pas de mise a jour apr&egrave;s edition de l\'objet)',
	'liberer_url' => 'Liberer',
	'liste_des_urls' => 'Toutes les URLs',

	// T
	'texte_type_urls' => 'Vous pouvez choisir ci-dessous le mode de calcul de l\'adresse des pages.',
	'texte_type_urls_attention' => 'Attention ce réglage ne fonctionnera que si le fichier @htaccess@ est correctement installé à la racine du site.',
	'titre_type_urls' => 'Type d\'adresses URL',
	'tout_voir' => 'Voir toutes les URLs',

	// U
	'url_ajout_impossible' => 'Une erreur est survenue. Il n\'a pas &t& possible d\'enregistrer cette URL',
	'url_ajoutee' => 'L\'URL a &eacute;t&eacute; ajout&eacute;e',

	// V
	'verifier_url_nettoyee' => 'L\'URL a &eacute;t&eacute; corrig&eacute;e, vous pouvez verifier avant de l\'enregistrer.',
	'verrouiller_url' => 'Verrouiller'
);

?>
