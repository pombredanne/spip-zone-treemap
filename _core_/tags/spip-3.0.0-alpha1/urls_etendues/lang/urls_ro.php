<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'actualiser_toutes' => 'Mettre &agrave; jour toutes les urls', # NEW

	// I
	'icone_controler_urls' => 'URLs signifiantes', # NEW

	// L
	'label_tri_date' => 'Date', # NEW
	'label_tri_id' => 'ID', # NEW
	'label_tri_url' => 'URL', # NEW
	'label_url' => 'Nouvelle URL', # NEW
	'label_url_permanente' => 'Verrouiller cette URL (pas de mise a jour apr&egrave;s edition de l\'objet)', # NEW
	'liberer_url' => 'Liberer', # NEW
	'liste_des_urls' => 'Toutes les URLs', # NEW

	// T
	'texte_type_urls' => 'Puteţi alege mai jos modul de calcul a adreselor paginilor.',
	'texte_type_urls_attention' => 'Atenţie: acest reglaj nu va funcţiona corect decât dacă fişierul @htaccess@  este instalat în rădăcina site-ului.',
	'titre_type_urls' => 'Tip de adrese URL',
	'tout_voir' => 'Voir toutes les URLs', # NEW

	// U
	'url_ajout_impossible' => 'Une erreur est survenue. Il n\'a pas &t& possible d\'enregistrer cette URL', # NEW
	'url_ajoutee' => 'L\'URL a &eacute;t&eacute; ajout&eacute;e', # NEW

	// V
	'verifier_url_nettoyee' => 'L\'URL a &eacute;t&eacute; corrig&eacute;e, vous pouvez verifier avant de l\'enregistrer.', # NEW
	'verrouiller_url' => 'Verrouiller' # NEW
);

?>
