<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'actualiser_toutes' => 'تحديث كل عناوين URL',

	// I
	'icone_controler_urls' => 'عناوين URL ذات معنى ',

	// L
	'label_tri_date' => 'التاريخ',
	'label_tri_id' => 'رقم التسلسل',
	'label_tri_url' => 'عنوان URL',
	'label_url' => 'عنوان URL جديد',
	'label_url_permanente' => 'إقفال هذا العنوان (لا تحديث بعد تحرير العنصر)',
	'liberer_url' => 'إلغاء القفل',
	'liste_des_urls' => 'كل عناوين URL',

	// T
	'texte_type_urls' => 'يمكنك اختيار أسلوب حساب عناوين الصفحات.',
	'texte_type_urls_attention' => 'تنبيه، لن يعمل هذا الاعداد الا اذا كان ملف @htaccess@ مثبتاً بشكل سليم في أصل الموقع.',
	'titre_type_urls' => 'نوع عناوين URL',
	'tout_voir' => 'عرض كل عناوين URL',

	// U
	'url_ajout_impossible' => 'حصل خطأ. لم يتم تسجيل هذا العنوان',
	'url_ajoutee' => 'تمت إضافة هذا العنوان',

	// V
	'verifier_url_nettoyee' => 'تم تصحيح العنوان، يمكنك التأكد قبل تسجيله.',
	'verrouiller_url' => 'إقفال'
);

?>
