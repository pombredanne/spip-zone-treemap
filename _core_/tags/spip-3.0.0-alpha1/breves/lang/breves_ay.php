<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'breve' => 'Brève', # NEW
	'breves' => 'Jisk\'aptatanaka',

	// E
	'entree_breve_publiee' => '¿Aka jisk\'aptat yatiysnat?
',
	'entree_texte_breve' => 'Jisk\'aptata sawu',

	// I
	'icone_breves' => 'Jisk\'aptatanaka',
	'icone_ecrire_nouvel_article' => 'Aka t\'aqat jisk\'aptatanakaxa ',
	'icone_modifier_breve' => 'Aka jisk\'aptata mayjt\'ayaña',
	'icone_nouvelle_breve' => 'Machaq jiskht\'aptata',
	'info_1_breve' => '1 jisk\'aptata',
	'info_aucun_breve' => 'Aucune brève', # NEW
	'info_breves' => 'Jisk\'aptatanak apnaqaw apnaqasmaw',
	'info_breves_02' => 'Jisk\'aptatanaka',
	'info_breves_valider' => 'Jisk\'aptatanak iyaw sañataki',
	'info_gauche_numero_breve' => 'Jisk\'aptata',
	'info_nb_breves' => '@nb@ brèves', # NEW
	'item_breve_proposee' => 'Jisk\'aptat amtawi',
	'item_breve_refusee' => 'NON - Brève refusée', # NEW
	'item_breve_validee' => 'OUI - Brève validée', # NEW
	'item_non_utiliser_breves' => 'Ne pas utiliser les brèves', # NEW
	'item_utiliser_breves' => 'Utiliser les brèves', # NEW

	// L
	'logo_breve' => 'LOGO DE LA BRÈVE', # NEW

	// T
	'texte_breves' => 'Les brèves sont des textes courts et simples permettant de
	mettre en ligne rapidement des informations concises, de gérer
	une revue de presse, un calendrier d\'événements...', # NEW
	'titre_breve_proposee' => 'Jisk\'aptat amtata',
	'titre_breve_publiee' => 'Jisk\'aptat iyaw sata',
	'titre_breve_refusee' => 'Jisk\'aptat janiw sata',
	'titre_breves' => 'Les brèves', # NEW
	'titre_langue_breve' => 'LANGUE DE LA BRÈVE', # NEW
	'titre_page_breves' => 'Brèves' # NEW
);

?>
