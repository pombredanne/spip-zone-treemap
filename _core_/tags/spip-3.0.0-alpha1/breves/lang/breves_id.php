<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'breve' => 'Brève', # NEW
	'breves' => 'Artikel-artikel berita',

	// E
	'entree_breve_publiee' => 'Apakah artikel berita ini akan dipublikasikan?',
	'entree_texte_breve' => 'Teks artikel berita',

	// I
	'icone_breves' => 'Berita',
	'icone_ecrire_nouvel_article' => 'Berita dalam bagian ini',
	'icone_modifier_breve' => 'Modifikasi artikel berita ini',
	'icone_nouvelle_breve' => 'Tulis sebuah artikel berita baru',
	'info_1_breve' => '1 artikel berita',
	'info_aucun_breve' => 'Aucune brève', # NEW
	'info_breves' => 'Apakah situs anda menggunakan sistem pemberitaan?',
	'info_breves_02' => 'Berita',
	'info_breves_valider' => 'Artikel berita yang akan divalidasi',
	'info_gauche_numero_breve' => 'NOMOR ARTIKEL BERITA',
	'info_nb_breves' => '@nb@ brèves', # NEW
	'item_breve_proposee' => 'Artikel berita dikirim',
	'item_breve_refusee' => 'TIDAK - Artikel berita ditolak',
	'item_breve_validee' => 'YA - Artikel berita divalidasi',
	'item_non_utiliser_breves' => 'Jangan gunakan berita',
	'item_utiliser_breves' => 'Gunakan berita',

	// L
	'logo_breve' => 'LOGO ARTIKEL BERITA',

	// T
	'texte_breves' => 'Berita adalah teks singkat dan sederhana yang mengizinkan
	publikasi online informasi, ulasan-ulasan, peristiwa-peristiwa.....',
	'titre_breve_proposee' => 'Artikel berita yang dikirim',
	'titre_breve_publiee' => 'Artikel berita dipublikasi',
	'titre_breve_refusee' => 'Artikel berita ditolak',
	'titre_breves' => 'Berita',
	'titre_langue_breve' => 'BAHASA ARTIKEL BERITA',
	'titre_page_breves' => 'Berita'
);

?>
