<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'breve' => 'Brève', # NEW
	'breves' => 'Novinky',

	// E
	'entree_breve_publiee' => 'Má být tato novinka publikována?',
	'entree_texte_breve' => 'Text novinky',

	// I
	'icone_breves' => 'Novinky',
	'icone_ecrire_nouvel_article' => 'Novinky v této sekci',
	'icone_modifier_breve' => 'Změnit novinku',
	'icone_nouvelle_breve' => 'Napsat novinku',
	'info_1_breve' => '1 novinka',
	'info_aucun_breve' => 'Aucune brève', # NEW
	'info_breves' => 'Používá váš web novinky?',
	'info_breves_02' => 'Novinky',
	'info_breves_valider' => 'Novinky ke schválení',
	'info_gauche_numero_breve' => 'ČÍSLO NOVINKY',
	'info_nb_breves' => '@nb@ brèves', # NEW
	'item_breve_proposee' => 'Předložené novinky',
	'item_breve_refusee' => 'Odmítnutá novinka',
	'item_breve_validee' => 'Schválená novinka',
	'item_non_utiliser_breves' => 'Nepoužívat novinky',
	'item_utiliser_breves' => 'Používat novinky',

	// L
	'logo_breve' => 'LOGO NOVINKY',

	// T
	'texte_breves' => 'Novinky jsou krátké texty umožňující rychle zveřejňovat
 stručné informace, redigovat přehled tisku, přehled událostí...',
	'titre_breve_proposee' => 'Předložená novinka',
	'titre_breve_publiee' => 'Novinka zveřejněna',
	'titre_breve_refusee' => 'Novinka zamítnuta',
	'titre_breves' => 'Novinky',
	'titre_langue_breve' => 'JAZYK NOVINKY',
	'titre_page_breves' => 'Novinky'
);

?>
