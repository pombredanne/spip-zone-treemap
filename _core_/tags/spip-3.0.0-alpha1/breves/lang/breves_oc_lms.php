<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'breve' => 'Brève', # NEW
	'breves' => 'Brevas',

	// E
	'entree_breve_publiee' => 'Quela breva la chau publicar?',
	'entree_texte_breve' => 'Text de la breva',

	// I
	'icone_breves' => 'Brevas',
	'icone_ecrire_nouvel_article' => 'Las brevas contengudas dins quela rubrica',
	'icone_modifier_breve' => 'Modificar quela breva',
	'icone_nouvelle_breve' => 'Escriure una breva nuòva',
	'info_1_breve' => '1 breva',
	'info_aucun_breve' => 'Aucune brève', # NEW
	'info_breves' => 'Vòstre sit utiliza lo sistema de brevas?',
	'info_breves_02' => 'Brevas',
	'info_breves_valider' => 'Brevas de validar',
	'info_gauche_numero_breve' => 'BREVA NUMERO',
	'info_nb_breves' => '@nb@ brèves', # NEW
	'item_breve_proposee' => 'Breva prepausada',
	'item_breve_refusee' => 'NON - Breva refusada',
	'item_breve_validee' => 'ÒC - Breva validada',
	'item_non_utiliser_breves' => 'Pas utilizar las brevas',
	'item_utiliser_breves' => 'Utilizar las brevas',

	// L
	'logo_breve' => 'LÒGO DE LA BREVA',

	// T
	'texte_breves' => 'Las brevas son daus texts corts e simples que permeten de metre en linha regde de las informacions concisas, de gerir
 una revista de premsa, un chalendier d\'eveniments...',
	'titre_breve_proposee' => 'Breva prepausada',
	'titre_breve_publiee' => 'Breva publicada',
	'titre_breve_refusee' => 'Breva refusada',
	'titre_breves' => 'Las brevas',
	'titre_langue_breve' => 'LENGA DE LA BREVA',
	'titre_page_breves' => 'Brevas'
);

?>
