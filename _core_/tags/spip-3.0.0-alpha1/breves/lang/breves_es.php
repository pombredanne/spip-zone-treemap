<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'breve' => 'Breve',
	'breves' => 'Breves',

	// E
	'entree_breve_publiee' => '¿Publicamos ésta breve?',
	'entree_texte_breve' => 'Texto de la breve',

	// I
	'icone_breves' => 'Breves',
	'icone_ecrire_nouvel_article' => 'Las breves de esta sección',
	'icone_modifier_breve' => 'Modificar esta breve',
	'icone_nouvelle_breve' => 'Nueva breve',
	'info_1_breve' => '1 breve',
	'info_aucun_breve' => 'Ninguna breve',
	'info_breves' => 'Puedes utilizar el sistema de breves',
	'info_breves_02' => 'Breves',
	'info_breves_valider' => 'Breves a validar',
	'info_gauche_numero_breve' => 'Breve',
	'info_nb_breves' => '@nb@ breves',
	'item_breve_proposee' => 'Breve propuesta',
	'item_breve_refusee' => 'Breve rechazada',
	'item_breve_validee' => 'Breve validada',
	'item_non_utiliser_breves' => 'No utilizar las breves',
	'item_utiliser_breves' => 'Utilizar las breves',

	// L
	'logo_breve' => 'Logotipo de la breve...',

	// T
	'texte_breves' => 'Las breves son notas cortas y simples que permiten poner rápidamente en línea informaciones concisas, por ejemplo para manejar reseñas de prensa, un calendario de eventos...',
	'titre_breve_proposee' => 'Breve propuesta',
	'titre_breve_publiee' => 'Breve publicada',
	'titre_breve_refusee' => 'Breve rechazada',
	'titre_breves' => 'Las breves',
	'titre_langue_breve' => 'IDIOMA DE LA BREVE',
	'titre_page_breves' => 'Breves'
);

?>
