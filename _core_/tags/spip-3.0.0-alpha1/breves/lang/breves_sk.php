<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'breve' => 'Novinka',
	'breves' => 'Novinky',

	// E
	'entree_breve_publiee' => 'Chcete publikovať túto novinku?',
	'entree_texte_breve' => 'Text novinky',

	// I
	'icone_breves' => 'Novinky',
	'icone_ecrire_nouvel_article' => 'Novinky v tejto rubrike',
	'icone_modifier_breve' => 'Upraviť túto novinku',
	'icone_nouvelle_breve' => 'Vytvoriť novinku',
	'info_1_breve' => '1 novinka',
	'info_aucun_breve' => 'Žiadne novinky',
	'info_breves' => 'Chcete využívať novinky?',
	'info_breves_02' => 'Novinky',
	'info_breves_valider' => 'Novinky čakajúce na schválenie',
	'info_gauche_numero_breve' => 'NOVINKA ČÍSLO',
	'info_nb_breves' => '@nb@ noviniek',
	'item_breve_proposee' => 'Novinka odoslaná',
	'item_breve_refusee' => 'Nie - novinka zamietnutá',
	'item_breve_validee' => 'Áno - novinka schválená',
	'item_non_utiliser_breves' => 'Nepoužívať novinky',
	'item_utiliser_breves' => 'Používať novinky',

	// L
	'logo_breve' => 'LOGO NOVINKY',

	// T
	'texte_breves' => 'Novinky sú stručné správy, ktoré umožňujú
 publikovať bleskové správy, riadiť prehľady tlače, kalendár udalostí, atď.',
	'titre_breve_proposee' => 'Novinka odoslaná',
	'titre_breve_publiee' => 'Novinka publikovaná',
	'titre_breve_refusee' => 'Novinka zamietnutá',
	'titre_breves' => 'Novinky',
	'titre_langue_breve' => 'JAZYK NOVINKY',
	'titre_page_breves' => 'Novinky'
);

?>
