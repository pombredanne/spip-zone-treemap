<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'breve' => 'Brève', # NEW
	'breves' => 'Hírek',

	// E
	'entree_breve_publiee' => 'Ezt a hírt publikálni kell?',
	'entree_texte_breve' => 'Hír szövege',

	// I
	'icone_breves' => 'Hírek',
	'icone_ecrire_nouvel_article' => 'E rovatban szereplő hírek',
	'icone_modifier_breve' => 'A hír módosítása',
	'icone_nouvelle_breve' => 'Új hír írása',
	'info_1_breve' => '1 hír',
	'info_aucun_breve' => 'Aucune brève', # NEW
	'info_breves' => 'Az Ön honlapja használja-e a hírek rendszerét ?',
	'info_breves_02' => 'Hírek',
	'info_breves_valider' => 'Jóváhagyandó hírek',
	'info_gauche_numero_breve' => 'HÍR SZÁMA',
	'info_nb_breves' => '@nb@ brèves', # NEW
	'item_breve_proposee' => 'Javasolt hír',
	'item_breve_refusee' => 'NEM - Elutasított hír',
	'item_breve_validee' => 'IGEN - Elfogadott hír',
	'item_non_utiliser_breves' => 'Nem kell használni a híreket',
	'item_utiliser_breves' => 'Hírek használata',

	// L
	'logo_breve' => 'A HÍR LOGOJA',

	// T
	'texte_breves' => 'A hírek olyan rövid és egyszerű szövegek, melyeknek segítségével lehet gyorsan publikálni rövid információkat, napszemlét, eseménynaptárt...',
	'titre_breve_proposee' => 'Javasolt hír',
	'titre_breve_publiee' => 'Publikált hír',
	'titre_breve_refusee' => 'Elutasított hír',
	'titre_breves' => 'A hírek',
	'titre_langue_breve' => 'A HÍR NYELVE',
	'titre_page_breves' => 'Hírek'
);

?>
