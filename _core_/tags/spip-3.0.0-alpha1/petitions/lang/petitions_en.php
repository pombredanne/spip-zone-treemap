<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'alerte_confirmer_suppression_signatures' => 'Vous allez supprimer toutes les signatures de cette p&eacute;tition. &Ecirc;tes-vous sur de vouloir continuer ?', # NEW
	'aucune_signature' => 'Aucune signature', # NEW

	// B
	'bouton_checkbox_envoi_message' => 'possibilit&eacute; d\'envoyer un message', # NEW
	'bouton_checkbox_indiquer_site' => 'indiquer obligatoirement un site Web', # NEW
	'bouton_checkbox_signature_unique_email' => 'une seule signature par adresse email', # NEW
	'bouton_checkbox_signature_unique_site' => 'une seule signature par site Web', # NEW

	// F
	'filtre' => 'Filtre :', # NEW
	'form_pet_adresse_site' => 'URL of your site',
	'form_pet_aucune_signature' => 'No signature matching this code...',
	'form_pet_confirmation' => 'Please confirm your signature:',
	'form_pet_deja_signe' => 'You have already signed this text.',
	'form_pet_envoi_mail_confirmation' => 'A confirmation email has just been sent to  @email@. You should visit the URL given in this email to validate your signature.',
	'form_pet_mail_confirmation' => 'Hello,

You asked to sign the petition:
@titre@.

You provided the following information:
    Name: @nom_email@
    Site: @nom_site@ - @url_site@
@message@

IMPORTANT...
To validate your signature, you just need to connect to
the address below (otherwise your request
will be rejected):
    @url@

Thank you for participating!
',
	'form_pet_message_commentaire' => 'Any message or comments?', # MODIF
	'form_pet_nom_site2' => 'Name of your website',
	'form_pet_probleme_liens' => 'Please edit your message, removing all hyperlinks.',
	'form_pet_probleme_technique' => 'Technical problem: signatures are interrupted momentarily.',
	'form_pet_signature_validee' => 'Your signature is valid. Thank you!',
	'form_pet_site_deja_enregistre' => 'This site is already registered',
	'form_pet_url_invalide' => 'The URL that you entered is not valid.',
	'form_pet_votre_email' => 'Your email address',
	'form_pet_votre_nom' => 'Your name or alias',
	'form_pet_votre_site' => 'If you have a website, you can enter the URL below',

	// I
	'icone_relancer_signataire' => 'Relancer le signataire', # NEW
	'icone_relancer_signataires' => 'Relancer les signataires', # NEW
	'icone_suivi_petitions' => 'Suivre/g&eacute;rer les p&eacute;titions', # NEW
	'icone_supprimer_signature' => 'Supprimer cette signature', # NEW
	'icone_supprimer_signatures' => 'Supprimer les signatures', # NEW
	'icone_valider_signature' => 'Valider cette signature', # NEW
	'icone_valider_signatures' => 'Valider les signatures', # NEW
	'info_adresse_email' => 'EMAIL:',
	'info_fonctionnement_petition' => 'Fonctionnement de la p&eacute;tition :', # NEW
	'info_signature_supprimee' => 'Signature deleted',
	'info_signature_supprimee_erreur' => 'Error: this deletion code does not correspond to any signatures',
	'info_site_web' => 'WEBSITE:',
	'info_texte_message' => 'Text of your message:',

	// L
	'lien_reponse_article' => 'Comment on this article',

	// N
	'nombre_signatures' => '@nb@ signatures', # NEW

	// S
	'sans_nom' => 'Anonyme', # NEW
	'signatures_article' => 'Signatures de l\'article :', # NEW
	'signatures_aucune' => 'Aucune', # NEW
	'signatures_meme_auteur' => 'Les signatures du m', # NEW
	'signatures_meme_site' => 'Les signatures avec le m', # NEW
	'signatures_poubelle' => 'Supprim&eacute;es', # NEW
	'signatures_prop' => 'En attente', # NEW
	'signatures_publie' => 'Confirm&eacute;es', # NEW
	'signatures_toutes' => 'Toutes', # NEW
	'statut_poubelle' => 'Supprim&eacute;e', # NEW
	'statut_prop' => 'En attente de validation', # NEW
	'statut_publie' => 'Confirm&eacute;e', # NEW

	// T
	'texte_descriptif_petition' => 'Descriptif de la p&eacute;tition', # NEW
	'titre_page_controle_petition' => 'Suivi des p&eacute;titions', # NEW
	'titre_selection_action' => 'Sélection', # NEW
	'titre_signatures_attente' => 'Signatures en attente de validation', # NEW
	'titre_signatures_publie' => 'Signatures confirm&eacute;es', # NEW
	'titre_suivi_petition' => 'Suivi des p&eacute;titions', # NEW
	'tout_voir' => 'Voir toutes les signatures', # NEW

	// U
	'une_signature' => '1 signature', # NEW

	// V
	'voir_signatures_objet' => 'Voir les signatures' # NEW
);

?>
