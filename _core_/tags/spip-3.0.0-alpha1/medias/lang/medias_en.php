<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_audio' => 'No sound',
	'aucun_document' => 'No document',
	'aucun_file' => 'No document',
	'aucun_image' => 'No image',
	'aucun_video' => 'No video', # MODIF
	'aucune_utilisation' => 'This document isn\'t used.', # MODIF
	'aucune_vignette' => 'No thumbnail',

	// B
	'bouton_ajouter_document' => 'Add a document',
	'bouton_ajouter_image' => 'Add an image',
	'bouton_ajouter_image_document' => 'Add an image or a document',
	'bouton_attacher_document' => 'Attach',
	'bouton_copier_local' => 'Copy on the site',
	'bouton_download_depuis' => 'from:', # MODIF
	'bouton_download_local' => 'my computer',
	'bouton_download_par_ftp' => 'the server',
	'bouton_download_par_mediatheque' => 'media library', # MODIF
	'bouton_download_sur_le_web' => 'internet',
	'bouton_enlever_document' => 'Unlink',
	'bouton_enlever_supprimer_document' => 'Delete',
	'bouton_enlever_supprimer_document_confirmation' => 'Etes-vous s&ucirc;r de vouloir supprimer le document ?', # NEW
	'bouton_modifier_document' => 'Modify',
	'bouton_parcourir' => 'Browse...',
	'bouton_refresh_document' => 'Refresh document',
	'bouton_remplacer_document' => 'Replace this document',
	'bouton_remplacer_image' => 'Replace this image',
	'bouton_remplacer_image_document' => 'Replace by an image or a document',
	'bouton_supprimer' => 'Delete',
	'bouton_supprimer_tous_orphelins' => 'Delete all unused',
	'bouton_supprimer_tous_orphelins_confirmation' => 'Do you really want to delete @nb@ unused?', # MODIF
	'bouton_upload' => 'Upload', # MODIF

	// D
	'des_audios' => '@nb@ sounds',
	'des_documents' => '@nb@ documents',
	'des_files' => '@nb@ documents',
	'des_images' => '@nb@ images',
	'des_utilisations' => '@nb@ uses',
	'des_videos' => '@nb@ videos', # MODIF
	'descriptif' => 'Description',
	'document_attache_succes' => 'The document was successfully added', # MODIF
	'document_copie_locale_succes' => 'The file was successfully copied on the site', # MODIF
	'document_installe_succes' => 'The file was successfully uploaded', # MODIF
	'document_vu' => 'Insert in text', # MODIF
	'documents' => 'Media library', # MODIF
	'documents_brises' => 'Broken', # MODIF
	'documents_distants' => 'Remote',
	'documents_locaux' => 'Local',
	'documents_non_publies' => 'Unpublished', # MODIF
	'documents_orphelins' => 'Unused', # MODIF
	'documents_publies' => 'Published', # MODIF
	'documents_tous' => 'All',
	'double_clic_inserer_doc' => 'Double-click to insert this shortcut in the text',

	// E
	'entree_dimensions' => 'Size:',
	'entree_titre_document' => 'Document title:',
	'entree_titre_image' => 'Image title:',
	'erreur_aucun_document' => 'This document doesn\'t exist in the media library', # MODIF
	'erreur_aucun_fichier' => 'No file was found', # MODIF
	'erreur_chemin_distant' => 'The remote file @nom@ could not be found', # MODIF
	'erreur_chemin_ftp' => 'The specified file was not found on server', # MODIF
	'erreur_copie_fichier' => 'Unable to copy the file @nom@',
	'erreur_dossier_tmp_manquant' => 'Un dossier temporaire est manquant pour télécharger les fichiers', # NEW
	'erreur_ecriture_fichier' => 'Erreur lors de l\'écriture du fichier sur le disque', # NEW
	'erreur_format_fichier_image' => 'The format of @nom@ is not suitable for an image',
	'erreur_indiquez_un_fichier' => 'Specify a file!',
	'erreur_insertion_document_base' => 'Impossible d\'enregistrer le document @fichier@ en base de donn&eacute;es', # NEW
	'erreur_suppression_vignette' => 'Erreur lors de la suppression de la vignette', # NEW
	'erreur_upload_type_interdit' => 'Downloading files of type @nom@ is not allowed', # MODIF
	'erreur_upload_vignette' => 'Error loading thumbnail @nom@',
	'erreurs' => '@nb@ erreurs', # NEW
	'erreurs_voir' => 'Voir les @nb@ erreurs', # NEW

	// F
	'fichier_distant' => 'Fichier distant', # NEW
	'fichier_manquant' => 'Fichier manquant', # NEW
	'fichier_modifie' => 'The file has been modified. Click save button.', # MODIF
	'format_date_incorrect' => 'The date or time are incorrect',

	// I
	'icone_creer_document' => 'Add a document',
	'id' => 'ID',
	'image_tourner_180' => 'Rotate 180°',
	'image_tourner_droite' => 'Rotate 90° right',
	'image_tourner_gauche' => 'Rotate 90° left',
	'info_doc_max_poids' => 'Documents must necessarily be less than @maxi@ (this file is @actuel@).',
	'info_document' => 'Document',
	'info_document_indisponible' => 'This document is not available',
	'info_documents' => 'Documents',
	'info_gauche_numero_document' => 'DOCUMENT NUMBER', # MODIF
	'info_hauteur' => 'Height',
	'info_heure' => 'Time',
	'info_illustrations' => 'Illustrations',
	'info_image_max_poids' => 'Images must necessarily be less than @maxi@ (this file is @actuel@).',
	'info_inclusion_directe' => 'Direct inclusion:',
	'info_inclusion_vignette' => 'Include vignette:',
	'info_installer_tous_documents' => 'Install all the documents',
	'info_largeur' => 'Width',
	'info_logo_max_taille' => 'Logos must be less than @maxi@ (this file is @actuel@).',
	'info_modifier_document' => 'Modify the document',
	'info_portfolio' => 'Portfolio',
	'info_referencer_doc_distant' => 'Reference a document on Internet:',
	'info_statut_document' => 'This document is:', # MODIF
	'info_telecharger' => 'Upload from your computer:',
	'infos' => 'Technical infos',
	'item_autoriser_selectionner_date_en_ligne' => 'Allow changes to the date of each document',
	'item_non_autoriser_selectionner_date_en_ligne' => 'The date of a document is the day it was added to the site',

	// L
	'label_activer_document_objets' => 'Activer les documents pour les contenus&nbsp;:', # NEW
	'label_apercu' => 'Overview', # MODIF
	'label_caracteristiques' => 'Features', # MODIF
	'label_credits' => 'Credits', # MODIF
	'label_fichier' => 'File',
	'label_fichier_vignette' => 'Thumbnail',
	'label_parents' => 'This document is linked to', # MODIF
	'label_refdoc_joindre' => 'Document number', # MODIF
	'lien_tout_enlever' => 'Remove all',
	'logo' => 'Logo',

	// M
	'media_audio' => 'Sounds',
	'media_file' => 'Other',
	'media_image' => 'Images',
	'media_video' => 'Videos', # MODIF

	// N
	'nb_documents_installe_succes' => '@nb@ files successfully uploaded', # MODIF

	// O
	'objet_document' => 'Document', # NEW
	'objet_documents' => 'Documents', # NEW

	// P
	'par_date' => 'Date',
	'par_hauteur' => 'Height',
	'par_id' => 'ID',
	'par_largeur' => 'Width',
	'par_taille' => 'Weight',
	'par_titre' => 'Title',

	// T
	'texte_documents_joints' => 'You can allow the addition of documents (office files, images,
 multimedia, etc.) to articles and/or sections. These files
 may then be referenced in
 the article or displayed separately.',
	'texte_documents_joints_2' => 'This setting does not prevent the direct inclusion of images in articles.',
	'titre_documents_joints' => 'Attached documents',
	'titre_page_documents_edit' => 'Modifier le document : @titre@', # NEW
	'tous_les_medias' => 'All media',
	'tout_dossier_upload' => 'The whole @upload@ directory',
	'tout_voir' => 'Tout voir', # NEW

	// U
	'un_audio' => '1 sound',
	'un_document' => '1 document',
	'un_file' => '1 document',
	'un_image' => '1 image',
	'un_video' => '1 video', # MODIF
	'une_utilisation' => '1 use',
	'upload_fichier_zip' => 'ZIP file',
	'upload_fichier_zip_texte' => 'The file you are intending to install is a ZIP file.',
	'upload_fichier_zip_texte2' => 'This file can be:',
	'upload_info_mode_document' => 'Send to portfolio', # MODIF
	'upload_info_mode_image' => 'Remove from portfolio',
	'upload_limit' => 'This file is too big for the server; the maximum size allowed for <i>upload</i> is @max@.',
	'upload_zip_conserver' => 'Keep the archive file after extracting its contents',
	'upload_zip_decompacter' => 'decompressed and each file it contains will be installed on the site. The files which will be installed are:',
	'upload_zip_mode_document' => 'D&eacute;poser toutes les images dans le portfolio', # NEW
	'upload_zip_telquel' => 'installed as is, as a ZIP file;',
	'upload_zip_titrer' => 'Add titles according to the filenames',

	// V
	'verifier_documents_brises' => 'Check missing files', # MODIF
	'verifier_documents_inutilises' => 'V&eacute;rifier les liens des documents', # NEW
	'vignette_supprimee' => 'The thumbnail has been deleted' # MODIF
);

?>
