<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_audio' => 'Kein Ton',
	'aucun_document' => 'Kein Dokument',
	'aucun_file' => 'Keine Datei',
	'aucun_image' => 'Kein Bild',
	'aucun_video' => 'Keine Videos',
	'aucune_utilisation' => 'Dieses Dokument wird nicht verwendet.',
	'aucune_vignette' => 'Kein Miniaturbild',

	// B
	'bouton_ajouter_document' => 'Dokument hinzufügen',
	'bouton_ajouter_image' => 'Bild hinzufügen',
	'bouton_ajouter_image_document' => 'Bild oder Dokument hinzufügen',
	'bouton_attacher_document' => 'Anh&auml;ngen',
	'bouton_copier_local' => 'Lokale Kopie anlegen',
	'bouton_download_depuis' => 'Von:',
	'bouton_download_local' => 'Mein Computer',
	'bouton_download_par_ftp' => 'FTP-Server',
	'bouton_download_par_mediatheque' => 'Mediathek',
	'bouton_download_sur_le_web' => 'Internet',
	'bouton_enlever_document' => 'Entfernen',
	'bouton_enlever_supprimer_document' => 'L&ouml;schen',
	'bouton_enlever_supprimer_document_confirmation' => 'Wollen sie dieses Dokument wirklich l&ouml;schen?',
	'bouton_modifier_document' => 'Bearbeiten',
	'bouton_parcourir' => 'Durchsuchen...',
	'bouton_refresh_document' => 'Dokument neu laden',
	'bouton_remplacer_document' => 'Dieses Dokument ersetzen',
	'bouton_remplacer_image' => 'Dieses Bild ersetzen',
	'bouton_remplacer_image_document' => 'Durch ein Dokument oder Bild ersetzen',
	'bouton_supprimer' => 'L&ouml;schen',
	'bouton_supprimer_tous_orphelins' => 'L&ouml;schen alle dokument ungenutzt',
	'bouton_supprimer_tous_orphelins_confirmation' => 'Wollen Sie wirklich bis @nb@ unbenutzte l�schen ?',
	'bouton_upload' => '&Uuml;bertragen',

	// D
	'des_audios' => '@nb@ T&ouml;ne',
	'des_documents' => '@nb@ Dokumente',
	'des_files' => '@nb@ Dokumente',
	'des_images' => '@nb@ Bilder',
	'des_utilisations' => '@nb@ Verwendungen',
	'des_videos' => '@nb@ Videos',
	'descriptif' => 'Beschreibung',
	'document_attache_succes' => 'Das Dokument wurde erfolgreich hinzugef&uuml;gt.',
	'document_copie_locale_succes' => 'Die Datei wurde erfolgreich in auf den Server &uuml;bertragen.',
	'document_installe_succes' => 'Die Datei wurde erfolgreich &uuml;bertragen',
	'document_vu' => 'In den Text eingef&uuml;gt.',
	'documents' => 'Mediathek',
	'documents_brises' => 'Besch&auml;digt',
	'documents_distants' => 'Externe',
	'documents_locaux' => 'Lokale',
	'documents_non_publies' => 'Nicht ver&ouml;ffentlicht',
	'documents_orphelins' => 'Ungenutzt',
	'documents_publies' => 'Ver&ouml;ffentlicht',
	'documents_tous' => 'Alle',
	'double_clic_inserer_doc' => 'Mügen Sie mit einem Doppelklick das Kürzel in den Text ein',

	// E
	'entree_dimensions' => 'Grössenangaben:',
	'entree_titre_document' => 'Titel des Dokuments:',
	'entree_titre_image' => 'Titel des Bilds:',
	'erreur_aucun_document' => 'Dieses Dokument ist nicht in der Mediathek vorhanden.',
	'erreur_aucun_fichier' => 'Es wurde kein Dokument gefunden.',
	'erreur_chemin_distant' => 'Die Datei @nom@ konnte nicht &uuml;ber das Netz erreicht werden.',
	'erreur_chemin_ftp' => 'Die angegebene Datei wurde nicht auf dem Server gefunden.',
	'erreur_copie_fichier' => 'Die Datei @nom@ konnte nicht kopiert werden.',
	'erreur_dossier_tmp_manquant' => 'Es fehlt ein temporäres Verzeichnis zum Übertragen der Dateien',
	'erreur_ecriture_fichier' => 'Fehler beim Speichern der Datei',
	'erreur_format_fichier_image' => 'Das Format von @nom@ ist nicht als Bild geeeignet.',
	'erreur_indiquez_un_fichier' => 'Geben sie eine Datei an!',
	'erreur_insertion_document_base' => 'Das Dokument @fichier@kann nicht in der Datenbank erfasst werden.',
	'erreur_suppression_vignette' => 'Fehler beim Löschen des Thumbnails',
	'erreur_upload_type_interdit' => 'Die &Uuml;bertragung von Dokumenten mit dem Namen @nom@ ist nicht erlaubt.',
	'erreur_upload_vignette' => 'Fehler beim Laden des Miniaturbilds @nom@',
	'erreurs' => '@nb@ Fehler',
	'erreurs_voir' => '@nb@ Fehler ansehen',

	// F
	'fichier_distant' => 'Externe Datei',
	'fichier_manquant' => 'Datei fehlt',
	'fichier_modifie' => 'Die Datei wurde ge&auml;ndert. Klicken sie hier zum Speichern.',
	'format_date_incorrect' => 'Datum oder Uhrzeit sind falsch.',

	// I
	'icone_creer_document' => 'Dokument hinzuf&uuml;gen',
	'id' => 'ID',
	'image_tourner_180' => 'Um 180° drehen',
	'image_tourner_droite' => 'Um 90° nach rechts drehen',
	'image_tourner_gauche' => 'Um 90° nach links drehen',
	'info_doc_max_poids' => 'Die Dokumente d&uuml;rfen h&ouml;chstens @maxi@ gro&szlig; sein (diese Datei hat @actuel@).',
	'info_document' => 'Dokument',
	'info_document_indisponible' => 'Dokument nicht verfügbar',
	'info_documents' => 'Dokumente',
	'info_gauche_numero_document' => 'DOKUMENT NUMMER',
	'info_hauteur' => 'H&ouml;he',
	'info_heure' => 'Stunde',
	'info_illustrations' => 'Illustrationen',
	'info_image_max_poids' => 'Die Bilder d&uuml;rfen h&ouml;chstens @maxi@ gro&szlig; sein (diese Datei hat @actuel@).',
	'info_inclusion_directe' => 'Direkte Einbindung:',
	'info_inclusion_vignette' => 'Einbindung des Icons:',
	'info_installer_tous_documents' => 'Alle Dokumente installieren',
	'info_largeur' => 'Breite',
	'info_logo_max_taille' => 'Die Logogrösse darf nicht @maxi@ überschreiten (aktuelle Dateigrösse @actuel@).',
	'info_modifier_document' => 'Dokument bearbeiten',
	'info_portfolio' => 'Portfolio',
	'info_referencer_doc_distant' => 'Dokument im Internet verlinken:',
	'info_statut_document' => 'Dieses Dokument ist:',
	'info_telecharger' => 'Von Ihrem Computer hochladen:',
	'infos' => 'Technische Informationen',
	'item_autoriser_selectionner_date_en_ligne' => 'Festlegen eines Datums für jedes Dokument gestatten',
	'item_non_autoriser_selectionner_date_en_ligne' => 'Das Datum der Dokumente entspricht dem Zeitpunkt des Uploads.',

	// L
	'label_activer_document_objets' => 'Dokumente für die Inhalte freischalten:',
	'label_apercu' => 'Vorschau',
	'label_caracteristiques' => 'Eigenschaften',
	'label_credits' => 'Beteiligte',
	'label_fichier' => 'Datei',
	'label_fichier_vignette' => 'Miniaturbild',
	'label_parents' => 'Dieses Dokument geh&ouml;rt zu',
	'label_refdoc_joindre' => 'Dokument Nummer',
	'lien_tout_enlever' => 'Alles entfernen',
	'logo' => 'Logo',

	// M
	'media_audio' => 'T&ouml;ne',
	'media_file' => 'Andere',
	'media_image' => 'Bilder',
	'media_video' => 'Videos',

	// N
	'nb_documents_installe_succes' => '@nb@ Dateien wurden erfolgreich &uuml;bertragen.',

	// O
	'objet_document' => 'Dokument',
	'objet_documents' => 'Dokumente',

	// P
	'par_date' => 'Datum',
	'par_hauteur' => 'H&ouml;he',
	'par_id' => 'ID',
	'par_largeur' => 'Breite',
	'par_taille' => 'Gr&ouml;&szlig;e',
	'par_titre' => 'Titel',

	// T
	'texte_documents_joints' => 'Sie können die Zuordnung von Dokumenten (Office-Dokumente, Bilder, Multimedia-Dateien, etc.) zu Artikeln und/oder Rubriken erlauben. Diese Dokumente können anschliessend innerhalb von Artikeln verlinkt oder separat angezeigt werden.<p>',
	'texte_documents_joints_2' => 'Diese Einstellung widerspricht nicht dem Einfügen von Bildern in Artikel.',
	'titre_documents_joints' => 'beigefügte Dokumente',
	'titre_page_documents_edit' => 'Dokument bearbeiten: @titre@',
	'tous_les_medias' => 'Alle Medien',
	'tout_dossier_upload' => 'Das ganze Verzeichnis @upload@',
	'tout_voir' => 'Alle anzeigen',

	// U
	'un_audio' => '1 Ton',
	'un_document' => '1 Dokument',
	'un_file' => '1 Dokument',
	'un_image' => '1 Bild',
	'un_video' => '1 Video',
	'une_utilisation' => '1 Verwendung',
	'upload_fichier_zip' => 'ZIP-Datei',
	'upload_fichier_zip_texte' => 'Sie möchsten eine ZIP-Datei installieren.',
	'upload_fichier_zip_texte2' => 'SPIP kann:',
	'upload_info_mode_document' => 'Im Portfolio ablegen',
	'upload_info_mode_image' => 'Aus dem Portfolio entfernen',
	'upload_limit' => 'Datei zu gross zum Hochladen. Maximale <i>upload</i>-Grösse: @max@.',
	'upload_zip_conserver' => 'Archiv nach Entpacken beibehalten',
	'upload_zip_decompacter' => 'Das ZIP-Archiv auspacken. Diese Dateien werden dann auf dem Server installiert:',
	'upload_zip_mode_document' => 'Alle Bilder im Portfolio ablegen',
	'upload_zip_telquel' => 'Das ZIP-Archiv als eine Archivdatei installieren.',
	'upload_zip_titrer' => 'Dateinamen als Titel verwenden',

	// V
	'verifier_documents_brises' => 'Fehlende Dateien pr&uuml;fen',
	'verifier_documents_inutilises' => 'Links der Dokumente prüfen',
	'vignette_supprimee' => 'Das MIniaturbild wurde gel&ouml;scht.'
);

?>
