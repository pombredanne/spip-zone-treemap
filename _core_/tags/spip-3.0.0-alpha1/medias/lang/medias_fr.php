<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_core_/plugins/medias/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_audio' => 'Aucune bande son',
	'aucun_document' => 'Aucun document',
	'aucun_file' => 'Aucun document',
	'aucun_image' => 'Aucune image',
	'aucun_video' => 'Aucune s&eacute;quence',
	'aucune_utilisation' => 'Ce document n\'est pas utilis&eacute;',
	'aucune_vignette' => 'Aucune vignette',

	// B
	'bouton_ajouter_document' => 'Ajouter un document',
	'bouton_ajouter_image' => 'Ajouter une image',
	'bouton_ajouter_image_document' => 'Ajouter une image ou un document',
	'bouton_attacher_document' => 'Attacher',
	'bouton_copier_local' => 'Copier dans le site',
	'bouton_download_depuis' => 'depuis&nbsp;:',
	'bouton_download_local' => 'mon ordinateur',
	'bouton_download_par_ftp' => 'le serveur',
	'bouton_download_par_mediatheque' => 'la mediath&egrave;que',
	'bouton_download_sur_le_web' => 'internet',
	'bouton_enlever_document' => 'Detacher',
	'bouton_enlever_supprimer_document' => 'Supprimer',
	'bouton_enlever_supprimer_document_confirmation' => 'Etes-vous s&ucirc;r de vouloir supprimer le document ?',
	'bouton_modifier_document' => 'Modifier',
	'bouton_parcourir' => 'Parcourir...',
	'bouton_refresh_document' => 'Rafraichir le document',
	'bouton_remplacer_document' => 'Remplacer ce document',
	'bouton_remplacer_image' => 'Remplacer cette image',
	'bouton_remplacer_image_document' => 'Remplacer par une image ou un document',
	'bouton_supprimer' => 'Supprimer',
	'bouton_supprimer_tous_orphelins' => 'Supprimer tous les orphelins',
	'bouton_supprimer_tous_orphelins_confirmation' => 'Etes-vous s&ucirc;r de vouloir supprimer @nb@ orphelins ?',
	'bouton_upload' => 'T&eacute;l&eacute;charger',

	// D
	'des_audios' => '@nb@ bandes sons',
	'des_documents' => '@nb@ documents',
	'des_files' => '@nb@ documents',
	'des_images' => '@nb@ images',
	'des_utilisations' => '@nb@ utilisations',
	'des_videos' => '@nb@ s&eacute;quences',
	'descriptif' => 'Description',
	'document_attache_succes' => 'Le document a bien &eacute;t&eacute; ajout&eacute;',
	'document_copie_locale_succes' => 'Le fichier a bien &eacute;t&eacute; copi&eacute; dans le site',
	'document_installe_succes' => 'Le fichier a bien &eacute;t&eacute; charg&eacute;',
	'document_vu' => 'Ins&eacute;r&eacute; dans le texte',
	'documents' => 'Mediath&egrave;que',
	'documents_brises' => 'Bris&eacute;s',
	'documents_distants' => 'Distants',
	'documents_locaux' => 'Locaux',
	'documents_non_publies' => 'Non publi&eacute;s',
	'documents_orphelins' => 'Inutilis&eacute;s',
	'documents_publies' => 'Publi&eacute;s',
	'documents_tous' => 'Tous',
	'double_clic_inserer_doc' => 'Double-cliquez pour insérer ce raccourci dans le texte',

	// E
	'entree_dimensions' => 'Dimensions :',
	'entree_titre_document' => 'Titre du document :',
	'entree_titre_image' => 'Titre de l\'image :',
	'erreur_aucun_document' => 'Ce document n\'existe pas dans la mediath&egrave;que',
	'erreur_aucun_fichier' => 'Aucun fichier n\'a &eacute;t&eacute; trouv&eacute;',
	'erreur_chemin_distant' => 'Le fichier distant @nom@ n\'a pas pu &ecirc;tre trouv&eacute;',
	'erreur_chemin_ftp' => 'Le fichier indiqu&eacute; n\'a pas &eacute;t&eacute; trouv&eacute; sur le serveur',
	'erreur_copie_fichier' => 'Impossible de copier le fichier @nom@',
	'erreur_dossier_tmp_manquant' => 'Un dossier temporaire est manquant pour télécharger les fichiers',
	'erreur_ecriture_fichier' => 'Erreur lors de l\'écriture du fichier sur le disque',
	'erreur_format_fichier_image' => 'Le format de @nom@ ne convient pas pour une image',
	'erreur_indiquez_un_fichier' => 'Indiquez un fichier !',
	'erreur_insertion_document_base' => 'Impossible d\'enregistrer le document @fichier@ en base de donn&eacute;es',
	'erreur_suppression_vignette' => 'Erreur lors de la suppression de la vignette',
	'erreur_upload_type_interdit' => 'Le telechargement des fichiers du type de @nom@ n\'est pas autoris&eacute;',
	'erreur_upload_vignette' => 'Erreur lors du chargement de la vignette @nom@',
	'erreurs' => '@nb@ erreurs',
	'erreurs_voir' => 'Voir les @nb@ erreurs',

	// F
	'fichier_distant' => 'Fichier distant',
	'fichier_manquant' => 'Fichier manquant',
	'fichier_modifie' => 'Le fichier a &eacute;t&eacute; modifi&eacute;. Cliquez sur enregistrer.',
	'format_date_incorrect' => 'La date ou l\'heure sont incorrectes',

	// I
	'icone_creer_document' => 'Ajouter un document',
	'id' => 'ID',
	'image_tourner_180' => 'Rotation 180°',
	'image_tourner_droite' => 'Rotation 90° à droite',
	'image_tourner_gauche' => 'Rotation 90° à gauche',
	'info_doc_max_poids' => 'Les documents doivent obligatoirement faire moins de @maxi@ (ce fichier fait @actuel@).',
	'info_document' => 'Document',
	'info_document_indisponible' => 'Ce document n\'est pas disponible',
	'info_documents' => 'Documents',
	'info_gauche_numero_document' => 'DOCUMENT NUM&Eacute;RO',
	'info_hauteur' => 'Hauteur',
	'info_heure' => 'Heure',
	'info_illustrations' => 'Illustrations',
	'info_image_max_poids' => 'Les images doivent obligatoirement faire moins de @maxi@ (ce fichier fait @actuel@).',
	'info_inclusion_directe' => 'Inclusion directe :',
	'info_inclusion_vignette' => 'Inclusion de la vignette :',
	'info_installer_tous_documents' => 'Installer tous les documents',
	'info_largeur' => 'Largeur',
	'info_logo_max_taille' => 'Les logos doivent obligatoirement faire moins de @maxi@ (ce fichier fait @actuel@).',
	'info_modifier_document' => 'Modifier le document',
	'info_portfolio' => 'Portfolio',
	'info_referencer_doc_distant' => 'Référencer un document sur l\'internet :',
	'info_statut_document' => 'Ce document est&nbsp;:',
	'info_telecharger' => 'Télécharger depuis votre ordinateur :',
	'infos' => 'Infos techniques',
	'item_autoriser_selectionner_date_en_ligne' => 'Permettre de modifier la date de chaque document',
	'item_non_autoriser_selectionner_date_en_ligne' => 'La date des documents est celle de leur ajout sur le site',

	// L
	'label_activer_document_objets' => 'Activer les documents pour les contenus&nbsp;:',
	'label_apercu' => 'Aper&ccedil;u',
	'label_caracteristiques' => 'Caract&eacute;ristiques',
	'label_credits' => 'Cr&eacute;dits',
	'label_fichier' => 'Fichier',
	'label_fichier_vignette' => 'Vignette',
	'label_parents' => 'Ce document est li&eacute; &agrave;',
	'label_refdoc_joindre' => 'Document num&eacute;ro',
	'lien_tout_enlever' => 'Tout enlever',
	'logo' => 'Logo',

	// M
	'media_audio' => 'Bandes sons',
	'media_file' => 'Autres',
	'media_image' => 'Images',
	'media_video' => 'S&eacute;quences',

	// N
	'nb_documents_installe_succes' => '@nb@ fichiers charg&eacute;s avec succ&egrave;s',

	// O
	'objet_document' => 'Document',
	'objet_documents' => 'Documents',

	// P
	'par_date' => 'Date',
	'par_hauteur' => 'Hauteur',
	'par_id' => 'ID',
	'par_largeur' => 'Largeur',
	'par_taille' => 'Poids',
	'par_titre' => 'Titre',

	// T
	'texte_documents_joints' => 'Vous pouvez autoriser l\'ajout de documents (fichiers bureautiques, images,
 multimédia, etc.) aux articles et/ou aux rubriques. Ces fichiers
 peuvent ensuite être référencés dans
 l\'article, ou affichés séparément.</p>',
	'texte_documents_joints_2' => 'Ce réglage n\'empêche pas l\'insertion d\'images directement dans les articles.',
	'titre_documents_joints' => 'Documents joints',
	'titre_page_documents_edit' => 'Modifier le document : @titre@',
	'tous_les_medias' => 'Tous les medias',
	'tout_dossier_upload' => 'Tout le dossier @upload@',
	'tout_voir' => 'Tout voir',

	// U
	'un_audio' => '1 bande son',
	'un_document' => '1 document',
	'un_file' => '1 document',
	'un_image' => '1 image',
	'un_video' => '1 s&eacute;quence',
	'une_utilisation' => '1 utilisation',
	'upload_fichier_zip' => 'Fichier ZIP',
	'upload_fichier_zip_texte' => 'Le fichier que vous proposez d\'installer est un fichier Zip.',
	'upload_fichier_zip_texte2' => 'Ce fichier peut être :',
	'upload_info_mode_document' => 'D&eacute;poser dans le portfolio',
	'upload_info_mode_image' => 'Retirer du portfolio',
	'upload_limit' => 'Ce fichier est trop gros pour le serveur ; la taille maximum autorisée en <i>upload</i> est de @max@.',
	'upload_zip_conserver' => 'Conserver l’archive après extraction',
	'upload_zip_decompacter' => 'décompressé et chaque élément qu\'il contient installé sur le site. Les fichiers qui seront alors installés sur le site sont :',
	'upload_zip_mode_document' => 'D&eacute;poser toutes les images dans le portfolio',
	'upload_zip_telquel' => 'installé tel quel, en tant qu\'archive compressée Zip ;',
	'upload_zip_titrer' => 'Titrer selon le nom des fichiers',

	// V
	'verifier_documents_brises' => 'V&eacute;rifier les fichiers manquants',
	'verifier_documents_inutilises' => 'V&eacute;rifier les liens des documents',
	'vignette_supprimee' => 'La vignette a &eacute;t&eacute; supprim&eacute;e'
);

?>
