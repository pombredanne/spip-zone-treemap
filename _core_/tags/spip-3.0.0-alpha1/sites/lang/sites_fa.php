<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_dispo' => 'En attente', # NEW
	'articles_meme_auteur' => 'Tous les articles de cet auteur', # NEW
	'articles_off' => 'Bloqués', # NEW
	'articles_publie' => 'Publiés', # NEW
	'articles_refuse' => 'Supprimés', # NEW
	'articles_tous' => 'Tous', # NEW
	'aucun_article_syndic' => 'Aucun article syndiqué', # NEW
	'avis_echec_syndication_01' => 'ارتباط با سايتهاى ديگر برقرار نشد : يا فايل بك اند نا خوانا ست يا هيچ مقاله اى ندارد',
	'avis_echec_syndication_02' => 'ارتباط با سايتهاى ديگر برقرار نشد : دسترسى به بك اند در اين سايت ممكن نميباشد       ',
	'avis_site_introuvable' => 'سايت پيدا نميشود',
	'avis_site_syndique_probleme' => 'توجه: مشترك‌سازي اين سايت به مشكلى برخورد كرده است؛ و سيستم موقتأ  قطع مي‌باشد. به فايل فهرست تازه ها ى اين سايت  نگاه كنيد، (<b>@url_syndic@</b>)و دوباره براى دريافت اطلاعات جديد اقدام كنيد. ',
	'avis_sites_probleme_syndication' => 'اين سايتها مشكل پيوند به سايتهاى ديگر را دارند ',
	'avis_sites_syndiques_probleme' => 'اين سايتهاى پيوندى به مشكلى برخورده اند',

	// B
	'bouton_radio_modere_posteriori' => 'اعلان پيش از تأئيد',
	'bouton_radio_modere_priori' => 'اعلان پس از تأئيد',
	'bouton_radio_non_syndication' => 'بدون پيوند به سايتهاى ديگر',
	'bouton_radio_syndication' => 'پيوند سايتى',

	// C
	'confirmer_purger_syndication' => 'Êtes-vous certain de vouloir supprimer tous les articles syndiqués de ce site ?', # NEW

	// E
	'entree_adresse_fichier_syndication' => 'آدرس فايل براى ارتباط ميان سايتى:',
	'entree_adresse_site' => ' </b> آدرس سايت <b>[اجبارى] ',
	'entree_description_site' => 'توصيف سايت',

	// F
	'form_prop_nom_site' => 'نام سايت',

	// I
	'icone_article_syndic' => 'Article syndiqué', # NEW
	'icone_articles_syndic' => 'Articles syndiqués', # NEW
	'icone_controler_syndication' => 'Publication des articles syndiqués', # NEW
	'icone_modifier_site' => 'اين سايت را اصلاح كنيد',
	'icone_referencer_nouveau_site' => 'مشخصات يك سايت جديد را بدهيد',
	'icone_site_reference' => 'Sites référencés', # NEW
	'icone_supprimer_article' => 'Supprimer cet article', # NEW
	'icone_supprimer_articles' => 'Supprimer ces articles', # NEW
	'icone_valider_article' => 'Valider cet article', # NEW
	'icone_valider_articles' => 'Valider ces articles', # NEW
	'icone_voir_sites_references' => 'به سايتهاى مرجع نگاه كنيد',
	'info_a_valider' => '[معتبر شود]',
	'info_bloquer' => 'ببندید',
	'info_bloquer_lien' => 'اين پيوند را مسدود كنيد',
	'info_derniere_syndication' => ':آخرين پيوند سايتى انجام شده در تاريخ ',
	'info_liens_syndiques_1' => 'پيوند ميان سايتى',
	'info_liens_syndiques_2' => '.در انتظار تأئيد شدن هستند',
	'info_nom_site_2' => ' [اجبارى] <b>نام سايت</b>',
	'info_panne_site_syndique' => 'سايت پيوندى خراب است',
	'info_probleme_grave' => 'نقص در',
	'info_question_proposer_site' => 'كى ميتواند سايتهاى مرجع را پيشنهاد كند ؟',
	'info_retablir_lien' => 'اين پيوند را دوباره برقرار كنيد',
	'info_site_attente' => 'سايت در انتظار تائيد',
	'info_site_propose' => ':اين سايت در تاريخ زير پيشنهاد شده ',
	'info_site_reference' => 'سايت مرجع روى خط ',
	'info_site_refuse' => 'سايت پذيرفته نشده',
	'info_site_syndique' => '...اين سايت پيوند دارد',
	'info_site_valider' => 'اين سايت ها بايد معتبر شوند',
	'info_sites_referencer' => 'ارجاع يك سايت',
	'info_sites_refuses' => 'سايت هاى پذيرفته نشده',
	'info_statut_site_1' => ': اين سايت',
	'info_statut_site_2' => 'منتشر شده',
	'info_statut_site_3' => 'پيشنهاد شده',
	'info_statut_site_4' => 'در سطل',
	'info_syndication' => ':پيوند سايتى',
	'info_syndication_articles' => 'مقاله',
	'item_bloquer_liens_syndiques' => 'پيوندهاى سايتى را براى تأئيد مسدود كنيد',
	'item_gerer_annuaire_site_web' => 'يك راهنما از سايتهاى تارنما بسازيد',
	'item_non_bloquer_liens_syndiques' => 'پيوندهاى نتيجه ى ارتباط ميان سايتى را مسدود نكنيد',
	'item_non_gerer_annuaire_site_web' => 'راهنماى سايتهاى تارنما را غير فعال كنيد',
	'item_non_utiliser_syndication' => 'از پيوند سايتى خودكار استفاده نكنيد',
	'item_utiliser_syndication' => 'از پيوند سايتى خودكار استفاده كنيد',

	// L
	'lien_mise_a_jour_syndication' => 'اكنون به روز كنيد',
	'lien_nouvelle_recuperation' => 'اقدام به بازگيرى دوباره داده ها كنيد',
	'lien_purger_syndication' => 'Effacer tous les articles syndiqués', # NEW

	// N
	'nombre_articles_syndic' => '@nb@ articles syndiqués', # NEW

	// S
	'statut_off' => 'Supprimé', # NEW
	'statut_prop' => 'En attente', # NEW
	'statut_publie' => 'Publié', # NEW
	'syndic_choix_moderation' => 'برای پیوندهای بعدی از سوی این سایت چه کار میکنید؟',
	'syndic_choix_oublier' => 'برای پیوندهایی که در فایل سایتهای مرجع یافت نمیشوند چه میکنید ؟',
	'syndic_choix_resume' => 'بعضی از سایتها متن کامل مقالات را عرضه میکنند. در اینصورت آیا مایلید که آنرا به سایتتان پیوند بزنید:',
	'syndic_lien_obsolete' => 'پیوند بیفایده',
	'syndic_option_miroir' => 'بطور خودکار مسدود کنید',
	'syndic_option_oubli' => 'پاک کنید (پس از @mois@ ماه)',
	'syndic_option_resume_non' => 'متن کامل مقالات (به زبان HTML)',
	'syndic_option_resume_oui' => 'خلاصه ی مقاله (بصورت متن)',
	'syndic_options' => 'گزینش سایتهای مرجع :',

	// T
	'texte_liens_sites_syndiques' => 'پيوندهاى مربوط به سايتهاى مرجع ميتوانند مسدود شوند، سپس ميتوانيد آنها را تك تك باز كنيد يا اينكه سايت به سايت پيوندهاى آينده را مسدود كنيد. ',
	'texte_messages_publics' => 'پيامهاى همگانى مقاله :',
	'texte_non_fonction_referencement' => 'شما ميتوانيد از اين كاربرد خودكار استفاده نكنيد، دراينصورت خودتان بايد مشخصات سايت را تعيين كنيد',
	'texte_referencement_automatique' => 'ثبت خودكار يك سايت در جستجوگرها، شما ميتوانيد ثبت تارنماى سايت را با مشخص كردن آدرس URLو يا آدرس فايل RSS در زير انجام  دهيد.اسپيپ تمام اطلاعات اعم از عنوان، توصيف،... را بطور خودكار جمع آورى خواهد كرد.',
	'texte_referencement_automatique_verifier' => 'لطفاً پيش از ثبت اطلاعات ارايه شده توسط <tt>@url@</tt> آن را تأييد كنيد.',
	'texte_syndication' => 'اگر سايت اجازه دهد، مي‌توانيد بطور خودكار،  فهرست تازه‌ها را بگيريد. براى اين منظور، شما بايد گزينش پخش همزمان (سنديكاسيون) را فعال كنيد.<blockquote><i> تعدادى از ميزبانان اين عمل را غيرفعال مي‌كنند. در آن صورت نمي‌توانيد از آن استفاده كنيد.</i></blockquote>',
	'titre_articles_syndiques' => 'مقالات پيوندى گرفته شده از اين سايت',
	'titre_dernier_article_syndique' => 'آخرين مقالات پيوندى',
	'titre_page_sites_tous' => 'سايتهاى مرجع',
	'titre_referencement_sites' => 'ارجاع و پيوند سايتها ',
	'titre_site_numero' => 'سايت شماره :',
	'titre_sites_proposes' => 'سايتهاى پيشنهادى',
	'titre_sites_references_rubrique' => 'سايتهاى مرجع در اين بخش',
	'titre_sites_syndiques' => 'سايتهاى پيوندى',
	'titre_sites_tous' => 'سايتهاى مرجع',
	'titre_syndication' => 'پيوند سايتها',
	'tout_voir' => 'Voir tous les articles syndiqués', # NEW

	// U
	'un_article_syndic' => '1 article syndiqué' # NEW
);

?>
