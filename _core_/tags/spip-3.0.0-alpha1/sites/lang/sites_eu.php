<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_dispo' => 'En attente', # NEW
	'articles_meme_auteur' => 'Tous les articles de cet auteur', # NEW
	'articles_off' => 'Bloqués', # NEW
	'articles_publie' => 'Publiés', # NEW
	'articles_refuse' => 'Supprimés', # NEW
	'articles_tous' => 'Tous', # NEW
	'aucun_article_syndic' => 'Aucun article syndiqué', # NEW
	'avis_echec_syndication_01' => 'Sindikazioak huts egin du: adierazitako "backend" delakoa ulertezina da edo ez du artikulurik proposatzen.',
	'avis_echec_syndication_02' => 'Sindikazioak huts egin du: leku honen "backend" delakora iristea ezinezkoa da.',
	'avis_site_introuvable' => 'Gunea ez da aurkitzen',
	'avis_site_syndique_probleme' => 'ADI: Leku honen sindikazioak arazoak topatzen ditu. Sistema etenik dago oraingoz.(@url_syndic@) leku horren sindikaziorako fitxategiaren helbidea egiazta ezazu, etasaia ezazu informazioen berreskurapen berri bat. ',
	'avis_sites_probleme_syndication' => 'Leku hauek sindikazio-arazo bat dute',
	'avis_sites_syndiques_probleme' => 'Sindikatutako leku hauek arazoak dituzte',

	// B
	'bouton_radio_modere_posteriori' => 'gerora moderatua',
	'bouton_radio_modere_priori' => 'aurretiaz moderatua',
	'bouton_radio_non_syndication' => 'Sindikaziorik ez',
	'bouton_radio_syndication' => 'Sindikazioa',

	// C
	'confirmer_purger_syndication' => 'Êtes-vous certain de vouloir supprimer tous les articles syndiqués de ce site ?', # NEW

	// E
	'entree_adresse_fichier_syndication' => 'Sindikazioarako "backend"fitxeroaren helbidea.',
	'entree_adresse_site' => 'Gunearen helbidea [Derrigorrezkoa]',
	'entree_description_site' => 'Gunearen deskribapena',

	// F
	'form_prop_nom_site' => 'Gunearen izena',

	// I
	'icone_article_syndic' => 'Article syndiqué', # NEW
	'icone_articles_syndic' => 'Articles syndiqués', # NEW
	'icone_controler_syndication' => 'Publication des articles syndiqués', # NEW
	'icone_modifier_site' => 'Leku hau aldatu',
	'icone_referencer_nouveau_site' => 'Leku berri bat aipatu',
	'icone_site_reference' => 'Sites référencés', # NEW
	'icone_supprimer_article' => 'Supprimer cet article', # NEW
	'icone_supprimer_articles' => 'Supprimer ces articles', # NEW
	'icone_valider_article' => 'Valider cet article', # NEW
	'icone_valider_articles' => 'Valider ces articles', # NEW
	'icone_voir_sites_references' => 'Aipatutako lekuak ikusi',
	'info_a_valider' => '[egiaztatzeko]',
	'info_bloquer' => 'Blokatu',
	'info_bloquer_lien' => 'Lotura hau blokatu',
	'info_derniere_syndication' => 'Gune honen azken sindikazioa egin zen eguna:',
	'info_liens_syndiques_1' => 'esteka sindikatuak',
	'info_liens_syndiques_2' => 'balidazioaren zain daude.',
	'info_nom_site_2' => 'Gunearen izena [Nahitaezkoa]',
	'info_panne_site_syndique' => 'Sindikatutako guneak arazoak dauzka',
	'info_probleme_grave' => '-ren arazoa',
	'info_question_proposer_site' => 'Nork proposatu ahal ditu gune erreferentziatuak ?',
	'info_retablir_lien' => 'esteka berrezarri',
	'info_site_attente' => 'Balioztatua izateko zain dagoen Webgunea',
	'info_site_propose' => 'Toki proposatua',
	'info_site_reference' => 'Linean erreferentziatutako gunea',
	'info_site_refuse' => 'Web Gune errefusatua',
	'info_site_syndique' => 'Gune hau sindikatuta dago...',
	'info_site_valider' => 'Balidatzeko guneak',
	'info_sites_referencer' => 'Gune bat erreferentziatu',
	'info_sites_refuses' => 'Ezetsitako guneak',
	'info_statut_site_1' => 'Gune hau dago :',
	'info_statut_site_2' => 'Argitaratuta',
	'info_statut_site_3' => 'Proposatuta',
	'info_statut_site_4' => 'Zakarrontzian',
	'info_syndication' => 'Sindikazioa :',
	'info_syndication_articles' => 'artikulua(k)',
	'item_bloquer_liens_syndiques' => 'Onartzeko prozesuan dauden sindikatutako loturakblokeatu',
	'item_gerer_annuaire_site_web' => 'Web lekuen urtekari bat kudeatu',
	'item_non_bloquer_liens_syndiques' => 'Ez blokeatu sindikazio-loturak',
	'item_non_gerer_annuaire_site_web' => 'Web lekuen urtekaria desaktibatu',
	'item_non_utiliser_syndication' => 'Ez erabili sindikazio automatikoa',
	'item_utiliser_syndication' => 'Sindikazio automatikoa erabili',

	// L
	'lien_mise_a_jour_syndication' => 'Orain gaurkotu',
	'lien_nouvelle_recuperation' => 'Datuak berreskuratzen berriro saiatu',
	'lien_purger_syndication' => 'Effacer tous les articles syndiqués', # NEW

	// N
	'nombre_articles_syndic' => '@nb@ articles syndiqués', # NEW

	// S
	'statut_off' => 'Supprimé', # NEW
	'statut_prop' => 'En attente', # NEW
	'statut_publie' => 'Publié', # NEW
	'syndic_choix_moderation' => 'Zer egin gune huntarik etorriko diren hurrengi loturekin ?',
	'syndic_choix_oublier' => 'Sindikazio fitxategietan gehiago agertzen ez diren loturekin zer egin ?',
	'syndic_choix_resume' => 'Gune zonbaitzuek artikuluen testu osoa hedatzen dute. eskugarri delarik sindikatu al nahi duzu :',
	'syndic_lien_obsolete' => 'Zahartutako lotura',
	'syndic_option_miroir' => 'automatikoki blokatu',
	'syndic_option_oubli' => 'ezabatu (@mois@ hilabeteren ondotik)',
	'syndic_option_resume_non' => 'Artikuluen eduki osoa (HTML formatuan)',
	'syndic_option_resume_oui' => 'bilduma sinple bat (testu formatuan)',
	'syndic_options' => 'Sindikazio aukerak :',

	// T
	'texte_liens_sites_syndiques' => 'sindikatutakoguneetako estekak a priori blokeatu ahal dira ; azpiko konfigurazioak sindikatutakoguneen konfigurazio lehenetsia adierazten du bere sorrera ondoren. Hala ere, gero estekabakoitza banan-banan desblokeatzea posible da, edo lekuz leku, gune batetik edo bestetik datozenestekak blokeatzeko aukera izatea.',
	'texte_messages_publics' => 'Artikuluaren mezu publikoak',
	'texte_non_fonction_referencement' => 'Agian nahiago duzu funtzio automatiko hau ezerabili, eta zuk zeuk adierazi toki honi dagozkien osagaiak...',
	'texte_referencement_automatique' => '<b>Leku bat automatikoki erreferentzia egitea</b>
Leku baten URL-a edo lekuaren "backend" artxiboaren helbidea, SPIP automatikoki argibideak (izenburua, azalpena....) berreskuratuko ditu.',
	'texte_referencement_automatique_verifier' => 'Veuillez vérifier les informations fournies par <tt>@url@</tt> avant d\'enregistrer.', # NEW
	'texte_syndication' => 'Berritasunen zerrenda, Web-guneak halauzten duenean, automatikoki berreskuratzea posible da.
Horretarako, bilketa aktibatu behar duzu. Ostapen zuzkitzaile batzuk ez dute funtzionalitate hau aktibatzen; kasu horretan, ezin izango duzu zure gunearen edukiaren bilketa egin.',
	'titre_articles_syndiques' => 'Toki honetako elkartutako artikuluak',
	'titre_dernier_article_syndique' => 'Azken artikulu elkartuak',
	'titre_page_sites_tous' => 'Erreferentziatutako tokiak',
	'titre_referencement_sites' => 'Toki erreferentziaketa etaelkarketa',
	'titre_site_numero' => 'Tokia',
	'titre_sites_proposes' => 'Proposatutako tokiak',
	'titre_sites_references_rubrique' => 'Atal honetan erreferentziatutako tokiak',
	'titre_sites_syndiques' => 'Elkartutako tokiak',
	'titre_sites_tous' => 'Erreferentziatutako tokiak',
	'titre_syndication' => 'Tokien elkarpena',
	'tout_voir' => 'Voir tous les articles syndiqués', # NEW

	// U
	'un_article_syndic' => '1 article syndiqué' # NEW
);

?>
