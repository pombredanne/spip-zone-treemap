<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_dispo' => 'En attente', # NEW
	'articles_meme_auteur' => 'Tous les articles de cet auteur', # NEW
	'articles_off' => 'Bloqués', # NEW
	'articles_publie' => 'Publiés', # NEW
	'articles_refuse' => 'Supprimés', # NEW
	'articles_tous' => 'Tous', # NEW
	'aucun_article_syndic' => 'Aucun article syndiqué', # NEW
	'avis_echec_syndication_01' => 'D\'Syndicatioun huet nët fonktionnéiert: de Backend ass nët ze dekodéieren oder en proposéiert guer keen Artikel.',
	'avis_echec_syndication_02' => 'D\'Syndicatioun huet nët fonktionnéiert: onméiglech de Backend vun dësem Site ze ereechen.',
	'avis_site_introuvable' => 'De Site ass nët ze fannen',
	'avis_site_syndique_probleme' => 'Opgepasst: d\'Syndicatioun vun dësem Site huet nët fonktionnéiert; de System ass elo zäitweis ënnerbrach. Kontrolléiert d\'Adress vum Syndicatiouns-Fichier vun dësem Site (<b>@url_syndic@</b>) a probéiert nach eng Kéier d\'Informatiounen ze kréien.',
	'avis_sites_probleme_syndication' => 'Dës Siten hun e Syndicatiouns-Problem',
	'avis_sites_syndiques_probleme' => 'Dës syndikéiert Siten hun e Problem',

	// B
	'bouton_radio_modere_posteriori' => 'modéréiert "à posteriori"',
	'bouton_radio_modere_priori' => 'modéréiert "à priori"',
	'bouton_radio_non_syndication' => 'Keng Syndicatioun',
	'bouton_radio_syndication' => 'Syndicatioun:',

	// C
	'confirmer_purger_syndication' => 'Êtes-vous certain de vouloir supprimer tous les articles syndiqués de ce site ?', # NEW

	// E
	'entree_adresse_fichier_syndication' => 'Adress vum Fichier fir d\'Syndicatioun:',
	'entree_adresse_site' => '<b>Adress vum Site</b> [Obligatoresch]',
	'entree_description_site' => 'Beschreiwung vum Site',

	// F
	'form_prop_nom_site' => 'Numm vum Site',

	// I
	'icone_article_syndic' => 'Article syndiqué', # NEW
	'icone_articles_syndic' => 'Articles syndiqués', # NEW
	'icone_controler_syndication' => 'Publication des articles syndiqués', # NEW
	'icone_modifier_site' => 'Dëse Site änneren',
	'icone_referencer_nouveau_site' => 'Neie Site uginn',
	'icone_site_reference' => 'Sites référencés', # NEW
	'icone_supprimer_article' => 'Supprimer cet article', # NEW
	'icone_supprimer_articles' => 'Supprimer ces articles', # NEW
	'icone_valider_article' => 'Valider cet article', # NEW
	'icone_valider_articles' => 'Valider ces articles', # NEW
	'icone_voir_sites_references' => 'Referenzéiert Siten weisen',
	'info_a_valider' => '[ze validéieren]',
	'info_bloquer' => 'blockéieren',
	'info_bloquer_lien' => 'Link blockéieren',
	'info_derniere_syndication' => 'Läscht Syndicatioun vun dësem Site den',
	'info_liens_syndiques_1' => 'Syndikéiert Linken',
	'info_liens_syndiques_2' => 'waarden ob eng Validatioun.',
	'info_nom_site_2' => '<b>Numm vun ärem Site</b> [Obligatoresch]',
	'info_panne_site_syndique' => 'Pann vum syndiquéierten Site',
	'info_probleme_grave' => 'Problem mat',
	'info_question_proposer_site' => 'Wie kann e Site proposéieren?',
	'info_retablir_lien' => 'Dëse Link zerëcksetzen',
	'info_site_attente' => 'Site waard op d\'Validatioun',
	'info_site_propose' => 'Site proposéiert den:',
	'info_site_reference' => 'Referenzéierten Site, publizéiert',
	'info_site_refuse' => 'Refuséierten Site',
	'info_site_syndique' => 'Dëse Site ass syndiquéiert...',
	'info_site_valider' => 'Siten ze validéieren',
	'info_sites_referencer' => 'E Site referenzéieren',
	'info_sites_refuses' => 'Déi refuséiert Siten',
	'info_statut_site_1' => 'Dëse Site ass:',
	'info_statut_site_2' => 'Publizéiert',
	'info_statut_site_3' => 'Proposéiert',
	'info_statut_site_4' => 'An der Dreckskëscht',
	'info_syndication' => 'Syndicatioun:',
	'info_syndication_articles' => 'Artikel(en)',
	'item_bloquer_liens_syndiques' => 'Déi syndiquéiert Linken fir eng Validatioun blockéieren',
	'item_gerer_annuaire_site_web' => 'Eng Link-Sammlung maachen',
	'item_non_bloquer_liens_syndiques' => 'D\'Linken aus der Syndicatioun nët blockéieren',
	'item_non_gerer_annuaire_site_web' => 'D\'Link-Sammlung ausschalten',
	'item_non_utiliser_syndication' => 'Déi automatesch Syndicatioun ausschalten',
	'item_utiliser_syndication' => 'Déi automatesch Syndicatioun aschalten',

	// L
	'lien_mise_a_jour_syndication' => 'Elo updaten',
	'lien_nouvelle_recuperation' => 'Eng nei Restauratioun vun den Daten probéieren',
	'lien_purger_syndication' => 'Effacer tous les articles syndiqués', # NEW

	// N
	'nombre_articles_syndic' => '@nb@ articles syndiqués', # NEW

	// S
	'statut_off' => 'Supprimé', # NEW
	'statut_prop' => 'En attente', # NEW
	'statut_publie' => 'Publié', # NEW
	'syndic_choix_moderation' => 'Wat soll mat de nächste Linken vun dësem Site geschéien?',
	'syndic_choix_oublier' => 'Wat soll mat de Linken geschéien déi nët méi am Syndicatiouns-Fichier sinn?',
	'syndic_choix_resume' => 'Verschidden Siten publizéieren de kompletten Text vun den Artikelen. Wann deen do ass, wëll dir syndiquéieren:',
	'syndic_lien_obsolete' => 'falsche Link',
	'syndic_option_miroir' => 'automatesch blockéieren',
	'syndic_option_oubli' => '(no @mois@ Méint) läschen',
	'syndic_option_resume_non' => 'de kompletten Inhalt vun den Artikelen (am HTML-Format)',
	'syndic_option_resume_oui' => 'en einfachen Résumé (am Text-Format)',
	'syndic_options' => 'Optiounen vun der Syndicatioun:',

	// T
	'texte_liens_sites_syndiques' => 'D\'Linken vun de syndiquéierten Siten kënnen fir d\'éischt mol blockéiert ginn; déi Astellung ass den Default fir déi nei syndiquéiert Siten. Duerno kann een all Link eenzel deblockéieren, oder Site fir Site blockéieren oder nët.',
	'texte_messages_publics' => 'Öffentlëch Messagen vum Artikel:',
	'texte_non_fonction_referencement' => 'Dir wëllt villäicht dës automatesch Fonktioun nët aschalten a selwer d\'Elementer vun ärem Site uginn...',
	'texte_referencement_automatique' => '<b>Automatëscht Referenzéieren vun engem Site</b><br />Dir kënnt e Site séier referenzéieren an dem der hei d\'URL oder d\'Adress vun sengem Syndicatiouns-Fichier ugitt. SPIP kritt dann automatesch d\'Informatiounen (Titel, Beschreiwung...).',
	'texte_referencement_automatique_verifier' => 'Kontrolléiert d\'Informatiounen déi vun <tt>@url@</tt> koum sinn ier dër späichert.',
	'texte_syndication' => 'Et ass méiglech, wann de Site ët erlaabt, automatesch d\'Lëscht vun senge Neiegkeeten ze kréien. Schalt duerfir d\'Syndicatioun an.
<blockquote><i>Verschidde Provideren verhënneren dës Prozedur; an deem Fall kënnt dir d\'Syndicatioun nët op ärem Site benotzen.</i></blockquote>',
	'titre_articles_syndiques' => 'Syndiquéiert Artikelen vun dësem Site',
	'titre_dernier_article_syndique' => 'Läscht syndiquéiert Artikelen',
	'titre_page_sites_tous' => 'Déi referenzéiert Siten',
	'titre_referencement_sites' => 'Siten referenzéieren a Syndicatioun',
	'titre_site_numero' => 'SITE NUMMER:',
	'titre_sites_proposes' => 'Proposéiert Siten',
	'titre_sites_references_rubrique' => 'Referenzéiert Siten an dëser Rubrik',
	'titre_sites_syndiques' => 'Syndiquéiert Siten',
	'titre_sites_tous' => 'Déi referenzéiert Siten',
	'titre_syndication' => 'Syndicatioun vun de Siten',
	'tout_voir' => 'Voir tous les articles syndiqués', # NEW

	// U
	'un_article_syndic' => '1 article syndiqué' # NEW
);

?>
