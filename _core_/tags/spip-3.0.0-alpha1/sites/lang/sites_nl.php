<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_dispo' => 'En attente', # NEW
	'articles_meme_auteur' => 'Tous les articles de cet auteur', # NEW
	'articles_off' => 'Bloqués', # NEW
	'articles_publie' => 'Publiés', # NEW
	'articles_refuse' => 'Supprimés', # NEW
	'articles_tous' => 'Tous', # NEW
	'aucun_article_syndic' => 'Aucun article syndiqué', # NEW
	'avis_echec_syndication_01' => 'De syndicatie is mislukt : de aangeduide backend is niet te ontcijferen of stelt geen enkel artikel voor.',
	'avis_echec_syndication_02' => 'De syndicatie is mislukt : het is onmogelijk de backend van deze site te bereiken.',
	'avis_site_introuvable' => 'Site onvindbaar.',
	'avis_site_syndique_probleme' => 'Opgelet: syndicatie van deze site heeft een probleem ondervonden ; het systeem wordt dus tijdelijk onderbroken. Controleert het adres van het bestand van syndicatie van deze site (<b>@url_syndic@</b>), en probeert nogmaals de informatie binnen te halen.',
	'avis_sites_probleme_syndication' => 'Deze sites hebben een probleem met syndicatie',
	'avis_sites_syndiques_probleme' => 'Deze gesyndiceerde sites hebben een probleem',

	// B
	'bouton_radio_modere_posteriori' => 'nadien modereren',
	'bouton_radio_modere_priori' => 'vooraf modereren',
	'bouton_radio_non_syndication' => 'Geen syndicatie',
	'bouton_radio_syndication' => 'Syndicatie :',

	// C
	'confirmer_purger_syndication' => 'Êtes-vous certain de vouloir supprimer tous les articles syndiqués de ce site ?', # NEW

	// E
	'entree_adresse_fichier_syndication' => 'Adres van het bestand voor syndicatie :',
	'entree_adresse_site' => '<b>website adres</b> [verplicht]',
	'entree_description_site' => 'Site beschrijving',

	// F
	'form_prop_nom_site' => 'Naam van de website',

	// I
	'icone_article_syndic' => 'Article syndiqué', # NEW
	'icone_articles_syndic' => 'Articles syndiqués', # NEW
	'icone_controler_syndication' => 'Publication des articles syndiqués', # NEW
	'icone_modifier_site' => 'Deze site wijzigen',
	'icone_referencer_nouveau_site' => 'Een nieuwe site koppelen',
	'icone_site_reference' => 'Sites référencés', # NEW
	'icone_supprimer_article' => 'Supprimer cet article', # NEW
	'icone_supprimer_articles' => 'Supprimer ces articles', # NEW
	'icone_valider_article' => 'Valider cet article', # NEW
	'icone_valider_articles' => 'Valider ces articles', # NEW
	'icone_voir_sites_references' => 'Gekoppelde sites zien',
	'info_a_valider' => '[ter goedkeuring]',
	'info_bloquer' => 'blokkeren',
	'info_bloquer_lien' => 'deze koppeling tegenhouden',
	'info_derniere_syndication' => 'De laatste syndicatie van deze site heeft plaatsgevonden op ',
	'info_liens_syndiques_1' => 'gesyndiceerde koppelingen',
	'info_liens_syndiques_2' => 'wachten op goedkeuring.',
	'info_nom_site_2' => '<b>Naam van de website</b> [verplicht]',
	'info_panne_site_syndique' => 'Gesyndiceerde site is niet bereikbaar',
	'info_probleme_grave' => 'probleem van',
	'info_question_proposer_site' => 'Wie kan referentiesites voorstellen?',
	'info_retablir_lien' => 'deze link hernieuwen',
	'info_site_attente' => 'website ter goedkeuring voorgelegd',
	'info_site_propose' => 'Site voorgesteld op :',
	'info_site_reference' => 'Online gekoppelde site',
	'info_site_refuse' => 'Website weigerd',
	'info_site_syndique' => 'Gesyndiceerde site...',
	'info_site_valider' => 'Goed te keuren sites',
	'info_sites_referencer' => 'Een site koppelen',
	'info_sites_refuses' => 'De geweigerde websites',
	'info_statut_site_1' => 'Deze site is:',
	'info_statut_site_2' => 'Publicerd',
	'info_statut_site_3' => 'Voorgesteld',
	'info_statut_site_4' => 'Naar de vuilnisbak',
	'info_syndication' => 'nieuwsovername (syndicatie) :',
	'info_syndication_articles' => 'artikel(s)',
	'item_bloquer_liens_syndiques' => 'Gesyndiceerde koppelingen tegenhouden voor goedkeuring ',
	'item_gerer_annuaire_site_web' => 'Een adresboek van de websites aanmaken',
	'item_non_bloquer_liens_syndiques' => 'Koppelingen komende van de syndicatie niet blokkeren',
	'item_non_gerer_annuaire_site_web' => 'Adresboek van websites desactiveren',
	'item_non_utiliser_syndication' => 'Automatische syndicatie niet gebruiken',
	'item_utiliser_syndication' => 'Automatische syndicatie gebruiken',

	// L
	'lien_mise_a_jour_syndication' => 'Update nu',
	'lien_nouvelle_recuperation' => 'Probeer de gegevens opnieuw te vinden',
	'lien_purger_syndication' => 'Effacer tous les articles syndiqués', # NEW

	// N
	'nombre_articles_syndic' => '@nb@ articles syndiqués', # NEW

	// S
	'statut_off' => 'Supprimé', # NEW
	'statut_prop' => 'En attente', # NEW
	'statut_publie' => 'Publié', # NEW
	'syndic_choix_moderation' => 'Wat te doen met volgende links afkomstig van deze site ?',
	'syndic_choix_oublier' => 'Wat te doen met links die niet langer voorkomen in het bestand voor syndicatie ?',
	'syndic_choix_resume' => 'Sommige sites verspreiden de volledige tekst van een artikel. Indien deze beschikbaar is, wil je hem dan ontvangen :',
	'syndic_lien_obsolete' => 'obsoleet link',
	'syndic_option_miroir' => 'automatisch blokkeren',
	'syndic_option_oubli' => 'uitwissen (na @mois@ maanden)',
	'syndic_option_resume_non' => 'de volledige inhoud van de artikels (in HTML formaat)',
	'syndic_option_resume_oui' => 'een eenvoudige samenvattig (tekstformaat)',
	'syndic_options' => 'Syndicatie opties :',

	// T
	'texte_liens_sites_syndiques' => 'Koppelingen komende van gesyndiceerde sites kunnen op voorhand
   geblokkeerd worden; de instelling
   hieronder zegt wat de standaardinstelling is
   voor gesyndiceerde sites als ze aangemaakt worden.
   Het is vervolgens perfect mogelijk om elke koppeling
   individueel te ontgrendelen, of om per site
   te kiezen of je koppelingen wil blokkeren of niet.',
	'texte_messages_publics' => 'Publieke berichten van dit artikel :',
	'texte_non_fonction_referencement' => 'Je kan kiezen om deze automatische functie uit te schakelen en zelf aangeven welke elementen je wil opnemen voor deze site.',
	'texte_referencement_automatique' => '..<b>Geautomatiseerde koppelen van een site</b><br />U kunt een website snel verwijzen door het verlangde naar URL adres, of het adres van zijn bestand van syndication hieronder aan te geven. SPIP zal de informatie automatisch terugkrijgen betreffende deze plaats (titel, beschrijving...).',
	'texte_referencement_automatique_verifier' => 'Gelieve de inlichtingen te controleren die per <tt>@url@</tt> worden verstrekt, alvorens te registreren.',
	'texte_syndication' => 'Het is mogelijk om van een site automatisch een lijst met
 nieuws, als de website het toelaat, binnen te halen.
 Hiervoor dien je de syndicatie (nieuwsovername) te
 activeren. <blockquote><i>Sommige hostingproviders laten
 dit niet toe; in dat geval kan je geen nieuwsovername
 vanaf je site laten gebeuren.</i></blockquote>',
	'titre_articles_syndiques' => 'Gesyndiceerde artikels komende van deze site',
	'titre_dernier_article_syndique' => 'Meest recente gesyndiceerde artikels',
	'titre_page_sites_tous' => 'De referentiesites',
	'titre_referencement_sites' => 'Koppelen van sites en syndicatie',
	'titre_site_numero' => 'WEBSITE NUMMER:',
	'titre_sites_proposes' => 'Voorgestelde sites',
	'titre_sites_references_rubrique' => 'Gekoppelde sites in deze rubriek',
	'titre_sites_syndiques' => 'Gesyndiceerde sites',
	'titre_sites_tous' => 'Gekoppelde sites',
	'titre_syndication' => 'Syndicatie van websites',
	'tout_voir' => 'Voir tous les articles syndiqués', # NEW

	// U
	'un_article_syndic' => '1 article syndiqué' # NEW
);

?>
