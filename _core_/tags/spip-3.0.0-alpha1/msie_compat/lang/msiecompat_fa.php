<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'choix_explication' => '<p>اين پيكربندي انطباق‌پذيري سايت همگاني را با اينترنت اكسپلورر بهبود مي‌دهد.</p>
   <ul>
    <li><a href=\'http://jquery.khurshid.com/ifixpng.php\'>iFixPng</a> (<b>default setting</b>)فقدان شفافيت تصاوير در ماكرو سافت اينترنت اكسپلورر 5 و 6 را بهبود مي‌دهد.</li>
<li><a href=\'http://code.google.com/p/ie7-js/\'>IE7.js</a>شفافيت پي.ان.چي را تصحيح مي‌كند و سلكتور‌هاي سي.اس.اس 2 براي اينترنت اكسپلورر 5 و 6 مي‌افزايد.  (<a href=\'http://ie7-js.googlecode.com/svn/test/index.html\'>در اينجا فهرستي از سلكتور‌هاي انطباق پذير كه توس آي.اي 7 جاوااسكريپت و آي.اي 8 جاوا اسكريپت ارايه شده وجود دارد. </li>
 <li>IE8.js  بهبود مي‌بخشدIE7.js with CSS  سلكتورهاي MSIE 5 to 7.</li>
<li>IE7-squish fixes three bugs in MSIE 6 (including the double margin on floating elements), but side effects may appear. </li>
</ul>',
	'choix_non' => 'غيرفعال: چيزي به اسكلت‌هاي من اضافه نكن',
	'choix_titre' => 'ماكروسافت اينترنت اكسپلورر كامپاتيبليتي (انطباق‌پذيري)'
);

?>
