<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'choix_explication' => 'Diese Konfiguration verbessert die Kompatibilität mit dem Webrowser Internet Explorer.</p>
   <ul>
   <li><a href=\'http://jquery.khurshid.com/ifixpng.php\'>iFixPng</a> (<b>par défaut</b>) stellt die Semi-Transparenz von PNG Dateien in MSIE 5 und 6 wieder her.</li>
   <li><a href=\'http://code.google.com/p/ie7-js/\'>IE7.js</a> korrigiert PNG-Dateien und fügt CSS2 Selektoren für MSIE 5 und 6 hinzu.(<a href=\'http://ie7-js.googlecode.com/svn/test/index.html\'>Liste der mit IE7.js und IE8.js eingeführten kompatiblen Selektoren</a>).</li>
   <li>IE8.js ergänzt IE7.js durch Verbesserung der CSS-Verhaltensweisen von MSIE 5 bis 7. </li>
   <li>IE7-squish korrigiert drei Fehler des MSIE 6 (speziell verdoppelte margins bei float-Elementen), unerwünschte Nebeneffekte können jedoch nicht vollkommen ausgeschlossen werden (der Websmaster sollte die Kompatibilität prüfen).</li>
   </ul>', # MODIF
	'choix_non' => 'Nicht aktivieren: Meinen Skeletten wird nichts hinzugefügt.', # MODIF
	'choix_titre' => 'Kompatibilität mit dem Microsoft Internet Explorer' # MODIF
);

?>
