<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'choix_explication' => '<p>يتيح لك هذا الإعداد تحسين توافق الموقع العمومي مع متصفح إنترنت إكسبلورر.</p>
   <ul>
li><a href=\'http://jquery.khurshid.com/ifixpng.php\'>iFixPng</a> (<b>إفتراضياً</b>) يسترجع الشفافية الجزئية للصور ذات تنسيق PNG في الإصدارين الخامس والسادس من إنترنت إكسبلورر.</li>
<li><a href=\'http://code.google.com/p/ie7-js/\'>IE7.js</a> يصحح صور PNG ويضيف أنماط CSS للإصدارين الخامس والسادس من إنترنت إكسبلورر (<a href=\'http://ie7-js.googlecode.com/svn/test/index.html\'> يمكن الرجوع الى قائمة أنماط CSS المتوافقة التي أدخلها IE7.js وIE8.js</a>).</li>
 <li>يكمل IE8.js الإصدار السابع بإغناء تصرف CSS  للإصدارات خمسة الى سبعة من إنترنت إكسبلورر.</li>
 <li>يصحح IE7-squish ثلاثة أخطاء في الإصدار السادس من إنترنت إكسبلورر (خاصة الهامش المزدوج للعناصر العائمة) لكنه قد يتسبب ببعض المؤثرات غير المرغوب فيها (يجب على المصمم ان يتأكد من التوافقية).</li>
   </ul> ',
	'choix_non' => 'عدم التفعيل: عدم إضافة اي شيء على الصفحات النموذجية',
	'choix_titre' => 'التوافق مع مايكروسوفت انترنت اكسبلورر'
);

?>
