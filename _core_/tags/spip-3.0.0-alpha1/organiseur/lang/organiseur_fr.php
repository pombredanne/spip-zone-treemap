<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_core_/plugins/organiseur/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cal_jour_entier' => 'Journée',
	'cal_par_jour' => 'jour',
	'cal_par_mois' => 'mois',
	'cal_par_semaine' => 'semaine',

	// L
	'loading' => 'Chargement...'
);

?>
