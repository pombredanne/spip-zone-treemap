<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cal_jour_entier' => 'Deň',
	'cal_par_jour' => 'deň',
	'cal_par_mois' => 'mesiac',
	'cal_par_semaine' => 'týždeň',

	// L
	'loading' => 'Spúšťa sa...'
);

?>
