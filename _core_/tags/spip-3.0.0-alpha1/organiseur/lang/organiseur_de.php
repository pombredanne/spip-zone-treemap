<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cal_jour_entier' => 'ganztägig',
	'cal_par_jour' => 'Tag',
	'cal_par_mois' => 'Monat',
	'cal_par_semaine' => 'Woche',

	// L
	'loading' => 'lade ...'
);

?>
