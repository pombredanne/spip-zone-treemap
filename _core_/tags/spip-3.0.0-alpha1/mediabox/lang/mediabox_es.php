<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_reinitialiser' => 'Reinicializar',
	'boxstr_close' => 'Cerrar',
	'boxstr_current' => '{current}/{total}',
	'boxstr_next' => 'Siguiente',
	'boxstr_previous' => 'Anterior',
	'boxstr_slideshowStart' => 'Presentaci&oacute;n',
	'boxstr_slideshowStop' => 'Detener',
	'boxstr_zoom' => 'Zoom',

	// E
	'explication_selecteur' => 'Indica los selectores que activar&aacute;n la caja (Expresi&oacute;n CSS o selector jQuery)',
	'explication_selecteur_galerie' => 'Indica los selectores que activar&aacute;n la caja en forma de galer&iacute;a. (Expresi&oacute;n CSS o selector jQuery)',
	'explication_splash_url' => 'Indica la URL de un contenido multimedia que se mostrar&aacute; en una caja la primera vez que un visitante accede al sitio p&uacute;blico',
	'explication_traiter_toutes_images' => '&iquest;Insertar una caja en todas las im&aacute;genes?',

	// L
	'label_active' => 'Activer la mediabox dans le site public', # NEW
	'label_apparence' => 'Apariencia',
	'label_aucun_style' => 'No usar ning&uacute;n estilo por defecto',
	'label_choix_transition_elastic' => 'El&aacute;stico',
	'label_choix_transition_fade' => 'Degradado',
	'label_choix_transition_none' => 'Ninguno',
	'label_maxheight' => 'Alto m&aacute;ximo (% o px)',
	'label_maxwidth' => 'Ancho m&aacute;ximo (% o px)',
	'label_minheight' => 'Alto minimo (% o px)',
	'label_minwidth' => 'Ancho minimo (% o px)',
	'label_opacite' => 'Opacit&eacute; du fond', # NEW
	'label_selecteur_commun' => 'Selector general',
	'label_selecteur_galerie' => 'Selector de galer&iacute;a',
	'label_skin' => 'Estilo visual',
	'label_slideshow_speed' => 'Duraci&oacute;n de las diapositivas (ms)',
	'label_speed' => 'Velocidad de transici&oacute;n (ms)',
	'label_splash' => 'Caja Splash',
	'label_splash_url' => 'URL del contenido',
	'label_traiter_toutes_images' => 'Im&aacute;genes',
	'label_transition' => 'Efecto de transici&oacute;n',

	// T
	'titre_menu_box' => 'Caja Multimedia',
	'titre_page_configurer_box' => 'Configurac&iacute;on de la caja multimedia'
);

?>
