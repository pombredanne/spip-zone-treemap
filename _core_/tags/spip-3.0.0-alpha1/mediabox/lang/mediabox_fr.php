<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_core_/plugins/mediabox/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_reinitialiser' => 'R&eacute;initialiser',
	'boxstr_close' => 'Fermer',
	'boxstr_current' => '{current}/{total}',
	'boxstr_next' => 'Suivant',
	'boxstr_previous' => 'Pr&eacute;c&eacute;dent',
	'boxstr_slideshowStart' => 'Diaporama',
	'boxstr_slideshowStop' => 'Arr&ecirc;ter',
	'boxstr_zoom' => 'Zoom',

	// E
	'explication_selecteur' => 'Indiquez la cible des &eacute;l&eacute;ments qui d&eacute;clencheront la bo&icirc;te. (Expression CSS ou &eacute;tendue jQuery)',
	'explication_selecteur_galerie' => 'Indiquez la cible des &eacute;l&eacute;ments &agrave; regrouper en galerie. (Expression CSS ou &eacute;tendue jQuery)',
	'explication_splash_url' => 'Indiquez l\'url du m&eacute;dia &agrave; afficher automatiquement dans une bo&icirc;te lors de la premi&egrave;re visite sur le site public.',
	'explication_traiter_toutes_images' => 'Ins&eacute;rer une bo&icirc;te sur toutes les images ?',

	// L
	'label_active' => 'Activer la mediabox dans le site public',
	'label_apparence' => 'Apparence',
	'label_aucun_style' => 'N\'ins&eacute;rer aucun habillage par d&eacute;faut',
	'label_choix_transition_elastic' => 'Elastique',
	'label_choix_transition_fade' => 'Fondu enchain&eacute;',
	'label_choix_transition_none' => 'Sans effet de transition',
	'label_maxheight' => 'Hauteur Maxi (% ou px)',
	'label_maxwidth' => 'Largeur Maxi (% ou px)',
	'label_minheight' => 'Hauteur Mini (% ou px)',
	'label_minwidth' => 'Largeur Mini (% ou px)',
	'label_opacite' => 'Opacit&eacute; du fond',
	'label_selecteur_commun' => 'En g&eacute;n&eacute;ral',
	'label_selecteur_galerie' => 'En galerie',
	'label_skin' => 'Habillage visuel',
	'label_slideshow_speed' => 'Temps d\'exposition des photos du diaporama (ms)',
	'label_speed' => 'Vitesse de transition (ms)',
	'label_splash' => 'Splash Bo&icirc;te',
	'label_splash_url' => 'URL &agrave; afficher',
	'label_traiter_toutes_images' => 'Images',
	'label_transition' => 'Transition entre deux affichages',

	// T
	'titre_menu_box' => 'Bo&icirc;te Multim&eacute;dia',
	'titre_page_configurer_box' => 'Configuration de la Bo&icirc;te multim&eacute;dia'
);

?>
