<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Desaniciar les estadístiques', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Evolución de les visites<br />@visites@ visites',
	'icone_repartition_actuelle' => 'Amosar el repartu actual',
	'icone_repartition_visites' => 'Repartu de les visites',
	'icone_statistiques_visites' => 'Estadístiques',
	'info_affichier_visites_articles_plus_visites' => 'Amosar les visites pa <b>los artículos más visitaos dende l\'entamu:</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Cómo lleer esti cuadru',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'Estadístiques de les visites',
	'info_popularite_2' => 'popularidá del sitiu:',
	'info_popularite_3' => 'popularidá: @popularite@; visites: @visites@',
	'info_popularite_5' => 'popularidá:',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_gerer_statistiques' => '¿El sitiu tien que xestionar les estadístiques de les visites?',
	'info_question_vignettes_referer' => 'Cuando consultes les estadístiques, pues amosar miniatures de los sitios d\'orixe de les visites',
	'info_question_vignettes_referer_oui' => 'Amosar captures de los sitios d\'orixe de les visites',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'visites:',
	'info_visites_plus_populaires' => 'Amosar les visites de <b>los artículos más populares</b> y de <b>los caberos artículos espublizaos:</b>',
	'info_zoom' => 'zoom',
	'item_gerer_statistiques' => 'Xestionar les estadístiques',

	// O
	'onglet_origine_visites' => 'Orixe de les visites',
	'onglet_repartition_debut' => 'dende l\'entamu',
	'onglet_repartition_lang' => 'Repartío por llingües',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Esta orde esborra tolos datos tocantes a les estadístiques de visites al sitiu, incluyendo la popularidá de los artículos.',
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'El rangu de l\'artículu,
  na clasificación por popularidá, vién indicáu nel
  marxe; la popularidá de l\'artículu (una estimación del
  númberu de visites diaries que recibirá si el ritmu actual de
  consultes se mantuviere) y el númberu de visites recibíes
  dende\'l so espublizamientu amuésense na burbuya
  que apaez al pasar el mur pol títulu.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'Les barres escures representen les entráes acumuláes (total de les sub-estayes), les barres clares el númberu de visites pa cada estaya.',
	'titre_evolution_visite' => 'Evolución de les visites',
	'titre_liens_entrants' => 'Enllaces entrantes',
	'titre_page_statistiques' => 'Estadístiques por estayes',
	'titre_page_statistiques_visites' => 'Estadístiques de les visites',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
