<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Esborrar les estadístiques', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Evolució de les visites<br />@visites@ visites',
	'icone_repartition_actuelle' => 'Mostrar la distribució actual',
	'icone_repartition_visites' => 'Repartiment de les visites',
	'icone_statistiques_visites' => 'Estadístiques',
	'info_affichier_visites_articles_plus_visites' => 'Mostrar les visites pels <b>articles més visitats des del començament:</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Com llegir aquest quadre',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'Estadístiques de les visites',
	'info_popularite_2' => 'popularitat del lloc:',
	'info_popularite_3' => 'popularitat: @popularite@ ; visites: @visites@',
	'info_popularite_5' => 'popularitat:',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_gerer_statistiques' => 'El vostre lloc ha de gestionar les estadístiques de les visites?',
	'info_question_vignettes_referer' => 'Quan consulteu les estadístiques, podeu visualitzar una estimació dels llocs d\'origen de les visites',
	'info_question_vignettes_referer_oui' => 'Mostrar les captures dels llocs d\'origen de les visites',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'visites:',
	'info_visites_plus_populaires' => 'Mostrar les visites pels <b>articles més populars</b> i pels <b>últims articles publicats:</b>',
	'info_zoom' => 'zoom',
	'item_gerer_statistiques' => 'Gestionar les estadístiques',

	// O
	'onglet_origine_visites' => 'Origen de les visites',
	'onglet_repartition_debut' => 'des del començament',
	'onglet_repartition_lang' => 'Repartiment per llengües',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Aquesta ordre esborra totes les dades lligades a les estadístiques de visita del lloc, fins i tot la popularitat dels articles.',
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'El rang de l\'article,
 dins de la classificació per popularitat, està indicat al marge; la popularitat de l\'article (una estimació del nombre de visites quotidianes que rebrà l\'article si es manté el ritme actual de
  consultes) i el nombre de visites rebudes
 des que va estar per primer cop en línia estàn visibles al menú que apareix quan passem el ratolí per sobre del títol.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'Les columnes fosques representen les entrades acumulades (total de les subseccions), les columnes clares el número de visites per a cada secció.',
	'titre_evolution_visite' => 'Evolució de les visites',
	'titre_liens_entrants' => 'Enllaços entrants',
	'titre_page_statistiques' => 'Estadistiques per seccions',
	'titre_page_statistiques_visites' => 'Estatistiques de les visites',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
