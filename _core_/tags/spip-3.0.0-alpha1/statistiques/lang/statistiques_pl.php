<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Effacer les statistiques', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Rozwój wizyt<br />@visites@ wizyt',
	'icone_repartition_actuelle' => 'Pokaż aktualną dystrybucję',
	'icone_repartition_visites' => 'Rozłożenie wizyt',
	'icone_statistiques_visites' => 'Statystyki',
	'info_affichier_visites_articles_plus_visites' => 'Pokaż odwiedziny dla <b>najczęściej odwiedzanych artykułów od początku:</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Jak odczytywac tą tabelę',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'Odwiedź statystyki',
	'info_popularite_2' => 'popularność strony:',
	'info_popularite_3' => 'popularność: @popularite@; odwiedziny: @visites@',
	'info_popularite_5' => 'popularność:',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_gerer_statistiques' => 'Czy Twoja strona ma prowadzić statystykę odwiedzin?',
	'info_question_vignettes_referer' => 'Lorsque vous consultez les statistiques, vous pouvez visualiser des aperçus des sites d\'origine des visites', # NEW
	'info_question_vignettes_referer_oui' => 'Afficher les captures des sites d\'origine des visites', # NEW
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'odwiedziny:',
	'info_visites_plus_populaires' => 'Pokaż odwiedziny dla <b>najpopularniejszych artykułów</b> i dla <b>artykułów ostatnio opublikowanych:</b>',
	'info_zoom' => 'zoom',
	'item_gerer_statistiques' => 'Zarządzaj statystykami',

	// O
	'onglet_origine_visites' => 'Wizyty z URL-i',
	'onglet_repartition_debut' => 'od początku',
	'onglet_repartition_lang' => 'Porządkowanie wg języków',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Cette commande efface toutes les données liées aux statistiques de visite du site, y compris la popularité des articles.', # NEW
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'Szereg artykułów występujących,
  w klasyfikacji popularności jest na marginesie marge ; popularność artykułu(szacunek
  dziennej liczby wizyt zostanie obiczony jeśli częstotliwość wizyt zostanie utrzymana)
 a liczba wizyt,
od początku opublikowania artykułu pojawi się kiedy najedziesz myszką na tytuł artykułu.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'Ciemne paski oznaczają podsumowanie wszystkich odwiedzin w poddziałach, paski jasne liczbę wizyt dla poszczególnych działów.',
	'titre_evolution_visite' => 'Ewolucja odwiedzin',
	'titre_liens_entrants' => 'Linki wejściowe',
	'titre_page_statistiques' => 'Statystyki działu',
	'titre_page_statistiques_visites' => 'Statystyka odwiedzin',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
