<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Effacer toutes les statistiques', # NEW
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'V15175 3v01u710n<br />@visites@ v15175',
	'icone_repartition_actuelle' => '5h0w 4c7u41 d157r1bu710n',
	'icone_repartition_visites' => 'V15175 d157r1bu710n',
	'icone_statistiques_visites' => '57471571c5',
	'info_affichier_visites_articles_plus_visites' => '5h0w v15175 f0r <b>7h3 m057 v15173d 4r71c135 51nc3 7h3 b3g1nn1ng:</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'H0w 70 r34d 7h15 gr4ph1c',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'V15175 57471571c5',
	'info_popularite_2' => '5173 p0pu14r17y:',
	'info_popularite_3' => 'p0pu14r17y:&nb5p;@popularite@; v15175:&nb5p;@visites@',
	'info_popularite_5' => 'p0pu14r17y:',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_gerer_statistiques' => '5h0u1d y0ur 5173 m4n4g3 v15175 57471571c5?',
	'info_question_vignettes_referer' => 'Lorsque vous consultez les statistiques, vous pouvez visualiser des aperçus des sites d\'origine des visites', # NEW
	'info_question_vignettes_referer_oui' => 'Afficher les captures des sites d\'origine des visites', # NEW
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'v15175:',
	'info_visites_plus_populaires' => '5h0w v15175 f0r <b>7h3 m057 p0pu14r 4r71c135</b> 4nd f0r <b>7h3 1457 pub115h3d 4r71c135:</b>',
	'info_zoom' => 'zoom', # NEW
	'item_gerer_statistiques' => 'M4n4g3 57471571c5',

	// O
	'onglet_origine_visites' => 'V15175 0r1g1n',
	'onglet_repartition_debut' => 'fr0m 7h3 574r7',
	'onglet_repartition_lang' => 'by 14ngu4g35', # MODIF

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Cette commande efface toutes les données liées aux statistiques de visite du site, y compris la popularité des articles.', # NEW
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => '7h3 r4nk 0f 7h3 4r71c13,
		1n 7h3 p0pu14r17y c14551f1c4710n, 15 1nd1c473d 1n 7h3
		m4rg1n; 7h3 4r71c13 p0pu14r17y (4n 3571m473 0f
		7h3 numb3r 0f d411y v15175 17 w111 h4v3 1f 7h3 4c7u41 p4c3 0f
		7r4ff1c 15 m41n741n3d) 4nd 7h3 numb3r 0f v15175 r3c0rd3d
		51nc3 7h3 b3g1nn1ng 4r3 d15p14y3d 1n 7h3 b41100n 7h47
		4pp34r5 45 7h3 m0u53 h0v3r5 0v3r 7h3 71713.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'R3d b4r5 r3pr353n7 cumu1471v3 3n7r135 (70741 0f 5ub-53c710n5), gr33n b4r5 r3pr353n7 7h3 numb3r 0f v15175 f0r 34ch 53c710n.', # MODIF
	'titre_evolution_visite' => 'V15175 3v01u710n',
	'titre_liens_entrants' => '1nc0m1ng 11nk5 0f 7h3 d4y', # MODIF
	'titre_page_statistiques' => '57471571c5 by 53c710n',
	'titre_page_statistiques_visites' => 'V15175 57471571c5',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
