<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Statistiken löschen', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'Daten komprimiert als Datei @fichier@ gesichert',
	'bouton_radio_sauvegarde_non_compressee' => 'Daten unkomprimiert als Datei@fichier@ gesichert',

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Besuchsentwicklung<br />@visites@ Abrufe',
	'icone_repartition_actuelle' => 'Aktuelle Verteilung anzeigen',
	'icone_repartition_visites' => 'Verteilung der Besuche',
	'icone_statistiques_visites' => 'Statistiken',
	'info_affichier_visites_articles_plus_visites' => 'Besuche der <b>beliebtesten Artikel seit Start der Website</b> anzeigen:',
	'info_base_restauration' => 'Datenbank wird wiederhergestellt.',
	'info_comment_lire_tableau' => 'Interpretation der Tabelle',
	'info_erreur_restauration' => 'Fehler bei der Wiederherstellung: Datei nicht vorhanden.',
	'info_forum_statistiques' => 'Besucherstatistiken',
	'info_popularite_2' => 'Beliebtheit der Website: ',
	'info_popularite_3' => 'Beliebtheit: @popularite@ ; Besuche: @visites@',
	'info_popularite_5' => 'Beliebtheit:',
	'info_previsions' => 'Vorschau:',
	'info_question_gerer_statistiques' => 'Soll SPIP Besucherstatistiken anlegen?',
	'info_question_vignettes_referer' => 'Sie können die Besucherstatistiken mit Thumbnails der Herkunftswebsites (referer) ergänzen',
	'info_question_vignettes_referer_oui' => 'Thumbnails der Herkunftswebsites anzeigen',
	'info_restauration_sauvegarde_insert' => '@archive@ wird in die Datenbank eingefügt',
	'info_sauvegarde_articles' => 'Artikel sichern',
	'info_sauvegarde_articles_sites_ref' => 'Artikel der verlinkten Websites sichern',
	'info_sauvegarde_auteurs' => 'Autoren sichern',
	'info_sauvegarde_breves' => 'Meldungen sichern',
	'info_sauvegarde_documents' => 'Dokumente sichern',
	'info_sauvegarde_echouee' => 'Wenn die Sicherung fehlgeschlagen ist («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Foren sichern',
	'info_sauvegarde_groupe_mots' => 'Schlagwortgruppen sichern',
	'info_sauvegarde_messages' => 'Nachrichten sichern',
	'info_sauvegarde_mots_cles' => 'Schlagworte sichern',
	'info_sauvegarde_petitions' => 'Petitionen sichern',
	'info_sauvegarde_refers' => 'Referer sichern',
	'info_sauvegarde_reussi_01' => 'Sicherung erfolgreich',
	'info_sauvegarde_rubrique_reussi' => 'Die Tabellen der Rubrik @titre@ wurden in der Datei @archive@ gesichert. Siekönnen',
	'info_sauvegarde_rubriques' => 'Rubriken sichern',
	'info_sauvegarde_signatures' => 'Signaturen der Petitionen sichern',
	'info_sauvegarde_sites_references' => 'Verlinkte Websites sichern',
	'info_sauvegarde_type_documents' => 'Dokumenttypen sichern',
	'info_sauvegarde_visites' => 'Besucherzahlen sichern',
	'info_visites' => 'Besuche:',
	'info_visites_plus_populaires' => 'Seitenabrufe für die <b>beliebtesten Artikel</b> und die <b>letzten veröffentlichten Artikel:</b>',
	'info_zoom' => 'Zoom',
	'item_gerer_statistiques' => 'Statistiken verwalten',

	// O
	'onglet_origine_visites' => 'Ursprung der Besuche',
	'onglet_repartition_debut' => 'von Anfang an',
	'onglet_repartition_lang' => 'nach Sprachen',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Aktuelle Datenbank und Sicherung zusammenführen',
	'sauvegarde_fusionner_depublier' => 'Zusammengeführte Objekte offline stellen',
	'sauvegarde_url_origine' => 'Ggf. URL der Ursprungssite:',

	// T
	'texte_admin_effacer_stats' => 'Dieser Befehl löscht alle Daten der Besucherstatistiken, auch die zur Popularität der Artikel.',
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Sie können die Sicherung als komprimierte Datei anlegen, um den Download zu beschleunigen und Speicherplatz zu sparen.',
	'texte_admin_tech_04' => 'Aös Vorbereitung für das Zusammenführen mit einer anderen Datenbank, können sie den Bereich der Datensicherung bestimmen:',
	'texte_comment_lire_tableau' => 'Die Position eines Artikels wird durch einen Balken angezeigt. Seine Popularität (eine Schätzung der täglichen Besucher im Fall, dass die Zugriffe konstant bleiben) und die Anzahl der Besuche von Anfang an werden angezeigt, wenn der Mauszeiger über den Titel bewegt wird.',
	'texte_sauvegarde_compressee' => 'Die Sicherung wird als unkomprimierte Datei @fichier@ angelegt.',
	'texte_signification' => 'Die roten Balken stellen die Summe der Einträge dar (Summe der Unterrubriken), die hellen Balken symbolisieren die Anzahl der Seitenabrufe pro Rubrik.',
	'titre_evolution_visite' => 'Entwicklung der Seitenabrufe',
	'titre_liens_entrants' => 'Referer von heute',
	'titre_page_statistiques' => 'Statistiken pro Rubrik',
	'titre_page_statistiques_visites' => 'Statistik der Seitenabrufe',

	// V
	'visites_journalieres' => 'Anzahl Besuche pro Tag',
	'visites_mensuelles' => 'Anzahl Besucher pro Tag'
);

?>
