<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Vymazať štatistiku', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Počet návštev<br />@visites@ návštev',
	'icone_repartition_actuelle' => 'Zobraziť aktuálnu distribúciu',
	'icone_repartition_visites' => 'Distribúcia návštev',
	'icone_statistiques_visites' => 'Štatistika',
	'info_affichier_visites_articles_plus_visites' => 'Zobraziť návštevy <b>najnavštevovanejších článkov od začiatku:</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Ako čítať tento graf',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'Štatistika návštev',
	'info_popularite_2' => 'popularita stránky:',
	'info_popularite_3' => 'popularita: @popularite@, návštev: @visites@',
	'info_popularite_5' => 'popularita:',
	'info_previsions' => 'predpovede:',
	'info_question_gerer_statistiques' => 'Chcete mať na vašej stránke štatistiku?',
	'info_question_vignettes_referer' => 'Keď sa pozriete do štatistiky, uvidíte ukážku každej stránky, z ktorej prišiel nejaký návštevník. ',
	'info_question_vignettes_referer_oui' => 'Zobraziť obrázky odkazujúcich stránok',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'Návštev:',
	'info_visites_plus_populaires' => 'Zobraziť návštevy <b>najčítanejších</b> a <b>najnovších publikovaných článkov:</b>',
	'info_zoom' => 'lupa',
	'item_gerer_statistiques' => 'Riadiť štatistiky',

	// O
	'onglet_origine_visites' => 'Pôvod návštev',
	'onglet_repartition_debut' => 'v každom čase',
	'onglet_repartition_lang' => 'Distribúcia podľa jazyka',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Tento príkaz vymaže všetky štatistiky o návštevách na stránke, vrátane popularity článkov.',
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'Články sa hodnotia podľa popularity.
 Ak prejdete myšou po názve nejakého článku,
zobrazí sa jeho popularita (tzn. približný počet
 návštev za deň, ktorý bude mať, ak bude pokračovať súčasný trend)
  a počet návštev zaznamenaných odvtedy, čo bol prvýkrát publikovaný.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'Tmavé čiary znázorňujú kumulatívne vstupy (celkom za podrubriky), svetlé čiary znázorňujú počet návštev každej rubriky.',
	'titre_evolution_visite' => 'Úroveň návštev',
	'titre_liens_entrants' => 'Prichádzajúce odkazy',
	'titre_page_statistiques' => 'Štatistiky podľa rubrík',
	'titre_page_statistiques_visites' => 'Štatistiky návštev',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
