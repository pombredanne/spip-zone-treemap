<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Effacer toutes les statistiques', # NEW
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Thống kê<br />@visites@ độc giả',
	'icone_repartition_actuelle' => 'Cho xem phân loại hiện nay',
	'icone_repartition_visites' => 'Phân bố số thăm viếng',
	'icone_statistiques_visites' => 'Thống kê',
	'info_affichier_visites_articles_plus_visites' => 'Cho thấy số lần vào xem của <b>những bài được đọc nhiều nhất từ ban đầu:</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Cách đọc đồ thị này',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'Thống kê thăm viếng',
	'info_popularite_2' => 'Mức phổ thông của website: ',
	'info_popularite_3' => 'Mức phổ thông: @popularite@; lần viếng: @visites@',
	'info_popularite_5' => ' phổ thông:',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_gerer_statistiques' => 'Có muốn quản trị các con số thống kê?',
	'info_question_vignettes_referer' => 'Lorsque vous consultez les statistiques, vous pouvez visualiser des aperçus des sites d\'origine des visites', # NEW
	'info_question_vignettes_referer_oui' => 'Afficher les captures des sites d\'origine des visites', # NEW
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => ' thăm viếng:',
	'info_visites_plus_populaires' => 'Cho thấy số lần vào xem của <b>những bài được chuộng nhất</b> và của <b>những bài đăng tải sau cùng:</b>',
	'info_zoom' => 'phóng lớn',
	'item_gerer_statistiques' => 'Quản trị số thống kê',

	// O
	'onglet_origine_visites' => 'Xuất xứ của thăm viếng',
	'onglet_repartition_debut' => 'từ ban đầu',
	'onglet_repartition_lang' => 'Phổ biến theo ngôn ngữ',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Cette commande efface toutes les données liées aux statistiques de visite du site, y compris la popularité des articles.', # NEW
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'Bài vở được sắp hạng theo mức độ phổ thông (xem cột trái). Để xem mức độ phổ thông (ước lượng số lần vào xem hàng ngày theo đà hiện tại) và số lần vào xem của mỗi bài, bạn di chuyển con chuột lên trên tên bài.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'Thanh đậm biểu hiện tất cả số bài (tổng cộng của các đề mục phụ), thanh lợt biểu hiện số lần vào xem của mỗi đề mục. ',
	'titre_evolution_visite' => 'Thống Kê',
	'titre_liens_entrants' => 'Các điểm nối vào',
	'titre_page_statistiques' => 'Thống kê theo từng đề mục',
	'titre_page_statistiques_visites' => 'Thống kê về thăm viếng',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
