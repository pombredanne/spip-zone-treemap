<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Effacer toutes les statistiques', # NEW
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Evolucion de las vesitas<br />@visites@ vesitas',
	'icone_repartition_actuelle' => 'Afichar la reparticion actuau',
	'icone_repartition_visites' => 'Reparticion de las vesitas',
	'icone_statistiques_visites' => 'Estadisticas',
	'info_affichier_visites_articles_plus_visites' => 'Afichar las vesitas entaus <b>articles mei vesitats dempuish lo començament:</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Quin legir aqueth tablèu',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'Estadisticas de las vesitas',
	'info_popularite_2' => 'popularitat deu sit:',
	'info_popularite_3' => 'popularitat: @popularite@; vesitas: @visites@',
	'info_popularite_5' => 'popularitat:',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_gerer_statistiques' => 'E deu gerir, lo vòste sit, las estadisticas de las vesitas?',
	'info_question_vignettes_referer' => 'Lorsque vous consultez les statistiques, vous pouvez visualiser des aperçus des sites d\'origine des visites', # NEW
	'info_question_vignettes_referer_oui' => 'Afficher les captures des sites d\'origine des visites', # NEW
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'vesitas:',
	'info_visites_plus_populaires' => 'Afichar las vesitas <b>taus articles mei populars</b> e <b>taus darrèrs articles publicats:</b>',
	'info_zoom' => 'zoom',
	'item_gerer_statistiques' => 'Gerir las estadisticas',

	// O
	'onglet_origine_visites' => 'Origina de las vesitas',
	'onglet_repartition_debut' => 'dempuish la començança',
	'onglet_repartition_lang' => 'Reparticion per lengas',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Cette commande efface toutes les données liées aux statistiques de visite du site, y compris la popularité des articles.', # NEW
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'Lo reng de l\'article,
dens lo classament per popularitat, qu\'ei indicat dens lo marge; la popularitat de l\'article 
(ua estimacion deu nombre de vesitas quotidianas qu\'eth receberà se lo ritme actuau de consultacion se mantien) e lo nombre de vesitas recebudas
dempuish lo començament que s\'afichan dens la bohòrla 
qu\'apareish quan la murga e passa suu títol.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'Las barras escuras que representan las entradas cumuladas (totau de las sosrubricas), las barras claras lo nombre de vesitas tà cada rubrica.',
	'titre_evolution_visite' => 'Evolucion de las vesitas',
	'titre_liens_entrants' => 'Los ligams entrants',
	'titre_page_statistiques' => 'Estadisticas per rubricas',
	'titre_page_statistiques_visites' => 'Estadisticas de las vesitas',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
