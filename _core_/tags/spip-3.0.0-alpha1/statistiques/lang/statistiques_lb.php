<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Statistik läschen', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Evolutioun vun den Visiten<br />@visites@ Visiten',
	'icone_repartition_actuelle' => 'Déi aktuell Verdeelung weisen',
	'icone_repartition_visites' => 'Verdeelung vun de Visiten',
	'icone_statistiques_visites' => 'Statistik',
	'info_affichier_visites_articles_plus_visites' => 'D\'Visiten fir <b>déi am meeschten gefroten Artikel zënter dem Ufank</b> weisen:',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Wéi een dës Tabell liesen soll',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'Statistik vun de Visiten',
	'info_popularite_2' => 'Popularitéit vum Site:',
	'info_popularite_3' => 'Popularitéit: @popularite@; Visiten: @visites@',
	'info_popularite_5' => 'Popularitéit:',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_gerer_statistiques' => 'Soll äre Site d\'Statistik vun de Visiten géréieren?',
	'info_question_vignettes_referer' => 'Wann dir d\'Statistik kuckt kënnt dir eng Preview vun den Ausgangs-Site gesinn',
	'info_question_vignettes_referer_oui' => 'Preview vun den Ausgangs-Site weisen',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'Visiten:',
	'info_visites_plus_populaires' => 'Visiten weisen fir <b>déi populärsten</b> a fir <b>déi läscht publizéiert</b> Artikelen:',
	'info_zoom' => 'Zoom',
	'item_gerer_statistiques' => 'Statistik maachen',

	// O
	'onglet_origine_visites' => 'Origine vun de Visiten',
	'onglet_repartition_debut' => 'zënter Ufank',
	'onglet_repartition_lang' => 'Verdeelung no Sprooch',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Dës Prozedur läscht d\'Statistik an d\'Popularitéit vun den Artikele vun ärem Site.',
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'D\'Plaz vum Artikel am Klassement no Popularitéit steet am Rand; d\'Popularitéit vum Artikel (eng Estimatioun vun der Zuel vun de Visiten pro Dag wann ët esou weidergeet) an Zuel vun de Visiten zënter Ufank stinn an der Erklärung déi erschéngt wann dier mat der Maus iwwer den Titel fuert.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'Déi kumuléiert Donnéen sinn donkel (Total vun den Ënner-Rubriken), d\'Zuel vun de Visiten fir all Rubrik sinn hell.',
	'titre_evolution_visite' => 'Évolutioun vun de Visiten',
	'titre_liens_entrants' => 'Linken déi eran komm sinn',
	'titre_page_statistiques' => 'Statistik pro Rubrik',
	'titre_page_statistiques_visites' => 'Statistik vun de Visiten',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
