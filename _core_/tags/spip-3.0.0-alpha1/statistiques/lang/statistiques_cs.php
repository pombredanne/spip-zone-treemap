<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Delete the statistics', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Vývoj návštěvnosti<br />@visites@ visites', # MODIF
	'icone_repartition_actuelle' => 'Zobrazit aktuální rozdělení',
	'icone_repartition_visites' => 'Rozdělení návštěv',
	'icone_statistiques_visites' => 'Statistiky',
	'info_affichier_visites_articles_plus_visites' => 'Zobrazit návštěvnost <b>nejoblíbenějších článků od počátku:</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Vysvětlivky k tabulce',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'Statistiky návštěvnosti',
	'info_popularite_2' => 'oblíbenost webu:',
	'info_popularite_3' => 'oblívenost: @popularite@; návštěvy: @visites@',
	'info_popularite_5' => 'oblíbenost:',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_gerer_statistiques' => 'Mají se na vašem webu vést statistiky návštěvnosti?',
	'info_question_vignettes_referer' => 'When you consult the statistics, you can see a preview of the originating sites of the visits. ', # NEW
	'info_question_vignettes_referer_oui' => 'Show the screenshots of the originating sites', # NEW
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'návštěvy:',
	'info_visites_plus_populaires' => 'Zobrazit návštevnost <b>nejčtenějších</b> a <b>nejnovějších článků:</b>',
	'info_zoom' => 'zvětšit/zmenšit',
	'item_gerer_statistiques' => 'Správa statistik',

	// O
	'onglet_origine_visites' => 'Původ návštěv',
	'onglet_repartition_debut' => 'od počátku',
	'onglet_repartition_lang' => 'Rozdělení podle jazyků',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'This command deletes all data linked to the statistics of visits to the site, including the relative popularity of articles.', # NEW
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'Pořadí článku v žebříčku,
  oblíbenosti je uvedeno na okraji.
  Oblíbenost článku (odhad každodenního
  počtu návštěvníků za předpokladu, že počet návštěv bude pokračovat dosavadním tempem)
  a počet celkových návštěv za celou dobu zveřejnění
  jsou uvedeny v místní nabídce, která se zobrazí
  při umístění myši nad názvem článku.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'Tmavě jsou označeny kumulované údaje (celkem v rámci podsekcí), světle je uveden počet návštěv v jednotlivých sekcích.',
	'titre_evolution_visite' => 'Vývoj návštěvnosti',
	'titre_liens_entrants' => 'Příchozí odkazy',
	'titre_page_statistiques' => 'Statistiky podle sekcí',
	'titre_page_statistiques_visites' => 'Statistiky návštěvnosti',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
