<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'İstatistikleri sil', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Ziyaretlerin gelişimi<br /> @visites@ ziyaret',
	'icone_repartition_actuelle' => 'Mevcut dağılımı göster',
	'icone_repartition_visites' => 'Ziyaretlerin dağılımı',
	'icone_statistiques_visites' => 'İstatistikler',
	'info_affichier_visites_articles_plus_visites' => '<b>Başlangıçtan beri en çok ziyaret edilen makaleleri<b> göster :',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Bu tabloyu nasıl okumalı ?',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'Ziyaret istatistikleri',
	'info_popularite_2' => 'Sitenin popülerliği',
	'info_popularite_3' => 'popülerlik : @popularite@ ; ziyaretler : @visites@',
	'info_popularite_5' => 'Popülerlik  ',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_gerer_statistiques' => 'Siteniz ziyaret istatistiğini yönetmeli mi ?',
	'info_question_vignettes_referer' => 'İstatistikleri incelediğinizde, ziyaretlerin yapıldığı kaynak sitelerden görüntüler görebilirsiniz',
	'info_question_vignettes_referer_oui' => 'Ziyaretlerin yapıldığı kaynak sitelerden görüntüler göster',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'Ziyaret :',
	'info_visites_plus_populaires' => '<b>En popüler makaleler</b> ile <b>yayınlanan son makalelere</b> yapılan ziyaretleri görüntüle.',
	'info_zoom' => 'yakınlaştır',
	'item_gerer_statistiques' => 'İstatistikleri yönet',

	// O
	'onglet_origine_visites' => 'Ziyaretlerin kaynağı',
	'onglet_repartition_debut' => 'Başlangıçtan beri',
	'onglet_repartition_lang' => 'Dillere göre dağılım',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Bu komut, site ziyareti istatistiklerine bağlı tüm bilgileri siler (makalelerin popülerlikleri de dahil).',
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'Popülerlik sınıflandırmasında makalenin sırası kenarda belirtilmiştir ;
   makalenin popülerliği (şimdiki başvuru sıklığı devam ettiği takdirde,
   geleceği tahmin edilen günlük ziyaretçi sayısı) ve başından beri gelen
   ziyaretçi sayısı, fare başlık üzerinden geçtiğinde
   ortaya çıkan konuşma balonunun içinde görüntülenir. ',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'Koyu çizgiler, toplam girişleri (alt-bölümlerin toplamını), açık çizgiler ise, her bir bölüme gelen ziyaret sayısını temsil eder.',
	'titre_evolution_visite' => 'Ziyaretlerin gelişimi ',
	'titre_liens_entrants' => 'Giren bağlantılar',
	'titre_page_statistiques' => 'Bölümlere göre istatistikler ',
	'titre_page_statistiques_visites' => 'Ziyaret istatistikleri',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
