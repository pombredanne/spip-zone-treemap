<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Effacer les statistiques', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Evolisyon vizit yo<br />@visites@ vizit yo',
	'icone_repartition_actuelle' => 'Afiche repatisyon kounye-a',
	'icone_repartition_visites' => 'Repatisyon vizit yo',
	'icone_statistiques_visites' => 'Estatistik',
	'info_affichier_visites_articles_plus_visites' => 'Fè parè vizit yo <b>tout atik plis vizite depi koumansman  :</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Kouman fè lekti tablo-a',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'Estatistik tout vizit',
	'info_popularite_2' => 'popilarite sit-la :',
	'info_popularite_3' => 'kouman se popilè : @popularite@ ; vizit yo : @visites@',
	'info_popularite_5' => 'Kouman se popilè :',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_gerer_statistiques' => 'Eske w vle sit w kapab okipe estatistik pou vizit yo ?',
	'info_question_vignettes_referer' => 'Lorsque vous consultez les statistiques, vous pouvez visualiser des aperçus des sites d\'origine des visites', # NEW
	'info_question_vignettes_referer_oui' => 'Afficher les captures des sites d\'origine des visites', # NEW
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'vizit yo :',
	'info_visites_plus_populaires' => 'Afiche tout visit pou <b> atik ki pli popilè</b> ak pou <b>denyé atik an liy :</b>',
	'info_zoom' => 'gwoplan',
	'item_gerer_statistiques' => 'Okipe lestatistik yo',

	// O
	'onglet_origine_visites' => 'Lorijin vizit yo',
	'onglet_repartition_debut' => 'depi koumansman',
	'onglet_repartition_lang' => 'Repatisyon ant lang',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Cette commande efface toutes les données liées aux statistiques de visite du site, y compris la popularité des articles.', # NEW
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'Plas latik-la,
  nan klasman popilawite, se enskri sou kote ; populawite atik-la
 (yon kont nonb
  vizit pa jou atik-la ki rannkout si li kembe ritm-la kounyen-a)
 ak nonb vizit ki vini 
  depi koumansman, zot ka afich
  nan yon bil ki parèt kank w vole sou titr-la.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'Tou liy nwa se limaj tout rantré kimilé  (yon total pou tout souribrik), tout liy klèr se nonb visit sa-yo pou chak ribrik.',
	'titre_evolution_visite' => 'Evolisyon vizit yo',
	'titre_liens_entrants' => 'Lyen sa-yo ka rive',
	'titre_page_statistiques' => 'Estatsitik pou tout pati yo nan sit sa a',
	'titre_page_statistiques_visites' => 'Estatistik pou tout vizit yo',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
