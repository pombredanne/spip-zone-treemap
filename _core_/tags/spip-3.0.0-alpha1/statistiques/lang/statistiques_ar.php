<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'حذف الاحصاءات', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'نسخة احتياطية مضغوطة باسم @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'نيخة احتياطية غير مضغوطة باسم @fichier@',

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'تطور الزيارات <br />@visites@ زيارة ',
	'icone_repartition_actuelle' => 'إظهار التوزيع الحالي',
	'icone_repartition_visites' => 'توزيع الزيارات',
	'icone_statistiques_visites' => 'الإحصاءات',
	'info_affichier_visites_articles_plus_visites' => 'إظهار الزيارات المتعلقة <b> بأكثر المقالات زيارة منذ البداية:</ b>',
	'info_base_restauration' => 'القاعدة قيد الاسترجاع.',
	'info_comment_lire_tableau' => 'كيفية قراءة هذا الرسم البياني',
	'info_erreur_restauration' => 'خطأ خلال الاسترجاع: ملف غير موجود.',
	'info_forum_statistiques' => 'إحصاءات الزيارات',
	'info_popularite_2' => 'شعبية الموقع:',
	'info_popularite_3' => 'الشعبية: @popularite@؛ الزيارات: @visites@',
	'info_popularite_5' => 'الشعبية:',
	'info_previsions' => 'توقعات:',
	'info_question_gerer_statistiques' => 'هل تريد أن يدير الموقع إحصاءات الزيارات؟',
	'info_question_vignettes_referer' => 'عندما تعاين الاحضاءات، يمكنك ايضاً مشاهدة مصغرات عن المواقع الصادرة منها.',
	'info_question_vignettes_referer_oui' => 'عرض مصغرات عن مواقع مصدر الزيارات',
	'info_restauration_sauvegarde_insert' => 'إدراج @archive@ في القاعدة',
	'info_sauvegarde_articles' => 'حفظ المقالات',
	'info_sauvegarde_articles_sites_ref' => 'حفظ مقالات المواقع المبوبة',
	'info_sauvegarde_auteurs' => 'حفظ المؤلفين',
	'info_sauvegarde_breves' => 'حفظ الأخبار',
	'info_sauvegarde_documents' => 'حفظ المستندات',
	'info_sauvegarde_echouee' => 'اذا فشل النسخ الاحتياطي (مع رسالة «Maximum execution time exceeded»)،',
	'info_sauvegarde_forums' => 'حفظ المنتديات',
	'info_sauvegarde_groupe_mots' => 'حفظ مجموعات المفاتيح',
	'info_sauvegarde_messages' => 'حفظ الرسائل',
	'info_sauvegarde_mots_cles' => 'حفظ المفاتيح',
	'info_sauvegarde_petitions' => 'حفظ العرائض',
	'info_sauvegarde_refers' => 'حفظ مصادر الزيارات',
	'info_sauvegarde_reussi_01' => 'نجح النسخ الاحتياطي.',
	'info_sauvegarde_rubrique_reussi' => 'تم حفظ جداول القسم @titre@ في @archive@. يمكنك',
	'info_sauvegarde_rubriques' => 'حفظ الأقسام',
	'info_sauvegarde_signatures' => 'حفظ تواقيع العرائض',
	'info_sauvegarde_sites_references' => 'حفظ المواقع المبوبة',
	'info_sauvegarde_type_documents' => 'حفظ أنواع المستندات',
	'info_sauvegarde_visites' => 'حفظ الزيارات',
	'info_visites' => 'زيارة:',
	'info_visites_plus_populaires' => 'إظهار الزيارات <b>لأكثر المقالات شعبية</b> و<b>لأحدث المقالات المنشورة:</ b>',
	'info_zoom' => 'تكبير-تصغير العرض',
	'item_gerer_statistiques' => 'إدارة الإحصاءات',

	// O
	'onglet_origine_visites' => 'مصدر الزيارات',
	'onglet_repartition_debut' => 'منذ البداية',
	'onglet_repartition_lang' => 'التوزيع حسب اللغات',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'دمج قاعدة البيانات الحالية بالنسخة الاحتياطية',
	'sauvegarde_fusionner_depublier' => 'مضاعفة العناصر المدموجة',
	'sauvegarde_url_origine' => 'عنوان URL للموقع المصدر اذا وجد:',

	// T
	'texte_admin_effacer_stats' => 'هذا الأمر يحذف كل البيانات المرتبطة بإحصاءات  زيارات الموقع، بما فيها شعبية المقالات.',
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'بمكن اختيار ملف مضغوط للنسخة الاحتياطية، من أجل
تسريع نقله الى جهازك او الى جهاز خدمة مخصص والتوفير في المساحة على القرص الثابت.',
	'texte_admin_tech_04' => 'بهدف الدمج بقاعدة أخرى، يمكن اقتصار النسخ الاحتياطي على القسم:',
	'texte_comment_lire_tableau' => 'تظهر مرتبة
المقال في تصنيف الشعبية، في
الهامش؛ وتظهر شعبية المقال (تقدير
لعدد الزيارات اليومية التي قد يسجلها اذا حافظ
تصفح الموقع على وتيرته الحالية) وعدد الزيارات
المسجلة منذ البداية، في المربع الذي
يظهر لدى حوم مؤشر الفأرة فوق العنوان.',
	'texte_sauvegarde_compressee' => 'سيتم حفظ النسخة الاحتياطية في الملف غير المضغوط @fichier@.',
	'texte_signification' => 'القضبان الداكنة تجسد البنود المتراكمة (مجموع الأقسام الفرعية)، القضبان الفاتحة تجسد عدد الزيارات لكل قسم.',
	'titre_evolution_visite' => 'تطور الزيارات',
	'titre_liens_entrants' => 'الوصلات الآتية',
	'titre_page_statistiques' => 'الاحصاءات حسب القسم',
	'titre_page_statistiques_visites' => 'إحصاءات الزيارات',

	// V
	'visites_journalieres' => 'عدد الزيارات اليومية',
	'visites_mensuelles' => 'عدد الزيارات الشهرية'
);

?>
