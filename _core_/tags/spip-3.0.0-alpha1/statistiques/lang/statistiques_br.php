<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Diverkañ ar stadegoù', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Emdroadur ar gweladennoù : <br />@visites@ gweladenn',
	'icone_repartition_actuelle' => 'Diskouez an dasparzh er mare-mañ',
	'icone_repartition_visites' => 'Dasparzh ar gweladennoù',
	'icone_statistiques_visites' => 'Stadegoù',
	'info_affichier_visites_articles_plus_visites' => 'Diskouez ar gweladennoù evit <b>ar pennadoù lennet ar muiañ, abaoe an deroù :</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Penaos lenn an daolenn-mañ',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'Stadegoù gweladenniñ',
	'info_popularite_2' => 'brud al lec\'hienn :',
	'info_popularite_3' => 'brud; : @popularite@ ; gweladennoù : @visites@',
	'info_popularite_5' => 'brud; :',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_gerer_statistiques' => 'Ha rankout a ra ho lec\'hienn merañ stadegoù ar gweladennoù ?',
	'info_question_vignettes_referer' => 'En ur sellet ouzh ar stadegoù e c\'hallit teuler un tamm sell ouzh lec\'hiennoù orin ar weladennerien',
	'info_question_vignettes_referer_oui' => 'Diskouez skeudennoùigoù lec\'hiennoù orin ar weladennerien',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'gweladenn :',
	'info_visites_plus_populaires' => 'Diskouez ar gweladennoù evit <b>ar pennadoù brudetañ</b> hag evit <b>ar pennadoù nevez-embannet :</b>',
	'info_zoom' => 'zoum',
	'item_gerer_statistiques' => 'Merañ ar stadegoù',

	// O
	'onglet_origine_visites' => 'Orin ar gweladennoù',
	'onglet_repartition_debut' => 'abaoe an deroù',
	'onglet_repartition_lang' => 'Dasparzh diouzh ar yezh',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Diverket e vo an holl roadoù a denn da stadegoù gweladenniñ al lec\'hienn, en o zouez ar re a heuilh berzh ar pennadoù.',
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'Renk ar pennad,
  hervez e vrud, a c\'haller gwelet er marz;
  brud ar pennad (o rakwelet pet gweladenn en do bemdez ma kendalc\'h
  ar gweladenniñ d\'ar memes lusk) hag an niver a weladennoù abaoe
  an deroù a c\'heller gwelet er lagadenn a zeu war wel
  pa dremen al logodenn war an titl.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'Diskouez a ra ar barrennoù teñval sammad ar gweladennoù bodet (hollad an isrubrikennoù), hag ar barrennoù sklaer an niver a weladennoù evit pep rubrikenn.',
	'titre_evolution_visite' => 'Emdroadur ar gweladennoù',
	'titre_liens_entrants' => 'Liammoù davet al lec\'hienn',
	'titre_page_statistiques' => 'Stadegoù dre rubrikennoù',
	'titre_page_statistiques_visites' => 'Stadegoù gweladenniñ',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
