<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Borrar las estadísticas', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Evolución de las visitas<br />@visites@ visitas',
	'icone_repartition_actuelle' => 'Mostrar el reparto actual',
	'icone_repartition_visites' => 'Distribución de las visitas',
	'icone_statistiques_visites' => 'Estadísticas de visitas',
	'info_affichier_visites_articles_plus_visites' => 'Mostrar las visitas de <b>los artículos más visitados desde el inicio:</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Como leer este cuadro',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'Estadísticas de las visitas',
	'info_popularite_2' => 'Popularidad del sitio:',
	'info_popularite_3' => 'Popularidad: @popularite@  Visitas: @visites@',
	'info_popularite_5' => 'Popularidad:',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_gerer_statistiques' => '¿Deseas que tu sitio gestione las estadísticas de las visitas?',
	'info_question_vignettes_referer' => 'Cuando consultes las estadísticas, puedes tener una vista de los sitios de origen de las visitas',
	'info_question_vignettes_referer_oui' => 'Mostrar las capturas de los sitios de origen de las visitas',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'visitas:',
	'info_visites_plus_populaires' => 'Mostrar las visitas de <b>los artículos más populares</b> y de <b>los últimos artículos publicados</b>',
	'info_zoom' => 'zoom',
	'item_gerer_statistiques' => 'Gestionar las estadísticas',

	// O
	'onglet_origine_visites' => 'Origen de las visitas',
	'onglet_repartition_debut' => 'desde el principio',
	'onglet_repartition_lang' => 'Distribución por idiomas',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Esta orden borra todos los datos ligados con las estadísticas de visitas al sitio, incluyendo la popularidad de los artículos.',
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'El rango del artículo, en la clasificación por popularidad, está indicado al margen; la popularidad del artículo (una estimación del número de visitas cotidianas que recibirá si el ritmo actual de visitas se mantiene) y el número de visitas recibidas desde el inicio se muestran en el menú que aparece cuando pasamos el cursor sobre el título.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'Las barras oscuras representan las entradas acumuladas (total de las subsecciones), las barras claras el número de visitas de cada sección.',
	'titre_evolution_visite' => 'Evolución de las visitas',
	'titre_liens_entrants' => 'Los enlaces entrantes ',
	'titre_page_statistiques' => 'Estadísticas por sección',
	'titre_page_statistiques_visites' => 'Estadísticas de las visitas',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
