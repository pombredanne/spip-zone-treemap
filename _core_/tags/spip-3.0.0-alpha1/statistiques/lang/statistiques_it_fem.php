<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Cancellare le statistiche', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Evoluzione delle visite<br />@visites@ visite',
	'icone_repartition_actuelle' => 'Mostra la ripartizione attuale',
	'icone_repartition_visites' => 'Distribuzione delle visite',
	'icone_statistiques_visites' => 'Statistiche delle visite',
	'info_affichier_visites_articles_plus_visites' => 'Mostra le visite per <b>gli articoli più letti dall\'inaugurazione del sito:</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Come leggere questa tabella',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'Statistiche delle visite',
	'info_popularite_2' => 'popolarità del sito: ',
	'info_popularite_3' => 'popolarità: @popularite@; visite: @visites@',
	'info_popularite_5' => 'popolarità:',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_gerer_statistiques' => 'Il sito deve gestire le statistiche delle visite?',
	'info_question_vignettes_referer' => 'Quando si consultano le statistiche è possibile visualizzare delle anteprime dei siti di origine delle visite',
	'info_question_vignettes_referer_oui' => 'Visualizzare le catture di schermo dei siti di origine delle visite',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'visite:',
	'info_visites_plus_populaires' => 'Pubblica le visite per <b>gli articoli più popolari </b> e per <b>gli ultimi articoli pubblicati:</b>',
	'info_zoom' => 'zoom',
	'item_gerer_statistiques' => 'Gestisci le statistiche',

	// O
	'onglet_origine_visites' => 'Origine delle visite',
	'onglet_repartition_debut' => 'dall\'inizio',
	'onglet_repartition_lang' => 'Suddivisione per lingua',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Questo comando cancella tutti i dati collegati alle statistiche delle visite al sito, comprese la popolarità degli articoli.',
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'Il posizionamento dell\'articolo,
nella classifica per popolarità, è indicato qui
sopra. La popolarità dell\'articolo (una stima del
numero di visite quotidiane che riceverà rimanendo costante il ritmo attuale di
consultazione) e il numero di visite ricevute dall\'inizio,
sono visualizzati nel commento che appare
quando si passa con il mouse sopra al titolo.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'Le barre più scure rappresentano le entrate accumulate (per il totale delle sottorubriche), le barre più chiare il numero di visite per ogni rubrica.',
	'titre_evolution_visite' => 'Evoluzione delle visite',
	'titre_liens_entrants' => 'I link odierni al tuo sito',
	'titre_page_statistiques' => 'Statistiche per rubrica',
	'titre_page_statistiques_visites' => 'Statistiche delle visite',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
