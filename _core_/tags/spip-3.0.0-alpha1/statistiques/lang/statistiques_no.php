<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Delete the statistics', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => '<NEW> Évolution des visites<br />@visites@ visites',
	'icone_repartition_actuelle' => '<NEW> Afficher la répartition actuelle',
	'icone_repartition_visites' => '<NEW> Répartition des visites',
	'icone_statistiques_visites' => '<NEW> Statistiques des visites',
	'info_affichier_visites_articles_plus_visites' => '<NEW> Afficher les visites pour <b>les articles les plus visités depuis le début :</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => '<NEW> Comment lire ce tableau',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => '<NEW> Statistiques des visites',
	'info_popularite_2' => '<NEW> popularité du site :',
	'info_popularite_3' => '<NEW> popularité : @popularite@ ; visites : @visites@',
	'info_popularite_5' => '<NEW> popularité :',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_gerer_statistiques' => '<NEW> Votre site doit-il gérer les statistiques des visites ?',
	'info_question_vignettes_referer' => 'When you consult the statistics, you can see a preview of the originating sites of the visits. ', # NEW
	'info_question_vignettes_referer_oui' => 'Show the screenshots of the originating sites', # NEW
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => '<NEW> visites :',
	'info_visites_plus_populaires' => '<NEW> Afficher les visites pour <b>les articles les plus populaires</b> et pour <b>les derniers articles publiés :</b>',
	'info_zoom' => '<NEW>zoom', # NEW
	'item_gerer_statistiques' => '<NEW> Gérer les statistiques',

	// O
	'onglet_origine_visites' => '<NEW> Origine des visites',
	'onglet_repartition_debut' => '<NEW> depuis le début',
	'onglet_repartition_lang' => '<NEW> par langues',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'This command deletes all data linked to the statistics of visits to the site, including the relative popularity of articles.', # NEW
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => '<NEW> Le rang de l\'article,
		dans le classement par popularité, est indiqué dans la
		marge ; la popularité de l\'article (une estimation du
		nombre de visites quotidiennes qu\'il recevra si le rythme actuel de
		consultation se maintient) et le nombre de visites reçues
		depuis le début sont affichées dans la bulle qui
		apparaît lorsque la souris survole le titre.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => '<NEW> Les barres rouges représentent les entrées cumulées (total des sous-rubriques), les barres vertes le nombre de visites pour chaque rubrique.',
	'titre_evolution_visite' => '<NEW> Évolution des visites',
	'titre_liens_entrants' => '<NEW> Les liens entrants du jour',
	'titre_page_statistiques' => '<NEW> Statistiques par rubriques',
	'titre_page_statistiques_visites' => '<NEW> Statistiques des visites',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
