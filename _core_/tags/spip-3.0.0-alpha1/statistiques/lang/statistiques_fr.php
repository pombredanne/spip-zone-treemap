<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_core_/plugins/statistiques/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants',
	'bouton_effacer_statistiques' => 'Effacer toutes les statistiques',
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@',

	// C
	'csv' => 'csv',

	// I
	'icone_evolution_visites' => 'Évolution des visites<br />@visites@ visites',
	'icone_repartition_actuelle' => 'Afficher la répartition actuelle',
	'icone_repartition_visites' => 'Répartition des visites',
	'icone_statistiques_visites' => 'Statistiques',
	'info_affichier_visites_articles_plus_visites' => 'Afficher les visites pour <b>les articles les plus visités depuis le début :</b>',
	'info_base_restauration' => 'La base est en cours de restauration.',
	'info_comment_lire_tableau' => 'Comment lire ce tableau',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.',
	'info_forum_statistiques' => 'Statistiques des visites',
	'info_popularite_2' => 'popularité du site :',
	'info_popularite_3' => 'popularité : @popularite@ ; visites : @visites@',
	'info_popularite_5' => 'popularité :',
	'info_previsions' => 'prévisions :',
	'info_question_gerer_statistiques' => 'Votre site doit-il gérer les statistiques des visites ?',
	'info_question_vignettes_referer' => 'Lorsque vous consultez les statistiques, vous pouvez visualiser des aperçus des sites d\'origine des visites',
	'info_question_vignettes_referer_oui' => 'Afficher les captures des sites d\'origine des visites',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base',
	'info_sauvegarde_articles' => 'Sauvegarder les articles',
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés',
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs',
	'info_sauvegarde_breves' => 'Sauvegarder les brèves',
	'info_sauvegarde_documents' => 'Sauvegarder les documents',
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Sauvegarder les forums',
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots',
	'info_sauvegarde_messages' => 'Sauvegarder les messages',
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés',
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions',
	'info_sauvegarde_refers' => 'Sauvegarder les referers',
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez',
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques',
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions',
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés',
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents',
	'info_sauvegarde_visites' => 'Sauvegarder les visites',
	'info_visites' => 'visites :',
	'info_visites_plus_populaires' => 'Afficher les visites pour <b>les articles les plus populaires</b> et pour <b>les derniers articles publiés :</b>',
	'info_zoom' => 'zoom',
	'item_gerer_statistiques' => 'Gérer les statistiques',

	// O
	'onglet_origine_visites' => 'Origine des visites',
	'onglet_repartition_debut' => 'depuis le début',
	'onglet_repartition_lang' => 'Répartition par langues',

	// R
	'resume' => 'Resume',

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde',
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés',
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :',

	// T
	'texte_admin_effacer_stats' => 'Cette commande efface toutes les données liées aux statistiques de visite du site, y compris la popularité des articles.',
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.',
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ',
	'texte_comment_lire_tableau' => 'Le rang de l\'article,
		dans le classement par popularité, est indiqué dans la
		marge ; la popularité de l\'article (une estimation du
		nombre de visites quotidiennes qu\'il recevra si le rythme actuel de
		consultation se maintient) et le nombre de visites reçues
		depuis le début sont affichées dans la bulle qui
		apparaît lorsque la souris survole le titre.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.',
	'texte_signification' => 'Les barres foncées représentent les entrées cumulées (total des sous-rubriques), les barres claires le nombre de visites pour chaque rubrique.',
	'titre_evolution_visite' => 'Évolution des visites',
	'titre_liens_entrants' => 'Liens entrants',
	'titre_page_statistiques' => 'Statistiques par rubriques',
	'titre_page_statistiques_visites' => 'Statistiques des visites',

	// V
	'visites_journalieres' => 'Nombre de visites par jour',
	'visites_mensuelles' => 'Nombre de visites par mois'
);

?>
