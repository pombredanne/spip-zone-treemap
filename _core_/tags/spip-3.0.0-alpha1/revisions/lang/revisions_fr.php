<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_core_/plugins/revisions/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_comparer' => 'Voir les diff&eacute;rences',

	// D
	'diff_para_ajoute' => 'Paragraphe ajouté',
	'diff_para_deplace' => 'Paragraphe déplacé',
	'diff_para_supprime' => 'Paragraphe supprimé',
	'diff_texte_ajoute' => 'Texte ajouté',
	'diff_texte_deplace' => 'Texte déplacé',
	'diff_texte_supprime' => 'Texte supprimé',
	'differences_versions' => 'Diff&eacute;rences entre version N<sup>o</sup>@id_version@ et version N<sup>o</sup>@id_diff@',

	// I
	'icone_restaurer_version' => 'Restaurer la version N<sup>o</sup>@version@',
	'icone_suivi_revisions' => 'Suivi des r&eacute;visions',
	'info_1_revision' => '1 r&eacute;vision',
	'info_aucune_revision' => 'Aucune r&eacute;vision',
	'info_historique' => 'Révisions :',
	'info_historique_lien' => 'Afficher l\'historique des modifications',
	'info_historique_titre' => 'Suivi des révisions',
	'info_nb_revisions' => '@nb@ r&eacute;visions',

	// L
	'label_choisir_id_version' => 'S&eacute;lectionnez les versions &agrave; comparer',
	'label_config_revisions_objets' => 'Sur quels objets activer les r&eacute;visions&nbsp;:',

	// O
	'objet_editorial' => 'objet',

	// P
	'plugin_update' => 'Mise &agrave; jour du plugin "R&eacute;visions" en version @version@.',

	// T
	'titre_form_revisions_objets' => 'Activer les r&eacute;visions',
	'titre_restaurer_version' => 'Restaurer une version',
	'titre_revisions' => 'R&eacute;visions',

	// V
	'version_deplace_rubrique' => 'Déplacé de <b>« @from@ »</b> vers <b>« @to@ »</b>.',
	'version_initiale' => 'Version initiale',
	'voir_differences' => 'Voir les diff&eacute;rences',
	'voir_mes_revisions' => 'Mes r&eacute;visions',
	'voir_revisions' => 'Voir les r&eacute;visions (@objet@ @id_objet@&nbsp;: @titre@)',
	'voir_toutes_les_revisions' => 'Toutes les r&eacute;visions',

	// Z
	'z' => 'zzz'
);

?>
