<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORUM &amp; PETITION', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'save as compressed in @fichier@', # MODIF
	'bouton_radio_sauvegarde_non_compressee' => 'save as uncompressed in @fichier@', # MODIF

	// F
	'forum_probleme_database' => 'Database problem, your message could not be recorded.',

	// I
	'ical_lien_rss_breves' => 'Syndication of the site\'s news items',
	'icone_creer_mot_cle_breve' => 'Create a new keyword and attach it to this news item',
	'icone_forum_administrateur' => 'Fórum administrátorov',
	'icone_forum_suivi' => 'Forums follow-up', # MODIF
	'icone_publier_breve' => 'Uverejniť túto správu',
	'icone_refuser_breve' => 'Zamietnuť túto správu',
	'info_base_restauration' => 'Restoration of the database in progress.', # MODIF
	'info_breves_03' => 'news items',
	'info_breves_liees_mot' => 'News associated with this keyword',
	'info_breves_touvees' => 'News items found',
	'info_breves_touvees_dans_texte' => 'News items found (in the text)',
	'info_echange_message' => 'Systém SPIP umožňuje výměnu zpráv a vytváření soukromých
  diskusních skupin pro účastníky webu. Tuto funkci můžete
  zapnout nebo vypnout.', # NEW
	'info_erreur_restauration' => 'Restoration error: file not found.', # MODIF
	'info_forum_administrateur' => 'administrators\' forum', # MODIF
	'info_forum_interne' => 'internal forum', # MODIF
	'info_forum_ouvert' => 'In the site\'s private area, a forum is open to all
		registered editors. Below, you can enable an
		extra forum reserved for the administrators.',
	'info_gauche_suivi_forum' => 'The <i>forums follow-up</i> page is a management tool of your site (not a discussion or editing area). It displays all the contributions of the public forum of this article and allows you to manage these contributions.', # MODIF
	'info_modifier_breve' => 'Modify the news item:',
	'info_nombre_breves' => '@nb_breves@ news items,',
	'info_option_ne_pas_faire_suivre' => 'Do not forward forums messages', # MODIF
	'info_restauration_sauvegarde_insert' => 'Inserting @archive@ in the database', # MODIF
	'info_sauvegarde_articles' => 'Backup the articles', # MODIF
	'info_sauvegarde_articles_sites_ref' => 'Backup articles of referenced sites',
	'info_sauvegarde_auteurs' => 'Backup the authors', # MODIF
	'info_sauvegarde_breves' => 'Backup the news',
	'info_sauvegarde_documents' => 'Backup the documents', # MODIF
	'info_sauvegarde_echouee' => 'If the backup fails («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Backup the forums', # MODIF
	'info_sauvegarde_groupe_mots' => 'Backup keyword groups', # MODIF
	'info_sauvegarde_messages' => 'Backup the messages', # MODIF
	'info_sauvegarde_mots_cles' => 'Backup the keywords',
	'info_sauvegarde_petitions' => 'Backup the petitions',
	'info_sauvegarde_refers' => 'Backup the referrers', # MODIF
	'info_sauvegarde_reussi_01' => 'Backup successful.',
	'info_sauvegarde_rubrique_reussi' => 'The tables of the @titre@ section have been saved to @archive@. You can',
	'info_sauvegarde_rubriques' => 'Backup the sections', # MODIF
	'info_sauvegarde_signatures' => 'Backup petitions signatures',
	'info_sauvegarde_sites_references' => 'Backup referenced sites',
	'info_sauvegarde_type_documents' => 'Backup documents types', # MODIF
	'info_sauvegarde_visites' => 'Backup the visits', # MODIF
	'info_une_breve' => 'a news item,',
	'item_mots_cles_association_breves' => 'news items',
	'item_nouvelle_breve' => 'Nová novinka',

	// L
	'lien_forum_public' => 'Spravovať verejne fórum tohto článku',
	'lien_reponse_breve' => 'Reply to the news item',

	// S
	'sauvegarde_fusionner' => 'Merge the current database with the backup', # MODIF
	'sauvegarde_fusionner_depublier' => 'Unpublish the merged objects',
	'sauvegarde_url_origine' => 'If necessary, the URL of the source site:', # MODIF

	// T
	'texte_admin_tech_03' => 'You can choose to save the file in a compressed form, to 
	speed up its transfer to your machine or to a backup server and save some disk space.',
	'texte_admin_tech_04' => 'In order to merge with another database, you can restrict the backup to one section: ',
	'texte_sauvegarde_compressee' => 'Backup will be stored in the uncompressed file @fichier@.', # MODIF
	'titre_nouvelle_breve' => 'Nová novinka',
	'titre_page_breves_edit' => 'Modify the news item: «@titre@»',
	'titre_page_forum' => 'Administrators forum', # MODIF
	'titre_page_forum_envoi' => 'Poslať správu',
	'titre_page_statistiques_messages_forum' => 'Forum messages' # MODIF
);

?>
