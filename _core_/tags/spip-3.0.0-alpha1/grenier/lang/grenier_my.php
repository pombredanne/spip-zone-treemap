<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'ဖိုရမ် &amp; တောင်းဆိုမှု', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'ချုံ့ထားပြီး @fichier@ တွင်သိမ်းဆည်းရန်',
	'bouton_radio_sauvegarde_non_compressee' => 'ချုံ့မထားဘဲ @fichier@ တွင်သိမ်းဆည်းရန်',

	// F
	'forum_probleme_database' => 'သိုလောင်မှု ပြသနာ၊ သင့်စာစောင် သိမ်းဆည်း၍မရနိုင်ပါ',

	// I
	'ical_lien_rss_breves' => 'ပူးတွဲချိတ်ဆက်ရန် ဝက်ဘ်ဆိုက် သတင်းအချက်လက်များ',
	'icone_creer_mot_cle_breve' => 'Créer un nouveau mot-clé et le lier à cette brève', # NEW
	'icone_forum_administrateur' => 'စီမံသူများ ဖိုရမ်',
	'icone_forum_suivi' => 'ဖိုရမ်နောက်ဆက်တွဲ',
	'icone_publier_breve' => 'ဤသတင်းအချက်ကို ထုတ်ဝေပါ',
	'icone_refuser_breve' => 'ဤသတင်းအချက်အလက် အသစ်ကို ပယ်လိုက်ပါ',
	'info_base_restauration' => 'သတင်းအချက်လက် သိုလှောင်မှုကို စတင်ပြန်လည်ထည့်သွင်းရန်',
	'info_breves_03' => 'သတင်းအချက်အလက်များ',
	'info_breves_liees_mot' => 'သော့ချက်စကားလုံးနှင့် သတင်းကို ပေါင်းစည်းလိုက်သည်',
	'info_breves_touvees' => 'သတင်းအချက်အလက် အသစ်များတွေ့သည်',
	'info_breves_touvees_dans_texte' => 'သတင်းအချက်အလက်များတွေ့ရသည်(စာသားအတွင်းတွင်)',
	'info_echange_message' => 'သုံးစွဲသူအချင်းချင်းကြားတွင် ကိုယ်ပိုင်ဖိုရမ်တွင် ဆွေးနွေးခြင်း သတင်းစကားများ ဖလှယ်ခြင်းများကို SPIP မှ ခွင့်ပြုထားပါသည်။ ဤလုပ်ဆောင်ချက်များကို သင့်မှာရွေးချယ်ပိုင်ခွင့် ရှိသည်။',
	'info_erreur_restauration' => 'ပြန်လည်ဖော်ထုတ်မှုအခက်ခဲ - ဖိုင်ကို မတွေ့ပါ',
	'info_forum_administrateur' => 'ကြီးကြပ်သူဖိုရမ်',
	'info_forum_interne' => 'အတွင်းပိုင်းဆိုင်ရာ ဖိုရမ်',
	'info_forum_ouvert' => 'ကိုယ်ပိုင်ဝက်ဘ်ဆိုက်ဧရိယာတွင် ဖိုရမ်သည် မှတ်ပုံတင်ထားသော စာတည်းများအားလုံး ပါဝင်ခွင့်ရှိနေသည်',
	'info_gauche_suivi_forum' => '<i>ဖို​ရမ်နောက်ဆက်တွဲ</i> စာမျက်နှာသည် သင့်ဝက်ဘ်ဆိုက်၏ စီမံခန့်ခွဲသည့် ကိရိယာဖြစ်သည် (ဆွေးနွေးမှု (သို့) ပြန်လည်ပြုပြင်သည့် ဧရိယာမဟုတ်ပါ) ၄င်းသည်ဤဆောင်းပါး၏ အများသုံးဖိုရမ်ထည့်ဝင်မှုများကို ဖော်ပြပြီး ထိုထည့်ဝင်မှုများကို စီမံခွင့်ပြူသည်',
	'info_modifier_breve' => 'သတင်းအချက်များကို ပြုပြင်မွမ်းမံပါ',
	'info_nombre_breves' => '@nb_breves@ သတင်းအချက်များ',
	'info_option_ne_pas_faire_suivre' => 'ဖိုရမ်စာများကို အခြားတယောက်ထံသို့ ထပ်ဆင့်မပို့ပါနှင့်',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'ဆောင်းပါးများကို အရံသိမ်းထားပါ',
	'info_sauvegarde_articles_sites_ref' => 'ကိုးကားဝက်ဘ်ဆိုက်များမှ ဆောင်းပါးများကို အရံသိမ်းထားပါ',
	'info_sauvegarde_auteurs' => 'စာရေးသူများကို အရံသိမ်းထားပါ',
	'info_sauvegarde_breves' => 'သတင်းများကို အရံသိမ်းထားပါ',
	'info_sauvegarde_documents' => 'မှတ်တမ်းများကို အရံသိမ်းထားပါ',
	'info_sauvegarde_echouee' => 'အရံသိမ်းထားသော ဖိုင်များမှာ(« အများဆုံး ပြီးမြောက်သည့်အချိန် »)ဖြစ်ခဲ့လျှင်',
	'info_sauvegarde_forums' => 'ဖိုရမ်များကို အရံသိမ်ထားပါ',
	'info_sauvegarde_groupe_mots' => 'သော့ချက်စကားလုံးအုပ်စုများကို အရံသိမ်းထားပါ',
	'info_sauvegarde_messages' => 'စာများကို အရံသိမ်းထားပါ',
	'info_sauvegarde_mots_cles' => 'သော့ချက်စကားလုံးများကို အရံသိမ်းထားပါ',
	'info_sauvegarde_petitions' => 'တောင်းဆိုမှုများကို အရံသိမ်းထားပါ',
	'info_sauvegarde_refers' => 'အကိုးကားများယူသည့်နေရာကို အရံသိမ်းထားပါ',
	'info_sauvegarde_reussi_01' => 'အရံသိမ်းဆည်းမှု အောင်မြင်သည်',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'အခန်းကဏ္ဍများကို အရံကူးသိမ်းထားပါ',
	'info_sauvegarde_signatures' => 'တောင်းဆိုမှု သဘောသဘာဝများကို အရံကူးသိမ်းထားပါ',
	'info_sauvegarde_sites_references' => 'ကိုးကားဝက်ဘ်ဆိုက်များကို အရံကူးသိမ်းထားပါ',
	'info_sauvegarde_type_documents' => 'မှတ်တမ်းပုံစံများကို အရံကူးသိမ်းထားပါ',
	'info_sauvegarde_visites' => 'လာရောက်လည်ပတ်မှုကို အရံကူးသိမ်းထားပါ',
	'info_une_breve' => 'သတင်းအချက်အလက်တခု',
	'item_mots_cles_association_breves' => 'သတင်းအချက်အလက်များ',
	'item_nouvelle_breve' => 'သတင်းအချက်အလက် အသစ်',

	// L
	'lien_forum_public' => 'သည်ဆောင်းပါး၏ အ​​​​များသုံးဖိုရမ်ကို စီမံခန့်ခွဲပါ',
	'lien_reponse_breve' => 'သတင်းအချက်သို့ အကြောင်းပြန်ပါ',

	// S
	'sauvegarde_fusionner' => 'လက်ရှိ အချက်လက်သိုလှောင်မှုကို အရံကူးထားသောအရာနှင့် ပေါင်းစည်းပေးပါ',
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'အကယ်၍​အရေးကြီးပါက မူလဆိုက်၏ ဝက်ဘ်ဆိုက်လိပ်စာကိုရေးပါ',

	// T
	'texte_admin_tech_03' => 'ဆာဗာနေရာ အလွတ်ပိုမိုရရှိနိုင်ရန်နှင့် သိမ်း​ဆည်းမှု ပိုမိုမြန်ဆန်စေရန် ဖိုင်ကိုချုံ့ပြီးသိမ်းဆည်းရန် သင်ရွေးချယ်နိုင်ပါသည်',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'ချုံ့မထားသော @fichier@ ဖိုင်တွင်အရံသိမ်းဆည်းမည်',
	'titre_nouvelle_breve' => 'သတင်းအချက်အလက်အသစ်',
	'titre_page_breves_edit' => 'သတင်းအချက် «@titre@» ကို ပြန်လည်မွမ်းမံပါ',
	'titre_page_forum' => 'ကြီးကြပ်သူဖိုရမ်',
	'titre_page_forum_envoi' => 'မှာကြားချက်တစောင်ပို့ပါ',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
