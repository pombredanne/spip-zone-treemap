<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORUM &amp; PETIŢIE', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'salvare comprimată în @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'salvare necomprimată în @fichier@',

	// F
	'forum_probleme_database' => 'Problemă tehnică cu baza de date, mesajul dumneavoastră nu a fost înregistrat.',

	// I
	'ical_lien_rss_breves' => 'Sindicalizarea ştirilor site-ului',
	'icone_creer_mot_cle_breve' => 'Creaţi un nou cuvânt cheie şi legati-l de această ştire',
	'icone_forum_administrateur' => 'Forum-ul administratorilor',
	'icone_forum_suivi' => 'Urmărirea forum-urilor',
	'icone_publier_breve' => 'Publicaţi această ştire',
	'icone_refuser_breve' => 'Refuzaţi această ştire',
	'info_base_restauration' => 'Baza de date este pe cale să fie restaurată.',
	'info_breves_03' => 'ştiri',
	'info_breves_liees_mot' => 'Ştiri legate de acest cuvânt-cheie',
	'info_breves_touvees' => 'Ştiri găsite',
	'info_breves_touvees_dans_texte' => 'Ştiri găsite (în text)',
	'info_echange_message' => 'SPIP permite schimbul de mesaje şi constituirea de forumuri de discuţie
 private între participanţii site-ului. Puteţi activa sau
 dezactiva această funcţionalitate.',
	'info_erreur_restauration' => 'Eroare de restaurare: fişier inexistent.',
	'info_forum_administrateur' => 'forum-ul administratorilor',
	'info_forum_interne' => 'forum intern',
	'info_forum_ouvert' => 'În spaţiul privat, un forum este deschis pentru toţi redactorii înregistraţi.
   Puteţi activa mai jos un forum suplimentar, rezervat doar administratorilor.',
	'info_gauche_suivi_forum' => '<i>Pagina de urmărire a forum-urilor</i> este o unealtă de gestiune a site-ului (nu un spaţiu de discuţii sau de redactare). Această pagină afişează toate contribuţiile la forum-ul public al acestui articol şi vă permite să gestionaţi aceste contribuţii.',
	'info_modifier_breve' => 'Modificaţi ştirea:',
	'info_nombre_breves' => '@nb_breves@ ştiri,',
	'info_option_ne_pas_faire_suivre' => 'Faceţi ca mesajele forum-urilor să nu fie trimise autorilor articolelor',
	'info_restauration_sauvegarde_insert' => 'Inserţia @archive@ în bază',
	'info_sauvegarde_articles' => 'Salvaţi articolele',
	'info_sauvegarde_articles_sites_ref' => 'Salvaţi articolele site-urilor referenţiate',
	'info_sauvegarde_auteurs' => 'Salvaţi autorii',
	'info_sauvegarde_breves' => 'Salvaţi ştirea',
	'info_sauvegarde_documents' => 'Salvaţi documentele',
	'info_sauvegarde_echouee' => 'Dacă salvarea a eşuat («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Salvaţi forum-urile',
	'info_sauvegarde_groupe_mots' => 'Salvaţi grupurile de cuvinte-cheie',
	'info_sauvegarde_messages' => 'Salvaţi mesajele',
	'info_sauvegarde_mots_cles' => 'Salvaţi cuvintele-cheie',
	'info_sauvegarde_petitions' => 'Salvaţi petiţiile',
	'info_sauvegarde_refers' => 'Salvaţi referers',
	'info_sauvegarde_reussi_01' => 'Salvare reuşită.',
	'info_sauvegarde_rubrique_reussi' => 'Tabelele din rubrica @titre@ au fost salvate în @archive@. Puteţi',
	'info_sauvegarde_rubriques' => 'Salvaţi rubricile',
	'info_sauvegarde_signatures' => 'Salvaţi semnăturile petiţiilor',
	'info_sauvegarde_sites_references' => 'Salvaţi site-urile referenţiate',
	'info_sauvegarde_type_documents' => 'Salvaţi tipurile de documente',
	'info_sauvegarde_visites' => 'Salvaţi vizitele',
	'info_une_breve' => 'o ştire,',
	'item_mots_cles_association_breves' => 'ştirilor',
	'item_nouvelle_breve' => 'Ştire nouă',

	// L
	'lien_forum_public' => 'Gestionaţi forum-ul public al acestui articol',
	'lien_reponse_breve' => 'Răspuns la ştire',

	// S
	'sauvegarde_fusionner' => 'Îmbinaţi baza curentă cu cea salvată',
	'sauvegarde_fusionner_depublier' => 'De-publicaţi obiectele fuzionate',
	'sauvegarde_url_origine' => 'Eventual, URL-ul site-ului de origine :',

	// T
	'texte_admin_tech_03' => 'Puteţi să alegeţi să salvaţi fişierul sub o formă comprimată, atât pentru
 a micşora durata transferului către dumneavoastră sau spre un server de siguranţă, cât şi pentru a face economie de spaţiu de disc.',
	'texte_admin_tech_04' => 'În scopul de fuzionare cu o altă bază de date, puteţi limita salvarea la rubrica:',
	'texte_sauvegarde_compressee' => 'Salvarea va fi făcută într-un fişier necomprimat @fichier@.',
	'titre_nouvelle_breve' => 'Ştire nouă',
	'titre_page_breves_edit' => 'Modificaţi ştirea: « @titre@ »',
	'titre_page_forum' => 'Forum-ul administratorilor',
	'titre_page_forum_envoi' => 'Trimiteţi un mesaj',
	'titre_page_statistiques_messages_forum' => 'Mesajele forum-ului'
);

?>
