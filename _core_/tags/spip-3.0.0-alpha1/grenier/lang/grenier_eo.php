<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORUMO &amp; PETSKRIBO', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'savkopio kompaktita en @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'savkopio nekompaktita en @fichier@',

	// F
	'forum_probleme_database' => 'Datenbazo-problemo, via mesaĝo ne estas registrita.',

	// I
	'ical_lien_rss_breves' => 'Aboni retejajn fulm-informojn',
	'icone_creer_mot_cle_breve' => 'Krei novan ŝlosilvorton kaj ligi ĝin kun tiu ĉi fulminformo',
	'icone_forum_administrateur' => 'Forumo de la mastrumantoj',
	'icone_forum_suivi' => 'Supervido de la forumoj',
	'icone_publier_breve' => 'Publikigi tiun fulm-informon',
	'icone_refuser_breve' => 'Rifuzi tiun fulm-informon',
	'info_base_restauration' => 'La datenbazo estas restaŭriĝanta.',
	'info_breves_03' => 'fulm-informoj',
	'info_breves_liees_mot' => 'La fulm-informoj ligitaj kun tiu ŝlosilvorto',
	'info_breves_touvees' => 'Trovitaj fulm-informoj',
	'info_breves_touvees_dans_texte' => 'Trovitaj fulm-informoj (en la teksto)',
	'info_echange_message' => 'SPIP ebligas interŝanĝon de mesaĝoj kaj la starigon de privataj diskutforumoj
 inter la partoprenantoj de la retejo. Vi povas aktivigi aŭ
 malaktivigi tiun funkcion.',
	'info_erreur_restauration' => 'Restaŭro-eraro: neekzistanta dosiero.',
	'info_forum_administrateur' => 'forumo de la mastrumantoj',
	'info_forum_interne' => 'interna forumo',
	'info_forum_ouvert' => 'En la privata spaco de la retejo, forumo estas malfermita al ĉiuj
  registritaj redaktantoj. Vi povas, ĉi-sube, aktivigi
  plian forumon, rezervitan nur al mastrumantoj.',
	'info_gauche_suivi_forum' => 'La paĝo pri <i>superkontrolo de la forumoj</i> estas mastrumilo de via retejo (kaj ne diskutejo aŭ redaktejo). Ĝi afiŝas ĉiujn kontribuaĵojn de la publika forumo pri tiu artikolo, kaj ebligas al vi mastrumi tiujn kontribuaĵojn.',
	'info_modifier_breve' => 'Modifi la fulm-informon:',
	'info_nombre_breves' => '@nb_breves@ fulm-informoj,',
	'info_option_ne_pas_faire_suivre' => 'Ne plusendi la mesaĝojn de la forumoj',
	'info_restauration_sauvegarde_insert' => 'Enigo de @archive@ en la datumbazon',
	'info_sauvegarde_articles' => 'Konservi la artikolojn',
	'info_sauvegarde_articles_sites_ref' => 'Konservi la artikolojn de la referencigitaj retejoj',
	'info_sauvegarde_auteurs' => 'Konservi la aŭtorojn',
	'info_sauvegarde_breves' => 'Konservi la fulm-informojn',
	'info_sauvegarde_documents' => 'Konservi la dokumentojn',
	'info_sauvegarde_echouee' => 'Se la konservo malsukcesis («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Konservi la forumojn',
	'info_sauvegarde_groupe_mots' => 'Konservi la vortogrupojn',
	'info_sauvegarde_messages' => 'Konservi la mesaĝojn',
	'info_sauvegarde_mots_cles' => 'Konservi la ŝlosilvortojn',
	'info_sauvegarde_petitions' => 'Savkopii la petskribojn',
	'info_sauvegarde_refers' => 'Savkopii referencojn',
	'info_sauvegarde_reussi_01' => 'Savkopio sukcesis.',
	'info_sauvegarde_rubrique_reussi' => 'La tabeloj de la rubriko @titre@ estis savkopiitaj en @archive@. Vi povas',
	'info_sauvegarde_rubriques' => 'Konservi la rubrikojn',
	'info_sauvegarde_signatures' => 'Konservi la subskribojn de petskriboj',
	'info_sauvegarde_sites_references' => 'Konservi la referencigitajn retejojn',
	'info_sauvegarde_type_documents' => 'Konservi la dokumentotipojn',
	'info_sauvegarde_visites' => 'Konservi la vizitojn',
	'info_une_breve' => 'fulm-informo,',
	'item_mots_cles_association_breves' => 'al fulm-informoj',
	'item_nouvelle_breve' => 'Nova fulm-informo',

	// L
	'lien_forum_public' => 'Mastrumi la publikan forumon de tiu ĉi artikolo',
	'lien_reponse_breve' => 'Respondo al la fulm-informo',

	// S
	'sauvegarde_fusionner' => 'Kunfandi la nunan bazon kaj la savkopion',
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuale, la retadreso de la origina retejo :',

	// T
	'texte_admin_tech_03' => 'Vi povas elekti konservi la dosieron laŭ densigita formo, por
 rapidigi ties ŝuton hejmen aŭ al konservo-servilo, kaj por ŝpari diskospacon.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'La savkopio estos farita en la nedensigita dosiero @fichier@.',
	'titre_nouvelle_breve' => 'Nova fulm-informo',
	'titre_page_breves_edit' => 'Modifi la fulm-informon: « @titre@ »',
	'titre_page_forum' => 'Forumo de la mastrumantoj',
	'titre_page_forum_envoi' => 'Sendi mesaĝon',
	'titre_page_statistiques_messages_forum' => 'Forumaj mesaĝoj'
);

?>
