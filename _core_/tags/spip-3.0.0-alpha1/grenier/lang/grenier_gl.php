<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORO &amp; PETICIÓN',
	'bouton_radio_sauvegarde_compressee' => 'copia de seguridade comprimida en @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'copia de seguridade sen comprimir en @fichier@',

	// F
	'forum_probleme_database' => 'Problema da base de datos, a súa mensaxe non foi rexistrada.',

	// I
	'ical_lien_rss_breves' => 'Afiliación das breves do web',
	'icone_creer_mot_cle_breve' => 'Crear un nova palabra chave e ligala a esta breve',
	'icone_forum_administrateur' => 'Foro de xestión',
	'icone_forum_suivi' => 'Seguimento dos foros',
	'icone_publier_breve' => 'Publica esta breve',
	'icone_refuser_breve' => 'Rexeitar esta breve',
	'info_base_restauration' => 'A base está en proceso de restauración.',
	'info_breves_03' => 'breves',
	'info_breves_liees_mot' => 'As breves ligadas a esta palabra chave',
	'info_breves_touvees' => 'Breves atopadas',
	'info_breves_touvees_dans_texte' => 'Breves atopadas (no texto)',
	'info_echange_message' => 'SPIP permite o intercambio de mensaxes e a constitución de foros de discusión
  privados entre os participantes do web. Pode activar ou
  desactivar esta funcionalidade.',
	'info_erreur_restauration' => 'Erro de restauración: o ficheiro non existe.',
	'info_forum_administrateur' => 'foro do equipo de xestión',
	'info_forum_interne' => 'foro interno',
	'info_forum_ouvert' => 'Dentro do espazo privado do web, hai un foro aberto
  a toda os usuarios rexistrados. Aquí abaixo pode activar un foro
  complementar, reservado ao equipo de xestión.',
	'info_gauche_suivi_forum' => 'A p&amp;aacute;xina de <i>seguimento dos foros</i> &amp;eeacute; unha ferramenta de xesti&amp;oacute;n do web (e non un espazo de discusi&amp;oacute;n ou de redacci&amp;oacute;n). Mostra todas as contribuci&amp;oacute;ns no foro p&amp;uacute;blico deste artigo e permite xestionar estas contribuci&amp;oacute;ns.',
	'info_modifier_breve' => 'Modificar a breve:',
	'info_nombre_breves' => '@nb_breves@ breves,',
	'info_option_ne_pas_faire_suivre' => 'Non enviar avisos das mensaxes dos foros',
	'info_restauration_sauvegarde_insert' => 'Inserción de @archive@ na base de datos',
	'info_sauvegarde_articles' => 'Gardar os artigos',
	'info_sauvegarde_articles_sites_ref' => 'Gardar os artigos dos webs referidos',
	'info_sauvegarde_auteurs' => 'Gardar os autores',
	'info_sauvegarde_breves' => 'Gardar as breves',
	'info_sauvegarde_documents' => 'Gardar os documentos',
	'info_sauvegarde_echouee' => 'Se a copia de seguridade fallou («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Gardar os foros',
	'info_sauvegarde_groupe_mots' => 'Gardar os grupos de palabras',
	'info_sauvegarde_messages' => 'Gardar as mensaxes',
	'info_sauvegarde_mots_cles' => 'Gardar as palabras chave',
	'info_sauvegarde_petitions' => 'Gardar as peticións',
	'info_sauvegarde_refers' => 'Gardar os referentes',
	'info_sauvegarde_reussi_01' => 'Feita a copia de seguridade.',
	'info_sauvegarde_rubrique_reussi' => 'As táboas da sección @titre@ foron gardadas en @archive@. Pode',
	'info_sauvegarde_rubriques' => 'Gardar os temas',
	'info_sauvegarde_signatures' => 'Gardar as sinaturas de peticións',
	'info_sauvegarde_sites_references' => 'Gardar os webs referidos',
	'info_sauvegarde_type_documents' => 'Gardar os tipos de documentos',
	'info_sauvegarde_visites' => 'Gardar as visitas',
	'info_une_breve' => 'unha breve,',
	'item_mots_cles_association_breves' => 'ás breves',
	'item_nouvelle_breve' => 'Nova breve',

	// L
	'lien_forum_public' => 'Xestionar o foro público deste artigo',
	'lien_reponse_breve' => 'Resposta á breve',

	// S
	'sauvegarde_fusionner' => 'Fusionar a base de datos actual e a copia de seguridade',
	'sauvegarde_fusionner_depublier' => 'Despublicar os obxectos fusionados',
	'sauvegarde_url_origine' => 'Eventualemente, URL do web de orixe :',

	// T
	'texte_admin_tech_03' => 'Pode escoller gardar o ficheiro en formato comprimido, para optimizar
  a transferencia ou a copia nun servidor de copias de seguridade, e para aforrar espazo no disco.',
	'texte_admin_tech_04' => 'Nun intento de fusión con outra base, pode limitar a copia de seguridade á sección:',
	'texte_sauvegarde_compressee' => 'A copia de seguridade será feita no ficheiro sen comprimir @fichier@.',
	'titre_nouvelle_breve' => 'Nova breve',
	'titre_page_breves_edit' => 'Modificar a breve: « @titre@ »',
	'titre_page_forum' => 'Foro de xestión',
	'titre_page_forum_envoi' => 'Enviar unha mensaxe',
	'titre_page_statistiques_messages_forum' => 'Mensaxes de foro'
);

?>
