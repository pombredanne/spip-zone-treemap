<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'Foroa eta eskaera', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'ecrire/data/dump.xml.gzdelakoan kopia konprimitua',
	'bouton_radio_sauvegarde_non_compressee' => 'ecrire/data/dump.xml delakoan kopia ez-konprimitua',

	// F
	'forum_probleme_database' => 'Datu-basearen arazoa, zure mezua ez dago erregistraturik.',

	// I
	'ical_lien_rss_breves' => 'Guneko laburren sindikazioa',
	'icone_creer_mot_cle_breve' => 'Gako-hitz berri bat sortu eta labur huni lotu',
	'icone_forum_administrateur' => 'Administratzaileen foroa',
	'icone_forum_suivi' => 'Foroen gestioa',
	'icone_publier_breve' => 'Berri labur hau argitaratu',
	'icone_refuser_breve' => 'Berri labur hau ez onartu',
	'info_base_restauration' => 'Basea berrezarriaizaten ari da',
	'info_breves_03' => 'berri laburrak',
	'info_breves_liees_mot' => 'Gako-hitz honi lotutako berri laburrak',
	'info_breves_touvees' => 'Aurkitutako berri laburrak',
	'info_breves_touvees_dans_texte' => '(Testuan) aurkitutako berri laburrak',
	'info_echange_message' => 'SPIPek guneko partehartzaileen arteko mezuen trukaketa eta foro pribatuak ahalbidetzen ditu.
Funtzionalitate hau aktiba edo desaktibatzen ahal duzu.',
	'info_erreur_restauration' => 'Lehengoratze akatsa. Fitxeroa ez dago.',
	'info_forum_administrateur' => 'Administrazio taldearen foroa ',
	'info_forum_interne' => 'Barruko forua',
	'info_forum_ouvert' => 'Gunearen eremu pribatuan,erregistratuta dauden idazlari guztientzako foro bat zabalik dago.
 Behean, foro gehigarribat aktiba dezakezu, bakarrik administratzaileentzako erreserbatuta.',
	'info_gauche_suivi_forum' => 'Foroak jarraitzeko orrialdea zureguneko kudeaketa-tresna bat da (eta ez eztabaida edo idazketa-eremu bat). Artikulu honen foro publikoko kontribuzio guztiak bistaratzen ditu eta kontribuzio hauek administratzen uzten dizu.',
	'info_modifier_breve' => 'Berri laburra aldatu',
	'info_nombre_breves' => '@nb_breves@ labur,',
	'info_option_ne_pas_faire_suivre' => 'Ez bidali foroetako mezuak',
	'info_restauration_sauvegarde_insert' => '@archive@ datu-basean txertatu',
	'info_sauvegarde_articles' => 'Artikuluen ziurtasun kopia egin',
	'info_sauvegarde_articles_sites_ref' => 'Gune erreferentziatuetako artikuluak gorde',
	'info_sauvegarde_auteurs' => 'Egileak gorde',
	'info_sauvegarde_breves' => 'Berri laburrak gorde',
	'info_sauvegarde_documents' => 'Dokumentuak gorde',
	'info_sauvegarde_echouee' => 'Babeskopiak huts egiten badu («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Foroak gorde',
	'info_sauvegarde_groupe_mots' => 'Hitz-taldeak gorde',
	'info_sauvegarde_messages' => 'Mezuak gorde',
	'info_sauvegarde_mots_cles' => 'Gako-hitzak gorde',
	'info_sauvegarde_petitions' => 'Eskaerak gorde',
	'info_sauvegarde_refers' => 'Erreferituak gorde',
	'info_sauvegarde_reussi_01' => 'Baliozko babeskopia.',
	'info_sauvegarde_rubrique_reussi' => '@titre@ gaieko mahaiak @archive@-ean gordetuak izan dira.',
	'info_sauvegarde_rubriques' => 'Atalak gorde',
	'info_sauvegarde_signatures' => 'Eskaera-sinadurak gorde',
	'info_sauvegarde_sites_references' => 'Gune erreferientziatuak gorde',
	'info_sauvegarde_type_documents' => 'Dokumentu-motak gorde',
	'info_sauvegarde_visites' => 'Bisitak gorde',
	'info_une_breve' => 'berri labur bat,',
	'item_mots_cles_association_breves' => 'berri laburrekin',
	'item_nouvelle_breve' => 'Berri labur berria',

	// L
	'lien_forum_public' => 'Artikulu honen foroa kudeatu',
	'lien_reponse_breve' => 'Berri laburrarierantzuna',

	// S
	'sauvegarde_fusionner' => 'Une hunetako basea eta babeskopia batu',
	'sauvegarde_fusionner_depublier' => 'Despublikatu batutako objetuak',
	'sauvegarde_url_origine' => 'Beharrez, jatorrizko gunearen URLa :',

	// T
	'texte_admin_tech_03' => 'Fitxategia modu konprimituan gordetzea, bere transferentzia mozteko zure ordenagailuan edo babeskopien zerbitzari batean, eta disko-lekua aurreztea aukera dezakezu.',
	'texte_admin_tech_04' => 'Beste base betekin bat-egitearen helburuarekin, babeskopia atal huntara mugatzen ahal duzu :',
	'texte_sauvegarde_compressee' => 'Ziurtasun kopia ecrire/data/dump.xml konpresiorik gabeko artxibo batean egingo da.',
	'titre_nouvelle_breve' => 'Labur berria',
	'titre_page_breves_edit' => '« @titre@ » notalaburra aldatu',
	'titre_page_forum' => 'Administratzaileen eztabaida-lekua',
	'titre_page_forum_envoi' => 'Mezu bat bidali',
	'titre_page_statistiques_messages_forum' => 'Foroko mezuak'
);

?>
