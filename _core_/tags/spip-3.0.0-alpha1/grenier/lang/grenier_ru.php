<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'ФОРУМ И КОММЕНТАРИИ', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'сохранить сжатым в @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'сохранить распакованным в @fichier@',

	// F
	'forum_probleme_database' => 'Проблема базы данных, ваше сообщение не могло быть записано.',

	// I
	'ical_lien_rss_breves' => 'Объединение новостей сайта',
	'icone_creer_mot_cle_breve' => 'Создать новое ключевое слово и прикрепить его к этой новости',
	'icone_forum_administrateur' => 'Форум администраторов',
	'icone_forum_suivi' => 'Отслеживать форумы',
	'icone_publier_breve' => 'Опубликовать эту новость',
	'icone_refuser_breve' => 'Убрать эту новость',
	'info_base_restauration' => 'Восстановление базы данных в ходе работы.',
	'info_breves_03' => 'новости',
	'info_breves_liees_mot' => 'Новости, связанные с этим ключевым словом',
	'info_breves_touvees' => 'Новости найдены',
	'info_breves_touvees_dans_texte' => 'Новости найдены(в тексте)',
	'info_echange_message' => 'SPIP позволяет организовывать приватные
  форумы для обсуждений между участниками сайта. Вы можете включить или 
  отключить эту функцию.',
	'info_erreur_restauration' => 'Ошибка восстановления: файл не найден.',
	'info_forum_administrateur' => 'административный форум',
	'info_forum_interne' => 'внутренний форум',
	'info_forum_ouvert' => 'В административной части сайта, форум открыт для всех 
  зарегистрированных редакторов. Ниже Вы можете включить 
  дополнительный форум для отдельных администраторов.',
	'info_gauche_suivi_forum' => '<i>Продолжение форумов </i> страница - инструмент управления Вашего сайта (не область обсуждения или редактирования). Она показывает все вклады основного форума этой статьи и позволяет Вам управлять этими вкладами.',
	'info_modifier_breve' => 'Изменить новость:',
	'info_nombre_breves' => '@nb_breves@ новости,',
	'info_option_ne_pas_faire_suivre' => 'Не отправлять сообщения c форумов',
	'info_restauration_sauvegarde_insert' => 'Вставка @архива@ в базу данных',
	'info_sauvegarde_articles' => 'Резервная копия статей',
	'info_sauvegarde_articles_sites_ref' => 'Резервная копия статей связанных сайтов',
	'info_sauvegarde_auteurs' => 'Резервная копия авторов',
	'info_sauvegarde_breves' => 'Резервная копия новостей',
	'info_sauvegarde_documents' => 'Резервная копия документов',
	'info_sauvegarde_echouee' => 'Если резервная копия повреждена («Максимальный  лимит времени превышен»),',
	'info_sauvegarde_forums' => 'Резервная копия форумов',
	'info_sauvegarde_groupe_mots' => 'Резервная копия групп ключевых слов',
	'info_sauvegarde_messages' => 'Резервная копия сообщений',
	'info_sauvegarde_mots_cles' => 'Резервная копия ключевых слов',
	'info_sauvegarde_petitions' => 'Резервную копия комментариев',
	'info_sauvegarde_refers' => 'Резервная копия ссылок',
	'info_sauvegarde_reussi_01' => 'Резервная копия создана успешно',
	'info_sauvegarde_rubrique_reussi' => 'Таблицы @titre@ раздела были сохранены в @archive@. Вы можете',
	'info_sauvegarde_rubriques' => 'Резервная копия разделов',
	'info_sauvegarde_signatures' => 'Резервная копия подтверждений подписей',
	'info_sauvegarde_sites_references' => 'Резервная копия ссылок на сайты',
	'info_sauvegarde_type_documents' => 'Резервная копия типов документов',
	'info_sauvegarde_visites' => 'Резервная копия посещений',
	'info_une_breve' => 'новость,',
	'item_mots_cles_association_breves' => 'новости',
	'item_nouvelle_breve' => 'Новая новость',

	// L
	'lien_forum_public' => 'Управлять этой статьей основного форума',
	'lien_reponse_breve' => 'Ответить на новость',

	// S
	'sauvegarde_fusionner' => 'Объединить текущую базу данных с резервной копией',
	'sauvegarde_fusionner_depublier' => 'Не опубликовывать объединенные объекты',
	'sauvegarde_url_origine' => 'Если необходимо, адрес источника сайта:',

	// T
	'texte_admin_tech_03' => 'Вы можете архивировать файл с резервной копией для экономии времени копирования.',
	'texte_admin_tech_04' => 'Вы можете сделать как полную резервную копию сайта, так и копию только одного раздела:',
	'texte_sauvegarde_compressee' => 'Резервная копия будет сохранена в файле @fichier@.',
	'titre_nouvelle_breve' => 'Новая новость',
	'titre_page_breves_edit' => 'Изменить новость: «@titre@»',
	'titre_page_forum' => 'Форум администраторов',
	'titre_page_forum_envoi' => 'Отправить сообщение',
	'titre_page_statistiques_messages_forum' => 'Форум сообщений'
);

?>
