<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORUM &amp; OGŁOSZENIA', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'zapisz w postaci skompresowanej w @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'zapisz w postaci nieskompresowanej w @fichier@',

	// F
	'forum_probleme_database' => 'Problem z bazą danych, Twoja wiadomość nie mogła być zapisana.',

	// I
	'ical_lien_rss_breves' => 'Zapisz się do RSS newsów',
	'icone_creer_mot_cle_breve' => 'Utwórz nowe słowo kluczowe i połącz je z tym newsem',
	'icone_forum_administrateur' => 'Forum administratorów',
	'icone_forum_suivi' => 'Kontynuacja forum',
	'icone_publier_breve' => 'Publikuj ten news',
	'icone_refuser_breve' => 'Odrzuć ten news',
	'info_base_restauration' => 'Trwa odtwarzanie bazy danych.',
	'info_breves_03' => 'newsy',
	'info_breves_liees_mot' => 'Newsy powiązane z tym słowem kluczowym',
	'info_breves_touvees' => 'Newsy znalezione',
	'info_breves_touvees_dans_texte' => 'Newsy znalezione (w tekście)',
	'info_echange_message' => 'SPIP pozwala na wymianę wiadomości i utworzenie Forum dyskusyjnego
  prywatnie pomiędzy użytkownikami tej strony. Możesz włączyć lub wyłączyć
  tę funkcję.',
	'info_erreur_restauration' => 'Błąd odtwarzania : plik nie istnieje.',
	'info_forum_administrateur' => 'forum administratorów',
	'info_forum_interne' => 'forum wewnętrzne',
	'info_forum_ouvert' => 'W strefie prywatnej, forum jest otwarte dla wszystkich
  zarejestrownych redaktorów. Możecie także aktywować
  dodatkowe forum, zarezerwowane jedynie dla adminów.',
	'info_gauche_suivi_forum' => 'Strona <i>obserwacji forum</i> jest narzędziem zarządzania stroną (nie, miejscem dyskusji czy redakcji). Wyświetla ona wszystkie komentarze do danego artykułu na forum publicznym i pozwala edytować owe komentarze.',
	'info_modifier_breve' => 'Zmiana newsa:',
	'info_nombre_breves' => '@nb_breves@ newsów,',
	'info_option_ne_pas_faire_suivre' => 'Nie przesyłać wiadomości tego forum',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Backup artykułów',
	'info_sauvegarde_articles_sites_ref' => 'Zapisz artykuły ze zlinkowanych stron',
	'info_sauvegarde_auteurs' => 'Backup autorów',
	'info_sauvegarde_breves' => 'Backup newsów',
	'info_sauvegarde_documents' => 'Backup dokumentów',
	'info_sauvegarde_echouee' => 'Jeśli zapis się nie powiódł («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Backup forum',
	'info_sauvegarde_groupe_mots' => 'Backup grup słów kluczowych',
	'info_sauvegarde_messages' => 'Backup wiadomości',
	'info_sauvegarde_mots_cles' => 'Backup słów kluczowych',
	'info_sauvegarde_petitions' => 'Zapisz ogłoszenia',
	'info_sauvegarde_refers' => 'Zapisz odnośniki',
	'info_sauvegarde_reussi_01' => 'Backup zakończył się pomyślnie.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Kopia bezpieczeństwa działów',
	'info_sauvegarde_signatures' => 'Zapisz podpisy petycji',
	'info_sauvegarde_sites_references' => 'Zapisz zlinkowane strony',
	'info_sauvegarde_type_documents' => 'Backup typów dokumentów',
	'info_sauvegarde_visites' => 'Backup odwiedzin',
	'info_une_breve' => 'news,',
	'item_mots_cles_association_breves' => 'newsy',
	'item_nouvelle_breve' => 'Nowy news',

	// L
	'lien_forum_public' => 'Zarządzaj forum publicznym tego artykułu',
	'lien_reponse_breve' => 'Odpowiedz na ten news',

	// S
	'sauvegarde_fusionner' => 'Dokonać połączenia istniejącej bazy danych z backupem',
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Ewentualnie, URL strony oryginalnej :',

	// T
	'texte_admin_tech_03' => 'Możesz wybrać wykonanie kopii bezpieczeńśtwa pod postacią skompresowaną, w celu
 przyspieszenia ściągania pliku lub zapisywania na serwerze, i zarazem oszczędności przestrzeni dyskowej.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'Backup zostanie zrobiony w nieskompresowanym pliku @fichier@.',
	'titre_nouvelle_breve' => 'Nowy news',
	'titre_page_breves_edit' => 'Edytuj newsa: «@titre@»',
	'titre_page_forum' => 'Forum administratorów',
	'titre_page_forum_envoi' => 'Wyślij wiadomość',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
