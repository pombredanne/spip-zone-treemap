<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORUM &amp; PETICION', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'Salvagarda comprimida jos @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'Salvagarda non comprimida jos @fichier@',

	// F
	'forum_probleme_database' => 'Problèma de basa de donadas, vòstre messatge s\'es pas registrat.',

	// I
	'ical_lien_rss_breves' => 'Sindicacion de las brèvas del sit',
	'icone_creer_mot_cle_breve' => 'Crear un mot-clau nòu e lo ligar a aquela brèva',
	'icone_forum_administrateur' => 'Forum dels administrators',
	'icone_forum_suivi' => 'Seguit dels forums',
	'icone_publier_breve' => 'Publicar aquela brèva',
	'icone_refuser_breve' => 'Refusar aquela brèva',
	'info_base_restauration' => 'La basa es en cors de restauracion.',
	'info_breves_03' => 'brèvas',
	'info_breves_liees_mot' => 'Las brèvas ligadas a aquel mot clau',
	'info_breves_touvees' => 'Brèvas trobadas',
	'info_breves_touvees_dans_texte' => 'Brèvas trobadas (dins lo tèxt)',
	'info_echange_message' => 'SPIP permet d\'escambiar de messatges e de constituir de forums privats de discussion entre los participants del sit. Podètz activar o desactivar aquela foncionalitat.',
	'info_erreur_restauration' => 'Error de restauracion : fichièr inexistent.',
	'info_forum_administrateur' => 'forum dels administrators',
	'info_forum_interne' => 'forum intèrne',
	'info_forum_ouvert' => 'Dins l\'espaci privat del sit, un forum es dobèrt a totes los redactors registrats . Podètz, çai jos, activar un forum suplementari, reservat als administrators sonque.',
	'info_gauche_suivi_forum' => 'La pagina de <i>seguit dels forums</i> es una aisina de gestion del vòstre sit (mas es pas un espaci per discutir o per redigir). Aficha totas las contribucions del forum public d\'aquel article e vos permet de gerir aquelas contribucions.',
	'info_modifier_breve' => 'Modificar la brèva:',
	'info_nombre_breves' => '@nb_breves@ brèvas, ',
	'info_option_ne_pas_faire_suivre' => 'Far pas seguir los messatges dels forums',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Salvagardar los articles',
	'info_sauvegarde_articles_sites_ref' => 'Salvagardar los articles dels sits referenciats',
	'info_sauvegarde_auteurs' => 'Salvagardar los autors',
	'info_sauvegarde_breves' => 'Salvagardar las brèvas',
	'info_sauvegarde_documents' => 'Salvagardar los documents',
	'info_sauvegarde_echouee' => 'Se la salvagarda a abocat («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Salvagardar los forums',
	'info_sauvegarde_groupe_mots' => 'Salvagardar los grops de mots',
	'info_sauvegarde_messages' => 'Salvagardar los messatges',
	'info_sauvegarde_mots_cles' => 'Salvagardar los mots clau',
	'info_sauvegarde_petitions' => 'Salvagardar las peticions',
	'info_sauvegarde_refers' => 'Salvagardar los referidors',
	'info_sauvegarde_reussi_01' => 'Salvagarda capitada.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Salvagardar las rubricas',
	'info_sauvegarde_signatures' => 'Salvagardar las signaturas de peticions',
	'info_sauvegarde_sites_references' => 'Salvagardar los sits referenciats',
	'info_sauvegarde_type_documents' => 'Salvagardar los tipes de documents',
	'info_sauvegarde_visites' => 'Salvagardar las vesitas',
	'info_une_breve' => 'una brèva, ',
	'item_mots_cles_association_breves' => 'a las brèvas',
	'item_nouvelle_breve' => 'Brèva nòva',

	// L
	'lien_forum_public' => 'Gerir lo forum public d\'aquel article',
	'lien_reponse_breve' => 'Responsa a la brèva',

	// S
	'sauvegarde_fusionner' => 'Fusionar la basa actuala e lo salvament',
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventualament, URL del sit d\'origina :',

	// T
	'texte_admin_tech_03' => 'Podètz causir de salvagardar lo fichièr jos forma comprimida, per tal
 d\'abrivar son transferiment en cò vòstre o a un servidor de salvagardas, e per tal d\'estalviar d\'espaci disc.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'La salvagarda se farà dins lo fichièr non comprimit @fichier@.',
	'titre_nouvelle_breve' => 'Brèva novèla',
	'titre_page_breves_edit' => 'Modificar la brèva: «@titre@»',
	'titre_page_forum' => 'Forum pels administrators',
	'titre_page_forum_envoi' => 'Mandar un messatge',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
