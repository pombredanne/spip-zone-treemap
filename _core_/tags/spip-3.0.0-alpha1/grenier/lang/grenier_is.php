<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => '<NEW> FORUM & PÉTITION', # MODIF
	'bouton_radio_sauvegarde_compressee' => '<NEW> sauvegarde compressée sous @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => '<NEW> sauvegarde non compressée sous @fichier@',

	// F
	'forum_probleme_database' => 'Vegna vandamála í gagnagrunninum, þá voru skilaboðin ekki meðtekin.',

	// I
	'ical_lien_rss_breves' => 'Syndication des brèves du site', # NEW
	'icone_creer_mot_cle_breve' => 'Créer un nouveau mot-clé et le lier à cette brève', # NEW
	'icone_forum_administrateur' => 'Umræðuvettvangur ritstjóra',
	'icone_forum_suivi' => '<NEW> Suivi des forums',
	'icone_publier_breve' => 'Birta þessa stuttu frétt',
	'icone_refuser_breve' => 'Hafna þessari stuttu frétt',
	'info_base_restauration' => '<NEW> La base est en cours de restauration.',
	'info_breves_03' => '<NEW> brèves',
	'info_breves_liees_mot' => '<NEW> Les brèves liées à ce mot-clé',
	'info_breves_touvees' => '<NEW> Brèves trouvées',
	'info_breves_touvees_dans_texte' => '<NEW> Brèves trouvées (dans le texte)',
	'info_echange_message' => '<NEW> SPIP permet l\'échange de messages et la constitution de forums de discussion
		privés entre les participants du site. Vous pouvez activer ou
		désactiver cette fonctionnalité.',
	'info_erreur_restauration' => '<NEW> Erreur de restauration : fichier inexistant.',
	'info_forum_administrateur' => '<NEW> forum des administrateurs',
	'info_forum_interne' => '<NEW> forum interne',
	'info_forum_ouvert' => '<NEW> Dans l\'espace privé du site, un forum est ouvert à tous
		les rédacteurs enregistrés. Vous pouvez, ci-dessous, activer un
		forum supplémentaire, réservé aux seuls administrateurs.',
	'info_gauche_suivi_forum' => '<NEW> La page de <i>suivi des forums</i> est un outil de gestion de votre site (et non un espace de discussion ou de rédaction). Elle affiche toutes les contributions du forum public de cet article et vous permet de gérer ces contributions.',
	'info_modifier_breve' => '<NEW> Modifier la brève :',
	'info_nombre_breves' => '<NEW> @nb_breves@ brèves,',
	'info_option_ne_pas_faire_suivre' => '<NEW> Ne pas faire suivre les messages des forums',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => '<NEW> Sauvegarder les articles',
	'info_sauvegarde_articles_sites_ref' => '<NEW> Sauvegarder les articles des sites référencés',
	'info_sauvegarde_auteurs' => '<NEW> Sauvegarder les auteurs',
	'info_sauvegarde_breves' => '<NEW> Sauvegarder les brèves',
	'info_sauvegarde_documents' => 'Vistið skjölin',
	'info_sauvegarde_echouee' => '<NEW> Si la sauvegarde a échoué («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => '<NEW> Sauvegarder les forums',
	'info_sauvegarde_groupe_mots' => '<NEW> Sauvegarder les groupes de mots',
	'info_sauvegarde_messages' => '<NEW> Sauvegarder les messages',
	'info_sauvegarde_mots_cles' => '<NEW> Sauvegarder les mots-clés',
	'info_sauvegarde_petitions' => '<NEW> Sauvegarder les pétitions',
	'info_sauvegarde_refers' => '<NEW> Sauvegarder les referers',
	'info_sauvegarde_reussi_01' => '<NEW> Sauvegarde réussie.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => '<NEW> Sauvegarder les rubriques',
	'info_sauvegarde_signatures' => '<NEW> Sauvegarder les signatures de pétitions',
	'info_sauvegarde_sites_references' => '<NEW> Sauvegarder les sites référencés',
	'info_sauvegarde_type_documents' => '<NEW> Sauvegarder les types de documents',
	'info_sauvegarde_visites' => '<NEW> Sauvegarder les visites',
	'info_une_breve' => '<NEW> une brève,',
	'item_mots_cles_association_breves' => '<NEW> aux brèves',
	'item_nouvelle_breve' => '<NEW> Nouvelle brève',

	// L
	'lien_forum_public' => '<NEW> Gérer le forum public de cet article',
	'lien_reponse_breve' => '<NEW> Réponse à la brève',

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_tech_03' => '<NEW> Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => '<NEW> La sauvegarde sera faite dans le fichier non compressé @fichier@.',
	'titre_nouvelle_breve' => '<NEW> Nouvelle brève',
	'titre_page_breves_edit' => '<NEW> Modifier la brève : « @titre@ »',
	'titre_page_forum' => '<NEW> Forum des administrateurs',
	'titre_page_forum_envoi' => '<NEW> Envoyer un message',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
