<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/breves?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'breve' => 'Meldung',
	'breves' => 'Meldungen',

	// E
	'entree_breve_publiee' => 'Soll diese Meldung veröffentlicht werden?', # MODIF
	'entree_texte_breve' => 'Text der Meldung',

	// I
	'icone_breves' => 'Meldungen',
	'icone_ecrire_nouvel_article' => 'Meldungen in dieser Rubrik',
	'icone_modifier_breve' => 'Meldung bearbeiten',
	'icone_nouvelle_breve' => 'Neue Meldung schreiben',
	'info_1_breve' => '1 Meldung',
	'info_aucun_breve' => 'Keine Meldung',
	'info_breves' => 'Verwendet Ihre Website Meldungen?',
	'info_breves_02' => 'Meldungen',
	'info_breves_valider' => 'Zur Veröffentlichung vorgeschlagene Meldungen',
	'info_gauche_numero_breve' => 'MELDUNG NUMMER', # MODIF
	'info_nb_breves' => '@nb@ Meldungen',
	'item_breve_proposee' => 'Vorgeschlagene Meldung', # MODIF
	'item_breve_refusee' => 'NEIN - Meldung abgelehnt', # MODIF
	'item_breve_validee' => 'JA - Meldung freigegeben', # MODIF
	'item_non_utiliser_breves' => 'Keine Meldungen verwenden.',
	'item_utiliser_breves' => 'Meldungen verwenden',

	// L
	'logo_breve' => 'MELDUNGS-LOGO', # MODIF

	// T
	'texte_breves' => 'Meldungen sind kurze einfache Texte, um schnell und unkompliziert Nachrichten, eine Presseschau oder einen Veranstaltungskalender zu publizieren ...',
	'titre_breve_proposee' => 'Vorgeschlagene Meldungen',
	'titre_breve_publiee' => 'veröffentlichte Meldungen',
	'titre_breve_refusee' => 'abgelehnte Meldung',
	'titre_breves' => 'Meldungen',
	'titre_langue_breve' => 'SPRACHE DER MELDUNG', # MODIF
	'titre_nouvelle_breve' => 'Nouvelle brève', # NEW
	'titre_page_breves' => 'Meldungen'
);

?>
