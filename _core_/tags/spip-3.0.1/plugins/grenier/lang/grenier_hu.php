<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/grenier?lang_cible=hu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FÓRUM ÉS ALÁÍRÁSGYŰJTÉS (Petició)', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'tömörített mentés a következön: @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'nem tömörített mentés a következőn: @fichier@',

	// F
	'forum_probleme_database' => 'Adatbázis hiba, az Ön üzenetét nem rögzítettük.',

	// I
	'ical_lien_rss_breves' => 'Syndication des brèves du site', # NEW
	'icone_creer_mot_cle_breve' => 'Créer un nouveau mot-clé et le lier à cette brève', # NEW
	'icone_forum_administrateur' => 'Adminisztrátorok fóruma',
	'icone_forum_suivi' => 'Fórumok megfigyelése',
	'icone_publier_breve' => 'A hír publikálása',
	'icone_refuser_breve' => 'A hír elutasítása',
	'info_base_restauration' => 'Az adatbázis resztaurálás alatt van.',
	'info_breves_03' => 'hírek',
	'info_breves_liees_mot' => 'A kulcsszóhoz kötött hírek',
	'info_breves_touvees' => 'Talált hírek',
	'info_breves_touvees_dans_texte' => 'Talált hírek (a szövegben)',
	'info_echange_message' => 'SPIP engedi a magán üzenetcserét és vitafórumok alakítását
  a honlap résztvevői között. Ezt a lehetőséget lehet aktiválni, vagy inaktiválni.',
	'info_erreur_restauration' => 'Resztaurálási hiba : nem létező fájl.',
	'info_forum_administrateur' => 'adminisztrátorok fóruma',
	'info_forum_interne' => 'belső fórum',
	'info_forum_ouvert' => 'Minden regisztrált szerző részére van fórum a honlap privát részében.Lejjebb aktiválhat egy újabb fórumot, ami csak az adminisztrátoroknak lesz elérhető.',
	'info_gauche_suivi_forum' => 'A <i>fórumok megfigyelése</i> nevű oldal  a honlap egyik kezelési eszkőze (és nem pedig egy vitás, vagy szerzői rész). A cikk nyilvános fórumának összes hozzzászólását jeleníti meg és lehetővé teszi e hozzászólások kezelését.', # MODIF
	'info_modifier_breve' => 'A hír módosítása :',
	'info_nombre_breves' => '@nb_breves@ hír,',
	'info_option_ne_pas_faire_suivre' => 'Nem továbbítani a fórumok üzeneteit',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'A cikkek mentése',
	'info_sauvegarde_articles_sites_ref' => 'A felvett honlapok cikkeinek mentése',
	'info_sauvegarde_auteurs' => 'A szerzők mentése',
	'info_sauvegarde_breves' => 'A hírek mentése',
	'info_sauvegarde_documents' => 'A dokumentumok mentése',
	'info_sauvegarde_echouee' => 'Ha sikertelen a mentés («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'A fórumok mentése',
	'info_sauvegarde_groupe_mots' => 'A kulcsszó csoportok mentése',
	'info_sauvegarde_messages' => 'Az üzenetek mentése',
	'info_sauvegarde_mots_cles' => 'A kulcssavak mentése',
	'info_sauvegarde_petitions' => 'Az aláírásgyűjtések mentése',
	'info_sauvegarde_refers' => 'A "referers" mentése',
	'info_sauvegarde_reussi_01' => 'Mentés sikeres.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'A rovatok mentése',
	'info_sauvegarde_signatures' => 'A peticiók aláírásainak mentése',
	'info_sauvegarde_sites_references' => 'A felvett honlapok mentése',
	'info_sauvegarde_type_documents' => 'A dokumentumok tipusainak mentése',
	'info_sauvegarde_visites' => 'A látógatások mentése',
	'info_une_breve' => 'egy hír,',
	'item_mots_cles_association_breves' => 'hírekre',
	'item_nouvelle_breve' => 'Új hír',

	// L
	'lien_forum_public' => 'A cikk nyilvános fórumának kezelése',
	'lien_reponse_breve' => 'Hozzászólás a hírhez',

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_tech_03' => 'Tömörített formában is lehet menteni a fájlt, a rövidebb letöltési idő és a kisebb foglalt hely érdekében.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'A mentés a nem tömörített @fichier@ n. fájlba fog törtenni.',
	'titre_nouvelle_breve' => 'Új hír',
	'titre_page_breves_edit' => 'A hír módosítása : « @titre@ »',
	'titre_page_forum' => 'Adminisztrátori fórum',
	'titre_page_forum_envoi' => 'Üzenet küldés',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
