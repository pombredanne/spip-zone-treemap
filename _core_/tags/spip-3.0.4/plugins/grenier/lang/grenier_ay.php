<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'ARUSKIPAÑA &amp; IYAWSATANAKA', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'Jitthaptat qhanañcht\'ata ukan @fichier@ ',
	'bouton_radio_sauvegarde_non_compressee' => 'qhanancht\'at jan jitthaptat ukan @fichier@ ',

	// F
	'forum_probleme_database' => 'Pirwan yatiyawinakax jan waliru puritaw, yatiyawimax janiw qillqaskiti',

	// I
	'ical_lien_rss_breves' => 'Qamawit jisk\'aptatanak apxasiwi ',
	'icone_creer_mot_cle_breve' => 'Créer un nouveau mot-clé et le lier à cette brève', # NEW
	'icone_forum_administrateur' => 'Aruskipañawi apnaqirinakana',
	'icone_forum_suivi' => 'Aruskipañanakat uñch\'ukiwi',
	'icone_publier_breve' => 'Aka jisk\'aptata taqir yatiyaña',
	'icone_refuser_breve' => 'Aka jisk\'aptata janiw saña',
	'info_base_restauration' => 'Basex askichasiskiwa',
	'info_breves_03' => 'Jisk\'aptatanaka',
	'info_breves_liees_mot' => 'Jisk\'aptatanak aka imt\'at arur mayachata',
	'info_breves_touvees' => 'Jikit jisk\'aptatanak',
	'info_breves_touvees_dans_texte' => 'Jisk\'aptatanak jikita (sawuna)',
	'info_echange_message' => 'Kuntix lurañatak muntaxa, spip ukax yatiyawinak turkakipi ukhamarak jark\'at aruskipañanak qamawit arsurinakkam utji. Akax naktataspaw ukjamarak jiwt\'ayataspaw.',
	'info_erreur_restauration' => 'Sumachawit pantjata. Q\'ipix janiw utjkiti.',
	'info_forum_administrateur' => 'Qutu apnaqirita aruskipañawi',
	'info_forum_interne' => 'manqhat aruskipañawi',
	'info_forum_ouvert' => 'Qamawit ch\'usawj jark\'atapanx Taqpach qillqatanakatakikiw mä aruskipañawiw utji. Ukxarux, Mä aruskipañaw apxatat apnaqirinakatakikiw naktayaña.',
	'info_gauche_suivi_forum' => 'Web qamawixa &lt;i&gt;aruskipañawinakat uñch\'ukiwi&lt;/i&gt; mä irnaqañatak qamawimat apnaqawiwa ( janiw aruskipaw qamawxakiti ukhamarak janiw qillqayaw qamawxakiti). Taqpach yanapanaka aruskipañawinakat aka qillqatata ukhamarak apnaqayaraktamwa.',
	'info_modifier_breve' => 'Modifier la brève :', # NEW
	'info_nombre_breves' => '@nb_breves@ brèves,', # NEW
	'info_option_ne_pas_faire_suivre' => 'Ne pas faire suivre les messages des forums', # NEW
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_une_breve' => 'une brève,', # NEW
	'item_mots_cles_association_breves' => 'aux brèves', # NEW
	'item_nouvelle_breve' => 'Nouvelle brève', # NEW

	// L
	'lien_forum_public' => 'Gérer le forum public de cet article', # NEW
	'lien_reponse_breve' => 'Réponse à la brève', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'titre_nouvelle_breve' => 'Nouvelle brève', # NEW
	'titre_page_breves_edit' => 'Modifier la brève : « @titre@ »', # NEW
	'titre_page_forum' => 'Forum des administrateurs', # NEW
	'titre_page_forum_envoi' => 'Envoyer un message', # NEW
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
