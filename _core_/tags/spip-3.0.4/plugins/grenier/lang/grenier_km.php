<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/grenier?lang_cible=km
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'វេទិកា & បណ្តឹងជំទាស់', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'ច្បាប់ចំលង ត្រូវបានបង្ហាប់ ជា @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'ច្បាប់ចំលង គ្មានត្រូវបានបង្ហាប់ ជា @fichier@',

	// F
	'forum_probleme_database' => 'បញ្ហា​មូលដ្ឋានទិន្នន័យ, សារ​របស់​អ្នក ​មិនត្រូវបានរក្សា​ទុក។',

	// I
	'ical_lien_rss_breves' => 'បញ្ជាប់ពត៌មានខ្លី ​របស់​សៃថ៍',
	'icone_creer_mot_cle_breve' => 'បង្កើតថ្មី មួយពាក្យគន្លឹះ និងភ្ជាប់វា ទៅរបស់ដំណឹងនេះ',
	'icone_forum_administrateur' => 'វេទិកា​ នៃអ្នកអភិបាល',
	'icone_forum_suivi' => 'ការ​តាមដាន​ នៃវេទិកា',
	'icone_publier_breve' => 'បោះផ្សាយ ពត៌មានខ្លីនេះ',
	'icone_refuser_breve' => 'បដិសេធ ខ្ទង់ដំណឹងនេះ',
	'info_base_restauration' => 'មូលដ្ឋានទិន្នន័យ កំពុងត្រូវបានស្តារ។',
	'info_breves_03' => 'ខ្ទង់ដំណឹង',
	'info_breves_liees_mot' => 'ដំណឹងបានភ្ជាប់ជាមួយ ពាក្យគន្លឹះនេះ',
	'info_breves_touvees' => 'រកឃើញ ខ្ទង់ដំណឹង',
	'info_breves_touvees_dans_texte' => 'រកឃើញ ខ្ទង់ដំណឹង (ក្នុងអត្ថបទ)',
	'info_echange_message' => 'SPIP អនុញាត បណ្តូរសារ និង​ការ​បង្កើត​វេទិកា​ពិភាក្សា​ឯកជន ​រវាង​អ្នកចូលរួម នៃវ៉ែបសៃថ៍។ អ្នក​អាច​សកម្ម ឬ អសកម្ម មុខងារ នេះ។',
	'info_erreur_restauration' => 'កំហុស​ក្នុង​ការ​ចុះ​ឈ្មោះ ៖ ហ្វា​ល​រក​មិនឃើញ',
	'info_forum_administrateur' => 'វេទិកា​របស់​អ្នកគ្រប់គ្រង',
	'info_forum_interne' => 'វេទិកា​ផ្ទៃក្នុង',
	'info_forum_ouvert' => 'ក្នុងវាលឯកជន នៃសៃថ៍, មួយទីប្រជុំមតិ ត្រូវបានចំហ សំរាប់គ្រប់ស្មេរ ដែលបានចុះបញ្ជី។ អ្នកអាច, ធ្វើអោយសកម្ម មួយទីប្រជុំមតិបន្ថែម ខាងក្រោម, ត្រូវបានទុក សំរាប់ត្រឹម ពួកអ្នកអភិបាល។',
	'info_gauche_suivi_forum' => 'ទំព័រ <i>តាមដាន ទីប្រជុំមតិ</i> ជា មួយឧបករគ្រប់គ្រង នៃវ៉ែបសៃថ៍ របស់អ្នក (និង មិនជា ធ្លាពិភាក្សា ឬ​ ធ្លាបោះផ្សាយ​)។ វា បង្ហាញ គ្រប់ការរួមចំណែក របស់ទីប្រជុំមតិសាធារណះ នៃអត្ថបទនេះ និងអនុញ្ញាតអ្នក គ្រប់គ្រង ការរួមចំណែក ទាំងនេះ។',
	'info_modifier_breve' => 'កែប្រែ ពត៌មានខ្លី៖',
	'info_nombre_breves' => 'ខ្ទង់ដំណឹង នៃ @nb_breves@,',
	'info_option_ne_pas_faire_suivre' => 'កុំ​បញ្ជូន​សារ​របស់​វេទិកា',
	'info_restauration_sauvegarde_insert' => 'បញ្ចូល @archive@ ក្នុងមូលដ្ឋានទិន្នន័យ',
	'info_sauvegarde_articles' => 'បង្កើតច្បាប់ចំលង អត្ថបទនានា',
	'info_sauvegarde_articles_sites_ref' => 'បង្កើតច្បាប់ចំលង អត្ថបទនានា នៃសៃថ៍យោង',
	'info_sauvegarde_auteurs' => 'បង្កើតច្បាប់ចំលង អ្នកនិពន្ធនានា',
	'info_sauvegarde_breves' => 'បង្កើតច្បាប់ចំលង ពត៌មានសង្ខេបទាំងឡាយ',
	'info_sauvegarde_documents' => 'បង្កើតច្បាប់ចំលង ឯកសារទាំងឡាយ',
	'info_sauvegarde_echouee' => 'បើ​ច្បាប់ចំលង បានបរាជ័យ («បានផុតកំណត់ពេល អតិបរិមា»),',
	'info_sauvegarde_forums' => 'បង្កើតច្បាប់ចំលង វេទិកាទាំងឡាយ',
	'info_sauvegarde_groupe_mots' => 'បង្កើតច្បាប់ចំលង ក្រុមទាំងឡាយ នៃពាក្យ',
	'info_sauvegarde_messages' => 'បង្កើតច្បាប់ចំលង សារទាំងឡាយ',
	'info_sauvegarde_mots_cles' => 'បង្កើតច្បាប់ចំលង ពាក្យគន្លឹះទាំងឡាយ',
	'info_sauvegarde_petitions' => 'បង្កើតច្បាប់ចំលង បណ្តឹងជំទាស់ទាំងឡាយ',
	'info_sauvegarde_refers' => 'បង្កើតច្បាប់ចំលង ឧបករយោងទាំងឡាយ',
	'info_sauvegarde_reussi_01' => 'ការបង្កើតច្បាប់ចំលង បានជោគជ័យ។',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'បង្កើតច្បាប់ចំលង ផ្នែកទាំងឡាយ',
	'info_sauvegarde_signatures' => 'បង្កើតច្បាប់ចំលង ហត្ថលេខាទាំងឡាយ នៃបណ្តឹងជំទាស់',
	'info_sauvegarde_sites_references' => 'បង្កើតច្បាប់ចំលង សៃថ៍យោងទាំងឡាយ',
	'info_sauvegarde_type_documents' => 'ការ​ថត​ទុក​ប្រភេទ​ឯកសារ',
	'info_sauvegarde_visites' => 'បង្កើតច្បាប់ចំលង ការទស្សនាទាំងឡាយ',
	'info_une_breve' => 'មួយខ្ទង់ដំណឹង,',
	'item_mots_cles_association_breves' => 'ខ្ទង់ដំណឹង',
	'item_nouvelle_breve' => 'ខ្ទង់ដំណឹងថ្មី',

	// L
	'lien_forum_public' => 'គ្រប់គ្រង​វេទិកាសាធារណៈ​នៃ​អត្ថបទ​នេះ',
	'lien_reponse_breve' => 'ចំលើយ ទៅ​ពត៌មានខ្លី',

	// S
	'sauvegarde_fusionner' => 'ប​ញ្ជូ​ល​សំណុំ​ទិន្នន័យ​ថ្មីៗ​ជាមួយនឹង​ទិន្នន័យ​ថត​ទុក',
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'ទោះយ៉ាងណា, អាសយដ្ឋាន URL នៃសៃថ៍ប្រភព៖',

	// T
	'texte_admin_tech_03' => 'លោក​អ្នក​អាច​ជ្រើសរើស​រក្សា​ទុក​ហ្វា​ល​ដែល​មាន​ទម្រង់​បន្ថែ​ន​រួច ដើម្បី​ព​ន្លឿ​ន​ការ​ផ្ទេរ​របស់​វា​ទៅ​ម៉ាស៊ីន​របស់​លោក​អ្នក ឬ​ទៅ​ម៉ាស៊ីន​មេ​រក្សា​ទុក និង​ដើម្បី​រក្សា​បាន​នូវ​ថាស​ឌី​ស​ខ្លះៗ​។',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'ឯកសារ​ដែល​បាន​ថត​ទុក​នឹង​ត្រូវ​រក្សា​ទុក​នៅ​ក្នុង​ហ្វា​ល​ដែល​មិន​ប​ណ្ដែ​ន (uncompressed file) @fichier@',
	'titre_nouvelle_breve' => 'ខ្ទង់ថ្មី នៃដំណឹង',
	'titre_page_breves_edit' => 'កែប្រែ ពត៌មានខ្លី ៖ «@titre@»',
	'titre_page_forum' => 'វេទិកា​អ្នកគ្រប់គ្រង​ប្រព័ន្ធ',
	'titre_page_forum_envoi' => 'ផ្ញើ​សារ',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
