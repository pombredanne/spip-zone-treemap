<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/grenier?lang_cible=fon
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'Pképklé kpodo xojla kpo', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'wiwlihɛn ɖo wiwlanmɛ /data/dump.xml.gz',
	'bouton_radio_sauvegarde_non_compressee' => 'wiwlihɛn ɖo wiwlanmɛ /data/dump.xml.gz',

	// F
	'forum_probleme_database' => 'gbètákɛnxòkplé ɔ gbàfɔ, yè sìwú wlí wɛn mì tɔn hɛn á',

	// I
	'ical_lien_rss_breves' => 'Syndication des brèves du site', # NEW
	'icone_creer_mot_cle_breve' => 'Créer un nouveau mot-clé et le lier à cette brève', # NEW
	'icone_forum_administrateur' => 'kpékpé gán lɛ tɔn',
	'icone_forum_suivi' => 'àgun cícɔ',
	'icone_publier_breve' => 'sɔ xó kléwún é lɔ ɖó àtɛ',
	'icone_refuser_breve' => 'gbɛ xó kléwún ɔ',
	'info_base_restauration' => 'gbètákɛnxòkplé kò jà àfɔ ɖè jí.',
	'info_breves_03' => 'xó kléwún lɛ',
	'info_breves_liees_mot' => 'xó kléwún lɛ ɖê cádó gbètákɛn lɛ wú ɔ',
	'info_breves_touvees' => 'xó kléwún lɛ ɖê yè mɔ ɔ',
	'info_breves_touvees_dans_texte' => 'xó kléwún lɛ ɖê yè mɔ ɔ',
	'info_echange_message' => 'xó kpó ɖò yìyì wɛ',
	'info_erreur_restauration' => 'bíblò ɔ jàyì, gbètá ɔ ɖé á.',
	'info_forum_administrateur' => 'Tìtòtɔ lɛ sín kpíkplé',
	'info_forum_interne' => 'Kpékplé xɔmɛ tɔn ',
	'info_forum_ouvert' => 'nyɔwlántɔ lɛ sin àgùn ',
	'info_gauche_suivi_forum' => 'wémà é lɔ Tìtòtɔ lɛ kɛɖɛ wɛ síwú dàlɔmɛ',
	'info_modifier_breve' => 'ɖyɔ xó kléwún ɔ:',
	'info_nombre_breves' => '@nb_breves@ xó kléwún,',
	'info_option_ne_pas_faire_suivre' => 'Mi mà nɔ kpɔn wɛn lɛ ɖò kplékplé ɔ mɛ wó',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Wlí  Wékpá lɛ  hɛn',
	'info_sauvegarde_articles_sites_ref' => 'Mi Wlí  Wékpá lɛ ɖê sɔ kɛn nâ hɛn',
	'info_sauvegarde_auteurs' => 'Wlí nyɔwlántɔ lɛ hɛn',
	'info_sauvegarde_breves' => 'Wlí xó kléwún lɛ hɛn',
	'info_sauvegarde_documents' => 'Wlí wěmà lɛ hɛn',
	'info_sauvegarde_echouee' => 'Nú mi mà síwú wlíhɛn à,',
	'info_sauvegarde_forums' => 'Mi wlí kplékplé ɔ hɛn',
	'info_sauvegarde_groupe_mots' => 'Mi wlí xóxókplé lɛ hɛn',
	'info_sauvegarde_messages' => 'Mi wlí wɛn lɛ hɛn',
	'info_sauvegarde_mots_cles' => 'Mi wlí gbètá kɛn lɛ hɛn',
	'info_sauvegarde_petitions' => 'Mi wlí xójíjlá lɛ hɛn',
	'info_sauvegarde_refers' => 'Mi wlí referers lɛ hɛn',
	'info_sauvegarde_reussi_01' => 'Wlíhɛn nyɔ.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Mi wlí àkpáxwé lɛ hɛn',
	'info_sauvegarde_signatures' => 'Mi wlí àlɔɖówěmá kpó xójíjlá lɛ hɛn',
	'info_sauvegarde_sites_references' => 'Mi wlí gblogblojí yé ɖèkúnú ná  lɛ hɛn',
	'info_sauvegarde_type_documents' => 'Mi wlí wěmá àkpáɖé  lɛ hɛn',
	'info_sauvegarde_visites' => 'Mi jōnɔ bíbà  lɛ hɛn',
	'info_une_breve' => 'xó kléwún,',
	'item_mots_cles_association_breves' => 'xó kléwún',
	'item_nouvelle_breve' => 'xó kléwún',

	// L
	'lien_forum_public' => 'mi tò mɛ ɖò kplékplé mɛ ɔ lɛ',
	'lien_reponse_breve' => 'yígbjè nú xó kléwún é lɔ',

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_tech_03' => 'mi ná sɔ mɔ àlì gégé.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'mi ná wlíhɛn ɖò gbètá ɔ mɛ@fichier@.',
	'titre_nouvelle_breve' => 'xó kléwún lɛ',
	'titre_page_breves_edit' => 'ɖyɔ xó kléwún lɛ : « @titre@ »',
	'titre_page_forum' => 'tìtòtɔ lɛ sín kplékplé',
	'titre_page_forum_envoi' => 'sɛ wɛn dó',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
