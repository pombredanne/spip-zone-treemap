<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/grenier?lang_cible=cpf
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FOROM & FILSINYATIR', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'Sovgard lé koprésé èk @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'sovgard lé pa koprésé èk @fichier@',

	// F
	'forum_probleme_database' => 'Astèr néna inn larlik èk out bazdodné. Porézon sa minm, out modékri la pwinn finn dèt anrozistré.',

	// I
	'ical_lien_rss_breves' => 'Syndication des brèves du site', # NEW
	'icone_creer_mot_cle_breve' => 'Créer un nouveau mot-clé et le lier à cette brève', # NEW
	'icone_forum_administrateur' => 'Forom kozman bann komandèr',
	'icone_forum_suivi' => 'Swivi bann forom',
	'icone_publier_breve' => 'Pibliyé tit-nouvèl-la',
	'icone_refuser_breve' => 'Afront tit-nouvèl-la',
	'info_base_restauration' => 'Labaz lé antrin drosarz son promyé léta.',
	'info_breves_03' => 'Bann tit-nouvèl',
	'info_breves_liees_mot' => 'Bann tit-nouvèl an gatiraz èk molaklé-la ',
	'info_breves_touvees' => 'Bann tit-nouvèl ni la finn trouvé',
	'info_breves_touvees_dans_texte' => 'Bann tit-nouvèl ni la finn trouvé (dann tèks-la)',
	'info_echange_message' => 'Èk SPIP ou pé anvwayé bann modékri é fé zwé bann forom kozman
(forom privé) antrozot. Lé posib mèt ou dégrèn fonksyon-la kom sak ou vé.',
	'info_erreur_restauration' => 'Kanard dann rotour-sovgard labaz : lo fisyé-la i ékzist pa.',
	'info_forum_administrateur' => 'forom bann komandèr',
	'info_forum_interne' => 'forom-dodan',
	'info_forum_ouvert' => 'Dann léspas privé lo sit, inn forom lé ouver pou tout bann lotèr. Ou pé osi fé zwé inn ot forom sèlman pou bann komandèr. Anon vwar anba tèrla.',
	'info_gauche_suivi_forum' => 'Paz <i>swivi bann forom</i> lé inn zoutiy pou zèr an liyn out sit (pa inn léspas kozman o lékritir). Li afis tout bann kontribusyon dann forom-déor pou lartik-minm. Ou pé zèr tout banna dopwi lapazwèb-la.',
	'info_modifier_breve' => 'Sanz la tit-nouvèl :',
	'info_nombre_breves' => '@nb_breves@ bann tit-nouvèl,',
	'info_option_ne_pas_faire_suivre' => 'Anpés fé swiv bann modékri bann forom',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sovgard bann zartik',
	'info_sauvegarde_articles_sites_ref' => 'Sovgard bann zartik bann sit référansé',
	'info_sauvegarde_auteurs' => 'Sovgard bann lotèr',
	'info_sauvegarde_breves' => 'Sovgard bann tit-nouvèl',
	'info_sauvegarde_documents' => 'Sovgard bann dokiman',
	'info_sauvegarde_echouee' => 'Si sovgard-la la pa finn marsé(«Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Sovgard bann forom',
	'info_sauvegarde_groupe_mots' => 'Sovgard bann group bann molaklé',
	'info_sauvegarde_messages' => 'Sovgard bann modékri',
	'info_sauvegarde_mots_cles' => 'Sovgard bann molaklé',
	'info_sauvegarde_petitions' => 'Sovgard bann fil bann sinyatir',
	'info_sauvegarde_refers' => 'Sovgard bann référèr',
	'info_sauvegarde_reussi_01' => 'Sovgard la finn marsé.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sovgard bann rubrik',
	'info_sauvegarde_signatures' => 'Sovgard bann fil bann sinyatir',
	'info_sauvegarde_sites_references' => 'Sovgard bann sit référansé',
	'info_sauvegarde_type_documents' => 'Sovgard bann lespès-dokiman',
	'info_sauvegarde_visites' => 'Sovgard bann vizit',
	'info_une_breve' => 'inn tit-nouvèl,',
	'item_mots_cles_association_breves' => 'èk bann tit-nouvèl',
	'item_nouvelle_breve' => 'Nouvèl tit-nouvèl',

	// L
	'lien_forum_public' => 'Zèr lo forom piblik dann zartik-la',
	'lien_reponse_breve' => 'Réponn tit-nouvèl-la',

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_tech_03' => 'Ou pé swazi sovgard lo fisyé dann manyir-konpersé. Manyir-la i permé rand pli takini lo transfèr d-fisyé sir out lordinatèr oubyin sa lo servèr pou bann sovgard. Lé itil aou pour konsèrv out léspas-diks.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'Sovgard-la i sra pou fé sir lo fisyé konpèrsé @fichier@.',
	'titre_nouvelle_breve' => 'Nouvèl tit-nouvèl',
	'titre_page_breves_edit' => 'Sanz la tit-nouvèl : « @titre@ »',
	'titre_page_forum' => 'Forom bann komandèr',
	'titre_page_forum_envoi' => 'Anvwa inn modékri',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
