<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/grenier?lang_cible=bs
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'PETICIJA & FORUM', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'Snimiti sa kompresijom pod @fichier@', # MODIF
	'bouton_radio_sauvegarde_non_compressee' => 'Snimiti bez  kompresije pod @fichier@', # MODIF

	// F
	'forum_probleme_database' => 'Problem sa bazom podataka, Vasa poruka nije registrovana.',

	// I
	'ical_lien_rss_breves' => 'Syndication des brèves du site', # NEW
	'icone_creer_mot_cle_breve' => 'Créer un nouveau mot-clé et le lier à cette brève', # NEW
	'icone_forum_administrateur' => 'Forum administratora',
	'icone_forum_suivi' => 'Pracenje foruma',
	'icone_publier_breve' => 'Objavi ovu kratku poruku',
	'icone_refuser_breve' => 'Odbij ovu kratku poruku',
	'info_base_restauration' => 'Baza je u toku obnavljanja.',
	'info_breves_03' => 'kratke poruke',
	'info_breves_liees_mot' => 'Kratke poruke koje su vezane za ovu kljucnu rijec',
	'info_breves_touvees' => 'Pronadjene kratke poruke',
	'info_breves_touvees_dans_texte' => 'Pronadjene kratke poruke (u tekstu)',
	'info_echange_message' => 'SPIP dozvoljava razmjenu poruka izmedju ucesnika stranice i kreiranje privatnih foruma za diskusiju. Mozete ukljuciti ili iskljuciti ovu funkciju.',
	'info_erreur_restauration' => 'Greska u obnavljanju: nepostojeci dokument.',
	'info_forum_administrateur' => 'forum administratora',
	'info_forum_interne' => 'interni forum',
	'info_forum_ouvert' => 'Forum na privatnoj stranici je otvoren svim regristrovanim urednicima. Ispod mozete aktivirati dodatni forum, koji je rezervisan samo za administratore.',
	'info_gauche_suivi_forum' => 'Strana <i>pracenja foruma</i> je alatka za rukovodjene vasom stranicom (ne prostor za diskusiju  i redakciju). Ona izlistava sve doprinose javnog foruma ovog clanka i dozvoljava rukovodjenje tim doprinosima.', # MODIF
	'info_modifier_breve' => 'Izmijeni kratku poruku:',
	'info_nombre_breves' => '@nb_breves@ kratke poruke,',
	'info_option_ne_pas_faire_suivre' => 'Onemoguci prosljedjivanje poruka na forumima',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Backup clanaka',
	'info_sauvegarde_articles_sites_ref' => 'Backup clanaka preporucenih stranica',
	'info_sauvegarde_auteurs' => 'Backup autora',
	'info_sauvegarde_breves' => 'Backup kratkih poruka',
	'info_sauvegarde_documents' => 'Backup dokumenata',
	'info_sauvegarde_echouee' => 'Ako backup nije uspjeo(«Dostignuto je maksimalno vrijeme izvrsavanja»),',
	'info_sauvegarde_forums' => 'Backup forum\\tab ',
	'info_sauvegarde_groupe_mots' => 'Backup grupa rijeci',
	'info_sauvegarde_messages' => 'Backup poruka',
	'info_sauvegarde_mots_cles' => 'Backup kljucnih rijeci',
	'info_sauvegarde_petitions' => 'Backup peticija',
	'info_sauvegarde_refers' => 'Backup refera',
	'info_sauvegarde_reussi_01' => 'Backup uspijeo.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Backup rubrika',
	'info_sauvegarde_signatures' => 'Backup potpisa peticija',
	'info_sauvegarde_sites_references' => 'Backup preporucenih stranica',
	'info_sauvegarde_type_documents' => 'Backup tipova dokumenata',
	'info_sauvegarde_visites' => 'Backup posjeta',
	'info_une_breve' => 'jedna kratka poruka,',
	'item_mots_cles_association_breves' => 'za kratke poruke',
	'item_nouvelle_breve' => 'Nova kratka poruka',

	// L
	'lien_forum_public' => 'Uredi javni forum ovog clanka',
	'lien_reponse_breve' => 'Odgovori na kratku poruku',

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_tech_03' => 'Mozete izabrati pohranjivanje dokumenta u kompresovanoj formi, da bi ste skratili transfer kod vas ili nekog servera za pohranjivanje i da bi ste smanjili prostor na maticnoj ploci. ',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'Pohranjivanje ce se izvrsiti u nekompresovanom dokumentu @fichier@.', # MODIF
	'titre_nouvelle_breve' => 'Nova kratka poruka',
	'titre_page_breves_edit' => 'Izmijeni kratku poruku: «  @titre@ »',
	'titre_page_forum' => 'Forum administratora',
	'titre_page_forum_envoi' => 'Posalji poruku',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
