<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_radio_sauvegarde_compressee' => 'lagre komprimert i @fichier@', # MODIF
	'bouton_radio_sauvegarde_non_compressee' => 'lagre (ukomprimert) i @fichier@', # MODIF

	// I
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Ta sikkerhetskopi av artiklene',
	'info_sauvegarde_articles_sites_ref' => 'Ta sikkerhetskopi av refererte nettsteder',
	'info_sauvegarde_auteurs' => 'Ta sikkerhetskopi av forfatterne',
	'info_sauvegarde_breves' => 'Ta sikkerhetskopi av nyhetene',
	'info_sauvegarde_documents' => 'Ta sikkerhetskopi av dokumentene',
	'info_sauvegarde_echouee' => 'Hvis sikkerhetskopieringen feiler («Maksimal kjøretid er overskredet»),',
	'info_sauvegarde_forums' => 'Ta sikkerhetskopi av forumene',
	'info_sauvegarde_groupe_mots' => 'Ta sikkerhetskopi av nøkkelordgrupper',
	'info_sauvegarde_messages' => 'Ta sikkerhetskopi av meldinger',
	'info_sauvegarde_mots_cles' => 'Ta sikkerhetskopi av nøkkelordene',
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sikkerhetskopiering gjennomført.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Ta sikkerhetskopi av seksjonene',
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Ta sikkerhetskopi av dokumenttyper',
	'info_sauvegarde_visites' => 'Ta sikkerhetskopi av besøk',

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.' # NEW
);

?>
