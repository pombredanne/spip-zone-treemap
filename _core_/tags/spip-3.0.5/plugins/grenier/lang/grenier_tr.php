<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/grenier?lang_cible=tr
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORUM & İMZA KAMPANYASI', # MODIF
	'bouton_radio_sauvegarde_compressee' => '@fichier@ altında sıkıştırılmış olarak sakla',
	'bouton_radio_sauvegarde_non_compressee' => '<b>ecrire/data/dump.xml.gz</b> altında sıkıştırılmamış olarak sakla',

	// F
	'forum_probleme_database' => 'Veritabanı sorunu, mesajınız kaydedilemedi.',

	// I
	'ical_lien_rss_breves' => 'Sitenin kısa haberlerinin paylaşımı',
	'icone_creer_mot_cle_breve' => 'Yeni bir anahtar sözcük oluştur ve bu kısa habere bağla',
	'icone_forum_administrateur' => 'Yöneticiler forumu',
	'icone_forum_suivi' => 'Forum takibi',
	'icone_publier_breve' => 'Bu kısa haberi yayınla',
	'icone_refuser_breve' => 'Bu haberi reddet',
	'info_base_restauration' => 'Veritabanının geri yüklenmesi sürüyor.',
	'info_breves_03' => 'kısa haberle',
	'info_breves_liees_mot' => 'Bu anahtar sözcüğe bağlı kısa haberler',
	'info_breves_touvees' => 'Bulunan haberler',
	'info_breves_touvees_dans_texte' => '(metin içerisinde) bulunan haberler ',
	'info_echange_message' => 'SPIP site katılımcıları arasında ileti alışverişi
                           ve özel sohbet forumları oluşturma olanağı verir.
                           Bu işlevi çalıştırabilir veya durdurabilirsiniz.',
	'info_erreur_restauration' => 'Geri yüklemede hata: dosya bulunamadı.',
	'info_forum_administrateur' => 'Yöneticiler forumu',
	'info_forum_interne' => 'İç forum',
	'info_forum_ouvert' => 'Sitenin özel alanında, tüm kayıtlı yazarlara açık olan bir forum mevcuttur.
                        Aşağıda, sadece yöneticilere özel, 
                        fazladan bir forumu çalıştırabilirsiniz.',
	'info_gauche_suivi_forum' => '<i>Forumları izleme</i> sayfası sitenizi yönetme aracıdır (sohbet, ya da yazı yazma alanı değildir). Bir makaleye kamu forumundan yapılan tüm katkıları gösterir ve bu katkıları yönetmenizi sağlar.',
	'info_modifier_breve' => 'Kısa haberi değiştir :',
	'info_nombre_breves' => '@nb_breves@ kısa haber,',
	'info_option_ne_pas_faire_suivre' => 'Forum iletilerini gönderme',
	'info_restauration_sauvegarde_insert' => '@archive@ in veri tabanına eklenmesi',
	'info_sauvegarde_articles' => 'Makaleleri yedekle',
	'info_sauvegarde_articles_sites_ref' => 'Atıfta bulunulan sitelerin makalelerini yedekle',
	'info_sauvegarde_auteurs' => 'Yazarların yedekle',
	'info_sauvegarde_breves' => 'Kısa haberleri yedekle',
	'info_sauvegarde_documents' => 'Dokümanları yedekle',
	'info_sauvegarde_echouee' => 'Yedekleme başarısız ise  («Maksimum işletim süresi aşıldı»)',
	'info_sauvegarde_forums' => 'Forumları yedekle',
	'info_sauvegarde_groupe_mots' => 'Anahtar sözcük guruplarını yedekle',
	'info_sauvegarde_messages' => 'İletileri yedekle',
	'info_sauvegarde_mots_cles' => 'Anahtar sözcükleri yedekle',
	'info_sauvegarde_petitions' => 'Dilekçeleri yedekle',
	'info_sauvegarde_refers' => 'Atıfta bulunanları yedekle',
	'info_sauvegarde_reussi_01' => 'Yedekleme başarılı.',
	'info_sauvegarde_rubrique_reussi' => '@titre@ başlığına ait tablolar @archive@ e kaydedildi. Bunları',
	'info_sauvegarde_rubriques' => 'Bölümleri yedekle',
	'info_sauvegarde_signatures' => 'Dilekçe imzalarınn yedekle',
	'info_sauvegarde_sites_references' => 'Atıfta bulunan siteleri yedekle',
	'info_sauvegarde_type_documents' => 'Doküman tiplerini yedekle',
	'info_sauvegarde_visites' => 'Ziyaretleri yedekle',
	'info_une_breve' => 'bir kısa haber,',
	'item_mots_cles_association_breves' => 'Kısa haberlere ',
	'item_nouvelle_breve' => 'Yeni kısa haber',

	// L
	'lien_forum_public' => 'Bu makalenin kamu forumunu yönet',
	'lien_reponse_breve' => 'Bu kısa habere yanıt',

	// S
	'sauvegarde_fusionner' => 'Mevcut veritabanı ile yedeği birleştir',
	'sauvegarde_fusionner_depublier' => 'Birleştirilmiş nesneleri yayınlama',
	'sauvegarde_url_origine' => 'Kaynak sitenin URL\'si :',

	// T
	'texte_admin_tech_03' => 'Sizin sunucunuza ya da bir yedekleme sunucusuna transferini hızlandırmak,
    ve disk alanını boşa harcamamak için, isterseniz dosyayı sıkıştırılmış biçimde saklamayı seçebilirsiniz. ',
	'texte_admin_tech_04' => 'Bir başka veri tabanı ile birleştirme amacıyla yedeklemeyi şu başlıkla sınırlandırabilirsiniz: ',
	'texte_sauvegarde_compressee' => 'Yedekleme sıkıştırılmamış biçimde @fichier@ dosyasına yapılacaktır.',
	'titre_nouvelle_breve' => 'Yeni kısa haber',
	'titre_page_breves_edit' => 'Kısa haberi değiştir : « @titre@ »',
	'titre_page_forum' => 'Yöneticiler forumu ',
	'titre_page_forum_envoi' => 'İleti yolla',
	'titre_page_statistiques_messages_forum' => 'Forum  mesajları'
);

?>
