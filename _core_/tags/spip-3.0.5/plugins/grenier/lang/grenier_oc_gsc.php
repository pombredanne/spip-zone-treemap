<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/grenier?lang_cible=oc_gsc
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORUM E PETICION', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'Sauvagarda comprimida devath @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'Sauvagarda non comprimida devath @fichier@',

	// F
	'forum_probleme_database' => 'Problèma de basa de dadas, lo vòste messatge non s\'ei pas registrat.',

	// I
	'ical_lien_rss_breves' => 'Syndication des brèves du site', # NEW
	'icone_creer_mot_cle_breve' => 'Créer un nouveau mot-clé et le lier à cette brève', # NEW
	'icone_forum_administrateur' => 'Forum deus administrators',
	'icone_forum_suivi' => 'Seguit deus forums',
	'icone_publier_breve' => 'Publicar aquera brèva',
	'icone_refuser_breve' => 'Arrefusar aquera brèva',
	'info_base_restauration' => 'La basa qu\'ei en cors de restauracion.',
	'info_breves_03' => 'brèvas',
	'info_breves_liees_mot' => 'Las brèvas ligadas a aqueth mot clau',
	'info_breves_touvees' => 'Brèvas trobadas',
	'info_breves_touvees_dans_texte' => 'Brèvas trobadas (dens lo tèxt)',
	'info_echange_message' => 'SPIP que permet d\'escambiar messatges e de constituir forums privats de discussion entre los participants deu sit. Que podetz activar o desactivar aquera foncionalitat.',
	'info_erreur_restauration' => 'Error de restauracion: fichièr inexistent.',
	'info_forum_administrateur' => 'forum deus administrators',
	'info_forum_interne' => 'forum intèrne',
	'info_forum_ouvert' => 'Dens l\'espaci privat deu sit, un forum qu\'ei aubèrt a tots los redactors registrats. Que podetz, ça devath, activar un forum suplementari, reservat aus administrators sonque.',
	'info_gauche_suivi_forum' => 'La pagina de <i>seguit deus forums</i> qu\'ei un gatge de gestion deu vòste sit (mes n\'ei pas un espaci tà discutir o tà redigir). Qu\'aficha totas las contribucions deu forum public d\'aqueth article e que\'vs permet de gerir aqueras contribucions.',
	'info_modifier_breve' => 'Modificar la brèva:',
	'info_nombre_breves' => '@nb_breves@ brèvas, ',
	'info_option_ne_pas_faire_suivre' => 'Non har pas seguir los messatges deus forums',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvagardar los articles',
	'info_sauvegarde_articles_sites_ref' => 'Sauvagardar los articles deus sits referenciats',
	'info_sauvegarde_auteurs' => 'Sauvagardar los autors',
	'info_sauvegarde_breves' => 'Sauvagardar las brèvas',
	'info_sauvegarde_documents' => 'Sauvagardar los documents',
	'info_sauvegarde_echouee' => 'Se la sauvagarda s\'i ei mauescaduda(«Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Sauvagardar los forums',
	'info_sauvegarde_groupe_mots' => 'Sauvagardar los grops de mots',
	'info_sauvegarde_messages' => 'Sauvagardar los messatges',
	'info_sauvegarde_mots_cles' => 'Sauvagardar los mots clau',
	'info_sauvegarde_petitions' => 'Sauvagardar las peticions',
	'info_sauvegarde_refers' => 'Sauvagardar los referiders',
	'info_sauvegarde_reussi_01' => 'Sauvagarda escaduda.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvagardar las rubricas',
	'info_sauvegarde_signatures' => 'Sauvagardar las signaturas de peticions',
	'info_sauvegarde_sites_references' => 'Sauvagardar los sits referenciats',
	'info_sauvegarde_type_documents' => 'Sauvagardar los tipes de documents',
	'info_sauvegarde_visites' => 'Sauvagardar las vesitas',
	'info_une_breve' => 'ua brèva, ',
	'item_mots_cles_association_breves' => 'a las brèvas',
	'item_nouvelle_breve' => 'Brèva nava',

	// L
	'lien_forum_public' => 'Gerir lo forum public d\'aqueth article',
	'lien_reponse_breve' => 'Responsa a la brèva',

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_tech_03' => 'Que podetz causir de sauvagardar lo fichièr devath forma comprimida, entà
 accelerar lo son transferiment a vòste o a çò d\'un servider de sauvagardas, e entà estauviar espaci disc.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'La sauvagarda que\'s harà dens lo fichièr non comprimit @fichier@.',
	'titre_nouvelle_breve' => 'Brèva navèra',
	'titre_page_breves_edit' => 'Modificar la brèva: «@titre@»',
	'titre_page_forum' => 'Forum entaus administrators',
	'titre_page_forum_envoi' => 'Mandar un messatge',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
