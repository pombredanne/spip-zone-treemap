<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/grenier?lang_cible=zh
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => '论坛和请求', # MODIF
	'bouton_radio_sauvegarde_compressee' => '存为压缩文件@fichier@', # MODIF
	'bouton_radio_sauvegarde_non_compressee' => '存为不压缩的文件@fichier@', # MODIF

	// F
	'forum_probleme_database' => '数据库问题,您的消息未能保存.',

	// I
	'ical_lien_rss_breves' => 'Syndication des brèves du site', # NEW
	'icone_creer_mot_cle_breve' => 'Créer un nouveau mot-clé et le lier à cette brève', # NEW
	'icone_forum_administrateur' => '管理员论坛',
	'icone_forum_suivi' => '跟踪论坛',
	'icone_publier_breve' => '发表简要',
	'icone_refuser_breve' => '拒绝该简要',
	'info_base_restauration' => '正在修复数据库.',
	'info_breves_03' => '简要',
	'info_breves_liees_mot' => '与关键词关键的简要',
	'info_breves_touvees' => '找到的简要',
	'info_breves_touvees_dans_texte' => '(在文本中)找到的简要',
	'info_echange_message' => 'SPIP 允许在讨论者中交换消息和私有讨论论坛
  . 你可激活或
  使这个特性不可用.',
	'info_erreur_restauration' => '恢复失败: 文件未找到.',
	'info_forum_administrateur' => '管理者论坛',
	'info_forum_interne' => '内部论坛',
	'info_forum_ouvert' => '站点的私有区, 论坛对
  所有注册用户开放. 下面, 你可以激活一个为管理员
  保留的论坛.',
	'info_gauche_suivi_forum' => ' <i>论坛跟踪</i> 页是你站点的一个管理工具 (不是讨论或编辑区). 它显示这篇文章的所有论坛出版物并允许你管理这些出版物.', # MODIF
	'info_modifier_breve' => '修改新闻:',
	'info_nombre_breves' => '@nb_breves@ 新闻,',
	'info_option_ne_pas_faire_suivre' => '不要转寄论坛消息',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => '备份文章',
	'info_sauvegarde_articles_sites_ref' => '备份引用站点的文章',
	'info_sauvegarde_auteurs' => '备份作者',
	'info_sauvegarde_breves' => '备份新闻',
	'info_sauvegarde_documents' => '备份文档',
	'info_sauvegarde_echouee' => '备份失败(«超时»),',
	'info_sauvegarde_forums' => '备份论坛',
	'info_sauvegarde_groupe_mots' => '备份关键词组',
	'info_sauvegarde_messages' => '备份消息',
	'info_sauvegarde_mots_cles' => '备份关键词',
	'info_sauvegarde_petitions' => '备份请求',
	'info_sauvegarde_refers' => '备份参考',
	'info_sauvegarde_reussi_01' => '备份成功.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => '备份专栏',
	'info_sauvegarde_signatures' => '备份请求签名',
	'info_sauvegarde_sites_references' => '备份引用站点',
	'info_sauvegarde_type_documents' => '备份文档类型',
	'info_sauvegarde_visites' => '备份访问者',
	'info_une_breve' => '一条新闻,',
	'item_mots_cles_association_breves' => '新闻',
	'item_nouvelle_breve' => '新新闻',

	// L
	'lien_forum_public' => '管理文章的公共论坛',
	'lien_reponse_breve' => '回应新闻',

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_tech_03' => '你可以选择保存文件为压缩格式, to 
 为加速传输你的机器或你的服务器,保留磁盘空间.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => '备份将在未压缩文件 @fichier@.', # MODIF
	'titre_nouvelle_breve' => '新新闻',
	'titre_page_breves_edit' => '修改新闻: «@titre@»',
	'titre_page_forum' => '管理论坛',
	'titre_page_forum_envoi' => '发送消息',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
