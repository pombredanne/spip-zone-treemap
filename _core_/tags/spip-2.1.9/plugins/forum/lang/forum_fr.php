<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'aucun_message_forum' => 'Aucun message de forum',

	// I
	'icone_bruler_message' => "Signaler comme Spam",
	'icone_bruler_messages' => "Signaler comme Spam",
	'icone_legitimer_message' => "Signaler comme licite",
	'icone_supprimer_messages' => 'Supprimer ces messages',
	'icone_valider_messages' => 'Valider ces messages',
	'icone_valider_repondre_message' => 'Valider &amp; R&eacute;pondre &agrave; ce message',
	'info_liens_texte' => 'Lien(s) contenu(s) dans le texte du message',
	'info_liens_titre' => 'Lien(s) contenu(s) dans le titre du message',
	'interface_onglets' => 'Interface avec onglets',
	'interface_formulaire' => 'Interface formulaire',

	'info_1_message_forum' => '1 message de forum',
	'info_nb_messages_forum' => '@nb@ messages de forum',

	// M
	'messages_aucun' => 'Aucun',
	'messages_meme_auteur' => 'Tous les messages de cet auteur',
	'messages_meme_email' => 'Tous les messages de cet email',
	'messages_meme_ip' => 'Tous les messages de cette IP',
	'messages_off' => 'Supprim&eacute;s',
	'messages_perso' => 'Personnels',
	'messages_prive' => 'Priv&eacute;s',
	'messages_privoff' => 'Supprim&eacute;s',
	'messages_privrac' => 'G&eacute;n&eacute;raux',
	'messages_privadm' => 'Administrateurs',
	'messages_prop' => 'Propos&eacute;s',
	'messages_publie' => 'Publi&eacute;s',
	'messages_spam' => 'Spam',
	'messages_tous' => 'Tous',

	// S
	'statut_prop' => 'Propos&eacute;',
	'statut_publie' => 'Publi&eacute;',
	'statut_spam' => 'Spam',
	'statut_off' => 'Supprim&eacute;',

	// T
	'texte_en_cours_validation'=> 'Les articles, br&egrave;ves, forums ci dessous sont propos&eacute;s &agrave; la publication.',
	'tout_voir' => 'Voir tous les messages',
	'texte_messages_publics' => 'Messages publics sur&nbsp;:',

	// V
	'voir_messages_objet' => 'voir les messages'

);


?>