<?php
/**
 * Test unitaire de la fonction id_table_objet
 * du fichier ./base/connect_sql.php
 *
 * genere automatiquement par TestBuilder
 * le 2011-08-16 13:14
 */

	$test = 'id_table_objet';
	$remonte = "../";
	while (!is_dir($remonte."ecrire"))
		$remonte = "../$remonte";
	require $remonte.'tests/test.inc';
	find_in_path("./base/connect_sql.php",'',true);

	// chercher la fonction si elle n'existe pas
	if (!function_exists($f='id_table_objet')){
		find_in_path("inc/filtres.php",'',true);
		$f = chercher_filtre($f);
	}

	//
	// hop ! on y va
	//
	$err = tester_fun($f, essais_id_table_objet());
	
	// si le tableau $err est pas vide ca va pas
	if ($err) {
		die ('<dl>' . join('', $err) . '</dl>');
	}

	echo "OK";
	

	function essais_id_table_objet(){
		$essais = array (
  0 => 
  array (
    0 => 'id_article',
    1 => 'articles',
  ),
  1 => 
  array (
    0 => 'id_article',
    1 => 'article',
  ),
  2 => 
  array (
    0 => 'id_article',
    1 => 'spip_articles',
  ),
  3 => 
  array (
    0 => 'id_rubrique',
    1 => 'rubriques',
  ),
  4 => 
  array (
    0 => 'id_rubrique',
    1 => 'rubrique',
  ),
  5 => 
  array (
    0 => 'id_rubrique',
    1 => 'spip_rubriques',
  ),
  6 => 
  array (
    0 => 'id_breve',
    1 => 'breves',
  ),
  7 => 
  array (
    0 => 'id_breve',
    1 => 'breve',
  ),
  8 => 
  array (
    0 => 'id_breve',
    1 => 'spip_breves',
  ),
  9 => 
  array (
    0 => 'id_mot',
    1 => 'mots',
  ),
  10 => 
  array (
    0 => 'id_mot',
    1 => 'mot',
  ),
  11 => 
  array (
    0 => 'id_mot',
    1 => 'spip_mots',
  ),
  12 => 
  array (
    0 => 'id_groupe',
    1 => 'groupes_mots',
  ),
  13 => 
  array (
    0 => 'id_groupe',
    1 => 'groupes_mot',
  ),
  14 => 
  array (
    0 => 'id_groupe',
    1 => 'groupe_mot',
  ),
  15 => 
  array (
    0 => 'id_groupe',
    1 => 'spip_groupes_mots',
  ),
  16 => 
  array (
    0 => 'id_syndic',
    1 => 'syndic',
  ),
  17 => 
  array (
    0 => 'id_syndic',
    1 => 'site',
  ),
  18 => 
  array (
    0 => 'id_syndic',
    1 => 'spip_syndic',
  ),
  19 => 
  array (
    0 => 'id_syndic_article',
    1 => 'syndic_articles',
  ),
  20 => 
  array (
    0 => 'id_syndic_article',
    1 => 'syndic_article',
  ),
  21 => 
  array (
    0 => 'id_syndic_article',
    1 => 'spip_syndic_articles',
  ),
  22 => 
  array (
    0 => 'id_article',
    1 => 'petitions',
  ),
  23 => 
  array (
    0 => 'id_article',
    1 => 'petition',
  ),
  24 => 
  array (
    0 => 'id_article',
    1 => 'spip_petitions',
  ),
  25 => 
  array (
    0 => 'id_signature',
    1 => 'signatures',
  ),
  26 => 
  array (
    0 => 'id_signature',
    1 => 'signature',
  ),
  27 => 
  array (
    0 => 'id_signature',
    1 => 'spip_signatures',
  ),
  28 => 
  array (
    0 => 'id_auteur',
    1 => 'auteurs',
  ),
  29 => 
  array (
    0 => 'id_auteur',
    1 => 'auteur',
  ),
  30 => 
  array (
    0 => 'id_auteur',
    1 => 'spip_auteurs',
  ),
  31 => 
  array (
    0 => 'id_document',
    1 => 'documents',
  ),
  32 => 
  array (
    0 => 'id_document',
    1 => 'document',
  ),
  33 => 
  array (
    0 => 'id_document',
    1 => 'spip_documents',
  ),
  34 => 
  array (
    0 => 'id_document',
    1 => 'doc',
  ),
  35 => 
  array (
    0 => 'id_document',
    1 => 'img',
  ),
  36 => 
  array (
    0 => 'id_document',
    1 => 'emb',
  ),
  37 => 
  array (
    0 => 'id_forum',
    1 => 'forums',
  ),
  38 => 
  array (
    0 => 'id_forum',
    1 => 'forum',
  ),
  39 => 
  array (
    0 => 'id_forum',
    1 => 'spip_forums',
  ),
  40 => 
  array (
    0 => 'id_message',
    1 => 'messages',
  ),
  41 => 
  array (
    0 => 'id_message',
    1 => 'message',
  ),
  42 => 
  array (
    0 => 'id_message',
    1 => 'spip_messages',
  ),
);
		return $essais;
	}












































?>