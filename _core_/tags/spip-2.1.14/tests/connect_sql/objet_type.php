<?php
/**
 * Test unitaire de la fonction objet_type
 * du fichier ./base/connect_sql.php
 *
 * genere automatiquement par TestBuilder
 * le 2011-08-16 12:26
 */

	$test = 'objet_type';
	$remonte = "../";
	while (!is_dir($remonte."ecrire"))
		$remonte = "../$remonte";
	require $remonte.'tests/test.inc';
	find_in_path("./base/connect_sql.php",'',true);

	// chercher la fonction si elle n'existe pas
	if (!function_exists($f='objet_type')){
		find_in_path("inc/filtres.php",'',true);
		$f = chercher_filtre($f);
	}

	//
	// hop ! on y va
	//
	$err = tester_fun($f, essais_objet_type());
	
	// si le tableau $err est pas vide ca va pas
	if ($err) {
		die ('<dl>' . join('', $err) . '</dl>');
	}

	echo "OK";
	

	function essais_objet_type(){
		$essais = array (
  0 => 
  array (
    0 => 'article',
    1 => 'articles',
  ),
  1 => 
  array (
    0 => 'article',
    1 => 'article',
  ),
  2 => 
  array (
    0 => 'article',
    1 => 'spip_articles',
  ),
  3 => 
  array (
    0 => 'rubrique',
    1 => 'rubriques',
  ),
  4 => 
  array (
    0 => 'rubrique',
    1 => 'rubrique',
  ),
  5 => 
  array (
    0 => 'rubrique',
    1 => 'spip_rubriques',
  ),
  6 => 
  array (
    0 => 'breve',
    1 => 'breves',
  ),
  7 => 
  array (
    0 => 'breve',
    1 => 'breve',
  ),
  8 => 
  array (
    0 => 'breve',
    1 => 'spip_breves',
  ),
  9 => 
  array (
    0 => 'mot',
    1 => 'mots',
  ),
  10 => 
  array (
    0 => 'mot',
    1 => 'mot',
  ),
  11 => 
  array (
    0 => 'mot',
    1 => 'spip_mots',
  ),
  12 => 
  array (
    0 => 'syndic_article',
    1 => 'syndic_articles',
  ),
  13 => 
  array (
    0 => 'syndic_article',
    1 => 'spip_syndic_articles',
  ),
  14 => 
  array (
    0 => 'petition',
    1 => 'petitions',
  ),
  15 => 
  array (
    0 => 'petition',
    1 => 'petition',
  ),
  16 => 
  array (
    0 => 'petition',
    1 => 'spip_petitions',
  ),
  17 => 
  array (
    0 => 'signature',
    1 => 'signatures',
  ),
  18 => 
  array (
    0 => 'signature',
    1 => 'signature',
  ),
  19 => 
  array (
    0 => 'signature',
    1 => 'spip_signatures',
  ),
  20 => 
  array (
    0 => 'groupes_mot',
    1 => 'groupes_mots',
  ),
  21 => 
  array (
    0 => 'groupes_mot',
    1 => 'groupes_mot',
  ),
  22 => 
  array (
    0 => 'groupes_mot',
    1 => 'spip_groupes_mots',
  ),
  23 => 
  array (
    0 => 'site',
    1 => 'site',
  ),
  24 => 
  array (
    0 => 'syndic',
    1 => 'syndic',
  ),
  25 => 
  array (
    0 => 'syndic',
    1 => 'spip_syndic',
  ),
  26 => 
  array (
    0 => 'syndic_article',
    1 => 'syndic_article',
  ),
  27 => 
  array (
    0 => 'forum',
    1 => 'forum',
  ),
  28 => 
  array (
    0 => 'forum',
    1 => 'forums',
  ),
  29 => 
  array (
    0 => 'message',
    1 => 'messages',
  ),
  30 => 
  array (
    0 => 'message',
    1 => 'message',
  ),
  31 => 
  array (
    0 => 'message',
    1 => 'spip_messages',
  ),
  32 => 
  array (
    0 => 'document',
    1 => 'documents',
  ),
  33 => 
  array (
    0 => 'document',
    1 => 'document',
  ),
  34 => 
  array (
    0 => 'document',
    1 => 'spip_documents',
  ),
  35 => 
  array (
    0 => 'auteur',
    1 => 'auteurs',
  ),
  36 => 
  array (
    0 => 'auteur',
    1 => 'auteur',
  ),
  37 => 
  array (
    0 => 'auteur',
    1 => 'spip_auteurs',
  ),
);
		return $essais;
	}















































?>