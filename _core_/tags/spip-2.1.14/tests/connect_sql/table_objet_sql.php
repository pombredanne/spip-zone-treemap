<?php
/**
 * Test unitaire de la fonction table_objet_sql
 * du fichier ./base/connect_sql.php
 *
 * genere automatiquement par TestBuilder
 * le 2011-08-16 12:43
 */

	$test = 'table_objet_sql';
	$remonte = "../";
	while (!is_dir($remonte."ecrire"))
		$remonte = "../$remonte";
	require $remonte.'tests/test.inc';
	find_in_path("./base/connect_sql.php",'',true);

	// chercher la fonction si elle n'existe pas
	if (!function_exists($f='table_objet_sql')){
		find_in_path("inc/filtres.php",'',true);
		$f = chercher_filtre($f);
	}

	//
	// hop ! on y va
	//
	$err = tester_fun($f, essais_table_objet_sql());
	
	// si le tableau $err est pas vide ca va pas
	if ($err) {
		die ('<dl>' . join('', $err) . '</dl>');
	}

	echo "OK";
	

	function essais_table_objet_sql(){
		$essais = array (
  0 => 
  array (
    0 => 'spip_articles',
    1 => 'articles',
  ),
  1 => 
  array (
    0 => 'spip_articles',
    1 => 'article',
  ),
  2 => 
  array (
    0 => 'spip_articles',
    1 => 'spip_articles',
  ),
  3 => 
  array (
    0 => 'spip_rubriques',
    1 => 'rubriques',
  ),
  4 => 
  array (
    0 => 'spip_rubriques',
    1 => 'rubrique',
  ),
  5 => 
  array (
    0 => 'spip_rubriques',
    1 => 'spip_rubriques',
  ),
  6 => 
  array (
    0 => 'spip_breves',
    1 => 'breves',
  ),
  7 => 
  array (
    0 => 'spip_breves',
    1 => 'breve',
  ),
  8 => 
  array (
    0 => 'spip_breves',
    1 => 'spip_breves',
  ),
  9 => 
  array (
    0 => 'spip_mots',
    1 => 'mots',
  ),
  10 => 
  array (
    0 => 'spip_mots',
    1 => 'mot',
  ),
  11 => 
  array (
    0 => 'spip_mots',
    1 => 'spip_mots',
  ),
  12 => 
  array (
    0 => 'spip_groupes_mots',
    1 => 'groupe_mots',
  ),
  13 => 
  array (
    0 => 'spip_groupes_mots',
    1 => 'spip_groupes_mots',
  ),
  14 => 
  array (
    0 => 'spip_groupes_mots',
    1 => 'groupes_mot',
  ),
  15 => 
  array (
    0 => 'spip_syndic',
    1 => 'syndic',
  ),
  16 => 
  array (
    0 => 'spip_syndic',
    1 => 'site',
  ),
  17 => 
  array (
    0 => 'spip_syndic_articles',
    1 => 'syndic_articles',
  ),
  18 => 
  array (
    0 => 'spip_syndic_articles',
    1 => 'syndic_article',
  ),
  19 => 
  array (
    0 => 'spip_syndic_articles',
    1 => 'spip_syndic_articles',
  ),
  20 => 
  array (
    0 => 'spip_petitions',
    1 => 'petition',
  ),
  21 => 
  array (
    0 => 'spip_petitions',
    1 => 'petitions',
  ),
  22 => 
  array (
    0 => 'spip_petitions',
    1 => 'spip_petitions',
  ),
  23 => 
  array (
    0 => 'spip_signatures',
    1 => 'signature',
  ),
  24 => 
  array (
    0 => 'spip_signatures',
    1 => 'signatures',
  ),
  25 => 
  array (
    0 => 'spip_signatures',
    1 => 'spip_signatures',
  ),
  26 => 
  array (
    0 => 'spip_auteurs',
    1 => 'auteur',
  ),
  27 => 
  array (
    0 => 'spip_auteurs',
    1 => 'auteurs',
  ),
  28 => 
  array (
    0 => 'spip_auteurs',
    1 => 'spip_auteurs',
  ),
  29 => 
  array (
    0 => 'spip_breves',
    1 => 'breve',
  ),
  30 => 
  array (
    0 => 'spip_breves',
    1 => 'breves',
  ),
  31 => 
  array (
    0 => 'spip_breves',
    1 => 'spip_breves',
  ),
  32 => 
  array (
    0 => 'spip_documents',
    1 => 'document',
  ),
  33 => 
  array (
    0 => 'spip_documents',
    1 => 'documents',
  ),
  34 => 
  array (
    0 => 'spip_documents',
    1 => 'spip_documents',
  ),
  35 => 
  array (
    0 => 'spip_documents',
    1 => 'doc',
  ),
  36 => 
  array (
    0 => 'spip_documents',
    1 => 'img',
  ),
  37 => 
  array (
    0 => 'spip_documents',
    1 => 'emb',
  ),
  38 => 
  array (
    0 => 'spip_forum',
    1 => 'forum',
  ),
  39 => 
  array (
    0 => 'spip_forum',
    1 => 'forums',
  ),
  40 => 
  array (
    0 => 'spip_groupes_mots',
    1 => 'groupe_mot',
  ),
  41 => 
  array (
    0 => 'spip_groupes_mots',
    1 => 'groupe',
  ),
  42 => 
  array (
    0 => 'spip_messages',
    1 => 'message',
  ),
  43 => 
  array (
    0 => 'spip_messages',
    1 => 'messages',
  ),
  44 => 
  array (
    0 => 'spip_messages',
    1 => 'spip_messages',
  ),
);
		return $essais;
	}




























































?>