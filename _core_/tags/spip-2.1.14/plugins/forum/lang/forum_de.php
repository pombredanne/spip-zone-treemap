<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_core_/branches/spip-2.1/plugins/forum/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'aucun_message_forum' => 'Kein Beitrag',
	
	// I
	'icone_bruler_message' => "Als SPAM markieren",
	'icone_bruler_messages' => "Als SPAM markieren",
	'icone_legitimer_message' => "Als erw&uuml;nscht markieren",
	'icone_supprimer_messages' => 'Diese Beitr&auml;ge l&ouml;schen',
	'icone_valider_repondre_message' => 'Beantworten dieses Beitrags best&auml;tigen',
	'icone_valider_messages' => 'Diese Beitr&auml;ge freischalten',
	'info_1_message_forum' => '1 Beitrag im Forum',
	'info_nb_messages_forum' => '@nb@ Beitr&auml;ge im Forum',
	'interface_onglets' => 'Interface mit Reitern',
	'interface_formulaire' => 'Interface als Formular',
	
	// M
	'messages_meme_auteur' => 'Alle Beitr&auml;ge dieses Autors',
	'messages_meme_email' => 'Alle Beitr&auml;ge mit dieser E-Mail',
	'messages_meme_ip' => 'Alle Beitr&auml;ge von dieser IP',
	
	// statuts
	'messages_aucun' => 'Kein',
	'messages_off' => 'Gel&ouml;scht',
	'messages_perso' => 'Pers&ouml;nlich',
	'messages_prive' => 'Privat',
	'messages_privoff' => 'Gel&ouml;scht',
	'messages_privrac' => 'Allgemein',
	'messages_privadm' => 'Administratoren',
	'messages_prop' => 'Vorgeschlagen',
	'messages_publie' => 'Ver&ouml;ffentlicht',
	'messages_spam' => 'Spam',
	'messages_tous' => 'Alle',
	
	// S
	'statut_off' => 'Gel&ouml;scht',
	'statut_prop' => 'Vorgeschlagen',
	'statut_publie' => 'Ver&ouml;ffentlicht',
	'statut_spam' => 'Spam',
	
	// T 
	'texte_en_cours_validation'=> 'Diese Artikel, Meldungen und Forumsbeitr&auml;ge sind zur Ver&ouml;ffentlichung vorgeschlagen',
	'tout_voir' => 'Alle Beitr&auml;ge anzeigen',
);


?>