<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/grenier?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FÓRUM E PETIÇÃO',
	'bouton_radio_sauvegarde_compressee' => 'Cópia de segurança compactadas em @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'Cópia de segurança não compactada em @fichier@',

	// F
	'forum_probleme_database' => 'Problema de base da dados: sua mensagem não foi cadastrada.',

	// I
	'ical_lien_rss_breves' => 'Sindicação das notas do site',
	'icone_creer_mot_cle_breve' => 'Criar uma nova palavra-chave e associá-la a esta nota',
	'icone_forum_administrateur' => 'Fórum dos administradores',
	'icone_forum_suivi' => 'Acompanhamento dos fóruns',
	'icone_publier_breve' => 'Publicar esta nota',
	'icone_refuser_breve' => 'Recusar esta nota',
	'info_base_restauration' => 'A base está sendo restaurada.',
	'info_breves_03' => 'notas',
	'info_breves_liees_mot' => 'As notas associadas a esta palavra-chave',
	'info_breves_touvees' => 'Notas encontradas',
	'info_breves_touvees_dans_texte' => 'Notas encontradas (no texto)',
	'info_echange_message' => 'O SPIP permite a troca de mensagens e a formação de fóruns de discussão privados entre os participantes do site. Você pode ativar ou desativar esta funcionalidade.',
	'info_erreur_restauration' => 'Erro de restauração: arquivo inexistente.',
	'info_forum_administrateur' => 'fórum dos administradores',
	'info_forum_interne' => 'fórum interno',
	'info_forum_ouvert' => 'Na área privada do site, há um fórum aberto à participação de todos os redatores cadastrados. Abaixo, você pode ativar um fórum suplementar, reservado apenas aos administradores.',
	'info_gauche_suivi_forum' => 'A página de <i>acompanhamento dos fóruns</i> é uma ferramenta de gestão do seu site (e não um espaço de discussão ou de redação). Ela exibe todas as contrubuições do fórum público desta matéria e permite-lhe gerenciar suas contribuições.',
	'info_modifier_breve' => 'Editar a nota:',
	'info_nombre_breves' => '@nb_breves@ notas,',
	'info_option_ne_pas_faire_suivre' => 'Não encaminhar as mensagens dos fóruns',
	'info_restauration_sauvegarde_insert' => 'Inserção de @archive@ na base',
	'info_sauvegarde_articles' => 'Fazer cópia de segurança das matérias',
	'info_sauvegarde_articles_sites_ref' => 'Fazer cópia de segurança das matérias dos sites referenciados',
	'info_sauvegarde_auteurs' => 'Fazer cópia de segurança dos autores',
	'info_sauvegarde_breves' => 'Fazer cópia de segurança das notas',
	'info_sauvegarde_documents' => 'Fazer cópia de segurança dos documentos',
	'info_sauvegarde_echouee' => 'Se a cópia de segurança falhou («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Fazer cópia de segurança dos fóruns',
	'info_sauvegarde_groupe_mots' => 'Fazer cópia de segurança dos grupos de palavras-chave',
	'info_sauvegarde_messages' => 'Fazer cópia de segurança das mensagens',
	'info_sauvegarde_mots_cles' => 'Fazer cópia de segurança das palavras-chave',
	'info_sauvegarde_petitions' => 'Fazer cópia de segurança das petições',
	'info_sauvegarde_refers' => 'Fazer cópia de segurança dos referenciadores',
	'info_sauvegarde_reussi_01' => 'Cópia de segurança completada com sucesso.',
	'info_sauvegarde_rubrique_reussi' => 'As tabelas da seção @titre@ foram copiadas em @archive@. Você pode',
	'info_sauvegarde_rubriques' => 'Fazer cópia de segurança das seções',
	'info_sauvegarde_signatures' => 'Fazer cópia de segurança das assinaturas das petições',
	'info_sauvegarde_sites_references' => 'Fazer cópia de segurança dos sites referenciados',
	'info_sauvegarde_type_documents' => 'Fazer cópia de segurança dos tipos de documentos',
	'info_sauvegarde_visites' => 'Fazer cópia de segurança das visitas',
	'info_une_breve' => 'uma nota,',
	'item_mots_cles_association_breves' => 'às notas',
	'item_nouvelle_breve' => 'Nova nota',

	// L
	'lien_forum_public' => 'Gerenciar o fórum público desta matéria',
	'lien_reponse_breve' => 'Resposta à nota',

	// S
	'sauvegarde_fusionner' => 'Combinar a base atual e a cópia de segurança',
	'sauvegarde_fusionner_depublier' => 'Despublicar os objetos fundidos',
	'sauvegarde_url_origine' => 'Eventualmente, URL do site de origem:',

	// T
	'texte_admin_tech_03' => 'Você pode escolher fazer a cópia de segurança em formato compactado, para encurtar a sua transferência para a sua máquina local, ou para um servidor de cópias de segurança, e para economizar espaço em disco.',
	'texte_admin_tech_04' => 'Com o objetivo de fusão com outra base de dados, você pode limitar a cópia de segurança à seção: ',
	'texte_sauvegarde_compressee' => 'A cópia de segurança da base será feita no arquivo não compactado @fichier@.',
	'titre_nouvelle_breve' => 'Nova nota',
	'titre_page_breves_edit' => 'Editar a nota: &laquo;@titre@&raquo;',
	'titre_page_forum' => 'Fórum dos administradore',
	'titre_page_forum_envoi' => 'Enviar uma mensagem',
	'titre_page_statistiques_messages_forum' => 'Mensagens de fórum'
);

?>
