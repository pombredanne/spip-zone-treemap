<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/grenier?lang_cible=da
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORUM & APPELLER', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'gem komprimeret i @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'gem ukomprimeret i @fichier@',

	// F
	'forum_probleme_database' => 'Databaseproblem, dit indlæg er ikke modtaget.',

	// I
	'ical_lien_rss_breves' => 'Syndication des brèves du site', # NEW
	'icone_creer_mot_cle_breve' => 'Créer un nouveau mot-clé et le lier à cette brève', # NEW
	'icone_forum_administrateur' => 'Administratorforum',
	'icone_forum_suivi' => 'Forumopfølgning',
	'icone_publier_breve' => 'Offentliggør nyhed',
	'icone_refuser_breve' => 'Afvis nyhed',
	'info_base_restauration' => 'Genoprettelse af databasen er i gang.',
	'info_breves_03' => 'nyheder',
	'info_breves_liees_mot' => 'Nyheder knyttet til dette nøgleord',
	'info_breves_touvees' => 'Fundne nyheder',
	'info_breves_touvees_dans_texte' => 'Nyheder fundet (i teksten)',
	'info_echange_message' => 'SPIP tillader udveksling af meddelelser og oprettelse af private diskussionsforummer mellem deltagere på webstedet. Du kan til- eller fravælge denne funktion.',
	'info_erreur_restauration' => 'Fejl under genopretning: fil findes ikke.',
	'info_forum_administrateur' => 'administratorforum',
	'info_forum_interne' => 'internt forum',
	'info_forum_ouvert' => 'I det private afsnit af webstedet er der et forum åbent for alle registrerede redaktører. Nedenfor kan du åbne et ekstra forum alene for administratorer.',
	'info_gauche_suivi_forum' => '<i>Forumopfølgning</i> er et administrationsværktøj (ikke et diskussions- eller redigeringsområde). Det viser alle indlæg i det offentlige forum knyttet til en bestemt artikel og giver dig mulighed for at administrere indlæggene.',
	'info_modifier_breve' => 'Ret nyhed:',
	'info_nombre_breves' => '@nb_breves@ nyheder,',
	'info_option_ne_pas_faire_suivre' => 'Videresend ikke meddelelser i forummer',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sikkerhedskopi af artikler',
	'info_sauvegarde_articles_sites_ref' => 'Sikkerhedskopi af henvisninger til websteder',
	'info_sauvegarde_auteurs' => 'Sikkerhedskopi af forfattere',
	'info_sauvegarde_breves' => 'Sikkerhedskopi af nyheder',
	'info_sauvegarde_documents' => 'Sikkerhedskopi af dokumenter',
	'info_sauvegarde_echouee' => 'Hvis sikkerhedskopiering mislykkes («Max. eksekveringstid overskredet»),',
	'info_sauvegarde_forums' => 'Sikkerhedskopi af forummer',
	'info_sauvegarde_groupe_mots' => 'Sikkerhedskopi af nøgleordsgrupper',
	'info_sauvegarde_messages' => 'Sikkerhedskopi af meddelelser',
	'info_sauvegarde_mots_cles' => 'Sikkerhedskopi af nøgleord',
	'info_sauvegarde_petitions' => 'Sikkerhedskopi af appeller',
	'info_sauvegarde_refers' => 'Sikkerhedskopi af henvisende sider',
	'info_sauvegarde_reussi_01' => 'Sikkerhedskopiering gennemført.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sikkerhedskopi af afsnit',
	'info_sauvegarde_signatures' => 'Sikkerhedskopi af underskrifter',
	'info_sauvegarde_sites_references' => 'Sikkerhedskopi af links til websteder',
	'info_sauvegarde_type_documents' => 'Sikkerhedskopi af dokumenttyper',
	'info_sauvegarde_visites' => 'Sikkerhedskopi af besøg',
	'info_une_breve' => 'en nyhed,',
	'item_mots_cles_association_breves' => 'nyhederne',
	'item_nouvelle_breve' => 'Ny nyhed',

	// L
	'lien_forum_public' => 'Vedligehold denne artikels offentlige forum',
	'lien_reponse_breve' => 'Kommenter denne nyhed',

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_tech_03' => 'Du kan vælge at gemme filen i komprimeret form for hurtigere at kunne overføre den til din maskine eller tage en sikkerhedskopi af serveren og spare diskplads.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'Sikkerhedskopien vil blive lagt i den ukomprimerede fil @fichier@.',
	'titre_nouvelle_breve' => 'Ny nyhed',
	'titre_page_breves_edit' => 'Ret nyhed: «@titre@»',
	'titre_page_forum' => 'Administratorforum',
	'titre_page_forum_envoi' => 'Send meddelelse',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
