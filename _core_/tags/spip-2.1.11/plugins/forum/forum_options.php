<?php

/**
 * Declaration du pipeline forum_objets_depuis_env
 * Correspond a un array objet/cles primaires des tables qui acceptent
 * qu'un forum recherche dans l'environnement d'appel l'identifiant sur lequel s'appliquera
 * le forum, de sorte que son appel ne se trouve pas necessairement
 * dans une boucle englobante
 */
$GLOBALS['spip_pipeline']['forum_objets_depuis_env'] = '';

?>
