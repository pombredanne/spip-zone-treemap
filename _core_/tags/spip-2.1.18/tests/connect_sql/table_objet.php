<?php
/**
 * Test unitaire de la fonction table_objet
 * du fichier ./base/connect_sql.php
 *
 * genere automatiquement par TestBuilder
 * le 2011-08-16 12:22
 */

	$test = 'table_objet';
	$remonte = "../";
	while (!is_dir($remonte."ecrire"))
		$remonte = "../$remonte";
	require $remonte.'tests/test.inc';
	find_in_path("./base/connect_sql.php",'',true);

	// chercher la fonction si elle n'existe pas
	if (!function_exists($f='table_objet')){
		find_in_path("inc/filtres.php",'',true);
		$f = chercher_filtre($f);
	}

	//
	// hop ! on y va
	//
	$err = tester_fun($f, essais_table_objet());
	
	// si le tableau $err est pas vide ca va pas
	if ($err) {
		die ('<dl>' . join('', $err) . '</dl>');
	}

	echo "OK";
	

	function essais_table_objet(){
		$essais = array (
  0 => 
  array (
    0 => 'articles',
    1 => 'article',
  ),
  1 => 
  array (
    0 => 'auteurs',
    1 => 'auteur',
  ),
  2 => 
  array (
    0 => 'breves',
    1 => 'breve',
  ),
  3 => 
  array (
    0 => 'documents',
    1 => 'document',
  ),
  4 => 
  array (
    0 => 'documents',
    1 => 'doc',
  ),
  5 => 
  array (
    0 => 'documents',
    1 => 'img',
  ),
  6 => 
  array (
    0 => 'documents',
    1 => 'emb',
  ),
  7 => 
  array (
    0 => 'groupes_mots',
    1 => 'groupe_mots',
  ),
  8 => 
  array (
    0 => 'groupes_mots',
    1 => 'groupe_mot',
  ),
  9 => 
  array (
    0 => 'groupes_mots',
    1 => 'groupe',
  ),
  10 => 
  array (
    0 => 'messages',
    1 => 'message',
  ),
  11 => 
  array (
    0 => 'mots',
    1 => 'mot',
  ),
  12 => 
  array (
    0 => 'petitions',
    1 => 'petition',
  ),
  13 => 
  array (
    0 => 'rubriques',
    1 => 'rubrique',
  ),
  14 => 
  array (
    0 => 'signatures',
    1 => 'signature',
  ),
  15 => 
  array (
    0 => 'syndic',
    1 => 'syndic',
  ),
  16 => 
  array (
    0 => 'syndic',
    1 => 'site',
  ),
  17 => 
  array (
    0 => 'syndic_articles',
    1 => 'syndic_article',
  ),
  18 => 
  array (
    0 => 'types_documents',
    1 => 'type_document',
  ),
  19 => 
  array (
    0 => 'types_documents',
    1 => 'extension',
  ),
);
		return $essais;
	}

























?>