<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-themes
// Langue: fr
// Date: 30-07-2011 15:01:38
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// T
	'themes_description' => 'Des thèmes pour l\'espace privé',
	'themes_slogan' => 'Des thèmes pour l\'espace privé',
);
?>