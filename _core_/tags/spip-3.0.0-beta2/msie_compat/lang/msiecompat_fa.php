<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'choix_explication' => '<p>اين پيكربندي انطباق‌پذيري سايت همگاني را با اينترنت اكسپلورر بهبود مي‌دهد.</p>
   <ul>
    <li><a href=\'http://jquery.khurshid.com/ifixpng.php\'>iFixPng</a> (<b>default setting</b>)فقدان شفافيت تصاوير در ماكرو سافت اينترنت اكسپلورر 5 و 6 را بهبود مي‌دهد.</li>
<li><a href=\'http://code.google.com/p/ie7-js/\'>IE7.js</a>شفافيت پي.ان.چي را تصحيح مي‌كند و سلكتور‌هاي سي.اس.اس 2 براي اينترنت اكسپلورر 5 و 6 مي‌افزايد.  (<a href=\'http://ie7-js.googlecode.com/svn/test/index.html\'>در اينجا فهرستي از سلكتور‌هاي انطباق پذير كه توس آي.اي 7 جاوااسكريپت و آي.اي 8 جاوا اسكريپت ارايه شده وجود دارد. </li>
 <li>IE8.js  بهبود مي‌بخشدIE7.js with CSS  سلكتورهاي MSIE 5 to 7.</li>
<li>IE7-squish fixes three bugs in MSIE 6 (including the double margin on floating elements), but side effects may appear. </li>
</ul>', # MODIF
	'choix_ie7' => '<a href=\'http://code.google.com/p/ie7-js/\'>IE7.js</a> corrige les images PNG et ajoute des sélecteurs CSS2 pour MSIE 5 et 6 (<a href=\'http://ie7-js.googlecode.com/svn/test/index.html\'>vous pouvez consulter la liste des sélecteurs compatibles introduits par IE7.js et IE8.js</a>).', # NEW
	'choix_ie7squish' => 'IE7-squish corrige trois bugs de MSIE 6 (notamment la double marge des éléments flottants), mais des effets indésirables peuvent apparaître (le webmestre doit vérifier la compatibilité).', # NEW
	'choix_ie8' => 'IE8.js complète IE7.js en enrichissant les comportements des CSS de MSIE 5 à 7.', # NEW
	'choix_ifixpng' => 'Choix par défaut, <a href=\'http://jquery.khurshid.com/ifixpng.php\'>iFixPng</a> rétablit la semi-transparence les images au format PNG sous MSIE 5 et 6.', # NEW
	'choix_non' => 'غيرفعال: چيزي به اسكلت‌هاي من اضافه نكن',
	'choix_titre' => 'ماكروسافت اينترنت اكسپلورر كامپاتيبليتي (انطباق‌پذيري)'
);

?>
