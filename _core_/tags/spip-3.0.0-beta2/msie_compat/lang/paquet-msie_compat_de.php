<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-msie_compat
// Langue: de
// Date: 03-08-2011 19:45:21
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// M
	'msie_compat_description' => 'Bietet verschiedene Methoden für:

-* transparente PNG Dateien unter MSIE~6 

-* und /oder diverse CSS Selektoren für MSIE~6 und~7.',
	'msie_compat_nom' => 'Unterstützung älterer Webbrowser',
);
?>