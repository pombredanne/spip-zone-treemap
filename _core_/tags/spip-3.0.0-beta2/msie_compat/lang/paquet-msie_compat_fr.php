<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-msie_compat
// Langue: fr
// Date: 03-08-2011 19:45:21
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// M
	'msie_compat_description' => 'Permet de sélectionner différentes méthodes 

-* pour assurer l\'affichage des fichiers PNG transparents sous MSIE~6 

-* et/ou activer certains sélecteurs CSS dans MSIE~6 et~7.',
	'msie_compat_nom' => 'Support vieux navigateurs',
	'msie_compat_slogan' => 'PNG et sélecteurs CSS pour les vieux navigateurs',
);
?>