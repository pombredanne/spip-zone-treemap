<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'choix_explication' => '<p>يتيح لك هذا الإعداد تحسين توافق الموقع العمومي مع متصفح إنترنت إكسبلورر.</p>
   <ul>
li><a href=\'http://jquery.khurshid.com/ifixpng.php\'>iFixPng</a> (<b>إفتراضياً</b>) يسترجع الشفافية الجزئية للصور ذات تنسيق PNG في الإصدارين الخامس والسادس من إنترنت إكسبلورر.</li>
<li><a href=\'http://code.google.com/p/ie7-js/\'>IE7.js</a> يصحح صور PNG ويضيف أنماط CSS للإصدارين الخامس والسادس من إنترنت إكسبلورر (<a href=\'http://ie7-js.googlecode.com/svn/test/index.html\'> يمكن الرجوع الى قائمة أنماط CSS المتوافقة التي أدخلها IE7.js وIE8.js</a>).</li>
 <li>يكمل IE8.js الإصدار السابع بإغناء تصرف CSS  للإصدارات خمسة الى سبعة من إنترنت إكسبلورر.</li>
 <li>يصحح IE7-squish ثلاثة أخطاء في الإصدار السادس من إنترنت إكسبلورر (خاصة الهامش المزدوج للعناصر العائمة) لكنه قد يتسبب ببعض المؤثرات غير المرغوب فيها (يجب على المصمم ان يتأكد من التوافقية).</li>
   </ul> ', # MODIF
	'choix_ie7' => '<a href=\'http://code.google.com/p/ie7-js/\'>IE7.js</a> corrige les images PNG et ajoute des sélecteurs CSS2 pour MSIE 5 et 6 (<a href=\'http://ie7-js.googlecode.com/svn/test/index.html\'>vous pouvez consulter la liste des sélecteurs compatibles introduits par IE7.js et IE8.js</a>).', # NEW
	'choix_ie7squish' => 'IE7-squish corrige trois bugs de MSIE 6 (notamment la double marge des éléments flottants), mais des effets indésirables peuvent apparaître (le webmestre doit vérifier la compatibilité).', # NEW
	'choix_ie8' => 'IE8.js complète IE7.js en enrichissant les comportements des CSS de MSIE 5 à 7.', # NEW
	'choix_ifixpng' => 'Choix par défaut, <a href=\'http://jquery.khurshid.com/ifixpng.php\'>iFixPng</a> rétablit la semi-transparence les images au format PNG sous MSIE 5 et 6.', # NEW
	'choix_non' => 'عدم التفعيل: عدم إضافة اي شيء على الصفحات النموذجية',
	'choix_titre' => 'التوافق مع مايكروسوفت انترنت اكسبلورر'
);

?>
