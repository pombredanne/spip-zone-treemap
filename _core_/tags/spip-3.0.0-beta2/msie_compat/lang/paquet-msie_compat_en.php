<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-msie_compat
// Langue: en
// Date: 30-07-2011 15:01:37
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// M
	'msie_compat_description' => 'Offers different javascript methods to add support for:
-* transparent PNG files to MSIE~6 
-* and/or various CSS selectors to MSIE~6 and~7.',
	'msie_compat_slogan' => 'PNG files and various CSS selectors for old browsers',
);
?>