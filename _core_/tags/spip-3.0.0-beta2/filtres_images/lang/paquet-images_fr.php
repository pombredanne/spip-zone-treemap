<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-images
// Langue: fr
// Date: 30-07-2011 15:01:37
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// I
	'images_description' => 'Filtres de transformation d\'image et de couleurs',
	'images_slogan' => 'Filtres de transformation d\'image et de couleurs',
);
?>