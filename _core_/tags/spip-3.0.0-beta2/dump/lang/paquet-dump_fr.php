<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-dump
// Langue: fr
// Date: 30-07-2011 15:01:37
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// D
	'dump_description' => 'Sauvegarde de la base en SQLite et restauration',
	'dump_slogan' => 'Sauvegarde et restauration de la base SPIP',
);
?>