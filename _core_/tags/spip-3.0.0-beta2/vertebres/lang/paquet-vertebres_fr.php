<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-vertebres
// Langue: fr
// Date: 03-08-2011 19:45:21
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// V
	'vertebres_description' => 'Vertebres propose un moyen de lire une table SQL en indiquant
	comme argument de page la table à lire, précédé
	de &quot;table:&quot;, par exemple : <code>ecrire/?exec=vertebres&table=spip_articles</code>',
	'vertebres_nom' => 'Vertèbres',
	'vertebres_slogan' => 'Lecteur de tables SQL',
);
?>