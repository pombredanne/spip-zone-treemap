<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-vertebres
// Langue: de
// Date: 03-08-2011 19:45:21
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// V
	'vertebres_description' => 'Wirbel ermöglichen SQL Tabellen zu lesen, indem einer Seite der Parameter &quot;table:&quot; vorangestellt wird: <code>spip.php?page=table:articles</code>',
	'vertebres_nom' => 'Wirbel',
	'vertebres_slogan' => 'SQL Tabellen lesen',
);
?>