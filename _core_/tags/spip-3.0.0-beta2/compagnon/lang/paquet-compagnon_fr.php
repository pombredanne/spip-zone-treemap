<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'compagnon_nom' => "Compagnon",
	'compagnon_slogan' => "Assistant de premiers pas avec SPIP",
	'compagnon_description' => "Le compagnon offre une aide aux utilisateurs
		lors de leur première visite dans l'espace privé de SPIP.",
);

?>
