<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'c_accueil_bienvenue' => "Bienvenue @nom@ !",
	'c_accueil_texte' => "Vous venez d'entrer dans l'espace privé de SPIP.",
	'c_accueil_texte_revenir' => "
		Cette page présente l'activité éditoriale récente sur votre site.
		Vous pouvez revenir sur cette page à tout instant en cliquant l'icone de maison,
		sur la partie haute, sous votre nom.",
	'c_accueil_configurer_site' => "Configurer votre site",
	'c_accueil_configurer_site_texte' => "Une des premières choses à faire est de donner un nom à votre site.
		Il se nomme actuellement &laquo;&nbsp;@nom@&nbsp;&raquo;. Le nom du site est affiché tout en haut de cette page.
		En cliquant dessus, vous pourrez modifier son nom, mais aussi lui donner un logo et un slogan.",

	'c_accueil_publication' => "Publiez !",
	'c_accueil_publication_texte' => "Pour publier une page, il vous faudra créer un article.
		Il est nécessaire pour cela de créer au moins une rubrique. Vous pouvez le faire dans le menu &laquo;&nbsp;Édition&nbsp;&raquo; en cliquant &laquo;&nbsp;Rubriques&nbsp;&raquo;.",

	'c_article_redaction' => "L'article est en cours de rédaction",
	'c_article_redaction_texte' => "Pour publier le contenu de cet article sur le site public, il faut changer son statut.
		Dans le cadre sur le côté, changez &laquo;&nbsp;en cours de rédaction&nbsp;&raquo; par &laquo;&nbsp;publié en ligne&nbsp;&raquo;.",

	'c_article_redaction_redacteur' => "L'article est en cours de rédaction",
	'c_article_redaction_redacteur_texte' => "Pour proposer votre article aux administrateurs du site et aux autres rédacteurs,
		dans le cadre sur le côté, changez &laquo;&nbsp;en cours de rédaction&nbsp;&raquo; par &laquo;&nbsp;proposé à la publication&nbsp;&raquo;.",

	'c_articles_creer' => "Comment créer un article ?",
	'c_articles_creer_texte' => "Vous ne pourrez créer un article depuis cette page
		qu'à partir du moment où il existe une rubrique dans votre site.
		Vous pouvez la créer depuis le menu &laquo;&nbsp;Édition&nbsp;&raquo;, puis &laquo;&nbsp;Rubriques&nbsp;&raquo;.",

	'c_job' => "Les travaux à faire…",
	'c_job_texte' => "Cette page liste les prochaines tâches de maintenance
		que doit effectuéer SPIP. Ces tâches sont exécutées à interval régulier
		ou ponctuellement pour des traitements lourds demandés par des plugins,
		tel que l'envoi d'email en masse.",
	
	'c_rubriques_creer' => "Creez une première rubrique !",
	'c_rubriques_creer_texte' => "Les rubriques sont la structure de base du site.
		Dedans, vous pourrez y créer des articles.
		Commencez par créer une première rubrique.",
		
	'c_rubrique_publier' => "Creez un article",
	'c_rubrique_publier_texte' => "Une rubrique n'est visible sur le site public
		qu'à partir du moment où elle contient au moins un contenu publié dedans. Par exemple, un article publié.
		Créez donc un article. Vous pouvez le faire depuis cette page, sous le descriptif de votre rubrique.",



	// E
	'explication_activer_compagnon' => "Le compagnon ajoute des commentaires sur certaines pages
			de l'espace privé pour aider à prendre en main SPIP. Souhaitez-vous l'activer ?",
	'explication_reinitialiser_compagnon' => "Les messages déjà vus par un auteur ne sont plus affichés ensuite.
			Souhaitez-vous réinitialiser ces messages ?",
	
	// L
	'label_activer_compagnon' => "Activer le compagnon ?",
	'label_reinitialiser_compagnon' => "Réinitialiser les messages du compagnon ?",
	
	// O
	'ok' => "OK",
	'ok_bien' => "Bien !",
	'ok_jai_compris' => "J'ai compris !",
	'ok_merci' => "Merci",
	'ok_parfait' => "Parfait !",

	// R
	'reinitialiser' => "Réinitialiser",
	'reinitialiser_moi' => "Oui, uniquement ceux que j'ai déjà lu",
	'reinitialiser_tous' => "Oui, quelque soit l'auteur",
	'reinitialisation' => "Réinitialisation",
	'reinitialisation_ok' => "La réinitialisation est effectuée.",
	
	// T
	'titre_compagnon' => "Le Compagnon",
	'titre_page_configurer_compagnon' => "Configurer le Compagnon",

);

?>
