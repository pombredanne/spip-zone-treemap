<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'actualiser_toutes' => 'Aktualizovať všetky adresy',

	// B
	'bouton_supprimer_url' => 'Odstrániť túto internetovú adresu',

	// E
	'erreur_arbo_2_segments_max' => 'Pre objekt nemôžete použiť viac ako dve zložky adresy',
	'explication_editer' => 'Pokročilé riadenie internetových adries vám umožňuje upravovať internetové adresy redakčných stránok každého objektu a spravovať históriu ich vývoja.',

	// I
	'icone_configurer_urls' => 'Nastaviť internetové adresy',
	'icone_controler_urls' => 'Zmysluplné adresy',
	'info_1_url' => '1 adresa',
	'info_id_parent' => '#parent',
	'info_nb_urls' => '@nb@ internetových adries',
	'info_objet' => 'Objekt',

	// L
	'label_tri_date' => 'Dátum',
	'label_tri_id' => 'ID',
	'label_tri_url' => 'Adresa',
	'label_url' => 'Nová adresa',
	'label_url_minuscules_0' => 'Nechať veľké/malé písmená podľa nadpisu',
	'label_url_minuscules_1' => 'V internetových adresách používať malé písmená',
	'label_url_permanente' => 'Zamknúť túto adresu (žiadne aktualizácie po úprave objektu)',
	'label_url_sep_id' => 'Znak na oddelenie čísla, ktoré sa pridá v prípade duplikátu',
	'label_urls_activer_controle_oui' => 'Aktivovať pokročilé riadenie internetových adries',
	'label_urls_nb_max_car' => 'Maximálny počet znakov',
	'label_urls_nb_min_car' => 'Minimálny počet znakov',
	'liberer_url' => 'Potvrdiť',
	'liste_des_urls' => 'Všetky internetové adresy',

	// T
	'texte_type_urls' => 'Môžete si vybrať, ako sa vytvoria adresy stránky.',
	'texte_type_urls_attention' => 'Varovanie: toto nastavenie bude fungovať, iba ak je súbor @htaccess@ správne nainštalovaný v koreňovom adresári stránky.',
	'texte_urls_nb_max_car' => 'Ak bude názov dlhší, bude skrátený.',
	'texte_urls_nb_min_car' => 'Ak bude názov kratší, použije sa jeho identifikačné číslo.',
	'titre_gestion_des_urls' => 'Riadenie internetových adries',
	'titre_type_arbo' => 'Stromovité adresy',
	'titre_type_html' => 'Adresy z objektov HTML',
	'titre_type_libres' => 'Voľné adresy',
	'titre_type_page' => 'Adresy podľa stránky',
	'titre_type_propres' => 'Čisté adresy',
	'titre_type_propres2' => 'Čisté adresy +<tt>.html</tt>',
	'titre_type_propres_qs' => 'Čisté adresy v požiadavke',
	'titre_type_simple' => 'Jednoduché internetové adresy',
	'titre_type_standard' => 'Historické internetové adresy',
	'titre_type_urls' => 'Typ internetových adries',
	'tout_voir' => 'Zobraziť všetky internetové adresy',

	// U
	'url_ajout_impossible' => 'Došlo k chybe. Táto adresa sa nedá zaregistrovať.',
	'url_ajoutee' => 'Internetová adresa bola pridaná',

	// V
	'verifier_url_nettoyee' => 'Internetová adresa bola opravená, môžete ju pred uložením skontrolovať.',
	'verrouiller_url' => 'Zamknúť'
);

?>
