<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-urls
// Langue: fr
// Date: 30-07-2011 15:01:39
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// U
	'urls_description' => 'Gestion des variantes d\'URL signifiantes ou non',
	'urls_slogan' => 'Gestion des variantes d\'URL signifiantes ou non',
);
?>