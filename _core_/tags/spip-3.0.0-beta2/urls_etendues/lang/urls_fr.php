<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_core_/plugins/urls_etendues/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'actualiser_toutes' => 'Mettre à jour toutes les urls',

	// B
	'bouton_supprimer_url' => 'Supprimer cette URL',

	// E
	'erreur_arbo_2_segments_max' => 'Vous ne pouvez pas utiliser plus de deux segments d\'URL pour un objet',
	'explication_editer' => 'La gestion avancée des urls vous permet d\'éditer l\'url des pages de chaque objet éditorial, et de gérer l\'historique de leur évolution.',

	// I
	'icone_configurer_urls' => 'Configurer les URLs',
	'icone_controler_urls' => 'URLs signifiantes',
	'info_1_url' => '1 URL',
	'info_id_parent' => '#parent',
	'info_nb_urls' => '@nb@ URLs',
	'info_objet' => 'Objet',

	// L
	'label_tri_date' => 'Date',
	'label_tri_id' => 'ID',
	'label_tri_url' => 'URL',
	'label_url' => 'Nouvelle URL',
	'label_url_minuscules_0' => 'Conserver la casse du titre',
	'label_url_minuscules_1' => 'Forcer les urls en minuscules',
	'label_url_permanente' => 'Verrouiller cette URL (pas de mise a jour après edition de l\'objet)',
	'label_url_sep_id' => 'Caractère pour séparer le numéro ajouté en cas de doublon',
	'label_urls_activer_controle_oui' => 'Activer la gestion avancée des URLs',
	'label_urls_nb_max_car' => 'Nombre maximum de caractères',
	'label_urls_nb_min_car' => 'Nombre minimum de caractères',
	'liberer_url' => 'Liberer',
	'liste_des_urls' => 'Toutes les URLs',

	// T
	'texte_type_urls' => 'Vous pouvez choisir ci-dessous le mode de calcul de l\'adresse des pages.',
	'texte_type_urls_attention' => 'Attention ce réglage ne fonctionnera que si le fichier @htaccess@ est correctement installé à la racine du site.',
	'texte_urls_nb_max_car' => 'Si le titre est plus long, il sera coupé.',
	'texte_urls_nb_min_car' => 'Si le titre est plus court, c\'est son Numéro identifiant qui sera utilisé.',
	'titre_gestion_des_urls' => 'Gestion des URLs',
	'titre_type_arbo' => 'URLs Arborescentes',
	'titre_type_html' => 'URLs Objets HTML',
	'titre_type_libres' => 'URLs Libres',
	'titre_type_page' => 'URLs Page',
	'titre_type_propres' => 'URLs Propres',
	'titre_type_propres2' => 'URLs Propres+<tt>.html</tt>',
	'titre_type_propres_qs' => 'URLs Propres en query-string',
	'titre_type_simple' => 'URLs Simples',
	'titre_type_standard' => 'URLs Historiques',
	'titre_type_urls' => 'Type d\'adresses URL',
	'tout_voir' => 'Voir toutes les URLs',

	// U
	'url_ajout_impossible' => 'Une erreur est survenue. Il n\'a pas &t& possible d\'enregistrer cette URL',
	'url_ajoutee' => 'L\'URL a été ajoutée',

	// V
	'verifier_url_nettoyee' => 'L\'URL a été corrigée, vous pouvez verifier avant de l\'enregistrer.',
	'verrouiller_url' => 'Verrouiller'
);

?>
