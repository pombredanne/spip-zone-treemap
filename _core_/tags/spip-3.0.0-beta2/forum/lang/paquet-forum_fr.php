<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-forum
// Langue: fr
// Date: 30-07-2011 15:01:37
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// F
	'forum_description' => 'Forum de SPIP (privé et public)',
	'forum_slogan' => 'Gestion des forums privés et publics dans SPIP',
);
?>