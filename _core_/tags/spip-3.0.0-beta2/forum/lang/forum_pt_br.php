<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_message_forum' => 'Aucun message de forum', # NEW

	// B
	'bouton_radio_articles_futurs' => 'apenas às próximas matérias (sem ação sobre a base de dados).',
	'bouton_radio_articles_tous' => 'a todas as matérias, sem exceção.',
	'bouton_radio_articles_tous_sauf_forum_desactive' => 'a todas as matérias, exceto aquelas em que o fórum está desativado.',
	'bouton_radio_enregistrement_obligatoire' => 'Cadastro obrigatório (os usuários devem se cadastrar, fornecendo seu e-mail, antes de poderem enviar suas contribuições).',
	'bouton_radio_moderation_priori' => 'Moderação prévia (as contribuições só serão exibidas publicamente após a aprovação dos administradores).',
	'bouton_radio_modere_abonnement' => 'por assinatura',
	'bouton_radio_modere_posteriori' => 'moderação à posteriori',
	'bouton_radio_modere_priori' => 'moderação à priori',
	'bouton_radio_publication_immediate' => 'Publicação imediata das mensagens
 (as contribuições serão exibidas assim que forem enviadas; os administradores poderão excluí-las em seguida).',

	// D
	'documents_interdits_forum' => 'Documentos proibídos no fórum',

	// E
	'erreur_enregistrement_message' => 'Votre message n\'a pas pu être enregistré en raison d\'un problème technique', # NEW

	// F
	'form_pet_message_commentaire' => 'Uma mensagem ou comentário?',
	'forum' => 'Fórum',
	'forum_acces_refuse' => 'Você não tem mais acesso a estes fóruns.',
	'forum_attention_dix_caracteres' => '<b>Atenção!</b> Sua mensagem precisa ter pelo menos dez caracteres.',
	'forum_attention_message_non_poste' => 'Attention, vous n\'avez pas posté votre message !', # NEW
	'forum_attention_trois_caracteres' => '<b>Atenção!</b> Seu título precisa ter pelo menos três caracteres.',
	'forum_attention_trop_caracteres' => '<b>Atenção!</b> sua mensagem é muito longa (@compte@ caracteres): para que ela seja gravada, ela não pode ultrapassar @max@ caracteres.',
	'forum_avez_selectionne' => 'Você selecionou:',
	'forum_cliquer_retour' => 'Clique <a href=\'@retour_forum@\'>aqui</a> para continuar.',
	'forum_forum' => 'fórum',
	'forum_info_modere' => 'Este fórum é moderado a priori: a sua contribuição só será exibida após ser validada por um administrador do site.',
	'forum_lien_hyper' => '<b>Link hipertexto</b> (opcional)', # MODIF
	'forum_message' => 'Votre message', # NEW
	'forum_message_definitif' => 'Mensagem definitiva: enviar para o site',
	'forum_message_trop_long' => 'Sua mensagem é muito longa. O tamanho máimo é de 20000 caracteres.',
	'forum_ne_repondez_pas' => 'Não responda a esta mensagem e sim no fórum com o endereço a seguir:',
	'forum_page_url' => '(Se a sua mensagem se refere a uma matéria pubicada na Web, ou a uma página fornecendo mais informações, por favor informe abaixo o título da página e o seu endereço.)',
	'forum_permalink' => 'Lien permanent vers le commentaire', # NEW
	'forum_poste_par' => 'Mensagem enviada @parauteur@ em seguimento da sua matéria.',
	'forum_qui_etes_vous' => '<b>Quem é você?</b> (opcional)', # MODIF
	'forum_saisie_texte_info' => 'Ce formulaire accepte les raccourcis SPIP <code>[-&gt;url] {{gras}} {italique} &lt;quote&gt; &lt;code&gt;</code> et le code HTML <code>&lt;q&gt; &lt;del&gt; &lt;ins&gt;</code>. Pour créer des paragraphes, laissez simplement des lignes vides.', # NEW
	'forum_texte' => 'Texto da sua mensagem:', # MODIF
	'forum_titre' => 'Título:', # MODIF
	'forum_url' => 'URL:', # MODIF
	'forum_valider' => 'Validar a seleção',
	'forum_voir_avant' => 'Ver esta mensagem antes de a enviar', # MODIF
	'forum_votre_email' => 'Seu endereço de e-mail:', # MODIF
	'forum_votre_nom' => 'Seu nome (ou apelido):', # MODIF
	'forum_vous_enregistrer' => 'Para participar deste fórum, você precisa se cadastrar previamente. Por favor, informe abaixo o identificador pessoal que lhe foi fornecido. Se você não está cadastrado, você precisa',
	'forum_vous_inscrire' => 'cadastrar-se.',

	// I
	'icone_bruler_message' => 'Signaler comme Spam', # NEW
	'icone_bruler_messages' => 'Signaler comme Spam', # NEW
	'icone_legitimer_message' => 'Signaler comme licite', # NEW
	'icone_poster_message' => 'Postar uma mensagem',
	'icone_suivi_forum' => 'Acompanhamento do fórum público: @nb_forums@ contribuição(ões)',
	'icone_suivi_forums' => 'Acompanhar / gerenciar os fóruns',
	'icone_supprimer_message' => 'Suprimir esta mensagem',
	'icone_supprimer_messages' => 'Supprimer ces messages', # NEW
	'icone_valider_message' => 'Validar esta mensagem',
	'icone_valider_messages' => 'Valider ces messages', # NEW
	'icone_valider_repondre_message' => 'Valider & Répondre à ce message', # NEW
	'info_1_message_forum' => '1 message de forum', # NEW
	'info_activer_forum_public' => '<i>Para ativar os fóruns públicos, por favor, escolha o modo padrão de moderação:</i>', # MODIF
	'info_appliquer_choix_moderation' => 'Atribuir esta escolha de moderação:',
	'info_config_forums_prive' => 'Na área restrita do site, você pode ativar diversos tipos de fóruns:',
	'info_config_forums_prive_admin' => 'Um fórum reservado aos administradores do site:',
	'info_config_forums_prive_global' => 'Um fórum global, aberto a todos os redatores:',
	'info_config_forums_prive_objets' => 'Um fórum para cada matéria, nota, site referenciado etc.:',
	'info_desactiver_forum_public' => 'Desativar o uso dos fóruns públicos. Os fóruns públicos poderão ser autorizados caso a caso apenas nas matérias; não serão permitidos nas seções, notas etc.',
	'info_envoi_forum' => 'Envio dos fóruns aos autores das matérias',
	'info_fonctionnement_forum' => 'Funcionamento do fórum:',
	'info_forums_liees_mot' => 'Les messages de forum liés à ce mot', # NEW
	'info_gauche_suivi_forum_2' => 'A página de <i>acompanhamento dos fóruns</i> é uma ferramenta de gestão do seu site (e não um espaço de discussão ou de redação). Ela exibe todas as contrubuições do fórum público desta matéria e permite-lhe gerenciar suas contribuições.',
	'info_liens_syndiques_3' => 'fóruns',
	'info_liens_syndiques_4' => 'são',
	'info_liens_syndiques_5' => 'fórum',
	'info_liens_syndiques_6' => 'é',
	'info_liens_syndiques_7' => 'aguardando validação',
	'info_liens_texte' => 'Lien(s) contenu(s) dans le texte du message', # NEW
	'info_liens_titre' => 'Lien(s) contenu(s) dans le titre du message', # NEW
	'info_mode_fonctionnement_defaut_forum_public' => 'Modo de funcionamento padrão dos fóruns públicos',
	'info_nb_messages_forum' => '@nb@ messages de forum', # NEW
	'info_option_email' => 'Sempre que um visitante postar uma nova mensagem no fórum associado a uma matéria, os autores da matéria podem ser avisados dessa mensagem por e-mail. Informe para cada tipo de fórum se esta oção deve ser usada.',
	'info_pas_de_forum' => 'sem fórum',
	'info_question_visiteur_ajout_document_forum' => 'Se você deseja autorizar os visitantes a anexarem documentos (imagens, áudios...) às suas mensagens em fóruns, indique abaixo a lista das extensões de arquivos permitidos para os fóruns (ex.: gif, jpg, png, mp3).', # MODIF
	'info_question_visiteur_ajout_document_forum_format' => 'Se você deseja permitir todos os tipos de documentos considerados como seguros pelo SPIP, digite * (asterisco). Para não permitir, deixe em branco.', # MODIF
	'interface_formulaire' => 'Interface formulaire', # NEW
	'interface_onglets' => 'Interface avec onglets', # NEW
	'item_activer_forum_administrateur' => 'Ativar o fórum dos administradores',
	'item_config_forums_prive_global' => 'Ativar os fóruns dos redatores',
	'item_config_forums_prive_objets' => 'Ativar estes fóruns',
	'item_desactiver_forum_administrateur' => 'Desativar o fórum dos administradores',
	'item_non_config_forums_prive_global' => 'Desativar o fórum dos redatores',
	'item_non_config_forums_prive_objets' => 'Desativar estes fóruns',

	// L
	'lien_reponse_article' => 'Resposta à matéria',
	'lien_reponse_breve_2' => 'Resposta à nota',
	'lien_reponse_message' => 'Réponse au message', # NEW
	'lien_reponse_rubrique' => 'Resposta à seção',
	'lien_reponse_site_reference' => 'resposta ao site referenciado:', # MODIF

	// M
	'messages_aucun' => 'Aucun', # NEW
	'messages_meme_auteur' => 'Tous les messages de cet auteur', # NEW
	'messages_meme_email' => 'Tous les messages de cet email', # NEW
	'messages_meme_ip' => 'Tous les messages de cette IP', # NEW
	'messages_off' => 'Supprimés', # NEW
	'messages_perso' => 'Personnels', # NEW
	'messages_privadm' => 'Administrateurs', # NEW
	'messages_prive' => 'Privés', # NEW
	'messages_privoff' => 'Supprimés', # NEW
	'messages_privrac' => 'Généraux', # NEW
	'messages_prop' => 'Proposés', # NEW
	'messages_publie' => 'Publiés', # NEW
	'messages_spam' => 'Spam', # NEW
	'messages_tous' => 'Tous', # NEW

	// O
	'onglet_messages_internes' => 'Mensagens internas',
	'onglet_messages_publics' => 'Mensagens públicas',
	'onglet_messages_vide' => 'Mensagens sem texto',

	// R
	'repondre_message' => 'Responder a esta mensagem',

	// S
	'statut_off' => 'Supprimé', # NEW
	'statut_original' => 'original',
	'statut_prop' => 'Proposé', # NEW
	'statut_publie' => 'Publié', # NEW
	'statut_spam' => 'Spam', # NEW

	// T
	'text_article_propose_publication_forum' => 'Não hesite a dar a sua contribuição no fórum associado a esta matéria (no pé da página).',
	'texte_en_cours_validation' => 'Les articles, brèves, forums ci dessous sont proposés à la publication.', # NEW
	'texte_en_cours_validation_forum' => 'Não hesite a dar a sua opinião sobre as mesmas nos fóruns a elas associados.',
	'texte_messages_publics' => 'Messages publics sur :', # NEW
	'titre_cadre_forum_administrateur' => 'Fórum privado dos administradores',
	'titre_cadre_forum_interne' => 'Fórum interno',
	'titre_config_forums_prive' => 'Fóruns da área privada',
	'titre_forum' => 'Fórum',
	'titre_forum_suivi' => 'Acompanhamento dos fóruns',
	'titre_page_forum_suivi' => 'Acompanhamento dos fóruns',
	'titre_selection_action' => 'Sélection', # NEW
	'tout_voir' => 'Voir tous les messages', # NEW

	// V
	'voir_messages_objet' => 'voir les messages' # NEW
);

?>
