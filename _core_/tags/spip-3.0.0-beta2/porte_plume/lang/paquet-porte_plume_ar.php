<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-porte_plume
// Langue: ar
// Date: 03-08-2011 19:45:21
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// P
	'porte_plume_description' => 'الريشة هي شريط أدوات موسع لنظام SPIP يستخدم مكتبة جافاسكريبت [MarkItUp->http://markitup.jaysalvat.com/home/]

الأيقونات تأتي من مكتبة [FamFamFam->http://www.famfamfam.com/]',
	'porte_plume_nom' => 'الريشة',
	'porte_plume_slogan' => 'شريط أدوات لتحسين الكتابة',
);
?>