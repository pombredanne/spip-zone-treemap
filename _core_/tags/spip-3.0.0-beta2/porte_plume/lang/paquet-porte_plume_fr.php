<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-porte_plume
// Langue: fr
// Date: 30-07-2011 15:01:38
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// P
	'porte_plume_description' => 'Porte plume est une barre d\'outil extensible pour SPIP qui
	utilise la librairie javascript [MarkItUp->http://markitup.jaysalvat.com/home/]',
	'porte_plume_nom' => 'Porte plume',
	'porte_plume_slogan' => 'Une barre d\'outil pour bien écrire',
);
?>
