<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-porte_plume
// Langue: de
// Date: 03-08-2011 19:45:21
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// P
	'porte_plume_description' => 'Der Federhalter ist eine erweiterbare Werkzeugleiste für SPIP auf Grundlage der Javascript-Bibiliothek [MarkItUp->http://markitup.jaysalvat.com/home/]

	Die Icons stammen aus der Sammlung [FamFamFam->http://www.famfamfam.com/].',
	'porte_plume_nom' => 'Federhalter',
	'porte_plume_slogan' => 'Eine Meüleiste zum Verschönern der Texte',
);
?>