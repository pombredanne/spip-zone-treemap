<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-porte_plume
// Langue: en
// Date: 03-08-2011 19:45:21
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// P
	'porte_plume_description' => 'The Quill is a SPIP extensible toolbar which uses the javascript library 
	[MarkItUp->http://markitup.jaysalvat.com/home/]
	
	Icons are courtesy of the library [FamFamFam->http://www.famfamfam.com/]',
	'porte_plume_nom' => 'Quill',
	'porte_plume_slogan' => 'A toolbar to enhance your texts',
);
?>