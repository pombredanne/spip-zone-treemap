<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'DISKUSNÉ FÓRUM A PETÍCIA',
	'bouton_radio_sauvegarde_compressee' => 'uložiť komprimovanú v súbore @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'uložiť ako dekomprimovanú v súbore @fichier@',

	// F
	'forum_probleme_database' => 'Database problem, your message could not be recorded.',

	// I
	'ical_lien_rss_breves' => 'Syndication of the site\'s news items',
	'icone_creer_mot_cle_breve' => 'Create a new keyword and attach it to this news item',
	'icone_forum_administrateur' => 'Fórum administrátorov',
	'icone_forum_suivi' => 'Sledovanie diskusných fór',
	'icone_publier_breve' => 'Uverejniť túto správu',
	'icone_refuser_breve' => 'Zamietnuť túto správu',
	'info_base_restauration' => 'Prebieha obnova databázy.',
	'info_breves_03' => 'news items',
	'info_breves_liees_mot' => 'News associated with this keyword',
	'info_breves_touvees' => 'News items found',
	'info_breves_touvees_dans_texte' => 'News items found (in the text)',
	'info_echange_message' => 'Systém SPIP umožňuje výmenu správ a vytváranie súkromných
  diskusných fór pre používateľov stránky. Túto funkciu môžete
  zapnúť alebo vypnúť.',
	'info_erreur_restauration' => 'Chyba pri obnove: súbor sa nenašiel.',
	'info_forum_administrateur' => 'diskusné fórum administrátorov',
	'info_forum_interne' => 'interné diskusné fórum',
	'info_forum_ouvert' => 'In the site\'s private area, a forum is open to all
		registered editors. Below, you can enable an
		extra forum reserved for the administrators.',
	'info_gauche_suivi_forum' => 'Stránka na <i>sledovanie diskusných fór</i> je riadiaci nástroj vášho webu (nie zóna na diskusie alebo úpravy). Uvádza všetky príspevky verejného diskusného fóra k tomuto článku a umožňuje vám o týchto príspevkoch rozhodovať.',
	'info_modifier_breve' => 'Modify the news item:',
	'info_nombre_breves' => '@nb_breves@ news items,',
	'info_option_ne_pas_faire_suivre' => 'Nepreposielať príspevky z diskusných fór',
	'info_restauration_sauvegarde_insert' => 'Vkladanie archívu @archive@ do databázy',
	'info_sauvegarde_articles' => 'Zálohovať články',
	'info_sauvegarde_articles_sites_ref' => 'Backup articles of referenced sites',
	'info_sauvegarde_auteurs' => 'Zálohovať autorov',
	'info_sauvegarde_breves' => 'Backup the news',
	'info_sauvegarde_documents' => 'Zálohovať dokumenty',
	'info_sauvegarde_echouee' => 'If the backup fails («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Zálohovať diskusné fóra',
	'info_sauvegarde_groupe_mots' => 'Zálohovať skupiny kľúčových slov',
	'info_sauvegarde_messages' => 'Zálohovať príspevky',
	'info_sauvegarde_mots_cles' => 'Backup the keywords',
	'info_sauvegarde_petitions' => 'Backup the petitions',
	'info_sauvegarde_refers' => 'Zálohovať referery',
	'info_sauvegarde_reussi_01' => 'Backup successful.',
	'info_sauvegarde_rubrique_reussi' => 'The tables of the @titre@ section have been saved to @archive@. You can',
	'info_sauvegarde_rubriques' => 'Zálohovať rubriky',
	'info_sauvegarde_signatures' => 'Backup petitions signatures',
	'info_sauvegarde_sites_references' => 'Backup referenced sites',
	'info_sauvegarde_type_documents' => 'Zálohovať typy dokumentov',
	'info_sauvegarde_visites' => 'Zálohovať návštevy',
	'info_une_breve' => 'a news item,',
	'item_mots_cles_association_breves' => 'news items',
	'item_nouvelle_breve' => 'Nová novinka',

	// L
	'lien_forum_public' => 'Spravovať verejne fórum tohto článku',
	'lien_reponse_breve' => 'Reply to the news item',

	// S
	'sauvegarde_fusionner' => 'Zlúčiť databázu so zálohou',
	'sauvegarde_fusionner_depublier' => 'Unpublish the merged objects',
	'sauvegarde_url_origine' => 'Ak ju treba, adresa zdrojovej stránky:',

	// T
	'texte_admin_tech_03' => 'You can choose to save the file in a compressed form, to 
	speed up its transfer to your machine or to a backup server and save some disk space.',
	'texte_admin_tech_04' => 'In order to merge with another database, you can restrict the backup to one section: ',
	'texte_sauvegarde_compressee' => 'Záloha bude uložená v dekomprimovanom súbore @fichier@.',
	'titre_nouvelle_breve' => 'Nová novinka',
	'titre_page_breves_edit' => 'Modify the news item: «@titre@»',
	'titre_page_forum' => 'Diskusné fórum administrátorov',
	'titre_page_forum_envoi' => 'Poslať správu',
	'titre_page_statistiques_messages_forum' => 'Príspevky z diskusných fór'
);

?>
