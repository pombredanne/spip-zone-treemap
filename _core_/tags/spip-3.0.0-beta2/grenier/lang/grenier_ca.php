<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FÒRUM & PETICIÓ',
	'bouton_radio_sauvegarde_compressee' => 'còpia de seguretat comprimida a @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'còpia de seguretat no comprimida a @fichier@',

	// F
	'forum_probleme_database' => 'Problema de la base de dades, el vostre missatge no s\'ha registrat.',

	// I
	'ical_lien_rss_breves' => 'Sindicació de les breus del lloc',
	'icone_creer_mot_cle_breve' => 'Crear una nova paraula clau i lligar-la a aquesta breu',
	'icone_forum_administrateur' => 'Fòrum dels administradors/administradores',
	'icone_forum_suivi' => 'Seguiment dels fòrums',
	'icone_publier_breve' => 'Publicar aquesta breu',
	'icone_refuser_breve' => 'Refusar aquesta breu',
	'info_base_restauration' => 'La restauració de la base de dades està en curs.',
	'info_breves_03' => 'breus',
	'info_breves_liees_mot' => 'Les breus lligades a aquesta paraula clau',
	'info_breves_touvees' => 'Breus trobades',
	'info_breves_touvees_dans_texte' => 'Breus trobades (en el text)',
	'info_echange_message' => 'SPIP permet l\'intercanvi de missatges i la creació de fòrums de discussió
  privats entre els participants d\'un lloc. Podeu habilitar o
  inhabilitar aquesta funcionalitat.',
	'info_erreur_restauration' => 'Error de restauració: fitxer inexistent.',
	'info_forum_administrateur' => ' fòrum dels administradors',
	'info_forum_interne' => 'fòrum intern',
	'info_forum_ouvert' => 'Dins l\'espai privat del lloc, hi ha un fòrum obert a tots
  els redactors registrats. Més avall, podeu activar un fòrum suplementari,
  reservat només als adminsitradors. ',
	'info_gauche_suivi_forum' => 'La pàgina de <i>seguiment dels fòrums</i> és una eina de gestió del vostre lloc Web (i no un espai de discussió o de redacció). Mostra totes les contribucions del fòrum públic d\'aquest article i us permet gestionar aquestes contribucions.',
	'info_modifier_breve' => 'Modificar la breu:',
	'info_nombre_breves' => '@nb_breves@ breus,',
	'info_option_ne_pas_faire_suivre' => 'No reenviar els missatges dels fòrums',
	'info_restauration_sauvegarde_insert' => 'Inserció de @archive@ a la base',
	'info_sauvegarde_articles' => 'Crear còpia de seguretat dels articles',
	'info_sauvegarde_articles_sites_ref' => 'Crear còpia de seguretat dels articles dels llocs referenciats',
	'info_sauvegarde_auteurs' => 'Crear còpia de seguretat dels autors',
	'info_sauvegarde_breves' => 'Crear còpia de seguretat de les breus',
	'info_sauvegarde_documents' => 'Crear còpia de seguretat dels documents',
	'info_sauvegarde_echouee' => 'Si la còpia de seguretat fracassa («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Crear còpia de seguretat dels fòrums',
	'info_sauvegarde_groupe_mots' => 'Crear còpia de seguretat dels grups de paraules',
	'info_sauvegarde_messages' => 'Crear còpia de seguretat dels missatges',
	'info_sauvegarde_mots_cles' => 'Crear còpia de seguretat de les paraules clau',
	'info_sauvegarde_petitions' => 'Crear còpia de seguretat de les peticions',
	'info_sauvegarde_refers' => 'Crear còpia de seguretat de les referències',
	'info_sauvegarde_reussi_01' => 'Còpia de seguretat amb èxit.',
	'info_sauvegarde_rubrique_reussi' => 'Les taules de la secció @titre@ s\'han desat a @archive@. Podeu',
	'info_sauvegarde_rubriques' => 'Crear còpia de seguretat de les seccions',
	'info_sauvegarde_signatures' => 'Crear còpia de seguretat de les firmes de petició',
	'info_sauvegarde_sites_references' => 'Guardar els llocs referenciats',
	'info_sauvegarde_type_documents' => 'Crear còpia de seguretat dels tipus de documents',
	'info_sauvegarde_visites' => 'Crear còpia de seguretat de les visites',
	'info_une_breve' => 'una breu,',
	'item_mots_cles_association_breves' => 'a les breus',
	'item_nouvelle_breve' => 'Nova breu',

	// L
	'lien_forum_public' => 'Gestionar el fòrum públic d\'aquest article',
	'lien_reponse_breve' => 'Resposta a la breu',

	// S
	'sauvegarde_fusionner' => 'Fusionar la base actual i la de seguretat',
	'sauvegarde_fusionner_depublier' => 'Despublicar els objectes fusionats',
	'sauvegarde_url_origine' => 'Eventualment, URL del lloc d\'origen: ',

	// T
	'texte_admin_tech_03' => 'Es pot triar guardar el fitxer comprimit per retallar temps de transferència fins a casa seva o guardar-lo en un servidor per a còpies de seguretat, i estalviar l\'espai de disc.',
	'texte_admin_tech_04' => 'En un objectiu de fusió amb una altra base, podeu limitar la còpia de seguretat a la secció:  ',
	'texte_sauvegarde_compressee' => 'La còpia de seguretat es farà a l\'arxiu no comprimit @fichier@.',
	'titre_nouvelle_breve' => 'Nova breu',
	'titre_page_breves_edit' => ' Modificar la breu: " @titre@ "',
	'titre_page_forum' => 'Fòrum dels administradors',
	'titre_page_forum_envoi' => 'Enviar un missatge',
	'titre_page_statistiques_messages_forum' => 'Missatges de fòrum'
);

?>
