<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORUM & PETITION', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'save as compressed in @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'save as uncompressed in @fichier@', # NEW

	// F
	'forum_probleme_database' => 'Tietokantavirhe, viestiäsi ei voitu tallentaa.',

	// I
	'ical_lien_rss_breves' => 'Syndication of the site\'s news items', # NEW
	'icone_creer_mot_cle_breve' => 'Create a new keyword and attach it to this news item', # NEW
	'icone_forum_administrateur' => 'Administrators\' forum', # NEW
	'icone_forum_suivi' => 'Forums follow-up', # NEW
	'icone_publier_breve' => 'Publish this news item', # NEW
	'icone_refuser_breve' => 'Reject this news item', # NEW
	'info_base_restauration' => 'Tietokannan palautus käynnissä.',
	'info_breves_03' => 'uutisotsikoita',
	'info_breves_liees_mot' => 'Tähän hakusanaan liittyviä uutisia',
	'info_breves_touvees' => 'Uutisotsikoita löytyi',
	'info_breves_touvees_dans_texte' => 'Uutisaiheita löytyi (tekstin joukossa)',
	'info_echange_message' => 'SPIP allows the exchange of messages and the creation of private
		discussion forums between participants to the site. You can enable or
		disable this feature.', # NEW
	'info_erreur_restauration' => 'Restoration error: file not found.', # NEW
	'info_forum_administrateur' => 'ylläpitäjän foorumi',
	'info_forum_interne' => 'sisäinen foorumi',
	'info_forum_ouvert' => 'In the site\'s private area, a forum is open to all
		registered editors. Below, you can enable an
		extra forum reserved for the administrators.', # NEW
	'info_gauche_suivi_forum' => 'The <i>forums follow-up</i> page is a management tool of your site (not a discussion or editing area). It displays all the contributions of the public forum of this article and allows you to manage these contributions.', # NEW
	'info_modifier_breve' => 'Muokkaa uutisotsikoita:',
	'info_nombre_breves' => '@nb_breves@ uutisotsikoita,',
	'info_option_ne_pas_faire_suivre' => 'Älä lähetä edelleen foorumien viestejä',
	'info_restauration_sauvegarde_insert' => 'Inserting @archive@ in the database', # NEW
	'info_sauvegarde_articles' => 'Varmistuskopioi artikkelit',
	'info_sauvegarde_articles_sites_ref' => 'Varmistuskopioi referoitujen  sivustojen artikkelit ',
	'info_sauvegarde_auteurs' => 'Varmistuskopioi kirjoittajat',
	'info_sauvegarde_breves' => 'Varmistuskopioi uutiset',
	'info_sauvegarde_documents' => 'Varmistuskopioi asiakirjat',
	'info_sauvegarde_echouee' => 'Jos varmistuskopionti epäonnistuu  («Pisin suoritusaika ylitetty»),',
	'info_sauvegarde_forums' => 'Varmistuskopioi foorumit',
	'info_sauvegarde_groupe_mots' => 'Varmistuskopioi hakusana-ryhmät',
	'info_sauvegarde_messages' => 'Varmistuskopioi viestit',
	'info_sauvegarde_mots_cles' => 'Varmistuskopioi hakusanat',
	'info_sauvegarde_petitions' => 'Varmistuskopioi vetoomukset',
	'info_sauvegarde_refers' => 'Varmistuskopioi viittaajat',
	'info_sauvegarde_reussi_01' => 'Varmistuskopionti onnistui.',
	'info_sauvegarde_rubrique_reussi' => 'The tables of the @titre@ section have been saved to @archive@. You can', # NEW
	'info_sauvegarde_rubriques' => 'Varmistuskopioi lohkot',
	'info_sauvegarde_signatures' => 'Varmistuskopioi vetoomusten allekirjoitukset',
	'info_sauvegarde_sites_references' => 'Varmistuskopioi viitatut sivustot',
	'info_sauvegarde_type_documents' => 'Varmistuskopioi asiakirjojen lajit',
	'info_sauvegarde_visites' => 'Varmistuskopioi käynnit',
	'info_une_breve' => 'a news item,', # NEW
	'item_mots_cles_association_breves' => 'news items', # NEW
	'item_nouvelle_breve' => 'New news item', # NEW

	// L
	'lien_forum_public' => 'Manage this article\'s public forum', # NEW
	'lien_reponse_breve' => 'Reply to the news item', # NEW

	// S
	'sauvegarde_fusionner' => 'Merge the current database with the backup', # NEW
	'sauvegarde_fusionner_depublier' => 'Unpublish the merged objects', # NEW
	'sauvegarde_url_origine' => 'If necessary, the URL of the source site:', # NEW

	// T
	'texte_admin_tech_03' => 'You can choose to save the file in a compressed form, to 
	speed up its transfer to your machine or to a backup server and save some disk space.', # NEW
	'texte_admin_tech_04' => 'In order to merge with another database, you can restrict the backup to one section: ', # NEW
	'texte_sauvegarde_compressee' => 'Backup will be stored in the uncompressed file @fichier@.', # NEW
	'titre_nouvelle_breve' => 'Uusi uutisotsikko',
	'titre_page_breves_edit' => 'Muokkaa uutisotsikkoa: «@titre@»',
	'titre_page_forum' => 'Ylläpitäjien foorumi',
	'titre_page_forum_envoi' => 'Lähetä viesti',
	'titre_page_statistiques_messages_forum' => 'Forum messages' # NEW
);

?>
