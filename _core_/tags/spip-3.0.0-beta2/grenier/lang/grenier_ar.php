<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'المنتدى والعرائض',
	'bouton_radio_sauvegarde_compressee' => 'حفظ بملف مضغوط في @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'حفظ بملف غير مضغوط في @fichier@',

	// F
	'forum_probleme_database' => 'مشكلة في قاعدة البيانات، لم يتم تسجيل مشاركتك.',

	// I
	'ical_lien_rss_breves' => 'ترخيص أخبار الموقع',
	'icone_creer_mot_cle_breve' => 'إنشاء مفتاح جديد وربطه بهذا الخبر',
	'icone_forum_administrateur' => 'منتدى المديرين',
	'icone_forum_suivi' => 'متابعة المنتديات',
	'icone_publier_breve' => 'نشر هذا الخبر',
	'icone_refuser_breve' => 'رفض هذا الخبر',
	'info_base_restauration' => 'القاعدة قيد الاسترجاع',
	'info_breves_03' => 'الأخبار',
	'info_breves_liees_mot' => 'الأخبار المرتبطة بهذا المفتاح',
	'info_breves_touvees' => 'الأخبار التي عثر عليها',
	'info_breves_touvees_dans_texte' => 'الأخبار التي عثر عليها (في المادة)',
	'info_echange_message' => 'يتيح SPIP  تبادل الرسائل وإنشاء منتديات نقاش
خاصة بين المشتركين في الموقع. يمكنك تفعيل هذه
الوظيفة أو إيقافها.',
	'info_erreur_restauration' => 'خطأ إسترجاع: ملف غير موجود.',
	'info_forum_administrateur' => 'منتدى الإدارة',
	'info_forum_interne' => 'المنتدى الداخلي',
	'info_forum_ouvert' => 'في مجال الموقع الخاص، هناك منتدى مفتوح لجميع
المحررين المسجلين. يمكنك أدناه، تفعيل
منتدى إضافي محصور بالمدراء.',
	'info_gauche_suivi_forum' => 'تشكل صفحة <b>متابعة المنتديات</b> أداة إدارة للموقع (وليست مجالاً للنقاش أو التحرير). تعرض هذه الصفحة كل مشاركات المنتدى العام وتتيح إدارة هذه المشاركات.',
	'info_modifier_breve' => 'تعديل الخبر:',
	'info_nombre_breves' => '@nb_breves@ خبر،',
	'info_option_ne_pas_faire_suivre' => 'عدم تمرير مشاركات المنتديات',
	'info_restauration_sauvegarde_insert' => 'إدراج @archive@ في القاعدة',
	'info_sauvegarde_articles' => 'نسخ إحتياطي للمقالات',
	'info_sauvegarde_articles_sites_ref' => 'نسخ إحتياطي لمقالات المواقع المبوبة',
	'info_sauvegarde_auteurs' => 'نسخ إحتياطي للمؤلفين',
	'info_sauvegarde_breves' => 'نسخ إحتياطي للأخبار',
	'info_sauvegarde_documents' => 'نسخ إحتياطي للمنتديات',
	'info_sauvegarde_echouee' => 'اذا فشل النسخ الاحتياطي («Maximum execution time exceeded»)، ',
	'info_sauvegarde_forums' => 'نسخ إحتياطي للمنتديات',
	'info_sauvegarde_groupe_mots' => 'نسخ إحتياطي لمجموعات المفاتيح',
	'info_sauvegarde_messages' => 'نسخ إحتياطي للرسائل',
	'info_sauvegarde_mots_cles' => 'نسخ إحتياطي للمفاتيح',
	'info_sauvegarde_petitions' => 'نسخ إحتياطي للعرائض',
	'info_sauvegarde_refers' => 'نسخ إحتياطي لمواقع المصدر',
	'info_sauvegarde_reussi_01' => 'نجح النسخ الاحتياطي.',
	'info_sauvegarde_rubrique_reussi' => 'ان حفظ جداول القسم @titre@ في @archive@. يمكنك',
	'info_sauvegarde_rubriques' => 'نسخ إحتياطي للأقسام',
	'info_sauvegarde_signatures' => 'نسخ إحتياطي لتوقيعات العرائض',
	'info_sauvegarde_sites_references' => 'نسخ إحتياطي للمواقع المبوبة',
	'info_sauvegarde_type_documents' => 'نسخ إحتياطي لأنواع المستندات',
	'info_sauvegarde_visites' => 'نسخ إحتياطي للزيارات',
	'info_une_breve' => 'خبر،',
	'item_mots_cles_association_breves' => 'الأخبار',
	'item_nouvelle_breve' => 'خبر جديد',

	// L
	'lien_forum_public' => 'إدارة المنتدى العمومي لهذا المقال',
	'lien_reponse_breve' => 'الرد على الخبر',

	// S
	'sauvegarde_fusionner' => 'دمج القاعدة الحالية والنسخة الاحتياطية',
	'sauvegarde_fusionner_depublier' => 'الغاء نشر العناصر المدمجة',
	'sauvegarde_url_origine' => 'عنوان URL للموقع المصدر، اذا امكن:',

	// T
	'texte_admin_tech_03' => 'يمكنك اختيار حفظ الملف بشكل مضغوط
لتسريع نقله الى جهازك أو الى خادم إحتياطي وتوفير يعض المساحة على القرص الثابت.',
	'texte_admin_tech_04' => 'من اجل الدمج في قاعدة اخرى، يمكن اقتصار النسخة الاحتياطية على القسم:',
	'texte_sauvegarde_compressee' => 'سيتم حفظ النسخة الاحتياطية في ملف غير مضغوط @fichier@.',
	'titre_nouvelle_breve' => 'خبر جديد',
	'titre_page_breves_edit' => 'تعديل الخبر: «@titre@»',
	'titre_page_forum' => 'منتدى الإدارة',
	'titre_page_forum_envoi' => 'إرسال مشاركة',
	'titre_page_statistiques_messages_forum' => 'مشاركات المنتدى'
);

?>
