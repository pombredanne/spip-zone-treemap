<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FOROM HA SINADEG', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'restr savete gwasket dindan @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'restr savete anwasket dindan @fichier@',

	// F
	'forum_probleme_database' => 'Kudenn diaz titouroù, n\'eo ket bet enrollet ho kemennadenn.',

	// I
	'ical_lien_rss_breves' => 'Sindikadur berrskridoù al lec\'hienn-mañ',
	'icone_creer_mot_cle_breve' => 'Krouiñ ur ger-alc\'hwez nevez hag e liammañ ouzh ar berrskrid-mañ',
	'icone_forum_administrateur' => 'Forom ar verourien',
	'icone_forum_suivi' => 'Heuliañ ar foromoù',
	'icone_publier_breve' => 'Embann ar berrskrid-mañ',
	'icone_refuser_breve' => 'Nac\'hañ ar berrskrid-mañ',
	'info_base_restauration' => 'Oc\'h assevel an diaz titouroù emeur.',
	'info_breves_03' => 'berrskridoù',
	'info_breves_liees_mot' => 'Ar berrskridoù stag ar ger-stur-mañ outo',
	'info_breves_touvees' => 'Berrskridoù kavet',
	'info_breves_touvees_dans_texte' => 'Berrskridoù kavet (en destenn)',
	'info_echange_message' => 'SPIP a ginnig eskemm kemennadennoù ha sevel foromoù prevez etre izili al lec\'hienn. Gallout a rit ober gant ar servij-mañ, pe get.',
	'info_erreur_restauration' => 'Fazi assevel : n\'eus ket eus ar restr-mañ.',
	'info_forum_administrateur' => 'forom ar verourien',
	'info_forum_interne' => 'forom diabarzh',
	'info_forum_ouvert' => 'E lodenn brevez al lec\'hienn ez eus ur forom digor d\'an holl skridaozerien enrollet.
  Amañ dindan e c\'hellit dibab ma vo savet ur forum evit ar verourien hepken ivez.',
	'info_gauche_suivi_forum' => 'Pajenn <i>heuliañ ar foromoù</i> a dalvez da verañ ho lec\'hienn, ha ket da dabutal pe da skridaozañ. Warni emañ an holl evezhiadennoù graet war ar forom foran diwar-benn ar pennad-mañ, deoc\'h da c\'hallout o merañ.',
	'info_modifier_breve' => 'Kemmañ ar berrskrid :',
	'info_nombre_breves' => '@nb_breves@ berrskrid,',
	'info_option_ne_pas_faire_suivre' => 'Arabat heuliañ kemennadennoù ar foromoù',
	'info_restauration_sauvegarde_insert' => 'Ouzhpennañ @archive@ en diaz',
	'info_sauvegarde_articles' => 'Enrollañ ar pennadoù',
	'info_sauvegarde_articles_sites_ref' => 'Enrollañ pennadoù al lec\'hiennoù menegeret',
	'info_sauvegarde_auteurs' => 'Enrollañ ar skridaozerien',
	'info_sauvegarde_breves' => 'Enrollañ ar berrskridoù',
	'info_sauvegarde_documents' => 'Enrollañ an teuliadoù',
	'info_sauvegarde_echouee' => 'M\'eo c\'hwitet ar savete («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Enrollañ ar foromoù',
	'info_sauvegarde_groupe_mots' => 'Enrollañ ar strolladoù gerioù',
	'info_sauvegarde_messages' => 'Enrollañ ar c\'hemennadennoù',
	'info_sauvegarde_mots_cles' => 'Enrollañ ar gerioù-stur',
	'info_sauvegarde_petitions' => 'Enrollañ ar sinadegoù',
	'info_sauvegarde_refers' => 'Enrollañ ar referers',
	'info_sauvegarde_reussi_01' => 'Enrolladenn kaset da benn vat',
	'info_sauvegarde_rubrique_reussi' => 'Saveteet eo bet taolennoù ar rubrikenn @titre@ e @archive@. Gallout a rit',
	'info_sauvegarde_rubriques' => 'Enrollañ ar rubrikennoù',
	'info_sauvegarde_signatures' => 'Enrollañ sinadurioù ar sinadegoù',
	'info_sauvegarde_sites_references' => 'Enrollañ al lec\'hiennoù menegeret',
	'info_sauvegarde_type_documents' => 'Enrollañ ar seurtoù teulioù',
	'info_sauvegarde_visites' => 'Enrollañ ar gweladennoù',
	'info_une_breve' => 'ur berrskrid,',
	'item_mots_cles_association_breves' => 'ouzh ar berrskridoù',
	'item_nouvelle_breve' => 'Berrskrid nevez',

	// L
	'lien_forum_public' => 'Merañ forom foran ar pennad-mañ',
	'lien_reponse_breve' => 'Respont d\'ar berrskrid',

	// S
	'sauvegarde_fusionner' => 'Kendeuziñ an diaz red gant ar savete',
	'sauvegarde_fusionner_depublier' => 'Dizembann ar pezh zo bet kendeuzet',
	'sauvegarde_url_origine' => 'Marteze, URL al lec\'hienn orin :',

	// T
	'texte_admin_tech_03' => 'Gallout a rit dibab saveteiñ stumm gwasket ar restr evit krennañ war an treuzkas d\'hoc\'h urzhiataer pe d\'ur servijer savete, hag evit ma kemerfe nebeutoc\'h a blas.',
	'texte_admin_tech_04' => 'Mar fell deoc\'h kendeuziñ gant un diaz all e c\'hellit dibab saveteiñ ur rubrikenn resis hepken: ',
	'texte_sauvegarde_compressee' => 'Er restr n\'eo ket bet gwasket @fichier@ e vo graet ar saveteiñ.',
	'titre_nouvelle_breve' => 'Berrskrid nevez',
	'titre_page_breves_edit' => 'Kemmañ ar berrskrid : « @titre@ »',
	'titre_page_forum' => 'Forom ar verourien',
	'titre_page_forum_envoi' => 'Kas ur gemennadenn',
	'titre_page_statistiques_messages_forum' => 'Kemennadennoù forom'
);

?>
