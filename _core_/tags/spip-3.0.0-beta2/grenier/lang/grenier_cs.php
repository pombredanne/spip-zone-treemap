<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FÓRUM A PETICE', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'uložit komprimovaně v  @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'uložit nekomprimovaně v @fichier@',

	// F
	'forum_probleme_database' => 'Kvůli problémům s databází nebylo možné vaši zprávu zaznamenat.',

	// I
	'ical_lien_rss_breves' => 'Syndication of the site\'s news items', # NEW
	'icone_creer_mot_cle_breve' => 'Create a new keyword and attach it to this news item', # NEW
	'icone_forum_administrateur' => 'Diskusní skupina správců',
	'icone_forum_suivi' => 'Navazující zprávy diskusní skupiny',
	'icone_publier_breve' => 'Zveřejnit novinku',
	'icone_refuser_breve' => 'Odmítnout novinku',
	'info_base_restauration' => 'Databáze se obnovuje.',
	'info_breves_03' => 'novinky',
	'info_breves_liees_mot' => 'Novinky spojené s tímto klíčovým slovem',
	'info_breves_touvees' => 'Nalezené novinky',
	'info_breves_touvees_dans_texte' => 'Nalezené novinky (v textu)',
	'info_echange_message' => 'Systém SPIP umožňuje výměnu zpráv a vytváření soukromých
  diskusních skupin pro účastníky webu. Tuto funkci můžete
  zapnout nebo vypnout.',
	'info_erreur_restauration' => 'Chyba při obnově: soubor neexistuje.',
	'info_forum_administrateur' => 'diskusní skupina správců',
	'info_forum_interne' => 'interní diskusní skupina',
	'info_forum_ouvert' => 'V privátní části webu je diskusní skupina otevřena všem
  zaregistrovaným redaktorům. Zde můžete aktivovat další diskusní
  skupinu vyhrazenou pouze pro správce.',
	'info_gauche_suivi_forum' => 'Stránka pro <i>sledování diskusních skupin</i> je určena ke správě vašeho webu (nejedná se o prostor pro diskusi ani pro redigování). Jsou na ní zobrazeny všechny diskusní příspěvky z veřejné skupiny a umožňuje vám jejich správu.', # MODIF
	'info_modifier_breve' => 'Změnit novinku:',
	'info_nombre_breves' => '@nb_breves@ novinky,',
	'info_option_ne_pas_faire_suivre' => 'Nesledovat zprávy z diskusní skupiny',
	'info_restauration_sauvegarde_insert' => 'Inserting @archive@ in the database', # NEW
	'info_sauvegarde_articles' => 'Uložit články',
	'info_sauvegarde_articles_sites_ref' => 'Uložit články z webu, na který vede odkaz',
	'info_sauvegarde_auteurs' => 'Uložit autory',
	'info_sauvegarde_breves' => 'Uložit novinky',
	'info_sauvegarde_documents' => 'Uložit dokumenty',
	'info_sauvegarde_echouee' => 'Pokud se uložení nezdařilo («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Uložit diskusní skupiny',
	'info_sauvegarde_groupe_mots' => 'Uložit skupiny slov',
	'info_sauvegarde_messages' => 'Uložit zprávy',
	'info_sauvegarde_mots_cles' => 'Uložit klíčová slova',
	'info_sauvegarde_petitions' => 'Uložit petice',
	'info_sauvegarde_refers' => 'Uložit osoby, které zadali odkazy',
	'info_sauvegarde_reussi_01' => 'Uložení proběhlo úspěšně.',
	'info_sauvegarde_rubrique_reussi' => 'The tables of the @titre@ section have been saved to @archive@. You can', # NEW
	'info_sauvegarde_rubriques' => 'Uložit sekce',
	'info_sauvegarde_signatures' => 'Uložit podpisy pod peticemi',
	'info_sauvegarde_sites_references' => 'Uložit weby na něž vedou odkazy',
	'info_sauvegarde_type_documents' => 'Uložit typy dokumentů',
	'info_sauvegarde_visites' => 'Uložit návštěvy',
	'info_une_breve' => 'jedna novinka,',
	'item_mots_cles_association_breves' => 'k novinkám',
	'item_nouvelle_breve' => 'Nová novinka',

	// L
	'lien_forum_public' => 'Správa veřejné diskusní skupiny k tomuto článku',
	'lien_reponse_breve' => 'Odpověď na novinku',

	// S
	'sauvegarde_fusionner' => 'Merge the current database with the backup', # NEW
	'sauvegarde_fusionner_depublier' => 'Unpublish the merged objects', # NEW
	'sauvegarde_url_origine' => 'If necessary, the URL of the source site:', # NEW

	// T
	'texte_admin_tech_03' => 'Můžete se rozhodnout o uložení komprimovaného souboru. Tím zkrátíte
 dobu potřebnou k jeho přenosu u vás nebo na serveru kde máte zálohy a ušetříte místo na disku.',
	'texte_admin_tech_04' => 'In order to merge with another database, you can restrict the backup to one section: ', # NEW
	'texte_sauvegarde_compressee' => 'Záloha bude uložena do nekomprimovaného souboru @fichier@.',
	'titre_nouvelle_breve' => 'Nová novinka',
	'titre_page_breves_edit' => 'Změnit novinku: "@titre@"',
	'titre_page_forum' => 'Diskusní skupina správců',
	'titre_page_forum_envoi' => 'Odeslat zprávu',
	'titre_page_statistiques_messages_forum' => 'Forum messages' # NEW
);

?>
