<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORUM & PETISI', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'simpan dalam bentuk kompresi di @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'simpan dalam bentuk tidak terkompresi di @fichier@',

	// F
	'forum_probleme_database' => 'Permasalahan database, pesan anda tidak dapat disimpan.',

	// I
	'ical_lien_rss_breves' => 'Sindikasi artikel berita situs',
	'icone_creer_mot_cle_breve' => 'Créer un nouveau mot-clé et le lier à cette brève', # NEW
	'icone_forum_administrateur' => 'Forum administrator',
	'icone_forum_suivi' => 'Tindak lanjut forum',
	'icone_publier_breve' => 'Publikasi artikel berita ini',
	'icone_refuser_breve' => 'Tolak artikel berita ini',
	'info_base_restauration' => 'Pemulihan database dalam proses.',
	'info_breves_03' => 'artikel berita',
	'info_breves_liees_mot' => 'Berita-berita yang terasosiasi dengan kata kunci ini',
	'info_breves_touvees' => 'Artikel berita ditemukan',
	'info_breves_touvees_dans_texte' => 'Artikel berita ditemukan (dalam teks)',
	'info_echange_message' => 'SPIP mengizinkan pertukaran pesan dan pembuatan forum
		diskusi pribadi di antara pengunjung situs. Anda dapat mengaktifkan
		atau menonaktifka fitur ini.',
	'info_erreur_restauration' => 'Kesalahan pemulihan: berkas tidak ditemukan.',
	'info_forum_administrateur' => 'forum administrators',
	'info_forum_interne' => 'forum internal',
	'info_forum_ouvert' => 'Di area pribadi situs, sebuah forum terbuka untuk semua
		editor yang terdaftar. Di bawah, anda dapat mengaktifkan
		forum tambahan direservasi untuk para administrator.',
	'info_gauche_suivi_forum' => 'Halaman <i>tindak lanjut forum</i> adalah alat bantu pengelola situs anda (bukan area diskusi atau pengeditan). Halaman ini menampilkan semua kontribusi forum umum artikel ini dan mengizinkan anda untuk mengelola kontribusi-kontribusi ini.',
	'info_modifier_breve' => 'Modifikasi artikel berita:',
	'info_nombre_breves' => '@nb_breves@ artikel berita,',
	'info_option_ne_pas_faire_suivre' => 'Jangan teruskan pesan forum',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Backup artikel',
	'info_sauvegarde_articles_sites_ref' => 'Backup artikel-artikel dari situs-situs referensi',
	'info_sauvegarde_auteurs' => 'Backup penulis',
	'info_sauvegarde_breves' => 'Backup berita',
	'info_sauvegarde_documents' => 'Backup dokumen',
	'info_sauvegarde_echouee' => 'Jika backup gagal («Waktu eksekusi maksimum terlampaui»),',
	'info_sauvegarde_forums' => 'Backup forum',
	'info_sauvegarde_groupe_mots' => 'Backup kelompok kata kunci',
	'info_sauvegarde_messages' => 'Backup pesan',
	'info_sauvegarde_mots_cles' => 'Backup kata-kata kunci',
	'info_sauvegarde_petitions' => 'Backup petisi',
	'info_sauvegarde_refers' => 'Backup pereferensi',
	'info_sauvegarde_reussi_01' => 'Backup berhasil.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Backup Bagian',
	'info_sauvegarde_signatures' => 'Backup tanda tangan petisi',
	'info_sauvegarde_sites_references' => 'Backup situs-situs referensi',
	'info_sauvegarde_type_documents' => 'Backup tipe dokumen',
	'info_sauvegarde_visites' => 'Backup kunjungan',
	'info_une_breve' => 'sebuah artikel berita,',
	'item_mots_cles_association_breves' => 'artikel berita',
	'item_nouvelle_breve' => 'Artikel berita baru',

	// L
	'lien_forum_public' => 'Kelola artikel forum umum ini',
	'lien_reponse_breve' => 'Balasan pada artikel berita',

	// S
	'sauvegarde_fusionner' => 'Gabung database sekarang dengan backup',
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Jika diperlukan, URL situs sumber:',

	// T
	'texte_admin_tech_03' => 'Anda dapat memilih untuk menyimpan berkas dalam bentuk kompresi 
	untuk mempercepat tranfer ke mesin anda atau server backup dan menyimpan sejumlah ruangan di disk.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'Backup akan disimpan dalam berkas tidak terkompresi @fichier@.',
	'titre_nouvelle_breve' => 'Artikel berita baru',
	'titre_page_breves_edit' => 'Modifikasi artikel berita: «@titre@»',
	'titre_page_forum' => 'Forum administrator',
	'titre_page_forum_envoi' => 'Kirim sebuah pesan',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
