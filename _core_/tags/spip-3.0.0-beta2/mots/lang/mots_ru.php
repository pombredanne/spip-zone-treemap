<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'avis_conseil_selection_mot_cle' => '<b>Важная группа:</b> Настоятельно рекомендуется выбрать ключевое слово в этой группе.',
	'avis_doublon_mot_cle' => 'Слово с таким название уже существует. Вы уверенны, что хотите создать еще одно?',

	// B
	'bouton_checkbox_qui_attribue_mot_cle_administrateurs' => 'администраторы сайта',
	'bouton_checkbox_qui_attribue_mot_cle_redacteurs' => 'редакторы',
	'bouton_checkbox_qui_attribue_mot_cle_visiteurs' => 'посетители основной части сайта, когда они размещают сообщение на форуме.',

	// C
	'creer_et_associer_un_mot' => '<NEW>Créér et associer un mot-clé', # MODIF

	// I
	'icone_creation_groupe_mots' => 'Создать новую группу ключевых слов',
	'icone_creation_mots_cles' => 'Создать новое ключевое слово',
	'icone_modif_groupe_mots' => 'Изменить эту группу ключевых слов',
	'icone_modifier_mot' => 'Изменить ключевое слово',
	'icone_mots_cles' => 'Кейворды',
	'icone_supprimer_groupe_mots' => 'Удалить эту группу',
	'icone_voir_groupe_mots' => '<NEW>Voir ce groupe de mots', # NEW
	'icone_voir_tous_mots_cles' => 'Показать все ключевые слова',
	'info_1_groupe_mots' => '<NEW>1 groupe de mots', # NEW
	'info_articles_lies_mot' => 'Статьи с этим ключевым словом',
	'info_aucun_groupe_mots' => '<NEW>Aucun groupe de mots', # NEW
	'info_aucun_mot_cle' => 'No keywords', # NEW
	'info_changer_nom_groupe' => 'Изменить название этой группы:',
	'info_creation_mots_cles' => 'Создать и настроить ключевые слова сайта можно здесь',
	'info_dans_groupe' => 'В группе:',
	'info_delet_mots_cles' => 'Вы сделали запрос на удаление ключевого слова
<b>@titre_mot@</b> (@type_mot@).Это ключевое слово связывается с 
<b> @texte_lie </b> Вы должны подтвердить это решение: ',
	'info_groupe_important' => 'Важная группа',
	'info_modifier_mot' => 'Редактировать ключевое слово:',
	'info_mots_cles' => 'Ключевые слова',
	'info_mots_cles_association' => 'Ключевые слова в этой группе могут быть связаны с:',
	'info_nb_groupe_mots' => '<NEW>@nb@ groupes de mots', # NEW
	'info_oui_suppression_mot_cle' => 'Я хочу удалить это ключевое слово.',
	'info_question_mots_cles' => 'Хотите ли Вы использовать на Вашем сайте ключевые слова?',
	'info_qui_attribue_mot_cle' => 'Ключевые слова в этой группе могут быть применены к:',
	'info_remplacer_mot' => 'Replace "@titre@"', # NEW
	'info_retirer_mot' => 'Удалить ключевое слово',
	'info_retirer_mots' => 'Удалить все ключевые слова',
	'info_rubriques_liees_mot' => 'Разделы, связанные с этим ключевым словом',
	'info_selection_un_seul_mot_cle' => 'Вы можете выбрать <b> только одно ключевое слово </b> к этой группе.',
	'info_supprimer_mot' => 'удалить это ключевое слово',
	'info_titre_mot_cle' => 'Имя или название этого ключевого слова',
	'info_un_mot' => 'Одно ключевое слово одновременно',
	'item_ajout_mots_cles' => 'Разрешить дополнение форумов ключевыми словами',
	'item_non_ajout_mots_cles' => 'Не разрешать дополнение форумов ключевыми словами',
	'item_non_utiliser_config_groupe_mots_cles' => 'Не использовать дополнительную настройку групп ключевого слова',
	'item_non_utiliser_mots_cles' => 'Не использовать ключевые слова',
	'item_utiliser_config_groupe_mots_cles' => 'Использовать дополнительную настройку групп ключевых слов',
	'item_utiliser_mots_cles' => 'Использовать ключевые слова',

	// L
	'lien_ajouter_mot' => 'Add this keyword', # NEW
	'logo_groupe' => 'LOGO FOR THIS GROUP', # NEW
	'logo_mot_cle' => 'ЛОГОТИП КЛЮЧЕВОГО СЛОВА',

	// T
	'texte_config_groupe_mots_cles' => 'Вы желаете включить дополнительные настройки групп ключевых слов, 
   определяя, например, что уникальное слово в 
 группе   может быть отобрано, что группа является важной ...?',
	'texte_mots_cles' => 'Ключевые слова позволяют Вам создавать тематические связи между Вашими 
 статьями,  независимо от их размещения в разделе. Таким образом, Вы можете 
  обогатить навигацию Вашего сайта или даже использовать эти 
 свойства для настройки статей в своих шаблонах.',
	'texte_mots_cles_dans_forum' => 'Хотите ли Вы разрешить посетителям выбирать ключевые слова в форумах основного сайта? (Предупреждение: эту опцию довольно сложно использовать правильно.)',
	'texte_nouveau_mot' => 'Новое ключевое слово',
	'titre_config_groupe_mots_cles' => 'Настройка групп ключевых слов',
	'titre_gauche_mots_edit' => 'НОМЕР КЛЮЧЕВОГО СЛОВА:',
	'titre_groupe_mots' => '<NEW>Groupe de mots-clés', # NEW
	'titre_groupe_mots_numero' => '<NEW>GROUPE DE MOTS NUMÉRO :', # NEW
	'titre_groupes_mots' => '<NEW>Groupes de mots-clés', # NEW
	'titre_mots_cles_dans_forum' => 'Ключевые слова в форумах основного сайта',
	'titre_mots_tous' => 'Ключевые слова',
	'titre_nouveau_groupe' => 'Новая группа',
	'titre_objets_lies_mot' => '<NEW>Liés à ce mot-clé :', # NEW
	'titre_page_mots_tous' => 'Ключевые слова'
);

?>
