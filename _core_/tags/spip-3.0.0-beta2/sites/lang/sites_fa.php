<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_dispo' => 'در انتظار',
	'articles_meme_auteur' => 'تمام مقاله‌هاي اين نويسنده',
	'articles_off' => 'بلوك‌ها',
	'articles_publie' => 'منتشر شده',
	'articles_refuse' => 'حذفي‌ها',
	'articles_tous' => 'تمام',
	'aucun_article_syndic' => 'بدون مقاله‌ي پيوندي',
	'avis_echec_syndication_01' => 'پيوند برقرار نشد : يا فايل «بك اند» ناخوانا ست يا هيچ مقاله اى ندارد',
	'avis_echec_syndication_02' => 'پيوند برقرار نشد : دسترسى به «بك اند» اين سايت ممكن نيست.',
	'avis_site_introuvable' => 'سايت پيدا نميشود',
	'avis_site_syndique_probleme' => 'توجه: مشترك‌سازي اين سايت به مشكلى برخورد كرده است؛ و سيستم موقتأ  قطع مي‌باشد. به نشاني فايل پيوندي اين سايت را چك كنيد (<b>@url_syndic@</b>)و دوباره براى دريافت اطلاعات جديد اقدام كنيد. ',
	'avis_sites_probleme_syndication' => 'اين سايتها در مشترك‌سازي با مشكل مواجه شده‌اند',
	'avis_sites_syndiques_probleme' => 'اين سايتهاى پيوندى به مشكلى برخورده اند',

	// B
	'bouton_exporter' => 'Exporter', # NEW
	'bouton_importer' => 'Importer', # NEW
	'bouton_radio_modere_posteriori' => 'بعد از تعديل ',
	'bouton_radio_modere_priori' => 'پيش از تعديل',
	'bouton_radio_non_syndication' => 'بدون پيوند به سايتهاى ديگر',
	'bouton_radio_syndication' => 'مشترك سازي: ',

	// C
	'confirmer_purger_syndication' => 'آيا مي‌خواهيد تمام مقاله‌هاي پيوندي با اين سايت را حذف كنيد؟',

	// E
	'entree_adresse_fichier_syndication' => 'آدرس فايل براى ارتباط ميان سايتى:',
	'entree_adresse_site' => ' </b> آدرس سايت <b>[اجبارى] ',
	'entree_description_site' => 'توصيف سايت',
	'erreur_fichier_format_inconnu' => 'Le format du fichier @fichier@ n\'est pas pris en charge.', # NEW
	'erreur_fichier_incorrect' => 'Impossible de lire le fichier.', # NEW

	// F
	'form_prop_nom_site' => 'نام سايت',

	// I
	'icone_article_syndic' => 'مقاله‌ي پيوندي',
	'icone_articles_syndic' => 'مقاله‌هاي پيوندي',
	'icone_controler_syndication' => 'انتشار مقاله‌هاي پيوندي ',
	'icone_modifier_site' => 'اين سايت را اصلاح كنيد',
	'icone_referencer_nouveau_site' => 'ارجاع يك سايت جديد',
	'icone_site_reference' => 'سايت‌هاي ارجاعي',
	'icone_supprimer_article' => 'حذف اين مقاله',
	'icone_supprimer_articles' => 'حذف اين مقاله‌ها',
	'icone_valider_article' => 'ثبت اين مقاله',
	'icone_valider_articles' => 'ثبت اين مقاله‌ها',
	'icone_voir_sites_references' => 'به سايتهاى ارجاعي نگاه كنيد',
	'info_1_site_importe' => '1 site a été importé', # NEW
	'info_a_valider' => '[معتبر شود]',
	'info_aucun_site_importe' => 'Aucun site n\'a pu être importé', # NEW
	'info_bloquer' => 'ببندید',
	'info_bloquer_lien' => 'اين پيوند را مسدود كنيد',
	'info_derniere_syndication' => ':آخرين پيوند سايتى انجام شده در تاريخ ',
	'info_liens_syndiques_1' => 'پيوند‌هاي مشترك‌سازي شده',
	'info_liens_syndiques_2' => 'در انتظار تأئيد شدن هستند.',
	'info_nb_sites_importes' => '@nb@ sites ont été importés', # NEW
	'info_nom_site_2' => ' [اجبارى] <b>نام سايت</b>',
	'info_panne_site_syndique' => 'سايت پيوندى خراب است',
	'info_probleme_grave' => 'مشكل',
	'info_question_proposer_site' => 'كى ميتواند سايتهاى مرجع را پيشنهاد كند ؟',
	'info_retablir_lien' => 'اين پيوند را دوباره برقرار كنيد',
	'info_site_attente' => 'سايت در انتظار تائيد',
	'info_site_propose' => 'اين سايت در تاريخ زير پيشنهاد شده: ',
	'info_site_reference' => 'سايت ارجاعي آنلاين',
	'info_site_refuse' => 'سايت پذيرفته نشده',
	'info_site_syndique' => 'اين سايت پيوند دارد . . . ',
	'info_site_valider' => 'اين سايت ها بايد معتبر شوند',
	'info_sites_referencer' => 'ارجاع يك سايت',
	'info_sites_refuses' => 'سايت هاى پذيرفته نشده',
	'info_statut_site_1' => 'اين سايت :',
	'info_statut_site_2' => 'منتشر شده',
	'info_statut_site_3' => 'پيشنهاد شده',
	'info_statut_site_4' => 'در سطل',
	'info_syndication' => 'پيوند سايتى :',
	'info_syndication_articles' => 'مقاله(ها)',
	'item_bloquer_liens_syndiques' => 'پيوندهاى سايتى را براى تأئيد بلوكه كنيد',
	'item_gerer_annuaire_site_web' => 'يك راهنما از سايتهاى تارنما بسازيد',
	'item_non_bloquer_liens_syndiques' => 'پيوندهاى نتيجه ى ارتباط ميان سايتى را مسدود نكنيد',
	'item_non_gerer_annuaire_site_web' => 'راهنماى سايتهاى تارنما را غير فعال كنيد',
	'item_non_utiliser_syndication' => 'از پيوند سايتى خودكار استفاده نكنيد',
	'item_utiliser_syndication' => 'از پيوند سايتى خودكار استفاده كنيد',

	// L
	'label_exporter_avec_mots_cles_1' => 'Exporter les mots-clés sous forme de tags', # NEW
	'label_exporter_id_parent' => 'Exporter les sites de la rubrique', # NEW
	'label_exporter_publie_seulement_1' => 'Exporter uniquement les sites publiés', # NEW
	'label_fichier_import' => 'Fichier HTML', # NEW
	'label_importer_les_tags_1' => 'Importer les tags sous forme de mot-clé', # NEW
	'label_importer_statut_publie_1' => 'Publier automatiquement les sites', # NEW
	'lien_mise_a_jour_syndication' => 'اكنون به روز كنيد',
	'lien_nouvelle_recuperation' => 'اقدام به بازگيرى دوباره داده ها كنيد',
	'lien_purger_syndication' => 'پاك سازي تمام مقاله‌هاي پيوندي',

	// N
	'nombre_articles_syndic' => '@nb@ مقاله‌ي پيوندي',

	// S
	'statut_off' => 'حذفي',
	'statut_prop' => 'در انتظار',
	'statut_publie' => 'منتشر شده',
	'syndic_choix_moderation' => 'برای پیوندهای بعدی از سوی این سایت چه کار میکنید؟',
	'syndic_choix_oublier' => 'برای پیوندهایی که در فایل سایتهای پيوندي ظاهر نمیشوند چه میکنید؟',
	'syndic_choix_resume' => 'بعضی از سایتها متن کامل مقالات را عرضه میکنند. در اینصورت آیا مایلید که آنرا به سایتتان پیوند بزنید:',
	'syndic_lien_obsolete' => 'پیوند بیفایده',
	'syndic_option_miroir' => 'بطور خودکار مسدود کنید',
	'syndic_option_oubli' => '(پس از @mois@ ماه) پاکشان کنید',
	'syndic_option_resume_non' => 'متن کامل مقالات (با فرمت اج‌تي‌ام‌ال)',
	'syndic_option_resume_oui' => 'خلاصه ی مقاله (با فرمت متن)',
	'syndic_options' => 'گزینه‌هاي مشترك‌سازي:',

	// T
	'texte_expliquer_export_bookmarks' => 'Vous pouvez exporter une liste de sites au format Marque-page HTML,
	pour vous permettre ensuite de l\'importer dans votre navigateur ou dans un service en ligne', # NEW
	'texte_expliquer_import_bookmarks' => 'Vous pouvez importer une liste de sites au format Marque-page HTML,
	en provenance de votre navigateur ou d\'un service en ligne de gestion des Marques-pages.', # NEW
	'texte_liens_sites_syndiques' => 'پيوندهاى مربوط به سايتهاى مرجع ميتوانند مسدود شوند، سپس ميتوانيد آنها را تك تك باز كنيد يا اينكه سايت به سايت پيوندهاى آينده را مسدود كنيد. ',
	'texte_messages_publics' => 'پيامهاى همگانى مقاله :',
	'texte_non_fonction_referencement' => 'شما ميتوانيد از اين كاربرد خودكار استفاده نكنيد، دراينصورت خودتان بايد مشخصات سايت را تعيين كنيد',
	'texte_referencement_automatique' => 'ثبت خودكار يك سايت در جستجوگرها، شما ميتوانيد ثبت تارنماى سايت را با مشخص كردن آدرس URLو يا آدرس فايل RSS در زير انجام  دهيد.اسپيپ تمام اطلاعات اعم از عنوان، توصيف،... را بطور خودكار جمع آورى خواهد كرد.',
	'texte_referencement_automatique_verifier' => 'لطفاً پيش از ثبت اطلاعات ارايه شده توسط <tt>@url@</tt> آن را تأييد كنيد.',
	'texte_syndication' => 'اگر سايت اجازه دهد، مي‌توانيد بطور خودكار،  فهرست تازه‌ها را بگيريد. براى اين منظور، شما بايد گزينش پخش همزمان (سنديكاسيون) را فعال كنيد.<blockquote><i> تعدادى از ميزبانان اين عمل را غيرفعال مي‌كنند. در آن صورت نمي‌توانيد از آن استفاده كنيد.</i></blockquote>',
	'titre_articles_syndiques' => 'مقالات پيوندى گرفته شده از اين سايت',
	'titre_dernier_article_syndique' => 'آخرين مقالات پيوندى',
	'titre_exporter_bookmarks' => 'Exporter des Marques-pages', # NEW
	'titre_importer_bookmarks' => 'Importer des Marques-pages', # NEW
	'titre_importer_exporter_bookmarks' => 'Importer et Exporter des Marques-pages', # NEW
	'titre_page_sites_tous' => 'سايتهاى مرجع',
	'titre_referencement_sites' => 'ارجاع سايت‌ها و مشترك‌سازي',
	'titre_site_numero' => 'سايت شماره :',
	'titre_sites_proposes' => 'سايتهاى پيشنهادى',
	'titre_sites_references_rubrique' => 'سايتهاى مرجع در اين بخش',
	'titre_sites_syndiques' => 'سايتهاى پيوندى',
	'titre_sites_tous' => 'سايتهاى ارجاعي',
	'titre_syndication' => 'مشترك‌سازي سايتها',
	'tout_voir' => 'تمام مقاله‌هاي پيوندي را بنگريد',

	// U
	'un_article_syndic' => '1 مقاله پيوندي'
);

?>
