<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-sites
// Langue: fr
// Date: 30-07-2011 15:01:38
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'sites_description' => 'Sites et syndication dans SPIP (privé et public)',
	'sites_slogan' => 'Gestion des sites et de la syndication dans SPIP',
);
?>