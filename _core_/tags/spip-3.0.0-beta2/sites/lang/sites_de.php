<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_dispo' => 'En attente', # NEW
	'articles_meme_auteur' => 'Tous les articles de cet auteur', # NEW
	'articles_off' => 'Bloqués', # NEW
	'articles_publie' => 'Publiés', # NEW
	'articles_refuse' => 'Supprimés', # NEW
	'articles_tous' => 'Tous', # NEW
	'aucun_article_syndic' => 'Aucun article syndiqué', # NEW
	'avis_echec_syndication_01' => 'Syndikation fehlgeschlagen: Die Backend-Datei konnte nicht gelesen werden oder enthält keinen Artikel.',
	'avis_echec_syndication_02' => 'Syndikation fehlgeschlagen: Backend-Datei dieser Website ist nicht erreichbar.',
	'avis_site_introuvable' => 'Website nicht auffindbar',
	'avis_site_syndique_probleme' => 'Achtung! Bei der Syndikation dieser Website ist ein Problem aufgetreten und das System ist vorübergehend außer Betrieb. Überprüfen Sie die Adresse der backend-Datei der Website (<b>@url_syndic@</b>) und versuchen Sie, die Informationen erneut abzurufen.',
	'avis_sites_probleme_syndication' => 'Es gab ein Problem mit der Syndikation dieser Websites',
	'avis_sites_syndiques_probleme' => 'Es gab ein Problem mit der Syndikation dieser Websites',

	// B
	'bouton_exporter' => 'Exporter', # NEW
	'bouton_importer' => 'Importer', # NEW
	'bouton_radio_modere_posteriori' => 'Nachträgliche Moderation',
	'bouton_radio_modere_priori' => 'Vorgeschaltete Moderation',
	'bouton_radio_non_syndication' => 'Syndikation nicht verwenden',
	'bouton_radio_syndication' => 'Syndikation:',

	// C
	'confirmer_purger_syndication' => 'Êtes-vous certain de vouloir supprimer tous les articles syndiqués de ce site ?', # NEW

	// E
	'entree_adresse_fichier_syndication' => 'Adresse der backend-Datei:',
	'entree_adresse_site' => '<b>Adresse der Website</b> [Pflichtfeld]',
	'entree_description_site' => 'Beschreibung der Website',
	'erreur_fichier_format_inconnu' => 'Le format du fichier @fichier@ n\'est pas pris en charge.', # NEW
	'erreur_fichier_incorrect' => 'Impossible de lire le fichier.', # NEW

	// F
	'form_prop_nom_site' => 'Name der Website',

	// I
	'icone_article_syndic' => 'Article syndiqué', # NEW
	'icone_articles_syndic' => 'Articles syndiqués', # NEW
	'icone_controler_syndication' => 'Publication des articles syndiqués', # NEW
	'icone_modifier_site' => 'Website bearbeiten',
	'icone_referencer_nouveau_site' => 'Neue Website verlinken',
	'icone_site_reference' => 'Sites référencés', # NEW
	'icone_supprimer_article' => 'Supprimer cet article', # NEW
	'icone_supprimer_articles' => 'Supprimer ces articles', # NEW
	'icone_valider_article' => 'Valider cet article', # NEW
	'icone_valider_articles' => 'Valider ces articles', # NEW
	'icone_voir_sites_references' => 'Verlinkte Websites anzeigen',
	'info_1_site_importe' => '1 site a été importé', # NEW
	'info_a_valider' => '[zu bestätigen]',
	'info_aucun_site_importe' => 'Aucun site n\'a pu être importé', # NEW
	'info_bloquer' => 'sperren',
	'info_bloquer_lien' => 'Link sperren',
	'info_derniere_syndication' => 'Datum der letzten Syndikation dieser Website: ',
	'info_liens_syndiques_1' => 'Links (Syndikation)',
	'info_liens_syndiques_2' => 'warten auf Freigabe.',
	'info_nb_sites_importes' => '@nb@ sites ont été importés', # NEW
	'info_nom_site_2' => '<b>Name der Website</b> [Pflichtfeld]',
	'info_panne_site_syndique' => 'Syndizierte Website defekt',
	'info_probleme_grave' => 'Problem mit',
	'info_question_proposer_site' => 'Wer darf Websites zur Verlinkung vorschlagen?',
	'info_retablir_lien' => 'Link wieder freigeben',
	'info_site_attente' => 'Websites, die auf Freigabe warten',
	'info_site_propose' => 'Website vorgeschlagen am:',
	'info_site_reference' => 'Verlinkte Website online',
	'info_site_refuse' => 'Abgelehnte Website',
	'info_site_syndique' => 'Diese Website ist syndiziert...',
	'info_site_valider' => 'Websites, die auf Freigabe warten',
	'info_sites_referencer' => 'Website verlinken',
	'info_sites_refuses' => 'Abgelehnte Websites',
	'info_statut_site_1' => 'Diese Website ist:',
	'info_statut_site_2' => 'Veröffentlicht',
	'info_statut_site_3' => 'Vorgeschlagen',
	'info_statut_site_4' => 'Gelöscht',
	'info_syndication' => 'Syndikation:',
	'info_syndication_articles' => 'Artikel',
	'item_bloquer_liens_syndiques' => 'Syndizierte Websites bis zur Freigabe sperren',
	'item_gerer_annuaire_site_web' => 'Website-Verzeichnis verwalten',
	'item_non_bloquer_liens_syndiques' => 'Links zu syndizierten Websites nicht sperren',
	'item_non_gerer_annuaire_site_web' => 'Website-Verzeichnis abschalten',
	'item_non_utiliser_syndication' => 'Keine automatische Syndikation verwenden',
	'item_utiliser_syndication' => 'Automatische Syndikation verwenden',

	// L
	'label_exporter_avec_mots_cles_1' => 'Exporter les mots-clés sous forme de tags', # NEW
	'label_exporter_id_parent' => 'Exporter les sites de la rubrique', # NEW
	'label_exporter_publie_seulement_1' => 'Exporter uniquement les sites publiés', # NEW
	'label_fichier_import' => 'Fichier HTML', # NEW
	'label_importer_les_tags_1' => 'Importer les tags sous forme de mot-clé', # NEW
	'label_importer_statut_publie_1' => 'Publier automatiquement les sites', # NEW
	'lien_mise_a_jour_syndication' => 'Jetzt aktualisieren',
	'lien_nouvelle_recuperation' => 'Abruf der Daten erneut versuchen',
	'lien_purger_syndication' => 'Effacer tous les articles syndiqués', # NEW

	// N
	'nombre_articles_syndic' => '@nb@ articles syndiqués', # NEW

	// S
	'statut_off' => 'Supprimé', # NEW
	'statut_prop' => 'En attente', # NEW
	'statut_publie' => 'Publié', # NEW
	'syndic_choix_moderation' => 'Was soll mit weiteren Links dieser Seite geschehen?',
	'syndic_choix_oublier' => 'Was soll mit Links geschehen, die nicht mehr im Feed übertragen werden?',
	'syndic_choix_resume' => 'Manche Websites übertragen den vollständigen Text von Artikeln. In welcher Form möchten sie diese Artikel übernehmen:',
	'syndic_lien_obsolete' => 'veralteter Link',
	'syndic_option_miroir' => 'automatisch sperren',
	'syndic_option_oubli' => 'nach @mois@ Monaten löschen',
	'syndic_option_resume_non' => 'Den vollständigen Inhalt der Artikel (im HTML-Format)',
	'syndic_option_resume_oui' => 'Eine einfache Zusammenfassung (im Textformat)',
	'syndic_options' => 'Syndikation - Optionen:',

	// T
	'texte_expliquer_export_bookmarks' => 'Vous pouvez exporter une liste de sites au format Marque-page HTML,
	pour vous permettre ensuite de l\'importer dans votre navigateur ou dans un service en ligne', # NEW
	'texte_expliquer_import_bookmarks' => 'Vous pouvez importer une liste de sites au format Marque-page HTML,
	en provenance de votre navigateur ou d\'un service en ligne de gestion des Marques-pages.', # NEW
	'texte_liens_sites_syndiques' => 'Links zu syndizierten Websites können bis zur Freigabe durch einen Administrator gesperrt bleiben. Hier legen Sie die Grundeinstellung fest. Sie können diese Einstellung für jede Site einzeln ändern, bzw. nach und nach entscheiden, wie die neuen Links einer Website behandelt werden sollen.',
	'texte_messages_publics' => 'Öffentliche Beiträge des Artikels:',
	'texte_non_fonction_referencement' => 'Sie können diese automatische Funktion ignorieren und die für Ihre Website wichtigen Elemente selber angeben ...',
	'texte_referencement_automatique' => '<b>Automatische Verlinkung einer Website</b><br />Sie können eine Website im Schnellverfahren verlinken, indem Sie seinen URL oder den seiner Backend-Datei angeben. SPIP wird dann die Daten der Website selbständig einlesen (Titel, Beschreibung ...).',
	'texte_referencement_automatique_verifier' => 'Bitte überprüfen Sie die Informationen von <tt>@url@</tt> vor dem Speichern.',
	'texte_syndication' => 'Manche Websites ermöglichen den automatischen Abruf einer Liste von neuen Artikeln. Um diese Daten nutzen zu können, müssen Sie die Syndikation aktivieren.
                <blockquote><i>Manche Provider unterbinden die Nutzung dieser Funktion. Dann können Sie keine syndizierten Inhalte nutzen.</i></blockquote>',
	'titre_articles_syndiques' => 'Syndizierte Artikel dieser Website',
	'titre_dernier_article_syndique' => 'Neue syndizierte Artikel',
	'titre_exporter_bookmarks' => 'Exporter des Marques-pages', # NEW
	'titre_importer_bookmarks' => 'Importer des Marques-pages', # NEW
	'titre_importer_exporter_bookmarks' => 'Importer et Exporter des Marques-pages', # NEW
	'titre_page_sites_tous' => 'Verlinkte Websites',
	'titre_referencement_sites' => 'Verlinkung und Syndikation',
	'titre_site_numero' => 'WEBSITE NUMMER:',
	'titre_sites_proposes' => 'Vorgeschlagene Websites',
	'titre_sites_references_rubrique' => 'Verlinkte Websites in dieser Rubrik',
	'titre_sites_syndiques' => 'Syndizierte Websites',
	'titre_sites_tous' => 'Verlinkte Websites',
	'titre_syndication' => 'Website Syndikation',
	'tout_voir' => 'Voir tous les articles syndiqués', # NEW

	// U
	'un_article_syndic' => '1 article syndiqué' # NEW
);

?>
