<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-tw
// Langue: fr
// Date: 30-07-2011 15:01:38
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// T
	'tw_description' => 'Intégrer TextWheel dans SPIP',
	'tw_slogan' => 'Gestion de la typographie SPIP avec TextWheel',
);
?>