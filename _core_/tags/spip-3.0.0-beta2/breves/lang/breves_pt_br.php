<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'breve' => 'Nota',
	'breves' => 'Notas',

	// E
	'entree_breve_publiee' => 'Esta nota deve ser publicada?', # MODIF
	'entree_texte_breve' => 'Texto da nota',

	// I
	'icone_breves' => 'Notas',
	'icone_ecrire_nouvel_article' => 'As notas contidas nesta seção',
	'icone_modifier_breve' => 'Editar esta nota',
	'icone_nouvelle_breve' => 'Escrever uma nova nota',
	'info_1_breve' => '1 nota',
	'info_aucun_breve' => 'Nenhuma nota',
	'info_breves' => 'O seu site utiliza o sistema de notas?',
	'info_breves_02' => 'Notas',
	'info_breves_valider' => 'Notas para validar',
	'info_gauche_numero_breve' => 'NOTA NÚMERO', # MODIF
	'info_nb_breves' => '@nb@ notas',
	'item_breve_proposee' => 'Nota proposta', # MODIF
	'item_breve_refusee' => 'NÃO - Nota recusada', # MODIF
	'item_breve_validee' => 'SIM - Nota validada', # MODIF
	'item_non_utiliser_breves' => 'Não utilizar as notas',
	'item_utiliser_breves' => 'Utilizar as notas',

	// L
	'logo_breve' => 'LOGO DA NOTA', # MODIF

	// T
	'texte_breves' => 'As notas são textos curtos e simples que permitem rapidamente disponibilizar online informações concisas, gerenciar o clipping, um calendário de eventos...',
	'titre_breve_proposee' => 'Nota proposta',
	'titre_breve_publiee' => 'Nota publicada',
	'titre_breve_refusee' => 'Nota recusada',
	'titre_breves' => 'As notas',
	'titre_langue_breve' => 'IDIOMA DA NOTA', # MODIF
	'titre_nouvelle_breve' => 'Nouvelle brève', # NEW
	'titre_page_breves' => 'Notas'
);

?>
