<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'breve' => 'Brève', # NEW
	'breves' => 'Nieuwsberichten',

	// E
	'entree_breve_publiee' => 'Dient dit bericht te worden gepubliceerd ?', # MODIF
	'entree_texte_breve' => 'Tekst van het bericht',

	// I
	'icone_breves' => 'Berichten',
	'icone_ecrire_nouvel_article' => 'De berichten van deze rubriek',
	'icone_modifier_breve' => 'Dit bericht wijzigen',
	'icone_nouvelle_breve' => 'Een nieuwsbericht schrijven',
	'info_1_breve' => '1 bericht',
	'info_aucun_breve' => 'Geen nieuws',
	'info_breves' => 'Werkt je website met het systeem van berichten?',
	'info_breves_02' => 'nieuwsberichten',
	'info_breves_valider' => 'Berichten ter goedkeuring',
	'info_gauche_numero_breve' => 'NUMMER BERICHT', # MODIF
	'info_nb_breves' => '@nb@ brèves', # NEW
	'item_breve_proposee' => 'Bericht voorgesteld', # MODIF
	'item_breve_refusee' => 'NEEN - Geweigerd nieuwsbericht', # MODIF
	'item_breve_validee' => 'JA - Goedgekeurd nieuwsbericht', # MODIF
	'item_non_utiliser_breves' => 'Geen gebruik maken van nieuwsberichten',
	'item_utiliser_breves' => 'Nieuwsberichten gebruiken',

	// L
	'logo_breve' => 'LOGO VAN HET NIEUWSBERICHT', # MODIF

	// T
	'texte_breves' => 'Berichten zijn korte en eenvoudige teksten die je toelaten nieuws snel online te brengen, een persbericht op te stellen, een evenementenkalender te maken, ...',
	'titre_breve_proposee' => 'Voorgesteld bericht',
	'titre_breve_publiee' => 'Gepubliceerd bericht',
	'titre_breve_refusee' => 'Geweigerd bericht',
	'titre_breves' => 'Berichten',
	'titre_langue_breve' => 'TAAL VAN HET NIEUWSBERICHT', # MODIF
	'titre_nouvelle_breve' => 'Nouvelle brève', # NEW
	'titre_page_breves' => 'Berichten'
);

?>
