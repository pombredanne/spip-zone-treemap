<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-breves
// Langue: fr
// Date: 30-07-2011 15:01:36
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// B
	'breves_description' => 'Les brèves sont des informations courtes, sans auteur.',
	'breves_slogan' => 'Gestion des brèves dans SPIP',
);
?>