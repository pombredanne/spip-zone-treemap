<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'breve' => 'Новость',
	'breves' => 'Новости',

	// E
	'entree_breve_publiee' => 'Является ли эта новость опубликованной?', # MODIF
	'entree_texte_breve' => 'Текст новости',

	// I
	'icone_breves' => 'Новости',
	'icone_ecrire_nouvel_article' => 'Новости в этом разделе',
	'icone_modifier_breve' => 'Изменить эту новость',
	'icone_nouvelle_breve' => 'Написать новость',
	'info_1_breve' => '1 новость',
	'info_aucun_breve' => 'Aucune brève', # NEW
	'info_breves' => 'Использовать систему новостей?',
	'info_breves_02' => 'Новости',
	'info_breves_valider' => 'Новости на утверждении',
	'info_gauche_numero_breve' => 'ЧИСЛО НОВОСТЕЙ', # MODIF
	'info_nb_breves' => '@nb@ news items', # NEW
	'item_breve_proposee' => 'Новости отправлены', # MODIF
	'item_breve_refusee' => 'НЕТ - отключить новость', # MODIF
	'item_breve_validee' => 'Да - включить новость ', # MODIF
	'item_non_utiliser_breves' => 'Не использовать новости',
	'item_utiliser_breves' => 'Использовать новости',

	// L
	'logo_breve' => 'ЛОГОТИП НОВОСТИ', # MODIF

	// T
	'texte_breves' => 'Новости - короткие и простые тексты, которые используют 
 онлайн публикацию  сжатой информации, управляют 
 обзором прессы, календарем событий...',
	'titre_breve_proposee' => 'Новости отправлены',
	'titre_breve_publiee' => 'Новости опубликованы',
	'titre_breve_refusee' => 'Новости отклонены',
	'titre_breves' => 'Новости',
	'titre_langue_breve' => 'ЯЗЫК НОВОСТИ', # MODIF
	'titre_nouvelle_breve' => 'Nouvelle brève', # NEW
	'titre_page_breves' => 'Новости'
);

?>
