<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'info_compresseur_titre' => 'Optimaliseringen en samendrukking',
	'info_question_activer_compactage_css' => 'Wilt u de verdichting van de stijl bladen  activeren (CSS) ?', # MODIF
	'info_question_activer_compactage_js' => 'Wilt u de verdichting van de scripts activeren (javascript) ?', # MODIF
	'info_question_activer_compresseur' => 'Wilt u de samendrukking van de stroom activeren HTTP ?', # MODIF
	'item_compresseur_closure' => 'Gebruik Google Sluiting Compiler [experimental]',
	'item_compresseur_css' => 'Activer la compression des feuilles de styles (CSS)', # NEW
	'item_compresseur_html' => 'Activer la compression du HTML', # NEW
	'item_compresseur_js' => 'Activer la compression des scripts (javascript)', # NEW

	// T
	'texte_compacter_avertissement' => 'De aandacht om niet deze opties tijdens de ontwikkeling van uw site te activeren: de compact elementen verliezen elke leesbaarheid.',
	'texte_compacter_script_css' => 'SPIP kan compact de javascript scripts en de CSS stijl bladen, om ze te registreren in statische bestanden; dat versnelt de display van de site.',
	'texte_compresseur_page' => 'SPIP kan elke bladzijde automatisch samenpersen die hij naar
 bezoekers van de site verzendt. Dit regelen maakt het mogelijk om de drukke reep te optimaliseren (de
 site is sneller achter een verbinding aan gering debiet), maar
 vraagt meer macht aan de server.',
	'titre_compacter_script_css' => 'Verdichting van de scripts en CSS',
	'titre_compresser_flux_http' => 'Samendrukking van de HTTP stroom' # MODIF
);

?>
