<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'info_compresseur_titre' => 'Оптимизация и сжатие',
	'info_question_activer_compactage_css' => 'Хотите ли Вы включить сжатие таблиц стиля CSS?', # MODIF
	'info_question_activer_compactage_js' => 'Хотите ли Вы включить сжатие файлов Javascript?', # MODIF
	'info_question_activer_compresseur' => 'Хотите ли Вы включить сжатие данных HTTP?', # MODIF
	'item_compresseur_closure' => 'Utiliser Google Closure Compiler [expérimental]', # NEW
	'item_compresseur_css' => 'Activer la compression des feuilles de styles (CSS)', # NEW
	'item_compresseur_html' => 'Activer la compression du HTML', # NEW
	'item_compresseur_js' => 'Activer la compression des scripts (javascript)', # NEW

	// T
	'texte_compacter_avertissement' => 'Будьте осторожны, не включайте эти опции во время усовершенствования Вашего сайта: сжатые элементы станут трудными для чтения и исправления.',
	'texte_compacter_script_css' => 'SPIP может сжать Javascript файлы и CSS таблицы стилей и сохранить их как статические файлы. Это ускорит показ сайта.',
	'texte_compresseur_page' => 'SPIP может автоматически сжимать каждую страницу, которая отправляется. Эта опция уменьшает использованную пропускную способность, делая сайт быстрее при более низкой скорости соединения), но это требует больше ресурсов сервера.',
	'titre_compacter_script_css' => 'Сжатие скриптов и CSS',
	'titre_compresser_flux_http' => 'Сжатие данных HTTP ' # MODIF
);

?>
