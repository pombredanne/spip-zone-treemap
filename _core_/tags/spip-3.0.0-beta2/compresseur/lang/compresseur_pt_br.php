<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'info_compresseur_titre' => 'Otimizações e compressão',
	'info_question_activer_compactage_css' => 'Você deseja ativar a compactação das folhas de estilo (CSS)?', # MODIF
	'info_question_activer_compactage_js' => 'Você deseja ativar a compactação dos scripts (javascript)?', # MODIF
	'info_question_activer_compresseur' => 'Você deseja ativar a compressão do fluxo HTTP?', # MODIF
	'item_compresseur_closure' => 'Utiliser Google Closure Compiler [expérimental]', # NEW
	'item_compresseur_css' => 'Activer la compression des feuilles de styles (CSS)', # NEW
	'item_compresseur_html' => 'Activer la compression du HTML', # NEW
	'item_compresseur_js' => 'Activer la compression des scripts (javascript)', # NEW

	// T
	'texte_compacter_avertissement' => 'Atenção para não ativar estas opções durante o desenvolvimento do seu site: os elementos compactados perdem toda a legibilidade.',
	'texte_compacter_script_css' => 'O SPIP pode compactar os scripts javascript e as folhas de estilo CSS, para gravá-los nos arquivos de estatísticas; isto acelera a exibição do site.',
	'texte_compresseur_page' => 'O SPIP pode comprimir automaticamente cada página que ele envia aos visitantes do site. Esta regulagem permite otimizar a banda de transmissão (le site torna-se mais rápido quando estiver sob uma conexão de banda baixa), mas demanda mais potência do servidor.',
	'titre_compacter_script_css' => 'Compactação de scripts e CSS',
	'titre_compresser_flux_http' => 'Compressão do fluxo HTTP' # MODIF
);

?>
