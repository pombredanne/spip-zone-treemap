<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'info_compresseur_titre' => 'Optimierung und Kompression',
	'info_question_activer_compactage_css' => 'Möchten Sie die Stylesheets (CSS) komprimieren?', # MODIF
	'info_question_activer_compactage_js' => 'Möchten Sie die Javascripte komprimieren ?', # MODIF
	'info_question_activer_compresseur' => 'Möchten sie die HTTP-Übertragung komprimieren?', # MODIF
	'item_compresseur_closure' => 'Utiliser Google Closure Compiler [expérimental]', # NEW
	'item_compresseur_css' => 'Activer la compression des feuilles de styles (CSS)', # NEW
	'item_compresseur_html' => 'Activer la compression du HTML', # NEW
	'item_compresseur_js' => 'Activer la compression des scripts (javascript)', # NEW

	// T
	'texte_compacter_avertissement' => 'Achtung: Bitte aktivieren Sie diese Option nicht während der Entwicklung Ihrer Site. Die komprimierten Elemente verlieren dadurch ihre Lesbarkeit.',
	'texte_compacter_script_css' => 'SPIP kann Javascripte und CSS Stilvorlagen komprimieren. Dadurch wird die Anzeigegeschwindigkeit der Website erhöht.',
	'texte_compresseur_page' => 'SPIP kann alle Seiten, die zum Besucher übertragen werden
komprimieren.Diese Einstellung ermöglicht es, Bandbreite zu sparen (Die
Site wird über langsame Verbindungen schneller angezeigt), benötigt
jedoch zusätzliche Serverleistung.',
	'titre_compacter_script_css' => 'Skripte und CSS komprimieren',
	'titre_compresser_flux_http' => 'HTTP-Übertragung komprimieren' # MODIF
);

?>
