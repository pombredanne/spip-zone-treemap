<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-compresseur
// Langue: fr
// Date: 30-07-2011 15:01:36
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
	'compresseur_description' => 'Compression des css et javascript dans l\'entete des pages html de <code>ecrire/</code> et/ou du site public',
	'compresseur_slogan' => 'Compression des css et javascript',
);
?>