<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-squelettes_par_rubrique
// Langue: fr
// Date: 30-07-2011 15:01:38
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'squelettes_par_rubrique_description' => 'Support des squelettes suffixés par numéro de rubrique et/ou par code de langue :
(-23.html, =23.html, et .en.html)',
	'squelettes_par_rubrique_slogan' => 'Support des squelettes suffixés dans SPIP',
);
?>