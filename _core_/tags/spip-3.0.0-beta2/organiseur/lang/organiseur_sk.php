<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_enregistrer_brouillon' => 'Uložiť koncept',
	'bouton_envoyer_message' => 'Poslať',
	'bouton_envoyer_message_maintenant' => 'Poslať teraz',

	// C
	'cal_jour_entier' => 'Deň',
	'cal_par_jour' => 'deň',
	'cal_par_mois' => 'mesiac',
	'cal_par_semaine' => 'týždeň',

	// E
	'erreur_destinataire_invalide' => 'Príjemca @dest@ nie je platný',

	// I
	'icone_ecrire_nouveau_message' => 'Poslať novú správu',
	'icone_ecrire_nouveau_pensebete' => 'Vytvoriť novú pripomienku',
	'icone_ecrire_nouvelle_annonce' => 'Poslať nový oznam',
	'icone_effacer_message' => 'Vymazať túto správu',
	'icone_modifier_annonce' => 'Upraviť tento oznam',
	'icone_modifier_pensebete' => 'Upraviť túto pripomienku',
	'icone_supprimer_message' => 'Odstrániť túto správu',
	'info_1_message_envoye' => '1 odoslaná správa',
	'info_1_message_nonlu' => '1 nová správa',
	'info_agenda_interne' => 'Interný kalendár',
	'info_message_a' => 'Komu',
	'info_message_date' => 'Dátum',
	'info_message_de' => 'Odosielateľ',
	'info_message_non_lu' => 'Nová správa',
	'info_message_objet' => 'Objekt',
	'info_nb_messages_envoyes' => '@nb@ odoslaných správ',
	'info_nb_messages_nonlus' => '@nb@ nových správ',
	'info_selection_annonces' => 'Oznamy',
	'info_selection_messages' => 'Správy',
	'info_selection_pensebetes' => 'Pripomienky',
	'info_type_message_affich' => 'Oznam',
	'info_type_message_normal' => 'Správa',
	'info_type_message_pb' => 'Pripomienka',
	'info_type_message_rv' => 'Stretnutie',

	// L
	'label_destinataires' => 'Do',
	'label_texte' => 'Text',
	'label_titre' => 'Predmet',
	'loading' => 'Spúšťa sa...',

	// M
	'message' => 'Správa',
	'messages' => 'Správy',

	// N
	'notification_annonce_lire_a_ladresse' => 'To si môžete prečítať na adrese @url@.',
	'notification_annonce_publie_1' => '[@nom_site_spip@] Všeobecný oznam',
	'notification_message_lire_a_ladresse' => 'Môžete si to prečítať a odpovedať na to na adrese @url@.',
	'notification_message_publie_1' => '[@nom_site_spip@] Nová správa',
	'notification_message_recu_de' => '@nom@ vám poslal(a) novú správu.',

	// T
	'texte_message_brouillon' => 'Táto správa je uložená ako koncept',
	'titre_agenda_rv' => 'Stretnutia',
	'titre_boite_envoi' => 'Na odoslanie',
	'titre_boite_reception' => 'Doručená pošta'
);

?>
