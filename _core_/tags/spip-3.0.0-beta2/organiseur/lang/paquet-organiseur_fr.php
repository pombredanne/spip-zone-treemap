<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-organiseur
// Langue: fr
// Date: 30-07-2011 15:01:38
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// O
	'organiseur_description' => 'Outils de travail éditorial en groupe',
	'organiseur_slogan' => 'Outils de travail éditorial en groupe',
);
?>