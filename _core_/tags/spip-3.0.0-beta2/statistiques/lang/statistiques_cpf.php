<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Effacer toutes les statistiques', # NEW
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Lévolisyon do bann vizit<br />@visites@ visites',
	'icone_repartition_actuelle' => 'Armontr kouk sa lé partazé astèr ',
	'icone_repartition_visites' => 'Koman i partaz bann vizit',
	'icone_statistiques_visites' => 'Bann sifklé bann vizit',
	'info_affichier_visites_articles_plus_visites' => 'Armontr bann vizit pou <b>bann zartik lé pli vizité dopwi lo komansman :</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Koman i pé lir lo tablo-la',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'Bann sifklé pou bann visit',
	'info_popularite_2' => 'Popularité lo sitwèb :',
	'info_popularite_3' => 'Koman lé populèr : @popularite@ ; bann vizit : @visites@',
	'info_popularite_5' => 'popularité :',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_vignettes_referer' => 'Lorsque vous consultez les statistiques, vous pouvez visualiser des aperçus des sites d\'origine des visites', # NEW
	'info_question_vignettes_referer_oui' => 'Afficher les captures des sites d\'origine des visites', # NEW
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'bann vizit :',
	'info_visites_plus_populaires' => 'Afis bann vizit pou <b>bann zartik sak lé pli populèr</b> e <b>bann dernyé zartik an liyn :</b>',
	'info_zoom' => 'zoum',
	'item_gerer_statistiques' => 'Zèr bann sifklè pou bann vizit', # MODIF

	// O
	'onglet_origine_visites' => 'Lorizin bann vizit',
	'onglet_repartition_debut' => 'dopwi komansman',
	'onglet_repartition_lang' => 'Bann vizit sanm tout bann lang',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Cette commande efface toutes les données liées aux statistiques de visite du site, y compris la popularité des articles.', # NEW
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'Dann lamarz-la, ou va trouv lo rand lartik pou vwar kouk sa lé populèr ; pou sa minm, kank
ou bat-karé si lo non inn lartik, néna inn bil sak i aparé : dann bil-la, i va trouv la popularité lartik èk lo nonb vizit dopwi lorizin.La popularité lé kom inn prévizyon
lo nonb vizit posib sir lo sit porézon lo ritm aktyèl lé stab.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'Bann bar fénwar i roprézant  bann zantré kimilé (pou lansanm bann sourubrik), bann bar klèr lo nonb bann vizit pou sakinn rubrik.',
	'titre_evolution_visite' => 'Lévolisyon do bann vizit',
	'titre_liens_entrants' => 'Bann gatiraz larivé',
	'titre_page_statistiques' => 'Bann sifklé sanm inn rubrik',
	'titre_page_statistiques_visites' => 'Bann sifklé pou bann vizit',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
