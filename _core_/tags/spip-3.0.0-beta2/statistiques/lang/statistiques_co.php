<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Sguassà e statìstiche', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Evuluzione di e vìsite<br />@visites@ vìsite',
	'icone_repartition_actuelle' => 'Vede u spartimentu attuale',
	'icone_repartition_visites' => 'Spartimentu di e vìsite',
	'icone_statistiques_visites' => 'Statìstiche',
	'info_affichier_visites_articles_plus_visites' => 'Vede e vìsite per <b>l\'artìculu i più letti dipoi u principiu di u situ :</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Cumu leghje stu tavulone ?',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'Statìstiche di e vìsite',
	'info_popularite_2' => 'pupularità di u situ : ',
	'info_popularite_3' => 'pupularità : @popularite@ ; vìsite : @visites@',
	'info_popularite_5' => 'pupularità :',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_vignettes_referer' => 'Cunsultendu e statìstiche, pudete puru vede in antìcipu siti d\'orìgine di e vìsite',
	'info_question_vignettes_referer_oui' => 'Vede a cattura di screnu di i siti d\'orìgine di e vìsite',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'vìsite :',
	'info_visites_plus_populaires' => 'Vede e vìsite per <b>l\'artìculi i più pupulari</b> è per <b>l\'ùltimi artìculi pubblicati :</b>',
	'info_zoom' => 'zoom',
	'item_gerer_statistiques' => 'Gestisce e statìstiche', # MODIF

	// O
	'onglet_origine_visites' => 'Urìgine di e vìsite',
	'onglet_repartition_debut' => 'da u principiu',
	'onglet_repartition_lang' => 'Scumpartimentu per lingue',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Sta cumanda sguassa tutti i dati leati à e statìstiche di vìsite di u situ, cumpresa a pupularità di l\'artìculi.',
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'U rangu di l\'artìculu in a classìfica pè pupularità hè indettatu quì sopra. A pupularità di l\'artìculu (stimata da 
	u nùmeru di vìsite cutidianu ch\'ellu riceverà l\'artìculu s\'ellu ferma uguale u rìtimu attuale di cunsultazione) è u nùmeru di vìsite ricevute 
	dipoi u principiu sò da vede in a scatuletta chì s\'apre quandu omu passa u topu sopr\'à u tìtulu.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'L\'aste più scure riprisentanu l\'entrate cumulate (per u tutale di e sotturùbbriche), l\'aste più chjare u nùmeru di vìsite 
		per ogni rùbbrica.',
	'titre_evolution_visite' => 'Evuluzione di e vìsite',
	'titre_liens_entrants' => 'Lee versu u situ',
	'titre_page_statistiques' => 'Statìstiche per rùbbrica',
	'titre_page_statistiques_visites' => 'Statìstiche di vìsite',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
