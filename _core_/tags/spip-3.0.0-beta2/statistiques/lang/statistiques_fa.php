<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'محو كردن آمارها', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'نمودار بازديدها <br /> @visites@ بازديد  ',
	'icone_repartition_actuelle' => 'نمايش توزيع كنونى',
	'icone_repartition_visites' => 'نمودار بازديدها',
	'icone_statistiques_visites' => 'آمار بازديد كنندگان',
	'info_affichier_visites_articles_plus_visites' => 'فهرست مقاله هائى كه بيش از ديگر مقاله ها خوانده شدن از ابتدا نشان دهيد',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'طرز خواندن اين جدول',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'آمار بازديد كنند گان',
	'info_popularite_2' => ': محبوبيت سايت',
	'info_popularite_3' => '@popularite@ : محبوبيت سايت @visites@ : بازديد',
	'info_popularite_5' => ': محبوبيت',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_gerer_statistiques' => 'آيا ميخواهيد سايتتان آمار بينند گان را اداره كند؟',
	'info_question_vignettes_referer' => 'هنگام مراجعه به آمارها، مي‌توانيد نشاني سايت‌هاي مبداء بازديد‌كنندگان را ببينيد.',
	'info_question_vignettes_referer_oui' => 'گيراندازي سايت‌هاي مبداء بازديدكنندگان را نشان دادن',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => ':بازديد ',
	'info_visites_plus_populaires' => 'نمودار بازديد محبوب ترين مقالات و آخرين مقالات منتشر شده را نمايش دهيد ',
	'info_zoom' => 'زوم',
	'item_gerer_statistiques' => 'آمار را اداره كنيد ',

	// O
	'onglet_origine_visites' => 'مبدأ بازديدها',
	'onglet_repartition_debut' => 'از زمان شروع',
	'onglet_repartition_lang' => 'با زبانهاى',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'اين فرمان تمام داده‌هاي مرتبط با آمارهاي بازديد‌ها از اين سايت و از جمله محبوبيت نسبي مقالات را پاك مي‌كند.',
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'رتبه مقاله در رده بندى محبوبيتها در حاشيه صفحه مشخص شده كه نشانگر تعداد بازديدهاى روزانه مقاله ميباشد وتعداد بازديدها از ابتدا با قرار دادن نوك فلش برروى مقاله مشخص خواهد شد',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'خطوط پر رنگ نمودار وروديها (مجموع زير بخشها) و خطوط روشن نشانگر تعداد بازديدكنندگان هر بخش ميباشد.',
	'titre_evolution_visite' => 'تحول بازديدها',
	'titre_liens_entrants' => 'پيوندهاى ورودى ',
	'titre_page_statistiques' => 'آمار براى هر بخش',
	'titre_page_statistiques_visites' => 'آمار بازديدها',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
