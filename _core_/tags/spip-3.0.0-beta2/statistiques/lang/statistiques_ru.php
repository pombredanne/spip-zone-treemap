<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Удалить статистику', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Уровень посещений<br />@visites@ ',
	'icone_repartition_actuelle' => 'На данный момент',
	'icone_repartition_visites' => 'Статистика посещений',
	'icone_statistiques_visites' => 'Статистика',
	'info_affichier_visites_articles_plus_visites' => 'Самые посещаемые статьи с начала работы сайта: </b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Как прочитать этот график',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'История посещений',
	'info_popularite_2' => 'популярность сайта:',
	'info_popularite_3' => 'популярность: @popularite@; посещения: @visites@',
	'info_popularite_5' => 'популярность:',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_gerer_statistiques' => 'Должен ли Ваш сайт включить историю посещений?',
	'info_question_vignettes_referer' => 'Когда Вы принимаете во внимание статистику, Вы можете просмотреть текущие посещения сайтов. ',
	'info_question_vignettes_referer_oui' => 'Показывать изображения главной страницы',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'посещения:',
	'info_visites_plus_populaires' => 'Показать статистику для <b> самых популярных </b> и <b>последних опубликованных статей:</b>',
	'info_zoom' => 'увеличить',
	'item_gerer_statistiques' => 'Вести статистику',

	// O
	'onglet_origine_visites' => 'Источники переходов',
	'onglet_repartition_debut' => 'с начала',
	'onglet_repartition_lang' => 'Распределение по языкам',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => '<MODIF>Это команда удаляет все данные, которые связаны со статистикой посещений сайта,включая сравнительную популярность статей. ',
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'Справа 
 показывается уровень 
  посещаемости статьи; черной линией отображается прогноз посещений (при условии 
 что сохранится 
  текущая динамика), и зеленым цветом - количество посещений за день. 
 Для более подробной информации 
 наведите мышку на интересующую Вас дату. ',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'Темные штрихи представляют накопленные записи (общее количество подразделов), светлые штрихи, представляют количество посещений для каждого раздела.',
	'titre_evolution_visite' => 'Уровень посещений',
	'titre_liens_entrants' => 'Источники переходов',
	'titre_page_statistiques' => 'Статистика разделов',
	'titre_page_statistiques_visites' => 'История посещений',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
