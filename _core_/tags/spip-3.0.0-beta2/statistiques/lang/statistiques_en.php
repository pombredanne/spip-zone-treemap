<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Delete only incoming links',
	'bouton_effacer_statistiques' => 'Delete all statistics',
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'Uncompressed backup in @fichier@',

	// C
	'csv' => 'csv',

	// I
	'icone_evolution_visites' => 'Amount of visits <br />@visites@ visits',
	'icone_repartition_actuelle' => 'Show current distribution',
	'icone_repartition_visites' => 'Distribution of Visits',
	'icone_statistiques_visites' => 'Statistics',
	'info_affichier_visites_articles_plus_visites' => 'Show visits for <b>the most popular articles of all time:</b>',
	'info_base_restauration' => 'The database is being restored.',
	'info_comment_lire_tableau' => 'How to read this graph',
	'info_erreur_restauration' => 'Restoration error: file does not exist.',
	'info_forum_statistiques' => 'Visit statistics',
	'info_popularite_2' => 'site popularity:',
	'info_popularite_3' => 'popularity: @popularite@; visits: @visites@',
	'info_popularite_5' => 'popularity:',
	'info_previsions' => 'forecasts:',
	'info_question_vignettes_referer' => 'When you consult the statistics, you can see a preview of any referring sites from which a visitor came. ',
	'info_question_vignettes_referer_oui' => 'Show screenshots of referring sites',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Backup the articles',
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Backup the autors',
	'info_sauvegarde_breves' => 'Backup the news items',
	'info_sauvegarde_documents' => 'Backup the documents',
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Backup the forums',
	'info_sauvegarde_groupe_mots' => 'Backup the keyword groups',
	'info_sauvegarde_messages' => 'Backup the messages',
	'info_sauvegarde_mots_cles' => 'Backup the keywords',
	'info_sauvegarde_petitions' => 'Backup the petitions',
	'info_sauvegarde_refers' => 'Backup the referers',
	'info_sauvegarde_reussi_01' => 'Successful backup.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Backup the sections',
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'visits:',
	'info_visites_plus_populaires' => 'Show visits for <b>the most popular articles</b> and <b>the most recent articles:</b>',
	'info_zoom' => 'zoom',
	'item_gerer_statistiques' => 'Manage visitor statistics',

	// O
	'onglet_origine_visites' => 'Visitors came from',
	'onglet_repartition_debut' => 'of all time',
	'onglet_repartition_lang' => 'Distribution by language',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventually, the original site URL:',

	// T
	'texte_admin_effacer_stats' => 'This command deletes all statistics on visits to the site, including article popularity.',
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'Articles are ranked by popularity.
 Mouseover any article\'s title to
 show its popularity (i.e. estimated number
 of daily visits it will have if the
 current trend continues), and the
 number of visits recorded since it was
 first published.',
	'texte_sauvegarde_compressee' => 'The backup will be done in the uncompressed file @fichier@.',
	'texte_signification' => 'Dark bars represent cumulative entries (total subsections), light bars represent the number of visits for each section.',
	'titre_evolution_visite' => 'Visitor Statistics',
	'titre_liens_entrants' => 'Incoming links',
	'titre_page_statistiques' => 'Statistics by section',
	'titre_page_statistiques_visites' => 'Visit statistics',

	// V
	'visites_journalieres' => 'Number of visits per day',
	'visites_mensuelles' => 'Number of visits per month'
);

?>
