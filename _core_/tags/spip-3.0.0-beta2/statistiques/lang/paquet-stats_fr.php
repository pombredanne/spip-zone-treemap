<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-stats
// Langue: fr
// Date: 03-08-2011 19:45:21
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'stats_description' => 'Statistiques de SPIP',
	'stats_nom' => 'Statistiques',
	'stats_slogan' => 'Gestion des statistiques dans SPIP',
);
?>