<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Ştergeţi statisticile', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Evoluţia vizitelor<br />@visites@ vizite',
	'icone_repartition_actuelle' => 'Afişaţi repartiţia curentă',
	'icone_repartition_visites' => 'Repartiţia vizitelor',
	'icone_statistiques_visites' => 'Statistici',
	'info_affichier_visites_articles_plus_visites' => 'Afişează vizitele pentru <b>cele mai vizitate articole de la început:</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Cum se citeşte acest tablou',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'Statisticile vizitelor',
	'info_popularite_2' => 'popularitatea site-ului :',
	'info_popularite_3' => 'popularitate : @popularite@ ; vizites : @visites@',
	'info_popularite_5' => 'popularitate :',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_vignettes_referer' => 'Atunci când vizualizaţi statisticile, puteţi vizualiza pre-vederi ale site-uri de unde provin vizitele',
	'info_question_vignettes_referer_oui' => 'Afişaţi capturile site-urilor de unde provin vizitele',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'vizite :',
	'info_visites_plus_populaires' => 'Afişaţi vizitele pentru <b>articolele cele mai populare</b> şi pentru <b>ultimele articole publicate</b> :',
	'info_zoom' => 'mărire',
	'item_gerer_statistiques' => 'Gestionaţi statisticile', # MODIF

	// O
	'onglet_origine_visites' => 'Originea vizitelor',
	'onglet_repartition_debut' => 'de la început',
	'onglet_repartition_lang' => 'Repartiţia pe limbi',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Această comandă şterge toate datele legate de statisticile vizitelor în site, incluzând şi datele de popularitate a articolelor.',
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'Rangul articolului,
  în clasamentul de popularitate este indicat cu o marjă ;
  popularitatea articolului (care este o estimare a numărului de vizite zilnice pe care articolul le va primi dacă ritmul actual de consultare se menţine) şi numărul de vizite primite
  de la început sunt afişate în mica fereastră care apare la un survol al mouse-ului deasupra titlului.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'Barele întunecate reprezintă intrarile cumulate (totalul sub-rubricilor), barele  deschise la culoare reprezintă numărul de vizite pentru fiecare rubrică.',
	'titre_evolution_visite' => 'Evoluţia vizitelor',
	'titre_liens_entrants' => 'Legături în intrare',
	'titre_page_statistiques' => 'Statisticile pe rubrici',
	'titre_page_statistiques_visites' => 'Statisticile vizitelor',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
