<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'លុបចេញ ស្ថិតិ', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'ការវិវត្ត នៃចំណូលមើល<br />@visites@ ចំណូលមើល',
	'icone_repartition_actuelle' => 'បង្ហាញចេញ ​បំណែងចែក​បច្ចុប្បន្ន',
	'icone_repartition_visites' => 'បំណែងចែក ការ​ទស្សនា',
	'icone_statistiques_visites' => 'ស្ថិតិ',
	'info_affichier_visites_articles_plus_visites' => 'បង្ហាញចំណូលមើល សំរាប់ <b>អត្ថបទ ត្រូវបានចូលមើល ច្រើនបំផុត ពីដើម ៖</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'ធ្វើមេចអាន តារាងនេះ',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'ស្ថិតិ ចំណូលមើល',
	'info_popularite_2' => 'ភាពប្រជាប្រិយ នៃសៃថ៍ ៖',
	'info_popularite_3' => 'ភាពប្រជាប្រិយ ៖  @popularite@; ចំណូលមើល ៖ @visites@',
	'info_popularite_5' => 'ប្រជាប្រិយភាព៖',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_vignettes_referer' => 'Lorsque vous consultez les statistiques, vous pouvez visualiser des aperçus des sites d\'origine des visites', # NEW
	'info_question_vignettes_referer_oui' => 'Afficher les captures des sites d\'origine des visites', # NEW
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'ការ​ទស្សនា ៖',
	'info_visites_plus_populaires' => 'បង្ហាញ​ចេញ ​ការ​ទស្សនា​ទាំងឡាយ សំរាប់<b>អត្ថបទ​ដែល​ប្រជាប្រិយ​បំផុត</b> និង​ សំរាប់ <b>អត្ថបទ ត្រូវបានផ្សព្វផ្សាយ ថ្មីៗ ៖</b>',
	'info_zoom' => 'ពង្រីក',
	'item_gerer_statistiques' => 'គ្រប់គ្រង​ស្ថិតិ', # MODIF

	// O
	'onglet_origine_visites' => 'ភាព​ដើម នៃការទស្សនា',
	'onglet_repartition_debut' => 'តាំងពី​ដំបូង',
	'onglet_repartition_lang' => 'បំណែងចែក ​តាមភាសា',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Cette commande efface toutes les données liées aux statistiques de visite du site, y compris la popularité des articles.', # NEW
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'កំរិត​នៃ​អត្ថបទ នៅ​ក្នុង​ប្រភេទ​ភាព​ពេញ​និយម ត្រូវ​បាន​បង្ហាញ​នៅ​ក្នុង​រឹម​ទំព័រ​។ ភាព​ពេញ​និយម​របស់​អត្ថបទ (​ចំនួន​ប៉ាន់​ប្រមាណ​នៃ​ការ​ទស្សនា​ប្រចាំ​ថ្ងៃ​ដែល​វា​មាន​ប្រសិនបើ​ល្បឿន​ជាក់ស្ដែង​នៃ​ចរាចរ​សេវាកម្ម​ត្រូវ​បាន​រក្សា​) ហើយ​ចំនួន​នៃ​ការ​ទស្សនា​ដែល​កត់ត្រា​តាំងពី​ពេល​ចាប់ផ្ដើម​ត្រូវ​បាន​បង្ហាញ​នៅ​ក្នុង​បា​ឡូង​ដែល​មាន​សភាព​ជា​រូប​សំកាំង​នៅ​លើ​ចំណងជើង​។',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'របារ​ខ្មៅ​តំណាង​ឱ្យ​ពាក្យ​រួម (​សរុប​នៃ​ផ្នែក​រង​) របារ​មិនសូវ​ដិត​តំណាង​ឱ្យ​ចំនួន​នៃ​ការ​ទស្សនា​របស់​ផ្នែក​នីមួយៗ​។',
	'titre_evolution_visite' => 'ការវិវត្ត នៃចំណូលមើល',
	'titre_liens_entrants' => 'តំណភ្ជាប់ចូល',
	'titre_page_statistiques' => 'ស្ថិតិ តាមផ្នែក',
	'titre_page_statistiques_visites' => 'ស្ថិតិ ចំណូលមើល',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
