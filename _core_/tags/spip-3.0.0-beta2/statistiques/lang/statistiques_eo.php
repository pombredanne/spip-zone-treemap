<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Forviŝi statistikojn', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Evoluado de la vizitoj<br />@visites@ vizitoj',
	'icone_repartition_actuelle' => 'Afiŝi la nunan distribuon',
	'icone_repartition_visites' => 'Vizitoj-distribuo',
	'icone_statistiques_visites' => 'Statistikoj',
	'info_affichier_visites_articles_plus_visites' => 'Afiŝi la vizitojn por la <b>plej vizititaj artikoloj ekde la komenco :</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Kiel legi tiun ĉi tabelon',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'Statistikoj pri vizitoj',
	'info_popularite_2' => 'populareco de la retejo :',
	'info_popularite_3' => 'populareco : @popularite@ ; vizitoj : @visites@',
	'info_popularite_5' => 'populareco :',
	'info_previsions' => 'prévisions :', # NEW
	'info_question_vignettes_referer' => 'Kiam vi konsultas statistikojn, vi povas havi superrigardon pri tio, de kiuj retejoj venas vizitantoj',
	'info_question_vignettes_referer_oui' => 'Vidigi de kiuj retejoj venas vizitantoj',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'vizitoj :',
	'info_visites_plus_populaires' => 'Afiŝi la vizitojn por <b>la plej popularaj artikoloj</b> kaj por <b>la laste publikigitaj artikoloj :</b>',
	'info_zoom' => 'zomo',
	'item_gerer_statistiques' => 'Mastrumi statistikojn', # MODIF

	// O
	'onglet_origine_visites' => 'Deveno de la vizitintoj',
	'onglet_repartition_debut' => 'dekomence',
	'onglet_repartition_lang' => 'Distribuo laŭ lingvoj',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Cette commande efface toutes les données liées aux statistiques de visite du site, y compris la popularité des articles.', # NEW
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'La rango de la artikolo,
  en la klasifiko laŭ populareco, estas indikita en la
  marĝeno ; la populareco de la artikolo (taksado de la
  nombro de ĉiutagaj vizitoj, kiun ĝi atingos se la nuna vizit-ritmo
  tiel daŭros) kaj la nombro de vizitoj ricevitaj
  de la komenco afiŝiĝas en la veziko kiu
  aperas kiam la tajpmontrilo superpasas la titolon.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'La ruĝaj stangoj reprezentas la sumigitajn datenojn (sumo de la sub-rubrikoj), la helaj stangoj, la nombron de vizitoj por ĉiu rubriko.',
	'titre_evolution_visite' => 'Evoluo de la vizitoj',
	'titre_liens_entrants' => 'Enirintaj ligoj',
	'titre_page_statistiques' => 'Statistikoj laŭ rubrikoj',
	'titre_page_statistiques_visites' => 'Statistikoj de la vizitoj',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
