<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Odstrániť len prichádzajúce odkazy',
	'bouton_effacer_statistiques' => 'Vymazať všetky štatistiky',
	'bouton_radio_sauvegarde_compressee' => 'komprimovaná záloha v súbore @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'dekomprimovaná záloha v súbore @fichier@',

	// C
	'csv' => 'csv',

	// I
	'icone_evolution_visites' => 'Počet návštev<br />@visites@ návštev',
	'icone_repartition_actuelle' => 'Zobraziť aktuálnu distribúciu',
	'icone_repartition_visites' => 'Distribúcia návštev',
	'icone_statistiques_visites' => 'Štatistika',
	'info_affichier_visites_articles_plus_visites' => 'Zobraziť návštevy <b>najnavštevovanejších článkov od začiatku:</b>',
	'info_base_restauration' => 'Prebieha obnova databázy.',
	'info_comment_lire_tableau' => 'Ako čítať tento graf',
	'info_erreur_restauration' => 'Chyba pri obnove: súbor neexistuje.',
	'info_forum_statistiques' => 'Štatistika návštev',
	'info_popularite_2' => 'popularita stránky:',
	'info_popularite_3' => 'popularita: @popularite@, návštev: @visites@',
	'info_popularite_5' => 'popularita:',
	'info_previsions' => 'predpovede:',
	'info_question_vignettes_referer' => 'Keď sa pozriete do štatistiky, uvidíte ukážku každej stránky, z ktorej prišiel nejaký návštevník. ',
	'info_question_vignettes_referer_oui' => 'Zobraziť obrázky odkazujúcich stránok',
	'info_restauration_sauvegarde_insert' => 'Vloženie archívu @archive@ do databázy',
	'info_sauvegarde_articles' => 'Zálohovať články',
	'info_sauvegarde_articles_sites_ref' => 'Zálohovať články odkazovaných stránok',
	'info_sauvegarde_auteurs' => 'Zálohovať autorov',
	'info_sauvegarde_breves' => 'Zálohovať novinky',
	'info_sauvegarde_documents' => 'Zálohovať dokumenty',
	'info_sauvegarde_echouee' => 'Ak sa zálohu nepodarí vytvoriť ("Prekročenie maximálneho času na vykonanie operácie"),',
	'info_sauvegarde_forums' => 'Zálohovať diskusné fóra',
	'info_sauvegarde_groupe_mots' => 'Zálohovať skupiny kľúčových slov',
	'info_sauvegarde_messages' => 'Zálohovať správy',
	'info_sauvegarde_mots_cles' => 'Zálohovať kľúčové slová',
	'info_sauvegarde_petitions' => 'Zálohovať petície',
	'info_sauvegarde_refers' => 'Zálohovať referery',
	'info_sauvegarde_reussi_01' => 'Záloha bola úspešne vytvorená.',
	'info_sauvegarde_rubrique_reussi' => 'Tabuľky v rubrike @titre@ boli uložené do archívu @archive@. Môžete',
	'info_sauvegarde_rubriques' => 'Zálohovať rubriky',
	'info_sauvegarde_signatures' => 'Zálohovať podpisy pod petície',
	'info_sauvegarde_sites_references' => 'Zálohovať odkazované stránky',
	'info_sauvegarde_type_documents' => 'Zálohovať typy dokumentov',
	'info_sauvegarde_visites' => 'Zálohovať návštevy',
	'info_visites' => 'Návštev:',
	'info_visites_plus_populaires' => 'Zobraziť návštevy <b>najčítanejších</b> a <b>najnovších publikovaných článkov:</b>',
	'info_zoom' => 'lupa',
	'item_gerer_statistiques' => 'Riadiť štatistiky návštev',

	// O
	'onglet_origine_visites' => 'Pôvod návštev',
	'onglet_repartition_debut' => 'v každom čase',
	'onglet_repartition_lang' => 'Distribúcia podľa jazyka',

	// R
	'resume' => 'Zhrnutie',

	// S
	'sauvegarde_fusionner' => 'Zlúčiť aktuálnu databázu so zálohou',
	'sauvegarde_fusionner_depublier' => 'Zrušiť publikovanie zlúčených objektov',
	'sauvegarde_url_origine' => 'Pre prípad potreby pôvodná adresa stránky:',

	// T
	'texte_admin_effacer_stats' => 'Tento príkaz vymaže všetky štatistiky o návštevách na stránke, vrátane popularity článkov.',
	'texte_admin_effacer_toutes_stats' => 'Prvé tlačidlo vymaže všetky štatistiky: návštevy, údaje o popularite článkov a prichádzajúcich odkazoch.',
	'texte_admin_tech_03' => 'Môžete sa rozhodnúť, že súbor uložíte v komprimovanej podobe,
 aby ste skrátili čas jeho prenosu na server so zálohou alebo z neho a ušetrili miesto na disku.',
	'texte_admin_tech_04' => 'Cieľ zlúčenia s inou databázou, zálohu môžete obmedziť na nejakú rubriku:',
	'texte_comment_lire_tableau' => 'Články sa hodnotia podľa popularity.
 Ak prejdete myšou po názve nejakého článku,
zobrazí sa jeho popularita (tzn. približný počet
 návštev za deň, ktorý bude mať, ak bude pokračovať súčasný trend)
  a počet návštev zaznamenaných odvtedy, čo bol prvýkrát publikovaný.',
	'texte_sauvegarde_compressee' => 'Záloha sa vytvorí v dekomprimovanom súbore @fichier@.',
	'texte_signification' => 'Tmavé čiary znázorňujú kumulatívne vstupy (celkom za podrubriky), svetlé čiary znázorňujú počet návštev každej rubriky.',
	'titre_evolution_visite' => 'Úroveň návštev',
	'titre_liens_entrants' => 'Prichádzajúce odkazy',
	'titre_page_statistiques' => 'Štatistiky podľa rubrík',
	'titre_page_statistiques_visites' => 'Štatistiky návštev',

	// V
	'visites_journalieres' => 'Počet návštev za deň',
	'visites_mensuelles' => 'Počet návštev za mesiac'
);

?>
