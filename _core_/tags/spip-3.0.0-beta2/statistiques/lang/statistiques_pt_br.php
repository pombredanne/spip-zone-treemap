<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_effacer_referers' => 'Effacer seulement les liens entrants', # NEW
	'bouton_effacer_statistiques' => 'Apagar as estatísticas', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'sauvegarde compressée sous @fichier@', # NEW
	'bouton_radio_sauvegarde_non_compressee' => 'sauvegarde non compressée sous @fichier@', # NEW

	// C
	'csv' => 'csv', # NEW

	// I
	'icone_evolution_visites' => 'Evolução das visitas<br />@visites@ visitas',
	'icone_repartition_actuelle' => 'Exibir a repartição atual',
	'icone_repartition_visites' => 'Repartição das visitas',
	'icone_statistiques_visites' => 'Estatísticas',
	'info_affichier_visites_articles_plus_visites' => 'Exibir as visitas para <b>as matérias mais visitada após o lançamento:</b>',
	'info_base_restauration' => 'La base est en cours de restauration.', # NEW
	'info_comment_lire_tableau' => 'Como ler esta tabela',
	'info_erreur_restauration' => 'Erreur de restauration : fichier inexistant.', # NEW
	'info_forum_statistiques' => 'Estatísticas das visitas',
	'info_popularite_2' => 'popularidade do site:',
	'info_popularite_3' => 'popularidade: @popularite@; visitas: @visites@',
	'info_popularite_5' => 'popularidade:',
	'info_previsions' => 'previsões:',
	'info_question_vignettes_referer' => 'Ao consultar as estatísticas, você poderá visualizar resumos dos sites que originaram as visitas',
	'info_question_vignettes_referer_oui' => 'Exibir as capturas dos sites de origem das visitas',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvegarder les articles', # NEW
	'info_sauvegarde_articles_sites_ref' => 'Sauvegarder les articles des sites référencés', # NEW
	'info_sauvegarde_auteurs' => 'Sauvegarder les auteurs', # NEW
	'info_sauvegarde_breves' => 'Sauvegarder les brèves', # NEW
	'info_sauvegarde_documents' => 'Sauvegarder les documents', # NEW
	'info_sauvegarde_echouee' => 'Si la sauvegarde a échoué («Maximum execution time exceeded»),', # NEW
	'info_sauvegarde_forums' => 'Sauvegarder les forums', # NEW
	'info_sauvegarde_groupe_mots' => 'Sauvegarder les groupes de mots', # NEW
	'info_sauvegarde_messages' => 'Sauvegarder les messages', # NEW
	'info_sauvegarde_mots_cles' => 'Sauvegarder les mots-clés', # NEW
	'info_sauvegarde_petitions' => 'Sauvegarder les pétitions', # NEW
	'info_sauvegarde_refers' => 'Sauvegarder les referers', # NEW
	'info_sauvegarde_reussi_01' => 'Sauvegarde réussie.', # NEW
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvegarder les rubriques', # NEW
	'info_sauvegarde_signatures' => 'Sauvegarder les signatures de pétitions', # NEW
	'info_sauvegarde_sites_references' => 'Sauvegarder les sites référencés', # NEW
	'info_sauvegarde_type_documents' => 'Sauvegarder les types de documents', # NEW
	'info_sauvegarde_visites' => 'Sauvegarder les visites', # NEW
	'info_visites' => 'visitas:',
	'info_visites_plus_populaires' => 'Exibir os visitantes para <b>as matérias mais populares</b> e para <b>as mais recentes matérias publicadas:</b>',
	'info_zoom' => 'zoom',
	'item_gerer_statistiques' => 'Gerenciar as estatísticas', # MODIF

	// O
	'onglet_origine_visites' => 'Origem das visitas',
	'onglet_repartition_debut' => 'desde o início',
	'onglet_repartition_lang' => 'Repartição por idiomas',

	// R
	'resume' => 'Resume', # NEW

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_effacer_stats' => 'Este comando apaga todos os dados ligados às estatísticas de visitação do site, incluindo a popularidade das matérias.',
	'texte_admin_effacer_toutes_stats' => 'Le premier bouton supprime toutes les statistiques : visites, popularité des articles et liens entrants.', # NEW
	'texte_admin_tech_03' => 'Vous pouvez choisir de sauvegarder le fichier sous forme compressée, afin
	d\'écourter son transfert chez vous ou sur un serveur de sauvegardes, et d\'économiser de l\'espace disque.', # NEW
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_comment_lire_tableau' => 'A classificação da matéria, na classificação por popularidade, é indicada na margem; a popularidade de uma matéria (uma estimativa do número de visitas diárias que ela receberia se o ritmo atual de acesso se mantivesse) e o número de visitas recebidas depois do lançamento são exibidas na dica que aparece quando o cursor do mouse se sobrepõe ao título.',
	'texte_sauvegarde_compressee' => 'La sauvegarde sera faite dans le fichier non compressé @fichier@.', # NEW
	'texte_signification' => 'As barras escuras representão as entradas acumuladas (total das subseções), as barras claras, o número de visitas para cada seção.',
	'titre_evolution_visite' => 'Evolução das visitas',
	'titre_liens_entrants' => 'Links de entrada',
	'titre_page_statistiques' => 'Estatísticas por seções',
	'titre_page_statistiques_visites' => 'Estatísticas de visitas',

	// V
	'visites_journalieres' => 'Nombre de visites par jour', # NEW
	'visites_mensuelles' => 'Nombre de visites par mois' # NEW
);

?>
