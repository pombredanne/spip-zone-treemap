<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-revisions
// Langue: fr
// Date: 30-07-2011 15:01:38
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// R
	'revisions_description' => 'Suivi des modifications des objets éditoriaux',
	'revisions_slogan' => 'Suivi des modifications des objets éditoriaux',
);
?>