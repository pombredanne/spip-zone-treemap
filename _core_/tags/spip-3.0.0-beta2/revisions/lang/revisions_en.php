<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_comparer' => 'Show revisions',

	// D
	'diff_para_ajoute' => 'Added paragraph',
	'diff_para_deplace' => 'Moved paragraph',
	'diff_para_supprime' => 'Deleted paragraph',
	'diff_texte_ajoute' => 'Added text',
	'diff_texte_deplace' => 'Moved text',
	'diff_texte_supprime' => 'Deleted text',
	'differences_versions' => 'Différences entre version N<sup>o</sup>@id_version@ et version N<sup>o</sup>@id_diff@', # NEW

	// I
	'icone_restaurer_version' => 'Restaurer la version N<sup>o</sup>@version@', # NEW
	'icone_suivi_revisions' => 'Tracking of the revisions',
	'info_1_revision' => '1 revision',
	'info_aucune_revision' => 'No revision',
	'info_historique' => 'Revisions:',
	'info_historique_lien' => 'Display list of versions',
	'info_historique_titre' => 'Revision tracking',
	'info_nb_revisions' => '@nb@ revisions',

	// L
	'label_choisir_id_version' => 'Select the versions to compare',
	'label_config_revisions_objets' => 'Sur quels objets activer les révisions :', # NEW

	// O
	'objet_editorial' => 'object',

	// P
	'plugin_update' => 'Mise à jour du plugin "Révisions" en version @version@.', # NEW

	// T
	'titre_form_revisions_objets' => 'Enable revisions',
	'titre_restaurer_version' => 'Restore a version',
	'titre_revisions' => 'Revisions',

	// V
	'version_deplace_rubrique' => 'Moved from <b>« @from@ »</b> to <b>« @to@ »</b>.',
	'version_initiale' => 'Initial version',
	'voir_differences' => 'Voir les différences', # NEW
	'voir_mes_revisions' => 'My revisions',
	'voir_revisions' => 'Voir les révisions (@objet@ @id_objet@ : @titre@)', # NEW
	'voir_toutes_les_revisions' => 'All revisions'
);

?>
