<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-petitions
// Langue: fr
// Date: 30-07-2011 15:01:38
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// P
	'petitions_description' => 'Pétitions dans SPIP',
	'petitions_slogan' => 'Gestion des pétitions dans SPIP',
);
?>