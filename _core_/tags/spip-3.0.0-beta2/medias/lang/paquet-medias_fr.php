<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-medias
// Langue: fr
// Date: 30-07-2011 15:01:37
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// M
	'medias_description' => 'Gestion des medias de SPIP',
	'medias_slogan' => 'Gestion des medias dans SPIP',
);
?>