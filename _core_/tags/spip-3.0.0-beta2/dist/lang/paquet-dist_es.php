<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-dist
// Langue: es
// Date: 30-07-2011 15:01:36
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// D
	'dist_description' => 'Juego de esqueletos por defecto en SPIP 3.',
	'dist_slogan' => 'Juego de esqueletos por defecto en SPIP 3',
);
?>