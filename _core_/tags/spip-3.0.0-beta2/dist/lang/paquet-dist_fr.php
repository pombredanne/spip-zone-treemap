<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-dist
// Langue: fr
// Date: 30-07-2011 15:01:36
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// D
	'dist_description' => 'Jeu de squelettes distribué avec SPIP 3.',
	'dist_slogan' => 'Squelettes par défaut de SPIP 3',
);
?>