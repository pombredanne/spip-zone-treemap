<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-mediabox
// Langue: ar
// Date: 03-08-2011 19:45:21
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// M
	'mediabox_description' => 'افتراضياً، يتم إضفاء صندوق الفرجة على كل الوصلات الى الصور (لها نوع خاصية يصف mime/type الصورة) إضافة الى الوصلات المزودة بنمط mediabox.



من الممكن إعداد كل وصلة على حدة من خلال أنماط إضافية:

-* يتيح <code>boxIframe</code>  فتح الوصلة في إطار iframe

-* يتيج <code>boxWidth-350px</code> تحديد عرض ٣٥٠ نقطة للإطار

-* يتيح <code>boxHeight-90pc</code> تحديد ارتفاع ٩٠٪ للإطار



وتتيح لوحة إعداد تعديل الإعدادات العامة حسب الرغبة كما تتيح اختيار شكل الصندوق من بين الأشكال المتاحة',
	'mediabox_nom' => 'صندوق الفرجة',
	'mediabox_slogan' => 'صندوق الفرجة',
);
?>