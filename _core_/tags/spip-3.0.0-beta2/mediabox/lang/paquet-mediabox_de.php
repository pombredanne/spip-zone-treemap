<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-mediabox
// Langue: de
// Date: 03-08-2011 19:45:21
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// M
	'mediabox_nom' => 'MediaBox',
	'mediabox_slogan' => 'Multimedia-Box',
);
?>