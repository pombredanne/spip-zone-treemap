<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-mediabox
// Langue: en
// Date: 30-07-2011 15:01:37
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// M
	'mediabox_description' => 'By default, all links to pictures (with a type attribute describing the mime/type of the picture) and
links with the class <code>.mediabox</code> are enriched by multimedia box.

You can configure each link on a case by case basis with additional classes:
-* <code>boxIframe</code> enables to open link in iframe box; 
-* <code>boxWidth-350px</code> enables to specify a width of 350px for the box;
-* <code>boxHeight-90pc</code> enables to specify a height of 90% for the box

A configuration panel lets you change the general settings to your liking, and the appearance of the box among the available skins.

This plugin works on skeletons which have the <code>#INSERT_HEAD</code> tag.',
	'mediabox_slogan' => 'Media box',
);
?>