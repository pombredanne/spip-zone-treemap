<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_reinitialiser' => 'Obnoviť',
	'boxstr_close' => 'Zatvoriť',
	'boxstr_current' => '{teraz}/{celkom}',
	'boxstr_next' => 'Ďalšia',
	'boxstr_previous' => 'Predchádzajúca',
	'boxstr_slideshowStart' => 'Prezentácia',
	'boxstr_slideshowStop' => 'Zastaviť',
	'boxstr_zoom' => 'Lupa',

	// E
	'explication_selecteur' => 'Uveďte cieľový prvok, ktorý bude ovládať box. (jQuery alebo výraz CSS)',
	'explication_selecteur_galerie' => 'Uveďte cieľové prvky, ktoré sa majú zlúčiť do galérie. (jQuery alebo výraz CSS)',
	'explication_splash_url' => 'Zadajte adresu média, ktoré sa má automaticky zobraziť v boxe pri prvej návšteve verejnej stránky.',
	'explication_traiter_toutes_images' => 'Vložiť pole okolo všetkých obrázkov?',

	// L
	'label_active' => 'Aktivovať multimediálny box na verejnej stránke',
	'label_apparence' => 'Vzhľad',
	'label_aucun_style' => 'Nepoužívať rám ako predvolenú možnosť',
	'label_choix_transition_elastic' => 'Elastický',
	'label_choix_transition_fade' => 'Tieň',
	'label_choix_transition_none' => 'Bez efektu prechodu',
	'label_maxheight' => 'Maximálna výška (% alebo px)',
	'label_maxwidth' => 'Maximálna šírka (% alebo px)',
	'label_minheight' => 'Minimálna výška (% alebo px)',
	'label_minwidth' => 'Minimálna šírka (% alebo px)',
	'label_opacite' => 'Priehľadnosť pozadia',
	'label_selecteur_commun' => 'Vo všeobecnosti',
	'label_selecteur_galerie' => 'V režime Galéria',
	'label_skin' => 'Farebný motív',
	'label_slideshow_speed' => 'Čas zobrazenia obrázkov v prezentácii (v ms)',
	'label_speed' => 'Rýchlosť prechodu (v ms)',
	'label_splash' => 'Úvodný box',
	'label_splash_url' => 'Adresa, ktorá sa má zobraziť',
	'label_traiter_toutes_images' => 'Obrázky',
	'label_transition' => 'Prechod medzi dvoma zobrazeniami',

	// T
	'titre_menu_box' => 'Multimediálny box',
	'titre_page_configurer_box' => 'Nastavenie Multimediálneho boxu'
);

?>
