<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Module: paquet-svp
// Langue: fr

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'svp_description' => 'Ce plugin fournit, d\'une part,  une API permettant de collecter, d\'effectuer des recherches multi-critères et de présenter les informations
	sur les plugins SPIP - modules fonctionnels, thèmes et squelettes, et d\'autre part, une nouvelle interface d\'administration des plugins gérant les dépendances entre plugins.',
	'svp_slogan' => 'SerVeur d\'information et de téléchargement des Plugins',
);
?>
