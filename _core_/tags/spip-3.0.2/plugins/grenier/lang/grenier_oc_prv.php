<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/grenier?lang_cible=oc_prv
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORUM E PETICION', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'Sauvagarda comprimida sota @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'Sauvagarda non comprimida sota @fichier@',

	// F
	'forum_probleme_database' => 'Problèma de basa de donadas, vòstre messatge s\'es pas registrat.',

	// I
	'ical_lien_rss_breves' => 'Syndication des brèves du site', # NEW
	'icone_creer_mot_cle_breve' => 'Créer un nouveau mot-clé et le lier à cette brève', # NEW
	'icone_forum_administrateur' => 'Forum deis administrators',
	'icone_forum_suivi' => 'Seguit dei forums',
	'icone_publier_breve' => 'Publicar aquela brèva',
	'icone_refuser_breve' => 'Refusar aquela brèva',
	'info_base_restauration' => 'La basa es en cors de restauracion.',
	'info_breves_03' => 'brèvas',
	'info_breves_liees_mot' => 'Lei brèvas liadas a aqueu mot clau',
	'info_breves_touvees' => 'Brèvas trobadas',
	'info_breves_touvees_dans_texte' => 'Brèvas trobadas (dins lo tèxt)',
	'info_echange_message' => 'SPIP permet d\'escambiar de messatges e de constituir de forums privats de discussion entre lei participants dau sit. Podètz activar o desactivar aquela foncionalitat.',
	'info_erreur_restauration' => 'Error de restauracion: fichier inexistent.',
	'info_forum_administrateur' => 'forum deis administrators',
	'info_forum_interne' => 'forum intèrne',
	'info_forum_ouvert' => 'Dins l\'espaci privat dau sit, un forum es dobèrt a totei lei redactors registrats. Podètz, çai sota, activar un forum suplementari, reservat ren qu\'ais administrators.',
	'info_gauche_suivi_forum' => 'La pagina de <i>seguit dei forums</i> es una aisina de gestion dau vòstre sit (mai es pas un espaci per discutir o per redigir). Aficha totei lei contribucions dau forum public d\'aquel article e vos permet de gerir aquelei contribucions.',
	'info_modifier_breve' => 'Modificar la brèva:',
	'info_nombre_breves' => '@nb_breves@ brèvas, ',
	'info_option_ne_pas_faire_suivre' => 'Pas faire seguir lei messatges dei forums',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvagardar leis articles',
	'info_sauvegarde_articles_sites_ref' => 'Sauvagardar leis articles dei sits referenciats',
	'info_sauvegarde_auteurs' => 'Sauvagardar leis autors',
	'info_sauvegarde_breves' => 'Sauvagardar lei brèvas',
	'info_sauvegarde_documents' => 'Sauvagardar lei documents',
	'info_sauvegarde_echouee' => 'Se la sauvagarda s\'es encalada(«Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Sauvagardar lei forums',
	'info_sauvegarde_groupe_mots' => 'Sauvagardar lei grops de mots',
	'info_sauvegarde_messages' => 'Sauvagardar lei messatges',
	'info_sauvegarde_mots_cles' => 'Sauvagardar lei mots clau',
	'info_sauvegarde_petitions' => 'Sauvagardar lei peticions',
	'info_sauvegarde_refers' => 'Sauvagardar lei referidors',
	'info_sauvegarde_reussi_01' => 'Sauvagarda capitada.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvagardar lei rubricas',
	'info_sauvegarde_signatures' => 'Sauvagardar lei signaturas de peticions',
	'info_sauvegarde_sites_references' => 'Sauvagardar lei sits referenciats',
	'info_sauvegarde_type_documents' => 'Sauvagardar lei tipes de documents',
	'info_sauvegarde_visites' => 'Sauvagardar lei vesitas',
	'info_une_breve' => 'una brèva, ',
	'item_mots_cles_association_breves' => 'ai brèvas',
	'item_nouvelle_breve' => 'Brèva nòva',

	// L
	'lien_forum_public' => 'Gerir lo forum public d\'aquel article',
	'lien_reponse_breve' => 'Respònsa a la brèva',

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_tech_03' => 'Podètz chausir de sauvagardar lo fichier sota forma comprimida, per fin
 d\'abrivar son transferiment au vòstre o a un servidor de sauvagardas, e per fin d\'esparnhar d\'espaci disc.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'La sauvagarda se farà dins lo fichier non comprimit @fichier@.',
	'titre_nouvelle_breve' => 'Brèva novèla',
	'titre_page_breves_edit' => 'Modificar la brèva: «@titre@»',
	'titre_page_forum' => 'Forum per leis administrators',
	'titre_page_forum_envoi' => 'Mandar un messatge',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
