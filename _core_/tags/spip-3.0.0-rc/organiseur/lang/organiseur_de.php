<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_enregistrer_brouillon' => 'Enregistrer en brouillon', # NEW
	'bouton_envoyer_message' => 'Envoyer', # NEW
	'bouton_envoyer_message_maintenant' => 'Envoyer maintenant', # NEW

	// C
	'cal_jour_entier' => 'ganztägig',
	'cal_par_jour' => 'Tag',
	'cal_par_mois' => 'Monat',
	'cal_par_semaine' => 'Woche',

	// E
	'erreur_destinataire_invalide' => 'Le destinataire @dest@ n\'est pas valide', # NEW

	// I
	'icone_ecrire_nouveau_message' => 'Envoyer un nouveau message', # NEW
	'icone_ecrire_nouveau_pensebete' => 'Ecrire un nouveau pense-bête', # NEW
	'icone_ecrire_nouvelle_annonce' => 'Envoyer une nouvelle annonce', # NEW
	'icone_effacer_message' => 'Effacer ce message', # NEW
	'icone_modifier_annonce' => 'Modifier cette annonce', # NEW
	'icone_modifier_pensebete' => 'Modifier ce pense-bête', # NEW
	'icone_supprimer_message' => 'Supprimer ce message', # NEW
	'info_1_message_envoye' => '1 message envoyé', # NEW
	'info_1_message_nonlu' => '1 nouveau message', # NEW
	'info_agenda_interne' => 'Agenda interne', # NEW
	'info_message_a' => 'A', # NEW
	'info_message_date' => 'Date', # NEW
	'info_message_de' => 'De', # NEW
	'info_message_non_lu' => 'Nouveau message', # NEW
	'info_message_objet' => 'Objet', # NEW
	'info_nb_messages_envoyes' => '@nb@ messages envoyés', # NEW
	'info_nb_messages_nonlus' => '@nb@ nouveaux messages', # NEW
	'info_selection_annonces' => 'Annonces', # NEW
	'info_selection_messages' => 'Messages', # NEW
	'info_selection_pensebetes' => 'Pense-bêtes', # NEW
	'info_type_message_affich' => 'Annonce', # NEW
	'info_type_message_normal' => 'Message', # NEW
	'info_type_message_pb' => 'Pense-bête', # NEW
	'info_type_message_rv' => 'Rendez-vous', # NEW

	// L
	'label_destinataires' => 'À', # NEW
	'label_texte' => 'Texte', # NEW
	'label_titre' => 'Sujet', # NEW
	'loading' => 'lade ...',

	// M
	'message' => 'Message', # NEW
	'messages' => 'Messages', # NEW

	// N
	'notification_annonce_lire_a_ladresse' => 'Vous pouvez la lire à l\'adresse suivante @url@.', # NEW
	'notification_annonce_publie_1' => '[@nom_site_spip@] Annonce générale', # NEW
	'notification_message_lire_a_ladresse' => 'Vous pouvez le lire et y répondre à l\'adresse suivante @url@.', # NEW
	'notification_message_publie_1' => '[@nom_site_spip@] Nouveau message', # NEW
	'notification_message_recu_de' => 'Vous avez reçu un nouveau message de la part de @nom@.', # NEW

	// T
	'texte_message_brouillon' => 'Ce message est enregistré comme brouillon', # NEW
	'titre_agenda_rv' => 'Rendez-vous', # NEW
	'titre_boite_envoi' => 'Boîte d\'envoi', # NEW
	'titre_boite_reception' => 'Boîte de reception' # NEW
);

?>
