<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/grenier?lang_cible=ast
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORU & SOLICITÚ', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'copia de seguridá comprimida en @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'copia de seguridá nun comprimida en @fichier@',

	// F
	'forum_probleme_database' => 'Problema de base datos, el to mensaxe nun quedó rexistráu.',

	// I
	'ical_lien_rss_breves' => 'Sindicación de les breves del sitiu',
	'icone_creer_mot_cle_breve' => 'Crear una nueva pallabra-clave y lligala con esta breve',
	'icone_forum_administrateur' => 'Foru d\'alministradores',
	'icone_forum_suivi' => 'Siguimientu de los foros',
	'icone_publier_breve' => 'Espublizar esta breve',
	'icone_refuser_breve' => 'Refugar esta breve',
	'info_base_restauration' => 'La base ta en procesu de restauración.',
	'info_breves_03' => 'breves',
	'info_breves_liees_mot' => 'Les breves lligáes con esta pallabra-clave',
	'info_breves_touvees' => 'Breves alcontráes',
	'info_breves_touvees_dans_texte' => 'Breves alcontráes (nel testu)',
	'info_echange_message' => 'SPIP permite l\'intercambéu de mensaxes y la creación de foros de discutiniu
  privaos ente los participantes nel sitiu. Pues activar o
  desactivar esta carauterística.',
	'info_erreur_restauration' => 'Error na restauración: archivu inesistente.',
	'info_forum_administrateur' => 'foru de los alministradores',
	'info_forum_interne' => 'foru internu',
	'info_forum_ouvert' => 'Nel espaciu priváu del sitiu, hai un foru abiertu a tolos
  redactores rexistráos. Equí embaxo puedes activar un
  foru suplementariu, acutáu sólo pa los alministradores.',
	'info_gauche_suivi_forum' => 'La páxina de <i>siguimientu de los foros</i> ye una ferramienta de xestión del sitiu Web (y non un espaciu pal discutiniu o la redaición). Amuesa toles contribuciones del foru públicu d\'esti artículu y te permite remanar eses contribuciones.',
	'info_modifier_breve' => 'Modificar la breve:',
	'info_nombre_breves' => '@nb_breves@ noticies breves,',
	'info_option_ne_pas_faire_suivre' => 'Nun mandar copia de los mensaxes de los foros',
	'info_restauration_sauvegarde_insert' => 'Amestura de @archive@ na base',
	'info_sauvegarde_articles' => 'Facer copia de seguridá de los artículos',
	'info_sauvegarde_articles_sites_ref' => 'Facer copia de seguridá de los artículos de sitios referenciaos',
	'info_sauvegarde_auteurs' => 'Facer copia de seguridá de los autores',
	'info_sauvegarde_breves' => 'Facer copia de seguridá de les breves',
	'info_sauvegarde_documents' => 'Facer copia de seguridá de los documentos',
	'info_sauvegarde_echouee' => 'Si falló la copia de seguridá («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Facer copia de seguridá de los foros',
	'info_sauvegarde_groupe_mots' => 'Facer copia de seguridá de los grupos de pallabres',
	'info_sauvegarde_messages' => 'Facer copia de seguridá de los mensaxes',
	'info_sauvegarde_mots_cles' => 'Facer copia de seguridá de les pallabres-clave',
	'info_sauvegarde_petitions' => 'Facer copia de seguridá de les solicitudes',
	'info_sauvegarde_refers' => 'Facer copia de seguridá de los referidores',
	'info_sauvegarde_reussi_01' => 'Copia de seguridá correuta.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la estaya @titre@ guardáronse en @archive@. Puedes',
	'info_sauvegarde_rubriques' => 'Facer copia de seguridá de les estayes',
	'info_sauvegarde_signatures' => 'Facer copia de seguridá de les robles de solicitudes',
	'info_sauvegarde_sites_references' => 'Facer copia de seguridá de los sitios referenciaos',
	'info_sauvegarde_type_documents' => 'Facer copia de seguridá de los tipos de documentu',
	'info_sauvegarde_visites' => 'Facer copia de seguridá de les visites',
	'info_une_breve' => 'una breve,',
	'item_mots_cles_association_breves' => 'a les breves',
	'item_nouvelle_breve' => 'Breve nueva',

	// L
	'lien_forum_public' => 'Xestionar el foru públicu d\'esti artículu',
	'lien_reponse_breve' => 'Respuesta a la breve',

	// S
	'sauvegarde_fusionner' => 'Fusionar la base actual y la copia de seguridá',
	'sauvegarde_fusionner_depublier' => 'Desespublizar los oxetos fusionaos',
	'sauvegarde_url_origine' => 'Si ye\'l casu, URL del sitiu d\'orixe:',

	// T
	'texte_admin_tech_03' => 'Pues escoyer guardar l\'archivu en forma comprimida, col envís
 d\'amenorgar so tresferencia al to ordenador o a un sirvidor de copies de seguridá, y d\'aforrar l\'espaciu en discu.',
	'texte_admin_tech_04' => 'Si se quier facer una fusión con otra base, pues llendar la copia a la estaya: ',
	'texte_sauvegarde_compressee' => 'La copia de seguridá va facese nel archivu non comprimíu @fichier@.',
	'titre_nouvelle_breve' => 'Breve nueva',
	'titre_page_breves_edit' => 'Cambear la breve: «@titre@»',
	'titre_page_forum' => 'Foru de los alministradores',
	'titre_page_forum_envoi' => 'Unviar un mensaxe',
	'titre_page_statistiques_messages_forum' => 'Mensaxes de foros'
);

?>
