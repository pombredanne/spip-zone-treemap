<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/grenier?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORUM + PETITION',
	'bouton_radio_sauvegarde_compressee' => 'komprimiert sichern nach @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'unkomprimiert sichern nach @fichier@',

	// F
	'forum_probleme_database' => 'Datenbankfehler. Ihr Beitrag wurde nicht gespeichert',

	// I
	'ical_lien_rss_breves' => 'Syndikation der Meldungen dieser Website',
	'icone_creer_mot_cle_breve' => 'Schlagwort anlegen und dieser Meldung zuordnen.',
	'icone_forum_administrateur' => 'Forum der Administratoren',
	'icone_forum_suivi' => 'Foren moderieren',
	'icone_publier_breve' => 'Meldung veröffentlichen',
	'icone_refuser_breve' => 'Kurzmeldung ablehnen',
	'info_base_restauration' => 'Datenbank wird wieder hergestellt.',
	'info_breves_03' => 'Meldungen',
	'info_breves_liees_mot' => 'Mit diesem Schlagwort verknüpfte Meldungen',
	'info_breves_touvees' => 'Gefundene Meldungen',
	'info_breves_touvees_dans_texte' => 'Gefundene Meldungen (im Text)',
	'info_echange_message' => 'In SPIP können Sie interne Nachrichten austauschen und interne Foren für Teilnehmer der Website anlegen. Diese Funktionen können Sie abschalten.',
	'info_erreur_restauration' => 'Fehler bei der Wiederherstellung: Datei nicht vorhanden.',
	'info_forum_administrateur' => 'Forum der Administratoren',
	'info_forum_interne' => 'Internes Forum',
	'info_forum_ouvert' => 'Im Redaktionssystem der Website gibt es ein Forum für alle Redakteure. Sie können hier ein Forum <i>nur für Administratoren</i> einschalten.',
	'info_gauche_suivi_forum' => 'Die Seite zum <i>Verwalten der Foren</i> hat eine administrative Funktion und dient nicht zum Diskutieren mit den Lesern. Es werden alle Einträge im öffentlichen Forum des Artikels gezeigt, so dass Sie diese einzeln ein- und ausschalten können.',
	'info_modifier_breve' => 'Meldung bearbeiten:',
	'info_nombre_breves' => '@nb_breves@ Kurzmeldugen, ',
	'info_option_ne_pas_faire_suivre' => 'Keine Beiträge aus den Foren an die Autoren verschicken.',
	'info_restauration_sauvegarde_insert' => 'Einfügen der Sicherung @archive@ in die Datenbank',
	'info_sauvegarde_articles' => 'Artikel sichern',
	'info_sauvegarde_articles_sites_ref' => 'Artikel der verlinkten Sites sichern',
	'info_sauvegarde_auteurs' => 'Autoren sichern',
	'info_sauvegarde_breves' => 'Meldungen sichern',
	'info_sauvegarde_documents' => 'Dokumente sichern',
	'info_sauvegarde_echouee' => 'Falls die Sicherung fehlgeschlagen ist(„Maximum execution time exceeded“),',
	'info_sauvegarde_forums' => 'Foren sichern',
	'info_sauvegarde_groupe_mots' => 'Schlagwort-Kategorien sichern',
	'info_sauvegarde_messages' => 'Nachrichten sichern',
	'info_sauvegarde_mots_cles' => 'Schlagworte sichern',
	'info_sauvegarde_petitions' => 'Petitionen sichern',
	'info_sauvegarde_refers' => 'Referer sichern',
	'info_sauvegarde_reussi_01' => 'Sicherung abgeschlossen.',
	'info_sauvegarde_rubrique_reussi' => 'Die Tabellen der Rubrik @titre@ wurden in der Sicherung @archive@ gespeichert. Sie können',
	'info_sauvegarde_rubriques' => 'Rubriken sichern',
	'info_sauvegarde_signatures' => 'Unterschriften der Petitionen sichern',
	'info_sauvegarde_sites_references' => 'Verlinkte Websites sichern',
	'info_sauvegarde_type_documents' => 'Dokumenttypen sichern',
	'info_sauvegarde_visites' => 'Besucherzahlen sichern',
	'info_une_breve' => 'Eine Meldung, ',
	'item_mots_cles_association_breves' => 'Meldungen zuordnen',
	'item_nouvelle_breve' => 'Neue Meldung',

	// L
	'lien_forum_public' => 'Öffentliches Forum für diesen Artikel verwalten',
	'lien_reponse_breve' => 'Antwort auf Meldung',

	// S
	'sauvegarde_fusionner' => 'Aktuelle Datenbank und Sicherungskopie zusammenführen',
	'sauvegarde_fusionner_depublier' => 'Zusammengeführte Objekte offline stellen',
	'sauvegarde_url_origine' => 'Wenn gewünscht URL der Quelle:',

	// T
	'texte_admin_tech_03' => 'Sie können die Daten in komprimierter Form sichern und damit Speicherplatz auf dem Server und Downloadzeit sparen.',
	'texte_admin_tech_04' => 'Um das Zusammenführen mit einer anderen Datenbank vorzubereiten, können Sie die Datensicherung auf eine bestimmte Rubrik beschränken: ',
	'texte_sauvegarde_compressee' => 'Die Sicherung wird als unkomprimierte Datei @fichier@ angelegt.',
	'titre_nouvelle_breve' => 'Neue Meldung',
	'titre_page_breves_edit' => 'Meldung bearbeiten: “@titre@”',
	'titre_page_forum' => 'Administratorforum',
	'titre_page_forum_envoi' => 'Nachricht schicken',
	'titre_page_statistiques_messages_forum' => 'Forumsbeiträge'
);

?>
