<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/grenier?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORUM & PETITION',
	'bouton_radio_sauvegarde_compressee' => 'save as compressed in @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'save as uncompressed in @fichier@',

	// F
	'forum_probleme_database' => 'Database problem, your message could not be recorded.',

	// I
	'ical_lien_rss_breves' => 'Syndication of site news items',
	'icone_creer_mot_cle_breve' => 'Create a new keyword and attach it to this news item',
	'icone_forum_administrateur' => 'Administrators\' forum',
	'icone_forum_suivi' => 'Manage forums',
	'icone_publier_breve' => 'Publish this news item',
	'icone_refuser_breve' => 'Reject this news item',
	'info_base_restauration' => 'Restoration of the database in progress.',
	'info_breves_03' => 'news items',
	'info_breves_liees_mot' => 'News items with this keyword',
	'info_breves_touvees' => 'News items found',
	'info_breves_touvees_dans_texte' => 'News items found (in the text)',
	'info_echange_message' => 'SPIP allows the exchange of messages and the creation of private
  discussion forums for site members. You can enable or
  disable this feature.',
	'info_erreur_restauration' => 'Restoration error: file not found.',
	'info_forum_administrateur' => 'administrators\' forum',
	'info_forum_interne' => 'internal forum',
	'info_forum_ouvert' => 'A forum is available to all
  registered editors in the site\'s private area. You can enable an
  extra forum reserved for the administrators here.',
	'info_gauche_suivi_forum' => 'The <i>forum management</i> page is a site management tool (not a discussion or editing area). It displays all the contributions to the public forum of this article and allows you to manage these contributions.',
	'info_modifier_breve' => 'Modify the news item:',
	'info_nombre_breves' => '@nb_breves@ news items,',
	'info_option_ne_pas_faire_suivre' => 'Do not forward forum messages',
	'info_restauration_sauvegarde_insert' => 'Inserting @archive@ in the database',
	'info_sauvegarde_articles' => 'Backup the articles',
	'info_sauvegarde_articles_sites_ref' => 'Backup articles of referenced sites',
	'info_sauvegarde_auteurs' => 'Backup the authors',
	'info_sauvegarde_breves' => 'Backup the news',
	'info_sauvegarde_documents' => 'Backup the documents',
	'info_sauvegarde_echouee' => 'If the backup fails ("Maximum execution time exceeded"),',
	'info_sauvegarde_forums' => 'Backup the forums',
	'info_sauvegarde_groupe_mots' => 'Backup keyword groups',
	'info_sauvegarde_messages' => 'Backup messages',
	'info_sauvegarde_mots_cles' => 'Backup keywords',
	'info_sauvegarde_petitions' => 'Backup petitions',
	'info_sauvegarde_refers' => 'Backup referrers',
	'info_sauvegarde_reussi_01' => 'Backup successful.',
	'info_sauvegarde_rubrique_reussi' => 'The tables of the @titre@ section have been saved to @archive@. You can',
	'info_sauvegarde_rubriques' => 'Backup sections',
	'info_sauvegarde_signatures' => 'Backup petition signatures',
	'info_sauvegarde_sites_references' => 'Backup referenced sites',
	'info_sauvegarde_type_documents' => 'Backup document types',
	'info_sauvegarde_visites' => 'Backup visitor statistics',
	'info_une_breve' => 'a news item,',
	'item_mots_cles_association_breves' => 'news items',
	'item_nouvelle_breve' => 'New news item',

	// L
	'lien_forum_public' => 'Manage the public forum for this article',
	'lien_reponse_breve' => 'Comment on this news item',

	// S
	'sauvegarde_fusionner' => 'Merge current database with the backup',
	'sauvegarde_fusionner_depublier' => 'Unpublish any merged objects',
	'sauvegarde_url_origine' => 'URL of the source site, if required:',

	// T
	'texte_admin_tech_03' => 'You can opt to save the file in compressed format, to
 reduce filesize and allow faster downloading or copying to a backup server.',
	'texte_admin_tech_04' => 'When merginge two databases, you can restrict the backup to one section: ',
	'texte_sauvegarde_compressee' => 'Backup will be stored in the uncompressed file @fichier@.',
	'titre_nouvelle_breve' => 'New news item',
	'titre_page_breves_edit' => 'Modify the news item: «@titre@»',
	'titre_page_forum' => 'Administrators forum',
	'titre_page_forum_envoi' => 'Send a message',
	'titre_page_statistiques_messages_forum' => 'Forum messages'
);

?>
