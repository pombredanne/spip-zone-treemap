<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/grenier?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORO & FIRMAS', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'respaldo comprimido en @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'respaldo no comprimido en @fichier@',

	// F
	'forum_probleme_database' => 'Problema de la base de datos, tu mensaje no ha sido registrado.',

	// I
	'ical_lien_rss_breves' => 'Sindicación de breves del sitio',
	'icone_creer_mot_cle_breve' => 'Crear una nueva palabra clave y aplicarla a esta breve',
	'icone_forum_administrateur' => 'Foro de administradoras y administradores',
	'icone_forum_suivi' => 'Seguimiento de los foros',
	'icone_publier_breve' => 'Publicar esta breve',
	'icone_refuser_breve' => 'Rechazar esta breve',
	'info_base_restauration' => 'La base está siendo restaurada',
	'info_breves_03' => 'breves',
	'info_breves_liees_mot' => 'Las breves ligadas a esta palabra clave',
	'info_breves_touvees' => 'Breves localizadas',
	'info_breves_touvees_dans_texte' => 'Breves localizadas (en el texto)',
	'info_echange_message' => 'En función de las preferencias, SPIP permite el intercambio de mensajes y la existencia de foros internos
 privados entre los participantes del sitio. Esta funcionalidad
se puede activar o desactivar .',
	'info_erreur_restauration' => 'Error de restauración. Archivo inexistente.',
	'info_forum_administrateur' => 'Foro del grupo de administración ',
	'info_forum_interne' => 'foro interno',
	'info_forum_ouvert' => 'En el espacio privado del sitio hay un foro abierto a todas las personas inscritas. A continuación, puedes activar un foro suplementario reservado a los administradores y administradoras.',
	'info_gauche_suivi_forum' => 'La página de <i>seguimiento de los foros</i> es una herramienta de gestión de tu sitio (y no un espacio de diálogo o de redacción). Muestra todas las contribuciones del foro público de este artículo y te permite administrarlas.',
	'info_modifier_breve' => 'Modificar la breve',
	'info_nombre_breves' => '@nb_breves@ breves,',
	'info_option_ne_pas_faire_suivre' => 'No enviar los mensajes de los foros',
	'info_restauration_sauvegarde_insert' => 'Inserción de @archive@ en la base',
	'info_sauvegarde_articles' => 'Guardar los artículos',
	'info_sauvegarde_articles_sites_ref' => 'Guarar los artículos de los sitios referenciados',
	'info_sauvegarde_auteurs' => 'Crear un respaldo de los autores y autoras',
	'info_sauvegarde_breves' => 'Guarar las breves',
	'info_sauvegarde_documents' => 'Guardar los documentos',
	'info_sauvegarde_echouee' => 'Si ha fallado al guardar («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Guardar los foros',
	'info_sauvegarde_groupe_mots' => 'Guardar los grupos de palabras',
	'info_sauvegarde_messages' => 'Guardar los mensajes',
	'info_sauvegarde_mots_cles' => 'Guardar las palabras clave',
	'info_sauvegarde_petitions' => 'Guardar las peticiones',
	'info_sauvegarde_refers' => 'Guardar los "referers"',
	'info_sauvegarde_reussi_01' => 'Copia de respaldo lograda.',
	'info_sauvegarde_rubrique_reussi' => 'Las tablas de la sección @titre@ se han guardado en @archive@. Puedes',
	'info_sauvegarde_rubriques' => 'Guardar las secciones',
	'info_sauvegarde_signatures' => 'Guardar las firmas de las peticiones',
	'info_sauvegarde_sites_references' => 'Guardar los sitios referenciados',
	'info_sauvegarde_type_documents' => 'Guardar los tipos de documentos',
	'info_sauvegarde_visites' => 'Guardar las visitas',
	'info_une_breve' => 'una breve,',
	'item_mots_cles_association_breves' => 'a las breves',
	'item_nouvelle_breve' => 'Nueva breve',

	// L
	'lien_forum_public' => 'Gestionar el foro de este artículo',
	'lien_reponse_breve' => 'Respuesta a la breve',

	// S
	'sauvegarde_fusionner' => 'Fusionar la base actual y el respaldo',
	'sauvegarde_fusionner_depublier' => 'Despublicar los objetos fusionados',
	'sauvegarde_url_origine' => 'Eventualemente, URL del sitio de origen:',

	// T
	'texte_admin_tech_03' => 'Puedes elegir de guardar el archivo comprimido, y así acortar el tiempo de transferencia hacia tu ordenador o un servidor de respaldo, aparte de ganar espacio en el disco.',
	'texte_admin_tech_04' => 'En caso de fusión con otra base, la copia de respaldo puede limitarse a la sección: ',
	'texte_sauvegarde_compressee' => 'La copia de respaldo será guardada en un archivo no comprimido @fichier@.',
	'titre_nouvelle_breve' => 'Nueva breve',
	'titre_page_breves_edit' => 'Modificar la nota breve: « @titre@ »',
	'titre_page_forum' => 'Foro de los administradores',
	'titre_page_forum_envoi' => 'Enviar un mensaje',
	'titre_page_statistiques_messages_forum' => 'Mensajes del foro'
);

?>
