<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/paquet-forum?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'forum_description' => 'Foren in SPIP (Redaktionsbereich und öffentliche Website)',
	'forum_slogan' => 'Gestion des forums privés et publics dans SPIP' # NEW
);

?>
