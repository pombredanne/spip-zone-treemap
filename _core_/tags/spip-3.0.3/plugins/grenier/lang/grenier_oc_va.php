<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/grenier?lang_cible=oc_va
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORUM E PETICION', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'Sauvagarda comprimia sos @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'Sauvagarda non comprimia sos @fichier@',

	// F
	'forum_probleme_database' => 'Problèma de basa de donaas, vòstre messatge s\'es pas registrat.',

	// I
	'ical_lien_rss_breves' => 'Syndication des brèves du site', # NEW
	'icone_creer_mot_cle_breve' => 'Créer un nouveau mot-clé et le lier à cette brève', # NEW
	'icone_forum_administrateur' => 'Forum dels administrators',
	'icone_forum_suivi' => 'Seguit dels forums',
	'icone_publier_breve' => 'Publicar aquela brèva',
	'icone_refuser_breve' => 'Refusar aquela brèva',
	'info_base_restauration' => 'La basa es en cors de restauracion.',
	'info_breves_03' => 'brèvas',
	'info_breves_liees_mot' => 'Las brèvas liaas a aqueu mot clau',
	'info_breves_touvees' => 'Brèvas trobaas',
	'info_breves_touvees_dans_texte' => 'Brèvas trobaas (dins lo tèxt)',
	'info_echange_message' => 'SPIP permet d\'eschambiar de messatges e de constituir de forums privats de discussion entre los participants dau sit. Poètz activar o desactivar aquela foncionalitat.',
	'info_erreur_restauration' => 'Error de restauracion: fichier inexistent.',
	'info_forum_administrateur' => 'forum dels administrators',
	'info_forum_interne' => 'forum intèrne',
	'info_forum_ouvert' => 'Dins l\'espaci privat dau sit, un forum es dobèrt vèrs totes los redactors registrats. Poètz, çai sos, activar un forum suplementari, reservat mas qu\'als administrators.',
	'info_gauche_suivi_forum' => 'La pagina de <i>seguit dels forums</i> es un esplech de gestion de vòstre sit (mas es pas un espaci per discutir o per redigir). Aficha totas las contribucions dau forum public d\'aquel article e vos permet de gerir aquelas contribucions.',
	'info_modifier_breve' => 'Modificar la brèva:',
	'info_nombre_breves' => '@nb_breves@ brèvas, ',
	'info_option_ne_pas_faire_suivre' => 'Pas far sègre los messatges dels forums',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvagardar los articles',
	'info_sauvegarde_articles_sites_ref' => 'Sauvagardar los articles dels sits referenciats',
	'info_sauvegarde_auteurs' => 'Sauvagardar los autors',
	'info_sauvegarde_breves' => 'Sauvagardar las brèvas',
	'info_sauvegarde_documents' => 'Sauvagardar los documents',
	'info_sauvegarde_echouee' => 'Se la sauvagarda a mal capitat («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Sauvagardar los forums',
	'info_sauvegarde_groupe_mots' => 'Sauvagardar los grops de mots',
	'info_sauvegarde_messages' => 'Sauvagardar los messatges',
	'info_sauvegarde_mots_cles' => 'Sauvagardar los mots clau',
	'info_sauvegarde_petitions' => 'Sauvagardar las peticions',
	'info_sauvegarde_refers' => 'Sauvagardar los referiors',
	'info_sauvegarde_reussi_01' => 'Sauvagarda abotia.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvagardar las rubricas',
	'info_sauvegarde_signatures' => 'Sauvagardar las signaturas de peticions',
	'info_sauvegarde_sites_references' => 'Sauvagardar los sits referenciats',
	'info_sauvegarde_type_documents' => 'Sauvagardar los tipes de documents',
	'info_sauvegarde_visites' => 'Sauvagardar las vesitas',
	'info_une_breve' => 'una brèva, ',
	'item_mots_cles_association_breves' => 'a las brèvas',
	'item_nouvelle_breve' => 'Brèva nòva',

	// L
	'lien_forum_public' => 'Gerir lo forum public d\'aquel article',
	'lien_reponse_breve' => 'Respònsa a la brèva',

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_tech_03' => 'Poètz chausir de sauvagardar lo fichier sos forma comprimia, a fin
 d\'abrivar son transferiment vèrs vos o en cò d\'un servior de sauvagardas, e a fin d\'esparnhar d\'espaci disc.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'La sauvagarda se farà dins lo fichier non comprimit @fichier@.',
	'titre_nouvelle_breve' => 'Brèva novèla',
	'titre_page_breves_edit' => 'Modificar la brèva: «@titre@»',
	'titre_page_forum' => 'Forum per los administrators',
	'titre_page_forum_envoi' => 'Mandar un messatge',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
