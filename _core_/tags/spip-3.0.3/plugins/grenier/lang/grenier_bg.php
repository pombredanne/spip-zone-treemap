<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/grenier?lang_cible=bg
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'ФОРУМ и МОЛБИ', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'Запазване в компресиран вид в @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'Запазване в некомпресиран вид в @fichier@',

	// F
	'forum_probleme_database' => 'Проблем с базата данни: съобщението Ви не бе запаметено.',

	// I
	'ical_lien_rss_breves' => 'Syndication des brèves du site', # NEW
	'icone_creer_mot_cle_breve' => 'Créer un nouveau mot-clé et le lier à cette brève', # NEW
	'icone_forum_administrateur' => 'Форум на администраторите',
	'icone_forum_suivi' => 'Допълнения във форумите',
	'icone_publier_breve' => 'Публикуване на новината',
	'icone_refuser_breve' => 'Отхвърляне на новината',
	'info_base_restauration' => 'Базата данни е в процес на възстановяване.',
	'info_breves_03' => 'Нови публикации',
	'info_breves_liees_mot' => 'Новини, свързани с ключовата дума',
	'info_breves_touvees' => 'Намерени новини',
	'info_breves_touvees_dans_texte' => 'Намерени новини (в текста)',
	'info_echange_message' => 'СПИП позволява обмяната на съобщения и създаването на лични
  форуми за дискусия между участниците на сайта. Това свойство може
  да се включва или изключва по желание.',
	'info_erreur_restauration' => 'Грешка при възстановяване: файлът не беше намерен.',
	'info_forum_administrateur' => 'форум за администратори',
	'info_forum_interne' => 'вътрешен форум',
	'info_forum_ouvert' => 'В личната зона на сайта форумът е достъпен за всички
  регистрирани редактори. По-надолу можете да
  активирате допълнителен форум, запазен за администраторите.',
	'info_gauche_suivi_forum' => 'Страницата <i>Допълнения във форумите</i> е инструмент за управление на сайта (а не зона за дискусии или за публикации). Тя показва целия принос от съобщения в публичния форум на статията и позволява боравенето с тези съобщения.',
	'info_modifier_breve' => 'Промяна настройките на новините:',
	'info_nombre_breves' => '@nb_breves@ новини,',
	'info_option_ne_pas_faire_suivre' => 'Без препращане на съобщения от форума',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Архивиране на статиите',
	'info_sauvegarde_articles_sites_ref' => 'Архивиране на статии от сайтове с препратки',
	'info_sauvegarde_auteurs' => 'Архив на авторите',
	'info_sauvegarde_breves' => 'Архив на новините',
	'info_sauvegarde_documents' => 'Архив на документите',
	'info_sauvegarde_echouee' => 'Ако архивирането пропадне («Времето за изпълнение изтече»),',
	'info_sauvegarde_forums' => 'Архив на форумите',
	'info_sauvegarde_groupe_mots' => 'Архив на групите ключови думи',
	'info_sauvegarde_messages' => 'Архив на съобщенията',
	'info_sauvegarde_mots_cles' => 'Архив на ключовите думи',
	'info_sauvegarde_petitions' => 'Архив на молбите',
	'info_sauvegarde_refers' => 'Архивиране на референтите',
	'info_sauvegarde_reussi_01' => 'Архивирането завърши успешно.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Архив на рубриките',
	'info_sauvegarde_signatures' => 'Архив на заявените молби',
	'info_sauvegarde_sites_references' => 'Архивиране на свързани сайтове',
	'info_sauvegarde_type_documents' => 'Архив на видовете документи',
	'info_sauvegarde_visites' => 'Архив на посещенията',
	'info_une_breve' => 'новина,',
	'item_mots_cles_association_breves' => 'новините',
	'item_nouvelle_breve' => 'Нова новина',

	// L
	'lien_forum_public' => 'Управление на публичния форум към статията',
	'lien_reponse_breve' => 'Отговор на новината',

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_tech_03' => 'За да спестите дисково пространство можете да изберете или
 да съхраните файла в компресиран вид, или да засилите трансфера му до Вашата машина или до архивен сървър.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'Архивът ще бъде запазен в некомресираният файл @fichier@.',
	'titre_nouvelle_breve' => 'Новина',
	'titre_page_breves_edit' => 'Промяна на новината: "@titre@"',
	'titre_page_forum' => 'Форум на администраторите',
	'titre_page_forum_envoi' => 'Изпращане на съобщение',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
