<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/grenier?lang_cible=oc_ni
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FÒRO & PETICION', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'Sauvagarda comprimida sota @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'Sauvagarda non comprimida sota @fichier@',

	// F
	'forum_probleme_database' => 'Problèma de basa de donadas, lo voastre messatge es pas estat registrat.',

	// I
	'ical_lien_rss_breves' => 'Sindicacion dei brèvas dau sit',
	'icone_creer_mot_cle_breve' => 'Créer un nouveau mot-clé et le lier à cette brève', # NEW
	'icone_forum_administrateur' => 'Fòro dei administrators',
	'icone_forum_suivi' => 'Seguit dei fòros',
	'icone_publier_breve' => 'Publicar aquela brèva',
	'icone_refuser_breve' => 'Refudar aquela brèva',
	'info_base_restauration' => 'La basa es en cors de restauracion.',
	'info_breves_03' => 'brèvas',
	'info_breves_liees_mot' => 'Li brèvas ligadi à-n-aqueu mòt clau',
	'info_breves_touvees' => 'Brèvas trobadi',
	'info_breves_touvees_dans_texte' => 'Brèvas trobadi (dins lo tèxt)',
	'info_echange_message' => 'SPIP permete l\'escambi de messatges e la constitucion de fòros privats de discucion entre lu participants dau sit. Podètz activar ò desactivar aquela foncionalitat',
	'info_erreur_restauration' => 'Error de restauracion: fichier inexistent.',
	'info_forum_administrateur' => 'fòro dei administrators',
	'info_forum_interne' => 'fòro interne',
	'info_forum_ouvert' => 'Dins l\'espaci privat dau sit, un fòro es dubèrt à toi lu redactors registrats. Podètz, çai sota, activar un fòro suplementari, reservat ai solets administrators.',
	'info_gauche_suivi_forum' => 'La pàgina de <i>seguit dei fòros</i> es una aisina de gestion dau voastre sit (ma es pas un espaci de discussion ò de redaccion). Fa paréisser toti li contribucions dau fòro public d\'aquel article e vos permete de gerar aqueli contribucions.',
	'info_modifier_breve' => 'Modificar la brèva:',
	'info_nombre_breves' => '@nb_breves@ brèvas, ',
	'info_option_ne_pas_faire_suivre' => 'Non faire sègre lu messatges dei fòros',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvagardar lu articles',
	'info_sauvegarde_articles_sites_ref' => 'Sauvagardar lu articles dei sits referençats',
	'info_sauvegarde_auteurs' => 'Sauvagardar lu autors',
	'info_sauvegarde_breves' => 'Sauvagardar li brèvas',
	'info_sauvegarde_documents' => 'Sauvagardar lu documents',
	'info_sauvegarde_echouee' => 'Se la sauvagarda a soït («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Sauvagardar lu fòros',
	'info_sauvegarde_groupe_mots' => 'Sauvagardar lu grops de mòts',
	'info_sauvegarde_messages' => 'Sauvagardar lu messatges',
	'info_sauvegarde_mots_cles' => 'Sauvagardar lu mòts clau',
	'info_sauvegarde_petitions' => 'Sauvagardar li peticions',
	'info_sauvegarde_refers' => 'Sauvagardar lu referits',
	'info_sauvegarde_reussi_01' => 'Sauvagarda capitada.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvagardar li rubricas',
	'info_sauvegarde_signatures' => 'Sauvagardar li signaturas de peticions',
	'info_sauvegarde_sites_references' => 'Sauvagardar lu sits referençats',
	'info_sauvegarde_type_documents' => 'Sauvagardar lu tipes de documents',
	'info_sauvegarde_visites' => 'Sauvagardar li visitas',
	'info_une_breve' => 'una brèva, ',
	'item_mots_cles_association_breves' => 'ai brèvas',
	'item_nouvelle_breve' => 'Novèla brèva',

	// L
	'lien_forum_public' => 'Gerar lo fòro public d\'aquel article',
	'lien_reponse_breve' => 'Respoasta à la brèva',

	// S
	'sauvegarde_fusionner' => 'Fusionar la basa actuala e lo sauvament',
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventualament, URL dau sit d\'origina :',

	// T
	'texte_admin_tech_03' => 'Podètz chausir de sauvagardar lo fichier sota forma comprimida, per fin
 d\'abrivar lo sieu transferiment dau voastre ò sus un servidor de sauvagardas, e per fin d\'esparnhar d\'espaci disc.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'La sauvagarda serà facha dins lo fichier non comprimit @fichier@.',
	'titre_nouvelle_breve' => 'Novèla brèva',
	'titre_page_breves_edit' => 'Modificar la brèva: « @titre@ »',
	'titre_page_forum' => 'Fòro dei administrators',
	'titre_page_forum_envoi' => 'Mandar un messatge',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
