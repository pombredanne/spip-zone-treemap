<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/grenier?lang_cible=cpf_hat
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FOWOM & PETISYON', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'yon sòvgad konprese sou @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'yon sòvgad sak se pa konprese sou @fichier@',

	// F
	'forum_probleme_database' => 'Pwoblèm ak bazdone, mesaj ou te pa enskri.',

	// I
	'ical_lien_rss_breves' => 'Syndication des brèves du site', # NEW
	'icone_creer_mot_cle_breve' => 'Créer un nouveau mot-clé et le lier à cette brève', # NEW
	'icone_forum_administrateur' => 'Fowom pou komandè yo',
	'icone_forum_suivi' => 'Swiv tout fowom yo',
	'icone_publier_breve' => 'Pibliye tinouvèl',
	'icone_refuser_breve' => 'Refize tinouvèl',
	'info_base_restauration' => 'Labaz ap restore',
	'info_breves_03' => 'tinouvèl yo',
	'info_breves_liees_mot' => 'Tinouvèl yo ki jwenn ak mokle-a',
	'info_breves_touvees' => 'Tinouvèl yo ki te fin rannkont',
	'info_breves_touvees_dans_texte' => 'Tinouvèl yo ki te fin rannkont (nan tèks)',
	'info_echange_message' => 'SPIP ka pémè lechanj mesaj ek kreyasyon fowom kozman privé
 ant tout moun ka soutni sit la. W kap mèt oubyen kraze fonksyonalite a.',
	'info_erreur_restauration' => 'Erè pou restorasyon : fichye-an ap pa egsiste.',
	'info_forum_administrateur' => 'fowom pou komandè yo',
	'info_forum_interne' => 'fowom andan sit la',
	'info_forum_ouvert' => 'Nan lespas privé sit-la, yon fowom gen ouvè pou tout moun
 ki rédaktè enskri. W kapab mèt, isit-anba, yon fowom
 anplis, ki résèvé pou komandé yo sèlman.',
	'info_gauche_suivi_forum' => 'Paj <i>swivi fowom yo</i> se outiy pou sit ou (kontrè yon espas pou soutni ek ekri ant zot itlizatè). Li afich tout patisipasyon nan fowom piblik yon atik ak rann w kapab okipe yo.',
	'info_modifier_breve' => 'Chanje tinouvèl-la :',
	'info_nombre_breves' => '@nb_breves@ tinouvèl (yo),',
	'info_option_ne_pas_faire_suivre' => 'Voye pa mesaj fowom yo ',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sòvgade atik yo',
	'info_sauvegarde_articles_sites_ref' => 'Sòvgade atik yo ki nan sit referansé yo',
	'info_sauvegarde_auteurs' => 'Sòvgade lotè yo ',
	'info_sauvegarde_breves' => 'Sòvgade tinouvèl yo ',
	'info_sauvegarde_documents' => 'Sòvgade dokiman yo ',
	'info_sauvegarde_echouee' => 'Si sòvgad-la te pa mache («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Sove fowom yo',
	'info_sauvegarde_groupe_mots' => 'Sòvgade gwoup-mokle yo ',
	'info_sauvegarde_messages' => 'Sòvgade mesaj yo ',
	'info_sauvegarde_mots_cles' => 'Sòvgade mokle yo ',
	'info_sauvegarde_petitions' => 'Sòvgade petisyon yo ',
	'info_sauvegarde_refers' => 'Sòvgade rèfèrè yo ',
	'info_sauvegarde_reussi_01' => 'Sòvgad-la te mache',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sòvgade ribrik yo ',
	'info_sauvegarde_signatures' => 'Sòvgade tout siyati nan petisyon yo ',
	'info_sauvegarde_sites_references' => 'Sòvgade sit referansé yo',
	'info_sauvegarde_type_documents' => 'Sòvgade tip-dokiman yo ',
	'info_sauvegarde_visites' => 'Sòvgade vizit yo ',
	'info_une_breve' => 'yon tinouvèl,',
	'item_mots_cles_association_breves' => 'nan tinouvèl yo',
	'item_nouvelle_breve' => 'Nouvo tinouvèl',

	// L
	'lien_forum_public' => 'Zèr fowom piblik atik la',
	'lien_reponse_breve' => 'Réponn tinouvèl-la',

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_tech_03' => 'Ou kapab chwazi sòvgad fichye-a nan fòm koprésé, pou rann transfè ali taptap
 sou machin a w osnon yon sèvè pou lasòvgad è pou gad swen pa depanse tro lespas diks lòdinatè.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'Sovgad ke pou fè nan yon fichye pa koprésé sa-a, ki rele @fichier@.', # MODIF
	'titre_nouvelle_breve' => 'Nouvo tinouvèl',
	'titre_page_breves_edit' => 'Chanje tinouvèl-la : « @titre@ »',
	'titre_page_forum' => 'Fowom komandè yo',
	'titre_page_forum_envoi' => 'Voye yon mesaj',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
