<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/grenier?lang_cible=sv
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORUM & NAMNINSAMLINGAR', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'spara komprimerat i @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'spara utan att komprimera i @fichier@',

	// F
	'forum_probleme_database' => 'Databas-problem, ditt meddelande kunde inte sparas.',

	// I
	'ical_lien_rss_breves' => 'Syndikering av sajtens nyheter',
	'icone_creer_mot_cle_breve' => 'Skapa ett nytt nyckelord och koppla det till den här nyheten',
	'icone_forum_administrateur' => 'Administratörernas forum',
	'icone_forum_suivi' => 'Vidare till forumen',
	'icone_publier_breve' => 'Publicera nyhet',
	'icone_refuser_breve' => 'Refusera nyheten',
	'info_base_restauration' => 'Återskapande av databasen pågår.',
	'info_breves_03' => 'Nyheter',
	'info_breves_liees_mot' => 'Nyheter knutna till det här nyckelordet',
	'info_breves_touvees' => 'Nyheter funna',
	'info_breves_touvees_dans_texte' => 'Nyheter funna (i texten)',
	'info_echange_message' => 'SPIP tillåter utbyte av meddelanden och skapandet av privata
  diskussionsforum bland sajtens deltagare. Du kan tillåta
  eller förbjuda det.',
	'info_erreur_restauration' => 'Fel vid återskapande: filen finns inte.',
	'info_forum_administrateur' => 'Administratörernas forum',
	'info_forum_interne' => 'Internt forum',
	'info_forum_ouvert' => 'I sajtens privata del är ett forum öppet för alla
  redaktörer. Nedan kan du aktivera ett
  extra forum reserverat för administratörerna.',
	'info_gauche_suivi_forum' => 'Sidna för<i>forumuppföljning</i> är ett administrationsverktyg för din sajt (inte en diskussions- eller redigeringsida). Den visar alla bidrag till det publika forum som hör till artikeln och där kan du hantera bidragen contributions.',
	'info_modifier_breve' => 'Editera nyheten:',
	'info_nombre_breves' => '@nb_breves@ nyheter,',
	'info_option_ne_pas_faire_suivre' => 'Vidarebefordra inte forummeddelanden',
	'info_restauration_sauvegarde_insert' => 'Läser in @archive@ i databasen',
	'info_sauvegarde_articles' => 'Backa upp artiklarna',
	'info_sauvegarde_articles_sites_ref' => 'Säkerhetskopiera artiklar från länkade sajter',
	'info_sauvegarde_auteurs' => 'Säkerhetskopiera redaktörerna',
	'info_sauvegarde_breves' => 'Ta en säkerhetskopia av nyheterna',
	'info_sauvegarde_documents' => 'Säkerhetskopiera dokumenten',
	'info_sauvegarde_echouee' => 'Om säkerhetskopieringen misslyckas («Maximal utförandetid överskreds»),',
	'info_sauvegarde_forums' => 'Säkerhetskopiera forumen',
	'info_sauvegarde_groupe_mots' => 'Säkerhetskopiera nyckelordsgrupperna',
	'info_sauvegarde_messages' => 'Säkerhetskopiera meddelanden',
	'info_sauvegarde_mots_cles' => 'Säkerhetskopiera nyckelorden',
	'info_sauvegarde_petitions' => 'Säkerhetskopiera namninsamlingarna',
	'info_sauvegarde_refers' => 'Säkerhetskopiera länkarna',
	'info_sauvegarde_reussi_01' => 'Säkerhetskopieringen lyckades.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Säkerhetskopiera avdelningarna',
	'info_sauvegarde_signatures' => 'Säkerhetskopiera namininsamlingarnas underskrifter',
	'info_sauvegarde_sites_references' => 'säkerhetskopiera länkade sajter',
	'info_sauvegarde_type_documents' => 'Säkerhetskopiera dokumenttyper',
	'info_sauvegarde_visites' => 'Säkerhetskopiera besök',
	'info_une_breve' => 'en nyhet,',
	'item_mots_cles_association_breves' => 'nyheter',
	'item_nouvelle_breve' => 'Ny nyhet',

	// L
	'lien_forum_public' => 'Hantera den här artikelns publika forum',
	'lien_reponse_breve' => 'Skriv ett svar på nyheten',

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_tech_03' => 'Du kan välja att spara filen i komprimerad form för att 
 snabba upp överföringen till din dator eller till en backupserver och spara lite diskutrymme.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'Säkerhetskopian kommer att sparas okomprimerad i filen @fichier@.',
	'titre_nouvelle_breve' => 'Ny nyhet',
	'titre_page_breves_edit' => 'Editera nyheten: «@titre@»',
	'titre_page_forum' => 'Administratörernas forum',
	'titre_page_forum_envoi' => 'Sänd ett meddelande',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
