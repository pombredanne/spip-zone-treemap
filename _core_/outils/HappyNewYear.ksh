#!/bin/bash
a=$(find . -name "*.php" |grep -v extensions/ | grep -v /config/ | grep -v index.php | grep -v public.php | grep -v prive.php )
echo -n "Fichiers: "
echo $a|wc -w
for i in $a
do
	echo $i 
	sed -e "s/Copyright (c) 2001-20../Copyright (c) 2001-2012/"  $i > ~/tmp/f.php
	if diff -q $i ~/tmp/f.php
	then
		:
	else
		diff $i ~/tmp/f.php
    		mv ~/tmp/f.php $i
	fi
done
