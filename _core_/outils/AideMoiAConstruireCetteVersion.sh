#!/bin/sh 
#Adresse du repertoire svn du core de spip 
RepSpip=/home/ben/svn/spip

#Adresse du repertoire svn des extensions ( plugins intalles avec SPIP et developpes sur la zone ) 
RepExtensions=/home/ben/svn/spip-zone/_core_

#Nom de la branche actuelle 
NomBranche=spip-3.0

#le tag a construire 
NomAncienTag=spip-3.0.0

#le tag a construire 
NomNouveauTag=spip-3.0.2



RepSpipBranche=$RepSpip/branches
RepSpipTag=$RepSpip/tags

RepExtensionsBranche=$RepExtensions/branches
RepExtensionsTag=$RepExtensions/tags

# utile pour recuperer les logs 
# Numero de commit de $NomAncienTag ( a scripter mais pb de grep avec language francais ou anglais ) 
echo  "svn info $RepSpipTag/$NomAncienTag |grep cation |grep vision"
NumeroCommitRevisionPrecedenteSpip=`svn info $RepSpipTag/$NomAncienTag |grep cation |grep vision | cut -d ":" -f2` 
echo "Hop $NumeroCommitRevisionPrecedenteSpip" 
# Numero de commit de $NomNouveauTag ( a scripter mais pb de grep avec language francais ou anglais ) 
echo  "svn info $RepExtensionsTag/$NomAncienTag |grep cation |grep vision"
NumeroCommitRevisionPrecedenteZone=`svn info $RepExtensionsTag/$NomAncienTag |grep cation |grep vision | cut -d ":" -f2 `
echo "Hop $NumeroCommitRevisionPrecedenteZone" 

#################################################################
# README 
#################################################################
## 1) avoir a disposition un checkout du core 
## 2) avoir a dispo un checkout de la zone ( ou de zone/_core_) 
## 3) Mettre � jour la zone  ( svn update ) 
## 4) Mettre � jour le core ( svn update ) 
## 5) Mettre a jour les variables dans ce script AideMoiAConstruireCetteVersion 
## 6) Mettre a jour le numero de version $spip_version_branche dans  ~/svn/spip/branches/spip-2.1/ecrire/inc_version.php ( dans le cas pr�sent) 
## 7) Jouer une premiere fois le script 
## 8) Utiliser les deux fichiers logExtension.txt et logSpip.txt present dans le repertoire courant pour preparer le changelog 
## 9)Mettre a jour le changelog.txt dans le repertoire ~/svn/spip/branches/spip-2.1/CHANGELOG.txt 
##10)Jouer une seconde fois le script 
##11) normalement c'est bon , v�rifier en faisant un svn diff � partir des deux endroits ~/svn/spip/tags et ~/svn/spip-zone/_core_
##12) se placer dans chacun des repertoires et commiter 
##13) modifier le archivelist.txt en ajoutant la g�nr�ation du zip d'archive : "tags/spip-2.1.6;archives/SPIP-v2-1.6;spip"
##14) attendre que l'empacteur passe 
##15) Verifier que le zip correspond et le faire tester 
##16) modifier le archivelist.txt en modifiant la stable avec le bon tag "tags/spip-2.1.6;stable/spip;spip" 
##17) attendre que l'empacteur passe 
##18) Verifier que le zip correspond et le faire tester 
##19) modifier les squelettes spip.net  ~/svn/spip-zone/_galaxie_/www.spip.net/squelettes/modeles version_stabledate.html version_stable.html
##20) les commiter et demander de mettre a jour spip.net 
##21) spread the world 
##    ( verifier que l'on est abonn� aux listes avant d'envoyer / faire AUTANT de mails que de listes sinon crossposting et necessite une intervention manuelle de l'admin de la liste pour valider ) 
##    spip-ann@rezo.net ( moderee par defaut, necessite validation manuelle de l'admin de la liste) 
##    spip-dev@rezo.net 
##    spip-zone@rezo.net 
##    spip@rezo.net 
##    spip-trad@rezo.net 
##    twitter spipeau / facebook / spip.org / seenthis  
## Mettre a jour http://www.spip.net/fr_article4449.html 
##



#################################################################
# Pour info stdout debut janvier 2011 
#################################################################
##on tag la branche des extensions
##svn cp /home/scriibe/svn/spip-zone/_core_/branches/spip-2.1 /home/scriibe/svn/spip-zone/_core_/tags/TEST-2.1.6
##------------------------------------------
##svn cp /home/scriibe/svn/spip-zone/_core_/branches/spip-2.1 /home/scriibe/svn/spip-zone/_core_/tags/TEST-2.1.6
##A         /home/scriibe/svn/spip-zone/_core_/tags/TEST-2.1.6
##on tag la branche de spip
##svn cp /home/scriibe/svn/spip/branches/spip-2.1 /home/scriibe/svn/spip/tags/TEST-2.1.6
##------------------------------------------
##A         /home/scriibe/svn/spip/tags/TEST-2.1.6
##Propri�t� 'svn:externals' d�finie sur '.'
##


#################################################################
# le script 
#################################################################

RepOut="$RepExtensions/outils/"

#on commence par supprimer les repertoires ( utile en cas de relance du script plusieurs fois) 
rm -rf $RepExtensionsTag/$NomNouveauTag
rm -rf $RepSpipTag/$NomNouveauTag



### RECEUPRATION DES LOGS EXTENSIONS ###
cd $RepExtensionsBranche/$NomBranche
svn log -r $NumeroCommitRevisionPrecedenteZone:HEAD | tr -d '\n' | sed -s 's/---r/\nr/g' | sed 's/---//g' >$RepOut/logExtension.txt
sed -i 's/1 line//g' $RepOut/logExtension.txt
sed -i 's/[0-9]* lines//g' $RepOut/logExtension.txt


echo '' >$RepOut/logExtensionParUser.txt
for i in `svn log -r $NumeroCommitRevisionPrecedenteZone:HEAD  -q | cut -d "|" -f2 | grep -v "\-\-" |sort -u `
do 
  echo '----------------------------------' >>$RepOut/logExtensionParUser.txt
  grep $i $RepOut/logExtension.txt >>$RepOut/logExtensionParUser.txt
done 

#Un peu de comsetique sur le fichier de log ###
sed -i 's/brunobergot\@gmail.com/b_b       /g' $RepOut/logExtension.txt
sed -i 's/cedric\@yterium.com/cedric   /g' $RepOut/logExtension.txt
sed -i 's/booz\@rezo.net/booz     /g' $RepOut/logExtension.txt
sed -i 's/da\@weeno.net/davux    /g' $RepOut/logExtension.txt
sed -i 's/denisb\@laposte.net/denisb   /g' $RepOut/logExtension.txt
sed -i 's/esj\@rezo.net/esj      /g' $RepOut/logExtension.txt
sed -i 's/fil\@rezo.net/fil      /g' $RepOut/logExtension.txt
sed -i 's/kent1\@arscenic.info/kent1    /g' $RepOut/logExtension.txt
sed -i 's/marcimat\@rezo.net/marcimat /g' $RepOut/logExtension.txt
sed -i 's/romy\@rezo.net/romy     /g' $RepOut/logExtension.txt
sed -i 's/eric\@smellup.net/eric     /g' $RepOut/logExtension.txt
sed -i 's/rastapopoulos\@spip.org/rastapopoulos     /g' $RepOut/logExtension.txt
sed -i 's/arno\@rezo.net/arno     /g' $RepOut/logExtension.txt
sed -i 's/ben.spip\@gmail.com/ben     /g' $RepOut/logExtension.txt




### RECEUPRATION DES LOGS SPIP  ###
cd $RepSpipBranche/$NomBranche
svn log -r $NumeroCommitRevisionPrecedenteSpip:HEAD | tr -d '\n' | sed -s 's/---r/\nr/g' | sed 's/---//g'  >$RepOut/logSpip.txt


sed -i 's/1 line//g' $RepOut/logSpip.txt
sed -i 's/[0-9]* lines//g' $RepOut/logSpip.txt


echo '' >$RepOut/logSpipParUser.txt
for i in `svn log -r $NumeroCommitRevisionPrecedenteSpip:HEAD  -q | cut -d "|" -f2 | grep -v "\-\-" |sort -u `
do 
  echo '----------------------------------' >>$RepOut/logSpipParUser.txt
  grep $i $RepOut/logSpip.txt >>$RepOut/logSpipParUser.txt
done 



#Un peu de comsetique sur le fichier de log ###
sed -i 's/brunobergot\@gmail.com/b_b       /g' $RepOut/logSpip.txt
sed -i 's/cedric\@yterium.com/cedric   /g' $RepOut/logSpip.txt
sed -i 's/booz\@rezo.net/booz     /g' $RepOut/logSpip.txt
sed -i 's/da\@weeno.net/davux    /g' $RepOut/logSpip.txt
sed -i 's/denisb\@laposte.net/denisb   /g' $RepOut/logSpip.txt
sed -i 's/esj\@rezo.net/esj      /g' $RepOut/logSpip.txt
sed -i 's/fil\@rezo.net/fil      /g' $RepOut/logSpip.txt
sed -i 's/kent1\@arscenic.info/kent1    /g' $RepOut/logSpip.txt
sed -i 's/marcimat\@rezo.net/marcimat /g' $RepOut/logSpip.txt
sed -i 's/romy\@rezo.net/romy     /g' $RepOut/logSpip.txt
sed -i 's/eric\@smellup.net/eric     /g' $RepOut/logSpip.txt
sed -i 's/rastapopoulos\@spip.org/rastapopoulos     /g' $RepOut/logSpip.txt
sed -i 's/arno\@rezo.net/arno     /g' $RepOut/logSpip.txt
sed -i 's/ben.spip\@gmail.com/ben     /g' $RepOut/logSpip.txt




### GESTION DES EXTENSIONS ###
echo on tag la branche des extensions 
echo svn cp $RepExtensionsBranche/$NomBranche $RepExtensionsTag/$NomNouveauTag
echo "------------------------------------------"
echo svn cp $RepExtensionsBranche/$NomBranche $RepExtensionsTag/$NomNouveauTag
svn cp $RepExtensionsBranche/$NomBranche $RepExtensionsTag/$NomNouveauTag

### GESTION DE SPIP  ###
echo on tag la branche de spip  
echo svn cp $RepSpipBranche/$NomBranche $RepSpipTag/$NomNouveauTag
echo "------------------------------------------"
svn cp $RepSpipBranche/$NomBranche $RepSpipTag/$NomNouveauTag

### on modifie de facon propre les externals pour brancher sur la version ###
cd $RepSpipTag/$NomNouveauTag/
svn propget svn:externals . >$RepOut/dir-prop-base 
cat $RepOut/dir-prop-base | sed "s/_core_\/branches\/$NomBranche/_core_\/tags\/$NomNouveauTag/g"  > $RepOut/dir-prop-base2
cd $RepSpipTag/$NomNouveauTag/ 
svn propset  svn:externals .  --file $RepOut/dir-prop-base2 
