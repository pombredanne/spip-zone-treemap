Sauvegarde et restauration de la base SPIP
-------------------------------------------

Cette variante du plugin "dump" permet d'importer/exporter en sqlite ET/OU sql

L'origine de ce besoin est que l'export .sqlite plante pour certains hébergeurs qui ne supportent pas ce type de base de données.
Il est sensé corriger le bug signalé ici : http://core.spip.org/issues/2717

L'import est inspiré du logiciel BigDump qui permet de réaliser des imports de fichiers lourds.
L'export est inspiré du plugin Saveauto qui réalise déjà un export au format .sql