<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_core_/branches/spip-2.1/plugins/forum/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'aucun_post_forum' => 'No post forum',
	
	// I
	'icone_bruler_post' => "Report as Spam",
	'icone_bruler_messages' => "Report as Spam",
	'icone_legitimer_messages' => "Report as lawful",
	'icone_supprimer_messages' => 'Refuse these posts',
	'icone_valider_repondre_post' => 'Confirm & Reply to this post',
	'icone_valider_messages' => 'Confirm these posts',
	'info_liens_texte' => 'Link(s) in the text of the post',
	'info_liens_titre' => 'Link(s) in the title of the post',
	'interface_onglets' => 'Tabbed interface',
	'interface_formulaire' => 'Form interface',
	'info_1_message_forum' => '1 forum post',
	'info_message_numero' => 'Post number:',
	'info_nb_messages_forum' => '@nb@ forum posts',

	// L
	'label_afficher' => 'Display:',
	'label_nb_messages' => 'messages',
	'label_selectionner' => 'Select:',
	'lien_reponse_objet' => 'Reply to (@objet@)',
	
	// M
	'message_lie_objets' => 'Linked to the "@objet@" objects.',
	'message_lie_objet_id_objet' => 'Linked to the object "@objet@" #@id@ : <a href="@url@" class="spip_in">@titre@</a>.',
	'message_marque_comme_spam' => 'One post marked as spam',
	'message_publie' => 'One published post',
	'message_rien_a_faire' => 'No post has been selected',
	'message_supprime' => 'One deleted post',
	'messages_aucun' => 'None',
	'messages_marques_comme_spam' => '@nb@ posts marked as spam',
	'messages_meme_auteur' => 'All posts from this author',
	'messages_meme_email' => 'All posts from this email',
	'messages_meme_ip' => 'All posts from this IP',
	'messages_off' => 'Deleted',
	'messages_perso' => 'Personnals',
	'messages_prive' => 'Private',
	'messages_privoff' => 'Deleted',
	'messages_privrac' => 'General',
	'messages_privadm' => 'Administrators',
	'messages_prop' => 'Proposed',
	'messages_publie' => 'Published',
	'messages_publies' => '@nb@ published posts',
	'messages_spam' => 'Spam',
	'messages_supprimes' => '@nb@ deleted posts',
	'messages_tous' => 'All',
	
	// S
	'statut_off' => 'Refused',
	'statut_prop' => 'Proposed',
	'statut_publie' => 'Published',
	'statut_spam' => 'Spam',
	
	// T
	'texte_en_cours_validation'=> 'Articles, news, forums below are submitted to publication.',
	'tout_voir' => 'Display all posts',
	'texte_messages_publics' => 'Public messages:',
	
	// V
	'voir_messages_objet' => 'display the posts'

);


?>