<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/grenier?lang_cible=pt
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FÓRUM & ABAIXO-ASSINADO', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'Salvaguarda comprimida como @fichier@', # MODIF
	'bouton_radio_sauvegarde_non_compressee' => 'salvaguarda não comprimida como   @fichier@', # MODIF

	// F
	'forum_probleme_database' => 'Problema de base de dados, a sua mensagem não foi registada.',

	// I
	'ical_lien_rss_breves' => 'Syndication des brèves du site', # NEW
	'icone_creer_mot_cle_breve' => 'Créer un nouveau mot-clé et le lier à cette brève', # NEW
	'icone_forum_administrateur' => 'Fórum dos administradores',
	'icone_forum_suivi' => 'Seguimento dos fóruns',
	'icone_publier_breve' => 'Publicar esta notícia',
	'icone_refuser_breve' => 'Recusar esta notícia',
	'info_base_restauration' => 'A base está em curso de restauro',
	'info_breves_03' => 'notícias',
	'info_breves_liees_mot' => 'As notícias ligadas a esta palavra-chave',
	'info_breves_touvees' => 'Notícias encontradas',
	'info_breves_touvees_dans_texte' => 'Notícias encontradas (no texto)',
	'info_echange_message' => 'SPIP permite a troca de mensagens e a constituição de fóruns privados de discussão
entre os participantes do sítio. Pode activar ou
 desactivar esta funcionalidade.',
	'info_erreur_restauration' => 'Erro de restauro : ficheiro inexistente',
	'info_forum_administrateur' => 'fórum dos administradores',
	'info_forum_interne' => 'fórum interno',
	'info_forum_ouvert' => 'No espaço privado do sítio, um fórum está aberto a todos
 os redactores registados. Pode, a seguir, activar um
 fórum suplementar, reservado apenas aos administradores',
	'info_gauche_suivi_forum' => 'A página de <i>acompanhamento dos fóruns </i> é um instrumento de gestão do seu sítio (e não um espaço de discussão ou de redacção). Exibe todas as contribuições do fórum público deste artigo e permite-lhe gerir essas contribuições. ', # MODIF
	'info_modifier_breve' => 'Modificar a notícia :',
	'info_nombre_breves' => '@nb_breves@ notícias,',
	'info_option_ne_pas_faire_suivre' => 'Não fazer seguir as mensagens dos fóruns',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Salvaguardar os artigos',
	'info_sauvegarde_articles_sites_ref' => 'Salvaguardar os artigos dos sítios referenciados',
	'info_sauvegarde_auteurs' => 'Salvaguardar os autores',
	'info_sauvegarde_breves' => 'Salvaguardar as notícias',
	'info_sauvegarde_documents' => 'Salvaguardar os documentos',
	'info_sauvegarde_echouee' => 'Se a salvaguarda falhar («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Salvaguardar os fóruns',
	'info_sauvegarde_groupe_mots' => 'Salvaguardar os grupos de palavras',
	'info_sauvegarde_messages' => 'Salvaguardar as mensagens',
	'info_sauvegarde_mots_cles' => 'Salvaguardar as palavras-chave',
	'info_sauvegarde_petitions' => 'Salvaguardar os abaixo-assinados',
	'info_sauvegarde_refers' => 'Salvaguardar os referers',
	'info_sauvegarde_reussi_01' => 'Salvaguarda bem sucedida',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Salvaguardar as rubricas',
	'info_sauvegarde_signatures' => 'Salvaguardar as assinaturas de abaixo-assinados',
	'info_sauvegarde_sites_references' => 'Salvaguardar os sítios referenciados',
	'info_sauvegarde_type_documents' => 'Salvaguardar os tipos de documentos',
	'info_sauvegarde_visites' => 'Salvaguardar as visitas',
	'info_une_breve' => 'uma notícia,',
	'item_mots_cles_association_breves' => 'às notícias',
	'item_nouvelle_breve' => 'Nova notícia',

	// L
	'lien_forum_public' => 'Gerir o fórum público deste artigo',
	'lien_reponse_breve' => 'Resposta à notícia',

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_tech_03' => 'Pode escolher a salvaguarda do ficheiro sob a forma comprimida, para
encurtar a sua transferência para o seu computador ou para um servidor de salvaguarda e poupar o espaço do disco.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'A salvaguarda far-se-á no ficheiro não comprimido @fichier@.', # MODIF
	'titre_nouvelle_breve' => 'Nova notícia',
	'titre_page_breves_edit' => 'Modificar a notícia : « @titre@ »',
	'titre_page_forum' => 'Fórum dos administradores',
	'titre_page_forum_envoi' => 'Enviar uma mensagem',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
