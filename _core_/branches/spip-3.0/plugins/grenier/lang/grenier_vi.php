<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/grenier?lang_cible=vi
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'DIỄN ĐÀN & THỈNH NGUYỆN THƯ', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'lưu trữ lại dưới dạng nén trong
@fichier@', # MODIF
	'bouton_radio_sauvegarde_non_compressee' => 'lưu trữ lại dưới dạng thường trong
@fichier@', # MODIF

	// F
	'forum_probleme_database' => 'Có vấn đề với database, thư tín của bạn không lưu trữ được.',

	// I
	'ical_lien_rss_breves' => 'Syndication of the site\'s news items', # NEW
	'icone_creer_mot_cle_breve' => 'Create a new keyword and attach it to this news item', # NEW
	'icone_forum_administrateur' => 'Diễn đàn quản trị viên',
	'icone_forum_suivi' => 'Quản trị diễn đàn',
	'icone_publier_breve' => 'Ðăng mẫu tin này',
	'icone_refuser_breve' => 'Từ chối tin ngắn này',
	'info_base_restauration' => 'Đang hồi phục lại database.',
	'info_breves_03' => ' tin ngắn',
	'info_breves_liees_mot' => 'Những tin ngắn liên hệ đến từ then chốt này',
	'info_breves_touvees' => 'Những tin ngắn tìm thấy',
	'info_breves_touvees_dans_texte' => 'Những tin ngắn tìm thấy (trong văn bản)',
	'info_echange_message' => 'SPIP cho phép trao đổi thư tín và lập ra những diễn đàn trao đổi riêng giữa các tham dự viên. Bạn có thể tắt/mở đặc điểm này.',
	'info_erreur_restauration' => 'Lỗi phục hồi: không tìm thấy hồ sơ.',
	'info_forum_administrateur' => 'Diễn đàn quản trị viên',
	'info_forum_interne' => 'Diễn đàn nội bộ',
	'info_forum_ouvert' => 'Trong vùng riêng, một diễn đàn được mở ra cho mọi chủ bút đã có ghi danh. Bạn có thể tắt/mở một diễn đàn phụ trội dành riêng cho các quản trị viên.',
	'info_gauche_suivi_forum' => 'Trang <i>Quản trị Diễn Đàn</i> là một phương tiện quản trị của trang web (không dùng để trao đổi hay sửa đổi). Trang này liệt kê tất cả mọi thư tín trong diễn đàn công cộng của bài này và cho phép bạn quản trị những thư tín này.',
	'info_modifier_breve' => 'Sửa đổi tin ngắn:',
	'info_nombre_breves' => '@nb_breves@ tin ngắn, ',
	'info_option_ne_pas_faire_suivre' => 'Đừng chuyển thư tín của diễn đàn',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Dự trữ các bài vở',
	'info_sauvegarde_articles_sites_ref' => 'Dự trữ bài vở của các website nối kết',
	'info_sauvegarde_auteurs' => 'Dự trữ các tác giả',
	'info_sauvegarde_breves' => 'Dự trữ các tin ngắn',
	'info_sauvegarde_documents' => 'Dự trữ tài liệu',
	'info_sauvegarde_echouee' => 'Nếu dự trữ thất bại («quá tối đa thời gian thực hiện»),',
	'info_sauvegarde_forums' => 'Dự trữ các diễn đàn',
	'info_sauvegarde_groupe_mots' => 'Dự trữ nhóm từ then chốt',
	'info_sauvegarde_messages' => 'Dự trữ nhắn tin',
	'info_sauvegarde_mots_cles' => 'Dự trữ các từ then chốt',
	'info_sauvegarde_petitions' => 'Dự trữ các thỉnh nguyện thư',
	'info_sauvegarde_refers' => 'Dự trữ referrers',
	'info_sauvegarde_reussi_01' => 'Dự trữ thành công.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Dự trữ các đề mục',
	'info_sauvegarde_signatures' => 'Dự trữ các chữ ký thỉnh nguyện thư',
	'info_sauvegarde_sites_references' => 'Dự trữ các website nối kết',
	'info_sauvegarde_type_documents' => 'Dự trữ tên loại tài liệu',
	'info_sauvegarde_visites' => 'Dự trữ các viếng thăm',
	'info_une_breve' => 'một tin ngắn, ',
	'item_mots_cles_association_breves' => 'tin ngắn',
	'item_nouvelle_breve' => 'Tin ngắn mới',

	// L
	'lien_forum_public' => 'Quản trị diễn đàn công của bài này',
	'lien_reponse_breve' => 'Hồi âm tin ngắn',

	// S
	'sauvegarde_fusionner' => 'Merge the current database with the backup', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'If necessary, the URL of the source site:', # NEW

	// T
	'texte_admin_tech_03' => 'Bạn có thể chọn giữ hồ sơ trong dạng nén để sao chép, chuyển tải cho lẹ, cũng như tiết kiệm được chỗ trong dĩa.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'Dự trữ sẽ chứa trong hồ sơ không nén @fichier@.', # MODIF
	'titre_nouvelle_breve' => 'Tin ngắn mới',
	'titre_page_breves_edit' => 'Sửa đổi tin ngắn: «@titre@»',
	'titre_page_forum' => 'Diễn đàn cho các quản lý',
	'titre_page_forum_envoi' => 'Gửi thư tín',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
