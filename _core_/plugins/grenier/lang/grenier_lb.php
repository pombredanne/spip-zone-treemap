<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/grenier?lang_cible=lb
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORUM & PETITIOUN',
	'bouton_radio_sauvegarde_compressee' => 'Backup ass kompriméiert ënner @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'Backup ass nët kompriméiert ënner @fichier@',

	// F
	'forum_probleme_database' => 'Problem mat der Datebank, äre Message gouf nët enregistréiert.',

	// I
	'ical_lien_rss_breves' => 'Syndicatioun vun de Kurzmeldungen vum Site',
	'icone_creer_mot_cle_breve' => 'E neit Schlësselwuert maachen an et un dës Kurzmeldung bannen',
	'icone_forum_administrateur' => 'Forum vun den Adminstrateuren',
	'icone_forum_suivi' => 'Suivi vun den Forum\'en',
	'icone_publier_breve' => 'Dës Kuerzmeldung publizéieren',
	'icone_refuser_breve' => 'Dës Kuerzmeldung refuséieren',
	'info_base_restauration' => 'D\'Datebank gët grad restauréiert.',
	'info_breves_03' => 'Kuerzmeldungen',
	'info_breves_liees_mot' => 'Kuerzmeldungen déi un dëst Schlësselwuert gebonnen sinn',
	'info_breves_touvees' => 'Kuerzmeldungen fonnt',
	'info_breves_touvees_dans_texte' => 'Kuerzmeldungen fonnt (am Text)',
	'info_echange_message' => 'SPIP erlaabt d\'Schécken vu Messagen a privat Forum\'en tëschent den Mataarbechter vum Site. Dir kënnt dës Fonktionnalitéit an- oder ausschalten.',
	'info_erreur_restauration' => 'Fehler bei der Restauratioun: de Fichier gëtt ët nët.',
	'info_forum_administrateur' => 'Forum vun den Administrateuren',
	'info_forum_interne' => 'Internen Forum',
	'info_forum_ouvert' => 'Am privaten Deel vum Site gët ët e Forum fir d\'Redakteren. Dir kënnt hei nach e Forum, just fir d\'Administrateuren, aschalten.',
	'info_gauche_suivi_forum' => 'D\'Säit <i>Gestioun Forum\'en</i> ass fir d\'Maintenance (an nët fir ze diskutéieren oder ze schreiwen). Si weist all d\'Beiträg vum öffentlëche Forum fir dësen Artikel an erlaabt dës Beiträg ze géréieren.',
	'info_modifier_breve' => 'Kuerzmeldung änneren:',
	'info_nombre_breves' => '@nb_breves@ Kuerzmeldungen,',
	'info_option_ne_pas_faire_suivre' => 'D\'Messagen vun de Forum\'en nët schécken',
	'info_restauration_sauvegarde_insert' => 'Insert vum @archive@ an d\'Datebank',
	'info_sauvegarde_articles' => 'Artikelen späicheren',
	'info_sauvegarde_articles_sites_ref' => 'Artikelen vun de referenzéierten Siten späicheren',
	'info_sauvegarde_auteurs' => 'Auteuren späicheren',
	'info_sauvegarde_breves' => 'Kuerzmeldungen späicheren',
	'info_sauvegarde_documents' => 'Dokumenter späicheren',
	'info_sauvegarde_echouee' => 'Wann de Backup nët fonktionnéiert ("Maximum execution time exceeded"),',
	'info_sauvegarde_forums' => 'Forum\'en späicheren',
	'info_sauvegarde_groupe_mots' => 'Schlësselwierder-Gruppen späicheren',
	'info_sauvegarde_messages' => 'Messagen späicheren',
	'info_sauvegarde_mots_cles' => 'Schlësselwierder späicheren',
	'info_sauvegarde_petitions' => 'Petitiounen späicheren',
	'info_sauvegarde_refers' => 'Referer\'en späicheren',
	'info_sauvegarde_reussi_01' => 'Backup färdeg.',
	'info_sauvegarde_rubrique_reussi' => 'D\'Tafelen vun der Rubrik @titre@ sinn am @archive@ gespäichert. Dir kënnt',
	'info_sauvegarde_rubriques' => 'Rubriken späicheren',
	'info_sauvegarde_signatures' => 'Ënnerschrëften vun de Petitiounen späicheren',
	'info_sauvegarde_sites_references' => 'Referenzéiert Siten späicheren',
	'info_sauvegarde_type_documents' => 'Dokument-Typen späicheren',
	'info_sauvegarde_visites' => 'Visiten späicheren',
	'info_une_breve' => 'eng Kuerzmeldung,',
	'item_mots_cles_association_breves' => 'vun de Kuerzmeldungen',
	'item_nouvelle_breve' => 'Nei Kuerzmeldung',

	// L
	'lien_forum_public' => 'Öffentlëche Forum vum Artikel aschalten',
	'lien_reponse_breve' => 'Äntwert op d\'Kuerzmeldung',

	// S
	'sauvegarde_fusionner' => 'Déi aktuell Datebank mam Backup fusionnéieren',
	'sauvegarde_fusionner_depublier' => 'Déi fusionnéiert Objekter nët méi veröffentlëche',
	'sauvegarde_url_origine' => 'Eventuell d\'Url vum Ausgangs-Site:',

	// T
	'texte_admin_tech_03' => 'Dir kënnt de Fichier kompriméiert späicheren fir méi e séieren Downlued oder Transfert op e Backup-Server, a fir Disk-Plaz ze spueren.',
	'texte_admin_tech_04' => 'Fir mat enger anerer Datebank ze fusionnéiere kënnt dir de Backup op dës Rubrik begrenzen: ',
	'texte_sauvegarde_compressee' => 'De Backup gët den nët kompriméierten Fichier @fichier@.',
	'titre_nouvelle_breve' => 'Nei Kuerzmeldung',
	'titre_page_breves_edit' => 'Kuerzmeldung änneren: "@titre@"',
	'titre_page_forum' => 'Forum vun den Administrateuren',
	'titre_page_forum_envoi' => 'Message schécken',
	'titre_page_statistiques_messages_forum' => 'Forum-Messagen'
);

?>
