<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/grenier?lang_cible=oc_ni_la
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORUM E PETICION', # MODIF
	'bouton_radio_sauvegarde_compressee' => 'Sauvagarda comprimida sota @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'Sauvagarda non comprimida sota @fichier@',

	// F
	'forum_probleme_database' => 'Problèma de basa de donadas, lo vòstre messatge non s\'es registrat.',

	// I
	'ical_lien_rss_breves' => 'Syndication des brèves du site', # NEW
	'icone_creer_mot_cle_breve' => 'Créer un nouveau mot-clé et le lier à cette brève', # NEW
	'icone_forum_administrateur' => 'Forum dei administrators',
	'icone_forum_suivi' => 'Segut dei forums',
	'icone_publier_breve' => 'Publicar aquela brèva',
	'icone_refuser_breve' => 'Refusar aquela brèva',
	'info_base_restauration' => 'La basa es en cors de restauracion.',
	'info_breves_03' => 'brèvas',
	'info_breves_liees_mot' => 'Li brèvas ligadi a aqueu mot clau',
	'info_breves_touvees' => 'Brèvas trobadi',
	'info_breves_touvees_dans_texte' => 'Brèvas trobadi (dins lo tèxt)',
	'info_echange_message' => 'SPIP permete d\'escambiar de messatges e de constituir de forums privats de discussion entre lu participants dau sit. Podètz activar ò desactivar aquela foncionalitat.',
	'info_erreur_restauration' => 'Error de restauracion: fichier inexistent.',
	'info_forum_administrateur' => 'forum dei administrators',
	'info_forum_interne' => 'forum intèrne',
	'info_forum_ouvert' => 'Dins l\'espaci privat dau sit, un forum es dubèrt a toi lu redactors registrats. Podètz, çai sota, activar un forum suplementari, reservat basta au administrators.',
	'info_gauche_suivi_forum' => 'La pàgina de <i>segut dei forums</i> es una aisina de gestion dau vòstre sit (mas non es un espaci per discutir ò per redigir). Aficha toti li contribucions dau forum public d\'aquel article e vos permete de gerir aqueli contribucions.',
	'info_modifier_breve' => 'Modificar la brèva:',
	'info_nombre_breves' => '@nb_breves@ brèvas, ',
	'info_option_ne_pas_faire_suivre' => 'Non faire sègre lu messatges dei forums',
	'info_restauration_sauvegarde_insert' => 'Insertion de @archive@ dans la base', # NEW
	'info_sauvegarde_articles' => 'Sauvagardar lu articles',
	'info_sauvegarde_articles_sites_ref' => 'Sauvagardar lu articles dei sits referenciats',
	'info_sauvegarde_auteurs' => 'Sauvagardar lu autors',
	'info_sauvegarde_breves' => 'Sauvagardar li brèvas',
	'info_sauvegarde_documents' => 'Sauvagardar lu documents',
	'info_sauvegarde_echouee' => 'Se la sauvagarda s\'es encalada(«Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Sauvagardar lu forums',
	'info_sauvegarde_groupe_mots' => 'Sauvagardar lu grops de mots',
	'info_sauvegarde_messages' => 'Sauvagardar lu messatges',
	'info_sauvegarde_mots_cles' => 'Sauvagardar lu mots clau',
	'info_sauvegarde_petitions' => 'Sauvagardar li peticions',
	'info_sauvegarde_refers' => 'Sauvagardar lu referidors',
	'info_sauvegarde_reussi_01' => 'Sauvagarda capitada.',
	'info_sauvegarde_rubrique_reussi' => 'Les tables de la rubrique @titre@ ont été sauvegardée dans @archive@. Vous pouvez', # NEW
	'info_sauvegarde_rubriques' => 'Sauvagardar li rubricas',
	'info_sauvegarde_signatures' => 'Sauvagardar li signaturas de peticions',
	'info_sauvegarde_sites_references' => 'Sauvagardar lu sits referenciats',
	'info_sauvegarde_type_documents' => 'Sauvagardar lu tipes de documents',
	'info_sauvegarde_visites' => 'Sauvagardar li vesitas',
	'info_une_breve' => 'una brèva, ',
	'item_mots_cles_association_breves' => 'ai brèvas',
	'item_nouvelle_breve' => 'Brèva nòva',

	// L
	'lien_forum_public' => 'Gerir lo forum public d\'aquel article',
	'lien_reponse_breve' => 'Respòsta a la brèva',

	// S
	'sauvegarde_fusionner' => 'Fusionner la base actuelle et la sauvegarde', # NEW
	'sauvegarde_fusionner_depublier' => 'Dépublier les objets fusionnés', # NEW
	'sauvegarde_url_origine' => 'Eventuellement, URL du site d\'origine :', # NEW

	// T
	'texte_admin_tech_03' => 'Podètz chausir de sauvagardar lo fichier sota forma comprimida, per fin
 d\'abrivar lo sieu transferiment au vòstre ò a un servidor de sauvagardas, e per fin d\'esparnhar d\'espaci disc.',
	'texte_admin_tech_04' => 'Dans un but de fusion avec une autre base, vous pouvez limiter la sauvegarde à la rubrique: ', # NEW
	'texte_sauvegarde_compressee' => 'La sauvagarda si farà dins lo fichier non comprimit @fichier@.',
	'titre_nouvelle_breve' => 'Brèva novèla',
	'titre_page_breves_edit' => 'Modificar la brèva: «@titre@»',
	'titre_page_forum' => 'Forum per lu administrators',
	'titre_page_forum_envoi' => 'Mandar un messatge',
	'titre_page_statistiques_messages_forum' => 'Messages de forum' # NEW
);

?>
