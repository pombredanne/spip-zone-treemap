<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/grenier?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => 'FORUM E PETIZIONI',
	'bouton_radio_sauvegarde_compressee' => 'backup compresso in @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'backup non compresso in @fichier@',

	// F
	'forum_probleme_database' => 'Problema nel database. Il tuo messaggio non è stato registrato.',

	// I
	'ical_lien_rss_breves' => 'Syndication delle brevi del sito',
	'icone_creer_mot_cle_breve' => 'Creare una nuova parola chiave e collegarla a questa breve',
	'icone_forum_administrateur' => 'Forum degli amministratori',
	'icone_forum_suivi' => 'Monitoraggio dei forum',
	'icone_publier_breve' => 'Pubblica questa breve',
	'icone_refuser_breve' => 'Rifiuta questa breve',
	'info_base_restauration' => 'Il database è in fase di ripristino.',
	'info_breves_03' => 'brevi',
	'info_breves_liees_mot' => 'Le brevi collegate a questa parola chiave',
	'info_breves_touvees' => 'Brevi trovate',
	'info_breves_touvees_dans_texte' => 'Brevi trovate (nel testo)',
	'info_echange_message' => 'SPIP permette lo scambio di messaggi e la costituzione di forum di discussione
privati tra i membri del sito. Puoi attivare o disattivare questa funzionalità.',
	'info_erreur_restauration' => 'Errore di ripristino: file inesistente.',
	'info_forum_administrateur' => 'forum degli amministratori',
	'info_forum_interne' => 'forum interno',
	'info_forum_ouvert' => 'Nell\'area riservata del sito, è disponibile un forum per
tutti i redattori registrati. È anche possibile attivare un
secondo forum riservato ai soli amministratori.',
	'info_gauche_suivi_forum' => 'La pagina di <i>monitoraggio dei forum</i> è uno strumento di gestione del sito (e non uno spazio di discussione o di redazione). In essa sono pubblicati tutti i contributi del forum pubblico di quest\'articolo, permettendone la gestione.',
	'info_modifier_breve' => 'Modifica la breve:',
	'info_nombre_breves' => '@nb_breves@ brevi, ',
	'info_option_ne_pas_faire_suivre' => 'Non segnalare i nuovi messaggi',
	'info_restauration_sauvegarde_insert' => 'Inserimento di @archive@ nel database',
	'info_sauvegarde_articles' => 'Salva gli articoli',
	'info_sauvegarde_articles_sites_ref' => 'Salva gli articoli dei siti citati',
	'info_sauvegarde_auteurs' => 'Salva gli autori',
	'info_sauvegarde_breves' => 'Salva le brevi',
	'info_sauvegarde_documents' => 'Salva i documenti',
	'info_sauvegarde_echouee' => 'Se il salvataggio è fallito («Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'Salva i forum',
	'info_sauvegarde_groupe_mots' => 'Salva i gruppi di parole',
	'info_sauvegarde_messages' => 'Salva i messaggi',
	'info_sauvegarde_mots_cles' => 'Salva le parole chiave',
	'info_sauvegarde_petitions' => 'Salva le petizioni  ',
	'info_sauvegarde_refers' => 'Salva i <em>referrer</em> (siti che fanno riferimento al tuo)',
	'info_sauvegarde_reussi_01' => 'Salvataggio riuscito.',
	'info_sauvegarde_rubrique_reussi' => 'Le tabelle della rubrica @titre@ sono state salvate in @archive@. È possibile',
	'info_sauvegarde_rubriques' => 'Salva le rubriche',
	'info_sauvegarde_signatures' => 'Salva le adesioni alle petizioni',
	'info_sauvegarde_sites_references' => 'Salva i siti in repertorio',
	'info_sauvegarde_type_documents' => 'Salva i tipi di documento',
	'info_sauvegarde_visites' => 'Salva le visite',
	'info_une_breve' => 'una breve, ',
	'item_mots_cles_association_breves' => 'alle brevi',
	'item_nouvelle_breve' => 'Nuova breve',

	// L
	'lien_forum_public' => 'Gestisci il forum pubblico di quest\'articolo',
	'lien_reponse_breve' => 'Risposta alla breve',

	// S
	'sauvegarde_fusionner' => 'Unire il database attuale con la copia di backup',
	'sauvegarde_fusionner_depublier' => 'Depubblicare gli oggetti fusi',
	'sauvegarde_url_origine' => 'Eventualmente, URL del sito di origine:',

	// T
	'texte_admin_tech_03' => 'Puoi scegliere di salvare il file in forma compressa, al fine di ridurre
i tempi di trasferimento in rete e di risparmiare dello spazio su disco.',
	'texte_admin_tech_04' => 'Al fine di una fusione con un altro database, è possibile limitare il salvataggio alla rubrica: ',
	'texte_sauvegarde_compressee' => 'Il salvataggio avverrà nel file non compresso @fichier@.',
	'titre_nouvelle_breve' => 'Nuova breve',
	'titre_page_breves_edit' => 'Modifica la breve: «@titre@»',
	'titre_page_forum' => 'Forum degli amministratori',
	'titre_page_forum_envoi' => 'Invia un messaggio',
	'titre_page_statistiques_messages_forum' => 'Messaggi dei forum'
);

?>
