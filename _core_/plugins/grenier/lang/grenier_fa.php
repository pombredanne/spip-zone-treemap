<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/grenier?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_forum_petition' => ' (طومار) سخنگاه و درخواست ',
	'bouton_radio_sauvegarde_compressee' => 'ذخيره فشرده در  @fichier@',
	'bouton_radio_sauvegarde_non_compressee' => 'ذخيره فشرده نشده در@fichier@',

	// F
	'forum_probleme_database' => 'نقص در داده پى ها، پيام شما ثبت نشده',

	// I
	'ical_lien_rss_breves' => 'پیوند اخبار کوتاه سایت',
	'icone_creer_mot_cle_breve' => 'ساختن كليدواژه‌ي جديد و چسباندن آن به اين خبر',
	'icone_forum_administrateur' => 'سخنگاه براى گردانند گان سايت',
	'icone_forum_suivi' => 'دنباله سخنگاه',
	'icone_publier_breve' => 'اين مقاله كوتاه را منتشر كنيد',
	'icone_refuser_breve' => 'اين مقاله كوتاه را رد كنيد',
	'info_base_restauration' => '.داده پى ها در حال بازسازى ميباشند',
	'info_breves_03' => 'مقاله هاى كوتاه',
	'info_breves_liees_mot' => 'مقاله هاى كوتاه مربوط به اين واژه-كليد',
	'info_breves_touvees' => 'مقاله هاى كوتاه پيدا شده',
	'info_breves_touvees_dans_texte' => ' (مقاله هاى كوتاه پيدا شده (درون متن',
	'info_echange_message' => 'اين سيستم امكان تبادل پيام و مباحثه خصوصى را بين كاربران سايت ميدهد. شما ميتوانيد اين كاربرد را فعال يا غير فعال كنيد',
	'info_erreur_restauration' => '.اشتباه در بازسازى : فايل موجود نميباشد',
	'info_forum_administrateur' => 'سخنگاه براى گردانند گان سايت',
	'info_forum_interne' => 'سخنگاه داخلى',
	'info_forum_ouvert' => 'در قسمت همگانى سايت، يك سخنگاه براى همه نگارند گان موجود ميباشد. شما ميتوانيد، در زير يك سخنگاه اضافى مربوط به گردانند گان سايت را فعال كنيد.',
	'info_gauche_suivi_forum' => 'صفحه دنباله سخنگاه از ابزار اداره سايتتان ميباشد (و نه مكانى براى بحث يا نگارش) و امكان اعلان تمام پيامها ى سخنگاه همگانى اين مقاله و همچنين اداره پيامها را بشما ميدهد',
	'info_modifier_breve' => ':اصلاح مقاله كوتاه',
	'info_nombre_breves' => 'مقاله كوتاه @nb_breves@ ',
	'info_option_ne_pas_faire_suivre' => 'پيام هاى سخنگاه را ارسال نكنيد',
	'info_restauration_sauvegarde_insert' => 'گنجاندن @archive@ در پايگاه',
	'info_sauvegarde_articles' => 'مقالات را ذخيره كنيد',
	'info_sauvegarde_articles_sites_ref' => 'مقالات سايتهاى پيوندى را ذخيره كنيد',
	'info_sauvegarde_auteurs' => 'فهرست نويسندگان را ذخيره كنيد',
	'info_sauvegarde_breves' => 'مقالات كوتاه را ذخيره كنيد',
	'info_sauvegarde_documents' => 'اسناد را ذخيره كنيد',
	'info_sauvegarde_echouee' => 'اگر ذخيره سازى شكست خورد(«Maximum execution time exceeded»),',
	'info_sauvegarde_forums' => 'سخنگاه را ذخيره كنيد',
	'info_sauvegarde_groupe_mots' => 'گروه واژه ها را ذخيره كنيد',
	'info_sauvegarde_messages' => 'پيام ها را ذخيره كنيد',
	'info_sauvegarde_mots_cles' => 'واژه-كليدها را ذخيره كنيد',
	'info_sauvegarde_petitions' => 'سخنگاه را ذخيره كنيد',
	'info_sauvegarde_refers' => 'مراجع را ذخيره كنيد',
	'info_sauvegarde_reussi_01' => 'ذخيره سازى موفقيت آميز',
	'info_sauvegarde_rubrique_reussi' => 'جدول‌هاي بخش @titre@ در @archive@ نگه‌داري شده‌اند. شما مي‌تواتيد  ',
	'info_sauvegarde_rubriques' => 'بخشها را ذخيره كنيد',
	'info_sauvegarde_signatures' => 'امضاهاى سخنگاه را ذخيره كنيد',
	'info_sauvegarde_sites_references' => 'سايتهاى پيوندى را ذخيره كنيد',
	'info_sauvegarde_type_documents' => 'انواع اسناد را ذخيره كنيد',
	'info_sauvegarde_visites' => 'بازديدها را ذخيره كنيد',
	'info_une_breve' => ',يك مقاله كوتاه',
	'item_mots_cles_association_breves' => 'به مقالات كوتاه',
	'item_nouvelle_breve' => 'مقاله كوتاه جديد',

	// L
	'lien_forum_public' => 'سخنگاه همگانى اين مقاله را اداره كنيد',
	'lien_reponse_breve' => 'پاسخ به مقاله كوتاه',

	// S
	'sauvegarde_fusionner' => 'داده پی کنونی را ادغام و ذخیره کنید',
	'sauvegarde_fusionner_depublier' => 'عدم چاپ چيزهاي ادغام شده',
	'sauvegarde_url_origine' => 'احتمالأ URL سایت اصلی: ',

	// T
	'texte_admin_tech_03' => 'شما ميتوانيد گزينش ذخيره كردن فايلها را بصورت فشرده انتخاب كنيد تا مسير انتقالشان را به خودتان يا به سرور ذخيره كوتاه تر كنيد. همچنين فضاى كمترى را در ديسك سخت اشغال كنيد.',
	'texte_admin_tech_04' => 'براي ادغام با پايگاه‌داده‌هاي ديگر، مي‌توانيد پشتيبان‌گيري (بك‌آپ) را به يك بخش محدود كنيد: ',
	'texte_sauvegarde_compressee' => 'ذخيره در فايل فشرده نشده صورت خواهد گرفت@fichier@.',
	'titre_nouvelle_breve' => 'مقاله كوتاه جديد',
	'titre_page_breves_edit' => ' اصلاح مقاله كوتاه : « @titre@ »',
	'titre_page_forum' => 'سخنگاه گردانندگان سايت',
	'titre_page_forum_envoi' => 'يك پيام بفرستيد',
	'titre_page_statistiques_messages_forum' => 'پيام‌هاي سخنگاه'
);

?>
