<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/charter?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_checkbox' => 'A multiple choice',
	'label_checkbox_1' => 'A first choice',
	'label_checkbox_2' => 'A second choice',
	'label_checkbox_long_label' => 'A multiple choice with a very long label that uses a class <tt>.long_label</tt>',
	'label_checkbox_long_label_1' => 'A first choice',
	'label_checkbox_long_label_2' => 'A second choice',
	'label_radio' => 'A single choice',
	'label_radio_non' => 'No',
	'label_radio_oui' => 'Yes',
	'label_select' => 'A select entry',
	'label_select_non' => 'No thanks, not at all',
	'label_select_oui' => 'That\'s it, YES',
	'label_text' => 'A text input',
	'label_text_long_label' => 'An entry with a very very long label that uses a class <tt>.long_label</tt>',
	'label_text_obli' => 'A mandatory input',
	'label_textarea' => 'A block entry',
	'label_textarea_pleine_largeur' => 'A block entry with <tt>.pleine_largeur</tt>',
	'label_textarea_pleine_largeur_obli' => 'A mandatory block entry with <tt>.pleine_largeur</tt>',
	'legend' => 'Legend of a fieldset for a group of entries',

	// T
	'titre_boites' => 'Boxes',
	'titre_charte' => 'Convention',
	'titre_formulaires' => 'Forms',
	'titre_icones' => 'Icons',
	'titre_listes' => 'Lists',
	'titre_typo' => 'Typo'
);

?>
