<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/charter?lang_cible=ar
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_checkbox' => 'خيار متعدد',
	'label_checkbox_1' => 'خيار أول',
	'label_checkbox_2' => 'خيار ثان',
	'label_checkbox_long_label' => 'خيار متعدد بعنوان طويل جداً يستخدم نمط <tt>.long_label</tt>',
	'label_checkbox_long_label_1' => 'خيار أول',
	'label_checkbox_long_label_2' => 'خيار ثان',
	'label_radio' => 'خيار وحيد',
	'label_radio_non' => 'كلا',
	'label_radio_oui' => 'نعم',
	'label_select' => 'إدخال على شكل تحديد (select)',
	'label_select_non' => 'كلا أبداً، مشكور',
	'label_select_oui' => 'نعم، هكذا بالفعل',
	'label_text' => 'إدخال نصي',
	'label_text_long_label' => 'إدخال بعنوان طويل جداً يستخدم نمط <tt>.long_label</tt>',
	'label_text_obli' => 'إدخال إجباري',
	'label_textarea' => 'إدخال على شكل مقطع نص',
	'label_textarea_pleine_largeur' => 'إدخال على شكل مقطع بخاصية <tt>.pleine_largeur</tt>',
	'label_textarea_pleine_largeur_obli' => 'إدخال إجباري على شكل مقطع بخاصية <tt>.pleine_largeur</tt>',
	'legend' => 'عنوان حقل مجموعة إدخال ',

	// T
	'titre_boites' => 'مربعات',
	'titre_charte' => 'ميثاق',
	'titre_formulaires' => 'استمارات',
	'titre_icones' => 'أيقونات',
	'titre_listes' => 'لوائح',
	'titre_typo' => 'خطأ مطبعي'
);

?>
