<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/charter?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_checkbox' => 'يك گزينه‌ي چندوجهي',
	'label_checkbox_1' => 'يك گزينه اوليه',
	'label_checkbox_2' => 'يك گزينه ثانويه',
	'label_checkbox_long_label' => 'يك گزينه‌ي چندوجهي با برچسب خيلي خيلي بلند كه از كلاس 
<tt>.long_label</tt>
اسفتاده مي‌كند',
	'label_checkbox_long_label_1' => 'يك گزينه‌ي اوليه',
	'label_checkbox_long_label_2' => 'يك گزينه‌ي ثانويه',
	'label_radio' => 'يك گزينه‌ي تك',
	'label_radio_non' => 'نه',
	'label_radio_oui' => 'بله',
	'label_select' => 'يك مدخل منتخب',
	'label_select_non' => 'نه ممنون، اصلاً',
	'label_select_oui' => 'بله، همينطور است',
	'label_text' => 'يك متن ورودي ',
	'label_text_long_label' => ' يك حمله‌ با برچسب بسيار بسيار بلند كه از كلاس
<tt>.long_label</tt>
 استفاده مي‌كند.',
	'label_text_obli' => 'يك مدخل اجباري',
	'label_textarea' => 'يك بلوك مدخل',
	'label_textarea_pleine_largeur' => 'يك بلوك مدخل با 
 <tt>.pleine_largeur</tt>',
	'label_textarea_pleine_largeur_obli' => 'يك بلوك مدخل اجباري با 
<tt>.pleine_largeur</tt>',
	'legend' => 'شرح يك ميدن براي يك گروه از مدخل‌ها',

	// T
	'titre_boites' => 'چارگوش‌ها',
	'titre_charte' => 'منشور',
	'titre_formulaires' => 'فرم‌ها',
	'titre_icones' => 'صورتك‌ها',
	'titre_listes' => 'فهرست‌ها',
	'titre_typo' => 'تايپو'
);

?>
