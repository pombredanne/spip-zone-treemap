<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'affichage' => 'Affichage :',
	'attr_alt' => 'Pas de balise alt ?? ',
	'conteneur' => 'Conteneur :',
	'fichier_introuvable' => 'Fichier introuvable',
	'filtrer' => 'Filtrer :',
	'info_breve' => 'Breve ',
	'info_breves' => 'Breves',
	'info_doc' => 'Cette page r&eacute;capitule la liste de tous vos documents. Pour modifier les informations de chaque document, suivez le lien vers la page de sa rubrique.',
	'info_reparer' => 'Cette page reconstitue les liens entre documents et articles dans lequel ils sont ins&eacute;r&eacute;s par un tag <code><imgxx> <docxx> ou <embxx></code>',
	'info_rubrique' => 'Rubrique ',
	'lien_ajoute' => 'lien ajout&eacute;',
	'mis_jour_liens' => 'Liens mis a jour',
	'mis_jour_tailles' => 'Mettre les tailles a jour',
	'par' => 'Par @numero@',
	'portfolio' => 'Portfolio',
	'reparer_liens' => 'Reparer les liens',
	'info_syndication' => 'Syndication',
	'sans_titre_descriptif' => 'Sans titre ni descriptif',
	'taille_erronee' => 'Taille erron&eacute;e',
	'type' => 'Type :',
	'tous' => 'Tous',
	'tous_doc'  => 'Tous vos documents',
	'tous_docs'  => 'Tous les Documents'
);
?>
