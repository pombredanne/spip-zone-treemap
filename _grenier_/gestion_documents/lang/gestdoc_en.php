<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'affichage' => 'Display :',
	'attr_alt' => 'alt tag lacking ? ',
	'conteneur' => 'Container :',
	'fichier_introuvable' => 'No file found',
	'filtrer' => 'Filter :',
	'info_breve' => 'News item ',
	'info_breves' => 'News items',
	'info_doc' => 'This page presents the list of all your attached documents. To change the information concerning a document, follow the link to the page of its section.',
	'info_reparer' => 'This page reconstitutes the links between documents and the articles in which they are inserted by means of an <code><imgxx>, <docxx> or <embxx></code> tag.',
	'info_rubrique' => 'Section ',
	'lien_ajoute' => 'link added',
	'mis_jour_liens' => 'Links updated',
	'mis_jour_tailles' => 'Correct the filesizes',
	'par' => 'By @numero@',
	'portfolio' => 'Portfolio',
	'reparer_liens' => 'Repare links',
	'info_syndication' => 'Syndication',
	'sans_titre_descriptif' => 'Without title or description',
	'taille_erronee' => 'With wrong filesize',
	'type' => 'Type :',
	'tous' => 'All',
	'tous_doc'  => 'All your documents',
	'tous_docs'  => 'All documents'
);
?>
