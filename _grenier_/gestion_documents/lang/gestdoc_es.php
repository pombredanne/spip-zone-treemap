<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Traduccion _ES Javier SJ: Enero 2007

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'affichage' => 'Visualizaci&oacute;n:',
	'attr_alt' => 'No hay etiqueta alt ?? ',
	'conteneur' => 'Contenedor :',
	'fichier_introuvable' => 'Archivo no encontrado',
	'filtrer' => 'Filtrar :',
	'info_breve' => 'Breve ',
	'info_breves' => 'Breves',
	'info_doc' => 'Esta p&aacute;gina resume la lista de todos tus documentos. Para modificar las informaciones de cada documento, siga el enlace hacia la p&aacute;gina de su secci&oacute;n.',
	'info_reparer' => 'Esta p&aacute;gina reconstruye los enlaces entre documentos y art&oacute;culos en los cuales ha sido insertados mediante una etiqueta <code><imgxx> <docxx> o <embxx></code>',
	'info_rubrique' => 'Secci&oacute;n ',
	'lien_ajoute' => 'enlace a&ntilde;adido',
	'mis_jour_liens' => 'Enlaces actualizados',
	'mis_jour_tailles' => 'Actualizar los tama&ntilde;os',
	'par' => 'En grupos de @numero@',
	'portfolio' => 'Portfolio',
	'reparer_liens' => 'Reparar los enlaces',
	'info_syndication' => 'Sindicaci&oacute;n',
	'sans_titre_descriptif' => 'Sin t&iacute;tulo ni descripcici&oacute;n',
	'taille_erronee' => 'Tama&ntilde;o incorrecto',
	'type' => 'Tipo: ',
	'tous' => 'Todos',
	'tous_doc'  => 'Todos tus documentos',
	'tous_docs'  => 'Todos los Documentos'
);
?>
