<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'affichage' => 'Visualizza:',
	'attr_alt' => 'Attributo alt mancante?? ',
	'conteneur' => 'Contenitore:',
	'fichier_introuvable' => 'File introvabile',
	'filtrer' => 'Filtro:',
	'info_breve' => 'Breve ',
	'info_breves' => 'Brevi',
	'info_doc' => 'Questa pagina mostra una lista di tutti i tuoi documenti. Per modificare le informazioni di un documento, segui il link verso la pagina del suo contenitore (il quadratino in basso).',
	'info_reparer' => 'Questa pagina ricostruisce i link tra i documenti e gli articoli nei quali sono inseriti attraverso un segnaposto <code><imgxx> <docxx> o <embxx></code>',
	'info_rubrique' => 'Rubrica ',
	'lien_ajoute' => 'link aggiunto',
	'mis_jour_liens' => 'Link aggiornati',
	'mis_jour_tailles' => 'Aggiorna le dimensioni',
	'par' => '@numero@ alla volta',
	'portfolio' => 'Lista documenti',
	'reparer_liens' => 'Ripara i link',
	'info_syndication' => 'Siti repertoriati',
	'sans_titre_descriptif' => 'Senza titolo n&egrave; descrizione',
	'taille_erronee' => 'Dimensioni errate',
	'type' => 'Tipo:',
	'tous' => 'Tutti',
	'tous_doc'  => 'Tutti i tuoi documenti',
	'tous_docs'  => 'Tutti i Documenti'
);

?>
