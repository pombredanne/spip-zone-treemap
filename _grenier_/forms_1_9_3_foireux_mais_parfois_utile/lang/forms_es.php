<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_barre_typo' => 'Activar la barra tipogr&aacute;fica',
	'adresse_invalide' => 'Esta direcci&oacute;n de correo no es v&eacute;lida.',
	'afficher' => 'Indicar',
	'aide_contextuelle' => 'Ayuda contextual',
	'ajouter_champ' => 'A&ntilde;adir un campo',
	'ajouter_champ_type' => 'Crear un campo de tipo&nbsp;:',
	'ajouter_choix' => 'A&ntilde;adir una opci&oacute;n',
	'apparence_formulaire' => 'Aspecto del formulario',
	'article_inserer_un_formulaire' => 'Insertar un formulario',
	'article_inserer_un_formulaire_detail' => 'Podr&aacute; insertar formularios en sus art&iacute;cules para permitir que los visitantes introduzcan datos. Escoja un formulario de la lista adjunta y copie el bloque en el texto del art&iacute;culo.',
	'article_recopier_raccourci' => 'Copie el atajo para insertar este formulario en el texto del art&iacute;cule',
	'articles_utilisant' => 'Art&iacute;culos que utilizan este formulario',
	'attention' => 'Atenci&oacute;n:',
	'aucune_reponse' => 'Ninguna respuesta',
	'avis_message_confirmation' => 'Se ha enviado un mensaje de confirmaci&oacute;n a @mail@',

	// B
	'boite_info' => 'Haga clic sobre un formulario para modificarlo o verlo antes de eliminarlo.',

	// C
	'cfg_activer' => 'S&iacute;',
	'cfg_associer_donnees' => 'Asociar los datos de las tablas',
	'cfg_associer_donnees_articles' => 'Permitir asociar datos con los art&iacute;culos:',
	'cfg_associer_donnees_auteurs' => 'Permitir asociar datos con los autores:',
	'cfg_associer_donnees_rubriques' => 'Permitir asociar datos con las secciones:',
	'cfg_bouton_type_image' => 'bot&oacute;n de tipo de imagen',
	'cfg_bouton_type_texte' => 'bot&oacute;n de tipo texto (submit)',
	'cfg_bouton_valider' => 'Tipo de bot&oacute;n de validaci&oacute;n',
	'cfg_bouton_valider_texte' => 'Puedes precisar que tipo de bot&oacute;n de validaci&oacute;n se usa en los formularios:',
	'cfg_desactiver' => 'No',
	'cfg_inserer_head' => 'Inserci&oacute;n en &lt;head&gt;',
	'cfg_inserer_head_texte' => 'Insertar autom&aacute;ticamente la llamada a los siguientes archivos en la secci&oacute;n &lt;head&gt; de las p&aacute;ginas del sitio (recomendado):',
	'champ_descendre' => 'bajar',
	'champ_email_details' => 'Por favor, escriba una direcci&oacute;n de correo v&aacute;lida (como minombre@proveedor.com).',
	'champ_listable_admin' => 'Mostrar este campo en las listas del espacio privado',
	'champ_listable_publique' => 'Mostrar este campo en las listas p&uacute;blicas',
	'champ_monter' => 'subir',
	'champ_necessaire' => 'Debe completar este campo.',
	'champ_nom' => 'Nombre del campo',
	'champ_nom_bloc' => 'Nombre del bloque',
	'champ_nom_groupe' => 'Grupo',
	'champ_nom_texte' => 'Texto',
	'champ_public' => 'Este campo es visible en el espacio p&uacute;blico',
	'champ_saisie_desactivee' => 'Desactivar la edici&oacute;n de este campo',
	'champ_specifiant' => 'Este campo clasifica los datos (orden, filtro, descripci&oacute;n)',
	'champ_table_jointure_type' => 'Tipo de tabla para las uniones',
	'champ_type_date' => 'Fecha',
	'champ_type_email' => 'direcci&oacute;n e-mail',
	'champ_type_fichier' => 'archivo a subir (upload)',
	'champ_type_joint' => 'Uni&oacute;n con otra tabla',
	'champ_type_ligne' => 'l&iacute;nea de texto',
	'champ_type_monnaie' => 'Monetario',
	'champ_type_mot' => 'palabras clave',
	'champ_type_multiple' => 'opci&oacute;n m&uacute;ltiple',
	'champ_type_numerique' => 'Num&eacute;rico',
	'champ_type_password' => 'Contrase&ntilde;a',
	'champ_type_select' => 'opci&oacute;n &uacute;nica',
	'champ_type_separateur' => 'Nuevo bloque de preguntas',
	'champ_type_texte' => 'texto',
	'champ_type_textestatique' => 'Mensaje de explicaci&oacute;n',
	'champ_type_url' => 'direcci&oacute;n del sitio Web',
	'champ_url_details' => 'Por favor, escriba una direcci&oacute;n Web v&aacute;lida (como http://www.misitio.com/...).',
	'champs_formulaire' => 'Campos del formulario ',
	'changer_choix_multiple' => 'Cambiar a elecci&oacute;n m&uacute;ltiple',
	'changer_choix_unique' => 'Cambiar a elecci&oacute;n &uacute;nica',
	'choisir_email' => 'Elegir el email en funci&oacute;n de',
	'confirm_supprimer_champ' => '&iquest;Est&aacute;s seguro de que quieres suprimir el campo \'@champ@\'?',
	'confirm_supprimer_donnee' => '&iquest;Est&aacute;s seguro de que quieres suprimir el dato \'@donnee@\'?',
	'confirm_vider_table' => '&iquest;Est&aacute;s seguro de que quieres vaciar el contenido de la tabla \'@table@\'?',
	'confirmer_champ_password' => 'Introduce dos veces la palabra',
	'confirmer_password' => 'Confirmaci&oacute;n',
	'confirmer_reponse' => 'Confirmar la respuesta por mail con:',
	'csv_classique' => 'CSV cl&aacute;sico (,)',
	'csv_excel' => 'CSV para Excel (;)',
	'csv_tab' => 'CSV con tabulaciones',

	// D
	'date' => 'Fecha',
	'date_invalide' => 'Formato de la fecha inv&aacute;lido',
	'donnees_modifiable' => 'Datos modificables por el usuario',
	'donnees_multiple' => 'Respuestas m&uacute;ltiples.',
	'donnees_nonmodifiable' => 'Datos no modificables por el usuario.',
	'donnees_nonmultiple' => 'Respuesta &uacute;nica.',
	'donnees_prot' => 'Datos protegidos. Los datos recogidos no ser&aacute;n accesibles por los visitantes del sitio',
	'donnees_pub' => 'Datos p&uacute;blicos. Los datos recogidos ser&aacute;n accesibles por los visitantes del sitio.',
	'dupliquer' => 'Duplicar',

	// E
	'echec_upload' => 'Ha fallado la transferencia del archivo.',
	'edit_champ_obligatoire' => 'es obligatorio rellenar este campo',
	'editer' => 'Editar',
	'email_independant' => 'Email independiente de la respuesta',
	'exporter' => 'Exportar',
	'exporter_article' => 'Exportar a un art&iacute;culo',

	// F
	'fichier_trop_gros' => 'Este archivo es demasiado grande.',
	'fichier_type_interdit' => 'Este tipo de archivo est&aacute; prohibido.',
	'form_erreur' => 'Error&nbsp;:',
	'format_fichier' => 'Formato del fichero&nbsp;:',
	'format_liste' => 'lista desplegable',
	'format_liste_ou_radio' => 'Formato de la lista',
	'format_radio' => 'botones tipo radio',
	'forms_obligatoires' => 'Formularios obligatorios para la introducci�n de &eacute;ste :',
	'formulaire' => 'Formulario',
	'formulaire_aller' => 'Ir al formulario ',
	'formulaires_copie' => 'Copia de @nom@',
	'formulaires_sondages' => 'Formularios y encuestas',

	// H
	'html_wrapper' => 'Encapsular los campos dentro del c&oacute;digo html',

	// I
	'icone_ajouter_donnees' => 'An&tilde;dir respuestas',
	'icone_creer_formulaire' => 'Crear un nuevo formulario',
	'icone_creer_table' => 'Crear una nueva tabla',
	'importer_form' => 'Importar un formulario',
	'info_apparence' => 'He aqu&iacute; una previsualizaci&oacute;n del formulario tal como aparecer&aacute; a los visitantes del sitio p&uacute;blico.',
	'info_articles_lies_donnee' => 'Los art&iacute;iculos le&iacute;dos',
	'info_champs_formulaire' => 'Aqu&iacute; se crean y modifican los campos que luego podr&aacute;n rellenar los visitantes.',
	'info_obligatoire_02' => '[Obligatorio]',
	'info_rubriques_liees_donnee' => 'Las secciones enlazadas',
	'info_sondage' => 'Si su formulario  es una encuesta, les resultados de los campos de tipo &laquo; selecci&oacute;n &raquo; ser&aacute;n totalizados y mostrados.',
	'info_supprimer_formulaire' => '�Realmente desea eliminar este formulario?',
	'info_supprimer_formulaire_reponses' => 'Hay respuestas a este formulario. �Desea realmente eliminarlo?',

	// L
	'lien_apercu' => 'Resumen',
	'lien_champ' => 'Campos',
	'lien_propriete' => 'Propiedades',
	'lien_retirer_donnee_liante' => 'Quitar el link',
	'lien_retirer_donnee_liee' => 'Quitar el link',
	'lier_articles' => 'Permitir asociar las respuestas a un art&iacute;culo',
	'lier_documents' => 'Permetir asociar documentos a las respuestas',
	'lier_documents_mail' => 'Adjuntar los documentos al email',
	'liste_choix' => 'Lista de las opciones propuestas',

	// M
	'moderation_donnees' => 'Validar los antes de su publicaci&oacute;n :',
	'modifiable_donnees' => 'Datos modificables desde el espacio p&uacute;blico&nbsp;:',
	'monetaire_invalide' => 'Campo monetario no v&aacute;lido',
	'monnaie_euro' => 'Euro (&euro;)',
	'multiple_donnees' => 'Introducci&oacute;n de los datos en el espacio p&uacute;blico&nbsp;:',

	// N
	'nb_decimales' => 'N&uacute;mero de decimales',
	'nombre_reponses' => '@nombre@ respuestas',
	'nouveau_champ' => 'Nuevo campo',
	'nouveau_choix' => 'Nueva opci&oacute;n',
	'nouveau_formulaire' => 'Nuevo formulario ',
	'numerique_invalide' => 'Campo num&eacute;rico no v&aacute;lido',

	// P
	'page' => 'P&aacute;gina',
	'pas_mail_confirmation' => 'No hay mail de confirmaci&oacute;n',
	'probleme_technique' => 'Problema t&eacute;cnico. Su respuesta no ha podido ser tomada en cuenta.',
	'probleme_technique_upload' => 'Problema t&eacute;cnico. Ha fallado la transferencia del archivo.',
	'publication_donnees' => 'Publicaci&oacute;n de los datos',

	// R
	'rang' => 'Fila',
	'remplir_un_champ' => 'Por favor, complete al menos un campo.',
	'reponse' => 'Respuesta @id_reponse@',
	'reponse_depuis' => 'Desde la p&aacute;gina ',
	'reponse_enregistree' => 'Su respuesta ha sido registrada.',
	'reponse_envoyee' => 'Respuesta enviada el ',
	'reponse_envoyee_a' => 'a',
	'reponse_retrovez' => 'Encuentre esta respuesta en el espacio de administraci&oacute;n&nbsp;:',
	'reponses' => 'respuestas',
	'resultats' => 'Resultados: ',

	// S
	'site_introuvable' => 'No se ha encontrado este sitio.',
	'sondage_deja_repondu' => 'Ud. ya ha participado de esta encuesta',
	'sondage_non' => ' Este formulario  no es una encuesta',
	'sondage_oui' => 'Este formulario es una encuesta',
	'suivi_formulaire' => 'Seguimiento del formulario ',
	'suivi_formulaires' => 'Seguimiento de los formularios',
	'suivi_reponses' => 'Seguimiento de las respuestas',
	'supprimer' => 'Suprimir',
	'supprimer_champ' => 'Suprimir este campo',
	'supprimer_choix' => 'suprimir esta opci&oacute;n',
	'supprimer_formulaire' => 'Suprimir este formulario ',
	'supprimer_reponse' => 'Suprimir esta respuesta',

	// T
	'tables' => 'Tablas',
	'taille_max' => 'Tama&ntilde;o m&aacute;ximo (en kb)',
	'telecharger' => 'Descargar',
	'telecharger_reponses' => ' Descargar las respuestas',
	'titre_formulaire' => 'T&iacute;tulo del formulario ',
	'total_votes' => 'Total de votos',
	'tous_formulaires' => 'Todos los formularios',
	'tous_sondages' => 'Todas las encuestas',
	'tous_sondages_proteges' => 'Todas las encuestas protegidas',
	'tous_sondages_public' => 'Todas las encuestas p&uacute;blicas',
	'toutes_tables' => 'Todas las tablas',
	'type_form' => 'Tipo de formulario',

	// U
	'une_reponse' => 'una respuesta',
	'unite_monetaire' => 'Unidad monetaria',

	// V
	'valider' => 'Aceptar',
	'verif_web' => 'comprobar la existencia del sitio Web',
	'vider' => 'Vaciar',
	'voir_article' => 'Ver el art&iacute;culo',
	'voir_resultats' => 'Ver los resultados'
);

?>
