<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/_stable_/cfg/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// E
	'extraarticles' => 'Exemple de champ extra sur un article',

	// T
	'testauteur' => 'Exemple sur champ extra/cfg de la table auteur',
	'testcasier' => 'Exemple de casier',
	'testclassic' => 'Exemple m&eacute;ta classique',
	'testinclure' => 'Exemple d\'inclusion de squelette',
	'testmulti' => 'Exemple entr&eacute;es multiples',
	'testphp' => 'Exemple de stockage php',
	'testsimple' => 'Exemple simple',
	'testtable' => 'Exemple sur champs d\'une table sql'
);

?>
