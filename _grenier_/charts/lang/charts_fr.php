<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'apparence_graphique' => "Apparence du Graphique",
'article_inserer_un_chart'=> "Ins&eacute;rer un graphique",
'article_inserer_un_chart_detail' => "Vous pouvez ins&eacute;rer des graphiques dans vos articles. Choisissez un graphique dans la liste ci-dessous et recopiez le raccourci dans le texte de l'article.",
'articles_lies' => "Articles utilisant ce graphique",
'article_recopier_raccourci' => "Recopiez ce raccourci dans le texte de l'article pour ins&eacute;rer ce graphique",

'code_du_graphique' => 'Code XML du graphique',
'confirmer_suppression' => "Voulez-vous vraiment supprimer ce graphique ?",
'copie_de' => 'Copie de @titre@',
'couleur_fond' => "Couleur de fond",

'dupliquer' => "Dupliquer",
'graphiques' => 'Graphiques',

'hauteur' => "Hauteur",

'icone_creer_chart' => "Cr&eacute;er un nouveau graphique",
'info_obligatoire_02' => '[Obligatoire]',

'largeur' => "Largeur",

'nouveau_graphique' => 'Nouveau Graphique',
'previsu_info' => "Voici une pr&eacute;visualisation du graphique tel qu'il appara&icirc;tra aux visiteurs du site public.",

'supprimer' => 'Supprimer ce graphique',
'titre_graphique' => "Titre du graphique",
'tous_graphiques' => "Tous les Graphiques",
'tous_graphiques_desc' => "Cliquez sur un graphique pour le modifier ou le visualiser avant suppression."
);


?>