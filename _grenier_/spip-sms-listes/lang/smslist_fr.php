<?php
/*
 * Spip SMS Liste
 * Gestion de liste de diffusion de SMS
 *
 * Auteur :
 * Cedric Morin
 * (c) 2007 - Distribue sous licence GNU/GPL
 *
 */

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'abo_listes' => 'Choisir un th&egrave;me',
'derniers_envois' => 'Envois R&eacute;cents',
'envois_programmes' => 'Envois programm&eacute;s',
'envois_en_cours' => 'Envois en cours',
'envois_interrompus' => 'Envois interrompus',
'envois_restants' => 'Envois restants',
'icone_gerer_messages' => "G&eacute;rer les SMS",
'icone_gerer_abonnes' => "G&eacute;rer les Abonn&eacute;s",
'icone_gerer_listes' => "G&eacute;rer les listes de diffusion",
'icone_boite_d_envoi' => "Bo&icirc;te d'Envoi",
'icone_configurer' => "Configurer",

'messages_en_redaction' => 'Messages en cours de r&eacute;daction',
'spip_sms_liste' => 'Spip-SMS-listes',
'reprendre_envoi' => 'Reprendre cet envoi',
'stopper_envoi' => 'Stopper cet envoi',

'abonnement_enregistre' => 'Votre abonement a bien &eacute;t&eacute; pris en compte.',
'abonnement_supprime' => 'Votre abonnement a &eacute;t&eacute; arr&ecirc;t&eacute;',
'abonnement_modifie' => 'Votre abonnement a &eacute;t&eacute; mis &agrave; jour',
'desabonnement' => 'Arr&ecirc;ter votre abonement',

);


?>
