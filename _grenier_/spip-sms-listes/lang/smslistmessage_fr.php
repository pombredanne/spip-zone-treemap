<?php
/*
 * Spip SMS Liste
 * Gestion de liste de diffusion de SMS
 *
 * Auteur :
 * Cedric Morin
 * (c) 2007 - Distribue sous licence GNU/GPL
 *
 */

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// page tables_tous
'icone_creer_table' => "Cr&eacute;er une nouvelle table de SMS",
'type_des_tables' => 'SMS',
'toutes_tables' => "Tous les SMS",

// page donnees_tous
'icone_ajouter_donnees' => "Cr&eacute;er des SMS",
'telecharger_reponses' => "Exporter les SMS",
'importer_donnees_csv' => "Importer des SMS",

// page donnees_edit
'lien_retirer_donnee_liee' => 'Retirer ce SMS',
'lien_retirer_donnee_liante' => 'Retirer ce SMS',
'articles_utilisant'=> "Articles li&eacute; a ce SMS",
'suivi_reponses' => "Voir les SMS",
'aucune_reponse' => "Aucun SMS",
'une_reponse' => "Un SMS",
'nombre_reponses' => "@nombre@ sms",
'texte_donnee_statut' => "Statut de ce SMS",
'texte_statut_prepa' => "En cours de r&eacute;daction",
'texte_statut_prop' => " En cours d'envoi",
'texte_statut_publie' => " Envoy&eacute;",
'texte_statut_poubelle' => "Supprim&eacute;",
'texte_statut_refuse' => " Envoi refus&eacute;",

// page forms_edit
'champs_formulaire'=>"Champs de la table de SMS",
'info_supprimer_formulaire' => "Voulez-vous vraiment supprimer cette table de SMS ?",
'info_supprimer_formulaire_reponses' => "Cette table contient des SMS. Voulez-vous vraiment la supprimer ?",

'formulaire' => "Table des SMS",
'nouveau_formulaire' => "Nouvelle table de SMS",
'supprimer_formulaire' => "Supprimer cette table de SMS",
'titre_formulaire' => "Titre de la table de SMS",


);


?>
