<?php
/*
 * Spip SMS Liste
 * Gestion de liste de diffusion de SMS
 *
 * Auteur :
 * Cedric Morin
 * (c) 2007 - Distribue sous licence GNU/GPL
 *
 */

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// page tables_tous
'icone_creer_table' => "Cr&eacute;er une nouvelle liste de diffusion SMS",
'type_des_tables' => 'Listes de diffusion SMS',
'toutes_tables' => "Toutes les listes de diffusion SMS",

// page donnees_tous
'icone_ajouter_donnees' => "Cr&eacute;er des listes de diffusion",
'telecharger_reponses' => "Exporter les listes de diffusion",
'importer_donnees_csv' => "Importer des listes de diffusion",

// page donnees_edit
'lien_retirer_donnee_liee' => 'Retirer cette liste de diffusion',
'lien_retirer_donnee_liante' => 'Enlever de cette liste de diffusion',
'articles_utilisant'=> "Articles utilisant cette liste",
'suivi_reponses' => "Voir les listes de diffusion",
'aucune_reponse' => "Aucune liste de diffusion",
'une_reponse' => "Une liste de diffusion",
'nombre_reponses' => "@nombre@ listes de diffusion",
'texte_donnee_statut' => "Statut de cette liste",
'texte_statut_prepa' => "???",
'texte_statut_prop' => "Suspendue",
'texte_statut_publie' => "Active",
'texte_statut_poubelle' => "Supprim&eacute;",
'texte_statut_refuse' => "???",

// page forms_edit
'champs_formulaire'=>"Champs du format de la liste",
'info_supprimer_formulaire' => "Voulez-vous vraiment supprimer ce format de liste ?",
'info_supprimer_formulaire_reponses' => "Des listes sont associ&eacute;es &agrave; ce format de liste. Voulez-vous vraiment le supprimer ?",

'formulaire' => "Format de liste de diffusion",
'nouveau_formulaire' => "Nouveau format de liste",
'supprimer_formulaire' => "Supprimer ce format de liste",
'titre_formulaire' => "Titre du format de la liste",

);


?>
