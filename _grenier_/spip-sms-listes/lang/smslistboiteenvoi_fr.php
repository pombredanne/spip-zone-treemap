<?php
/*
 * Spip SMS Liste
 * Gestion de liste de diffusion de SMS
 *
 * Auteur :
 * Cedric Morin
 * (c) 2007 - Distribue sous licence GNU/GPL
 *
 */

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// page tables_tous
'icone_creer_table' => "Cr&eacute;er une nouvelle bo&icirc;te d'envoi",
'type_des_tables' => 'Bo&icirc;te d\'envoi',
'toutes_tables' => "Toutes les bo&icirc;tes d'envoi",

// page donnees_tous
'icone_ajouter_donnees' => "Programmer un envoi",
'telecharger_reponses' => "Exporter la liste des envois en instance",
'importer_donnees_csv' => "Importer une liste d'envois",

// page donnees_edit
'lien_retirer_donnee_liee' => 'Retirer cet envoi',
'lien_retirer_donnee_liante' => 'Retirer de cet envoi',
'articles_utilisant'=> "Articles li&eacute; a cet envoi",
'suivi_reponses' => "Voir les envois programm&eacute;s",
'aucune_reponse' => "Aucun Envoi en instance",
'une_reponse' => "Un Envoi en instance",
'nombre_reponses' => "@nombre@ envois en instance",
'texte_donnee_statut' => "Statut de cet Envoi",
'texte_statut_prepa' => "En attente",
'texte_statut_prop' => "En cours d'envoi",
'texte_statut_publie' => "Fini",
'texte_statut_poubelle' => "Supprim&eacute;",
'texte_statut_refuse' => "Envoi stopp&eacute;",

// page forms_edit
'champs_formulaire'=>"Champs de la bo&icirc;te d'envoi",
'info_supprimer_formulaire' => "Voulez-vous vraiment supprimer cette bo&icirc;te d'envoi ?",
'info_supprimer_formulaire_reponses' => "Cette bo&icirc;te contient des envois. Voulez-vous vraiment la supprimer ?",

'formulaire' => "Bo&icirc;te d'envoi",
'nouveau_formulaire' => "Nouvelle Bo&icirc;te d'envoi",
'supprimer_formulaire' => "Supprimer cette Bo&icirc;te d'envoi",
'titre_formulaire' => "Titre de la Bo&icirc;te d'envoi",


);


?>
