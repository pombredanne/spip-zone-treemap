<?php
/*
 * Spip SMS Liste
 * Gestion de liste de diffusion de SMS
 *
 * Auteur :
 * Cedric Morin
 * (c) 2007 - Distribue sous licence GNU/GPL
 *
 */

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// page tables_tous
'icone_creer_table' => "Cr&eacute;er une nouvelle liste de Comptes SMS",
'type_des_tables' => 'Comptes SMS',
'toutes_tables' => "Tous les tables de Comptes SMS",

// page donnees_tous
'icone_ajouter_donnees' => "Cr&eacute;er des Comptes SMS",
'telecharger_reponses' => "Exporter les Comptes SMS",
'importer_donnees_csv' => "Importer des Comptes SMS",

// page donnees_edit
'lien_retirer_donnee_liee' => 'Retirer ce compte SMS',
'lien_retirer_donnee_liante' => 'Retirer ce compte SMS',
'articles_utilisant'=> "Articles li&eacute; a ce compte SMS",
'suivi_reponses' => "Voir les comptes SMS",
'aucune_reponse' => "Aucun compte SMS",
'une_reponse' => "Un compte SMS",
'nombre_reponses' => "@nombre@ compte SMS",
'texte_donnee_statut' => "Statut de ce compte SMS",
'texte_statut_prepa' => "En cours de configuration",
'texte_statut_prop' => "En attente",
'texte_statut_publie' => "Configur&eacute;",
'texte_statut_poubelle' => "Supprim&eacute;",
'texte_statut_refuse' => "Compte annul&eacute;",

// page forms_edit
'champs_formulaire'=>"Champs de la table de Comptes SMS",
'info_supprimer_formulaire' => "Voulez-vous vraiment supprimer cette liste de Comptes SMS ?",
'info_supprimer_formulaire_reponses' => "Cette table contient des Comptes SMS. Voulez-vous vraiment la supprimer ?",

'formulaire' => "Liste des Comptes SMS",
'nouveau_formulaire' => "Nouvelle liste de Comptes SMS",
'supprimer_formulaire' => "Supprimer cette liste de Comptes SMS",
'titre_formulaire' => "Titre de la liste de Comptes SMS",


);


?>
