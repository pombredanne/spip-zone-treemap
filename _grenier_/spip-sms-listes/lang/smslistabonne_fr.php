<?php
/*
 * Spip SMS Liste
 * Gestion de liste de diffusion de SMS
 *
 * Auteur :
 * Cedric Morin
 * (c) 2007 - Distribue sous licence GNU/GPL
 *
 */

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// page tables_tous
'icone_creer_table' => "Cr&eacute;er une nouvelle table d'abonn&eacute; SMS",
'type_des_tables' => 'Abonn&eacute;s SMS',
'toutes_tables' => "Tous les abonn&eacute;s SMS",

// page donnees_tous
'icone_ajouter_donnees' => "Cr&eacute;er des abonn&eacute;s",
'telecharger_reponses' => "Exporter les abonn&eacute;s",
'importer_donnees_csv' => "Importer des abonn&eacute;s",

// page donnees_edit
'lien_retirer_donnee_liee' => 'Desabonner',
'lien_retirer_donnee_liante' => 'Retirer cet abonn&eacute;',
'articles_utilisant'=> "Articles li&eacute; a cet abonn&eacute;",
'suivi_reponses' => "Voir les abonn&eacute;s",
'aucune_reponse' => "Aucun abonn&eacute;",
'une_reponse' => "Un abonn&eacute;",
'nombre_reponses' => "@nombre@ abonn&eacute;s",
'texte_donnee_statut' => "Statut de cet abonn&eacute;",
'texte_statut_prepa' => "???",
'texte_statut_prop' => "D&eacute;sabonn&eacute;",
'texte_statut_publie' => "Abonn&eacute;",
'texte_statut_poubelle' => "Supprim&eacute;",
'texte_statut_refuse' => "Banni",

// page forms_edit
'champs_formulaire'=>"Champs de la table d'abonn&eacute;",
'info_supprimer_formulaire' => "Voulez-vous vraiment supprimer cette table d'abonn&eacute;s ?",
'info_supprimer_formulaire_reponses' => "Cette table contient des abonn&eacute;s. Voulez-vous vraiment la supprimer ?",

'formulaire' => "Table des abonn&eacute;",
'nouveau_formulaire' => "Nouvelle table d'abonn&eacute;s",
'supprimer_formulaire' => "Supprimer cette table d'abonn&eacute;s",
'titre_formulaire' => "Titre de la table d'abonn&eacute;",



);


?>
