<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(

// B
'bouton_maj_methode' => 'M&eacute;thodes de mise &agrave; jour',
'bouton_maj_source' => 'Sources de mise &agrave; jour',

// I
'info_gauche_mise_a_jour1' => 'Cette page est uniquement accessible aux responsables du site.',
'info_gauche_mise_a_jour2' => 'Elle donne acc&egrave;s aux diff&eacute;rentes fonctions de mise &agrave; jour du site.',

// M
'methodes_pas_ok' => 'Aucune methode exploitable n\'est disponible sur ce serveur. Ce plugin est inutilisable.',
'mise_a_jour' => 'Mise &agrave; jour',

// S
'svn_ok' => 'Commande SVN trouv&eacute;e.',
'svn_pas_ok' => 'Commande SVN non trouv&eacute;e ou pas autoris&eacute;e.',
'spip_loader_pas_ok' => 'Le script <tt>spip_loader.php</tt> n\'a pas &eacute;t&eacute; trouv&eacute; &agrave; la racine de ce site.'

);

?>