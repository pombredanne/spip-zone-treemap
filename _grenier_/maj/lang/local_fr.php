<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(

'texte_admin_tech_01' => 'Cette option vous permet de sauvegarder le contenu de la base dans un fichier qui sera stock&eacute; dans le r&eacute;pertoire @dossier@. N\'oubliez pas &eacute;galement de <a href="../spip.php?action=mes_fichiers">r&eacute;cup&eacute;rer l\'int&eacute;gralit&eacute;</a> du r&eacute;pertoire @img@, qui contient les images et les documents utilis&eacute;s dans les articles et les rubriques.',

'texte_restaurer_sauvegarde' => 'Cette option vous permet de restaurer une sauvegarde pr&eacute;c&eacute;demment
		effectu&eacute;e de la base. A cet effet, le fichier contenant la sauvegarde doit avoir &eacute;t&eacute;
		plac&eacute; dans le r&eacute;pertoire @dossier@.
		Soyez prudent avec cette fonctionnalit&eacute;&nbsp;: <b>les modifications, pertes &eacute;ventuelles, sont
		irr&eacute;versibles.</b><br />
		Si vous utilisez le script spip_loader.php pour mettre &agrave; jour votre site, vous pouvez restaurer un zip de vos fichiers personnels en cliquant sur <a href="../spip.php?action=decompresser_mes_fichiers">ce lien</a>.',

);

?>