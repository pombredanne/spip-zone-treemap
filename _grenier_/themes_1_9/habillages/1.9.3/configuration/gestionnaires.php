<?php

// Fonction qui definie les differents gestionnaires d'habillages. 
function gestionnaires_habillages() {
$hab_gestionnaires = array(public => array(squelettes, themes), prive => array(icones, couleurs, styles), divers  => array(config, aide));
return $hab_gestionnaires;
}

?>