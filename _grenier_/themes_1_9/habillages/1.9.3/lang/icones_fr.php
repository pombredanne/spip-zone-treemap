<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP nomm� admin_lang genere le NOW()
// langue / language = fr

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajout_couleur' => 'Ajouter couleurs d\'interface ',


// D
'def_couleurs' => 'Les jeux de couleurs s&eacute;lectionn&eacute;s seront disponibles dans 
					le bandeau secondaire de SPIP.
					Les couleurs cr&eacute;&eacute;es par vos soins dans le fichier : 
					mes_couleurs.php
					s\'affichent &agrave; la suite des jeux ci-dessus. ',
'def_page_selecteur' => 'Cette page recense tous les jeux d\'ic&ocirc;nes disponibles par ce plugin.
					Le bouton [Voir] &agrave; la suite du nom, 
					donne acc&egrave;s &agrave; l\'affichage de toutes les ic&ocirc;nes du \'pack\' !
					Le changement de jeu d\'ic&ocirc;nes est imm&eacute;diat ! ',


// G
'gros_titre_selecteur' => 'S&eacute;lecteur d\'ic&ocirc;nes Espace Priv&eacute; ',
'gros_titre_toutes_icones' => 'Ic&ocirc;nes du pack ',


// I
'icones_interface' => 'Ic&ocirc;nes interface ',


// M
'mes_couleurs' => 'mes_couleurs ',


// N
'nb_fichiers' => '@n@ fichiers ',


// P
'pack_actif' => 'Pack actif : @pack@ ',


// R
'retour_selecteur' => 'Retour S&eacute;lecteur ic&ocirc;nes ',


// S
'selecteur_icones' => 'selecteur icones ',
'signature' => 'ICOP 1.0&lt;br /&gt;Scoty, &lt;a href=\'http://www.koakidi.com\'&gt;koakidi.com&lt;/a&gt;&lt;br /&gt;(03/2007) ',


// T
'title_plugin_icop' => 'Plugin Ic&ocirc;nes Espace Priv&eacute; ',
'tout_repertoire' => 'Tout le R&eacute;pertoire ',
'toutes_icones' => 'Ic&ocirc;nes du pack ',


// V
'voir_toutes_icones' => 'Voir toutes les ic&ocirc;nes ',


// Z
'z' => 'z '


);

?>