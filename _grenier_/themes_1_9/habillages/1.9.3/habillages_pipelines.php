<?php
$p=explode(basename(_DIR_PLUGINS)."/",str_replace('\\','/',realpath(dirname(__FILE__))));
define('_DIR_PLUGIN_HABILLAGES',(_DIR_PLUGINS.end($p)));

function habillages_ajouter_boutons($boutons_admin) {
		// si on est admin
		if ($GLOBALS['connect_statut'] == "0minirezo" && $GLOBALS["connect_toutes_rubriques"]) {
		  if (_request('exec')=='habillages_accueil'){
			  $boutons_admin['configuration']->sousmenu['habillages_accueil']= new Bouton(
			"../"._DIR_PLUGIN_HABILLAGES."/../img_pack/habillages_icone-22.png",  // icone
			_L('Habillages')	// titre
			);
	  		}
	  		else {
		  $boutons_admin['configuration']->sousmenu['habillages_accueil']= new Bouton(
			"../"._DIR_PLUGIN_HABILLAGES."/../img_pack/habillages_icone-22.png",  // icone
			_L('Habillages')	// titre
			);
		}
		}
		return $boutons_admin;
	}

function habillages_ajouter_onglets($flux) {
	include_spip('configuration/gestionnaires');		
	

	if (_request('exec')=='habillages_prive' || _request('exec')=='habillages_accueil' || _request('exec')=='habillages_squelettes' || _request('exec')=='habillages_extras' || _request('exec')=='habillages_logos' || _request('exec')=='habillages_icones' || _request('exec')=='habillages_aide' || _request('exec')=='habillages_themes' || _request('exec')=='habillages_config') {
		$flux['data']['accueil']= new Bouton(
		_DIR_PLUGIN_HABILLAGES.'img_pack/habillages_accueil-22.png', 'Accueil', generer_url_ecrire("habillages_accueil"));
	
		foreach (gestionnaires_habillages() as $gestionnaire=>$sous_gestionnaires) {
			foreach ($sous_gestionnaires as $sous_gestionnaire) {
			if ($GLOBALS['meta']['habillages_'.$sous_gestionnaire.'_on'] == "oui") {
				$flux['data'][$gestionnaire]= new Bouton(
				_DIR_PLUGIN_HABILLAGES.'img_pack/habillages_'.$gestionnaire.'-22.png', $gestionnaire, generer_url_ecrire("habillages_".$gestionnaire.""));
			}
		  }
	   }
	
	$flux['data']['aide']= new Bouton(
	_DIR_PLUGIN_HABILLAGES.'img_pack/habillages_aide-22.png', 'Aide', generer_url_ecrire("habillages_aide")); 
}
return $flux;
}

function habillages_header_prive($flux) {
	if (_request('exec')=='habillages_accueil' || _request('exec')=='habillages_squelettes' || _request('exec')=='habillages_themes' || _request('exec')=='habillages_logos' || _request('exec')=='habillages_icones' || _request('exec')=='habillages_extras' || _request('exec')=='habillages_aide' || _request('exec')=='habillages_config') {
		$flux .= '<link rel="stylesheet" href="'._DIR_PLUGIN_HABILLAGES.'img_pack/habillages_habillages.css" type="text/css" >'."\n";
		}
		
	global $exec;
	include_spip('inc/meta');
	lire_metas();
	$theme_link = $GLOBALS['meta']['habillages_icones'];
	
	if (isset($GLOBALS['meta']['habillages_icones']) AND ($c=$GLOBALS['meta']['habillages_icones'])!="") {
  	$flux .= '<link rel="stylesheet" href="'.$theme_link.'style.css" type="text/css" />'."\n";
	}

	return $flux;
}

//function habillages_body_prive($texte) {
//		$texte = str_replace('dist', 'Oups', $texte);
//		return $texte;
//}
	
?>