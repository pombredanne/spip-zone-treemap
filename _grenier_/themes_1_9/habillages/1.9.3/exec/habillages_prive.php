<?php
$p=explode(basename(_DIR_PLUGINS)."/",str_replace('\\','/',realpath(dirname(__FILE__))));
define('_DIR_PLUGIN_HABILLAGES',(_DIR_PLUGINS.end($p)));

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/config');
include_spip('inc/plugin');
include_spip('inc/presentation');
include_spip('inc/layer');
include_spip('inc/actions');
include_spip('configuration/gestionnaires');

function exec_habillages_prive() {
	global $connect_statut;
	global $connect_toutes_rubriques;
	global $spip_lang_right;

		if ($connect_statut != '0minirezo' OR !$connect_toutes_rubriques) {
		debut_page(_T('icone_admin_plugin'), "configuration", "plugin");
		echo _T('avis_non_acces_page');
		fin_page();
		exit;
		}
		
		if (_request('gestionnaire')=='icones'){
			# changer le pack icones
			if (($cp=_request('change_pack'))!==NULL ) {
				ecrire_meta('habillages_img_pack',$cp);
				ecrire_metas();
			}
		}
		
		if (_request('gestionnaire')=='styles' AND _request('chemin_style')!==NULL){
				ecrire_meta('habillages_styles',_request('chemin_style'));
				ecrire_metas();
		}
		
	if (isset($_GET['surligne']))
		$surligne = $_GET['surligne'];
	global $couleur_claire;
	debut_page(_T('commun:titre_plug'), "configuration", "habillages");
	echo "<style type='text/css'>\n";
	echo <<<EOF
div.cadre-padding ul li {
	list-style:none ;
}
div.cadre-padding ul {
	padding-left:1em;
	margin:.5em 0 .5em 0;
}
div.cadre-padding ul ul {
	border-left:5px solid #DFDFDF;
}
div.cadre-padding ul li li {
	margin:0;
	padding:0 0 0.25em 0;
}
div.cadre-padding ul li li div.nomplugin, div.cadre-padding ul li li div.nomplugin_on {
	border:1px solid #AFAFAF;
	padding:.3em .3em .6em .3em;
	font-weight:normal;
}
div.cadre-padding ul li li div.nomplugin a, div.cadre-padding ul li li div.nomplugin_on a {
	outline:0;
	outline:0 !important;
	-moz-outline:0 !important;
}
div.cadre-padding ul li li div.nomplugin_on {
	background:$couleur_claire;
	font-weight:bold;
}
div.cadre-padding div.droite label {
	padding:.3em;
	background:#EFEFEF;
	border:1px dotted #95989F !important;
	border:1px solid #95989F;
	cursor:pointer;
	margin:.2em;
	display:block;
	width:10.1em;
}
div.cadre-padding input {
	cursor:pointer;
}
div.detailplugin {
	border-top:1px solid #B5BECF;
	padding:.6em;
	background:#F5F5F5;
}
div.detailplugin hr {
	border-top:1px solid #67707F;
	border-bottom:0;
	border-left:0;
	border-right:0;
	}
EOF;
	echo "</style>";
	
	echo '<img src="' . _DIR_PLUGIN_HABILLAGES. '/../img_pack/habillages_icone-48.png">';
	gros_titre(_T('commun:titre_plug'));

	echo barre_onglets("habillages", 'prive');
	
	debut_gauche();
	debut_boite_info();
	foreach (gestionnaires_habillages() as $gestionnaire=>$sous_gestionnaires) {
		if ($gestionnaire == "prive") {
		foreach ($sous_gestionnaires as $sous_gestionnaire) {
			if ($GLOBALS['meta']['habillages_'.$sous_gestionnaire.'_on']=="oui") {
			echo '<form exec="'.generer_url_ecrire('habillages_prive').'" method="post">';
			echo "<input type='submit' name='gestionnaire' value='".$sous_gestionnaire."' class='fondo'>";
			echo "</form>";
			}
		}
	  }
	}
	fin_boite_info();

	debut_droite();
	
	debut_boite_info();
		if (_request('gestionnaire')==NULL){
			echo "coucou";
		}
		if (_request('gestionnaire')=='icones'){
			include_spip('inc/habillages_icones');
			habillages_icones();
		}
		if (_request('gestionnaire')=='couleurs'){
			include_spip('inc/habillages_couleurs');
			habillages_couleurs();
		}
		if (_request('gestionnaire')=='styles'){
			include_spip('inc/habillages_styles');
			habillages_styles();
		}
	fin_boite_info();
	

	echo "\n<input type='hidden' name='id_auteur' value='$connect_id_auteur' />";
	echo "\n<input type='hidden' name='hash' value='" . calculer_action_auteur("valide_plugin") . "'>";
	echo "\n<input type='hidden' name='changer_gestion' value='oui'>";

	echo "\n<p>";

	echo "<div style='text-align:$spip_lang_right'>";
	echo "<input type='submit' name='Valider' value='"._T('bouton_valider')."' class='fondo'>";
	echo "</div>";
	echo "</form>";
		
	echo fin_gauche(), fin_page();

}

?>
