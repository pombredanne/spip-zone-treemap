<?php
$p=explode(basename(_DIR_PLUGINS)."/",str_replace('\\','/',realpath(dirname(__FILE__))));
define('_DIR_PLUGIN_HABILLAGES',(_DIR_PLUGINS.end($p)));

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/config');
include_spip('inc/plugin');
include_spip('inc/presentation');
include_spip('inc/layer');
include_spip('inc/actions');
include_spip('configuration/gestionnaires');

function exec_habillages_accueil() {
	global $connect_statut;
	global $connect_toutes_rubriques;
	global $spip_lang_right;
	
		if ($connect_statut != '0minirezo' OR !$connect_toutes_rubriques) {
		debut_page(_T('icone_admin_plugin'), "configuration", "plugin");
		echo _T('avis_non_acces_page');
		fin_page();
		exit;
		}
			
		$habillages_changer = _request('changer_gestion');
		
		foreach (gestionnaires_habillages() as $gestionnaire_un=>$sous_gestionnaires_un) {
    		foreach ($sous_gestionnaires_un as $sous_gestionnaire_un) {
	    	
	    	if ($habillages_changer=='oui'){
				if (_request($sous_gestionnaire_un) != "") {
				ecrire_meta('habillages_'.$sous_gestionnaire_un.'_on', 'oui');
				ecrire_metas;
				}
				else {
				ecrire_meta('habillages_'.$sous_gestionnaire_un.'_on', 'non');
				ecrire_metas;
				}
			}
		}
		}
		
	if (isset($_GET['surligne']))
		$surligne = $_GET['surligne'];
	global $couleur_claire;
	debut_page(_T('commun:titre_plug'), "configuration", "habillages");
	echo "<style type='text/css'>\n";
	echo <<<EOF
div.cadre-padding ul li {
	list-style:none ;
}
div.cadre-padding ul {
	padding-left:1em;
	margin:.5em 0 .5em 0;
}
div.cadre-padding ul ul {
	border-left:5px solid #DFDFDF;
}
div.cadre-padding ul li li {
	margin:0;
	padding:0 0 0.25em 0;
}
div.cadre-padding ul li li div.nomplugin, div.cadre-padding ul li li div.nomplugin_on {
	border:1px solid #AFAFAF;
	padding:.3em .3em .6em .3em;
	font-weight:normal;
}
div.cadre-padding ul li li div.nomplugin a, div.cadre-padding ul li li div.nomplugin_on a {
	outline:0;
	outline:0 !important;
	-moz-outline:0 !important;
}
div.cadre-padding ul li li div.nomplugin_on {
	background:$couleur_claire;
	font-weight:bold;
}
div.cadre-padding div.droite label {
	padding:.3em;
	background:#EFEFEF;
	border:1px dotted #95989F !important;
	border:1px solid #95989F;
	cursor:pointer;
	margin:.2em;
	display:block;
	width:10.1em;
}
div.cadre-padding input {
	cursor:pointer;
}
div.detailplugin {
	border-top:1px solid #B5BECF;
	padding:.6em;
	background:#F5F5F5;
}
div.detailplugin hr {
	border-top:1px solid #67707F;
	border-bottom:0;
	border-left:0;
	border-right:0;
	}
EOF;
	echo "</style>";
	
	echo '<img src="' . _DIR_PLUGIN_HABILLAGES. '/../img_pack/habillages_icone-48.png">';
	gros_titre(_T('commun:titre_plug'));

	echo barre_onglets("habillages", 'accueil');
	
	debut_gauche();
	debut_boite_info();
	echo "<div class='intro_grotitre'>";
	echo gros_titre(_T('habillages:accueil_infos_titre'))."</div><br />";
	echo "<div class='intro'>";
	echo _T('habillages:accueil_infos')."<br /><br />";
	echo "<img src='"._DIR_PLUGIN_HABILLAGES."/../img_pack/habillages_squelettes-22.png' />&nbsp;";
	echo "<strong>"._T('habillages:accueil_titre_public')."</strong>";
	echo _T('habillages:accueil_infos_deux')."<br /><br />";
	echo "<img src='"._DIR_PLUGIN_HABILLAGES."/../img_pack/habillages_icones-22.png' />&nbsp;";
	echo "<strong>"._T('habillages:accueil_titre_prive')."</strong>";
	echo _T('habillages:accueil_infos_trois');
	echo "</div>";
	fin_boite_info();

	debut_droite();
	
	echo '<form exec="'.generer_url_action('habillages_accueil').'" method="post">';
	debut_boite_info();
	echo "<table border='0' cellpadding='0' cellspacing='0' id='subtab' align='center'>";
	
	foreach (gestionnaires_habillages() as $gestionnaire=>$sous_gestionnaires) {
		
    	echo "<tr>";
    	echo "<td colspan='4' id='hab_inputxt' class='toile_foncee hab_stitre'>Gestionnaire ".$gestionnaire;
    	//echo _T('commun:variable_titre');
    	echo "</td></tr>";
    
    	foreach ($sous_gestionnaires as $sous_gestionnaire) {
	    			
			if ($GLOBALS['meta']['habillages_'.$sous_gestionnaire.'_on'] == 'oui') {
				$checked_sous_gestionnaire = " checked='checked'";
			}
			else {$checked_sous_gestionnaire = "";}
	    	echo "<tr><td id='hab_input' class='toile_claire hab_fondclair'>&nbsp;";
            echo "</td><td id='hab_input' class='toile_claire hab_fondclair'>";
            echo '<img src="' . _DIR_PLUGIN_HABILLAGES. '/../img_pack/habillages_'.$sous_gestionnaire.'-22.png">';
            echo "</td><td id='hab_input' class='toile_claire hab_fondclair'>";
        	echo "<input type='checkbox' name='".$sous_gestionnaire."' value='".$sous_gestionnaire."'$checked_sous_gestionnaire></td>";
        	echo "<td id='hab_inputxt' class='toile_claire hab_fondclair'>".$sous_gestionnaire;
        	//echo _T('commun:variable_soustitre');
        	echo "</td></tr>";
    	}
	}
	echo "</table>";
	fin_boite_info();
	
	// Fin reecriture code de la page.
	
	
	echo "\n<input type='hidden' name='id_auteur' value='$connect_id_auteur' />";
	echo "\n<input type='hidden' name='hash' value='" . calculer_action_auteur("valide_plugin") . "'>";
	echo "\n<input type='hidden' name='changer_gestion' value='oui'>";

	echo "\n<p>";

	echo "<div style='text-align:$spip_lang_right'>";
	echo "<input type='submit' name='Valider' value='"._T('bouton_valider')."' class='fondo'>";
	echo "</div>";
	echo "</form>";

	echo "<a href='".generer_url_ecrire('admin_lang', 'module=habillages')."'>Modifier les textes</a><br /><br />";
		
	echo fin_gauche(), fin_page();

}

?>
