<?php
/*
+--------------------------------------------+
| ICOP 1.0 (03/2007) - SPIP 1.9.2
+--------------------------------------------+
| H. AROUX . Scoty . koakidi.com
| Script certifi� KOAK2.0 strict, mais si !
+--------------------------------------------+
| surcharge pour vos couleurs perso !
+--------------------------------------------+
1 - Renommez ce fichier en : mes_couleurs.php
2 - Placez le soit :
	- ../config/ ( <- logique !! )
	- en racine du plugin : plugins/icone_prive/
	- o� vous voulez ...
3 - Recopiez la s�quence ci-dessous pour chaque
	jeu en modifiant, bien-s�r, 
	par les valeurs de couleur de votre choix !
+--------------------------------------------+
*/
global $mes_couleurs;


# sequence � dupliquer pour chaque jeu :
// nom_couleur
$mes_couleurs[]=array(
	"couleur_foncee" => "#AC9B04",
	"couleur_claire" => "#E2D77E",
	"couleur_lien" => "#7B6E02",
	"couleur_lien_off" => "#9B8C09"
);

// ...

?>
