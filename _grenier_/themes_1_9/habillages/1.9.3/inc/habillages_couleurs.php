<?php

// Fonction qui affiche le gestionnaire de couleurs. 
function habillages_couleurs() {
global $mes_couleurs;
$check = "checked='checked'";

# lister couleur dispo

$meta_coul = array();
if($GLOBALS['meta']['habillages_couleurs']!='') {
	$meta_coul = explode(',',$GLOBALS['meta']['habillages_couleurs']);
}

	debut_boite_info();
	echo "<div class='intro_grotitre'>";
	echo gros_titre(_T('icones:ajout_couleur'))."</div><br />";
	//echo '<form action="'.generer_url_ecrire('habillages_prive').'" method="post">';
	echo '<form action="'.generer_url_action('habillages_ecrirecouleur').'" method="post">';
	echo "\n<input type='hidden' name='gestionnaire' value='couleurs' />";
	echo "<input type='hidden' name='redirect' value='".generer_url_ecrire("habillages_prive")."' />\n";
	echo "<input type='hidden' name='hash' value='".calculer_action_auteur("ecrirecouleur-rien")."' />\n";
	echo "<input type='hidden' name='id_auteur' value='".$connect_id_auteur."' />\n";
	
	echo "<table width='105' cellpadding='2' cellspacing='0' border='0' align='center'>";
	$i=0;
	foreach($mes_couleurs as $nc => $val) {
		$i++;
		$aff_check = (in_array($nc,$meta_coul))? $check: '';
		// au dela de 9 (icop) => couleurs perso : mes_couleurs.php
		if($i==10) {
			echo "<tr bgcolor='".$couleur_claire."'><td colspan='3'></td></tr>\n";
		}
		echo "<tr><td width='20'><input type='checkbox' name='ajout_coul[]' value='$nc' ".$aff_check." /></td>";
		echo "<td>".http_img_pack("rien.gif",'',"width='35' height='15' style='background-color:".$val['couleur_foncee'].";'")."</td>";
		echo "<td>".http_img_pack("rien.gif",'',"width='35' height='15' style='background-color:".$val['couleur_claire'].";'")."</td></tr>";
		
	}

	echo "</table>";
	echo "<div align='right'><input type='submit' value='"._T('valider')."' class='fondo' /></div>";
	echo "</form>"; 
	echo "<div class='intro'>";
	echo _T('icones:def_couleurs');
	echo "</div>";
	fin_boite_info();

}

?>