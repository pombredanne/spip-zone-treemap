<?php

// Fonction qui affiche le gestionnaire d'icones. 
function habillages_icones() {
	
	$check = "checked='checked'";
	
 	# lister les repertoires d'icones
	$meta_pack = $GLOBALS['meta']['habillages_img_pack'];
	$derrep = strtok($meta_pack,'/');
	while($derrep = strtok('/')) { $pack_actif=$derrep; }
	
	if($meta_pack=='' || $meta_pack==_DIR_IMG_PACK) {
		$meta_pack=_DIR_IMG_PACK;
		$pack_actif='Spip';
	}

	
	# on force spip en premier !
	$packs=array();
	$packs[]='spip';
	
	// Bouger le chemin ci-dessous afin de pouvoir le passer en variable.
	$d = dir(_DIR_PLUGIN_HABILLAGES.'/themes_natifs/icones_prives/');
    while (false !== ($entry = $d->read()) ) {
		if($entry!= "." && $entry != "..") {
			$packs[]=$entry;
		}
	}
	$d->close();
  	
	debut_cadre_couleur(_DIR_PLUGIN_HABILLAGES."/../img_pack/habillages_icones-22.png");	
	
	debut_boite_info();
	echo gros_titre(_T('icones:gros_titre_selecteur'));
	echo _T('icones:def_page_selecteur');
	echo "<p><strong>";
	echo (_T('icones:pack_actif', array('pack' => majuscules($pack_actif))));
	echo "</strong></p>";
	fin_boite_info();
	echo "<br />";

# affichage

	debut_boite_info();
	echo '<form action="'.generer_url_ecrire('habillages_prive').'" method="post">';
	echo "\n<input type='hidden' name='gestionnaire' value='icones' />";
	echo "<div align='right'><input type='submit' value='"._T('valider')."' class='fondo' /></div>";
	
	foreach($packs as $pack) {
		
		if($pack=='spip') {
			$repert = _DIR_IMG_PACK;
			$nom_theme = 'Spip';
			#$value='spip';
		}
		else {
			$repert = _DIR_PLUGIN_HABILLAGES.'/themes_natifs/icones_prives/'.$pack."/";
			$theme = $repert."/theme.xml";
			lire_fichier($theme, $texte);
				$arbre = parse_plugin_xml($texte);
				$arbre = $arbre['theme'][0];
				$type_theme = trim(applatit_arbre($arbre['type']));
				$nom_theme = applatit_arbre($arbre['nom']);
				$auteur_theme = applatit_arbre($arbre['auteur']);
				$version_theme = applatit_arbre($arbre['version']);
				$description_theme = applatit_arbre($arbre['description']);
			#$value = $repert;
		}
		$pack_select = "<img src='".$repert."puce-verte.gif' />";
	
	echo "<p></p>";
    echo "<table border='0' cellpadding='0' cellspacing='0' id='subtab' align='center'>";

	echo "<tr><td id='hab_input' class='toile_foncee hab_stitre'>";
	echo "<input type='radio' name='change_pack' value='$repert' ".(($meta_pack==$repert)? $check : '')." /></td>";
	echo "<td id='hab_inputxt' class='toile_foncee hab_stitre'>";
	echo (($meta_pack==$repert)? $pack_select."&nbsp;" : '')."<span class='verdana3'><b>".$nom_theme."</b></span>
			&nbsp;&middot;&middot;&nbsp;<span class='verdana2'>".$version_theme."</span>";

	echo "</td>";
	echo "<td id='hab_inpuico' class='toile_foncee hab_stitre'>";

    echo "<a href='".generer_url_ecrire('icop_listing','pack='.$pack)."' title='"._T('icones:voir_toutes_icones')."'>";
    echo "<img src='".$repert."cal-suivi.png' /></a>";
    
	echo "</td></tr>";
	echo "<tr>";
	echo "<td colspan='3' class='toile_claire hab_fondclair cellule48' onmouseover='changestyle('bandeauaccueil');>";
	echo "<a href='#'></a>";
	echo "</td></tr>";
		echo "<tr>";
	echo "<td colspan='3' class='toile_claire hab_fondclair cellule48' onmouseover='changestyle('bandeauaccueil');>";
	echo "<a href='#'>";
	echo "<img src='".$repert."asuivre-48.png' title='' alt='ico' />";
	echo "<img src='".$repert."documents-48.png' title='' alt='ico' />";
	echo "<img src='".$repert."messagerie-48.png' title='' alt='ico' />";
	echo "<img src='".$repert."redacteurs-48.png' title='' alt='ico' />";
	echo "<img src='".$repert."statistiques-48.png' title='' alt='ico' />";
	echo "<img src='".$repert."administration-48.png' title='' alt='ico' /></a>";
	echo "</td></tr>";
	echo "</table>";
		
	}

	
	fin_boite_info();

}

?>