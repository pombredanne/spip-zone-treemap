<?php
$chercher_styles = find_in_path("styles");
define('_DIR_STYLES',$chercher_styles);

// Fonction qui ecrit le gestionnaire de gestion de styles. 
function habillages_styles() {
	global $connect_statut;
	global $connect_toutes_rubriques;
	global $spip_lang_right;
	$surligne = "";

	echo "<style type='text/css'>\n";
	echo <<<EOF
ul#gallery{float:left}
ul#gallery, ul#gallery li{list-style:none;margin:0;padding:0}
ul#gallery li{float:left;display:inline;margin: 0 0 20px 20px;width:120px;text-align:center}
ul#gallery img{display:block;width:100px;height:70px;border:0px solid;margin:0 auto 5px}
ul#gallery a{display:block;height:90px;padding: 10px 0;background: $couleur_claire;color: #333;border:1px solid $couleur_foncee;text-decoration: none}
ul#gallery a:hover,ul#gallery a.selected{background: $couleur_foncee;color: #FFF;border-color:#000}
div#details{clear:left}
EOF;
	echo "</style>\n";
	
	echo gros_titre(_T('icone_admin_styles'),'',false);
	echo "<div style='margin:1em;'>";
	echo debut_cadre_relief('',true);

	global $couleur_foncee;
	echo _request('style');	
	$styles = preg_files(_DIR_STYLES,"/style_prive[.]html$");
	
	if (count($styles)){
		echo '<ul id="gallery">';
		foreach($styles as $l){
			$dir = dirname($l);
			$base = basename($dir);
			$dir_clean = str_replace('../', '', $dir);
			$img = "";
			$selected = "";
			if (isset($GLOBALS['meta']['habillages_styles']))
				//$selected = ($GLOBALS['meta']['habillages_styles']== "$base.html")?" class='selected'":"";

			echo '<form action="'.generer_url_ecrire('habillages_prive').'" method="post">';
			echo "exemple des styles";
			echo "\n<input type='hidden' name='gestionnaire' value='styles' />";
			echo "\n<input type='hidden' name='chemin_style' value='".$dir_clean."' />";
			echo "<input type='hidden' name='hash' value='".calculer_action_auteur("ecrirestyle-rien")."' />\n";
			echo "<input type='hidden' name='id_auteur' value='".$connect_id_auteur."' />\n";
			echo "<li><input type='submit' value='".$base."' class='fondo' /></li>";
			echo '</form>';
		}
		echo "</ul>";
	}
	echo "</div>";	
}

?>