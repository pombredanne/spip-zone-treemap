"roadmap.txt", ca permet d'eviter de nommer ce fichier "feuille_de_route.txt"...

-+- Pour la prochaine version -+-
- Voir pour virer minipres : trop lourd � maintenir. Jouer avec le repertoire des th�mes (r�fl�chir � une
arborescence) et habillages_options.
- Ajouter les premiers th�mes de l'espace priv�.
- Ins�rer dist-gala avec des th�mes qui s'adaptent au mieux � tous les layouts, au pire � une grande majorit�.
- G�n�raliser la personnalisation des chemins de th�mes en plus des squelettes pour tous les aspects des 
habillages : espace priv�, th�mes de squelettes, et icones de l'espace priv�. (tester avec des personnalisation 
partout)
- Donner la possibilit� de retirer l'aide des onglets sur la page d'accueil.

--> Le plugin se dirige vers des personnalisation des structures (squelettes, notamment dist_gala), des couleurs 
et styles appliqu�s � ces structures, des ic�nes pour l'espace priv�, des couleurs pour l'espace priv�, et des 
styles pour ce m�me espace.

--MEMO--
-> l'habillages de l'espace priv� se fait avec des fichiers nomm�s par la couleur de surcharge ("n" pour "noir"
et "b" pour "blanc") et par le pourcentage d'opacit� (n100.png est donc un fichier qui contient du noir � 100%
d'opacit�); voir si il ne serait pas plus judicieux de faire une banque d'images afin que les style_prive.html
aille puiser dedans en fonction de l'habillages que l'on veut rendre, plut�t que de d�finir des fichiers image 
dans chaque dossier de style.


-+- Apr�s la prochaine version -+-
Ecrire des petits howto pour personnaliser des th�mes dans un premier temps pour dist_gala et l'espace priv�.


-+- A voir sur la faisabilit� technique et pour une version ult�rieure. -+-

1) Integration du plugin "coloriage dist" : � revoir en fonction de la complexit� graphique que cela 
peut donner aux utilisateurs qui vont risquer de faire des sites tr�s laids. La premi�re �tape reste donc des 
th�mes pr�-d�finis qu'on aime ou qu'on aime pas mais qui �vitent les fautes de go�t.

2) Integration du plugin "cfg" : a revoir de plus pr�s, ne voyant plus l'int�r�t de g�rer le plugin
"habillages" avec cfg pour l'instant.
--> Problematique des mots cles : elle est fortement att�nu�e par "dist_gala", qu'il faut plut�t 
d�velopper avec des th�mes graphiques appropri�s au sein du plugin "habillages".

f r a n c k . d u c a s at f r e e . f r

Plugin "Habillages"
&copy; 2006-2007 - Distribu&#233; sous licence GNU/GPL