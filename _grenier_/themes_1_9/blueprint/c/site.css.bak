/* -------------------------------------------------------------- 
  
   Based on Bjorkoy.com/blueprint main site styles
   (This file is not a part of the framework)
   
-------------------------------------------------------------- */

body {
  background: url(blueprint/lib/img/baseline.png);
  margin-top:18px;
}

h1 {
  margin: 0;
  height: 54px;
}

p#quote {
  font-size:1.5em;
  line-height:36px;
  font-family: Georgia, serif;
}
span.bp {
  color:#369;
  font-weight:bold;
}

#note {
  background: #E5EFFA;
  padding: 18px 10px;
  margin-bottom:18px;
}
#note h3 {
  font-size: 1em;
  padding: 0px;
  font-weight: bold;
  color: #069;
}
a.download img {
  margin-bottom:14px;
}

p.notice {
  padding:16px;
  background: #E6EFC2;
  color: #529214;
  border: 2px solid #C6D880;
}
p.notice a {
  color: #529214;
  text-decoration: underline;
}
p.error {
  padding:16px;
  background: #FBE3E4;
  color: darkred;
  border: 2px solid #FBC2C4;  
}
p.error a {
  color: darkred;
  text-decoration: underline;
}

/* -------------------------------------------------------------- 
  
   Based Spip's habillage.css
   
-------------------------------------------------------------- */

/* Entete */

#entete #nom_site_spip, #entete a .spip_logos {
	display: block;
	float: left;
	font-weight: bold;
	font-size: 3em; }
#entete a { text-decoration: none; }
#entete .formulaire_recherche { float: right; }

/* Fil d'Ariane */
#hierarchie {
	clear: both;
	font-size: 0.8em; 
	margin-bottom: 1.875em; 
	line-height: 1.875em; }

/* Pied de page */
#pied {
	clear: both;
	width: 100%;
	margin-top: 3em;
	border-top: 0.1em solid #ccc;
	padding: 0.2em 0.4em;
	text-align: center; }
#pied small { font-size: 0.8em; }
#pied img { vertical-align: bottom; }


/*  ------------------------------------------
/*  Habillage des menus et de la navigation
/*  ------------------------------------------ */

/*  Habillage general des menus de navigation
---------------------------------------------- */
.rubriques, .breves, .syndic, .forums, .divers {
	min-width: 10em;
	border: 2px solid #CCC;
	margin-bottom: 14px;
	font-size: 10px;
}

.menu-titre {
	padding: 13px 0.4em 11px 0.4em;
	border-bottom: 2px dotted #CCC;
	text-align: center;
	font-weight: bold;
	font-size:14px;color:#333;
}

#navigation p, .encart p { text-align: left;  }
#navigation ul, .encart ul { text-align: left; list-style: none; margin: 9px 0 9px 9px;}
#navigation li, .encart li {line-height:18px; font-size:12px;}

/*  Des couleurs specifiques selon les types de menus
---------------------------------------------- */
.rubriques { background: #EAFFEA;border-color: #B3EFB3;}
.rubriques .menu-titre { background: #F2FFF2;border-color: #B3EFB3; }

.breves { background: #FFEAEA; border-color: #FBC2C4;}
.breves .menu-titre { background: #FFF2F2; border-color: #FBC2C4;}

.syndic { background: #E8F8F8; border-color: #B3EFEF;}
.syndic .menu-titre { background: #F0FCFC; border-color: #B3EFEF;}

.forums { background: #EAEAFF; border-color: #C2C3EF;}
.forums .menu-titre { background: #F2F2FF; border-color: #C2C3EF;}

.divers { background: #F8F8E8; border-color: #EFEFC2;}
.divers .menu-titre { background: #FCFCF0; border-color: #EFEFC2;}

/*  ------------------------------------------
/*  Habillage du contenu
/*  ------------------------------------------ */

/*  Cartouche et titraille
---------------------------------------------- */
.cartouche { margin-bottom: 36px; }

.cartouche .titre {
	font-size: 22px;
	padding:7px 0;
	color: #000; }
#contenu .surtitre, #contenu .soustitre { font-size: 14px; padding:3px 0;}

.cartouche .spip_logos {
	float: right;
	margin-left: 18px;
	margin-bottom: 18px; }

.cartouche p { clear: left; }
.cartouche small { font-size: 12px; }

.cartouche .traductions { font-size: 12px; padding:3px 0;}
.cartouche .traductions * { display: inline; }
.cartouche .traductions li { padding-left: 1em; }


/*  Mise en forme des textes du contenu
---------------------------------------------- */
.chapo {
	margin-bottom: 18px;
	font-size:1.5em;
	line-height:36px;
	font-family: Georgia, serif;
	}

.lien {
	background: #EEE;
	margin: 9px;
	margin-bottom: 18px;
	padding: 3px;
	border: 1px solid #CCC;
	font-size: 12px;
	font-weight: bold; }

.texte {color: #000;font-size:14px;}

.ps, .notes {
	margin-top: 10px;
	padding-top: 6px;
	border-top: 2px solid #CCC;}
.ps { font-size: 12px;line-height:18px; }
.notes { clear: both; }
.ps h2, .notes h2 { font-size: 18px; margin:9px 0;font-weight: bold; }

/*  Portfolio
---------------------------------------------- */
#documents_portfolio {
	clear: both;
	margin-top: 1.4em;
	padding-top: 2px;
	border-top: 2px solid #CCC; }
#documents_portfolio h2 { margin-bottom: 0.4em; font-size: 0.88em; font-weight: bold; }

#document_actif { margin-top: 1em; }

/*  Listes de documents joints (a un article ou une rubrique)
---------------------------------------------- */
#documents_joints {
	margin-top: 1.4em;
	padding-top: 2px;
	border-top: 2px solid #CCC; }
#documents_joints h2 { margin-bottom: 0.4em; font-size: 0.88em; font-weight: bold; }
#documents_joints ul { margin: 0; padding: 0; list-style: none; }
#documents_joints li { margin-bottom: 0.4em; }
#documents_joints li .spip_doc_titre {}
#documents_joints li .spip_doc_titre small { font-weight: normal; }
#documents_joints li .spip_doc_descriptif {}

/* Listes d'articles et extraits introductifs
----------------------------------------------- */
.liste-articles ul { margin: 0; padding: 0; list-style: none; }
.liste-articles li { margin-bottom: 18px; clear: both; }

.liste-articles li .titre {
	font-size: 22px;
	padding:0;margin:6px 0;}

.liste-articles li .spip_logos {
	float: right;
	margin-left: 18px;
	margin-bottom: 18px;
	clear: right; }

.liste-articles li p { margin: 9px 0; padding: 0; line-height:18px;}

.liste-articles li .enclosures {
	float: right;
	text-align: right;
	max-width: 60%;
	margin: 0; }

.liste-articles li small {
	display: block;
	font-size: 12px; line-height:18px;}

.liste-articles li .texte {
	margin-top: 6px;
	margin-bottom: 10px;
	border: 1px solid #CCC;
	padding: 9px;
	font-size: 14px;
	line-height: 18px; }

.pagination { font-size: 0.90em; }

/*  ------------------------------------------
/*  Habillage specifique du plan du site
/*  ------------------------------------------ */

.page_plan .cartouche { display: none; }

.page_plan #contenu h2 {
	clear: both;
	background: #EEE;
	border: 1px solid #CCC;
	padding: 0.5em;
	margin-bottom: 1em;
	font-weight: bold;
	text-align: center; }

.page_plan #contenu .contre-encart ul {
	display: block;
	clear: left;
	margin-top: 0;
	margin-bottom: 1em;
	padding-top: 0; }
.page_plan #contenu .contre-encart li {}

/*  ------------------------------------------
/*  Habillage des formulaires
/*  ------------------------------------------ */

.formulaire_spip { text-align: left; font-size: 12px; }
.formulaire_spip p { margin: 3px 0; padding: 0; }

.formulaire_spip fieldset {}
.formulaire_spip legend {}

.formulaire_spip label {}
.formulaire_spip .forml { width: 99%; font-family: inherit; font-size: inherit; }

/* Boutons */
.spip_bouton { text-align: right; }
.spip_bouton input { float: right; }

/* Reponse du formulaire */
.reponse_formulaire { font-weight: bold; color: #e86519; }
fieldset.reponse_formulaire { border-color: #e86519; font-weight: normal; }

/* Previsualisation du message */
fieldset.previsu { padding: 9px; }

/* Formulaire de login au forum */
.formulaire_login_forum .forml { width: 12em; }
.formulaire_login_forum .spip_logos { float: right; padding-left: 10px; }

/* Choix des mots-clefs */
ul.choix_mots { float: left; width: 47%; margin: 0; padding: 0; list-style: none; } 
ul.choix_mots label { display: inline; }

/* Formulaire de recherche */
.formulaire_recherche { width: 12em; text-align: left; }
.formulaire_recherche label { display: none; }

/*  ------------------------------------------
/*  Habillage des forums
/*  ------------------------------------------ */

.forum-repondre, .forum-decompte {
	clear: both;
	margin-top: 2.5em;
	padding-top: 2px;
	border-top: 2px solid #CCC;
	font-size: 0.88em;
	font-weight: bold; }

/* * Habillage des forums */
ul.forum { display: block; clear: both; margin: 0; padding: 0; }
ul.forum, ul.forum ul { list-style: none; }
.forum-fil { margin-top: 1.5em; }
.forum-fil ul { display: block; margin: 0; padding: 0; margin-left: 1em; }
.forum-chapo .forum-titre, .forum-chapo .forum-titre a { display: block; margin: 0; padding: 0; font-weight: bold; text-decoration: none; color: #333; }
.forum-chapo small {}
.forum-texte { margin: 0; padding: 0.5em 1em 0 1em; color: #333; font-size: 0.82em; }
.forum-texte .forum-lien {}
.forum-texte .forum-repondre-message { margin: 0; padding: 1px 0; text-align: right; }

/* Boite d'un forum : eclaircissement progressif des bords */
ul .forum-message { border: 1px solid #666; margin: 0; padding: 0; margin-bottom: 1em; }
ul ul .forum-message { border: 1px solid #A4A4A4; }
ul ul ul .forum-message { border: 1px solid #B8B8B8; }
ul ul ul ul .forum-message { border: 1px solid #CCC; }
ul ul ul ul ul .forum-message { border: 1px solid #E0E0E0; }
ul ul ul ul ul ul .forum-message { border: 1px dotted #E0E0E0; }

/* Boite de titre d'un forum : mise en couleur selon la profondeur du forum */
ul .forum-chapo { border: none; border-bottom: 1px dotted #B8B8B8; margin: 0; padding: 3px 6px 2px 6px; font-size: 0.88em; background: #C4E0E0; }
ul ul .forum-chapo { background: #D4E8E8; }
ul ul ul .forum-chapo { background: #E4F0F0; }
ul ul ul ul .forum-chapo { background: #F4F8F8; border-bottom: 1px dotted #E0E0E0; }
ul ul ul ul ul .forum-chapo { background: #FFF; }

/*  ------------------------------------------
/*  Habillage des petitions
/*  ------------------------------------------ */

#signatures { clear: both; font-size: 0.77em; }

#signatures h2 {
	padding: 0.5em;
	text-align: center;
	font-size: 1.2em;
	font-weight: bold; }

#signatures table { width: 100%; margin: 1em 0; }
#signatures thead { display: none; } /* On n'affiche pas les titres du tableau */

#signatures td.signature-date {
	background: #E4F0F0;
	padding: 0.5em;
	white-space: nowrap; }

#signatures td.signature-nom {
	background: #ECF4F4;
	padding: 0.2em;
	text-align: center;
	font-weight: bold; }

#signatures td.signature-message {
	background: #F4F8F8;
	padding: 0.4em;
	font-size: 0.96em; }

/* SPIP-STYLE                                              version 1.9  */
/* Cette feuille contient les styles associes au code genere par SPIP.  */
/* Ces styles sont indispensables et doivent necessairement etre        */
/* definis pour le bon fonctionnement de vos squelettes.                */


/* Raccourcis typographiques de SPIP */
.spip_puce { list-style-position: outside; }

/* Paragraphes, citations, code et poesie */

.spip_poesie { 
	margin: 1.5em; 
	padding-left: 1em; 
	border-left: .1em solid #ccc;
	font-family: Garamond, Georgia, Times, serif; }
.spip_poesie div { text-indent: -60px; margin-left: 60px; }

.spip_code, .spip_cadre { font-family: monospace; font-style: normal;}
.spip_cadre { width: 99%; border: .1em inset; }

.spip_surligne { background: #FF6; }

/* Tableaux */


/* Logos, documents et images */
img, .spip_logos { margin: 0; padding: 0; border: none; }

.spip_documents { text-align: center; }
.spip_documents p { }
.spip_documents_center { clear: both; width: 100%; margin: auto; }
span.spip_documents_center { display: block; margin-top:1.5em; }
.spip_documents_left { float: left; margin-right: 15px; margin-bottom: 1.5em; }
.spip_documents_right { float: right; margin-left: 15px; margin-bottom: 1.5em; }
.spip_doc_titre { font-weight: bold; font-size: 0.8em; margin-left: auto; margin-right: auto; }
.spip_doc_descriptif { clear: both; font-size: 0.8em; margin-left: auto; margin-right: auto; }

/* Images typographiques (via image_typo), URL a revoir... */
.format_png { behavior: url(win_png.htc); }

/* modeles par defaut */
.spip_modele { float: right; display: block;background: #fafafa; width: 180px; }

/* Barre de raccourcis typographiques */
table.spip_barre { width: 100%; }

table.spip_barre a img {
	background: #FDA;
	padding: 3px;
	border: 1px outset #999; }
table.spip_barre a:hover img {
	background: #FFF;
	border: 1px solid #999; }

table.spip_barre input.barre { width: 100%; background: #EEE; }

/* * Couleurs des liens de Spip */
a.spip_note {} /* liens vers notes de bas de page */
a.spip_in { color: #900; } /* liens internes */
a.spip_out { color: #009; } /* liens sortants */
a.spip_url { color: #009; } /* liens url sortants */
a.spip_glossaire { color: #060; } /* liens vers encyclopedie */
.on { font-weight: bold; color: #000; } /* liens exposes */