/*
Nifty Corners Cubes - Layout
Documentation - Instruction (in English - en anglais) :
	http://www.html.it/articoli/niftycube/
*/

window.onload=function(){
Nifty("div#navigation,div#contenu","same-height");
Nifty("div#entete","transparent");
Nifty("div#page","big transparent");
Nifty("div.encart div.breves", "transparent");
Nifty("div#pied,div.chapo");
Nifty("div.liste-articles div.texte","transparent");
Nifty("div.contre-encart");
}