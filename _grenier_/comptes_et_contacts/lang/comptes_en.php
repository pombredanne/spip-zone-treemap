<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'info_cadre_navigation' => 'This plugin is under development. Its goal is to extend the SPIP authors, by adding two extention tables : Accounts (legal body) and Contacts (persons)',

	// N
	'nom_bouton_plugin' => 'Comptes &amp; Contacts',
	'nom_famille' => 'Family name',
	
	// P
	'prenom' => 'First name',
	
	// T
	'titre_page_gestion' => 'Accounts &amp; contacts plugin management page',
);
?>