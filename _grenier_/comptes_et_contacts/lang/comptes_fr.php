<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'info_cadre_navigation' => 'Ce plugin est en cours de d&eacute;veloppement. Son objectif est d\'&eacute;tendre le auteurs SPIP en ajoutant 2 tables d\'extension, Comptes (personnes morales) et Contact (personnes physiques).',

	// N
	'nom_bouton_plugin' => 'Comptes &amp; Contacts',
	'nom_famille' => 'Nom de famille',
	
	// P
	'prenom' => 'Pr&eacute;nom',
	
	// T
	'titre_page_gestion' => 'Page de gestion du plugin Comptes &amp; contacts',
);
?>