<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=zh
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => '将这个词语加入词典。',
	'ortho_aucune_suggestion' => '对这个词没有找到任何建议。',
	'ortho_avis_privacy' => '文章发布系统预设了拼写检查功能。在使用这一功能之前，请仔细阅读下列文字：',
	'ortho_avis_privacy2' => '为了检查一篇文章的拼写，网站将把词语列表发送到站外的« 拼写检查服务器 »中的一个。这些服务器是由万维网文章发布系统社区的成员创立的。为了保证一定程度的隐私性，列表中的词语的顺序被打乱。如果您仍然担心您的数据的安全，请不要激活这一功能（并请立即从网站上取回您的信息）。 ',
	'ortho_ce_mot_connu' => '这一词语在网站的词库中存在。',
	'ortho_description' => '               <div class=\'verdana2\'>文章发布系统预设了拼写检查功能。在使用这一功能之前，请仔细阅读下列文字：</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               为了检查一篇文章的拼写，网站将把词语列表发送到站外的« 拼写检查服务器 »中的一个。这些服务器是由万维网文章发布系统社区的成员创立的。为了保证一定程度的隐私性，列表中的词语的顺序被打乱。如果您仍然担心您的数据的安全，请不要激活这一功能（并请立即从网站上取回您的信息）。 
               </p></blockquote></div>',
	'ortho_dico_absent' => '未找到该语言的词库。',
	'ortho_mode_demploi' => '未辨认出的词语以红色下划线标出。您可以点击每一个词语以查看修改建议。',
	'ortho_mots_a_corriger' => '待修改词语',
	'ortho_orthographe' => '拼写',
	'ortho_supprimer_ce_mot' => '将改词语从词库中删除',
	'ortho_trop_de_fautes' => '注意：您的文章中错误太多。为了使系统不至于过载，没有计算任何修改建议。',
	'ortho_trop_de_fautes2' => '请修改最明显的错误，然后再试。',
	'ortho_verif_impossible' => '系统无法核对本文的拼写。',
	'ortho_verifier' => '检查拼写。'
);

?>
