<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=ja
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => '辞書にこの単語を追加',
	'ortho_aucune_suggestion' => 'この単語のための提案は見つかりませんでした。',
	'ortho_avis_privacy' => 'Spipはスペルチェッカーを含んでいます。しかしそれを有効にする前に、慎重に次の説明を読んでください:',
	'ortho_avis_privacy2' => '文章のスペルをチェックするために、サイトはチェックされる単語のリストを、Spipコミュニティーの人が利用可能な外部の"スペルチェックするサーバー"に送るでしょう。単語はいくらか機密性を高めるために、送られる前にぐちゃぐちゃに入れ替えられます。もしあなたのデータが心配なら、このオプションを有効にしないでください。（そして、すぐにあなたのすべての情報をWebから撤回してください）。',
	'ortho_ce_mot_connu' => 'この単語はサイトの辞書にあります。',
	'ortho_description' => '               <div class=\'verdana2\'>Spipはスペルチェッカーを含んでいます。しかしそれを有効にする前に、慎重に次の説明を読んでください:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               文章のスペルをチェックするために、サイトはチェックされる単語のリストを、Spipコミュニティーの人が利用可能な外部の"スペルチェックするサーバー"に送るでしょう。単語はいくらか機密性を高めるために、送られる前にぐちゃぐちゃに入れ替えられます。もしあなたのデータが心配なら、このオプションを有効にしないでください。（そして、すぐにあなたのすべての情報をWebから撤回してください）。
               </p></blockquote></div>',
	'ortho_dico_absent' => 'この言語のための辞書が見つかりませんでした',
	'ortho_mode_demploi' => '認識できていない単語は、赤で強調されています。訂正候補を見るにはそれら単語をクリック。',
	'ortho_mots_a_corriger' => '訂正すべき単語',
	'ortho_orthographe' => 'スペリング',
	'ortho_supprimer_ce_mot' => '辞書からこの単語を取り除く',
	'ortho_trop_de_fautes' => 'あなたの文章はスペル間違いがあまりに多すぎます！システムに負担を掛けすぎないように、訂正部分は表示しません。',
	'ortho_trop_de_fautes2' => '最も明らかなミスから修正することから始めて、再び挑戦してみて下さい。',
	'ortho_verif_impossible' => 'システムはこの文章のスペルを検査することが出来ません。',
	'ortho_verifier' => 'スペルチェック'
);

?>
