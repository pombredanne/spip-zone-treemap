<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Pridať toto slovo do slovníka',
	'ortho_aucune_suggestion' => 'Pre toto slovo sa nenašiel žiaden návrh.',
	'ortho_avis_privacy' => 'SPIP obsahuje kontrolu preklepov. Predtým, ako ju aktivujete, si však, prosím, pozorne prečítajte nasledujúci odsek:',
	'ortho_avis_privacy2' => 'In order to check the spelling of a text, the site will send the list of words to be checked to an external "spelling server", which people of the Spip community have made available. The words are mixed up before being sent in order to maintain some level of confidentiality. If you are concerned about your data, do not activate this option (and withdraw all your information from the web at once).',
	'ortho_ce_mot_connu' => 'Toto slovo sa nachádza v slovníku stránky.',
	'ortho_description' => '               <div class=\'verdana2\'>Spip contains a spell checker. However, before enabling it, please read the following paragraph carefully:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               In order to check the spelling of a text, the site will send the list of words to be checked to an external "spelling server", which people of the Spip community have made available. The words are mixed up before being sent in order to maintain some level of confidentiality. If you are concerned about your data, do not activate this option (and withdraw all your information from the web at once).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'No dictionary was found for this language',
	'ortho_mode_demploi' => 'Nerozoznané slová boli zvýraznené červenou farbou. Kliknite na ne a zobrazia sa navrhované opravy.',
	'ortho_mots_a_corriger' => 'slová na opravu',
	'ortho_orthographe' => 'Pravopis',
	'ortho_supprimer_ce_mot' => 'Odstrániť toto slovo zo slovníka',
	'ortho_trop_de_fautes' => 'Vo vašom texte je priveľa chýba! Aby sa predišlo preťaženiu systému, žiadne opravy neboli navrhnuté.',
	'ortho_trop_de_fautes2' => 'Start by correcting the most obvious mistakes, then try again.',
	'ortho_verif_impossible' => 'Systém nemôže skontrolovať preklepy v tomto texte.',
	'ortho_verifier' => 'Kontrola pravopisu'
);

?>
