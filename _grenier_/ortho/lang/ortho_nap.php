<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_description' => '               <div class=\'verdana2\'>SPIP ha un controllo ortografico integrato. Prima di attivare questa funzionalit&agrave;, leggere attentamente quanto segue:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Per verificare l\'ortografia di un testo, il sito invier&agrave; la lista di parole da controllare a uno dei "server ortografici" esterni messi a disposizione al pubblico dai membri della comunit&agrave; SPIP. Le parole sono inviate in ordine casuale per rispettare la propriet&agrave; intellettuale degli autori. &Egrave; consigliabile disattivare tale funzionalit&agrave; se i testi trattati nel sito sono ritenuti troppo confidenziali (e magari si eviti di pubblicarli sul web...).
               </p></blockquote></div>', # MODI
);

?>
