<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=oc_lnc
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Ajustar aqueste mot al diccionari',
	'ortho_aucune_suggestion' => 'S\'es pas trobat cap de suggestion per aqueste mot.',
	'ortho_avis_privacy' => 'Un verificador d\'ortografia es integrat a SPIP. Pr\'aquò, abans d\'activar aquela foncionalitat, volgatz legir amb atencion lo paragraf seguent:',
	'ortho_avis_privacy2' => 'Per verificar l\'ortografia d\'un tèxt, lo sit mandarà la lista dels mots de contrarotlar vèrs un dels "servidors d\'ortografia" extèrnes que los diferents sòcis de la comunitat SPIP los an botats a vòstra disposicion. Se manda los mots dins lo desòrdre per fin de garentir un minim de confidencialitat. Se vos fasètz de lagui per las donadas vòstras, activetz pas aquela opcion (e levatz sul pic vòstras informacions del web).',
	'ortho_ce_mot_connu' => 'Aqueste mot fa partida del diccionari del sit.',
	'ortho_description' => '               <div class=\'verdana2\'>Un verificador d\'ortografia es integrat a SPIP. Pr\'aquò, abans d\'activar aquela foncionalitat, volgatz legir amb atencion lo paragraf seguent:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Per verificar l\'ortografia d\'un tèxt, lo sit mandarà la lista dels mots de contrarotlar vèrs un dels "servidors d\'ortografia" extèrnes que los diferents sòcis de la comunitat SPIP los an botats a vòstra disposicion. Se manda los mots dins lo desòrdre per fin de garentir un minim de confidencialitat. Se vos fasètz de lagui per las donadas vòstras, activetz pas aquela opcion (e levatz sul pic vòstras informacions del web).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'S\'es pas trobat ges de diccionari per aquesta lenga',
	'ortho_mode_demploi' => 'Los mots non reconeguts son subrelinhats de roge. Podètz clicar sus cada mot per afichar de suggestions de correccion.',
	'ortho_mots_a_corriger' => 'mots de corregir',
	'ortho_orthographe' => 'Ortografia',
	'ortho_supprimer_ce_mot' => 'Levar aquel mot del diccionari',
	'ortho_trop_de_fautes' => 'Atencion: vòstre tèxt ten tròp d\'errors, lai se suggerís pas ges de correccion per tal de  subrecargar pas lo sistèma.',
	'ortho_trop_de_fautes2' => 'Començatz per corregir las errors mai evidentas e tornatz assajar puèi.',
	'ortho_verif_impossible' => 'Lo sistèma pòt pas verificar l\'ortografia d\'aqueste tèxt.',
	'ortho_verifier' => 'Verificar l\'ortografia'
);

?>
