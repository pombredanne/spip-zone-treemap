<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_description' => '               <div class=\'verdana2\'>Spip inneh&aring;ller en r&auml;ttstavningsfunktion. Innan du anv&auml;nder den skall du l&auml;sa f&ouml;ljande mening noggrant:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               F&ouml;r att kontrollera stavningen av en text, kommer Spip att skicka en lista med orden till en extern "r&auml;ttstavningsserver", som personer i Spip-gemenskapen st&auml;llt till ditt f&ouml;rfogande. Orden blandas om innan de skickas f&ouml;r att skydda inneh&aring;llet till viss del. Om du &auml;r orolig &ouml;ver detta, aktivera inte r&auml;ttstavningen (och ta bort din information fr&aring;n webben).
               </p></blockquote></div>', # MODI
);

?>
