<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=pt
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Incluir esta palavra ao dicionário',
	'ortho_aucune_suggestion' => 'Nenhuma sugestão foi encontrada para esta palavra.',
	'ortho_avis_privacy' => 'Um verificador de ortografia faz parte do SPIP. Porém, antes de activar esta funcionalidade, por favor, leia atentamente o parágrafo a seguir:',
	'ortho_avis_privacy2' => 'Para verificar a ortografia de um texto, o site irá enviar a lista de palavras para verificação para um dos «servidores de ortografia» externos, colocados à sua disposição por diferentes membros da comunidade SPIP. As palavras são enviadas desordenadamente, para garantir um mínimo de confidencialidade. Se se preocupa com a confidencialidade dos seus dados, não active esta opção (e retire imediatamente as suas informações da web).',
	'ortho_ce_mot_connu' => 'Esta palavra faz parte do dicionário do site.',
	'ortho_description' => '               <div class=\'verdana2\'>Um verificador de ortografia faz parte do SPIP. Porém, antes de activar esta funcionalidade, por favor, leia atentamente o parágrafo a seguir:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Para verificar a ortografia de um texto, o site irá enviar a lista de palavras para verificação para um dos «servidores de ortografia» externos, colocados à sua disposição por diferentes membros da comunidade SPIP. As palavras são enviadas desordenadamente, para garantir um mínimo de confidencialidade. Se se preocupa com a confidencialidade dos seus dados, não active esta opção (e retire imediatamente as suas informações da web).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Não foi encontrado nenhum dicionário para este idioma',
	'ortho_mode_demploi' => 'As palavras não reconhecidas estão sublinhadas a vermelho. Pode clicar em cada palavra para colocar sugestões de correcção.',
	'ortho_mots_a_corriger' => 'palavras para corrigir',
	'ortho_orthographe' => 'Ortografia',
	'ortho_supprimer_ce_mot' => 'Retirar esta palavra do dicionário',
	'ortho_trop_de_fautes' => 'Atenção: o seu texto contém demasiados erros, nenhuma correção será sugerida de modo a não sobrecarregar o sistema.',
	'ortho_trop_de_fautes2' => 'Comece por corrigir os erros mais óbvios e tente novamente.',
	'ortho_verif_impossible' => 'O sistema não pode verificar a ortografia deste texto.',
	'ortho_verifier' => 'Verificar a ortografia'
);

?>
