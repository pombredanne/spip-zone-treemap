<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Añadir esta palabra al diccionario',
	'ortho_aucune_suggestion' => 'No se han encontrado sugerencias para esta palabra.',
	'ortho_avis_privacy' => 'En SPIP se integra un corrector ortográfico. Sin embargo, antes de activar esta característica, deberías leer atentamente el siguiente párrafo:',
	'ortho_avis_privacy2' => 'Para comprobar la ortografía de un texto, el sitio web enviará la lista de palabras a revisar a uno de los « servidores ortográficos » externos puestos a disposición por distintos miembros de la comunidad SPIP. Las palabras se envían desordenadas para asegurar un mínimo de confidencialidad. Si sientes preocupación por tus datos, no actives esta opción (y retira enseguida tu información de la Red).',
	'ortho_ce_mot_connu' => 'Esta palabra está en el diccionario del sitio.',
	'ortho_description' => '               <div class=\'verdana2\'>En SPIP se integra un corrector ortográfico. Sin embargo, antes de activar esta característica, deberías leer atentamente el siguiente párrafo:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Para comprobar la ortografía de un texto, el sitio web enviará la lista de palabras a revisar a uno de los « servidores ortográficos » externos puestos a disposición por distintos miembros de la comunidad SPIP. Las palabras se envían desordenadas para asegurar un mínimo de confidencialidad. Si sientes preocupación por tus datos, no actives esta opción (y retira enseguida tu información de la Red).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'No se ha encontrado ningún diccionario para este idioma',
	'ortho_mode_demploi' => 'Se han subrayado en rojo las palabras no reconocidas. Puedes pulsar sobre cada palabra para mostrar sugerencias para su corrección.',
	'ortho_mots_a_corriger' => 'palabras por corregir',
	'ortho_orthographe' => 'Ortografía',
	'ortho_supprimer_ce_mot' => 'Suprimir esta palabra del diccionario',
	'ortho_trop_de_fautes' => 'Atención: tu texto contiene demasiadas faltas de ortografía, no se ha sugerido ninguna corrección para no sobrecargar el sistema.',
	'ortho_trop_de_fautes2' => 'Comienza por corregir las faltas más evidentes y vuelve a intentarlo.',
	'ortho_verif_impossible' => 'El sistema no puede comprobar la ortografía de este texto.',
	'ortho_verifier' => 'Revisar la ortografía'
);

?>
