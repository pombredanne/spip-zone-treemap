<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=oc_prv
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Ajustar aquest mot au diccionari',
	'ortho_aucune_suggestion' => 'S\'es pas trobat ges de suggestion per aquest mot.',
	'ortho_avis_privacy' => 'Un verificador d\'ortografia es integrat a SPIP. Pasmens, avans d\'activar aquela foncionalitat, vougatz legir amb atencion lo paragraf seguent:',
	'ortho_avis_privacy2' => 'Per verificar l\'ortografia d\'un tèxt, lo sit mandarà la lista dei mots de contrarotlar vèrs un dei "servidors d\'ortografia" extèrnes que lei diferents sòcis de la comunitat SPIP leis an botats a vòstra disposicion. Se manda lei mots dins lo desòrdre per fin de garentir un minim de confidencialitat. Se vos fasètz de lagui per lei donadas vòstras, activetz pas aquela opcion (e levatz sus lo còp vòstreis informacions dau web).',
	'ortho_ce_mot_connu' => 'Aquest mot fa partida dau diccionari dau sit.',
	'ortho_description' => '               <div class=\'verdana2\'>Un verificador d\'ortografia es integrat a SPIP. Pasmens, avans d\'activar aquela foncionalitat, vougatz legir amb atencion lo paragraf seguent:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Per verificar l\'ortografia d\'un tèxt, lo sit mandarà la lista dei mots de contrarotlar vèrs un dei "servidors d\'ortografia" extèrnes que lei diferents sòcis de la comunitat SPIP leis an botats a vòstra disposicion. Se manda lei mots dins lo desòrdre per fin de garentir un minim de confidencialitat. Se vos fasètz de lagui per lei donadas vòstras, activetz pas aquela opcion (e levatz sus lo còp vòstreis informacions dau web).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'S\'es pas trobat ges de diccionari per aquesta lenga',
	'ortho_mode_demploi' => 'Lei mots non reconeguts son subrelinhats de roge. Podètz clicar sus cada mot per afichar de suggestions de correccion.',
	'ortho_mots_a_corriger' => 'mots de corregir',
	'ortho_orthographe' => 'Ortografia',
	'ortho_supprimer_ce_mot' => 'Levar aqueu mot dau diccionari',
	'ortho_trop_de_fautes' => 'Atencion: vòstre tèxt ten tròp d\'errors, i se suggerís ges de correccion per fin de pas subrecargar lo sistèma.',
	'ortho_trop_de_fautes2' => 'Començatz per corregir leis errors mai evidentas e tornatz assajar puei.',
	'ortho_verif_impossible' => 'Lo sistèma pòt pas verificar l\'ortografia d\'aquest tèxt.',
	'ortho_verifier' => 'Verificar l\'ortografia'
);

?>
