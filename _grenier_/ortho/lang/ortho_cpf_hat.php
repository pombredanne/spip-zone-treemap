<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=cpf_hat
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Ajoute mo-a nan diksyonè',
	'ortho_aucune_suggestion' => 'Pa oken sijésyon te fin rannkont pou mo sa-a.',
	'ortho_avis_privacy' => 'Nan SPIP gen yon vérifyè lòtograf. Avan mèt ali, mèsi fè atansyon ek ann fè lekti ti tèks isit anba :',
	'ortho_avis_privacy2' => 'Pou gade lòtograf yo tèks sit-la ke voye lalis mo pou kontrolen sou yon « sève lòtograf » déò ki diferan mamn kominoté SPIP ka bay laksé pou tout moun. Tout mo a-yo ki voye nan désòd pou sere bagay sekré. Si w efreye done aw se pa proteje,   akti pa lopsyon an.',
	'ortho_ce_mot_connu' => 'Mo an ki rankont nan diksyonè sit-la.',
	'ortho_description' => '               <div class=\'verdana2\'>Nan SPIP gen yon vérifyè lòtograf. Avan mèt ali, mèsi fè atansyon ek ann fè lekti ti tèks isit anba :</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Pou gade lòtograf yo tèks sit-la ke voye lalis mo pou kontrolen sou yon « sève lòtograf » déò ki diferan mamn kominoté SPIP ka bay laksé pou tout moun. Tout mo a-yo ki voye nan désòd pou sere bagay sekré. Si w efreye done aw se pa proteje,   akti pa lopsyon an.
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Pa oken diksyonè ki prézan pou lalang-la',
	'ortho_mode_demploi' => 'Tout mo ki pa rokoni, yo se afiche nan liy rouj. W kapab klike sou chak mo sa-yo pou afiche kèk sijésyon pou korije.',
	'ortho_mots_a_corriger' => 'kèk mo fòk zot ki korije',
	'ortho_orthographe' => 'Lòtograf',
	'ortho_supprimer_ce_mot' => 'Efase mo a nan diksyonè',
	'ortho_trop_de_fautes' => 'Tansyon : tèks la ki sere trod fòt. Akoz bagay-la pa oken koreksyon ki pwopoze. Se pou pa sichaje sitèm la. ',
	'ortho_trop_de_fautes2' => 'Komans korije pli gwo fòt lotogwaf ek esèy aprè yon lot fwa.',
	'ortho_verif_impossible' => 'Sistèm-la se pa kapab gade lòtograf tèks-la.',
	'ortho_verifier' => 'Gade lòtograf'
);

?>
