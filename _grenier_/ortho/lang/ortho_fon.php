<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=fon
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Zé gɔ ná wěmámɛ',
	'ortho_aucune_suggestion' => 'Yé mɔ àyì ɖé ɖó wékwín é lɔ wú á.',
	'ortho_avis_privacy' => 'Amɔ có nú mi ná sɔ wěmà mi tɔn ɖé xlɛ hún mi ván núkún ɖémɛ ɖésú hwɛ',
	'ortho_avis_privacy2' => 'Amɔ có nú mi ná sɔ wěmà mi tɔn ɖé xlɛ hún mi ván núkún ɖémɛ ɖésú hwɛ',
	'ortho_ce_mot_connu' => 'wékwín é lɔ ɖò wěmámɛ',
	'ortho_description' => '               <div class=\'verdana2\'>Amɔ có nú mi ná sɔ wěmà mi tɔn ɖé xlɛ hún mi ván núkún ɖémɛ ɖésú hwɛ</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Amɔ có nú mi ná sɔ wěmà mi tɔn ɖé xlɛ hún mi ván núkún ɖémɛ ɖésú hwɛ
               </p></blockquote></div>',
	'ortho_dico_absent' => 'yé mɔ wěmà ɖé ɖò gbè é lɔ mɛ á',
	'ortho_mode_demploi' => 'wékwín ɖê yé mà tùn à lɛ ɔ ɖò vɔvɔmɛ. Zìn yé bó kpɔn àyì é yè b ɖó yèwú ɔ.',
	'ortho_mots_a_corriger' => 'bló wékwín ɔ ɖó',
	'ortho_orthographe' => 'winwlán fɔ ɖìɖé',
	'ortho_supprimer_ce_mot' => 'Bɛ wékwín sín wěmámɛ',
	'ortho_trop_de_fautes' => 'Vlɛ mi klɛn àfɔ súkpɔ ɖò wěmà mi tɔn mɛ ',
	'ortho_trop_de_fautes2' => 'Mi jɛ wěmà lɛ blóɖó jí nú mi má wá wàzɔ gɔná ó.',
	'ortho_verif_impossible' => 'Mi jɛ wěmà lɛ blóɖó jí nú mi má wá wàzɔ gɔná ó.',
	'ortho_verifier' => 'mi kpɔn winwlán fɔ ɖìɖé'
);

?>
