<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=pl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Dodaj to słowo do słownika',
	'ortho_aucune_suggestion' => 'Nie znaleziono żadnej podpowiedzi dla tego słowa.',
	'ortho_avis_privacy' => 'SPIP posiada zintegrowany korektor ortografii. Jednak zanim go włączysz, przeczytaj uważnie następny paragraf :',
	'ortho_avis_privacy2' => 'Aby sprawdzić ortografię tekstu system wyśle listę słów do sprawdzenia na jeden z "serwerów ortograficznych" oddanych do dyspozycji przez członków społeczności SPIP. Słowa są wysyłane jako nieuporządkowana lista słów aby zagwarantować poufność. Jeśli masz obawy o poufność danych nie włączaj tej opcji (i usuń informacje z internetu).  ',
	'ortho_ce_mot_connu' => 'Słowo jest w słowniku stronu.',
	'ortho_description' => '               <div class=\'verdana2\'>SPIP posiada zintegrowany korektor ortografii. Jednak zanim go włączysz, przeczytaj uważnie następny paragraf :</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Aby sprawdzić ortografię tekstu system wyśle listę słów do sprawdzenia na jeden z "serwerów ortograficznych" oddanych do dyspozycji przez członków społeczności SPIP. Słowa są wysyłane jako nieuporządkowana lista słów aby zagwarantować poufność. Jeśli masz obawy o poufność danych nie włączaj tej opcji (i usuń informacje z internetu).  
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Dle tego języka nie został znaleziony żaden słownik',
	'ortho_mode_demploi' => 'Nierozpoznane słowa są podkreślone na czerwono. Kliknij na każdym słowie aby wyświetlić podpowiedzi korekt.',
	'ortho_mots_a_corriger' => 'słowa do poprawienia',
	'ortho_orthographe' => 'Ortografia',
	'ortho_supprimer_ce_mot' => 'Usuń słowo ze słownika',
	'ortho_trop_de_fautes' => 'Uwaga : twój tekst zawiera za dużo błędów. Żadne korekty nie będą sugerowane aby nie przeciążać systemu.',
	'ortho_trop_de_fautes2' => 'Zacznij od usunięcia najbardziej oczywistych błędów i spróbuj powtórnie.',
	'ortho_verif_impossible' => 'System nie może sprawdzić ortografii w tym tekście.',
	'ortho_verifier' => 'Sprawdź ortografię'
);

?>
