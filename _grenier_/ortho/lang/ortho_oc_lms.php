<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=oc_lms
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Ajustar queste mot au diccionari',
	'ortho_aucune_suggestion' => 'S\'es pas trobat brisa de suggestion per queste mot.',
	'ortho_avis_privacy' => 'Un verificador d\'ortografia es integrat a SPIP. Emper, avans d\'activar quela foncionalitat, volhatz legir emb atencion lo paragraf seguent:',
	'ortho_avis_privacy2' => 'Per verificar l\'ortografia d\'un text, lo sit mandará la lista daus mots de contrarotlar vers un daus "servidors d\'ortografia" externes que los diferents sòcis de la comunitat SPIP los an botats a vòstra disposicion. Se manda los mots dins lo desòrdre per fin de garentir un minim de confidencialitat. Se portatz pena per las donadas vòstras, activetz pas quela opcion (e tiratz còp sec vòstras informacions dau web).',
	'ortho_ce_mot_connu' => 'Queste mot fai partida dau diccionari dau sit.',
	'ortho_description' => '               <div class=\'verdana2\'>Un verificador d\'ortografia es integrat a SPIP. Emper, avans d\'activar quela foncionalitat, volhatz legir emb atencion lo paragraf seguent:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Per verificar l\'ortografia d\'un text, lo sit mandará la lista daus mots de contrarotlar vers un daus "servidors d\'ortografia" externes que los diferents sòcis de la comunitat SPIP los an botats a vòstra disposicion. Se manda los mots dins lo desòrdre per fin de garentir un minim de confidencialitat. Se portatz pena per las donadas vòstras, activetz pas quela opcion (e tiratz còp sec vòstras informacions dau web).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'S\'es pas trobat brisa de diccionari per questa lenga',
	'ortho_mode_demploi' => 'Los mots non reconeguts son subrelinhats de roge. Podetz clicar sus chasque mot per afichar de las suggestions de correccion.',
	'ortho_mots_a_corriger' => 'mots de corregir',
	'ortho_orthographe' => 'Ortografia',
	'ortho_supprimer_ce_mot' => 'Tirar queu mot dau diccionari',
	'ortho_trop_de_fautes' => 'Atencion: vòstre text ten tròp d\'errors, lai se suggerís brisa de correccion per fin de pas subrecharjar lo sistema.',
	'ortho_trop_de_fautes2' => 'Començatz per corregir las errors mai evidentas e tornatz assajar puei.',
	'ortho_verif_impossible' => 'Lo sistèma pòt pas verificar l\'ortografia de queste text.',
	'ortho_verifier' => 'Verificar l\'ortografia'
);

?>
