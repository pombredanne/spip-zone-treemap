<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=cpf
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Azouté lé mo-la andann lo diksyonèr',
	'ortho_aucune_suggestion' => 'Néna pa okinn idé pou lé mo-la.',
	'ortho_avis_privacy' => 'Andann Spip néna in vérifiktèr lo grafi. Avann ou fé zwé ali, ni konsèy aou li byin sat lé marké anba tèrla :',
	'ortho_avis_privacy2' => 'Po vérifyé la grafi lo tèks, sit-la sar envwayé lo list bann mo po kontrolé si  inn « servèr  la grafi »  déor, sat désertinn dalon l kominoté SPIP i rann piblik pou tout. Néna pwin okinn ord pou envwayé tout bann mot-la. Komsala lo sékré lé respété. Si ou l apèr, anserv pas lopsyon-la (ou pé mèma artir koméla out bann zinformasyon si lo wèb).',
	'ortho_ce_mot_connu' => 'Lé mo-la li lé sort pa andan lo diksyonèr lo sit.',
	'ortho_description' => '               <div class=\'verdana2\'>Andann Spip néna in vérifiktèr lo grafi. Avann ou fé zwé ali, ni konsèy aou li byin sat lé marké anba tèrla :</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Po vérifyé la grafi lo tèks, sit-la sar envwayé lo list bann mo po kontrolé si  inn « servèr  la grafi »  déor, sat désertinn dalon l kominoté SPIP i rann piblik pou tout. Néna pwin okinn ord pou envwayé tout bann mot-la. Komsala lo sékré lé respété. Si ou l apèr, anserv pas lopsyon-la (ou pé mèma artir koméla out bann zinformasyon si lo wèb).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Ni la pa trouvé okinn diksyonèr po zot lang-la.',
	'ortho_mode_demploi' => 'Bann mot lé pa konu li lé sirlinyé n rouz. Ou pé klik azot po afisé bann koreksyon ni propoz aou.',
	'ortho_mots_a_corriger' => 'bann mo pou korizé',
	'ortho_orthographe' => 'Grafi',
	'ortho_supprimer_ce_mot' => 'Artir lé mo-la andann lo diksyonèr',
	'ortho_trop_de_fautes' => 'Pangar : néna trod larlik èk lo grafi, lé pa posib afisé bann zidé pou korizé akoz lo sistèm i sré tro krazé.',
	'ortho_trop_de_fautes2' => 'Koriz avann out grogrinn larlik èk out grafi, aprésa ou sra pou esèy diksyonèr-la.',
	'ortho_verif_impossible' => 'Sistèm-la lé pa kav vérifyé lo tèks-la.',
	'ortho_verifier' => 'Vérifyé lo grafi'
);

?>
