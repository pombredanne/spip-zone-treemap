<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=lb
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Dëst Wuert an den Dictionnaire derbäi setzen',
	'ortho_aucune_suggestion' => 'Keng Suggestioun fonnt fir dëst Wuert.',
	'ortho_avis_privacy' => 'En Orthographie-Korrektor ass a SPIP integréert. Ier dier dës Fonktionnalitéit aschalt, liest w.e.g. de nächsten Abschnit:',
	'ortho_avis_privacy2' => 'Fir d\'Orthographie vun engem Text ze kontrolléieren, schéckt de Site d\'Lëscht vun de Wierder op en externen "Orthographie-Server" deen vun de Membere vun der SPIP-Communautéit bereed gestalt gëtt. D\'Wierder ginn duergerneen geschéckt fir e Minimum vun Vertraulëchkeet ze garantéieren. Wann dir ëm är Daten besuergt sidd, da schalt dës Optioun nët an (an huelt direkt är Informationen vum Internet erof).',
	'ortho_ce_mot_connu' => 'Dëst Wuert gët ët am Dictionnaire vum Site.',
	'ortho_description' => '               <div class=\'verdana2\'>En Orthographie-Korrektor ass a SPIP integréert. Ier dier dës Fonktionnalitéit aschalt, liest w.e.g. de nächsten Abschnit:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Fir d\'Orthographie vun engem Text ze kontrolléieren, schéckt de Site d\'Lëscht vun de Wierder op en externen "Orthographie-Server" deen vun de Membere vun der SPIP-Communautéit bereed gestalt gëtt. D\'Wierder ginn duergerneen geschéckt fir e Minimum vun Vertraulëchkeet ze garantéieren. Wann dir ëm är Daten besuergt sidd, da schalt dës Optioun nët an (an huelt direkt är Informationen vum Internet erof).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Fir dës Sprooch gouf keen Dictionnaire fonnt.',
	'ortho_mode_demploi' => 'Déi onbekannte Wierder si roud markéiert. Dir kënnt op all Wuert klicken fir Viirschéi fir d\'Korrektur ze gesinn.',
	'ortho_mots_a_corriger' => 'Wierder ze verbesseren',
	'ortho_orthographe' => 'Orthographie',
	'ortho_supprimer_ce_mot' => 'Dëst Wuert aus dem Dictionnaire läschen',
	'ortho_trop_de_fautes' => 'Opgepasst: ären Text huet zevill Fehler, ët gët keng Korrektur virgeschloën fir de System nët ze iwwerlaaschten.',
	'ortho_trop_de_fautes2' => 'Verbessert déi einfachst Fehler a probéiert nach eng Kéier.',
	'ortho_verif_impossible' => 'De System kann d\'Orthographie vun dësem Text nët kontrolléieren.',
	'ortho_verifier' => 'Orthographie kontrolléieren'
);

?>
