<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Ajouter ce mot au dictionnaire', # NEW
	'ortho_aucune_suggestion' => 'Aucune suggestion n\'a été trouvée pour ce mot.', # NEW
	'ortho_avis_privacy' => 'Un vérificateur d\'orthographe est intégré à SPIP. Toutefois, avant d\'activer cette fonctionnalité, veuillez lire avec attention le paragraphe suivant :', # NEW
	'ortho_avis_privacy2' => 'Pour vérifier l\'orthographe d\'un texte, le site va envoyer la liste des mots à contrôler vers l\'un des « serveurs d\'orthographe » externes mis à votre disposition par différents membres de la communauté SPIP. Les mots sont envoyés dans le désordre afin d\'assurer un minimum de confidentialité. Si vous avez des craintes pour vos données, n\'activez pas cette option (et retirez tout de suite vos informations du Web).', # NEW
	'ortho_ce_mot_connu' => 'Ce mot fait partie du dictionnaire du site.', # NEW
	'ortho_description' => '               <div class=\'verdana2\'>Un vérificateur d\'orthographe est intégré à SPIP. Toutefois, avant d\'activer cette fonctionnalité, veuillez lire avec attention le paragraphe suivant :</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Pour vérifier l\'orthographe d\'un texte, le site va envoyer la liste des mots à contrôler vers l\'un des « serveurs d\'orthographe » externes mis à votre disposition par différents membres de la communauté SPIP. Les mots sont envoyés dans le désordre afin d\'assurer un minimum de confidentialité. Si vous avez des craintes pour vos données, n\'activez pas cette option (et retirez tout de suite vos informations du Web).
               </p></blockquote></div>', # NEW
	'ortho_dico_absent' => 'Aucun dictionnaire n\'a été trouvé pour cette langue', # NEW
	'ortho_mode_demploi' => 'Les mots non reconnus sont surlignés en rouge. Vous pouvez cliquer sur chaque mot pour afficher des suggestions de correction.', # NEW
	'ortho_mots_a_corriger' => 'mots à corriger', # NEW
	'ortho_orthographe' => 'Orthographe', # NEW
	'ortho_supprimer_ce_mot' => 'Enlever ce mot du dictionnaire', # NEW
	'ortho_trop_de_fautes' => 'Attention : votre texte contient trop de fautes, aucune correction n\'est suggérée afin de ne pas surcharger le système.', # NEW
	'ortho_trop_de_fautes2' => 'Kunayman jan walinak uñjktax ukanak askicham ukhamarak mayamp yant\'am.',
	'ortho_verif_impossible' => 'Le système ne peut pas vérifier l\'orthographe de ce texte.', # NEW
	'ortho_verifier' => 'Vérifier l\'orthographe' # NEW
);

?>
