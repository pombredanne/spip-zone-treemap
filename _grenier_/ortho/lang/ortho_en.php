<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Add this word to the dictionary',
	'ortho_aucune_suggestion' => 'No suggestion was found for this word.',
	'ortho_avis_privacy' => 'SPIP contains an internal spell checker. However, before enabling it, please read the following paragraph carefully:',
	'ortho_avis_privacy2' => 'In order to check the spelling of a text, the site will send the list of words to be checked to an external "spelling server", which people of the SPIP community have made available. The words are mixed up before being sent in order to maintain some level of confidentiality. If you are concerned about your data, do not activate this option (and withdraw all your information from the web at once).',
	'ortho_ce_mot_connu' => 'This word is in the site\'s dictionary.',
	'ortho_description' => '               <div class=\'verdana2\'>SPIP includes an internal spell checker. However, before enabling it, please read the following paragraph carefully:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               In order to check the spelling of a text, the site will send the list of words to be checked to an external "spelling server", which people of the SPIP community have made available. The words are mixed up before being sent in order to maintain some level of confidentiality. If you are concerned about your data, do not activate this option (and withdraw all your information from the web at once).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'No dictionary was found for this language',
	'ortho_mode_demploi' => 'Unrecognised words have been highlighted in red. Click on these words to see suggested corrections.',
	'ortho_mots_a_corriger' => 'words to correct',
	'ortho_orthographe' => 'Spelling',
	'ortho_supprimer_ce_mot' => 'Remove this word from the dictionary',
	'ortho_trop_de_fautes' => 'Your text contains too many mistakes! In order to avoid overloading the system, no corrections have been suggested.',
	'ortho_trop_de_fautes2' => 'Start by correcting the most obvious mistakes, then try again.',
	'ortho_verif_impossible' => 'The system is unable to check the spelling of this text.',
	'ortho_verifier' => 'Spell check'
);

?>
