<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=cs
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Přidat slovo do slovníku',
	'ortho_aucune_suggestion' => 'K tomuto slovu nebyl nalezen žádný návrh.',
	'ortho_avis_privacy' => 'Součástí systému SPIP je program pro kontrolu překlepů. Než ho však zapnete, přečtěte si pečlivě následující informaci:',
	'ortho_avis_privacy2' => 'Při kontrole pravopisu systém využívá seznamy slov na externích " pravopisných serverech " jež dávají k dispozici členové komunity SPIP. Pro zajištění alespoň minimální ochrany údajů jsou slova na tyto servry zasílána v přeházeném pořadí. Máte-li obavy o svá data, tuto funkci nezapínejte (a neprodleně odstraňte svá data z webu).',
	'ortho_ce_mot_connu' => 'Toto slovo je ve slovníku na webu.',
	'ortho_description' => '               <div class=\'verdana2\'>Součástí systému SPIP je program pro kontrolu překlepů. Než ho však zapnete, přečtěte si pečlivě následující informaci:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Při kontrole pravopisu systém využívá seznamy slov na externích " pravopisných serverech " jež dávají k dispozici členové komunity SPIP. Pro zajištění alespoň minimální ochrany údajů jsou slova na tyto servry zasílána v přeházeném pořadí. Máte-li obavy o svá data, tuto funkci nezapínejte (a neprodleně odstraňte svá data z webu).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Pro tento jazyk nebyl nalezen žádný slovník',
	'ortho_mode_demploi' => 'Neznámá slova jsou označena červeně. Navrhované opravy zobrazíte klepnutím na takto označená slova.',
	'ortho_mots_a_corriger' => 'slovo k opravě',
	'ortho_orthographe' => 'Pravopis',
	'ortho_supprimer_ce_mot' => 'Odstranit slovo ze slovníku',
	'ortho_trop_de_fautes' => 'Pozor!: v textu je přílš mnoho chyb. Aby nedošlo k přetížení systému, nejsou navrhovány žádné opravy.',
	'ortho_trop_de_fautes2' => 'Odstraňte chyby patrné na první pohled a zkuste to znovu.',
	'ortho_verif_impossible' => 'Systém nemůže zkontrolavat překlepy v tomto textu.',
	'ortho_verifier' => 'Zkontrolovat překlepy'
);

?>
