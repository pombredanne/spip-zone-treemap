<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=ast
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Amestar esta pallabra al diccionariu',
	'ortho_aucune_suggestion' => 'Nun s\'atopó suxerencia dala pa esta pallabra.',
	'ortho_avis_privacy' => 'Hai un comprebador d\'ortografía integráu en SPIP. Sicasí, enantes d\'activar esta carauterística, has de lleer con atención el párrafu siguiente:',
	'ortho_avis_privacy2' => 'Pa comprebar la ortografía d\'un testu, el sitiu va unviar la llista de pallabres a controlar a unu de los «sirvidores d\'ortografía» esternos que ufren diferentes miembros de la comunidá SPIP. Les pallabres unvíense en desorde a la fin d\'asegurar un mínimu de confidencialidá. Si tuvieres medrana de que se vean los tos datos, nun actives esta opción (y desanicia darréu toa la to información de la Rede).',
	'ortho_ce_mot_connu' => 'Esta pallabra pertenez al diccionariu del sitiu.',
	'ortho_description' => '               <div class=\'verdana2\'>Hai un comprebador d\'ortografía integráu en SPIP. Sicasí, enantes d\'activar esta carauterística, has de lleer con atención el párrafu siguiente:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Pa comprebar la ortografía d\'un testu, el sitiu va unviar la llista de pallabres a controlar a unu de los «sirvidores d\'ortografía» esternos que ufren diferentes miembros de la comunidá SPIP. Les pallabres unvíense en desorde a la fin d\'asegurar un mínimu de confidencialidá. Si tuvieres medrana de que se vean los tos datos, nun actives esta opción (y desanicia darréu toa la to información de la Rede).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Nun s\'atopó dengún diccionariu pa esta llingua',
	'ortho_mode_demploi' => 'Les pallabres nun reconocíes sollíniense en roxu. Pues calcar en cada pallabra pa amosar suxerencies de correición.',
	'ortho_mots_a_corriger' => 'pallabres a correxir',
	'ortho_orthographe' => 'Ortografía',
	'ortho_supprimer_ce_mot' => 'Desaniciar esta pallabra del diccionariu',
	'ortho_trop_de_fautes' => 'Atención: el to testu contién abondes faltes, nun se fai suxerencia de correición dala pa nun sobrecargar el sistema.',
	'ortho_trop_de_fautes2' => 'Entama por iguar les faltes más evidentes y lluéu torna a intentalo.',
	'ortho_verif_impossible' => 'El sistema nun pue comprebar la ortografía d\'esti testu.',
	'ortho_verifier' => 'Verificar la ortografía'
);

?>
