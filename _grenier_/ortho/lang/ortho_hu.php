<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=hu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Hozzáadni ezt a szót a szótárban',
	'ortho_aucune_suggestion' => 'Nincs tipp találva arra a szóra.',
	'ortho_avis_privacy' => 'Az SPIP egy helyesírás ellenőrzőt tartalmaz. Azonban mielőbb aktiválna ezt a lehetőséget, figyelemmel olvassa a következő szöveget :',
	'ortho_avis_privacy2' => 'Egy szöveg helyesírásának ellenőrzésére, a honlap fogja küldeni az ellenőrizendő szavak listáját az egyik külső "helyesírási szerver" felé, melyet az SPIP közönség egyes tagjai rendelkezésre bocsájtanak. A szavak más sorrendben vannak küldve, minimális bizalmasság érdekében. Ha félti adatait, tiltsa ezt az opciót (és azonnal vonja vissza informacióit a webről).',
	'ortho_ce_mot_connu' => 'Ez a szó szerepel a honlap szótárában.',
	'ortho_description' => '               <div class=\'verdana2\'>Az SPIP egy helyesírás ellenőrzőt tartalmaz. Azonban mielőbb aktiválna ezt a lehetőséget, figyelemmel olvassa a következő szöveget :</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Egy szöveg helyesírásának ellenőrzésére, a honlap fogja küldeni az ellenőrizendő szavak listáját az egyik külső "helyesírási szerver" felé, melyet az SPIP közönség egyes tagjai rendelkezésre bocsájtanak. A szavak más sorrendben vannak küldve, minimális bizalmasság érdekében. Ha félti adatait, tiltsa ezt az opciót (és azonnal vonja vissza informacióit a webről).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Nincs szótár erre a nyelvre',
	'ortho_mode_demploi' => 'A fel nem ismert szavak pirosban vannak jelölve. Minden szóra lehet kattintani javítasi javaslatok megjelenítésére.',
	'ortho_mots_a_corriger' => 'javítandó szavak',
	'ortho_orthographe' => 'Helyesírás',
	'ortho_supprimer_ce_mot' => 'Kivonni a szót a szótárból',
	'ortho_trop_de_fautes' => 'Vigyázat : A szövege túl sok hibát tartalmaz, nincs javaslat javításra azért, hogy a rendszer ne legyen túlterhelve.',
	'ortho_trop_de_fautes2' => 'Elöszőr javítsa a legegyértelműbb hibákat, majd probálkozzon újra.',
	'ortho_verif_impossible' => 'A rendszer nem tudja ellenőrizni e szöveg helyesírását.',
	'ortho_verifier' => 'Helyesírás ellenőrzés indítása'
);

?>
