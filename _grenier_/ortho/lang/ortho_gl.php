<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=gl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Engadir a palabra ao dicionario',
	'ortho_aucune_suggestion' => 'Non hai suxestión relacionada con este termo',
	'ortho_avis_privacy' => 'Un comprobador ortográfico foi integrado en SPIP. De todas maneiras, antes de o activar, lea con atención o parágrafo seguinte :',
	'ortho_avis_privacy2' => 'Para comprobar a ortografía dun texto, o web vai enviar a lista de termos para comprobar a un dos « servidores d\'ortografía » externos que poñen á súa disposición varios membros da comunidade SPIP. As verbas son enviadas con desorde co fin de asegurar un mínimo de confidencialidade. Se vostede ten reservas cos seus datos, non active esta opción (e retire todo dato persoal do web).',
	'ortho_ce_mot_connu' => 'Este termo está recollido no dicionario do web',
	'ortho_description' => '               <div class=\'verdana2\'>Un comprobador ortográfico foi integrado en SPIP. De todas maneiras, antes de o activar, lea con atención o parágrafo seguinte :</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Para comprobar a ortografía dun texto, o web vai enviar a lista de termos para comprobar a un dos « servidores d\'ortografía » externos que poñen á súa disposición varios membros da comunidade SPIP. As verbas son enviadas con desorde co fin de asegurar un mínimo de confidencialidade. Se vostede ten reservas cos seus datos, non active esta opción (e retire todo dato persoal do web).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Non se atopou ningún dicionario para esta  lingua',
	'ortho_mode_demploi' => 'Os termos non recoñecidos están subliñados en vermello. Pode premer sobre cada verba para ver suxestións de corrección',
	'ortho_mots_a_corriger' => 'Termos para corrixir',
	'ortho_orthographe' => 'Ortógrafo',
	'ortho_supprimer_ce_mot' => 'Sacar este termo do dicionario',
	'ortho_trop_de_fautes' => 'Atención : o seu texto contén demasiadas faltas, non se suxire ningunha corrección co fin de non sobrecargar o sistema.',
	'ortho_trop_de_fautes2' => 'Comece por corrixir as faltas máis evidentes e reinténteo logo.',
	'ortho_verif_impossible' => 'O sistema non pode comprobar a ortografía deste texto.',
	'ortho_verifier' => 'Comprobar a ortografía'
);

?>
