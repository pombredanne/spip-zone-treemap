<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Dit woord aan het woordenboek toevoegen',
	'ortho_aucune_suggestion' => 'Voor dit woord hebben we geen enkele suggestie gevonden.',
	'ortho_avis_privacy' => 'Een spellingcontrole is toegevoegd aan SPIP. Vooraleer deze functie te activeren, raden we je aan de volgende paragraaf aandachtig te lezen :',
	'ortho_avis_privacy2' => 'Tijdens het uitvoeren van een spellingcontrole zal de site de te controleren woordenlijst verzenden naar een van de externe « spellingservers » die door verschillende leden van de SPIP-gemeenschap ter beschikking gesteld worden. De woorden worden in willekeurige volgorde verstuurd, dit om een minimum aan geheimhouding te waarborgen. Indien uw gegevens vertrouwelijk zijn, activeer deze functie dan niet (en haal uw gegevens ook van het internet).',
	'ortho_ce_mot_connu' => 'Dit woord staat in het woordenboek van de site.',
	'ortho_description' => '               <div class=\'verdana2\'>Een spellingcontrole is toegevoegd aan SPIP. Vooraleer deze functie te activeren, raden we je aan de volgende paragraaf aandachtig te lezen :</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Tijdens het uitvoeren van een spellingcontrole zal de site de te controleren woordenlijst verzenden naar een van de externe « spellingservers » die door verschillende leden van de SPIP-gemeenschap ter beschikking gesteld worden. De woorden worden in willekeurige volgorde verstuurd, dit om een minimum aan geheimhouding te waarborgen. Indien uw gegevens vertrouwelijk zijn, activeer deze functie dan niet (en haal uw gegevens ook van het internet).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Voor deze taal werd geen woordenboek gevonden',
	'ortho_mode_demploi' => 'Niet-herkende woorden zijn rood onderlijnd. Je kan een woord aanklikken om suggesties voor verbetering weer te geven.',
	'ortho_mots_a_corriger' => 'te verbeteren woorden',
	'ortho_orthographe' => 'Spelling',
	'ortho_supprimer_ce_mot' => 'Dit woord uit het woordenboek verwijderen',
	'ortho_trop_de_fautes' => 'Opgelet : uw tekst bevat veel schrijffouten. Correcties worden niet voorgesteld om het systeem niet te overbelasten.',
	'ortho_trop_de_fautes2' => 'Begin met het verbeteren van de meest zichtbare fouten en probeer vervolgens opnieuw.',
	'ortho_verif_impossible' => 'Het systeem kan de schrijfwijze van deze tekst niet controleren.',
	'ortho_verifier' => 'Schrijfwijze controleren'
);

?>
