<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Wort zum Wörterbuch hinzufügen',
	'ortho_aucune_suggestion' => 'Für dieses Wort wurde kein Vorschlag gefunden.',
	'ortho_avis_privacy' => 'SPIP hat eine integrierte Rechtschreibkorrektur. Vor dem Aktivieren lesen sie bitte diesen Text aufmerksam durch:',
	'ortho_avis_privacy2' => 'Damit die Rechtschreibung eines Texts überprüft werden kann, schickt SPIP die Worte an «Orthographieserver», die von Mitgliedern der SPIP-Gemeinde zur Verfügung gestellt werden. Die Worte werden in zufälliger Reihenfolge übertragen, um ein Maximun an Vertraulichkeit zu ermöglichen. Wenn Ihnen diese Methode nicht sicher genug ist, verzichten sie bitte darauf, die Rechtschreibprüfung zu aktivieren (und entfernen ihre Informationen sofort aus dem Internet).',
	'ortho_ce_mot_connu' => 'Dieses Wort steht im Wörterbuch der Website.',
	'ortho_description' => '               <div class=\'verdana2\'>SPIP hat eine integrierte Rechtschreibkorrektur. Vor dem Aktivieren lesen sie bitte diesen Text aufmerksam durch:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Damit die Rechtschreibung eines Texts überprüft werden kann, schickt SPIP die Worte an «Orthographieserver», die von Mitgliedern der SPIP-Gemeinde zur Verfügung gestellt werden. Die Worte werden in zufälliger Reihenfolge übertragen, um ein Maximun an Vertraulichkeit zu ermöglichen. Wenn Ihnen diese Methode nicht sicher genug ist, verzichten sie bitte darauf, die Rechtschreibprüfung zu aktivieren (und entfernen ihre Informationen sofort aus dem Internet).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Für diese Sprache wurde kein Wörterbuch gefunden.',
	'ortho_mode_demploi' => 'Nicht erkannte Worte sind rot markiert. Sie können auf jedes Wort klicken, um die Korrekturvorschläge anzuzeigen.',
	'ortho_mots_a_corriger' => 'Zu korrigieren',
	'ortho_orthographe' => 'Orthographie',
	'ortho_supprimer_ce_mot' => 'Wort aus dem Wörerbuch löschen',
	'ortho_trop_de_fautes' => 'Achtung: Ihr Text enthält zu viele Fehler. Es wurden keine Korrekturvorschläge gemacht, um das System nicht zu überlasten.',
	'ortho_trop_de_fautes2' => 'Korrigieren sie zunächst die offensichtlichen Fehler und starten dann einen neuen Versuch.',
	'ortho_verif_impossible' => 'Das System kann die Rechtschreibung dieses Texts nicht prüfen.',
	'ortho_verifier' => 'Rechtschreibung prüfen'
);

?>
