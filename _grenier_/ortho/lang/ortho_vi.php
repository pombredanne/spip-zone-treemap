<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=vi
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Thêm từ này vào tự điển',
	'ortho_aucune_suggestion' => 'Không có đề nghị nào cho từ này.',
	'ortho_avis_privacy' => 'Spip có cơ phận xét lỗi chính tả. Tuy nhiên, trước khi mở lên, xin vui lòng đọc kỹ ghi chú sau:',
	'ortho_avis_privacy2' => 'Để xét lỗi chính tả một văn bản, trang web sẽ gửi danh sách các từ cần xét qua  một "máy xét lỗi" bên ngoài do cộng đồng Spip thực hiện. Các từ được trộn lẫn lại trước khi gửi đi để có được một mức độ riêng tư. Nếu bạn thật quan tâm đến dữ liệu của riêng mình, xin đừng mở đặc tính xét lỗi này lên (và thâu hồi ngay dữ liệu của bạn ra khỏi trang web).',
	'ortho_ce_mot_connu' => 'Từ này có trong tự điển.',
	'ortho_description' => '               <div class=\'verdana2\'>Spip có cơ phận xét lỗi chính tả. Tuy nhiên, trước khi mở lên, xin vui lòng đọc kỹ ghi chú sau:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Để xét lỗi chính tả một văn bản, trang web sẽ gửi danh sách các từ cần xét qua  một "máy xét lỗi" bên ngoài do cộng đồng Spip thực hiện. Các từ được trộn lẫn lại trước khi gửi đi để có được một mức độ riêng tư. Nếu bạn thật quan tâm đến dữ liệu của riêng mình, xin đừng mở đặc tính xét lỗi này lên (và thâu hồi ngay dữ liệu của bạn ra khỏi trang web).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Không tìm thấy tự điển của ngôn ngữ này.',
	'ortho_mode_demploi' => 'Những từ không nhận ra đã được tô đỏ. Bấm vào các từ này để xem đề nghị sửa đổi.',
	'ortho_mots_a_corriger' => 'Từ cần sửa',
	'ortho_orthographe' => 'Đánh vần',
	'ortho_supprimer_ce_mot' => 'Xóa từ này khỏi tự điển',
	'ortho_trop_de_fautes' => 'Văn bản có quá nhiều lỗi! Không có đề nghị sửa đổi nào cả, để tránh làm quá tải máy.',
	'ortho_trop_de_fautes2' => 'Hãy điều chỉnh những lỗi hiển nhiên trước, rồi thử lại.',
	'ortho_verif_impossible' => 'Hệ thống không thể xét lỗi chính tả của văn bản này.',
	'ortho_verifier' => 'Xét lỗi'
);

?>
