<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=eu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Hitz hau hiztegira erantsi',
	'ortho_aucune_suggestion' => 'Ez da aurkitu proposamenik hitz honetarako.',
	'ortho_avis_privacy' => 'SPIP-ean ortografia-zuzentzaile bat txertatu da. Hala ere,funtzionalitate hau aktibatu ondoren, mesedez hurrengo paragrafoa arretaz irakurri :',
	'ortho_avis_privacy2' => 'Testu baten ortografia egiaztatzeko, guneak kontrolatzeko hitzen zerrenda bidaliko du SPIP komunitateko kide desberdinek zure eskura jarritako kanpoko ortografia-zerbitzarietako batera. Hitzak nahastuta bidaltzen dira konfidentzialtasun minimo bat ziurtatzeko. Zure datuengatik beldur bazara, ez aktibatu aukera hau (eta berehala zure datuak kendu Web-atik).',
	'ortho_ce_mot_connu' => 'Hitz hau guneko hiztegiaren parte da.',
	'ortho_description' => '               <div class=\'verdana2\'>SPIP-ean ortografia-zuzentzaile bat txertatu da. Hala ere,funtzionalitate hau aktibatu ondoren, mesedez hurrengo paragrafoa arretaz irakurri :</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Testu baten ortografia egiaztatzeko, guneak kontrolatzeko hitzen zerrenda bidaliko du SPIP komunitateko kide desberdinek zure eskura jarritako kanpoko ortografia-zerbitzarietako batera. Hitzak nahastuta bidaltzen dira konfidentzialtasun minimo bat ziurtatzeko. Zure datuengatik beldur bazara, ez aktibatu aukera hau (eta berehala zure datuak kendu Web-atik).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Ez da aurkitu hiztegirik hizkuntza honentzako',
	'ortho_mode_demploi' => 'Hitz ezezagunak gorrian azpimarratuta daude. Hitz bakoitzean klikatu ahal duzu zuzenketa-proposamenak erakusteko.',
	'ortho_mots_a_corriger' => 'zuzentzeko hitzak',
	'ortho_orthographe' => 'Ortografia',
	'ortho_supprimer_ce_mot' => 'Hitz hau kendu hiztegitik',
	'ortho_trop_de_fautes' => 'Erne : zure testuak akats gehiegi ditu, ez da proposatu zuzenketarik sistema gehiego ez kargatzeko.',
	'ortho_trop_de_fautes2' => 'Akats nabarienak zuzentzen hasi eta saiatu berriro.',
	'ortho_verif_impossible' => 'Sistemak ezin dutestu honetako ortografia egiaztatu.',
	'ortho_verifier' => 'Ortografia zuzendu'
);

?>
