<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=eo
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Aldoni tiun vorton al la vortaro',
	'ortho_aucune_suggestion' => 'Neniu propono estis trovita por tiu vorto.',
	'ortho_avis_privacy' => 'SPIP enhavas ortografian kontrolilon. Tamen, antaŭ ol aktivigi tiun funkcion, atente legu la jenan paragrafon :',
	'ortho_avis_privacy2' => 'Por kontroli la ortografion de teksto, la retejo sendas liston de kontrolotaj vortoj al eksteraj « ortografiaj serviloj », kiujn disponigas al vi SPIP-anoj. Vortoj estas senditaj malorde por estigi minimuman konfidencon. Tamen, se vi timas pri via datenoj, ne aktivigu tiun elekton (kaj ankaŭ forigu tuj viajn datenoj el la reto).',
	'ortho_ce_mot_connu' => 'Tiu vorto troviĝas en la vortaro de la retejo.',
	'ortho_description' => '               <div class=\'verdana2\'>SPIP enhavas ortografian kontrolilon. Tamen, antaŭ ol aktivigi tiun funkcion, atente legu la jenan paragrafon :</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Por kontroli la ortografion de teksto, la retejo sendas liston de kontrolotaj vortoj al eksteraj « ortografiaj serviloj », kiujn disponigas al vi SPIP-anoj. Vortoj estas senditaj malorde por estigi minimuman konfidencon. Tamen, se vi timas pri via datenoj, ne aktivigu tiun elekton (kaj ankaŭ forigu tuj viajn datenoj el la reto).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Neniu vortaro estis trovita por tiu lingvo',
	'ortho_mode_demploi' => 'Ne rekonitaj vortoj estas ruĝe substrekitaj. Vi povas musklaki ĉiun vorton por afiŝi korekto-sugestojn.',
	'ortho_mots_a_corriger' => 'korektendaj vortoj',
	'ortho_orthographe' => 'Ortografio',
	'ortho_supprimer_ce_mot' => 'Forviŝi tiun ĉi vorton el la vortaro',
	'ortho_trop_de_fautes' => 'Atentu : via teksto enhavas tro da eraroj. Neniu korekto estas sugestita por ne superŝuti la sistemon.',
	'ortho_trop_de_fautes2' => 'Bonvolu korekti la plej evidentajn erarojn kaj poste reprovu.',
	'ortho_verif_impossible' => 'La sistemo ne povas kontroli la ortografion de tiu teksto.',
	'ortho_verifier' => 'Kontrolu la ortografion'
);

?>
