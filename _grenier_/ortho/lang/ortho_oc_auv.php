<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=oc_auv
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Ajustar aqueste mot au diccionari',
	'ortho_aucune_suggestion' => 'S\'es pas trobat ges de suggestion per aqueste mot.',
	'ortho_avis_privacy' => 'Un verificador d\'ortografia z-es integrat a SPIP. Pr\'aquò, avans d\'activar aquela foncionalitat, volhatz legir amb atencion lo paragraf seguent:',
	'ortho_avis_privacy2' => 'Per verificar l\'ortografia d\'un tèxt, lo sit mandará la lista daus mots de contrarotlar vèrs un daus "servidors d\'ortografia" extèrnes que los diferents sòcis de la comunitat SPIP los an botats a vòstra disposicion. Se manda los mots dins lo desòrdre per fin de garentir un minim de confidencialitat. Se vos fasètz de pensament per las donadas vòstras, activetz pas aquela opcion (e tiratz sus lo còp vòstras informacions dau web).',
	'ortho_ce_mot_connu' => 'Aqueste mot fai partida dau diccionari dau sit.',
	'ortho_description' => '               <div class=\'verdana2\'>Un verificador d\'ortografia z-es integrat a SPIP. Pr\'aquò, avans d\'activar aquela foncionalitat, volhatz legir amb atencion lo paragraf seguent:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Per verificar l\'ortografia d\'un tèxt, lo sit mandará la lista daus mots de contrarotlar vèrs un daus "servidors d\'ortografia" extèrnes que los diferents sòcis de la comunitat SPIP los an botats a vòstra disposicion. Se manda los mots dins lo desòrdre per fin de garentir un minim de confidencialitat. Se vos fasètz de pensament per las donadas vòstras, activetz pas aquela opcion (e tiratz sus lo còp vòstras informacions dau web).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'S\'es pas trobat ges de diccionari per aquesta lenga',
	'ortho_mode_demploi' => 'Los mots non reconeguts son sobrelinhats de roge. Podètz clicar sus chasque mot per afichar de suggestions de correccion.',
	'ortho_mots_a_corriger' => 'mots de corregir',
	'ortho_orthographe' => 'Ortografia',
	'ortho_supprimer_ce_mot' => 'Tirar aquel mot dau diccionari',
	'ortho_trop_de_fautes' => 'Atencion: vòstre tèxt ten tròp d\'errors, lai se suggerís pas ges de correccion per fin de pas sobrecharjar lo sistèma.',
	'ortho_trop_de_fautes2' => 'Començatz per corregir las errors mai evidentas e tornatz assajar puèi.',
	'ortho_verif_impossible' => 'Lo sistèma pòt pas verificar l\'ortografia d\'aqueste tèxt.',
	'ortho_verifier' => 'Verificar l\'ortografia'
);

?>
