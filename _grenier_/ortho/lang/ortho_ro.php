<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=ro
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Adăugaţi acest cuvânt în dicţionar',
	'ortho_aucune_suggestion' => 'Nici o sugestie nu a fost găsită pentru acest cuvânt.',
	'ortho_avis_privacy' => 'Un verificator de ortografie este integrat în SPIP. Vă rugăm, totuşi, ca înainte să activaţi această funcţie să citiţi paragraful următor :',
	'ortho_avis_privacy2' => 'Pentru verificarea ortografiei unui text, site-ul va trimite lista de cuvinte către unul dintre « server-ele de ortografie » externe pus la dispoziţie de către diferiţii membrii ai comunităţii SPIP. Cuvintele sunt trimise în dezordine în scopul de a asigura un minim de confidenţialitate. Dacă aveţi totuși temeri pentru confidenţialitatea datelor dumneavoastră nu activaţi această opţiune (şi retrageţi-vă imediat toate informaţiile de pe Web).',
	'ortho_ce_mot_connu' => 'Acest cuvânt face parte din dicţionarul site-ului.',
	'ortho_description' => '               <div class=\'verdana2\'>Un verificator de ortografie este integrat în SPIP. Vă rugăm, totuşi, ca înainte să activaţi această funcţie să citiţi paragraful următor :</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Pentru verificarea ortografiei unui text, site-ul va trimite lista de cuvinte către unul dintre « server-ele de ortografie » externe pus la dispoziţie de către diferiţii membrii ai comunităţii SPIP. Cuvintele sunt trimise în dezordine în scopul de a asigura un minim de confidenţialitate. Dacă aveţi totuși temeri pentru confidenţialitatea datelor dumneavoastră nu activaţi această opţiune (şi retrageţi-vă imediat toate informaţiile de pe Web).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Nici un dicţionar nu a fost găsit pentru această limbă',
	'ortho_mode_demploi' => 'Cuvintele care nu au fost recunoscute sunt subliniate în roşu. Puteţi face click pe fiecare cuvânt pentru a afişa sugestiile de corecţie.',
	'ortho_mots_a_corriger' => 'cuvinte de corectat',
	'ortho_orthographe' => 'Ortografie',
	'ortho_supprimer_ce_mot' => 'Ştergeţi acest cuvânt din dicţionar',
	'ortho_trop_de_fautes' => 'Atenţie : textul dumneavoastră conţine prea multe greşeli, nici o corecţie nu a fost sugerată pentru a nu supra-încărca sistemul.',
	'ortho_trop_de_fautes2' => 'Începeţi prin a corecta erorile cele mai evidente şi re-încercaţi după aceea.',
	'ortho_verif_impossible' => 'Sistemul nu poate verifica ortografie acestui text.',
	'ortho_verifier' => 'Verificaţi ortografia'
);

?>
