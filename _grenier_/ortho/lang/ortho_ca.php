<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Afegir aquest mot al diccionari',
	'ortho_aucune_suggestion' => 'No s\'ha trobat cap suggerència per aquesta paraula.',
	'ortho_avis_privacy' => 'La verificació ortogràfica ha estat integrada a l\'SPIP. Abans d\'activar aquesta funcionalitat, si us plau llegeisca el següent paràgraf amb atenció: ',
	'ortho_avis_privacy2' => 'Per verificar l\'ortografia d\'un text, el lloc enviarà la llista de les paraules a controlar cap un dels "servidors ortogràfics" externs posats a la vostra disposició per diferents membres de la comunitat SPIP. Les paraules són enviades desordenades per tal d\'assegurar un mínim de confidencialitat. Si patiu per la confidencialitat de les vostres dades no activeu aquesta opció (i retireu immediatament les vostres informacions del lloc Web).',
	'ortho_ce_mot_connu' => 'Aquesta paraula forma part del diccionari del lloc Web.',
	'ortho_description' => '               <div class=\'verdana2\'>La verificació ortogràfica ha estat integrada a l\'SPIP. Abans d\'activar aquesta funcionalitat, si us plau llegeisca el següent paràgraf amb atenció: </div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Per verificar l\'ortografia d\'un text, el lloc enviarà la llista de les paraules a controlar cap un dels "servidors ortogràfics" externs posats a la vostra disposició per diferents membres de la comunitat SPIP. Les paraules són enviades desordenades per tal d\'assegurar un mínim de confidencialitat. Si patiu per la confidencialitat de les vostres dades no activeu aquesta opció (i retireu immediatament les vostres informacions del lloc Web).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'No ha estat trobat cap diccionari per aquesta llengua',
	'ortho_mode_demploi' => 'Les paraules no reconegudes són subratllades en roig. Podeu clicar a sobre de cada mot per afegir alguna de les paraules suggerides.',
	'ortho_mots_a_corriger' => 'paraules a corregir',
	'ortho_orthographe' => 'Ortografia',
	'ortho_supprimer_ce_mot' => 'Treure aquesta paraula del diccionari',
	'ortho_trop_de_fautes' => 'Atenció: el vostre text conté massa errades, no es suggereix cap correció per tal de no sobrecarregar el sistema.',
	'ortho_trop_de_fautes2' => 'Comenceu per corregir les errades més evidents i torneu-ho a provar.',
	'ortho_verif_impossible' => 'El sistema no pot verificar l\'ortografia d\'aquest text.',
	'ortho_verifier' => 'Verificar l\'ortografia'
);

?>
