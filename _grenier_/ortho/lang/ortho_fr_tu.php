<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=fr_tu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Ajouter ce mot au dictionnaire',
	'ortho_aucune_suggestion' => 'Aucune suggestion n\'a été trouvée pour ce mot.',
	'ortho_avis_privacy' => 'Un vérificateur d\'orthographe est intégré à SPIP. Toutefois, avant d\'activer cette fonctionnalité, lis avec attention le paragraphe suivant :',
	'ortho_avis_privacy2' => 'Pour vérifier l\'orthographe d\'un texte, le site va envoyer la liste des mots à contrôler vers l\'un des « serveurs d\'orthographe » externes mis à votre disposition par différents membres de la communauté SPIP. Les mots sont envoyés dans le désordre afin d\'assurer un minimum de confidentialité. Si tu as des craintes pour tes données, n\'active pas cette option (et retire tout de suite tes informations du Web).',
	'ortho_ce_mot_connu' => 'Ce mot fait partie du dictionnaire du site.',
	'ortho_description' => '               <div class=\'verdana2\'>Un vérificateur d\'orthographe est intégré à SPIP. Toutefois, avant d\'activer cette fonctionnalité, lis avec attention le paragraphe suivant :</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Pour vérifier l\'orthographe d\'un texte, le site va envoyer la liste des mots à contrôler vers l\'un des « serveurs d\'orthographe » externes mis à votre disposition par différents membres de la communauté SPIP. Les mots sont envoyés dans le désordre afin d\'assurer un minimum de confidentialité. Si tu as des craintes pour tes données, n\'active pas cette option (et retire tout de suite tes informations du Web).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Aucun dictionnaire n\'a été trouvé pour cette langue',
	'ortho_mode_demploi' => 'Les mots non reconnus sont surlignés en rouge. Tu peux cliquer sur chaque mot pour afficher des suggestions de correction.',
	'ortho_mots_a_corriger' => 'mots à corriger',
	'ortho_orthographe' => 'Orthographe',
	'ortho_supprimer_ce_mot' => 'Enlever ce mot du dictionnaire',
	'ortho_trop_de_fautes' => 'Attention : ton texte contient trop de fautes, aucune correction n\'est suggérée afin de ne pas surcharger le système.',
	'ortho_trop_de_fautes2' => 'Commence par corriger les fautes les plus évidentes et réessaye ensuite.',
	'ortho_verif_impossible' => 'Le système ne peut pas vérifier l\'orthographe de ce texte.',
	'ortho_verifier' => 'Vérifier l\'orthographe'
);

?>
