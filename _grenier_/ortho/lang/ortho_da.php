<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_description' => '               <div class=\'verdana2\'>Spip contains a spell checker. However, before enabling it, please read the following paragraph carefully:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               In order to check spelling  a text, the site will send the list of words to be checked to an external "spelling server", which people of the Spip community have made available. The words are mixed up before being sent in order to maintain some level of confidentiality. If you are concerned about your data, do not activate this option (and withdraw all your information from the web at once).
               </p></blockquote></div>', # MODI
);

?>
