<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=tr
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Bu sözcüğü sözlüğe ekle',
	'ortho_aucune_suggestion' => 'Bu sözcük için hiç öneri bulunamadı.',
	'ortho_avis_privacy' => 'SPIP içerisinde bir imlâ kontrolü mevcuttur. Ancak, bu işlevi çalıştırmadan önce lütfen bir sonraki paragrafı dikkatlice okuyunuz :',
	'ortho_avis_privacy2' => 'Bir metnin imlâsını denetlemek için, site, denetlenecek sözcüklerin listesini SPIP topluluğunun çeşitli üyeleri  tarafından kullanımınıza açılan, dışarıdaki « yazım sunucuları »\'ndan birine gönderecektir. Kelimeler minimum gizlilik sağlamak üzere düzensiz gönderilmektedir.  Verileriniz için endişe ediyorsanız, bu işlevi çalıştırmayınız (ve bilgilerinizi derhal Web\'den geri çekiniz).',
	'ortho_ce_mot_connu' => 'Bu sözcük sitenin sözlüğünde mevcuttur. ',
	'ortho_description' => '               <div class=\'verdana2\'>SPIP içerisinde bir imlâ kontrolü mevcuttur. Ancak, bu işlevi çalıştırmadan önce lütfen bir sonraki paragrafı dikkatlice okuyunuz :</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Bir metnin imlâsını denetlemek için, site, denetlenecek sözcüklerin listesini SPIP topluluğunun çeşitli üyeleri  tarafından kullanımınıza açılan, dışarıdaki « yazım sunucuları »\'ndan birine gönderecektir. Kelimeler minimum gizlilik sağlamak üzere düzensiz gönderilmektedir.  Verileriniz için endişe ediyorsanız, bu işlevi çalıştırmayınız (ve bilgilerinizi derhal Web\'den geri çekiniz).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Bu dilde herhangi bir sözlük bulunamamıştır.',
	'ortho_mode_demploi' => 'Tanınmayan sözcüklerin üstü kırmızı ile çizilmiştir.  Her bir sözcüğe düzeltme önerileri almak üzere tıklayabilirsiniz. ',
	'ortho_mots_a_corriger' => 'Düzeltilecek sözcükler ',
	'ortho_orthographe' => 'Yazım',
	'ortho_supprimer_ce_mot' => 'Bu sözcüğü sözlükten çıkart',
	'ortho_trop_de_fautes' => 'Dikkat : metninizde çok fazla hata var sisteme aşırı yüklenilmemesi için hiç bir öneri getirilmemiiştir.',
	'ortho_trop_de_fautes2' => 'Önce bariz hataları düzeltiniz sonra tekrar deneyiniz. ',
	'ortho_verif_impossible' => 'Sistem, bu metnin imlâsını denetleyemiyor. ',
	'ortho_verifier' => 'Yazımı kontrol et'
);

?>
