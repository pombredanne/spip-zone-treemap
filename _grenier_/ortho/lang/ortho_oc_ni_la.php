<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=oc_ni_la
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Apondre aquest mot au diccionari',
	'ortho_aucune_suggestion' => 'Non s\'es trobat minga de suggestion per aquest mot.',
	'ortho_avis_privacy' => 'Un verificador d\'ortografia es integrat a SPIP. Totun, avans d\'activar aquela foncionalitat, vorgatz legir emb atencion lo paragraf seguent:',
	'ortho_avis_privacy2' => 'Per verificar l\'ortografia d\'un tèxt, lo sit mandarà la lista dei mots de contrarotlar vèrs un dei "servidors d\'ortografia" extèrnes que lu diferents sòcis de la comunitat SPIP lu an botats a la vòstra disposicion. Si manda lu mots dins lo desòrdre per fin de garentir un minim de confidencialitat. Se vos fètz de lagui per li donadas vòstri, non activetz aquela opcion (e levatz sus lo còup li vòstri informacions dau web).',
	'ortho_ce_mot_connu' => 'Aquest mot fa partida dau diccionari dau sit.',
	'ortho_description' => '               <div class=\'verdana2\'>Un verificador d\'ortografia es integrat a SPIP. Totun, avans d\'activar aquela foncionalitat, vorgatz legir emb atencion lo paragraf seguent:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Per verificar l\'ortografia d\'un tèxt, lo sit mandarà la lista dei mots de contrarotlar vèrs un dei "servidors d\'ortografia" extèrnes que lu diferents sòcis de la comunitat SPIP lu an botats a la vòstra disposicion. Si manda lu mots dins lo desòrdre per fin de garentir un minim de confidencialitat. Se vos fètz de lagui per li donadas vòstri, non activetz aquela opcion (e levatz sus lo còup li vòstri informacions dau web).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Non s\'es trobat minga de diccionari per aquesta lenga',
	'ortho_mode_demploi' => 'Lu mots non reconeguts son sobrelinhats de roge. Podètz clicar sus cada mot per afichar de suggestions de correccion.',
	'ortho_mots_a_corriger' => 'mots de corregir',
	'ortho_orthographe' => 'Ortografia',
	'ortho_supprimer_ce_mot' => 'Levar aqueu mot dau diccionari',
	'ortho_trop_de_fautes' => 'Atencion: lo vòstre tèxt ten tròup d\'errors, i se suggerisse minga de correccion per fin de non sobrecargar lo sistèma.',
	'ortho_trop_de_fautes2' => 'Començatz per corregir lu errors mai evidents e tornatz assaiar pi.',
	'ortho_verif_impossible' => 'Lo sistèma non pòu verificar l\'ortografia d\'aquest tèxt.',
	'ortho_verifier' => 'Verificar l\'ortografia'
);

?>
