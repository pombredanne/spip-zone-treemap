<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=bs
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Dodaj rijec u rijecnik',
	'ortho_aucune_suggestion' => 'Nije pronadjena ni jedna sugestija za ovu rijec.',
	'ortho_avis_privacy' => 'Provjera pravopisa je integrisana u SPIP. Svaki put kad aktivirate ovu funkciju, procitajte sljedeci odlomak:',
	'ortho_avis_privacy2' => 'Da bi se provjerio pravopis jednog teksta, ova stranica ce poslati listu rijeci na jedan od externih "pravopisnih servera", koji su vam pruzeni na  raspolaganje od strane raznih clanova SPIP komune. Rijeci ce  biti poslane u pogresnom rasporedu da bi vam osigurali minimum  sigurnosti. Ako su vasi podaci povjerljivi, ne aktivirajte ovu opciju (i odmah povucite vase informacije sa Web-a).',
	'ortho_ce_mot_connu' => 'Ova rijec je dio rijecnika stranice.',
	'ortho_description' => '               <div class=\'verdana2\'>Provjera pravopisa je integrisana u SPIP. Svaki put kad aktivirate ovu funkciju, procitajte sljedeci odlomak:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Da bi se provjerio pravopis jednog teksta, ova stranica ce poslati listu rijeci na jedan od externih "pravopisnih servera", koji su vam pruzeni na  raspolaganje od strane raznih clanova SPIP komune. Rijeci ce  biti poslane u pogresnom rasporedu da bi vam osigurali minimum  sigurnosti. Ako su vasi podaci povjerljivi, ne aktivirajte ovu opciju (i odmah povucite vase informacije sa Web-a).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Nije pronadjen ni jedan rijecnik za ovaj jezik',
	'ortho_mode_demploi' => 'Rijeci koje nisu prepoznate, podvucene  su crvenom bojom. Mozete kliknuti na svaku rijec i izabrati  neku od prijedloga za ispravku.',
	'ortho_mots_a_corriger' => 'rijeci za ispraviti',
	'ortho_orthographe' => 'Pravopis',
	'ortho_supprimer_ce_mot' => 'Odstrani ovu rijec iz rijecnika',
	'ortho_trop_de_fautes' => 'Paznja: vas tekst sadrzi previse gresaka. Nijedna ispravka nije predlozena da bi se sprijecilo preopterecenje sistema.',
	'ortho_trop_de_fautes2' => 'Pocnite sa ispravljanjem ociglednih gresaka, zatim pokusajte ponovo.',
	'ortho_verif_impossible' => 'Sistem ne moze provjeriti pravopis ovog teksta.',
	'ortho_verifier' => 'Provjera pravopisa'
);

?>
