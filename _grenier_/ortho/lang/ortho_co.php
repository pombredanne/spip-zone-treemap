<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=co
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Aghjunghje sta parulla à u dizziunariu',
	'ortho_aucune_suggestion' => 'Nisun suggerimentu per sta parulla.',
	'ortho_avis_privacy' => 'SPIP hà un cuntrollu urtugràficu integratu. Ma prima di attivà sta funziunalità, leghjite puru cù assai cura ciò chì seguita :',
	'ortho_avis_privacy2' => 'Per verificà l\'urtugrafia di un testu, u situ manderà a lista di parulle da cuntrullà à unu frà i « servori 
	urtugràfichi » esterni messi à dispusizione vostra da a cumunità SPIP. E parulle seranu mandate in disòrdine, da rispettà 
	a pruprietà intellettuale di l\'autori. Sè vo vi fate u penseru per i vostri dati, ùn attivate micca tale funziunalità (è sguassate li puru sùbbitu 
	da u web).',
	'ortho_ce_mot_connu' => 'Sta parulla face parte di u dizziunariu di u situ.',
	'ortho_description' => '               <div class=\'verdana2\'>SPIP hà un cuntrollu urtugràficu integratu. Ma prima di attivà sta funziunalità, leghjite puru cù assai cura ciò chì seguita :</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Per verificà l\'urtugrafia di un testu, u situ manderà a lista di parulle da cuntrullà à unu frà i « servori 
	urtugràfichi » esterni messi à dispusizione vostra da a cumunità SPIP. E parulle seranu mandate in disòrdine, da rispettà 
	a pruprietà intellettuale di l\'autori. Sè vo vi fate u penseru per i vostri dati, ùn attivate micca tale funziunalità (è sguassate li puru sùbbitu 
	da u web).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Nisun dizziunariu dispunìbule per sta lingua',
	'ortho_mode_demploi' => 'E parulle scunnisciute sò sottufrisgiate di rossu. Pudete puru cliccà nant\'à ogni parulla per vede suggerimenti di currezzione.',
	'ortho_mots_a_corriger' => 'parulle da curregge',
	'ortho_orthographe' => 'Urtugrafia',
	'ortho_supprimer_ce_mot' => 'Sguassà sta parulla da u dizziunariu',
	'ortho_trop_de_fautes' => 'Attenti : U testu cuntene troppu sbagli, ùn hè suggerita nisuna currezzione da ùn troppu caricà u sistema.',
	'ortho_trop_de_fautes2' => 'Principià pè curregge i sbagli i più grossi è turnà à pruvà dopu.',
	'ortho_verif_impossible' => 'U sistema ùn pò micca cuntrullà l\'urtugrafia di stu testu.',
	'ortho_verifier' => 'Cuntrullà l\'urtugrafia'
);

?>
