<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=oc_ni
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Ajustar aqueste mòt au diccionari',
	'ortho_aucune_suggestion' => 'S\'es trobat minga de suggestion per aquèu mot.',
	'ortho_avis_privacy' => 'Un verificador d\'ortografia es integrat à SPIP. Pr\'aquò, avant d\'activar aquela foncionalitat, vorgatz legir embé atencion lo paragraf seguent :',
	'ortho_avis_privacy2' => 'Per verificar l\'ortografia d\'un tèxte, lo site mandarà la tièra dei mòts de controlar vèrs un dei « servidors d\'orthografia » extèrnes que lu diferents sòcis de la comunitat SPIP an mes à la voastra disposicion. Si manda lu mòts dins lo desòrdre per fin d\'assegurar un mìnimo de confidencialitat. S\'avetz paur per li voastri donadas, activetz pas aquela opcion (e levatz sus lo còup li voastri informacions dau web).',
	'ortho_ce_mot_connu' => 'Aquèu mot fa partida dau diccionari dau sit.',
	'ortho_description' => '               <div class=\'verdana2\'>Un verificador d\'ortografia es integrat à SPIP. Pr\'aquò, avant d\'activar aquela foncionalitat, vorgatz legir embé atencion lo paragraf seguent :</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Per verificar l\'ortografia d\'un tèxte, lo site mandarà la tièra dei mòts de controlar vèrs un dei « servidors d\'orthografia » extèrnes que lu diferents sòcis de la comunitat SPIP an mes à la voastra disposicion. Si manda lu mòts dins lo desòrdre per fin d\'assegurar un mìnimo de confidencialitat. S\'avetz paur per li voastri donadas, activetz pas aquela opcion (e levatz sus lo còup li voastri informacions dau web).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Minga dicionari es estat trobat per aquela lenga',
	'ortho_mode_demploi' => 'Lu mòts non reconoissuts son subrelinhats de roge. Podètz clicar sus cada mòt per afichar de suggestions de correccion.',
	'ortho_mots_a_corriger' => 'mòts da corregir',
	'ortho_orthographe' => 'Ortografia',
	'ortho_supprimer_ce_mot' => 'Levar aquèu mòt dau diccionari',
	'ortho_trop_de_fautes' => 'Atencion : lo voastre tèxte ten tròp d\'errors, e se sugerís ges de correccion per fin de pas subrecargar lo sistèma.',
	'ortho_trop_de_fautes2' => 'Començatz per corregir li errors mai evidenti e tornatz assajar puèi.',
	'ortho_verif_impossible' => 'Lo sistèma pòut pas verificar l\'ortografia d\'aquèu tèxte.',
	'ortho_verifier' => 'Verificar l\'ortografia'
);

?>
