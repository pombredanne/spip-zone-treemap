<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Ouzhpennañ ar ger-mañ d\'ar geriadur',
	'ortho_aucune_suggestion' => 'N\'eus bet kavet kinnig ebet evit ar ger-mañ.',
	'ortho_avis_privacy' => 'Un difazier reizhskrivañ zo e SPIP. A-raok ober gant ar servij-mañ, lennit gant aked ar rannbennad da-heul, mar plij :',
	'ortho_avis_privacy2' => 'A-benn gwiriañ reizhskrivadur un destenn e kaso SPIP ar roll gerioù da wiriañ da « servijerioù reizhskrivañ » diavaez, kinniget deoc\'h gant tud eus kumuniezh SPIP. En dizurzh e vez kaset ar gerioù, evit ma ne vefe ket komprenet ar pezh zo en ho pennad. M\'hoc\'h eus aon evit ho titouroù, na rit ket gant ar servij-mañ (ha diverkit diouzhtu ho titouroù diouzh ar rouedad).',
	'ortho_ce_mot_connu' => 'E geriadur al lec\'hienn emañ ar ger-mañ.',
	'ortho_description' => '               <div class=\'verdana2\'>Un difazier reizhskrivañ zo e SPIP. A-raok ober gant ar servij-mañ, lennit gant aked ar rannbennad da-heul, mar plij :</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               A-benn gwiriañ reizhskrivadur un destenn e kaso SPIP ar roll gerioù da wiriañ da « servijerioù reizhskrivañ » diavaez, kinniget deoc\'h gant tud eus kumuniezh SPIP. En dizurzh e vez kaset ar gerioù, evit ma ne vefe ket komprenet ar pezh zo en ho pennad. M\'hoc\'h eus aon evit ho titouroù, na rit ket gant ar servij-mañ (ha diverkit diouzhtu ho titouroù diouzh ar rouedad).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'N\'eus bet kavet geriadur ebet evit ar yezh-mañ',
	'ortho_mode_demploi' => 'E ruz eo bet uslinennet ar gerioù n\'int ket bet anavezet. Klikañ war pep ger a c\'hallit ober evit lenn kinnigoù reizhañ.',
	'ortho_mots_a_corriger' => 'gerioù da reizhañ',
	'ortho_orthographe' => 'Reizhskrivañ',
	'ortho_supprimer_ce_mot' => 'Tennañ ar ger-mañ eus ar geriadur',
	'ortho_trop_de_fautes' => 'Diwallit : re a fazioù zo en ho testenn. N\'eus bet kinniget reizhadenn ebet kuit da soulgargañ ar reizhiad.',
	'ortho_trop_de_fautes2' => 'Klaskit reizhañ ar fazioù brasañ ha klaskit en-dro da c\'houde.',
	'ortho_verif_impossible' => 'N\'hall ket ar reizhiad gwiriañ reizhskrivadur an destenn-mañ.',
	'ortho_verifier' => 'Gwiriañ ar reizhskrivañ'
);

?>
