<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=oc_gsc
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Ajustar aqueste mot au diccionari',
	'ortho_aucune_suggestion' => 'Non s\'ei pas trobat nada suggestion per aqueste mot.',
	'ortho_avis_privacy' => 'Un verificader d\'ortografia qu\'ei integrat a SPIP. Totun, abans d\'activar aquera foncionalitat, volhatz legir dab atencion lo paragraf seguent:',
	'ortho_avis_privacy2' => 'Entà verificar l\'ortografia d\'un tèxt, lo sit que mandarà la lista deus mots de contrarotlar vèrs un deus "serviders d\'ortografia" extèrnes que los diferents sòcis de la comunitat SPIP e\'us an botats a la vòsta disposicion. Que\'s manda los mots dens lo desòrdre per fin de garentir un minim de confidencialitat. Se\'vs hètz  lagui entà las dadas vòstas, n\'activetz pas aquera opcion (e tiratz suu pic las vòstas informacions deu web).',
	'ortho_ce_mot_connu' => 'Aqueste mot que hè partida deu diccionari deu sit.',
	'ortho_description' => '               <div class=\'verdana2\'>Un verificader d\'ortografia qu\'ei integrat a SPIP. Totun, abans d\'activar aquera foncionalitat, volhatz legir dab atencion lo paragraf seguent:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Entà verificar l\'ortografia d\'un tèxt, lo sit que mandarà la lista deus mots de contrarotlar vèrs un deus "serviders d\'ortografia" extèrnes que los diferents sòcis de la comunitat SPIP e\'us an botats a la vòsta disposicion. Que\'s manda los mots dens lo desòrdre per fin de garentir un minim de confidencialitat. Se\'vs hètz  lagui entà las dadas vòstas, n\'activetz pas aquera opcion (e tiratz suu pic las vòstas informacions deu web).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Non s\'ei pas trobat nat diccionari per aquesta lenga',
	'ortho_mode_demploi' => 'Los mots non reconeguts que son suberlinhats de roge. Que podetz clicar sus cada mot entà afichar suggestions de correccion.',
	'ortho_mots_a_corriger' => 'mots de corregir',
	'ortho_orthographe' => 'Ortografia',
	'ortho_supprimer_ce_mot' => 'Tirar aqueth mot deu diccionari',
	'ortho_trop_de_fautes' => 'Atencion: lo vòste tèxt que tien tròp d\'errors, non s\'i suggereish pas nada correccion per fin de non subercargar pas lo sistèma.',
	'ortho_trop_de_fautes2' => 'Començatz per corregir las errors mei evidentas e tornatz ensajar puish.',
	'ortho_verif_impossible' => 'Lo sistèma non pòt pas verificar l\'ortografia d\'aqueste tèxt.',
	'ortho_verifier' => 'Verificar l\'ortografia'
);

?>
