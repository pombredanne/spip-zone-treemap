<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Aggiungere questa parola al dizionario',
	'ortho_aucune_suggestion' => 'Nessun suggerimento per questa parola.',
	'ortho_avis_privacy' => 'SPIP ha un controllo ortografico integrato. Prima di attivare questa funzionalità, leggere attentamente quanto segue:',
	'ortho_avis_privacy2' => 'Per verificare l\'ortografia di un testo, il sito invierà la lista di parole da controllare a uno dei "server ortografici" esterni messi a disposizione al pubblico dai membri della comunità SPIP. Le parole sono inviate in ordine casuale per rispettare la proprietà intellettuale degli autori. È consigliabile disattivare tale funzionalità se i testi trattati nel sito sono ritenuti troppo confidenziali (e magari si eviti di pubblicarli sul web...).',
	'ortho_ce_mot_connu' => 'Questa parola fa parte del dizionario del sito.',
	'ortho_description' => '               <div class=\'verdana2\'>SPIP ha un controllo ortografico integrato. Prima di attivare questa funzionalità, leggere attentamente quanto segue:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Per verificare l\'ortografia di un testo, il sito invierà la lista di parole da controllare a uno dei "server ortografici" esterni messi a disposizione al pubblico dai membri della comunità SPIP. Le parole sono inviate in ordine casuale per rispettare la proprietà intellettuale degli autori. È consigliabile disattivare tale funzionalità se i testi trattati nel sito sono ritenuti troppo confidenziali (e magari si eviti di pubblicarli sul web...).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Nessun dizionario disponibile per questa lingua',
	'ortho_mode_demploi' => 'Le parole non riconosciute sono sottolineate in rosso. È possibile cliccare su ogni parola per visualizzare eventuali suggerimenti per la correzione.',
	'ortho_mots_a_corriger' => 'parole da correggere',
	'ortho_orthographe' => 'Ortografia',
	'ortho_supprimer_ce_mot' => 'Eliminare questa parola dal dizionario',
	'ortho_trop_de_fautes' => 'Attenzione: il testo contiene troppi errori, non è stato fornito alcun suggerimento di correzione per evitare di sovraccaricare il sistema.',
	'ortho_trop_de_fautes2' => 'Correggere gli errori più evidenti e ricontrollare il testo in un secondo tempo.',
	'ortho_verif_impossible' => 'Il sistema non può controllare l\'ortografia di questo testo.',
	'ortho_verifier' => 'Controllare l\'ortografia'
);

?>
