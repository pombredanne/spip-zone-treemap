<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=id
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'Tambahkan kata ini ke dalam kamus',
	'ortho_aucune_suggestion' => 'Tidak ada saran yang ditemukan untuk kata ini.',
	'ortho_avis_privacy' => 'SPIP memiliki pemeriksa ejaan. Walaupun demikian, sebelum mengaktifkannya, silakan baca paragraf berikut baik-baik:',
	'ortho_avis_privacy2' => 'Untuk memerikas ejaan sebuah teks, situs akan mengirimkan daftar kata-kata yang akan diperiksa ke server pengeja eksternal, yang telah disediakan oleh komunitas SPIP. Kata-kata akan dicampur sebelum dikirim guna menjaga tingkat kerahasiaan. Jika anda khawatir dengan data anda, jangan aktifkan opsi ini (dan tarik semua informasi anda dari web sekaligus).',
	'ortho_ce_mot_connu' => 'Kata ini terdapat di dalam kamus situs.',
	'ortho_description' => '               <div class=\'verdana2\'>SPIP memiliki pemeriksa ejaan. Walaupun demikian, sebelum mengaktifkannya, silakan baca paragraf berikut baik-baik:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               Untuk memerikas ejaan sebuah teks, situs akan mengirimkan daftar kata-kata yang akan diperiksa ke server pengeja eksternal, yang telah disediakan oleh komunitas SPIP. Kata-kata akan dicampur sebelum dikirim guna menjaga tingkat kerahasiaan. Jika anda khawatir dengan data anda, jangan aktifkan opsi ini (dan tarik semua informasi anda dari web sekaligus).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'Tidak ada kamus ditemukan untuk bahasa ini',
	'ortho_mode_demploi' => 'Kata-kata yang tidak dikenal diberi tanda merah. Klik pada kata-kata tersebut untuk melihat koreksi yang disarankan.',
	'ortho_mots_a_corriger' => 'kata yang akan dikoreksi',
	'ortho_orthographe' => 'Ejaan',
	'ortho_supprimer_ce_mot' => 'Hapus kata ini dari kamus',
	'ortho_trop_de_fautes' => 'Teks anda mengandung banyak kesalahan! Agar sistem tidak terberati oleh kesalahan tersebut, tidak ada koreksi yang diberikan.',
	'ortho_trop_de_fautes2' => 'Mulai dari mengkoreksi kesalahan-kesalahan yang paling kentara, kemudian coba lagi.',
	'ortho_verif_impossible' => 'Sistem tidak dapat memeriksa ejaan dari teks ini.',
	'ortho_verifier' => 'Pemeriksaan ejaan'
);

?>
