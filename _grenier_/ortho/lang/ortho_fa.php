<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ortho?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'ortho_ajouter_ce_mot' => 'اين واژه را به فرهنگ واژه ها اضافه نماييد',
	'ortho_aucune_suggestion' => 'هيچ موردى براى اين واژه پيدا نشد',
	'ortho_avis_privacy' => 'اسپيپ داراى يك تصحيح كننده ى املا ميباشد. با وجود اين پيش از فعال كردن آن بند زير را با دقت بخوانيد:',
	'ortho_avis_privacy2' => 'براى بازبينى املاى يك متن, سايت فهرستى از واژه ها را به يكى از سرورهاى خارجى ويژه ى املا كه توسط اعضاى اسپيپ در اختيارتان گذاشته شده است ميفرستد. براى حفظ داده ها واژه ها بصورت نامنظم فرستاده ميشوند. اگر نگران امنيت داده هايتان هستيد ميتوانيد اين گزينش را غير فعال كنيد (و اطلاعاتتان را فورى از تارنما خارج نماييد).',
	'ortho_ce_mot_connu' => 'اين واژه در فرهنگ  واژه هاى سايت موجود است',
	'ortho_description' => '               <div class=\'verdana2\'>اسپيپ داراى يك تصحيح كننده ى املا ميباشد. با وجود اين پيش از فعال كردن آن بند زير را با دقت بخوانيد:</div>
               <div class=\'verdana2\'><blockquote class=\'spip\'><p>
               براى بازبينى املاى يك متن, سايت فهرستى از واژه ها را به يكى از سرورهاى خارجى ويژه ى املا كه توسط اعضاى اسپيپ در اختيارتان گذاشته شده است ميفرستد. براى حفظ داده ها واژه ها بصورت نامنظم فرستاده ميشوند. اگر نگران امنيت داده هايتان هستيد ميتوانيد اين گزينش را غير فعال كنيد (و اطلاعاتتان را فورى از تارنما خارج نماييد).
               </p></blockquote></div>',
	'ortho_dico_absent' => 'براى اين زبان فرهنگ واژه ها موجود نميباشد',
	'ortho_mode_demploi' => 'واژه هاى ناشناخته با رنگ قرمز مشخص شده اند شما ميتوانيد با كليك كردن روى آنها گزينه هاى ديگر را بيابيد',
	'ortho_mots_a_corriger' => 'اين واژه بايد تصحيح شود',
	'ortho_orthographe' => 'املا',
	'ortho_supprimer_ce_mot' => 'اين واژه را از فرهنگ برداريد',
	'ortho_trop_de_fautes' => 'توجه: متنتان حاوى اشتباهات بسياريست, براى اشغال نكردن سيستم هيچگونه تصحيحى صورت نميگيرد. ',
	'ortho_trop_de_fautes2' => 'با تصحيح غلط هاى آشكار آغاز كنيد و سپس دوباره تكرار كنيد. ',
	'ortho_verif_impossible' => 'سيستم قادر به بازبينى املا اين متن نيست.',
	'ortho_verifier' => ' بازبينى املا '
);

?>
