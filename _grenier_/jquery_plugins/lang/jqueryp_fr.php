<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'bouton_mettre_a_jour' => 'Mettre &agrave; jour',
	
	'decriptif_choix_plugins_defaut' => 
		'Cette page permet de choisir 
		les extensions de jquery
		charg&eacute;es par d&eacute;faut pour chaque page de SPIP (public & priv&eacute;)',
		
	'decriptif_installer_plugins' => 
		'Cette page permet d\'installer des plugins jquery
		dans le dossier /lib de votre SPIP',
	
	'donnees_plugin_introuvables' => 'Impossible de trouver les donnees du plugin !',
	
	'erreur' => 'Erreur',
	
	'fichiers_installes' => 'Les fichiers suivants ont &eacute;t&eacute; install&eacute;s :',
	
	'installation_librairies' => 'Installation de librairies',
	
	'jqueryp' => 'Activer des plugins JQuery',
	'jquery_plugins' => 'jQuery Plugins',
	'jqueryp_installer_plugins' => 'Installer ou mettre &agrave; jour des plugins JQuery',
		
	'liste_plugins' => 'Liste des plugins',
	
	'ok' => 'OK',
	
	'telechargement_librairie' => 'T&eacute;l&eacute;chargement de la librairie @nom@.',
	
	
	// les librairies
	'autocomplete' => 'AutoComplete',
	'autocomplete_description' => 'Completion automatique',
	
	'datepicker' => 'DatePicker',
	'datepicker_description' => 'Une pipette pour les dates',
	
	'easing' => 'Easing',
	'easing_description' => ' ',
	
	'farbtastic' => 'Farbtastic',
	'farbtastic_description' => 'Une pipette pour les couleurs',
		
	'syncheight' => 'SyncHeight',
	'syncheight_description' => 'Harmoniser la hauteur de colonnes',
	
	'ui' => 'UI : User Interface',
	'ui_description' => ' ',
	
	'validate' => 'Validate',
	'validate_description' => 'V&eacute;rifier les champs d\'un formulaire',
	
	
	'yav' => 'Yav',
	'yav_description' => 'V&eacute;rifier les champs d\'un formulaire',	
);


?>
