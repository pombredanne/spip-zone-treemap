<?php

	$GLOBALS[$GLOBALS['idx_lang']] = array(
		'manage_points' => 'Manage points on the map',
		'manage_points_title' => 'click here to manage the points on the map',
		'points_on_image' => 'Points on the map',
		'import_csv' => 'Import a csv file',
		'export_csv' => 'Export a csv file',
		'add_point' => 'Add a point',
		'delete_points' => 'Delete all points',
		'actions' => 'Actions',
		'modify_point' => 'Modify the annotation',
		'delete_point' => 'Delete the annotation',
		'no_data_points' => 'No point on the image',
		'click_point' => 'Please click on the image to select a point',
		'coordinates' => 'Coordinates',
		'save_point' => 'Save this point',
		'select_fields' => 'Select the fields to add',
		'import' => 'Import',
		'preview_csv' => 'Preview of the csv file',
		'click_select_csv' => 'Click on the icons to select a csv file',
		'download' => 'Download',
		'no_data_csv' => 'No data in the csv file',
		'image' => 'Image',
		'info_no_coord' => 'No coordinate. Please select an image from the previous menu and click on a point'		
);

?>
