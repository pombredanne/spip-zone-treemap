<?php

	$GLOBALS[$GLOBALS['idx_lang']] = array(
		'manage_points' => 'Gestisci i punti sulla mappa',
		'manage_points_title' => 'clicca qui per gestire i punti sulla mappa',
		'points_on_image' => 'Punti sull\'immagine',
		'import_csv' => 'Importa un file csv',
		'export_csv' => 'Esporta un file csv',
		'add_point' => 'Aggiungi un punto',
		'delete_points' => 'Cancella tutti i punti',
		'actions' => 'Azioni',
		'modify_point' => 'Modifica l\'annotazione',
		'delete_point' => 'Cancella l\'annotazione',
		'no_data_points' => 'Nessun punto sull\'immagine',
		'click_point' => 'Clicca sull\'immagine per selezionare un punto',
		'coordinates' => 'Coordinate',
		'save_point' => 'Salva questo punto',
		'select_fields' => 'Scegli i campi da aggiungere',
		'import' => 'Importa',
		'preview_csv' => 'Anteprima del file csv',
		'click_select_csv' => 'Clicca sulle icone per selezionare un file csv',
		'download' => 'Scarica',
		'no_data_csv' => 'Nessun dato nel file csv',
		'image' => 'Immagine',
		'info_no_coord' => 'Nessuna coordinata. Seleziona un\'immagine dal men&ugrave; precedente e clicca su un punto'
);

?>
