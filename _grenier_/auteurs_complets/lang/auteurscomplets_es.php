<?php

     /**
     * SPIP-Auteurs Complets
     *
     * Copyright (c) 2006-2007
     * Quentin Drouet
     *
     * Ce programme est un logiciel libre distribue sous licence GNU/GPL.
     * Pour plus de details voir le fichier COPYING.txt.
     *
     **/

$GLOBALS[$GLOBALS['idx_lang']] = array(
'entree_nom_famille'  => 'Su apellido',
'entree_nom_' => 'Apellido',
'entree_prenom'  => 'Su nombre',
'entree_prenom_'  => 'Nombre',
'entree_organisation'  => 'Su organizaci&oacute;n/ empresa / asociaci&oacute;n',
'entree_url_organisation'  => 'URL de su organizaci&oacute;n/ empresa / asociaci&oacute;n',
'entree_telephone'  => 'Su n&uacute;mero de tel&eacute;fono',
'entree_fax'  => 'Su n&uacute;mero de fax',
'entree_skype'  => 'Su usuario Skype',
'entree_adresse'  => 'Su  direcci&oacute;n',
'entree_codepostal'  => 'Su  c&oacute;digo postal',
'entree_ville'  => 'Su ciudad',
'entree_pays'  => 'Su pa&iacute;s',
'entree_latitude'  => 'Su latitud (en valor num&eacute;rico ex: 92.8)',
'entree_longitude'  => 'Su longitud (en valor num&eacute;rico ex: 15.324)',
'infos_supp'  => 'Informaciones suplementarias',
'coordonnees_sup'  => 'COORDENADAS SUPLEMENTARIAS',
'affiche_organisation'  => 'Organizaci&oacute;n : ',
'affiche_telephone'  => 'Tel&eacute;fono : ',
'affiche_fax'  => 'Fax : ',
'affiche_skype'  => 'Contacto Skype : ',
'affiche_coordonnees_geo'  => 'Coordenadas Geogr&aacute;ficas',
'affiche_latitude'  => 'Latitud : ',
'affiche_longitude'  => 'Longitud : ',
'affiche_adresse'  => 'Direcci&oacute;n',
'paramprofil'  => 'Modificar su perfil',
'localisation'  => 'Ubicaci&oacute;n',

// Pour le modele id_card
'id_card'  => 'Carnet de Identidad :',
'lat'  => 'Lat :',
'long'  => 'Long :',
'contacts'  => 'Contactos :',
'organisation'  => 'Organizaci&oacute;n / Empresa / Asociaci&oacute;n :',
'email' => 'Email :',
'tel'  => 'Tel :',
'fax'  => 'Fax :',
'website'  => 'Sitio WEB :',
'vos_articles'  => 'Sus  Art&iacute;culos :',

// Pour le formulaire profil
'error_bad_user' => 'Ud no corresponde a &eacute;ste autor, por lo tanto Ud. no lo puede modificar...',
'votre_profil' => 'Su perfil',

// Pour le formulaire des listes
'pass_recevoir_mail'=>'La direcci&oacute;n email ingresada existe en la base.<br /><br /> Recibir&aacute; un email indic&aacute;ndole como modificar su suscripci&oacute;n. ',
'abonnement_mail_passcookie' => '(Mensaje Automatico)
Para modificar su suscripci&oacute;n al newsletter del sitio :
@nom_site_spip@ (@adresse_site@)

Por favor dirijase a la siguiente direcci&oacute;n:

    @adresse_site@/spip.php?page=abonnement_ac&d=@cookie@

donde podr&aacute; confirmar la  modificaci&oacute;n de su suscripci&oacute;n.',

'Z' => 'ZZzZZzzz'

);

?>
