<?php
     /**
     * SPIP-Auteurs Complets
     *
     * Copyright (c) 2006
     * Quentin Drouet
     *
     * Ce programme est un logiciel libre distribue sous licence GNU/GPL.
     * Pour plus de details voir le fichier COPYING.txt.
     *
     **/

$GLOBALS[$GLOBALS['idx_lang']] = array(
'entree_nom_famille' => 'Votre nom de famille',
'entree_nom_' => 'Nom',
'entree_prenom' => 'Votre pr&eacute;nom',
'entree_prenom_' => 'Pr&eacute;nom',
'entree_organisation' => 'Votre organisation / entreprise / association',
'entree_url_organisation' => 'URL de votre organisation / entreprise / association',
'entree_telephone' => 'Votre num&eacute;ro de t&eacute;l&eacute;phone',
'entree_fax' => 'Votre num&eacute;ro de fax',
'entree_skype' => 'Votre contact Skype',
'entree_adresse' => 'Votre adresse',
'entree_codepostal' => 'Votre code postal',
'entree_ville' => 'Votre ville',
'entree_pays' => 'Votre pays',
'entree_latitude' => 'Votre latitude (en valeur num&eacute;raire ex: 92.8)',
'entree_longitude' => 'Votre longitude (en valeur num&eacute;raire ex: 15.324)',
'infos_supp' => 'Infos suppl&eacute;mentaires',
'coordonnees_sup' => 'COORDONN&Eacute;ES SUPPL&Eacute;MENTAIRES',
'affiche_organisation' => 'Organisation : ',
'affiche_telephone' => 'T&eacute;l&eacute;phone : ',
'affiche_fax' => 'Fax : ',
'affiche_skype' => 'Contact Skype : ',
'affiche_coordonnees_geo' => 'Coordonn&eacute;es G&eacute;ographiques ',
'affiche_latitude' => 'Latitude : ',
'affiche_longitude' => 'Longitude : ',
'affiche_adresse' => 'Adresse',
'paramprofil' => 'Modifier vos param&egrave;tres',
'localisation' => 'Localisation',

// Pour le modele id_card
'id_card' => 'CARTE D\'IDENTIT&Eacute; :',
'lat' => 'Lat :',
'long' => 'Long :',
'contacts' => 'Contacts :',
'organisation' => 'Organisation / Entreprise / Association :',
'email' => 'Email :',
'tel' => 'Tel :',
'fax' => 'Fax :',
'website' => 'Website :',
'vos_articles' => 'Vos Articles :',

// Pour le formulaire profil
'error_bad_user' => 'Vous ne correspondez pas &agrave; cet auteur, vous ne pouvez donc pas le modifier ...',
'votre_profil' => 'Votre profil',

// Pour le formulaire des listes
'pass_recevoir_mail'=>'L\'adresse mail utilis&eacute;e est d&eacute;j&agrave; dans la base.<br /><br /> Vous allez recevoir un email vous indiquant comment modifier votre abonnement. ',
'abonnement_mail_passcookie' => '(ceci est un message automatique)
Pour modifier votre abonnement &agrave; la lettre d\'information de ce site :
@nom_site_spip@ (@adresse_site@)

Veuillez vous rendre &agrave; l\'adresse suivante :

    @adresse_site@/spip.php?page=abonnement_ac&d=@cookie@

Vous pourrez alors confirmer la modification de votre abonnement.',

'Z' => 'ZZzZZzzz'

);

?>