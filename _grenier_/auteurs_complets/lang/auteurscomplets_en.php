<?php
     /**
     * SPIP-Auteurs Complets
     *
     * Copyright (c) 2006
     * Quentin Drouet
     *
     * Ce programme est un logiciel libre distribue sous licence GNU/GPL.
     * Pour plus de details voir le fichier COPYING.txt.
     *
     **/

$GLOBALS[$GLOBALS['idx_lang']] = array(
'entree_nom_famille' => 'Your last name',
'entree_nom_' => 'Last name',
'entree_prenom' => 'Your first name',
'entree_prenom_' => 'First Name',
'entree_organisation' => 'Votre organisation / entreprise / association',
'entree_url_organisation' => 'URL of your organisation / firm / non-profit org.',
'entree_telephone' => 'Your phone number',
'entree_fax' => 'Your fax number',
'entree_skype' => 'Your Skype contact',
'entree_adresse' => 'Your address',
'entree_codepostal' => 'Your postal code',
'entree_ville' => 'Your town',
'entree_pays' => 'Your countrie',
'entree_latitude' => 'Your latitude (numeric ex: 92.8)',
'entree_longitude' => 'Your longitude (numeric ex: 15.324)',
'infos_supp' => 'Added Infos',
'coordonnees_sup' => 'COORDONN&Eacute;ES SUPPL&Eacute;MENTAIRES',
'affiche_organisation' => 'Organisation : ',
'affiche_telephone' => 'T&eacute;l&eacute;phone : ',
'affiche_fax' => 'Fax : ',
'affiche_skype' => 'Skype contact : ',
'affiche_coordonnees_geo' => 'Geographical Coordinates ',
'affiche_latitude' => 'Latitude : ',
'affiche_longitude' => 'Longitude : ',
'affiche_adresse' => 'Address',
'paramprofil' => 'Modify your parameters',
'localisation' => 'Localization',

// Pour le modele id_card
'id_card' => 'IDENTITY CARD :',
'lat' => 'Lat :',
'long' => 'Long :',
'contacts' => 'Contacts :',
'organisation' => 'Organisation / Firm / Non-Profit Org. :',
'email' => 'Email :',
'tel' => 'Tel :',
'fax' => 'Fax :',
'website' => 'Website :',
'vos_articles' => 'Your Articles :',

// Pour le formulaire profil
'error_bad_user' => 'You are not logged as this author, so you can\'t modify him ...',
'votre_profil' => 'Your profile',

// Pour le formulaire des listes
'pass_recevoir_mail'=>'The e-mail address you are using is already in the database.<br /><br /> You will receive a confirmation e-mail specifying how to change your subscription.',
'abonnement_mail_passcookie' => '(this is an automatically generated e-mail)
To change your subscription to the newsletter, please visit the following webpage:
@nom_site_spip@ (@adresse_site@)

Please visit the following webpage:

@adresse_site@/spip.php?page=abonnement_ac&d=@cookie@',

'Z' => 'ZZzZZzzz'

);

?>