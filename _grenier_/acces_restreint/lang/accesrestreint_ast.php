<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_droits_auteur' => 'Axuntame a los que tienen permit&iacute;o entrar n\'esta estaya',
	'auteurs' => 'autores',

	// B
	'bouton_creer_la_zone' => 'Crear la estaya nueva',

	// C
	'colonne_id' => 'Num',
	'creer_zone' => 'Crear una estaya nueva',

	// D
	'descriptif' => 'Descrici&oacute;n',

	// I
	'icone_creer_zone' => 'Crear una estaya nueva',
	'icone_menu_config' => 'Accesu Torg&aacute;o',
	'icone_supprimer_zone' => 'Desaniciar esta estaya',
	'info_ajouter_zones' => 'A&ntilde;adir toes les estayes',
	'info_auteurs_lies_zone' => 'Los autores con accesu a esta estaya',
	'info_page' => 'Estae p&aacute;xina te permite xestionar les estayes con accesu torg&aacute;u nel to sitiu',
	'info_retirer_zone' => 'Quitar de la estaya',
	'info_retirer_zones' => 'Quitar de toes les estayes',

	// P
	'page_zones_acces' => 'Accesu Torg&aacute;o',
	'privee' => 'Privao',
	'publique' => 'P&uacute;blicu',

	// R
	'rubriques' => 'seiciones',
	'rubriques_zones_acces' => 'Seiciones de la estaya',

	// S
	'selectionner_une_zone' => 'Seleicionar una estaya',

	// T
	'titre' => 'T&iacute;tulu',
	'titre_ajouter_zone' => 'Amestar la estaya',
	'titre_table' => 'Toes les estayes d\'accesu',
	'titre_zones_acces' => 'Estayes d\'accesu torg&aacute;o',

	// V
	'voir_toutes' => 'Ver toes les estayes',

	// Z
	'zone_numero' => 'ESTAYA N&Uacute;MBERU:',
	'zone_restreinte_espace_prive' => 'Torgar l\'accesu a esta estaya nel espaciu priv&aacute;o',
	'zone_restreinte_publique' => 'Torgar l\'accesu a esta estaya na parte p&uacute;blica'
);

?>
