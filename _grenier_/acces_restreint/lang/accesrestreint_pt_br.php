<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_droits_auteur' => 'Conceder-me direitos de acesso a esta &aacute;rea',
	'auteurs' => 'autores',

	// B
	'bouton_creer_la_zone' => 'Criar a nova &aacute;rea',

	// C
	'colonne_id' => 'N&amp;ordm;.',
	'creer_zone' => 'Criar uma nova &aacute;rea',

	// D
	'descriptif' => 'Descri&ccedil;&atilde;o r&aacute;pida',

	// I
	'icone_creer_zone' => 'Criar uma nova &aacute;rea',
	'icone_menu_config' => 'Acesso Restrito',
	'icone_supprimer_zone' => 'Excluir est&aacute; &aacute;rea',
	'info_ajouter_zones' => 'Incluir todas as &aacute;reas',
	'info_auteurs_lies_zone' => 'Os autores com acesso a esta &aacute;rea',
	'info_page' => 'esta p&aacute;gina permite-lhe gerar as &aacute;reas de acesso restrito do seu site',
	'info_retirer_zone' => 'Retirar da &aacute;rea',
	'info_retirer_zones' => 'retirar de todas as &aacute;reas',

	// P
	'page_zones_acces' => 'Acesso Restrito',
	'privee' => 'Privado',
	'publique' => 'Publicado',

	// R
	'rubriques' => 'se&ccedil;&otilde;es',
	'rubriques_zones_acces' => 'Se&ccedil;&otilde;es da &aacute;rea',

	// S
	'selectionner_une_zone' => 'Selecionar uma &aacute;rea',

	// T
	'titre' => 'T&iacute;tulo',
	'titre_ajouter_zone' => 'Aderir &agrave; &aacute;rea',
	'titre_table' => 'Todas as &aacute;reas de acesso',
	'titre_zones_acces' => '&Aacute;reas de acesso restrito',

	// V
	'voir_toutes' => 'Ver todas as &aacute;reas',

	// Z
	'zone_numero' => '&Aacute;REA N&Uacute;MERO:',
	'zone_restreinte_espace_prive' => 'Restringir o acesso a esta &aacute;rea no espa&ccedil;o privado',
	'zone_restreinte_publique' => 'restringir o acesso a esta &aacute;rea na &aacute;rea p&uacute;blica'
);

?>
