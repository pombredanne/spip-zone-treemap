<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_droits_auteur' => 'Afegir-me els drets d\'acc&eacute;s a aquesta zona',
	'auteurs' => 'autors',

	// B
	'bouton_creer_la_zone' => 'Crear la nova zona',

	// C
	'colonne_id' => 'N&uacute;m',
	'creer_zone' => 'Crear una nova zona',

	// D
	'descriptif' => 'Descripci&oacute;',

	// I
	'icone_creer_zone' => 'Crear una nova zona',
	'icone_menu_config' => 'Acc&eacute;s Restringit ',
	'icone_supprimer_zone' => 'Suprimir aquesta zona',
	'info_ajouter_zones' => 'Afegir totes les zones',
	'info_auteurs_lies_zone' => 'Els autors que tenen acc&eacute;s a aquesta zona',
	'info_page' => 'Aquesta p&agrave;gina permet gestionar les zones d\'acc&eacute;s restringit del vostre lloc',
	'info_retirer_zone' => 'Treure de la zona',
	'info_retirer_zones' => 'Treure de totes les zones',

	// P
	'page_zones_acces' => 'Acc&eacute;s Restringit',
	'privee' => 'Privat',
	'publique' => 'P&uacute;blic',

	// R
	'rubriques' => 'seccions',
	'rubriques_zones_acces' => 'Seccions de la zona',

	// S
	'selectionner_une_zone' => 'Seleccionar una zona',

	// T
	'titre' => 'T&iacute;tol',
	'titre_ajouter_zone' => 'Tornar a la zona',
	'titre_table' => 'Totes les zones d\'acc&eacute;s',
	'titre_zones_acces' => 'Zones d\'acc&eacute;s restringit',

	// V
	'voir_toutes' => 'Veure totes les zones',

	// Z
	'zone_numero' => 'ZONA N&Uacute;MERO&nbsp;:',
	'zone_restreinte_espace_prive' => 'Restringir l\'acc&eacute;s a aquesta zona a l\'espai privat',
	'zone_restreinte_publique' => 'Restringir l\'acc&eacute;s a aquesta zona a la part p&uacute;blica'
);

?>
