<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_droits_auteur' => 'Mir selbst den Zugang zu diesem Bereich erlauben',
	'auteurs' => 'Autoren',

	// B
	'bouton_creer_la_zone' => 'Neuen Bereich erstellen',

	// C
	'colonne_id' => 'Num',
	'confirmer_retirer_auteur_zone' => 'M&ouml;chten Sie diesen Autor wirklich aus dem Bereich entfernen?',
	'confirmer_retirer_auteurs' => 'M&ouml;chten wirklich alle Autoren aus dem Bereich entfernen?',
	'confirmer_supprimer_zone' => 'Wollen Sie diesen Bereich wirklich l&ouml;schen?',
	'creer_zone' => 'Neuen Bereich anlegen',

	// D
	'descriptif' => 'Beschreibung',

	// I
	'icone_menu_config' => 'Zugangskontrolle',
	'icone_supprimer_zone' => 'Bereich l&ouml;schen',
	'info_acces_restreint' => 'Diese Seite ist gesch&uuml;tzt. Bitte melden sie sich an, um auf sie zuzugreifen.',
	'info_ajouter_auteur' => 'Autor hinzuf&uuml;gen',
	'info_ajouter_auteurs' => 'Alle Autoren hinzuf&uuml;gen',
	'info_ajouter_zones' => 'Alle Bereichen hinzuf&uuml;gen',
	'info_aucun_acces' => 'Zugriff verboten',
	'info_aucun_auteur' => 'Kein Autor f&uuml;r diesen Bereich',
	'info_aucune_zone' => 'Kein Bereich',
	'info_auteurs_lies_zone' => 'Autoren mit Zugang zu diesem Bereich',
	'info_page' => 'Hier k&ouml;nnen Sie die gesch&uuml;tzten Bereiche Ihrer Webseite verwalten.',
	'info_retirer_auteurs' => 'ALle Autoren entfernen',
	'info_retirer_zone' => 'Bereich l&ouml;schen',
	'info_retirer_zones' => 'Alle Bereiche l&ouml;schen',

	// M
	'modifier_zone' => 'Bereich &auml;ndern',

	// P
	'page_zones_acces' => 'Zugangskontrolle',
	'par_titre' => 'Nach Titel',
	'privee' => 'Privat',
	'publique' => '&Ouml;ffentlich',

	// R
	'rubriques' => 'Rubriken',
	'rubriques_zones_acces' => 'Rubriken des Bereiches',

	// S
	'selectionner_une_zone' => 'Bereich ausw&auml;hlen',

	// T
	'titre' => 'Titel',
	'titre_ajouter_zone' => 'zum Bereich zuf&uuml;gen',
	'titre_cadre_modifier_zone' => 'Bereich &auml;ndern',
	'titre_table' => 'Alle Bereiche mit Zugangskontrolle',
	'titre_zones_acces' => 'Bereiche mit Zugangskonrolle',
	'toutes' => 'Alle',

	// V
	'voir_toutes' => 'Alle Bereiche anzeigen',

	// Z
	'zone_numero' => 'BEREICH NUMMER:',
	'zone_restreinte_espace_prive' => 'Zugang im Redaktionsbereich kontrollieren',
	'zone_restreinte_publique' => 'Zugangskontrolle f&uuml;r den &ouml;ffentlichen Bereich'
);

?>
