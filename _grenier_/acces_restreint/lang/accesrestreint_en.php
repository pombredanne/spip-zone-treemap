<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_droits_auteur' => 'Give me instant acces to this area',
	'auteurs' => 'authors',

	// B
	'bouton_creer_la_zone' => 'Create new area',

	// C
	'colonne_id' => '#',
	'creer_zone' => 'Create a new area',

	// D
	'descriptif' => 'Description',

	// I
	'icone_creer_zone' => 'Create new restricted area',
	'icone_menu_config' => 'restricted access',
	'icone_supprimer_zone' => 'Delete this area',
	'info_ajouter_zones' => 'Add all areas',
	'info_auteurs_lies_zone' => 'Authors with access to this area',
	'info_page' => 'This page allows you to create restricted areas in your site',
	'info_retirer_zone' => 'Move out of area',
	'info_retirer_zones' => 'Move out of all areas',

	// P
	'page_zones_acces' => 'restricted access',
	'privee' => 'Private',
	'publique' => 'Public',

	// R
	'rubriques' => 'sections',
	'rubriques_zones_acces' => 'Restricted area\\\'s sections',

	// S
	'selectionner_une_zone' => 'Select a restricted area',

	// T
	'titre' => 'Title',
	'titre_ajouter_zone' => 'Add to a restricted area',
	'titre_table' => 'All restricted areas',
	'titre_zones_acces' => 'Restricted areas',

	// V
	'voir_toutes' => 'Show all areas',

	// Z
	'zone_numero' => 'ZONE NUMBER:',
	'zone_restreinte_espace_prive' => 'Restrict access to this area in the private area',
	'zone_restreinte_publique' => 'Restrict access to this area in the public site'
);

?>
