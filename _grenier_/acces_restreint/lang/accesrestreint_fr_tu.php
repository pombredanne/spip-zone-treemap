<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_droits_auteur' => 'M\'ajouter les droits d\'acc&egrave;s &agrave; cette zone',
	'auteurs' => 'auteurs',

	// B
	'bouton_creer_la_zone' => 'Cr&eacute;er la nouvelle zone',

	// C
	'colonne_id' => 'Num',
	'creer_zone' => 'Cr&eacute;er une nouvelle zone',

	// D
	'descriptif' => 'Descriptif',

	// I
	'icone_creer_zone' => 'Cr&eacute;er une nouvelle zone',
	'icone_menu_config' => 'Acc&egrave;s Restreint',
	'icone_supprimer_zone' => 'Supprimer cette zone',
	'info_ajouter_zones' => 'Ajouter toutes les zones',
	'info_auteurs_lies_zone' => 'Les auteurs ayant acc&egrave;s &agrave; cette zone',
	'info_page' => 'Cette page te permets de g&eacute;rer les zones d\'acc&egrave;s restreint de ton site',
	'info_retirer_zone' => 'Enlever de la zone',
	'info_retirer_zones' => 'Enlever de toutes les zones',

	// P
	'page_zones_acces' => 'Acc&egrave;s Restreint',
	'privee' => 'Priv&eacute;e',
	'publique' => 'Publique',

	// R
	'rubriques' => 'rubriques',
	'rubriques_zones_acces' => 'Rubriques de la zone',

	// S
	'selectionner_une_zone' => 'S&eacute;lectionner une zone',

	// T
	'titre' => 'Titre',
	'titre_ajouter_zone' => 'Rejoindre la zone',
	'titre_table' => 'Toutes les zones d\'acc&egrave;s',
	'titre_zones_acces' => 'Zones d\'acc&egrave;s restreint',

	// V
	'voir_toutes' => 'Voir toutes les zones',

	// Z
	'zone_numero' => 'ZONE NUM&Egrave;RO&nbsp;:',
	'zone_restreinte_espace_prive' => 'Restreindre l\'acc&egrave;s &agrave; cette zone dans l\'espace priv&eacute;',
	'zone_restreinte_publique' => 'Restreindre l\'acc&egrave;s &agrave; cette zone dans la partie publique'
);

?>
