<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_droits_auteur' => 'Engadir os dereitos de acceso a esta zona',
	'auteurs' => 'autores',

	// B
	'bouton_creer_la_zone' => 'Crear a nova zona',

	// C
	'colonne_id' => 'N&uacute;m.',
	'creer_zone' => 'Crear unha nova zona',

	// D
	'descriptif' => 'Descritivo',

	// I
	'icone_creer_zone' => 'Crear unha nova zona',
	'icone_menu_config' => 'Acceso restrinxido',
	'icone_supprimer_zone' => 'Suprimir esta zona',
	'info_ajouter_zones' => 'Engadir todas as zonas',
	'info_auteurs_lies_zone' => 'Os autores que te&ntilde;en acceso a esta zona',
	'info_page' => 'Esta p&aacute;xina permite xestionar as zonas de acceso restrinxido do seu web',
	'info_retirer_zone' => 'Retirar da zona',
	'info_retirer_zones' => 'Retirar de todas as zonas',

	// P
	'page_zones_acces' => 'Acceso restrinxido',
	'privee' => 'Privado',
	'publique' => 'P&uacute;blico',

	// R
	'rubriques' => 'secci&oacute;ns',
	'rubriques_zones_acces' => 'Secci&oacute;ns da zona',

	// S
	'selectionner_une_zone' => 'Seleccionar unha zona',

	// T
	'titre' => 'T&iacute;tulo',
	'titre_ajouter_zone' => 'Reconectar a zona',
	'titre_table' => 'Todas as zonas de acceso',
	'titre_zones_acces' => 'Zonas de acceso restrinxido',

	// V
	'voir_toutes' => 'Ver todas as zonas',

	// Z
	'zone_numero' => 'ZONA N&Uacute;MERO&nbsp;:',
	'zone_restreinte_espace_prive' => 'Restrinxir o acceso a esta zona no espazo privado',
	'zone_restreinte_publique' => 'Restrinxir o acceso a esta zona na parte p&uacute;blica'
);

?>
