<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP 
// Esto es un archivo de idioma SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// Aide des boutons supplémentaires de la barre typo
'barre_intertitre2' => 'Transformar en {2{intert&iacute;tulos nivel dos}2}',
'barre_intertitre3' => 'Transformar en {3{intert&iacute;tulos nivel tres}3}',
'barre_langue' => 'Idioma abreviado', 
'barre_miseenevidence' => 'Poner el texto en [*evidencia*]',
'barre_exposant' => 'Poner el texto en &lt;sup&gt;potencia&lt;/sup&gt;',
'barre_petitescapitales' => 'Poner el texto en &lt;sc&gt;may&uacute;sculas peque&ntilde;as&lt;/sc&gt;',
'barre_centrer' => '[|Centrar|] el p&aacute;rrafo',
'barre_alignerdroite' => '[/Al&iacute;nea a la derecha/] el p&aacute;rrafo',
'barre_encadrer' => '[(Encuadrar)] el p&aacute;rrafo',
'barre_e_accent_grave' => 'Insertar una E may&uacute;scula con acento grave',
'barre_ea' => 'Insertar una E en la A',
'barre_ea_maj' => 'Insertar una E en la A may&uacute;scula',
'barre_c_cedille_maj' => 'Insertar una C cedilla may&uacute;scula',

'barre_chercher' => 'Buscar remplazar',
'barre_tableau' => 'Insertar una tabla',
'barre_glossaire' => 'Entrada de [?glosario] (Wikipedia)',


'barre_caracteres' => 'Caracteres espaciales',
'barre_adresse' => 'Dirección',
'barre_lien_externe' => 'Enlace externo',
'barre_bulle' => 'Burbuja de ayuda',
'barre_gestion_caption' => 'Encabezado y resumen',
'barre_gestion_colonne' => 'N&uacute;m de columnas',
'barre_gestion_ligne' => 'N&uacute;m de l&iacute;neas',
'barre_gestion_entete' => 'Encabezado',
'barre_gestion_taille' => 'Tama&ntilde;o fijo',
'barre_gestion_cr_chercher' => 'Buzcar',
'barre_gestion_cr_remplacer' => 'Remplazar',
'barre_gestion_cr_casse' => 'Respetar may&uacute;scula/min&uacute;scula',
'barre_gestion_cr_tout' => 'Remplazar todo',
'barre_gestion_cr_entier' => 'Palabra entera',

'barre_ancres' => 'Gesti&oacute;n de anclas',
'barre_gestion_anc_caption' => 'Gesti&oacute;n de anclas',
'barre_gestion_anc_inserer' => 'Insertar un ancla',
'barre_gestion_anc_nom' => 'Nombre  del ancla',
'barre_gestion_anc_pointer' => 'Enlazar hacia un ancla',
'barre_gestion_anc_cible' => 'Ancla destino',
'barre_gestion_anc_bulle' => 'Burbuja de ayuda del ancla'

);
?>
