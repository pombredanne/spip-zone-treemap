<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// Aide des boutons suppl�mentaires de la barre typo
'barre_intertitre2' => 'Converti in {2{sottotitolo 2o-livello}2}',
'barre_intertitre3' => 'Converti in {3{sottotitolo 3o-livello}3}',
'barre_langue' => 'Abbreviazione lingua', 
'barre_miseenevidence' => '[*Evidenziare*] il testo',
'barre_exposant' => 'Sovrimpressione',
'barre_petitescapitales' => 'Maiuscoletto',
'barre_centrer' => '[|Centrare|] il paragrafo',
'barre_alignerdroite' => '[/allinea a destra/] il paragrafo',
'barre_encadrer' => '[(Riquadro)] attorno al paragrafo',
'barre_e_accent_grave' => 'Inserisci E grave maiuscola',
'barre_ea' => 'Inserisci &aelig;',
'barre_ea_maj' => 'Inserisci &AElig;',
'barre_c_cedille_maj' => 'Inserisci una C maiuscola con cedilla',

'barre_chercher' => 'Trova & Sostituisci',
'barre_tableau' => 'Inserisci/modifica una tabella',
'barre_glossaire' => 'Voce di [?Glossario] (Wikipedia)',

'barre_poesie' => 'stile poetico',
'barre_caracteres' => 'Caratteri speciali',
'barre_adresse' => 'Indirizzo',
'barre_lien_externe' => 'Link esterno',
'barre_bulle' => 'Guida',
'barre_gestion_caption' => 'Titoli e Indice',
'barre_gestion_colonne' => 'No. di colonne',
'barre_gestion_ligne' => 'No. di righe',
'barre_gestion_entete' => 'Intestazione',
'barre_gestion_taille' => 'Dimensione fissa',
'barre_gestion_cr_changercasse' => 'Converti maiuscolo/minuscolo',
'barre_gestion_cr_changercassemajuscules' => 'Converti in MAIUSCOLO',
'barre_gestion_cr_changercasseminuscules' => 'Converti in minuscolo',
'barre_gestion_cr_chercher' => 'Cerca',
'barre_gestion_cr_remplacer' => 'Sostituisci',
'barre_gestion_cr_casse' => 'Controlla Maiuscole/minuscole',
'barre_gestion_cr_tout' => 'Sostituisci tutto',
'barre_gestion_cr_entier' => 'Solo parole intere',
'barre_preview' => 'Modalit� Anteprima',

'barre_ancres' => 'Segnalibro',
'barre_gestion_anc_caption' => 'Segnalibri',
'barre_gestion_anc_inserer' => 'Converti in Segnalibro',
'barre_gestion_anc_nom' => 'Nome Segnalibro',
'barre_gestion_anc_pointer' => 'Punta a un Segnalibro',
'barre_gestion_anc_cible' => 'Segnalibro referenziato',
'barre_gestion_anc_bulle' => 'Guida del Segnalibro',
'barre_avances' => 'Allineamento&nbsp;!',
'barre_boutonsavances' => 'Formattazione supplementare, utilizzare con moderazione e discernimento&nbsp;!'

);
?>