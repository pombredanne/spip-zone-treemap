<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// Aide des boutons supplémentaires de la barre typo
'barre_intertitre2' => 'Transformer en {2{intertitre niveau deux}2}',
'barre_intertitre3' => 'Transformer en {3{intertitre niveau trois}3}',
'barre_langue' => 'Langue abr&eacute;g&eacute;e', 
'barre_miseenevidence' => 'Mettre le texte en [*&eacute;vidence*]',
'barre_exposant' => 'Mettre le texte en &lt;sup&gt;exposant&lt;/sup&gt;',
'barre_petitescapitales' => 'Mettre le texte en &lt;sc&gt;petites capitales&lt;/sc&gt;',
'barre_centrer' => '[|Centrer|] le paragraphe',
'barre_alignerdroite' => '[/Aligne &agrave; droite/] le paragraphe',
'barre_encadrer' => '[(Encadrer)] le paragraphe',
'barre_e_accent_grave' => 'Ins&eacute;rer un E majuscule accent grave',
'barre_ea' => 'Ins&eacute;rer un E dans l\'A',
'barre_ea_maj' => 'Ins&eacute;rer un E dans l\'A majuscule',
'barre_c_cedille_maj' => 'Ins&eacute;rer un C c&eacute;dille majuscule',

'barre_chercher' => 'Chercher Remplacer',
'barre_tableau' => 'Ins&eacute;rer/modifier (le s&eacute;lectionner avant) un tableau',
'barre_glossaire' => 'Entr&eacute;e du [?glossaire] (Wikipedia)',

'barre_poesie' => 'Mettre en forme comme une &lt;poesie&gt;po&eacute;sie&lt;/poesie&gt;',
'barre_caracteres' => 'Caract&egrave;res sp&eacute;ciaux',
'barre_adresse' => 'Adresse',
'barre_lien_externe' => 'Lien externe',
'barre_bulle' => 'Bulle d\'aide',
'barre_galerie' => 'Ouvrir la galerie',
'barre_gestion_caption' => 'Caption et R&eacute;sum&eacute;',
'barre_gestion_colonne' => 'Nb colonnes',
'barre_gestion_ligne' => 'Nb lignes',
'barre_gestion_entete' => 'Entete',
'barre_gestion_taille' => 'Taille fixe',
'barre_gestion_cr_changercasse' => 'Changer la casse',
'barre_gestion_cr_changercassemajuscules' => 'Passer en majuscules',
'barre_gestion_cr_changercasseminuscules' => 'Passer en minuscules',
'barre_gestion_cr_chercher' => 'Chercher',
'barre_gestion_cr_remplacer' => 'Remplacer',
'barre_gestion_cr_casse' => 'Respecter la casse',
'barre_gestion_cr_tout' => 'Tout remplacer',
'barre_gestion_cr_entier' => 'Mot entier',
'barre_preview' => 'Mode pr&eacute;visualisation',

'barre_ancres' => 'Gestion des ancres',
'barre_gestion_anc_caption' => 'Gestion des ancres',
'barre_gestion_anc_inserer' => 'Transformer en ancre',
'barre_gestion_anc_nom' => 'Nom de l\'ancre',
'barre_gestion_anc_pointer' => 'Pointer vers une ancre',
'barre_gestion_anc_cible' => 'Ancre cible',
'barre_gestion_anc_bulle' => 'Bulle d\'aide ancre',
'barre_avances' => 'Du sens, du sens&nbsp;!',
'barre_boutonsavances' => 'Mises en sens suppl&eacute;mentaires, &agrave; utiliser avec mod&eacute;ration et discernement&nbsp;!',
'cfg_puces' => 'Traitement des puces',
'cfg_titraille' => 'Titraille',
'cfg_insertcss' => 'Insertion CSS',

'galerie' => 'Galerie de documents'

);
?>
