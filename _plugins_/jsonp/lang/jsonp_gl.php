<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A

	// B

	// C
'configuracion_article' => 'Selecciona os campos do artigo que aparecer&aacute;n no jsonp',

	// D
'description' => '<h3>Arquivos jsonp:</h3>O plugin JsonP xera dous arquivos jsonp. O chamado jsonp-full que cont&eacute;n toda a informaci&oacute;n que ten un artigo e o jsonp que cont&eacute;n a informaci&oacute;n que se pode configurar no formulario da dereita. Podedes ver unha previsualizaci&oacute;n dos dous artigos nos seguintes enderezos:
<br />
<strong><a href="../spip.php?page=jsonp">jsonp</a></strong>
<br />
<strong><a href="../spip.php?page=jsonp-all">jsonp-all</a></strong>
<br /><br />
<h3>Lectura do jsonp:</h3>Tam&eacute;n podes ver unha p&aacute;xina de exemplo de como coller a informaci&oacute;n no jsonp a trav&eacute;s de jquery:
<br />
<strong><a href="../spip.php?page=example-load-jsonp">exemplo ler jsonp</a></strong>',

	// E
'explication_numero_articulos' => 'N&uacute;mero de artigos que aparecer&aacute;n logo no arquivo jsonp',

	// I
'incluir_audios' => 'Inclu&iacute; os audios',	
'incluir_chapo' => 'Inclu&iacute;r a cabeceira',	
'incluir_descriptif' => 'Inclu&iacute;r a descrici&oacute;n',	
'incluir_docs' => 'Inclu&iacute;r os dem&aacute;is documentos',	
'incluir_introduction' => 'Inclu&iacute;r a introducci&oacute;n',	
'incluir_images' => 'Inclu&iacute;r as imaxes',	
'incluir_lang' => 'Inclu&iacute;r o idioma',	
'incluir_logo_article' => 'Inclu&iacute;r o logo do artigo',	
'incluir_logo_article_survol' => 'Inclu&iacute;r o logotipo de paso do rato',	
'incluir_mots' => 'Inclu&iacute;r as palabras clave',	
'incluir_notes' => 'Inclu&iacute;r as notas',	
'incluir_popularite' => 'Inclu&iacute;r a popularidade do artigo',	
'incluir_ps' => 'Inclu&iacute;r o p&eacute; do artigo',	
'incluir_surtitre' => 'Inclu&iacute;r o sobret&iacute;tulo',	
'incluir_soustitre' => 'Inclu&iacute;r o subt&iacute;tulo',	
'incluir_titre' => 'Inclu&iacute;r o t&iacute;tulo',	
'incluir_url_article' => 'Inclu&iacute;r o url do artigo',	
'incluir_visites' => 'Inclu&iacute;r as visitas ao artigo',	
	
	// L

	// M

	// N
'numero_articulos' => 'N&uacute;mero de artigos',

	// P

	// S
'selecciona_rubrique' => 'Selecciona as secci&oacute;ns',
'selecciona_rubrique_label' => 'Escolle as secci&oacute;ns que conter&aacute;n os artigos que aparecer&aacute;n no jsonp.',

	// T
'title' => 'Jsonp',
'todas_as_seccions' => 'Escolle todas as secci&oacute;ns'

	// U

	// V

);

?>
