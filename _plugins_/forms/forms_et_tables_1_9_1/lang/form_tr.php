<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/form?lang_cible=tr
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Bu formu kullanan makaleler',
	'aucune_reponse' => 'Hiç cevap yok',

	// C
	'champs_formulaire' => 'Form alanları',

	// F
	'formulaire' => 'Form',

	// I
	'icone_ajouter_donnees' => 'Cevap ekle',
	'icone_creer_table' => 'Yeni bir form oluştur',
	'importer_donnees_csv' => 'cevapları ihraç et',
	'info_supprimer_formulaire' => 'Bu formu gerçekten silmek istiyor musunuz ?',
	'info_supprimer_formulaire_reponses' => 'Bu formla ilintili cevaplar. 
Gerçekten simek istiyor musunuz ?',

	// L
	'lien_retirer_donnee_liante' => 'Bu cevaptan yapılan bağı çıkart',
	'lien_retirer_donnee_liee' => 'Bu cevabı çıkart',

	// N
	'nombre_reponses' => '@nombre@ cevap',
	'nouveau_formulaire' => 'Yeni form',

	// S
	'suivi_reponses' => 'Cevapları göster',
	'supprimer_formulaire' => 'Bu formu sil',

	// T
	'telecharger_reponses' => 'Cevapları indiriniz',
	'texte_donnee_statut' => 'Bu cevabın statüsü',
	'texte_statut_poubelle' => 'Silinen cevap',
	'texte_statut_prepa' => 'Yazılmakta olan cevap',
	'texte_statut_prop' => 'Önerilen cevap',
	'texte_statut_publie' => 'Yayınlanan cevap',
	'texte_statut_refuse' => 'Reddedilen cevap',
	'titre_formulaire' => 'Form başlığı',
	'toutes_tables' => 'Tüm formlar',
	'type_des_tables' => 'Formlar',

	// U
	'une_reponse' => 'Bir cevap'
);

?>
