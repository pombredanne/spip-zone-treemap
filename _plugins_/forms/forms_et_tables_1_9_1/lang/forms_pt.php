<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_barre_typo' => 'Activer la barre typographique', # NEW
	'adresse_invalide' => 'Cette adresse n\'est pas valide.', # NEW
	'afficher' => 'Afficher', # NEW
	'aide_contextuelle' => 'Aide contextuelle', # NEW
	'ajouter_champ' => 'Ajouter un champ', # NEW
	'ajouter_champ_type' => 'Créer un champ de type :', # NEW
	'ajouter_choix' => 'Ajouter un choix', # NEW
	'apparence_formulaire' => 'Apparence du formulaire de saisie', # NEW
	'article_inserer_un_formulaire' => 'Insérer un formulaire', # NEW
	'article_inserer_un_formulaire_detail' => 'Vous pouvez insérer des formulaires dans vos articles afin de permettre aux visiteurs d\'entrer des informations. Choisissez un formulaire dans la liste ci-dessous et recopiez le raccourci dans le texte de l\'article.', # NEW
	'article_recopier_raccourci' => 'Recopiez ce raccourci dans le texte de l\'article pour insérer ce formulaire', # NEW
	'articles_utilisant' => 'Articles utilisant ce formulaire', # NEW
	'attention' => 'Attention :', # NEW
	'aucune_reponse' => 'aucune donnée', # NEW
	'avis_message_confirmation' => 'Un message de confirmation est envoyé à @mail@', # NEW

	// B
	'boite_info' => 'Cliquez sur les icones pour voir les données associées a un formulaire, l\'editer, le dupliquer, l\'exporter, vider les données ou le supprimer.', # NEW

	// C
	'cfg_activer' => 'Oui', # NEW
	'cfg_associer_donnees' => 'Associer les données des tables', # NEW
	'cfg_associer_donnees_articles' => 'Permettre d\'associer les donnees aux articles :', # NEW
	'cfg_associer_donnees_auteurs' => 'Permettre d\'associer les donnees aux auteurs :', # NEW
	'cfg_associer_donnees_rubriques' => 'Permettre d\'associer les donnees aux rubriques :', # NEW
	'cfg_bouton_type_image' => 'bouton de type image', # NEW
	'cfg_bouton_type_texte' => 'bouton de type texte (submit)', # NEW
	'cfg_bouton_valider' => 'Type de bouton valider', # NEW
	'cfg_bouton_valider_texte' => 'Vous pouvez préciser quel type de bouton valider utiliser dans les formulaires :', # NEW
	'cfg_desactiver' => 'Non', # NEW
	'cfg_inserer_head' => 'Insertion dans le <head>', # NEW
	'cfg_inserer_head_texte' => 'Inserer automatiquement les appels aux fichiers suivants dans la section &lt;head&gt; des pages du site (recommandé) :', # NEW
	'champ_descendre' => 'descendre', # NEW
	'champ_email_details' => 'Veuillez entrer une adresse e-mail valide (de type vous@fournisseur.com).', # NEW
	'champ_listable_admin' => 'Afficher ce champ dans les listes de l\'espace privé', # NEW
	'champ_listable_publique' => 'Afficher ce champ dans les listes publiques', # NEW
	'champ_monter' => 'monter', # NEW
	'champ_necessaire' => 'Ce champ doit être rempli.', # NEW
	'champ_nom' => 'Nom du champ', # NEW
	'champ_nom_bloc' => 'Nom du bloc', # NEW
	'champ_nom_groupe' => 'Groupe', # NEW
	'champ_nom_texte' => 'Texte', # NEW
	'champ_public' => 'Ce champ est visible dans l\'espace public', # NEW
	'champ_saisie_desactivee' => 'Désactiver la saisie de ce champ', # NEW
	'champ_specifiant' => 'Ce champ qualifie la donnée (tri, filtre, description)', # NEW
	'champ_table_jointure_type' => 'Type de table pour les jointures', # NEW
	'champ_type_date' => 'Date', # NEW
	'champ_type_email' => 'Adresse e-mail', # NEW
	'champ_type_fichier' => 'Fichier à télécharger', # NEW
	'champ_type_joint' => 'Jointure avec une autre table', # NEW
	'champ_type_ligne' => 'Ligne de texte', # NEW
	'champ_type_monnaie' => 'Monétaire', # NEW
	'champ_type_mot' => 'Mots-clés', # NEW
	'champ_type_multiple' => 'Choix multiple', # NEW
	'champ_type_numerique' => 'Numérique', # NEW
	'champ_type_password' => 'Mot de passe', # NEW
	'champ_type_select' => 'Choix unique', # NEW
	'champ_type_separateur' => 'Nouveau bloc de questions', # NEW
	'champ_type_texte' => 'Texte', # NEW
	'champ_type_textestatique' => 'Message d\'explication', # NEW
	'champ_type_url' => 'Adresse de site Web', # NEW
	'champ_url_details' => 'Veuillez entrer une adresse Web valide (de type http://www.monsite.com/...).', # NEW
	'champs_formulaire' => 'Champs du formulaire', # NEW
	'changer_choix_multiple' => 'Changer en choix multiple', # NEW
	'changer_choix_unique' => 'Changer en choix unique', # NEW
	'choisir_email' => 'Choisir l\'email en fonction de', # NEW
	'confirm_supprimer_champ' => 'Etes-vous certain de vouloir supprimer le champ \'@champ@\' ?', # NEW
	'confirm_supprimer_donnee' => 'Etes-vous certain de vouloir supprimer la donnee \'@donnee@\' ?', # NEW
	'confirm_vider_table' => 'Etes-vous certain de vouloir vider le contenu de la table \'@table@\' ?', # NEW
	'confirmer_champ_password' => 'Libelle si double saisie', # NEW
	'confirmer_password' => 'Confirmation', # NEW
	'confirmer_reponse' => 'Envoyer un mail d\'accusé de réception avec :', # NEW
	'csv_classique' => 'CSV classique (,)', # NEW
	'csv_excel' => 'CSV pour Excel (;)', # NEW
	'csv_tab' => 'CSV avec tabulations', # NEW

	// D
	'date' => 'Date', # NEW
	'date_invalide' => 'Format de la date invalide', # NEW
	'donnees_modifiable' => 'Données modifiables par l\'utilisateur.', # NEW
	'donnees_multiple' => 'Réponses multiples.', # NEW
	'donnees_nonmodifiable' => 'Données non modifiables par l\'utilisateur.', # NEW
	'donnees_nonmultiple' => 'Réponse unique.', # NEW
	'donnees_prot' => 'Données protégées. Les données enregistrées ne seront accessibles que depuis l\'interface privée.', # NEW
	'donnees_pub' => 'Données publiques. Les données enregitrées seront accessibles aux visiteurs du site.', # NEW
	'dupliquer' => 'Dupliquer', # NEW

	// E
	'echec_upload' => 'Le transfert du fichier a échoué.', # NEW
	'edit_champ_obligatoire' => 'Ce champ est obligatoire', # NEW
	'editer' => 'Editer', # NEW
	'email_independant' => 'Email independant de la reponse', # NEW
	'exporter' => 'Exporter', # NEW
	'exporter_article' => 'Exporter dans un article', # NEW

	// F
	'fichier_trop_gros' => 'Ce fichier est trop gros.', # NEW
	'fichier_type_interdit' => 'Ce type de fichier est interdit.', # NEW
	'form_erreur' => 'Erreur :', # NEW
	'format_fichier' => 'Format du fichier :', # NEW
	'format_liste' => 'déroulante', # NEW
	'format_liste_ou_radio' => 'Format de la liste', # NEW
	'format_radio' => 'boutons radio', # NEW
	'forms_obligatoires' => 'Formulaires obligatoires pour la saisie de celui-ci :', # NEW
	'formulaire' => 'Formulaire', # NEW
	'formulaire_aller' => 'Aller au formulaire', # NEW
	'formulaires_copie' => 'Copie de @nom@', # NEW
	'formulaires_sondages' => 'Formulaires et sondages', # NEW

	// H
	'html_wrapper' => 'Encapsuler le champ dans le code html', # NEW

	// I
	'icone_ajouter_donnees' => 'Ajouter des réponses', # NEW
	'icone_creer_formulaire' => 'Créer un nouveau formulaire', # NEW
	'icone_creer_table' => 'Créer un nouveau formulaire', # NEW
	'importer_form' => 'Importer un formulaire', # NEW
	'info_apparence' => 'Voici une prévisualisation du formulaire tel qu\'il apparaîtra aux visiteurs du site public.', # NEW
	'info_articles_lies_donnee' => 'Les articles liés', # NEW
	'info_champs_formulaire' => 'Vous pouvez ici créer et modifier les champs que les visiteurs pourront remplir.', # NEW
	'info_obligatoire_02' => '[Obigatório]',
	'info_rubriques_liees_donnee' => 'Les rubriques liées', # NEW
	'info_sondage' => 'Si votre formulaire est un sondage, les résultats des champs de type « sélection » seront additionnés et affichés.', # NEW
	'info_supprimer_formulaire' => 'Voulez-vous vraiment supprimer ce formulaire ?', # NEW
	'info_supprimer_formulaire_reponses' => 'Des données sont associées à ce formulaire. Voulez-vous vraiment le supprimer ?', # NEW

	// L
	'lien_apercu' => 'Aperçu', # NEW
	'lien_champ' => 'Champs', # NEW
	'lien_propriete' => 'Propriétés', # NEW
	'lien_retirer_donnee_liante' => 'Retirer le lien', # NEW
	'lien_retirer_donnee_liee' => 'Retirer le lien', # NEW
	'lier_articles' => 'Permettre d\'associer les données aux articles', # NEW
	'lier_documents' => 'Permettre de joindre des documents aux données', # NEW
	'lier_documents_mail' => 'Joindre les documents à l\'email', # NEW
	'liste_choix' => 'Liste des choix proposés', # NEW

	// M
	'moderation_donnees' => 'Valider les données avant publication :', # NEW
	'modifiable_donnees' => 'Donnés modifiables dans l\'espace public :', # NEW
	'monetaire_invalide' => 'Champ monétaire invalide', # NEW
	'monnaie_euro' => 'Euro (€)', # NEW
	'multiple_donnees' => 'Saisie des données dans l\'espace public :', # NEW

	// N
	'nb_decimales' => 'Nombre de décimales', # NEW
	'nombre_reponses' => '@nombre@ données', # NEW
	'nouveau_champ' => 'Nouveau champ', # NEW
	'nouveau_choix' => 'Nouveau choix', # NEW
	'nouveau_formulaire' => 'Nouveau formulaire', # NEW
	'numerique_invalide' => 'Champ numérique invalide', # NEW

	// P
	'page' => 'Page', # NEW
	'pas_mail_confirmation' => 'Pas de mail confirmation', # NEW
	'probleme_technique' => 'Problème technique. Votre saisie n\'a pas pu être prise en compte.', # NEW
	'probleme_technique_upload' => 'Problème technique. Le transfert du fichier a échoué.', # NEW
	'publication_donnees' => 'Publication des données', # NEW

	// R
	'rang' => 'Rang', # NEW
	'remplir_un_champ' => 'Veuillez remplir au moins un champ.', # NEW
	'reponse' => 'Reponse @id_reponse@', # NEW
	'reponse_depuis' => 'Depuis la page ', # NEW
	'reponse_enregistree' => 'Votre saisie a été enregistrée.', # NEW
	'reponse_envoyee' => 'Réponse envoyée le ', # NEW
	'reponse_envoyee_a' => 'à', # NEW
	'reponse_retrovez' => 'Retrouvez cette réponse dans l\'interface d\'administration :', # NEW
	'reponses' => 'réponses', # NEW
	'resultats' => 'Résultats : ', # NEW

	// S
	'site_introuvable' => 'Ce site n\'a pas été trouvé.', # NEW
	'sondage_deja_repondu' => 'Vous avez déjà répondu a ce sondage.', # NEW
	'sondage_non' => 'Ce formulaire n\'est pas un sondage', # NEW
	'sondage_oui' => 'Ce formulaire est un sondage.', # NEW
	'suivi_formulaire' => 'Suivi du formulaire', # NEW
	'suivi_formulaires' => 'Suivi des formulaires', # NEW
	'suivi_reponses' => 'Suivi des réponses', # NEW
	'supprimer' => 'Supprimer', # NEW
	'supprimer_champ' => 'Supprimer ce champ', # NEW
	'supprimer_choix' => 'supprimer ce choix', # NEW
	'supprimer_formulaire' => 'Supprimer ce formulaire', # NEW
	'supprimer_reponse' => 'Supprimer cette réponse', # NEW

	// T
	'tables' => 'Tables', # NEW
	'taille_max' => 'Taille maximale (en ko)', # NEW
	'telecharger' => 'Télécharger', # NEW
	'telecharger_reponses' => 'Télécharger les réponses', # NEW
	'titre_formulaire' => 'Titre du formulaire', # NEW
	'total_votes' => 'Total des votes', # NEW
	'tous_formulaires' => 'Tous les formulaires', # NEW
	'tous_sondages' => 'Tous les sondages', # NEW
	'tous_sondages_proteges' => 'Tous les sondages protégés', # NEW
	'tous_sondages_public' => 'Tous les sondages publics', # NEW
	'toutes_tables' => 'Tous les formulaires', # NEW
	'type_form' => 'Type de formulaire', # NEW

	// U
	'une_reponse' => 'une donnée', # NEW
	'unite_monetaire' => 'Unité monétaire', # NEW

	// V
	'valider' => 'Valider', # NEW
	'verif_web' => 'vérifier l\'existence du site Web', # NEW
	'vider' => 'Vider', # NEW
	'voir_article' => 'Voir l\'article', # NEW
	'voir_resultats' => 'Voir les résultats' # NEW
);

?>
