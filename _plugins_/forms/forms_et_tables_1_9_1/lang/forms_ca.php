<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/forms?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_barre_typo' => 'Activar la barra tipogràfica',
	'adresse_invalide' => 'Aquesta adreça no és vàlida',
	'afficher' => 'Mostrar',
	'aide_contextuelle' => 'Ajuda contextual ',
	'ajouter_champ' => 'Afegir un camp',
	'ajouter_champ_type' => 'Crear un camp de tipus:',
	'ajouter_choix' => 'Afegir una elecció',
	'apparence_formulaire' => 'Aparença del formulari d\'entrada de dades',
	'article_inserer_un_formulaire' => 'Inserir un formulari',
	'article_inserer_un_formulaire_detail' => 'Podeu inserir formularis als vostres articles per tal de permetre als visitants entrar informacions. Escolliu un formulari de la llista que hi ha més avall i copieu la drecera al text de l\'article.',
	'article_recopier_raccourci' => 'Per inserir aquest formulari, copieu aquesta drecera al text de l\'article',
	'articles_utilisant' => 'Articles que utilitzen aquest formulari',
	'attention' => 'Atenció:',
	'aucune_reponse' => 'cap dada',
	'avis_message_confirmation' => 'S\'ha enviat un missatge de confirmació a @mail@',

	// B
	'boite_info' => 'Feu un clic damunt les icones per veure les dades associades a un formulari, editar-lo, duplicar-lo, exportar-lo, buidar les dades o suprimir-lo. ',

	// C
	'cfg_activer' => 'Si',
	'cfg_associer_donnees' => 'Associar les dades de les taules',
	'cfg_associer_donnees_articles' => 'Permetre associar les dades als articles:',
	'cfg_associer_donnees_auteurs' => 'Permetre associar les dades als autors:',
	'cfg_associer_donnees_rubriques' => 'Permetre associar les dades a les seccions:',
	'cfg_bouton_type_image' => 'botó de tipus imatge',
	'cfg_bouton_type_texte' => 'botó de tipus text (submit)',
	'cfg_bouton_valider' => 'Tipus de botó validar',
	'cfg_bouton_valider_texte' => 'Podeu precisar quin tipus de botó validar utilitzareu als formularis:',
	'cfg_desactiver' => 'No',
	'cfg_inserer_head' => 'Inserció al &lt;head&gt;',
	'cfg_inserer_head_texte' => 'Inserir automàticament les crides als fitxers següents en la secció &lt;head&gt; de les pàgines del lloc (recomanat):',
	'champ_descendre' => 'baixar',
	'champ_email_details' => 'Entreu un correu electrònic vàlid (del tipus vosaltres@hostatjador.com).',
	'champ_listable_admin' => 'Visualitzar aquest camps a les llistes de l\'espai privat',
	'champ_listable_publique' => 'Visualitzar aquest camp a les llistes públiques',
	'champ_monter' => 'pujar',
	'champ_necessaire' => 'Aquest camp s\'ha d\'omplir.',
	'champ_nom' => 'Nom del camp',
	'champ_nom_bloc' => 'Nom del bloc',
	'champ_nom_groupe' => 'Grup',
	'champ_nom_texte' => 'Text',
	'champ_public' => 'Aquest camp és visible a l\'espai públic',
	'champ_saisie_desactivee' => 'Desactivar l\'entrada de dades d\'aquest camp',
	'champ_specifiant' => 'Aquest camp qualifica la dada (classificació, filtre, descripció)',
	'champ_table_jointure_type' => 'Tipus de taula per les connexions',
	'champ_type_date' => 'Data',
	'champ_type_email' => 'Correu electrònic',
	'champ_type_fichier' => 'Fitxer per descarregar',
	'champ_type_joint' => 'Connexió amb una altra taula',
	'champ_type_ligne' => 'Línia de text',
	'champ_type_monnaie' => 'Monetari',
	'champ_type_mot' => 'Paraules clau',
	'champ_type_multiple' => 'Elecció múltiple',
	'champ_type_numerique' => 'Numèric',
	'champ_type_password' => 'Contrasenya',
	'champ_type_select' => 'Elecció única',
	'champ_type_separateur' => 'Nou bloc de preguntes',
	'champ_type_texte' => 'Text',
	'champ_type_textestatique' => 'Missatge d\'explicació',
	'champ_type_url' => 'Adreça del lloc Web',
	'champ_url_details' => 'Entreu una adreça Web vàlida (del tipus http://www.elmeulloc.com/...).',
	'champs_formulaire' => 'Camps del formulari',
	'changer_choix_multiple' => 'Canviar a elecció múltiple',
	'changer_choix_unique' => 'Canviar a elecció única',
	'choisir_email' => 'Escollir el correu electrònic en funció de',
	'confirm_supprimer_champ' => 'Segur que voleu suprimir el camp \'@champ@\'?',
	'confirm_supprimer_donnee' => 'Segur que voleu suprimir la dada \'@donnee@\' ?',
	'confirm_vider_table' => 'Segur que voleu buidar el contingut de la taula \'@table@\'?',
	'confirmer_champ_password' => 'Anota si es doble l\'entrada de dades ',
	'confirmer_password' => 'Confirmació',
	'confirmer_reponse' => 'Enviar un correu electrònic de justificant de recepció amb:',
	'csv_classique' => 'CSV clàssic (,)',
	'csv_excel' => 'CSV per Excel (;)',
	'csv_tab' => 'CSV amb tabulacions',

	// D
	'date' => 'Data',
	'date_invalide' => 'Format de la data invàlid ',
	'donnees_modifiable' => 'Dades modificables per l\'usuari.',
	'donnees_multiple' => 'Respostes múltiples.',
	'donnees_nonmodifiable' => 'Dades no modificables per l\'usuari.',
	'donnees_nonmultiple' => 'Resposta única.',
	'donnees_prot' => 'Dades protegides. Les dades enregistrades només seran accessibles des de la interfície privada.',
	'donnees_pub' => 'Dades públiques. Les dades enregistrades seran accessibles als visitants del lloc. ',
	'dupliquer' => 'Duplicar',

	// E
	'echec_upload' => 'La transferència del fitxer ha fracassat.',
	'edit_champ_obligatoire' => 'Aquest camp és obligatori',
	'editer' => 'Editar',
	'email_independant' => 'Correu electrònic independent de la resposta',
	'exporter' => 'Exportar',
	'exporter_article' => 'Exportar a dins d\'un article',

	// F
	'fichier_trop_gros' => 'Aquest fitxer és massa pesant.',
	'fichier_type_interdit' => 'Aquest tipus de fitxer està prohibit.',
	'form_erreur' => 'Error:',
	'format_fichier' => 'Format del fitxer:',
	'format_liste' => 'desenvolupant',
	'format_liste_ou_radio' => 'Format de la llista',
	'format_radio' => 'botons de grup',
	'forms_obligatoires' => 'Formularis obligatoris per l\'entrada de dades d\'aquest:',
	'formulaire' => 'Formulari',
	'formulaire_aller' => 'Anar al formulari ',
	'formulaires_copie' => 'Còpia de @nom@',
	'formulaires_sondages' => 'Formularis i sondejos',

	// H
	'html_wrapper' => 'Encapsular el camp en el codi html',

	// I
	'icone_ajouter_donnees' => 'Afegir respostes',
	'icone_creer_formulaire' => 'Crear un nou formulari',
	'icone_creer_table' => 'Crear un nou formulari',
	'importer_form' => 'Importar un formulari',
	'info_apparence' => 'Aquí teniu una visualització prèvia del formulari tal i com apareixerà als visitants del lloc públic.',
	'info_articles_lies_donnee' => 'Els articles lligats',
	'info_champs_formulaire' => 'Podeu crear aquí i modificar els camps que els visitants podran omplir.',
	'info_obligatoire_02' => '[Obligatori]',
	'info_rubriques_liees_donnee' => 'Les seccions lligades',
	'info_sondage' => 'Si el vostre formulari és un sondeig, els resultats dels camps del tipus «selecció» s\'afegiran i es mostraran. ',
	'info_supprimer_formulaire' => 'Realment voleu suprimir aquest formulari?',
	'info_supprimer_formulaire_reponses' => 'Hi ha dades associades a aquest formulari. Realment voleu suprimir-lo?',

	// L
	'lien_apercu' => 'Visualització prèvia',
	'lien_champ' => 'Camps',
	'lien_propriete' => 'Propietats',
	'lien_retirer_donnee_liante' => 'Treure l\'enllaç',
	'lien_retirer_donnee_liee' => 'Treure l\'enllaç',
	'lier_articles' => 'Permetre associar les dades als articles',
	'lier_documents' => 'Permetre adjuntar documents a les dades ',
	'lier_documents_mail' => 'Adjuntar els documents al correu electrònic',
	'liste_choix' => 'Llista de les eleccions proposades',

	// M
	'moderation_donnees' => 'Validar les dades abans de la publicació:',
	'modifiable_donnees' => 'Dades modificables a l\'espai públic:',
	'monetaire_invalide' => 'Camp monetari invàlid',
	'monnaie_euro' => 'Euro (€)',
	'multiple_donnees' => 'Entrada de dades a l\'espai públic:',

	// N
	'nb_decimales' => 'Número de decimals',
	'nombre_reponses' => '@nombre@ dades',
	'nouveau_champ' => 'Nou camp',
	'nouveau_choix' => 'Nova elecció ',
	'nouveau_formulaire' => 'Nou formulari',
	'numerique_invalide' => 'Camp numèric invàlid',

	// P
	'page' => 'Pàgina',
	'pas_mail_confirmation' => 'Cap correu electrònic de confirmació',
	'probleme_technique' => 'Problema tècnic. La vostra entrada de dades no s\'ha pogut tenir en compte.',
	'probleme_technique_upload' => 'Problema tècnic. La transferència del fitxer ha fracassat.',
	'publication_donnees' => 'Publicació de dades',

	// R
	'rang' => 'Rang',
	'remplir_un_champ' => 'Vulgueu omplir com a mínim un camp.',
	'reponse' => 'Resposta @id_reponse@',
	'reponse_depuis' => 'Des de la pàgina',
	'reponse_enregistree' => 'La vostra entrada de dades ha estat enregistrada.',
	'reponse_envoyee' => 'Resposta enviada el',
	'reponse_envoyee_a' => 'a',
	'reponse_retrovez' => 'Recupereu aquesta resposta a la interfície d\'administració:',
	'reponses' => 'respostes',
	'resultats' => 'Resultats: ',

	// S
	'site_introuvable' => 'Aquest lloc no s\'ha trobat.',
	'sondage_deja_repondu' => 'Ja heu respost a aquest sondeig.',
	'sondage_non' => 'Aquest formulari no és un sondeig',
	'sondage_oui' => 'Aquest formulari és un sondeig.',
	'suivi_formulaire' => 'Seguiment del formulari',
	'suivi_formulaires' => 'Seguiment dels formularis',
	'suivi_reponses' => 'Seguiment de respostes',
	'supprimer' => 'Suprimir',
	'supprimer_champ' => 'Suprimir aquest camp',
	'supprimer_choix' => 'suprimir aquesta elecció',
	'supprimer_formulaire' => 'Suprimir aquest formulari',
	'supprimer_reponse' => 'Suprimir aquesta resposta',

	// T
	'tables' => 'Taules',
	'taille_max' => 'Mida màxima (en kb)',
	'telecharger' => 'Baixar',
	'telecharger_reponses' => 'Baixar les respostes',
	'titre_formulaire' => 'Títol del formulari',
	'total_votes' => 'Total de vots',
	'tous_formulaires' => 'Tots els formularis',
	'tous_sondages' => 'Tots els sondejos ',
	'tous_sondages_proteges' => 'Tots els sondejos protegits',
	'tous_sondages_public' => 'Tots els sondejos públics',
	'toutes_tables' => 'Tots els formularis',
	'type_form' => 'Tipus de formulari',

	// U
	'une_reponse' => 'una dada',
	'unite_monetaire' => 'Unitat monetària',

	// V
	'valider' => 'Validar',
	'verif_web' => 'verificar l\'existència del lloc Web',
	'vider' => 'Buidar',
	'voir_article' => 'Veure l\'article',
	'voir_resultats' => 'Veure els resultats'
);

?>
