<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/table?lang_cible=ast
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Artículos qu\'usen esta tabla',
	'aucune_reponse' => 'Dengún datu',

	// C
	'champs_formulaire' => 'Campos de la tabla',

	// F
	'formulaire' => 'Tabla',

	// I
	'icone_ajouter_donnees' => 'Amestar datos',
	'icone_creer_table' => 'Crear una tabla nueva',
	'importer_donnees_csv' => 'Importar datos',
	'info_supprimer_formulaire' => '¿De verdá quies desaniciar esta tabla?',
	'info_supprimer_formulaire_reponses' => 'Hai datos asociaos con esta tabla. ¿De verdá quies desaniciala?',

	// L
	'lien_retirer_donnee_liante' => 'Quitar l\'enllaz d\'esti datu',
	'lien_retirer_donnee_liee' => 'Desaniciar esti datu',

	// N
	'nombre_reponses' => '@nombre@ datos',
	'nouveau_formulaire' => 'Tabla nueva',

	// S
	'suivi_reponses' => 'Ver los datos',
	'supprimer_formulaire' => 'Desaniciar esta tabla',

	// T
	'telecharger_reponses' => 'Descargar los datos',
	'texte_donnee_statut' => 'Estáu d\'esti datu',
	'texte_statut_poubelle' => 'Datu desaniciáu',
	'texte_statut_prepa' => 'Datu en cursu de redaición',
	'texte_statut_prop' => 'Datu propuestu',
	'texte_statut_publie' => 'Datu espublizáu',
	'texte_statut_refuse' => 'Datu refugáu',
	'titre_formulaire' => 'Títulu de la tabla',
	'toutes_tables' => 'Toes les tables',
	'type_des_tables' => 'Tables',

	// U
	'une_reponse' => 'Un datu'
);

?>
