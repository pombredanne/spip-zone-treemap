<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/form?lang_cible=ro
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Articole folosind acest formular',
	'aucune_reponse' => 'Nici un răspuns',

	// C
	'champs_formulaire' => 'Câmpurile formularului',

	// F
	'formulaire' => 'Formular',

	// I
	'icone_ajouter_donnees' => 'Adăugaţi răspunsuri',
	'icone_creer_table' => 'Creaţi un nou formular',
	'importer_donnees_csv' => 'Importaţi răspunsuri',
	'info_supprimer_formulaire' => 'Vreţi cu adevărat să ştergeţi acest formular ?',
	'info_supprimer_formulaire_reponses' => 'Există răspunsuri asociate cu acest forumalar.
 Vreţi cu adevărat să îl ştergeţi ?',

	// L
	'lien_retirer_donnee_liante' => 'Retrageţi legătura de la acest răspuns',
	'lien_retirer_donnee_liee' => 'Retrageţi acest răspuns',

	// N
	'nombre_reponses' => '@nombre@ răspunsuri',
	'nouveau_formulaire' => 'Formular nou',

	// S
	'suivi_reponses' => 'Vedeţi răspunsurile',
	'supprimer_formulaire' => 'Ştergeţi acest forumlar',

	// T
	'telecharger_reponses' => 'Descărcaţi răspunsurile',
	'texte_donnee_statut' => 'Starea acestui răspuns',
	'texte_statut_poubelle' => 'Răspuns şters',
	'texte_statut_prepa' => 'Răspuns în curs de redactare',
	'texte_statut_prop' => 'Răspuns propus',
	'texte_statut_publie' => 'Răspuns publicat',
	'texte_statut_refuse' => 'Răspuns refuzat',
	'titre_formulaire' => 'Titlul formularului',
	'toutes_tables' => 'Toate formularele',
	'type_des_tables' => 'Formulare',

	// U
	'une_reponse' => 'Un răspuns'
);

?>
