<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/forms/forms_et_tables_1_9_1/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Articles utilisant ce formulaire',
	'aucune_reponse' => 'Aucune réponse',

	// C
	'champs_formulaire' => 'Champs du formulaire',

	// F
	'formulaire' => 'Formulaire',

	// I
	'icone_ajouter_donnees' => 'Ajouter des réponses',
	'icone_creer_table' => 'Créer un nouveau formulaire',
	'importer_donnees_csv' => 'Importer des réponses',
	'info_supprimer_formulaire' => 'Voulez-vous vraiment supprimer ce formulaire ?',
	'info_supprimer_formulaire_reponses' => 'Des Réponse sont associées à ce formulaire. 
Voulez-vous vraiment le supprimer ?',

	// L
	'lien_retirer_donnee_liante' => 'Retirer le lien depuis cette réponse',
	'lien_retirer_donnee_liee' => 'Retirer cette réponse',

	// N
	'nombre_reponses' => '@nombre@ réponses',
	'nouveau_formulaire' => 'Nouveau formulaire',

	// S
	'suivi_reponses' => 'Voir les réponses',
	'supprimer_formulaire' => 'Supprimer ce formulaire',

	// T
	'telecharger_reponses' => 'Téléchargez les réponses',
	'texte_donnee_statut' => 'Statut de cette réponse',
	'texte_statut_poubelle' => 'Réponse supprimée',
	'texte_statut_prepa' => 'Réponse en cours de rédaction',
	'texte_statut_prop' => 'Réponse proposée',
	'texte_statut_publie' => 'Réponse publiée',
	'texte_statut_refuse' => 'Réponse refusée',
	'titre_formulaire' => 'Titre du formulaire',
	'toutes_tables' => 'Tous les formulaires',
	'type_des_tables' => 'Formulaires',

	// U
	'une_reponse' => 'Une réponse'
);

?>
