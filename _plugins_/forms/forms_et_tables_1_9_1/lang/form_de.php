<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/form?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Artikel, die dieses Formular verwenden',
	'aucune_reponse' => 'Keine Antwort',

	// C
	'champs_formulaire' => 'Felder des Formulars',

	// F
	'formulaire' => 'Formular',

	// I
	'icone_ajouter_donnees' => 'Antworten hinzufügen',
	'icone_creer_table' => 'Neues Formular anlegen',
	'importer_donnees_csv' => 'Antworten importieren',
	'info_supprimer_formulaire' => 'Möchten Sie das Formular wirklich löschen?',
	'info_supprimer_formulaire_reponses' => 'Zu diesem Formular gehören Antworten.
Möchten Sie es wirklich löschen?',

	// L
	'lien_retirer_donnee_liante' => 'Link aus dieser Antwort entfernen',
	'lien_retirer_donnee_liee' => 'Antwort löschen',

	// N
	'nombre_reponses' => '@nombre@ Antworten',
	'nouveau_formulaire' => 'Neues Formular',

	// S
	'suivi_reponses' => 'Antworten ansehen',
	'supprimer_formulaire' => 'Formular löschen',

	// T
	'telecharger_reponses' => 'Antworten herunterladen',
	'texte_donnee_statut' => 'Status der Antwort',
	'texte_statut_poubelle' => 'Antwort gelöscht',
	'texte_statut_prepa' => 'Antwort in Bearbeitung',
	'texte_statut_prop' => 'vorgeschlagene Antwort',
	'texte_statut_publie' => 'veröffentlichte Antwort',
	'texte_statut_refuse' => 'abgelehnte Antwort',
	'titre_formulaire' => 'Titel des Formulars',
	'toutes_tables' => 'Alle Formulare',
	'type_des_tables' => 'Formulare',

	// U
	'une_reponse' => 'Eine Antwort'
);

?>
