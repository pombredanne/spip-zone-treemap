<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Articles utilisant ce formulaire', # NEW
	'aucune_reponse' => 'Aucune réponse', # NEW

	// C
	'champs_formulaire' => 'Champs du formulaire', # NEW

	// F
	'formulaire' => 'Formulaire', # NEW

	// I
	'icone_ajouter_donnees' => 'Ajouter des réponses', # NEW
	'icone_creer_table' => 'Créer un nouveau formulaire', # NEW
	'importer_donnees_csv' => 'Importer des réponses', # NEW
	'info_supprimer_formulaire' => 'Vill du verkligen radera formuläret ?',
	'info_supprimer_formulaire_reponses' => 'Des Réponse sont associées à ce formulaire. 
Voulez-vous vraiment le supprimer ?', # NEW

	// L
	'lien_retirer_donnee_liante' => 'Retirer le lien depuis cette réponse', # NEW
	'lien_retirer_donnee_liee' => 'Retirer cette réponse', # NEW

	// N
	'nombre_reponses' => '@nombre@ réponses', # NEW
	'nouveau_formulaire' => 'Nouveau formulaire', # NEW

	// S
	'suivi_reponses' => 'Voir les réponses', # NEW
	'supprimer_formulaire' => 'Supprimer ce formulaire', # NEW

	// T
	'telecharger_reponses' => 'Téléchargez les réponses', # NEW
	'texte_donnee_statut' => 'Statut de cette réponse', # NEW
	'texte_statut_poubelle' => 'Réponse supprimée', # NEW
	'texte_statut_prepa' => 'Réponse en cours de rédaction', # NEW
	'texte_statut_prop' => 'Réponse proposée', # NEW
	'texte_statut_publie' => 'Réponse publiée', # NEW
	'texte_statut_refuse' => 'Réponse refusée', # NEW
	'titre_formulaire' => 'Titre du formulaire', # NEW
	'toutes_tables' => 'Tous les formulaires', # NEW
	'type_des_tables' => 'Formulaires', # NEW

	// U
	'une_reponse' => 'Une réponse' # NEW
);

?>
