<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => '<NEW>Pennadoù a implij an daolenn-se',
	'aucune_reponse' => '<NEW>Roadenn ebet',

	// C
	'champs_formulaire' => '<NEW>Maeziennoù an daolenn',

	// F
	'formulaire' => '<NEW>Taolenn',

	// I
	'icone_ajouter_donnees' => '<NEW>Ouzhpennañ roadennoù',
	'icone_creer_table' => '<NEW>Krouiñ un daolenn nevez',
	'importer_donnees_csv' => '<NEW>Enporzhiañ roadennoù',
	'info_supprimer_formulaire' => '<NEW>Ha sur oc\'h e fell deoc\'h diverkañ an daolenn-mañ?',
	'info_supprimer_formulaire_reponses' => '<NEW>Roadennoù zo stag ouzh an daolenn-mañ. Diverkañ memes tra?',

	// L
	'lien_retirer_donnee_liante' => '<NEW>Lemel an liamm kuit adalek ar roadenn-mañ',
	'lien_retirer_donnee_liee' => '<NEW>Lemel ar roadenn-mañ kuit',

	// N
	'nombre_reponses' => '<NEW>@nombre@ roadenn',
	'nouveau_formulaire' => '<NEW>Taolenn nevez',

	// S
	'suivi_reponses' => '<NEW>Gwelet ar roadennoù',
	'supprimer_formulaire' => '<NEW>Diverkañ an daolenn-mañ',

	// T
	'telecharger_reponses' => '<NEW>Pellgargañ ar roadennoù',
	'texte_donnee_statut' => '<NEW>Statud ar roadenn-mañ',
	'texte_statut_poubelle' => '<NEW>Roadenn bet diverket',
	'texte_statut_prepa' => '<NEW>O skridaozañ ar roadenn',
	'texte_statut_prop' => '<NEW>Roadenn kinniget',
	'texte_statut_publie' => '<NEW>Roadenn embannet',
	'texte_statut_refuse' => '<NEW>Roadenn nac\'het',
	'titre_formulaire' => '<NEW>Titl an daolenn',
	'toutes_tables' => '<NEW>An holl daolennoù',
	'type_des_tables' => '<NEW>Taolennoù',

	// U
	'une_reponse' => '<NEW>Ur roadenn'
);

?>
