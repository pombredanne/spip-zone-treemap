<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/forms?lang_cible=gl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_barre_typo' => 'Activar a barra tipográfica',
	'adresse_invalide' => 'Este enderezo non é válido',
	'afficher' => 'Mostrar',
	'aide_contextuelle' => 'Axuda contextual',
	'ajouter_champ' => 'Engadir un campo',
	'ajouter_champ_type' => 'Crear un campo de tipo :',
	'ajouter_choix' => 'Engadir unha opción',
	'apparence_formulaire' => 'Apariencia do formulario de saída',
	'article_inserer_un_formulaire' => 'Inserir un formulario',
	'article_inserer_un_formulaire_detail' => 'Pode inserir formularios nos seus artigos co fin de permitirlles aos visitantes introducir informacións. Escolla un formulario na lista seguinte e recopie o resumo no texto do artigo.',
	'article_recopier_raccourci' => 'Recopie o resumo (atallo) no texto do artigo para inserir este formulario',
	'articles_utilisant' => 'Artigo que usan este formulario',
	'attention' => 'Atención :',
	'aucune_reponse' => 'ningún dato',
	'avis_message_confirmation' => 'Enviouse unha mensaxe de confirmación a @mail@',

	// B
	'boite_info' => 'Prema sobre as íconas para ver os datos asociados a un formulario, editalo, duplicalo, exportalo, baleirar os datos ou suprimilo.',

	// C
	'cfg_activer' => 'Si',
	'cfg_associer_donnees' => 'Asociar os datos das táboas',
	'cfg_associer_donnees_articles' => 'Permitir asociar os datos aos artigos :',
	'cfg_associer_donnees_auteurs' => 'Permitir asociar os datos aos autores :',
	'cfg_associer_donnees_rubriques' => 'Permitir asociar os datos ás seccións :',
	'cfg_bouton_type_image' => 'botón de tipo imaxe',
	'cfg_bouton_type_texte' => 'botón de tipo texto (submit)',
	'cfg_bouton_valider' => 'Tipo de botón validar',
	'cfg_bouton_valider_texte' => 'Pode precisar que tipo de botón validar quere usar nos formularios :',
	'cfg_desactiver' => 'Non',
	'cfg_inserer_head' => 'Inserción no &lt;head&gt;',
	'cfg_inserer_head_texte' => 'Inserir automaticamente as chamadas aos ficheiros seguintes na sección &lt;head&gt; das páxinas do web (recomendado) :',
	'champ_descendre' => 'descender',
	'champ_email_details' => 'Introduza un e-mail válido (do tipo eu@dominio.com).',
	'champ_listable_admin' => 'Mostrar este campo nas listas do espazo privado',
	'champ_listable_publique' => 'Mostrar este campo nas listas públicas',
	'champ_monter' => 'montar',
	'champ_necessaire' => 'Este campo debe ser cuberto.',
	'champ_nom' => 'Nome do campo',
	'champ_nom_bloc' => 'Nome do bloque',
	'champ_nom_groupe' => 'Grupo',
	'champ_nom_texte' => 'Texto',
	'champ_public' => 'Este campo é visible no espazo público',
	'champ_saisie_desactivee' => 'Desactivar a saída deste campo',
	'champ_specifiant' => 'Este campo cualifica os datos (tri, filtro, descrición)',
	'champ_table_jointure_type' => 'Tipo de táboa para as consultas sql (join)',
	'champ_type_date' => 'Data',
	'champ_type_email' => 'Enderezo de correo-e',
	'champ_type_fichier' => 'Ficheiro para descarga',
	'champ_type_joint' => 'Consulta combinada con outra táboa (join)',
	'champ_type_ligne' => 'Liña de texto',
	'champ_type_monnaie' => 'Monetario',
	'champ_type_mot' => 'Palabras-clave',
	'champ_type_multiple' => 'Elección múltiple',
	'champ_type_numerique' => 'Numérico',
	'champ_type_password' => 'Palabra clave',
	'champ_type_select' => 'Elección única',
	'champ_type_separateur' => 'Novo bloque de preguntas',
	'champ_type_texte' => 'Texto',
	'champ_type_textestatique' => 'Mensaxe explicativo',
	'champ_type_url' => 'Enderezo do web',
	'champ_url_details' => 'Introduza un enderezo web válido(do tipo http://www.meuweb.com/...).',
	'champs_formulaire' => 'Campos do formulario',
	'changer_choix_multiple' => 'Cambiar a elección múltiple',
	'changer_choix_unique' => 'Cambiar a elección múltiple',
	'choisir_email' => 'Escoller o correo-e en función de',
	'confirm_supprimer_champ' => 'Está certo de querer suprimir os campo \'@champ@\' ?',
	'confirm_supprimer_donnee' => 'Está certo de querer suprimir o dato \'@donnee@\' ?',
	'confirm_vider_table' => 'Está certo de querer suprimir o contido da táboa \'@table@\' ?',
	'confirmer_champ_password' => 'Texto (libelle) a dobre saída',
	'confirmer_password' => 'Confirmación',
	'confirmer_reponse' => 'Enviar un correo de aviso de recepción con :',
	'csv_classique' => 'CSV clásico (,)',
	'csv_excel' => 'CSV para Excel (;)',
	'csv_tab' => 'CSV con tabuladores',

	// D
	'date' => 'Data',
	'date_invalide' => 'O formato da data non é válido',
	'donnees_modifiable' => 'Datos modificables polo usuario.',
	'donnees_multiple' => 'Respostas múltiples.',
	'donnees_nonmodifiable' => 'Datos non modificables polo usuario.',
	'donnees_nonmultiple' => 'Resposta única.',
	'donnees_prot' => 'Datos protexidos. Os datos rexistrados non serán accesibles senón a través do espazo privado.',
	'donnees_pub' => 'Datos públicos. Os datos rexistrados serán accesibles para os visitantes do web.',
	'dupliquer' => 'Duplicar',

	// E
	'echec_upload' => 'Fallou a transferencia do ficheiro.',
	'edit_champ_obligatoire' => 'Este campo é obrigado',
	'editer' => 'Editar',
	'email_independant' => 'Correo-e independente da resposta',
	'exporter' => 'Exportar',
	'exporter_article' => 'Exportar nuna artigo',

	// F
	'fichier_trop_gros' => 'Este ficheiro é demasiado groso.',
	'fichier_type_interdit' => 'Este tipo de ficheiro está prohibido.',
	'form_erreur' => 'Erro :',
	'format_fichier' => 'Formato do ficheiro :',
	'format_liste' => 'desenvolvente',
	'format_liste_ou_radio' => 'Formato da lista',
	'format_radio' => 'botóns radio',
	'forms_obligatoires' => 'Formularios obrigados para a saída destes :',
	'formulaire' => 'Formulario',
	'formulaire_aller' => 'Ir ao formulario',
	'formulaires_copie' => 'Copia de @nom@',
	'formulaires_sondages' => 'Formularios e sondaxes',

	// H
	'html_wrapper' => 'Encapsular o campo no código html',

	// I
	'icone_ajouter_donnees' => 'Engadir respostas',
	'icone_creer_formulaire' => 'Crear un novo formulario',
	'icone_creer_table' => 'Crear un novo formulario',
	'importer_form' => 'Importar un formulario',
	'info_apparence' => 'Velaquí unha previsualización do formulario tal cal vai aparecer para os visitantes do web público.',
	'info_articles_lies_donnee' => 'Os artigos ligados',
	'info_champs_formulaire' => 'Pode crear aquí e modificar os campos que os visitantes poderán cubrir.',
	'info_obligatoire_02' => '[Obrigatorio]',
	'info_rubriques_liees_donnee' => 'As seccións ligadas',
	'info_sondage' => 'Se o seu formulario é unha sondaxe, os resultados dos campos de tipo « selección » serán engadidos e mostrados.',
	'info_supprimer_formulaire' => 'Está certo de que quere suprimir este formulario ?',
	'info_supprimer_formulaire_reponses' => 'Hai datos asociados a este formulario. Está certo de que o quere suprimir ?',

	// L
	'lien_apercu' => 'Advertencia',
	'lien_champ' => 'Campos',
	'lien_propriete' => 'Propiedades',
	'lien_retirer_donnee_liante' => 'Retirar a ligazón',
	'lien_retirer_donnee_liee' => 'Retirar a ligazón',
	'lier_articles' => 'Permite asociar os datos aos artigos',
	'lier_documents' => 'Permitir asociar documentos aos datos',
	'lier_documents_mail' => 'Anexar os documentos ao correo-e',
	'liste_choix' => 'Lista de eleccións propostas',

	// M
	'moderation_donnees' => 'Validar os datos antes da súa publicación :',
	'modifiable_donnees' => 'Datos modificables desde o espazo público :',
	'monetaire_invalide' => 'Campo de moeda inválido',
	'monnaie_euro' => 'Euro (€)',
	'multiple_donnees' => 'Saída de datos desde o espazo público :',

	// N
	'nb_decimales' => 'Número de decimais',
	'nombre_reponses' => '@nombre@ datos',
	'nouveau_champ' => 'Novo campo',
	'nouveau_choix' => 'Nova opción',
	'nouveau_formulaire' => 'Novo formulario',
	'numerique_invalide' => 'Campo de número inválido',

	// P
	'page' => 'Páxina',
	'pas_mail_confirmation' => 'Non hai correo de confirmación',
	'probleme_technique' => 'Problema técnico. A súa saída non foi posible tomala en conta.',
	'probleme_technique_upload' => 'Problema técnico. A transferencia do ficheiro fallou.',
	'publication_donnees' => 'Publicación de datos',

	// R
	'rang' => 'Rango',
	'remplir_un_champ' => 'Cubra cando menos un campo.',
	'reponse' => 'Resposta @id_reponse@',
	'reponse_depuis' => 'Logo da páxina',
	'reponse_enregistree' => 'A súa saída foi rexistrada.',
	'reponse_envoyee' => 'Resposta enviada o',
	'reponse_envoyee_a' => 'para',
	'reponse_retrovez' => 'Encontre esta resposta na interfaz de administración :',
	'reponses' => 'respostas',
	'resultats' => 'Resultados :',

	// S
	'site_introuvable' => 'Este web non foi atopado.',
	'sondage_deja_repondu' => 'Vostede xa respondeu a esta sondaxe.',
	'sondage_non' => 'Este formulario non é unha sondaxe',
	'sondage_oui' => 'Este formulario é unha sondaxe.',
	'suivi_formulaire' => 'Seguimento do formulario',
	'suivi_formulaires' => 'Seguimento dos formularios',
	'suivi_reponses' => 'Seguimento das respostas',
	'supprimer' => 'Suprimir',
	'supprimer_champ' => 'Suprimir o campo',
	'supprimer_choix' => 'Suprimir esta elección',
	'supprimer_formulaire' => 'Suprimir este formulario',
	'supprimer_reponse' => 'Suprimir esta resposta',

	// T
	'tables' => 'Táboas',
	'taille_max' => 'Tamaño máximo (en kb)',
	'telecharger' => 'Descargar',
	'telecharger_reponses' => 'Descargar as respostas',
	'titre_formulaire' => 'Título do formulario',
	'total_votes' => 'Total de votos',
	'tous_formulaires' => 'Todos os formualarios',
	'tous_sondages' => 'Todas as sondaxes',
	'tous_sondages_proteges' => 'Todas as sondaxes protexidas',
	'tous_sondages_public' => 'Todas as sondaxes públicas',
	'toutes_tables' => 'Todos os formularios',
	'type_form' => 'Tipo de formulario',

	// U
	'une_reponse' => 'un dato',
	'unite_monetaire' => 'Unidade monetaria',

	// V
	'valider' => 'Validar',
	'verif_web' => 'verificar a existencia do web',
	'vider' => 'Baleirar',
	'voir_article' => 'Ver o artigo',
	'voir_resultats' => 'Ver os resultados'
);

?>
