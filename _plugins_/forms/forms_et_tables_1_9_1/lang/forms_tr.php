<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/forms?lang_cible=tr
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_barre_typo' => 'Tipografik çubuğu aktive et',
	'adresse_invalide' => 'Bu adres geçerli değil.',
	'afficher' => 'Göster',
	'aide_contextuelle' => 'Konu ile ilgili yardım',
	'ajouter_champ' => 'Bir alan ekle',
	'ajouter_champ_type' => 'Şu tip bir alan oluştur :',
	'ajouter_choix' => 'Bir seçenek ekle',
	'apparence_formulaire' => 'Giriş formunun görünüşü',
	'article_inserer_un_formulaire' => 'Bir form ekle',
	'article_inserer_un_formulaire_detail' => 'Makalelerinize ziyaretçilerin doldurabileceği formlar ekleyebilirsiniz. Aşağıdaki listeden bir form seçiniz ve kısayolu makalenizin metnine kopyalayınız.',
	'article_recopier_raccourci' => 'Bu formu, makalenin metnine eklemek için bu kısayolu kopyalayınız',
	'articles_utilisant' => 'Bu formu kullanan makaleler',
	'attention' => 'Dikkat :',
	'aucune_reponse' => 'hiç bir veri ',
	'avis_message_confirmation' => 'Bir onay mesajı @mail@ adresine gönderildi',

	// B
	'boite_info' => 'Bir formala ilintili verileri görmek, formu düzenlemek, çoğaltmak, ihraç etmek, verileri boşaltmak veya formu kaldırmak için ikonlara tıklayınız.',

	// C
	'cfg_activer' => 'Evet',
	'cfg_associer_donnees' => 'Tablo verilerini ilişkilendiriniz',
	'cfg_associer_donnees_articles' => 'Verileri makalelerle ilişkilendirmeye izin ver :',
	'cfg_associer_donnees_auteurs' => 'Verileri yazarlarla ilişkilendirmeye izin ver :',
	'cfg_associer_donnees_rubriques' => 'Verileri başlıklarla ilişkilendirmeye izin ver :',
	'cfg_bouton_type_image' => 'resim tipindeki düğmeler',
	'cfg_bouton_type_texte' => 'metin tipindeki düğmeler (gönder)',
	'cfg_bouton_valider' => 'Onayla tipindeki düğmeler',
	'cfg_bouton_valider_texte' => 'Formlarda ne tip bir onay düğmesi kullanılacağını belirtebilirsiniz :',
	'cfg_desactiver' => 'Hayır',
	'cfg_inserer_head' => '&lt;head&gt e ekleme;',
	'cfg_inserer_head_texte' => 'Site sayfalarından şu dosyalara yapılan çağrıları otomatik olarak &lt;head&gt; bölümüne ekle (önerilir) :',
	'champ_descendre' => 'in',
	'champ_email_details' => 'Lütfen geçelri bi re-posta adresi, giriniz (isminiz@servissaglayiciniz.com gibi).',
	'champ_listable_admin' => 'Bu alanı özel alan listelerinde göster',
	'champ_listable_publique' => 'Bu alanı kamusal listelerde göster',
	'champ_monter' => 'çık',
	'champ_necessaire' => 'Bu alan doldurulmalıdır.',
	'champ_nom' => 'Alan ismi',
	'champ_nom_bloc' => 'Blok ismi',
	'champ_nom_groupe' => 'Grup',
	'champ_nom_texte' => 'Metin',
	'champ_public' => 'Bu alan kamusal alanda görüntülenebilir',
	'champ_saisie_desactivee' => 'Bu alana giriş yapılmasını engelle',
	'champ_specifiant' => 'Bu alan veriyi belirtir (sıralama, filtre, tanımlama)',
	'champ_table_jointure_type' => 'Birleştirmeler (jointure) için tablo tipi',
	'champ_type_date' => 'Tarih',
	'champ_type_email' => 'E-posta adresi',
	'champ_type_fichier' => 'İndirilecek dosya',
	'champ_type_joint' => 'Başka bir tabloyla birleştirme (JOİNTURE)',
	'champ_type_ligne' => 'Metin satırı',
	'champ_type_monnaie' => 'Parasal',
	'champ_type_mot' => 'Anahtar-sözcükler',
	'champ_type_multiple' => 'Çoktan seçmeli ',
	'champ_type_numerique' => 'Sayısal',
	'champ_type_password' => 'Şifre',
	'champ_type_select' => 'Tek seçenek',
	'champ_type_separateur' => 'Yeni soru bloğu',
	'champ_type_texte' => 'Metin',
	'champ_type_textestatique' => 'Açıklama mesajı',
	'champ_type_url' => 'Web sitesi adresi',
	'champ_url_details' => 'Geçerli bir Web adresi giriniz (http://www.monsite.com/... biçiminde).',
	'champs_formulaire' => 'Form alanları',
	'changer_choix_multiple' => 'Çoktan seçmeli hale getir',
	'changer_choix_unique' => 'Tek seçenekli hale getir',
	'choisir_email' => 'E-postayı şuna göre seçiniz',
	'confirm_supprimer_champ' => '\'@champ@\' alanını silmek istediğinizden emin misiniz ?',
	'confirm_supprimer_donnee' => '\'@donnee@\' verisini silmek istediğinizden emin misiniz ?',
	'confirm_vider_table' => '\'@table@\' tablosunun içeriğini boşaltmak istediğinizden emin misiniz ?',
	'confirmer_champ_password' => 'Çift giriş yapıldıysa',
	'confirmer_password' => 'Onay',
	'confirmer_reponse' => 'Bir alındı e-postası gönder :',
	'csv_classique' => 'CSV klasik (,)',
	'csv_excel' => 'Excel için CSV (;)',
	'csv_tab' => 'Tab ile CSV ',

	// D
	'date' => 'Tarih',
	'date_invalide' => 'Tarih formatı geçersiz',
	'donnees_modifiable' => 'Kullanıcı tarafından değiştirilebilen veriler.',
	'donnees_multiple' => 'Çoklu cevap.',
	'donnees_nonmodifiable' => 'Kullanıcı tarafından değiştirilemeyen veriler.',
	'donnees_nonmultiple' => 'Tekil (unique) cevap.',
	'donnees_prot' => 'Korunmuş veriler. Kaydedilen verilere sadece özel arayüzden erişilebilecek.',
	'donnees_pub' => 'Kamusal veriler. Kaydedilen veriler site ziyaretçilerine açık olacak.',
	'dupliquer' => 'Bir tane daha kopyala (Dupliquer)',

	// E
	'echec_upload' => 'Dosya transferi gerçekleşmedi.',
	'edit_champ_obligatoire' => 'Bu alan zorunlu',
	'editer' => 'Düzenle',
	'email_independant' => 'Cevaptan bağımsız e-posta',
	'exporter' => 'İhraç et',
	'exporter_article' => 'Bir makaleye ihraç et',

	// F
	'fichier_trop_gros' => 'Bu dosya çok büyük.',
	'fichier_type_interdit' => 'Bu dosya tipi yasak.',
	'form_erreur' => 'Hata :',
	'format_fichier' => 'Dosya formatı :',
	'format_liste' => 'kayan',
	'format_liste_ou_radio' => 'Liste formatı',
	'format_radio' => 'radyo düğmeleri',
	'forms_obligatoires' => 'Giriş için zorunlu formlar  :',
	'formulaire' => 'Form',
	'formulaire_aller' => 'Forma git',
	'formulaires_copie' => '@nom@ kopyalanması',
	'formulaires_sondages' => 'Formlar ve sondajlar',

	// H
	'html_wrapper' => 'Alanı html kodlarıyla çevrele',

	// I
	'icone_ajouter_donnees' => 'Cevap ekle',
	'icone_creer_formulaire' => 'Yeni bir form oluştur',
	'icone_creer_table' => 'Yeni bir form oluştur',
	'importer_form' => 'Bir form ithal et',
	'info_apparence' => 'Formu, kamusal site ziyaretçilerine görüneceği biçimde öngörüntüle. ',
	'info_articles_lies_donnee' => 'İlintili makaleler',
	'info_champs_formulaire' => 'Burada ziyaretçilerin doldurabileceği alanları oluşturabilir ve değiştirebilirsiniz.',
	'info_obligatoire_02' => '[Zorunlu]',
	'info_rubriques_liees_donnee' => 'İlintili başlıklar',
	'info_sondage' => 'Eğer formunuz bir sondaj ise « seçim » tipindeki alanlara ait sonuçlar toplanacak ve öyle gösterilecektir.',
	'info_supprimer_formulaire' => 'Bu formu gerçekten silmek istiyor musunuz ?',
	'info_supprimer_formulaire_reponses' => 'Bu formla ilintili veriler var. Gerçekten silmek istiyor musunuz  ?',

	// L
	'lien_apercu' => 'Öngörüntüle',
	'lien_champ' => 'Alanlar',
	'lien_propriete' => 'Özellikler',
	'lien_retirer_donnee_liante' => 'Bağı çıkart',
	'lien_retirer_donnee_liee' => 'Bağı çıkart',
	'lier_articles' => 'Verileri makalelere eklemeye izin ver',
	'lier_documents' => 'Belgeleri verilere eklemeye izin ver',
	'lier_documents_mail' => 'Belgeleri e-postata ekle',
	'liste_choix' => 'Önerilen seçim listesi',

	// M
	'moderation_donnees' => 'Yayınlamadan önce verileri geçerli kıl :',
	'modifiable_donnees' => 'Kamusal alanda değiştirilebilir veriler :',
	'monetaire_invalide' => 'Geçersiz para alanı',
	'monnaie_euro' => 'Euro (€)',
	'multiple_donnees' => 'Kamusal alanda veri girişi :',

	// N
	'nb_decimales' => 'Ondalık adedi',
	'nombre_reponses' => '@nombre@ veri',
	'nouveau_champ' => 'Yeni alan',
	'nouveau_choix' => 'yeni seçim',
	'nouveau_formulaire' => 'Yeni form',
	'numerique_invalide' => 'Geçersiz sayısal alan ',

	// P
	'page' => 'Sayfa',
	'pas_mail_confirmation' => 'Onay e-postası olmasın',
	'probleme_technique' => 'Teknik sorun. Girişiniz işleme konamadı.',
	'probleme_technique_upload' => 'Teknik sorun. Dosya transferi başarısız.',
	'publication_donnees' => 'Verilerin yayınlanması',

	// R
	'rang' => 'Sıra',
	'remplir_un_champ' => 'En azından bir alanı doldurunuz.',
	'reponse' => 'Cevap @id_reponse@',
	'reponse_depuis' => 'Sayfasından',
	'reponse_enregistree' => 'Girişiniz kaydedildi.',
	'reponse_envoyee' => 'Bu cevap şu tarihte gönderildi ',
	'reponse_envoyee_a' => 'şuraya ',
	'reponse_retrovez' => 'Bu cevabı yönetim arayüzünde bulunuz :',
	'reponses' => 'cevaplar',
	'resultats' => 'Sonuçlar : ',

	// S
	'site_introuvable' => 'Bu site bulunamadı',
	'sondage_deja_repondu' => 'Bu sondaja zaten cevap verdiniz.',
	'sondage_non' => 'Bu form bir sondaj değil',
	'sondage_oui' => 'Bu form bir sondaj.',
	'suivi_formulaire' => 'Formun izlenmesi',
	'suivi_formulaires' => 'Formların izlenmesi',
	'suivi_reponses' => 'Cevapların izlenmesi',
	'supprimer' => 'Sil',
	'supprimer_champ' => 'Bu alanı sil',
	'supprimer_choix' => 'bu seçeneği sil',
	'supprimer_formulaire' => 'Bu formu sil',
	'supprimer_reponse' => 'Bu cevabı sil',

	// T
	'tables' => 'Tablolar',
	'taille_max' => 'Maksimum boyut (kb)',
	'telecharger' => 'İndir',
	'telecharger_reponses' => 'Cevapları indir',
	'titre_formulaire' => 'Form başlığı',
	'total_votes' => 'Toplam oy',
	'tous_formulaires' => 'Tüm formlar',
	'tous_sondages' => 'Tüm sondajlar',
	'tous_sondages_proteges' => 'Korunan tüm formlar',
	'tous_sondages_public' => 'Tüm kamusal sondajlar',
	'toutes_tables' => 'Tüm formlar',
	'type_form' => 'Form tipi',

	// U
	'une_reponse' => 'bir veri',
	'unite_monetaire' => 'Para birimi',

	// V
	'valider' => 'Onayla',
	'verif_web' => 'Web sitesinin varlığını kontrol et',
	'vider' => 'Boşalt',
	'voir_article' => 'Makaleyi göster',
	'voir_resultats' => 'Sonuçları göster'
);

?>
