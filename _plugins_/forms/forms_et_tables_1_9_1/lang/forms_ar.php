<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/forms?lang_cible=ar
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_barre_typo' => 'تفعيل شريط ادوات الكتابة',
	'adresse_invalide' => 'هذا العنوان ليس صالحاً.',
	'afficher' => 'عرض',
	'aide_contextuelle' => 'تعليمات فورية',
	'ajouter_champ' => 'إضافة حقل',
	'ajouter_champ_type' => 'إنشاء حقل من نوع:',
	'ajouter_choix' => 'إضافة خيار',
	'apparence_formulaire' => 'شكل الاستمارة',
	'article_inserer_un_formulaire' => 'إدراج استمارة',
	'article_inserer_un_formulaire_detail' => 'يمكنك إدراج استمارات في مقالاتك للسماح للزوار بإدخال معلومات. يجب اختيار استمارة من اللائحة ادناه وتسخ امر الادراج في مادة المقال.',
	'article_recopier_raccourci' => 'يجب تسخ أمر الادراج هذا في مادة المقال لإدراج هذه الاستمارة',
	'articles_utilisant' => 'المقالات التي تستخدم هذه الاستمارة',
	'attention' => 'تحذير:',
	'aucune_reponse' => 'لا وجود لأي بيانات',
	'avis_message_confirmation' => 'تم إرسال رسالة تأكيد الى @mail@',

	// B
	'boite_info' => 'أنقر الأيقونات لعرض البيانات المرتبطة بالاستمارة او تحرير الاستمارة او تكرارها او تصديرها او تفريغها من بياناتها او حذفها.',

	// C
	'cfg_activer' => 'نعم',
	'cfg_associer_donnees' => 'ربط البيانات بجداول',
	'cfg_associer_donnees_articles' => 'السماح بربط البيانات بالمقالات:',
	'cfg_associer_donnees_auteurs' => 'السماح بربط البيانات بالمؤلفين:',
	'cfg_associer_donnees_rubriques' => 'السماح بربط البيانات بالأقسام:',
	'cfg_bouton_type_image' => 'زر من نوع صورة',
	'cfg_bouton_type_texte' => 'زر من نوع نص (إدخال)',
	'cfg_bouton_valider' => 'نوع زر الموافقة',
	'cfg_bouton_valider_texte' => 'يمكنك تحديد اي نوع من أزرار الموافقة يتم استخدامه في الاستمارات:',
	'cfg_desactiver' => 'كلا',
	'cfg_inserer_head' => 'إدراج في مقطع &lt;head&gt;',
	'cfg_inserer_head_texte' => 'إدراج آلي لنداءات الملفات التالية في مقطع &lt;head&gt; في صفحات الموقع (مفضل):',
	'champ_descendre' => 'الى الأسفل',
	'champ_email_details' => 'الرجاء إدخال عنوان بريد الكتروتي صالح (من نوع you@provider.com).',
	'champ_listable_admin' => 'عرض هذا الحقل في لوائح المجال الخاص',
	'champ_listable_publique' => 'عرض هذا الحقل في اللوائح العمومية',
	'champ_monter' => 'الى الأعلى',
	'champ_necessaire' => 'يجب ملء هدا الحقل.',
	'champ_nom' => 'اسم الحقل',
	'champ_nom_bloc' => 'اسم الكتلة',
	'champ_nom_groupe' => 'مجموعة',
	'champ_nom_texte' => 'نص',
	'champ_public' => 'هذا الحقل ظاهر في الموقع العمومي',
	'champ_saisie_desactivee' => 'تعطيل الادخال في هذا الحقل',
	'champ_specifiant' => 'هذا الحقل يصف المعلومة (فرز، ترشيح، وصف)',
	'champ_table_jointure_type' => 'نوع الجداول للوصل',
	'champ_type_date' => 'تاريخ',
	'champ_type_email' => 'عنوان بريد الكتروني',
	'champ_type_fichier' => 'ملف للتحميل',
	'champ_type_joint' => 'وصل بجدول آخر',
	'champ_type_ligne' => 'سطر نص',
	'champ_type_monnaie' => 'نقدي',
	'champ_type_mot' => 'مفاتيح',
	'champ_type_multiple' => 'خيار متعدد',
	'champ_type_numerique' => 'رقمي',
	'champ_type_password' => 'كلمة سر',
	'champ_type_select' => 'خيار فريد',
	'champ_type_separateur' => 'كنلة جديدة من الأسئلة',
	'champ_type_texte' => 'نص',
	'champ_type_textestatique' => 'رسالة توضيح',
	'champ_type_url' => 'عنوان موقع',
	'champ_url_details' => 'الرجاء إدخال عنوان موقع صالح (من نوع http://www.mysite.com/...).',
	'champs_formulaire' => 'حقول الاستمارة',
	'changer_choix_multiple' => 'تغيير الى خيار متعدد',
	'changer_choix_unique' => 'تغيير الى خيار فريد',
	'choisir_email' => 'اختيار البريد الالكتروني اعتماداً على',
	'confirm_supprimer_champ' => 'هل تريد فعلاً حذف الحقل \'@champ@\'؟',
	'confirm_supprimer_donnee' => 'هل تريد فعلاً حذف المعلومة \'@donnee@\'؟',
	'confirm_vider_table' => 'هل تريد فعلاً تفريغ الجدول \'@table@\' من محتواه؟',
	'confirmer_champ_password' => 'التعريف اذا كان هناك ادخال مزدوج',
	'confirmer_password' => 'تأكيد',
	'confirmer_reponse' => 'التأكيد على الرد بالبريد بواسطة:',
	'csv_classique' => 'ملف CSV تقليدي (,)',
	'csv_excel' => 'ملف CSV لاكسل (;)',
	'csv_tab' => 'ملف CSV مع حقول',

	// D
	'date' => 'التاريخ',
	'date_invalide' => 'تنسيق التاريخ غير صالح',
	'donnees_modifiable' => 'بيانات يمكن للمستخدم تعديلها.',
	'donnees_multiple' => 'اجابات متعددة.',
	'donnees_nonmodifiable' => 'بيانات لا يمكن للمستخدم تعديلها.',
	'donnees_nonmultiple' => 'جواب وحيد',
	'donnees_prot' => 'استمارة محمية. لن تكون البيانات المسجلة متاحة الا في المجال الخاص.',
	'donnees_pub' => 'استمارة عمومية. سنكون البيانات المسجلة متاحة لزوار الموقع.',
	'dupliquer' => 'مضاعفة',

	// E
	'echec_upload' => 'فشل نقل الملف.',
	'edit_champ_obligatoire' => 'هذا الحقل إجباري',
	'editer' => 'تحرير',
	'email_independant' => 'بريد الكتروني مستقل عن الرد',
	'exporter' => 'تصدير',
	'exporter_article' => 'تصدير الى مقال',

	// F
	'fichier_trop_gros' => 'هذا الملف كبير جداً.',
	'fichier_type_interdit' => 'هذا النوع من الملفات ممنوع.',
	'form_erreur' => 'خطأ:',
	'format_fichier' => 'تنسيق الملف:',
	'format_liste' => 'منسدلة',
	'format_liste_ou_radio' => 'نوع اللائحة',
	'format_radio' => 'زر راديو',
	'forms_obligatoires' => 'الاستمارات الاجبارية لملء الحالية:',
	'formulaire' => 'استمارة',
	'formulaire_aller' => 'الذهاب الى الاستمارة',
	'formulaires_copie' => 'نسخة من @nom@',
	'formulaires_sondages' => 'الاستمارات والاستطلاعات',

	// H
	'html_wrapper' => 'إدراج الحقل في علامات HTML.',

	// I
	'icone_ajouter_donnees' => 'إضافة أجابات',
	'icone_creer_formulaire' => 'إنشاء استمارة جديدة',
	'icone_creer_table' => 'إنشاء استمارة جديدة',
	'importer_form' => 'جلب استمارة',
	'info_apparence' => 'هذه معاينة للاستمارة كما ستظهر لزوار الموقع العمومي.',
	'info_articles_lies_donnee' => 'المقالات المرتبطة',
	'info_champs_formulaire' => 'هنا يمكنك أنشاء وتعديل الحقول التي يملئها الزوار في ما بعد.',
	'info_obligatoire_02' => '[إجباري]',
	'info_rubriques_liees_donnee' => 'الاقسام المرتبطة',
	'info_sondage' => 'اذا كانت استمارتك استطلاعاً، سيتم جمع النتائج من نوع «اختيار» ثم عرضها.',
	'info_supprimer_formulaire' => 'هل تريد فعلاً حذف هذه الاستمارة؟',
	'info_supprimer_formulaire_reponses' => 'جاءت ردود على هذه الاستمارة. هل تريد فعلاً حذفها؟',

	// L
	'lien_apercu' => 'معاينة',
	'lien_champ' => 'الحقول',
	'lien_propriete' => 'المواصفات',
	'lien_retirer_donnee_liante' => 'ازالة الرابط',
	'lien_retirer_donnee_liee' => 'ازالة الرابط',
	'lier_articles' => 'السماح برتط البيانات بالمقالات',
	'lier_documents' => 'السماح بربط مستندات بالبيانات',
	'lier_documents_mail' => 'ربط المستندات يالبريد الالكتروني',
	'liste_choix' => 'لائحة الخيارات المقترحة',

	// M
	'moderation_donnees' => 'مصادقة على البيانات قبل النشر:',
	'modifiable_donnees' => 'بيانات يمكن تعديلها في الموقع العمومي:',
	'monetaire_invalide' => 'حقل نقدي غير صالح',
	'monnaie_euro' => 'يورو (€)',
	'multiple_donnees' => 'إدخال البيانات في الموقع العمومي:',

	// N
	'nb_decimales' => 'عدد الكسور العشرية',
	'nombre_reponses' => '@nombre@ رد',
	'nouveau_champ' => 'حقل جديد',
	'nouveau_choix' => 'خيار جديد',
	'nouveau_formulaire' => 'استمارة جديدة',
	'numerique_invalide' => 'حقل رقمي غير صالح',

	// P
	'page' => 'صفحة',
	'pas_mail_confirmation' => 'بدون بريد تأكيد',
	'probleme_technique' => 'مشكلة تقنية. لم يتم الاخذ بردك.',
	'probleme_technique_upload' => 'مشكلة تقنية. فشل نقل الملف.',
	'publication_donnees' => 'نشر البيانات',

	// R
	'rang' => 'مرتبة',
	'remplir_un_champ' => 'الرجاء تعبئة حقل واحد على الأقل.',
	'reponse' => 'الرد @id_reponse@',
	'reponse_depuis' => 'من الصفحة ',
	'reponse_enregistree' => 'تم تسجيل ردك.',
	'reponse_envoyee' => 'رد مرسل في ',
	'reponse_envoyee_a' => 'الى',
	'reponse_retrovez' => 'الذهاب الى هذا الرد في صفحة الإدارة:',
	'reponses' => 'ردود',
	'resultats' => 'النتائج: ',

	// S
	'site_introuvable' => 'لم يتم العثور على هذا الموقع.',
	'sondage_deja_repondu' => 'سبق وأجبت على هذا الاستطلاع.',
	'sondage_non' => 'هذه الاستمارة ليست استطلاعاً',
	'sondage_oui' => 'هذه الاستمارة هي استطلاع.',
	'suivi_formulaire' => 'متابعة الاستمارة',
	'suivi_formulaires' => 'متابعة الاستمارات',
	'suivi_reponses' => 'متابعة الردود',
	'supprimer' => 'حذف',
	'supprimer_champ' => 'حذف هذا الحقل',
	'supprimer_choix' => 'حذف هذا الخيار',
	'supprimer_formulaire' => 'حذف هذه الاستمارة',
	'supprimer_reponse' => 'حذف هذا الرد',

	// T
	'tables' => 'جداول',
	'taille_max' => 'الحجم الأقصى (بالكيلوبايت)',
	'telecharger' => 'تحميل',
	'telecharger_reponses' => 'تحميل الردود',
	'titre_formulaire' => 'عنوان الاستمارة',
	'total_votes' => 'مجموع الأصوات',
	'tous_formulaires' => 'كل الاستمارات',
	'tous_sondages' => 'كل الاستطلاعات',
	'tous_sondages_proteges' => 'كل الاستطلاعات المحمية',
	'tous_sondages_public' => 'كل الاستطلاعات العمومية',
	'toutes_tables' => 'كل الاستمارات',
	'type_form' => 'نوع الاستمارة',

	// U
	'une_reponse' => 'معلومة',
	'unite_monetaire' => 'الوحدة النقدية',

	// V
	'valider' => 'نصديق',
	'verif_web' => 'التأكد من وجود الموقع',
	'vider' => 'تفريغ',
	'voir_article' => 'عرض المقال',
	'voir_resultats' => 'عرض النتائج'
);

?>
