<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/forms/forms_et_tables_1_9_1/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Articles utilisant cette table',
	'aucune_reponse' => 'Aucune donnée',

	// C
	'champs_formulaire' => 'Champs de la table',

	// F
	'formulaire' => 'Table',

	// I
	'icone_ajouter_donnees' => 'Ajouter des données',
	'icone_creer_table' => 'Créer une nouvelle table',
	'importer_donnees_csv' => 'Importer des données',
	'info_supprimer_formulaire' => 'Voulez-vous vraiment supprimer cette table ?',
	'info_supprimer_formulaire_reponses' => 'Des données sont associées à cette table. Voulez-vous vraiment la supprimer ?',

	// L
	'lien_retirer_donnee_liante' => 'Retirer le lien depuis cette donnée',
	'lien_retirer_donnee_liee' => 'Retirer cette donnée',

	// N
	'nombre_reponses' => '@nombre@ données',
	'nouveau_formulaire' => 'Nouvelle table',

	// S
	'suivi_reponses' => 'Voir les données',
	'supprimer_formulaire' => 'Supprimer cette table',

	// T
	'telecharger_reponses' => 'Téléchargez les données',
	'texte_donnee_statut' => 'Statut de cette donnée',
	'texte_statut_poubelle' => 'Donnée supprimée',
	'texte_statut_prepa' => 'Donnée en cours de rédaction',
	'texte_statut_prop' => 'Donnée proposée',
	'texte_statut_publie' => 'Donnée publiée',
	'texte_statut_refuse' => 'Donnée refusée',
	'titre_formulaire' => 'Titre de la table',
	'toutes_tables' => 'Toutes les tables',
	'type_des_tables' => 'Tables',

	// U
	'une_reponse' => 'Une donnée'
);

?>
