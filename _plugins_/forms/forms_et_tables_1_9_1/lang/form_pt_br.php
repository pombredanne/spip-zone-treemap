<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/form?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Matérias que usam este formulário',
	'aucune_reponse' => 'Nenhuma resposta',

	// C
	'champs_formulaire' => 'Campos do formulário',

	// F
	'formulaire' => 'Formulário',

	// I
	'icone_ajouter_donnees' => 'Incluir respostas',
	'icone_creer_table' => 'Criar um novo formulário',
	'importer_donnees_csv' => 'Importar respostas',
	'info_supprimer_formulaire' => 'Você quer realmente excluir este formulário?',
	'info_supprimer_formulaire_reponses' => 'Há respostas associadas a este formulário. 
Você quer realmente excluí-lo?',

	// L
	'lien_retirer_donnee_liante' => 'Retirar o link depois desta resposta',
	'lien_retirer_donnee_liee' => 'Retirar esta resposta',

	// N
	'nombre_reponses' => '@nombre@ respostas',
	'nouveau_formulaire' => 'Novo formulário',

	// S
	'suivi_reponses' => 'Ver as respostas',
	'supprimer_formulaire' => 'Excluir este formulário',

	// T
	'telecharger_reponses' => 'Transferir as respostas',
	'texte_donnee_statut' => 'Status desta resposta',
	'texte_statut_poubelle' => 'Resposta excluída',
	'texte_statut_prepa' => 'Resposta em fase de redação',
	'texte_statut_prop' => 'Resposta proposta para publicação',
	'texte_statut_publie' => 'Resposta publicada',
	'texte_statut_refuse' => 'Resposta recusada',
	'titre_formulaire' => 'Título do formulário',
	'toutes_tables' => 'Todos os formulários',
	'type_des_tables' => 'Formulários',

	// U
	'une_reponse' => 'Uma resposta'
);

?>
