<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/form?lang_cible=km
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Articles utilisant ce formulaire', # NEW
	'aucune_reponse' => 'គ្មានចំលើយ',

	// C
	'champs_formulaire' => 'វាល នៃបែបបទ',

	// F
	'formulaire' => 'បែបបទ',

	// I
	'icone_ajouter_donnees' => 'បន្ថែម ចំលើយ',
	'icone_creer_table' => 'បង្កើត មួយបែបទថ្មី',
	'importer_donnees_csv' => 'នាំចូល ចំលើយ',
	'info_supprimer_formulaire' => 'Voulez-vous vraiment supprimer ce formulaire ?', # NEW
	'info_supprimer_formulaire_reponses' => 'Des Réponse sont associées à ce formulaire. 
Voulez-vous vraiment le supprimer ?', # NEW

	// L
	'lien_retirer_donnee_liante' => 'Retirer le lien depuis cette réponse', # NEW
	'lien_retirer_donnee_liee' => 'Retirer cette réponse', # NEW

	// N
	'nombre_reponses' => '@nombre@ ចំលើយ',
	'nouveau_formulaire' => 'បែបបទថ្មី',

	// S
	'suivi_reponses' => 'មើល រាល់ចំលើយ',
	'supprimer_formulaire' => 'Supprimer ce formulaire', # NEW

	// T
	'telecharger_reponses' => 'Téléchargez les réponses', # NEW
	'texte_donnee_statut' => 'ស្ថានភាព នៃចំលើយនេះ',
	'texte_statut_poubelle' => 'ចំលើយ ត្រូវបានលុបចេញ',
	'texte_statut_prepa' => 'Réponse en cours de rédaction', # NEW
	'texte_statut_prop' => 'Réponse proposée', # NEW
	'texte_statut_publie' => 'Réponse publiée', # NEW
	'texte_statut_refuse' => 'Réponse refusée', # NEW
	'titre_formulaire' => 'ចំណងជើង នៃបែបបទ',
	'toutes_tables' => 'គ្រប់បែបបទ',
	'type_des_tables' => 'Formulaires', # NEW

	// U
	'une_reponse' => 'មួយចំលើយ'
);

?>
