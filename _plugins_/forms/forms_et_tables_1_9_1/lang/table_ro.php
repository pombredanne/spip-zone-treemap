<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/table?lang_cible=ro
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Articole folosind această tabelă',
	'aucune_reponse' => 'Fără date',

	// C
	'champs_formulaire' => 'Câmpurile tabelei',

	// F
	'formulaire' => 'Tabelă',

	// I
	'icone_ajouter_donnees' => 'Adăugaţi date',
	'icone_creer_table' => 'Creaţi o nouă tabelă',
	'importer_donnees_csv' => 'Importaţi date',
	'info_supprimer_formulaire' => 'Vreţi cu adevărat să ştergeţi această tabelă ?',
	'info_supprimer_formulaire_reponses' => 'Există date asociate cu această tabelă. Doriţi cu adevărat să o ştergeţi ?',

	// L
	'lien_retirer_donnee_liante' => 'Retrageţi legătura de la această informaţie/dată',
	'lien_retirer_donnee_liee' => 'Retrageţi această informaţie/dată',

	// N
	'nombre_reponses' => '@nombre@ date',
	'nouveau_formulaire' => 'Tabelă nouă',

	// S
	'suivi_reponses' => 'Vedeţi datele',
	'supprimer_formulaire' => 'Ştergeţi această tabelă',

	// T
	'telecharger_reponses' => 'Descărcaţi datele',
	'texte_donnee_statut' => 'Starea acestei informaţii/date',
	'texte_statut_poubelle' => 'Informaţie/dată ştearsă',
	'texte_statut_prepa' => 'Informaţie/dată în curs de redactare',
	'texte_statut_prop' => 'Informaţie/dată propusă',
	'texte_statut_publie' => 'Informaţie/dată publicată',
	'texte_statut_refuse' => 'Informaţie/dată refuzată',
	'titre_formulaire' => 'Titlul tabelei',
	'toutes_tables' => 'Toate tabelele',
	'type_des_tables' => 'Tabele',

	// U
	'une_reponse' => 'O informaţie/dată'
);

?>
