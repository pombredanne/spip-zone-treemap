<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/forms?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_barre_typo' => 'Aktivovať panel s klávesovými skratkami',
	'adresse_invalide' => 'Táto adresa nie je platná.',
	'afficher' => 'Zobraziť',
	'aide_contextuelle' => 'Kontextová nápoveď',
	'ajouter_champ' => 'Pridať pole',
	'ajouter_champ_type' => 'Vytvoriť pole typu:',
	'ajouter_choix' => 'Pridať výber',
	'apparence_formulaire' => 'Vzhľad vstupného formulára',
	'article_inserer_un_formulaire' => 'Vložiť formulár',
	'article_inserer_un_formulaire_detail' => 'Do článku môžete vkladať formuláre, aby ste umožnili návštevníkom poskytnúť informácie. Vyberte si, prosím, formulár zo zoznamu a do textu článku si skopírujte kód na vloženie. ',
	'article_recopier_raccourci' => 'Na vloženie tohto formulára skopírujte do textu článku tento  kód',
	'articles_utilisant' => 'Články, ktoré využívajú tento formulár',
	'attention' => 'Pozor:',
	'aucune_reponse' => 'žiadne údaje',
	'avis_message_confirmation' => 'Na adresu @mail@ bola odoslaná potvrdzovacia správa',

	// B
	'boite_info' => 'Kliknite na tieto ikony, aby ste mohli zobraziť údaje z formulára, upraviť formulár, kopírovať ho, exportovať ho, vyprázdniť ho alebo ho odstrániť.',

	// C
	'cfg_activer' => 'Áno',
	'cfg_associer_donnees' => 'Priradiť údaje z tabuliek',
	'cfg_associer_donnees_articles' => 'Umožniť priraďovať dáta k článkom:',
	'cfg_associer_donnees_auteurs' => 'Umožniť priraďovať dáta k autorom:',
	'cfg_associer_donnees_rubriques' => 'Umožniť priraďovať dáta k rubrikám:',
	'cfg_bouton_type_image' => 'tlačidlo s obrázkom',
	'cfg_bouton_type_texte' => 'textové tlačidlo (poslať)',
	'cfg_bouton_valider' => 'Tlačidlo potvrdiť',
	'cfg_bouton_valider_texte' => 'Môžete určiť, aký druh tlačidiel sa použije vo formulároch:',
	'cfg_desactiver' => 'Nie',
	'cfg_inserer_head' => 'Vložiť do &lt;hlavičky&gt;',
	'cfg_inserer_head_texte' => 'Do &lt;hlavičky&gt; nasledovných súborov stránky automaticky vkladať odkazy (odporúča sa):',
	'champ_descendre' => 'dole',
	'champ_email_details' => 'Prosím, zadajte platnú e-mailovú adresu (napr. vas@poskytovatel.sk).',
	'champ_listable_admin' => 'Toto pole zobraziť v zoznamoch súkromnej zóny',
	'champ_listable_publique' => 'Toto pole zobraziť vo verejných zoznamoch ',
	'champ_monter' => 'hore',
	'champ_necessaire' => 'Toto pole treba vyplniť.',
	'champ_nom' => 'Názov poľa',
	'champ_nom_bloc' => 'Názov bloku',
	'champ_nom_groupe' => 'Skupina',
	'champ_nom_texte' => 'Text',
	'champ_public' => 'Toto pole sa zobrazí na verejnej stránke',
	'champ_saisie_desactivee' => 'Znemožniť úpravu tohto poľa',
	'champ_specifiant' => 'Toto pole určuje dáta (triedenie, filter, popis)',
	'champ_table_jointure_type' => 'Typ tabuľky na pripojenie',
	'champ_type_date' => 'Dátum',
	'champ_type_email' => 'E-mailová adresa',
	'champ_type_fichier' => 'Súbory na stiahnutie',
	'champ_type_joint' => 'Spojiť s inou tabuľkou',
	'champ_type_ligne' => 'Riadok textu',
	'champ_type_monnaie' => 'Mena',
	'champ_type_mot' => 'Kľúčové slová',
	'champ_type_multiple' => 'Výber z viac možností',
	'champ_type_numerique' => 'Číselné',
	'champ_type_password' => 'Heslo',
	'champ_type_select' => 'Výber z dvoch možností',
	'champ_type_separateur' => 'Nový blok otázok',
	'champ_type_texte' => 'Text',
	'champ_type_textestatique' => 'Vysvetlivka',
	'champ_type_url' => 'Adresa internetovej stránky',
	'champ_url_details' => 'Prosím, zadajte platnú internetovú adresu (napr. http://www.mojastranka.sk/...).',
	'champs_formulaire' => 'Polia formulára',
	'changer_choix_multiple' => 'Zmeniť na výber z viac možností',
	'changer_choix_unique' => 'Zmeniť na výber z dvoch možností',
	'choisir_email' => 'Vyberte si e-mail podľa',
	'confirm_supprimer_champ' => 'Určite chcete odstrániť pole "@champ@"?',
	'confirm_supprimer_donnee' => 'Určite chcete odstrániť dáta "@donnee@"?',
	'confirm_vider_table' => 'Určite chcete odstrániť všetky dáta z tabuľky "@table@"?',
	'confirmer_champ_password' => 'Menovka pre dvojité pole (s potvrdením)',
	'confirmer_password' => 'Potvrdenie',
	'confirmer_reponse' => 'Confirm reply by mail with:',
	'csv_classique' => 'Klasické CSV (,)',
	'csv_excel' => 'CSV pre Excel (;)',
	'csv_tab' => 'CSV s tabulátormi',

	// D
	'date' => 'Dátum',
	'date_invalide' => 'Neplatný formát dátumu',
	'donnees_modifiable' => 'Dáta, ktoré môže používateľ upraviť.',
	'donnees_multiple' => 'Výber z viac možností.',
	'donnees_nonmodifiable' => 'Dáta, ktoré používateľ nemôže upraviť.',
	'donnees_nonmultiple' => 'Single choice.',
	'donnees_prot' => 'Protected form. The recorded data will be available from the private area only.',
	'donnees_pub' => 'Public form. The recorded data will be available to the site visitors.',
	'dupliquer' => 'Duplikovať',

	// E
	'echec_upload' => 'Presun súboru sa nepodaril.',
	'edit_champ_obligatoire' => 'Toto pole je povinné',
	'editer' => 'Upraviť',
	'email_independant' => 'E-mail nezávislý od odpovede',
	'exporter' => 'Exportovať',
	'exporter_article' => 'Exportovať do článku',

	// F
	'fichier_trop_gros' => 'Tento súbor je príliš veľký.',
	'fichier_type_interdit' => 'Tento typ súborov je zakázaný.',
	'form_erreur' => 'Chyba:',
	'format_fichier' => 'Formát súboru:',
	'format_liste' => 'drop-down',
	'format_liste_ou_radio' => 'Formát zoznamu',
	'format_radio' => 'rádiové gombíky',
	'forms_obligatoires' => 'Formuláre potrebné na úpravu:',
	'formulaire' => 'Formulár',
	'formulaire_aller' => 'Prejsť na formulár',
	'formulaires_copie' => 'Kópia @nom@',
	'formulaires_sondages' => 'Formuláre a ankety',

	// H
	'html_wrapper' => 'Uzavrieť pole v HTML kóde',

	// I
	'icone_ajouter_donnees' => 'Pridať odpovede',
	'icone_creer_formulaire' => 'Vytvoriť nový formulár',
	'icone_creer_table' => 'Create a new form',
	'importer_form' => 'Importovať formulár',
	'info_apparence' => 'Toto je ukážka formulára, takto ho uvidia návštevníci verejnej stránky.',
	'info_articles_lies_donnee' => 'Odkazy na články',
	'info_champs_formulaire' => 'Tu môžete vytvárať a upravovať polia, ktoré budú vypĺňať návštevníci.',
	'info_obligatoire_02' => '[Povinné]',
	'info_rubriques_liees_donnee' => 'Odkazy na rubriky',
	'info_sondage' => 'Ak je váš formulár anketou, výsledky výberových polí budú pridané a zobrazia sa.',
	'info_supprimer_formulaire' => 'Určite chcete odstrániť tento formulár?',
	'info_supprimer_formulaire_reponses' => 'Na tento formulár prišli odpovede. Určite ho chcete odstrániť?',

	// L
	'lien_apercu' => 'Zoznam',
	'lien_champ' => 'Polia',
	'lien_propriete' => 'Vlastnosti',
	'lien_retirer_donnee_liante' => 'Vymazať tento odkaz',
	'lien_retirer_donnee_liee' => 'Vymazať odkaz',
	'lier_articles' => 'Umožniť priraďovanie dát a článkov',
	'lier_documents' => 'Umožniť k dátam pripájať dokumenty',
	'lier_documents_mail' => 'Pripájať dokumenty k e-mailom',
	'liste_choix' => 'Zoznam dostupných výberov',

	// M
	'moderation_donnees' => 'Skontrolovať dáta pred publikovaním:',
	'modifiable_donnees' => 'Dáta, ktoré sa dajú upravovať z verejnej stránky:',
	'monetaire_invalide' => 'Neplatné pole Mena',
	'monnaie_euro' => 'Euro (€)',
	'multiple_donnees' => 'Úprava dát z verejnej stránky:',

	// N
	'nb_decimales' => 'Počet desatinných miest',
	'nombre_reponses' => '@number@ odpovedí',
	'nouveau_champ' => 'Nové pole',
	'nouveau_choix' => 'Nový výber',
	'nouveau_formulaire' => 'Nový formulár',
	'numerique_invalide' => 'Neplatné číselné pole',

	// P
	'page' => 'Stránka',
	'pas_mail_confirmation' => 'Žiadny potvrdzujúci e-mail',
	'probleme_technique' => 'technical problem. Your reply can not be taken into account.',
	'probleme_technique_upload' => 'Technický problém. Presun súboru sa nepodaril.',
	'publication_donnees' => 'Publikovanie dát',

	// R
	'rang' => 'Hodnotenie',
	'remplir_un_champ' => 'Prosím, vyplňte aspoň jedno pole.',
	'reponse' => 'odpoveď @id_reponse@',
	'reponse_depuis' => 'Zo stránky',
	'reponse_enregistree' => 'Vaša odpoveď bola zaregistrovaná.',
	'reponse_envoyee' => 'Odpoveď odoslaná',
	'reponse_envoyee_a' => 'na',
	'reponse_retrovez' => 'Nájsť túto odpoveď v administračnom rozhraní:',
	'reponses' => 'odpovede',
	'resultats' => 'Výsledky: ',

	// S
	'site_introuvable' => 'Táto stránka sa nenašla.',
	'sondage_deja_repondu' => 'Na túto anketu ste už odpovedali',
	'sondage_non' => 'Tento formulár nie je anketa',
	'sondage_oui' => 'Tento formulár je anketa',
	'suivi_formulaire' => 'Sledovanie formulára',
	'suivi_formulaires' => 'Sledovanie formulárov',
	'suivi_reponses' => 'Replies follow-up',
	'supprimer' => 'Odstrániť',
	'supprimer_champ' => 'Odstrániť toto pole',
	'supprimer_choix' => 'odstrániť tento výber',
	'supprimer_formulaire' => 'Odstrániť tento formulár',
	'supprimer_reponse' => 'Odstrániť túto odpoveď',

	// T
	'tables' => 'Tabuľky',
	'taille_max' => 'Maximálna veľkosť (v kB)',
	'telecharger' => 'Stiahnuť',
	'telecharger_reponses' => 'Stiahnuť odpovede',
	'titre_formulaire' => 'Názov formulára',
	'total_votes' => 'Celkom hlasov',
	'tous_formulaires' => 'Všetky formuláre',
	'tous_sondages' => 'Všetky ankety',
	'tous_sondages_proteges' => 'Všetky chránené ankety',
	'tous_sondages_public' => 'Všetky verejné ankety',
	'toutes_tables' => 'Všetky formuláre',
	'type_form' => 'Typ formulára',

	// U
	'une_reponse' => 'jedna položka dát',
	'unite_monetaire' => 'Menová jednotka',

	// V
	'valider' => 'Overiť',
	'verif_web' => 'Skontrolovať existenciu stránky',
	'vider' => 'Vyprázdniť',
	'voir_article' => 'Zobraziť článok',
	'voir_resultats' => 'Zobraziť výsledky'
);

?>
