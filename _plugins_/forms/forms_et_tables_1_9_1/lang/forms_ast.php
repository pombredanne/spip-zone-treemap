<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/forms?lang_cible=ast
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_barre_typo' => 'Activar la barra tipográfica',
	'adresse_invalide' => 'Esta direición nun ye válida.',
	'afficher' => 'Amosar',
	'aide_contextuelle' => 'Aida contestual',
	'ajouter_champ' => 'Añadir un campu',
	'ajouter_champ_type' => 'Crear un campu de tipu:',
	'ajouter_choix' => 'Añader una seleición',
	'apparence_formulaire' => 'Aspeutu del formulariu d\'edición',
	'article_inserer_un_formulaire' => 'Amestar un formulariu',
	'article_inserer_un_formulaire_detail' => 'Vas poder amestar formularios a los tos artículos pa permiti-yos a los visitantes inxertar información. Escueye un formulariu de la llista d\'embaxo y copia l\'atayu nel testu de l\'artículu.',
	'article_recopier_raccourci' => 'Copia esti atayu nel testu de l\'artículu pa amesta-y esti formulariu',
	'articles_utilisant' => 'Artículos que tan usando esti formulariu',
	'attention' => 'Atención:',
	'aucune_reponse' => 'dengún datu',
	'avis_message_confirmation' => 'Un mensaxe de confirmación ta acabante d\'unviase a @mail@',

	// B
	'boite_info' => 'Pica enriba los iconos pa ver los datos asociaos con un formulariu, editalu, duplicalu, esportalu, vacialu de datos o suprimilu.',

	// C
	'cfg_activer' => 'Si',
	'cfg_associer_donnees' => 'Rellacionar los datos de les tables',
	'cfg_associer_donnees_articles' => 'Permitir rellacionar los datos colos artículos:',
	'cfg_associer_donnees_auteurs' => 'Permitir rellacionar los datos colos autores:',
	'cfg_associer_donnees_rubriques' => 'Permitir rellacionar los datos coles seiciones:',
	'cfg_bouton_type_image' => 'botón de tipu imaxe',
	'cfg_bouton_type_texte' => 'botón de tipu testu (unviar)',
	'cfg_bouton_valider' => 'Tipu de botón validar',
	'cfg_bouton_valider_texte' => 'Puedes escoyer qué tipu de botón validar va utilizase nos formularios:',
	'cfg_desactiver' => 'Non',
	'cfg_inserer_head' => 'Inxertar nel <head>',
	'cfg_inserer_head_texte' => 'Inxertar automáticamente les llamáes a los ficheros siguientes na seición &lt;head&gt; de les páxines del sitiu (encamentao):',
	'champ_descendre' => 'baxar',
	'champ_email_details' => 'Hai qu\'escribir unes señes d\'e-mail válides (del tipu nome@eltocorreu.com).',
	'champ_listable_admin' => 'Amosar esti campu nes llistes de l\'espaciu privao',
	'champ_listable_publique' => 'Amosar esti campu nes llistes públiques',
	'champ_monter' => 'xubir',
	'champ_necessaire' => 'Hai qu\'enllenar esti campu.',
	'champ_nom' => 'Nome del campu',
	'champ_nom_bloc' => 'Nome del bloque',
	'champ_nom_groupe' => 'Grupu',
	'champ_nom_texte' => 'Testu',
	'champ_public' => 'Esti campu ye visible dende l\'espaciu públicu',
	'champ_saisie_desactivee' => 'Desactivar la edición d\'esti campu',
	'champ_specifiant' => 'Esti campu clasifica\'l datu (orden, filtru, descripción)',
	'champ_table_jointure_type' => 'Tipu de tabla pa les xuntures',
	'champ_type_date' => 'Fecha',
	'champ_type_email' => 'Señes d\'e-mail',
	'champ_type_fichier' => 'Ficheru a descargar',
	'champ_type_joint' => 'Xuntura con otra tabla',
	'champ_type_ligne' => 'Llínia de testu',
	'champ_type_monnaie' => 'Monetariu',
	'champ_type_mot' => 'Pallabres-clave',
	'champ_type_multiple' => 'Escoyeta múltiple',
	'champ_type_numerique' => 'Numbéricu',
	'champ_type_password' => 'Contraseña',
	'champ_type_select' => 'Escoyeta única',
	'champ_type_separateur' => 'Nuevu bloque d\'entrugues',
	'champ_type_texte' => 'Testu',
	'champ_type_textestatique' => 'Mensaxe d\'esplicación',
	'champ_type_url' => 'Señes del sitiu Web',
	'champ_url_details' => 'Hai qu\'inxertar unes señes Web válides (del tipu http://www.elmiositiu.com/...).',
	'champs_formulaire' => 'Campos del formulariu',
	'changer_choix_multiple' => 'Cambear a escoyeta múltiple',
	'changer_choix_unique' => 'Cambear a escoyeta única',
	'choisir_email' => 'Escoyer l\'e-mail en función de',
	'confirm_supprimer_champ' => '¿Estás seguru de que quies desaniciar el campu \'@champ@\'?',
	'confirm_supprimer_donnee' => '¿De xuru quies desaniciar el datu \'@donnee@\'?',
	'confirm_vider_table' => '¿De xuru quies vaciar el conteníu de la tabla \'@table@\'?',
	'confirmer_champ_password' => 'Testu pa la escritura doble',
	'confirmer_password' => 'Confirmación',
	'confirmer_reponse' => 'Enviar un corréu pa confirmar la receición con:',
	'csv_classique' => 'CSV clásicu (,)',
	'csv_excel' => 'CSV pa Excel (;)',
	'csv_tab' => 'CSV con tabulaciones',

	// D
	'date' => 'Fecha',
	'date_invalide' => 'Formatu fecha non válidu',
	'donnees_modifiable' => 'Datos iguables pel usuariu.',
	'donnees_multiple' => 'Respuestes múltiples.',
	'donnees_nonmodifiable' => 'Datos non iguables pel usuariu.',
	'donnees_nonmultiple' => 'Respuesta única.',
	'donnees_prot' => 'Datos protexíos. Los datos grabaos nun van ser accesibles más que dende l\'interfaz privao.',
	'donnees_pub' => 'Datos públicos. Los datos grabaos tarán accesibles pa les visites del sitiu.',
	'dupliquer' => 'Duplicar',

	// E
	'echec_upload' => 'Fallu na tresferencia del ficheru.',
	'edit_champ_obligatoire' => 'Esti campu ye obligatoriu',
	'editer' => 'Iguar',
	'email_independant' => 'Email independiente de la respuesta',
	'exporter' => 'Esportar',
	'exporter_article' => 'Esportar nun artículu',

	// F
	'fichier_trop_gros' => 'Esti ficheru ye demasiao grande.',
	'fichier_type_interdit' => 'Esti tipu de ficheru ta torgau.',
	'form_erreur' => 'Fallu:',
	'format_fichier' => 'Formatu d\'archivu:',
	'format_liste' => 'desplegable',
	'format_liste_ou_radio' => 'Formatu de la llista',
	'format_radio' => 'botones radio',
	'forms_obligatoires' => 'Formularios obligatorios pa la edición d\'esti:',
	'formulaire' => 'Formulariu',
	'formulaire_aller' => 'Dir pal formulariu',
	'formulaires_copie' => 'Copia de @nom@',
	'formulaires_sondages' => 'Formularios y encuestes',

	// H
	'html_wrapper' => 'Enxertar el campu nel códigu html',

	// I
	'icone_ajouter_donnees' => 'Amestar respuestes',
	'icone_creer_formulaire' => 'Crear un nuevu formulariu',
	'icone_creer_table' => 'Crear un nuevu formulariu',
	'importer_form' => 'Importar un formulariu',
	'info_apparence' => 'Esta ye una previsualización del formulariu tal como-yos apaecerá a les visites del sitiu públicu.',
	'info_articles_lies_donnee' => 'Los artículos enllazáos',
	'info_champs_formulaire' => 'Equí pues crear y cambear los campos que les visites van poder enllenar.',
	'info_obligatoire_02' => '[Obligatorio]',
	'info_rubriques_liees_donnee' => 'Les estayes enllazáes',
	'info_sondage' => 'Si el formulariu ye una encuesta, los resultaos de los campos de tipu «seleición» súmense y amuésense.',
	'info_supprimer_formulaire' => '¿De verdá quiés desaniciar esti formulariu?',
	'info_supprimer_formulaire_reponses' => 'Hai datos asociaos con esti formulariu. ¿De verdá quiés desanicialu?',

	// L
	'lien_apercu' => 'Güeyada',
	'lien_champ' => 'Campos',
	'lien_propriete' => 'Propiedáes',
	'lien_retirer_donnee_liante' => 'Quitar l\'enllace',
	'lien_retirer_donnee_liee' => 'Quitar l\'enllace',
	'lier_articles' => 'Permitir que s\'asocien los datos colos artículos',
	'lier_documents' => 'Permitir amestar documentos a los datos',
	'lier_documents_mail' => 'Amestar los documentos al email',
	'liste_choix' => 'Lista les escoyetes propuestes',

	// M
	'moderation_donnees' => 'Validar los datos enantes d\'espublizalos:',
	'modifiable_donnees' => 'Datos camudables nel espaciu públicu:',
	'monetaire_invalide' => 'Campu monetariu non válidu',
	'monnaie_euro' => 'Euro (€)',
	'multiple_donnees' => 'Iguar los datos nel espaciu públicu:',

	// N
	'nb_decimales' => 'Númberu decimales',
	'nombre_reponses' => '@nombre@ datos',
	'nouveau_champ' => 'Nuevu campu',
	'nouveau_choix' => 'Nueva eleición',
	'nouveau_formulaire' => 'Nuevu formulariu',
	'numerique_invalide' => 'Campu numbéricu non válidu',

	// P
	'page' => 'Páxina',
	'pas_mail_confirmation' => 'Ensin corréu de confirmación',
	'probleme_technique' => 'Problema técnicu. Lo que iguaste nun va poder tenese en cuenta.',
	'probleme_technique_upload' => 'Problema técnicu. Falló la tresferencia de l\'archivu.',
	'publication_donnees' => 'Publicación de los datos',

	// R
	'rang' => 'Rangu',
	'remplir_un_champ' => 'Ties que llenar polo menos un campu.',
	'reponse' => 'Respuesta @id_reponse@',
	'reponse_depuis' => 'Dende la páxina ',
	'reponse_enregistree' => 'La to igua quedó grabada.',
	'reponse_envoyee' => 'Respuesta unviada el ',
	'reponse_envoyee_a' => 'a',
	'reponse_retrovez' => 'Alcuentra esta respuesta na interfaz d\'alministración:',
	'reponses' => 'respuestes',
	'resultats' => 'Resultaos: ',

	// S
	'site_introuvable' => 'Esti sitiu nun s\'alcuentra.',
	'sondage_deja_repondu' => 'Ya respondisti a esta encuesta.',
	'sondage_non' => 'Esti formulariu nun ye una encuesta',
	'sondage_oui' => 'Esti formulariu ye una encuesta.',
	'suivi_formulaire' => 'Siguimientu del formulariu',
	'suivi_formulaires' => 'Siguimientu de los formularios',
	'suivi_reponses' => 'Siguimientu de les respuestes',
	'supprimer' => 'Desaniciar',
	'supprimer_champ' => 'Desaniciar esti campu',
	'supprimer_choix' => 'desaniciar esta respuesta',
	'supprimer_formulaire' => 'Desaniciar esti formulariu',
	'supprimer_reponse' => 'Desaniciar esta respuesta',

	// T
	'tables' => 'Tables',
	'taille_max' => 'Tamañu máximu (en kb)',
	'telecharger' => 'Tresferir',
	'telecharger_reponses' => 'Tresferir les respuestes',
	'titre_formulaire' => 'Títulu del formulariu',
	'total_votes' => 'Votos totales',
	'tous_formulaires' => 'Toos los formularios',
	'tous_sondages' => 'Toes les encuestes',
	'tous_sondages_proteges' => 'Toes les encuestes protexíes',
	'tous_sondages_public' => 'Toes les encuestes públiques',
	'toutes_tables' => 'Toos los formularios',
	'type_form' => 'Tipu de formulariu',

	// U
	'une_reponse' => 'un datu',
	'unite_monetaire' => 'Unidá monetaria',

	// V
	'valider' => 'Validar',
	'verif_web' => 'verificar la esistencia del sitiu Web',
	'vider' => 'Vaciar',
	'voir_article' => 'Ver l\'artículu',
	'voir_resultats' => 'Ver los resultaos'
);

?>
