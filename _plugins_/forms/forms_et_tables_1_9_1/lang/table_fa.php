<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/table?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'مقالاتي كه از اين جدول استفاده مي‌كنند',
	'aucune_reponse' => 'بي‌ داده',

	// C
	'champs_formulaire' => 'ميدان‌هاي جدول ',

	// F
	'formulaire' => 'جدول ',

	// I
	'icone_ajouter_donnees' => 'افزودن داده‌ها',
	'icone_creer_table' => 'ايجاد جدول جديد',
	'importer_donnees_csv' => 'ورود داده‌ها',
	'info_supprimer_formulaire' => 'به راستي مي‌خواهيد اين جدول را پاك كنيد؟',
	'info_supprimer_formulaire_reponses' => 'اين داده‌ها مال اين جدول هستند. به راستي مي‌خواهيد پاكشان كنيد؟ ',

	// L
	'lien_retirer_donnee_liante' => 'پيوند اين داده را برداشتن',
	'lien_retirer_donnee_liee' => 'حذف اين داده',

	// N
	'nombre_reponses' => '@nombre@ داده‌ها',
	'nouveau_formulaire' => 'جدول جديد',

	// S
	'suivi_reponses' => 'ديدن اين داده‌ها',
	'supprimer_formulaire' => 'حذف اين جدول',

	// T
	'telecharger_reponses' => 'بارگذاري داده‌ها',
	'texte_donnee_statut' => 'وضعيت اين داده',
	'texte_statut_poubelle' => 'داده‌ي حذف شده',
	'texte_statut_prepa' => 'داده در دست ويرايش ',
	'texte_statut_prop' => 'داده‌ي پيشنهادي',
	'texte_statut_publie' => 'داده‌ي منتشر‌ شده',
	'texte_statut_refuse' => 'داده‌ي ردي ',
	'titre_formulaire' => 'تيتر جدول ',
	'toutes_tables' => 'تمام جدول‌ها',
	'type_des_tables' => 'جدول‌ها',

	// U
	'une_reponse' => 'يك داده'
);

?>
