<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/table?lang_cible=km
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Articles utilisant cette table', # NEW
	'aucune_reponse' => 'គ្មានទិន្នន័យ',

	// C
	'champs_formulaire' => 'វាល នៃតារាង',

	// F
	'formulaire' => 'តារាង',

	// I
	'icone_ajouter_donnees' => 'បន្ថែម ទិន្នន័យ',
	'icone_creer_table' => 'បង្កើត មួយតារាងថ្មី',
	'importer_donnees_csv' => 'នាំចូល ទិន្នន័យ',
	'info_supprimer_formulaire' => 'អ្នកប្រាដចង់ លុបចេញ តារាងនេះ?',
	'info_supprimer_formulaire_reponses' => 'Des données sont associées à cette table. Voulez-vous vraiment la supprimer ?', # NEW

	// L
	'lien_retirer_donnee_liante' => 'ដកចេញ តំណភ្ជាប់ ពីទិន្នន័យនេះ',
	'lien_retirer_donnee_liee' => 'Retirer cette donnée', # NEW

	// N
	'nombre_reponses' => '@nombre@ ទិន្នន័យ',
	'nouveau_formulaire' => 'តារាងថ្មី',

	// S
	'suivi_reponses' => 'មើល ទិន្នន័យ​',
	'supprimer_formulaire' => 'លុបចេញ តារាងនេះ',

	// T
	'telecharger_reponses' => 'Téléchargez les données', # NEW
	'texte_donnee_statut' => 'ស្ថានភាព នៃទិន្នន័យនេះ',
	'texte_statut_poubelle' => 'ទិន្នន័យ ត្រូវបានលុបចេញ',
	'texte_statut_prepa' => 'Donnée en cours de rédaction', # NEW
	'texte_statut_prop' => 'Donnée proposée', # NEW
	'texte_statut_publie' => 'Donnée publiée', # NEW
	'texte_statut_refuse' => 'Donnée refusée', # NEW
	'titre_formulaire' => 'ចំណងជើង នៃតារាង',
	'toutes_tables' => 'គ្រប់តារាង',
	'type_des_tables' => 'តារាង',

	// U
	'une_reponse' => 'មួយទិន្នន័យ'
);

?>
