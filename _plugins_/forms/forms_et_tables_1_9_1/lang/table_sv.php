<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Articles utilisant cette table', # NEW
	'aucune_reponse' => 'Aucune donnée', # NEW

	// C
	'champs_formulaire' => 'Champs de la table', # NEW

	// F
	'formulaire' => 'Table', # NEW

	// I
	'icone_ajouter_donnees' => 'Ajouter des données', # NEW
	'icone_creer_table' => 'Créer une nouvelle table', # NEW
	'importer_donnees_csv' => 'Importer des données', # NEW
	'info_supprimer_formulaire' => 'Vill du verkligen radera tabellen ? ',
	'info_supprimer_formulaire_reponses' => 'Des données sont associées à cette table. Voulez-vous vraiment la supprimer ?', # NEW

	// L
	'lien_retirer_donnee_liante' => 'Retirer le lien depuis cette donnée', # NEW
	'lien_retirer_donnee_liee' => 'Retirer cette donnée', # NEW

	// N
	'nombre_reponses' => '@nombre@ données', # NEW
	'nouveau_formulaire' => 'Nouvelle table', # NEW

	// S
	'suivi_reponses' => 'Voir les données', # NEW
	'supprimer_formulaire' => 'Supprimer cette table', # NEW

	// T
	'telecharger_reponses' => 'Téléchargez les données', # NEW
	'texte_donnee_statut' => 'Statut de cette donnée', # NEW
	'texte_statut_poubelle' => 'Donnée supprimée', # NEW
	'texte_statut_prepa' => 'Donnée en cours de rédaction', # NEW
	'texte_statut_prop' => 'Donnée proposée', # NEW
	'texte_statut_publie' => 'Donnée publiée', # NEW
	'texte_statut_refuse' => 'Donnée refusée', # NEW
	'titre_formulaire' => 'Titre de la table', # NEW
	'toutes_tables' => 'Toutes les tables', # NEW
	'type_des_tables' => 'Tables', # NEW

	// U
	'une_reponse' => 'Une donnée' # NEW
);

?>
