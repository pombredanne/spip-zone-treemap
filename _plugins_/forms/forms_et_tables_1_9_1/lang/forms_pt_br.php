<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/forms?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_barre_typo' => 'Ativar a barra tipográfica',
	'adresse_invalide' => 'Este endereço não é válido',
	'afficher' => 'Exibir',
	'aide_contextuelle' => 'Ajuda contextual',
	'ajouter_champ' => 'Incluir um campo',
	'ajouter_champ_type' => 'Criar um campo do tipo:',
	'ajouter_choix' => 'Incluir uma opção',
	'apparence_formulaire' => 'Aparência do formulário de inscrição',
	'article_inserer_un_formulaire' => 'Inserir um formulário',
	'article_inserer_un_formulaire_detail' => 'Você pode inserir formulários em seus artigos, para solicitar informações aos visitantes. Escolha um formulário na lista abaixo e copie a tag no artigo.',
	'article_recopier_raccourci' => 'Copie a tag no texto do artigo para inserir este formulário',
	'articles_utilisant' => 'Matérias que usam este formulário',
	'attention' => 'Atenção:',
	'aucune_reponse' => 'sem dados',
	'avis_message_confirmation' => 'Uma mensagem de confirmação é envida a @mail@',

	// B
	'boite_info' => 'Clique nos ícones para ver os dados associados a um formulário, editá-lo, duplicá-lo, exportá-lo, limpar dos dados ou excluí-lo.',

	// C
	'cfg_activer' => 'Sim',
	'cfg_associer_donnees' => 'Associar os dados das tabelas',
	'cfg_associer_donnees_articles' => 'Permitir a associação dos dados às matérias:',
	'cfg_associer_donnees_auteurs' => 'Permitir a associação dos dados aos autores:',
	'cfg_associer_donnees_rubriques' => 'Permitir a associação dos dados às seções:',
	'cfg_bouton_type_image' => 'botão do tipo imagem',
	'cfg_bouton_type_texte' => 'botão do tipo texto (submit)',
	'cfg_bouton_valider' => 'Tipo de botão validar',
	'cfg_bouton_valider_texte' => 'Você pode especificar que tipo de botão Validar usar nos formulários:',
	'cfg_desactiver' => 'Não',
	'cfg_inserer_head' => 'Inclusão no &lt;head&gt;',
	'cfg_inserer_head_texte' => 'Inserir automaticamente as chamadas aos arquivos seguintes na seção &lt;head&gt; das páginas do site (recomendado):',
	'champ_descendre' => 'descer',
	'champ_email_details' => 'Por favor, informe um endereço eletrônico válido (tipo voce@provedor.com.br)',
	'champ_listable_admin' => 'Exibir este campo nas listas do espaço privado',
	'champ_listable_publique' => 'Exibir este campo nas listas publicadas',
	'champ_monter' => 'subir',
	'champ_necessaire' => 'Este campo precisa ser preenchido',
	'champ_nom' => 'Nome do campo',
	'champ_nom_bloc' => 'Nome do bloco',
	'champ_nom_groupe' => 'Grupo',
	'champ_nom_texte' => 'Texto',
	'champ_public' => 'Este campo é visível no espaço público',
	'champ_saisie_desactivee' => 'Desativar a entrada neste campo',
	'champ_specifiant' => 'Este campo qualifica os dados (ordenacão, filtro, descrição)',
	'champ_table_jointure_type' => 'Tipo de tabela para as junções',
	'champ_type_date' => 'Data',
	'champ_type_email' => 'endereço eletrônico',
	'champ_type_fichier' => 'arquivo a enviar',
	'champ_type_joint' => 'Junção com uma outra tabela',
	'champ_type_ligne' => 'linha de texto',
	'champ_type_monnaie' => 'Monetário',
	'champ_type_mot' => 'palavras-chaves',
	'champ_type_multiple' => 'múltiplas escolhas',
	'champ_type_numerique' => 'Numérico',
	'champ_type_password' => 'Senha',
	'champ_type_select' => 'escolha única',
	'champ_type_separateur' => 'novo bloco de questões',
	'champ_type_texte' => 'texto',
	'champ_type_textestatique' => 'mensagem explicativa',
	'champ_type_url' => 'endereço de página web',
	'champ_url_details' => 'Por favor, informe uma página web válida (tipo http://www.meusite.com.br)',
	'champs_formulaire' => 'Campos do formulário',
	'changer_choix_multiple' => 'Mudar para múltipla escolha',
	'changer_choix_unique' => 'Mudar para escolha única',
	'choisir_email' => 'Escolher o e-mail em função de',
	'confirm_supprimer_champ' => 'Vocês tem certeza de que quer excluir o campo  \'@champ@\' ?',
	'confirm_supprimer_donnee' => 'Você tem certeza de que quer excluir o dado \'@donnee@\' ?',
	'confirm_vider_table' => 'Você está certo de que quer apagar o conteúdo da tabela \'@table@\'?',
	'confirmer_champ_password' => 'Nome para campo duplo (confirmação)',
	'confirmer_password' => 'Confirmação',
	'confirmer_reponse' => 'Enviar um e-mail de aviso de recepção com:',
	'csv_classique' => 'CSV classico (,)',
	'csv_excel' => 'CSV para Excel (;)',
	'csv_tab' => 'CSV com tabs',

	// D
	'date' => 'Data',
	'date_invalide' => 'Formato de data inválido',
	'donnees_modifiable' => 'Dados que o usuário pode editar.',
	'donnees_multiple' => 'Múltipla escolha.',
	'donnees_nonmodifiable' => 'Dados que o usuário não pode editar.',
	'donnees_nonmultiple' => 'Única escolha.',
	'donnees_prot' => 'Formulário restrito. Os dados gravados estarão disponíveis apenas na área privada.',
	'donnees_pub' => 'Formulário público. Os dados gravados estarão disponíveis para os visitantes do site.',
	'dupliquer' => 'Duplicar',

	// E
	'echec_upload' => 'A transferência do arquivo falhou.',
	'edit_champ_obligatoire' => 'Este campo é obrigatório',
	'editer' => 'Editar',
	'email_independant' => 'E-mail independente da reposta',
	'exporter' => 'Exportar',
	'exporter_article' => 'Exportar em uma matéria',

	// F
	'fichier_trop_gros' => 'Este arquivo é muito grande.',
	'fichier_type_interdit' => 'Este tipo de arquivo é proibido.',
	'form_erreur' => 'Erro :',
	'format_fichier' => 'Formato do arquivo:',
	'format_liste' => 'drop-down',
	'format_liste_ou_radio' => 'Formato da lista',
	'format_radio' => 'botões rádio',
	'forms_obligatoires' => 'Formulários necessários para a edição deste:',
	'formulaire' => 'Formulário',
	'formulaire_aller' => 'Ir para o formulário',
	'formulaires_copie' => 'Cópia de @nom@',
	'formulaires_sondages' => 'Formulários e enquetes',

	// H
	'html_wrapper' => 'Emcapsular o campo no código html',

	// I
	'icone_ajouter_donnees' => 'Incluir respostas',
	'icone_creer_formulaire' => 'Criar um novo formulário',
	'icone_creer_table' => 'Criar um novo formulário',
	'importer_form' => 'Importar um formulário',
	'info_apparence' => 'Esta é uma visualização prévia da aparência do formulário para os visitantes do site público.',
	'info_articles_lies_donnee' => 'As matérias vinculadas',
	'info_champs_formulaire' => 'Aqui, você pode criar e modificar os campos que os visitantes poderão preencher.',
	'info_obligatoire_02' => '[Campo obrigatório]',
	'info_rubriques_liees_donnee' => 'As seções vinculadas',
	'info_sondage' => 'Se o seu formulário é uma enquete, os resultados dos campos do tipo « seleção » serão somados e exibidos.',
	'info_supprimer_formulaire' => 'Você quer mesmo excluir este formulário?',
	'info_supprimer_formulaire_reponses' => 'Há dados associados a este formulário. Você quer realmente excluí-lo?',

	// L
	'lien_apercu' => 'Visualizar',
	'lien_champ' => 'Campos',
	'lien_propriete' => 'Propriedades',
	'lien_retirer_donnee_liante' => 'Retirar o vínculo',
	'lien_retirer_donnee_liee' => 'Retirar o vínculo',
	'lier_articles' => 'Permitir que se associe os dados às matérias',
	'lier_documents' => 'Permitir vincular os documentos aos dados',
	'lier_documents_mail' => 'Vincular os documentos ao e-mail',
	'liste_choix' => 'Lista de escolhas propostas',

	// M
	'moderation_donnees' => 'Validar os dados antes da publicação:',
	'modifiable_donnees' => 'Dados editáveis na área pública:',
	'monetaire_invalide' => 'Campo monetário inválido',
	'monnaie_euro' => 'Euro (€)',
	'multiple_donnees' => 'Inserção dos dados na área pública:',

	// N
	'nb_decimales' => 'Número de decimais',
	'nombre_reponses' => '@nombre@ respostas',
	'nouveau_champ' => 'Campo novo',
	'nouveau_choix' => 'Nova escolha',
	'nouveau_formulaire' => 'Novo formulário',
	'numerique_invalide' => 'Campo numérico inválido',

	// P
	'page' => 'Página',
	'pas_mail_confirmation' => 'Sem e-mail de confirmação',
	'probleme_technique' => 'Problema técnico. Sua resposta não pode ser registrada.',
	'probleme_technique_upload' => 'Problema técnico. A transferência do arquivo falhou.',
	'publication_donnees' => 'Publicação dos dados',

	// R
	'rang' => 'Ranking',
	'remplir_un_champ' => 'Preencha, por favor, ao menos um campo.',
	'reponse' => 'Resposta @id_reponse@',
	'reponse_depuis' => 'Da página',
	'reponse_enregistree' => 'Sua resposta foi registrada!',
	'reponse_envoyee' => 'Resposta enviada em',
	'reponse_envoyee_a' => 'a',
	'reponse_retrovez' => 'Encontre esta resposta na interface de administração:',
	'reponses' => 'respostas',
	'resultats' => 'Resultados: ',

	// S
	'site_introuvable' => 'Esta página web não foi encontrada',
	'sondage_deja_repondu' => 'Você já respondeu a esta enquete.',
	'sondage_non' => 'Este formulário não é uma enquete',
	'sondage_oui' => 'Este formulário é uma enquete.',
	'suivi_formulaire' => 'Acompanhamento do formulário',
	'suivi_formulaires' => 'Acompanhamento dos formulários',
	'suivi_reponses' => 'Acompanhamento das respostas',
	'supprimer' => 'Excluir',
	'supprimer_champ' => 'Excluir este campo',
	'supprimer_choix' => 'excluir esta opção',
	'supprimer_formulaire' => 'Excluir este formulário',
	'supprimer_reponse' => 'Excluir esta resposta',

	// T
	'tables' => 'Tabelas',
	'taille_max' => 'Tamanho máximo (em KB)',
	'telecharger' => 'Transferir',
	'telecharger_reponses' => 'Transferir as respostas',
	'titre_formulaire' => 'Título do formulário',
	'total_votes' => 'Total dos votos',
	'tous_formulaires' => 'Todos os formulários',
	'tous_sondages' => 'Todas as enquetes',
	'tous_sondages_proteges' => 'Todas as enquetes restritas',
	'tous_sondages_public' => 'Todas as enquetes públicas',
	'toutes_tables' => 'Todos os formulários',
	'type_form' => 'Tipo de formulário',

	// U
	'une_reponse' => 'um dado',
	'unite_monetaire' => 'Unidade monetária',

	// V
	'valider' => 'Enviar',
	'verif_web' => 'verificar a existência do website',
	'vider' => 'Limpar',
	'voir_article' => 'Consultar a matéria',
	'voir_resultats' => 'Ver os resultados'
);

?>
