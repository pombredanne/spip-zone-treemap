<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/table?lang_cible=tr
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Bu tabloyu kullanan makaleler',
	'aucune_reponse' => 'Hiçbir veri yok',

	// C
	'champs_formulaire' => 'Tablo alanları',

	// F
	'formulaire' => 'Tablo',

	// I
	'icone_ajouter_donnees' => 'Veri ekle',
	'icone_creer_table' => 'Yeni bir tablo oluştur',
	'importer_donnees_csv' => 'Veri ithal et',
	'info_supprimer_formulaire' => 'Bu tabloyu gerçekten silmek istiyor musunuz ?',
	'info_supprimer_formulaire_reponses' => 'Bu tabloyla ilintili veriler var. Gerçekten silmek istiyor musunuz ?',

	// L
	'lien_retirer_donnee_liante' => 'Bu veriden gelen bağı kaldır',
	'lien_retirer_donnee_liee' => 'Bu veriyi kaldır',

	// N
	'nombre_reponses' => '@nombre@ veri',
	'nouveau_formulaire' => 'Yeni tablo',

	// S
	'suivi_reponses' => 'Verileri göster',
	'supprimer_formulaire' => 'Bu tabloyu sil',

	// T
	'telecharger_reponses' => 'Verileri indir',
	'texte_donnee_statut' => 'Bu verinin durumu',
	'texte_statut_poubelle' => 'Silinen veri',
	'texte_statut_prepa' => 'Yazılmakta olan veri',
	'texte_statut_prop' => 'Önerilen veri',
	'texte_statut_publie' => 'Yayınlanan veri',
	'texte_statut_refuse' => 'Reddedilen veri',
	'titre_formulaire' => 'Tablo başlığı',
	'toutes_tables' => 'Tüm tablolar',
	'type_des_tables' => 'Tablolar',

	// U
	'une_reponse' => 'Bir veri'
);

?>
