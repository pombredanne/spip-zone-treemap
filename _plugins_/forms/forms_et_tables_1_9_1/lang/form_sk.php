<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/form?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Články, ktoré využívajú tento formulár',
	'aucune_reponse' => 'Žiadna odpoveď',

	// C
	'champs_formulaire' => 'Polia formulára',

	// F
	'formulaire' => 'Formulár',

	// I
	'icone_ajouter_donnees' => 'Pridať odpovede',
	'icone_creer_table' => 'Vytvoriť nový formulár',
	'importer_donnees_csv' => 'Nahrávajú sa odpovede',
	'info_supprimer_formulaire' => 'Naozaj chcete odstrániť tento formulár?',
	'info_supprimer_formulaire_reponses' => 'K tomuto formuláru je priradených niekoľko odpovedí. 
Naozaj ho chcete odstrániť?',

	// L
	'lien_retirer_donnee_liante' => 'Odstrániť odkaz z tejto odpovede',
	'lien_retirer_donnee_liee' => 'Odstrániť túto odpoveď',

	// N
	'nombre_reponses' => 'odpovedí: @nombre@',
	'nouveau_formulaire' => 'Nový formulár',

	// S
	'suivi_reponses' => 'Zobraziť odpovede',
	'supprimer_formulaire' => 'Odstrániť tento formulár',

	// T
	'telecharger_reponses' => 'Stiahnite si odpovede',
	'texte_donnee_statut' => 'Stav spracovania tejto odpovede',
	'texte_statut_poubelle' => 'Odstránená odpoveď',
	'texte_statut_prepa' => 'Odpoveď sa upravuje',
	'texte_statut_prop' => 'Odoslaná odpoveď',
	'texte_statut_publie' => 'Publikovaná odpoveď',
	'texte_statut_refuse' => 'Zamietnutá odpoveď',
	'titre_formulaire' => 'Názov formulára',
	'toutes_tables' => 'Všetky formuláre',
	'type_des_tables' => 'Formuláre',

	// U
	'une_reponse' => 'Jedna odpoveď'
);

?>
