<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/table?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Články, ktoré využívajú túto tabuľku',
	'aucune_reponse' => 'Žiadne údaje',

	// C
	'champs_formulaire' => 'Polia tabuľky',

	// F
	'formulaire' => 'Tabuľka',

	// I
	'icone_ajouter_donnees' => 'Pridať údaje',
	'icone_creer_table' => 'Vytvoriť tabuľku',
	'importer_donnees_csv' => 'Nahrať údaje',
	'info_supprimer_formulaire' => 'Naozaj chcete vymazať túto tabuľku?',
	'info_supprimer_formulaire_reponses' => 'K tejto tabuľke sú priradené existujúce údaje. Naozaj ju chcete vymazať?',

	// L
	'lien_retirer_donnee_liante' => 'Odstrániť prepojenie z tejto položky dát',
	'lien_retirer_donnee_liee' => 'Odstrániť tento údaj',

	// N
	'nombre_reponses' => '@nombre@ údajov',
	'nouveau_formulaire' => 'Nová tabuľka',

	// S
	'suivi_reponses' => 'Zobraziť údaje',
	'supprimer_formulaire' => 'Vymazať túto tabuľku',

	// T
	'telecharger_reponses' => 'Stiahnuť údaje',
	'texte_donnee_statut' => 'Stav tohto údaja',
	'texte_statut_poubelle' => 'Údaj odstránený',
	'texte_statut_prepa' => 'Údaj sa upravuje',
	'texte_statut_prop' => 'Navrhovaný údaj',
	'texte_statut_publie' => 'Publikovaný údaj',
	'texte_statut_refuse' => 'Zamietnutý údaj',
	'titre_formulaire' => 'Názov tabuľky',
	'toutes_tables' => 'Všetky tabuľky',
	'type_des_tables' => 'Tabuľky',

	// U
	'une_reponse' => 'Jeden údaj'
);

?>
