<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Art&iacute;culos que usan este formulario',
	'aucune_reponse' => 'No hay respuestas',

	// C
	'champs_formulaire' => 'Campos del formulario',

	// F
	'formulaire' => 'Formulario',

	// I
	'icone_ajouter_donnees' => 'A&ntilde;adir respuestas',
	'icone_creer_table' => 'Crear un formulario nuevo',
	'importer_donnees_csv' => 'Importar respuestas',
	'info_supprimer_formulaire' => '&iquest;Realmente quieres suprimir este formulario?',
	'info_supprimer_formulaire_reponses' => 'Hay respuestas asociadas con el formulario. 
&iquest;Realmente quieres suprimirlo?',

	// L
	'lien_retirer_donnee_liante' => 'Retirar el enlace de esta respuesta',
	'lien_retirer_donnee_liee' => 'Retirar esta respuesta',

	// N
	'nombre_reponses' => '@nombre@ respuestas',
	'nouveau_formulaire' => 'Nuevo formulario',

	// S
	'suivi_reponses' => 'Ver las respuestas',
	'supprimer_formulaire' => 'Suprimir este formulario',

	// T
	'telecharger_reponses' => 'Descarga las respuestas',
	'texte_donnee_statut' => 'Estado de esta respuesta',
	'texte_statut_poubelle' => 'Respuesta suprimida',
	'texte_statut_prepa' => 'Respuesta en proceso de redacci&oacute;n',
	'texte_statut_prop' => 'Respuesta propuesta',
	'texte_statut_publie' => 'Respuesta publicada',
	'texte_statut_refuse' => 'Respuesta rehusada',
	'titre_formulaire' => 'T&iacute;tulo del formulario',
	'toutes_tables' => 'Todos los formularios',
	'type_des_tables' => 'Formularios',

	// U
	'une_reponse' => 'Una respuesta'
);

?>
