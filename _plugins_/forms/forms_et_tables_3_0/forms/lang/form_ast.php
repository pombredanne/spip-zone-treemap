<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Art&iacute;culos que usen esti formulariu',
	'aucune_reponse' => 'Denguna respuesta',

	// C
	'champs_formulaire' => 'Campos del formulariu',

	// F
	'formulaire' => 'Formulariu',

	// I
	'icone_ajouter_donnees' => 'A&ntilde;adir les respuestes',
	'icone_creer_table' => 'Crear un nuevu formulariu',
	'importer_donnees_csv' => 'Importar les respuestes',
	'info_supprimer_formulaire' => '&iquest;De verd&aacute; quies desaniciar esti formulariu?',
	'info_supprimer_formulaire_reponses' => 'Hai respuestes asoci&aacute;es con esti formulariu. 
&iquest;De verd&aacute; quies desanicialu?',

	// L
	'lien_retirer_donnee_liante' => 'Retirar el enllaz d\'esta respuesta',
	'lien_retirer_donnee_liee' => 'Retirar esta respuesta',

	// N
	'nombre_reponses' => '@nombre@ respuestes',
	'nouveau_formulaire' => 'Nuevu formulariu',

	// S
	'suivi_reponses' => 'Ver les respuestes',
	'supprimer_formulaire' => 'Desaniciar esti formulariu',

	// T
	'telecharger_reponses' => 'Descargar les respuestes',
	'texte_donnee_statut' => 'Est&aacute;u d\'esta respuesta',
	'texte_statut_poubelle' => 'Respuesta desaniciada',
	'texte_statut_prepa' => 'Respuesta en cursu de redaici&oacute;n',
	'texte_statut_prop' => 'Repuesta propuesta',
	'texte_statut_publie' => 'Respuesta publicada',
	'texte_statut_refuse' => 'Respuesta refugada',
	'titre_formulaire' => 'T&iacute;tulu del formulariu',
	'toutes_tables' => 'Toos los formularios',
	'type_des_tables' => 'Formularios',

	// U
	'une_reponse' => 'Una respuesta'
);

?>
