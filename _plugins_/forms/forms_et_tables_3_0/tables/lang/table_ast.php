<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Art&iacute;culos qu\'usen esta tabla',
	'aucune_reponse' => 'Deng&uacute;n datu',

	// C
	'champs_formulaire' => 'Campos de la tabla',

	// F
	'formulaire' => 'Tabla',

	// I
	'icone_ajouter_donnees' => 'Amestar datos',
	'icone_creer_table' => 'Crear una tabla nueva',
	'importer_donnees_csv' => 'Importar datos',
	'info_supprimer_formulaire' => '&iquest;De verd&aacute; quies desaniciar esta tabla?',
	'info_supprimer_formulaire_reponses' => 'Hai datos asociaos con esta tabla. &iquest;De verd&aacute; quies desaniciala?',

	// L
	'lien_retirer_donnee_liante' => 'Quitar l\'enllaz d\'esti datu',
	'lien_retirer_donnee_liee' => 'Desaniciar esti datu',

	// N
	'nombre_reponses' => '@nombre@ datos',
	'nouveau_formulaire' => 'Tabla nueva',

	// S
	'suivi_reponses' => 'Ver los datos',
	'supprimer_formulaire' => 'Desaniciar esta tabla',

	// T
	'telecharger_reponses' => 'Descargar los datos',
	'texte_donnee_statut' => 'Est&aacute;u d\'esti datu',
	'texte_statut_poubelle' => 'Datu desanici&aacute;u',
	'texte_statut_prepa' => 'Datu en cursu de redaici&oacute;n',
	'texte_statut_prop' => 'Datu propuestu',
	'texte_statut_publie' => 'Datu espubliz&aacute;u',
	'texte_statut_refuse' => 'Datu refug&aacute;u',
	'titre_formulaire' => 'T&iacute;tulu de la tabla',
	'toutes_tables' => 'Toes les tables',
	'type_des_tables' => 'Tables',

	// U
	'une_reponse' => 'Un datu'
);

?>
