<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_barre_typo' => 'Ativar a barra tipogr&aacute;fica',
	'adresse_invalide' => 'Este endere&ccedil;o n&atilde;o &eacute; v&aacute;lido',
	'afficher' => 'Exibir',
	'aide_contextuelle' => 'Ajuda contextual',
	'ajouter_champ' => 'Incluir um campo',
	'ajouter_champ_type' => 'Criar um campo do tipo:',
	'ajouter_choix' => 'Incluir uma op&ccedil;&atilde;o',
	'apparence_formulaire' => 'Apar&ecirc;ncia do formul&aacute;rio de inscri&ccedil;&atilde;o',
	'article_inserer_un_formulaire' => 'Inserir um formul&aacute;rio',
	'article_inserer_un_formulaire_detail' => 'Voc&ecirc; pode inserir formul&aacute;rios em seus artigos, para solicitar informa&ccedil;&otilde;es aos visitantes. Escolha um formul&aacute;rio na lista abaixo e copie a tag no artigo.',
	'article_recopier_raccourci' => 'Copie a tag no texto do artigo para inserir este formul&aacute;rio',
	'articles_utilisant' => 'Mat&eacute;rias que usam este formul&aacute;rio',
	'attention' => 'Aten&ccedil;&atilde;o:',
	'aucune_reponse' => 'sem dados',
	'avis_message_confirmation' => 'Uma mensagem de confirma&ccedil;&atilde;o &eacute; envida a @mail@',

	// B
	'boite_info' => 'Clique nos &iacute;cones para ver os dados associados a um formul&aacute;rio, edit&aacute;-lo, duplic&aacute;-lo, export&aacute;-lo, limpar dos dados ou exclu&iacute;-lo.',

	// C
	'cfg_activer' => 'Sim',
	'cfg_associer_donnees' => 'Associar os dados das tabelas',
	'cfg_associer_donnees_articles' => 'Permitir a associa&ccedil;&atilde;o dos dados &agrave;s mat&eacute;rias:',
	'cfg_associer_donnees_auteurs' => 'Permitir a associa&ccedil;&atilde;o dos dados aos autores:',
	'cfg_associer_donnees_rubriques' => 'Permitir a associa&ccedil;&atilde;o dos dados &agrave;s se&ccedil;&otilde;es:',
	'cfg_bouton_type_image' => 'bot&atilde;o do tipo imagem',
	'cfg_bouton_type_texte' => 'bot&atilde;o do tipo texto (submit)',
	'cfg_bouton_valider' => 'Tipo de bot&atilde;o validar',
	'cfg_bouton_valider_texte' => 'Voc&ecirc; pode especificar que tipo de bot&atilde;o Validar usar nos formul&aacute;rios:',
	'cfg_desactiver' => 'N&atilde;o',
	'cfg_inserer_head' => 'Inclus&atilde;o no &lt;head&gt;',
	'cfg_inserer_head_texte' => 'Inserir automaticamente as chamadas aos arquivos seguintes na se&ccedil;&atilde;o &lt;head&gt; das p&aacute;ginas do site (recomendado):',
	'champ_descendre' => 'descer',
	'champ_email_details' => 'Por favor, informe um endere&ccedil;o eletr&ocirc;nico v&aacute;lido (tipo voce@provedor.com.br)',
	'champ_listable_admin' => 'Exibir este campo nas listas do espa&ccedil;o privado',
	'champ_listable_publique' => 'Exibir este campo nas listas publicadas',
	'champ_monter' => 'subir',
	'champ_necessaire' => 'Este campo precisa ser preenchido',
	'champ_nom' => 'Nome do campo',
	'champ_nom_bloc' => 'Nome do bloco',
	'champ_nom_groupe' => 'Grupo',
	'champ_nom_texte' => 'Texto',
	'champ_public' => 'Este campo &eacute; vis&iacute;vel no espa&ccedil;o p&uacute;blico',
	'champ_saisie_desactivee' => 'Desativar a entrada neste campo',
	'champ_specifiant' => 'Este campo qualifica os dados (ordenac&atilde;o, filtro, descri&ccedil;&atilde;o)',
	'champ_table_jointure_type' => 'Tipo de tabela para as jun&ccedil;&otilde;es',
	'champ_type_date' => 'Data',
	'champ_type_email' => 'endere&ccedil;o eletr&ocirc;nico',
	'champ_type_fichier' => 'arquivo a enviar',
	'champ_type_joint' => 'Jun&ccedil;&atilde;o com uma outra tabela',
	'champ_type_ligne' => 'linha de texto',
	'champ_type_monnaie' => 'Monet&aacute;rio',
	'champ_type_mot' => 'palavras-chaves',
	'champ_type_multiple' => 'm&uacute;ltiplas escolhas',
	'champ_type_numerique' => 'Num&eacute;rico',
	'champ_type_password' => 'Senha',
	'champ_type_select' => 'escolha &uacute;nica',
	'champ_type_separateur' => 'novo bloco de quest&otilde;es',
	'champ_type_texte' => 'texto',
	'champ_type_textestatique' => 'mensagem explicativa',
	'champ_type_url' => 'endere&ccedil;o de p&aacute;gina web',
	'champ_url_details' => 'Por favor, informe uma p&aacute;gina web v&aacute;lida (tipo http://www.meusite.com.br)',
	'champs_formulaire' => 'Campos do formul&aacute;rio',
	'changer_choix_multiple' => 'Mudar para m&uacute;ltipla escolha',
	'changer_choix_unique' => 'Mudar para escolha &uacute;nica',
	'choisir_email' => 'Escolher o e-mail em fun&ccedil;&atilde;o de',
	'confirm_supprimer_champ' => 'Voc&ecirc;s tem certeza de que quer excluir o campo  \'@champ@\' ?',
	'confirm_supprimer_donnee' => 'Voc&ecirc; tem certeza de que quer excluir o dado \'@donnee@\' ?',
	'confirm_vider_table' => 'Voc&ecirc; est&aacute; certo de que quer apagar o conte&uacute;do da tabela \'@table@\'?',
	'confirmer_champ_password' => 'Nome para campo duplo (confirma&ccedil;&atilde;o)',
	'confirmer_password' => 'Confirma&ccedil;&atilde;o',
	'confirmer_reponse' => 'Enviar um e-mail de aviso de recep&ccedil;&atilde;o com:',
	'csv_classique' => 'CSV classico (,)',
	'csv_excel' => 'CSV para Excel (;)',
	'csv_tab' => 'CSV com tabs',

	// D
	'date' => 'Data',
	'date_invalide' => 'Formato de data inv&aacute;lido',
	'donnees_modifiable' => 'Dados que o usu&aacute;rio pode editar.',
	'donnees_multiple' => 'M&uacute;ltipla escolha.',
	'donnees_nonmodifiable' => 'Dados que o usu&aacute;rio n&atilde;o pode editar.',
	'donnees_nonmultiple' => '&Uacute;nica escolha.',
	'donnees_prot' => 'Formul&aacute;rio restrito. Os dados gravados estar&atilde;o dispon&iacute;veis apenas na &aacute;rea privada.',
	'donnees_pub' => 'Formul&aacute;rio p&uacute;blico. Os dados gravados estar&atilde;o dispon&iacute;veis para os visitantes do site.',
	'dupliquer' => 'Duplicar',

	// E
	'echec_upload' => 'A transfer&ecirc;ncia do arquivo falhou.',
	'edit_champ_obligatoire' => 'Este campo &eacute; obrigat&oacute;rio',
	'editer' => 'Editar',
	'email_independant' => 'E-mail independente da reposta',
	'exporter' => 'Exportar',
	'exporter_article' => 'Exportar em uma mat&eacute;ria',

	// F
	'fichier_trop_gros' => 'Este arquivo &eacute; muito grande.',
	'fichier_type_interdit' => 'Este tipo de arquivo &eacute; proibido.',
	'form_erreur' => 'Erro&nbsp;:',
	'format_fichier' => 'Formato do arquivo:',
	'format_liste' => 'drop-down',
	'format_liste_ou_radio' => 'Formato da lista',
	'format_radio' => 'bot&otilde;es r&aacute;dio',
	'forms_obligatoires' => 'Formul&aacute;rios necess&aacute;rios para a edi&ccedil;&atilde;o deste:',
	'formulaire' => 'Formul&aacute;rio',
	'formulaire_aller' => 'Ir para o formul&aacute;rio',
	'formulaires_copie' => 'C&oacute;pia de @nom@',
	'formulaires_sondages' => 'Formul&aacute;rios e enquetes',

	// H
	'html_wrapper' => 'Emcapsular o campo no c&oacute;digo html',

	// I
	'icone_ajouter_donnees' => 'Incluir respostas',
	'icone_creer_formulaire' => 'Criar um novo formul&aacute;rio',
	'icone_creer_table' => 'Criar um novo formul&aacute;rio',
	'importer_form' => 'Importar um formul&aacute;rio',
	'info_apparence' => 'Esta &eacute; uma visualiza&ccedil;&atilde;o pr&eacute;via da apar&ecirc;ncia do formul&aacute;rio para os visitantes do site p&uacute;blico.',
	'info_articles_lies_donnee' => 'As mat&eacute;rias vinculadas',
	'info_champs_formulaire' => 'Aqui, voc&ecirc; pode criar e modificar os campos que os visitantes poder&atilde;o preencher.',
	'info_obligatoire_02' => '[Campo obrigat&oacute;rio]',
	'info_rubriques_liees_donnee' => 'As se&ccedil;&otilde;es vinculadas',
	'info_sondage' => 'Se o seu formul&aacute;rio &eacute; uma enquete, os resultados dos campos do tipo &laquo; sele&ccedil;&atilde;o &raquo; ser&atilde;o somados e exibidos.',
	'info_supprimer_formulaire' => 'Voc&ecirc; quer mesmo excluir este formul&aacute;rio?',
	'info_supprimer_formulaire_reponses' => 'H&aacute; dados associados a este formul&aacute;rio. Voc&ecirc; quer realmente exclu&iacute;-lo?',

	// L
	'lien_apercu' => 'Visualizar',
	'lien_champ' => 'Campos',
	'lien_propriete' => 'Propriedades',
	'lien_retirer_donnee_liante' => 'Retirar o v&iacute;nculo',
	'lien_retirer_donnee_liee' => 'Retirar o v&iacute;nculo',
	'lier_articles' => 'Permitir que se associe os dados &agrave;s mat&eacute;rias',
	'lier_documents' => 'Permitir vincular os documentos aos dados',
	'lier_documents_mail' => 'Vincular os documentos ao e-mail',
	'liste_choix' => 'Lista de escolhas propostas',

	// M
	'moderation_donnees' => 'Validar os dados antes da publica&ccedil;&atilde;o:',
	'modifiable_donnees' => 'Dados edit&aacute;veis na &aacute;rea p&uacute;blica:',
	'monetaire_invalide' => 'Campo monet&aacute;rio inv&aacute;lido',
	'monnaie_euro' => 'Euro (&euro;)',
	'multiple_donnees' => 'Inser&ccedil;&atilde;o dos dados na &aacute;rea p&uacute;blica:',

	// N
	'nb_decimales' => 'N&uacute;mero de decimais',
	'nombre_reponses' => '@nombre@ respostas',
	'nouveau_champ' => 'Campo novo',
	'nouveau_choix' => 'Nova escolha',
	'nouveau_formulaire' => 'Novo formul&aacute;rio',
	'numerique_invalide' => 'Campo num&eacute;rico inv&aacute;lido',

	// P
	'page' => 'P&aacute;gina',
	'pas_mail_confirmation' => 'Sem e-mail de confirma&ccedil;&atilde;o',
	'probleme_technique' => 'Problema t&eacute;cnico. Sua resposta n&atilde;o pode ser registrada.',
	'probleme_technique_upload' => 'Problema t&eacute;cnico. A transfer&ecirc;ncia do arquivo falhou.',
	'publication_donnees' => 'Publica&ccedil;&atilde;o dos dados',

	// R
	'rang' => 'Ranking',
	'remplir_un_champ' => 'Preencha, por favor, ao menos um campo.',
	'reponse' => 'Resposta @id_reponse@',
	'reponse_depuis' => 'Da p&aacute;gina',
	'reponse_enregistree' => 'Sua resposta foi registrada!',
	'reponse_envoyee' => 'Resposta enviada em',
	'reponse_envoyee_a' => 'a',
	'reponse_retrovez' => 'Encontre esta resposta na interface de administra&ccedil;&atilde;o:',
	'reponses' => 'respostas',
	'resultats' => 'Resultados: ',

	// S
	'site_introuvable' => 'Esta p&aacute;gina web n&atilde;o foi encontrada',
	'sondage_deja_repondu' => 'Voc&ecirc; j&aacute; respondeu a esta enquete.',
	'sondage_non' => 'Este formul&aacute;rio n&atilde;o &eacute; uma enquete',
	'sondage_oui' => 'Este formul&aacute;rio &eacute; uma enquete.',
	'suivi_formulaire' => 'Acompanhamento do formul&aacute;rio',
	'suivi_formulaires' => 'Acompanhamento dos formul&aacute;rios',
	'suivi_reponses' => 'Acompanhamento das respostas',
	'supprimer' => 'Excluir',
	'supprimer_champ' => 'Excluir este campo',
	'supprimer_choix' => 'excluir esta op&ccedil;&atilde;o',
	'supprimer_formulaire' => 'Excluir este formul&aacute;rio',
	'supprimer_reponse' => 'Excluir esta resposta',

	// T
	'tables' => 'Tabelas',
	'taille_max' => 'Tamanho m&aacute;ximo (em KB)',
	'telecharger' => 'Transferir',
	'telecharger_reponses' => 'Transferir as respostas',
	'titre_formulaire' => 'T&iacute;tulo do formul&aacute;rio',
	'total_votes' => 'Total dos votos',
	'tous_formulaires' => 'Todos os formul&aacute;rios',
	'tous_sondages' => 'Todas as enquetes',
	'tous_sondages_proteges' => 'Todas as enquetes restritas',
	'tous_sondages_public' => 'Todas as enquetes p&uacute;blicas',
	'toutes_tables' => 'Todos os formul&aacute;rios',
	'type_form' => 'Tipo de formul&aacute;rio',

	// U
	'une_reponse' => 'um dado',
	'unite_monetaire' => 'Unidade monet&aacute;ria',

	// V
	'valider' => 'Enviar',
	'verif_web' => 'verificar a exist&ecirc;ncia do website',
	'vider' => 'Limpar',
	'voir_article' => 'Consultar a mat&eacute;ria',
	'voir_resultats' => 'Ver os resultados'
);

?>
