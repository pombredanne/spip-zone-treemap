<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_barre_typo' => 'Activar la barra tipogr&aacute;fica',
	'adresse_invalide' => 'Esta direici&oacute;n nun ye v&aacute;lida.',
	'afficher' => 'Amosar',
	'aide_contextuelle' => 'Aida contestual',
	'ajouter_champ' => 'A&ntilde;adir un campu',
	'ajouter_champ_type' => 'Crear un campu de tipu:',
	'ajouter_choix' => 'A&ntilde;ader una seleici&oacute;n',
	'apparence_formulaire' => 'Aspeutu del formulariu d\'edici&oacute;n',
	'article_inserer_un_formulaire' => 'Amestar un formulariu',
	'article_inserer_un_formulaire_detail' => 'Vas poder amestar formularios a los tos art&iacute;culos pa permiti-yos a los visitantes inxertar informaci&oacute;n. Escueye un formulariu de la llista d\'embaxo y copia l\'atayu nel testu de l\'art&iacute;culu.',
	'article_recopier_raccourci' => 'Copia esti atayu nel testu de l\'art&iacute;culu pa amesta-y esti formulariu',
	'articles_utilisant' => 'Art&iacute;culos que tan usando esti formulariu',
	'attention' => 'Atenci&oacute;n:',
	'aucune_reponse' => 'deng&uacute;n datu',
	'avis_message_confirmation' => 'Un mensaxe de confirmaci&oacute;n ta acabante d\'unviase a @mail@',

	// B
	'boite_info' => 'Pica enriba los iconos pa ver los datos asociaos con un formulariu, editalu, duplicalu, esportalu, vacialu de datos o suprimilu.',

	// C
	'cfg_activer' => 'Si',
	'cfg_associer_donnees' => 'Rellacionar los datos de les tables',
	'cfg_associer_donnees_articles' => 'Permitir rellacionar los datos colos art&iacute;culos:',
	'cfg_associer_donnees_auteurs' => 'Permitir rellacionar los datos colos autores:',
	'cfg_associer_donnees_rubriques' => 'Permitir rellacionar los datos coles seiciones:',
	'cfg_bouton_type_image' => 'bot&oacute;n de tipu imaxe',
	'cfg_bouton_type_texte' => 'bot&oacute;n de tipu testu (unviar)',
	'cfg_bouton_valider' => 'Tipu de bot&oacute;n validar',
	'cfg_bouton_valider_texte' => 'Puedes escoyer qu&eacute; tipu de bot&oacute;n validar va utilizase nos formularios:',
	'cfg_desactiver' => 'Non',
	'cfg_inserer_head' => 'Inxertar nel &lt;head&gt;',
	'cfg_inserer_head_texte' => 'Inxertar autom&aacute;ticamente les llam&aacute;es a los ficheros siguientes na seici&oacute;n &lt;head&gt; de les p&aacute;xines del sitiu (encamentao):',
	'champ_descendre' => 'baxar',
	'champ_email_details' => 'Hai qu\'escribir unes se&ntilde;es d\'e-mail v&aacute;lides (del tipu nome@eltocorreu.com).',
	'champ_listable_admin' => 'Amosar esti campu nes llistes de l\'espaciu privao',
	'champ_listable_publique' => 'Amosar esti campu nes llistes p&uacute;bliques',
	'champ_monter' => 'xubir',
	'champ_necessaire' => 'Hai qu\'enllenar esti campu.',
	'champ_nom' => 'Nome del campu',
	'champ_nom_bloc' => 'Nome del bloque',
	'champ_nom_groupe' => 'Grupu',
	'champ_nom_texte' => 'Testu',
	'champ_public' => 'Esti campu ye visible dende l\'espaciu p&uacute;blicu',
	'champ_saisie_desactivee' => 'Desactivar la edici&oacute;n d\'esti campu',
	'champ_specifiant' => 'Esti campu clasifica\'l datu (orden, filtru, descripci&oacute;n)',
	'champ_table_jointure_type' => 'Tipu de tabla pa les xuntures',
	'champ_type_date' => 'Fecha',
	'champ_type_email' => 'Se&ntilde;es d\'e-mail',
	'champ_type_fichier' => 'Ficheru a descargar',
	'champ_type_joint' => 'Xuntura con otra tabla',
	'champ_type_ligne' => 'Ll&iacute;nia de testu',
	'champ_type_monnaie' => 'Monetariu',
	'champ_type_mot' => 'Pallabres-clave',
	'champ_type_multiple' => 'Escoyeta m&uacute;ltiple',
	'champ_type_numerique' => 'Numb&eacute;ricu',
	'champ_type_password' => 'Contrase&ntilde;a',
	'champ_type_select' => 'Escoyeta &uacute;nica',
	'champ_type_separateur' => 'Nuevu bloque d\'entrugues',
	'champ_type_texte' => 'Testu',
	'champ_type_textestatique' => 'Mensaxe d\'esplicaci&oacute;n',
	'champ_type_url' => 'Se&ntilde;es del sitiu Web',
	'champ_url_details' => 'Hai qu\'inxertar unes se&ntilde;es Web v&aacute;lides (del tipu http://www.elmiositiu.com/...).',
	'champs_formulaire' => 'Campos del formulariu',
	'changer_choix_multiple' => 'Cambear a escoyeta m&uacute;ltiple',
	'changer_choix_unique' => 'Cambear a escoyeta &uacute;nica',
	'choisir_email' => 'Escoyer l\'e-mail en funci&oacute;n de',
	'confirm_supprimer_champ' => '&iquest;Est&aacute;s seguru de que quies desaniciar el campu \'@champ@\'?',
	'confirm_supprimer_donnee' => '&iquest;De xuru quies desaniciar el datu \'@donnee@\'?',
	'confirm_vider_table' => '&iquest;De xuru quies vaciar el conten&iacute;u de la tabla \'@table@\'?',
	'confirmer_champ_password' => 'Testu pa la escritura doble',
	'confirmer_password' => 'Confirmaci&oacute;n',
	'confirmer_reponse' => 'Enviar un corr&eacute;u pa confirmar la receici&oacute;n con:',
	'csv_classique' => 'CSV cl&aacute;sicu (,)',
	'csv_excel' => 'CSV pa Excel (;)',
	'csv_tab' => 'CSV con tabulaciones',

	// D
	'date' => 'Fecha',
	'date_invalide' => 'Formatu fecha non v&aacute;lidu',
	'donnees_modifiable' => 'Datos iguables pel usuariu.',
	'donnees_multiple' => 'Respuestes m&uacute;ltiples.',
	'donnees_nonmodifiable' => 'Datos non iguables pel usuariu.',
	'donnees_nonmultiple' => 'Respuesta &uacute;nica.',
	'donnees_prot' => 'Datos protex&iacute;os. Los datos grabaos nun van ser accesibles m&aacute;s que dende l\'interfaz privao.',
	'donnees_pub' => 'Datos p&uacute;blicos. Los datos grabaos tar&aacute;n accesibles pa les visites del sitiu.',
	'dupliquer' => 'Duplicar',

	// E
	'echec_upload' => 'Fallu na tresferencia del ficheru.',
	'edit_champ_obligatoire' => 'Esti campu ye obligatoriu',
	'editer' => 'Iguar',
	'email_independant' => 'Email independiente de la respuesta',
	'exporter' => 'Esportar',
	'exporter_article' => 'Esportar nun art&iacute;culu',

	// F
	'fichier_trop_gros' => 'Esti ficheru ye demasiao grande.',
	'fichier_type_interdit' => 'Esti tipu de ficheru ta torgau.',
	'form_erreur' => 'Fallu:',
	'format_fichier' => 'Formatu d\'archivu:',
	'format_liste' => 'desplegable',
	'format_liste_ou_radio' => 'Formatu de la llista',
	'format_radio' => 'botones radio',
	'forms_obligatoires' => 'Formularios obligatorios pa la edici&oacute;n d\'esti:',
	'formulaire' => 'Formulariu',
	'formulaire_aller' => 'Dir pal formulariu',
	'formulaires_copie' => 'Copia de @nom@',
	'formulaires_sondages' => 'Formularios y encuestes',

	// H
	'html_wrapper' => 'Enxertar el campu nel c&oacute;digu html',

	// I
	'icone_ajouter_donnees' => 'Amestar respuestes',
	'icone_creer_formulaire' => 'Crear un nuevu formulariu',
	'icone_creer_table' => 'Crear un nuevu formulariu',
	'importer_form' => 'Importar un formulariu',
	'info_apparence' => 'Esta ye una previsualizaci&oacute;n del formulariu tal como-yos apaecer&aacute; a les visites del sitiu p&uacute;blicu.',
	'info_articles_lies_donnee' => 'Los art&iacute;culos enllaz&aacute;os',
	'info_champs_formulaire' => 'Equ&iacute; pues crear y cambear los campos que les visites van poder enllenar.',
	'info_obligatoire_02' => '[Obligatorio]',
	'info_rubriques_liees_donnee' => 'Les estayes enllaz&aacute;es',
	'info_sondage' => 'Si el formulariu ye una encuesta, los resultaos de los campos de tipu &laquo;seleici&oacute;n&raquo; s&uacute;mense y amu&eacute;sense.',
	'info_supprimer_formulaire' => '&iquest;De verd&aacute; qui&eacute;s desaniciar esti formulariu?',
	'info_supprimer_formulaire_reponses' => 'Hai datos asociaos con esti formulariu. &iquest;De verd&aacute; qui&eacute;s desanicialu?',

	// L
	'lien_apercu' => 'G&uuml;eyada',
	'lien_champ' => 'Campos',
	'lien_propriete' => 'Propied&aacute;es',
	'lien_retirer_donnee_liante' => 'Quitar l\'enllace',
	'lien_retirer_donnee_liee' => 'Quitar l\'enllace',
	'lier_articles' => 'Permitir que s\'asocien los datos colos art&iacute;culos',
	'lier_documents' => 'Permitir amestar documentos a los datos',
	'lier_documents_mail' => 'Amestar los documentos al email',
	'liste_choix' => 'Lista les escoyetes propuestes',

	// M
	'moderation_donnees' => 'Validar los datos enantes d\'espublizalos:',
	'modifiable_donnees' => 'Datos camudables nel espaciu p&uacute;blicu:',
	'monetaire_invalide' => 'Campu monetariu non v&aacute;lidu',
	'monnaie_euro' => 'Euro (&euro;)',
	'multiple_donnees' => 'Iguar los datos nel espaciu p&uacute;blicu:',

	// N
	'nb_decimales' => 'N&uacute;mberu decimales',
	'nombre_reponses' => '@nombre@ datos',
	'nouveau_champ' => 'Nuevu campu',
	'nouveau_choix' => 'Nueva eleici&oacute;n',
	'nouveau_formulaire' => 'Nuevu formulariu',
	'numerique_invalide' => 'Campu numb&eacute;ricu non v&aacute;lidu',

	// P
	'page' => 'P&aacute;xina',
	'pas_mail_confirmation' => 'Ensin corr&eacute;u de confirmaci&oacute;n',
	'probleme_technique' => 'Problema t&eacute;cnicu. Lo que iguaste nun va poder tenese en cuenta.',
	'probleme_technique_upload' => 'Problema t&eacute;cnicu. Fall&oacute; la tresferencia de l\'archivu.',
	'publication_donnees' => 'Publicaci&oacute;n de los datos',

	// R
	'rang' => 'Rangu',
	'remplir_un_champ' => 'Ties que llenar polo menos un campu.',
	'reponse' => 'Respuesta @id_reponse@',
	'reponse_depuis' => 'Dende la p&aacute;xina ',
	'reponse_enregistree' => 'La to igua qued&oacute; grabada.',
	'reponse_envoyee' => 'Respuesta unviada el ',
	'reponse_envoyee_a' => 'a',
	'reponse_retrovez' => 'Alcuentra esta respuesta na interfaz d\'alministraci&oacute;n:',
	'reponses' => 'respuestes',
	'resultats' => 'Resultaos: ',

	// S
	'site_introuvable' => 'Esti sitiu nun s\'alcuentra.',
	'sondage_deja_repondu' => 'Ya respondisti a esta encuesta.',
	'sondage_non' => 'Esti formulariu nun ye una encuesta',
	'sondage_oui' => 'Esti formulariu ye una encuesta.',
	'suivi_formulaire' => 'Siguimientu del formulariu',
	'suivi_formulaires' => 'Siguimientu de los formularios',
	'suivi_reponses' => 'Siguimientu de les respuestes',
	'supprimer' => 'Desaniciar',
	'supprimer_champ' => 'Desaniciar esti campu',
	'supprimer_choix' => 'desaniciar esta respuesta',
	'supprimer_formulaire' => 'Desaniciar esti formulariu',
	'supprimer_reponse' => 'Desaniciar esta respuesta',

	// T
	'tables' => 'Tables',
	'taille_max' => 'Tama&ntilde;u m&aacute;ximu (en kb)',
	'telecharger' => 'Tresferir',
	'telecharger_reponses' => 'Tresferir les respuestes',
	'titre_formulaire' => 'T&iacute;tulu del formulariu',
	'total_votes' => 'Votos totales',
	'tous_formulaires' => 'Toos los formularios',
	'tous_sondages' => 'Toes les encuestes',
	'tous_sondages_proteges' => 'Toes les encuestes protex&iacute;es',
	'tous_sondages_public' => 'Toes les encuestes p&uacute;bliques',
	'toutes_tables' => 'Toos los formularios',
	'type_form' => 'Tipu de formulariu',

	// U
	'une_reponse' => 'un datu',
	'unite_monetaire' => 'Unid&aacute; monetaria',

	// V
	'valider' => 'Validar',
	'verif_web' => 'verificar la esistencia del sitiu Web',
	'vider' => 'Vaciar',
	'voir_article' => 'Ver l\'art&iacute;culu',
	'voir_resultats' => 'Ver los resultaos'
);

?>
