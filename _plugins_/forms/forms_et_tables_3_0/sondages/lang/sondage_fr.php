<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/_stable_/forms/forms_et_tables_1_9_1/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_utilisant' => 'Articles utilisant ce sondage',
	'aucune_reponse' => 'Aucune r&eacute;ponse',

	// C
	'champs_formulaire' => 'Questions du sondage',
	'confirm_vider_table' => '&Ecirc;tes-vous certain de vouloir supprimer les r&eacute;ponse au sondage \'@table@\' ?',

	// F
	'formulaire' => 'Sondage',

	// I
	'icone_ajouter_donnees' => 'Ajouter des r&eacute;ponses',
	'icone_creer_table' => 'Cr&eacute;er un nouveau sondage',
	'importer_donnees_csv' => 'Importer des r&eacute;ponses',
	'info_supprimer_formulaire' => 'Voulez-vous vraiment supprimer ce sondage ?',
	'info_supprimer_formulaire_reponses' => 'Des R&eacute;ponse sont associ&eacute;es &agrave; ce sondage. 
Voulez-vous vraiment le supprimer ?',

	// L
	'lien_retirer_donnee_liante' => 'Retirer le lien depuis cette r&eacute;ponse',
	'lien_retirer_donnee_liee' => 'Retirer cette r&eacute;ponse',

	// N
	'nombre_reponses' => '@nb@ r&eacute;ponses',
	'nouveau_formulaire' => 'Nouveau sondage',

	// S
	'suivi_reponses' => 'Voir les r&eacute;ponses',
	'supprimer_formulaire' => 'Supprimer ce sondage',
	'supprimer_reponse' => 'Supprimer cette r&eacute;ponse',

	// T
	'telecharger_reponses' => 'T&eacute;l&eacute;chargez les r&eacute;ponses',
	'texte_donnee_statut' => 'Statut de cette r&eacute;ponse',
	'texte_statut_poubelle' => 'R&eacute;ponse supprim&eacute;e',
	'texte_statut_prepa' => 'R&eacute;ponse en cours de r&eacute;daction',
	'texte_statut_prop' => 'R&eacute;ponse propos&eacute;e',
	'texte_statut_publie' => 'R&eacute;ponse publi&eacute;e',
	'texte_statut_refuse' => 'R&eacute;ponse refus&eacute;e',
	'titre_formulaire' => 'Titre du sondage',
	'toutes_tables' => 'Tous les sondages',
	'type_des_tables' => 'Sondages',

	// U
	'une_reponse' => '1 r&eacute;ponse'
);

?>
