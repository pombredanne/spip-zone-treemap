<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'adresse_invalide'=>"This address is not valid",
'ajouter_champ' => "Add a field",
'ajouter_champ_type' => "Create a field of type&nbsp;:",
'ajouter_choix' => "Add a choice",
'apparence_formulaire' => "Form layout",
'article_inserer_un_formulaire'=> "Insert a form",
'article_inserer_un_formulaire_detail' => "You can insert forms into articles to allow visitors to provide information. Please choose the form in the list below and copy the insertion code into the text of the article.",
'article_recopier_raccourci' => "Copy this insertion code in the text of the article to insert this form",
'articles_utilisant'=> "Articles using this form",
'attention' => "Warning :",
'avis_message_confirmation'=>"A confirmation message was sent by @mail@",

'boite_info' => "Click on a form to modify it or visualise it before suppression",

'champ_type_ligne'=>"line of text",
'champ_type_texte'=>"text",
'champ_type_url'=>"address of web page",
'champ_type_email'=>"e-mail address",
'champ_type_select'=>"unique choice",
'champ_type_multiple'=>"multiple choice",
'champ_type_fichier'=>"files to download",
'champ_type_mot'=>"key-words",
'champ_type_separateur'=>"New question block",
'champ_type_textestatique'=>"Explanation message",
'champ_email_details'=>"Please enter a valid email address (e.g. you@provider.com).",
'champ_url_details'=>"Please enter a valid web address (e.g. http://www.mypage.com/...).",
'champ_monter' => "move up",
'champ_descendre' => "move down",
'champ_necessaire'=>"This field must be filled in.",
'champ_nom' => "Name of the field",
'champ_nom_bloc' => "Name of this block",
'champ_nom_groupe' => "Group",
'champ_nom_texte' => "Text",
'champs_formuaire'=>"Form's fields",
'changer_choix_multiple' => "Change to multiple choice",
'changer_choix_unique' => "Change to unique choice",
'choisir_email' => "Choose the email according to",
'confirmer_reponse' => "Confirm the answer by email using :",
'csv_classique' => "classical CSV (,)",
'csv_excel' => "CSV for Excel (;)",
'csv_tab' => "CSV with tabulations",

'date' => "Date",
'dupliquer' => "Duplicate",

'echec_upload'=>"File transfer failed.",
'edit_champ_obligatoire' => 'This field is mandatory',
'email_independant' => 'Email independent from the answer',
'exporter_article' => 'Export to an article',

'icone_creer_formulaire' => "Create a new form",
'info_apparence' => "This is a preview of the form as it will appear to public site visitors",
'info_champs_formulaire' => "Here, you can edit and create the fields that the visitors will have to fill",
'info_obligatoire_02' => '[Required]',
'info_sondage' => "If your form is a survey, the results of the fields type &laquo; choice &raquo; will be summed and displayed",
'info_supprimer_formulaire' => "Are you sure you want to remove this form?",
'info_supprimer_formulaire_reponses' => "This form has answers; are you sure you want to remove it?",

'liste_choix' => "List of proposed choices",

'fichier_trop_gros'=>"This file is too large.",
'fichier_type_interdit'=>"This file type is forbidden.",
'form_erreur' => "Error:",
'format_fichier' => "File type&nbsp;:",
'formulaire' => "Form",
'formulaire_aller' => "Go to the form",
'formulaires_sondages' => "Forms and surveys",
'formulaires_copie' => "Copy of @nom@", 

'nombre_reponses'=>"@number@ answers",
'nouveau_champ' => "New field",
'nouveau_choix' => "New choice",
'nouveau_formulaire' => "New form",

'page'=>"Page",
'pas_mail_confirmation'=>"No confirmation email",
'probleme_technique'=>"technical problem.Your answer can not be taken into account.",
'probleme_technique_upload'=>"technical problem. File transfer failed.",

'site_introuvable'=>"This web site was not found.",
'sondage'=>"Survey",
'sondage_non'=>"This form is not a survey",
'sondage_pub'=>"This form is a public survey. The results will be accessible by the site's visitors",
'sondage_prot'=>" This form is a protected survey. The results will only be accessible form the private area.",
'suivi_formulaire' => "Form follow-up",
'suivi_formulaires' => "Forms follow-up",
'suivi_reponses' => "Answer follow-up",
'supprimer_champ' => "Remove this field",
'supprimer_choix' => "Remove this choice",
'supprimer_formulaire' => "Remove this form",
'supprimer_reponse' => "Remove this reply",

'remplir_un_champ'=>"Please, fill in at least one field.",
'reponse_enregistree'=>"Your answer has been registered.",
'resultats' => 'Results: ',
'reponse' => 'Answer @id_reponse@',
'reponse_depuis' => 'From the page ', 
'reponse_envoyee' => 'Answer made on the ',
'reponse_envoyee_a' => 'to', 
'reponse_retrovez' => "Find this answer in the private area :",
'reponses' => 'Answers',

'taille_max' => "Maximum size (in Kb)",
'telecharger' => "Download",
'telecharger_reponses' => "Download the answers",
'titre_formulaire' => "Form's title",
'total_votes' => "Number of votes",
'tous_formulaires' => "All the forms",
'tous_sondages_proteges' => "All the protected surveys",
'tous_sondages_public' => "All the public surveys",

'valider'=>"Validate",
'verif_web' => "Check the existence of the Web site",
'voir_article'=>"See the article",
'voir_resultats'=>"See the results",
);


?>