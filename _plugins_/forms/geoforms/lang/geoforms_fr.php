<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// langue / language = fr

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configuration' => 'GeoForms',
	'configuration_titre' => 'Configuration du plugin GeoForms',
	'configuration_warning' => '!!ATTENTION!! : Pour l\'instant, cette interface de configuration NE marche PAS avec SPIP > &agrave; 2.1',
	'configuration_warning2' => '--> Passer plut&ocirc;t par la page de configuration de "GoogleMap Api" (CFG) pour d&eacute;finir la position par d&eacute;faut des cartes et la cl&eacute; API.',
	
	// G
	'geoloc_x'=>'Geolocalisation-X',
	'geoloc_y'=>'Geolocalisation-Y',
	'geoloc_z'=>'Geolocalisation-Z',

	// L
	'latitude_longitude_geox'=>'Latitude',
	'latitude_longitude_geoy'=>'Longitude',
	'latitude_longitude_geoz'=>'Altitude',

	'lambert_geox'=>'X',
	'lambert_geoy'=>'Y',
	'lambert_geoz'=>'Altitude',
	'lambert1' => 'Lambert 1',
	'lambert2' => 'Lambert 2',
	'lambert3' => 'Lambert 3',
	'lambert4' => 'Lambert 4',
	'lambertgc' => 'Lambert Grand Champ',
	'lambert93' => 'Lambert 93',

);

?>
