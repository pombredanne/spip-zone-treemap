<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'info_obligatoire_02' => '[Obligatoire]',
'article_inserer_un_formulaire'=> "Ins&eacute;rer un formulaire",
'article_inserer_un_formulaire_detail' => "Vous pouvez ins&eacute;rer des formulaires dans vos articles afin de permettre aux visiteurs d'entrer des informations. Choisissez un formulaire dans la liste ci-dessous et recopiez le raccourci dans le texte de l'article.",
'article_recopier_raccourci' => "Recopiez ce raccourci dans le texte de l'article pour ins&eacute;rer ce formulaire",
'icone_creer_formulaire' => "Cr&eacute;er un nouveau formulaire",
'champ_type_ligne'=>"ligne de texte",
'champ_type_texte'=>"texte",
'champ_type_url'=>"adresse de site Web",
'champ_type_email'=>"adresse e-mail",
'champ_type_select'=>"choix unique",
'champ_type_multiple'=>"choix multiple",
'champ_type_fichier'=>"fichier &agrave; t&eacute;l&eacute;charger",
'champ_type_mot'=>"mots-cl&eacute;s",
'champ_type_separateur'=>"Nouveau bloc de questions",
'champ_type_textestatique'=>"Message d'explication",
'champ_email_details'=>"Veuillez entrer une adresse e-mail valide (de type vous@fournisseur.com).",
'champ_url_details'=>"Veuillez entrer une adresse Web valide (de type http://www.monsite.com/...).",
'form_erreur' => "Erreur&nbsp;:",
'probleme_technique'=>"Probl&egrave;me technique. Votre r&eacute;ponse n'a pas pu &ecirc;tre prise en compte.",
'probleme_technique_upload'=>"Probl&egrave;me technique. Le transfert du fichier a &eacute;chou&eacute;.",
'champ_necessaire'=>"Ce champ doit &ecirc;tre rempli.",
'adresse_invalide'=>"Cette adresse n'est pas valide.",
'site_introuvable'=>"Ce site n'a pas &eacute;t&eacute; trouv&eacute;.",
'echec_upload'=>"Le transfert du fichier a &eacute;chou&eacute;.",
'fichier_trop_gros'=>"Ce fichier est trop gros.",
'fichier_type_interdit'=>"Ce type de fichier est interdit.",
'remplir_un_champ'=>"Veuillez remplir au moins un champ.",
'reponse_enregistree'=>"Votre r&eacute;ponse a &eacute;t&eacute; enregistr&eacute;e.",
'valider'=>"Valider",
'avis_message_confirmation'=>"Un message de confirmation est envoy&eacute; &agrave; @mail@",
'voir_resultats'=>"Voir les r&eacute;sultats",
'nombre_reponses'=>"@nombre@ r&eacute;ponses",
'faites_un_choix' => "Faites un choix"
);


?>