<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Translated from French by Etienne B. aka Loiseau2nuit
// http://www.loiseau2nuit.net -- l.oiseau2nuit [AT] gmail.com

$GLOBALS[$GLOBALS['idx_lang']] = array(


'dictionnaire_indexe' => "indexed dictionnary",

'etoile_1' => "1 star",
'etoile_2' => "2 stars",
'etoile_3' => "3 stars",
'etoile_4' => "4 stars",
'etoile_5' => "5 stars",

'filtrer' => "Filtering :",
'filtrer_aucun' => "No index word with more than @points@ points",
'filtrer_plus_1' => "more than 1 point",
'filtrer_plus_10' => "more than 10 points",
'filtrer_plus_100' => "more than 100 points",

'gestion_idexation' => "Indexation managment",

'index' => "Index @table@",
'indexation_a_jour' => "Upgrading Site\'s indexation data ",
'indexation_forcer' => "Force site's indexation",
'indexation_non_configuree' => "Table indexation not configured",
'indexation_purger' => "Click here to purge indexation tables",
'indexation_relancer' => "Relaunch site's indexation without data purge",
'indexation_resetter' => "Click here to reset indexation settings",
'indexation_statut' => "Indexation status",
'indexer_aucun' => "No element to index",
'info_admin_index' => "This page shows site's indexation level.",
'info_index_tous' => "This page shows the list of indexed words on your
site and their occurencies.",

'moteur_recherche' => "Search engine",

'occurences' => "occurencies",

'score' => "score",

'tous_mots' => "All indexed words",
'tous_mots_table' => "All indexed words : table @table@",
'tout' => "All",

'vocabulaire_indexe' => "See indexed vocabulary"
);


?>
