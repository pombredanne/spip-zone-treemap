<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


'dictionnaire_indexe' => "Dictionnaire indexe",

'etoile_1' => "1 etoile",
'etoile_2' => "2 etoiles",
'etoile_3' => "3 etoiles",
'etoile_4' => "4 etoiles",
'etoile_5' => "5 etoiles",

'filtrer' => "Filtrer :",
'filtrer_aucun' => "Aucun mot indexe avec plus de @points@ points",
'filtrer_plus_1' => "+ de 1 point",
'filtrer_plus_10' => "+ de 10 points",
'filtrer_plus_100' => "+ de 100 points",

'gestion_idexation' => "Gestion de l'indexation",

'index' => "Index @table@",
'indexation_a_jour' => "Mettre &agrave; jour les infos d'indexation du site",
'indexation_forcer' => "Forcer l'indexation du site",
'indexation_non_configuree' => "Indexation de la table non configur&eacute;e",
'indexation_purger' => "Cliquez ici pour purger les tables d'indexation",
'indexation_relancer' => "Relancer l'indexation du site sans purger les donn&eacute;es",
'indexation_resetter' => "Cliquez ici pour resetter les param&egrave;tres d'indexation",
'indexation_statut' => "Statut de l'indexation",
'indexer_aucun' => "Aucun &eacute;l&eacute;ment &agrave; indexer",
'info_admin_index' => "Cette page r&eacute;capitule l'avancement de l'indexation du site.",
'info_index_tous' => "Cette page r&eacute;capitule la liste des mots indexes sur votre site et de leur occurence.",

'moteur_recherche' => "Moteur de recherche",

'occurences' => "occurences",

'score' => "score",

'tous_mots' => "Tous les Mots Indexes",
'tous_mots_table' => "Tous les Mots Indexes : table @table@",
'tout' => "Tout",

'vocabulaire_indexe' => "Voir le vocabulaire indexe"
);


?>
