<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-inserer_modeles
// Langue: fr
// Date: 12-12-2011 23:52:09
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// I
	'inserer_modeles_description' => 'Ce plugin fournit une aide à l\'insertion des modèles dans vos textes à travers des formulaires de saisies. Ces formulaires sont accessibles dans la colonne de droite ou via un bouton dans le porte-plume.',
	'inserer_modeles_slogan' => 'Une aide à l\'insertion des modèles dans vos textes',
);
?>