<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/sedna?lang_cible=km
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'បង្ហាញ អត្ថបទសង្ខេប',
	'afficher_sources' => 'បង្ហាញ ប្រភពនេះ',
	'annee' => 'ឆ្នាំ',
	'articles_recents_court' => 'អត្ថបទថ្មីៗ',

	// C
	'connexion' => 'បញ្ជាប់',

	// D
	'deconnexion' => 'ពិនិត្យចេញ',
	'derniere_syndication' => 'Dernière syndication de ce site effectuée', # NEW
	'deuxjours' => 'ពីរថ្ងៃ',

	// L
	'liens' => 'អត្ថបទ',
	'liens_pluriel' => 'អត្ថបទ នានា',

	// M
	'masquer_resume' => 'បិទបាំង អត្ថបទសង្ខេប',
	'masquer_sources' => 'បិទបាំង ប្រភពនេះ',
	'mois' => 'ខែ',

	// P
	'pas_articles' => 'Pas d\'article dans cette période !', # NEW
	'pas_synchro' => 'Ne pas synchroniser', # NEW
	'preferences' => 'ចំណូលចិត្ត',
	'probleme_de_syndication' => 'problème de syndication', # NEW

	// S
	'semaine' => 'សប្តាហ៍',
	'sources' => 'ប្រភព',
	'synchro' => 'Synchroniser', # NEW
	'synchro_titre' => 'Enregistrer sur le site la liste des articles lus', # NEW
	'syndication_ajour' => 'Mettre à jour maintenant', # NEW
	'syndication_fait' => 'Syndication effectuée', # NEW

	// T
	'toutes' => 'ទាំងអស់'
);

?>
