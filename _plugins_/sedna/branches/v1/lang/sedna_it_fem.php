<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/sedna?lang_cible=it_fem
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Visualizza i riassunti',
	'afficher_sources' => 'Visualizza il sorgente',
	'annee' => 'Anno',
	'articles_recents_court' => 'Articoli recenti',

	// C
	'connexion' => 'Connessione',

	// D
	'deconnexion' => 'Sconnessione',
	'derniere_syndication' => 'Ultima syndication effettuata di questo sito',
	'deuxjours' => 'Due giorni',

	// L
	'liens' => 'articolo',
	'liens_pluriel' => 'articoli',

	// M
	'masquer_resume' => 'Nascondi i riassunti',
	'masquer_sources' => 'Nascondi il sorgente',
	'mois' => 'Mese',

	// P
	'pas_articles' => 'Non ci sono articoli in questo periodo!',
	'pas_synchro' => 'Non sincronizzare',
	'preferences' => 'Preferenze',
	'probleme_de_syndication' => 'problema di syndication',

	// S
	'semaine' => 'Settimana',
	'sources' => 'Fonti',
	'synchro' => 'Sincronizza',
	'synchro_titre' => 'Salva sul sito l\'elenco degli articoli letti',
	'syndication_ajour' => 'Aggiorna adesso',
	'syndication_fait' => 'Syndication effettuata',

	// T
	'toutes' => 'Tutte'
);

?>
