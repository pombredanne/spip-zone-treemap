<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/sedna?lang_cible=bg
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Показване на резюметата',
	'afficher_sources' => 'Показване на източника',
	'annee' => 'Година',
	'articles_recents_court' => 'Скорошни публикации',

	// C
	'connexion' => 'Вход',

	// D
	'deconnexion' => 'Изход',
	'derniere_syndication' => 'Последното обединение от показания източник',
	'deuxjours' => 'Два дена',

	// L
	'liens' => 'статия',
	'liens_pluriel' => 'статии',

	// M
	'masquer_resume' => 'Скриване на резюметата',
	'masquer_sources' => 'Скриване на източника',
	'mois' => 'Месец',

	// P
	'pas_articles' => 'Без публикации през този период!',
	'pas_synchro' => 'Без обединение',
	'preferences' => 'Настройки',
	'probleme_de_syndication' => 'Проблем с обединението',

	// S
	'semaine' => 'Седмица',
	'sources' => 'Източници',
	'synchro' => 'Синхронизация',
	'synchro_titre' => 'Запазване на списъка от прочетените на сайта публикации',
	'syndication_ajour' => 'Актуализиране сега',
	'syndication_fait' => 'Обединението е готово',

	// T
	'toutes' => 'Всички'
);

?>
