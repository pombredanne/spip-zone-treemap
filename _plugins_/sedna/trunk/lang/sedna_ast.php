<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/sedna?lang_cible=ast
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Amosar los resumes',
	'afficher_sources' => 'Amosar esta fonte',
	'annee' => 'Añu',
	'articles_recents_court' => 'Artículos recientes',

	// C
	'connexion' => 'Conexón',

	// D
	'deconnexion' => 'Desconexón',
	'derniere_syndication' => 'La cabera sindicación d\'esti sitiu fízose',
	'deuxjours' => 'Dos díes',

	// L
	'liens' => 'artículu', # MODIF
	'liens_pluriel' => 'artículos', # MODIF

	// M
	'masquer_resume' => 'Esconder los resumes',
	'masquer_sources' => 'Esconder esti orixe',
	'mois' => 'Mes',

	// P
	'pas_articles' => 'Ensin artículos nesti periodu',
	'pas_synchro' => 'Non sincronizar',
	'preferences' => 'Preferencies',
	'probleme_de_syndication' => 'problema cola sindicación',

	// S
	'semaine' => 'Selmana',
	'sources' => 'Fontes',
	'synchro' => 'Sincronizar',
	'synchro_titre' => 'Grabar nel sitiu la llista d\'artículos lleíos',
	'syndication_ajour' => 'Actualizar agora',
	'syndication_fait' => 'Fecha la sindicación',

	// T
	'toutes' => 'Toes'
);

?>
