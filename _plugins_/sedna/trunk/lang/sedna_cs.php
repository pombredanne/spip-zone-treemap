<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/sedna?lang_cible=cs
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Zobrazovat souhrny',
	'afficher_sources' => 'Zobrazit tento zdroj',
	'annee' => 'Uplynulý rok',
	'articles_recents_court' => 'Příspěvky z poslední doby',

	// C
	'connexion' => 'Uživatelské jméno',

	// D
	'deconnexion' => 'Odhlášení',
	'derniere_syndication' => 'Datum posledního zpracování dat na těchto internetových stránkách',
	'deuxjours' => 'Dva dny',

	// L
	'liens' => 'článek', # MODIF
	'liens_pluriel' => 'články', # MODIF

	// M
	'masquer_resume' => 'Skrýt souhrny',
	'masquer_sources' => 'Skrýt tento zdroj',
	'mois' => 'Měsíc',

	// P
	'pas_articles' => 'V zadaném období nejsou žádné články!',
	'pas_synchro' => 'Nesynchronizovat',
	'preferences' => 'Nastavení',
	'probleme_de_syndication' => 'Problém se zpracováním dat',

	// S
	'semaine' => 'Týden',
	'sources' => 'Zdroje',
	'synchro' => 'Synchronizovat',
	'synchro_titre' => 'Uložit seznam přečtených příspěvků na webu',
	'syndication_ajour' => 'Aktualizovat',
	'syndication_fait' => 'Zpracování dat provedeno',

	// T
	'toutes' => 'Všechny'
);

?>
