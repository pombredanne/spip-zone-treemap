<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/sedna?lang_cible=oc_ni
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Afichar lu resumits',
	'afficher_sources' => 'Afichar aquela foant',
	'annee' => 'An',
	'articles_recents_court' => 'Articles recents',

	// C
	'connexion' => 'Connexion',

	// D
	'deconnexion' => 'Desconnexion',
	'derniere_syndication' => 'La darriera sindicacion facha d\'aqueu sit',
	'deuxjours' => 'Doi jorns',

	// L
	'liens' => 'article', # MODIF
	'liens_pluriel' => 'articles', # MODIF

	// M
	'masquer_resume' => 'Mascar lu resumits',
	'masquer_sources' => 'Mascar aquela foant',
	'mois' => 'Mes',

	// P
	'pas_articles' => 'Li a minga d\'article dins aquest periòde !',
	'pas_synchro' => 'Non sincronisar ',
	'preferences' => 'Preferénças',
	'probleme_de_syndication' => 'Problèma de sindicacion',

	// S
	'semaine' => 'Setmana',
	'sources' => 'Fònts',
	'synchro' => 'Sincronisar',
	'synchro_titre' => 'Registrar dins aqueu sit la tièra dei articles legits',
	'syndication_ajour' => 'Actualisar aüra',
	'syndication_fait' => 'La sincronisacion s\'es facha',

	// T
	'toutes' => 'Toti'
);

?>
