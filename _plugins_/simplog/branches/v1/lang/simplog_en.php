<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/simplog?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'actualiser' => 'Refresh',
	'autoriser_non' => 'You are not allowed.',

	// C
	'clic_rafraichir' => 'Click to refresh',
	'clic_senstri' => 'Click to reverse the sort of data',
	'consultation_logs' => 'Viewing log files',

	// D
	'description_simplog' => 'This plugin allows to simply view the log files in the SPIP <i>tmp/</i> directory.',

	// F
	'fichier' => 'File',
	'fichier_inconnu' => 'Unknown file',

	// L
	'liste_fic' => 'Files list',
	'logs' => 'Viewing logs',

	// P
	'plugin_simplog' => 'Simplog plugin',

	// R
	'rep_tmp' => 'Your folder',

	// Z
	'z' => 'z'
);

?>
