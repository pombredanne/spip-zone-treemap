<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/simplog?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'actualiser' => 'Aktualizovať',
	'autoriser_non' => 'Nemáte dostatočné povolenia.',

	// C
	'clic_rafraichir' => 'Kliknite na obnovenie',
	'clic_senstri' => 'Kliknite na výmenu poradia zobrazenia dát',
	'consultation_logs' => 'Čítanie súborov protokolov',

	// D
	'description_simplog' => 'Tento zásuvný modul vám jednoducho umožňuje zobraziť :) obsah súborov protokolov v priečinku <i>tmp</i> SPIPu.',

	// F
	'fichier' => 'Súbor',
	'fichier_inconnu' => 'Súbor neznámy',

	// L
	'liste_fic' => 'Zoznam súborov',
	'logs' => 'Čítanie denníkov',

	// P
	'plugin_simplog' => 'Zásuvný modul Simplog',

	// R
	'rep_tmp' => 'Váš priečinok',

	// Z
	'z' => 'z'
);

?>
