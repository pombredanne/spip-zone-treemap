<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/simplog/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'actualiser' => 'Actualiser',
	'autoriser_non' => 'Vous n\'êtes pas autorisé.',

	// C
	'clic_rafraichir' => 'Cliquer pour rafraîchir',
	'clic_senstri' => 'Cliquer pour inverser l\'ordre d\'affichage des données',
	'consultation_logs' => 'Consultation des fichiers de traces',

	// D
	'description_simplog' => 'Ce plugin permet de visualiser simplement :) les fichiers de traces contenus dans le répertoire <i>tmp</i> de Spip.',

	// F
	'fichier' => 'Fichier',
	'fichier_inconnu' => 'Fichier inconnu',

	// L
	'liste_fic' => 'Liste des fichiers',
	'logs' => 'Consultation des journaux',

	// P
	'plugin_simplog' => 'Plugin Simplog',

	// R
	'rep_tmp' => 'Votre Répertoire',

	// Z
	'z' => 'z'
);

?>
