<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-refbase
// Langue: en
// Date: 15-11-2011 17:19:31
// Items: 1

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// R
	'refbase_description' => 'This plugin allows to import bibliographic references from a [refbase->http://www.refbase.net] database and to display them 
in SPIP with a model called <code><refbase></code>.',
	'refbase_slogan' => 'Import refbase bibliographic references',
);
?>