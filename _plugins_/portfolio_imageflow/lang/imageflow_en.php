<?php

// lang/imageflow_fr.php

// $LastChangedRevision$
// $LastChangedBy$
// $LastChangedDate$

$GLOBALS['i18n_imageflow_en'] = array(

	'reset' => "Reset"

	, 'error_php_old' => "This version of PHP is not fully supported. You need 4.3.2 or above."
	, 'error_gd_missing' => "You are missing the GD extension for PHP, sorry but I cannot continue."
	, 'error_gd_not_png' => "This version of the GD extension cannot output PNG images."
	, 'error_gd_old' => "GD library is too old. Version 2.0.1 or later is required, and 2.0.28 is strongly recommended."

);

?>