<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'azioni' => "Action",
'campo_inesistente' => 'Champ @campo@ inexistant',
'configura_campi' => 'Configurer un champ d\'indexation de la table @tabella@',
'definisci_campi' => 'd&eacute;finir les champs',
'elimina_indice' => 'Supprimer l\'indexation',
'err' => 'Erreur',
'err_definizione_campi' => 'Erreur de d&eacute;finition des champs',
'err_nessuna_tabella' => 'Erreur: table non d&eacute;finie.',
'err_tabella_indicizzata' => 'La table @tabella@ est deja index&eacute;e',
'err_tabella_non_indicizzata' => 'La table @tabella@ n\'est pas indexee',
'err_tabella_non_trovata' => 'Table @tabella@ non trouv&eacute;e',
'filtri' => 'filtre',
'importanza' => 'Poids',
'indicizza' => 'Indexation',
'indicizzazione_tabelle' => 'Indexation de la table',
'info_configurazione' => '<p>Cette page permet de configurer les param&egrave;tres d\'indexation des champs</p>',
'info_indicizzazione' => '<p>Cette page permet de s&eacute;lectionner les tables &agrave; indexer</p>
			<h3>Legende du statut des tables</h3>
	     <ul style=\'margin:0;padding:10px;\'>
	     <li><strong>Table non d&eacute;finie dans SPIP</strong>: la table n\'est pas utilisable dans une boucle et donc il est inutile de l\'indexer</li>
	     <li><strong>Table exclue de l\'indexation</strong>: la table n\'est pas indexable</li>
	     <li><strong>Table non index&eacute;e</strong>: la table n\'est pas index&eacute;e mais il est possible de l\'indexer</li>
	     <li><strong>Aucun champ index&eacute;</strong>: la table est index&eacute;e mais aucun champ n\'est configur&eacute; pour l\'indexation</li>
	     <li><strong>Table index&eacute;e</strong>: la table est index&eacute;e</li></ul>
	     <h3>Legende des Actions</h3>
	     <ul style=\'margin:0;padding:10px;\'><li><strong>Indexer</strong>: Indexation de la table</li>
	     <li><strong>Modification du champ</strong>: d&eacute;finir les champs &agrave; indexer dans la table</li>
	     <li><strong>Supprimer l\'indexation</strong>: Ne plus indexer cette table et &eacute;liminer les donn&eacute;es d\'indexation</li></ul>',
'min_lungh' => "Long. mini",
'modifica_campi' => 'Modifier les param&egrave;tres',
'nessun_campo_indicizzato' => 'aucun champ index&eacute;',
'nome_tabella' => 'Nom de la table',
'nome_campo' => 'Nom du champ',
'stato_tabella' => 'Etat de la table',
'tabelle' => 'Table',
'tabella_esclusa' => 'table exclue de l\'indexation',
'tabella_non_definita' => 'table non definie dans SPIP',
'tabella_non_indicizzata' => 'table non indexable',
'tabella_indicizzata' => 'table index&eacute;e',
);

?>
