<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'azioni' => "Azioni",
'campo_inesistente' => 'Campo @campo@ inesistente',
'configura_campi' => 'Configura i campi da indicizzare della tabella @tabella@',
'definisci_campi' => 'definisci campi',
'elimina_indice' => 'elimina indice',
'err' => 'Errore',
'err_definizione_campi' => 'Errore nella definizione dei campi',
'err_nessuna_tabella' => 'Errore: tabella non specificata.',
'err_tabella_indicizzata' => 'La tabella @tabella@ &egrave; gi&agrave; indicizzata',
'err_tabella_non_indicizzata' => 'La tabella @tabella@ non &egrave; indicizzata',
'err_tabella_non_trovata' => 'Tabella @tabella@ non trovata',
'filtri' => 'filtri',
'importanza' => 'Importanza',
'indicizza' => 'indicizza',
'indicizzazione_tabelle' => 'Indicizzazione tabelle',
'info_configurazione' => '<p>Questa pagina permette di configurare i parametri di indicizzazione dei campi</p>',
'info_indicizzazione' => '<p>Questa pagina permette di selezionare le tabelle da indicizzare.</p>
			<h3>Legenda Stato Tabella</h3>
	     <ul style=\'margin:0;padding:10px;\'>
	     <li><strong>Tabella non definita in SPIP</strong>: la tabella non &egrave; utilizzabile in un ciclo e quindi &egrave; inutile indicizzarla</li>
	     <li><strong>Tabella esclusa dall\'indicizzazione</strong>: la tabella non &egrave; indicizzabile</li>
	     <li><strong>Tabella non indicizzata</strong>: la tabella non &egrave; indicizzata ma &egrave; possibile indicizzarla</li>
	     <li><strong>Nessun campo indicizzato</strong>: la tabella &egrave; indicizzata ma non &egrave; configurato alcun campo per l\'indicizzazione</li>
	     <li><strong>Tabella indicizzata</strong>: la tabella &egrave; indicizzata regolarmente</li></ul>
	     <h3>Legenda Azioni</h3>
	     <ul style=\'margin:0;padding:10px;\'><li><strong>Indicizza</strong>: Indicizza la tabella</li>
	     <li><strong>Modifica campi</strong>: definisce i campi da incidizzare per la tabella</li>
	     <li><strong>Elimina indice</strong>: Non indicizza la tabella e ne elimina i dati di indicizzazione</li></ul>',
'min_lungh' => "Lungh. minima",
'modifica_campi' => 'modifica campi',
'nessun_campo_indicizzato' => 'nessun campo indicizzato',
'nome_tabella' => 'Nome tabella',
'nome_campo' => 'Nome campo',
'stato_tabella' => 'Stato tabella',
'tabelle' => 'Tabelle',
'tabella_esclusa' => 'tabella esclusa dall\'indicizzazione',
'tabella_non_definita' => 'tabella non definita in spip',
'tabella_non_indicizzata' => 'tabella non indicizzata',
'tabella_indicizzata' => 'tabella indicizzata',
);

?>
