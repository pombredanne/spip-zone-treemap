<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/flickrcc?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_image_flickr' => 'Afegir una imatge Flickr',

	// C
	'cc_commerciale' => 'CC - Ús comercial possible',
	'cc_modification' => 'CC - Modificació possible',
	'cc_pas_de_restriction' => 'No es coneixen restriccions',
	'cc_paternite' => 'Creative Commons - Paternitat',

	// F
	'fermer' => 'Tancar',
	'flickr_cc_explication' => '<p>Busqueu imatges Creative Commons a Flickr indicant els termes de la cerca que hi ha més amunt. Eventualment, preciseu el tipus de llicència a les que estan sotmeses les imatges.</p>
<p>Feu un clic al damunt de la (o les) imatge(s) escollida(es) entre els resultats de la cerca: immediatament s\'associa al vostre article. Feu un clic al damunt del nom de l\'autor per accedir a la pàgina Flickr d\'aquesta imatge.</p>
<p>Aquestes imatges es posen a disposició pels seus mateixos autors: per respectar els seus drets i el seu treball, </p>
<ul><li>no suprimiu la menció de l\'autor i l\'enllaç cap a la seva pàgina a Flickr ;</li>
<li>respecteu escrupolosament la llicència Creative Commons associada a cada imatge (en concret, no utilitzeu una imatge en un marc comercial si la llicència no ho autoritza;</li>
<li>si l\'autor de la imatge us ho demana, retireu-la del vostre lloc Web en un termini de 24 hores.</li>
</ul>
<p>Aquest producte utilitza l\'API de Flickr però no està ni sostingut ni certificat per Flickr.</p>',

	// R
	'rechercher_flickr' => 'Buscar a Flickr',
	'resultat_par_date' => 'per data',
	'resultat_par_interet' => 'per interès',
	'resultat_par_pertinence' => 'per pertinença'
);

?>
