<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/flickrcc?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_image_flickr' => 'Añadir una imagen de Flickr',

	// C
	'cc_commerciale' => 'CC - Uso comercial posible',
	'cc_modification' => 'CC - Modificación posible',
	'cc_pas_de_restriction' => 'Sin restricciones conocidas',
	'cc_paternite' => 'Creative Commons - Reconocimiento',

	// F
	'fermer' => 'Cerrar',
	'flickr_cc_explication' => '<p>Buscar imágenes con licencia Creative Commons en Flickr indicando los términos de búsqueda más arriba. Indica si es el caso la naturaleza de las licencias a las que se acogen las imágenes.</p>
<p>Pulsa en la (o las) imagen(es) elegida(s) entre los resultados de la búsqueda: la imagen se asocia inmediatamente con el artículo. Pulsa en el nombre del autor para acceder a la página en Flickr de esta imagen.</p>
<p>Estas imágenes se facilitan por sus respectivos autores: para respetar sus derechos y su trabajo,</p>
<ul><li>no suprimas la mención del autor y el enlace a su página en Flickr ;</li>
<li>respeta rigurosamente la licencia Creative Commons asociada con cada imagen (en particular, no uses una imagen en un sitio comercial si la licencia no lo autoriza;</li>
<li>si el autor de la imagen te lo pide, debes retirar la imagen de tu sitio en un plazo de 24 horas.</li>
</ul>
<p>Este producto utiliza el API Flickr pero no está ni soportado, ni certificado por Flickr.</p>',

	// R
	'rechercher_flickr' => 'Buscar en Flickr',
	'resultat_par_date' => 'por fecha',
	'resultat_par_interet' => 'por interés',
	'resultat_par_pertinence' => 'por pertinencia'
);

?>
