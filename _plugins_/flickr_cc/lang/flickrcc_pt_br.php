<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/flickrcc?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_image_flickr' => 'Incluir uma imagem Flickr',

	// C
	'cc_commerciale' => 'CC - Uso comercial autorizado',
	'cc_modification' => 'CC - Modificação autorizada',
	'cc_pas_de_restriction' => 'Sem restrições conhecidas',
	'cc_paternite' => 'Creative Commons - Paternidade',

	// F
	'fermer' => 'Fechar',
	'flickr_cc_explication' => '<p>Pesquise imagens sob o Creative Commons no Flickr indicando os termos da pesquisa abaixo. Especifique eventualmente os tipos de licenças a que estão submetidas as imagens.</p>
<p>Clique na(s) imagens(s) escolhida(s) entre os resultados da pesquisa: ela será imediatamente associada à sua matéria. Clique sobre o nome do autor para acessar a página dessa imagem no Flickr.</p>
<p>Essas imagens são disponibilizadas pelos seus respectivos autores: para respeitar os seus direitos e os seus trabalhos,</p>
<ul><li>não exclua a menção do autor e o link para a sua página no Flickr;</li>
<li>respeite rigorosamente a licença Creative Commons associada a cada imagem (especialmente, não use em um contexto comercial uma imagem cuja licença não o permita);</li>
<li>se o autor da imagem lhe solicitar, você deverá excluir a imagem do seu site, no espaço de 24 horas.</li>
</ul>
<p>Este produto usa a API Flickr ms não é mantido nem certificado pelo Flickr.</p>',

	// R
	'rechercher_flickr' => 'Pesquisar no Flickr',
	'resultat_par_date' => 'por data',
	'resultat_par_interet' => 'por interesse',
	'resultat_par_pertinence' => 'por pertinência'
);

?>
