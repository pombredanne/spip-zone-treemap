<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/flickrcc?lang_cible=ro
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_image_flickr' => 'Adăugaţi o imagine Flickr',

	// C
	'cc_commerciale' => 'CC - Utilizare comercială posibilă',
	'cc_modification' => 'CC - Modificare posibilă',
	'cc_pas_de_restriction' => 'Fără restricţii cunoscute',
	'cc_paternite' => 'Creative Commons - Paternitate',

	// F
	'fermer' => 'Închideţi',
	'flickr_cc_explication' => '<p>Căutaţi imagini Creative Commons pe Flickr indicând termenii căutarii. Precizaţi eventual şi natura licenţelor la care sunt supuse imaginile.</p>
<p>Faceţi clic pe imaginile alese din rezultatele căutării : vor fi imediat asociate articolului dvs. Faceţi clic pe numele autorului pentru a acceda la pagina Flickr a acestei imagini.</p>
<p>Aceste imagini sunt puse la dispoziţie de către autorii lor respectivi : pentru a respecta drepturile lor şi munca lor,</p>
<ul><li>nu suprimaţi menţiunea autorului şi legătura către pagina Flickr ;</li>
<li>respectaţi riguros licenţa Creative Commons asociată fiecărie imagini (în mod particular, nu utilizaţi o imagine într-un cadru comercial dacă licenţa nu autorizează ;</li>
<li>dacă autorul unei imagini va cere acest lucru, veţi retrage imaginea de pe site-ul dvs. într-un termen de 24 de ore.</li>
</ul>
<p>Acest produs utilizează API_ul Flickr, dar nu este nici susţinut nici certificat de către Flickr.</p>',

	// R
	'rechercher_flickr' => 'Căutaţi pe Flickr',
	'resultat_par_date' => 'după dată',
	'resultat_par_interet' => 'după interes',
	'resultat_par_pertinence' => 'după relevanţă'
);

?>
