<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/flickrcc?lang_cible=gl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_image_flickr' => 'Engadir unha imaxe de Flickr',

	// C
	'cc_commerciale' => 'CC - Uso comercial aceptado',
	'cc_modification' => 'CC - Modificación aceptada',
	'cc_pas_de_restriction' => 'Non se declara ningunha restrición',
	'cc_paternite' => 'Creative Commons - Autoría',

	// F
	'fermer' => 'Pechar',
	'flickr_cc_explication' => '<p>Procurar imaxes con licenza Creative Commons sobre Flickr indicando os termos da procura seguintes. Pode precisar a natureza das licenzas ás que están sometidas as imaxes.</p>
<p>Prema sobre a (ou as) imaxes(s) escollida(s) entre os resultados da procura : está inmediatamente asociada ao seu artigo. Prema sobre o nome do autor para acceder á páxina de Flickr correspondente a esta imaxe.</p>
<p>Estas imaxes ofrecidas polos seus autores respectivos : para respectar os seus dereitos de autor e o seu traballo,</p>
<ul><li>non suprima a mención do autor nin a ligazón sobre a súa páxina de Flick ;</li>
<li>respecte rigorosamente a licenza Creative Commons asociada á cada imaxe (en particular, non empregue unha imaxe nun ámbito comercial se a licenza non o autoriza ;</li>
<li>se o autor da imaxe llo demanda, debe retirar a imaxe do seu web nun prazo de 24 horas.</li>
</ul>
<p>Este produto usa a API de Flickr mais non esá sustentado, nin certificado por Flickr.</p>',

	// R
	'rechercher_flickr' => 'Procurar sobre Flickr',
	'resultat_par_date' => 'por data',
	'resultat_par_interet' => 'por interese',
	'resultat_par_pertinence' => 'por pertinencia'
);

?>
