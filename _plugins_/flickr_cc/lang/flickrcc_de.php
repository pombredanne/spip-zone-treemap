<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/flickrcc?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_image_flickr' => 'Ein Bild aus Flickr hinzufügen',

	// C
	'cc_commerciale' => 'CC - Gewerbliche Verwendung gestattet',
	'cc_modification' => 'CC - Bearbeitung gestattet',
	'cc_pas_de_restriction' => 'Keine bekannten Beschränkungen',
	'cc_paternite' => 'Creative Commons - Autorenangaben erforderlich',

	// F
	'fermer' => 'Schließen',
	'flickr_cc_explication' => '<p>Suchen Sie auf Flickr nach Bilder unter der <i>Creative Commons Lizenz</i>, indem Sie unten Ihre Suchbegriffe eingeben. Geben Sie falls erforderlich den gewünschten Lizenztyp an.</p><p>Klicken Sie auf das Bild oder die Bilder der Fundstellenliste. Sie werden Ihrem Artikel sofort zugeordnet. Klicken Sie auf den Autorennamen, um auf die Flickr Seite zu gelangen.</p><p>Diese Bilder werden von den Urhebern kostenlos bereitgestellt. Bitte respektieren Sie ihre Rechnte und</p><ul><li>entfernen keine Angaben zum Urheber und seiner Seite auf Flickr.</li><li>halten Sie sich bitte an die Lizenzbedingungen (unterlassen Sie es insbesondere, Bilder kommerziell zu verwenden, für die es nicht gestattet ist.)</li><li>falls ein Urheber es verlangt, müs&uuml;sen Sie seine Bilder unverzüglich aus ihrer Website entfernen</li></ul><p>Dieses Plugin verwendet die Flickr API, wird von Flickr jedoch nicht supportet.</p>',

	// R
	'rechercher_flickr' => 'Auf Flickr suchen',
	'resultat_par_date' => 'Nach Datum',
	'resultat_par_interet' => 'Nach Interesse',
	'resultat_par_pertinence' => 'Nach Relevanz'
);

?>
