<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_image_flickr' => 'Ajouter une image Flickr', # NEW

	// C
	'cc_commerciale' => 'CC - Utilisation commerciale possible', # NEW
	'cc_modification' => 'CC - Modification possible', # NEW
	'cc_pas_de_restriction' => 'Pas de restrictions connues', # NEW
	'cc_paternite' => 'Creative Commons - Paternité', # NEW

	// F
	'fermer' => 'Stäng',
	'flickr_cc_explication' => '<p>Recherchez des images en Creative Commons sur Flickr en indiquant les termes de la recherche ci-dessus. Précisez éventuellement les natures de licences auxquelles sont soumises les images.</p>
<p>Cliquez sur la (ou les) images(s) choisie(s) parmi les résultats de la recherche : elle est immédiatement associée à votre article. Cliquez sur le nom de l\'auteur pour accéder à la page Flickr de cette image.</p>
<p>Ces images sont mises à disposition par leurs auteurs respectifs : pour respecter leurs droits et leur travail,</p>
<ul><li>ne supprimez pas la mention de l\'auteur et le lien vers leur page Flickr ;</li>
<li>respectez rigoureusement la licence Creative Commons associée à chaque image (en particulier, n\'utilisez pas une image dans un cadre commercial si la licence de l\'autorise pas ;</li>
<li>si l\'auteur de l\'image vous le demande, vous devez retirer l\'image de votre site dans un délai de 24 heures.</li>
</ul>
<p>Ce produit utilise l’API Flickr mais n’est ni soutenu, ni certifié par Flickr.</p>', # NEW

	// R
	'rechercher_flickr' => 'Rechercher sur Flickr', # NEW
	'resultat_par_date' => 'par date', # NEW
	'resultat_par_interet' => 'par intérêt', # NEW
	'resultat_par_pertinence' => 'par pertinence' # NEW
);

?>
