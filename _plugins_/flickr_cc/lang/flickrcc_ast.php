<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/flickrcc?lang_cible=ast
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_image_flickr' => 'Amestar una imaxe de Flickr',

	// C
	'cc_commerciale' => 'CC - Usu comercial posible',
	'cc_modification' => 'CC - Modificación posible',
	'cc_pas_de_restriction' => 'Ensin torgues conocíes',
	'cc_paternite' => 'Creative Commons - Reconocimientu d\'autor',

	// F
	'fermer' => 'Zarrar',
	'flickr_cc_explication' => '<p>Restola imaxes de Creative Commons en Flickr enxertando enriba los términos a guetar. Precisa, si ye\'l casu, los tipos de llicencia embaxo los que tan les imáxenes.</p>
<p>Calca na (o nes) imaxe(s) escoyía(es) ente los resultaos de la gueta: la imaxe améstase de secute col to artículu. Calca enriba\'l nome de l\'autor pa dir a la páxina en Flickr d\'esa imaxen.</p>
<p>Estes imaxes úfrense polos sos respeutivos autores: col envís de respetar los sos derechos y el so trabayu,</p>
<ul><li>nun desanicies la mención de l\'autor y l\'enllace a la so páxina en Flickr ;</li>
<li>respeta rigurosamente la llicencia Creative Commons asociada con cada imaxe (en particular, nun utilizar una imaxe nun proyeutu comercial si la llicencia nun lo permite;</li>
<li>si l\'autor de la imaxe te lo pidiere, debes desaniciar la imaxe del to sitiu nun plazu de 24 hores.</li>
</ul>
<p>Esti productu utiliza l’API Flickr pero nun ta nin sofitáu, nin certificáu por Flickr.</p>',

	// R
	'rechercher_flickr' => 'Restolar en Flickr',
	'resultat_par_date' => 'por fecha',
	'resultat_par_interet' => 'pol interés',
	'resultat_par_pertinence' => 'polo atináo'
);

?>
