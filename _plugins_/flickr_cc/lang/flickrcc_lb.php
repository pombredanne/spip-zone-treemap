<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/flickrcc?lang_cible=lb
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_image_flickr' => 'E Flickr-Bild derbäi setzen',

	// C
	'cc_commerciale' => 'CC - Kommerziellt Benotzen erlaabt',
	'cc_modification' => 'CC - Ännerung erlaabt',
	'cc_pas_de_restriction' => 'Kéng bekannte Restriktiounen',
	'cc_paternite' => 'Creative Commons - Credit',

	// F
	'fermer' => 'Zoumaachen',
	'flickr_cc_explication' => '<p>Sicht Creative Commons Biller op Flickr andeem dir hei eng Sich agidd. Präziséiert och wéi eng Lizenz d\'Biller sollen hun.</p>
<p>Klickt op d\'Bild oder op d\'Biller vum Resultat: si ginn direkt un ären Artikel ugebonnen. Klickt op de Numm vum Auteur fir op d\'Flickr-Säit vun dësem Bild ze kommen.</p>
<p>Dës Biller ginn vun den Auteuren zur Verfügung gestallt: fir hier Rechter an hier Aarbecht ze respektéieren,</p>
<ul><li>läscht d\'Mentioun vum Auteur an de Link op d\'Flickr-Säit nët;</li>
<li>respektéiert genee d\'Creative Commons Lizenz vun all Bild;</li>
<li>wann den Auteur dat verlaangt musst dir innerhalb vu 24 Stonnen d\'Bild vun ärem Site läschen.</li>
</ul>
<p>Dëst Produkt benotzt d\'API vu Flickr mais ass awer nët vu Flickr ënnerstëtzt oder zertifiéiert.</p>',

	// R
	'rechercher_flickr' => 'Op Flickr sichen',
	'resultat_par_date' => 'no Datum',
	'resultat_par_interet' => 'no Intérêt',
	'resultat_par_pertinence' => 'no Warscheinlechkeet'
);

?>
