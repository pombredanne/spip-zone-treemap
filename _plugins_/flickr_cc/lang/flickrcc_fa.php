<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/flickrcc?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_image_flickr' => 'افزودن تصوير از فليكر(Flicker)',

	// C
	'cc_commerciale' => 'سي.سي (cc): استفاده‌ي تجاري مجاز',
	'cc_modification' => 'سي‌سي (‍‍CC)- دستكاري مجاز',
	'cc_pas_de_restriction' => 'محدوديت‌هاي كپي‌رايت ناشناخته',
	'cc_paternite' => 'مشاعات خلاق - پروانه‌ي انتساب',

	// F
	'fermer' => 'بستن',
	'flickr_cc_explication' => 'جستجو براي تصاوير منتشر شده تحت پروانه‌هاي مشاعات خلاق در فليكر. كليد‌واژه‌(ها)ي خود را وارد كنيد و نوع پروانه‌اي را كه مي‌خواهيد استفاده كنيد مشخص سازيد.</p>
<p>روي تصويري كه مي‌خواهيد در متن خود بگنجانيد كليك كنيد: به طور خودكار بارگذاري و به سند شما وصل خواهد شد. روي نام مؤلف كليك كنيد تا در فليكر به صفحه‌ي اصلي برسيد.</p>
<p>اين تصاوير در فليكر با مؤلفان خودشان منتشر مي‌شوند: لطفاً به كار و حق مؤلف احترام بگذاريد.,</p>
<ul><li>نام مؤلف و پيوند به صفحه‌ي آنان در فليكر را حذف نكنيد ;</li>
<li>به پروانه‌ي مشاعات خلاق هر يك از تصاوير احترام بگذاريد (به عنوان مثال، اگر پروانه اجازه نمي‌دهد از عكس‌ها استفاده‌ي تجاري نبريد) ;</li>
<li>اگر مؤلف خواهان حذف تصويرش از وب سايت شما شود، طي 24 ساعت بايد اطاعت كنيد.</li>
<p>اين ابزار از اي‌پي‌آي فليكر استفاده مي‌كند اما مورد تأييد و حمايت فليكر نيست.</p>', # MODIF

	// R
	'rechercher_flickr' => 'جستجو در فليكر',
	'resultat_par_date' => 'طبق تاريخ',
	'resultat_par_interet' => 'طبق علاقه',
	'resultat_par_pertinence' => 'طبق تناسب '
);

?>
