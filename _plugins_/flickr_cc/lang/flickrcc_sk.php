<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/flickrcc?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_image_flickr' => 'Pridať obrázok z Flickra',

	// C
	'cc_commerciale' => 'CC – možné používanie na komerčné účely',
	'cc_modification' => 'CC – sú možné zmeny',
	'cc_pas_de_restriction' => 'Žiadne známe obmedzenia',
	'cc_paternite' => 'Creative Commons – Attribution License',

	// F
	'fermer' => 'Zatvoriť',
	'flickr_cc_explication' => '<p>Vyhľadávajte na Flickri obrázky publikované na základe licencií Creative Commons. Zadajte svoje vlastné kľúčové slovo (-á) a uveďte typ licencií, ktorý chcete použiť.</p>
<p>Kliknite na obrázok, ktorý chcete vložiť do svojho článku: bude automaticky stiahnutý a pripojený k vášmu dokumentu. Ak sa chcete dostať na pôvodnú stránku na Flickri, kliknite na meno autora.</p>
<p>Tieto obrázky na Flicri publikovali ich autori: prosím, rešpektujte ich prácu a autorské práva:</p>
<ul><li>neodstraňujte meno autora a odkaz na jeho stránku na Flickri,</li>
<li>rešpektujte podmienky licencie Creative Commons pri každom obrázku (napr. nevyužívajte obrázok na komerčné účely, ak to licencia nepovoľuje),</li>
<li>ak autor požiada o odstránenie svojho obrázka z vašej stránky, musíte mu vyhovieť do 24 hodín.</li>
</ul>
<p>Tento nástroj využíva Flickr API, ale Flickr ho neschválil ani nepovolil.</p>',

	// R
	'rechercher_flickr' => 'Vyhľadať na Flickri',
	'resultat_par_date' => 'podľa dátumu',
	'resultat_par_interet' => 'podľa záujmu',
	'resultat_par_pertinence' => 'podľa dôležitosti'
);

?>
