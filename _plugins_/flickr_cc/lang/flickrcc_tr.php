<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/flickrcc?lang_cible=tr
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_image_flickr' => 'Bir Flickr resmi ekle',

	// C
	'cc_commerciale' => 'CC - Ticarî kullanım mümkün',
	'cc_modification' => 'CC - Düzeltme mümkün',
	'cc_pas_de_restriction' => 'Bilinen bir kısıtlama yok',
	'cc_paternite' => 'Creative Commons - Aidiyet',

	// F
	'fermer' => 'Kapat',
	'flickr_cc_explication' => 'Flickr\'da, yukarıdaki arama terimlerini belirterek Creative Commons resimleri arayın. Ayrıca resimlerin lisans tiplerini de belirtin. </p>
<p>Arama sonucundaki resme veya resimlere tıklayın : resim hemen makalenize bağlanacaktır. Bu resme ait sayfaya erişmek için yazar ismine tıklayın. </p>
<p>Bu resimler sırayla belirtilen yazarlara aittir ve kullanımınıza sunulmuştur : haklarına ve çalışmalarına saygı göstermek için ,</p>
<ul><li>yazarın ismine ve Flickr sayfasına yönlendiren bağlantıyı silmeyin ;</li>
<li>Creative Commons lisansına harfiyen uyun (özllikle, eğer lisans buna izin vermiyorsa resmi ticarî bir alanda kullanmayın ;</li>
<li>eğer yazar talep ederse resmi sitenizden 24 saat içinde silmelisiniz.</li>
</ul>
<p>Bu rüürn Flickr API\'sini kullanır ama ne destek verir ne de Flickr tarafından tanınmıştır.</p>',

	// R
	'rechercher_flickr' => 'Flickr\'da ara',
	'resultat_par_date' => 'tarihe göre',
	'resultat_par_interet' => 'ilgiye göre',
	'resultat_par_pertinence' => 'tutarlılığa göre'
);

?>
