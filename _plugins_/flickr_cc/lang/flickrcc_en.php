<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/flickrcc?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_image_flickr' => 'Add a picture from Flickr',

	// C
	'cc_commerciale' => 'CC - Allowed commercial use',
	'cc_modification' => 'CC - Modification allowed',
	'cc_pas_de_restriction' => 'No known copyright restrictions',
	'cc_paternite' => 'Creative Commons - Attribution License',

	// F
	'fermer' => 'Close',
	'flickr_cc_explication' => '<p>Search for pictures published under Creative Commons licences on Flickr. Insert your own keyword(s) and specify the type of licences you want to use.</p>
<p>Click on the picture(s) you wish to insert in your article: each will be automatically downloaded and joined to your document. Click on the authors name to view the original page on Flickr.</p>
<p>These pictures are published on Flickr by their own authors: please respect their work and copyright:</p>
<ul><li>don\'t remove the name of the author nor the link to their Flickr page;</li>
<li>respect the terms of the Creative Commons licence of each picture (for instance, don\'t use the picture for commercial use if the licence doesn\'t allow it);</li>
<li>if the author asks for the removal of a picture from your website, you have to comply within 24 hours.</li>
</ul>
<p>This tool uses the Flickr API but is neither endorsed nor certified by Flickr.</p>',

	// R
	'rechercher_flickr' => 'Search on Flickr',
	'resultat_par_date' => 'by date',
	'resultat_par_interet' => 'by topicality',
	'resultat_par_pertinence' => 'by relevance'
);

?>
