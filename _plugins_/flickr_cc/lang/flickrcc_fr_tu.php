<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/flickrcc?lang_cible=fr_tu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_image_flickr' => 'Ajouter une image Flickr',

	// C
	'cc_commerciale' => 'CC - Utilisation commerciale possible',
	'cc_modification' => 'CC - Modification possible',
	'cc_pas_de_restriction' => 'Pas de restrictions connues',
	'cc_paternite' => 'Creative Commons - Paternité',

	// F
	'fermer' => 'Fermer',
	'flickr_cc_explication' => '<p>Recherche des images en Creative Commons sur Flickr en indiquant les termes de la recherche ci-dessus. Précise éventuellement les natures de licences auxquelles sont soumises les images.</p>
<p>Clique sur la (ou les) images(s) choisie(s) parmi les résultats de la recherche : elle est immédiatement associée à ton article. Clique sur le nom de l\'auteur pour accéder à la page Flickr de cette image.</p>
<p>Ces images sont mises à disposition par leurs auteurs respectifs : pour respecter leurs droits et leur travail,</p>
<ul><li>ne supprime pas la mention de l\'auteur et le lien vers leur page Flickr ;</li>
<li>respecte rigoureusement la licence Creative Commons associée à chaque image (en particulier, n\'utilise pas une image dans un cadre commercial si la licence de l\'autorise pas ;</li>
<li>si l\'auteur de l\'image te le demande, tu dois retirer l\'image de ton site dans un délai de 24 heures.</li>
</ul>
<p>Ce produit utilise l’API Flickr mais n’est ni soutenu, ni certifié par Flickr.</p>',

	// R
	'rechercher_flickr' => 'Rechercher sur Flickr',
	'resultat_par_date' => 'par date',
	'resultat_par_interet' => 'par intérêt',
	'resultat_par_pertinence' => 'par pertinence'
);

?>
