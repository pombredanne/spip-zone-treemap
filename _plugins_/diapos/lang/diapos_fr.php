<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;
$GLOBALS[$GLOBALS['idx_lang']] = array(
	'cfg'                       =>'Configuration de Diapos',
	'cfg_largeur_imagespa'    =>'Largeur',
	'cfg_hauteur_imagespa'             =>'Hauteur',
	'cfg_descriptifpa'            =>'<b>Diapos paysage</b><br /><em>Choisissez ici la taille de vos images qui seront affich&eacute;es par le module paysage. Vous pouvez la renseigner soit en pixels soit en ems (px ou em, sans espace entre le chiffre et la d&eacute;finition).</em>',
	'cfg_marges_imagespati'    =>'Marges portrait',
	'cfg_marges_imagespa'    =>'* <em>Le premier chiffre correspond &agrave; la marge sup&eacute;rieure, puis dans le sens des aiguiles d\'une montre, marge droite, inf&eacute;rieure et gauche. Vous pouvez les renseigner soit en pixels soit en ems. Si vous mettez une seule marge, elle sera valable pour toutes. Si vous en mettez deux, elles fonctionnent comme un miroir : la marge sup&eacute;rieure vaudra aussi pour l\'inf&eacute;rieure et celle de droite vaudra pour celle de gauche. Si vous en mettez trois, vous donnez une nouvelle valeur &agrave; l\'inf&eacute;rieure. Si vous voulez quatre marges diff&eacute;rentes, il vous faut renseigner toutes les marges.</em>',
	'cfg_largeur_imagespo'    =>'Largeur',
	'cfg_hauteur_imagespo'             =>'Hauteur',
	'cfg_descriptifpo'            =>'<b>Diapos portrait</b><br /><em>Choisissez ici la taille de vos images qui seront affich&eacute;es par le module portrait (en pixels ou ems).</em>',
	'cfg_marges_imagespoti'    =>'Marges portrait',
	'cfg_valider'        =>'Valider'
	);  
?>
