<?php

	$GLOBALS[$GLOBALS['idx_lang']] = array(
		'titre'				=>	'Boite &agrave; outils',
		'taille'			=>	'Taille du texte',
		'textemoins'		=>	'diminuer le texte',
		'texteplus'			=>	'augmenter le texte',
		'pdf'				=>	'Format pdf',
		'impression'		=>	'Impression',
		'impression_title'	=>	'version imprimable de l\'article',
		'auteur'			=>	'Du m&ecirc;me auteur',
		'reaction'			=>	'R&eacute;actions',
		'reagir'			=>	'R&eacute;agir &agrave; l\'article',
		'config_titre'		=>	'Boite &agrave; outils pour article',
		'config_descr'		=>	'vous pouvez configurer les &eacute;l&eacute;ments &agrave; afficher dans la boite &agrave; outils',
		'elements'			=>	'El&eacute;ments &agrave; afficher',
		'mail'				=>	'envoyer par mail',
		'compteur'			=>	'nombre de r&eacute;actions',
		'note'				=>	'donner une note &agrave; l\'article avec le plugin notation',
		'rss'				=>	'Syndiquer la rubrique',
		'rssforum'			=>	'Syndiquer le forum de l\'article',
		'descriptif'		=>	'
		<p>Vous pouvez configurer les &eacute;l&eacute;ments &agrave; afficher dans la boite &agrave; outils<br />
Documentation compl&egrave;te sur <a href="http://www.spip-contrib.net/Boite-a-outils-pour-articles-version-plugin">contrib</a><br />

Plugins compl&eacute;mentaires
<ul>
<li><a href="http://www.spip-contrib.net/Plugin-Article-PDF">Article PDF</a></li>

<li><a href="http://www.spip-contrib.net/Plugin-envoyer-a-un-ami">Envoyer &agrave; un ami</a></li>

<li>Recommander &agrave; un ami</a></li>

<li><a href="http://www.spip-contrib.net/Texte-agrandi-ou-pleine-page">Bouton texte</a></li>

<li><a href="http://www.spip-contrib.net/Notation-d-articles-SPIP">Notation</a></li>


</ul></p>'
	);

?>