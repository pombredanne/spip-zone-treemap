<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-outils_article
// Langue: fr
// Date: 18-06-2012 14:15:31
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// O
	'outils_article_description' => 'Insérer une panoplie d\'outils pour vos articles sous forme de barre ou de boite grâce à deux noisettes à insérer dans vos squelettes : 
-* <code>[(#INCLURE{fond=inc-barre-outils}{id_article})]</code> pour la version en barre
-* <code>[(#INCLURE{fond=inc-boite-outils}{id_article})]</code> pour la version en boite',
	'outils_article_nom' => 'Boite à outils',
	'outils_article_slogan' => 'Collection d\'outils pour articles',
);
?>