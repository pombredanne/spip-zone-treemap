<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-outils_article
// Langue: es
// Date: 18-06-2012 14:15:31
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// O
	'outils_article_description' => 'Insertar una panoplia de herramientas para sus artículos en forma de barra o de cojea con dos avellanas que deben insertarse en sus esqueletos : 
-* <code>[(#INCLURE{fond=inc-barre-outils}{id_article})]</code> para la versión barra
-* <code>[(#INCLURE{fond=inc-boite-outils}{id_article})]</code> para la versión cojea',
	'outils_article_nom' => 'Caja de herramientas',
	'outils_article_slogan' => 'Colecciòn de herramientas a los artculos',
);
?>