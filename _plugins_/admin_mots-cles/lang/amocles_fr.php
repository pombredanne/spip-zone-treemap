<?php

	// lang/amocles_fr.php
	
	// $LastChangedRevision$
	// $LastChangedBy$
	// $LastChangedDate$

$GLOBALS['i18n_'._AMOCLES_PREFIX.'_fr'] = array(

'administration_mots_cles' => "Administration des mots-cl&eacute;s"
, 'administrateurs_mots_cles' => "Administrateurs des mots-cl&eacute;s"
, 'deleguer_admin' => "D&eacute;l&eacute;guer l'administration des mots-cl&eacute;s"

, 'options_' => "Options :"
, 'options_texte' => "Vous pouvez ajouter tous les administrateurs (restreints ou non) et/ou tous les auteurs.
		Vous pourrez ensuite en retirer certains en utilisant l'option de la liste ci-dessus."
, 'ajouter_tous_admins' => "Ajouter tous les administrateurs."
, 'ajouter_tous_auteurs' => "Ajouter tous les auteurs."

// boite config afficher milieu

, 'inserer_milieu' => "Boite d'&eacute;dition multilingue"
, 'inserer_milieu_desc' => "Le site emploie plusieurs langues. Vous pouvez ins&eacute;rer la boite d'&eacute;dition 
	des mots-cl&eacute;s permettant de traduire le mot-cl&eacute; langue par langue, 
	avec insertion automatique des &lt;multi&gt;. "
, 'inserer_boite_edit' => "Ins&eacute;rer le boite d'&eacute;dition multilingue."

// boite importation
, 'importer_mots_cles' => "Importer des mots-cl&eacute;s"
, 'info_importer_mots_cles' => "Vous pouvez importer des mots-cl&eacute;s &agrave; partir d'un fichier texte.
<br />Le fichier texte doit contenir un mot-cl&eacute; par ligne.<br />
Chaque ligne doit &ecirc;tre compos&eacute;e ainsi (exemple pour le mot-cl&eacute; <strong>vert</strong>):<br />
<tt style='display:block;margin:0.75em 0;background-color:#ccc;border:1px solid #999;padding:1ex;'>Vert<span style='color:#f66'>[separateur]</span>Descriptif de vert<span style='color:#f66'>[separateur]</span>Texte de vert</tt>
<tt style='color:#f66'>[separateur]</tt> est un caract&egrave;re de tabulation.<br />
<ol>
	<li>S&eacute;lectionnez le groupe des mots-cl&eacute;s dans lequel vous souhaitez importer les mots-cl&eacute;s.</li>
	<li>S&eacute;lectionnez le fichier &agrave; importer et validez.</li>
</ol>
Les mots-cl&eacute;s en double sont ignor&eacute;s."
, 'importation_' => "Importation :"
, 'selectionnez_fichier_import' => "S&eacute;lectionnez un fichier de mots-cl&eacute;s &agrave; importer.<br />"
, 'selectionnez_groupe_destination' => "Vous devez s&eacute;lectionner un groupe de destination pour y importer vos mots-cl&eacute;s.<br />"
, 'vos_groupes_' => "Vos groupes de mots-cl&eacute;s :"

, 'exporter_mots_cles' => "Exporter des mots-cl&eacute;s"
, 'export_mots' => "Export mots-cl&eacute;s"
, 'info_exporter_mots_cles' => "S&eacute;lectionnez le groupe de mots-cl&eacute;s &agrave; exporter.<br />Les mots-cl&eacute;s sont export&eacute;s dans un fichier texte."
, 'exportation_' => "Exportation :"
, 'pas_de_groupe' => " Aucun groupe s&eacute;lectionn&eacute;."
, 'total_export' => "Total export: "
, 'groupe_vide' => "Groupe de mots-cl&eacute;s vide."

, 'titre_cadre_stopwords' => "Mots vides :"
, 'editer_stopwords' => "Editer les mots vides"
, 'vos_mots_vides_' => "Vos mots vides :"
, 'mots_vides_pour_' => "Mots vides pour @nom_lang@ [@lang@]"
, 'info_stopwords' => "Les mots vides sont ignor&eacute;s lors de la proposition d'insertion de mots-cl&eacute;s dans l'article."
, 'selectionnez_langue_stopwords' => "S&eacute;lectionnez une langue dans la liste de gauche pour localiser les mots vides"

, 'mots_detectes' => "Mots d&eacute;tect&eacute;s dans cet article"
, 'mot' => "Mot"
, 'occurence' => "Occurence"
, 'poids' => "Poids"

// page mots_edit
, 'selectionnez_langue' => "S&eacute;lectionnez une langue dans la liste de gauche pour localiser le mot-cl&eacute;"

);

?>