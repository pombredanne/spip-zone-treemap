<?php

// French Mimes Module for v2.3 (translated by Olivier Pariseau)

$GLOBALS['spx']["mimes"]=array(
	// dir, exe, file
	"dir"	=> "R&eacute;pertoire",
	"exe"	=> "Ex&eacute;cutable",
	"file"	=> "Fichier",

	// text
	"text"	=> "Fichier texte",
	
	// programming
	"php"	=> "Script PHP",
	"sql"	=> "Fichier SQL",
	"perl"	=> "Script PERL",
	"html"	=> "Page HTML",
	"js"	=> "Fichier Javascript",
	"css"	=> "Fichier CSS",
	"cgi"	=> "Script CGI",
	// C++
	"cpps"	=> "Fichier source C++",
	"cpph"	=> "Fichier source C++",
	// Java
	"javas"	=> "Fichier source Java",
	"javac"	=> "Fichier classe Java",
	// Pascal
	"pas"	=> "Fichier Pascal",
	
	// images
	"gif"	=> "Image GIF",
	"jpg"	=> "Image JPG",
	"bmp"	=> "Image BMP",
	"png"	=> "Image PNG",
	
	// compressed
	"zip"	=> "Archive ZIP",
	"tar"	=> "Archive TAR",
	"gzip"	=> "Archive GZIP",
	"bzip2"	=> "Archive BZIP2",
	"rar"	=> "Archive RAR",
	
	// music
	"mp3"	=> "Fichier audio MP3",
	"wav"	=> "Fichier audio WAV",
	"midi"	=> "Fichier audio MIDI",
	"real"	=> "Fichier RealAudio",
	
	// movie
	"mpg"	=> "Fichier vido MPG",
	"mov"	=> "Fichier Vid&eacute;o",
	"avi"	=> "Fichier vid&eacute;o AVI",
	"flash"	=> "Fichier vid&eacute;o Flash",
	
	// Micosoft / Adobe
	"word"	=> "Document Word",
	"excel"	=> "Document Excel",
	"pdf"	=> "Fichier PDF"
); ?>