<?php

$GLOBALS['i18n_cumulus_en'] = array(


// C
'choix_groupes_mots_exclus' => 'Setting keywords groups to exclude',
'couleurs' => 'Colors',
'configuration_cumulus' => 'Cumulus\'s  configuration',
'couleur_fond' => 'Background color',
'couleur_reference_max' => 'Color of the Words with maximum reference',
'couleur_reference_min' => 'Color of the Words with minimum reference',
'couleur_survol'=> 'Color of the Words hover',
'fond_transparent' => 'Transparent background',

// D
'defaut_titre' => 'cumulus',
'descriptif_configuration' => 'Cumulus\'s parameters to configure (all are optionnals)',

// E
'explication_palette' => 'to choose a color: first click on the circle, then click on the squarre',
'explication_groupe_exclus' => 'keywords in ticked\'s groups will not be viewing in Cumulus',

// I
'id_groupe' => 'groupe\'s id',

// H
'hauteur' => 'Height in pixels',
// L
'largeur' => 'Width in pixels',

// R
'reglages' => 'Various settings',

// S
'saisir_couleur' => 'choose a color by clicking in the input box',

// T
'texte' => 'text',
'titre_bloc_cumulus' => 'Title of cumulus\'s bloc',
'texte_titre_nuage' => 'Title of the tags cloud',
'titre' => 'Title',
'titre_cumulus' => 'Cumulus',

// V
'vitesse' => 'Speed of the Cumulus'

);

?>
