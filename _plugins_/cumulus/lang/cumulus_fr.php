<?php

$GLOBALS['i18n_cumulus_fr'] = array(

// C
'choix_groupes_mots_exclus' => 'D&eacute;finition des groupes de mots cl&eacute; &agrave; exclure',
'couleurs' => 'Couleurs',
'configuration_cumulus' => 'Configuration de Cumulus',
'couleur_fond' => 'Couleur du fond',
'couleur_reference_max' => 'Couleur des mots maximum reference',
'couleur_reference_min' => 'Couleur des mots minimum reference',
'couleur_survol'=> 'Couleur des mots au survol',
'fond_transparent' => 'Fond transparent',


// D
'defaut_titre' => 'cumulus',
'descriptif_configuration' => 'parametres de configuration de Cumulus',

// E
'erreur_install_groupe_technique' => 'erreur cr&eacute;ation du groupe de mots cl&eacute;s &agrave; exclure pour Cumulus',
'explication_groupe_exclus' => 'les mots cl&eacute;s des groupes coch&eacute; ne seront pas affich&eacute;s dans Cumulus',
'explication_palette' => 'pour choisir une couleur: cliquez d\'abord sur le cercle puis sur le carr&eacute;',

// I
'id' => 'id groupe',
// H
'hauteur' => 'Hauteur en pixels',
// L
'largeur' => 'Largeur en pixels',

// R
'reglages' => 'R&eacute;glages divers',

// S
'saisir_couleur' => 'choisissez une couleur en cliquant dans la zone de saisie',

// T
'texte' => 'texte',
'titre_bloc_cumulus' => 'Titre du bloc (id)',
'texte_titre_nuage' => 'Titre du cumulus',
'titre_cumulus' => ' Cumulus',
'texte_titre_site_plus'=>'Ce texte sera affich&eacute; en haut de page &agrave; cot&eacute; du logo du site',

// V
'vitesse' => 'Vitesse de d&eacute;filement'

);

?>
