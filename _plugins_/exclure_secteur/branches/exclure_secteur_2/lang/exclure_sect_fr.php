<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-exclure_sect
// Langue: fr
// Date: 18-12-2011 12:10:03
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'cfg_avances'			=>'R&eacute;glages avanc&eacute;s du plugin',
	'cfg_exclure_secteur'	=>'Configuration du plugin exclure-secteur',
	'cfg_explication'	=>'Choisissez les secteurs &agrave; exclure',
	'cfg_id_explicite'	=>"Ne pas appliquer aux les boucles sur lequel l'identifiant de l'objet est explicité ou pris dans le contexte. (Permet de ne pas modifier le squelette article.html)",
	'cfg_tout'			=>'Est-ce que le crit&eacute;re <code>{tout}</code> est &eacute;quivalent au crit&eacute;re <code>{tout_voir}</code> ?',
// E
	'exclure_secteur'=>'Exclure secteur',
);
?>