<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/fulltext?lang_cible=uk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accents_pas_pris' => 'Позначки над/між буквами не беруться до уваги ("різдв\'яний" чи "різдвяний", і дадуть такий ж результат, включаючи "різдв\'яний", "різдвяний" ...)',
	'activer_indexation' => 'Activer l\'indexation des fichiers @ext@', # NEW
	'asie' => 'азія',
	'asterisque_terminale' => 'не дасть ніякого результату: зірочка має бути в кінці',
	'aussi' => 'також',

	// C
	'casse_indifferente' => 'Написання (з великої/ з малої) букв у словах не має значення.',
	'configuration_indexation_document' => 'Configuration de l\'indexation des documents', # NEW
	'configurer_egalement_doc' => 'Vous pouvez également configurer l\'indexation des documents :', # MODIF
	'convertir_myisam' => 'Переміна на MyISAM',
	'convertir_toutes' => 'Перемінити всі вікна в MyISAM',
	'convertir_utf8' => 'перемінити в UTF-8, щоб відновити зв\'язок ',
	'creer_tous' => 'Створіть усі запропоновані індекси ПОВНОГО ТЕКСТУ',

	// D
	'des_utilisations' => '@nb@ utilisations', # NEW
	'descriptif' => 'Descriptif', # NEW
	'documents_proteges' => 'Documents protégés', # MODIF

	// E
	'enfan' => 'дит',
	'enfance' => 'дитинство',
	'enfant' => 'дитина',
	'enfanter' => 'дитина',
	'enfantillage' => 'дитячість',
	'enfants' => 'діти',
	'erreur_binaire_indisponible' => 'Ce logiciel n\'est pas disponible sur le serveur.', # NEW
	'erreur_doc_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .doc', # MODIF
	'erreur_intervalle_cron' => 'Vous devez indiquer un intervalle supérieur à une seconde.', # MODIF
	'erreur_nb_docs' => 'Vous devez indiquer un nombre de documents à traiter par itération supérieur à un.', # MODIF
	'erreur_pdf_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .pdf', # MODIF
	'erreur_ppt_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .ppt', # MODIF
	'erreur_taille_index' => 'Il faut au moins indexer un caractère.', # MODIF
	'erreur_verifier_configuration' => 'Il y a des erreurs de configuration.', # NEW
	'erreur_xls_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .xls', # MODIF
	'et' => 'І',
	'etranger' => 'незнайомець',
	'exemples' => 'Приклади використання',
	'explication_option_readonly' => 'Cette option est forcée sur ce site et n\'est donc pas configurable.', # NEW

	// F
	'fant' => 'ина',
	'fonctionnement_recherche' => 'Як пошукова система працює на сайті',
	'fulltext_cree' => 'ПОВНИЙ ТЕКСТ створено',
	'fulltext_creer' => 'Créer l\'index @index@', # NEW
	'fulltext_documentation' => 'Pour plus d\'information sur la configuration, consultez la documentation en ligne :', # NEW
	'fulltext_documents' => 'Fulltext - Documents', # NEW
	'fulltext_index' => 'Fulltext - Index', # NEW

	// G
	'general' => 'Général', # NEW

	// I
	'id' => 'ID', # NEW
	'il_faut_myisam' => 'MyISAM - необхідний',
	'incoherence_charset' => 'Існує розбіжність між набором символів вашого сайту і бази даних. Це може привести до поганих результатів при пошуку слів з буквами, що мають позначки:',
	'index_regenere' => 'перевстановлений індекс вікна',
	'index_reinitialise' => 'Нові документи, в яких були виявлені помилки, були повторно інстильовані',
	'index_reinitialise_ptg' => 'Les documents protégés ont tous été réinitialisés', # NEW
	'index_reinitialise_totalement' => 'Les document ont tous été réinitialisés', # NEW
	'index_supprime' => 'видалений індекс',
	'indiquer_chemin_bin' => 'Indiquer le chemin vers le binaire traitant l\'indexation des', # NEW
	'indiquer_options_bin' => 'Indiquer les options pour l\'indexation des', # NEW
	'infos' => 'Informations', # NEW
	'infos_documents_proteges' => 'Vous trouverez ici la liste des documents protégés et donc non-indexés par Fulltext', # NEW
	'infos_fulltext_document' => 'Vous pourrez ici choisir quels type de documents sont indexés par Fulltext et configurer les binaires utilisés et leurs options.', # NEW
	'intervalle_cron' => 'Intervalle de temps entre deux passages du CRON (en secondes).', # NEW

	// L
	'liste_tables_connues' => 'Ось список вікон, взятих до уваги при пошуку. Ви можете додати більше елементів ПОВНОГО ТЕКСТУ -- перегляньте документацію на',
	'logo' => 'Logo', # NEW

	// M
	'mais_pas' => 'але НЕ',
	'message_ok_configuration' => 'Enregistrement de vos préférences terminée', # MODIF
	'message_ok_update_configuration' => 'Mise à jour de vos préférences terminée', # MODIF

	// N
	'nb_err' => 'En erreur d\'indexation', # NEW
	'nb_index' => 'Indexés', # MODIF
	'nb_non_index' => 'Non-indexés', # MODIF
	'nb_ptg' => 'Protégés (non-indexés)', # MODIF
	'necessite_version_php' => 'Nécessite PHP 5.2 au minimum, ainsi que l\'option -enable-zip.', # MODIF
	'nombre_caracteres' => 'Nombre de caractères indexés (depuis le debut du document).', # MODIF
	'nombre_documents' => 'Nombre de documents à traiter par itération du CRON', # MODIF

	// O
	'ou_bien' => 'або ще',

	// P
	'pas_document_ptg' => 'Il n\'y a pas de document protégé.', # NEW
	'pas_index' => 'Без індексу ПОВНОГО ТЕКСТУ',
	'premier_soit' => 'АБО',

	// Q
	'que_des_exemples' => 'NB : les adresses de binaires et options proposées ici ab initio ne sont que des exemples.', # MODIF

	// R
	'regenerer_tous' => 'Повторно встановіть усі індекси ПОВНОГО ТЕКСТУ',
	'reinitialise_index_doc' => 'Повторно інстелюйте індекси документів, що показують помилки',
	'reinitialise_index_ptg' => 'Réinitialiser l\'indexation des documents protégés', # NEW
	'reinitialise_totalement_doc' => 'Réinitialiser l\'indexation de tous les documents', # NEW
	'reserve_webmestres' => 'Сторінка доступна тільки вебадміністраторам',
	'retour_configuration_fulltext' => 'Retour à la configuration de Fulltext', # MODIF
	'retourne' => 'Повертає тексти, що містять',

	// S
	'sequence_exacte' => 'точний вираз',
	'soit' => 'ЧИ',
	'statistiques_indexation' => 'Statistiques d\'indexation des documents :', # NEW
	'supprimer' => 'Видалити',

	// T
	'table_convertie' => 'вікно перемінити на MyISAM',
	'table_format' => 'Формат цього вікна -',
	'table_non_reconnue' => 'нерозпізнане вікно',
	'textes_premier' => 'але спершу показує тексти, які містять',
	'titre_page_fulltext_index' => 'Configuration des index de recherche', # NEW

	// U
	'une_utilisation' => '1 utilisation', # NEW
	'utiliser_operateurs_logiques' => 'Пошук використовує стандартн их логічн их оператор ів.',

	// V
	'voir_doc_ptg' => 'Voir les documents protegés' # NEW
);

?>
