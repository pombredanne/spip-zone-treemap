<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/fulltext?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accents_pas_pris' => 'Akzente werden nicht berücksichtigt, es werden die selben Ausdrücke gefunden, egal ob Sie « déjà » oder « deja » eingeben.',
	'activer_indexation' => 'Activer l\'indexation des fichiers @ext@', # NEW
	'asie' => 'Asien',
	'asterisque_terminale' => 'kein Ausdruck wird gefunden: Das Sternchen * muß am Ende stehen.',
	'aussi' => 'auch',

	// C
	'casse_indifferente' => 'Groß- und Kleinschreibung der gesuchten Begriffe spielt keine Rolle.',
	'configuration_indexation_document' => 'Configuration de l\'indexation des documents', # NEW
	'configurer_egalement_doc' => 'Vous pouvez également configurer l\'indexation des documents :', # MODIF
	'convertir_myisam' => 'Nach MyISAM konvertieren',
	'convertir_toutes' => 'Alle Tabellen nach MyISAM konvertieren',
	'convertir_utf8' => 'nach UTF-8 konvertieren um Kohärenz der Daten wieder herzustellen',
	'creer_tous' => 'Alle vorgeschlagenen FULLTEXT Indexe anlegen',

	// D
	'des_utilisations' => '@nb@ utilisations', # NEW
	'descriptif' => 'Descriptif', # NEW
	'documents_proteges' => 'Documents protégés', # MODIF

	// E
	'enfan' => 'Kin',
	'enfance' => 'Kindheit',
	'enfant' => 'Kind',
	'enfanter' => 'Kinder kriegen',
	'enfantillage' => 'Kinderei',
	'enfants' => 'Kinder',
	'erreur_binaire_indisponible' => 'Ce logiciel n\'est pas disponible sur le serveur.', # NEW
	'erreur_doc_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .doc', # MODIF
	'erreur_intervalle_cron' => 'Vous devez indiquer un intervalle supérieur à une seconde.', # MODIF
	'erreur_nb_docs' => 'Vous devez indiquer un nombre de documents à traiter par itération supérieur à un.', # MODIF
	'erreur_pdf_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .pdf', # MODIF
	'erreur_ppt_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .ppt', # MODIF
	'erreur_taille_index' => 'Il faut au moins indexer un caractère.', # MODIF
	'erreur_verifier_configuration' => 'Il y a des erreurs de configuration.', # NEW
	'erreur_xls_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .xls', # MODIF
	'et' => 'UND',
	'etranger' => 'Fremder',
	'exemples' => 'Anwendungsbeispiele',
	'explication_option_readonly' => 'Cette option est forcée sur ce site et n\'est donc pas configurable.', # NEW

	// F
	'fant' => 'ind',
	'fonctionnement_recherche' => 'Funktionsweise der Suchmaschine dieser Website',
	'fulltext_cree' => 'FULLTEXT angelegt',
	'fulltext_creer' => 'Créer l\'index @index@', # NEW
	'fulltext_documentation' => 'Pour plus d\'information sur la configuration, consultez la documentation en ligne :', # NEW
	'fulltext_documents' => 'Fulltext - Documents', # NEW
	'fulltext_index' => 'Fulltext - Index', # NEW

	// G
	'general' => 'Général', # NEW

	// I
	'id' => 'ID', # NEW
	'il_faut_myisam' => 'MyISAM wird benötigt',
	'incoherence_charset' => 'Die unterschiedlichen Zeichensätze ihrer Website und ihrer Datenbanktabellen können falsche Ergebnisse bei der Suche nach Sonderzeichen auslösen:',
	'index_regenere' => 'Index der Tabelle neu erzeugt',
	'index_reinitialise' => 'Die fehlerhaften Dokumente wurden neu initialisiert',
	'index_reinitialise_ptg' => 'Les documents protégés ont tous été réinitialisés', # NEW
	'index_reinitialise_totalement' => 'Les document ont tous été réinitialisés', # NEW
	'index_supprime' => 'Index gelöscht',
	'indiquer_chemin_bin' => 'Indiquer le chemin vers le binaire traitant l\'indexation des', # NEW
	'indiquer_options_bin' => 'Indiquer les options pour l\'indexation des', # NEW
	'infos' => 'Informationen',
	'infos_documents_proteges' => 'Vous trouverez ici la liste des documents protégés et donc non-indexés par Fulltext', # NEW
	'infos_fulltext_document' => 'Vous pourrez ici choisir quels type de documents sont indexés par Fulltext et configurer les binaires utilisés et leurs options.', # NEW
	'intervalle_cron' => 'Intervalle de temps entre deux passages du CRON (en secondes).', # NEW

	// L
	'liste_tables_connues' => 'Hier sind alle bei der Suche berücksichtigten Tabellen aufgeführt. Sie können weitere FULLTEXT-Elemente hinzufügen. Siehe die Dokumenation unter folgender Adresse: ',
	'logo' => 'Logo', # NEW

	// M
	'mais_pas' => 'aber NICHT',
	'message_ok_configuration' => 'Enregistrement de vos préférences terminée', # MODIF
	'message_ok_update_configuration' => 'Mise à jour de vos préférences terminée', # MODIF

	// N
	'nb_err' => 'En erreur d\'indexation', # NEW
	'nb_index' => 'Indexés', # MODIF
	'nb_non_index' => 'Non-indexés', # MODIF
	'nb_ptg' => 'Protégés (non-indexés)', # MODIF
	'necessite_version_php' => 'Nécessite PHP 5.2 au minimum, ainsi que l\'option -enable-zip.', # MODIF
	'nombre_caracteres' => 'Nombre de caractères indexés (depuis le debut du document).', # MODIF
	'nombre_documents' => 'Nombre de documents à traiter par itération du CRON', # MODIF

	// O
	'ou_bien' => 'oder',

	// P
	'pas_document_ptg' => 'Il n\'y a pas de document protégé.', # NEW
	'pas_index' => 'Kein FULLTEXT-Index',
	'premier_soit' => 'ENTWEDER',

	// Q
	'que_des_exemples' => 'NB : les adresses de binaires et options proposées ici ab initio ne sont que des exemples.', # MODIF

	// R
	'regenerer_tous' => 'Alle FULLTEXT-Indexe neu erzeugen',
	'reinitialise_index_doc' => 'Fehlerhafte Dokumente neu indizieren.',
	'reinitialise_index_ptg' => 'Réinitialiser l\'indexation des documents protégés', # NEW
	'reinitialise_totalement_doc' => 'Réinitialiser l\'indexation de tous les documents', # NEW
	'reserve_webmestres' => 'Seite nur für Webmaster zugänglich',
	'retour_configuration_fulltext' => 'Retour à la configuration de Fulltext', # MODIF
	'retourne' => 'Findet die Texte, welche folgendes enthalten:',

	// S
	'sequence_exacte' => 'Genaue Wortfolge',
	'soit' => 'ODER',
	'statistiques_indexation' => 'Statistiques d\'indexation des documents :', # NEW
	'supprimer' => 'Löschen',

	// T
	'table_convertie' => 'Tabelle nach MyISAM konvertiert',
	'table_format' => 'Diese Tabelle ist im Format',
	'table_non_reconnue' => 'Tabelle nicht erkannt',
	'textes_premier' => 'sondern zeigt als erstes die Texte, die folgendes enthalten:',
	'titre_page_fulltext_index' => 'Configuration des index de recherche', # NEW

	// U
	'une_utilisation' => '1 utilisation', # NEW
	'utiliser_operateurs_logiques' => 'Die Suche funktioniert mit den bekanntesten Operatoren.',

	// V
	'voir_doc_ptg' => 'Voir les documents protegés' # NEW
);

?>
