<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/fulltext?lang_cible=lv
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accents_pas_pris' => 'Mīkstinājuma zīmes un akcenti netiks ņemti vērā (meklējot "sāļš" vai "sals", rezultāti būs tie paši: "sāļš", "sals", "sāls"...)',
	'activer_indexation' => 'Activer l\'indexation des fichiers @ext@', # NEW
	'asie' => 'āzija',
	'asterisque_terminale' => 'nesniegs rezultātus: zvaigznītei jābūt noslēdzošai',
	'aussi' => 'arī',

	// C
	'casse_indifferente' => 'Burtu lielums vārdos (lielie/mazie burti) nav nozīmīgs.',
	'configuration_indexation_document' => 'Configuration de l\'indexation des documents', # NEW
	'configurer_egalement_doc' => 'Vous pouvez également configurer l\'indexation des documents :', # MODIF
	'convertir_myisam' => 'Pārveidot uz MyISAM',
	'convertir_toutes' => 'Pārveidot visu uz MyISAM',
	'convertir_utf8' => 'pārveidot uz UTF-8 lai atjaunotu saskaņotību',
	'creer_tous' => 'Radīt visus ieteiktos FULLTEXT rādītājus',

	// D
	'des_utilisations' => '@nb@ utilisations', # NEW
	'descriptif' => 'Descriptif', # NEW
	'documents_proteges' => 'Documents protégés', # MODIF

	// E
	'enfan' => 'bērn',
	'enfance' => 'bērnība',
	'enfant' => 'bērns',
	'enfanter' => 'bērns',
	'enfantillage' => 'bērnišķība',
	'enfants' => 'bērni',
	'erreur_binaire_indisponible' => 'Ce logiciel n\'est pas disponible sur le serveur.', # NEW
	'erreur_doc_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .doc', # MODIF
	'erreur_intervalle_cron' => 'Vous devez indiquer un intervalle supérieur à une seconde.', # MODIF
	'erreur_nb_docs' => 'Vous devez indiquer un nombre de documents à traiter par itération supérieur à un.', # MODIF
	'erreur_pdf_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .pdf', # MODIF
	'erreur_ppt_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .ppt', # MODIF
	'erreur_taille_index' => 'Il faut au moins indexer un caractère.', # MODIF
	'erreur_verifier_configuration' => 'Il y a des erreurs de configuration.', # NEW
	'erreur_xls_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .xls', # MODIF
	'et' => 'UN',
	'etranger' => 'svešinieks',
	'exemples' => 'Lietojuma piemēri',
	'explication_option_readonly' => 'Cette option est forcée sur ce site et n\'est donc pas configurable.', # NEW

	// F
	'fant' => 'rns',
	'fonctionnement_recherche' => 'Kā meklētājs darbojas šajā mājaslapā',
	'fulltext_cree' => 'radīts FULLTEXT',
	'fulltext_creer' => 'Créer l\'index @index@', # NEW
	'fulltext_documentation' => 'Pour plus d\'information sur la configuration, consultez la documentation en ligne :', # NEW
	'fulltext_documents' => 'Fulltext - Documents', # NEW
	'fulltext_index' => 'Fulltext - Index', # NEW

	// G
	'general' => 'Général', # NEW

	// I
	'id' => 'ID', # NEW
	'il_faut_myisam' => 'nepieciešams MyISAM',
	'incoherence_charset' => 'Novērota neatbilstība rakstzīmēs starp jūsu lapu un datubāzi. Tas var novest pie nepilnīgiem rezultātiem meklējumos, kas ietver mīkstinājuma zīmes:',
	'index_regenere' => 'tabulas rādītājs atjaunots',
	'index_reinitialise' => 'Les documents en erreur ont été réinitialisés', # NEW
	'index_reinitialise_ptg' => 'Les documents protégés ont tous été réinitialisés', # NEW
	'index_reinitialise_totalement' => 'Les document ont tous été réinitialisés', # NEW
	'index_supprime' => 'rādītājs izdzēsts',
	'indiquer_chemin_bin' => 'Indiquer le chemin vers le binaire traitant l\'indexation des', # NEW
	'indiquer_options_bin' => 'Indiquer les options pour l\'indexation des', # NEW
	'infos' => 'Informations', # NEW
	'infos_documents_proteges' => 'Vous trouverez ici la liste des documents protégés et donc non-indexés par Fulltext', # NEW
	'infos_fulltext_document' => 'Vous pourrez ici choisir quels type de documents sont indexés par Fulltext et configurer les binaires utilisés et leurs options.', # NEW
	'intervalle_cron' => 'Intervalle de temps entre deux passages du CRON (en secondes).', # NEW

	// L
	'liste_tables_connues' => 'Šeit ir tabulu saraksts, kas tiek ņemts vērā, izmantojot meklētāju. Jūs varat pievienot papildus FULLTEXT elementus -- skatīt dokumentāciju',
	'logo' => 'Logo', # NEW

	// M
	'mais_pas' => 'bet NE',
	'message_ok_configuration' => 'Enregistrement de vos préférences terminée', # MODIF
	'message_ok_update_configuration' => 'Mise à jour de vos préférences terminée', # MODIF

	// N
	'nb_err' => 'En erreur d\'indexation', # NEW
	'nb_index' => 'Indexés', # MODIF
	'nb_non_index' => 'Non-indexés', # MODIF
	'nb_ptg' => 'Protégés (non-indexés)', # MODIF
	'necessite_version_php' => 'Nécessite PHP 5.2 au minimum, ainsi que l\'option -enable-zip.', # MODIF
	'nombre_caracteres' => 'Nombre de caractères indexés (depuis le debut du document).', # MODIF
	'nombre_documents' => 'Nombre de documents à traiter par itération du CRON', # MODIF

	// O
	'ou_bien' => 'vai arī',

	// P
	'pas_document_ptg' => 'Il n\'y a pas de document protégé.', # NEW
	'pas_index' => 'Nav FULLTEXT rādītāja',
	'premier_soit' => 'VAI NU',

	// Q
	'que_des_exemples' => 'NB : les adresses de binaires et options proposées ici ab initio ne sont que des exemples.', # MODIF

	// R
	'regenerer_tous' => 'Atjaunot visus FULLTEXT rādītājus',
	'reinitialise_index_doc' => 'Réinitialiser l\'indexation des documents en erreur', # NEW
	'reinitialise_index_ptg' => 'Réinitialiser l\'indexation des documents protégés', # NEW
	'reinitialise_totalement_doc' => 'Réinitialiser l\'indexation de tous les documents', # NEW
	'reserve_webmestres' => 'Tikai tīmekļa pārzinis drīkst lieto šo lapu',
	'retour_configuration_fulltext' => 'Retour à la configuration de Fulltext', # MODIF
	'retourne' => 'Atgriež tekstus, kas sevī ietver',

	// S
	'sequence_exacte' => 'precīzā frāze',
	'soit' => 'VAI',
	'statistiques_indexation' => 'Statistiques d\'indexation des documents :', # NEW
	'supprimer' => 'Izdzēst',

	// T
	'table_convertie' => 'tabula pārveidota uz MyISAM',
	'table_format' => 'Šīs tabulas formāts ir',
	'table_non_reconnue' => 'neatpazīta tabula',
	'textes_premier' => 'bet rāda tos tekstus pirmos, kas sevī ietver',
	'titre_page_fulltext_index' => 'Configuration des index de recherche', # NEW

	// U
	'une_utilisation' => '1 utilisation', # NEW
	'utiliser_operateurs_logiques' => 'Šis meklētājs izmanto standarta loģiskos operatorus.',

	// V
	'voir_doc_ptg' => 'Voir les documents protegés' # NEW
);

?>
