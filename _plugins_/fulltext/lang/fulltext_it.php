<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/fulltext?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accents_pas_pris' => 'Non si tiene conto degli accenti (« perché » o « perche », daranno lo stesso « perché », « perchè », « perche »...)',
	'activer_indexation' => 'Activer l\'indexation des fichiers @ext@', # NEW
	'asie' => 'asia',
	'asterisque_terminale' => 'non darà niente: l\'asterisco * dev\'essere alla fine',
	'aussi' => 'anche',

	// C
	'casse_indifferente' => 'La cassa (minuscolo/maiuscolo) delle parole ricercate è indifferente.',
	'configuration_indexation_document' => 'Configuration de l\'indexation des documents', # NEW
	'configurer_egalement_doc' => 'Vous pouvez également configurer l\'indexation des documents :', # MODIF
	'convertir_myisam' => 'Convertire a MyISAM',
	'convertir_toutes' => 'Convertire tutte le tavole a MyISAM',
	'convertir_utf8' => 'convertire a UTF-8 per ristorare la coerenza',
	'creer_tous' => 'Creare tutti gli indici FULLTEXT suggeriti',

	// D
	'des_utilisations' => '@nb@ utilisations', # NEW
	'descriptif' => 'Descriptif', # NEW
	'documents_proteges' => 'Documents protégés', # MODIF

	// E
	'enfan' => 'bambi',
	'enfance' => 'bambinaia',
	'enfant' => 'bambino',
	'enfanter' => 'bambina',
	'enfantillage' => 'bambinaggine',
	'enfants' => 'bambini',
	'erreur_binaire_indisponible' => 'Ce logiciel n\'est pas disponible sur le serveur.', # NEW
	'erreur_doc_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .doc', # MODIF
	'erreur_intervalle_cron' => 'Vous devez indiquer un intervalle supérieur à une seconde.', # MODIF
	'erreur_nb_docs' => 'Vous devez indiquer un nombre de documents à traiter par itération supérieur à un.', # MODIF
	'erreur_pdf_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .pdf', # MODIF
	'erreur_ppt_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .ppt', # MODIF
	'erreur_taille_index' => 'Il faut au moins indexer un caractère.', # MODIF
	'erreur_verifier_configuration' => 'Il y a des erreurs de configuration.', # NEW
	'erreur_xls_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .xls', # MODIF
	'et' => 'E',
	'etranger' => 'straniero',
	'exemples' => 'Esempi d\'utilizzazione',
	'explication_option_readonly' => 'Cette option est forcée sur ce site et n\'est donc pas configurable.', # NEW

	// F
	'fant' => 'no',
	'fonctionnement_recherche' => 'Funzionamento',
	'fulltext_cree' => 'FULLTEXT creato',
	'fulltext_creer' => 'Créer l\'index @index@', # NEW
	'fulltext_documentation' => 'Pour plus d\'information sur la configuration, consultez la documentation en ligne :', # NEW
	'fulltext_documents' => 'Fulltext - Documents', # NEW
	'fulltext_index' => 'Fulltext - Index', # NEW

	// G
	'general' => 'Général', # NEW

	// I
	'id' => 'ID', # NEW
	'il_faut_myisam' => 'Bisogna MyISAM',
	'incoherence_charset' => 'Un\'incoerenza tra il charset del vostro sito e quello delle tavole della vostra base di dati rischia di falsificare le ricerche con caratteri accentati',
	'index_regenere' => 'indice della tavola rigenerati',
	'index_reinitialise' => 'I documenti sbagliati sono stati inizializzati di nuovo',
	'index_reinitialise_ptg' => 'Les documents protégés ont tous été réinitialisés', # NEW
	'index_reinitialise_totalement' => 'Les document ont tous été réinitialisés', # NEW
	'index_supprime' => 'indice cancellato',
	'indiquer_chemin_bin' => 'Indiquer le chemin vers le binaire traitant l\'indexation des', # NEW
	'indiquer_options_bin' => 'Indiquer les options pour l\'indexation des', # NEW
	'infos' => 'Informations', # NEW
	'infos_documents_proteges' => 'Vous trouverez ici la liste des documents protégés et donc non-indexés par Fulltext', # NEW
	'infos_fulltext_document' => 'Vous pourrez ici choisir quels type de documents sont indexés par Fulltext et configurer les binaires utilisés et leurs options.', # NEW
	'intervalle_cron' => 'Intervalle de temps entre deux passages du CRON (en secondes).', # NEW

	// L
	'liste_tables_connues' => 'Ecco l\'elenco delle tabelle prese in considerazione per le ricerche. È possibile aggiungere più elementi FULLTEXT - vedere la documentazione in',
	'logo' => 'Logo', # NEW

	// M
	'mais_pas' => 'ma NON',
	'message_ok_configuration' => 'Enregistrement de vos préférences terminée', # MODIF
	'message_ok_update_configuration' => 'Mise à jour de vos préférences terminée', # MODIF

	// N
	'nb_err' => 'En erreur d\'indexation', # NEW
	'nb_index' => 'Indexés', # MODIF
	'nb_non_index' => 'Non-indexés', # MODIF
	'nb_ptg' => 'Protégés (non-indexés)', # MODIF
	'necessite_version_php' => 'Nécessite PHP 5.2 au minimum, ainsi que l\'option -enable-zip.', # MODIF
	'nombre_caracteres' => 'Nombre de caractères indexés (depuis le debut du document).', # MODIF
	'nombre_documents' => 'Nombre de documents à traiter par itération du CRON', # MODIF

	// O
	'ou_bien' => 'oppure',

	// P
	'pas_document_ptg' => 'Il n\'y a pas de document protégé.', # NEW
	'pas_index' => 'Nessun indice FULLTEXT',
	'premier_soit' => 'SIA',

	// Q
	'que_des_exemples' => 'NB : les adresses de binaires et options proposées ici ab initio ne sont que des exemples.', # MODIF

	// R
	'regenerer_tous' => 'Rigenera tutti gli indici FULLTEXT',
	'reinitialise_index_doc' => 'Reinizializzare l\'indicizzazione dei documenti che mostrano un errore',
	'reinitialise_index_ptg' => 'Réinitialiser l\'indexation des documents protégés', # NEW
	'reinitialise_totalement_doc' => 'Réinitialiser l\'indexation de tous les documents', # NEW
	'reserve_webmestres' => 'Pagina riservata ai webmasters',
	'retour_configuration_fulltext' => 'Retour à la configuration de Fulltext', # MODIF
	'retourne' => 'Ritorna testi che contengono',

	// S
	'sequence_exacte' => 'la frase esatta',
	'soit' => 'o',
	'statistiques_indexation' => 'Statistiques d\'indexation des documents :', # NEW
	'supprimer' => 'Cancella',

	// T
	'table_convertie' => 'Tavola convertita a MyISAM',
	'table_format' => 'Questa tavola è al formato',
	'table_non_reconnue' => 'tavola non riconosciuta',
	'textes_premier' => 'ma presenta primo i testi che contengono',
	'titre_page_fulltext_index' => 'Configuration des index de recherche', # NEW

	// U
	'une_utilisation' => '1 utilisation', # NEW
	'utiliser_operateurs_logiques' => 'La ricerca utilizza gli operatori logici i più conosciuti',

	// V
	'voir_doc_ptg' => 'Voir les documents protegés' # NEW
);

?>
