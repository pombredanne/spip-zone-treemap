<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/fulltext?lang_cible=pl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accents_pas_pris' => 'Akcenty i znaki diakrytyczne nie są uwzględniane w szukaniu ("żółty" i "zolty", dadzą takie same wyniki, w tym "zołty", "żołty", "żólty", ...)',
	'activer_indexation' => 'Activer l\'indexation des fichiers @ext@', # NEW
	'asie' => 'Azja',
	'asterisque_terminale' => 'nie da żadnego wyniku: gwiazdka może być użyta tylko na końcu',
	'aussi' => 'także',

	// C
	'casse_indifferente' => 'Wielkość (małe/wielkie litery) nie ma znaczenia dla szukania.',
	'configuration_indexation_document' => 'Configuration de l\'indexation des documents', # NEW
	'configurer_egalement_doc' => 'Vous pouvez également configurer l\'indexation des documents :', # MODIF
	'convertir_myisam' => 'Konwertuj na MyISAM',
	'convertir_toutes' => 'Konwertuj wszystkie tablice na MyISAM',
	'convertir_utf8' => 'konwertuj na UTF-8 by przywrócić spójność',
	'creer_tous' => 'Utwórz wszystkie zalecane indeksy FULLTEXT',

	// D
	'des_utilisations' => '@nb@ utilisations', # NEW
	'descriptif' => 'Descriptif', # NEW
	'documents_proteges' => 'Documents protégés', # MODIF

	// E
	'enfan' => 'dziec',
	'enfance' => 'dzieciństwo',
	'enfant' => 'dziecko',
	'enfanter' => 'dzieciątko',
	'enfantillage' => 'dziecinada',
	'enfants' => 'dzieci',
	'erreur_binaire_indisponible' => 'Ce logiciel n\'est pas disponible sur le serveur.', # NEW
	'erreur_doc_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .doc', # MODIF
	'erreur_intervalle_cron' => 'Vous devez indiquer un intervalle supérieur à une seconde.', # MODIF
	'erreur_nb_docs' => 'Vous devez indiquer un nombre de documents à traiter par itération supérieur à un.', # MODIF
	'erreur_pdf_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .pdf', # MODIF
	'erreur_ppt_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .ppt', # MODIF
	'erreur_taille_index' => 'Il faut au moins indexer un caractère.', # MODIF
	'erreur_verifier_configuration' => 'Il y a des erreurs de configuration.', # NEW
	'erreur_xls_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .xls', # MODIF
	'et' => 'I',
	'etranger' => 'nieznane',
	'exemples' => 'Przykłady zastosowania',
	'explication_option_readonly' => 'Cette option est forcée sur ce site et n\'est donc pas configurable.', # NEW

	// F
	'fant' => 'iec',
	'fonctionnement_recherche' => 'Jak działa mechanizm szukania na tej stronie',
	'fulltext_cree' => 'FULLTEXT utworzony',
	'fulltext_creer' => 'Créer l\'index @index@', # NEW
	'fulltext_documentation' => 'Pour plus d\'information sur la configuration, consultez la documentation en ligne :', # NEW
	'fulltext_documents' => 'Fulltext - Documents', # NEW
	'fulltext_index' => 'Fulltext - Index', # NEW

	// G
	'general' => 'Général', # NEW

	// I
	'id' => 'ID', # NEW
	'il_faut_myisam' => 'MyISAM wymagany',
	'incoherence_charset' => 'Występuje niezgodność strony kodowej strony www i bazy danych. To może powodować błędne wyniki dla szukania wg warunków zawierających akcenty i znaki diakrytyczne:',
	'index_regenere' => 'tablica indeksów odbudowana',
	'index_reinitialise' => 'Dokument opisujący błąd został ponownie zainicjalizowany',
	'index_reinitialise_ptg' => 'Les documents protégés ont tous été réinitialisés', # NEW
	'index_reinitialise_totalement' => 'Les document ont tous été réinitialisés', # NEW
	'index_supprime' => 'indeks usunięty',
	'indiquer_chemin_bin' => 'Indiquer le chemin vers le binaire traitant l\'indexation des', # NEW
	'indiquer_options_bin' => 'Indiquer les options pour l\'indexation des', # NEW
	'infos' => 'Informations', # NEW
	'infos_documents_proteges' => 'Vous trouverez ici la liste des documents protégés et donc non-indexés par Fulltext', # NEW
	'infos_fulltext_document' => 'Vous pourrez ici choisir quels type de documents sont indexés par Fulltext et configurer les binaires utilisés et leurs options.', # NEW
	'intervalle_cron' => 'Intervalle de temps entre deux passages du CRON (en secondes).', # NEW

	// L
	'liste_tables_connues' => 'Oto lista tablic branych pod uwagę przy wyszukiwaniu. Możesz dodać więcej elementów FULLTEXT - zobacz dokumentacją na',
	'logo' => 'Logo', # NEW

	// M
	'mais_pas' => 'ale NIE',
	'message_ok_configuration' => 'Enregistrement de vos préférences terminée', # MODIF
	'message_ok_update_configuration' => 'Mise à jour de vos préférences terminée', # MODIF

	// N
	'nb_err' => 'En erreur d\'indexation', # NEW
	'nb_index' => 'Indexés', # MODIF
	'nb_non_index' => 'Non-indexés', # MODIF
	'nb_ptg' => 'Protégés (non-indexés)', # MODIF
	'necessite_version_php' => 'Nécessite PHP 5.2 au minimum, ainsi que l\'option -enable-zip.', # MODIF
	'nombre_caracteres' => 'Nombre de caractères indexés (depuis le debut du document).', # MODIF
	'nombre_documents' => 'Nombre de documents à traiter par itération du CRON', # MODIF

	// O
	'ou_bien' => 'lub także',

	// P
	'pas_document_ptg' => 'Il n\'y a pas de document protégé.', # NEW
	'pas_index' => 'Brak indeksu FULLTEXT',
	'premier_soit' => 'TAKŻE',

	// Q
	'que_des_exemples' => 'NB : les adresses de binaires et options proposées ici ab initio ne sont que des exemples.', # MODIF

	// R
	'regenerer_tous' => 'Wygeneruj ponownie wszystkie indeksy FULLTEXT',
	'reinitialise_index_doc' => 'Ponownie zainicjuj dokumenty opisujące błąd',
	'reinitialise_index_ptg' => 'Réinitialiser l\'indexation des documents protégés', # NEW
	'reinitialise_totalement_doc' => 'Réinitialiser l\'indexation de tous les documents', # NEW
	'reserve_webmestres' => 'Tylko administratorzy mogą korzystać z tej strony',
	'retour_configuration_fulltext' => 'Retour à la configuration de Fulltext', # MODIF
	'retourne' => 'Zwróci tekst zawierający',

	// S
	'sequence_exacte' => 'dokładny tekst',
	'soit' => 'LUB',
	'statistiques_indexation' => 'Statistiques d\'indexation des documents :', # NEW
	'supprimer' => 'Usuń',

	// T
	'table_convertie' => 'tablica przekonwertowana na MyISAM',
	'table_format' => 'Ta tablica jest w formacie',
	'table_non_reconnue' => 'nierozpoznana tablica',
	'textes_premier' => 'ale pokazuje pierwszy tekst zawierający',
	'titre_page_fulltext_index' => 'Configuration des index de recherche', # NEW

	// U
	'une_utilisation' => '1 utilisation', # NEW
	'utiliser_operateurs_logiques' => 'Mechanizm szukania używa standardowych operatorów.',

	// V
	'voir_doc_ptg' => 'Voir les documents protegés' # NEW
);

?>
