<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/fulltext?lang_cible=pt
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accents_pas_pris' => 'Os acentos são ignorados («órgão» ou «orgao» são interpretados da mesma forma...)',
	'activer_indexation' => 'Activer l\'indexation des fichiers @ext@', # NEW
	'asie' => 'Ásia',
	'asterisque_terminale' => 'não dará nada: o asterisco * deve estar no fim',
	'aussi' => 'também',

	// C
	'casse_indifferente' => 'O uso de maíusculas ou minúsculas é indiferente.',
	'configuration_indexation_document' => 'Configuration de l\'indexation des documents', # NEW
	'configurer_egalement_doc' => 'Vous pouvez également configurer l\'indexation des documents :', # MODIF
	'convertir_myisam' => 'Converter em MyISAM',
	'convertir_toutes' => 'Converter todas as tabelas em MyISAM',
	'convertir_utf8' => 'converter em UTF-8 para repor a coerência.',
	'creer_tous' => 'Criar todos os índices FULLTEXT sugeridos',

	// D
	'des_utilisations' => '@nb@ utilisations', # NEW
	'descriptif' => 'Descriptif', # NEW
	'documents_proteges' => 'Documents protégés', # MODIF

	// E
	'enfan' => 'Cria',
	'enfance' => 'criatividade',
	'enfant' => 'Criador',
	'enfanter' => 'criar',
	'enfantillage' => 'Criação',
	'enfants' => 'criança',
	'erreur_binaire_indisponible' => 'Ce logiciel n\'est pas disponible sur le serveur.', # NEW
	'erreur_doc_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .doc', # MODIF
	'erreur_intervalle_cron' => 'Vous devez indiquer un intervalle supérieur à une seconde.', # MODIF
	'erreur_nb_docs' => 'Vous devez indiquer un nombre de documents à traiter par itération supérieur à un.', # MODIF
	'erreur_pdf_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .pdf', # MODIF
	'erreur_ppt_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .ppt', # MODIF
	'erreur_taille_index' => 'Il faut au moins indexer un caractère.', # MODIF
	'erreur_verifier_configuration' => 'Il y a des erreurs de configuration.', # NEW
	'erreur_xls_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .xls', # MODIF
	'et' => 'E',
	'etranger' => 'estrangeiro',
	'exemples' => 'Exemplos de utilização',
	'explication_option_readonly' => 'Cette option est forcée sur ce site et n\'est donc pas configurable.', # NEW

	// F
	'fant' => 'ção',
	'fonctionnement_recherche' => 'Funcionamento do motor de procura deste sítio',
	'fulltext_cree' => 'FULLTEXT criado',
	'fulltext_creer' => 'Créer l\'index @index@', # NEW
	'fulltext_documentation' => 'Pour plus d\'information sur la configuration, consultez la documentation en ligne :', # NEW
	'fulltext_documents' => 'Fulltext - Documents', # NEW
	'fulltext_index' => 'Fulltext - Index', # NEW

	// G
	'general' => 'Général', # NEW

	// I
	'id' => 'ID', # NEW
	'il_faut_myisam' => 'precisa de MyISAM',
	'incoherence_charset' => 'Uma incoerência entre os caractères do seu sítio e o das tabelas da sua base de dados arrisca-se a falhar nas procuras com caractères acentuados:',
	'index_regenere' => 'o índice da tabela foi regenerado',
	'index_reinitialise' => 'Os documentos com erro foram recomeçados',
	'index_reinitialise_ptg' => 'Les documents protégés ont tous été réinitialisés', # NEW
	'index_reinitialise_totalement' => 'Les document ont tous été réinitialisés', # NEW
	'index_supprime' => 'O índice foi suprimido',
	'indiquer_chemin_bin' => 'Indiquer le chemin vers le binaire traitant l\'indexation des', # NEW
	'indiquer_options_bin' => 'Indiquer les options pour l\'indexation des', # NEW
	'infos' => 'Informations', # NEW
	'infos_documents_proteges' => 'Vous trouverez ici la liste des documents protégés et donc non-indexés par Fulltext', # NEW
	'infos_fulltext_document' => 'Vous pourrez ici choisir quels type de documents sont indexés par Fulltext et configurer les binaires utilisés et leurs options.', # NEW
	'intervalle_cron' => 'Intervalle de temps entre deux passages du CRON (en secondes).', # NEW

	// L
	'liste_tables_connues' => 'Estão aqui as tabelas conhecidas da procura. Pode acrescentar elementos FULLTEXT, cf. documentação ao endereço',
	'logo' => 'Logo', # NEW

	// M
	'mais_pas' => 'mas NÃO',
	'message_ok_configuration' => 'Enregistrement de vos préférences terminée', # MODIF
	'message_ok_update_configuration' => 'Mise à jour de vos préférences terminée', # MODIF

	// N
	'nb_err' => 'En erreur d\'indexation', # NEW
	'nb_index' => 'Indexés', # MODIF
	'nb_non_index' => 'Non-indexés', # MODIF
	'nb_ptg' => 'Protégés (non-indexés)', # MODIF
	'necessite_version_php' => 'Nécessite PHP 5.2 au minimum, ainsi que l\'option -enable-zip.', # MODIF
	'nombre_caracteres' => 'Nombre de caractères indexés (depuis le debut du document).', # MODIF
	'nombre_documents' => 'Nombre de documents à traiter par itération du CRON', # MODIF

	// O
	'ou_bien' => 'ou',

	// P
	'pas_document_ptg' => 'Il n\'y a pas de document protégé.', # NEW
	'pas_index' => 'Não há índice FULLTEXT',
	'premier_soit' => 'OU',

	// Q
	'que_des_exemples' => 'NB : les adresses de binaires et options proposées ici ab initio ne sont que des exemples.', # MODIF

	// R
	'regenerer_tous' => 'Regenerar todos os índices FULLTEXT',
	'reinitialise_index_doc' => 'Reinicializar a indexação dos documentos com erro',
	'reinitialise_index_ptg' => 'Réinitialiser l\'indexation des documents protégés', # NEW
	'reinitialise_totalement_doc' => 'Réinitialiser l\'indexation de tous les documents', # NEW
	'reserve_webmestres' => 'Página reservada aos webmasters',
	'retour_configuration_fulltext' => 'Retour à la configuration de Fulltext', # MODIF
	'retourne' => 'Dá os textos que contêm',

	// S
	'sequence_exacte' => 'exactamente a sequência de palavras',
	'soit' => 'OU',
	'statistiques_indexation' => 'Statistiques d\'indexation des documents :', # NEW
	'supprimer' => 'Suprimir',

	// T
	'table_convertie' => 'tabela convertida em MyISAM',
	'table_format' => 'Esta tabela está no formato',
	'table_non_reconnue' => 'tabela não reconhecida',
	'textes_premier' => 'mas apresenta primeiro os textos que contêm',
	'titre_page_fulltext_index' => 'Configuration des index de recherche', # NEW

	// U
	'une_utilisation' => '1 utilisation', # NEW
	'utiliser_operateurs_logiques' => 'A procura utiliza os operadores lógicos mais vulgares.',

	// V
	'voir_doc_ptg' => 'Voir les documents protegés' # NEW
);

?>
