<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/fulltext?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accents_pas_pris' => 'Los acentos no son tenidos en cuenta',
	'activer_indexation' => 'Activer l\'indexation des fichiers @ext@', # NEW
	'asie' => 'asia',
	'asterisque_terminale' => 'no devolverá nada: el asterisco * debe ser terminal',
	'aussi' => 'también',

	// C
	'casse_indifferente' => 'El recuadro (minúscula/mayúscula) de búsqueda de palabras en indiferente',
	'configuration_indexation_document' => 'Configuration de l\'indexation des documents', # NEW
	'configurer_egalement_doc' => 'Vous pouvez également configurer l\'indexation des documents :', # MODIF
	'convertir_myisam' => 'Convertir en MyISAM',
	'convertir_toutes' => 'Convertir todos las caudros en MyISAM',
	'convertir_utf8' => 'convertir en UTF-8 para restaurar la coherencia',
	'creer_tous' => 'Crear todo los index FULLTEXT sugeridos',

	// D
	'des_utilisations' => '@nb@ utilisations', # NEW
	'descriptif' => 'Descriptif', # NEW
	'documents_proteges' => 'Documents protégés', # MODIF

	// E
	'enfan' => 'hijo',
	'enfance' => 'infancia',
	'enfant' => 'niño',
	'enfanter' => 'dar a luz',
	'enfantillage' => 'chiquilinada',
	'enfants' => 'hijos',
	'erreur_binaire_indisponible' => 'Ce logiciel n\'est pas disponible sur le serveur.', # NEW
	'erreur_doc_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .doc', # MODIF
	'erreur_intervalle_cron' => 'Vous devez indiquer un intervalle supérieur à une seconde.', # MODIF
	'erreur_nb_docs' => 'Vous devez indiquer un nombre de documents à traiter par itération supérieur à un.', # MODIF
	'erreur_pdf_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .pdf', # MODIF
	'erreur_ppt_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .ppt', # MODIF
	'erreur_taille_index' => 'Il faut au moins indexer un caractère.', # MODIF
	'erreur_verifier_configuration' => 'Il y a des erreurs de configuration.', # NEW
	'erreur_xls_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .xls', # MODIF
	'et' => 'Y',
	'etranger' => 'extranjero',
	'exemples' => 'Ejemplos de utilización',
	'explication_option_readonly' => 'Cette option est forcée sur ce site et n\'est donc pas configurable.', # NEW

	// F
	'fant' => 'fant',
	'fonctionnement_recherche' => 'Funcionamiento del motor de búsqueda de este sitio',
	'fulltext_cree' => 'FULLTEXT creado',
	'fulltext_creer' => 'Créer l\'index @index@', # NEW
	'fulltext_documentation' => 'Pour plus d\'information sur la configuration, consultez la documentation en ligne :', # NEW
	'fulltext_documents' => 'Fulltext - Documents', # NEW
	'fulltext_index' => 'Fulltext - Index', # NEW

	// G
	'general' => 'Général', # NEW

	// I
	'id' => 'ID', # NEW
	'il_faut_myisam' => 'hace falta MyISAM',
	'incoherence_charset' => 'Una incoherencia entre el charset de su sitio y el de sus base de datos puede adulterar las búsquedas con caracteres acentuados',
	'index_regenere' => 'index de los cuadros regenerados',
	'index_reinitialise' => 'Los documentos en error han sido re-inicializados',
	'index_reinitialise_ptg' => 'Les documents protégés ont tous été réinitialisés', # NEW
	'index_reinitialise_totalement' => 'Les document ont tous été réinitialisés', # NEW
	'index_supprime' => 'index suprimido',
	'indiquer_chemin_bin' => 'Indiquer le chemin vers le binaire traitant l\'indexation des', # NEW
	'indiquer_options_bin' => 'Indiquer les options pour l\'indexation des', # NEW
	'infos' => 'Informations', # NEW
	'infos_documents_proteges' => 'Vous trouverez ici la liste des documents protégés et donc non-indexés par Fulltext', # NEW
	'infos_fulltext_document' => 'Vous pourrez ici choisir quels type de documents sont indexés par Fulltext et configurer les binaires utilisés et leurs options.', # NEW
	'intervalle_cron' => 'Intervalle de temps entre deux passages du CRON (en secondes).', # NEW

	// L
	'liste_tables_connues' => 'Esta es la lista de los cuadros conocidas de la búsqueda. Puede agregar elementos FULLTEXT, ver la documentación de la dirección',
	'logo' => 'Logo', # NEW

	// M
	'mais_pas' => 'pero NO',
	'message_ok_configuration' => 'Enregistrement de vos préférences terminée', # MODIF
	'message_ok_update_configuration' => 'Mise à jour de vos préférences terminée', # MODIF

	// N
	'nb_err' => 'En erreur d\'indexation', # NEW
	'nb_index' => 'Indexés', # MODIF
	'nb_non_index' => 'Non-indexés', # MODIF
	'nb_ptg' => 'Protégés (non-indexés)', # MODIF
	'necessite_version_php' => 'Nécessite PHP 5.2 au minimum, ainsi que l\'option -enable-zip.', # MODIF
	'nombre_caracteres' => 'Nombre de caractères indexés (depuis le debut du document).', # MODIF
	'nombre_documents' => 'Nombre de documents à traiter par itération du CRON', # MODIF

	// O
	'ou_bien' => 'o bien',

	// P
	'pas_document_ptg' => 'Il n\'y a pas de document protégé.', # NEW
	'pas_index' => 'Sin index FULLTEXT',
	'premier_soit' => 'SEA',

	// Q
	'que_des_exemples' => 'NB : les adresses de binaires et options proposées ici ab initio ne sont que des exemples.', # MODIF

	// R
	'regenerer_tous' => 'Regenerar todos los index FULLTEXT',
	'reinitialise_index_doc' => 'Reinicializar la indexación de los documentos en error.',
	'reinitialise_index_ptg' => 'Réinitialiser l\'indexation des documents protégés', # NEW
	'reinitialise_totalement_doc' => 'Réinitialiser l\'indexation de tous les documents', # NEW
	'reserve_webmestres' => 'Página reservada a los administradores web (webmaster)',
	'retour_configuration_fulltext' => 'Retour à la configuration de Fulltext', # MODIF
	'retourne' => 'Volver a los textos que contienen',

	// S
	'sequence_exacte' => 'la secuencia exacta de palabras',
	'soit' => 'SEA',
	'statistiques_indexation' => 'Statistiques d\'indexation des documents :', # NEW
	'supprimer' => 'Suprimir',

	// T
	'table_convertie' => 'cuadro convertido en MyISAM',
	'table_format' => 'Este cuadro tiene formato',
	'table_non_reconnue' => 'cuadro no reconocido',
	'textes_premier' => 'pero mostrar primero los textos que contienen',
	'titre_page_fulltext_index' => 'Configuration des index de recherche', # NEW

	// U
	'une_utilisation' => '1 utilisation', # NEW
	'utiliser_operateurs_logiques' => 'La búsqueda utiliza los operadores lógicos más corrientes',

	// V
	'voir_doc_ptg' => 'Voir les documents protegés' # NEW
);

?>
