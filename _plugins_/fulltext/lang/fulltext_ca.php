<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/fulltext?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accents_pas_pris' => 'Els accents no es tenen en compte (« déjà » o « deja », ens retornaran el mateix « déjà », « dejà », « déja »...)',
	'activer_indexation' => 'Activer l\'indexation des fichiers @ext@', # NEW
	'asie' => 'àsia ',
	'asterisque_terminale' => 'no retornarà res: l\'asterisc* ha de ser terminal',
	'aussi' => 'també',

	// C
	'casse_indifferente' => 'La caixa (minúscula/majúscula) de les paraules cercades és indiferent.',
	'configuration_indexation_document' => 'Configuration de l\'indexation des documents', # NEW
	'configurer_egalement_doc' => 'Vous pouvez également configurer l\'indexation des documents :', # MODIF
	'convertir_myisam' => 'Convertir en MyISAM',
	'convertir_toutes' => 'Convertir totes les taules en MyISAM',
	'convertir_utf8' => 'convertir en UTF-8 per restaurar la coherència',
	'creer_tous' => 'Crear tots els índexs FULLTEXT suggerits',

	// D
	'des_utilisations' => '@nb@ utilisations', # NEW
	'descriptif' => 'Descriptif', # NEW
	'documents_proteges' => 'Documents protégés', # MODIF

	// E
	'enfan' => 'infan',
	'enfance' => 'infància',
	'enfant' => 'infant',
	'enfanter' => 'infantar',
	'enfantillage' => 'criaturada',
	'enfants' => 'infants',
	'erreur_binaire_indisponible' => 'Ce logiciel n\'est pas disponible sur le serveur.', # NEW
	'erreur_doc_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .doc', # MODIF
	'erreur_intervalle_cron' => 'Vous devez indiquer un intervalle supérieur à une seconde.', # MODIF
	'erreur_nb_docs' => 'Vous devez indiquer un nombre de documents à traiter par itération supérieur à un.', # MODIF
	'erreur_pdf_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .pdf', # MODIF
	'erreur_ppt_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .ppt', # MODIF
	'erreur_taille_index' => 'Il faut au moins indexer un caractère.', # MODIF
	'erreur_verifier_configuration' => 'Il y a des erreurs de configuration.', # NEW
	'erreur_xls_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .xls', # MODIF
	'et' => 'I',
	'etranger' => 'estranger',
	'exemples' => 'Exemples d\'ús',
	'explication_option_readonly' => 'Cette option est forcée sur ce site et n\'est donc pas configurable.', # NEW

	// F
	'fant' => 'fant',
	'fonctionnement_recherche' => 'Funcionament del motor de cerca d\'aquest lloc',
	'fulltext_cree' => 'FULLTEXT creat',
	'fulltext_creer' => 'Créer l\'index @index@', # NEW
	'fulltext_documentation' => 'Pour plus d\'information sur la configuration, consultez la documentation en ligne :', # NEW
	'fulltext_documents' => 'Fulltext - Documents', # NEW
	'fulltext_index' => 'Fulltext - Index', # NEW

	// G
	'general' => 'Général', # NEW

	// I
	'id' => 'ID', # NEW
	'il_faut_myisam' => 'cal MyISAM',
	'incoherence_charset' => 'Una incoherència entre el joc de caràcters del vostre lloc i el de les taules de la vostra base de dades pot falsejar les cerques amb caràcters accentuats:',
	'index_regenere' => 'índex de la taula reconstruïda',
	'index_reinitialise' => 'Els documents amb error s\'han reinicialitzat',
	'index_reinitialise_ptg' => 'Les documents protégés ont tous été réinitialisés', # NEW
	'index_reinitialise_totalement' => 'Les document ont tous été réinitialisés', # NEW
	'index_supprime' => 'índex suprimit',
	'indiquer_chemin_bin' => 'Indiquer le chemin vers le binaire traitant l\'indexation des', # NEW
	'indiquer_options_bin' => 'Indiquer les options pour l\'indexation des', # NEW
	'infos' => 'Informations', # NEW
	'infos_documents_proteges' => 'Vous trouverez ici la liste des documents protégés et donc non-indexés par Fulltext', # NEW
	'infos_fulltext_document' => 'Vous pourrez ici choisir quels type de documents sont indexés par Fulltext et configurer les binaires utilisés et leurs options.', # NEW
	'intervalle_cron' => 'Intervalle de temps entre deux passages du CRON (en secondes).', # NEW

	// L
	'liste_tables_connues' => 'Heus ací la llista de les taules conegudes de la cerca. Podeu afegir-hi elements FULLTEXT, segons la documentació, a l\'adreça',
	'logo' => 'Logo', # NEW

	// M
	'mais_pas' => 'però NO',
	'message_ok_configuration' => 'Enregistrement de vos préférences terminée', # MODIF
	'message_ok_update_configuration' => 'Mise à jour de vos préférences terminée', # MODIF

	// N
	'nb_err' => 'En erreur d\'indexation', # NEW
	'nb_index' => 'Indexés', # MODIF
	'nb_non_index' => 'Non-indexés', # MODIF
	'nb_ptg' => 'Protégés (non-indexés)', # MODIF
	'necessite_version_php' => 'Nécessite PHP 5.2 au minimum, ainsi que l\'option -enable-zip.', # MODIF
	'nombre_caracteres' => 'Nombre de caractères indexés (depuis le debut du document).', # MODIF
	'nombre_documents' => 'Nombre de documents à traiter par itération du CRON', # MODIF

	// O
	'ou_bien' => 'o bé',

	// P
	'pas_document_ptg' => 'Il n\'y a pas de document protégé.', # NEW
	'pas_index' => 'Cap índex FULLTEXT',
	'premier_soit' => 'O',

	// Q
	'que_des_exemples' => 'NB : les adresses de binaires et options proposées ici ab initio ne sont que des exemples.', # MODIF

	// R
	'regenerer_tous' => 'Renovar tots els índex FULLTEXT',
	'reinitialise_index_doc' => 'Tornar a iniciar la indexació dels documents amb errors',
	'reinitialise_index_ptg' => 'Réinitialiser l\'indexation des documents protégés', # NEW
	'reinitialise_totalement_doc' => 'Réinitialiser l\'indexation de tous les documents', # NEW
	'reserve_webmestres' => 'Pàgina reservada als webmestres',
	'retour_configuration_fulltext' => 'Retour à la configuration de Fulltext', # MODIF
	'retourne' => 'Retorna els texts que contenen',

	// S
	'sequence_exacte' => 'exactament la seqüència de paraules',
	'soit' => 'O',
	'statistiques_indexation' => 'Statistiques d\'indexation des documents :', # NEW
	'supprimer' => 'Suprimir',

	// T
	'table_convertie' => 'taula convertida en MyISAM',
	'table_format' => 'Aquesta taula està en format',
	'table_non_reconnue' => 'taula no reconeguda',
	'textes_premier' => 'però presenta primer els texts que contenen',
	'titre_page_fulltext_index' => 'Configuration des index de recherche', # NEW

	// U
	'une_utilisation' => '1 utilisation', # NEW
	'utiliser_operateurs_logiques' => 'La cerca utilitza els operadors lògics més corrents.',

	// V
	'voir_doc_ptg' => 'Voir les documents protegés' # NEW
);

?>
