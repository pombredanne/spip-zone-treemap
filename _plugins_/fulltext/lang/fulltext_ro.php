<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/fulltext?lang_cible=ro
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accents_pas_pris' => 'Accentele nu sunt luate în considerare (« înţelegere » sau « intelegere », vor avea ca rezultat: « înţelegere », « inţelegere », « întelegere »...)',
	'activer_indexation' => 'Activer l\'indexation des fichiers @ext@', # NEW
	'asie' => 'asia',
	'asterisque_terminale' => 'nu va întoarce nimic: asteriscul * trebuie să fie la final',
	'aussi' => 'de asemenea',

	// C
	'casse_indifferente' => 'Minuscul/majuscul este nerelevant.',
	'configuration_indexation_document' => 'Configuration de l\'indexation des documents', # NEW
	'configurer_egalement_doc' => 'Vous pouvez également configurer l\'indexation des documents :', # MODIF
	'convertir_myisam' => 'Convertiţi în MyISAM',
	'convertir_toutes' => 'Convertiţi toate tabelele în MyISAM',
	'convertir_utf8' => 'convertiţi în UTF-8 pentru a restaura coerenţa',
	'creer_tous' => 'Creaţi toţi indecşii FULLTEXT sugeraţi',

	// D
	'des_utilisations' => '@nb@ utilisations', # NEW
	'descriptif' => 'Descriptif', # NEW
	'documents_proteges' => 'Documents protégés', # MODIF

	// E
	'enfan' => 'cop',
	'enfance' => 'copilărie',
	'enfant' => 'copil',
	'enfanter' => 'copiii',
	'enfantillage' => 'copilăresc',
	'enfants' => 'copii',
	'erreur_binaire_indisponible' => 'Ce logiciel n\'est pas disponible sur le serveur.', # NEW
	'erreur_doc_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .doc', # MODIF
	'erreur_intervalle_cron' => 'Vous devez indiquer un intervalle supérieur à une seconde.', # MODIF
	'erreur_nb_docs' => 'Vous devez indiquer un nombre de documents à traiter par itération supérieur à un.', # MODIF
	'erreur_pdf_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .pdf', # MODIF
	'erreur_ppt_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .ppt', # MODIF
	'erreur_taille_index' => 'Il faut au moins indexer un caractère.', # MODIF
	'erreur_verifier_configuration' => 'Il y a des erreurs de configuration.', # NEW
	'erreur_xls_bin' => 'Vous devez renseigner le binaire à utiliser pour extraire les .xls', # MODIF
	'et' => 'ŞI',
	'etranger' => 'străin',
	'exemples' => 'Exemple de utilizare',
	'explication_option_readonly' => 'Cette option est forcée sur ce site et n\'est donc pas configurable.', # NEW

	// F
	'fant' => 'pil',
	'fonctionnement_recherche' => 'Funcţionarea motorului de căutare a acestui site',
	'fulltext_cree' => 'FULLTEXT creat',
	'fulltext_creer' => 'Créer l\'index @index@', # NEW
	'fulltext_documentation' => 'Pour plus d\'information sur la configuration, consultez la documentation en ligne :', # NEW
	'fulltext_documents' => 'Fulltext - Documents', # NEW
	'fulltext_index' => 'Fulltext - Index', # NEW

	// G
	'general' => 'Général', # NEW

	// I
	'id' => 'ID', # NEW
	'il_faut_myisam' => 'trebuie MyISAM',
	'incoherence_charset' => 'O incoerenţă intre charset-ul site-ului şi cel al tabelelor din baza de date riscă să dea rezultate de căutare false în cazul caracterelor accentuate:',
	'index_regenere' => 'index-ul tabelei regenerat',
	'index_reinitialise' => 'Documentele în eroare au fost resetate',
	'index_reinitialise_ptg' => 'Les documents protégés ont tous été réinitialisés', # NEW
	'index_reinitialise_totalement' => 'Les document ont tous été réinitialisés', # NEW
	'index_supprime' => 'index şters',
	'indiquer_chemin_bin' => 'Indiquer le chemin vers le binaire traitant l\'indexation des', # NEW
	'indiquer_options_bin' => 'Indiquer les options pour l\'indexation des', # NEW
	'infos' => 'Informations', # NEW
	'infos_documents_proteges' => 'Vous trouverez ici la liste des documents protégés et donc non-indexés par Fulltext', # NEW
	'infos_fulltext_document' => 'Vous pourrez ici choisir quels type de documents sont indexés par Fulltext et configurer les binaires utilisés et leurs options.', # NEW
	'intervalle_cron' => 'Intervalle de temps entre deux passages du CRON (en secondes).', # NEW

	// L
	'liste_tables_connues' => 'Iată lista tabelelor cunoscute pentru căutare. Puteţi să adăugaţi elemente FULLTEXT, conform documentaţiei de la adresa',
	'logo' => 'Logo', # NEW

	// M
	'mais_pas' => 'dar NU',
	'message_ok_configuration' => 'Enregistrement de vos préférences terminée', # MODIF
	'message_ok_update_configuration' => 'Mise à jour de vos préférences terminée', # MODIF

	// N
	'nb_err' => 'En erreur d\'indexation', # NEW
	'nb_index' => 'Indexés', # MODIF
	'nb_non_index' => 'Non-indexés', # MODIF
	'nb_ptg' => 'Protégés (non-indexés)', # MODIF
	'necessite_version_php' => 'Nécessite PHP 5.2 au minimum, ainsi que l\'option -enable-zip.', # MODIF
	'nombre_caracteres' => 'Nombre de caractères indexés (depuis le debut du document).', # MODIF
	'nombre_documents' => 'Nombre de documents à traiter par itération du CRON', # MODIF

	// O
	'ou_bien' => 'sau ',

	// P
	'pas_document_ptg' => 'Il n\'y a pas de document protégé.', # NEW
	'pas_index' => 'Fără index FULLTEXT',
	'premier_soit' => 'FIE',

	// Q
	'que_des_exemples' => 'NB : les adresses de binaires et options proposées ici ab initio ne sont que des exemples.', # MODIF

	// R
	'regenerer_tous' => 'Regeneraţi toţi indecşii FULLTEXT',
	'reinitialise_index_doc' => 'Resetaţi indexarea documentelor în eroare',
	'reinitialise_index_ptg' => 'Réinitialiser l\'indexation des documents protégés', # NEW
	'reinitialise_totalement_doc' => 'Réinitialiser l\'indexation de tous les documents', # NEW
	'reserve_webmestres' => 'Pagină rezervată webmaster-ilor',
	'retour_configuration_fulltext' => 'Retour à la configuration de Fulltext', # MODIF
	'retourne' => 'Întoarce textele care conţin',

	// S
	'sequence_exacte' => 'exact secvenţa de cuvinte',
	'soit' => 'SAU',
	'statistiques_indexation' => 'Statistiques d\'indexation des documents :', # NEW
	'supprimer' => 'Ştergeţi',

	// T
	'table_convertie' => 'tabelă convertită în MyISAM',
	'table_format' => 'Această tabelă este în formatul',
	'table_non_reconnue' => 'tabelă nerecunoscută',
	'textes_premier' => 'dar prezintă mai întâi textele care conţin',
	'titre_page_fulltext_index' => 'Configuration des index de recherche', # NEW

	// U
	'une_utilisation' => '1 utilisation', # NEW
	'utiliser_operateurs_logiques' => 'Căutarea utilizează operatorii logici cei mai curenţi.',

	// V
	'voir_doc_ptg' => 'Voir les documents protegés' # NEW
);

?>
