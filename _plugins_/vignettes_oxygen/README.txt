Vignettes pour SPIP basées sur les icônes Oxygen (Sources : http://www.softicons.com/free-icons/system-icons/oxygen-icons-by-oxygen), sous licence GNU LGPL (http://www.gnu.org/licenses/lgpl.html)

Existent en deux versions : 128x128 px et 52x52 px.

Utilisation de la police Liberation pour les extensions (http://en.wikipedia.org/wiki/Liberation_fonts).

Les sources SVG sont disponibles sur SPIP-Zone : http://zone.spip.org/trac/spip-zone/browser/_graphismes_/vignettes/oxygen