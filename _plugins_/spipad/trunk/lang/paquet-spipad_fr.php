<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'spipad_description' => 'Créer et gérer des annonces avec SPIP',
	'spipad_nom' => 'Annonces',
	'spipad_slogan' => 'Les annonces gérées par spip',
);

?>