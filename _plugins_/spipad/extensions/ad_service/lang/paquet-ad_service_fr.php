<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ad_service_description' => 'Extension de spipad pour les annonces de services à la personne',
	'ad_service_nom' => 'Annonces services',
	'ad_service_slogan' => '',
);

?>