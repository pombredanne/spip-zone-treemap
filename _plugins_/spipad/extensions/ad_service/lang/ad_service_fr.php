<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_ad_service' => 'Ajouter ce ad service',

	// E
	'explication_centre' => 'Ville au centre de votre zone d\'intervention',
	'explication_longitude' => 'longitude en valeur décimale',
	'explication_rayon' => 'A combien de kilomètres accepter vous d\'intervenir',
	'explication_titre' => 'Un titre pour votre proposition de service (important pour la recherche)',
	'explication_type_service' => 'Sélectionner votre type de service',

	// I
	'icone_creer_ad_service' => 'Créer une annonce service',
	'icone_modifier_ad_service' => 'Modifier cette annonce',
	'info_1_ad_service' => 'Une annonce service',
	'info_ad_services_auteur' => 'Les annonces services de cet auteur',
	'info_aucun_ad_service' => 'Aucune annonce service',
	'info_nb_ad_services' => '@nb@ annonce(s) service(s)',

	// L
	'label_centre' => 'Centre de la zone',
	'label_cesu' => 'CESU',
	'label_deduction_fiscale' => 'Déduction fiscale',
	'label_descriptif' => 'Descriptif',
	'label_lattitude' => 'Lattitude',
	'label_longitude' => 'Longitude',
	'label_rayon' => 'Rayon d\'intervention',
	'label_tarif_horaire' => 'Tarif Horaire',
	'label_titre' => 'Titre de l\'annonce',
	'label_type_service' => 'Type de service',

	// R
	'retirer_lien_ad_service' => 'Retirer cette annonce',
	'retirer_tous_liens_ad_services' => 'Retirer tous les annonces de service',

	// T
	'texte_ajouter_ad_service' => 'Ajouter une annonce service',
	'texte_changer_statut_ad_service' => 'Cette annonce service est :',
	'texte_creer_associer_ad_service' => 'Créer et associer une annonce service',
	'titre_ad_service' => 'Annonce service',
	'titre_ad_services' => 'Annonces services',
	'titre_ad_services_rubrique' => 'Annonce service de la rubrique',
	'titre_langue_ad_service' => 'Langue de cette annonce service',
	'titre_logo_ad_service' => 'Logo de l\'annonce',
);

?>