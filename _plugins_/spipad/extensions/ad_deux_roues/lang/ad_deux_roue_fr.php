<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_ad_deux_roue' => 'Ajouter cette annonce 2 roues',

	// I
	'icone_creer_ad_deux_roue' => 'Créer une annonce 2 roues',
	'icone_modifier_ad_deux_roue' => 'Modifier cette annonce 2 roues',
	'info_1_ad_deux_roue' => 'Une annonce 2 roues',
	'info_ad_deux_roues_auteur' => 'Les annonces 2 roues de cet auteur',
	'info_aucun_ad_deux_roue' => 'Aucune annonce 2 roues',
	'info_nb_ad_deux_roues' => '@nb@ annonces 2 roues',

	// L
	'label_descriptif' => 'Descriptif',
	'label_kilometrage' => 'Kilométrage',
	'label_marque' => 'Marque',
	'label_modele' => 'Modèle',
	'label_titre' => 'Titre',

	// R
	'retirer_lien_ad_deux_roue' => 'Retirer cette annonce 2 roues',
	'retirer_tous_liens_ad_deux_roues' => 'Retirer toutes les annonces 2 roues',

	// T
	'texte_ajouter_ad_deux_roue' => 'Ajouter une annonce 2 roues',
	'texte_changer_statut_ad_deux_roue' => 'Cette annonce 2 roues est :',
	'texte_creer_associer_ad_deux_roue' => 'Créer et associer une annonce 2 roues',
	'titre_ad_deux_roue' => 'Annonce 2 roues',
	'titre_ad_deux_roues' => 'Annonces 2 roues',
	'titre_ad_deux_roues_rubrique' => 'Annonces 2 roues de la rubrique',
	'titre_langue_ad_deux_roue' => 'Langue de cette annonce 2 roues',
	'titre_logo_ad_deux_roue' => 'Logo de cette annonce 2 roues',
);

?>