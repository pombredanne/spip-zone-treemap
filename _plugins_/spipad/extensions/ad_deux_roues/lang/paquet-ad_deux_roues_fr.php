<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ad_deux_roues_description' => 'Extension de spipad pour les annonces de 2 roues',
	'ad_deux_roues_nom' => 'SpipAd - 2roues',
	'ad_deux_roues_slogan' => '',
);

?>