<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_proposes' => 'Articles propos&eacute;s',
	'articles_publies' => 'Articles publi&eacute;s',
	'articles_refuses' => 'Articles refus&eacute;s',

	// D
	'date' => 'Date',

	// I
	'icone_menu_config' => 'Stats de publication',
	'info_page' => 'Cette page affiche le nombre d\'articles publi&eacute;s, propos&eacute;s et refus&eacute;s
	chaque jour durant les 30 derniers jours.',

	// T
	'titre_page' => 'Statistiques de publication'

);


?>
