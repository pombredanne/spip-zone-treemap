<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'element_jeu' => 'Jeu',
	'element_jeu_description' => 'Ajout d\'un jeu',

	'element_sondage' => 'Sondage',
	'element_sondage_description' => 'Ajout d\'un sondage',

	// L
	'label_id_jeu' => 'Choix du jeu',
	'label_id_sondage' => 'Choix du sondage',

);

?>
