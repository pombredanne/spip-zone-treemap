<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'celements_description' => 'Propose des éléments qui peuvent être utilisés ou dont vous
		pouvez vous inspirer !',
	'celements_nom' => 'Eléments, le catalogue',
	'celements_slogan' => 'Fournit une liste d\'éléments',
);

?>
