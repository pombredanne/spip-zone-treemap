<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/selecteur_generique/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'selecteurgenerique_description' => 'Remplace les sélecteurs (auteurs, mots, rubriques) par quelque chose de plus léger & rapide, capable de traiter autant de données que possible.',
	'selecteurgenerique_nom' => 'Sélecteur générique',
	'selecteurgenerique_slogan' => 'Amélioration des sélecteurs de l\'espace privé'
);

?>
