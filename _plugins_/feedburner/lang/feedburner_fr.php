<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// 
///  Fichier produit par PlugOnet
// Module: paquet-feedburner
// Langue: fr
// Date: 10-11-2011 11:25:42
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'legend_config' => 'Param&egrave;tres de votre flux FeedBurner (<i>Feed Details</i>)',
	'label_feedId' => 'Identifiant du flux',
	'label_url' => 'URL de redirection du flux RSS',
	'explication_url' => 'L\'adresse de votre flux feedburner est de la forme <tt>http://feeds2.feedburner.com/@feedId@</tt>. Indiquez-la ici si vous souhaitez que les visiteurs de votre RSS soient redirig&#233;s vers le flux feedburner.',
);
?>