<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// 
///  Fichier produit par PlugOnet
// Module: paquet-feedburner
// Langue: fr
// Date: 10-11-2011 11:25:42
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// F
	'feedburner_description' => 'Utilisez [FeedBurner->http://www.feedburner.com] sur votre site ! Proposez à vos internautes de {{s\'abonner par mail et/ou flux RSS}}, à leur choix, simplement, pour recevoir les derniers articles du site, proprement.',
	'feedburner_slogan' => 'Utilisez FeedBurner sur votre site',
);
?>