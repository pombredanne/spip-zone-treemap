<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-grappes
// Langue: es
// Date: 10-10-2012 16:25:01
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// G
	'grappes_description' => 'Permite agrupar elementos de SPIP
        como autores/as, secciones, artículos (cuando hay una interfaz que lo permite)...
        en un mismo universo (un racimo)',
	'grappes_nom' => 'Racimos',
);
?>