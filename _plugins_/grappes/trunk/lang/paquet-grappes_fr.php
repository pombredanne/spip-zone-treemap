<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-grappes
// Langue: fr
// Date: 10-10-2012 16:25:01
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// G
	'grappes_description' => 'Permet de regrouper des éléments de SPIP
	comme des auteurs, rubriques, articles (lorsque l\'interface existe pour)...
	dans un même univers (une grappe)',
	'grappes_nom' => 'Grappes',
	'grappes_slogan' => 'Regrouper des objets Spip dans un même univers',
);
?>