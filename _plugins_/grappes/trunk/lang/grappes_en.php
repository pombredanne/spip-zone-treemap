<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
// A
'action_inconnue' => 'Unknown action : @action@',
'autoriser_associer_non' => 'You are not authorized to perform this action.',

// B
'bouton_ajouter' => 'Add',
'bouton_chercher' => 'Search',
'bouton_checkbox_qui_administrateurs' => 'Administrators',
'bouton_checkbox_qui_redacteurs' => 'Editors',

// D
'delier' => 'Unlink',

// I
'icone_creation_grappe' => 'Create a new bunch',
'icone_modifier_grappe' => 'Modify this bunch',
'icone_supprimer_grappe' => 'Delete this bunch',
'icone_voir_toutes_grappes' => 'View all bunches',
'info_acces_0minirezo' => 'Administrators',
'info_acces_1comite' => 'Editor',
'info_changer_nom_grappe' => 'Change the name of the bunch',
'info_creation_grappes' => 'Create or modify elements bunches',
'info_grappes_association' => 'What kind of objects can be linked to the bunch?',
'info_lier_auteurs' => 'Authors',
'info_lier_articles' => 'Articles',
'info_lier_breves' => 'News items',
'info_lier_documents' => 'Documents',
'info_lier_mots' => 'Keywords',
'info_lier_grappes' => 'Bunches',
'info_lier_rubriques' => 'Sections',
'info_lier_sites' => 'Websites',
'info_lier_syndic' => 'Websites',
'info_nom_grappe' => 'Name of the bunch',
'info_qui_peut_lier' => 'Who can link objects to the bunch?',
'info_rechercher' => 'Search', // search new objects to link

'item_groupes_association_articles' => 'The articles',
'item_groupes_association_auteurs' => 'The authors',
'item_groupes_association_breves' => 'The news items',
'item_groupes_association_documents' => 'The documents',
'item_groupes_association_grappes' => 'The bunches',
'item_groupes_association_groupes_mots' => 'The keyword groups',
'item_groupes_association_mots' => 'The keywords',
'item_groupes_association_rubriques' => 'The sections',
'item_groupes_association_syndic' => 'The websites',

// L
'lier' => 'Link',

// P
'pas_de_identifiant' => 'Name unknown',

// T
'texte_descriptif' => 'Description',

'titre_grappe' => 'Bunch',
'titre_nouvelle_grappe' => 'New bunch',
'titre_page_grappes' => 'Bunches',

// V
'voir' => 'View',
);

?>
