<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-grappes
// Langue: en
// Date: 10-10-2012 16:25:01
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// G
	'grappes_description' => 'Allows to group together elements of SPIP
        such as authors, sections, articles (when a dedicated interface exists)...
        in a same universe (just like a bunch of grapes)',
	'grappes_nom' => 'Bunches of grapes',
);
?>