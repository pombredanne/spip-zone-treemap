<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
// A
'action_inconnue' => 'Action "@action@" inconnue',
'autoriser_associer_non' => 'Vous n\'êtes pas autorisé à effectuer cette action.',

// B
'bouton_ajouter' => 'Ajouter',
'bouton_checkbox_qui_administrateurs' => 'Les administrateurs',
'bouton_checkbox_qui_id_admin' => 'Le créateur de la grappe (Vous?)',
'bouton_checkbox_qui_redacteurs' => 'Les rédacteurs',
'bouton_chercher' => 'Chercher',

// D
'delier' => 'Délier',
'delier_tout' => 'Tout délier',

// I
'icone_creation_grappe' => 'Créer une nouvelle grappe',
'icone_modifier_grappe' => 'Modifier cette grappe',
'icone_supprimer_grappe' => 'Supprimer cette grappe',
'icone_voir_toutes_grappes' => 'Voir toutes les grappes',
'info_1_grappe' => '1 grappe',
'info_aucune_grappe' => 'Aucune grappe',
'info_changer_nom_grappe' => 'Changer le nom de la grappe',
'info_creation_grappes' => 'Créer ou modifier des grappes d\'éléments',
'info_grappes_association' => 'Quels objets peut on lier à la grappe ?',
'info_grappes_miennes' => 'Mes grappes',
'info_grappes_toutes' => 'Toutes les grappes',
'info_nb_grappes' => '@nb@ grappes',
'info_nom_grappe' => 'Nom de la grappe',
'info_qui_peut_lier' => 'Qui peut lier les objets à la grappe ?',
'info_rechercher' => 'Chercher', // chercher de nouveaux objets a lier

'item_groupes_association_articles' => 'Les articles',
'item_groupes_association_auteurs' => 'Les auteurs',
'item_groupes_association_breves' => 'Les brèves',
'item_groupes_association_documents' => 'Les documents',
'item_groupes_association_grappes' => 'Les grappes',
'item_groupes_association_groupes_mots' => 'Les groupes de mots',
'item_groupes_association_mots' => 'Les mots',
'item_groupes_association_rubriques' => 'Les rubriques',
'item_groupes_association_syndic' => 'Les sites',

// L
'label_acces' => 'Accès',
'label_liaisons' => 'Objets',
'label_type' => 'Type',
'lier' => 'Lier',

// P
'pas_de_identifiant' => 'Nom inconnu',

// T
'texte_descriptif' => 'Descriptif',

'titre_grappe' => 'Grappe',
'titre_grappes' => 'Grappes',
'titre_logo_grappe' => 'Logo de la grappe',
'titre_nouvelle_grappe' => 'Nouvelle Grappe',
'titre_page_grappes' => 'Grappes',

// V
'voir' => 'Voir',
);

?>
