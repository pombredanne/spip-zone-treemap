<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-grappes
// Langue: de
// Date: 10-10-2012 16:25:01
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// G
	'grappes_description' => 'Neue Objekte (Trauben) künnen aus SPIP-Objekten zusammengestellt werden,
		wenn diese ein Interface haben.',
	'grappes_nom' => 'Trauben',
);
?>