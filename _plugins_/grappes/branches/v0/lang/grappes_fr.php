<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
// A
'action_inconnue' => 'Action "@action@" inconnue',
'autoriser_associer_non' => 'Vous n\'&ecirc;tes pas autoris&eacute; &agrave; effectuer cette action.',

// B
'bouton_ajouter' => 'Ajouter',
'bouton_chercher' => 'Chercher',
'bouton_checkbox_qui_administrateurs' => 'Les administrateurs',
'bouton_checkbox_qui_id_admin' => 'Le cr&eacute;ateur de la grappe (Vous?)',
'bouton_checkbox_qui_redacteurs' => 'Les r&eacute;dacteurs',

// D
'delier' => 'D&eacute;lier',

// I
'icone_creation_grappe' => 'Cr&eacute;er une nouvelle grappe',
'icone_modif_grappe' => 'Modifier cette grappe',
'icone_supprimer_grappe' => 'Supprimer cette grappe',
'icone_voir_toutes_grappes' => 'Voir toutes les grappes',
'info_acces_0minirezo' => 'Administrateurs',
'info_acces_1comite' => 'R&eacute;dacteurs',
'info_changer_nom_grappe' => 'Changer le nom de la grappe',
'info_creation_grappes' => 'Cr&eacute;er ou modifier des grappes d\'&eacute;l&eacute;ments',
'info_grappes_association' => 'Quels objets peut on lier à la grappe ?',
'info_lier_auteurs' => 'Auteurs',
'info_lier_articles' => 'Articles',
'info_lier_breves' => 'Breves',
'info_lier_documents' => 'Documents',
'info_lier_mots' => 'Mots',
'info_lier_grappes' => 'Grappes',
'info_lier_rubriques' => 'Rubriques',
'info_lier_sites' => 'Sites',
'info_lier_syndic' => 'Sites',
'info_nom_grappe' => 'Nom de la grappe',
'info_qui_peut_lier' => 'Qui peut lier les objets à la grappe ?',
'info_rechercher' => 'Chercher', // chercher de nouveaux objets a lier

'item_groupes_association_articles' => 'Les articles',
'item_groupes_association_auteurs' => 'Les auteurs',
'item_groupes_association_breves' => 'Les br&egrave;ves',
'item_groupes_association_documents' => 'Les documents',
'item_groupes_association_grappes' => 'Les grappes',
'item_groupes_association_groupes_mots' => 'Les groupes de mots',
'item_groupes_association_mots' => 'Les mots',
'item_groupes_association_rubriques' => 'Les rubriques',
'item_groupes_association_syndic' => 'Les sites',

// L
'lier' => 'Lier',

// P
'pas_de_identifiant' => 'Nom inconnu',

// T
'texte_descriptif' => 'Descriptif',

'titre_grappe' => 'Grappe',
'titre_nouvelle_grappe' => 'Nouvelle Grappe',
'titre_page_grappes' => 'Grappes',

// V
'voir' => 'Voir',
);

?>
