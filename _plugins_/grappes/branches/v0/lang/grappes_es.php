<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
// A
'action_inconnue' => 'Acci&oacute;n "@action@" desconocida',
'autoriser_associer_non' => 'No tiene autorisaci&oacute;n para efectuar esta acci&oacute;n.',

// B
'bouton_ajouter' => 'A&ntilde;adir',
'bouton_chercher' => 'Buscar',
'bouton_checkbox_qui_administrateurs' => 'Administradores y administradoras del sitio',
'bouton_checkbox_qui_redacteurs' => 'Redactoras y redactores',

// D
'delier' => 'Disociar',

// I
'icone_creation_grappe' => 'Crear un nuevo racimo',
'icone_modif_grappe' => 'Modificar este racimo',
'icone_supprimer_grappe' => 'Suprimir este racimo',
'icone_voir_toutes_grappes' => 'Ver todos los racimos',
'info_acces_0minirezo' => 'Administradores/as',
'info_acces_1comite' => 'Redactores/as',
'info_changer_nom_grappe' => 'Cambiar el nombre del racimo',
'info_creation_grappes' => 'Crear o modificar racimos de elementos',
'info_grappes_association' => '&iquest;Cu&acute;les objetos pueden ser asociados al racimo?',
'info_lier_auteurs' => 'Autores/as',
'info_lier_articles' => 'Art&iacute;culos',
'info_lier_breves' => 'Notas breves',
'info_lier_documents' => 'Documentos',
'info_lier_mots' => 'Palabras',
'info_lier_grappes' => 'Racimos',
'info_lier_rubriques' => 'Secciones',
'info_lier_sites' => 'Sitios',
'info_lier_syndic' => 'Sitios',
'info_nom_grappe' => 'Nombre del racimo',
'info_qui_peut_lier' => '&iquest;Qui&eacute;n puede asociar objetos al racimo?',
'info_rechercher' => 'Buscar', // buscar nuevos objetos para asociar

'item_groupes_association_articles' => 'Los art&iacute;culos',
'item_groupes_association_auteurs' => 'Las autoras y autores',
'item_groupes_association_breves' => 'Las notas breves',
'item_groupes_association_documents' => 'Los documentos',
'item_groupes_association_grappes' => 'Los racimos',
'item_groupes_association_groupes_mots' => 'Los grupos de palabras',
'item_groupes_association_mots' => 'Las palabras',
'item_groupes_association_rubriques' => 'Las secciones',
'item_groupes_association_syndic' => 'Los sitios',

// L
'lier' => 'Asociar',

// P
'pas_de_identifiant' => 'Nombre desconocido',

// T
'texte_descriptif' => 'Descripci&oacute;n',

'titre_grappe' => 'Racimo',
'titre_nouvelle_grappe' => 'Nuevo racimo',
'titre_page_grappes' => 'Racimos',

// V
'voir' => 'Ver',
);

?>
