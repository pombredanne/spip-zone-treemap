<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
// A
'action_inconnue' => 'Aktion "@action@" ist unbekannt',
'autoriser_associer_non' => 'Die Aktion d&uuml;fen sie nicht ausf&uuml;hren.',

// B
'bouton_ajouter' => 'Hinzuf&uuml;gen',
'bouton_chercher' => 'Suchen',
'bouton_checkbox_qui_administrateurs' => 'Administratoren',
'bouton_checkbox_qui_redacteurs' => 'Redakteure',

// D
'delier' => 'trennen',

// I
'icone_creation_grappe' => 'Neue Traube anlegen',
'icone_modif_grappe' => 'Traube bearbeiten',
'icone_supprimer_grappe' => 'Traube l&ouml;schen',
'icone_voir_toutes_grappes' => 'Alle Trauben zeigen',
'info_acces_0minirezo' => 'Administratoren',
'info_acces_1comite' => 'Redakteure',
'info_changer_nom_grappe' => 'Name der Traube &auml;ndern',
'info_creation_grappes' => 'Trauben aus Elementen zusammenstellen oder bearbeiten',
'info_grappes_association' => 'Welche Objekte kann man in die Traube einf&uuml;gen?',
'info_lier_auteurs' => 'Autoren',
'info_lier_articles' => 'Artikel',
'info_lier_breves' => 'Meldungen',
'info_lier_documents' => 'Dokumente',
'info_lier_mots' => 'Schlagworte',
'info_lier_grappes' => 'Trauben',
'info_lier_rubriques' => 'Rubriken',
'info_lier_sites' => 'Websites',
'info_lier_syndic' => 'Websites',
'info_nom_grappe' => 'Name der Taube',
'info_qui_peut_lier' => 'Wer kann Elemente zur Taube hinzuf&uuml;gen?',
'info_rechercher' => 'Suchen', // neue Elemente für eine Traube

'item_groupes_association_articles' => 'Artikel',
'item_groupes_association_auteurs' => 'Autoren',
'item_groupes_association_breves' => 'Meldungen',
'item_groupes_association_documents' => 'Dokumente',
'item_groupes_association_grappes' => 'Trauben',
'item_groupes_association_groupes_mots' => 'Schlagwortgruppen',
'item_groupes_association_mots' => 'Schlagworte',
'item_groupes_association_rubriques' => 'Rubriken',
'item_groupes_association_syndic' => 'Websites',

// L
'lier' => 'Verbinden',

// P
'pas_de_identifiant' => 'Name unbekannt',

// T
'texte_descriptif' => 'Beschreibung',

'titre_grappe' => 'Trauben',
'titre_nouvelle_grappe' => 'Neue Traube',
'titre_page_grappes' => 'Trauben',

// V
'voir' => 'Anzeigen',
);

?>
