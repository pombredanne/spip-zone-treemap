<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// B
  'bouton_reset_video' => 'Effacer cette vid&eacute;o et recommencer',
  'bouton_ajouter_video' => 'Enregistrer cette vid&eacute;o',
  'bouton_nouvelle_video' => 'Commencer une nouvelle vid&eacute;o',

	// N
  'nouvelle_video' => 'Nouvelle vid&eacute;o',

  // P
  'params_connect'=>'Param&egrave;tres de connexion Kaltura',
  'partner_id' => 'ID partenaire',
  'propulse_par' => 'Une vid&eacute;o collaborative de @nom@ propuls&eacute;e par <a href=\'http://www.kaltura.com\'>Kaltura</a>',
  
  // S
  'secret'=> 'Secret',
  'subp_id' => 'ID sous-partenaire',
  
  // V
  'video_de_nom' => 'Vid&eacute;o de @nom@',
);


?>
