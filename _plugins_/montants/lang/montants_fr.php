<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
'configurer_montants'=>'Configurer des montants fixes',
'action_creer_nouveau_montant'=>'Créer un nouveau montant',
'action_editer'=>'Editer',
'action_supprimer'=>'Supprimer',
'montants_titre'=>'Montants',
'configurer_montant' => 'Configurer le prix des objets (articles) par defaut',
'label_objet'=>'Objet',
'label_prix_ht'=>'Prix HT',
'label_prix'=>'Prix',
'label_ids_objet'=>'Liste des identifiants (optionnel)',
'label_le_parent'=>'Identifiant du parent (optionnel)',
'label_taxe'=>'Taxe',
'label_descriptif'=>'Commentaire',
'label_parent'=>'Parent',
'explication_ids_objet' =>'Identifiants de l\'objet qui ont ce montant -séparés par une virgule-',
'explication_le_parent'=>'Numéro identifiant du premier parent de la branche',
'liste_des_montants'=>'Liste des montants',
'editer_montant' => 'Modifiez le montant',
'creer_montant' => 'Créer un nouveau montant',
'prix_defaut'=>'Prix par défaut :',
'prix'=>'Prix :',
'modifier_prix_defaut'=>'Modifier le prix par défaut',
'bouton_supprimer_montants_confirmer' => 'Etes-vous sûr de vouloir supprimer le montant ?',
'prix_par_defaut'=>'Soit un prix spécifique soit 0 pour utiliser le prix par défaut. <br />Le prix actuel est de <strong>@prix@</strong>',
'page_configurer_affichage'=>'Configurer l\'affichage des montants',
'explication_secteurs'=>'Choisir le ou les secteurs pour lesquels rajouter ce champ supplémentaire. Ne rien sélectionner pour que le champ soit rajouté à tous les articles et rubriques du site.',
'label_secteurs'=>'Secteurs concernés',


);

?>
