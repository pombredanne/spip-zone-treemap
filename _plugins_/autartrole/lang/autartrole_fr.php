<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION'))
	return;
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'confirmer_suppression_role' => "&Ecirc;tes-vous certain(e) de ne plus vouloir indiquer le r&ocirc;le de cet(te) auteur(e) dans cet article ?",

	// E
	'editer_role' => "&Eacute;dition du r&ocirc;le",
	'effacer_role' => "Effacer le r&ocirc;le de cette personne pour cet article",

	// L
	'label_article' => "&OElig;vre : ",
	'label_auteur' => "avec la participation de : ",
	'label_rang' => "affichage en position : ",
	'label_role' => "en qualit&eacute; de : ",
	'liste_roles' => "Auteur(e) et r&ocirc;le",
	'liste_roles_article' => "Auteur(e)s et r&ocirc;les de l'article",
	'liste_roles_auteur' => "Articles et r&ocirc;le de l'auteur(e)",

	// M
	'message_aucun_role' => "Aucun r&ocirc;le...",
	'message_aucun_role_article' => "Aucun r&ocirc;le indiqu&eacute; dans cet article.",
	'message_aucun_role_auteur' => "Aucun r&ocirc;le indiqu&eacute; pour cette personne.",
	'message_erreur_article' => "Article inexistant :-S",
	'message_erreur_auteur' => "Auteur(e) inexistant(e) :-S",
	'message_erreur_lien' => "Couple auteur(e)/article non effectif :-o",
	'message_succes_changement' => "Modification(s) effectu&eacute;e(s)",
	'modifier_role' => "Modifier le r&ocirc;le de cet(te) auteur(e) pour cet article",

	// T
	'titre_bloc_article' => "D&eacute;tail des participations",
	'titre_bloc_auteur' => "Liste de ses participations",

);

?>