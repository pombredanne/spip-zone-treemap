<?php
/**
 * @name 		Francais
 * @author 		Piero Wbmstr <@link piero.wbmstr@gmail.com>
 * @license		http://creativecommons.org/licenses/by-nc-sa/3.0/ Creative Commons BY-NC-SA
 * @version 	1.0 (06/2009)
 * @package		Pub Banner
 * @subpackage	Languages
 */

$GLOBALS['i18n_pubban_fr'] = array(
	
// Liens generaux
	'pubban' => 'Pub Banner',
	'pubban_titre' => 'Banni&#232;res publicitaires',
	'gestion_pubban' => 'Gestion de banni&egrave;res publicitaires',
 	'home' => 'Retour au gestionnaire de pub',
	'see_doc' => 'Voir la documentation (en interne)',
	'texte_brut' => 'Texte brut',
	'new_window' => 'Nouvelle fen&ecirc;tre',
	'see_doc_in_texte_brut' => 'Voir la doc en texte brut (probl&egrave;mes de squelette)',
	'see_doc_in_new_window' => 'Ouvrir la doc dans une nouvelle fen&ecirc;tre',
	'download_flash_player' => 'La visualisation de cet objet n&eacute;cessite le logiciel Adobe Flash Player. Cliquez ici pour l\'obtenir gratuitement.',

// Statuts
	'statut' => 'Statut',
	'actif' => 'Activ&eacute;',
	'inactif' => 'Inactiv&eacute;',
	'poubelle' => '&Agrave; la poubelle',
	'obsolete' => 'Obsol&egrave;te',

// Textes generaux
	'type' => 'Type',
	'titre' => 'Intitul&eacute;',
	'date_add' => 'Cr&eacute;ation',
	'outils' => 'Outils',
	'type_encart' => 'Encart',
	'type_empl' => 'Emplacement',
	'pub' => 'publicit&eacute;(s)',
	'no_limit' => 'illimit&eacute;',
	'doc_info' => 'Nous vous renvoyons &agrave; la doc du plugin pour plus d\'informations :',
	'select_articles_choose' => '&gt; liste de vos articles',
	'width' => 'Largeur',
	'height' => 'Hauteur',
	'trash_is_empty' => 'La poubelle est vide',
	'error_global' => 'Il semble qu\'une erreur soit survenue ...',

// Boutons
	'btn_modifier' => 'Modifier',
	'btn_voir' => 'Visualiser',
	'btn_apercu' => 'Aper&ccedil;u',
	'btn_details' => 'D&eacute;tails',
	'btn_editer' => 'Editer',
	'btn_supprimer' => 'Supprimer',
	'btn_active' => 'Activer',
	'btn_desactive' => 'D&eacute;sactiver',
	'btn_imprimer' => 'Imprimer',
	'btn_supprimer' => 'Supprimer',
	'btn_inverser' => 'Inverser la liste',
	'btn_reabiliter' => 'R&eacute;cup&eacute;rer',
	'btn_lister_empl' => 'Lister les publicit&eacute;s de cette banni&egrave;re',
	'btn_see_liste' => 'Voir la liste',

// Page accueil
	'intro_admin' => 'Gestion des <i>Banni&egrave;res Publicitaires</i>',
	'texte_admin' => 'Les publicit&eacute;s s\'organisent en diff&eacute;rentes <b>banni&egrave;res</b>, l\'objet affich&eacute; dans les squelettes, comportant chacune un pannel de publicit&eacute;s, les <b>encarts</b>, qui s\'affichent alternativement &agrave; chaque appel de banni&egrave;re.<br /><br />Le plugin enregistre les nombres d\'affichages et de clics sur chaque encart, permettant de pr&eacute;senter des <b>statistiques d&eacute;taill&eacute;es</b>, utiles pour estimer le rendement des banni&egrave;res.',
	'info_stats' => 'Quelques chiffres ...',
	'nb_emplacements' => 'Nombre d\'emplacements',
	'nb_pub' => 'Nombre total de publicit&eacute;s', 
	'nb_pub_actives' => 'Dont actives', 
	'nb_pub_inactives' => 'Dont inactives', 
	'nb_pub_obsoletes' => 'Dont obsol&egrave;tes', 
	'nb_affichages' => 'Nombre total d\'affichages', 
	'nb_clics' => 'Nombre total de clics', 
	'info_ratio' => 'Ratio (clics/affichages)', 

// Page recherche
	'info_search_box' => '<i>Rechercher</i> > saisissez une r&eacute;f&eacute;rence, un mot ou groupe de mots &agrave; rechercher',
	'result_match' => 'entr&eacute;e correspond &agrave; votre recherche.',
	'results_match' => 'entr&eacute;es correspondent &agrave; votre recherche.',
	'no_results_match' => 'Aucune entr&eacute;e ne correspond &agrave; votre recherche.',
	'search_results' => 'R&eacute;sultats de votre recherche',
	'retour_search' => 'Retour &agrave; la recherche',
	'search_pubban' => 'Rechercher dans les banni&egrave;res',

// Page infos
	'page_infos' => 'Conseils et informations',
	'infos_pubban' => 'Informations et conseils ...',
	'infos_texte' => 
"{{Le rendement d'un espace publicitaire d&eacute;pend principalement de deux composantes :
-* son format,
-* sa position sur la page.}}

{{{Format}}}

G&eacute;n&eacute;ralement, les formats de publicit&eacute;s plus larges que hauts obtiennent un rendement sup&eacute;rieur gr&acirc;ce &agrave; leur grande convivialit&eacute;.
Les informations y sont assimil&eacute;es plus facilement car le lecteur peut lire davantage de texte sans avoir &agrave; changer de ligne.

{{{Positionnement}}}

Les nombreuses &eacute;tudes statistiques montrent que les emplacements publicitaires pr&eacute;sents en haut d'une page web, haut de page et haut de contenu, ont de meilleures performances.

{{Les standards publicitaires apportent les cl&eacute;s pour proposer des banni&egrave;res ou construire des publicit&eacute;s.}}

{{{Tailles classiques des banni&egrave;res publicitaires & poids maximum}}}

-* {{la banni&egrave;re}} : 468x60 px | 35 Ko
-* {{le skyscraper}} : 120x600 px | 50 Ko
-* {{le pav&eacute;}} : 300x250 px | 50 Ko
-* {{le carr&eacute;}} : 250x250 px | 50 Ko
-* {{le bouton}} (logos ...) : jusqu'&agrave; 120 px (120x60 px)

{{{Conseils}}}

-* Les fichiers propos&eacute;s pour les publicit&eacute;s doivent faire moins de 50 Ko, pour ne pas gêner le chargement du contenu de la page.
-* Pour les animations, il est pr&eacute;f&eacute;rable de recommander des images de 15 secondes maximum.

{{{Les tarifs}}}
{{Deux principales m&eacute;thodes de tarification :}}

-* {{CPM - Le co&ucirc;t pour mille}} ({affichages}) est la m&eacute;thode la plus utilis&eacute;e,  elle semble être devenue un standard en la mati&egrave;re.
-* {{CPC - Le co&ucirc;t au clic}}, juste derri&egrave;re, est plus complexe &agrave; chiffrer.

{{Le forfait}} est &eacute;galement utilis&eacute; pour les campagnes hors normes : incrustation dans les pages, objet publicitaire intrusif, campagne active ...
",

// Page statistiques
	'page_stats' => 'Page de statistiques',
	'stats_pubban' => 'Statistiques des publicit&eacute;s',
	'statistiques_pubban' => 'Statistiques des banni&egrave;res publicitaires',
	'intro_stats' => 'Lecture des statistiques',
	'intro_texte_stats' => 'Les chiffres des statistiques permettent d\'estimer l\'efficacit&eacute; des encarts publicitaires : en particulier, <b>le ratio global</b> indique le nombre de clic par rapport au nombre d\'affichage des encarts.<br /><br />Il est int&eacute;ressant de comparer les ratios des banni&egrave;res en fonction de leur positionnement notamment.<br /><br /><i>(cf. LICENSES en bas de page)</i><br />',
	'voir_page' => '<br /><b>Voir la page :',
	'licence_stats' => '<i><b>LICENCES :</b><br /><b>\'wz_jsgraphics.js\'</b> :: v. 2.33 - (c) 2002-2004 Walter Zorn [<a href="http://www.walterzorn.com" target="_blank">www.walterzorn.com</a>]<br /><b>\'graph.js\', \'line.js\' & \'pie.js\'</b> :: (c) Balamurugan S. 2005 [<a href="http://www.jexp.com" target="_blank">www.jexp.com</a>]</i>',
	'no_datas_yet' => 'Il n\'y a pas encore de donnée statistique exploitable ...',
	'info_evo' => '10 blocs * 10 jours (100 derniers jours)',
	'no_clic_in_period' => 'Il n\'y a eu aucun clic dans la période choisie.',
	'evo_empl' => '&Eacute;volution des performances',
	'perf_empl' => 'Performance des banni&egrave;res',
	'clics' => 'Clics',
	'clics_txt' => 'clic(s)',
	'aujourdhui' => 'Aujourd\'hui',
	'affi_txt' => 'affichage(s)',
	'ratio' => 'Ratio (clics/affichages)',
	'derniers_jours' => 'derniers jours',
	'ratio_txt' => 'Ratio', 
	'no_clic_for_emp' => 'Certains emplacements ne sont pas représentés dans le graphique car il n\'ont eu aucun clic dans la période choisie.',
	'period' => 'P&eacute;riode du ',
	'au' => ' au ',
	'comment_ratio' => '(nombre de clics / nombre d&prime;affichages)',

// Page liste pub
	'liste_pub' => 'Liste des publiclit&eacute;s',
	'intro_pub' => 'Encarts publicitaires',
	'intro_pub_texte' => 'Voici la liste des publicit&eacute;s inscrites sur le site.<br /><br />Vous pouvez ici les <b>activer</b> ou les <b>d&eacute;sactiver</b>, les <i>jeter &agrave; la poubelle</i>, ainsi que les <b>modifier</b> ou <b>obtenir un aper&ccedil;u</b> ...<br /><br />Les publicit&eacute;s <i>obsol&egrave;tes</i> ont une validit&eacute; d&eacute;pass&eacute;e : nombre de clics ou d\'affichages atteints, dates r&eacute;volues.',
	'pub_actives' => 'Liste des publicit&eacute;s actives',
	'pub_inactives' => 'Liste des publicit&eacute;s inactives',
	'pub_obsoletes' => 'Liste des publicit&eacute;s obsol&egrave;tes',
	'no_pub_yet' => 'Il n\'y a pas encore de publicit&eacute; enregistr&eacute;e ...',
	'no_pub_active_yet' => 'Il n\'y a pas encore de publicit&eacute; activ&eacute;e ...',

// Page pub details
	'view_pub' => 'D&eacute;tails d\'un encart publicitaire',
	'titre_info_pub' => 'PUBLICIT&Eacute; NUM&Eacute;RO :',
	'pub_is' => 'Cette publicit&eacute; est',
	'retour_liste_pub' => 'Retour &agrave; la liste compl&egrave;te des publicit&eacute;s',
	'no_pub_found' => 'Publicit&eacute; introuvable ...',
	'fiche' => 'Fiche',
	'apercu' => 'Aper&ccedil;u',
	'stats' => 'Donn&eacute;es statistiques',
	'date_pub' => 'Date d\'enregistrement',
	'emplacement_pub' => 'Emplacement',
	'titre_tablo_url' => 'URL de redirection (au clic)',
	'nb_affires_pub' => 'Nombre d\'affichages restant',
	'nb_clicres_pub' => 'Nombre de clics restant',
	'voir_un_apercu' => 'Voir un aper&ccedil;u (fen&ecirc;tre popup)',
	'dates_validite_pub' => 'Dates de validit&eacute;',
	'date_debut' => 'Date de d&eacute;but de validit&eacute;',
	'date_fin' => 'Date de fin de validit&eacute;',
	'url_pub' => 'URL de redirection (au clic)',
	'code_pub' => 'Code ou adresse de l\'objet &agrave; afficher',
	'confirm_delete' => 'Attention : vous avez demand&eacute; &agrave; mettre un encart publicitaire &agrave; la poubelle ...\n\nCliquez sur OK pour confirmer :',

// Page pub edition
	'nouveau_pub' => 'Cr&eacute;er une nouvelle publicit&eacute;',
	'nouveau_pub_dans_emplacement' => 'Ajouter une nouvelle publicit&eacute;',
	'pub_edit' => '&Eacute;dition d\'un encart publicitaire',
	'intro_pub_edit' => '&Eacute;dition d\'encarts publicitaires',
	'intro_pub_edit_texte' => 'Cette page vous permet d\'ins&eacute;rer ou de modifier une publicit&eacute; selon des conditions particuli&egrave;res&nbsp;:<ul><li>pour <b>un nombre de clics</b> pr&eacute;cis,</li><li>pour un <b>nombre d\'affichages</b> d&eacute;fini,</li><li>selon des <b>dates de validit&eacute; pr&eacute;cises</b>.</li></ul>',
	
	'titre_cadre_ajouter_pub' => 'Cr&eacute;ation d\'une nouvelle publicit&eacute;',
	'titre_cadre_modifier_pub' => '&Eacute;dition d\'une publicit&eacute;',
	'infos_pub' => 'Contenu de la publicit&eacute;',
	'titre_tablo_nom' => 'Titre de la publicit&eacute;',
	'titre_tablo_emplacement' => 'Emplacement',
	'comment_multiple_empl' => 'Vous pouvez s&eacute;lectionner plusieurs emplacements en utilisant la touche \'MAJ.\' de votre clavier.',
	'titre_tablo_code' => 'Code html de l\'objet &agrave; afficher',
	'comment_code_pub' => '<i>Pour une publicit&eacute; de type \'image\' ou \'swf\', vous ne devez indiquer ici que l\'adresse url de cette image. Pour un objet flash, vous devez en indiquer le code complet ...</i>',
	'titre_tablo_date' => 'Date d\'ajout',
	'comment_url_optionnel' => 'Vous pouvez laisser ce champ vide, un clic sur la publicit&eacute; renverra alors sur la page d\'achat des encarts publicitaires.',
	'droits' => 'Droits ouverts sur la pub',
	'illimite' => 'Droits illimit&eacute;s',
	'comment_illimite' => '<i>Affichages et clics illimit&eacute;s ; vous pouvez pr&eacute;ciser une date de d&eacute;but ou de fin de p&eacute;riode d\'affichage.</i>',
	'droits_aff_pub' => 'Nombre d\'affichages',
	'droits_clic_pub' => 'Nombre de clics',
	'droits_dates_pub' => 'Dates',
	'debut' => 'D&eacute;but',
	'fin' => 'Fin',
	'comment_dates' => 'Notez les dates sous la forme \'AAAA-MM-JJ\'',
	'type_img' => 'Objet de type image',
	'type_swf' => 'Objet flash .swf',
	'type_flash' => 'Objet flash autre',
	'erreur_titre' => 'Vous devez indiquer un titre pour votre publicit&eacute; (<i>il apparaitra au passage de la souris</i>)',
	'erreur_url' => 'Vous devez indiquer une adresse URL de redirection de la publicit&eacute;',
	'erreur_code' => 'Veuillez saisir le code de la publicit&eacute;',
	'erreur_empl' => 'Vous n\'avez pas choisi d\'emplacement pour votre publicit&eacute; ...',
	'erreur_url_not_url' => 'L\'adresse saisie ne semble pas &ecirc;tre une adresse web ...',
	'erreur_url_no_response' => 'L\'adresse saisie ne r&eacute;pond pas ... &ecirc;tes vous s&ucirc;r qu\'elle soit valide ?',
	'erreur_img_not_img' => 'L\'url saisie ne semble pas correspondre &agrave; une image ...',
	'erreur_img_not_url' => 'L\'adresse web saisie est inaccessible ...',
	'erreur_nb_aff' => 'Vous n\'avez pas pr&eacute;cis&eacute; de nombre d\'affichage ...',
	'pas_emplacement_selectionne' => 'Vous n\'avez pas s&eacute;lectionn&eacute; d\'emplacement ...',
	'reponse_form_def_droits' => 'Veuillez saisir des droits pour le publicit&eacute; (une seule ligne)',
	'manque_date_fin' => 'Veuillez pr&eacute;ciser une date de fin',
	'edit_pub_ok_emplacements_differents' => 'OK - Valeurs enregistr&eacute;es mais les emplacements s&eacute;lectionn&eacute;s pour la publicit&eacute; ont des tailles diff&eacute;rentes ... Cela pourra g&eacute;n&eacute;rer des erreurs d\'affichage.',

// Page liste emplacement
	'list_empl' => 'Liste des banni&egrave;res',
	'intro_integer' => 'Les <i>banni&egrave;res</i> : emplacements publicitaires',
	'intro_integer_texte' => 'Voici la liste des banni&egrave;res inscrites sur le site.<br />Les banni&egrave;res se caract&eacute;risent principalement par leurs dimensions et leur positionnement dans vos squelettes.<br /><br />Vous pouvez ici les <b>activer</b> ou les <b>d&eacute;sactiver</b>, les <i>jeter &agrave; la poubelle</i>, ainsi que les <b>modifier</b> ...<br />',
	'confirm_delete_empl' => 'Attention : vous avez demand&eacute; &agrave; mettre un emplacement publicitaire &agrave; la poubelle ...\n\nCliquez sur OK pour confirmer :',
	'no_empl_yet' => 'Il n\'y a pas encore de banni&egrave;re configur&eacute;e ...',

// Page details emplacement
	'empl_is' => 'Cet emplacement est',
	'titre_nouvel_empl' => 'NOUVELLE BANNI&Egrave;RE',
	'titre_info_empl' => 'BANNI&Egrave;RE NUM&Eacute;RO :',
	'retour_liste_empl' => 'Retour &agrave; la liste des banni&egrave;res',
	'details_empl' => 'D&eacute;tails d\'une banni&egrave;re',
	'dimensions' => 'Dimensions',
	'ratio_pages' => 'Ratio de pages (visibilit&eacute;)',
	'no_empl_found' => 'Banni&egrave;re introuvable ...',
	'listing_empl' => 'Liste des publicit&eacute;s de cette banni&egrave;re',

// Page edit emplacement
	'nouveau_empl' => 'Cr&eacute;er une nouvelle banni&egrave;re',
	'integer_edit' => '&Eacute;dition d\'une banni&egrave;re',
	'intro_integer_edit' => '&Eacute;dition de banni&egrave;res',
	'intro_integer_edit_texte' => 'Pour appeler une banni&egrave;re dans vos squelettes, indiquez la balise&nbsp;: <br /><center><b># PUBBAN{nom_banniere}</b></center><br />Les espaces du titre r&eacute;el sont remplac&eacute;s par des <b>underscore</b> dans l\'appel &agrave; la balise.<br /><br /><i>Nous attirons votre attention sur le commentaire concernant le titre des banni&egrave;res : <u>&eacute;vitez les caract&egrave;res sp&eacute;ciaux</u>! Si vous souhaitez les utiliser, faites de nombreux tests avant mise en ligne ...</i>',
	'titre_cadre_modifier_empl' => 'Modification d\'une banni&egrave;re',
	'titre_cadre_ajouter_empl' => 'Cr&eacute;ation d\'une banni&egrave;re',
	'info_titre_banniere' => 'Titre de l\'emplacement',
	'info_titre_comment' => '<i>Il est fortement d&eacute;conseill&eacute; d\'utiliser des caract&egrave;res accentu&eacute;s ou sp&eacute;ciaux dans les titres de banni&egrave;res, cela pourrait provoquer une erreur lors de l\'appel de la balise PUBBAN ...</i>',
	'info_taille_banniere' => 'Dimensions de la banni&egrave;re',
	'en_pixels' => '<i>(en pixels)</i>',
	'info_emplacement' => 'Statut de l\'emplacmenet',
	'info_ratio_banniere' => 'Ratio de la banni&egrave;re (<i>optionnel</i>)',
	'en_pourcent' => '<i>(en %)</i>',
	'ratio_comment' => 'Quotient pages pr&eacute;sentant la banni&egrave;re / pages totales.',
	'error_titre_empl' => 'Vous devez indiquer un titre pour votre banni&egrave;re',
	'error_dimensions_missing_empl' => 'Vous devez d&eacute;finir des dimensions pour votre banni&egrave;re',
	'error_dimensions_numeric_empl' => 'Il semble qu\'il y ait eu une erreur de dimensions',

// Page poubelle
	'open_trash' => 'Ouvrir la poubelle',
	'content_trash' => 'Contenu de la poubelle',
	'vider_trash' => 'Vider la poubelle',
	'confirm_vider_poubelle' => '&Ecirc;tes-vous s&ucirc;r de vouloir vider la poubelle ?',
	'confirm_undelete' => 'Attention : vous avez demand&eacute; la r&eacute;habilitation d un encart publicitaire ...\n\nCliquez sur OK pour confirmer.',

// Boite info
	'plugin_spip' => 'un plugin pour <b>SPIP 2.0+</b>',
	'auteur' => 'Auteur',
	'documentation_info' => 'Documentation/Information',
	'url_update' => 'URL de t&eacute;l&eacute;chargement',
);
?>