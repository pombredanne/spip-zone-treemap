﻿<?php

	// lang/menudep_fr.php
	
	// $LastChangedRevision$
	// $LastChangedBy$
	// $LastChangedDate$

$GLOBALS['i18n_'._MENUDEP_PREFIX.'_fr'] = array(

	'menudep_nom' => "Menu dépliant"
	
,  'configuration_menu_depliant' => "Configuration du menu dépliant"
, 'configuration_menu_depliant_desc' => "Cette page vous permet de configurer le comportement graphique du menu de navigation
	présent dans l'espace public du site."

, 'configurer_selecteurs' => "Sélecteurs"
, 'configurer_selecteurs_desc' => "Les sélecteurs sont indispensables au bon fonctionnement du plugin. 
	Si vous utilisez les squelettes de distribution de SPIP, vous pouvez conserver les sélecteurs définis ici."
, 'configurer_id' => "Id du bloc de navigation "
, 'configurer_div' => "Elément et classe du conteneur "
, 'configurer_a' => "Chemin de l'ancre contenu dans le bloc de navigation "
, 'configurer_class' => "Classe de la rubrique courante (<em><code>EXPOSE</code></em>). Si vous ne voulez pas voir déplié le menu
	de la rubrique courante, indiquez ici une classe inexistante "

, 'configurer_absolute' => "Sous-menu flottant"
, 'configurer_absolute_desc' => "Par défaut, le sous-menu est incrusté dans le menu de navigation. Le sous-menu flottant apparaît au dessus 
	du menu principal."
, 'configurer_absolute_activer' => "Activer le mode sous-menu flottant"
, 'configurer_top' => "Position verticale "
, 'configurer_left' => "Position horizontale (une valeur négative déplace le sous-menu sur la gauche) "
, 'configurer_zindex' => "Position de la couche (z-index)"

, 'configurer_couleur' => "Couleur de fond et bordure du sous-menu"
, 'configurer_heriter' => "Héritage du style"
, 'configurer_heriter_activer' => "Le style des boites hérite du menu parent"
, 'configurer_heriter_desc' => "Par défaut, le sous-menu hérite de la couleur de fond et de la bordure du menu de navigation.
	Si cette couleur et cette bordure sont manquantes, la couleur de fond et la bordure indiqués
	ci-dessous lui seront attribués."
, 'configurer_opaque_activer' => "Le sous-menu hérite sa couleur de fond et sa bordure."
, 'configurer_bgcolor' => "Couleur de fond du sous-menu "
, 'configurer_border' => "Bordure du sous-menu "

, 'configurer_vitesse' => "Vitesse d'animation (apparition/disparition du sous-menu) "
, 'configurer_vitesse_desc' => "Placez dans ce
	champ une chaine de caractères représentant une des trois vitesses prédéfinies (<em>slow</em>, <em>normal</em> ou <em>fast</em>) 
	ou un nombre en millisecondes correspondant à la durée de l'animation."
, 'configurer_vitesse_in' => "Apparition du sous-menu (mettre 1 pour annuler l'animation) "
, 'configurer_vitesse_out' => "Disparition du sous-menu (mettre 1 pour annuler l'animation) "
, 'configurer_tempo' => "Temporisation (suspendre les évènements souris afin d'éviter l'emballement plier/déplier) en millisecondes"

, 'configurer_replier' => "Replier les sous-menus"
, 'configurer_replier_desc' => "Par défaut, lorsque la souris survole un sous-menu, le précédent sous-menu (sauf celui correspondant
	à la rubrique en cours) se replie. En décochant cette option, le précédent sous-menu reste déplié."
, 'configurer_replier_activer' => "Replier le sous-menu qui n'est plus survolé par la souris."
, 'configurer_reavant_activer' => "Replier le précédent sous-menu avant de déplier celui survolé par la souris."

, 'configurer_vider_cache_desc' => "Si vous désirez activer immédiatement les nouveaux réglages, vous devez vider le cache. 
	Sinon, ces nouveaux réglages seront activés au fur et à mesure de la mise à jour du site."

, 'bouton_reinit' => "Réinitialiser"

); //

?>