<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/manuelsite?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer_explication' => 'Tento zásuvný modul nainštaluje ikonu pomocníka, ktorá umožní zobraziť zo súkromnej zóny akúkoľvek stranu z manuálu o publikovaní na stránke. Tento manuál je @texte@ Musí redaktorom priblížiť štruktúru stránky, čo je v ktorej rubrike, ako zakódovať a nainštalovať video, atď. Skrátka všetko, čo chcete a čo je špecifické pre vašu stránku.',
	'configurer_explication_l_article' => '<a href="@url@" title="Manuál na publikovanie">článok #@idart@</a> z vašej stránky.',
	'configurer_explication_un_article' => 'článok stránky.',
	'configurer_titre' => 'Nastaviť manuál na publikovanie stránky',

	// E
	'erreur_article' => 'Článok manuálu definovaný v nastaveniach zásuvného modulu sa nenašiel: #@idart@',
	'erreur_pas_darticle' => 'Článok manuálu nie je definovaný v nastaveniach zásuvného modulu',
	'explication_afficher_bord_gauche' => 'Ikonu manuálu zobraziť vľavo hore (ak sa manuál zobrazí v stĺpci)',
	'explication_background_color' => 'Uveďte farbu pozadia oblasti, v ktorej sa zobrazí manuál',
	'explication_cacher_public' => 'Schovať tento článok na verejne prístupnej stránke, vrátane kanála rss',
	'explication_email' => 'Kontaktný e-mail pre redaktorov',
	'explication_faq' => 'Nižšie nájdete všeobecné bloky, ktoré môžete použiť pri písaní svojho manuálu (text sa zobrazí bez formátovania kódu). Stačí skopírovať a prilepiť príslušný kód do poľa pre text článku.<br />Ak nechcete zobraziť otázku, zadajte <i>|q=non</i>.<br />Ak chcete pridať parametre, zadajte <i>|params=p1:v1;p2:v2.</i>',
	'explication_id_article' => 'Zadajte číslo článku, ktorý je súčasťou manuálu',
	'explication_intro' => 'Úvodný text manuálu (bude umiestnený pred úvod, resp. perex)',
	'explication_largeur' => 'Zadajte šírku oblasti, v ktorej sa zobrazí manuál',

	// F
	'fermer_le_manuel' => 'Zatvoriť manuál',

	// H
	'help' => 'Pomoc: ',

	// I
	'intro' => 'Cieľom tohto dokumentu je pomôcť redaktorom s používaním stránky. Dopĺňa dokument s názvom "[Kurz SPIPu pre redaktorov->@url@]", ktorý poskytuje základnú pomoc s používaním SPIPu. Nájdete tu opis štruktúry stránky, technickú pomoc zameranú na konkrétne veci, atď.',

	// L
	'label_afficher_bord_gauche' => 'Zobrazenie',
	'label_background_color' => 'Farba pozadia',
	'label_cacher_public' => 'Schovať',
	'label_email' => 'E-mail',
	'label_id_article' => 'Č. článku',
	'label_intro' => 'Úvod',
	'label_largeur' => 'Šírka',
	'legende_apparence' => 'Vzhľad',
	'legende_contenu' => 'Obsah',

	// T
	'titre_faq' => 'Časté otázky o Manuále na publikovanie stránky',
	'titre_manuel' => 'Manuál na publikovanie stránky',
	'titre_menu' => 'Manuál na publikovanie stránky'
);

?>
