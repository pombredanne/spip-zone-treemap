<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-manuelsite?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'manuelsite_description' => 'This plugin displays an help icon on every page of the private space showing the Website Editor Manual. This manual is an article of the website. The plugin also provides a set of Frequently Asked Questions that can be easily inserted in your manual.',
	'manuelsite_nom' => 'Website Editor Manual',
	'manuelsite_slogan' => 'A manual specific to your site for your editors'
);

?>
