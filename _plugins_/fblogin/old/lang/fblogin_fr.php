<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'je_veux_utiliser_fb_insc' => 'Je veux utiliser mon compte FaceBook pour m\'inscrire',
'je_veux_utiliser_fb_login' => 'Utiliser mon compte FaceBook pour m\'identifier',
'je_veux_utiliser_fb' => 'Je veux &ecirc;tre reconnu quand je viens de Facebook',
'acceder_a_mon_profil_fb' => 'Mon profil Facebook',
'auth_fb_ok_connectez_vous' => 'Vous avez &eacute;t&eacute; identifi&eacute; par Facebook. Indiquez votre login et votre mot de passe pour finir.',
'ajouter_application' => 'Ajouter l\'application sur Facebook',

);
?>
