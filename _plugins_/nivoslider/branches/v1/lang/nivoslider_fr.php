<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/cfg/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'animspeed' => 'Vitesse de l\'animation',
	// B
	'bottommargin' => 'Marge sous le slider',
	'bouton_effacer' => 'Effacer',
	// C
	'caption_backcolor' => 'Couleur du fond',
	'caption_frontcolor' => 'Couleur du texte',
	'caption_opacity' => 'Transparence',
	'controls' => 'Contrôles',
	'controlnav' => 'Afficher les puces',
	'cfg_title_nivoslider' => 'Nivo Slider',
	'cfg_boite_nivoslider' => 'Configuration du plugin Nivo Slider.<br /><br />[Documentation->http://www.spip-contrib.net/Nivo-Slider-pour-SPIP]',
	// D
	'default' => 'Diaporama par défaut',
	'default_info' => 'Article utilisé comme réservoir d\'images lors de l\'inclusion du modèle [(#MODELE{nivoslider})] sans paramètre <i>id_article</i> à partir d\'un squelette :',
	'directionnav' => 'Activer les flèches de navigation',
	'directionnavhide' => 'Cacher les fl&egrave;ches (afficher au survol)',
	// E
	'effect' => 'Effet de transition',
	// F
	// G
	'general' => 'Apparence générale',
	// H
	'height' => 'Hauteur',
	// I 
	'image_align' => 'Recadrer',
	'image_backcolor' => 'Couleur de fond',
	// J
	// K
	'keyboardnav' => 'Utiliser les fl&eacute;ches droite &amp; gauche du clavier',
	// L
	'legend' => 'Légendes',
	// M
	// N
	'non' => 'non',
	// O
	'oui' => 'oui',
	// P
	'pausetime' => 'Pause entre les images',
	'pauseonhover' => 'Pause au survol',
	// Q
	// R
	// S
	'slices' => 'Nombre de tranches',
	// T
	// U
	'usethumbs' => 'Activer les miniatures',
	// V
	// W
	'width' => 'Largeur',
	// X
	// Y
	// Z
);

?>
