<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
'ajouter_playlist' => 'ajouter &agrave; la playlist',
'adapter_bd' => 'Adapter la base de donn&eacute;e afin d\'ajouter les colonnes necessaire au bon fonctionnement de la radio',

// B
'base_donnee' => 'Base de donn&eacute;e',
'base_donnee_expliq' => 'Ajoute une colonne "playlist" &agrave; votre table spip_documents, indispensable pour le fonctionnement de la webradio',
'base_modifie' => 'la base de donn&eacute;e a &eacute;t&eacute; modifi&eacute;e',

// C
'configuration' => 'Configuration',

// D
'descriptif' => 'Descriptif',

// I
'infos' => 'Cette page liste tous les documents MP3 présents dans la table documents, c\'est à dire tous les documents étant lié à un article (que ce soit en tant que document distant ou non).<br /><br />
Vous pouvez modifier le titre et le descriptif d\'un document et l\'&eacute;couter avant de l\'ajouter ou de le retirer de la playlist.<br /><br />
Un lien est aussi pr&eacute;sent pour visualiser l\'article auquel le document a &eacute;t&eacute; li&eacute;.<br /><br />
Si vous ne l\'avez pas encore fait : ',

// L
'liste_modifie' => 'ont &eacute;t&eacute; ajout&eacute; :',
// P
'pas_de_titre' => 'pas de titre',

// R
'retirer_playlist' => 'Retirer de la playlist',
'requete_effectue' => 'Requete SQL effectu&eacute;e',

// T
'titre' => 'Titre',
'transformer_liens_expliq' => 'Ces nouveaux documents apparaitrons dans les portofolios des articles conern&eacute;s.',
'transformer_liens' => 'Transformer, dans le texte de vos articles, les liens vers des fichier mp3 en documents distants (afin que ces liens soient pris en compte par la radio)',

// U
'une_seule_fois' => 'Attention, a ne faire qu\'une seule fois !',

// V
'visualiser_article' => 'Visualiser l\'article associ&eacute;',

// W
'webradio_radio_titre' => 'Gestion de la radio',
'webradio' => 'WebRadio'

);

?>