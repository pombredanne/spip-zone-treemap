<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://trac.rezo.net/spip-zone/_plugins_/_test_/inscription2/inscription2_2_0/lang
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'choisir_listes' => 'Choisissez ci-dessous les listes de diffusion &#224; proposer aux nouveaux adh&eacute;rents&nbsp;: ',
	
	// E
	'exp_newsletter' => 'Souhaitez-vous vous abonner à notre liste de diffusion&nbsp;?',
	'exp_newsletters' => 'Choisissez ci-dessous les listes de diffusion auxquelles vous souhaitez vous abonner',
	
	// D
	'deselect_listes' => '> tout d&#233;s&eacute;lectionner',
			
	// N
	'newsletter' => 'Listes de diffusion',
	'newsletter_label' => 'Abonnement aux listes de diffusion',
	
	// O
	'optout' => 'Cases &agrave; cocher non pr&#233;coch&#233;es (optout)',
	
	// S
	'select_listes' => '(vous pouvez s&#233;lectionner plusieurs listes avec la touche shift)',
		
	// Z
	'z' => 'zzz'
);

?>
