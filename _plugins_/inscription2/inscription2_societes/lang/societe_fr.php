<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'adresse' => 'Adresse',
	'ajouter_societe' => 'Ajouter une soci&eacute;t&eacute;',

	// C
	'creer_societe' => 'Ajouter une soci&eacute;t&eacute;',
	'code_postal' => 'Code Postal',
	'confirmer_supprimer_societe' => '&Ecirc;tes vous s&ucirc;r de vouloir supprimer cette soci&eacute;t&eacute; ?',
	'cfg_forme_affichage' => 'Mani&egrave;re d\'afficher les soci&eacute;t&eacute;s',
	'cfg_forme_unique' => 'Champs &agrave; s&eacute;lection unique',
	'cfg_forme_multiple' => 'Champs &agrave; s&eacute;lection multiple',

	// D
	'description_cfg' => 'Ce plugin permet d\'ajouter une liste de soci&eacute;t&eacute; s&eacute;lectionnable dans le profil des membres du site',
	
	// F
	'fax' => 'Fax',

	// I
	'icone_supprimer_societe' => 'Supprimer cette soci&eacute;t&eacute;',
	'id_societe' => 'Soci&eacute;t&eacute; (&agrave; choisir dans une liste)',
	'info_page_liste' => 'Cette page liste l\'ensemble des entreprises disponibles.',
	
	// L
	'liste_societes' => 'Liste des soci&eacute;t&eacute;s',

	// M
	'modifier_societe' => 'Modifier la soci&eacute;t&eacute;',

	// N
	'nom' => 'Nom',
	
	// P
	'pays' => 'Pays',

	// S
	'secteur' => 'Secteur',
	'societes' => 'Soci&eacute;t&eacute;s',
	'societe' => 'Soci&eacute;t&eacute;',
	'societe_choisir' => 'Choisissez une soci&eacute;t&eacute;',
	
	// T
	'telephone' => 'T&eacute;l&eacute;phone',
	
	// V
	'ville' => 'Ville',
	'voir_societes' => 'Visualiser les soci&eacute;t&eacute;s',

	// Z
	'zzzz' => 'ZZZZ'
);
?>
