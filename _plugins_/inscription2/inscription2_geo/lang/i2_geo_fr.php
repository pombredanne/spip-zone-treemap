<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://trac.rezo.net/spip-zone/_plugins_/_test_/inscription2/inscription2_2_0/lang
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	// G
	'geolocalisation' => 'G&eacute;olocalisation',
	
	// L
	'latitude' => 'Latitude',
	'longitude' => 'Longitude',
	
	// S
	'saisir_latitude_valide' => 'Veuillez saisir une latitude valide',
	'saisir_longitude_valide' => 'Veuillez saisir une longitude valide',
	
	// Z
	'z' => 'zzz'
);

?>