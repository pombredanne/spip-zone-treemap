<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'choisir_listes' => 'Choisissez ci-dessous les listes de diffusion &#224; proposer aux nouveaux adh&eacute;rents&nbsp;: ',
	
	// E
	'envoi_vers'=> 'envoi vers',
	'exp_liste' => 'Souhaitez-vous vous abonner à notre liste de diffusion&nbsp;?',
	'exp_listes' => 'Choisissez ci-dessous les listes de diffusion auxquelles vous souhaitez vous abonner',
	
	// D
	'deselect_listes' => '> tout d&#233;s&eacute;lectionner',
			
	// N
	'liste' => 'Listes de diffusion abomailmans',
	'liste_label' => 'Abonnement aux listes de diffusion',
	
	// O
	'optout' => 'Cases &agrave; cocher non pr&#233;coch&#233;es (optout)',
	
	// S
	'select_listes' => '(vous pouvez s&#233;lectionner plusieurs listes avec la touche shift)',
	
	// V
	'veut_s_abonner' => 'veut s\'abonner',
	
	// Z
	'z' => 'zzz'
);

?>
