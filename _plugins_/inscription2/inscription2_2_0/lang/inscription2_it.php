<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/inscription2?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_confirmer' => 'Da confermare',
	'accesrestreint' => 'Accesso vietato',
	'aconfirmer' => 'Da confermare',
	'action_adherent' => 'Operazioni',
	'activation_compte' => 'Attiva il tuo account',
	'adherents' => 'Utenti',
	'admin' => 'Amministratore',
	'admin_modifier_auteur_supp' => 'Informazioni supplementari',
	'adresse' => 'Indirizzo',
	'adresse_pro' => 'Indirizzo lavoro (Pro.)',
	'afficher_tous' => 'Mostra tutti gli utenti',
	'ajouter_adherent' => 'Crea un nuovo utente',
	'ans' => 'Anni',
	'aout' => 'Agosto',
	'aucun_resultat_recherche' => 'Nessun risultato per la tua ricerca.',
	'auteur' => 'Autore',
	'autre' => 'Altro',
	'avril' => 'Aprile',

	// B
	'bio' => 'Biografia',

	// C
	'caracteres' => 'caratteri', # MODIF
	'categorie' => 'Categoria di contribuzione',
	'chaine_valide' => 'Inserisci una stringa di caratteri',
	'chainelettre' => '(composta unicamente da lettere)',
	'chainenombre' => '(composta da lettere e/o cifre)',
	'champ_obligatoire' => 'Questo campo è obbligatorio',
	'choisir_categories' => 'Scegli una voce tra le seguenti:',
	'choisir_nouveau_password' => 'Puoi scegliere una nuova password cliccando sul seguente link',
	'choisir_zones' => 'Scegli le zone per nelle quali vuoi che i nuovi iscritti siano aggiunti',
	'choisissez' => 'Scegli',
	'choix_affordance_email' => 'Email',
	'choix_affordance_login' => 'Login (predefinito in SPIP)',
	'choix_affordance_login_email' => 'Login e email',
	'choix_domaine' => 'Scegli il tuo dominio',
	'civilite' => 'Stato civile',
	'code_postal' => 'CAP',
	'code_postal_pro' => 'CAP (lavoro)',
	'commentaire' => 'Commento',
	'compte_active' => 'Il tuo account su @nom_site@',
	'compte_efface' => 'Il tuo account è stato cancellato.',
	'conf_plugin' => 'Configurazione Inscription 2.0',
	'conf_plugin_page' => 'Configurazione memorizzata del plugin Inscription 2.0',
	'configs' => 'Configurazioni',
	'configuration' => 'Configurazione dei campi degli utenti',
	'confirmation' => '<p>Sei sicuro(a) di voler continuare?</p><p>Tutte le modifiche saranno irreversibili!!!</p>',
	'contacts_personnels' => 'Contatti personali',
	'contacts_pros' => 'Contatti professionali',
	'cp_valide' => 'Inserisci un CAP valido',
	'creation' => 'Data di creazione del file',

	// D
	'decembre' => 'Dicembre',
	'delete_user_select' => 'Cancella gli utenti selezionati',
	'demande_password' => 'La tua password:',
	'descriptif_plugin' => 'Troverai qui tutti gli utenti iscritti al sito. Il loro stato è indicato dal colore della loro icona.<br /><br />Puoi configurare dei campi supplementari, proposti in opzione ai visitatori al momento dell\'iscrizione.',
	'description_cfg' => 'Parametrizza i campi supplementari per gli utenti',
	'description_page' => 'Qui puoi verificare le tue scelte di configurazione salvate',
	'divers' => 'Varie',
	'domaine' => 'Dominio',
	'domaines' => 'Dominio',

	// E
	'editer_adherent' => 'Modifica utente',
	'effacement_auto_impossible' => 'L\'account non può essere cancellato automaticamente, contattaci.',
	'email' => 'E-Mail',
	'email_bonjour' => 'Buongiorno @nom@,',
	'email_deja_enregistre' => 'Questo indirizzo email è già registrato. Utilizza il form di login per accedere al tuo account.',
	'email_obligatoire' => 'Il campo email è obbligatorio',
	'email_valide' => 'Inserisci una email valida',
	'erreur_reglement_obligatoire' => 'Devi accettare il regolamento',
	'exp_divers' => 'Campo proposto ai visitatori con il nome del commento',
	'exp_publication' => 'Autorizzazione alla pubblicazione dei dati personali',
	'exp_statut' => 'Scegli lo stato che vuoi attribuire ai nuovi iscritti',
	'exp_statut_rel' => 'Campo diverso dallo stato di SPIP, che serve per il controllo interno di un\'iscrizione',
	'explication_affordance_form' => 'Campo mostrato sui form di login (#LOGIN_PUBLIC)',

	// F
	'fax' => 'Fax',
	'fax_pro' => 'Fax di lavoro',
	'feminin' => 'Femminile',
	'fevrier' => 'Febbraio',
	'fiche' => 'Pagina',
	'fiche_adherent' => 'Pagina utente',
	'fiche_expl' => 'Il campo sarà visibile nella pagina dell\'utente (spazio redazionale)',
	'fiche_mod' => 'Modificabile',
	'fiche_mod_expl' => 'Il campo sarà modificabile dall\'interfaccia pubblica dall\'utente a condizione di usare il plugin #CRAYONS',
	'fonction' => 'Funzione',
	'form' => 'Form',
	'form_expl' => 'Il campo sarà mostrato sul form  #INSCRIPTION2',
	'form_oblig_expl' => 'Rendi l\'inserimento obbligatorio nel form',
	'format' => 'Formato',
	'formulaire_inscription' => 'Modulo di iscrizione',
	'formulaire_inscription_ok' => 'La tua iscrizione è stata ricevuta. Riceverai una email con i tuoi parametri di connessione',
	'formulaire_login_deja_utilise' => 'Il nome utente è già registrato, scegline un altro',
	'formulaire_remplir_obligatoires' => 'Inserisci i campi obbligatori',
	'formulaire_remplir_validation' => 'Verifica i campi che non sono corretti.',

	// G
	'general_infos' => 'Informazioni generali',
	'geoloc' => 'Geolocalizzazione',
	'geomap_obligatoire' => 'Per utilizzare i dati di geolocalizzazione delle persone, è necessario installare il plugin GoogleMapApi...',
	'gerent' => 'Iscritto',
	'gestion_adherent' => 'Gestione degli utenti',

	// H
	'html' => 'HTML',

	// I
	'icone_afficher_utilisateurs' => 'Mostra gli utenti',
	'icone_menu_config' => 'Configurazione avanzata degli utenti',
	'identification' => 'Identificazione',
	'info_connection' => 'Informazioni di connessione',
	'info_defaut' => 'Informazioni obbligatorie',
	'info_defaut_desc' => 'Possibilità di parametrizzazione',
	'info_gener' => 'Informazioni generali',
	'info_gener_desc' => 'Opzioni che saranno richieste ai nuovi iscritti al sito',
	'info_internes' => 'Informazioni interne',
	'info_internes_desc' => 'Opzioni che saranno memorizzate nel database ma non saranno mostrate nel modulo dei nuovi iscritti',
	'info_perso' => 'Informazioni personali',
	'info_perso_desc' => 'Informazioni personali che saranno richieste ai nuovi iscritti al sito',
	'info_pro' => 'Informazioni professionali',
	'infos_adherent' => 'Informazioni supplementari',
	'infos_complementaires' => 'Informazioni complementari',
	'infos_personnelles' => 'Informazioni personali',
	'inscription2' => 'Inscription 2.0',
	'inscriptions' => 'Iscrizioni',
	'inserez_infos' => 'Inserisci le informazioni richieste',

	// J
	'janvier' => 'Gennaio',
	'juillet' => 'Luglio',
	'juin' => 'Giugno',

	// L
	'label_affordance_form' => 'Parametrizzazione dei moduli di login',
	'label_public_reglement' => 'Ho letto ed accettato il regolamento',
	'label_public_reglement_url' => 'Ho letto ed accettato il <a href="@url@" class="spip_in reglement">regolamento</a>',
	'label_reglement' => 'Regolamento da accettare',
	'label_reglement_article' => 'Articolo del sito corrispondente al regolamento',
	'label_validation_numero_international' => 'Forza i numeri di telefono ad essere in formato internazionale',
	'latitude' => 'Latitudine',
	'legend_affordance_form' => 'Modulo di login',
	'legend_oubli_pass' => 'Nessuna password / password dimenticata',
	'legend_reglement' => 'Regolamento del sito',
	'legend_validation' => 'Validazioni',
	'legende' => 'Legenda',
	'lisez_mail' => 'Una email è stata inviata all\'indirizzo fornito. Per attivare il tuo account segui le istruzioni.',
	'liste_adherents' => 'Vedi la lista degli utenti',
	'liste_comptes_titre' => 'Lista degli utenti',
	'login' => 'Nome utente (login)',
	'logo_auteur' => 'Logo',
	'longitude' => 'Longitudine',

	// M
	'mai' => 'Maggio',
	'mail_non_domaine' => 'L\'indirizzo email che hai fornito non appartiene al dominio che hai indicato. Verifica i dati',
	'mail_renvoye' => 'Questo indirizzo email è già stato memorizzato, attiva il tuo account seguendo le istruzioni contenute nell\'email.',
	'mars' => 'Marzo',
	'masculin' => 'Maschile',
	'message_auto' => '(questo è un messaggio automatico)',
	'mobile' => 'Cellulare',
	'mobile_pro' => 'Cellulare di lavoro',
	'modif_pass_titre' => 'Modifica la tua password',
	'moins_seconde' => 'meno di un secondo',
	'mot_passe_reste_identique' => 'La tua password non è stata modificata.',

	// N
	'naissance' => 'Data di nascita',
	'nb_users_supprimes' => '@nb@ cancellazioni effettuate.',
	'nettoyer_tables' => 'Pulisci le tabelle',
	'no_user_selected' => 'Non selezionato alcun utente.',
	'nom' => 'Firma',
	'nom_explication' => 'Il tuo nome o pseudonimo',
	'nom_famille' => 'Cognome',
	'nom_site' => 'Nome del sito',
	'non_renseigne' => 'non specificato.',
	'non_renseignee' => 'non specificato.',
	'novembre' => 'Novembre',
	'numero_valide' => 'Inserisci un numero valido',
	'numero_valide_international' => 'Questo numero deve essere in formato internazionale (es: +32 475 123 456)',

	// O
	'octobre' => 'Ottobre',

	// P
	'page_confirmation' => 'Pagina di conferma della tua iscrizione',
	'par_defaut' => 'Questo campo è obbligatorio',
	'pass' => 'Password',
	'pass_egal' => 'Inserisci nuovamente la password.',
	'pass_indiquez_cidessous' => 'Indica di seguito l\'indirizzo email con il quale ti sei precedentemente registrato. Riceverai una email con le istruzioni da seguire per modificare i tuoi dati.',
	'pass_minimum' => 'La tua password deve essere lunga almeno 5 caratteri',
	'pass_oubli_mot' => 'Modifica della password',
	'pass_rappel_email' => 'Promemoria: il tuo indirizzo email è "@email@".',
	'pass_rappel_login_email' => 'Promemoria: il tuo login è "@login@" ed il tuo indirizzo email è "@email@".',
	'pass_recevoir_mail' => 'Riceverai una email che ti indicherà come modificare il tuo accesso al sito.',
	'password_obligatoire' => 'La password è obbligatoria.',
	'password_retaper' => 'Conferma la password',
	'pays' => 'Stato',
	'pays_defaut' => 'Stato predefinito',
	'pays_pro' => 'Stato (lavoro)',
	'pgp' => 'Chiave PGP',
	'prenom' => 'Nome',
	'probleme_email' => 'Problema con la posta: l\'email di attivazione non può essere inviata.',
	'profession' => 'Professione',
	'profil_droits_insuffisants' => 'Spiacente non hai il permesso di modificare questo autore<br />',
	'profil_modifie_ok' => 'Le modifiche al tuo profilo sono state memorizzate correttamente.',
	'publication' => 'Pubblicazione',

	// R
	'raccourcis' => 'Scelta rapida',
	'rappel_login' => 'Promemoria: il tuo login è: ',
	'rappel_password' => 'La tua password',
	'recherche_case' => 'Nel campo:',
	'recherche_utilisateurs' => 'Ricerca un utente',
	'recherche_valeur' => 'Ricerca:',
	'redemande_password' => 'Inserisci nuovamente la password:',
	'rien_a_faire' => 'Niente da fare',

	// S
	'saisir_email_valide' => 'Inserisci un indirizzo email valido',
	'secondes' => 'secondi',
	'secteur' => 'Settore',
	'septembre' => 'Settembre',
	'sexe' => 'Sesso',
	'societe' => 'Società / Associazione ...',
	'statut' => 'Stato',
	'statut_rel' => 'Stato interno',
	'statuts_actifs' => 'I colori delle icone corrispondono ai seguenti stati:',
	'suppression_faite' => 'La cancellazione dei tuoi dati è stata effettuata',
	'supprimer_adherent' => 'Cancella utenti',
	'supprimer_adherent_red' => 'Canc',
	'surnom' => 'Pseudonimo',

	// T
	'table' => 'Tabella',
	'table_expl' => 'Il campo sarà mostrato sulla lista degli utenti (in redazione)',
	'tel' => 'Tel',
	'telephone' => 'Telefono',
	'telephone_pro' => 'Telefono di lavoro',
	'texte' => 'Testo',
	'texte_email_confirmation' => 'Il tuo account è attivo, puoi d\'ora in poi connetterti al sito utilizzando i tuoi parametri di accesso personali.

il tuo login è: @login@
ed hai impostato la tua password.

Grazie per esserti registrato

Il team di @nom_site@
@url_site@',
	'texte_email_inscription' => 'Stai per confermare la tua iscrizione al sito @nom_site@.

Clicca il link qui sotto per attivare il tuo account e scegliere la tua password.

@link_activation@


Grazie per la fiducia.

Il team di @nom_site@.
@url_site@

Si non hai richiesto questa iscrizione o se non vuoi più iscriverti al nostro sito, clicca il link qui sotto.
@link_suppresion@


',
	'titre_confirmation' => 'Conferma',

	// U
	'un_an' => 'un anno',
	'un_jour' => 'un giorno',
	'un_mois' => 'un mese',
	'une_heure' => 'un\'ora',
	'une_minute' => 'un minuto',
	'une_seconde' => 'un secondo',
	'url_site' => 'Url del sito',
	'url_societe' => 'Sito società',

	// V
	'validite' => 'Data di scadenza',
	'ville' => 'Città',
	'ville_pro' => 'Città (lavoro)',
	'visiteur' => 'Visitatore',
	'vos_contacts_personnels' => 'I tuoi contatti personali',
	'votre_adresse' => 'Il tuo indirizzo personale',
	'votre_adresse_pro' => 'Il tuo indirizzo (professionale)',
	'votre_login_mail' => 'Il tuo login o email:',
	'votre_mail' => 'La tua email:',
	'votre_nom_complet' => 'Il tuo nome completo',
	'votre_profil' => 'Il tuo profilo',

	// W
	'website' => 'Sito internet'
);

?>
