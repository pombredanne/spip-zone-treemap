<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/inscription2?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_confirmer' => 'To be confirmed',
	'accesrestreint' => 'Restricted access',
	'aconfirmer' => 'To be confirmed',
	'action_adherent' => 'Operations',
	'activation_compte' => 'Activate your account',
	'adherents' => 'Members',
	'admin' => 'Admin',
	'admin_modifier_auteur_supp' => 'Extra information',
	'adresse' => 'Address',
	'adresse_pro' => 'Professionnal address',
	'afficher_tous' => 'Show members',
	'ajouter_adherent' => 'Add a new member',
	'ans' => 'years',
	'aout' => 'August',
	'aucun_resultat_recherche' => 'There isn\'t any result that corresponds to your query.',
	'auteur' => 'Author',
	'autre' => 'Other',
	'avril' => 'April',

	// B
	'bio' => 'Biography',

	// C
	'caracteres' => 'Forbidden characters!',
	'categorie' => 'Category of contribution',
	'chaine_valide' => 'Please insert a string',
	'chainelettre' => '(composed only of letters)',
	'chainenombre' => '(composed of letters and/or digits)',
	'champ_obligatoire' => 'This field is required',
	'choisir_categories' => 'Select them in the following list:',
	'choisir_nouveau_password' => 'You will be able to choose a new password by clicking on the following link',
	'choisir_zones' => 'Choose the areas to which new members will be assigned',
	'choisissez' => 'Choose',
	'choix_affordance_email' => 'Email',
	'choix_affordance_login' => 'Login (SPIP default)',
	'choix_affordance_login_email' => 'Login & email',
	'choix_domaine' => 'Choose your domain',
	'civilite' => 'Title',
	'code_postal' => 'Postcode',
	'code_postal_pro' => 'Postcode',
	'commentaire' => 'Comments',
	'compte_active' => 'Your account on @nom_site@',
	'compte_efface' => 'Your account has been deleted.',
	'conf_plugin' => 'Inscription 2.0 settings',
	'conf_plugin_page' => 'Saved settings of the Inscription 2.0 plugin',
	'configs' => 'Configurations',
	'configuration' => 'User\'s fields configuration',
	'confirmation' => '<p>Are you sure you wish to continue</p><p>The changes made are irreversible!</p>',
	'contacts_personnels' => 'Personnal contacts',
	'contacts_pros' => 'Professional contacts',
	'cp_valide' => 'Please give a valid postcode',
	'creation' => 'Creation date of the record',

	// D
	'decembre' => 'December',
	'delete_user_select' => 'Delete selected user(s)',
	'demande_password' => 'Your password:',
	'descriptif_plugin' => 'Hera are all the site\'s members. Their status is shown by their icon\'s color.<br /><br />You can set up additional fields, that will be proposed to visitors when they subscribe.',
	'description_cfg' => 'Users\' additionnal fields configuration',
	'description_page' => 'Here you can check your saved settings',
	'divers' => 'any other business',
	'domaine' => 'Domain',
	'domaines' => 'Domain',

	// E
	'editer_adherent' => 'Edit member',
	'effacement_auto_impossible' => 'The account cannot be traced automatically. Please contact us.',
	'email' => 'Email',
	'email_bonjour' => 'Hello @nom@,',
	'email_deja_enregistre' => 'This email is already registered. Please, use the login form to get into your account.',
	'email_obligatoire' => 'An email address is required',
	'email_valide' => 'Please insert a valid e-mail address',
	'erreur_reglement_obligatoire' => 'You must accept the site\'s rules',
	'exp_divers' => 'Field which will be offered to the visitors with the name Comments',
	'exp_publication' => 'Authorization for publication of personal data',
	'exp_statut' => 'Choose the status to be given to new members',
	'exp_statut_rel' => 'This field is different from the SPIP status, this one is used for the internal control of an institution',
	'explication_affordance_form' => 'Fields used in the login form (#LOGIN_PUBLIC)',

	// F
	'fax' => 'Fax',
	'fax_pro' => 'Work fax',
	'feminin' => 'Ms.',
	'fevrier' => 'February',
	'fiche' => 'Record',
	'fiche_adherent' => 'Member\'s record',
	'fiche_expl' => ': Display this field  on member\'s record (backoffice)',
	'fiche_mod' => 'Can be modified',
	'fiche_mod_expl' => ': Member can modify this field from public interface, if the #CRAYONS plugin is active ',
	'fonction' => 'Function',
	'form' => 'Form',
	'form_expl' => ': Display this field on #INSCRIPTION2 form',
	'form_oblig_expl' => ' : required field on the form',
	'format' => 'Format',
	'formulaire_inscription' => 'Registration form',
	'formulaire_inscription_ok' => 'Your registration has been noted. You will receive your login information by email.',
	'formulaire_login_deja_utilise' => 'This login is already in use. Please choose another.',
	'formulaire_remplir_obligatoires' => 'Please fill in the required fields.',
	'formulaire_remplir_validation' => 'Please check the fields that are not valid',

	// G
	'general_infos' => 'General information',
	'geoloc' => 'Geographical positioning',
	'geomap_obligatoire' => 'To use the geographical positioning data for people, you need to install the GoogleMapApi plugin',
	'gerent' => 'Member',
	'gestion_adherent' => 'Members\' management',

	// H
	'html' => 'HTML',

	// I
	'icone_afficher_utilisateurs' => 'Display members',
	'icone_menu_config' => 'Members\' advanced configuration',
	'identification' => 'Identification',
	'info_connection' => 'Login information',
	'info_defaut' => 'Required information',
	'info_defaut_desc' => 'Configuration options',
	'info_gener' => 'General information',
	'info_gener_desc' => 'Options which new members will be asked about',
	'info_internes' => 'Internal information',
	'info_internes_desc' => 'Options which will be stored in the database but will not be displayed in the form for new members',
	'info_perso' => 'Personal information',
	'info_perso_desc' => 'Personal information requested of the new members of the site',
	'info_pro' => 'Professional information',
	'infos_adherent' => 'Extra information',
	'infos_complementaires' => 'Additional information',
	'infos_personnelles' => 'Personal information',
	'inscription2' => 'Inscription 2.0',
	'inscriptions' => 'Inscriptions',
	'inserez_infos' => 'Please fill in the requested information',

	// J
	'janvier' => 'January',
	'juillet' => 'July',
	'juin' => 'June',

	// L
	'label_affordance_form' => 'Login form configuration',
	'label_public_reglement' => 'I red and I accept site\'s rules',
	'label_public_reglement_url' => 'I red and I accept <a href="@url@" class="spip_in reglement">site\'s rules</a>',
	'label_reglement' => 'Accept site\'s rules',
	'label_reglement_article' => 'Site\'s rules article',
	'label_validation_numero_international' => 'Require international format for telephone numbers',
	'latitude' => 'Latitude',
	'legend_affordance_form' => 'Login form',
	'legend_oubli_pass' => 'No password set / password forgotten',
	'legend_reglement' => 'Site\'s rules',
	'legend_validation' => 'Validations',
	'legende' => 'Caption',
	'lisez_mail' => 'An email has been just sent to the address provided. To activate your account please follow the instructions.',
	'liste_adherents' => 'Show members\' list',
	'liste_comptes_titre' => 'Members\' list',
	'login' => 'Username (login)',
	'logo_auteur' => 'Logo',
	'longitude' => 'Longitude',

	// M
	'mai' => 'May',
	'mail_non_domaine' => 'The email that you entered does not belong to the chosen domain, please try again',
	'mail_renvoye' => 'This email address was already recorded. Please activate your account following the instructions contained in the email.',
	'mars' => 'March',
	'masculin' => 'Mr.',
	'message_auto' => '(this is an automatic message)',
	'mobile' => 'Mobile',
	'mobile_pro' => 'Work mobile',
	'modif_pass_titre' => 'Change your password',
	'moins_seconde' => 'less than a second',
	'mot_passe_reste_identique' => 'Your password has not been changed.',

	// N
	'naissance' => 'Date of birth',
	'nb_users_supprimes' => '@nb@ user(s) have been deleted',
	'nettoyer_tables' => 'Clean the tables',
	'no_user_selected' => 'Please select a user',
	'nom' => 'Signature',
	'nom_explication' => 'your name or nickname',
	'nom_famille' => 'Family Name',
	'nom_site' => 'Website name',
	'non_renseigne' => 'empty.',
	'non_renseignee' => 'empty.',
	'novembre' => 'November',
	'numero_valide' => 'Please give a valid number',
	'numero_valide_international' => 'Telephone numbers must comply with international format (ex: +32 475 123 456)',

	// O
	'octobre' => 'October',

	// P
	'page_confirmation' => 'Confirmation of your registration',
	'par_defaut' => 'This field is required',
	'pass' => 'Password',
	'pass_egal' => 'Please enter the same password as earlier.',
	'pass_indiquez_cidessous' => 'Give the email address with which you were previously registered.
You will receive and email telling you how to change your login.',
	'pass_minimum' => 'Your password must be at least 5 characters in length',
	'pass_oubli_mot' => 'Change your password',
	'pass_rappel_email' => 'Reminder: your email address is "@email@".',
	'pass_rappel_login_email' => 'Reminder: your login is "@login@" and your email address is "@email@".',
	'pass_recevoir_mail' => 'You will receive and email telling you how to change your login.',
	'password_obligatoire' => 'A password is required',
	'password_retaper' => 'Confirm the password',
	'pays' => 'Country',
	'pays_defaut' => 'Default country',
	'pays_pro' => 'Country',
	'pgp' => 'PGP key',
	'prenom' => 'First name',
	'probleme_email' => 'Email problem: the activation email cannot be sent.',
	'profession' => 'Profession',
	'profil_droits_insuffisants' => 'Sorry! You are not allowed to modify this author<br />',
	'profil_modifie_ok' => 'The changes to your profile have been saved.',
	'publication' => 'Publication',

	// R
	'raccourcis' => 'Shortcuts',
	'rappel_login' => 'Reminder: your login is: ',
	'rappel_password' => 'Your password',
	'recherche_case' => 'In field:',
	'recherche_utilisateurs' => 'Search users',
	'recherche_valeur' => 'Search:',
	'redemande_password' => 'Please fill in your password again:',
	'rien_a_faire' => 'Nothing to do',

	// S
	'saisir_email_valide' => 'Please enter a valid email',
	'secondes' => 'seconds',
	'secteur' => 'Sector of activity',
	'septembre' => 'September',
	'sexe' => 'Title',
	'societe' => 'Company / Association ...',
	'statut' => 'Status',
	'statut_rel' => 'Internal status',
	'statuts_actifs' => 'Status is shown by icon\'s color: ',
	'suppression_faite' => 'Deletion completed',
	'supprimer_adherent' => 'Delete members',
	'supprimer_adherent_red' => 'Del',
	'surnom' => 'Nickname',

	// T
	'table' => 'Table',
	'table_expl' => ': Display field on the members\' list (private area)',
	'tel' => 'Tel.',
	'telephone' => 'Phone',
	'telephone_pro' => 'Work phone',
	'texte' => 'Text',
	'texte_email_confirmation' => 'Your account has been activated. From now on you can access the site using your login and password.

Your login is: @login@
and you have just chosen your password.

Thank you for your trust

The @nom_site@ Team
@url_site@',
	'texte_email_inscription' => 'You are about to finish your registration for @nom_site@ site.

Click on the link below to activate your account and choose your password.

@link_activation@


Thank you for your trust.

The @nom_site@ Team 
@url_site@

If you did not ask for this registration or do not want to take part in the site anymore, click the link below.
@link_suppresion@


',
	'titre_confirmation' => 'Confirmation',

	// U
	'un_an' => 'a year',
	'un_jour' => 'a day',
	'un_mois' => 'a month',
	'une_heure' => 'an hour',
	'une_minute' => 'a minute',
	'une_seconde' => 'a second',
	'url_site' => 'Website url',
	'url_societe' => 'Company website',

	// V
	'validite' => 'Date of validity',
	'ville' => 'Town/City',
	'ville_pro' => 'Town/City',
	'visiteur' => 'Visitor',
	'vos_contacts_personnels' => 'Your personal contacts',
	'votre_adresse' => 'Your home address',
	'votre_adresse_pro' => 'Your professionnal address',
	'votre_login_mail' => 'Your login or email:',
	'votre_mail' => 'Your email:',
	'votre_nom_complet' => 'Your full name',
	'votre_profil' => 'Your profile',

	// W
	'website' => 'Website'
);

?>
