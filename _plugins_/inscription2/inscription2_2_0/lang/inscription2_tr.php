<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/inscription2?lang_cible=tr
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_confirmer' => 'Onaylanacak',
	'accesrestreint' => 'Sınırlı Erişim',
	'aconfirmer' => 'Onaylanacak',
	'action_adherent' => 'İşlemler', # MODIF
	'activation_compte' => 'Hesabınızı etkinleştirin',
	'adherents' => 'Üyeler', # MODIF
	'admin' => 'Yönetici',
	'admin_modifier_auteur_supp' => 'Ek bilgiler',
	'adresse' => 'Adres',
	'adresse_pro' => 'İş adresi (İş)',
	'afficher_tous' => 'Afficher tous les utilisateurs', # NEW
	'ajouter_adherent' => 'Yeni bir ziyaretçi oluştur', # MODIF
	'ans' => 'ans', # NEW
	'aout' => 'Ağustos',
	'aucun_resultat_recherche' => 'Aramanız için hiçbir sonuç yok.',
	'auteur' => 'Yazar',
	'autre' => 'Diğer',
	'avril' => 'Nisan',

	// B
	'bio' => 'Biyografi',

	// C
	'caracteres' => 'karakter', # MODIF
	'categorie' => 'Ödeme kategorisi',
	'chaine_valide' => 'Lütfen bir karakter jşzvşış üşışz',
	'chainelettre' => '(yalnız harflerden oluşan)',
	'chainenombre' => '(harf ve/veya rakamlardan oluşan)',
	'champ_obligatoire' => 'Bu alan zorunludur',
	'choisir_categories' => 'Aşğıdan seçiniz :',
	'choisir_nouveau_password' => 'Şu bağlantıya tıklayarak yeni bir şifre seçebilirsiniz',
	'choisir_zones' => 'Yeni üyelerin atanacağı alanları seçiniz',
	'choisissez' => 'Choisissez', # NEW
	'choix_affordance_email' => 'Email', # NEW
	'choix_affordance_login' => 'Login (par défaut dans SPIP)', # NEW
	'choix_affordance_login_email' => 'Login et email', # NEW
	'choix_domaine' => 'Bölgenizi seçiniz ("domaine")',
	'civilite' => 'Medenî durum',
	'code_postal' => 'Posta Kodu',
	'code_postal_pro' => 'Posta Kodu (İş)',
	'commentaire' => 'Yorum',
	'compte_active' => '@nom_site@ sitesindeki hesabınız',
	'compte_efface' => 'Hesabınız silindi.', # MODIF
	'conf_plugin' => 'Inscription2 Konfigürasyonu', # MODIF
	'conf_plugin_page' => 'Inscription2 eklentisinin kayıtlı konfigürasyonu', # MODIF
	'configs' => 'Konfigürasyonlar',
	'configuration' => 'Konfigürasyon', # MODIF
	'confirmation' => '<p>Devam etmek istediğinizden emin misiniz?</p><p>Yapılan değişiklikler geri alınamaz!!!',
	'contacts_personnels' => 'Kişisel bağlantılar',
	'contacts_pros' => 'Meslekî bağlantılar',
	'cp_valide' => 'Lütfen geçerli bir e-posta adresi giriniz',
	'creation' => 'Fişin oluşturulma tarihi',

	// D
	'decembre' => 'Aralık',
	'delete_user_select' => 'Supprimer le(s) utilisateur(s) sélectionné(s)', # NEW
	'demande_password' => 'Şifreniz :',
	'descriptif_plugin' => 'Vous trouverez ici tous les utilisateurs inscrits sur le site. Leur statut est indiqué par la couleur de leur icone.<br /><br />Vous pouvez configurer des champs supplémentaires, proposés en option aux visiteurs au moment de l\'inscription.', # NEW
	'description_cfg' => 'Bu eklenti sitenizin yazarlarına ait bilgileri genişletmeye olanak tanır, burada ek alanlar seçebilirsiniz', # MODIF
	'description_page' => 'Burada konfigürasyon tercihlerinizi kaydedildikleri gibi kontrol edebilirsiniz.',
	'divers' => 'Çeşitli',
	'domaine' => 'Alan ("Domaine")',
	'domaines' => 'Alan ("Domaine")',

	// E
	'editer_adherent' => 'Üye bilgilerini düzenle', # MODIF
	'effacement_auto_impossible' => 'Hesap otomatik olarak silinemez, bizle temas kurun.',
	'email' => 'E-Posta',
	'email_bonjour' => 'Günaydın @nom@,',
	'email_deja_enregistre' => 'Cette adresse email est déjà enregistrée. Utilisez le formulaire de connexion pour accéder à votre compte.', # NEW
	'email_obligatoire' => 'E-posta alanı zorunludur',
	'email_valide' => 'Lütfen geçerli bi re-posta giriniz',
	'erreur_reglement_obligatoire' => 'Vous devez accepter le règlement', # NEW
	'exp_divers' => 'Ziyaretçilere yorum ismiyle birlikte sunulacak alan',
	'exp_publication' => 'Kişisel bilgilerin yayınlanma izni',
	'exp_statut' => 'Yeni üyelere atamak istediğiniz durumu seçin',
	'exp_statut_rel' => 'SPIP durumundan farklı alan, bir kurumun içten kontrolü için kullanılır',
	'explication_affordance_form' => 'Champ affiché sur les formulaires d\'identification (#LOGIN_PUBLIC)', # NEW

	// F
	'fax' => 'Faks',
	'fax_pro' => 'Faks (İş)',
	'feminin' => 'Bayan',
	'fevrier' => 'Şubat',
	'fiche' => 'Fiş',
	'fiche_adherent' => 'Üye fişi', # MODIF
	'fiche_expl' => ' : Alan üye fişinde görünecek', # MODIF
	'fiche_mod' => 'Değiştirilebilir',
	'fiche_mod_expl' => ' : Alan, CRAYON eklentisi kullanması koşuluyla üye tarafından kamusal arayüzden değiştirilebilecek', # MODIF
	'fonction' => 'İşlev',
	'form' => 'Form',
	'form_expl' => ' : Alan INSCRIPTION2 formunda görünecek', # MODIF
	'form_oblig_expl' => ' : Formda girişi zorunlu kıl',
	'format' => 'Format',
	'formulaire_inscription' => 'Kayıt formu',
	'formulaire_inscription_ok' => 'Kaydınız sorunsuz biçimde alındı. Bağlantı için gerekli kullanıcı bilgilerinizi bir e-posta ile alacaksınız.',
	'formulaire_login_deja_utilise' => 'Bu kullanıcı ismi önceden alınmış, lütfen başka bir isim seçiniz.',
	'formulaire_remplir_obligatoires' => 'Lütfen zorunlu alanları doldurun',
	'formulaire_remplir_validation' => 'Lütfen onaylanmayan alanları kontrol edin.',

	// G
	'general_infos' => 'Genel bilgiler', # MODIF
	'geoloc' => 'Jeo-Yerelleştirme',
	'geomap_obligatoire' => 'Kişilerin jeo-yerelleştirilmesi bilgilerinin kullanılabilmesi için GoogleMapApi eklentisinin kurulması gerekli...',
	'gerent' => 'Üye',
	'gestion_adherent' => 'Üyelerin işletilmesi', # MODIF

	// H
	'html' => 'HTML',

	// I
	'icone_afficher_utilisateurs' => 'Afficher les utilisateurs', # NEW
	'icone_menu_config' => 'Inscription2', # MODIF
	'identification' => 'Identification', # NEW
	'info_connection' => 'Bağlantı bilgileri',
	'info_defaut' => 'Varsayılan bilgiler', # MODIF
	'info_defaut_desc' => 'Zorunlu, varsayılan bilgiler', # MODIF
	'info_gener' => 'Genel Bilgiler', # MODIF
	'info_gener_desc' => 'Sitenin yeni üyelerinden istenecek seçenekler',
	'info_internes' => 'İç bilgiler',
	'info_internes_desc' => 'Veritabanında saklanacak ama yeni üye formunda görünmeyecek seçenekler',
	'info_perso' => 'Kişisel bilgiler',
	'info_perso_desc' => 'Yeni üyelere sorulacak kişisel bilgiler',
	'info_pro' => 'İş bilgileri',
	'infos_adherent' => 'Ek bilgiler',
	'infos_complementaires' => 'Informations complémentaires', # NEW
	'infos_personnelles' => 'Kişisel bilgiler',
	'inscription2' => 'Inscription 2.0', # NEW
	'inscriptions' => 'Kayıtlar',
	'inserez_infos' => 'Lütfen istenen bilgileri girin',

	// J
	'janvier' => 'Ocak',
	'juillet' => 'Temmuz',
	'juin' => 'Haziran',

	// L
	'label_affordance_form' => 'Paramétrage des formulaires d\'identification', # NEW
	'label_public_reglement' => 'J\'ai lu et j\'accepte le règlement', # NEW
	'label_public_reglement_url' => 'J\'ai lu et j\'accepte le <a href="@url@" class="spip_in reglement">règlement</a>', # NEW
	'label_reglement' => 'Règlement à valider', # NEW
	'label_reglement_article' => 'Article original du site correspondant au règlement', # NEW
	'label_validation_numero_international' => 'Forcer les numéros de téléphone à être sous la forme internationale', # NEW
	'latitude' => 'Enlem',
	'legend_affordance_form' => 'Formulaire d\'identification', # NEW
	'legend_oubli_pass' => 'Pas de mot de passe / mot de passe oublié', # NEW
	'legend_reglement' => 'Règlement du site', # NEW
	'legend_validation' => 'Validations', # NEW
	'legende' => 'Lejand',
	'lisez_mail' => 'Verilen adrese bir e-posta gönderildi.Hesabınızı etkinleştirmek için lütfen talimatları izleyiniz.',
	'liste_adherents' => 'comptes_utilisateurs\'leri görüntüle', # MODIF
	'liste_comptes_titre' => 'Kullanıcı hesapları listesi', # MODIF
	'login' => 'Kullanıcı ismi (login)',
	'logo_auteur' => 'Logo', # NEW
	'longitude' => 'Boylam',

	// M
	'mai' => 'Mayıs',
	'mail_non_domaine' => 'Girdiğiniz e-posta adresi belirttiğiniz alana ait değil lütfen tekrar deneyiniz',
	'mail_renvoye' => 'Bu e-posta adresi önceden kaydedilmiş, e-postada belirtilen talimatları izleyerek hesabınızı etkinleştirin.',
	'mars' => 'Mart',
	'masculin' => 'Bay',
	'message_auto' => '(bu otomatik bir mesajdır)',
	'mobile' => 'Cep',
	'mobile_pro' => 'Cep (İş)',
	'modif_pass_titre' => 'Şifrenizi değiştirin',
	'moins_seconde' => 'moins d\'une seconde', # NEW
	'mot_passe_reste_identique' => 'Şifreniz değiştirilmedi.',

	// N
	'naissance' => 'Doğum tarihi',
	'nb_users_supprimes' => '@nb@ suppression(s) effectuée(s).', # NEW
	'nettoyer_tables' => 'Tabloları temizle',
	'no_user_selected' => 'Vous n\'avez sélectionné aucun utilisateur.', # NEW
	'nom' => 'İmza',
	'nom_explication' => 'isminiz veya  takma adınız',
	'nom_famille' => 'Soyadı',
	'nom_site' => 'Site ismi',
	'non_renseigne' => 'bildirilmedi.',
	'non_renseignee' => 'bildirilmedi.',
	'novembre' => 'Kasım',
	'numero_valide' => 'GLütfen geçerli bir sayı giriniz',
	'numero_valide_international' => 'Ce numéro doit être sous la forme internationale (ex: +32 475 123 456)', # NEW

	// O
	'octobre' => 'Ekim',

	// P
	'page_confirmation' => 'Kaydınızın onay sayfası',
	'par_defaut' => 'Bu alan zorunlu', # MODIF
	'pass' => 'Şifre',
	'pass_egal' => 'Lütfen bir öncekiyle aynı şifreyi girin.',
	'pass_indiquez_cidessous' => 'Aşağıdan kayıt olduğunuzda verdiğiniz e-posta adresini belirtin. Erişiminizi değiştirmek için gerekli adımları gösteren bir e-posta alacaksınız.',
	'pass_minimum' => 'Şifreniz en az 5 karakter içermelidir',
	'pass_oubli_mot' => 'Şifrenizin değiştirilmesi',
	'pass_rappel_email' => 'Rappel : votre adresse email est "@email@".', # NEW
	'pass_rappel_login_email' => 'Rappel : votre login est "@login@" et votre adresse email est "@email@".', # NEW
	'pass_recevoir_mail' => 'Siteye erişiminizi nasıl değiştirebileceğinizi belirten bir e-posta alacaksınız.',
	'password_obligatoire' => 'Şifre zorunludur.',
	'password_retaper' => 'Şifreyi onaylayın',
	'pays' => 'Ülke',
	'pays_defaut' => 'Pays par défaut', # NEW
	'pays_pro' => 'Ülke (İş)',
	'pgp' => 'PGP anahtarı',
	'prenom' => 'İsim',
	'probleme_email' => 'E-posta sorunu : etkinleştirmek e-postası gönderilemiyor.',
	'profession' => 'Meslek',
	'profil_droits_insuffisants' => 'Üzgünüm, bu yazarı değiştirme yetkiniz yok<br/>', # MODIF
	'profil_modifie_ok' => 'Profilinizde yaptıpınız değişiklikler dikkate alındı.',
	'publication' => 'Yayınlama',

	// R
	'raccourcis' => 'Kısa yollar',
	'rappel_login' => 'Hatırlatma : kullanıcı isminiz şöyle : ',
	'rappel_password' => 'Şifreniz',
	'recherche_case' => 'Arama alanı', # MODIF
	'recherche_utilisateurs' => 'kullanıcılarda arama', # MODIF
	'recherche_valeur' => 'Aranan içerik', # MODIF
	'redemande_password' => 'Şifrenizi tekrar girin :',
	'rien_a_faire' => 'Yapılacak bi rşey yok',

	// S
	'saisir_email_valide' => 'Lütfen geçerli bi re-posta adresi giriniz',
	'secondes' => 'secondes', # NEW
	'secteur' => 'Sektör',
	'septembre' => 'Eylül',
	'sexe' => 'Medenî durum',
	'societe' => 'Firma / Dernek ...',
	'statut' => 'Durum',
	'statut_rel' => 'İç durum',
	'statuts_actifs' => 'Les couleurs des icones correspondent aux statuts suivants : ', # NEW
	'suppression_faite' => 'Verileriniz silindi',
	'supprimer_adherent' => 'Üye sil', # MODIF
	'supprimer_adherent_red' => 'Sil',
	'surnom' => 'Takma isim',

	// T
	'table' => 'Tablo',
	'table_expl' => ' : alan üye listesinde görünecek (özel alan)', # MODIF
	'tel' => 'Tel.',
	'telephone' => 'Telefon',
	'telephone_pro' => 'Telefon (İş)',
	'texte' => 'Metin',
	'texte_email_confirmation' => 'Hesabınız etkin, şimdiden kişisel bilgilerinizi kullanarak siteye bağlanabilirsiniz.

Kullanıcı isminiz : @login@
ve şifrenizi de seçtiniz.

Güveniniz için teşekkürler

@nom_site@ ekibi
 @url_site@', # MODIF
	'texte_email_inscription' => '@nom_site@ sitesine kaydınızı onaylamak üzeresiniz. 

Hesabınızı etkinleştirmek ve bir şifre tanımlamak için aşağıdaki bağlantıya tıklayın.

@link_activation@


Güveninize teşekkürler.

@nom_site@ ekibi.
@url_site@

Bu kaydı siz talep etmediyseniz veya artık bu siteye üye olmak istemiyorsanız aşağıdaki bağlantıya tıklayınız.
@link_suppresion@


', # MODIF
	'titre_confirmation' => 'Onay',

	// U
	'un_an' => 'un an', # NEW
	'un_jour' => 'un jour', # NEW
	'un_mois' => 'un mois', # NEW
	'une_heure' => 'une heure', # NEW
	'une_minute' => 'une minute', # NEW
	'une_seconde' => 'une seconde', # NEW
	'url_site' => 'Site Url\'si',
	'url_societe' => 'Firma sitesi',

	// V
	'validite' => 'Geçerlilik tarihi',
	'ville' => 'şehir',
	'ville_pro' => 'Şehir (İş)',
	'visiteur' => 'Ziyaretçi',
	'vos_contacts_personnels' => 'Kişisel bağlantılar',
	'votre_adresse' => 'İş adresiniz',
	'votre_adresse_pro' => 'Adresiniz (İş)',
	'votre_login_mail' => 'Votre login ou email :', # NEW
	'votre_mail' => 'E-postanız :',
	'votre_nom_complet' => 'Tam isminiz',
	'votre_profil' => 'Profiliniz',

	// W
	'website' => 'İnternet sitesi'
);

?>
