<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/inscription2/inscription2_2_0/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_confirmer' => 'À confirmer',
	'accesrestreint' => 'Accès Restreint',
	'aconfirmer' => 'À confirmer',
	'action_adherent' => 'Opérations',
	'activation_compte' => 'Activez votre compte',
	'adherents' => 'Utilistateurs',
	'admin' => 'Admin',
	'admin_modifier_auteur_supp' => 'Informations supplémentaires',
	'adresse' => 'Adresse',
	'adresse_pro' => 'Adresse professionnelle (Pro.)',
	'afficher_tous' => 'Afficher tous les utilisateurs',
	'ajouter_adherent' => 'Créer un nouvel utilisateur',
	'ans' => 'ans',
	'aout' => 'août',
	'aucun_resultat_recherche' => 'Il n\'y a aucun résultat pour votre recherche.',
	'auteur' => 'Auteur',
	'autre' => 'Autre',
	'avril' => 'avril',

	// B
	'bio' => 'Biographie',

	// C
	'caracteres' => 'Caractères interdits !',
	'categorie' => 'Catégorie de cotisation',
	'chaine_valide' => 'Veuillez insérer une chaine de caractère',
	'chainelettre' => '(composée uniquement de lettres)',
	'chainenombre' => '(composée de lettres et/ou de chiffres)',
	'champ_obligatoire' => 'Ce champ est obligatoire',
	'choisir_categories' => 'Choisissez-les ci-dessous :',
	'choisir_nouveau_password' => 'Vous pourrez choisir un nouveau mot de passe en cliquant le lien suivant',
	'choisir_zones' => 'Veuillez choisir les zones auxquelles vous voulez que les nouveaux adhérents soient affectés',
	'choisissez' => 'Choisissez',
	'choix_affordance_email' => 'Email',
	'choix_affordance_login' => 'Login (par défaut dans SPIP)',
	'choix_affordance_login_email' => 'Login et email',
	'choix_domaine' => 'Veuillez choisir votre domaine',
	'civilite' => 'Civilité',
	'code_postal' => 'Code Postal',
	'code_postal_pro' => 'Code Postal (Pro.)',
	'commentaire' => 'Commentaire',
	'compte_active' => 'Votre compte sur @nom_site@',
	'compte_efface' => 'Votre compte a été effacé.',
	'conf_plugin' => 'Configuration Inscription 2.0',
	'conf_plugin_page' => 'Configuration stockée du plugin Inscription 2.0',
	'configs' => 'Configurations',
	'configuration' => 'Configuration des champs des utilisateurs',
	'confirmation' => '<p>Êtes vous sûr(e) de vouloir continuer?</p><p>Toutes les modifications seront irreversibles!!!</p>',
	'contacts_personnels' => 'Contacts personnels',
	'contacts_pros' => 'Contacts professionnels',
	'cp_valide' => 'Veuillez insérer un code postal valide',
	'creation' => 'Date de création de la fiche',

	// D
	'decembre' => 'décembre',
	'delete_user_select' => 'Supprimer le(s) utilisateur(s) sélectionné(s)',
	'demande_password' => 'Votre mot de passe :',
	'descriptif_plugin' => 'Vous trouverez ici tous les utilisateurs inscrits sur le site. Leur statut est indiqué par la couleur de leur icone.<br /><br />Vous pouvez configurer des champs supplémentaires, proposés en option aux visiteurs au moment de l\'inscription.',
	'description_cfg' => 'Paramétrer les champs supplémentaires pour les utilisateurs',
	'description_page' => 'Ici vous pouvez vérifier vos choix de configuration tels qu\'ils sont stockés',
	'divers' => 'Divers',
	'domaine' => 'Domaine',
	'domaines' => 'Domaine',

	// E
	'editer_adherent' => 'Éditer utilisateur',
	'effacement_auto_impossible' => 'Le compte ne peut être effacé automatiquement, contactez-nous.',
	'email' => 'E-Mail',
	'email_bonjour' => 'Bonjour @nom@,',
	'email_deja_enregistre' => 'Cette adresse email est déjà enregistrée. Utilisez le formulaire de connexion pour accéder à votre compte.',
	'email_obligatoire' => 'Le champ email est obligatoire',
	'email_valide' => 'Veuillez insérer un email valide',
	'erreur_reglement_obligatoire' => 'Vous devez accepter le règlement',
	'exp_divers' => 'Champ qui sera proposé aux visiteurs avec le nom de commentaire',
	'exp_publication' => 'Autorisation de publication des données personnelles',
	'exp_statut' => 'Choisissez le statut que vous voulez attribuer aux nouveaux adhérents',
	'exp_statut_rel' => 'Champ différent du statut de SPIP, celui-ci sert pour le controle interne d\'une institution',
	'explication_affordance_form' => 'Champ affiché sur les formulaires d\'identification (#LOGIN_PUBLIC)',

	// F
	'fax' => 'Fax',
	'fax_pro' => 'Fax professionnel',
	'feminin' => 'Madame',
	'fevrier' => 'février',
	'fiche' => 'Fiche',
	'fiche_adherent' => 'Fiche utilisateur',
	'fiche_expl' => ' : Le champ sera visible sur la fiche de l\'utilisateur (espace privé)',
	'fiche_mod' => 'Modifiable',
	'fiche_mod_expl' => ' : Le champ sera modifiable depuis l\'interface publique par l\'utilisateur à condition d\'utiliser le plugin #CRAYONS',
	'fonction' => 'Fonction',
	'form' => 'Formulaire',
	'form_expl' => ' : Le champ sera affiché sur le formulaire #INSCRIPTION2',
	'form_oblig_expl' => ' : Rendre la saisie obligatoire dans le formulaire',
	'format' => 'Format',
	'formulaire_inscription' => 'Formulaire d\'inscription',
	'formulaire_inscription_ok' => 'Votre inscription a bien été prise en compte. Vous allez recevoir par courrier électronique vos identifiants de connexion.',
	'formulaire_login_deja_utilise' => 'Le login est déja utilisé, veuillez en choisir un autre.',
	'formulaire_remplir_obligatoires' => 'Veuillez remplir les champs obligatoires',
	'formulaire_remplir_validation' => 'Veuillez vérifier les champs qui ne sont pas validés.',

	// G
	'general_infos' => 'Informations générales',
	'geoloc' => 'Géolocalisation',
	'geomap_obligatoire' => 'Pour utiliser les données de géolocalisation des personnes, il est nécessaire d\'installer le plugin GoogleMapApi...',
	'gerent' => 'Adhérent',
	'gestion_adherent' => 'Gestion des utilisateurs',

	// H
	'html' => 'HTML',

	// I
	'icone_afficher_utilisateurs' => 'Afficher les utilisateurs',
	'icone_menu_config' => 'Configuration avancée des utilisateurs',
	'identification' => 'Identification',
	'info_connection' => 'Informations de connexion',
	'info_defaut' => 'Informations obligatoires',
	'info_defaut_desc' => 'Possibilités de paramétrage',
	'info_gener' => 'Informations générales',
	'info_gener_desc' => 'Options qui seront demandées aux nouveaux adhérents du site',
	'info_internes' => 'Informations internes',
	'info_internes_desc' => 'Options qui seront stockées dans la base de données mais ne seront pas affichées dans le formulaire des nouveaux adhérents',
	'info_perso' => 'Informations personnelles',
	'info_perso_desc' => 'Informations personnelles qui seront demandées aux nouveaux adhérents du site',
	'info_pro' => 'Informations professionnelles',
	'infos_adherent' => 'Informations supplémentaires',
	'infos_complementaires' => 'Informations complémentaires',
	'infos_personnelles' => 'Informations personnelles',
	'inscription2' => 'Inscription 2.0',
	'inscriptions' => 'Inscriptions',
	'inserez_infos' => 'Veuillez saisir les informations demandées',

	// J
	'janvier' => 'janvier',
	'juillet' => 'juillet',
	'juin' => 'juin',

	// L
	'label_affordance_form' => 'Paramétrage des formulaires d\'identification',
	'label_public_reglement' => 'J\'ai lu et j\'accepte le règlement',
	'label_public_reglement_url' => 'J\'ai lu et j\'accepte le <a href="@url@" class="spip_in reglement">règlement</a>',
	'label_reglement' => 'Règlement à valider',
	'label_reglement_article' => 'Article original du site correspondant au règlement',
	'label_validation_numero_international' => 'Forcer les numéros de téléphone à être sous la forme internationale',
	'latitude' => 'Latitude',
	'legend_affordance_form' => 'Formulaire d\'identification',
	'legend_oubli_pass' => 'Pas de mot de passe / mot de passe oublié',
	'legend_reglement' => 'Règlement du site',
	'legend_validation' => 'Validations',
	'legende' => 'Légende',
	'lisez_mail' => 'Un email vient d\'être envoyé à l\'adresse fournie. Pour activer votre compte veuillez suivre les instructions.',
	'liste_adherents' => 'Voir la liste des utilisateurs',
	'liste_comptes_titre' => 'Liste des utilisateurs',
	'login' => 'Nom d\'utilisateur (login)',
	'logo_auteur' => 'Logo',
	'longitude' => 'Longitude',

	// M
	'mai' => 'mai',
	'mail_non_domaine' => 'L\'adresse email que vous avez donnée n\'appartient pas au domaine que vous avez indiqué veuillez essayer à nouveau',
	'mail_renvoye' => 'Cette adresse email a déjà été enregistrée, veuillez activer votre compte en suivant les instructions contenues dans le mail.',
	'mars' => 'mars',
	'masculin' => 'Monsieur',
	'message_auto' => '(ceci est un message automatique)',
	'mobile' => 'Mobile',
	'mobile_pro' => 'Mobile professionnel',
	'modif_pass_titre' => 'Modifier votre mot de passe',
	'moins_seconde' => 'moins d\'une seconde',
	'mot_passe_reste_identique' => 'Votre mot de passe n\'a pas été modifié.',

	// N
	'naissance' => 'Date de naissance',
	'nb_users_supprimes' => '@nb@ suppression(s) effectuée(s).',
	'nettoyer_tables' => 'Nettoyer tables',
	'no_user_selected' => 'Vous n\'avez sélectionné aucun utilisateur.',
	'nom' => 'Signature',
	'nom_explication' => 'votre nom ou votre pseudo',
	'nom_famille' => 'Nom de famille',
	'nom_site' => 'Nom du site',
	'non_renseigne' => 'non renseigné.',
	'non_renseignee' => 'non renseignée.',
	'novembre' => 'novembre',
	'numero_valide' => 'Veuillez insérer un numéro valide',
	'numero_valide_international' => 'Ce numéro doit être sous la forme internationale (ex: +32 475 123 456)',

	// O
	'octobre' => 'octobre',

	// P
	'page_confirmation' => 'Page de confirmation de votre inscription',
	'par_defaut' => 'Ce champ est obligatoire',
	'pass' => 'Mot de passe',
	'pass_egal' => 'Veuillez entrer le même mot de passe que précédemment.',
	'pass_indiquez_cidessous' => 'Indiquez ci-dessous l\'adresse email sous laquelle vous
			vous êtes précédemment enregistré. Vous
			recevrez un email vous indiquant la marche à suivre pour
			modifier votre accès.',
	'pass_minimum' => 'Votre mot de passe doit comporter au moins 5 caractères',
	'pass_oubli_mot' => 'Changement de votre mot de passe',
	'pass_rappel_email' => 'Rappel : votre adresse email est "@email@".',
	'pass_rappel_login_email' => 'Rappel : votre login est "@login@" et votre adresse email est "@email@".',
	'pass_recevoir_mail' => 'Vous allez recevoir un email vous indiquant comment modifier votre accès au site.',
	'password_obligatoire' => 'Le mot de passe est obligatoire.',
	'password_retaper' => 'Confirmez le mot de passe',
	'pays' => 'Pays',
	'pays_defaut' => 'Pays par défaut',
	'pays_pro' => 'Pays (Pro.)',
	'pgp' => 'Clé PGP',
	'prenom' => 'Prénom',
	'probleme_email' => 'Problème de mail : l\'email d\'activation ne peut pas être envoyé.',
	'profession' => 'Profession',
	'profil_droits_insuffisants' => 'Desolé vous n\'avez pas le droit de modifier cet auteur<br />',
	'profil_modifie_ok' => 'Les modifications de votre profil ont bien été prises en compte.',
	'publication' => 'Publication',

	// R
	'raccourcis' => 'Raccourcis',
	'rappel_login' => 'Rappel : votre identifiant est : ',
	'rappel_password' => 'Votre mot de passe',
	'recherche_case' => 'Dans le champ :',
	'recherche_utilisateurs' => 'Rechercher un utilisateur',
	'recherche_valeur' => 'Rechercher :',
	'redemande_password' => 'Réinserez votre mot de passe :',
	'rien_a_faire' => 'Rien à faire',

	// S
	'saisir_email_valide' => 'Veuillez saisir une adresse email valide',
	'secondes' => 'secondes',
	'secteur' => 'Secteur',
	'septembre' => 'septembre',
	'sexe' => 'Civilité',
	'societe' => 'Société / Association ...',
	'statut' => 'Statut',
	'statut_rel' => 'Statut interne',
	'statuts_actifs' => 'Les couleurs des icones correspondent aux statuts suivants : ',
	'suppression_faite' => 'La suppression de vos données a été effectuée',
	'supprimer_adherent' => 'Supprimer utilisateurs',
	'supprimer_adherent_red' => 'Sup',
	'surnom' => 'Surnom',

	// T
	'table' => 'Table',
	'table_expl' => ' : Le champ sera affiché sur la liste des utilisateurs (espace privé)',
	'tel' => 'Tél.',
	'telephone' => 'Téléphone',
	'telephone_pro' => 'Téléphone professionnel',
	'texte' => 'Texte',
	'texte_email_confirmation' => 'Votre compte est actif, vous pouvez dès maintenant vous connecter au site en utilisant vos identifiants personnels.

Votre login est : @login@
et vous venez de choisir votre mot de passe.

Merci de votre confiance

L\'équipe de @nom_site@
@url_site@',
	'texte_email_inscription' => 'vous êtes sur le point de confirmer votre inscription au site @nom_site@.

Cliquer le lien ci-dessous pour activer votre compte et choisir votre mot de passe.

@link_activation@


Merci de votre confiance.

L\'équipe de @nom_site@.
@url_site@

Si vous n\'avez pas demandé cette inscription ou si vous ne voulez plus faire partie de notre site, cliquez le lien ci-dessous.
@link_suppresion@


',
	'titre_confirmation' => 'Confirmation',

	// U
	'un_an' => 'un an',
	'un_jour' => 'un jour',
	'un_mois' => 'un mois',
	'une_heure' => 'une heure',
	'une_minute' => 'une minute',
	'une_seconde' => 'une seconde',
	'url_site' => 'Url du site',
	'url_societe' => 'Site société',

	// V
	'validite' => 'Date de validité',
	'ville' => 'Ville',
	'ville_pro' => 'Ville (Pro.)',
	'visiteur' => 'Visiteur',
	'vos_contacts_personnels' => 'Vos contacts personnels',
	'votre_adresse' => 'Votre adresse personnelle',
	'votre_adresse_pro' => 'Votre adresse (professionnelle)',
	'votre_login_mail' => 'Votre login ou email :',
	'votre_mail' => 'Votre email :',
	'votre_nom_complet' => 'Votre nom complet',
	'votre_profil' => 'Votre profil',

	// W
	'website' => 'Site Internet'
);

?>
