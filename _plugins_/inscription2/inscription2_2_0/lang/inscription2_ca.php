<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/inscription2?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_confirmer' => 'A confirmar',
	'accesrestreint' => 'Accés Restringit',
	'aconfirmer' => 'A confirmar',
	'action_adherent' => 'Operacions',
	'activation_compte' => 'Activeu el vostre compte',
	'adherents' => 'Usuaris',
	'admin' => 'Admin',
	'admin_modifier_auteur_supp' => 'Informacions suplementàries',
	'adresse' => 'Adreça',
	'adresse_pro' => 'Adreça professional (Pro.)',
	'afficher_tous' => 'Mostrar tots els usuaris',
	'ajouter_adherent' => 'Crear un nou usuari',
	'ans' => 'anys',
	'aout' => 'agost',
	'aucun_resultat_recherche' => 'La vostra cerca no ha donat cap resultat.',
	'auteur' => 'Autor',
	'autre' => 'Altre',
	'avril' => 'abril',

	// B
	'bio' => 'Biografia',

	// C
	'caracteres' => 'Caràcters prohibits!',
	'categorie' => 'Categoria de cotització',
	'chaine_valide' => 'Vulgueu inserir una cadena de caràcter',
	'chainelettre' => '(formada només per lletres)',
	'chainenombre' => '(formada per lletres i/o xifres)',
	'champ_obligatoire' => 'Aquest camp és obligatori',
	'choisir_categories' => 'Escolliu-les a sota:',
	'choisir_nouveau_password' => 'Podreu escollir una nova contrasenya clicant al següent enllaç',
	'choisir_zones' => 'Escolliu les zones a les que voleu que els nous adherents estiguin assignats',
	'choisissez' => 'Escolliu',
	'choix_affordance_email' => 'Correu electrònic ',
	'choix_affordance_login' => 'Login (per defecte a SPIP)',
	'choix_affordance_login_email' => 'Login i correu electrònic',
	'choix_domaine' => 'Escolliu el vostre domini',
	'civilite' => 'Estat civil',
	'code_postal' => 'Codi Postal',
	'code_postal_pro' => 'Codi Postal (Pro.)',
	'commentaire' => 'Comentari',
	'compte_active' => 'El vostre compte a @nom_site@',
	'compte_efface' => 'El vostre compte ha estat esborrat.',
	'conf_plugin' => 'Configuració Inscripció 2.0',
	'conf_plugin_page' => 'Configuració emmagatzemada del plugin Inscripció 2.0',
	'configs' => 'Configuracions',
	'configuration' => 'Configuració dels camps dels usuaris',
	'confirmation' => '<p>Esteu segurs(es) que voleu continuar?</p><p>Totes les modificacions seran irreversibles!!!</p>',
	'contacts_personnels' => 'Contactes personals',
	'contacts_pros' => 'Contactes professionals',
	'cp_valide' => 'Vulgueu inserir un codi postal vàlid',
	'creation' => 'Data de creació de la fitxa',

	// D
	'decembre' => 'desembre',
	'delete_user_select' => 'Suprimir el(s) usuari(s) seleccionats ',
	'demande_password' => 'La vostra contrasenya:',
	'descriptif_plugin' => 'Trobareu aquí tots els usuaris inscrits al lloc. El seu estatus està indicat pel color de la seva icona.<br /><br />Podeu configurar camps suplementaris, proposats en opció als visitants al moment de la inscripció.',
	'description_cfg' => 'Parametritzar els camps suplementaris pels usuaris',
	'description_page' => 'Aquí podeu verificar la vostra configuració escollida tal i com està emmagatzemada',
	'divers' => 'Divers',
	'domaine' => 'Domini',
	'domaines' => 'Domini',

	// E
	'editer_adherent' => 'Editar usuari',
	'effacement_auto_impossible' => 'El compte no es pot esborrar automàticament, contacteu-nos.',
	'email' => 'Correu electrònic',
	'email_bonjour' => 'Bon dia @nom@,',
	'email_deja_enregistre' => 'Aquest correu electrònic ja està registrat. Utilitzeu el formulari de connexió per accedir al vostre compte.',
	'email_obligatoire' => 'El camp correu electrònic és obligatori',
	'email_valide' => 'Vulgueu introduir un correu electrònic vàlid',
	'erreur_reglement_obligatoire' => 'Heu d\'acceptar el reglament',
	'exp_divers' => 'Camp que serà proposat als visitants amb el nom de comentari',
	'exp_publication' => 'Autorització de publicació de les dades personals',
	'exp_statut' => 'Escolliu l\'estat que voleu atribuir als nous adherents',
	'exp_statut_rel' => 'Camp diferent del estat d\'SPIP, aquest serveix pel control intern d\'una institució',
	'explication_affordance_form' => 'Camp mostrat als formularis d\'identificació (#LOGIN_PUBLIC)',

	// F
	'fax' => 'Fax',
	'fax_pro' => 'Fax professional',
	'feminin' => 'Senyora',
	'fevrier' => 'febrer',
	'fiche' => 'Fitxa',
	'fiche_adherent' => 'Fitxa usuari',
	'fiche_expl' => ': El camp serà visible a la fitxa de l\'usuari (espai privat)',
	'fiche_mod' => 'Modificable',
	'fiche_mod_expl' => ': El camp serà modificable des de la interfície pública per l\'usuari sempre i quan utilitzi el plugin #CRAYONS',
	'fonction' => 'Funció',
	'form' => 'Formulari',
	'form_expl' => ': El camp es mostrarà al formulari #INSCRIPTION2',
	'form_oblig_expl' => ': Fer que la introducció sigui obligatòria al formulari',
	'format' => 'Format',
	'formulaire_inscription' => 'Formulari d\'inscripció',
	'formulaire_inscription_ok' => 'La vostra inscripció s\'ha tingut en compte. Rebreu, per correu electrònic, els vostres identificadors de connexió.',
	'formulaire_login_deja_utilise' => 'L\'usuari ja s\'utilitza, escolliu-ne un altre.',
	'formulaire_remplir_obligatoires' => 'Ompliu els camps obligatoris',
	'formulaire_remplir_validation' => 'Verifiqueu els camps que no són vàlids.',

	// G
	'general_infos' => 'Informacions generals',
	'geoloc' => 'Geolocalització',
	'geomap_obligatoire' => 'Per utilitzar les dades de geolocalització de persones, és necessari instal·lar el plugin GoogleMapApi...',
	'gerent' => 'Adherent',
	'gestion_adherent' => 'Gestió d\'usuaris',

	// H
	'html' => 'HTML',

	// I
	'icone_afficher_utilisateurs' => 'Mostrar els usuaris',
	'icone_menu_config' => 'Configuració avançada d\'usuaris',
	'identification' => 'Identificació',
	'info_connection' => 'Informacions de connexió',
	'info_defaut' => 'Informacions obligatòries',
	'info_defaut_desc' => 'Possibilitat de configuració',
	'info_gener' => 'Informacions generals',
	'info_gener_desc' => 'Opcions que es demanaran als nous adherents del lloc',
	'info_internes' => 'Informacions internes',
	'info_internes_desc' => 'Opcions que seran emmagatzemades a la base de dades però que no seran mostrades en el formulari dels nous adherents',
	'info_perso' => 'Informacions personals',
	'info_perso_desc' => 'Informacions personals que es demanaran als nous adherents del lloc',
	'info_pro' => 'Informacions professionals',
	'infos_adherent' => 'Informacions suplementàries',
	'infos_complementaires' => 'Informacions complementàries',
	'infos_personnelles' => 'Informacions personals',
	'inscription2' => 'Inscripció 2.0',
	'inscriptions' => 'Inscripcions',
	'inserez_infos' => 'Introduïu les informacions demanades',

	// J
	'janvier' => 'gener',
	'juillet' => 'juliol',
	'juin' => 'juny',

	// L
	'label_affordance_form' => 'Configuració dels formularis d\'identificació',
	'label_public_reglement' => 'He llegit i accepto el reglament',
	'label_public_reglement_url' => 'He llegit i accepto el <a href="@url@" class="spip_in reglement">reglament</a>',
	'label_reglement' => 'Reglament a validar',
	'label_reglement_article' => 'Article original del lloc corresponent al reglament',
	'label_validation_numero_international' => 'Forçar que els números de telèfon tinguin format internacional',
	'latitude' => 'Latitud',
	'legend_affordance_form' => 'Formulari d\'identificació',
	'legend_oubli_pass' => 'Sense contrasenya / contrasenya oblidada ',
	'legend_reglement' => 'Reglament del lloc',
	'legend_validation' => 'Validacions',
	'legende' => 'Llegenda',
	'lisez_mail' => 'Us acabem d\'enviar un correu electrònic a l\'adreça donada. Per activar el vostre compte seguiu les instruccions.',
	'liste_adherents' => 'Veure la llista d\'usuaris',
	'liste_comptes_titre' => 'Llista d\'usuaris',
	'login' => 'Nom d\'usuari (login)',
	'logo_auteur' => 'Logo',
	'longitude' => 'Longitud ',

	// M
	'mai' => 'maig',
	'mail_non_domaine' => 'L\'adreça de correu que heu donat no pertany al domini que heu indicat. Proveu-ho altre cop',
	'mail_renvoye' => 'Aquesta adreça de correu ja ha estat registrada. Activeu el vostre compte seguint les instruccions que hi ha al correu electrònic.',
	'mars' => 'març',
	'masculin' => 'Senyor',
	'message_auto' => '(aquest és un missatge automàtic)',
	'mobile' => 'Mòbil',
	'mobile_pro' => 'Mòbil professional',
	'modif_pass_titre' => 'Modificar la vostra contrasenya',
	'moins_seconde' => 'menys d\'un segon',
	'mot_passe_reste_identique' => 'La vostra contrasenya no s\'ha modificat.',

	// N
	'naissance' => 'Data de naixement',
	'nb_users_supprimes' => '@nb@ supressions realitzades.',
	'nettoyer_tables' => 'Netejar les taules',
	'no_user_selected' => 'No heu seleccionat cap usuari.',
	'nom' => 'Signatura',
	'nom_explication' => 'el vostre nom o el vostre pseudo',
	'nom_famille' => 'Cognom',
	'nom_site' => 'Nom del lloc',
	'non_renseigne' => 'no informat.',
	'non_renseignee' => 'no informada.',
	'novembre' => 'novembre',
	'numero_valide' => 'Introduïu un número vàlid',
	'numero_valide_international' => 'Aquest número ha de tenir format internacional (ex: +34 475 123 456)',

	// O
	'octobre' => 'octubre',

	// P
	'page_confirmation' => 'Pàgina de confirmació de la vostra inscripció',
	'par_defaut' => 'Aquest camp és obligatori',
	'pass' => 'Contrasenya',
	'pass_egal' => 'Entreu la mateixa contrasenya que abans.',
	'pass_indiquez_cidessous' => 'Indiqueu a sota l\'adreça de correu amb la que us
   heu registrat anteriorment.
   Rebreu un correu electrònic indicant-vos què heu de fer per
   modificar el vostre accés.',
	'pass_minimum' => 'La vostra contrasenya ha de tenir com a mínim 5 caràcters',
	'pass_oubli_mot' => 'Canvi de la vostra contrasenya',
	'pass_rappel_email' => 'Recorda: la vostra adreça de correu és "@email@".',
	'pass_rappel_login_email' => 'Recorda: el vostre login és "@login@" i el vostre correu electrònic és "@email@".',
	'pass_recevoir_mail' => 'Rebreu un correu electrònic indicant-vos com modificar el vostre accés al lloc.',
	'password_obligatoire' => 'La contrasenya és obligatòria.',
	'password_retaper' => 'Confirmeu la contrasenya',
	'pays' => 'País',
	'pays_defaut' => 'País per defecte',
	'pays_pro' => 'País (Pro.)',
	'pgp' => 'Clau PGP',
	'prenom' => 'Nom',
	'probleme_email' => 'Problema de correu: el correu d\'activació no es pot enviar.',
	'profession' => 'Professió',
	'profil_droits_insuffisants' => 'Ho sentin però no teniu el dret de modificar aquest autor<br />',
	'profil_modifie_ok' => 'Les modificacions del vostre perfil s\'han tingut en compte.',
	'publication' => 'Publicació',

	// R
	'raccourcis' => 'Dreceres',
	'rappel_login' => 'Recorda: el vostre identificador és: ',
	'rappel_password' => 'La vostra contrasenya',
	'recherche_case' => 'En el camp: ',
	'recherche_utilisateurs' => 'Cercar un usuari',
	'recherche_valeur' => 'Cercar:',
	'redemande_password' => 'Torneu a introduir la vostra contrasenya:',
	'rien_a_faire' => 'Res a fer',

	// S
	'saisir_email_valide' => 'Introduïu una adreça de correu vàlida ',
	'secondes' => 'segons',
	'secteur' => 'Sector',
	'septembre' => 'setembre ',
	'sexe' => 'Estat civil',
	'societe' => 'Societat / Associació...',
	'statut' => 'Estat',
	'statut_rel' => 'Estat intern',
	'statuts_actifs' => 'Els colors de les icones corresponen als següents estatus: ',
	'suppression_faite' => 'La supressió de les vostres dades ha estat feta',
	'supprimer_adherent' => 'Suprimir usuaris',
	'supprimer_adherent_red' => 'Sup',
	'surnom' => 'Sobrenom',

	// T
	'table' => 'Taula',
	'table_expl' => ': El camp es mostrarà a la llista d\'usuaris (espai privat)',
	'tel' => 'Tel. ',
	'telephone' => 'Telèfon',
	'telephone_pro' => 'Telèfon professional ',
	'texte' => 'Text',
	'texte_email_confirmation' => 'El vostre compte està actiu, podeu, a partir d\'ara, connectar-vos al lloc utilitzant els vostres identificadors personals.

El vostre login és: @login@
i acabeu d\'escollir la vostra contrasenya.

Gràcies per la vostra confiança

L\'equip de @nom_site@
@url_site@',
	'texte_email_inscription' => 'esteu a punt de confirmar la vostra inscripció al lloc @nom_site@.

Cliqueu l\'enllaç que hi ha més avall per activar el vostre compte i escollir la vostra contrasenya.

@link_activation@


Gràcies per la vostra confiança.

L\'equip de @nom_site@.
@url_site@

Si no heu demanat aquesta inscripció o si no voleu, a partir d\'ara, formar part del nostre lloc, cliqueu l\'enllaç que hi ha a sota.
@link_suppresion@


',
	'titre_confirmation' => 'Confirmació',

	// U
	'un_an' => 'un any',
	'un_jour' => 'un dia',
	'un_mois' => 'un mes',
	'une_heure' => 'una hora',
	'une_minute' => 'un minut',
	'une_seconde' => 'un segon',
	'url_site' => 'URL del lloc',
	'url_societe' => 'Lloc associat',

	// V
	'validite' => 'Data de validesa',
	'ville' => 'Ciutat',
	'ville_pro' => 'Ciutat (Pro.)',
	'visiteur' => 'Visitant',
	'vos_contacts_personnels' => 'Els vostres contactes personals',
	'votre_adresse' => 'La vostra adreça personal',
	'votre_adresse_pro' => 'La vostra adreça (professional)',
	'votre_login_mail' => 'El vostre login o correu electrònic:',
	'votre_mail' => 'El vostre correu electrònic:',
	'votre_nom_complet' => 'El vostre nom complet',
	'votre_profil' => 'El vostre perfil',

	// W
	'website' => 'Lloc Internet'
);

?>
