<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/inscription2?lang_cible=gl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_confirmer' => 'Para confirmar',
	'accesrestreint' => 'Acceso restrinxido',
	'aconfirmer' => 'Para confirmar',
	'action_adherent' => 'Operacións',
	'activation_compte' => 'Active a súa conta',
	'adherents' => 'Usuarios',
	'admin' => 'Admin',
	'admin_modifier_auteur_supp' => 'Información complementaria',
	'adresse' => 'Enderezo',
	'adresse_pro' => 'Enderezo profesional (Pro.)',
	'afficher_tous' => 'Mostrar todos os usuarios',
	'ajouter_adherent' => 'Crear un novo usuario',
	'ans' => 'anos',
	'aout' => 'agosto',
	'aucun_resultat_recherche' => 'Non hai ningún resultado da súa busca.',
	'auteur' => 'Autor',
	'autre' => 'Outro',
	'avril' => 'abril',

	// B
	'bio' => 'Biografía',

	// C
	'caracteres' => 'caracteres', # MODIF
	'categorie' => 'Categoria de cotización',
	'chaine_valide' => 'Introduza unha cadea de caracteres',
	'chainelettre' => '(composta unicamente de letras)',
	'chainenombre' => '(composta de letras e/ou cifras)',
	'champ_obligatoire' => 'Este campo é obrigado',
	'choisir_categories' => 'Escolla a seguir:',
	'choisir_nouveau_password' => 'Pode escoller un novo contrasinal premendo a ligazón seguinte',
	'choisir_zones' => 'Escolla as zonas ás que os novos adherentes serán afectados',
	'choisissez' => 'Escolla',
	'choix_affordance_email' => 'Correo',
	'choix_affordance_login' => 'Login (predefinido en SPIP)',
	'choix_affordance_login_email' => 'Login e correo',
	'choix_domaine' => 'Escolla o seu dominio',
	'civilite' => 'Civilidade',
	'code_postal' => 'Código postal',
	'code_postal_pro' => 'Código postal (Prof.)',
	'commentaire' => 'Comentario',
	'compte_active' => 'A súa conta en @nom_site@',
	'compte_efface' => 'A súa conta foi eliminada.',
	'conf_plugin' => 'Configuración de Inscription2',
	'conf_plugin_page' => 'Configuración gardada do módulo Inscription2',
	'configs' => 'Configuracións',
	'configuration' => 'Configuración dos campos de usuarios',
	'confirmation' => '<p>Está segurod de querer continuar?</p><p>Todas as modificacións serán irreversibles!!!</p>',
	'contacts_personnels' => 'Contactos persoais',
	'contacts_pros' => 'Contactos profesionais',
	'cp_valide' => 'Insira un código postal válido',
	'creation' => 'Data de creación da ficha',

	// D
	'decembre' => 'decembro',
	'delete_user_select' => 'Suprimir o(s) usuario(s) seleccionado(s)',
	'demande_password' => 'O seu contrasinal:',
	'descriptif_plugin' => 'Encontrará aquí todos os usuarios inscritos no web. O seu status indícase coa cor da súa icona. <br /><br />Pode configurar os campos suplementarios, propostos opcionalmente aos visitantes no proceso de inscrición.',
	'description_cfg' => 'Parametrizar os campos suplementarios para os usuarios',
	'description_page' => 'Aquí pode comprobar as súas eleccións de configuración tal e como está gardada',
	'divers' => 'Diversos',
	'domaine' => 'Dominio',
	'domaines' => 'Dominio',

	// E
	'editer_adherent' => 'Editar o usuario',
	'effacement_auto_impossible' => 'A conta non se pode borrar automaticamente, contacte con nós.',
	'email' => 'Correo-e',
	'email_bonjour' => 'Bos días @nom@,',
	'email_deja_enregistre' => 'Este enderezo de correo xa está rexistrado.Utilice o formulario de conexión para acceder á súa conta.',
	'email_obligatoire' => 'O campo de correo-e é obrigado',
	'email_valide' => 'Introduza un enderezo de correo válido',
	'erreur_reglement_obligatoire' => 'Debe aceptar as condicións',
	'exp_divers' => 'Campo que será proposto aos visitantes co nome de comentario',
	'exp_publication' => 'Autorización para publicación de datos persoais',
	'exp_statut' => 'Escolla o estatuto que quere atribuírlles aos novos adherentes',
	'exp_statut_rel' => 'Campo diferente ao do estatuto de SPIP, este serve para o control interno dunha institución',
	'explication_affordance_form' => 'Campo mostrado nos formularios de identificación (#LOGIN_PUBLIC)',

	// F
	'fax' => 'Fax',
	'fax_pro' => 'Fax profesional',
	'feminin' => 'Señora',
	'fevrier' => 'febreiro',
	'fiche' => 'Ficha',
	'fiche_adherent' => 'Ficha de usuario',
	'fiche_expl' => ': O campo será visible na ficha do usuario (espazo privado)',
	'fiche_mod' => 'Modificable',
	'fiche_mod_expl' => ': O campo será modificable desde a interface pública polo usuario coa condición de empregar o módulo CRAYONS',
	'fonction' => 'Función',
	'form' => 'Formulario',
	'form_expl' => ': O campo será mostrado no formulario #INSCRIPTION2',
	'form_oblig_expl' => ' : Deixar a saída obrigada no formulario',
	'format' => 'Formato',
	'formulaire_inscription' => 'Formulairo de inscrición',
	'formulaire_inscription_ok' => 'A súa inscrición foi correctamente recibida. Vai recibir por correo electrónico os seus identificadores de conexión.',
	'formulaire_login_deja_utilise' => 'O login xa está en uso, escolla outro.',
	'formulaire_remplir_obligatoires' => 'Cubra os campos obrigados',
	'formulaire_remplir_validation' => 'Verificar os campos que non son validados.',

	// G
	'general_infos' => 'Datos xerais',
	'geoloc' => 'Xeolocalización',
	'geomap_obligatoire' => 'Para usar os datos de xeolocalización de persoas, cómpre instalar o módulo GoogleMapApi...',
	'gerent' => 'Adherente',
	'gestion_adherent' => 'Xestión de usuarios',

	// H
	'html' => 'HTML',

	// I
	'icone_afficher_utilisateurs' => 'Mostrar os usuarios',
	'icone_menu_config' => 'Configuración avanzada dos usuarios',
	'identification' => 'Identificación',
	'info_connection' => 'Datos de conexión',
	'info_defaut' => 'Datos obrigados',
	'info_defaut_desc' => 'Posibilidades de parametrización',
	'info_gener' => 'Datos xerais',
	'info_gener_desc' => 'Opcións que serán solicitadas aos novos adherentes do web',
	'info_internes' => 'Datos internos',
	'info_internes_desc' => 'Opcións que serán gardadas na base de datos pero que non serán mostradas no formulario dos novos adherentes',
	'info_perso' => 'Datos persoais',
	'info_perso_desc' => 'Datos persoais que serán solicitados aos novos adherentes do web',
	'info_pro' => 'Datos profesionais',
	'infos_adherent' => 'Datos complementarios',
	'infos_complementaires' => 'Información complementaria',
	'infos_personnelles' => 'Datos persoais',
	'inscription2' => 'Inscription 2.0',
	'inscriptions' => 'Inscricións',
	'inserez_infos' => 'Proporcione os datos solicitados',

	// J
	'janvier' => 'xaneiro',
	'juillet' => 'xullo',
	'juin' => 'xuño',

	// L
	'label_affordance_form' => 'Parametrización de formularios de identificación',
	'label_public_reglement' => 'Lin e acepto as condicións',
	'label_public_reglement_url' => 'Lin e acepto o <a href="@url@" class="spip_in reglement">as condicións</a>',
	'label_reglement' => 'Condicións para validar',
	'label_reglement_article' => 'Artigo orixinal do web correspondente ás condicións',
	'label_validation_numero_international' => 'Forzar os números de teléfono de modo que teñan a forma internacional',
	'latitude' => 'Latitude',
	'legend_affordance_form' => 'Formulario de identificación',
	'legend_oubli_pass' => 'Sen contrasinal / contrasinal esquecido',
	'legend_reglement' => 'Condicións do web',
	'legend_validation' => 'Validacións',
	'legende' => 'Lenda',
	'lisez_mail' => 'Acábase de enviar un correo ao enderezo proporcionado. Para activar a súa conta, siga as instrucións.',
	'liste_adherents' => 'Ver a lista de usuarios',
	'liste_comptes_titre' => 'Lista de usuarios',
	'login' => 'Nome de usuario (login)',
	'logo_auteur' => 'Logo',
	'longitude' => 'Lonxitude',

	// M
	'mai' => 'maio',
	'mail_non_domaine' => 'O enderezo de correo que proporcionou non pertence ao dominio que indicou; probe con outro diferente',
	'mail_renvoye' => 'Este enderezo xa foi rexistrado, active a súa conta seguindo as instrucións que contén a mensaxe de correo.',
	'mars' => 'marzo',
	'masculin' => 'Señor',
	'message_auto' => '(esta é unha mensaxe automática)',
	'mobile' => 'Móbil',
	'mobile_pro' => 'Móbil profesional',
	'modif_pass_titre' => 'Modificar o seu contrasinal',
	'moins_seconde' => 'menos dun segundo',
	'mot_passe_reste_identique' => 'O seu contrasinal non foi modificado.',

	// N
	'naissance' => 'Data de nacemento',
	'nb_users_supprimes' => '@nb@ supresión(s) efectuada(s).',
	'nettoyer_tables' => 'Limpar as táboas',
	'no_user_selected' => 'Non seleccionou a ningún usuario.',
	'nom' => 'Sinatura',
	'nom_explication' => 'O seu nome ou alcume',
	'nom_famille' => 'Apelido',
	'nom_site' => 'Nome do web',
	'non_renseigne' => 'non consignado.',
	'non_renseignee' => 'non informado',
	'novembre' => 'novembro',
	'numero_valide' => 'Introduza un número válido',
	'numero_valide_international' => 'Este número debe ter a forma internacional (ex: +32 475 123 456)',

	// O
	'octobre' => 'outubro',

	// P
	'page_confirmation' => 'Páxina de confirmación da súa inscrición',
	'par_defaut' => 'Este campo é obrigado',
	'pass' => 'Contrasinal',
	'pass_egal' => 'Introduza o mesmo contrasinal que antes.',
	'pass_indiquez_cidessous' => 'Indique seguidamente o enderezo de correo-e co cal tivese
 o rexistro anteriormente.
   Recibirá un correo no que se lle indicarán os pasos que debe seguir para
   modificar o seu acceso.',
	'pass_minimum' => 'O seu contrasinal debe ter cando menos  5 caracteres',
	'pass_oubli_mot' => 'Cambio do seu contrasinal',
	'pass_rappel_email' => 'Recordatorio: o seu enderezo de correo é "@email@".',
	'pass_rappel_login_email' => 'Recordatorio : o seu login é "@login@" e o seu enderezo de correo é "@email@".',
	'pass_recevoir_mail' => 'Recibirá un correo no que se lle indicará como modificar o seu acceso ao web.',
	'password_obligatoire' => 'O contrasinal é obrigado.',
	'password_retaper' => 'Confirme o contrasinal.',
	'pays' => 'País',
	'pays_defaut' => 'País predeterminado',
	'pays_pro' => 'País (Prof.)',
	'pgp' => 'Chave PGP',
	'prenom' => 'Nome',
	'probleme_email' => 'Hai un problema co correo : a mensaxe de correo para a activación non pode ser enviada.',
	'profession' => 'Profesión',
	'profil_droits_insuffisants' => 'Desculpe, non ten permisos para modificar este autor<br />',
	'profil_modifie_ok' => 'As modificacións do seu perfil foron recibidas correctamente.',
	'publication' => 'Publicación',

	// R
	'raccourcis' => 'Atallos',
	'rappel_login' => 'Rappel : o seu identificador é : ',
	'rappel_password' => 'O seu contrasinal',
	'recherche_case' => 'No campo :',
	'recherche_utilisateurs' => 'Buscar un usuario',
	'recherche_valeur' => 'Buscar:',
	'redemande_password' => 'Volva a introducir o seu contrasinal:',
	'rien_a_faire' => 'Nada que facer',

	// S
	'saisir_email_valide' => 'Dea un enderezo de correo válido',
	'secondes' => 'segundos',
	'secteur' => 'Sector',
	'septembre' => 'setembro',
	'sexe' => 'Estado civil',
	'societe' => 'Sociedade / Asociación ...',
	'statut' => 'Estatuto',
	'statut_rel' => 'Estatuto interno',
	'statuts_actifs' => 'As cores das iconas correspondentes aos status seguintes:',
	'suppression_faite' => 'A supresión dos seus datos foi realizada',
	'supprimer_adherent' => 'Suprimir os usuarios',
	'supprimer_adherent_red' => 'Sup',
	'surnom' => 'Sobrenome',

	// T
	'table' => 'Táboa',
	'table_expl' => ': O campo será mostrado na lista de usuarios (no espazo privado)',
	'tel' => 'Tél.',
	'telephone' => 'Teléfono',
	'telephone_pro' => 'Teléfono profesional',
	'texte' => 'Texto',
	'texte_email_confirmation' => 'A súa conta está activa, desde agora pode conectarse ao web e usar os seus identificadores persoais.

O seu login é : @login@
e acaba de escoller o contrasinal.

Agradecémoslle a súa confianza

O equipo de @nom_site@@url_site@',
	'texte_email_inscription' => 'Vai confirmar a súa inscrición no noso web @nom_site@. 

Prema a ligazón seguinte par activar a súa conta e escoller o seu contrasinal.

@link_activation@


Agradecémoslle a súa confianza.

O equipo de @nom_site@@url_site@

Se vostede non solicitouesta inscrición ou se non quereparticipar no noso sitio, prema,a ligazón seguinte.
@link_suppresion@


',
	'titre_confirmation' => 'Confirmación',

	// U
	'un_an' => 'un ano',
	'un_jour' => 'un día',
	'un_mois' => 'un mes',
	'une_heure' => 'unha hora',
	'une_minute' => 'un minuto',
	'une_seconde' => 'un segundo',
	'url_site' => 'Url do web',
	'url_societe' => 'Web da sociedade',

	// V
	'validite' => 'Data de validación',
	'ville' => 'Vila',
	'ville_pro' => 'Vila (Prof.)',
	'visiteur' => 'Visitante',
	'vos_contacts_personnels' => 'Os seus contactos profesionais',
	'votre_adresse' => 'O seu enderezo profesional',
	'votre_adresse_pro' => 'O seu enderezo (profesional)',
	'votre_login_mail' => 'O seu login ou correo :',
	'votre_mail' => 'O seu correo :',
	'votre_nom_complet' => 'O seu nome completo',
	'votre_profil' => 'O seu perfil',

	// W
	'website' => 'Web'
);

?>
