<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/inscription2?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_confirmer' => 'Zu best&auml;tigen',
	'accesrestreint' => 'Eingeschr&auml;nkter Zugang',
	'aconfirmer' => 'Zu bestätigen',
	'action_adherent' => 'Aktionen',
	'activation_compte' => 'Aktivieren Sie Ihr Konto',
	'adherents' => 'Nutzer',
	'admin' => 'Admin',
	'admin_modifier_auteur_supp' => 'Weiterführende Informationen',
	'adresse' => 'Adresse',
	'adresse_pro' => 'Adresse (beruflich)',
	'afficher_tous' => 'Alle Nutzer anzeigen',
	'ajouter_adherent' => 'Neuen Nutzer anlegen',
	'ans' => 'Jahre',
	'aout' => 'August',
	'aucun_resultat_recherche' => 'Nichts gefunden',
	'auteur' => 'Autor',
	'autre' => 'Andere',
	'avril' => 'April',

	// B
	'bio' => 'Biographie',

	// C
	'caracteres' => 'Unzulässige Zeichen!',
	'categorie' => 'Beitragsart',
	'chaine_valide' => 'Bitte fügen Sie eine Zeichenkette ein',
	'chainelettre' => '(besteht nur aus Buchstaben)',
	'chainenombre' => '(besteht aus Buchstaben und/oder Zahlen)',
	'champ_obligatoire' => 'Pflichtfeld',
	'choisir_categories' => 'Bitte hier auswählen:',
	'choisir_nouveau_password' => 'Klicken Sie auf den folgenden Link, um ein neues Passwort zu wählen.',
	'choisir_zones' => 'Bitte wählen Sie die Bereiche, zu denen neue Mitglieder gehören sollen.',
	'choisissez' => 'Auswählen',
	'choix_affordance_email' => 'E-Mail',
	'choix_affordance_login' => 'Login (SPIP-Standard)',
	'choix_affordance_login_email' => 'Login und E-Mail',
	'choix_domaine' => 'Bitte wählen Sie ihre Domain',
	'civilite' => 'Anrede',
	'code_postal' => 'Postleitzahl',
	'code_postal_pro' => 'Postleitzahl (beruflich)',
	'commentaire' => 'Kommentar',
	'compte_active' => 'Ihr Konto wurde aktiviert',
	'compte_efface' => 'Ihr Konto wurde gelöscht',
	'conf_plugin' => 'Konfiguration von Inscription 2.0',
	'conf_plugin_page' => 'Gespeicherte Konfiguration des Plugin Inscription 2.0',
	'configs' => 'Konfigurationen',
	'configuration' => 'Konfiguration der Nutzereinträge',
	'confirmation' => '<p>Wollen Sie wirklich fortfahren?</p><p>Alle Änderungen sind unwiederrufbar!!!</p>',
	'contacts_personnels' => 'Persönliche Kontakte',
	'contacts_pros' => 'Berufliche Kontakte',
	'cp_valide' => 'Bitte geben Sie eine gültige Postleitzahl ein',
	'creation' => 'Eintrag angelegt am',

	// D
	'decembre' => 'Dezember',
	'delete_user_select' => 'Ausgewählte Nutzer löschen',
	'demande_password' => 'Ihr Passwort:',
	'descriptif_plugin' => 'Hier sehen sie alle bei der Website eingeschriebenen Nutzer. Ihr Status wird die Farbe ihre Icons angezeigt.<br /><br />Sie können zusätzliche Felder einstellen, die den Nutzern bei der Anmeldung zum Ausfüllen angezeigt werden.',
	'description_cfg' => 'Zusatzfelder für Nutzer konfigurieren',
	'description_page' => 'Hier können Sie Ihre gespeicherten Einstellungen überprüfen.',
	'divers' => 'Verschiedenes',
	'domaine' => 'Domaine',
	'domaines' => 'Domain',

	// E
	'editer_adherent' => 'Nutzer bearbeiten',
	'effacement_auto_impossible' => 'Der Zugang kann nicht automatische gelöscht werden. Bitte nehmen sie mit uns Kontakt auf.',
	'email' => 'Email',
	'email_bonjour' => 'Hallo @nom@,',
	'email_deja_enregistre' => 'Diese E-Mail existiert bereits. Bitte verwende sie das Login-Formular, um zu ihrem Konto zu gelangen.',
	'email_obligatoire' => 'Email ist Pflichtfeld',
	'email_valide' => 'Bitte geben Sie eine gültige Mailadresse ein',
	'erreur_reglement_obligatoire' => 'Sie müssen die Nutzungsbedingungen anerkennen.',
	'exp_divers' => 'Dieses Feld steht den Besuchern für Kommentare zur Verfügung.',
	'exp_publication' => 'Genehmigung der Veröffentlichung der persönlichen Daten',
	'exp_statut' => 'Wählen Sie den Status für neue Mitglieder',
	'exp_statut_rel' => 'Das Feld enthält einen anderen Status als den von SPIP, damit eine Institution eigene Berechtigungen vergeben kann.',
	'explication_affordance_form' => 'Feld wird im Anmeldeformular (#LOGIN_PUBLIC) angezeigt',

	// F
	'fax' => 'Fax',
	'fax_pro' => 'Fax (beruflich)',
	'feminin' => 'Frau',
	'fevrier' => 'Februar',
	'fiche' => 'Eintrag',
	'fiche_adherent' => 'Nutzerseite',
	'fiche_expl' => ' : Das Feld wird auf der Nutzerseite im Redaktionssystem angezeigt',
	'fiche_mod' => 'Änderung möglich',
	'fiche_mod_expl' => ' : Das Feld kann vom Mitglied im öffentlichen Bereich geändert werden, wenn das Plugin #CRAYONS (Der Stift) installiert ist.',
	'fonction' => 'Funktion',
	'form' => 'Formular',
	'form_expl' => ' : Das Feld wird im Formular INSCRIPTION2 angezeigt',
	'form_oblig_expl' => ' : Pflichtfeld des Formulars',
	'format' => 'Format',
	'formulaire_inscription' => 'Anmeldeformular',
	'formulaire_inscription_ok' => 'Ihre Anmeldung wurde gespeichert. Sie erhalten nun eine Mail mit Ihren Verbindungsdaten.',
	'formulaire_login_deja_utilise' => 'Diese Bezeichnung wird bereits verwendet. Bitte wählen Sie eine andere.',
	'formulaire_remplir_obligatoires' => 'Bitte füllen Sie die Pflichtfelder aus.',
	'formulaire_remplir_validation' => 'Bitte überprüfen sie die ungültigen Felder.',

	// G
	'general_infos' => 'Allgemeine Informationen',
	'geoloc' => 'Geografische Lokalisierung',
	'geomap_obligatoire' => 'Um Daten zur Lokalisierung von Personen zu verwenden, wird das Plugin GoogleMapApi benötigt.',
	'gerent' => 'Mitglied',
	'gestion_adherent' => 'Nutzerverwaltung',

	// H
	'html' => 'HTML',

	// I
	'icone_afficher_utilisateurs' => 'Nutzer anzeigen',
	'icone_menu_config' => 'Erweiterte Nutzerverwaltung',
	'identification' => 'Identifikation',
	'info_connection' => 'Verbindungsinformationen',
	'info_defaut' => 'Obligatorische Angaben',
	'info_defaut_desc' => 'Einstellungen',
	'info_gener' => 'Allgemeine Informationen',
	'info_gener_desc' => 'Optionen, die von den neuen Mitgliedern des web site verlangt werden',
	'info_internes' => 'Interne Informationen',
	'info_internes_desc' => 'Options qui seront stockées dans la base de données mais ne seront pas affichées dans le formulaire des nouveaux adhérents',
	'info_perso' => 'Persönliche Informationen',
	'info_perso_desc' => 'Persönliche Informationen, die von bei neuen Nutzern der Website abgefragt werden',
	'info_pro' => 'Berufliche Angaben',
	'infos_adherent' => 'Zusatzinformationen',
	'infos_complementaires' => 'Zusatzinformationen',
	'infos_personnelles' => 'Neue persönliche Angaben',
	'inscription2' => 'Inscription 2.0',
	'inscriptions' => 'Anmeldungen',
	'inserez_infos' => 'Tragen Sie bitte die Angaben ein',

	// J
	'janvier' => 'Januar',
	'juillet' => 'Juli',
	'juin' => 'Juni',

	// L
	'label_affordance_form' => 'Einstellungen der Anmeldeformulare',
	'label_public_reglement' => 'Ich habe die Nutzungsbedingungen gelesen und akzeptiert.',
	'label_public_reglement_url' => 'Ich habe die <a href="@url@" class="spip_in reglement">Nutzungsbedingungen</a> gelesen und akzeptiert.',
	'label_reglement' => 'Nutzungsbedingungen bestätigen',
	'label_reglement_article' => 'Artikel, der die Nutzungsbedingungen enthält',
	'label_validation_numero_international' => 'Internationales Format für Telefonnummern erzwingen',
	'latitude' => 'Geographische Breite',
	'legend_affordance_form' => 'Loginformular',
	'legend_oubli_pass' => 'Kein Paßwort / Paßwort vergessen',
	'legend_reglement' => 'Nutzungsbedingungen',
	'legend_validation' => 'Bestätigungen',
	'legende' => 'Legende',
	'lisez_mail' => 'An die eingetragene Adresse wurde eine Email geschickt. Sie enthält die zur Aktivierung Ihres Kontos erforderlichen Informationen.',
	'liste_adherents' => 'Nutzerliste anzeigen',
	'liste_comptes_titre' => 'Nutzerliste',
	'login' => 'Benutzername (login)',
	'logo_auteur' => 'Logo',
	'longitude' => 'Geographische Länge',

	// M
	'mai' => 'Mai',
	'mail_non_domaine' => 'L\'adresse email que vous avez donnée n\'appartient pas au domaine que vous avez indiqué vueillez essayer à nouveau',
	'mail_renvoye' => 'Cette adresse email a déjà été enregistrée, veuillez activer votre compte en suivant les instructions contenues dans le mail.',
	'mars' => 'März',
	'masculin' => 'Herr',
	'message_auto' => '(Dies ist eine automatische Mitteilung)',
	'mobile' => 'Funktelefon',
	'mobile_pro' => 'Funktelefon (beruflich)',
	'modif_pass_titre' => 'Passwort ändern',
	'moins_seconde' => 'unter einer Sekunde',
	'mot_passe_reste_identique' => 'Ihr Passwort wurde geändert',

	// N
	'naissance' => 'Geburtstag',
	'nb_users_supprimes' => '@nb@ Nutzer gelöscht.',
	'nettoyer_tables' => 'Tabellen aufräumen',
	'no_user_selected' => 'Sie haben kein Mitglied ausgewählt',
	'nom' => 'Pseudonym',
	'nom_explication' => 'Ihr Name oder Pseudonym',
	'nom_famille' => 'Name',
	'nom_site' => 'Name er Website',
	'non_renseigne' => 'Keine Angabe',
	'non_renseignee' => 'Keine Angabe',
	'novembre' => 'November',
	'numero_valide' => 'Bitte geben Sie eine gültige Zahl ein',
	'numero_valide_international' => 'Die Telefonnummer muß im internationalen Format engegeben werden (z.B. +49 30 12345678)',

	// O
	'octobre' => 'Oktober',

	// P
	'page_confirmation' => 'Seite zur Bestätigung Ihrer Anmeldung',
	'par_defaut' => 'Pflichtfeld',
	'pass' => 'Passwort',
	'pass_egal' => 'Bitte wiederholen Sie das Passwort.',
	'pass_indiquez_cidessous' => 'Bitte geben Sie die Mailadresse an,
mit der Sie sich eingetragen haben.
Sie erhalten dann eine Nachricht, die Ihnen
sagt, wie Sie vorgehen müssen,
um Ihre Zugangsdaten zu ändern.',
	'pass_minimum' => 'Ihr Passwort muss mindestens 5 Zeichen lang sein.',
	'pass_oubli_mot' => 'Passwort ändern',
	'pass_rappel_email' => 'Erinnerung: Ihre E-Mail-Adresse ist "@email@".',
	'pass_rappel_login_email' => 'Erinnerung: Ihr Login ist "@login@" und ihre E-Mail-Adresse ist "@email@".',
	'pass_recevoir_mail' => 'Sie erhalten nun eine Mail mit Informationen zum Ändern Ihrer Zugangsdaten.',
	'password_obligatoire' => 'Das Passwort ist erforderlich.',
	'password_retaper' => 'Passwort bestätigen',
	'pays' => 'Land',
	'pays_defaut' => 'Standardland',
	'pays_pro' => 'Land (beruflich)',
	'pgp' => 'PGP Schlüssel',
	'prenom' => 'Vorname',
	'probleme_email' => 'Mailproblem: Die Mail zum Aktivieren des Kontos konnte nicht verschickt werden.',
	'profession' => 'Beruf',
	'profil_droits_insuffisants' => 'Sie dürfen diesen Autor nicht bearbeiten<br/>',
	'profil_modifie_ok' => 'Die Änderung Ihres Profils wurden gespeichert.',
	'publication' => 'Veröffentlichung',

	// R
	'raccourcis' => 'Schnellzugang',
	'rappel_login' => 'Erinnerung - Ihr Login lautet: ',
	'rappel_password' => 'Ihr Passwort',
	'recherche_case' => 'Im Feld:',
	'recherche_utilisateurs' => 'Redakteure suchen',
	'recherche_valeur' => 'Suchen:',
	'redemande_password' => 'Geben Sie Ihr Passwort erneut ein:',
	'rien_a_faire' => 'Keien Aktion erforderlich',

	// S
	'saisir_email_valide' => 'Bitte geben sie eine gültige E-Mail Adresse an.',
	'secondes' => 'Sekunden',
	'secteur' => 'Sektor',
	'septembre' => 'September',
	'sexe' => 'Anrede/Geschlecht',
	'societe' => 'Firma / Verein ...',
	'statut' => 'Status',
	'statut_rel' => 'Interner Status',
	'statuts_actifs' => 'Die Farbend der Icons entsprechen diesem Status:',
	'suppression_faite' => 'Ihr Daten wurden gelöscht.',
	'supprimer_adherent' => 'Nutzer löschen',
	'supprimer_adherent_red' => 'löschen',
	'surnom' => 'Spitzname',

	// T
	'table' => 'Liste',
	'table_expl' => ': Das Feld wird in der Nutzerliste angezeigt (Redaktionssystem)',
	'tel' => 'Tel.',
	'telephone' => 'Telefon',
	'telephone_pro' => 'Telefon (beruflich)',
	'texte' => 'Text',
	'texte_email_confirmation' => 'Ihr Konto ist aktiviert. Sie können sich jetzt mit Ihren persönlichen Zugangsdaten einloggen.n

Ihr Login ist : @login@
Ihr Passwort haben Sie soeben eingegeben.

Vielen Dank für Ihr Vertrauen

Das @nom_site@-Team',
	'texte_email_inscription' => 'Sie sind dabei, ihre Anmeldung bei der Website @nom_site@ abzuschließen.
Bitte klicken Sie auf den Link weiter unten klicken und legen sie ihr Passwort fest.

@link_activation@


Vielen Dank für Ihr Vertrauen.

Das @nom_site@-Team
@url_site@

Wenn Sie sich nicht angemeldet haben, klicken Sie bitte auf den Link weiter unten, um alle erfassten Daten zu löschen.n
@link_suppresion@


',
	'titre_confirmation' => 'Bestätigung',

	// U
	'un_an' => 'ein Jahr',
	'un_jour' => 'ein Tag',
	'un_mois' => 'ein Monat',
	'une_heure' => 'eine Stunde',
	'une_minute' => 'eine Minute',
	'une_seconde' => 'eine Sekunde',
	'url_site' => 'URL der Website',
	'url_societe' => 'Website (der Firma/Verein)',

	// V
	'validite' => 'Gültigkeitsdatum',
	'ville' => 'Stadt',
	'ville_pro' => 'Stadt (beruflich)',
	'visiteur' => 'Besucher',
	'vos_contacts_personnels' => 'Ihre persönlichen Kontakte',
	'votre_adresse' => 'Ihre persönliche Adresse',
	'votre_adresse_pro' => 'Ihre berufliche Adresse',
	'votre_login_mail' => 'Ihre E-Mail oder Login:',
	'votre_mail' => 'Ihr E-Mail:',
	'votre_nom_complet' => 'Ihr vollständiger Name',
	'votre_profil' => 'Ihr Profil',

	// W
	'website' => 'Website'
);

?>
