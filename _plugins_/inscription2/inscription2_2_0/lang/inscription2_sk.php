<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/inscription2?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_confirmer' => 'Na potvrdenie',
	'accesrestreint' => 'Obmedzený prístup',
	'aconfirmer' => 'Treba potvrdiť',
	'action_adherent' => 'Operácie',
	'activation_compte' => 'Aktivujte si účet',
	'adherents' => 'Členovia',
	'admin' => 'Administrátor',
	'admin_modifier_auteur_supp' => 'Informácie naviac',
	'adresse' => 'Adresa',
	'adresse_pro' => 'Adresa do práce',
	'afficher_tous' => 'Zobraziť členov',
	'ajouter_adherent' => 'Pridať nového člena',
	'ans' => 'rokov',
	'aout' => 'August',
	'aucun_resultat_recherche' => 'Vašej požiadavke nezodpovedá žiaden výsledok.',
	'auteur' => 'Autor',
	'autre' => 'Ostatní',
	'avril' => 'Apríl',

	// B
	'bio' => 'Životopis',

	// C
	'caracteres' => 'Zakázané znaky!',
	'categorie' => 'Kategória príspevku',
	'chaine_valide' => 'Prosím, vložte reťazec',
	'chainelettre' => '(má len písmená)',
	'chainenombre' => '(obsahuje písmená a/lebo číslice)',
	'champ_obligatoire' => 'Toto pole je povinné',
	'choisir_categories' => 'Vyberte ich v tomto zozname:',
	'choisir_nouveau_password' => 'Ak kliknete na tento odkaz, budete si môcť zvoliť nové heslo',
	'choisir_zones' => 'Vyberte oblasti, ku ktorým budú noví členovia priradení',
	'choisissez' => 'Vybrať',
	'choix_affordance_email' => 'E-mail',
	'choix_affordance_login' => 'Prihlasovacie meno (predvolené na SPIPE)',
	'choix_affordance_login_email' => 'Prihlasovacie meno a e-mail',
	'choix_domaine' => 'Vyberte si doménu',
	'civilite' => 'Oslovenie',
	'code_postal' => 'PSČ',
	'code_postal_pro' => 'PSČ',
	'commentaire' => 'Komentáre',
	'compte_active' => 'Váš účet na @nom_site@',
	'compte_efface' => 'Váš účet bol zrušený.',
	'conf_plugin' => 'Nastavenia Inscription 2.0',
	'conf_plugin_page' => 'Nastavenia zásuvného modulu Inscription 2.0 boli uložené',
	'configs' => 'Nastavenia',
	'configuration' => 'Nastavenie používateľských polí',
	'confirmation' => '<p>Naozaj chcete pokračovať?</p><p>Zmeny sa nedajú odvolať!</p>',
	'contacts_personnels' => 'Osobné kontakty',
	'contacts_pros' => 'Pracovné kontakty',
	'cp_valide' => 'Zadajte, prosím, platné PSČ',
	'creation' => 'Dátum vytvorenia záznamu',

	// D
	'decembre' => 'December',
	'delete_user_select' => 'Odstrániť vybraných používateľov',
	'demande_password' => 'Vaše heslo:',
	'descriptif_plugin' => 'Tu sú všetci členovia redakčného tímu stránky. Ich funkcia je označená farbou ich ikony.<br /><br />Môžete nastaviť ďalšie polia, ktoré budú ponúknuté návštevníkom, keď sa zaregistrujú.',
	'description_cfg' => 'Nastavenie ďalších používateľských polí',
	'description_page' => 'Tu si môžete skontrolovať svoje uložené nastavenia',
	'divers' => 'Rôzne',
	'domaine' => 'Doména',
	'domaines' => 'Domény',

	// E
	'editer_adherent' => 'Upraviť používateľa',
	'effacement_auto_impossible' => 'Účet nemožno sledovať automaticky. Prosím, napíšte nám.',
	'email' => 'E-mail',
	'email_bonjour' => 'Dobrý deň, @nom@,',
	'email_deja_enregistre' => 'Táto e-mailová adresa je už zaregistrovaná. Prosím, použite prihlasovací formulár, aby ste sa dostali k svojmu účtu.',
	'email_obligatoire' => 'E-mailová adresa je povinná',
	'email_valide' => 'Prosím, zadajte platnú e-mailovú adresu',
	'erreur_reglement_obligatoire' => 'Musíte akceptovať pravidlá stránky',
	'exp_divers' => 'Field which will be offered to the visitors with the name Comments',
	'exp_publication' => 'Povolenie na publikovanie osobných údajov',
	'exp_statut' => 'Vyberte funkciu pre nových používateľov',
	'exp_statut_rel' => 'This field is different from the SPIP status, this one is used for the internal control of an institution',
	'explication_affordance_form' => 'Polia, ktoré sa použijú v prihlasovacom formulári (#LOGIN_PUBLIC)',

	// F
	'fax' => 'Fax',
	'fax_pro' => 'Fax do práce',
	'feminin' => 'Slečna/Pani',
	'fevrier' => 'Február',
	'fiche' => 'Záznam',
	'fiche_adherent' => 'Záznam používateľa',
	'fiche_expl' => ': Display this field  on member\'s record (backoffice)',
	'fiche_mod' => 'Môže sa upraviť',
	'fiche_mod_expl' => ': Member can modify this field from public interface, if the #CRAYONS plugin is active ',
	'fonction' => 'Funkcia',
	'form' => 'Formulár',
	'form_expl' => ': Display this field on #INSCRIPTION2 form',
	'form_oblig_expl' => ': povinné polia formulára',
	'format' => 'Formát',
	'formulaire_inscription' => 'Registračný formulár',
	'formulaire_inscription_ok' => 'Your registration has been noted. You will receive your login information by email.',
	'formulaire_login_deja_utilise' => 'This login is already in use. Please choose another.',
	'formulaire_remplir_obligatoires' => 'Prosím, vyplňte povinné polia',
	'formulaire_remplir_validation' => 'Please check the fields that are not valid',

	// G
	'general_infos' => 'General information',
	'geoloc' => 'Geographical positioning',
	'geomap_obligatoire' => 'To use the geographical positioning data for people, you need to install the GoogleMapApi plugin',
	'gerent' => 'Member',
	'gestion_adherent' => 'Správa používateľov',

	// H
	'html' => 'HTML',

	// I
	'icone_afficher_utilisateurs' => 'Zobraziť používateľov',
	'icone_menu_config' => 'Members\' advanced configuration',
	'identification' => 'Identifikácia',
	'info_connection' => 'Prihlasovacie údaje',
	'info_defaut' => 'Povinné údaje',
	'info_defaut_desc' => 'Configuration options',
	'info_gener' => 'General information',
	'info_gener_desc' => 'Options which new members will be asked about',
	'info_internes' => 'Interné informácie',
	'info_internes_desc' => 'Options which will be stored in the database but will not be displayed in the form for new members',
	'info_perso' => 'Osobné údaje',
	'info_perso_desc' => 'Personal information requested of the new members of the site',
	'info_pro' => 'Údaje o zamestnaní',
	'infos_adherent' => 'Extra information',
	'infos_complementaires' => 'Additional information',
	'infos_personnelles' => 'Osobné údaje',
	'inscription2' => 'Inscription 2.0',
	'inscriptions' => 'Prihlásenia',
	'inserez_infos' => 'Please fill in the requested information',

	// J
	'janvier' => 'január',
	'juillet' => 'júl',
	'juin' => 'jún',

	// L
	'label_affordance_form' => 'Login form configuration',
	'label_public_reglement' => 'I red and I accept site\'s rules',
	'label_public_reglement_url' => 'I red and I accept <a href="@url@" class="spip_in reglement">site\'s rules</a>',
	'label_reglement' => 'Accept site\'s rules',
	'label_reglement_article' => 'Site\'s rules article',
	'label_validation_numero_international' => 'Require international format for telephone numbers',
	'latitude' => 'Zemepisná šírka',
	'legend_affordance_form' => 'Prihlasovací formulár',
	'legend_oubli_pass' => 'No password set / password forgotten',
	'legend_reglement' => 'Site\'s rules',
	'legend_validation' => 'Potvrdenia',
	'legende' => 'Caption',
	'lisez_mail' => 'An email has been just sent to the address provided. To activate your account please follow the instructions.',
	'liste_adherents' => 'Zobraziť zoznam používateľov',
	'liste_comptes_titre' => 'Zoznam používateľov',
	'login' => 'Používateľské meno (login)',
	'logo_auteur' => 'Logo',
	'longitude' => 'Zemepisná dĺžka',

	// M
	'mai' => 'máj',
	'mail_non_domaine' => 'The email that you entered does not belong to the chosen domain, please try again',
	'mail_renvoye' => 'This email address was already recorded. Please activate your account following the instructions contained in the email.',
	'mars' => 'marec',
	'masculin' => 'Pán',
	'message_auto' => '(toto je automatická správa)',
	'mobile' => 'Mobilný telefón',
	'mobile_pro' => 'Mobilný telefón do práce',
	'modif_pass_titre' => 'Zmeniť si heslo',
	'moins_seconde' => 'menej ako sekundu',
	'mot_passe_reste_identique' => 'Your password has not been changed.',

	// N
	'naissance' => 'Dátum narodenia',
	'nb_users_supprimes' => '@nb@ user(s) have been deleted',
	'nettoyer_tables' => 'Vymazať tabuľky',
	'no_user_selected' => 'Please select a user',
	'nom' => 'Podpis',
	'nom_explication' => 'your name or nickname',
	'nom_famille' => 'Priezvisko',
	'nom_site' => 'Názov stránky',
	'non_renseigne' => 'empty.',
	'non_renseignee' => 'empty.',
	'novembre' => 'november',
	'numero_valide' => 'Please give a valid number',
	'numero_valide_international' => 'Telephone numbers must comply with international format (ex: +32 475 123 456)',

	// O
	'octobre' => 'október',

	// P
	'page_confirmation' => 'Potvrdenie registrácie',
	'par_defaut' => 'Toto pole je povinné',
	'pass' => 'Heslo',
	'pass_egal' => 'Please enter the same password as earlier.',
	'pass_indiquez_cidessous' => 'Give the email address with which you were previously registered.
You will receive and email telling you how to change your login.',
	'pass_minimum' => 'Your password must be at least 5 characters in length',
	'pass_oubli_mot' => 'Zmeniť si heslo',
	'pass_rappel_email' => 'Na pripomenutie: vaša e-mailová adresa je "@email@".',
	'pass_rappel_login_email' => 'Na pripomenutie: vaše prihlasovacie meno je "@login@" a vaša e-mailová adresa je "@email@".',
	'pass_recevoir_mail' => 'You will receive and email telling you how to change your login.',
	'password_obligatoire' => 'Heslo je povinné.',
	'password_retaper' => 'Potvrďte heslo',
	'pays' => 'Krajina',
	'pays_defaut' => 'Default country',
	'pays_pro' => 'Krajina (zamestnania)',
	'pgp' => 'PGP key',
	'prenom' => 'First name',
	'probleme_email' => 'Email problem: the activation email cannot be sent.',
	'profession' => 'Zamestnanie',
	'profil_droits_insuffisants' => 'Sorry! You are not allowed to modify this author<br />',
	'profil_modifie_ok' => 'The changes to your profile have been saved.',
	'publication' => 'Publikovanie',

	// R
	'raccourcis' => 'Skratky',
	'rappel_login' => 'Na pripomenutie: vaše prihlasovacie meno je: ',
	'rappel_password' => 'Vaše heslo',
	'recherche_case' => 'V poli:',
	'recherche_utilisateurs' => 'Vyhľadať používateľov',
	'recherche_valeur' => 'Vyhľadať:',
	'redemande_password' => 'Please fill in your password again:',
	'rien_a_faire' => 'Nothing to do',

	// S
	'saisir_email_valide' => 'Prosím, zadajte platný e-mail',
	'secondes' => 'sekúnd',
	'secteur' => 'Sektor',
	'septembre' => 'september',
	'sexe' => 'Title',
	'societe' => 'Company / Association ...',
	'statut' => 'Status',
	'statut_rel' => 'Interný status',
	'statuts_actifs' => 'Status označuje farba ikony:',
	'suppression_faite' => 'Deletion completed',
	'supprimer_adherent' => 'Odstrániť používateľov',
	'supprimer_adherent_red' => 'Odstr',
	'surnom' => 'Prezývka',

	// T
	'table' => 'Tabuľka',
	'table_expl' => ': Display field on the members\' list (private area)',
	'tel' => 'Tel.',
	'telephone' => 'Phone',
	'telephone_pro' => 'Work phone',
	'texte' => 'Text',
	'texte_email_confirmation' => 'Your account has been activated. From now on you can access the site using your login and password.

Your login is: @login@
and you have just chosen your password.

Thank you for your trust

The @nom_site@ Team
@url_site@',
	'texte_email_inscription' => 'You are about to finish your registration for @nom_site@ site.

Click on the link below to activate your account and choose your password.

@link_activation@


Thank you for your trust.

The @nom_site@ Team 
@url_site@

If you did not ask for this registration or do not want to take part in the site anymore, click the link below.
@link_suppresion@


',
	'titre_confirmation' => 'Potvrdenie',

	// U
	'un_an' => 'rok',
	'un_jour' => 'deň',
	'un_mois' => 'mesiac',
	'une_heure' => 'hodina',
	'une_minute' => 'minúta',
	'une_seconde' => 'sekunda',
	'url_site' => 'Adresa stránky',
	'url_societe' => 'Company website',

	// V
	'validite' => 'Date of validity',
	'ville' => 'Mesto',
	'ville_pro' => 'Mesto (zam.)',
	'visiteur' => 'Návštevník',
	'vos_contacts_personnels' => 'Vaše osobné kontakty',
	'votre_adresse' => 'Vaša domáca adresa',
	'votre_adresse_pro' => 'Vaša adresa do práce',
	'votre_login_mail' => 'Vaše prihlasovacie meno alebo heslo:',
	'votre_mail' => 'Váš e-mail:',
	'votre_nom_complet' => 'Vaše celé meno',
	'votre_profil' => 'Vaša vizitka',

	// W
	'website' => 'Webová stránka'
);

?>
