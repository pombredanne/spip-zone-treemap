<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/inscription2?lang_cible=ast
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_confirmer' => 'Por confirmar',
	'accesrestreint' => 'Accesu Torgáu',
	'aconfirmer' => 'Por confirmar',
	'action_adherent' => 'Operaciones', # MODIF
	'activation_compte' => 'Activa la to cuenta',
	'adherents' => 'Asociaos', # MODIF
	'admin' => 'Almin',
	'admin_modifier_auteur_supp' => 'Información suplementaria',
	'adresse' => 'Señes',
	'adresse_pro' => 'Señes profesionales (Pro.)',
	'afficher_tous' => 'Afficher tous les utilisateurs', # NEW
	'ajouter_adherent' => 'Crear <br />un visitante nuevu', # MODIF
	'ans' => 'ans', # NEW
	'aout' => 'agostu',
	'aucun_resultat_recherche' => 'Nun hai dengún resultáu pa lo que guetasti.',
	'auteur' => 'Autor',
	'autre' => 'Otru',
	'avril' => 'abril',

	// B
	'bio' => 'Biografía',

	// C
	'caracteres' => 'carauteres', # MODIF
	'categorie' => 'Categoría de cotización',
	'chaine_valide' => 'Tienes qu\'amestar una cadena de carauteres',
	'chainelettre' => '(compuesta únicamente por lletres)',
	'chainenombre' => '(compuesta por lletres y/o númberos)',
	'champ_obligatoire' => 'Esti campu ye obligatoriu',
	'choisir_categories' => 'Esbíllales embaxo:',
	'choisir_nouveau_password' => 'Pues escoyer una clave nueva calcando nel enllaz siguiente',
	'choisir_zones' => 'Tienes qu\'escoyer les zones a les que quies que s\'amesten los nuevos asociaos',
	'choisissez' => 'Choisissez', # NEW
	'choix_affordance_email' => 'Email', # NEW
	'choix_affordance_login' => 'Login (par défaut dans SPIP)', # NEW
	'choix_affordance_login_email' => 'Login et email', # NEW
	'choix_domaine' => 'Tienes qu\'escoyer el to dominiu',
	'civilite' => 'Tratamientu',
	'code_postal' => 'Códigu Postal',
	'code_postal_pro' => 'Códigu Postal (Pro.)',
	'commentaire' => 'Comentariu',
	'compte_active' => 'La to cuenta en @nom_site@',
	'compte_efface' => 'To cuenta esborrose.', # MODIF
	'conf_plugin' => 'Configuración d\'Inscription2', # MODIF
	'conf_plugin_page' => 'Configuración guardada del plugin Inscription2', # MODIF
	'configs' => 'Configuraciones',
	'configuration' => 'Configuración', # MODIF
	'confirmation' => '<p>¿Tas seguru(a) de que quies siguir?</p><p>¡¡¡Tolos cambéos serán irreversibles!!!</p>',
	'contacts_personnels' => 'Contautos personales',
	'contacts_pros' => 'Contautos profesionales',
	'cp_valide' => 'Tienes qu\'inxertar un códigu postal válidu',
	'creation' => 'Fecha de creación de la ficha',

	// D
	'decembre' => 'avientu',
	'delete_user_select' => 'Supprimer le(s) utilisateur(s) sélectionné(s)', # NEW
	'demande_password' => 'La to clave:',
	'descriptif_plugin' => 'Vous trouverez ici tous les utilisateurs inscrits sur le site. Leur statut est indiqué par la couleur de leur icone.<br /><br />Vous pouvez configurer des champs supplémentaires, proposés en option aux visiteurs au moment de l\'inscription.', # NEW
	'description_cfg' => 'Esti complementu permite estender la información sobro los autores del sitiu, equí vas poder escoyer los campos suplementarios.', # MODIF
	'description_page' => 'Equí pues verificar los valores de configuración tal como tan guardaos',
	'divers' => 'Diversos',
	'domaine' => 'Dominiu',
	'domaines' => 'Dominiu',

	// E
	'editer_adherent' => 'Editar asociáu', # MODIF
	'effacement_auto_impossible' => 'La cuenta nun puede esborrase automáticamente, contauta non nos.',
	'email' => 'E-Mail',
	'email_bonjour' => 'Bones @nom@,',
	'email_deja_enregistre' => 'Cette adresse email est déjà enregistrée. Utilisez le formulaire de connexion pour accéder à votre compte.', # NEW
	'email_obligatoire' => 'El campu email ye obligatoriu',
	'email_valide' => 'Tienes qu\'inxertar un email válidu',
	'erreur_reglement_obligatoire' => 'Vous devez accepter le règlement', # NEW
	'exp_divers' => 'Campu que va ofrecese a les visites col nome de comentariu',
	'exp_publication' => 'Autorización pa espublizar los datos personales',
	'exp_statut' => 'Escueye l\'estatutu que quies da-yos a los asociaos nuevos',
	'exp_statut_rel' => 'Campu diferente del estatutu de SPIP, esti sirve pa llevar el control internu d\'una institución',
	'explication_affordance_form' => 'Champ affiché sur les formulaires d\'identification (#LOGIN_PUBLIC)', # NEW

	// F
	'fax' => 'Fax',
	'fax_pro' => 'Fax profesional',
	'feminin' => 'Señora',
	'fevrier' => 'febreru',
	'fiche' => 'Ficha',
	'fiche_adherent' => 'Ficha d\'asociau', # MODIF
	'fiche_expl' => ' : El campu verase na ficha d\'asociau', # MODIF
	'fiche_mod' => 'Modificable',
	'fiche_mod_expl' => ' : El campu podrá modificase dende l\'interfaz públicu pol asociau a condición d\'utilizar el plugin CRAYONS', # MODIF
	'fonction' => 'Función',
	'form' => 'Formulariu',
	'form_expl' => ' : El campu s\'amosará nel formulariu INSCRIPTION2', # MODIF
	'form_oblig_expl' => ' : Facer obligatoria la edición nel formulariu',
	'format' => 'Formatu',
	'formulaire_inscription' => 'Formulariu d\'inscripción',
	'formulaire_inscription_ok' => 'La to inscripción ta acabante d\'aceutase. Vas recibir per corréu electrónicu les identificaciones pa la conexón.',
	'formulaire_login_deja_utilise' => 'Esi login yá ta n\'usu, has d\'escoyer otru.',
	'formulaire_remplir_obligatoires' => 'Has de rellenar los campos obligatorios',
	'formulaire_remplir_validation' => 'Has de verificar los campos que tan ensin validar.',

	// G
	'general_infos' => 'Información Xeneral', # MODIF
	'geoloc' => 'Xeoallugamientu',
	'geomap_obligatoire' => 'Pa utilizar los datos de xeoallugamientu de les persones, ye necesario instalar el plugin GoogleMapApi...',
	'gerent' => 'Asociáu',
	'gestion_adherent' => 'Xestión d\'asociaos', # MODIF

	// H
	'html' => 'HTML',

	// I
	'icone_afficher_utilisateurs' => 'Afficher les utilisateurs', # NEW
	'icone_menu_config' => 'Inscripción2', # MODIF
	'identification' => 'Identification', # NEW
	'info_connection' => 'Información de conexón',
	'info_defaut' => 'Información por omisión', # MODIF
	'info_defaut_desc' => 'Información obligatoria por omisión', # MODIF
	'info_gener' => 'Información Xeneral', # MODIF
	'info_gener_desc' => 'Opciones que van preguntase a los asociaos nuevos del sitiu',
	'info_internes' => 'Información interna',
	'info_internes_desc' => 'Opciones que van guardase na base de datos pero nun s\'amosarán nel formulariu de los asociaos nuevos',
	'info_perso' => 'Información personal',
	'info_perso_desc' => 'Información personal que va preguntase a los nuevos asociaos del sitiu',
	'info_pro' => 'Información profesional',
	'infos_adherent' => 'Información suplementaria',
	'infos_complementaires' => 'Informations complémentaires', # NEW
	'infos_personnelles' => 'Información personal',
	'inscription2' => 'Inscription 2.0', # NEW
	'inscriptions' => 'Inscripciones',
	'inserez_infos' => 'Tienes qu\'amestar la información solicitada',

	// J
	'janvier' => 'xineru',
	'juillet' => 'xunetu',
	'juin' => 'xunu',

	// L
	'label_affordance_form' => 'Paramétrage des formulaires d\'identification', # NEW
	'label_public_reglement' => 'J\'ai lu et j\'accepte le règlement', # NEW
	'label_public_reglement_url' => 'J\'ai lu et j\'accepte le <a href="@url@" class="spip_in reglement">règlement</a>', # NEW
	'label_reglement' => 'Règlement à valider', # NEW
	'label_reglement_article' => 'Article original du site correspondant au règlement', # NEW
	'label_validation_numero_international' => 'Forcer les numéros de téléphone à être sous la forme internationale', # NEW
	'latitude' => 'Llatitú',
	'legend_affordance_form' => 'Formulaire d\'identification', # NEW
	'legend_oubli_pass' => 'Pas de mot de passe / mot de passe oublié', # NEW
	'legend_reglement' => 'Règlement du site', # NEW
	'legend_validation' => 'Validations', # NEW
	'legende' => 'Lleenda',
	'lisez_mail' => 'Vién d\'unviase un email a les señes qu\'apurriste. Pa activar la to cuenta has de siguir les instrucciones.',
	'liste_adherents' => 'Amosar les cuentes d\'usuarios', # MODIF
	'liste_comptes_titre' => 'Llista de cuentes d\'usuarios', # MODIF
	'login' => 'Nome d\'usuariu (login)',
	'logo_auteur' => 'Logo', # NEW
	'longitude' => 'Llonxitú',

	// M
	'mai' => 'mayu',
	'mail_non_domaine' => 'Les señes d\'email que nos diste nun pertenecen al dominiu que conseñaste, has de tentalo de nuevo',
	'mail_renvoye' => 'Estes señes d\'email ya tan rexistráes, has d\'activar la to cuenta siguiendo les instrucciones conteníes nel corréu.',
	'mars' => 'marzu',
	'masculin' => 'Señor',
	'message_auto' => '(esti ye un mensaxe automáticu)',
	'mobile' => 'Móvil',
	'mobile_pro' => 'Móvil profesional',
	'modif_pass_titre' => 'Camudar la to clave',
	'moins_seconde' => 'moins d\'une seconde', # NEW
	'mot_passe_reste_identique' => 'La to clave nun camudó.',

	// N
	'naissance' => 'Fecha de nacimientu',
	'nb_users_supprimes' => '@nb@ suppression(s) effectuée(s).', # NEW
	'nettoyer_tables' => 'Llimpiar tables',
	'no_user_selected' => 'Vous n\'avez sélectionné aucun utilisateur.', # NEW
	'nom' => 'Alcuñu',
	'nom_explication' => 'el to nome o nomatu',
	'nom_famille' => 'Apellíos',
	'nom_site' => 'Nome del sitiu',
	'non_renseigne' => 'non revisáu.',
	'non_renseignee' => 'non revisada.',
	'novembre' => 'payares',
	'numero_valide' => 'Has d\'inxertar un númberu válidu',
	'numero_valide_international' => 'Ce numéro doit être sous la forme internationale (ex: +32 475 123 456)', # NEW

	// O
	'octobre' => 'ochobre',

	// P
	'page_confirmation' => 'Páxina de confirmación de la inscripción',
	'par_defaut' => 'Esti campu ye obligatoriu', # MODIF
	'pass' => 'Clave',
	'pass_egal' => 'Has d\'escribir la mesma clave que l\'anterior.',
	'pass_indiquez_cidessous' => 'Indica embaxo les señes d\'email coles que tas
   rexistráu. Vas recibir un email que
   indicará los pasos a siguir pa camudar
   l\'accesu.',
	'pass_minimum' => 'La clave tien que tener polo menos 5 carauteres',
	'pass_oubli_mot' => 'Camudar la clave',
	'pass_rappel_email' => 'Rappel : votre adresse email est "@email@".', # NEW
	'pass_rappel_login_email' => 'Rappel : votre login est "@login@" et votre adresse email est "@email@".', # NEW
	'pass_recevoir_mail' => 'Vas recibir un email que indica cómo modificar el to accesu al sitiu.',
	'password_obligatoire' => 'La clave ye obligatoria.',
	'password_retaper' => 'Confirmar la clave',
	'pays' => 'País',
	'pays_defaut' => 'Pays par défaut', # NEW
	'pays_pro' => 'País (Pro.)',
	'pgp' => 'Clave PGP',
	'prenom' => 'Nome',
	'probleme_email' => 'Problema de corréu: l\'email d\'activación nun pue unviase.',
	'profession' => 'Oficiu',
	'profil_droits_insuffisants' => 'Paez que nun tienes permisu pa modificar esti autor<br/>', # MODIF
	'profil_modifie_ok' => 'Les modificaciones del to perfil rexistráronse bien.',
	'publication' => 'Espublizamientu',

	// R
	'raccourcis' => 'Atayos',
	'rappel_login' => 'Recordatoriu: el to identificador ye: ',
	'rappel_password' => 'La to clave',
	'recherche_case' => 'Campos de la gueta', # MODIF
	'recherche_utilisateurs' => 'Restolar ente los usuarios', # MODIF
	'recherche_valeur' => 'Conteníu guetáu', # MODIF
	'redemande_password' => 'Vuelve a escribir la to clave:',
	'rien_a_faire' => 'Ná por facer',

	// S
	'saisir_email_valide' => 'Has d\'escribir unes señes de corréu válides',
	'secondes' => 'secondes', # NEW
	'secteur' => 'Sector',
	'septembre' => 'setiembre',
	'sexe' => 'Tratamientu',
	'societe' => 'Sociedá / Asociación ...',
	'statut' => 'Estatutu',
	'statut_rel' => 'Estatutu internu',
	'statuts_actifs' => 'Les couleurs des icones correspondent aux statuts suivants : ', # NEW
	'suppression_faite' => 'Vien de facese\'l desaniciu de los tos datos',
	'supprimer_adherent' => 'Desaniciar asociáu', # MODIF
	'supprimer_adherent_red' => 'Des',
	'surnom' => 'Nomatu',

	// T
	'table' => 'Tabla',
	'table_expl' => ': El campu amosarase na llista d\'asociaos (espaciu priváu)', # MODIF
	'tel' => 'Tel.',
	'telephone' => 'Teléfonu',
	'telephone_pro' => 'Teléfonu profesional',
	'texte' => 'Testu',
	'texte_email_confirmation' => 'La to cuenta ta activa, darréu puedes coneutate al sitiu usando la to identificación personal.

El nome d\'usuariu ye: @login@
y vienes d\'escoyer la to clave.

Gracies pola confianza

L\'equipu de @nom_site@ 
@url_site@', # MODIF
	'texte_email_inscription' => 'Tas a piques de confirmar la to inscripción nel sitiu @nom_site@. 

Calca nel enllaz d\'embaxo p\'activar la to cuenta y escoyer to clave.

@link_activation@


Gracies pola to confianza.

L\'equipu de @nom_site@.
@url_site@

Si nun pidiste esta inscripción o si yá nun quies formar parte del nuestru sitiu, calca nel enllaz d\'embaxo.

@link_suppresion@


', # MODIF
	'titre_confirmation' => 'Confirmación',

	// U
	'un_an' => 'un an', # NEW
	'un_jour' => 'un jour', # NEW
	'un_mois' => 'un mois', # NEW
	'une_heure' => 'une heure', # NEW
	'une_minute' => 'une minute', # NEW
	'une_seconde' => 'une seconde', # NEW
	'url_site' => 'Url del sitiu',
	'url_societe' => 'Sitiu web de la sociedá',

	// V
	'validite' => 'Fecha de validez',
	'ville' => 'Ciudá',
	'ville_pro' => 'Ciudá (Pro.)',
	'visiteur' => 'Visitante',
	'vos_contacts_personnels' => 'Tos contautos personales',
	'votre_adresse' => 'Les tos señes personales',
	'votre_adresse_pro' => 'Les tos señes (profesionales)',
	'votre_login_mail' => 'Votre login ou email :', # NEW
	'votre_mail' => 'Tos señes de corréu:',
	'votre_nom_complet' => 'El to nome completu',
	'votre_profil' => 'El to perfil',

	// W
	'website' => 'Sitiu d\'Internet'
);

?>
