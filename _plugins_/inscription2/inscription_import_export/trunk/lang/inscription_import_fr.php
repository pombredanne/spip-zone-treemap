<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://trac.rezo.net/spip-zone/_plugins_/_test_/inscription2/inscription2_2_0/lang
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_auteurs' => 'Ajouter aux auteurs',
	'annulation_action' => 'Vous avez annul&eacute; l\'action pr&eacute;c&eacute;dente.',
	
	// C
	'champs_oblig' => 'Ce champs est obligatoire',
	'csv_ajouts' => 'Utilisateurs ajout&eacute;s depuis le CSV',
	'csv_erreurs' => 'Erreurs du fichier CSV',
	
	// D
	'derniers_utilisateurs' => 'Les @nb@ derniers utilisateurs de la base de donn&eacute;e',
	
	// E
	'exec_texte_boite_info' => 'Cette page vous permet d\'importer des auteurs dans les tables "auteurs" et "auteurs_elargis" de SPIP en uploadant un simple fichier CSV',
	'export_users_sites' => '@site@ - Export des utilisateurs - @date@',
	'exporter_users_csv_normal' => 'Exporter les utilisateurs au format CSV simple',
	'exporter_users_xls' => 'Exporter les utilisateurs au format CSV pour Excel',
	
	// F
	'fichier_vide' => 'Le fichier est vide.',
	'fichier_a_importer' => 'Fichier CSV &agrave; importer',
	
	// I
	'inscription_import_titre' => 'Import pour Inscription2',
	'inscription_import_gros_titre' => 'Import d\'utilisateurs par CSV',
	'importer_utilisateurs_csv' => 'Importer des utilisateurs par CSV',
	'import_fichier' => 'Import du fichier',
	
	// L
	'ligne_entete' => '1<sup>&egrave;re</sup> ligne d\'en-t&ecirc;te',
	
	// N
	'nb_ligne' => 'Ligne num&eacute;ro @nb@',
	
	// P
	'pas_importer' => 'Ne pas importer',
	'previsualiser' => 'Pr&eacute;visualiser',
	'previsualisation' => 'Pr&eacute;visualisation',
	'probleme_upload' => 'Probl&egrave;me lors du chargement du fichier (erreur @erreur@).',
	
	// R
	'revenir_etape' => 'Revenir &agrave; l\'&eacute;tape @step@',
	
	// S
	'separateur' => "Caract&egrave;re de s&eacute;paration des champs CSV",
	
	// T
	'total_ajouts' => '@nb@ utilisateurs ont &eacute;t&eacute; ajout&eacute;s.',
	'total_auteur' => '@nb@ auteurs au total.',
	'total_erreurs' => 'Il y a @nb@ lignes en erreur dans votre fichier.',
	'total_lignes' => 'Il existe @nb@ lignes au total dans votre fichier.',
	
	// Z
	'z' => 'zzz'
);

?>