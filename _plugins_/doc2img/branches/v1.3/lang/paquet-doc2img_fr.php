<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-doc2img
// Langue: fr
// Date: 03-06-2012 15:51:34
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// D
	'doc2img_description' => 'Ce plugin permet de convertir à la volée des pdf, tiff et autres types de document en image ou série d\'images. Ces images sont ensuite exploitables via une boucle spécifique.',
	'doc2img_nom' => 'Documents en Images',
	'doc2img_slogan' => 'Convertir des documents textuels à la volée',
);
?>