<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-doc2img
// Langue: en
// Date: 03-06-2012 15:51:34
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// D
	'doc2img_description' => 'This plugin will convert on the fly pdf, tiff and other kind of documents in image or serie of images. These images can then be used with a specific loop.',
	'doc2img_nom' => 'Documents to images',
	'doc2img_slogan' => 'Convert text documents on the fly',
);
?>