<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	//0
	'0_ticket' => 'aucun ticket',

	// 1
	'1_ticket' => 'ticket',

	//A
	'abonnement' => 'Abonnement',
	'accueil_site' => 'Accueil',
	'afaire_aller_jalon' => 'Aller au jalon',
	'afaire_colonne_assigne' => 'Traité par',
	'afaire_colonne_date' => 'Date',
	'afaire_colonne_id' => 'Numéro',
	'afaire_colonne_importance' => 'Importance',
	'afaire_colonne_jalon' => 'Jalon',
	'afaire_colonne_titre' => 'Résumé',
	'afaire_colonne_type' => 'Type',
	'afaire_msg_0_ticket_jalon' => 'Aucun ticket planifié pour ce jalon',
	'afaire_non_planifies' => 'Non planifiés',
	'afaire_page_sommaire' => 'Les jalons actuellement prévus sont :',
	'afaire_tickets_en_analyse' => 'Tickets en cours d\'analyse',
	'afaire_tickets_en_traitement' => 'Tickets en cours de traitement',
	'afaire_tickets_termines' => 'Tickets terminés',
	'agenda' => 'Agenda',
	'annuaire' => 'Annuaire',
	'aucune_breve' => 'Aucune brève',
	'aucun_document' => 'Aucun document',
	'aucun_message' => 'Aucun message',

	//B
	'bouton_telecharger' => 'Télécharger',
	'breves' => 'Brèves',

	//C
	'categories' => 'Catégories',
	'commentaires' => 'Commentaires',

	//D
	'derniers_visiteurs' => 'Derniers visiteurs',
	'documents' => 'Documents',

	//E
	'editer_breve' => 'Éditer la brève',
	'editer_groupe' => 'Éditer le groupe',
	'editer_message' => 'Éditer le message',
	'editer_profil' => 'Éditer mon profil',
	'editer_site' => 'Éditer le site',

	//F

	//G
	'groupes_travail' => 'Vos groupes de travail',

	//H

	//I
	'info_breves' => 'Les brèves',
	'info_breves_prop' => 'Les brèves proposés à la publication',
	'info_breves_publies' => 'Vos brèves publiés',
	'info_messages' => 'Vos messages',
	'info_messages_edition' => 'Vos messages en cours de rédaction',
	'info_messages_prop' => 'Vos messages proposés à la publication',
	'info_messages_publies' => 'Vos messages publiés',

	//L
	'laissez_commentaire' => 'Laissez un commentaire',
	'lire_la_suite' => 'Lire la suite',
	'lire_la_suite_de' => ' de ',
		
	//M
	'messages' => 'Messages',
	'modifier_ticket' => 'Modifier ce ticket',
	'mon_profil' => 'Mon profil',

	//N
	'n_tickets' => 'tickets',
	'nouveau_groupe' => 'Nouveau groupe',
	'nouveau_message' => 'Nouveau message',
	'nouveau_site' => 'Nouveau site',
	'nouveau_ticket' => 'Nouveau ticket',
	'nouvelle_breve' => 'Nouvelle brève',

	//P
	'pas_le_droit_ecrire' => 'Vous n\'avez pas d\'autorisation en écriture',

	//R
	'redaction' => 'Rédaction',
	'repondre_site' => 'Répondre à ce site',
	'retour' => 'Retour',

	//S
	'sites' => 'Sites',
	'suivis_public_info_email' => 'Adresse de suivis des notifications',

	//T
	'texte_modifier_auteur' => 'Modifier l\'auteur :',
	'texte_modifier_breve' => 'Modifier la brève :',
	'toutes_les_breves' => 'Toutes les brèves',
	'tous_les_sites' => 'Tous les sites',
	'tous_les_tickets' => 'Tous les tickets',
	'tous_mes_messages' => 'Tous mes messages',

	//V
	'voir_mon_profil' => 'Voir mon profil',

);

?>
