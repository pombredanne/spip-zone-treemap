<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Module: spiperipsum
// Langue: es

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'evangile_quotidien' => 'Evangelio del día',

	// S
	'saint_quotidien' => 'Santo del día',
);

?>
