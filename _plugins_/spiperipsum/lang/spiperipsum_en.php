<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Module: spiperipsum
// Langue: en

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'evangile_quotidien' => 'Daily Gospel',

	// S
	'saint_quotidien' => 'Saint of the day',
);

?>
