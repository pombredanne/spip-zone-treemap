<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Module: spiperipsum
// Langue: nl

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'evangile_quotidien' => 'Dagelijks Evangelie',

	// S
	'saint_quotidien' => 'Heiligen van de dag',
);

?>
