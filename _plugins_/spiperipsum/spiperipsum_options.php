<?php
// Langue par defaut si non supportee par le site serveur
define ('_SPIPERIPSUM_LANGUE_DEFAUT','en');

// Jour par defaut
define ('_SPIPERIPSUM_JOUR_DEFAUT','aujourdhui');

// Valeurs de l'argument lecture dans le modele evangile
define ('_SPIPERIPSUM_LECTURE_EVANGILE','evangile');
define ('_SPIPERIPSUM_LECTURE_PREMIERE','premiere');
define ('_SPIPERIPSUM_LECTURE_SECONDE','seconde');
define ('_SPIPERIPSUM_LECTURE_PSAUME','psaume');
define ('_SPIPERIPSUM_LECTURE_COMMENTAIRE','commentaire');
define ('_SPIPERIPSUM_LECTURE_SAINT','saint');
// -- Lecture par defaut
define ('_SPIPERIPSUM_LECTURE_DEFAUT','evangile');

// Valeurs de l'argument mode d'appel du modele (depuis article ou page zpip)
define ('_SPIPERIPSUM_MODE_ARTICLE','article');
define ('_SPIPERIPSUM_MODE_PAGE','page');
// -- Mode par defaut
define ('_SPIPERIPSUM_MODE_DEFAUT','article');

// Info par defaut
define ('_SPIPERIPSUM_INFO_DEFAUT','titre');
?>
