<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'plugins_liste' => 'Liste des plugins',
'plugins_liste_famille' => 'Liste des plugins ',
'texte_presente_plugin' => 'Cette page liste les plugins activ&eacute;s sur le site. Vous pouvez en d&eacute;sactiver un rapidement en d&eacute;cochant la case &agrave; cocher qui lui est d&eacute;di&eacute;e.',
'texte_presente_plugin_famille' => 'Cette page liste les plugins de la m&ecirc;me famille disponibles sur votre site.',
'plugin_etat_developpement' => 'Ce plugin est en d&eacute;veloppement et sa stabilit&eacute;e n\'est pas assur&eacute;e.',
'plugin_etat_experimental' => 'Attention : ce plugin est exp&eacute;rimental et il pourrait alt&eacute;rer le fonctionnement normal de votre site SPIP !',
'plugin_etat_test' => 'Ce plugin est en test. Si vous observez des disfonctionnements, vous pouvez en avertir son auteur.',
'plugin_etat_stable' => 'Ce plugin est stable.',
);


?>