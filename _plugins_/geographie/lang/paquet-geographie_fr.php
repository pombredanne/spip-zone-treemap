<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

//  G
	'geographie_description' => 'Ce plugin installe 4 bases géographiques contenant Pays, Noms et indicatifs des régions, départements et commnunes de France',
	'geographie_nom' => 'Géographie',
	'geographie_slogan' => 'Bases géographies de pays, régions, départements et communes',
);
?>