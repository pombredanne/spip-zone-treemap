<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(
	//A

	'action_modifier' => 'modifica',
	'action_modifier_title' => 'Modifica i parametri della rubrica',
	'appliquer_sous_rubriques' => 'Applica alle sottorubriche?',
	'action_supprimer' => 'elimina',
	'action_supprimer_title' => 'Elimina la personalizzazione dal database',
	'action_activer' => 'attiva',
	'action_activer_title' => 'Attiva la personalizzazione della rubrica',
	'action_desactiver' => 'disattiva',
	'action_desactiver_title' => 'Disattiva la personalizzazione della rubrica',
	'autoriser_les_articles' => 'Utilizza gli articoli?',
	'autoriser_les_sous_rubriques' => 'Utilizza le sottorubriche?',		
	
	//B
	
	'bouton_supprimer' => 'Elimina',

	//C

	'choix_rubrique' => 'Scegli la rubrica da personalizzare:',
	
	//E

	'erreur_action' => "L'azione @action@ non esiste",
	
	//I
	'icone_creer_priveperso' => 'Crea una nuova personalizzazione di rubrica',
	'info_contenu_articles' => 'Contenuto degli articoli',
	'info_contenu_rubriques' => 'Contenuto delle rubriche',
	'info_contenu_breves' => 'Contenuto delle brevi',
	'info_modifier_priveperso' => 'Personalizza una rubrica',
	'info_modif_priveperso' => 'Modifica della personalizzazione della rubrica @rubrique@:',
	'info_nouveau_priveperso' => 'Nuova personalizzazione di rubrica',
	'info_description_priveperso' => "Queste pagina ti consente di personalizzare la redazione per rubriche. Le personalizzazioni delle rubriche impostate verranno elencate qui. Avrai quindi la possibilit&agrave; di modificarle, eliminarle o solo disattivarle temporaneamente.",
	'info_description_priveperso_creer' => "Puoi scegliere il tipo di oggetto da mostrare, se utilizzare o no gli articoli, le sottorubriche, le brevi, la syndication dei siti, rubrica per rubrica. Puoi anche modificare i campi per gli articoli, le rubriche e le brevi.",		
		
				
	//L

	'liste_des_rubriques_perso' => 'Elenco delle rubriche personalizzate',
	'les_breves' => 'Utilizza le brevi?',
	'les_articles_syndiques' => 'Utilizza i siti in syndication?',
	'les_mots_cles' => 'Utilizza le parole chiave?',

	//N
	
	'non' =>'No',	
	
	//O

	'oui' => 'Si',	
	
	//P

	'personnaliser_espace_prive' => 'Personalizza la redazione',
	'personnaliser_texte' => 'Personalizza il testo dei campi',
	'personnaliser_texte_interro' => 'Personalizza il testo dei campi?',
	'perso_sauvegarde' => 'Personalizzazione della rubrica salvata!',
	'pb_sauvegarde' => 'Si &egrave; verificato un problema durante la scrittura in database',
	'Prive_perso'=> 'Personalizza redazione',

	//R
	'racine' => 'Radice',
	'rubrique_deja_perso' => 'Rubrica gi&agrave; personalizzata',
	'rubrique' => 'Rubrica',
	'rubrique_inexistante' => "La rubrica @rubrique@ non esiste",
	//S

	//T

	//V
	'veuillez_choisir_rubrique' => 'Scegli una rubrica!'
);
?>