<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-critere_mots
// Langue: fr
// Date: 01-08-2012 13:09:46
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
	'critere_mots_description' => 'Permet d\'afficher les éléments qui ont plusieurs mots clés en communs, via la variable $mots[] passée dans l\'environnement',
	'critere_mots_slogan' => 'Sélectionner des objets ayant des mots clés en communs',
);
?>