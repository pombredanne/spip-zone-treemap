<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP nomme admin_lang genere le NOW()
// langue / language = fr
// Traduction -- Pierre FICHES <pierre.fiches@free.fr>

$GLOBALS[$GLOBALS['idx_lang']] = array(
//elements de formulaire pour editer les coordonnees
'clave_engadida' => 'Clef ajout&eacute;e: ',
'clic_mapa' => 'Cliquer sur la carte pour changer les coordonn&eacute;es',
'clic_desplegar' => 'd&eacute;plier',
'configurar_mymap' => 'Configurer le Plugin mymap',
'conseguir' => '(obtenir)',
'cambiar' => 'PLAN GOOGLE MAP',
'boton_actualizar' => 'actualiser',
'titre_inserer_gmap' => 'INSERER LE PLAN GOOGLEMAP',
'titre_inserer_un_gmap' => 'PLAN GOOGLE MAP',
'joindre_gmap' => 'Plan Google Map',
'centrer' => 'Centrer cette carte ici',
'voir' => 'Plans Google Map',
'clave_engadida' => 'Clef ajout&eacute;e: ',
'conseguir' => '(obtenir)',
'default_geoloc' => 'Position par d&eacute;faut des cartes :',
'configuration' => 'Configuration',
'miseajour' => 'Mise &agrave; jour',
'default_geoloc' => 'Position par defaut des cartes :'
);

?>