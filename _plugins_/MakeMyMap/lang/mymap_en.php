<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP nomme admin_lang genere le NOW()
// langue / language = fr
// Traduction -- Pierre FICHES <pierre.fiches@free.fr>

$GLOBALS[$GLOBALS['idx_lang']] = array(
//elements de formulaire pour editer les coordonnees
'clave_engadida' => 'Added key: ',
'clic_mapa' => 'Clic on the map to change coordinates',
'clic_desplegar' => 'unfold',
'configurar_mymap' => 'Configure mymap Plugin',
'conseguir' => '(obtain)',
'cambiar' => 'change coordinates',
'boton_actualizar' => 'update',
'clave_engadida' => 'Added key: ',
'conseguir' => '(obtain)',
'default_geoloc' => 'Default maps position:',
'configuration' => 'Configuration',
'miseajour' => 'Update',
'default_geoloc' => 'Default position of maps:'
);

?>
