<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP nomme admin_lang genere le NOW()
// langue / language = en
// Traduction -- Daniel Viñar <dani@nbelvil.net>

$GLOBALS[$GLOBALS['idx_lang']] = array(
//elements de formulaire pour editer les coordonnees
'clave_engadida' => 'Added key: ',
'conseguir' => '(obtain)',
'default_geoloc' => 'Default maps position:',
'configuration' => 'Configuration',
'miseajour' => 'Update'
);

?>
