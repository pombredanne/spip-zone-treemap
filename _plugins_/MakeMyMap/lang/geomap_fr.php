<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP nomme admin_lang genere le NOW()
// langue / language = fr
// Traduction -- Pierre FICHES <pierre.fiches@free.fr>

$GLOBALS[$GLOBALS['idx_lang']] = array(
//elements de formulaire pour editer les coordonnees
'clave_engadida' => 'Clef ajout&eacute;e: ',
'conseguir' => '(obtenir)',
'default_geoloc' => 'Position par d&eacute;faut des cartes :',
'configuration' => 'Configuration',
'miseajour' => 'Mise &agrave; jour'
);

?>