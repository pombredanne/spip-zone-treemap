<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// Aide des boutons supplémentaires de la barre typo
'barre_intertitre2' => 'Transformer en {2{intertitre niveau deux}2}',
'barre_intertitre3' => 'Transformer en {3{intertitre niveau trois}3}',
'barre_miseenevidence' => 'Mettre le texte en [*&eacute;vidence*]',

'barre_exposant' => 'Mettre le texte en &lt;sup&gt;exposant&lt;/sup&gt;',

'barre_indice' => 'Mettre le texte en &lt;sub&gt;indice&lt;/sub&gt;',

'barre_petitescapitales' => 'Mettre le texte en &lt;sc&gt;petites capitales&lt;/sc&gt;',
'barre_centrer' => '[|Centrer|] le paragraphe',
'barre_alignerdroite' => '[/Aligne &agrave; droite/] le paragraphe',
'barre_encadrer' => '[(Encadrer)] le paragraphe',

'barre_barre' => '&lt;del&gt;Barrer&lt;/del&gt; le texte',

'barre_avances' => 'Du sens, du sens&nbsp;!',
'barre_boutonsavances' => 'Mises en sens suppl&eacute;mentaires, &agrave; utiliser avec mod&eacute;ration et discernement&nbsp;!',

'cfg_puces' => 'Traitement des puces',
'cfg_titraille' => 'Titraille',
'cfg_insertcss' => 'Insertion CSS'

);
?>
