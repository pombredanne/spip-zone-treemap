<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_un_auteur' => 'Ajouter un auteur',
	'auteur_ajoute' => 'Auteur ajout&eacute;',

	// B
	'bouton_valider' => 'Valider',

	// E
	'erreur_deja_id_auteur' => 'Cet auteur est d&eacute;j&agrave; auteur de l\'article',
	'erreur_pas_id_auteur' => 'Choisissez un auteur',

);

?>
