<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

// Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
'administration_sitra' => 'Administration Sitra',
'afficher_masquer' => 'Afficher / Masquer tout',
'autres_documents' => 'Autres documents',
'aucun_resultat' => 'Aucun résultat !',

// C
'categories' => 'Catégories',
'categories_disponibles' => 'Catégories disponibles',
'chemin_images' => 'Dossier des images exportées',
'criteres' => 'Critères',
'criteres_disponibles' => 'Critères disponibles',

// D
'date_au' => 'au',
'date_du' => 'du',
'date_le' => 'le',
'dossier_sitra' => 'Dossier des exports xml Sitra',


// E
'enregistrer' => 'Enregistrer',
'explication_chemin_images' => 'Chemin des images exportées depuis la racine du site Spip. <strong>Ne pas oublier le / final.</strong><br />Typiquement : IMG/images_sitra/',
'explication_dossier_sitra' => 'Chemin des exports Sitra depuis la racine du site Spip. <strong>Ne pas oublier le / final.</strong><br />Typiquement : sitra/',
'explication_id_site' => 'Identifiant du site (voir dans le paramètrage du site sur le client Sitra).<br />Typiquement : un entier',

'explication_langues' => 'Codes des langues exportées séparées par des virgules sans espaces.<br />Typiquement : fr,en',

'explication_mail_dest' => 'Email(s) du ou des destinataire(s) des messages d\'alerte ou d\'execution des imports. Laisser vide si pas de message souhaité.
<br />Typiquement : adresse@domaine.ext<br />ou nom prenom &lt;adresse@domaine.ext&gt;(forme préférable).
<br />Pour plusieurs destinataires, séparer les adresses par une virgule',

'explication_mail_from' => 'Email(s) d\'expédition des messages d\'alerte ou d\'exécution des imports. Laisser vide si pas de message souhaité.<br /> Typiquement : adresse@domaine.ext<br />ou nom prenom &lt;adresse@domaine.ext&gt; (forme préférable).',

'explication_mode_compresse' => 'Fichiers xml compressés ou non.',

'explication_mode_debug' => 'En mode debugage, les fichiers ne sont pas détruits, les messages ne sont pas envoyés, toutes les données importées et les détails des opérations sont affichées sur la page d\'import (mode pour tests).',

// H
'heure_debut_a' => 'à',
'heure_debut_de' => 'de',
'heure_fin_a' => 'à',

// I
'identifiant_site' => 'Identifiant Sitra du site',
'identifiant_sitra' => 'Identifiant SITRA',
'images_principales_ou_secondaires' => 'Images principales, secondaires ou logo',

// L
'langues' => 'Langues',
'legend_configuration_export' => 'Configuration des exports',
'legend_envoi_email' => 'Configuration des emails de suivi d\'activité ou d\'alerte',

// M
'mail_dest' => 'Adresse du (des) destinataire(s)',
'mail_from' => 'Adresse d\'expédition',
'mode_compresse' => 'Type d\'exports',
'mode_compresse_non' => 'Exports non compressés',
'mode_compresse_oui' => 'Exports compressés',
'mode_debug' => 'Mode débugage',
'mode_production' => 'Mode production',

// O
'obj_adresse' => 'Adresse',
'obj_altitude' => 'Altitude',
'obj_classement' => 'Classement',
'obj_classement_code' => 'Code de classement',
'obj_classement_orga' => 'Organisme de classement',
'obj_code_postal' => 'Code postal',
'obj_commune' => 'Commune',
'obj_date_debut' => 'Date de début',
'obj_date_fin' => 'Date de fin',
'obj_email' => 'Email',
'obj_fax' => 'Fax',
'obj_latitude' => 'Latitude',
'obj_lieu' => 'Lieu',
'obj_longitude' => 'Longitude',
'obj_n_insee' => 'N° insee',
'obj_reservation_url' => 'Url réservation',
'obj_tel_fax' => 'Téléphone-Fax',
'obj_telephone' => 'Téléphone',
'obj_web' => 'Web',
'objets_disponibles_pour_critere' => 'Objets disponibles pour le critère',
'objets_disponibles_par_categorie' => 'Objets disponibles par catégorie',
'objets_disponibles_par_selection' => 'Objets disponibles par sélection',
'objets_par_categorie' => 'Objets par catégorie',
'objets_par_selection' => 'Objets par sélection',


// P
'plus_de_details' => 'Plus de détails&#8230;',

// R
'rechercher' => 'Rechercher',
'renseignements' => 'Renseignements',
'resultats' => 'résultat(s)',
'retour' => 'Retour',

// S
'selections' => 'Sélections',
'selections_disponibles' => 'Sélections disponibles',
'sitra' => 'SITRA',

// T
'titre_objet' => 'Titre',
);

?>