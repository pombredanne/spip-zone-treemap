<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
	'cooliris_description' => 'Ce plugin permet d\'afficher les images d\'un article avec Cooliris, un mur 3D en flash',
	'cooliris_slogan' => 'Cooliris, un mur d\'images 3D '
);
?>
