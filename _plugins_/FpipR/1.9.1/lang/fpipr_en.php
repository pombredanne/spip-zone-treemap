<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
									   'Flickr' => 'Flickr',
									   'photos' => 'photos',
									   'by' => 'by',
									   'tags' => 'Tags',
									   'ajouter_photos' => 'Add photos from Flickr',
									   'ajouter_sets' => 'Add sets of photos from Flickr',
									   'autorisation_titre' => 'Flickr Authorization',
									   'identifie_ok' => 'Your are now identified on Flickr as the user: @user_id@',
									   'identifie_etape1' => 'You must first authorize the site to access your flickr photos by clicking <strong><a target="blank" href="@url@">here</a></strong><br/>
A new window will open, please follow the instructions it will provide.',
									   'identifie_etape2' => 'When you are done, you have to come back here to @form@ the authorization.',
									   'terminer' => 'finish',
									   'info_photos' => 'Please choose the photos to add.',
									   'info_sets' => 'Please choose the sets of photos to add.',
									   'demande_authentification' =>  'You must first authorize Flickr <a href="@url@">here</a>',
									   'pages' => 'Pages',
									   'retour' => 'Back',
									   'recherche' => 'Search',
									   'ordre' => 'Order by',
									   'text_search' => 'Full Text Search',
									   'date-posted-asc' => 'Date posted, ascendant',
									   'date-posted-desc' => 'Date posted, descendant',
									   'date-taken-asc' => 'Date taken, ascendant',
									   'date-taken-desc' => 'Date taken, descendant',
									   'interestingness-asc' => 'Interestingness, ascendant',
									   'interestingness-desc' => 'Interestingness, descendant',
									   'relevance' => 'Relevance',
									   'bookmarklet' => 'Bookmarklet',
									   'bookmarklet_info' => '<p>You can add a photo from flickr that is not yours by placing this link: <a href=\'@url@\'>Send this photo to SPIP</a> in your bookmarks.</p>
<p>When you visit a photo on flickr, by clicking this bookmark, you will get to a new page where you can select which article to add it to.</p>',
									   'ajouter_une_photo' => 'Add a photo',
									   'ajouter_une_photo_info' => 'Add the photo "@title@" by @owner@',
									   'choisir_un_article' => 'Choose an article',
									   'par' => '@title@ by <a href="@url@">@user@</a>',

									   'warning_family_friend' => 'Beware, this photo is only <strong>visible to familly and friends</strong>. If you add it to one of your article, it might be visible to everyone. Ask the authorisation to the author of the photo before continuing',
									   'warning_family' => 'Beware, this photo is only <strong>visible to familly</strong>. If you add it to one of your article, it might be visible to everyone. Ask the authorisation to the author of the photo before continuing',
									   'warning_friend' => 'Beware, this photo is only <strong>visible to friends</strong>. If you add it to one of your article, it might be visible to everyone. Ask the authorisation to the author of the photo before continuing',

									   'warning_copyright_general' => 'Beware, this photo is protected by a license: <strong><a href="@url@">"@name@"</a></strong>. Get more information and ask the author of the photo before posting on this site.',
									   'warning_copyright_0' => 'Beware, this photo is protected by <strong>copyright</strong>. You need to ask the author of the photo before posting it on this site.',
									   'warning_copyright_1' => 'Beware, this photo is protected by a license: <strong><a href="http://creativecommons.org/licenses/by-nc-sa/2.0/">"CC Attribution - NonCommercial - ShareAlike"</a></strong>. Get more information and ask the author of the photo if needed.',
									   'warning_copyright_2' => 'Beware, this photo is protected by a license: <strong><a href="http://creativecommons.org/licenses/by-nc/2.0/">"CC Attribution - NonCommercial"<a></strong>. Get more information and ask the author of the photo if needed.',
									   'warning_copyright_3' => 'Beware, this photo is protected by a license: <strong><a href="http://creativecommons.org/licenses/by-nc-nd/2.0/">"CC Attribution - NonCommercial - NoDerivs"</a></strong>. Get more information and ask the author of the photo if needed.',
									   'warning_copyright_4' => 'Beware, this photo is protected by a license: <strong><a href="http://creativecommons.org/licenses/by/2.0/">"Attribution"</a></strong>. Get more information and ask the author of the photo if needed.',
									   'warning_copyright_5' => 'Beware, this photo is protected by a license: <strong><a href="http://creativecommons.org/licenses/by-sa/2.0/">"Attribution -  ShareAlike"</a></strong>. Get more information and ask the author of the photo if needed.',
									   'warning_copyright_6' => 'Beware, this photo is protected by a license: <strong><a href="http://creativecommons.org/licenses/by-nd/2.0/">"CC Attribution - NoDerivs"</a></strong>. Get more information and ask the author of the photo if needed.',
									   'mauvaisop' => "You cannot use the operator @op@ on the criterion @critere@",
									   'mauvaise_imbrication' => "The loop FLICKR_PHOTO_@boucle@ should only be inside a loop FLICKR_PHOTOS_GETINFO",
									   'revoke' => 'Revoke Rights',
									   'revoke_info' => 'You can revoke the rights for this site to connect to you Flickr account by clicking this button and going to <a href="http://flickr.com/services/auth/list.gne">Flickr Authentication List</a>'
);

?>
