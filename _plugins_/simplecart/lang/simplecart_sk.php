<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/simplecart?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'add_to_cart' => 'Pridať do vozíka',

	// B
	'buy' => 'Kúpiť',

	// C
	'c_dineromail' => 'DineroMail',
	'c_email' => 'E-mail',
	'c_google_checkout' => 'Google Checkout',
	'c_paypal' => 'PayPal',
	'cart_configuration' => 'Nastavenia nákupného vozíka',
	'cart_headers' => 'Nadpisy nákupného vozíka',
	'cart_headers_explication' => 'Môžete upraviť to, ako sa nákupný vozík zobrazí. Ak chcete vedieť viac, prečítajte si <a href="http://simplecartjs.com/documentation.html">stránku o nastaveniach formátu nákupného vozíka.</a>',
	'checkout' => 'Pokladňa',
	'checkout2email' => 'Platba e-mailom',
	'checkout2email_explication' => 'Objednávka bola odoslaná na túto e-mailovú adresu. Nepoužíva sa žiaden konkrétny spôsob platby.',
	'checkout_methods' => 'Spôsoby platby',
	'checkout_to' => 'Platba',

	// D
	'description' => 'Jednoduchý nákupný vozík v javascripte',
	'devise' => 'Mena',
	'devise_choix' => 'Výber meny',
	'devise_explication' => 'Peňažná mena, ktorou sa bude platiť (dopredu skontrolujte, či je kompatibilná s vybraným spôsobom platby)',
	'dineromail_country' => 'Krajina',
	'dineromail_country_argentine' => 'Argentína',
	'dineromail_country_brazil' => 'Brazília',
	'dineromail_country_chile' => 'Čile',
	'dineromail_country_explication' => 'Vyberte si krajinu, kde ste si zaregistrovali svoj účet DineroMail.',
	'dineromail_country_mexico' => 'Mexiko',
	'dineromail_currency' => 'Mena transakcie',
	'dineromail_currency_explication' => 'Typ meny na transakciu. Neprebieha žiaden prevod. Ceny treba zadať vo vybranej mene',
	'dineromail_currency_local' => 'Miestna mena',
	'dineromail_currency_usd' => 'Americký dolár',
	'dineromail_header_image' => 'Obrázok hlavičky',
	'dineromail_header_image_explication' => 'Absolútna URL loga, ktoré sa zobrazí v hlavičke DineroMailu (jpg alebo gif, 150 px x 50 px)',
	'dineromail_merchant_id' => 'Číslo obchodníka',
	'dineromail_merchant_id_explication' => 'Identifikačné číslo vášho účtu DineroMail bez overovacieho čísla',
	'dineromail_payments_methods' => 'Spôsob platby cez DineroMail ',
	'dineromail_payments_methods_explication' => 'Textový reťazec, ktorý uvádza povolené spôsoby platby. Ak chcete povoliť všetky spôsoby platby dostupné v danej krajine, nevypĺňajte toto pole.',
	'dineromail_see' => 'Zobraziť',
	'dineromail_seller_name' => 'Názov predávajúceho',
	'dineromail_seller_name_explication' => 'Text, ktorý v hlavičke nahradí e-mail. Ak ho nechcete nahradiť, nevypĺňajte toto pole.',

	// E
	'empty' => 'Prázdny',
	'error_url' => 'URL chyby pri nákupe',
	'error_url_explication' => 'URL kde bude zákazník presmerovaný v prípade chyby pri transakcii',

	// F
	'final_total' => 'Spolu',

	// G
	'google_merchant_id' => 'Č. obchodníka',
	'google_merchant_id_explication' => 'Identifikačné číslo vášho účtu Google Merchant',

	// H
	'header_name' => 'Názov',
	'header_price' => 'Cena',
	'header_quantity' => 'Množstvo',
	'header_total' => 'Celkom',

	// O
	'ok_url' => 'Adresa pre úspešný nákup',
	'ok_url_explication' => 'Adresa, na ktorú je návštevník presmerovaný v prípade úspešného dokončenia transakcie.',
	'other_parameters' => 'Iné parametre',

	// P
	'paypal_account' => 'Účet PayPal',
	'paypal_account_explication' => 'E-mail vášho PayPal účtu, ak nejaký máte.',
	'pending_url' => 'URL čakajúcej transakcie',
	'pending_url_explication' => 'URL, kde bude zákazník presmerovaný v prípade čakajúcej transakce',

	// S
	'shipping_cost' => 'Náklady na prepravu',
	'shipping_flat_rate' => 'Fixná sadzba poštovného',
	'shipping_flat_rate_explication' => 'K celej objednávke pridajte fixnú sadzbu poštovného',
	'shipping_quantity_rate' => 'Sadzba prepravy podľa množstva',
	'shipping_quantity_rate_explication' => 'Pridať fixné náklady na prepravu pre každú položku vo vozíku',
	'shipping_total_rate' => 'Percentuálna sadzba prepravy',
	'shipping_total_rate_explication' => 'Pridať náklady na prepravu podľa celkovej ceny objednávky',
	'subtotal' => 'Celkom',

	// T
	'tax_and_shipping' => 'Dane a preprava',
	'tax_cost' => 'Daň',
	'tax_rate' => 'Sadzba dane',
	'tax_rate_explication' => 'Sadzba dane ako pomer. Príklad: 0,19 DPH',
	'title' => 'SimpleCart',

	// Y
	'your_cart' => 'Váš vozík'
);

?>
