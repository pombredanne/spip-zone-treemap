<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'add_to_cart' => 'Afegir a la cistella',

	// B
	'buy' => 'Comprar',

	// C
	'c_dineromail' => 'DinerMail',
	'c_email' => 'Email',
	'c_google_checkout' => 'Google Checkout',
	'c_paypal' => 'PayPal',
	'cart_configuration' => 'Configuració de la cistella',
	'cart_headers' => 'Capçaleres de la cistella', # MODIF
	'cart_headers_explication' => 'Podeu editar els camps que mostra la cistella. Si en voleu conèixer més detalls, llegiu <a href="http://simplecartjs.com/documentation.html">Cart formatting configuration page</a>', # MODIF
	'checkout' => 'Finalitzar la compra',
	'checkout2email' => 'Enviar comanda per e-mail',
	'checkout2email_explication' => 'La comanda s\'envia a la següent adreça. No s\'utilitza un mitjà de pagament.', # MODIF
	'checkout_methods' => 'Mitjans de pagament',
	'checkout_to' => 'Pagar per mitjà de', # MODIF

	// D
	'description' => 'Una cistella de compres simple en Javascript',
	'devise' => 'Moneda', # NEW NEW
	'devise_choix' => 'Elección de la moneda', # NEW NEW
	'devise_explication' => 'La moneda que se utilizará para el pago (Compruebe la compatibilidad antes de m&eacute;todo de pago)', # NEW
	'dineromail_country' => 'País',
	'dineromail_country_argentine' => 'Argentina',
	'dineromail_country_brazil' => 'Brasil',
	'dineromail_country_chile' => 'Xile',
	'dineromail_country_explication' => 'Escull el país on vau enregistrar el compte DineroMail.', # MODIF
	'dineromail_country_mexico' => 'Mèxic',
	'dineromail_currency' => 'Moneda de la transacció',
	'dineromail_currency_explication' => 'Tipus de moneda de la transacció. No hi ha conversió de tipus. Els preus s\'han d\'indicar en la moneda seleccionada', # MODIF
	'dineromail_currency_local' => 'Moneda local',
	'dineromail_currency_usd' => 'Dòlars nord-americans', # MODIF
	'dineromail_header_image' => 'Imatge de capçalera', # MODIF
	'dineromail_header_image_explication' => 'URL absoluta del logotip a mostrar a la capçalera de DineroMail (jpg o gif, 150px x 50px)', # MODIF
	'dineromail_merchant_id' => 'Número de compte', # MODIF
	'dineromail_merchant_id_explication' => 'Número d\'identificació del teu compte DineroMail sense el dígit verificador', # MODIF
	'dineromail_payments_methods' => 'Mitjans de pagament a DineroMail',
	'dineromail_payments_methods_explication' => 'Cadena de text que defineix els mètodes de pagament permesos. Deixar en blanc per habilitat tots els disponibles al país.', # MODIF
	'dineromail_see' => 'Ver', # NEW
	'dineromail_seller_name' => 'Nom del venedor',
	'dineromail_seller_name_explication' => 'Llegenda que el venedor vol mostrar a l\'encapçalament. Deixar en blanc per mostrar l\'e-mail associat al compte.', # MODIF

	// E
	'empty' => 'Buidar',
	'error_url' => 'URL compra errònia', # MODIF
	'error_url_explication' => 'URL cap on es redirecciona al comprador en cas de transacció errònia', # MODIF

	// F
	'final_total' => 'Total',

	// G
	'google_merchant_id' => 'Identificador Merchant ID',
	'google_merchant_id_explication' => 'Número d\'identificació del teu compte Google Merchant', # MODIF

	// H
	'header_name' => 'Nom',
	'header_price' => 'Preu',
	'header_quantity' => 'Quantitat', # MODIF
	'header_total' => 'Total',

	// O
	'ok_url' => 'URL compra amb èxit', # MODIF
	'ok_url_explication' => 'URL cap on es redirecciona al comprador en cas de transacció amb èxit', # MODIF
	'other_parameters' => 'Altres paràmetres', # MODIF

	// P
	'paypal_account' => 'Compte PayPal',
	'paypal_account_explication' => 'Si tens un compte a PayPal, escriu el e-mail del teu compte',
	'pending_url' => 'URL compra pendent',
	'pending_url_explication' => 'URL cap a on es redirecciona al comprador en cas d\'una transacció pendent.',

	// S
	'shipping_cost' => 'Despeses d\'enviament', # MODIF
	'shipping_flat_rate' => 'Taxa fixa d\'enviament',
	'shipping_flat_rate_explication' => 'Afegeix una taxa fixa a l\'ordre completa', # MODIF
	'shipping_quantity_rate' => 'Despeses d\'enviament per quantitat', # MODIF
	'shipping_quantity_rate_explication' => 'Afegeix un import fix per cada ítem a la cistella',
	'shipping_total_rate' => 'Despeses d\'enviament com a percentatge del total', # MODIF
	'shipping_total_rate_explication' => 'Afegeix una despesa d\'enviament proporcional al cost de l\'ordre', # MODIF
	'subtotal' => 'Subtotal',

	// T
	'tax_and_shipping' => 'Impostos i despeses d\'enviament', # MODIF
	'tax_cost' => 'Impostos',
	'tax_rate' => 'Taxa impositiva',
	'tax_rate_explication' => 'Taxa d\'impostos. Exemple: 0.19 de l\'Impost al Valor Afegit',
	'title' => 'SimpleCart',

	// Y
	'your_cart' => 'La teva cistella'
);

?>
