<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/simplecart?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'add_to_cart' => 'افزودن به سبد',

	// B
	'buy' => 'خريد',

	// C
	'c_dineromail' => 'دينرو ميل (DineroMail)',
	'c_email' => 'ايميل ',
	'c_google_checkout' => 'صندوق گوگل',
	'c_paypal' => 'پي‌پال (PayPal)',
	'cart_configuration' => 'پيكربندي سبد',
	'cart_headers' => 'سرصفحه‌ي سبد', # MODIF
	'cart_headers_explication' => 'ميدان‌هاي سبد را ويرايش كنيد. بنگريد به <a href="http://simplecartjs.com/documentation.html"> براي جزئيات به صفحه پيكربندي كارت</a> <NEW>Editez les champs du panier. Voir <a href="http://simplecartjs.com/documentation.html">Cart formatting configuration page</a> pour détails', # MODIF
	'checkout' => 'پايان خريد',
	'checkout2email' => 'ارسال نظر با ايميل ',
	'checkout2email_explication' => 'سفارش به نشاني ايميل زير ارسال شد. از هيچ روش پرداخت خاصي استفاده نمي‌كند. ', # MODIF
	'checkout_methods' => 'روش‌هاي پرداخت',
	'checkout_to' => 'پرداخت با ', # MODIF

	// D
	'description' => 'يك كارت ساده‌ي جاوااسكريپت',
	'devise' => 'پول ',
	'devise_choix' => 'انتخاب پول',
	'devise_explication' => 'پولي كه براي پرداخت استفاده مي‌شود (از پيش مطمئن شويد كه با روش پرداخت مطابق باشد)', # MODIF
	'dineromail_country' => 'كشور',
	'dineromail_country_argentine' => 'آرژانتين',
	'dineromail_country_brazil' => 'برزيل ',
	'dineromail_country_chile' => 'شيلي',
	'dineromail_country_explication' => 'انتخاب كشوري كه در آن حساب دنيروميل (DeniroMail) ثبت كرد‌ه‌ايد ', # MODIF
	'dineromail_country_mexico' => 'مكزيك',
	'dineromail_currency' => 'پول معامله',
	'dineromail_currency_explication' => 'نوع پول براي معاله. پول تبديل نمي‌شود.قيمت بايد با پول انتخاب شده پرداخت شود. ', # MODIF
	'dineromail_currency_local' => 'پول محلي ',
	'dineromail_currency_usd' => 'دلار آمريكا', # MODIF
	'dineromail_header_image' => 'تصوير سرصفحه', # MODIF
	'dineromail_header_image_explication' => 'يو.آر.ال مطلق لوگو براي نمايش در سرصفحه‌ي دينروميل (جي.پي.گ  يا جي.اي.اف، 150 پيكسل در 150 پيكسل)', # MODIF
	'dineromail_merchant_id' => 'شماره حساب', # MODIF
	'dineromail_merchant_id_explication' => 'شماره شناسايي (ID) حساب دينروميل شما بدون رقم تأييدكننده', # MODIF
	'dineromail_payments_methods' => 'روش‌هاي پرداخت دينروميل (DineroMail)',
	'dineromail_payments_methods_explication' => 'زنجيره‌ي متن كه روش‌هاي پرداخت مجاز را تعريف مي‌كند. آن را خالي بگذاريد تا تمام روش‌هاي ممكن پرداخت در آن كشور فعال باشد.', # MODIF
	'dineromail_see' => 'بنگريد',
	'dineromail_seller_name' => 'نام فروشنده',
	'dineromail_seller_name_explication' => 'متني كه فروشنده مي‌خواهد در سرصفحه نشان دهد', # MODIF

	// E
	'empty' => 'خالي',
	'error_url' => 'خطاي يو.آر.ال', # MODIF
	'error_url_explication' => 'يو.آر.ال كه مشتري در صورت بروز خطا در معامله به آن هدايت مي‌شود', # MODIF

	// F
	'final_total' => 'كل ',

	// G
	'google_merchant_id' => 'ID شناسايي بازرگاني ',
	'google_merchant_id_explication' => 'Numéro d\'identification de votre compte marchand Google', # MODIF

	// H
	'header_name' => 'Nom', # NEW
	'header_price' => 'Prix', # NEW
	'header_quantity' => 'Quantité', # MODIF
	'header_total' => 'Total', # NEW

	// O
	'ok_url' => 'URL achat réussi', # MODIF
	'ok_url_explication' => 'URL où sera redirigé l\'acheteur en cas de transaction réussie.', # MODIF
	'other_parameters' => 'Autres paramètres', # MODIF

	// P
	'paypal_account' => 'Compte PayPal', # NEW
	'paypal_account_explication' => 'Si vous avez un compte PayPal, enregistrez l\'email de votre compte.', # NEW
	'pending_url' => 'URL achat en suspend', # NEW
	'pending_url_explication' => 'URL de redirection pour l\'acheteur en cas de transaction suspendue.', # NEW

	// S
	'shipping_cost' => 'Coût d\'envoi', # MODIF
	'shipping_flat_rate' => 'Taxe fixe de l\'envoi', # NEW
	'shipping_flat_rate_explication' => 'Ajouter une taxe fixe à l\'ordre complet', # MODIF
	'shipping_quantity_rate' => 'Coût de l\'envoi par quantité', # MODIF
	'shipping_quantity_rate_explication' => 'Ajouter un montant fixe pour chaque item', # NEW
	'shipping_total_rate' => 'Coût d\'envoi en pourcentage de la somme totale', # MODIF
	'shipping_total_rate_explication' => 'Ajouter un coût d\'envoi proportionnel au coût de la commande', # MODIF
	'subtotal' => 'Sous-total', # NEW

	// T
	'tax_and_shipping' => 'Frais et coût d\'envoi', # MODIF
	'tax_cost' => 'Frais', # NEW
	'tax_rate' => 'Taxe imposable', # NEW
	'tax_rate_explication' => 'Taux imposable. Exemple: 0.19 de frais de TVA', # NEW
	'title' => 'SimpleCart', # NEW

	// Y
	'your_cart' => 'Votre panier' # NEW
);

?>
