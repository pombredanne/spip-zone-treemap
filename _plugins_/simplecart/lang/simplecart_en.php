<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/simplecart?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'add_to_cart' => 'Add to Cart',

	// B
	'buy' => 'Buy',

	// C
	'c_dineromail' => 'DineroMail',
	'c_email' => 'Email',
	'c_google_checkout' => 'Google Checkout',
	'c_paypal' => 'PayPal',
	'cart_configuration' => 'Cart configuration',
	'cart_headers' => 'Cart headers', # MODIF
	'cart_headers_explication' => '<You can format how the cart is displayed. Read the <a href="http://simplecartjs.com/documentation.html">Cart formatting configuration page</a> for details', # MODIF
	'checkout' => 'Checkout',
	'checkout2email' => 'Checkout by email',
	'checkout2email_explication' => 'The order is sent to the following email address. This does NOT use any specific payment method.', # MODIF
	'checkout_methods' => 'Payment methods',
	'checkout_to' => 'Checkout by', # MODIF

	// D
	'description' => 'A simple JavaScript shopping cart',
	'devise' => 'Currency',
	'devise_choix' => 'Currency selection',
	'devise_explication' => 'The currency which will be used for the payment (Check beforehand to make sure that it is compatible with the selected payment method)', # MODIF
	'dineromail_country' => 'Country',
	'dineromail_country_argentine' => 'Argentina',
	'dineromail_country_brazil' => 'Brazil',
	'dineromail_country_chile' => 'Chile',
	'dineromail_country_explication' => 'Select the country where you registered your DineroMail account.', # MODIF
	'dineromail_country_mexico' => 'Mexico',
	'dineromail_currency' => 'Transaction currency',
	'dineromail_currency_explication' => 'Currency type for the transaction. There is no currency conversion. Prices must be given in the selected currency', # MODIF
	'dineromail_currency_local' => 'Local currency',
	'dineromail_currency_usd' => 'US dollars', # MODIF
	'dineromail_header_image' => 'Header image', # MODIF
	'dineromail_header_image_explication' => 'The absolute URL of a logo to show on the DineroMail header (jpg or gif, 150px x 50px)', # MODIF
	'dineromail_merchant_id' => 'Merchant number', # MODIF
	'dineromail_merchant_id_explication' => 'Merchant number ID of your DineroMail account, without the verification digit', # MODIF
	'dineromail_payments_methods' => 'DineroMail payment method',
	'dineromail_payments_methods_explication' => 'Text string which defines the authorised payment methods. Leave it blank to enable all methods available for the country', # MODIF
	'dineromail_see' => 'See',
	'dineromail_seller_name' => 'Seller name',
	'dineromail_seller_name_explication' => 'Text replacement for \'email\' in the header. Leave it blank if you do not want it replaced', # MODIF

	// E
	'empty' => 'Empty',
	'error_url' => 'Purchase error URL', # MODIF
	'error_url_explication' => 'URL where the customer is redirected in case of an error in the transaction', # MODIF

	// F
	'final_total' => 'Total',

	// G
	'google_merchant_id' => 'Merchant ID',
	'google_merchant_id_explication' => 'ID number of your Google Merchant account', # MODIF

	// H
	'header_name' => 'Name',
	'header_price' => 'Price',
	'header_quantity' => 'Quantity', # MODIF
	'header_total' => 'Total',

	// O
	'ok_url' => 'Successful purchase URL', # MODIF
	'ok_url_explication' => 'URL where customer is redirected in case of a successful transaction', # MODIF
	'other_parameters' => 'Other parameters', # MODIF

	// P
	'paypal_account' => 'PayPal account',
	'paypal_account_explication' => 'Email of your PayPal account, if you have one.',
	'pending_url' => 'Purchase pending URL',
	'pending_url_explication' => 'URL where customer is redirected in case of a pending transaction',

	// S
	'shipping_cost' => 'Shipping cost', # MODIF
	'shipping_flat_rate' => 'Shipping flat rate',
	'shipping_flat_rate_explication' => 'Add a flat shipping rate to the entire order', # MODIF
	'shipping_quantity_rate' => 'Shipping rate by quantity', # MODIF
	'shipping_quantity_rate_explication' => 'Add a fix shipping cost for each item in the cart ',
	'shipping_total_rate' => 'Total shipping rate percentage', # MODIF
	'shipping_total_rate_explication' => 'Add a shipping cost proportional to the total order cost', # MODIF
	'subtotal' => 'Subtotal',

	// T
	'tax_and_shipping' => 'Taxes and shipping', # MODIF
	'tax_cost' => 'Tax cost',
	'tax_rate' => 'Tax rate',
	'tax_rate_explication' => 'Tax rate as proportion. Example: 0.19 of VAT',
	'title' => 'SimpleCart',

	// Y
	'your_cart' => 'Your cart'
);

?>
