<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'add_to_cart' => 'In den Warenkorb',

	// B
	'buy' => 'Kaufen',

	// C
	'c_dineromail' => 'DineroMail',
	'c_email' => 'E-Mail',
	'c_google_checkout' => 'Google Checkout',
	'c_paypal' => 'PayPal',
	'cart_configuration' => 'Warenkorb-Konfiguration',
	'cart_headers' => 'Warenkorb Kopfzeilen', # MODIF
	'cart_headers_explication' => 'Sie können einstellen, wie der Warenkorn angezeigt wird. Die Seite zur <a href="http://simplecartjs.com/documentation.html">Warenkorb-Konfiguration</a> erläutert Details.', # MODIF
	'checkout' => 'Bezahlen',
	'checkout2email' => 'Bezahlvorgang per Mail',
	'checkout2email_explication' => 'Die Bestellung wurde an folgende Adresse geschickt (Damit wird keine bestimmte Zahlungsmethode festgelegt):', # MODIF
	'checkout_methods' => 'Zahlungsmethoden',
	'checkout_to' => 'Bezahlen per', # MODIF

	// D
	'description' => 'Ein einfacher Warenkorb in Javascript',
	'devise' => 'Moneda', # NEW NEW
	'devise_choix' => 'Elección de la moneda', # NEW NEW
	'devise_explication' => 'La moneda que se utilizará para el pago (Compruebe la compatibilidad antes de m&eacute;todo de pago)', # NEW
	'dineromail_country' => 'Land',
	'dineromail_country_argentine' => 'Argentinien',
	'dineromail_country_brazil' => 'Brasilien',
	'dineromail_country_chile' => 'Chile',
	'dineromail_country_explication' => 'Wählen sie das Land, in dem sie ihr DineroMail Konto eröffnet haben.', # MODIF
	'dineromail_country_mexico' => 'Mexiko',
	'dineromail_currency' => 'Transaktionswährung',
	'dineromail_currency_explication' => 'Währung für die Transaktion. Es findet keine Umrechnung statt. Preise müssen in der augewählten Wärung angegeben werden.', # MODIF
	'dineromail_currency_local' => 'Lokale Währung',
	'dineromail_currency_usd' => 'US Dollar', # MODIF
	'dineromail_header_image' => 'Titelbild', # MODIF
	'dineromail_header_image_explication' => 'Absoluter URL eines Logos für den DineroMail Kopf(jpg oder gif, 150px x 50px)', # MODIF
	'dineromail_merchant_id' => 'Händler-Nummer', # MODIF
	'dineromail_merchant_id_explication' => 'Händler-Nummer of ihres DineroMail Kontos, ohne Prüfnummer', # MODIF
	'dineromail_payments_methods' => 'DineroMail Zahlungsmethoden',
	'dineromail_payments_methods_explication' => 'Zeichenkette zum Aktivieren von Zahlungsmethoden. Frei lassen um alle Zahlungsmethoden für ein Land zu autorisieren', # MODIF
	'dineromail_see' => 'Ver', # NEW
	'dineromail_seller_name' => 'Händlername',
	'dineromail_seller_name_explication' => 'Ersatztext für \'email\' im Kopf. Frei lassen um Ersetzung auszuschalten. ', # MODIF

	// E
	'empty' => 'Leer',
	'error_url' => 'Fehler-URL', # MODIF
	'error_url_explication' => 'Dieser URL wird dem Kunde beim Fehlschlagen des Bezahlens angezeigt.', # MODIF

	// F
	'final_total' => 'Summe',

	// G
	'google_merchant_id' => 'Händler ID',
	'google_merchant_id_explication' => 'ID Ihres Google Händler Kontos', # MODIF

	// H
	'header_name' => 'Name',
	'header_price' => 'Preis',
	'header_quantity' => 'Menge', # MODIF
	'header_total' => 'Summe',

	// O
	'ok_url' => 'Erfolgs-URL', # MODIF
	'ok_url_explication' => 'Dieser URL wird dem Kunden nach erfolgreichem Bezahlen angezeigt.', # MODIF
	'other_parameters' => 'Andere Einstellungen', # MODIF

	// P
	'paypal_account' => 'PayPal Konto',
	'paypal_account_explication' => 'Mailadresse ihres Paypal Kontos',
	'pending_url' => 'URL im Wartezustand',
	'pending_url_explication' => 'Dieser URL wird dem Kunden bei einem Bezahlvorgang im Wartezustand angezeigt.',

	// S
	'shipping_cost' => 'Versandkosten', # MODIF
	'shipping_flat_rate' => 'Pauschale Versandkosten',
	'shipping_flat_rate_explication' => 'Fügen sie der gesamten Bestellung pauschale Versandkosten hinzu.', # MODIF
	'shipping_quantity_rate' => 'Mengenabhängige Versandkosten', # MODIF
	'shipping_quantity_rate_explication' => 'Fügen sie einen festen Betrag Versandkosten pro Objekt im Warenkorb hinzu.',
	'shipping_total_rate' => 'Gesamte Versandkosten', # MODIF
	'shipping_total_rate_explication' => 'Fügen sie den zu Gesamtkosten proportionale Versandkosten hinzu', # MODIF
	'subtotal' => 'Zwischensumme',

	// T
	'tax_and_shipping' => 'Steuern und Versandkosten', # MODIF
	'tax_cost' => 'Steuern',
	'tax_rate' => 'Steuersatz',
	'tax_rate_explication' => 'Steuersatz als Faktor. Z.B.: 0.19 MwSt.',
	'title' => 'SimpleCart',

	// Y
	'your_cart' => 'Ihr Warenkorb'
);

?>
