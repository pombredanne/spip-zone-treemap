<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/simplecart?lang_cible=fr
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'add_to_cart' => 'Ajouter au panier',

	// B
	'buy' => 'Acheter',

	// C
	'c_dineromail' => 'DineroMail',
	'c_email' => 'Email',
	'c_google_checkout' => 'Google Checkout',
	'c_paypal' => 'PayPal',
	'cart_configuration' => 'Configuration du panier',
	'cart_headers' => 'Entête du panier',
	'cart_headers_explication' => 'Editez les champs du panier. Voir <a href="http://simplecartjs.com/documentation.html">Cart formatting configuration page</a> pour détails',
	'checkout' => 'Finaliser l\'achat',
	'checkout2email' => 'Envoyer une commande par email',
	'checkout2email_explication' => 'La commande est envoyée à l\'adresse suivante. On n\'utilise pas de moyen de paiement.',
	'checkout_methods' => 'Moyens de paiement',
	'checkout_to' => 'Payé par',

	// D
	'description' => 'Un panier d\'achat simple en javascript',
	'devise' => 'Devise',
	'devise_choix' => 'Choix de la devise',
	'devise_explication' => 'La devise qui sera utilisée pour le paiement (Vérifiez avant la compatibilité avec le moyen de paiement)',
	'dineromail_country' => 'Pays',
	'dineromail_country_argentine' => 'Argentine',
	'dineromail_country_brazil' => 'Bresil',
	'dineromail_country_chile' => 'Chili',
	'dineromail_country_explication' => 'Choisir le pays où a été enregistré le compte DineroMail.',
	'dineromail_country_mexico' => 'Mexique',
	'dineromail_currency' => 'Monnaie de la transaction',
	'dineromail_currency_explication' => 'Type de monnaie de la transaction. Il n\'y a pas de conversion de ce type. Les prix doivent être indiqués dans la monnaie sélectionnée',
	'dineromail_currency_local' => 'Monnaie locale',
	'dineromail_currency_usd' => 'Dollars américains',
	'dineromail_header_image' => 'Image de l\'en-tête',
	'dineromail_header_image_explication' => 'URL absolue du logo à montrer dans l\'en-tête de DineroMail (jpg ou gif, 150px x 50px)',
	'dineromail_merchant_id' => 'Numéro de compte',
	'dineromail_merchant_id_explication' => 'Numéro d\'identification de votre compte DineroMail sans l\'indicateur digital ',
	'dineromail_payments_methods' => 'Moyen de paiement DineroMail',
	'dineromail_payments_methods_explication' => 'Chaine de texte qui définit les moyens de paiement autorisés. Laisser vide pour permettre tous ceux disponibles dans le pays.',
	'dineromail_see' => 'Voir',
	'dineromail_seller_name' => 'Nom du vendeur',
	'dineromail_seller_name_explication' => 'Légende que le vendeur veut afficher dans l\'en-tête. Laisser vide pour afficher l\'adresse email associé au compte.',

	// E
	'empty' => 'Vider',
	'error_url' => 'URL achat erronné',
	'error_url_explication' => 'URL où se redirige l\'acheteur en cas de transaction erronnée',

	// F
	'final_total' => 'Total',

	// G
	'google_merchant_id' => 'Id de l\'identification du marchand',
	'google_merchant_id_explication' => 'Numéro d\'identification de votre compte marchand Google',

	// H
	'header_name' => 'Nom',
	'header_price' => 'Prix',
	'header_quantity' => 'Quantité',
	'header_total' => 'Total',

	// O
	'ok_url' => 'URL lors d\'un achat réussi',
	'ok_url_explication' => 'URL où sera redirigé l\'acheteur en cas de transaction réussie.',
	'other_parameters' => 'Autres paramètres',

	// P
	'paypal_account' => 'Compte PayPal',
	'paypal_account_explication' => 'Si vous avez un compte PayPal, enregistrez l\'email de votre compte.',
	'pending_url' => 'URL achat en suspend',
	'pending_url_explication' => 'URL de redirection pour l\'acheteur en cas de transaction suspendue.',

	// S
	'shipping_cost' => 'Coût d\'envoi',
	'shipping_flat_rate' => 'Taxe fixe de l\'envoi',
	'shipping_flat_rate_explication' => 'Ajouter une taxe fixe à l\'ordre complet',
	'shipping_quantity_rate' => 'Coût de l\'envoi par quantité',
	'shipping_quantity_rate_explication' => 'Ajouter un montant fixe pour chaque item',
	'shipping_total_rate' => 'Frais d\'envoi en pourcentage de la somme totale',
	'shipping_total_rate_explication' => 'Ajouter un coût d\'envoi proportionnel au coût de la commande',
	'subtotal' => 'Sous-total',

	// T
	'tax_and_shipping' => 'Frais et coût d\'envoi',
	'tax_cost' => 'Frais',
	'tax_rate' => 'Taxe imposable',
	'tax_rate_explication' => 'Taux imposable. Exemple: 0.19 de frais de TVA',
	'title' => 'SimpleCart',

	// Y
	'your_cart' => 'Votre panier'
);

?>
