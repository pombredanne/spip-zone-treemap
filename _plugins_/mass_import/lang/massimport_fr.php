<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
$GLOBALS[$GLOBALS['idx_lang']] = array(
  'icone' => 'Import d\'articles en masse',
  'page_mass_import' => 'Import en masse',
  'titre_mass_import' => 'Importer massivement des articles',
  'info_page' => 'Cette page permet d\'importer rapidement et en masse des articles dans SPIP sans passer par toutes les &eacute;tapes classiques',
  'rub_num' => 'Num&eacute;ro de la rubrique dans lequel importer les articles',
	'sep_interart' => 'S&eacute;parateur entre les articles',
	'sep_art' => 'S&eacute;parateur dans un article entre le titre et le corps du texte',
	'statut' => 'Statut des articles &agrave; importer:',
	'convert_url' =>'Convertir les URLs en syntaxe SPIP',	
	'text_import' => 'Texte &agrave; int&eacute;grer',
	'import' => 'Importer',
	'error_norub' => 'Erreur: la rubrique selectionn&eacute;e n\'existe pas.',
	'rub_dest' => 'Rubrique destination',
	'article_sucess' => 'Article import&eacute; n&ordm;:',
	'new_import' => 'Importer de nouveaux articles',
	'rub_target' => 'Rubrique destination:'

);

?>
