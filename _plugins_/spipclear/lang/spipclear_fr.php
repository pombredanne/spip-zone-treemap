<?php

// This is a SPIP-forum module file  --  Ceci est un fichier module de SPIP-forum

$GLOBALS[$GLOBALS['idx_lang']] = array(

//A
'ajouter_commentaire' => 'Ajouter un commentaire',
'archive' => 'Archive', // Non utilise
'archives' => 'Archives',
'archives_precedents' => 'Pr&eacute;c&eacute;dents',
'archives_suivants' => 'Suivants',
'attime' => '&agrave;',
'aucune_archive' => 'Aucune archive', // Non utilise
'aucune_categorie' => 'Aucune cat&eacute;gorie', // Non utilise
'aucun_commentaire' => 'Aucun commentaire', // Non utilise
'aucune_langue' => 'Aucune langue', // Non utilise
'aucun_lien' => 'Auncun lien', // Non utilise
'aucun_trackback' => 'Aucun trackback', // Non utilise

//B
'boss' => 'Administrateur', // Oubli
'bouton_configurer' => 'Configurer SpipClear', // CFG

//C
'calendrier' => 'Calendrier', // Non utilise
'categorie' => 'Cat&eacute;gorie', // Non utilise
'categories' => 'Cat&eacute;gories',
'commentaire' => 'commentaire',
'commentaires' => 'commentaires',
'commentaires_pour' => 'commentaires pour',
'commentaires_fermes' => 'Les commentaires pour ce billet sont ferm&eacute;s',
'credits' => '<a href="http://www.spip.net/fr">motoris&eacute; par SPIP</a>,
<a href="http://www.spip-contrib.net/SpipClear">propuls&eacute; par Spip.Clear</a>',

//D
'date_jour_abbr_1' => 'dim', // Non utilise
'date_jour_abbr_2' => 'lun', // Non utilise
'date_jour_abbr_3' => 'mar', // Non utilise
'date_jour_abbr_4' => 'mer', // Non utilise
'date_jour_abbr_5' => 'jeu', // Non utilise
'date_jour_abbr_6' => 'ven', // Non utilise
'date_jour_abbr_7' => 'sam', // Non utilise
'de' => 'de', // Non utilise

//E
'explication_configurer' => 'Consulter la <a href="http://www.spip-contrib.net/Spip-Clear" class="spip_out">documentation</a> sur SPIP-Contrib.',

//F
'fil_rss' => 'Fil RSS des billets',
'fil_rss_commentaires' => 'Fil RSS des commentaires',
'fil_atom' => 'Fil Atom des billets',
'fil_atom_commentaires' => 'Fil Atom des commentaires',

//G
'go_main' => 'Aller au contenu',
'go_sidebar' => 'Aller au menu',
'go_search' => 'Aller &agrave; la recherche',

//H
'hebergement' => 'h&eacute;bergement par',

//I
'info_choix_theme' => 'Choisissez le th&egrave;me que vous souhaitez utiliser pour votre blog', // CFG

//L
'label_theme_defaut' => 'Th&egrave;me par d&eacute;faut (type DotClear)', // CFG
'label_theme_perso' => 'Th&egrave;me perso (&agrave; d&eacute;finir ci-dessous)', // CFG
'label_nom_theme' => 'Saisissez le nom du th&egrave;me perso (correspond &agrave xxxx du sous-r&eacute;pertoire squelettes/themes/xxxx/)', // CFG
'langue' => 'Langue', // Non utilise
'langues' => 'Langues',
'legend_layout' => 'Pr&eacute;sentation', // CFG
'legend_theme' => 'Th&egrave;me', // CFG
'lien' => 'Lien', // Non utilise
'liens' => 'Liens',
'lire_la_suite' => 'Lire la suite',
'look_sommaire' => 'Personnalisation de la page d\'accueil',
'look_navigation' => 'Personnalisation du bloc de navigation',

//N
'navigation_desactiver_calendrier' => 'Ne pas afficher le calendrier dans la navigation',

//O
'oksearch' => 'ok',

//P
'permalink_pour' => 'Lien permanent vers',

//R
'recherche' => 'Recherche',
'resultat_recherche' => 'R&eacute;sultats pour votre recherche de',
'rss_pour' => 'fil RSS des commentaires de',
'rss_commentaire_article' => 'Fil des commentaires de ce billet',

//S
'secteurs_spipclear' => 'Secteurs SpipClear',
'secteur_spipclear_aucun' => 'Aucun',
'selection' => '&agrave; retenir',
'site_comment' => 'site',
'syndication' => 'Syndication',
'selectionner_theme' => 'Cliquez pour s&eacute;lectionner ce th&ecirc;me',
'sommaire_afficher_tags' => 'Ne pas afficher les tags dans les listes d\'articles (page d\'accueil, r&eacute;sultats de recherche, etc).',
'sommaire_articles_entiers' => 'Afficher les articles en entier (par d&eacute;faut, seule une introduction est affich&eacute;e)',
'sommaire_spipclear' => 'Appliquer SpipClear sur la page d\'accueil du site (ne fonctionne que pour un secteur).',

//T
'tag' => 'Tag',
'texte_erreur1' => 'La recherche de',
'texte_erreur2' => 'ne donne aucun r&eacute;sultat.',
'titre_erreur' => 'Erreur :',
'titre_configurer' => 'Configurer SpipClear',
'titre_form_configurer' => 'Configuration',
'trackback' => 'trackback', // Non utilise
'trackbacks' => 'trackbacks', // Non utilise
'trackbacks_pour_faire' => 'Pour faire un trackback sur ce billet', // Non utilise
'trackbacks_fermes' => 'Les trackbacks pour ce billet sont ferm&eacute;s.', // Non utilise
'themes_installes'=> '<strong>Les th&egrave;mes install&eacute;s sur ce spip</strong> (cliquez sur un l\'icone d\'un th&egrave;me pour le s&eacute;lectionner)',

//U
'un_seul_mot' => 'un seul mot', // Oubli

// V
'voir_capture'=> 'Afficher la capture d\'&eacute;cran',

);

?>
