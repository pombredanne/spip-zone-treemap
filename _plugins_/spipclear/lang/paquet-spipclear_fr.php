<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-spipclear
// Langue: fr
// Date: 14-03-2012 13:01:24
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'spipclear_description' => 'SpipClear est conçu pour transformer un secteur en blog ou carnet web. Pour cela, il s\'inspire de DotClear, un système de gestion de blog développé en php par Neokraft.net. Il est nécessaire de créer, a minima, les pages sommaire, article-x et rubrique-x pour activer le blog sur le secteur choisi.',
	'spipclear_slogan' => 'Un site à la dotclear en Spip',
);
?>