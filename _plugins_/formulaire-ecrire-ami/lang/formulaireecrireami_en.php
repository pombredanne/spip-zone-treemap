<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(


# formulaire_ecrire_ami
'envoyer_a_un_ami'		=> "Send to a friend",
'previsualisation'		=> "Message preview",
'message_envoye'		=> "Message sent !",
'retour'				=> "Return",
'email_ami'				=> "Email of your friend",
'email_ami_ko'			=> "The email of your friend is not valid",
'votre_nom'				=> "Your name",
'votre_nom_ko'			=> "Yout name is empty or invalid",
'commentaire'			=> "Comment",
'previsualiser'			=> "Preview",
'confirmer'				=> "Confirm",

# notifications
'a_souhaite_envoyer_lien'		=> "sent you a link from ".$GLOBALS['meta']['nom_site'],
'a_souhaite_envoyer_image'		=> "sent you a picture from ".$GLOBALS['meta']['nom_site'],
'a_laisse_commentaire'			=> "left a comment for you",
'a_souhaite_recommander_site'	=> "wanted to recommend you this website :",

'lien' => "Link",

'zzz' => ''

);

?>