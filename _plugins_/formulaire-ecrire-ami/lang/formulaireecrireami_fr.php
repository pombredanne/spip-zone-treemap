<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(


# formulaire_ecrire_ami
'envoyer_a_un_ami'		=> "Envoyer &agrave; un ami",
'previsualisation'		=> "Pr&eacute;visualisation du message",
'message_envoye'		=> "Message envoy&eacute; !",
'retour'				=> "Retour",
'email_ami'				=> "Email de votre ami",
'email_ami_ko'			=> "L'email de votre ami n'est pas valide",
'votre_nom'				=> "Votre nom",
'votre_nom_ko'			=> "Votre nom est vide ou invalide",
'commentaire'			=> "Commentaire",
'previsualiser'			=> "Pr&eacute;visualiser",
'confirmer'				=> "Confirmer",

# notifications
'a_souhaite_envoyer_lien'		=> "a souhaité vous envoyer un lien depuis ".$GLOBALS['meta']['nom_site'],
'a_souhaite_envoyer_image'		=> "a souhaité vous envoyer une image depuis ".$GLOBALS['meta']['nom_site'],
'a_laisse_commentaire'			=> "a laissé un commentaire à votre attention",
'a_souhaite_recommander_site'	=> "a souhaité vous recommander le site suivant :",

'lien' => "Lien",

'zzz' => ''

);

?>