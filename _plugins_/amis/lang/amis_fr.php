<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'ajouter_a_mes_amis' => 'ajouter à mes amis',

'des_invitations_amis' => '@nb@ invitations d\'ami',

'envoyer_un_message' => 'envoyer un message',

'invitations' => 'Mes invitations',
'inviter_ami' => 'Inviter un ami',

'label_email' => 'e-mail',
'label_message' => 'Ajouter un message personnel',
'label_inviter' => 'Inviter',
'label_recherche_ami' => 'Rechercher',
'label_rechercher' => 'Rechercher',

'mail_ajouterami_texte' => 'Bonjour @nom@ ! @ami@ a accepté votre invitation sur @nom_site@.',
'mail_ajouterami_titre' => 'Invitation acceptée sur le site @nom_site@.',
'mail_inviterami_texte' => 'Bonjour @nom@ ! @ami@ souhaite vous ajouter à sa liste d\'amis sur @nom_site@.',
'mail_inviterami_titre' => 'Invitation sur le site @nom_site@.',
'mail_inviterami_explication' => 'Gérer votre liste d\'amis :',
'mail_rejoignez_sujet' => '@nom@ vous invite sur @nom_site@',
'mail_rejoignez_message' => 'Inscrivez vous vite sur @nom_site@ (@url_site@) et rejoignez @nom@ dans la communauté des contributeurs.',

'nb_amis_un' => 'un ami',
'nb_amis' => '@nb@ amis',

'rechercher_ami' => 'Rechercher un ami',

'sollicitations' => 'Sollicitations',

'titre_amis' => 'Amis',
'titre_amis_auteur' => 'Amis de @nom@',

'une_invitation_amis' => '1 invitation d\'ami',

'votre_ami_a_ete_invite' => 'Votre demande a été envoyée à @email@'

);
?>