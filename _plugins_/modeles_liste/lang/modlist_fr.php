<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'article_inserer_un_modele'=> "Ins&eacute;rer un mod&egrave;le",
'article_inserer_un_modele_detail' => "Vous pouvez ins&eacute;rer des modeles dans vos articles afin d'enrichir leurs contenus. Choisissez un modele dans la liste ci-dessous et recopiez le raccourci dans le texte avant de le modifier a votre guise.",
'article_recopier_raccourci' => "Recopiez ce raccourci dans le texte pour ins&eacute;rer ce modele",
);


?>