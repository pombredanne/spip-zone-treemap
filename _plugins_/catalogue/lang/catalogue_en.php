<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activites' => 'Activities',
	'ajouter_variante' => 'Add a variante',

	// B
	'bouton_ajouter_variante' => 'Add a variante',

	// C
	'catalogue' => 'Plugin Catalogue',
	'configurer_le_catalogue' => "Configure the catalogue",
	'creer_variante' => 'Create a new variante of price for this item',
	'cat_variantes' => "Variantes",
	'confirmer_suppression_variante' => "Do you want to delete this variante of price ?",

	// E
	'edition_variante' => '&Eacute;dition variante',
	'editer_variante' => 'Edit the variante',

	// G
	'go' => 'Go !',

	// I
	'info_variante_edit' => "This page is under development, please accept our apologies.<br /><br />But you can totally edit the variant that appears on this page, and thanks to \"crayons\" : Double-click the field to edit, or click the small image that appears when you pass over an editable field.",
	'info_gauche_numero_variante' => "N<sup>o</sup>",
	'info_variante_utilisee_par' => "Variant used by:",
	'info_tous_articles_catalogue' => "All catalog items",

	// L
	'liste_articles' => "List all articles of the site with the attribute of catalog item.",
	'label_titre' => "Title",
	'label_descriptif' => "Description",
	'label_prix_ht' => "Price ex VAT",
	'label_tva' => "VAT rate", // value added tax
	'label_quantite' => "Quantity",
	'label_unite' => "Unit",
	'label_date' => "Date",
	'label_date_redac' => "Date of preparation",
	'label_statut' => "Status",
	'label_statut_redac' => "In progress",
	'label_statut_prop' => "Proposed for publication",
	'label_statut_publie' => "Published",

	// N
	'nom_plugin' => 'Catalogue',
	'neant' => 'none',

	// O
	'ok' => 'OK !',
	'option_dispo' => 'Available option',
	'options_dispo' => 'Available options',

	// P
	'prix_avec_taxes' => 'i.a.t', // including all taxes
	'prix_sans_taxes' => 'ex VAT', // excluding VAT

	// T
	'tarif' => 'Price',
	'tarifs' => 'Prices',
	'titre_bloc_catalogue' => "CATALOGUE : article prices and options",
	'titre_page_gestion' => 'Catalogue managemement page',

);
?>