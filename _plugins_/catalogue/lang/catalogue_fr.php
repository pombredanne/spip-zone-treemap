<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activites' => 'Activit&eacute;s',
	'ajouter_variante' => 'Ajouter une variante',

	// B
	'bouton_ajouter_variante' => 'Ajouter une variante',

	// C
	'catalogue' => 'Catalogue', // Plugin Catalogue
	'configurer_le_catalogue' => "Configurer le catalogue",
	'creer_variante' => 'Cr&eacute;er une nouvelle variante de prix pour cet article',
	'cat_variantes' => "Variantes",
	'confirmer_suppression_variante' => "Souhaitez-vous supprimer cette variante de prix ?",

	// E
	'edition_variante' => '&Eacute;dition variante',
	'editer_variante' => '&Eacute;diter la variante',

	// G
	'go' => 'Go !',

	// I
	'info_variante_edit' => 'Cette page est en cours de d&eacute;veloppement, veuillez nous en excuser.<br /><br />Cependant vous pouvez tout &agrave; fait &eacute;diter la variante qui s\'affiche sur cette page, et ceci gr&acirc;ce aux "crayons". Double-cliquez sur le champ &agrave; &eacute;diter, ou cliquez sur la petite image qui apparait au survol d\'un champ modifiable.',
	'info_gauche_numero_variante' => "N<sup>o</sup>;",
	'info_variante_utilisee_par' => "Variante utilis&eacute;e par :",
	'info_tous_articles_catalogue' => "Tous les articles du catalogue",

	// L
	'liste_articles' => "Liste de tous les articles du site ayant le statut d'article de catalogue.",
	'label_titre' => "Titre",
	'label_descriptif' => "Descriptif",
	'label_prix_ht' => "Prix HT",
	'label_tva' => "TVA",
	'label_quantite' => "Quantit&eacute;",
	'label_unite' => "Unit&eacute;", // pas que SI (cl, km, kg, etc.) aussi "pack/lot de N", etc.
	'label_date' => "Date",
	'label_date_redac' => "Date de r&eacute;daction",
	'label_statut' => "Statut",
	'label_statut_redac' => "En cours de r&eacute;daction",
	'label_statut_prop' => "Propos&eacute; à publication",
	'label_statut_publie' => "Publi&eacute;",

	// N
	'nom_plugin' => 'Catalogue',
	'neant' => 'n&eacute;ant',
	'nouvelle_variante' => "Nouvelle variante",

	// M
	'message_aucune_variante' => '<strong>Cet article n\'est pas un article du catalogue</strong><br />Pour ajouter cet article au catalogue, il suffit d\'indiquer un prix et quelques autres informations en cliquant sur "ajouter une variante".',
	'modifier' => 'Modifier',
	'modifier_variante' => "Modifier",
	'message_aucun_article' => "Aucun article n'est encore dans le catalogue",

	// O
	'ok' => 'OK !',
	'option_dispo' => 'Option disponible',
	'options_dispo' => 'Options disponibles',

	// P
	'prix_avec_taxes' => 'ttc',
	'prix_sans_taxes' => 'ht',

	// R
	'retour_article' => 'Retour &agrave; l\'article ',
	'rubriques_catalogue' => "Rubriques d'application du catalogue",

	// S
	'supprimer' => 'Supprimer',
	'supprimer_variante' => "Supprimer",

	// T
	'tarif' => 'Prix unique',
	'tarifs' => 'Tarifs',
	'titre_bloc_catalogue' => "CATALOGUE : prix et options li&eacute;s &agrave; l'article",
	'titre_page_gestion' => 'Page de gestion du catalogue',

);
?>
