<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(
	// Exec abonnements
	"abonnements" => "Abonnements",
	"abonnements_echus" => "Abonnements &eacute;chus",
	"ajouter_un_abonnement"=>"Ajouter un abonnement",
	"liste_des_abonnements" => "Liste des abonnements",
	"statistiques" => "Statistiques des abonnements",
	
	"editer_abonnement" => "Editer un abonnement",
	"creer_abonnement" => "Creer un abonnement",
	
	// actions
	"action_editer" => "&Eacute;diter",
	"action_supprimer" => "Supprimer",
	"action_ajouter" => "Ajouter",
	"action_creer_nouvel_abonnement" => "Cr&eacute;er un nouvel abonnement",

	"mabonner" => "M'abonner !",
	
	// formulaire
	"enregistrement_effectue" => "Enregistrement effectu&eacute;",
	"erreurs_formulaire" => "Des erreurs sont pr&eacute;sentes dans votre saisie.",
	
	// CFG
	"abonnement" => "Abonnement",
	"abonnement_relances" => "Relance des abonnements",
	
	"configuration_abonnements" => "Paramétrage",
	"configuration_description_abonnement" => "Cette page permet de d&eacute;finir les options d'abonnement de votre site.",
	"configuration_description_abonnement_relances" => "Cette page permet de d&eacute;finir les options de relances d'abonnement de votre site.",

	// Formulaires CFG
	"fieldset_securite" => "Securit&eacute;s",
	"fieldset_inscription" => "Relations avec Inscription 2",
	"fieldset_expediteur" => "Exp&eacute;diteur des mails de confirmation",
	"fieldset_confirmation" => "Mail de confirmation d'abonnement",
	"fieldset_echec" => "Mail d'&eacute;chec de l'abonnement",
	"fieldset_message_confirmation" => "Message affiché sur la page de retour de la banque",
	"fieldset_frais_de_port" => "Participation aux frais de port",
	"fieldset_articles_payants" => "Articles payants",
	"fieldset_relance" => "Email de relance a &eacute;ch&eacute;ance",
	"fieldset_relance_avant_1" => "Email de relance avant &eacute;ch&eacute;ance 1",
	"fieldset_relance_avant_2" => "Email de relance avant &eacute;ch&eacute;ance 2",
	"fieldset_relance_apres" => "Email de relance apr&egrave;s &eacute;ch&eacute;ance",

	"label_environnement" => "Environnement de travail",
	"label_environnement_test" => "Environnement de test",
	"label_environnement_prod" => "Environnement de production",
	"explication_environnement" => "Vous pouvez utiliser des fichiers de tests
		pour v&eacute;rifier que les abonnements fonctionnent correctement.
		Une fois en production,	ces fichiers de tests seront inaccessibles
		pour des raisons &eacute;vidente de s&eacute;curit&eacute;.",
	
	"label_action" => "Action",
	"label_libelle" => "Libell&eacute;",
	"label_duree" => "Dur&eacute;e",
	"label_periode" => "P&eacute;riode",
	"label_nom" => "Nom",
	"label_prenom" => "Pr&eacute;nom",
	"label_ville" => "Ville",
	"label_adresse" => "Adresse",
	"label_cp" => "Code postal",
	"label_date_creation" => "Date de cr&eacute;ation",
	"label_date_expiration" => "Date d'expiration",
	"label_pays" => "Pays",
	"label_email" => "Email",
	"label_sujet" => "Sujet",
	"label_texte" => "Texte",
	"label_jour" => "jours",
	"label_mois" => "mois",
	"label_commentaire" => "Commentaires",
	"label_tarif" => "Tarif",
	"label_montant" => "Montant",
	"label_frais_port_europe" => "Montant des frais de port pour l'europe",
	"label_frais_port_monde" => "Montant des frais de port pour le reste du monde",
	"explication_tarif" => "Indiquer le prix en euros.",
	"label_proposer_paiement" => "Proposer le paiment à l'inscription",
	"explication_proposer_paiement" => "Proposer un formulaire de paiement
		d&egrave;s la validation du formulaire d'inscription ?",

	// stats
	'nombre_abonnements' => "Nb.",
	"stats_montant" => "&euro;",
	'total' => "Total",
	"numeros_payants" => "Articles payants",
	"nombre_acheteurs_differents" => "acheteurs diff&eacute;rents : ",
	"diffusion_payee" => "Nb.",
	"abonnes" => "Abonn&eacute;s",
	"nombre_abonnes_a_jour" => "abonn&eacute;s &agrave; jour :",
	"nombre_abonnes_dechus" => "abonnements &eacute;chus :",
	"nombre_abonnes_relances" => "abonn&eacute;s relanc&eacute;s :",
	"nombre_abonnes_inscrits" => "abonn&eacute;s inscrits :",
	"nouveaux_abonnes_depuis_7_jours" => "Nouveaux abonn&eacute;s depuis 7 jours :",


	// mes_abonnements
	'votre_abonnement' => 'Votre abonnement',
	'echeance_le' => '&eacute;ch&eacute;ance le @date@',
	'echeance' => 'Ech&eacute;ance',
	'vos_articles' => 'Vos articles',
	'echance_abonnement_proche' => "Votre abonnement <i>@titre@</i> arrive &agrave; &eacute;ch&eacute;ance le @date@.",
	'echance_abonnement_passee' => "Votre abonnement <i>@titre@</i> est arriv&eacute; &agrave; &eacute;ch&eacute;ance le @date@.",
	'renouveler_abonnement_proche' => "Vous pouvez le renouveller d&egrave;s aujourd'hui ou bien choisir une autre formule ci-dessous.",
	'renouveler_abonnement_passe' => "Vous pouvez le renouveller ou bien choisir une autre formule ci-dessous.",

	// formulaire commander
	'info_profiter_de' => "Profitez pleinement de @titre@ en vous abonnant.",
	'bouton_commander' => "Commander",
	'choisir_son_abonnement' => "Choisir son abonnement",
	'commander_article' => "Commande de l'article :",
	
	'erreur_identification' => "Vous devez &ecirc;tre identifi&eacute; pour pouvoir vous abonner",
	'erreur_presente' => "Une erreur est pr&eacute;sente dans votre saisie",
	'erreur_selection_abonnement' => "Vous devez choisir un abonnement !",

	// page de test de commande
	"paiement" => "Paiement",
	"donnees_correspondances" => "Mes donn&eacute;es de correspondance",
	"mon_abonnement" => "Abonnement",
	"mon_article" => "Mon article",
	"simulation_paiement" => "Tester une simulation de paiement avec ces boutons",

	// page article_restreint
	"acces_refuse" => "Votre abonnement ne vous permet pas d'acc&eacute;der &agrave; cette page",
	"connexion_deja_abonne" => "Si vous &ecirc;tes d&eacute;j&agrave; abonn&eacute;, vous pouvez vous connecter &agrave; l'aide du formulaire ci-dessous",
	"abonner_ou_acheter_article" => "Vous pouvez vous abonner, ou bien acheter cet article &agrave; l'unit&eacute;",
);
?>
