<?php
  // ---------------------------------------------------------------------
  //
  // Sktheme : manage themes under SPIP (squelettes + habillages)
  //
  // Copyright (c) 2006 - Skedus
  //
  // This program is free software; you can redistribute it and/or modify
  // it under the terms of the GNU General Public License as published by
  // the Free Software Foundation; either version 2 of the License, or
  // (at your option) any later version.
  //
  // You should have received a copy of the GNU General Public License
  // along with this program; 
  //
  // ---------------------------------------------------------------------

$GLOBALS[$GLOBALS['idx_lang']] = 
    array(
	  // A
	  "available_habillage_list" => "Lista de pieles disponibles",
	  "available_squelette_list" => "Lista de esqueletos disponibles",
	  
	  // B

	  // C
	  "configuration" => "Configuraci&oacute;n avanzada",
	  "config_doc_directories" => "<b>Carpetas</b>:<br /> 
	  Es posible redefinir las carpetas de almacenaje de tus <i>'esqueletos'</i> 
	  y de tus <i>'pieles'</i>, por omisi&oacute;n est&aacute;n en la carpeta 
	  <i>'themes'</i>. Estas carpetas siempre son relativas a la ra&iacute;z detu sitio SPIP.
	  ",
	  "config_doc_switcher" => "<b>Alternador</b>:<br />
	  Le 'alternador' (o 'switcher' de temas permite hacer aparecer una lista de selecci&oacute;n
	  de temas en el espacio p&uacute;blico y as&iacute; probar los diferentes temas m&aacute;s 
	  r&aacute;pidamente. Es posible limitar esta posibilidad s&oacute;lo a los administradores.<br />
	  Adicionalmente, puedes cambiar aqu&iacute; el estilo de tiuselector de temas. 
	  <br />
	  <br />
	  También existe una baliza<br /> <b><small>#SKTHEME_HABILLAGES_SWITCHER</small></b><br />
	  que es posible integrar a los esqueletos para disponer, esta vez, de un selector que corresponda a 
      tu elecci&oacute;n de esqueletos. Igualmente, se puede modificar el estilo de este selector.
	  ",
	  // D
	  "documentation" => "Documentaci&oacute;n",
	  "extra_documentation" => "Documentaci&oacute;n extra",

	  // E

	  // F

	  // G

	  // H
	  "habillages_public_dir" => "Carpetas para las pieles p&uacute;blicas",

	  // I

	  // J

	  // K

	  // L

	  // M
	  "main_config_title" => "Configuraci&oacute;n",
	  "manage_theme" => "Gesti&oacute;n des temas",

	  // N
	  "no" => "No",
	  "no_available_habillage" => "Ninguna piel disponible",
	  "no_available_squelette" => "Aucun squelette disponible",

	  // O
	  "original" => "Original",

	  // P
	  "private_choice" => "Elecci&oacute;n tema privado",
	  "private_themes_config" => "Configuraci&oacute;n para los temas privados",
	  "private_theme_choice" => "Elecci&oacute;n del tema del espacio privado",
	  "public_choice" => "Elecci&oacute;n tema p&uacute;blico",
	  "public_theme_choice" => "Elecci&oacute;n del tema del espacio p&uacute;blico",
	  "public_themes_config" => "Configuraci&oacute;n para los temas p&uacute;blicos",
	  "public_theme_doc_squelette" => "<b>Elecci&oacute;n del esqueleto:</b><br />
	  Cada vez que seleccionar&aacute;s un esqueleto (m&aacute;s exactamente una distribuci&oacute;n de 
	  esqueletos) la lista de ropas disponibles se actualiza. El cuadradito indica el estado de la
	  distribuci&oacute;n de esqueletos: estable, test, experimental ...<br />
	  Para mayores detalles acerca de una distribuci&oacute;n pincha en la flecha negra.
	  ",
	  "public_theme_doc_habillage" => "<b>Elecci&oacute;n de la piel:</b><br />
	  Las 'pieles' permiten modificar los colores y el aspecto gr&aacute;fico de una distribuci&oacute;n 
	  de esqueletos. Se trata de CSS y/o de im&aacute;genes, ect. <br /> 
	  Una distribuci&oacute;n de esqueletos siempre contiene una presentaci&oacute;n gr&aacute;fica
      por omisi&oacute;n, que corresponde a la piel denominada <b>Original</b>.
	  ",
	  // Q

	  // R

	  // S
	  "save_habillage" => "Guardar la piel",
	  "save_public_directories" => "Guardar las carpetas ",
	  "save_squelette" => "Guardar es esqueleto",
	  "save_switcher_options" => "Guardar las opciones del alternador",
	  "squelettes_public_dir" => "Carpetas para los esqueletos p&uacute;blicos",
	  "switcher_activated" => "Activar el alternador de temas",
	  "switcher_admin_only" => "S&oacute;lo para los administratdores",
	  "switcher_style" => "Style du switcher",

	  // T
	  "to_be_done" => "&iexcl;a&uacute;n por hacer!",
	  "themes_switcher" => "Alternador de temas",

	  // U
	  "update_habillage_to" => "Actualizaci&oacute;n de la piel por: ",
	  "update_squelette_to" => "Actualizaci&oacute;n del esqueleto por: ",

	  // V

	  // W

	  // X

	  // Y
	  "yes" => "S&iacute;",

	  // Z

);

?>
