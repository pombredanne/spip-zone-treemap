<?php
  // ---------------------------------------------------------------------
  //
  // Sktheme : manage themes under SPIP (squelettes + habillages)
  //
  // Copyright (c) 2006 - Skedus
  //
  // This program is free software; you can redistribute it and/or modify
  // it under the terms of the GNU General Public License as published by
  // the Free Software Foundation; either version 2 of the License, or
  // (at your option) any later version.
  //
  // You should have received a copy of the GNU General Public License
  // along with this program; 
  //
  // ---------------------------------------------------------------------

$GLOBALS[$GLOBALS['idx_lang']] = 
    array(
	  // A
	  "available_habillage_list" => "Available habillages",
	  "available_squelette_list" => "Available squelettes",
	  
	  // B

	  // C
	  "configuration" => "Advanced configuration",
	  "config_doc_directories" => "<b>R&eacute;pertoires</b>:<br>
	  It is possible to re-defined default directories locations of yours
	  <i>squelettes/habillages</i> distributions.Default is  the <i>'themes'</i>
	   directory, directories are always relatives to
	  your SPIP root site.
	  ",
	  "config_doc_switcher" => "<b>Switcher</b>:<br>
	  The theme switcher can be activated to include a selection list in the
	  public area, and then easily test themes. It is possible to limit this
	  functionnality to the administrator only.<br>
	  Moreover you can modify the style of this theme selector.<br>
	  <br>
	  A new 'balise' <b>#SKTHEME_HABILLAGES_SWITCHER</b> is available to
	  include an 'habillage' selector list in your squelette. This list is
	  composed of all available 'habillage' corresponding to the squelette
	  distribution you have choosen. You can as well modify this
	  list selector style here.
	  
	  
	  ",
	  
	  // D
	  "documentation" => "Documentation",
	  "extra_documentation" => "Extra Documentation",

	  // E

	  // F

	  // G

	  // H
	  "habillages_public_dir" => "Public habillages directory",

	  // I

	  // J

	  // K

	  // L

	  // M
	  "main_config_title" => "Configuration",
	  "manage_theme" => "Manage themes",

	  // N
	  "no" => "No",
	  "no_available_habillage" => "No available habillage",
	  "no_available_squelette" => "No available squelette",

	  // O
	  "original" => "Original",

	  // P
	  "private_choice" => "Private theme choice",
	  "private_themes_config" => "Private themes configuration",
	  "private_theme_choice" => "Private theme choice",
	  "public_choice" => "Public theme choice",
	  "public_theme_choice" => "Public theme choice",
	  "public_themes_config" => "Public themes configuration",
	  "public_theme_doc_squelette" => "<b>Squelette choice:</b><br>
	  Each time you will choose a 'squelette' (more exactly a squelettes distribution)
	  the 'habillage' list will be updated with the available ones.
	  The small square represent the state of the distribution, stable,
	  test, experimental, ...<br>
	  Clic on the small black triangle to get more details about the distribution.
	  ",
	  "public_theme_doc_habillage" => "<b>Habillage choice:</b><br>
	  An Habillage is modifing colors and graphical aspect of your squelettes.
	  It is composed of CSS, pictures, ...<br>
	  A squelette distribution always contain a default 'habillage' which is
	  called <b>Original</b>.  
	  ",
	  
	  // Q

	  // R

	  // S
	  "save_habillage" => "Save habillage",
	  "save_public_directories" => "Save directories",
	  "save_squelette" => "Save squelette",
	  "save_switcher_options" => "Save switcher options",
	  "squelettes_public_dir" => "Public squelettes directory",
	  "switcher_activated" => "Theme switcher activated",
	  "switcher_admin_only" => "Only for administrators",
	  "switcher_style" => "Switcher style",

	  // T
	  "to_be_done" => "To be done !",
	  "themes_switcher" => "Theme switch",

	  // U
	  "update_habillage_to" => "Update habillage to : ",
	  "update_squelette_to" => "Update squelette to : ",

	  // V

	  // W

	  // X

	  // Y
	  "yes" => "Yes",

	  // Z

);

?>