<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-activite_editoriale
// Langue: fr
// Date: 21-05-2012 16:04:36
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
	'activite_editoriale_description' => 'Ce plugin a pour objectif de compléter les fonctionnalités de suivi de l\'activité éditoriale de SPIP.

Sont déjà proposés :
-* l\'ajout de deux champs aux rubriques pour indiquer un délai en jour au delà duquel elle doivent être alimentées, ainsi que les personnes à prévenir
-* des alertes par e-mail selon ces deux paramètres
-* un tableau de bord présentant les mêmes informations',
	'activite_editoriale_slogan' => 'Un site, c\'est bien ! Un site à jour, c\'est mieux !',
);
?>