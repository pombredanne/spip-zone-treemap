<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_choix' => 'Ajouter ce choix',

	// I
	'icone_creer_choix' => 'Créer un choix',
	'icone_modifier_choix' => 'Modifier ce choix',
	'info_1_choix' => 'Un choix',
	'info_aucun_choix' => 'Aucun choix',
	'info_choix_auteur' => 'Les choix de cet auteur',
	'info_nb_choix' => '@nb@ choix',

	// L
	'label_Ordre' => 'Ordre',
	'label_id_sondage' => 'Sondage',
	'label_titre' => 'Titre',

	// R
	'retirer_lien_choix' => 'Retirer ce choix',
	'retirer_tous_liens_choix' => 'Retirer tous les choix',

	// T
	'texte_ajouter_choix' => 'Ajouter un choix',
	'texte_changer_statut_choix' => 'Ce choix est :',
	'texte_creer_associer_choix' => 'Créer et associer un choix',
	'titre_choix' => 'Choix',
	'titre_choix_rubrique' => 'Choix de la rubrique',
	'titre_langue_choix' => 'Langue de ce choix',
	'titre_logo_choix' => 'Logo de ce choix',
);

?>