<?php


	/**
	 * SPIP-Sondages
	 *
	 * Copyright (c) 2006-2009
	 * Agence Art&eacute;go http://www.artego.fr
	 *  
	 * Ce programme est un logiciel libre distribue sous licence GNU/GPLv3.
	 * Pour plus de details voir http://www.gnu.org/licenses/gpl-3.0.html
	 *  
	 **/


	$GLOBALS[$GLOBALS['idx_lang']] = array(

		'vous_avez_deja_vote' => 'Vous avez d&eacute;j&agrave; vot&eacute;.',
		'voter' => 'Voter',
		'valider' => 'Valider',
		'faites_un_choix' => "Faites un choix",
		'merci_pour_votre_avis' => 'Merci pour votre avis.',
		'votes' => 'votes',


		'Z' => 'ZZzZZzzz'

	);

?>