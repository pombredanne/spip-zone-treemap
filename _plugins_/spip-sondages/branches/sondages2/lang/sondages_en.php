<?php


	/**
	 * SPIP-Sondages
	 *
	 * Copyright (c) 2006-2009
	 * Agence Artégo http://www.artego.fr
	 *  
	 * Ce programme est un logiciel libre distribue sous licence GNU/GPLv3.
	 * Pour plus de details voir http://www.gnu.org/licenses/gpl-3.0.html
	 *  
	 **/


	$GLOBALS[$GLOBALS['idx_lang']] = array(

		'vous_avez_deja_vote' => "You've already voted.",
		'voter' => 'Vote',
		'valider' => 'Confirm',
		'faites_un_choix' => "Please make a choice.",
		'merci_pour_votre_avis' => 'Thank you for voting.',
		'votes' => 'votes',


		'Z' => 'ZZzZZzzz'

	);

?>