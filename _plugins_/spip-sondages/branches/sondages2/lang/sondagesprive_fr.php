<?php


	/**
	 * SPIP-Sondages
	 *
	 * Copyright (c) 2006-2009
	 * Agence Art&eacute;go http://www.artego.fr
	 *  
	 * Ce programme est un logiciel libre distribue sous licence GNU/GPLv3.
	 * Pour plus de details voir http://www.gnu.org/licenses/gpl-3.0.html
	 *  
	 **/


	$GLOBALS[$GLOBALS['idx_lang']] = array(

		'editer_choix' => "Editer un choix",
		'titre' => "Titre",
		'position' => "Position",
		'en_premier' => "en premier",
		'apres' => "apr&egrave;s",
		'enregistrer' => "Enregistrer",
		'choix' => "Choix",
		'sondage_numero' => "SONDAGE NUMERO",
		'ce_sondage' => "ce sondage est",
		'en_cours_de_redaction' => "en cours de r&eacute;daction",
		'a_publier' => "&agrave; publier",
		'a_supprimer' => "&agrave; supprimer",
		'publie' => "publi&eacute;",
		'a_purger' => "&agrave; purger",
		'a_terminer' => "&agrave; terminer",
		'termine' => "termin&eacute;",
		'voir_en_ligne' => "Voir en ligne",
		'creer_nouveau_sondage' => "Cr&eacute;er un nouveau sondage",
		'aller_liste_sondages' => "Aller &agrave; la liste des sondages",
		'ajouter_choix' => "Ajouter un choix",
		'modifier_sondage' => "Modifier le sondage",
		'texte' => "Texte",
		'sondages' => "Sondages",
		'sondages_en_cours_de_redaction' => "Sondages en cours de r&eacute;daction",
		'sondages_publies' => "Sondages publi&eacute;s",
		'sondages_termines' => "Sondages termin&eacute;s",
		'tous_sondages_rubrique' => "Tous les sondages de cette rubrique",
		'item_mots_cles_association_sondages' => "aux sondages",
		'logo_sondage' => "LOGO DU SONDAGE",

		'Z' => 'ZZzZZzzz'

	);

?>