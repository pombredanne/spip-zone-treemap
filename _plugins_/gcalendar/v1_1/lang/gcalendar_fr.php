<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'adresse_mail' => "Adresse mail associée à l'agenda : ",
	'affichage_agenda' => "Affichage de l'adresse de l'agenda",

	// C
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',
	'chargement_agenda' => "Chargement de l'agenda...",
	
	// G
	'gcalendar_titre' => 'Gcalendar',

	// T
	'titre_page_configurer_gcalendar' => 'Configuration de l\'Agenda',
);

?>