<?php
// This is a du menu babbibel language file  --  Ceci est un fichier langue du menu babbibel

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A

'aleatoire' => 'Al&eacute;atoire <i>(pour rendre fous les visiteurs du site)</i>',
'article' => 'article(s)', 

// B

'babbi_description' => 'Ce plugin propose un menu d&#233;roulant horizontal s&#233;appuyant sur la librairie jQuery, configurable et sans limitation de profondeur. Le code &#233;tant tr&#232;s simple, le menu est facile &#224; personnaliser.',
'babbi_slogan' => 'Menu d&#233;roulant horizontal en jQuery',
// C


'cle_article' => 'Rubriques par mot cl&eacute; >>> Articles', 
'cle_duo' => 'Rubriques par mot cl&eacute; >>> Sous rubriques + articles',
'cle_sousrub' => 'Rubriques par mot cl&eacute; >>> Sous rubriques',
'cocher_accueil' => 'Cochez pour <b>supprimer</b> l&rsquo;item &quot;<b>Accueil</b>&quot;',

// D


// E

'explication' => 'Ici vous pouvez choisir le type de menu &agrave; afficher, l\'ordre des items, et le nombre d\'articles des menus',
'explication_inverse' => 'Cochez pour inverser le classement des articles',
'explication_nbarticles' => 'Saisissez le nombre d&acute;articles maximum &agrave; afficher (Laisser vide pour tout afficher)',


// F



// I

'inverse' => 'Inverser le tri ?',

// L
'label_accueil' => 'Item &quot;Accueil&quot;',
'label_classement' => 'Classement des articles affich&eacute;s',
'label_explication' => 'Liens documentations et pages de test',
'label_type' => 'Type de menu',
'lien_doc' => 'Cf. Documentation sur spip-contrib',

// M 
'message_erreur' => 'Votre saisie n\'a pas &eacute;t&eacute; enregistr&eacute;e car elle contient des erreurs !',

// N

'nb_article' => 'Nombre maximum d&acute;articles  &agrave; afficher',


// O



//P

'par_alpha' => 'Par ordre alphab&eacute;tique',
'par_date' => 'Par date',
'par_maj' => 'Par date de mise &agrave; jour',
'par_popu' => 'Par popularit&eacute;',

//Q

'que_des_nombres_ici' => 'Saisir un nombre sup&eacute;rieur ou &eacute;gal &agrave; z&eacute;ro',
'question_nbarticles' => 'Nombre maximum d\'articles &agrave; d&eacute;rouler',

//R



// S
'selection_menu' => 'S&eacute;lectionnez le type de menu d&eacute;roulant',
'secteur_sousrub' => 'Secteurs >>> Sous-rubriques',
'secteur_article' => 'Secteurs >>> Articles',
'secteur_duo' => 'Secteurs >>> Sous-rubriques + articles',



//T

'titre_menu' => 'Configuration du menu babbibel',
'titre_menu_babbibel' => 'Menu babbibel',
'test_multi' => 'Testez le menu multilingue par secteur',
'test_norm' => 'Testez le menu',


// Z

);

?>
