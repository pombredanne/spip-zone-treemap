<?php
// This is a du menu babbibel language file  --  Ceci est un fichier langue du menu babbibel

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A

'aleatoire' => 'Random (<i>to make crazy your visitors</i>)',
'article' => 'article(s)', 

// B


// C


'cle_article' => 'Section by keyword >>> Articles', 
'cle_sousrub' => 'Section by keyword >>> under sections',
'cle_duo' => 'Section by keyword >>> under sections + articles',
'cocher_accueil' => 'Check to delete &quot;<b>Home</b>&quot; item',

// D


// E

'explication' => 'Here you can choose the type of menu to be displayed, the order of items, and the number of menu items',
'explication_inverse' => 'Check to reverse the classification of articles',
'explication_nbarticles' => 'Enter the maximum number of items to display (Leave blank to view all)',


// F



// I

'inverse' => 'Reverse the sort ?',

// L
'label_accueil' => 'Display &quot;<b>Home</b>&quot;',
'label_classement' => 'Classification of items displayed',
'label_explication' => 'Documentation and test pages links',
'label_type' => 'Type of menu',
'lien_doc' => 'See documentation on spip-contrib.net',

// M 
'message_erreur' => 'Your input has not been registered because it contains errors !',

// N

'nb_article' => 'Maximum number of items to display',


// O



//P

'par_alpha' => 'Alphabetically',
'par_date' => 'By date',
'par_maj' => 'By date of update',
'par_popu' => 'By popularity',

//Q

'que_des_nombres_ici' => 'Enter a number equal or greater than zero',
'question_nbarticles' => 'Most items in place',

//R



// S
'selection_menu' => 'Select the type of menu',
'secteur_sousrub' => 'Sections >>> Under sections',
'secteur_article' => 'Sections >>> Articles',
'secteur_duo' => 'Sections >>> Under sections + articles',



//T

'titre_menu' => 'Configuration menu babbibel',
'titre_menu_babbibel' => 'Menu babbibel',
'test_multi' => 'Test the multilingual menu by sector',
'test_norm' => 'Test the menu',


// Z

);

?>
