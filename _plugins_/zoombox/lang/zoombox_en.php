<?php
$GLOBALS['i18n_zoombox_en'] = array(
	"titre" => "Zoombox",
	"descr_cfg" => "<p>Configuration of <strong>Zoombox</strong> plugin</p><br/><a href='http://www.spip-contrib.net/Zoombox-pour-SPIP' target='_blank'>Documentation of plugin</a>",
	"general_legend" => "Configuration &nbsp;",
	"selecteur" => "Selectors (CSS pointers):",
	"theme" => "Theme : ",
	"theme_zoombox" => "Zoombox",
	"theme_lightbox" => "Lightbox",
	"theme_prettyphoto" => "Prettyphoto",
	"theme_darkprettyphoto" => "DarkPrettyphoto",
	"theme_simple" => "Simple",
	"opacity" => "Opacity (between 0 and 1) : ",
	"duration" => "Zoombox opening animation duration (ms) : ",
	"animation" => "Animation ? : ",
	"oui" => "yes",
	"non" => "no",
	"largeur_video" => "Maximal width (for vid&eacute;os) : ",
	"hauteur_video" => "Maximal height (for vid&eacute;os) : ",
	"gallery" => "Gallery mode ? : ",
	"autoplay" => "Autoplay ? : ",
	"overflow" => "Allow content to overflow the screen ? : ",
	"enregistrer" => "Save",
	"effacer" => "Clear"
);
?>