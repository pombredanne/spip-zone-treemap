<?php
$GLOBALS['i18n_zoombox_fr'] = array(
	"titre" => "Zoombox",
	"descr_cfg" => "<p>Configuration du plugin <strong>Zoombox</strong></p><br/><a href='http://www.spip-contrib.net/Zoombox-pour-SPIP' target='_blank'>Documentation du plugin</a>",
	"general_legend" => "Configuration &nbsp;",
	"selecteur" => "S&eacute;lecteurs (pointeurs CSS) : ",
	"theme" => "Th&eacute;me : ",
	"theme_zoombox" => "Zoombox",
	"theme_lightbox" => "Lightbox",
	"theme_prettyphoto" => "Prettyphoto",
	"theme_darkprettyphoto" => "DarkPrettyphoto",
	"theme_simple" => "Simple",
	"opacity" => "Opacit&eacute; (entre 0 et 1) : ",
	"duration" => "Dur&eacute;e de l'animation d'ouverture (en ms) : ",
	"animation" => "Animation ? : ",
	"oui" => "oui",
	"non" => "non",
	"largeur_video" => "Largeur maximal (pour les vid&eacute;os) : ",
	"hauteur_video" => "Hauteur maximal (pour les vid&eacute;os) : ",
	"gallery" => "Mode gallerie ? : ",
	"autoplay" => "Lecture automatique ? : ",
	"overflow" => "Permettre les images plus larges que l'&eacute;cran ? : ",
	"enregistrer" => "Enregistrer",
	"effacer" => "Effacer"
);
?>