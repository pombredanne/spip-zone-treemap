<?php
/**
 * Fichier de langue : Anglais
 *
 * @author cy_altern
 * @license GNU/LGPL
 *
 * @package plugins
 * @subpackage odt2spip
 * @category import
 *
 * @version $Id$
 *
 */

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'attacher_fichier_odt' => 'Attach the ODT file to the article ?',
  
	// C
	'choix_fichier' => 'ODT file to use :',

	// E
	'err_enregistrement_fichier_sortie' => 'Error saving file snippet',
	'err_extension_xslt'=> 'PHP\'s XSLT functions are not operational in: ask activation XSL extensions to your server administrator',
	'err_import_snippet' => 'Error when creating the article with the plugin Snippets. Make sure it is properly installed and activated.',
	'err_repertoire_tmp' => 'Error: tmp/odt2spip folder or its subfolder / id_auteur has not been created',
	'err_telechargement_fichier' => 'Error: the file could not be recovered',
	'err_transformation_xslt' => 'Error processing XSLT file ODT',

	// I
	'importer_fichier' => 'Create an article from an OOo Writer file',
		
	// N
	'non' => 'no',
		
	// O
	'oui' => 'yes',
	
	// T
	'cet_article_version_odt' => 'OOo Writer version of this article' 

);

?>
