<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'cfg_explication_latitude' => "S&eacute;lectionnez la latitude par d&eacute;faut",
	'cfg_explication_longitude' => "S&eacute;lectionnez la longitude par d&eacute;faut",	
	'cfg_explication_maptype' => "Choisissez un type de carte",
	'cfg_explication_nomcarte' => "ID (au sens HTML) de la carte par d&eacute;faut",
	'cfg_explication_zoom' => "S&eacute;lectionnez le niveau de zoom par d&eacute;faut",
	'cfg_label_latitude' => "Latitude",
	'cfg_label_longitude' => "Longitude",
	'cfg_label_maptype' => "Type de carto",
	'cfg_label_nomcarte' => "Nom (id) de la carte",
	'cfg_label_zoom' => "Zoom",
	
	'confirmation_placement' => 'Enregistrer la position pour cet article',
	
	'label_adresse' => "Adresse : ",
	'label_latitude' => "Latitude : ",
	'label_longitude' => "Longitude : ",
	
	'message_confirmer_placement' => 'Vous pouvez &agrave; pr&eacute;sent corriger manuellement le placement ou le confirmer.',
	
	'or' => "OU",
	
	'titre_admin' => 'Google Maps API v3'
);
