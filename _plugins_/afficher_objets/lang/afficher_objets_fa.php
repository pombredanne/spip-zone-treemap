<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/afficher_objets?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'info_statut' => 'وضعيت',

	// N
	'nb_articles' => '@nb@ مقاله',

	// R
	'rang' => 'رتبه'
);

?>
