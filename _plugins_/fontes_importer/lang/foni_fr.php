<?php

// $LastChangedRevision$
// $LastChangedBy$
// $LastChangedDate$

$GLOBALS['i18n_' . _FONI_PREFIX . '_fr'] = array(

	'foni' => 'Fontes Importer'

	, 'fontes_dispo_sel' => 'Fontes diponibles/s&#233;lection'
	, 'selection_fonte' => 'S&#233;lection d&#39;une fonte compl&#233;mentaire'
	, 'fontes_dispo_sel_legend' => 'Cochez la case de la fonte que vous d&#233;sirez
		ajouter aux pages de votre site. Dans le champ de droite, le nom de la famille
		peut-&#234;tre modifi&#233;. Ce nom de famille de fonte est &#224; utiliser dans
		vos d&#233;clarations CSS.
		<br />Par exemple : <br />
		<code>body {<br />
		&nbsp;font-family: Dustismo, sans-serif<br />
		}</code>'
	, 'visualisation_fonte' => 'Visualisation de la fonte'
	, 'visualisation_fonte_txt' => 'Cliquez sur le nom d&#39;une fonte dans la liste pour voir
		appara&#238;tre un &#233;chantillon ici.'
	, 'include_font' => 'Incorporer la fonte'
	, 'methode' => 'M&#233;thode'
	, 'methode_legend' => 'La police s&#233;lectionn&#233;e peut-&#234;tre appel&#233;e par la page HTML
		via l&#39;utilisation classique de l&#39;attribut src (url) de @font-face, 
		ou incorpor&#233;e directement dans le code de chaque page.
		Dans le second cas, vous y gagnerez en nombre de hits,
		mais la page sera plus lourde &#224; charger.'
);
