<?php
/**
 * Plugin Thème privé
 * Arnaud Bérard - Mist. GraphX
 * Licence GNU/GPL
 * pour forcer au besoin sur un jeu particulier d'icones
 * les jeux supplémentaire sont a placer dans /prive/theme du plugin
 * cf : http://www.spip-contrib.net/Doc-SPIP3-theme-prive
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS['mes_preferences_defaut'] = 'spip';

?>