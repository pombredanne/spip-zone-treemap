<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
        // E
        'explication_spip_dist'=>'Le th&egrave;me par d&eacute;faut livr&eacute; avec Spip 3',
        'explication_vector_icons'=>'Des icones simples gérant le changement de couleur de l\'espace privé et [provenant de DesignerFolio ->http://www.dezinerfolio.com/freebie/30-free-vector-icons]',
        'explication_spip2'=>'Les icones du bandeau utilisé dans Spip 2',
        'explication_vector_icons_2'=>'Le même jeux mais moins austère',
        // I
        'info_elastic'=>'Elastic (largeur adaptable a l&apos;&eacute;cran)',
        // L
        'label_theme'=>'Choix du th&egrave;me de l&apos;espace priv&eacute;',
        'lien_theme_spip_dist'=> 'Theme Priv&eacute; spip3',
        'lien_theme_spip_2'=>'Th&egrave;me spip 2',
        'lien_theme_vector_icons' => 'Vector icons',
        'lien_theme_vector_icons_2'=>'Vector icons 2',
	// T
	'mes_preferences_titre' => 'Switcher thèmes privé',
);

?>