<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'mes_preferences_nom' => 'Mes pr&eacute;f&eacute;rences',
	'mes_preferences_description' => 'Ce plugin modifie les syt&egrave;me des prf&eacute;rences utilisateur de spip pour lui ajouter des fonctionalit&eacute;s ou options suppl&eacute;mentaires',
	'mes_preferences_slogan' => 'Ajouter des pr&eacute;f&eacute;rences utilisateur'
);

?>