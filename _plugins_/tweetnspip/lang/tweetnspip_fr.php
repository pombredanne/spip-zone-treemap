<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

  // main form
	'cfg_boite_tweetnspip' => 'Configuration du plugin Tweet\'N\'SPIP',
	'cfg_descr_tweetnspip' => 'Tweet\'N\'SPIP : Plugin jQuery Twitter pour SPIP',
	'cfg_titre_tweetnspip' => 'Tweet\'N\'SPIP',
	
	// parameters
	'cfg_inf_username' => 'Nom du user twitter &agrave; suivre.',
	'cfg_lbl_username' => 'Compte Twitter',
	
	'cfg_inf_numtweets' => 'Nombre de tweets &agrave; afficher.',
	'cfg_lbl_numtweets' => 'Nombre de tweets',
	
	'cfg_inf_loadertext' => 'Texte affich&eacute; pendant le chargement des tweets.',
	'cfg_lbl_loadertext' => 'Message de chargement',
	'cfg_def_loadertext' => 'Chargement des tweets...',
	
	'cfg_inf_slidein' => 'Activer ou non l&rsquo;effet de glissement vertical &agrave; la fin du charmement des tweets.',
	'cfg_lbl_slidein' => 'Effet d&rsquo;apparition',
	
	'cfg_inf_showheading' => 'Afficher ou pas l&rsquo;ent&ecirc;te.',
	'cfg_lbl_showheading' => 'Afficher l&rsquo;ent&ecirc;te',

	'cfg_inf_headingtext' => 'Texte de l&rsquo;ent&ecirc;te.',
	'cfg_lbl_headingtext' => 'Texte de l&rsquo;ent&ecirc;te',
	'cfg_def_headingtext' => 'Derniers Tweets',

	'cfg_inf_showprofilelink' => 'Affiche ou nom le lien vers le profile twitter.',
	'cfg_lbl_showprofilelink' => 'Afficher le lien vers le profil',
	
);
?>
