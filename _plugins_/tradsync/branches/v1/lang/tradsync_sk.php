<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/tradsync?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_mots_differents' => 'Články',
	'articles_non_traduits' => 'Články',
	'articles_originaux_modifies' => 'Články',

	// C
	'comparer_arborescence' => 'Porovnať stromovú štruktúru',
	'configurer_tradsync' => 'Pomoc k prekladu',
	'configurer_tradsync_description' => 'Nastaviť rôzne prvky na spresnenie poskytovanej pomoci',

	// E
	'editer_ce_mot' => 'Upraviť toto kľúčové slovo',
	'editer_cet_article' => 'Upraviť tento článok',
	'editer_cette_rubrique' => 'Upraviť túto rubriku',
	'espace_traducteurs' => 'Prekladateľské rozhranie',
	'explication_comparer_arborescence' => '		Dva prehliadače rubrík jeden vedľa druhého. Umožňuje vizuálne
		porovnať hierarchiu dvoch vetiev.
	',
	'explication_espace_traducteurs' => '		Prekladateľské rozhranie slúži na pomoc s vykonávaním prekladov, ich schvaľovaním a vyhľadávaním.
	',
	'explication_groupe_mots_multi' => '
		Choisir les groupes de mots qui reçoivent des mots traduits en utilisant
		des balises &lt;multi&gt; sur les titres. De la sorte, il sera possible de
		verifier qu\'une traduction est réalisée pour les mots de ces groupes.
	', # NEW
	'explication_langue' => 'Vyberte jazyk, pri ktorom sa musí overiť
		stav prekladov.',
	'explication_traductions_a_realiser' => '		V tomto rozhraní sa nachádza zoznam článkov, rubrík a kľúčových slov, ktoré sa nezdajú byť preložené.',
	'explication_traductions_a_verifier' => '
		Cet espace liste l\'ensemble des articles et rubriques dont
		l\'article d\'origine a été modifié
		ultérieurement à la traduction.
	', # NEW
	'explication_traductions_de_mots' => '		Urobiť zoznam kľúčových slov, ktoré sa nezdajú byť preložené.
	',
	'explication_traductions_mots_differents' => '
		Lister les objets traduits qui n\'ont pas exactement
		les mêmes mots clés attribués par rapport à l\'objet d\'origine.
	', # NEW
	'explication_traductions_mots_synchroniser' => '
		L\'action «synchroniser» copie l\'ensemble des mots de l\'élément
		d\'origine dans l\'élément traduit. Cependant, il ne supprime pas les
		mots-clés qui seraient présents en plus dans la traduction.
	', # NEW

	// I
	'info_actions' => 'Akcie',
	'info_revisions' => 'Opravy',
	'info_source' => 'Zdroj',
	'info_statut' => 'Stav',
	'info_statut_court' => 'S',
	'info_titre' => 'Názov',
	'info_type' => 'Skupina',

	// L
	'label_groupe_mots_multi' => 'Groupes de mots multis', # NEW
	'label_langue' => 'Cieľový jazyk',
	'langue' => 'Jazyk',
	'langue_court' => 'J',
	'langue_en_cours' => 'Vybraný jazyk je teraz: <strong>@langue@.</strong>',
	'liste_des_lieux' => 'Miesta na prehľadávanie',

	// M
	'mots_cles_differents' => 'Kľúčové slová odlišné od originálu',
	'mots_cles_non_traduits' => 'Kľúčové slová',
	'mots_non_traduits' => 'Nepreložené kľúčové slová',

	// N
	'navigateurs' => 'Prehliadače',
	'non_traduit' => 'Nepreložené alebo nedeklarované preklady',

	// O
	'origine_modifiee' => 'Originál bol zmenený',

	// R
	'rubriques_mots_differents' => 'Rubriky',
	'rubriques_non_traduites' => 'Rubriky',
	'rubriques_originales_modifiees' => 'Rubriky',

	// S
	'second_navigateur' => 'Druhý prehliadač',
	'selection_de_la_langue_cible' => 'Výber cieľového jazyka',
	'synchroniser' => 'Synchronizovať',
	'synchroniser_tout' => 'Synchronizovať všetky',

	// T
	'traductions_a_realiser' => 'Preklady, ktoré treba vykonať',
	'traductions_a_verifier' => 'Preklady, ktoré treba potvrdiť',
	'traductions_de_mots' => 'Preklady kľúčových slov',
	'traductions_mots_differents' => 'Synchronizovať kľúčové slová',

	// V
	'voir_article_source' => 'Zobraziť zdrojový článok',
	'voir_les_revisions_de_la_source' => 'Zobraziť opravy zdroja',
	'voir_rubrique_source' => 'Zobraziť zdrojovú rubriku',

	// Z
	'zbug_critere_necessite_parametre' => 'Kritérium {@critere@} si vyžaduje parameter'
);

?>
