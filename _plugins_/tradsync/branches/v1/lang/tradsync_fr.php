<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/tradsync/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_mots_differents' => 'Articles',
	'articles_non_traduits' => 'Articles',
	'articles_originaux_modifies' => 'Articles',

	// C
	'comparer_arborescence' => 'Comparer l\'arborescence',
	'configurer_tradsync' => 'Aide à la traduction',
	'configurer_tradsync_description' => 'Configurer les différentes éléments
		pour affiner l\'aide donnée.',

	// E
	'editer_ce_mot' => 'Éditer ce mot-clé',
	'editer_cet_article' => 'Éditer cet article',
	'editer_cette_rubrique' => 'Éditer cette rubrique',
	'espace_traducteurs' => 'Espace des traducteurs',
	'explication_comparer_arborescence' => '
		Deux navigateurs de rubriques l\'un à coté de l\'autre. Cela permet de comparer
		visuellement la hierarchie de deux branches.
	',
	'explication_espace_traducteurs' => '
		L\'espace des traducteurs a pour but de faciliter la tâche de recherche
		de traductions à réaliser ou à vérifier.
	',
	'explication_groupe_mots_multi' => '
		Choisir les groupes de mots qui reçoivent des mots traduits en utilisant
		des balises &lt;multi&gt; sur les titres. De la sorte, il sera possible de
		verifier qu\'une traduction est réalisée pour les mots de ces groupes.
	',
	'explication_langue' => 'Choisir la langue dont il faut verifier
		l\'état des traductions.',
	'explication_traductions_a_realiser' => '
		Cet espace liste l\'ensemble des articles, rubriques et mots
		qui ne semblent pas avoir été traduits.
	',
	'explication_traductions_a_verifier' => '
		Cet espace liste l\'ensemble des articles et rubriques dont
		l\'article d\'origine a été modifié
		ultérieurement à la traduction.
	',
	'explication_traductions_de_mots' => '
		Lister les mots qui ne semblent pas être traduits.
	',
	'explication_traductions_mots_differents' => '
		Lister les objets traduits qui n\'ont pas exactement
		les mêmes mots clés attribués par rapport à l\'objet d\'origine.
	',
	'explication_traductions_mots_synchroniser' => '
		L\'action «synchroniser» copie l\'ensemble des mots de l\'élément
		d\'origine dans l\'élément traduit. Cependant, il ne supprime pas les
		mots-clés qui seraient présents en plus dans la traduction.
	',

	// I
	'info_actions' => 'Actions',
	'info_revisions' => 'Révisions',
	'info_source' => 'Source',
	'info_statut' => 'Statut',
	'info_statut_court' => 'S',
	'info_titre' => 'Titre',
	'info_type' => 'Groupe',

	// L
	'label_groupe_mots_multi' => 'Groupes de mots multis',
	'label_langue' => 'Langue cible',
	'langue' => 'Langue',
	'langue_court' => 'L',
	'langue_en_cours' => 'La langue sélectionnée actuellement est : <strong>@langue@</strong>.',
	'liste_des_lieux' => 'Lieux à explorer',

	// M
	'mots_cles_differents' => 'Mots-clés différents de l\'original',
	'mots_cles_non_traduits' => 'Mots-clés',
	'mots_non_traduits' => 'Mots-clés non traduits',

	// N
	'navigateurs' => 'Navigateurs',
	'non_traduit' => 'Non traduits ou traductions non déclarées',

	// O
	'origine_modifiee' => 'Original modifié',

	// R
	'rubriques_mots_differents' => 'Rubriques',
	'rubriques_non_traduites' => 'Rubriques',
	'rubriques_originales_modifiees' => 'Rubriques',

	// S
	'second_navigateur' => 'Second navigateur',
	'selection_de_la_langue_cible' => 'Sélection de la langue cible',
	'synchroniser' => 'Synchroniser',
	'synchroniser_tout' => 'Synchroniser tout',

	// T
	'traductions_a_realiser' => 'Traductions à réaliser',
	'traductions_a_verifier' => 'Traductions à vérifier',
	'traductions_de_mots' => 'Traductions de mots-clés',
	'traductions_mots_differents' => 'Synchroniser les mots clés',

	// V
	'voir_article_source' => 'Voir l\'article source',
	'voir_les_revisions_de_la_source' => 'Voir les révisions de la source',
	'voir_rubrique_source' => 'Voir la rubrique source',

	// Z
	'zbug_critere_necessite_parametre' => 'Le critere {@critere@} necessite un parametre'
);

?>
