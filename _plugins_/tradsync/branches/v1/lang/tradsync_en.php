<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/tradsync?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_mots_differents' => 'Articles',
	'articles_non_traduits' => 'Articles',
	'articles_originaux_modifies' => 'Articles',

	// C
	'comparer_arborescence' => 'Compare hierarchies',
	'configurer_tradsync' => 'Help with translations',
	'configurer_tradsync_description' => 'Configure the various elements
		to fine tune the assistanec provided.',

	// E
	'editer_ce_mot' => 'Edit this keyword',
	'editer_cet_article' => 'Edit this article',
	'editer_cette_rubrique' => 'Edit this section',
	'espace_traducteurs' => 'Translation zone',
	'explication_comparer_arborescence' => '
		Two sector navigators, one beside the other. These are used to visually
		compare the hierarchies of two separate branches.
	',
	'explication_espace_traducteurs' => '
		The translation zone aims to facilitate the task of seeking out
		translations that need to be made or updated.
	',
	'explication_groupe_mots_multi' => '
		Select the keywords group that will be used to collect translated words by using
		the &lt;multi&gt; tags in titles. In this way, it will be possible to verify that
		a translation is made for keywords in that group.
	',
	'explication_langue' => 'Select the language to be used for checking
		the status of translations.',
	'explication_traductions_a_realiser' => '
		This area lists all of the articles, sections and keywords
		that do not appear to have been translated yet.
	',
	'explication_traductions_a_verifier' => '
		This area lists all of the articles and sections including
		the original article to be modified after the 
		translation.
	',
	'explication_traductions_de_mots' => '
		List the keywords which do not seem to have been translated yet.
	',
	'explication_traductions_mots_differents' => '
		List the translated objects which do not have exactly
		the same keywords assigned as for the original object.
	',
	'explication_traductions_mots_synchroniser' => '
		The «synchronise» action copies all of the keywords from the original
		element to the translated element. Note, however, that this does not delete any
		additional keywords that have already been linked to the translated object.
	',

	// I
	'info_actions' => 'Actions',
	'info_revisions' => 'Revisions',
	'info_source' => 'Source',
	'info_statut' => 'Status',
	'info_statut_court' => 'S',
	'info_titre' => 'Title',
	'info_type' => 'Group',

	// L
	'label_groupe_mots_multi' => 'Multi keyword groups',
	'label_langue' => 'Target language',
	'langue' => 'Language',
	'langue_court' => 'L',
	'langue_en_cours' => 'The currently selected language is : <strong>@langue@</strong>.',
	'liste_des_lieux' => 'Areas to be explored',

	// M
	'mots_cles_differents' => 'Keywords different from the original',
	'mots_cles_non_traduits' => 'Keywords',
	'mots_non_traduits' => 'Untranslated keywords',

	// N
	'navigateurs' => 'Navigators',
	'non_traduit' => 'Untranslated or translations not declared',

	// O
	'origine_modifiee' => 'Original modified',

	// R
	'rubriques_mots_differents' => 'Sections',
	'rubriques_non_traduites' => 'Sections',
	'rubriques_originales_modifiees' => 'Sections',

	// S
	'second_navigateur' => 'Second navigator',
	'selection_de_la_langue_cible' => 'Target language selection',
	'synchroniser' => 'Synchronise',
	'synchroniser_tout' => 'Synchronise all',

	// T
	'traductions_a_realiser' => 'Translations to be made',
	'traductions_a_verifier' => 'Translations to be checked',
	'traductions_de_mots' => 'Translations of keywords',
	'traductions_mots_differents' => 'Synchronise the keywords',

	// V
	'voir_article_source' => 'Show the source article',
	'voir_les_revisions_de_la_source' => 'Show the revisions of the source',
	'voir_rubrique_source' => 'Show the source section',

	// Z
	'zbug_critere_necessite_parametre' => 'The {@critere@} criteria requires a parameter'
);

?>
