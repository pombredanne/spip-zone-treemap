<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(
	'adresse_bitcoin' => 'Direcci&oacute;n de pago de Bitcoin',
	'adresse_bitcoin_precisions' => 'Es una cadena de car&aacute;cteres que representa la direcci&oacute;n a la cual le pueden hacer un pago v&iacute;a Bitcoin. Ejemplo: 1LA71x9eeoqJwhnWkXRSSiwL6oqTMM4wPE',
);
