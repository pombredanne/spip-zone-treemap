<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(
	'adresse_bitcoin' => 'Adresse de paiement Bitcoin',
	'adresse_bitcoin_precisions' => 'Il s\'agit d\'une chaîne représentant l\'adresse à laquelle les gens peuvent effectuer un paiement via Bitcoin. Exemple&nbsp;: 1LA71x9eeoqJwhnWkXRSSiwL6oqTMM4wPE',
);
