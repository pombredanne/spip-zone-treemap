<?php
/**
 * Plugin factures - Facturer avec Spip 2.0
 * Licence GPL (c) 2010
 * par Cyril Marion - Camille Lafitte
 */

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	// B
	'bouton_factures' => 'Factures &amp; Devis',
	'bouton_configurer_factures' => 'Configurer Factures &amp; Devis',

    // C
    'choisir_groupe_mots_facture' => 'Choisir un groupe de mots clé pour définir le type de facture (avoir, devis, proforma...).',
    'choisir_organisation' => 'Choisir une organisation (en général la votre...) qui sera l\'emetteur de toutes les factures.',
    'choisir_auteur_emetteur' => 'Choisir un auteur (en général vous ou celui attaché à votre organisation...) qui sera l\'emetteur de toutes les factures.',

	// E
	'editer_facturation_titre' => 'Editer une facture',
	'explication_liste_factures' => 'Objectif : un tableau, avec pagination, moteur de recherche, possibilit&eacute; de tri (clic sur colonne) et de filtre (s&eacute;lection d\'un mot cl&eacute;, p.ex. le type de document : facture ou devis)',
	'explications_page_factures' => 'Sur cette page :<br />
- liste des factures &amp; devis<br />
- bouton cr&eacute;er facture',
	'explications_page_config_factures' => 'essentiellement choix du groupe de mots cl&eacute; d&eacute;finissant les types de factures (facture, devis, proforma, avoir) et les organisations &eacute;mettrices de factures (au moins une, celle de celui qui facture); si pas de groupe pr&eacute;-d&eacute;fini on en cr&eacute;e un; si pas d\'Organisation pr&eacute;-d&eacute;finie, on en cr&eacute;e une.',

	// N
	'nom_defaut' => 'Fiche facture par d&eacute;faut',

	// T
	'titre_page_factures' => 'G&eacute;rer les Factures &amp; Devis',
	'titre_page_config_factures' => 'Configuration du plugin Factures &amp; Devis',
	'titre_liste_factures' => 'Liste des factures existantes',

    // Z
    'z_facture' => 'Test pour voir si le plugin factures était bien pris en compte',

);
?>