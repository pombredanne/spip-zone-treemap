<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/spipproprio?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'abbreviation_genre_label' => 'Abbrev. / Gender',
	'admin_legend' => 'About site\'s administration',
	'administrateur_aussi_comment' => 'Check this box if this structure is also <b>administrator</b> of the site.', # MODIF
	'administrateur_aussi_label' => 'Body site administrator',
	'administrateur_mail_label' => 'Administrative contact email adress of the site',
	'adresse_comment' => 'Complete these fields as accurately as possible and simple. If your entry permit, a link to a GoogleMap will be generated.', # MODIF
	'adresse_legend' => 'Adress',
	'all_site' => 'Whole site',
	'au_capital_de' => ' with a capital of ',

	// B
	'bouton_auto_fr' => 'Automatic fields for France',
	'bouton_exporter' => 'Launch export', # MODIF
	'bouton_importer' => 'Launch import', # MODIF
	'bouton_saisie_libre' => 'Free entry',

	// C
	'capital' => ' with a capital of ',
	'capital_social_comment' => 'Précisez la monnaie le cas échéant (<i>€ &dollar;</i>).', # NEW
	'capital_social_label' => 'Capital',
	'cnil_comment' => 'If you made ​​a statement to the CNIL, specify by what date and under what receipt.', # MODIF
	'cnil_date_label' => 'Filing date', # MODIF
	'cnil_legend' => 'CNIL informations',
	'cnil_numero_label' => 'N° of receipt', # MODIF
	'code_postal_label' => 'Zip code',
	'conditions_utilisation' => 'Terms of use',
	'coordonnees_adresse' => 'Adr.',
	'coordonnees_capital' => ' with a capital of ',
	'coordonnees_fax' => 'Fax',
	'coordonnees_mail' => 'Mail',
	'coordonnees_mailto_ttl' => 'Contact us by email',
	'coordonnees_tel' => 'Tel.', # MODIF
	'copyright_annee_label' => 'Creation year', # MODIF
	'copyright_comment' => 'To recap, in France, there\'s not necessary to have to file your copyright by an organization for an effective protection. The copyright information may be listed by default.', # MODIF
	'copyright_comment_label' => 'Protection comment',
	'copyright_complement_label' => 'Completion', # MODIF
	'copyright_default_comment' => 'Reproduction, even partial, of the contents of pages on this site without prior consent is strictly prohibited.', # MODIF
	'copyright_default_comment_multi' => '<multi>Reproduction, even partial, of the contents of pages on this site without prior consent is strictly prohibited (short citations are permitted by French law for comments and criticism, as they are strictly concurrent, and are specified the original author and the Internet link to the source page).[fr]La reproduction, même partielle, des contenus des pages de ce site sans accord préalable est strictement interdite (les citations sont autorisées par le droit français pour commentaires et critiques, tant que ceux-ci y sont strictement concomitants et que sont précisés l\'auteur original et le lien Internet vers la page source).</multi>', # NEW
	'copyright_legend' => 'Copyright informations',
	'createur' => 'creator', # MODIF

	// D
	'de_la' => ' of the ',
	'du' => ' of the ',

	// E
	'enregistre_pres' => ' registered with ', # MODIF
	'enregistree_pres' => ' registered with ', # MODIF
	'enregistrement_numero_label' => 'Under N°', # MODIF
	'enregistrement_organisme_comment' => 'Register of Commerce and Companies, Artists House  ...', # MODIF
	'enregistrement_organisme_label' => 'Registered with', # MODIF
	'enregistrement_ou_label' => 'AutoComplete for France (<i>option replacing the field above</i>)', # MODIF
	'enregistrement_siren_comment' => 'ex.: 123 456 78 (9: automatic key)', # MODIF
	'enregistrement_siren_label' => 'SIREN [<abbr title="Système d\'Identification du Répertoire des ENtreprises - single INSEE code designating the person\'s legal firm">?</abbr>]', # MODIF
	'enregistrement_siren_title' => 'Nine digits enterprise reference, you can enter only the first eight digits.', # MODIF
	'enregistrement_siret_comment' => 'ex.: 1234 (5: automatic key)', # MODIF
	'enregistrement_siret_label' => 'SIRET (NIC) [<abbr title="Identifiant d\'établissement - Numéro Interne de Classement">?</abbr>]', # MODIF
	'enregistrement_siret_title' => 'Five digits bureau reference, you can not enter leading zeros or the last digit.', # MODIF
	'enregistrement_tvaintra_comment' => 'ex.: FR 45 (automatic)',
	'enregistrement_tvaintra_label' => 'TVA Intra. [<abbr title="T.V.A. Intracommunautaire number">?</abbr>]', # MODIF
	'enregistrement_tvaintra_title' => 'Calculated automatically from the SIREN.', # MODIF
	'erreur_config' => '!! - An unknown error occured _',
	'erreur_export' => '!! - An unknown error occured _',
	'erreur_import' => '!! - An unknown error occured _',
	'exporter_configuration' => 'Configuration export (<i>informations on the owner, the publisher ...</i>)', # MODIF
	'exporter_fichiers_langues' => 'Export languages strings of the site', # MODIF
	'exporter_importer' => 'Export/Import',

	// F
	'fax' => 'Fax', # MODIF
	'fax_abbrev' => 'Fax',
	'feminin' => 'feminine', # MODIF
	'fonction_administrateur' => 'Administrator',
	'fonction_webmaster' => 'Webmaster',
	'forme' => 'Legal form',
	'forme_comment' => 'Enterprise (<i>precise status ...</i>), association ...', # MODIF
	'forme_label' => 'Legal form',

	// H
	'hebergeur' => 'hosting', # MODIF
	'home_page' => 'Home page',

	// I
	'idem_label' => 'Same structure as', # MODIF
	'import_avertissement' => 'Do not change these values ​​until you are sure you: this will erase your current configuration and your language channels!<br /><br /><b>It is strongly advised to leave the burden of this page to main webmaster of your site.</b>', # MODIF
	'importer_dump' => '<strong>Import file :</strong> ',
	'infos_createur' => 'Creator Informations', # MODIF
	'infos_hebergeur' => 'Hosting Informations', # MODIF
	'infos_idem' => 'Same structure', # MODIF
	'infos_legend' => 'General Informations', # MODIF
	'infos_proprietaire' => 'Owner Informations', # MODIF

	// L
	'la' => 'the ',
	'le' => 'the ',
	'legal_comment' => 'This information is optional. If they are captured, they will be mentioned in the information generated block.', # MODIF
	'legal_legend' => 'Legal informations of the structure', # MODIF
	'libelle_comment' => 'Complement associated with the structure name.', # MODIF
	'libelle_label' => 'Wording', # MODIF

	// M
	'mail_label' => 'Email contact',
	'mail_to' => 'Contact this adress',
	'masculin' => 'masculine',
	'mel' => 'Email',
	'mentions_legales' => 'Legals Statements', # MODIF

	// N
	'new_window' => 'New window', # MODIF
	'news' => 'News', # MODIF
	'nom_label' => 'Name of the structure',
	'notes' => 'NOTES',
	'num_invalide' => 'Num. not valide',
	'numero_maj' => 'N°', # MODIF

	// O
	'ok_config' => 'OK - The values were recorded _', # MODIF
	'ok_export' => 'OK - Your configuration has been exported in file :<br />"<strong>@fichier@</strong>"', # MODIF
	'ok_import' => 'OK - Your configuration has been restored', # MODIF
	'outil_exporter' => 'Backup tools',
	'outil_importer' => 'Restoration tools',
	'outils_de_communication' => 'Communication tools',

	// P
	'pays_label' => 'Country',
	'pconfig_avertissement' => 'Do not change these values ​​until you are sure: these values ​​are valid for the entire site.<br /><br /><b>It is strongly advised to leave the burden of this page to the main webmaster of your site.</b>', # MODIF
	'pconfig_divers_legend' => 'Additional information', # MODIF
	'pconfig_texte' => 'You can here define the coordinates of the body @type@ and its legal informations.', # MODIF
	'pconfig_texte_ajouts' => '<br />The system will present information that is specified.<br />Note that some features of the plugin, including the automatic construction of "legal information" and "terms of use", require that up information is entered.', # MODIF
	'pconfig_texte_lien_doc' => '<br /><br />The button below shows	the result of different features for verification:', # MODIF
	'pconfig_texte_notes' => '<span style="color: red;">*</span><small>Fields marked with a red asterisk may include \'multi\' block.<br /></small>', # MODIF
	'pconfig_titre_page' => 'Configuration', # MODIF
	'pconfig_titre_page_short' => 'Configuration',
	'pourquoi_ce_plugin' => 'Why this plugin ? (<i>legal recalls</i>)', # MODIF
	'presentation' => '{{{Obligations légales françaises concernant tout site internet}}}

Tout site Internet, qu’il soit professionnel ou "de loisirs", doit respecter un ensemble d’obligations légales dictées par la [LCEN|Voir le texte sur legifrance.fr->http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000801164] ({Loi pour la confiance dans l’économie numérique}) de juin 2004. Ces informations doivent être clairement accessibles à tout internaute utilisant le site et l’éditeur du site, son propriétaire, a le devoir de contrôler son contenu, autant en terme rédactionnel que concernant les commentaires laissés par les internautes.

Dans la suite de cette documentation, nous utiliserons les termes suivants :
- l’{{éditeur du site}} est son "propriétaire", l’organisme ou la personne qui en est responsable, qui en édite le contenu ;
- le {{créateur du site}} est l’organisme, la personne ou l’agence qui l’a créé, fabriqué, à la demande de l’éditeur ({il est parfois nécessaire de différencier le créateur graphique du créateur technique}) ;
- l’{{hébergeur du site}} est l’organisme ou la personne qui possède le serveur sur lequel le site est hébergé physiquement.

Ces trois entités doivent être clairement définies dans les mentions légales de tout site web. Le créateur du site ne fait l’objet d’aucune obligation légale mais il est préférable de suivre la même procédure que pour l’hébergeur.

{{Concernant les sites édités par une personne morale ({entreprise, association ...})}}

Une page "mentions légales" doit apparaître clairement sur le site, listant l’ensemble des informations ci-dessous.
- la raison sociale de l’éditeur ;
- son siège social ;
- un numéro de téléphone ;
- l’identité du directeur de publication et, s’il y a lieu, du directeur de rédaction ;
- le nom de l’organisme et le numéro d’enregistrement auprès duquel l’activité de la personne morale a été déclarée ({RCS, Maison des Artistes ...}) ;
- le capital social de la personne morale s’il y a lieu ;
- un contact du responsable administratif du site ({adresse courriel des directeurs cités plus haut ou du webmestre}).

{{Concernant les sites édités par une personne physique ({un particulier})}}

Une page "mentions légales" doit apparaître clairement sur le site, indiquant les {{nom}}, {{prénom}}, {{adresse de domicile}} et {{contact téléphonique}} du propriétaire.
<br />Il est cependant possibe de conserver son anonymat et de se dispenser de mentionner ces informations à partir du moment où elles ont été transmises, complètes et correctes, à l’hébergeur du site, qui peut conserver ces informations secrètes, sauf dans le cadre d’une procédure judiciaire. Les mentions légales devront dans ce cas mentionner ce choix.

{{Informations sur les conditions d’hébergement du site}}

Pour les personnes morales {{et}} physiques ({dans tout les cas pour résumer}), il est obligatoire de préciser les conditions d’hébergement du site, même si ce dernier est hébergé à titre gratuit.
<br />Les informations suivantes doivent être mentionnées :
- nom de l’hébergeur ;
- sa raison sociale ;
- son siège social ;
- un numéro de téléphone.

Dans le cas d’un site hébergé sur un serveur personnel ou propriété de l’éditeur, ces informations sont caduques mais cela devra être précisé.

{{Informations personnelles des internautes}}

Dans le cas où des informations personnelles des internautes sont enregistrées par le site, comme par exemple une adresse courriel pour l’inscription à une newsletter, les conditions d’utilisation de ces informations par l’éditeur du site devront être clairement énumérées, notamment leur divulgation à des tiers. 

Dans le cas d’une newsletter, il y a {{obligation de proposer un moyen de désinscription à volonté}} à l’internaute ({un lien présent dans chaque mail est la procédure courante}).

{{Mention concernant la CNIL}}

La [CNIL->http://www.cnil.fr/] ({Commission Nationale de l’Informatique et des Libertés}) est un organisme ayant pour mission la protection de la vie privée et des libertés individuelles.
<br />Dès lors qu’une information est enregistrée par votre site, qu’elle soit directement ou indirectement nominative, une déclaration doit être faite à la CNIL, qui vous transmet en retour un numéro de récépissé, que vous avez obligation de mentionner.
<br />Pour rappel, la CNIL propose une procédure de déclaration en ligne ({cf. site ci-dessous}).

De plus, l’internaute doit pouvoir obtenir un accès, une rectification s’il y a lieu et même un effacement de toutes les informations le concernant personnellement stockées par le site. Les mentions légales devront donc préciser clairement le moyen mis à disposition de l’internaute pour effectuer une telle demande.

Site de la CNIL : [->http://www.cnil.fr/]

A noter que des modifications récentes dispensent certains sites ({personnels, blogs ou associatifs}) de cette déclaration. Reportez-vous au site de la CNIL pour plus d’informations.

{{Mentions complémentaires conseillées}}

Afin de vous protéger au mieux, il est très fortement conseillé d’inclure dans vos mentions légales des informations sur la protection dont les contenus et médias présents sur le site font l’objet, l’étendue de votre responsabilité quant à ces contenus ({communément "Limitation de responsabilité"}), une information concernant les cookies éventuellement déposés par le site ({conseil CNIL}).

Plus généralement, il y a lieu d’adapter vos mentions légales à votre activité ({professions réglementées, site de commerce ...}).
', # NEW
	'presentation_outils_de_communication' => '{{{Valoriser son site web}}}

Le plugin {{Mentions Légales}} propose également un ensemble d’outils de présentation d’informations pour valoriser et mettre en avant vos atouts.

Il propose par exemple un modèle de "carte de visite" du site ainsi qu’une "v-Card" ({Virtual Card permettant de charger automatiquement vos informations de contact dans un gestionnaire de mails ou un carnet d’adresse}).

Nous vous renvoyons à la page d’exemples ci-dessous pour plus d’information ({cette page prend en compte les informations saisies dans les différents formulaires du plugin, il est donc nécessaire d’en remplir au minimum quelques uns pour visualisation ...}).', # NEW
	'presentation_plugin' => '{{{Fonctionnalités du plugin Mentions Légales}}}

Ce plugin vous propose un ensemble de formulaires et de modèles pour faciliter la mise en place des informations précitées. Il vous permet notamment de définir les informations concernant l’éditeur, le créateur et l’hébergeur du site et propose des modèles d’affichages de celles-ci.

Il est conseillé de prendre un moment pour personnaliser le modèle "mentions_legales.html" présent dans le répertoire "modeles/" du plugin. C’est ici que se construisent les textes légaux. Vous pouvez modifier ou ajouter des textes depuis le bouton "Textes de langue propriétaire" ci-dessous.

Vous pouvez également inclure ces modèles dans les objets éditoriaux de SPIP en utilisant les raccourcis typographiques "&lt;mentions_legales|&gt;" et "&lt;conditions_utilisation|&gt;".

Pour finir, des modèles de squelettes d’en-tête et de pieds de page sont proposés, construits à l’image de ceux de la distribution de SPIP, ajoutant les informations de copyright et un accès aux mentions légales du site. Vous pouvez bien entendu les personnaliser : il s’agit des fichiers "inc-head.html" et "inc-pied.html" présents dans le répertoire "public/inc/" du plugin. Pour les utiliser, placez les directement dans le répertoire "public/" ou à la racine de votre répertoire de squelettes.

{{{Note pour les utilisateurs de squelettes Zpip}}}

Depuis sa version 1.1, le plugin propose une adaptation Z-compatible des squelettes cités plus haut. Pour les visualiser, ajoutez simplement "_zpip" aux noms de squelettes ci-dessus. Le contenu de ces squelettes est disponible dans le répertoire "public/contenu/".
', # NEW
	'proprietaire' => 'owner', # MODIF
	'proprietaire_export_import' => 'Mentions Légales : maintenance', # MODIF
	'proprietaire_export_import_texte_supp' => 'You can here save your current configuration and your personal language strings, or restore them from a previous backup (<i>including from another site</i>) ...<br /><br />', # MODIF
	'proprietaire_retour_plateforme' => 'Back to the platform', # MODIF
	'proprietaire_texte' => 'Here you can set global options for the site used by the plugin <b>Mentions Légales</b>.<br /><br />You can also set strings specific languages ​​whose usage is recalled on the specific page.<br />', # MODIF
	'proprietaire_texte_supp' => 'This plugin offers you to automate and centralize the management of an \'institutional\' or \'professional\' website running SPIP.<br /><br />It allows you to enter a set of information about <b>the owner</b> of the site, organization, company, institution or individual in order to present various ways and eventually generate automatically copyright information, some \'Legal Statement\' and \'Terms of Use\'.', # MODIF
	'proprietaire_titre_page' => 'Plateform Mentions Légales', # MODIF
	'proprietaire_titre_page_short' => 'Plateform',
	'pskels_info_mentions_legales' => 'The blocks below provide information about defining the <b>creator</b> and <b>hosting</b> of the site to establish automatically some <b>Legal Statement</b> and <b>Terms of Use</b>.<br /><br /><u>These texts represent the statutory minimum for any website</u> and are very generic ... The should be considered as a working basis for creating your own texts.<br /><br />You can edit them on page \'Owner language texts\' if you enable the option below.', # MODIF
	'pskels_legal_legend_createur' => 'Information on site creator', # MODIF
	'pskels_legal_legend_hebergeur' => 'Information on site hosting', # MODIF
	'pskels_titre_page' => 'Configuration ok skeletons',
	'ptexte_cliquez_pour_editer' => 'Click here to edit the entry with shortcuts formatting', # MODIF
	'ptexte_editable_ttl' => 'Click the line to edit ...',
	'ptexte_form_titre' => 'Complete edition / New entry', # MODIF
	'ptexte_info_supp' => '<br /><b>Usage of strings on this page</b><br /><br />Strings on this page are saved in a language file called \'<b>proprietaire</b>\', so you can call them in your templates using the formula:<br /><br /><center><code>&lt;:proprietaire:raccourci:&gt;</code></center><br />Note that if you use shortcuts in typing your text, you must add to the call above the filter \'<b>|propre</b>\'. This would give a call to the example above, the shape:<br /><br /><center><code>&lt;:proprietaire:raccourci|propre:&gt;</code></center><br />You can more simply use the tag <b>TEXTES_PROPRIETAIRE</b> this way:<br /><br /><center><code>#TEXTES_PROPRIETAIRE{<br />shortcut,<br />arguments,<br />treatement}</code></center><br />where "arguments" corresponds to an array of arguments passed to the string and "treatement" to the name of the function of text processing (<i>"propre" by default</i>).', # MODIF
	'ptexte_info_tags' => '<br /><b>You can use the tags below into your texts (<i>they should be reported as written here: ’@texte@’</i>) :</b><ul><li>@nom_site@</li><li>@url_site@</li><li>@descriptif_site@</li><li>@proprietaire_forme@</li><li>@proprietaire_nom@</li><li>@proprietaire_forme@</li><li>@proprietaire_nom _responsable@</li><li>@proprietaire_fonction_ responsable_texte@</li><li>@proprietaire_ mail_administratif@</li></ul>This list is <b>not exhaustive</b> tag if present in the texts are not listed here, keep them ...', # MODIF
	'ptexte_info_texte' => '<ul><li><b>Languages files</b><br />The language file entries are defined by specifying a string\'s call (<i>entries \'shortcut\' of the table</i>) which corresponds to a full text (<i>entries \'displayed text\' of the table</i>), that can contain formatting and be as long as desired.<br /></li><li><b>Shortcuts Nomenclature</b><br />Shortcuts must be strings of text <u>without spaces or special characters</u> (<i>here you can enter your shortcuts with spaces, which will be replaced by underscore</i>).</li><li><b>Texts formatting</b><br />You can write your text in the same way that your SPIP articles ...</li></ul>', # MODIF
	'ptexte_info_titre' => 'Help / Reminders ...',
	'ptexte_supprimer' => 'Delete entry', # MODIF
	'ptexte_texte' => 'This page allows you to manage certain owner texts necessary for the site without the regular edition mode (<i>modified language file on the server</i>).<br /><br />Entries \'Text displayed\' in the table are editable by clicking on the \'shortcut\' correspondent. You can also add entries using the form \'Complete Edition\'.', # MODIF
	'ptexte_titre_page' => 'Language owner texts', # MODIF
	'ptexte_titre_page_short' => 'Language texts',

	// R
	'responsable_fonction_label' => 'Responsible function',
	'responsable_mail_label' => 'Responsible email',
	'responsable_nom_label' => 'Responsible name',
	'retour' => 'Back',
	'rue_label' => 'N° and Street', # MODIF

	// S
	'sauvegardes_dans_dump' => 'Your dumps are to be placed in the \'tmp/dump/\' directory.', # MODIF
	'serveur_legend' => 'Information about the server hosting the site', # MODIF
	'serveur_os_comment' => 'OS installed on this server.', # MODIF
	'serveur_os_label' => 'Running on a system ', # MODIF
	'serveur_os_web_label' => 'Web information link about this system', # MODIF
	'serveur_type_comment' => 'Specifications of the server hosting the site.', # MODIF
	'serveur_type_label' => 'Server type',
	'siege' => 'Headquarters', # MODIF
	'siren' => '<abbr title="French Système d\'Identification du Répertoire des ENtreprises">SIREN</abbr>', # MODIF
	'siren_abbrev' => 'Siren',
	'siret' => '<abbr title="French Système d\'Identification du Répertoire des ETablissements">SIRET</abbr>', # MODIF
	'siret_abbrev' => 'Siret',
	'site_web_label' => 'Web site URL',
	'sous_le_numero' => ' under reference ', # MODIF
	'spip_proprio' => 'Mentions Légales', # MODIF

	// T
	'tel' => 'Phone', # MODIF
	'tel_abbrev' => 'Tel', # MODIF
	'telecopie_label' => 'Fax number', # MODIF
	'telephone_label' => 'Phone numbre', # MODIF
	'testing_page_bloc' => 'Informations blocks',
	'testing_page_bloc_complet' => 'Full contact block',
	'testing_page_business_cards' => 'Business Cards (<i>v-card and cards</i>)',
	'testing_page_business_cards_complet' => 'Full "business cards" text',
	'testing_page_carte_visite' => 'Cards (<i>work in progress</i>)', # MODIF
	'testing_page_carte_visite_administrateur' => 'Adminitrator card',
	'testing_page_carte_visite_chef' => 'Responsible card',
	'testing_page_carte_visite_normale' => '"Classique" card (<i>default</i>)', # MODIF
	'testing_page_carte_visite_site' => 'Full website card (<i>web links</i>)', # MODIF
	'testing_page_carte_visite_webmaster' => 'Webmaster card',
	'testing_page_code' => 'Code: ',
	'testing_page_copyright' => 'Copyright informations',
	'testing_page_footer' => 'Copyright information',
	'testing_page_googlemap' => 'Googlemap',
	'testing_page_intro' => 'This page allows you to view different layouts of information given by the plugin. For each entry is indicated the calling code ({model or tag}) to include it in your skeletons.
	
We also recommend you take a look at the skeleton [legals->@mentions_legales@].', # MODIF
	'testing_page_liens_cartes_visite' => 'Information and links to cards',
	'testing_page_modele' => 'Model: ', # MODIF
	'testing_page_public' => 'Testing page',
	'testing_page_textes_proprietaire' => 'Usage of owner texts', # MODIF
	'testing_page_titre' => 'Test of owner configuration', # MODIF
	'testing_page_vcard' => 'Information and links to "vCard"',
	'tva_intracommunautaire' => 'V.A.T. Intra',
	'tva_intracommunautaire_abbrev' => 'VAT intra',
	'tva_non_applicable' => 'Not subject to VAT under the section 293B of the <abbr title="French Code Général des Impôts">CGI</abbr>', # MODIF
	'tva_nonapplicable_comment' => 'Not subject to VAT under the section 293B of the <abbr title="French Code Général des Impôts">CGI</abbr> ; micro-social regime.', # MODIF
	'tva_nonapplicable_label' => 'VAT not applicable',

	// U
	'utiliser_ce_plugin' => 'Usage of this plugin ...',

	// V
	'valider_pour_forcer' => '(vous pouvez cependant forcer une valeur vide en indiquant un underscore seul : "_")', # NEW
	'ville_label' => 'City'
);

?>
