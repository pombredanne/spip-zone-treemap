<?php
/**
 * RAPPELS : Obligations légales françaises concernant tout site internet
 *
 * Tout site Internet, qu'il soit professionnel ou 'de loisirs', doit respecter un ensemble
 * d'obligations légales dictées par la LCEN (<i>Loi pour la confiance dans l'économie numérique</i>)
 * de juin 2004. Ces informations doivent être clairement accessibles à tout internaute utilisant 
 * le site et l'éditeur du site, son propriétaire, a le devoir de contrôler son contenu, autant
 * en terme rédactionnel que sur les commentaires laissés par les internautes.
 *
 * Dans la suite de cette documentation, nous utiliserons les termes suivants :
 * <ul>
 *		<li>l'<b>éditeur du site</b> est son 'propriétaire', l'organisme ou la personne 
 * qui en est responsable, qui en édite le contenu</li>
 *		<li>le <b>créateur du site</b> est l'organisme, la personne ou l'agence qui l'a
 * construit, fabriqué, à la demande de l'éditeur (<i>il est parfois nécessaire de différencier
 * le créateur graphique du créateur technique</i>)</li>
 *		<li>l'<b>hébergeur du site</b> est l'organisme ou la personne qui possède le serveur
 * sur lequel le site est hébergé physiquement.</li>
 * </ul>
 * Ces trois entités doivent être clairement définies dans les mentions légales de tout site web. Le
 * créateur du site ne fait l'objet d'aucune obligation légale mais il est préférable de suivre
 * la même procédure que pour l'hébergeur.
 *
 * <b>Concernant les sites édités par une personne morale (<i>entreprise, association ...</i>)</b>
 * <br />Une page "mentions légales" doit apparaître clairement sur le site, listant l'ensemble des
 * informations ci-dessous.
 * <ul>
 *		<li>la raison sociale de l'éditeur</li>
 *		<li>son siège social</li>
 *		<li>un numéro de téléphone</li>
 *		<li>l'identité du directeur de publication et, s'il y a lieu, du directeur de rédaction</li>
 *		<li>le nom de l'organisme et le numéro d'enregistrement auprès duquel l'activité de la personne
 * morale a été déclarée (<i>RCS, Maison des Artistes ...</i>)</li>
 *		<li>le capital social de la personne morale s'il y a lieu</li>
 *		<li>un contact du responsable administratif du site (<i>adresse courriel des directeurs cités plus
 * haut ou du webmestre</i>)</li>
 * </ul>
 *
 * <b>Concernant les sites édités par une personne physique (<i>un particulier</i>)</b>
 * <br />Une page "mentions légales" doit apparaître clairement sur le site, indiquant 
 * les <b>nom</b>, <b>prénom</b>, <b>adresse de domicile</b> et <b>contact téléphonique</b> du
 * propriétaire.
 * <br />Il est cependant possibe de conserver son anonymat et de se dispenser de mentionner
 * ces informations à partir du moment où elles ont été transmises, complètes et correctes, à
 * l'hébergeur du site, qui peut conserver ces informations secrètes, sauf dans le cadre d'une procédure judiciaire.
 *
 * <b>Informations sur les conditions d'hébergement du site</b>
 * <br />Pour les personnes morales <b>et</b> physiques (<i>dans tout les cas pour résumer</i>), il est
 * obligatoire de préciser les conditions d'hébergement du site, même si ce dernier est hébergé à titre
 * gratuit.
 * <br />Les informations suivantes doivent être mentionnées :
 * <ul>
 *		<li>nom de l'hébergeur</li>
 *		<li>sa raison sociale</li>
 *		<li>son siège social</li>
 *		<li>un numéro de téléphone</li>
 * </ul>
 * Dans le cas d'un site hébergé sur un serveur personnel ou propriété de l'éditeur, ces informations
 * sont caduques mais cela devra être précisé.
 *
 * <b>Informations personnelles des internautes</b>
 * <br />Dans le cas où des informations personnelles des internautes sont enregistrées par le site,
 * comme par exemple une adresse courriel pour l'inscription à une newsletter, les conditions d'utilisation
 * de ces informations par l'éditeur du site devront être clairement énumérées, notamment leur divulgation
 * à des tiers. 
 *
 * Dans le cas d'une newsletter, il y a <b>obligation de proposer un moyen de désinscription à volonté</b>
 * à l'internaute (<i>un lien présent dans chaque mail est la procédure courante</i>).
 *
 * <b>Mention concernant la CNIL</b>
 * <br />La CNIL (<i>Commission Nationale Informatique et Libertés</i>) est un organisme ayant pour mission
 * la protection de la vie privée et des libertés individuelles.
 * <br />Dès lors qu'une information est enregistrée par votre site, qu'elle soit directement ou 
 * indirectement nominative, une déclaration doit être faite à la CNIL, qui vous transmet en retour 
 * un numéro de récépissé, que vous avez obligation de mentionner.
 * <br />Pour rappel, la CNIL propose une procédure de déclaration en ligne (<i>cf. site ci-dessous</i>).
 * 
 * De plus, l'internaute doit pouvoir obtenir un accès, une rectification s'il y a lieu et même un effacement
 * de toutes les informations le concernant personnellement stockées par le site. Les mentions légales devront donc
 * préciser clairement le moyen mis à disposition de l'internaute pour effectuer une telle demande.
 *
 * Site de la CNIL : <a href="http://www.cnil.fr/" target="_blank">http://www.cnil.fr/</a>
 *
 * A noter que des modifications récentes dispensent certains sites (<i>personnels, blogs ou associatifs</i>)
 * de cette déclaration. Reportez-vous au site de la CNIL pour plus d'informations.
 *
 * <b>Mentions complémentaires conseillées</b>
 * <br />Afin de vous protéger au mieux, il est très fortement conseillé d'inclure dans vos mentions légales
 * des informations sur la protection dont les contenus et médias présents sur le site font l'objet,
 * l'étendue de votre responsabilité quant à ces contenus (<i>communément 'Limitation de responsabilité'</i>),
 * une information concernant les cookies éventuellement déposés par le site (<i>conseil CNIL</i>).
 *
 * Plus généralement, il y a lieu d'adapter vos mentions légales à votre activité (<i>professions réglementées,
 * site de commerce ...</i>).
 *
 * <b>Fonctionnalités du plugin SPIP Propriétaire</b>
 *
 * Ce plugin vous propose un ensemble de formulaires et de modèles pour faciliter la mise en place 
 * des informations précitées. Il vous permet notament de définir les informations concernant l'éditeur,
 * le créateur et l'hébergeur du site et propose des modèles d'affichages de ces informations.
 *
 * Il est conseillé de prendre un moment pour personnaliser le modèle 'mentions_legales.html' présents
 * dans le répertoire 'modeles/' du plugin. C'est ici que se construisent les textes légaux. Vous pouvez
 * modifier ou ajouter des textes dans le fichier de langue 'textes_legaux_XX.php' (<i>avec XX la langue
 * concernée</i>) dans le répertoire 'lang/' du plugin.
 * 
 * Pour finir, des modèles de squelettes d'en-tête et de pieds de page sont proposés, construits
 * à l'image de ceux de la distribution de SPIP, ajoutant les informations de copyright et un accès
 * aux mentions légales du site. Vous pouvez bien entendu les personnaliser : il s'agit des fichiers
 * inc-head.html' et 'inc-pied.html' présents dans le répertoire 'public/inc/' du plugin. Pour les utiliser,
 * placez les directement dans le répertoire 'public/'.
 */
if (!defined("_ECRIRE_INC_VERSION")) return;
//ini_set('display_errors','1'); error_reporting(E_ALL);

// ---------------------------------------
// SETUP
// ---------------------------------------

/**
 * Nom du meta du plugin
 */
define('_META_SPIP_PROPRIO', 'spip_proprietaire');

// ---------------------------------------
// FONCTIONS
// ---------------------------------------

function make_google_map_proprietaire($conf, $who='proprietaire'){
	$str = $google_str = '';
	if (strlen($conf[$who.'_adresse_rue'])) 
		$str .= str_replace(array(',', ';', '.', ':', '/'), '', strip_tags($conf[$who.'_adresse_rue']));
	if (strlen($conf[$who.'_adresse_code_postal'])) 
		$str .= ' '.str_replace(array(',', ';', '.', ':', '/'), '', strip_tags($conf[$who.'_adresse_code_postal']));
	if (strlen($conf[$who.'_adresse_ville'])) 
		$str .= ' '.str_replace(array(',', ';', '.', ':', '/'), '', strip_tags($conf[$who.'_adresse_ville']));
	if (strlen($str)) {
		$entries = explode(' ', $str);
		foreach($entries as $entry)
			if (strlen($entry)) $google_str .= urlencode($entry).'+';
		$google_str = trim($google_str, '+');
		return $google_str;
	}
	return false;
}

// ---------------------------------------
// CONFIG
// ---------------------------------------

function spip_proprio_enregistrer_config($args){
	if (!is_array($args)) return;
	$mess = array();
	$_conf = spip_proprio_recuperer_config();
	$conf = $_conf ? array_merge($_conf, $args) : $args;
	include_spip('inc/meta');
	ecrire_meta(_META_SPIP_PROPRIO, serialize($conf), 'non');
	ecrire_metas();
	return true;
}

function spip_proprio_recuperer_config($str=''){
	if (!isset($GLOBALS['meta'][_META_SPIP_PROPRIO])) return NULL;
	$_conf = unserialize($GLOBALS['meta'][_META_SPIP_PROPRIO]);
	if (strlen($str)) {
		if (isset($_conf[$str])) return $_conf[$str];
		return false;
	}
	return $_conf;
}

/**
 * Choix par defaut des options de presentation pour les formulaires
 */
function spip_proprio_form_config() {
	global $spip_ecran, $spip_lang, $spip_display;
	$config = $GLOBALS['meta'];
	$config['lignes'] = ($spip_ecran == "large") ? 8 : 5;
	$config['lignes_longues'] = $config['lignes']+15;
	$config['afficher_barre'] = $spip_display != 4;
	$config['langue'] = $spip_lang;
	$config['_browser_caret'] = $GLOBALS['browser_caret'];
	return $config;
}

// ---------------------------------------
// TEXTES PROPRIETAIRE
// ---------------------------------------

/**
 * Fonction de gestion des textes proprietaire
 */
function spip_proprio_proprietaire_texte($str='', $args=array(), $langdef='fr'){
	$souvenir = $GLOBALS['spip_lang'];
	$GLOBALS['spip_lang'] = $langdef;
	
	// Verification que la langue existe
//	$test = _T('proprietaire:exemple');
	// Ne fonctionne pas correctement avec '_T', on reprend la traduction pure de SPIP
	static $traduire=false ;
 	if (!$traduire) {
		$traduire = charger_fonction('traduire', 'inc');
		include_spip('inc/lang');
	}
	$text = $traduire('proprietaire:test_fichier_langue',$GLOBALS['spip_lang']);

	if (!isset($GLOBALS['i18n_proprietaire_'.$langdef])){
		$test = _T('texteslegaux:exemple');
		creer_fichier_textes_proprietaire( $GLOBALS['i18n_texteslegaux_'.$langdef], $langdef );
		$GLOBALS['i18n_proprietaire_'.$langdef] = $GLOBALS['i18n_texteslegaux_'.$langdef];
	}
	$GLOBALS['spip_lang'] = $souvenir;
	return _T('proprietaire:'.$str, $args);
}

/**
 * Creation de tous les fichiers de langue 'proprietaire_XX'
 * pour toutes les langues utilisees en partie publique
 */
function spip_proprio_charger_toutes_les_langues(){
	// on force le chargement de proprietaire_XX si present
	// pour toutes les langues utilisees en partie publique
	$langues_du_site = array('fr');
	foreach(array('langues_utilisees', 'langues_multilingue', 'langue_site') as $ln_meta) {
		if (isset($GLOBALS['meta'][$ln_meta]))
			$langues_du_site = array_merge($langues_du_site, explode(',',$GLOBALS['meta'][$ln_meta]));
	}
	$langues_du_site = array_unique($langues_du_site);
	foreach($langues_du_site as $ln) {
		spip_proprio_proprietaire_texte('', '', $ln);
	}
	return;
}

function textes_proprietaire( $array=false, $lang=null ){
	if (is_null($lang)) $lang = $GLOBALS['spip_lang'];
	$globale = 'i18n_proprietaire_'.$lang;

	$ok1 = spip_proprio_proprietaire_texte();
	if ($lang != 'fr') 
		$ok2 = spip_proprio_proprietaire_texte('', '', $lang);
	if (isset($GLOBALS["$globale"]))
		$GLOBALS["$globale"] = array_merge($GLOBALS['i18n_proprietaire_fr'], $GLOBALS["$globale"]);
	else 
		$GLOBALS["$globale"] = $GLOBALS['i18n_proprietaire_fr'];
	if ($array)
		return $GLOBALS["$globale"];
	return false;
}

function charger_textes_proprietaire($bloc=true){
	include_spip('inc/presentation');
	include_spip('inc/texte');
	$div = '';

	$valeurs = array();
	$tableau = textes_proprietaire(true);
	if (isset($tableau) AND is_array($tableau)) {
		ksort($tableau);
		if($bloc) $div .= debut_cadre_relief('',true,'','','raccourcis');
		$div .= "\n<table class='spip' style='border:0;'>";
		$div .= "\n<tr class='titrem'><th class='verdana1'>"._T('module_raccourci')."</th>\n<th class='verdana2'>"._T('module_texte_affiche')."</th></tr>\n";
		$i = 0;
		foreach ($tableau as $raccourci => $val) {
			$bgcolor = alterner(++$i, 'row_even','row_odd');
			$div .= "\n<tr class='$bgcolor'><td class='verdana2' style='min-width:150px;'>"
				."<a href='".generer_url_ecrire('spip_proprio','page=textes&editer='.$raccourci)."' title='"._T('spipproprio:ptexte_cliquez_pour_editer')."'><b>$raccourci</b></td>\n"
				."<td id='$raccourci' class='arial2 editable' style='min-width:300px;'>".propre( $val )."</td></tr>";
		}
		$div .= "</table>";
		if ($bloc) $div .= fin_cadre_relief(true);
	}
	return $div;
}

function traiter_textes_proprietaire($raccourci, $lang='fr'){
	include_spip('inc/texte');
	$valeur = _request('value');
	$array_langue = textes_proprietaire(true);
//	$valeur = propre( $valeur );
	if (strlen($valeur)) $array_langue[$raccourci] = $valeur;
	elseif (isset($array_langue[$raccourci])) unset($array_langue[$raccourci]);
	if ($ok = creer_fichier_textes_proprietaire($array_langue, $lang))
		return $valeur;
	return false;
}

function creer_fichier_textes_proprietaire($array_langue, $lang='fr'){
	$file = 'proprietaire_'.$lang;
	$globale = 'i18n_proprietaire_'.$lang;
	if ( !file_exists( find_in_path('lang/'.$file) ) ){
		include_spip('inc/flock');
		$contenu = var_export($array_langue, true);
		$contenu_final = '<'."?php\n\$GLOBALS['$globale'] = $contenu;\n?".'>';
		$dir = _DIR_PLUGIN_SPIP_PROPRIO;
		$a = ecrire_fichier(
			($dir[strlen($dir)-1] == '/' ? substr($dir, 0, -1) : $dir).'/lang/'.$file.'.php', $contenu_final
		);
		return $a;
	}
}

function transformer_raccourci($str){
	include_spip('spip_proprio_fonctions');
	return spip_proprio_formater_nom_fichier($str);
}

?>