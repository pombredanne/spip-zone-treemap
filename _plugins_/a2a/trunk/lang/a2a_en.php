<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/a2a?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_lies' => 'LINKED ARTICLES',

	// C
	'cfg_a2a' => 'Relations between articles',
	'changer_type_liaison' => 'Change',
	'configurer_a2a' => 'Configurer les relations entre articles', # NEW

	// E
	'explication_navigateur' => 'Click on [Add] to navigate through the site and select the article(s) to link.',
	'explication_type_obligatoire' => 'Vous pouvez imposer de typer les liaisons', # NEW
	'explication_types_differents' => 'Cochez cette case pour permettre de lier plusieurs fois les deux mêmes articles, en changeant le type à chaque fois', # NEW
	'explication_types_liaisons' => 'Vous pouvez définir des types de relations : indiquez-en une par ligne, suivie d\'une virgule et d\'une description. Une ligne vide pour la valeur par défaut. La description peut être une chaîne de langue.', # NEW

	// I
	'info_nombre_articles_lies' => '@nb@ linked articles',
	'info_un_article_lie' => '1 linked article',

	// L
	'lien_lier_article' => 'Link to one or more articles',
	'lier_cet_article' => 'Link',
	'lier_cet_article_deux_cotes' => 'Link both sides',

	// O
	'ob_pb' => 'Il n\'est pas possible de rendre obligatoire le type de liaison : en effet les articles ci-dessous possèdent des liaisons non typées. Typez les d\'abords, puis rendez-obligatoire le typage.', # NEW

	// P
	'pas_articles_lies' => 'No linked articles',
	'pas_de_resultat' => 'No article found',

	// R
	'rang' => 'Rank',
	'rang_moins' => 'Up',
	'rang_plus' => 'Down',
	'recherche_arbo' => 'Tree navigation',
	'recherche_texte' => 'Textual search',
	'rechercher' => 'Search',
	'rechercher_seulement_titres' => 'Search only in titles',
	'rechercher_tip_articles_ids' => 'To find article number 123 directly, type \'art123\'.',
	'rechercher_un_article_a_lier' => 'Search for an article:',

	// S
	'sup_pb' => 'Impossible de supprimer les types de liaisons suivantes : des liaisons les utilisent. Il vous faut donc modifier au préalable ces liaions.', # NEW
	'supprimer_le_lien' => 'Unlink',
	'supprimer_le_lien_deux_cotes' => 'Unlink both sides',

	// T
	'td_pb' => 'Vous demandez l\'interdiction de lier plusieurs fois deux mêmes articles. Or certains articles possèdent plusieurs liaisons avec d\'autres articles. Corrigez cela avant.', # NEW
	'type' => 'Type',
	'type_inexistant' => 'Type de liaison inexistant', # NEW
	'type_liaison' => 'Type of link',
	'type_obligatoire' => 'Typing mandatory',
	'types_differents' => 'Multiple links',
	'types_liaisons' => 'Types of link',
	'types_liaisons_non_modifiables' => 'Les types de liaisons suivants ne sont pas modifiables via la configuration', # NEW

	// U
	'utilise_par_articles' => ', utilisé par les liaisons des articles :', # NEW

	// V
	'voir' => 'See'
);

?>
