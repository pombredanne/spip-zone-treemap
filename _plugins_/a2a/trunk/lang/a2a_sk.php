<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/a2a?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_lies' => 'PREPOJENÉ ČLÁNKY',

	// C
	'cfg_a2a' => 'Vzťahy medzi článkami',
	'changer_type_liaison' => 'Zmeniť',
	'configurer_a2a' => 'Nastaviť vzťahy medzi článkami',

	// E
	'explication_navigateur' => 'Ak sa chcete orientovať v stromovej štruktúre stránky, aby ste mohli vybrať, články, na ktoré chcete vytvoriť odkaz, kliknite na tlačidlo [Pridať].',
	'explication_type_obligatoire' => 'Môžete zadať typ odkazov',
	'explication_types_differents' => 'Zaškrtnite toto políčko, ak chcete niekoľko krát vytvoriť prepojenie na dva rovnaké články a hocikedy mať možnosť zmeniť jeho typ',
	'explication_types_liaisons' => 'Vous pouvez définir des types de relations : indiquez-en une par ligne, suivie d\'une virgule et d\'une description. Une ligne vide pour la valeur par défaut. La description peut être une chaîne de langue.', # NEW

	// I
	'info_nombre_articles_lies' => '@nb@ prepojených článkov',
	'info_un_article_lie' => '1 prepojený článok',

	// L
	'lien_lier_article' => 'Čítať jeden článok alebo viac článkov',
	'lier_cet_article' => 'Vytvoriť odkaz',
	'lier_cet_article_deux_cotes' => 'Prepojiť obe strany',

	// O
	'ob_pb' => 'Il n\'est pas possible de rendre obligatoire le type de liaison : en effet les articles ci-dessous possèdent des liaisons non typées. Typez les d\'abords, puis rendez-obligatoire le typage.', # NEW

	// P
	'pas_articles_lies' => 'Žiadne prepojené články',
	'pas_de_resultat' => 'Žiadne výsledky',

	// R
	'rang' => 'Hodnotenie',
	'rang_moins' => 'Vzostupne',
	'rang_plus' => 'Zostupne',
	'recherche_arbo' => 'Vyhľadať v stromovej štruktúre',
	'recherche_texte' => 'Textové vyhľadávanie',
	'rechercher' => 'Hľadať',
	'rechercher_seulement_titres' => 'Hľadať iba v nadpisoch',
	'rechercher_tip_articles_ids' => 'Ak chcete priamo zobraziť článok číslo 123, zadajte "art123".',
	'rechercher_un_article_a_lier' => 'Nájsť článok:',

	// S
	'sup_pb' => 'Impossible de supprimer les types de liaisons suivantes : des liaisons les utilisent. Il vous faut donc modifier au préalable ces liaions.', # NEW
	'supprimer_le_lien' => 'Zrušiť odkazy',
	'supprimer_le_lien_deux_cotes' => 'Zrušiť prepojenie',

	// T
	'td_pb' => 'Vous demandez l\'interdiction de lier plusieurs fois deux mêmes articles. Or certains articles possèdent plusieurs liaisons avec d\'autres articles. Corrigez cela avant.', # NEW
	'type' => 'Typ',
	'type_inexistant' => 'Typ prepojenia neexistuje',
	'type_liaison' => 'Typ prepojenia',
	'type_obligatoire' => 'Povinné rozlíšenie typu',
	'types_differents' => 'Viacnásobné prepojenia',
	'types_liaisons' => 'Typy prepojenia',
	'types_liaisons_non_modifiables' => 'Tieto typy prepojení sa v nastaveniach nedajú zmeniť ',

	// U
	'utilise_par_articles' => ', používa sa v prepojeniach medzi článkami:',

	// V
	'voir' => 'Zobraziť'
);

?>
