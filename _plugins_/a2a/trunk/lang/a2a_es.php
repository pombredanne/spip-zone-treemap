<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/a2a?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'appliquer_type_liaison' => 'Type de liaison :', # NEW
	'appliquer_type_liaison_explication' => 'Décrivez le type de lien à appliquer lors de la liaison. Utilisez de préférence un code ou un mot court.', # NEW
	'articles_lies' => 'Connectar un artículo',

	// E
	'explication_navigateur' => 'Cliquez sur [Ajouter] pour naviguer dans l\'arborescence du site afin de sélectionner les articles à lier.', # NEW

	// L
	'lier_cet_article' => 'Connectar',
	'lier_cet_article_deux_cotes' => 'Lier des deux cotés', # NEW

	// P
	'pas_articles_lies' => 'No artículo relacionado',
	'pas_de_resultat' => 'No resultados',

	// R
	'rang_moins' => 'Arriba',
	'rang_plus' => 'Abajo',
	'recherche_arbo' => 'Recherche arborescente', # NEW
	'recherche_texte' => 'Recherche textuelle', # NEW
	'rechercher' => 'Buscar',
	'rechercher_seulement_titres' => 'Rechercher seulement dans les titres', # NEW
	'rechercher_tip_articles_ids' => 'Pour afficher directement l\'article numéro 123, entrer \'art123\'.', # NEW
	'rechercher_un_article_a_lier' => 'Buscar un artículo:',

	// S
	'supprimer_le_lien' => 'Desconectar',
	'supprimer_le_lien_deux_cotes' => 'Délier des deux cotés', # NEW

	// V
	'voir' => 'Ver'
);

?>