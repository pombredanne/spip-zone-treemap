<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/a2a?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'appliquer_type_liaison' => 'Type ok link:',
	'appliquer_type_liaison_explication' => 'Describe the type of link to be applied when linking. Preferably use a code or a short word.',
	'articles_lies' => 'LINKED ARTICLES',

	// E
	'explication_navigateur' => 'Click on [Add] to navigate through the site and select the article(s) to link.',

	// I
	'info_nombre_articles_lies' => '@nb@ linked articles',
	'info_un_article_lie' => '1 linked article',

	// L
	'lien_lier_article' => 'Link to one or more articles',
	'lier_cet_article' => 'Link',
	'lier_cet_article_deux_cotes' => 'Link both sides',

	// P
	'pas_articles_lies' => 'No linked articles',
	'pas_de_resultat' => 'No article found',

	// R
	'rang' => 'Rank',
	'rang_moins' => 'Up',
	'rang_plus' => 'Down',
	'recherche_arbo' => 'Tree navigation',
	'recherche_texte' => 'Textual search',
	'rechercher' => 'Search',
	'rechercher_seulement_titres' => 'Search only in titles',
	'rechercher_tip_articles_ids' => 'To find article number 123 directly, type \'art123\'.',
	'rechercher_un_article_a_lier' => 'Search for an article:',

	// S
	'supprimer_le_lien' => 'Unlink',
	'supprimer_le_lien_deux_cotes' => 'Unlink both sides',

	// T
	'type' => 'Type',
	'type_liaison' => 'Type of link',

	// V
	'voir' => 'See'
);

?>
