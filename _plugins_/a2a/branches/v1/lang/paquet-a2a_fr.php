<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/a2a/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a2a_description' => 'Ce plugin permet de lier des articles de façon ponctuelle sans avoir à utiliser les mots-clés de SPIP. Il peut être utilisé pour mettre en place des liens de type « Lire aussi ».',
	'a2a_slogan' => 'Pour lier des articles'
);

?>
