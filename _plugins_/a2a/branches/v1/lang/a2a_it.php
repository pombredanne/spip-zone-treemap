<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/a2a?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'appliquer_type_liaison' => 'Type de liaison :', # NEW
	'appliquer_type_liaison_explication' => 'Décrivez le type de lien à appliquer lors de la liaison. Utilisez de préférence un code ou un mot court.', # NEW
	'articles_lies' => 'ARTICOLI COLLEGATI',

	// E
	'explication_navigateur' => 'Clicca su [Aggiungi] per navigare nella gerarchia del sito per selezionare gli articoli da collegare.',

	// I
	'info_nombre_articles_lies' => '@nb@ articles liés', # NEW
	'info_un_article_lie' => '1 article lié', # NEW

	// L
	'lien_lier_article' => 'Lier à un ou des articles', # NEW
	'lier_cet_article' => 'Collega',
	'lier_cet_article_deux_cotes' => 'Lier des deux cotés', # NEW

	// P
	'pas_articles_lies' => 'Nessun articolo collegato',
	'pas_de_resultat' => 'Nessun risultato',

	// R
	'rang' => 'Rang', # NEW
	'rang_moins' => 'Sopra',
	'rang_plus' => 'Sotto',
	'recherche_arbo' => 'Ricerca per rubrica',
	'recherche_texte' => 'Ricerca testuale',
	'rechercher' => 'Ricerca',
	'rechercher_seulement_titres' => 'Ricerca solamente nei titoli',
	'rechercher_tip_articles_ids' => 'Per mostrare direttamente l\'articolo numero 123, inserire \'art123\'.',
	'rechercher_un_article_a_lier' => 'Ricerca un articolo:',

	// S
	'supprimer_le_lien' => 'Scollega',
	'supprimer_le_lien_deux_cotes' => 'Délier des deux cotés', # NEW

	// T
	'type' => 'Type', # NEW
	'type_liaison' => 'Type de liaison', # NEW

	// V
	'voir' => 'Vedi'
);

?>
