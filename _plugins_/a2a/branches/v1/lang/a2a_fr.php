<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/a2a/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'appliquer_type_liaison' => 'Type de liaison :',
	'appliquer_type_liaison_explication' => 'Décrivez le type de lien à appliquer lors de la liaison. Utilisez de préférence un code ou un mot court.',
	'articles_lies' => 'ARTICLES LIÉS',

	// E
	'explication_navigateur' => 'Cliquez sur [Ajouter] pour naviguer dans l\'arborescence du site afin de sélectionner les articles à lier.',

	// I
	'info_nombre_articles_lies' => '@nb@ articles liés',
	'info_un_article_lie' => '1 article lié',

	// L
	'lien_lier_article' => 'Lier à un ou des articles',
	'lier_cet_article' => 'Lier',
	'lier_cet_article_deux_cotes' => 'Lier des deux cotés',

	// P
	'pas_articles_lies' => 'Pas d\'articles liés',
	'pas_de_resultat' => 'Pas de résultats',

	// R
	'rang' => 'Rang',
	'rang_moins' => 'Monter',
	'rang_plus' => 'Descendre',
	'recherche_arbo' => 'Recherche arborescente',
	'recherche_texte' => 'Recherche textuelle',
	'rechercher' => 'Rechercher',
	'rechercher_seulement_titres' => 'Rechercher seulement dans les titres',
	'rechercher_tip_articles_ids' => 'Pour afficher directement l\'article numéro 123, entrer \'art123\'.',
	'rechercher_un_article_a_lier' => 'Rechercher un article :',

	// S
	'supprimer_le_lien' => 'Délier',
	'supprimer_le_lien_deux_cotes' => 'Délier des deux cotés',

	// T
	'type' => 'Type',
	'type_liaison' => 'Type de liaison',

	// V
	'voir' => 'Voir'
);

?>
