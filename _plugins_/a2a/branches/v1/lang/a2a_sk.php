<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/a2a?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'appliquer_type_liaison' => 'Typ prepojenia:',
	'appliquer_type_liaison_explication' => 'Uveďte, aký typ odkazu vytvoríte. Optimálne bude, ak použijete kód alebo krátke slovo.',
	'articles_lies' => 'PREPOJENÉ ČLÁNKY',

	// E
	'explication_navigateur' => 'Ak sa chcete orientovať v stromovej štruktúre stránky, aby ste mohli vybrať, články, na ktoré chcete vytvoriť odkaz, kliknite na tlačidlo [Pridať].',

	// I
	'info_nombre_articles_lies' => '@nb@ prepojených článkov',
	'info_un_article_lie' => '1 prepojený článok',

	// L
	'lien_lier_article' => 'Čítať jeden článok alebo viac článkov',
	'lier_cet_article' => 'Vytvoriť odkaz',
	'lier_cet_article_deux_cotes' => 'Vytvoriť dva odkazy',

	// P
	'pas_articles_lies' => 'Žiadne prepojené články',
	'pas_de_resultat' => 'Žiadne výsledky',

	// R
	'rang' => 'Hodnotenie',
	'rang_moins' => 'Vzostupne',
	'rang_plus' => 'Zostupne',
	'recherche_arbo' => 'Vyhľadať v stromovej štruktúre',
	'recherche_texte' => 'Textové vyhľadávanie',
	'rechercher' => 'Hľadať',
	'rechercher_seulement_titres' => 'Hľadať iba v nadpisoch',
	'rechercher_tip_articles_ids' => 'Ak chcete priamo zobraziť článok číslo 123, zadajte "art123".',
	'rechercher_un_article_a_lier' => 'Nájsť článok:',

	// S
	'supprimer_le_lien' => 'Zrušiť odkazy',
	'supprimer_le_lien_deux_cotes' => 'Zrušiť oba odkazy',

	// T
	'type' => 'Typ',
	'type_liaison' => 'Typ prepojenia',

	// V
	'voir' => 'Zobraziť'
);

?>
