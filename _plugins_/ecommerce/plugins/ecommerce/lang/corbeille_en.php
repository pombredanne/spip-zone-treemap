<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
  // A
  'articles' => 'Articles',
  'aucun' => 'none',
  'auteurs' => 'Authors',

  // B
  'breves' => 'Breifs',

  // C
  'corbeille'=>'Recycle Bin',
  'choix_doc'=>'Chose the document types to display', 
  
  // D
  'doc_effaces' => 'Erased Documents:',
  
  // E
  'effacer' => 'Erase',
  'erreur' => 'Error',

  // F
  'forums_pri' => 'Private Forums',
  'forums_pub' => 'Publics Forums',

  // P
  'parution' => 'Date Published',
  'petitions' => 'Petitions',

  // R
  'readme' => 'This page allows the {{permanent deletion}} of documents sent to the recycle bin. You may leave documents in the recycle bin as long as you wish. It\'s {{up to you}} to put out the garbage.',
  
  // S
  'souci_grave' => 'Fatal Error!',

  // T
  'titre' => 'Title',
  'tous_les_articles' => 'All articles in the recycle bin:',
  'tous_les_auteurs' => 'All authors in the recycle bin:',
  'toutes_les_breves' => 'All breifs in the recycle bin:',
  'tous_les_messages_du_forum_pri' => 'All private forum messages in the recycle bin:',
  'tous_les_messages_du_forum_pub' => 'All public forum messages in the recycle bin:',
  'toutes_les_petitions' => 'All petitions in the recycle bin:',
  'tout' => 'All',

  // V
  'voir_detail' => 'see details'

);


?>
