<?php

$fond = "forum";
$delais = 3600;

// Exemples de personnalisation :
// @ http://www.spip.net/fr_article1825.html
//
// 1. seuls les mots-cles du groupe de mots numero 1 doivent s'afficher
// $afficher_groupe = array(1);
//
// 2. faire des forums uniquement pour affecter des mots-cles
// $afficher_texte = "non";

// En cas de forums sur abonnement, on a un panneau de login
$flag_dynamique = true;

include ("inc-public.php3");

?>
