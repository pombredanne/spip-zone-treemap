<BOUCLE_rubrique_principal(RUBRIQUES) {id_rubrique}>
<html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="#LANG_DIR" lang="#LANG">
<head>
<title>Il Salone</title>
<meta http-equiv="Content-Type" content="text/html; charset=#CHARSET" />

<!-- Ceci est la feuille de style par defaut pour les types internes a SPIP -->
<link rel="stylesheet" href="spip_style.css" type="text/css" />

<!-- Les feuilles de style specifiques aux presents squelettes -->
<link rel="stylesheet" href="#DOSSIER_SQUELETTE/typographie.css" type="text/css" />

<!-- media="..." permet de ne pas utiliser ce style sous Netscape 4 (sinon plantage) -->
<link rel="stylesheet" href="#DOSSIER_SQUELETTE/habillage.css" type="text/css" media="print, projection, screen, tv" />

<!-- Lien vers le backend pour navigateurs eclaires -->
<link rel="alternate" type="application/rss+xml" title="<:syndiquer_site:>" href="backend.php3" />
</head>
<body>
	<table style="text-align: left; width: 858px; height: 650px; background-color: rgb(86, 185, 239);" cellpadding="1" cellspacing="1">
	<tbody>
		<tr>
			<td style="height: 446px; width: 284px; background-color: rgb(87, 27, 27);">
				<BOUCLE_article_principal_logo(ARTICLES) {id_article=1}>
					[(#LOGO_ARTICLE|center||reduire_image{264,432})]
				</BOUCLE_article_principal_logo>
			</td>
			<td style="height: 446px; width: 565px; background-color: rgb(87, 27, 27);">


<div class="centre">
<p>
     <BOUCLE_fleches(ARTICLES){id_rubrique}>
        [(#COMPTEUR_BOUCLE|=={1}|?{' ',''})
   	<a href="[(#SELF|parametre_url{id_article,#ID_ARTICLE}|parametre_url{compteur,1})]" title="[(#TITRE)]">
        <img src='#DOSSIER_SQUELETTE/img_nav/start.gif' />
	</a>
        ]

        [(#COMPTEUR_BOUCLE|incremente|=={#ENV{compteur}}|?{' ',''})
   	<a href="[(#SELF|parametre_url{id_article,#ID_ARTICLE}|parametre_url{compteur,#_fleches:COMPTEUR_BOUCLE})]" title="[(#TITRE)]">
        <img src='#DOSSIER_SQUELETTE/img_nav/previous.gif' />
	</a>
        ]

        [(#COMPTEUR_BOUCLE|decremente|=={#ENV{compteur}}|?{' ',''})
   	<a href="[(#SELF|parametre_url{id_article,#ID_ARTICLE}|parametre_url{compteur,#_fleches:COMPTEUR_BOUCLE})]" title="[(#TITRE)]">
        <img src='#DOSSIER_SQUELETTE/img_nav/next.gif' />
	</a>
        ]

        [(#COMPTEUR_BOUCLE|=={#TOTAL_BOUCLE}|?{' ',''})
   	<a href="[(#SELF|parametre_url{id_article,#ID_ARTICLE}|parametre_url{compteur,#_fleches:TOTAL_BOUCLE})]" title="[(#TITRE)]">
        <img src='#DOSSIER_SQUELETTE/img_nav/end.gif' />
	</a>
        ]
     </BOUCLE_fleches>
</p>
</div>

<hr />
<div class="centre"><p>

<BOUCLE_zoom(ARTICLES) {id_article}>
	[(#LOGO_ARTICLE|||reduire_image{500})]<br />[(#TITRE)]<br />[(#DESCRIPTIF)]
</BOUCLE_zoom>
</B_zomm>

        <BOUCLE_first(ARTICLES) {id_rubrique} {0,1}>
	         [(#LOGO_ARTICLE|||reduire_image{500})]<br />[(#TITRE)]<br />[(#DESCRIPTIF)]
	</BOUCLE_first>
<//B_zoom>
</p></div>
<hr />


     <B_doc>
<div class="centre">
<p>
     <BOUCLE_doc(ARTICLES){id_rubrique}>
	    <a href="[(#SELF|parametre_url{id_article,#ID_ARTICLE}|parametre_url{id_document,#ID_DOCUMENT}|parametre_url{compteur,#_doc:COMPTEUR_BOUCLE})]" title="[(#TITRE)]">
            [(#LOGO_ARTICLE|||reduire_image{100})]
	    </a>


     [(#ENV{compteur}|=={1}|?{' ',''})
        [(#_doc:COMPTEUR_BOUCLE|=={4}|?{' ',''})
	    <a href="[(#SELF|parametre_url{id_article,#ID_ARTICLE}|parametre_url{id_document,#ID_DOCUMENT}|parametre_url{compteur,#_doc:COMPTEUR_BOUCLE})]" title="[(#TITRE)]">
            [(#LOGO_ARTICLE|||reduire_image{100})]
	    </a>
        ]
         [(#_doc:COMPTEUR_BOUCLE|=={5}|?{' ',''})
	    <a href="[(#SELF|parametre_url{id_article,#ID_ARTICLE}|parametre_url{id_document,#ID_DOCUMENT}|parametre_url{compteur,#_doc:COMPTEUR_BOUCLE})]" title="[(#TITRE)]">
            [(#LOGO_ARTICLE|||reduire_image{100})]
	    </a>
        ]
      ]
      [(#ENV{compteur}|=={2}|?{' ',''})
         [(#_doc:COMPTEUR_BOUCLE|=={5}|?{' ',''})
	    <a href="[(#SELF|parametre_url{id_article,#ID_ARTICLE}|parametre_url{id_document,#ID_DOCUMENT}|parametre_url{compteur,#_doc:COMPTEUR_BOUCLE})]" title="[(#TITRE)]">
            [(#LOGO_ARTICLE|||reduire_image{100})]
	    </a>
         ]
      ]
      </BOUCLE_doc>
      </B_doc>
</p>
</div>

				<B_menu_principal_texte>
					<BOUCLE_menu_principal_texte(RUBRIQUES) {id_rubrique} {par titre}>
						<div class="encart_menu_principal">
							<div class="menu-titre">
							[<div style="clear: both;"><a href="#URL_RUBRIQUE" [title="(#DESCRIPTIF|textebrut|entites_html)"]>(#LOGO_RUBRIQUE|center||reduire_image{300,300})</a></div>]
							</div>
							<div style="clear: both;"></div>
						</div>
					</BOUCLE_menu_principal_texte>
				</B_menu_principal_texte>

			</td>
		</tr>
		<tr>
			<td style="height: 196px; width: 284px; background-color: rgb(87, 27, 27);">
			<table style="text-align: left; width: 100%;" border="0" cellpadding="0" cellspacing="0">
			<tbody>
				<tr><td style="width: 95px;">
				<BOUCLE_rubrique_principal_logo(RUBRIQUES) {id_rubrique=1}>
					[(#LOGO_RUBRIQUE|center||reduire_image{89,177})]
				</BOUCLE_rubrique_principal_logo>
				</td>
				<td style="width: 287px; color: rgb(93, 199, 239);"><span class="text-ilsalone">4ieme Etage</span><br class="text-ilsalone" style="font-weight: bold;">
				<div class="text-ilsalone">
					1 Place broglie
					STRASBOURG
					<a href="mailto:boutique@ilsalone.com">Notre m�l</a>
					Du mardi au vendredi
					10H-13H et 14H30-19H
					Samedi
					10H-19H
				</div>
				</tr>
			</tbody>
			</table><span class="text-ilsalone"></span><span class="text-ilsalone"><br>
</span></td><td style="height: 196px; width: 565px; background-color: rgb(87, 27, 27);">
			<td style="height: 196px; width: 565px; background-color: rgb(87, 27, 27);">
				<B_menu_principal_logo>
					<BOUCLE_menu_principal_logo(RUBRIQUES) {id_rubrique>1} {racine} {par titre}>
						<div class="encart_menu_principal">
							<BOUCLE_articles_rubrique_logo(ARTICLES) {id_secteur} {par hasard} {0,1} >
								[<a href="#URL_SITE_SPIP/rubrique.php3?id_rubrique=#ID_SECTEUR" [title="(#DESCRIPTIF|textebrut|entites_html)"]>(#LOGO_ARTICLE|center||reduire_image{300,300})</a>]
							</BOUCLE_articles_rubrique_logo>
						</div>
					</BOUCLE_menu_principal_logo>
				</B_menu_principal_logo>
			</td>



</td>
</tr></tbody>
</table><br>
</body></html>
</BOUCLE_rubrique_principal>
