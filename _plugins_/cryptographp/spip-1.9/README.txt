Auteur: BobCaTT
Licence: GPL

Description:

[fr] Balise pour r�cup�rer des informations stoqu� dans les champs EXIF d'un fichier image

[en] SPIP Tag for retrieving EXIF metadata from an image file

Listes des fichiers:

[fr]
README.txt vous y etes,
_REGLES_DE_COMMIT.txt r�gles pour faire �voluer cette contrib,
plug_exif.php fichier d�clarant le nouveau tag. A importer dans votre fichier mes_fonctions.php3

[en]
README.txt you are here <-,
_REGLES_DE_COMMIT.txt rules of contribution on this project,
plug_exif.php file declaring the new tag, to import in your own mes_fonctions.php3


Page de la contrib:
[fr] http://spip-contrib.net/ecrire/articles.php3?id_article=788
[en] http://spip-contrib.net/ecrire/articles.php3?id_article=796


