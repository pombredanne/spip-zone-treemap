<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	//C
	'cfg_boite_lazyload' => 'Configuration du plugin jQuery Lazy Load',
	'cfg_descr_lazyload' => 'Lazy Load : Plugin jQuery Lazy Load pour SPIP',
	'cfg_inf_selecteur' => 'S&eacute;lecteur des images sur lesquelles Lazy Load sera appliqué (expression CSS ou &eacute;tendue jQuery).',
	'cfg_inf_distance' => 'Distance du bord de la fen&ecirc;tre &agrave; laquelle les images seront charg&eacute;es.',
	'cfg_inf_event' => '&Eacute;v&eacute;nement pour d&eacute;clencher l\'affichage des images (click, mouseover, etc).',
	'cfg_inf_effect' => 'Effet de transition lors du chargement des images (fadeIn, slideDown, etc).',
	
	'cfg_lbl_selecteur' => 'S&eacute;lecteur',
	'cfg_lbl_distance' => 'Distance',
	'cfg_lbl_event' => '&Eacute;v&eacute;nement',
	'cfg_lbl_effect' => 'Effet',
	'cfg_titre_lazyload' => 'Lazy Load'
	
);
?>
