<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
    'adjuntar_imagenes' => 'Attach images from Picasa',
	// B
	'buscar' => 'Search',
	'buscar_en_picasa' => 'Search public photos in Picasa Web Albums',

	
	//C
	'captcha' => 'Please, introduce the following captcha',
	'cargando' => 'Loading...',
	'cerrar' => 'Close',
	'conectar' => 'Connect to Picasa',

	// D
	'descripcion' => '<p>Spicasa use the Picasa Web API to connect with the services of Picasa Web Albums &copy;.<br/>
					 You can search public photos, o connect to your account to use your owns albums</p>
					 <p>If you have comments o suggestions, I\'ll be glade to read you, so
					 <a href="http://nqnwebs.com/-Contacto-" taget="_blank">write me</a></p>',
    'download_all' => 'Yoy can download the complete album',
	// E
    'el_album' => "The album",
    'email' => 'user@gmail.com',
    'exitosamente' => ' was retrieved succesfully.',
	// F

	// H

	// I
	'intente' => 'Please, try again.',


	// L
	'la_imagen' => 'The image',
	'link_add_album' => 'clicking here',
	'login' => 'Login in Picasa',
	'logged' => 'Connected as ',
    'login_invalid' => 'The username and/or password were not correct.',
	'login_fail' => 'Yoy attempt to login has failed',

	// M

	// N
    
    // O
    'o_puedes' => 'Or you can',
    

	// P
    'password' => 'password',
	// R
    'return' => 'return to yours albums.',
	// S

	// T

	// U
	'username' => 'User'

	// V
);

?>
