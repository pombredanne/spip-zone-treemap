<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'autocompletion_description' => 'Insérez dans vos formulaire une autocomplétion des communes/codes postaux et une géolocalisation sur Google map',
	'autocompletion_nom' => 'Autocomplétion',
	'autocompletion_slogan' => 'Autocomplétion et Géolocalisation',
);

?>