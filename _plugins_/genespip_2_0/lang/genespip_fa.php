<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/genespip?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_ete_mise_a_la_poubelle' => 'در وب سايت قرار گرفته',
	'acces_non' => 'حق دسترسي » محدود',
	'acces_oui' => 'حق دسترسي » جزئيات پرونده‌ها',
	'acces_restreint' => 'دسترسي به اين سايت محدود است',
	'administrateur' => 'ادمين',
	'adresse' => 'نشاني',
	'ajout_evenement' => 'افزودن يك برنامه',
	'ajout_lieu' => 'افزودن يك پيوند',
	'ajout_media' => 'افزودن يك پرونده رسانه (مديا)',
	'ajouter_nouveau_lien_article_spip_existant' => 'افزودن يك پيوند جديد به يك مقاله‌ي موجود اسپيپ',
	'annuler' => 'منتفي ',
	'arborescence_des' => 'بنگريد به شجره‌ي ',
	'arbre_asc' => 'شجره‌ي افزايشي ',
	'article_spip_lie' => 'مقاله‌ي شماره‌ي   پيوندي ',
	'aucune_fiche_possede_numero' => 'هيچ پرونده‌اي با آن رقم وجود ندارد',
	'avec' => 'با',

	// B
	'base_contient' => 'محتواي پايگاه‌داده‌ها',
	'base_genespip' => 'پايگاه ژن‌اسپيپ (GeneSpip)',

	// C
	'cellules_creees' => 'سلول‌هاي ايجاد شده',
	'cellules_occupees' => 'سلول‌هاي اشغال شده',
	'centans' => 'ممنوعيت نمايش تاريخ‌هاي كم‌تر از 100 سال',
	'charger' => 'بارگذاري',
	'choisir' => 'گزينش',
	'cliquer_ici_pour_lier_article_avec_fiche' => 'براي پيوند دادن مقاله با برگه اينحا را كليك كنيد',
	'confirmer' => 'تأييد',
	'contact' => 'تماس',
	'continuer' => 'ادامه',
	'creation_annulee' => 'ايجاد منتفي شد',
	'creation_fiche' => 'ايجاد پرونده',
	'creer' => 'ايجاد',
	'creer_document' => '›› ايجاد يك سند ››',

	// D
	'date' => 'تاريخ',
	'debut_gedcom' => 'آغاز ژن‌كام (GenCom)',
	'deces' => 'مرگ',
	'dep' => 'دپارت.',
	'departement' => 'دپارتمان',
	'depose_par' => 'Déposé par', # NEW
	'derniere_modif_le' => 'آخرين اصلاحيه در ',
	'derniere_modification' => 'آخرين روزآمدسازي پرونده',
	'descriptif_cfg' => '<p>اين منطقه براي پيكربندي داده‌هاي تغييرپذير پلاكين است.</p><p> براي خوب كاركردن اين پلاگين بايد همراه با پلاگين سي.اف.جي نصب شود</p>',
	'detail_fiche' => 'جزئيات پرونده',
	'document' => 'سندها', # MODIF
	'documents' => 'Documents', # NEW

	// E
	'enfant' => 'بچه‌ها',
	'enfants' => 'بچه‌ها',
	'enfants_couple' => 'بچه‌هاي جفت',
	'epouse' => 'همسر',
	'epoux' => 'شوهر',
	'est_present_dans_base' => 'در پايگاه موجود است',
	'evenement' => 'برنامه',
	'evenements' => 'برنامه‌ها',
	'evenements_lies' => 'برنامه‌هاي مرتبط',
	'export_gedcom' => 'صدور جدكام (GedCom)',
	'export_termine' => 'صدور انجام شد',
	'exporter' => 'صدور',

	// F
	'famille' => 'خانواده',
	'fermer' => 'بستن',
	'fiche_de' => 'پرونده‌ي ',
	'fiche_detail' => 'جزئيات پرونده',
	'fiche_document' => 'سند پرونده',
	'fiche_enfant' => 'بچه پرونده',
	'fiche_evt_par_lieu' => 'پرونده‌ي برنامه‌ها بر اساس مكان',
	'fiche_lieux' => 'پرونده‌ي مكان‌ها',
	'fiche_no' => 'پرونده‌ي شº',
	'fiche_num' => 'پرونده شماره(N)',
	'fiche_parents' => 'پرونده‌ي خانواده‌ها',
	'fiche_union' => 'پورنده‌ي متحد',
	'fiches' => 'پرونده‌ها',
	'fichier' => 'پرونده',
	'fichier_gedcom' => 'پرونده‌ي جدكام(GedCom)',

	// G
	'galerie' => 'گالري',
	'gedcom' => 'جدكام (GedCom)',
	'genealogie' => 'شجرنامه',
	'genespip' => 'ژن‌اسپيپ (GenSPIP)',

	// I
	'indication_format_photo' => '<br />بيشترين اندازه. 100 ك.ب',
	'individu' => 'فرد',
	'info_centans' => '<p>نمايش تاريخ‌هاي كم‌تر از 100 سال به طور خودكار ممنوع است. <br /> اين گزينه فقط در مورد بازديدكنندگان سايت كاربرد دارد<br /> اگر اين گزينه غيرفعال شده باشد، هنوز ممكن است نمايش پرونده به پرونده را محدود كرد.</p></p><NEW><p>Interdire systématiquement l‘affichage des dates ayant moins de 100 ans.<br />Cette option ne s‘applique que pour les visiteurs<br />Si cette option est désactivée, il est possible de limiter l‘affichage fiche par fiche.</p>',
	'info_deces' => 'اطلاعات مرگ',
	'info_doc' => '<p>پلاگين شجرنامه.</p> <p> شجره‌نامه خود را با اسپيپ در اينترنت منتشر كنيد.</p>',
	'info_gedcom_etape1' => 'شجره‌ي شما خالي است - يك پرونده‌ي جدكام (GedCom)وارد كنيد (يك م.ب حداكثر) يا يك پرونده‌ي نو بسازيد.<br />',
	'info_gedcom_etape2' => 'براي آنكه پرونده جدكام (GedCom)خود را وارد كنيد، ژن‌اسپيپ نياز دارد بداند چطور مي‌خواهيد داخل كردن «PLAC» پردازش كنيد<br />ژن‌اسپيپ نخستين 5 مدخل «PLAC«گزينش كرده است - يكي از آن ها را به عنوان مرجع براي بقيه انتخاب كنيد  ',
	'info_gedcom_etape3' => 'با شروع از انتخاب مدخل، مشخص كنيد كه ژن‌اسپيپ چگونه هر قطعه از اطلاعات را ترجمه كند',
	'info_mariage' => 'اطلاعات ازدواج',
	'info_multilingue' => '<p>نمايش سرصفحه‌ي چند زبانه در سايت. <br /> به منوي چندزبانگي اسپيپ برويد تا زبان‌هاي بي‌استفاده را غيرفعال سازيد</p>',
	'info_naissance' => 'اطلاعات تولد',
	'info_pub' => '<p>درآمدهاي آگهي براي خالق ژن‌اسپيپ در نظر گرفته مي‌شود،‌ و مي‌توانيد سرصفحه‌ي آگهي خود را، البته اگر بخواهيد سرصفحه‌ي خود را داشته‌ باشيند، از طريق اصلاح مطالب فايل : pub/200x200genespip.html - با اشتراك برنامه گوگل بگنجانيد</p>',
	'info_restriction_acces' => '<p>محدودسازي دسترسي به جزئيات پروند در سايت همگاني </p>',
	'info_restriction_acces0' => '<b>0</b> : محدوديت ادمين‌ها',
	'info_restriction_acces1' => '<b>1</b> : محدوديت ادمين‌ها و ويراستاران',
	'info_restriction_acces2' => '<b>2</b> : محدوديت براي ادمين‌ها، ويراستاران و بازديدگنندگان (قسمت خصوصي)',
	'info_restriction_acces3' => '<b>3</b> : بدون محدوديت ',
	'informations_sur_la_base' => 'اطلاعات روي پايگاه',
	'inscription' => 'پديره‌نويسي',
	'invalide' => 'بي‌اعتبار',

	// J
	'journal' => 'روزنگار',

	// L
	'le' => 'بر',
	'les_3_dernieres_publications_dans' => 'سه تا از آخرين نشريات',
	'lieu' => 'مكان',
	'lieux' => 'مكان‌ها',
	'limitation' => 'ناديدني كردن تاريخ‌ها بر روي سايت همگاني',
	'limitation_non' => '(» تاريخ‌هاي قابل رؤيت)',
	'limitation_oui' => '(» تاريخ‌هاي غيرقابل‌رؤيت)',
	'liste_des_articles' => 'فهرست مقالات اسپيپ',
	'liste_des_enfants' => 'فهرست بچه‌ها',
	'liste_des_lieux' => 'فهرست مكان‌ها',
	'liste_des_personnes_nees' => 'فهرست افراد متولد شده',
	'liste_des_unions' => 'فهرست ازدواج‌ها',
	'liste_noms' => 'فهرست نام‌ها',
	'liste_patronyme' => 'فهرست نام‌هاي خانوادگي',

	// M
	'mariage' => 'ازدواج',
	'mariage_le' => 'ازدواج',
	'media' => 'رسانه',
	'mere' => 'مادر',
	'mere_inconnu' => 'مادر ناشناخته',
	'metier' => 'شغل ',
	'mettre_jour_base_fichier_gedcom' => 'روزآمدسازي پايگاه با پرونده ژدكام (GedCom) (يك مگابايت حداكثر)',
	'mise_a_jour_liste_eclair' => 'روزآمدسازي فهرست خالي',
	'modifier' => 'اصلاح',
	'modifier_fiche' => 'اصلاح پرونده',
	'mon_pagerank' => 'صفحه‌‌رج من (PageRank) ',
	'multilingue' => 'سايت چندزبانه',

	// N
	'naissance' => 'تولد',
	'nettoyage_champ_nom' => 'پاك كردن ميدان اسم',
	'nettoyage_table_lieux' => 'پاك كردن جدول مكان‌ها',
	'nom' => 'اسم',
	'nombre_de_fiches' => 'تعداد پرونده‌ها',
	'nombre_de_patronymes' => 'تعداد نام‌ها‌ي خانوادگي',
	'note' => 'يادداشت',
	'note_individu' => 'يادداشت فردي',
	'nouvelle_fiche' => 'پرونده جديد',
	'nouvelle_liaison_document_realise' => '«سند» پيوند جديد',
	'nouvelle_union' => 'اتحاد جديد',
	'num_departement' => 'بخش نمره‌ي.',

	// P
	'pagerank_actuel' => 'صفحه‌‌رج فعلي ',
	'parente' => 'رابطه',
	'parents' => 'والدين',
	'patronymes' => 'نام‌هاي خانوادگي',
	'pays' => 'كشور',
	'pere' => 'پدر',
	'pere_inconnu' => 'پدر ناشناخته',
	'photo' => 'عكس',
	'photos' => 'عكس‌ها',
	'portrait' => 'پرتره',
	'poubelle' => 'سطل باطله',
	'prenom' => 'اسم كوچك',
	'pub' => 'آگهي',
	'publication' => 'انتشار',

	// R
	'redacteur' => 'ويراستار',
	'region' => 'منطقه',
	'requete' => 'كاووش',
	'requete_invalide' => 'كاووش نامعتبر',
	'restaure' => 'بازانباشت',
	'restreint' => 'محدود',
	'restriction_acces' => 'محدوديت دسترسي ',
	'retour_fiche' => 'ارجاع پرونده',
	'retour_sur_fiche' => 'ارجاع به پرونده',
	'retour_sur_fiche_sans_enregistrer' => 'ارجاع به پرونده بدون ثبت ',

	// S
	'sexe' => 'جنس',
	'signature' => 'امضاء ',
	'signatures' => 'امصاء‌ها',
	'site_genealogie_genespip' => 'سايت شجره‌نامه‌‌اي با ژن‌اسپيپ (GENESPIP)',
	'source' => 'منبع',
	'statistiques' => 'آمارها',
	'suppression_table_temporaire' => 'حذف جدول موقت',
	'supprimer' => 'حذف ',

	// T
	'table_lieux_cree' => 'جدول مكان‌ها ايجاد شده',
	'tableau_ocupe_a' => 'جدول اشغال شده در ',
	'telecharger' => 'بارگذاري',
	'telecharger_succes' => 'بارگذاري با موفقيت ',
	'theme' => 'پوسته(تم)',
	'themes' => 'پوسته‌‌ها',
	'titre_menu_genespip' => 'ژن‌اسپيپ',

	// U
	'union' => 'اتحادها،‌ ازدواج‌ها',
	'union_fiche_no' => 'پرونده‌ي اتحاد شماره‌ي',

	// V
	'valider' => 'معتبر ',
	'version_base_genespip' => 'نسخه‌ي پايگاه ژن‌اسپيپ ',
	'version_plugin' => 'پلاگين نسخه',
	'version_squelette' => 'نسخه اسكلت',
	'ville' => 'شهر',
	'visiteur' => 'بازديد‌كننده'
);

?>
