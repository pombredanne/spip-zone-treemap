<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/genespip?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_ete_mise_a_la_poubelle' => 'bol hodený do koša',
	'acces_non' => 'Prístupové práva » obmedzené',
	'acces_oui' => 'Prístupové práva » Podrobnosti o súboroch',
	'acces_restreint' => 'Prístup na túto stránku je obmedzený',
	'administrateur' => 'Administrátor',
	'adresse' => 'Adresa',
	'ajout_evenement' => 'Pridať novú udalosť',
	'ajout_lieu' => 'Pridať miesto',
	'ajout_media' => 'Pridať multimédium',
	'ajouter_nouveau_lien_article_spip_existant' => 'Pridať nový odkaz na existujúci článok v SPIPE',
	'annuler' => 'Zrušiť',
	'arborescence_des' => 'Stromová štruktúra',
	'arbre_asc' => 'Spodná časť stromu',
	'article_spip_lie' => 'Odkaz na článok v SPIPE číslo',
	'aucune_fiche_possede_numero' => 'Takéto číslo nemá žiaden záznam ',
	'avec' => 's',

	// B
	'base_contient' => 'Databáza obsahuje',
	'base_genespip' => 'Databáza GeneSpip',

	// C
	'cellules_creees' => 'Bunky vytvorené',
	'cellules_occupees' => 'Bunky obsadené',
	'centans' => 'Zakázať zobrazenie dátumov spred viac ako 100 rokov',
	'charger' => 'Nahrať',
	'choisir' => 'Vybrať',
	'cliquer_ici_pour_lier_article_avec_fiche' => 'Ak chcete pripojiť článok k listu, kliknite sem',
	'confirmer' => 'Potvrdiť',
	'contact' => 'Kontakt',
	'continuer' => 'Pokračovať',
	'creation_annulee' => 'Vytváranie zrušené',
	'creation_fiche' => 'Vytvoriť súbor',
	'creer' => 'Vytvoriť',
	'creer_document' => '"Vytvoriť dokument"',

	// D
	'date' => 'Dátum',
	'debut_gedcom' => 'Spustiť GedCom',
	'deces' => 'Smrť',
	'dep' => 'Okr.',
	'departement' => 'Okres',
	'depose_par' => 'Poslal(a)',
	'derniere_modif_le' => 'Naposledy zmenené',
	'derniere_modification' => 'Posledná zmena súboru',
	'descriptif_cfg' => '<p>Táto zóna sa používa na nastavenie základných premenných zásuvného modulu.</p><p> Na to, aby tento modul správne fungoval, musí byť nainštalovaný spolu so zásuvným modulom cfg</p>',
	'detail_fiche' => 'Podrobnosti o súbore',
	'document' => 'Dokument',
	'documents' => 'Dokumenty',

	// E
	'enfant' => 'dieťa',
	'enfants' => 'Deti',
	'enfants_couple' => 'Deti manželského páru',
	'epouse' => 'manželka',
	'epoux' => 'manžel',
	'est_present_dans_base' => 'existuje v databáze',
	'evenement' => 'Udalosť',
	'evenements' => 'Udalosti',
	'evenements_lies' => 'Prepojené udalosti',
	'export_gedcom' => 'Export v GedCome',
	'export_termine' => 'Export dokončený',
	'exporter' => 'Exportovať',

	// F
	'famille' => 'Rodina',
	'fermer' => 'Zatvoriť',
	'fiche_de' => 'Súbor na',
	'fiche_detail' => 'Podrobnosti o súbore',
	'fiche_document' => 'Dokument',
	'fiche_enfant' => 'Súbor detí',
	'fiche_evt_par_lieu' => 'Súbor udalostí pre miesto',
	'fiche_lieux' => 'Súbor s miestami',
	'fiche_no' => 'Súbor č.',
	'fiche_num' => 'Súbor č.',
	'fiche_parents' => 'Súbor rodičov',
	'fiche_union' => 'Súbor zväzku',
	'fiches' => 'Súbory',
	'fichier' => 'Súbor',
	'fichier_gedcom' => 'Súbor GedComu',

	// G
	'galerie' => 'Galéria',
	'gedcom' => 'GedCom',
	'genealogie' => 'Genealógia (rodokmeň)',
	'genespip' => 'GeneSPIP',

	// I
	'indication_format_photo' => '<br />Maximálna veľkosť 100 kB',
	'individu' => 'samostatný',
	'info_centans' => '<p>Natrvalo zakáže zobrazovanie dátumov spred 100 rokov.<br />Táto možnosť sa použije iba pri návštevníkoch stránky.<br />Ak bola deaktivovaná, zobrazenie sa stále dá obmedziť samostatne pri každom súbore.</p>',
	'info_deces' => 'Údaje o smrti',
	'info_doc' => '<p>Genealogický zásuvný modul.</p> <p>Publikujte svoj rodokmeň na internete pomocou stránky v SPIPe.</p>',
	'info_gedcom_etape1' => 'Váš rodostrom je prázdny – nahrajte súbor GedCom (max. 1 MB) alebo vytvorte nový súbor.<br />',
	'info_gedcom_etape2' => 'Na nahratie súboru GedCom GeneSPIP potrebuje vedieť, ako chcete spracúvať vstup"PLAC"<br />GeneSPIP vybral prvých 5 vstupov "PLAC", vyberte jeden z nich, ktorý bude pre ostatné slúžiť ako referenčný bod',
	'info_gedcom_etape3' => 'Počnúc prvým vstupom vyberte, ako má GeneSPIP každý údaj preložiť',
	'info_mariage' => 'Údaje o manželstve',
	'info_multilingue' => '<p>Na stránke zobraziť viacjazyčnú hlavičku.<br />Na deaktivovanie všetkých nepoužívaných jazykov použite viacjazyčné menu SPIPu</p>',
	'info_naissance' => 'Údaj o narodení',
	'info_pub' => '<p>Príjmy z reklamy sú určené pre autora GeneSPIPu a vy môžete vložiť svoju vlastnú hlavičku s reklamou tak, že upravíte obsah súboru: pub/200x200genespip.html – prihláste sa na odber reklamy to google adsense, ak si chcete vytvoriť vlastnú hlavičku</p>',
	'info_restriction_acces' => '<p>Obmedziť prístup k údajom na liste na verejne prístupnej stránke</p>',
	'info_restriction_acces0' => '<b>0:</b> Vyhradené pre administrátorov',
	'info_restriction_acces1' => '<b>1:</b> Vyhradené pre administrátorov a redaktorov',
	'info_restriction_acces2' => '<b>2:</b> Vyhradené pre administrátorov, redaktorov a zaregistrovaných návštevníkov',
	'info_restriction_acces3' => '<b>3:</b> Žiadne obmedzenia',
	'informations_sur_la_base' => 'Informácie o databáze',
	'inscription' => 'Odoberanie',
	'invalide' => 'Neplatný',

	// J
	'journal' => 'Súbor protokolu',

	// L
	'le' => 'dňa',
	'les_3_dernieres_publications_dans' => '3 najnovšie publikované v',
	'lieu' => 'miesto',
	'lieux' => 'Miesta',
	'limitation' => 'Nezobrazovať dátumy na verejne prístupnej stránke',
	'limitation_non' => '(» Dátumy sú viditeľné)',
	'limitation_oui' => '(» Dátumy sú neviditeľné)',
	'liste_des_articles' => 'Zoznam článkov v SPIPe',
	'liste_des_enfants' => 'Zoznam detí',
	'liste_des_lieux' => 'Zoznam miest',
	'liste_des_personnes_nees' => 'Zoznam ľudí narodených',
	'liste_des_unions' => 'Zoznam zväzkov',
	'liste_noms' => 'Zoznam mien',
	'liste_patronyme' => 'Zoznam priezvisk',

	// M
	'mariage' => 'manželstvo',
	'mariage_le' => 'Dátum sobáša',
	'media' => 'Multimédiá',
	'mere' => 'Matka',
	'mere_inconnu' => 'Matka neznáma',
	'metier' => 'Zamestnanie',
	'mettre_jour_base_fichier_gedcom' => 'Aktualizovať databázu s listom GedCom (maximálne 1 MB)',
	'mise_a_jour_liste_eclair' => 'Aktualizovať čistý zoznam',
	'modifier' => 'Upraviť',
	'modifier_fiche' => 'Upraviť súbor',
	'mon_pagerank' => 'Môj pageRank',
	'multilingue' => 'Viacjazyčná stránka',

	// N
	'naissance' => 'Dátum narodenia',
	'nettoyage_champ_nom' => 'Prázdne pole MENO',
	'nettoyage_table_lieux' => 'Prázdna tabuľka s miestami',
	'nom' => 'Meno',
	'nombre_de_fiches' => 'Počet súborov',
	'nombre_de_patronymes' => 'Počet priezvisk',
	'note' => 'Poznámka',
	'note_individu' => 'Samostatná poznámka',
	'nouvelle_fiche' => 'Nový súbor',
	'nouvelle_liaison_document_realise' => 'Prepojenie s novým dokumentom vytvorené',
	'nouvelle_union' => 'Nový zväzok',
	'num_departement' => 'Dpt. č. (pre Francúzsko)',

	// P
	'pagerank_actuel' => 'Aktuálny PageRank',
	'parente' => 'Vzťah',
	'parents' => 'Rodičia',
	'patronymes' => 'Priezviská',
	'pays' => 'Krajina',
	'pere' => 'Otec',
	'pere_inconnu' => 'Otec neznámy',
	'photo' => 'Fotka',
	'photos' => 'Fotky',
	'portrait' => 'Portrét/Fotografia/Obrázok',
	'poubelle' => 'Kôš',
	'prenom' => 'Krstné mená',
	'pub' => 'Reklama',
	'publication' => 'Publikovanie',

	// R
	'redacteur' => 'Redaktor',
	'region' => 'Región',
	'requete' => 'Požiadavka',
	'requete_invalide' => 'Neplatná požiadavka',
	'restaure' => 'Obnoviť',
	'restreint' => 'Obmedzený',
	'restriction_acces' => 'Obmedzenie prístupu',
	'retour_fiche' => 'Vrátiť súbor',
	'retour_sur_fiche' => 'Návrat k súboru',
	'retour_sur_fiche_sans_enregistrer' => 'Návrat k súboru bez uloženia',

	// S
	'sexe' => 'Pohlavie',
	'signature' => 'Podpis',
	'signatures' => 'Podpisy',
	'site_genealogie_genespip' => 'Genealogická stránka GENESPIP ',
	'source' => 'Zdroj',
	'statistiques' => 'Štatistiky',
	'suppression_table_temporaire' => 'Vymazať dočasnú tabuľku',
	'supprimer' => 'Vymazať',

	// T
	'table_lieux_cree' => 'Tabuľka s miestami vytvorená',
	'tableau_ocupe_a' => 'Tabuľka zaplnená',
	'telecharger' => 'Prenos súboru',
	'telecharger_succes' => 'Súbor bol úspešne prenesený',
	'theme' => 'Skin',
	'themes' => 'Skiny',
	'titre_menu_genespip' => 'GeneSPIP',

	// U
	'union' => 'Zväzky, manželstvá',
	'union_fiche_no' => 'Súbor zväzkov č.',

	// V
	'valider' => 'OK',
	'version_base_genespip' => 'Verzia databázy GeneSPIP ',
	'version_plugin' => 'Verzia zásuvného modulu',
	'version_squelette' => 'Verzia šablóny',
	'ville' => 'Mesto',
	'visiteur' => 'Návštevník'
);

?>
