<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/genespip_2_0/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_ete_mise_a_la_poubelle' => 'a été mise à la poubelle',
	'acces_non' => 'Droits d´accès » restreint',
	'acces_oui' => 'Droits d´accès » détails des fiches',
	'acces_restreint' => 'L´accès à ce site est limité',
	'administrateur' => 'Administrateur',
	'adresse' => 'Adresse',
	'ajout_evenement' => 'Ajouter un évènement',
	'ajout_lieu' => 'Ajouter un lieu',
	'ajout_media' => 'Ajouter un media',
	'ajouter_nouveau_lien_article_spip_existant' => 'Ajouter un nouveau lien vers un article SPIP existant',
	'annuler' => 'Annuler',
	'arborescence_des' => 'Voir l´arborescence des',
	'arbre_asc' => 'Arbre ascendant',
	'article_spip_lie' => 'N° article SPIP lié',
	'aucune_fiche_possede_numero' => 'Aucune fiche possède ce numéro',
	'avec' => 'avec',

	// B
	'base_contient' => 'La base contient',
	'base_genespip' => 'Base GeneSpip',

	// C
	'cellules_creees' => 'Cellules créées',
	'cellules_occupees' => 'Cellules occupées',
	'centans' => 'Interdire l‘affichage des dates de moins de 100 ans',
	'charger' => 'Charger',
	'choisir' => 'Choisir',
	'cliquer_ici_pour_lier_article_avec_fiche' => 'Cliquer ici pour lier l´article à la fiche',
	'confirmer' => 'Confirmer',
	'contact' => 'Contact',
	'continuer' => 'Continuer',
	'creation_annulee' => 'Création annulée',
	'creation_fiche' => 'Création de la fiche',
	'creer' => 'Créer',
	'creer_document' => '›› Créer un document ››',

	// D
	'date' => 'Date',
	'debut_gedcom' => 'Début GedCom',
	'deces' => 'Décès',
	'dep' => 'Dep.',
	'departement' => 'Département',
	'depose_par' => 'Déposé par',
	'derniere_modif_le' => 'Derniére moodif le',
	'derniere_modification' => 'Dernière mise à jour de la fiche',
	'descriptif_cfg' => '<p>Cette zone permet de configurer les variables de base du plugin.</p><p> Pour fonctionner correctement ce plugin doit être installé en parallèle avec le plugin cfg</p>',
	'detail_fiche' => 'Détail fiche',
	'document' => 'Document',
	'documents' => 'Documents',

	// E
	'enfant' => 'enfant',
	'enfants' => 'Enfants',
	'enfants_couple' => 'Enfants du couple',
	'epouse' => 'épouse',
	'epoux' => 'époux',
	'est_present_dans_base' => 'est prèsent dans la base',
	'evenement' => 'Évènement',
	'evenements' => 'Évènements',
	'evenements_lies' => 'Evènements liés',
	'export_gedcom' => 'Export GedCom',
	'export_termine' => 'Export terminé',
	'exporter' => 'Exporter',

	// F
	'famille' => 'Famille',
	'fermer' => 'Fermer',
	'fiche_de' => 'Fiche de',
	'fiche_detail' => 'Fiche détail',
	'fiche_document' => 'Fiche document',
	'fiche_enfant' => 'Fiche enfant',
	'fiche_evt_par_lieu' => 'Fiche évènements par lieu',
	'fiche_lieux' => 'Fiche lieux',
	'fiche_no' => 'Fiche nº',
	'fiche_num' => 'Fiche N°',
	'fiche_parents' => 'Fiche parents',
	'fiche_union' => 'Fiche union',
	'fiches' => 'Fiches',
	'fichier' => 'Fichier',
	'fichier_gedcom' => 'Fichier GedCom',

	// G
	'galerie' => 'Galerie',
	'gedcom' => 'GedCom',
	'genealogie' => 'Généalogie',
	'genespip' => 'GeneSPIP',

	// I
	'indication_format_photo' => '<br />Poid max. 100 ko',
	'individu' => 'individu',
	'info_centans' => '<p>Interdire systématiquement l‘affichage des dates ayant moins de 100 ans.<br />Cette option ne s‘applique que pour les visiteurs<br />Si cette option est désactivée, il est possible de limiter l‘affichage fiche par fiche.</p>',
	'info_deces' => 'Info décés',
	'info_doc' => '<p>Plugin de généalogie.</p> <p>Publier votre généalogie sur internet à partir d´un site SPIP.</p>',
	'info_gedcom_etape1' => 'Votre arbre est vide, importer un fichier GedCom (1 Mo Max) ou créer une nouvelle fiche.<br />',
	'info_gedcom_etape2' => 'Afin d´importer votre fichier GedCom, GeneSPIP a besoin de connaître votre méthode de traitement de l´entrée &quot;PLAC&quot;<br />GeneSPIP a sélectionner les 5 premières entrées &quot;PLAC&quot;, Choisissez en une qui servira de référence pour la suite',
	'info_gedcom_etape3' => 'A partir de l´entrée choisie, sélectionnez pour chaque information la manière dont GeneSPIP doit la traduire',
	'info_mariage' => 'Info mariage',
	'info_multilingue' => '<p>Afficher la bannière multilingue sur le site.<br />Allez dans le menu multilingue de SPIP pour désactiver les langues inutiles</p>',
	'info_naissance' => 'Info naissance',
	'info_pub' => '<p>Les revenus publicitaires sont déstinés au créateur de GeneSPIP, vous pouvez placer votre propre banniere publicitaire en modifiant le contenu du fichier pub/200x200genespip.html, abonnez-vous au programme google adsense si vous souhaitez créer votre banniere</p>',
	'info_restriction_acces' => '<p>Limiter les accès aux détails des fiches sur le site public</p>',
	'info_restriction_acces0' => '<b>0</b> : Limitation aux administrateurs',
	'info_restriction_acces1' => '<b>1</b> : Limitation aux administrateurs et rédacteurs',
	'info_restriction_acces2' => '<b>2</b> : Limitation aux administrateurs, rédacteurs et visiteurs(privée)',
	'info_restriction_acces3' => '<b>3</b> : Aucune limitation',
	'informations_sur_la_base' => 'Informations sur la base',
	'inscription' => 'Inscription',
	'invalide' => 'Invalide',

	// J
	'journal' => 'Journal d‘évènements',

	// L
	'le' => 'le',
	'les_3_dernieres_publications_dans' => 'Les 3 dernières publications dans',
	'lieu' => 'lieu',
	'lieux' => 'Lieux',
	'limitation' => 'Rendre invisible les dates sur le site publique',
	'limitation_non' => '(» Dates visibles)',
	'limitation_oui' => '(» Dates invisibles)',
	'liste_des_articles' => 'Liste des articles SPIP',
	'liste_des_enfants' => 'Liste des enfants',
	'liste_des_lieux' => 'Liste des lieux',
	'liste_des_personnes_nees' => 'Listes des personnes nées',
	'liste_des_unions' => 'Liste des unions',
	'liste_noms' => 'Liste des noms',
	'liste_patronyme' => 'Liste des patronymes',

	// M
	'mariage' => 'mariage',
	'mariage_le' => 'Mariage le',
	'media' => 'Média',
	'mere' => 'Mère',
	'mere_inconnu' => 'Mère inconnu',
	'metier' => 'Métier',
	'mettre_jour_base_fichier_gedcom' => 'Mettre à jour la base avec un fichier GedCom (1 Mo Max)',
	'mise_a_jour_liste_eclair' => 'Mise à jour de la liste éclair',
	'modifier' => 'Modifier',
	'modifier_fiche' => 'Modifier la fiche',
	'mon_pagerank' => 'Mon PageRank',
	'multilingue' => 'Site multilingue',

	// N
	'naissance' => 'Naissance',
	'nettoyage_champ_nom' => 'Nettoyage champ NOM',
	'nettoyage_table_lieux' => 'Nettoyage table LIEUX',
	'nom' => 'Nom',
	'nombre_de_fiches' => 'Nombre de fiches',
	'nombre_de_patronymes' => 'Nombre de patronymes',
	'note' => 'Note',
	'note_individu' => 'Note individu',
	'nouvelle_fiche' => 'Nouvelle fiche',
	'nouvelle_liaison_document_realise' => 'Nouvelle liaison ”document“ réalisée',
	'nouvelle_union' => 'Nouvelle union',
	'num_departement' => 'Nº Dép.',

	// P
	'pagerank_actuel' => 'PageRank actuel',
	'parente' => 'Parenté',
	'parents' => 'Parents',
	'patronymes' => 'Patronymes',
	'pays' => 'Pays',
	'pere' => 'Père',
	'pere_inconnu' => 'Père inconnu',
	'photo' => 'Photo',
	'photos' => 'Photos',
	'portrait' => 'Portrait',
	'poubelle' => 'Poubelle',
	'prenom' => 'Prénom',
	'pub' => 'pub',
	'publication' => 'Publication',

	// R
	'redacteur' => 'Rédacteur',
	'region' => 'Région',
	'requete' => 'Requête',
	'requete_invalide' => 'Requête invalide',
	'restaure' => 'Restaure',
	'restreint' => 'Restreint',
	'restriction_acces' => 'Restriction d´accès',
	'retour_fiche' => 'Retour fiche',
	'retour_sur_fiche' => 'Retour sur la fiche',
	'retour_sur_fiche_sans_enregistrer' => 'Retour sur la fiche sans enregistrer',

	// S
	'sexe' => 'Sexe',
	'signature' => 'Signature',
	'signatures' => 'Signatures',
	'site_genealogie_genespip' => 'Site de généalogie par GENESPIP',
	'source' => 'Source',
	'statistiques' => 'Statistiques',
	'suppression_table_temporaire' => 'Suppresion de la table temporaire',
	'supprimer' => 'Supprimer',

	// T
	'table_lieux_cree' => 'Table des lieux créé',
	'tableau_ocupe_a' => 'Tableau occupé à',
	'telecharger' => 'Télécharger',
	'telecharger_succes' => 'Télécharger avec succés',
	'theme' => 'Thème',
	'themes' => 'Thèmes',
	'titre_menu_genespip' => 'GeneSPIP',

	// U
	'union' => 'Unions, mariages',
	'union_fiche_no' => 'Union fiche n&ordm',

	// V
	'valider' => 'Valider',
	'version_base_genespip' => 'Version Base GeneSPIP',
	'version_plugin' => 'Version Plugin',
	'version_squelette' => 'Version Squelette',
	'ville' => 'Ville',
	'visiteur' => 'Visiteur'
);

?>
