<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_ete_mise_a_la_poubelle' => 'a été mise à la poubelle', # NEW
	'acces_non' => 'Direitos de acesso\\" restrito',
	'acces_oui' => 'Direitos de acesso\\" detalhes das fichas',
	'acces_restreint' => 'O acesso à este sítio é limitado',
	'administrateur' => 'Administrador',
	'adresse' => 'Endereço',
	'ajout_evenement' => 'Acrescentar um acontecimento',
	'ajout_lieu' => 'Ajouter un lieu', # NEW
	'ajout_media' => 'ACRESCENTAR UN MEDIA',
	'ajouter_nouveau_lien_article_spip_existant' => 'Ajouter un nouveau lien vers un article SPIP existant', # NEW
	'annuler' => 'Annuler', # NEW
	'arborescence_des' => 'Ver a arborescencia dos',
	'arbre_asc' => 'Árvore ascendente',
	'article_spip_lie' => 'N° article SPIP lié', # NEW
	'aucune_fiche_possede_numero' => 'Aucune fiche possède ce numéro', # NEW
	'avec' => 'com',

	// B
	'base_contient' => 'La base contient', # NEW
	'base_genespip' => 'Base GeneSpip', # NEW

	// C
	'cellules_creees' => 'Cellules créées', # NEW
	'cellules_occupees' => 'Cellules occupées', # NEW
	'centans' => 'Interdire l‘affichage des dates de moins de 100 ans', # NEW
	'charger' => 'Charger', # NEW
	'choisir' => 'Escolher',
	'cliquer_ici_pour_lier_article_avec_fiche' => 'Cliquer ici pour lier l´article à la fiche', # NEW
	'confirmer' => 'Confirmer', # NEW
	'contact' => 'Contacto',
	'continuer' => 'Continuer', # NEW
	'creation_annulee' => 'Création annulée', # NEW
	'creation_fiche' => 'Création de la fiche', # NEW
	'creer' => 'Criar',
	'creer_document' => '›› Créer un document ››', # NEW

	// D
	'date' => 'Data',
	'debut_gedcom' => 'Début GedCom', # NEW
	'deces' => 'Falecimento',
	'dep' => 'Departamento',
	'departement' => 'Departamento',
	'derniere_modif_le' => 'Derniére moodif le', # NEW
	'derniere_modification' => 'Dernière mise à jour de la fiche', # NEW
	'descriptif_cfg' => '<p>Cette zone permet de configurer les variables de base du plugin.</p><p> Pour fonctionner correctement ce plugin doit être installé en parallèle avec le plugin cfg</p>', # NEW
	'detail_fiche' => 'Détail fiche', # NEW
	'document' => 'Documentos',

	// E
	'enfant' => 'Ele (Ela) tem filhos na base ?',
	'enfants' => 'Filhos',
	'enfants_couple' => 'Filhos do casal',
	'epouse' => 'épouse', # NEW
	'epoux' => 'époux', # NEW
	'est_present_dans_base' => 'est prèsent dans la base', # NEW
	'evenement' => 'Acontecimento',
	'evenements' => 'Acontecimentos',
	'evenements_lies' => 'Evènements liés', # NEW
	'export_gedcom' => 'Export GedCom', # NEW
	'export_termine' => 'Export terminé', # NEW
	'exporter' => 'Exporter', # NEW

	// F
	'famille' => 'família',
	'fermer' => 'Fermer', # NEW
	'fiche_de' => 'Fiche de', # NEW
	'fiche_detail' => 'Fiche détail', # NEW
	'fiche_document' => 'Fiche document', # NEW
	'fiche_enfant' => 'Fiche enfant', # NEW
	'fiche_evt_par_lieu' => 'Fiche évènements par lieu', # NEW
	'fiche_lieux' => 'Fiche lieux', # NEW
	'fiche_no' => 'Fiche nº', # NEW
	'fiche_num' => 'Ficheiro N°',
	'fiche_parents' => 'Fiche parents', # NEW
	'fiche_union' => 'Fiche union', # NEW
	'fiches' => 'Fiches', # NEW
	'fichier' => 'Fichier', # NEW
	'fichier_gedcom' => 'Fichier GedCom', # NEW

	// G
	'galerie' => 'Galeria',
	'gedcom' => 'GedCom', # NEW
	'genealogie' => 'Généalogie', # NEW
	'genespip' => 'GeneSPIP', # NEW

	// I
	'indication_format_photo' => 'Peso max. 100 ko',
	'individu' => 'Indivíduo',
	'info_centans' => '<p>Interdire systématiquement l‘affichage des dates ayant moins de 100 ans.<br />Cette option ne s‘applique que pour les visiteurs<br />Si cette option est désactivée, il est possible de limiter l‘affichage fiche par fiche.</p>', # NEW
	'info_deces' => 'Info décés', # NEW
	'info_doc' => '<p>Plugin de genealogia.</p> <p>Publicar sua genealogia sobre a internet a partir d&amp;acute;un sitio SPIP.</p>',
	'info_gedcom_etape1' => 'A sua árvore está vasia, Importar um ficheiro GedCom (1 Mo Max) ou crear um novo ficheiro.<br />',
	'info_gedcom_etape2' => 'Afim d&amp;acute;importar o seu ficheiro GedCom, GeneSPIP precisa de conhecer seu méthodo de tratamento da entrada escolhida<br />GeneSPIP selecionou as 5 primeiras entradas &amp;quot;PLAC&amp;quot;, Escolher uma de entre elas que servira de referencia para a seguida',
	'info_gedcom_etape3' => 'A partir da entrada escolhida, selecione para cada informaçao a maneira como GeneSPIP deve tradusi-la',
	'info_mariage' => 'Info mariage', # NEW
	'info_multilingue' => '<p>Afficher la bannière multilingue sur le site.<br />Allez dans le menu multilingue de SPIP pour désactiver les langues inutiles</p>', # NEW
	'info_naissance' => 'Info naissance', # NEW
	'info_pub' => '<p>Os rendimentos publicitários são destinadoss au creador de GeneSPIP, você pode por a sua propria bandeira publicitária alterando o conteúdo do ficheiro pub/200x200genespip.html, subscreva ao programa google adsense se desejar crear a sua bandeira</p>',
	'info_restriction_acces' => 'Limitar os acessos de retalhos das fichas sobre o sítio público. Lista das limitações possíveis',
	'info_restriction_acces0' => '0 : Limitação a administradores',
	'info_restriction_acces1' => '1 : Limitação a administradores e editores',
	'info_restriction_acces2' => '2 : Limitação a administradores, a editores e visitantes (privado)',
	'info_restriction_acces3' => '3 : Nenhuma limitação ',
	'informations_sur_la_base' => 'Informações sobre a base',
	'inscription' => 'Inscripção',
	'invalide' => 'Invalide', # NEW

	// J
	'journal' => 'Journal d‘évènements', # NEW

	// L
	'le' => 'o',
	'les_3_dernieres_publications_dans' => 'As 3 ultimas publicações em',
	'lieu' => 'lieu', # NEW
	'lieux' => 'Lugares',
	'limitation' => 'Por invisíveis as datas no sítio público',
	'limitation_non' => '(Datas visíveis)',
	'limitation_oui' => '(Datas invisíveis)',
	'liste_des_articles' => 'Liste des articles SPIP', # NEW
	'liste_des_enfants' => 'Liste des enfants', # NEW
	'liste_des_lieux' => 'Liste des lieux', # NEW
	'liste_des_personnes_nees' => 'Listes des personnes nées', # NEW
	'liste_des_unions' => 'Liste des unions', # NEW
	'liste_noms' => 'Liste des noms', # NEW
	'liste_patronyme' => 'Lista dos patrônimos',

	// M
	'mariage' => 'casamento',
	'mariage_le' => 'União dia',
	'media' => 'Media',
	'mere' => 'Mãe',
	'mere_inconnu' => 'Mère inconnu',
	'metier' => 'Arte',
	'mettre_jour_base_fichier_gedcom' => 'Mettre à jour la base avec un fichier GedCom (1 Mo Max)', # NEW
	'mise_a_jour_liste_eclair' => 'Mise à jour de la liste éclair', # NEW
	'modifier' => 'Modificar',
	'modifier_fiche' => 'Modifier la fiche', # NEW
	'mon_pagerank' => 'Mon PageRank', # NEW
	'multilingue' => 'Site multilingue', # NEW

	// N
	'naissance' => 'Nascença',
	'nettoyage_champ_nom' => 'Nettoyage champ NOM', # NEW
	'nettoyage_table_lieux' => 'Nettoyage table LIEUX', # NEW
	'nom' => 'Nome',
	'nombre_de_fiches' => 'Quantidade de fichas',
	'nombre_de_patronymes' => 'Quantidade de patrônimos',
	'note' => 'Nota',
	'note_individu' => 'Note individu', # NEW
	'nouvelle_fiche' => 'Nouvelle fiche', # NEW
	'nouvelle_liaison_document_realise' => 'Nouvelle liaison ”document“ réalisée', # NEW
	'nouvelle_union' => 'Nouvelle union', # NEW
	'num_departement' => 'Nº Dép.', # NEW

	// P
	'pagerank_actuel' => 'PageRank actuel', # NEW
	'parente' => 'Parenté', # NEW
	'parents' => 'Pais',
	'patronymes' => 'Patrônimos',
	'pays' => 'País',
	'pere' => 'Pai',
	'pere_inconnu' => 'Père inconnu',
	'photo' => 'Foto',
	'photos' => 'Fotos',
	'portrait' => 'Portrait',
	'poubelle' => 'Lixo',
	'prenom' => 'Prenome',
	'pub' => 'Publicidade',
	'publication' => 'Publicação',

	// R
	'redacteur' => 'Editor',
	'region' => 'Região',
	'requete' => 'Requête', # NEW
	'requete_invalide' => 'Requête invalide', # NEW
	'restaure' => 'Restaure',
	'restreint' => 'Restreint', # NEW
	'restriction_acces' => 'Restrição de acesso',
	'retour_fiche' => 'Retour fiche', # NEW
	'retour_sur_fiche' => 'Retour sur la fiche', # NEW
	'retour_sur_fiche_sans_enregistrer' => 'Retour sur la fiche sans enregistrer', # NEW

	// S
	'sexe' => 'Sexo',
	'signature' => 'Assinatura',
	'signatures' => 'Assinaturas',
	'site_genealogie_genespip' => 'Site de généalogie par GENESPIP', # NEW
	'source' => 'Fonte',
	'statistiques' => 'Statistiques', # NEW
	'suppression_table_temporaire' => 'Suppresion de la table temporaire', # NEW
	'supprimer' => 'Suprimir',

	// T
	'table_lieux_cree' => 'Table des lieux créé', # NEW
	'tableau_ocupe_a' => 'Tableau occupé à', # NEW
	'telecharger' => 'Télécharger', # NEW
	'telecharger_succes' => 'Télécharger avec succés', # NEW
	'theme' => 'parâmetros do sítio público',
	'themes' => 'Thèmes', # NEW
	'titre_menu_genespip' => 'GeneSPIP', # NEW

	// U
	'union' => 'Uniões, casamentos',
	'union_fiche_no' => 'Union fiche n&ordm', # NEW

	// V
	'valider' => 'Aceitar',
	'version_base_genespip' => 'Versão Base GeneSPIP',
	'version_plugin' => 'Versão Plugin',
	'version_squelette' => 'Versão Esqueleto',
	'ville' => 'Cidade',
	'visiteur' => 'Visitante'
);

?>
