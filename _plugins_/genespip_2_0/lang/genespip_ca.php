<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/genespip?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_ete_mise_a_la_poubelle' => 's\'ha posat a la paperera',
	'acces_non' => 'Drets d\'accés » restringit',
	'acces_oui' => 'Drets d\'accés» detalls de les fitxes',
	'acces_restreint' => 'L\'accés a aquest lloc està limitat',
	'administrateur' => 'Administrador',
	'adresse' => 'Adreça',
	'ajout_evenement' => 'Afegir un esdeveniment',
	'ajout_lieu' => 'Afegir un lloc',
	'ajout_media' => 'Afegir un mitjà',
	'ajouter_nouveau_lien_article_spip_existant' => 'Afegir un nou enllaç cap a un article SPIP existent',
	'annuler' => 'Anular ',
	'arborescence_des' => 'Veure l\'arbre',
	'arbre_asc' => 'Arbre ascendent',
	'article_spip_lie' => 'Núm. article SPIP lligat',
	'aucune_fiche_possede_numero' => 'Cap fitxa té aquest número',
	'avec' => 'amb',

	// B
	'base_contient' => 'La base conté',
	'base_genespip' => 'Base GeneSpip',

	// C
	'cellules_creees' => 'Cèl·lules creades',
	'cellules_occupees' => 'Cèl·lules ocupades',
	'centans' => 'Prohibir la publicació de dates de menys de 100 anys',
	'charger' => 'Carregar',
	'choisir' => 'Escollir',
	'cliquer_ici_pour_lier_article_avec_fiche' => 'Fer un clic aquí per enllaçar l\'article a la fitxa',
	'confirmer' => 'Confirmar',
	'contact' => 'Contacte',
	'continuer' => 'Continuar',
	'creation_annulee' => 'Creació anul·lada ',
	'creation_fiche' => 'Creació de la fitxa',
	'creer' => 'Crear',
	'creer_document' => '››Crear un document››',

	// D
	'date' => 'Data',
	'debut_gedcom' => 'Inici GedCom',
	'deces' => 'Defunció',
	'dep' => 'Dep.',
	'departement' => 'Departament',
	'depose_par' => 'Déposé par', # NEW
	'derniere_modif_le' => 'Darrera modificació el ',
	'derniere_modification' => 'Darrera actualització de la fitxa',
	'descriptif_cfg' => '<p>Aquesta zona permet configurar les variables de base del connector.</p><p> Per funcionar correctament aquest connector s\'ha d\'instal·lar en paral·lel amb el connector cfg</p>',
	'detail_fiche' => 'Detall fitxa',
	'document' => 'Documents', # MODIF
	'documents' => 'Documents', # NEW

	// E
	'enfant' => 'nen',
	'enfants' => 'Nens',
	'enfants_couple' => 'Nens de la parella',
	'epouse' => 'esposa',
	'epoux' => 'espòs',
	'est_present_dans_base' => 'es troba a la base',
	'evenement' => 'Esdeveniment',
	'evenements' => 'Esdeveniments',
	'evenements_lies' => 'Esdeveniments lligats',
	'export_gedcom' => 'Exportar GedCom',
	'export_termine' => 'Exportació acabada',
	'exporter' => 'Exportar',

	// F
	'famille' => 'Família',
	'fermer' => 'Tancar',
	'fiche_de' => 'Fitxa de',
	'fiche_detail' => 'Fitxa detall',
	'fiche_document' => 'Fitxa document',
	'fiche_enfant' => 'Fitxa nen',
	'fiche_evt_par_lieu' => 'Fitxa esdeveniments per lloc',
	'fiche_lieux' => 'Fitxa llocs',
	'fiche_no' => 'Fitxa núm. ',
	'fiche_num' => 'Fitxa Núm.',
	'fiche_parents' => 'Fitxa pares',
	'fiche_union' => 'Fitxa unió',
	'fiches' => 'Fitxes',
	'fichier' => 'Fitxer',
	'fichier_gedcom' => 'Fitxer GedCom',

	// G
	'galerie' => 'Galeria',
	'gedcom' => 'GedCom',
	'genealogie' => 'Genealogia',
	'genespip' => 'GeneSPIP',

	// I
	'indication_format_photo' => '<br />Pes màxim 100 Kb',
	'individu' => 'individu',
	'info_centans' => '<p>Prohibir sistemàticament la publicació de dates que tinguin menys de 100 anys.<br />Aquesta opció només s\'aplica als visitants.<br />Si aquesta opció està desactivada, és possible limitar la publicació fitxa per fitxa.</p>',
	'info_deces' => 'Info defunció',
	'info_doc' => '<p>Connector de genealogia.</p> <p>Publicar la vostra genealogia a Internet a partir d\'un lloc SPIP.</p>',
	'info_gedcom_etape1' => 'El vostre arbre està buit, importar un fitxer GedCom (1 MB Màx.) o crear una nova fitxa.<br />',
	'info_gedcom_etape2' => 'Per tal d\'importar el vostre fitxer GedCom, GeneSPIP necessita conèixer el vostre mètode de tractament de l\'entrada "PLAC"<br />GeneSPIP a seleccionar les 5 primeres entrades  "PLAC". Escolliu-ne una que servirà de referència per les noves',
	'info_gedcom_etape3' => 'A partir de l\'entrada escollida, seleccioneu per cada informació com GeneSPIP l\'ha de traduir ',
	'info_mariage' => 'Info casament',
	'info_multilingue' => '<p>Mostrar la cibertira multilingüe al lloc.<br />Aneu al menú multilingüe de SPIP per desactivar les llengües inútils</p>',
	'info_naissance' => 'Info naixement',
	'info_pub' => '<p>Els ingressos publicitaris estan destinats al creador de GeneSPIP. Podeu situar la vostra pròpia cibertira publicitària modificant el contingut del fitxer pub/200x200genespip.html. Aboneu-vos al programa de Google AdSense si voleu crear la vostra cibertira</p>',
	'info_restriction_acces' => '<p>Limitar l\'accés als detalls de les fitxes al lloc públic</p>',
	'info_restriction_acces0' => '<b>0</b>: Limitació als administradors',
	'info_restriction_acces1' => '<b>1</b> : Limitació als administradors i redactors',
	'info_restriction_acces2' => '<b>2</b> : Limitació als administradors, redactors i visitants (privat)',
	'info_restriction_acces3' => '<b>3</b>: Cap limitació',
	'informations_sur_la_base' => 'Informacions sobre la base',
	'inscription' => 'Inscripció',
	'invalide' => 'Invàlid',

	// J
	'journal' => 'Diari d\'esdeveniments',

	// L
	'le' => 'el',
	'les_3_dernieres_publications_dans' => 'Les 3 darreres publicacions a',
	'lieu' => 'lloc',
	'lieux' => 'Llocs',
	'limitation' => 'Tornar invisibles les dates al lloc públic',
	'limitation_non' => '(» Dates visibles)',
	'limitation_oui' => '(» Dates invisibles)',
	'liste_des_articles' => 'Llista d\'articles SPIP',
	'liste_des_enfants' => 'Llista dels nens',
	'liste_des_lieux' => 'Llista dels llocs',
	'liste_des_personnes_nees' => 'Llista de les persones nascudes',
	'liste_des_unions' => 'Llista d\'unions',
	'liste_noms' => 'Llista dels noms',
	'liste_patronyme' => 'Llista de cognoms',

	// M
	'mariage' => 'casament',
	'mariage_le' => 'Casament el ',
	'media' => 'Mitjà',
	'mere' => 'Mare',
	'mere_inconnu' => 'Mare desconeguda',
	'metier' => 'Ofici',
	'mettre_jour_base_fichier_gedcom' => 'Actualitzar la base amb un fitxer GedCom (1 MB màx.)',
	'mise_a_jour_liste_eclair' => 'Actualitzar la llista clara',
	'modifier' => 'Modificar',
	'modifier_fiche' => 'Modificar la fitxa',
	'mon_pagerank' => 'El meu PageRank',
	'multilingue' => 'Lloc multilingüe',

	// N
	'naissance' => 'Naixement',
	'nettoyage_champ_nom' => 'Neteja del camp NOM',
	'nettoyage_table_lieux' => 'Neteja de la taula LLOCS',
	'nom' => 'Nom',
	'nombre_de_fiches' => 'Número de fitxes',
	'nombre_de_patronymes' => 'Número de cognoms',
	'note' => 'Nota',
	'note_individu' => 'Nota individu',
	'nouvelle_fiche' => 'Nova fitxa',
	'nouvelle_liaison_document_realise' => 'Nou enllaç ”document“ realitzat',
	'nouvelle_union' => 'Nova unió',
	'num_departement' => 'Núm. Dep.',

	// P
	'pagerank_actuel' => 'PageRank actual',
	'parente' => 'Parentiu',
	'parents' => 'Pares',
	'patronymes' => 'Cognoms',
	'pays' => 'País',
	'pere' => 'Pare',
	'pere_inconnu' => 'Pare desconegut',
	'photo' => 'Fotografia',
	'photos' => 'Fotografies',
	'portrait' => 'Retrat',
	'poubelle' => 'Paperera',
	'prenom' => 'Nom',
	'pub' => 'pub',
	'publication' => 'Publicació',

	// R
	'redacteur' => 'Redactor',
	'region' => 'Regió',
	'requete' => 'Petició',
	'requete_invalide' => 'Petició invàlida',
	'restaure' => 'Restaura',
	'restreint' => 'Restringit',
	'restriction_acces' => 'Accés restringit',
	'retour_fiche' => 'Tornar fitxa',
	'retour_sur_fiche' => 'Tornar a la fitxa',
	'retour_sur_fiche_sans_enregistrer' => 'Tornar a la fitxa sense registrar-se',

	// S
	'sexe' => 'Sexe',
	'signature' => 'Signatura',
	'signatures' => 'Signatures',
	'site_genealogie_genespip' => 'Lloc de genealogia per GENESPIP',
	'source' => 'Font',
	'statistiques' => 'Estadístiques',
	'suppression_table_temporaire' => 'Supressió de la taula temporal',
	'supprimer' => 'Suprimir',

	// T
	'table_lieux_cree' => 'Taula de llocs creats',
	'tableau_ocupe_a' => 'Taula ocupada a',
	'telecharger' => 'Descarregar',
	'telecharger_succes' => 'Descarregar amb èxit',
	'theme' => 'Tema',
	'themes' => 'Temes',
	'titre_menu_genespip' => 'GeneSPIP',

	// U
	'union' => 'Unions, casaments',
	'union_fiche_no' => 'Unió fitxa núm.',

	// V
	'valider' => 'Validar',
	'version_base_genespip' => 'Versió Base GeneSPIP',
	'version_plugin' => 'Versió Connector',
	'version_squelette' => 'Versió Esquelet',
	'ville' => 'Ciutat',
	'visiteur' => 'Visitant'
);

?>
