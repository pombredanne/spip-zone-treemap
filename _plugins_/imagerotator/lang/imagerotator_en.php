<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/cfg/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'aboutflash' => 'Download Flash Player to view this clip',
	'animspeed' => 'Transition speed',
	'align' => 'Align',
	// B
	'backcolor' => 'Background color of the controls',
	'bouton_effacer' => 'Delete',
	// C
	'cfg_title_imagerotator' => 'Image Rotator',
	'cfg_boite_imagerotator' => 'Image Rotator plugin configuration.<br /><br />[Documentation->http://www.longtailvideo.com/players/jw-image-rotator/]',
	'center' => 'centre',
	// D
	'default' => 'Default article',
	'displaywidth' => 'Display width',
	'displayheight' => 'Display height',
	// E
	// F
	'frontcolor' => 'Texts &amp; buttons color of the controls',
	// G
	// H
	'height' => 'Height',
	// I 
	// J
	// K
	// L
	'lightcolor' => 'Rollover color of the controls',
	// M
	// N
	'non' => 'no',
	// O
	'oui' => 'yes',
	'overstretch' => 'Sets how to stretch images to make them fit the display',
	// P
	// Q
	// R
	'rotatetime' => 'Pause between images',
	// S
	'screencolor' => 'Color of the display area',
	'shuffle' => 'Shuffle mode',
	'shownavigation' => 'Show arrows',
	'showdownload' => 'Show download link',
	// T
	'transition' => 'The transition to use between images',
	// U
	// V
	// W
	'width' => 'Width',
	// X
	// Y
	// Z
);

?>
