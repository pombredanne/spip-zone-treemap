<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/cfg/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'aboutflash' => 'T&eacute;l&eacute;chargez Flash Player pour voir cette animation',
	'animspeed' => 'Vitesse de l\'animation',
	'align' => 'Alignement',
	// B
	'backcolor' => 'Couleur du fond des contr&ocirc;les',
	'bouton_effacer' => 'Effacer',
	// C
	'cfg_title_imagerotator' => 'Image Rotator',
	'cfg_boite_imagerotator' => 'Configuration du plugin Image Rotator.<br /><br />[Documentation->http://www.longtailvideo.com/players/jw-image-rotator/]',
	'center' => 'centre',
	// D
	'default' => 'Diaporama par défaut',
	'displaywidth' => 'Largeur d\'affichage',
	'displayheight' => 'Hauteur d\'affichage',
	// E
	// F
	'frontcolor' => 'Couleur des textes &amp; boutons des contr&ocirc;les',
	'fit' => 'adapter',
	// G
	// H
	'height' => 'Hauteur',
	// I 
	// J
	// K
	// L
	'lightcolor' => 'Couleur de survol des contr&ocirc;les',
	'left' => 'gauche',
	// M
	// N
	'none' => 'sans',
	'non' => 'non',
	// O
	'oui' => 'oui',
	'overstretch' => 'Etirer les images',
	// P
	// Q
	// R
	'rotatetime' => 'Pause entre les images',
	'repeat' => 'En boucle',
	'right' => 'droite',
	// S
	'screencolor' => 'Couleur de fond de l\'animation',
	'shuffle' => 'Al&eacute;atoire',
	'shownavigation' => 'Afficher les contr&ocirc;les',
	'showdownload' => 'Afficher un lien de t&eacute;l&eacute;chargement',
	// T
	'transition' => 'Effet de transition',
	// U
	// V
	// W
	'width' => 'Largeur',
	// X
	// Y
	// Z
);

?>
