<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/multilingue?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'afficher_menu_select' => 'Muestra el menú desplegable (select)',

	// M
	'menu_langue' => 'Menú de idioma',
	'multilingue_titre' => 'Sitio web Multilingüe Fácil',

	// P
	'precision_menu_select' => 'Funcionará solo si se usan las inclusiones (INCLURE) originales de este plugin',

	// T
	'titre_page_configurer_multilingue' => 'Configuración de Sitio web Multilingüe Fácil'
);

?>
