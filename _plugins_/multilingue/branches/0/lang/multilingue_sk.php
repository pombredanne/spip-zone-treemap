<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/multilingue?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'afficher_menu_select' => 'Zobraziť v rozbaľovacom menu (select)',

	// M
	'menu_langue' => 'Jazykové menu',
	'multilingue_titre' => 'Jednoduchá viacjazyčná stránka',

	// P
	'precision_menu_select' => 'Nefunguje iba ak nepoužívate originálne oriešky zásuvného modulu',

	// T
	'titre_page_configurer_multilingue' => 'Nastavenia jednoduchej viacjazyčnej stránky'
);

?>
