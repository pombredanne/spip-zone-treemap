<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/multilingue/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'afficher_menu_select' => 'Afficher en menu dropdown (select)',

	// M
	'menu_langue' => 'Menu de langue',
	'multilingue_titre' => 'Site Multilingue Facile',

	// P
	'precision_menu_select' => 'Ne fonctionnerait uniquement si vous utilisez les noisettes originales du plugin',

	// T
	'titre_page_configurer_multilingue' => 'Configuration de Site Multilingue Facile'
);

?>
