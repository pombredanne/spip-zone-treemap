<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/multilingue?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'afficher_menu_select' => 'Show a dropdown menu (select)',

	// M
	'menu_langue' => 'Language selection menu',
	'multilingue_titre' => 'Multilingual Web-site Easy',

	// P
	'precision_menu_select' => 'Will only work if you use the original includes',

	// T
	'titre_page_configurer_multilingue' => 'Configuration of Multilingual Web-site Easy'
);

?>
