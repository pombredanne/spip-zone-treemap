<?php


	$rers_rub_offres = lire_config('rers/rers_rub_offres');
	$rers_rub_demandes = lire_config('rers/rers_rub_demandes');
	$rers_rub_vie = lire_config('rers/rers_rub_vie');

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2009                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/presentation');
include_spip('inc/actions');



// http://doc.spip.org/@exec_articles_dist
function exec_articles_dist()
{
	exec_articles_args(intval(_request('id_article')));
}

// http://doc.spip.org/@exec_articles_args
function exec_articles_args($id_article)
{
	pipeline('exec_init',array('args'=>array('exec'=>'articles','id_article'=>$id_article),'data'=>''));

	$row = sql_fetsel("*", "spip_articles", "id_article=$id_article");

	if (!$row
	OR !autoriser('voir', 'article', $id_article)) {
		include_spip('inc/minipres');
		echo minipres(_T('public:aucun_article'));
	} else {
		$row['titre'] = sinon($row["titre"],_T('info_sans_titre'));

		$res = debut_gauche('accueil',true)
		  .  articles_affiche($id_article, $row, _request('cherche_auteur'), _request('ids'), _request('cherche_mot'), _request('select_groupe'), _request('trad_err'), _request('debut'))
		  . "<br /><br /><div class='centered'>"
		. "</div>"
		. fin_gauche();

		$commencer_page = charger_fonction('commencer_page', 'inc');
		echo $commencer_page("&laquo; ". $row['titre'] ." &raquo;", "naviguer", "articles", $row['id_rubrique']);



		echo debut_grand_cadre(true),
			afficher_hierarchie($row['id_rubrique'],_T('titre_cadre_interieur_rubrique'),$id_article,'article',$row['id_secteur'],($row['statut'] == 'publie')),
			fin_grand_cadre(true),
			$res,
			fin_page();
	}
}

// http://doc.spip.org/@articles_affiche
function articles_affiche($id_article, $row, $cherche_auteur, $ids, $cherche_mot,  $select_groupe, $trad_err, $debut_forum=0, $statut_forum='prive')
{
	$rers_rub_offres = lire_config('rers/rers_rub_offres');
	$rers_rub_demandes = lire_config('rers/rers_rub_demandes');
	$rers_rub_vie = lire_config('rers/rers_rub_vie');
	global $connect_statut; //rers


	global $spip_lang_right, $dir_lang;

	$id_rubrique = $row['id_rubrique'];
	$id_secteur = $row['id_secteur'];
	$statut_article = $row['statut'];
	$titre = $row["titre"];
	$surtitre = $row["surtitre"];
	$soustitre = $row["soustitre"];
	$descriptif = $row["descriptif"];
	$nom_site = $row["nom_site"];
	$url_site = $row["url_site"];
	$texte = $row["texte"];
	$ps = $row["ps"];
	$date = $row["date"];
	$date_redac = $row["date_redac"];
	$extra = $row["extra"];
	$id_trad = $row["id_trad"];

	$virtuel = (strncmp($row["chapo"],'=',1)!==0) ? '' :
		chapo_redirige(substr($row["chapo"], 1));

	$statut_rubrique = autoriser('publierdans', 'rubrique', $id_rubrique);
	$flag_editable = autoriser('modifier', 'article', $id_article);

	// Est-ce que quelqu'un a deja ouvert l'article en edition ?
	if ($flag_editable
	AND $GLOBALS['meta']['articles_modif'] != 'non') {
		include_spip('inc/drapeau_edition');
		$modif = mention_qui_edite($id_article, 'article');
	} else
		$modif = array();


 // chargement prealable des fonctions produisant des formulaires

	$dater = charger_fonction('dater', 'inc');
	$editer_mots = charger_fonction('editer_mots', 'inc');
	$editer_auteurs = charger_fonction('editer_auteurs', 'inc');
	$referencer_traduction = charger_fonction('referencer_traduction', 'inc');
	$discuter = charger_fonction('discuter', 'inc');

	$meme_rubrique = charger_fonction('meme_rubrique', 'inc');
	$iconifier = charger_fonction('iconifier', 'inc');
	$icone = $iconifier('id_article', $id_article,'articles', false, $flag_editable);

	$boite = pipeline ('boite_infos', array('data' => '',
		'args' => array(
			'type'=>'article',
			'id' => $id_article,
			'row' => $row
		)
	));
	$navigation = ""; 

	$navigation .=	  debut_boite_info(true). $boite . fin_boite_info(true) . $icone ;



//rers       suppression du cadre "forum et petition" pour les rédacteurs
if ($connect_statut == '0minirezo') // rers
{ //rers
	$navigation .=	(_INTERFACE_ONGLETS?"":boites_de_config_articles($id_article));
} //RERS


	$navigation .= ($flag_editable ? boite_article_virtuel($id_article, $virtuel):'')
	  . pipeline('affiche_gauche',array('args'=>array('exec'=>'articles','id_article'=>$id_article),'data'=>''));


	$extra = creer_colonne_droite('', true);
//RERS     simplification interface rédacteurs :
//RERS		Cacher la liste (longue) des articles de la même rubrique
if ($connect_statut == '0minirezo') // rers
{
	$extra .= $meme_rubrique($id_rubrique, $id_article, 'article');
}

	$extra .=  pipeline('affiche_droite',array('args'=>array('exec'=>'articles','id_article'=>$id_article),'data'=>''))
	  . debut_droite('',true);

	// affecter les globales dictant les regles de typographie de la langue
	changer_typo($row['lang']);

	$actions =
	  ($flag_editable ? bouton_modifier_articles($id_article, $id_rubrique, $modif, _T('avis_article_modifie', $modif), "article-24.gif", "edit.gif",$spip_lang_right) : "");

	$haut =
		"<div class='bandeau_actions'>$actions</div>".
		(_INTERFACE_ONGLETS?"":"<span $dir_lang class='arial1 spip_medium'><b>" . typo($surtitre) . "</b></span>\n")
		. gros_titre($titre, '' , false)
		. (_INTERFACE_ONGLETS?"":"<span $dir_lang class='arial1 spip_medium'><b>" . typo($soustitre) . "</b></span>\n");

	$onglet_contenu =
	  afficher_corps_articles($id_article,$virtuel,$row);






	$onglet_proprietes = ((!_INTERFACE_ONGLETS) ? "" :"")
	  . $dater($id_article, $flag_editable, $statut_article, 'article', 'articles', $date, $date_redac)
	  . $editer_auteurs('article', $id_article, $flag_editable, $cherche_auteur, $ids);

//rers    (redacteurs) pour les rubriques autres que OFFRES et DEMANDES, cacher l'attribution de mots clé
global $connect_statut; //rers
$rers_rub_offres = lire_config('rers/rers_rub_offres');
$rers_rub_demandes = lire_config('rers/rers_rub_demandes');

if ($connect_statut == '0minirezo' OR ( $connect_statut !== '0minirezo' AND ($id_rubrique == $rers_rub_offres OR $id_rubrique == $rers_rub_demandes) ) ) // rers
{ //rers
	$onglet_proprietes .=   (!$editer_mots ? '' : $editer_mots('article', $id_article, $cherche_mot, $select_groupe, $flag_editable, false, 'articles'));
} //rers

	$onglet_proprietes .=  (!$referencer_traduction ? '' : $referencer_traduction($id_article, $flag_editable, $id_rubrique, $id_trad, $trad_err))
	  . pipeline('affiche_milieu',array('args'=>array('exec'=>'articles','id_article'=>$id_article),'data'=>''))
	  ;





	$onglet_documents = articles_documents('article', $id_article);
	$onglet_interactivite = (_INTERFACE_ONGLETS?boites_de_config_articles($id_article):"");



//rers       supprimer les forums sous les articles (rubriques OFFRES et DEMANDES seulement )
//RERS             dans l'espace privé, malgré l'autorisation de communiquer
//RERS             dans le forum des adhérents et forum des administrateurs
$rers_forum_offres_demandes = lire_config('rers/rers_forum_offres_demandes');
if (  ($id_rubrique !== $rers_rub_offres AND  $id_rubrique !== $rers_rub_demandes)
      OR  	
      (($id_rubrique == $rers_rub_offres OR  $id_rubrique == $rers_rub_demandes) 
			AND $rers_forum_offres_demandes == "on")
   )
{//RERS
	$onglet_discuter = !$statut_forum ? '' : ($discuter($id_article, 'articles', 'id_article', $statut_forum, $debut_forum));
} //RERS

	return
	  $navigation
	  . $extra
	  . "<div class='fiche_objet'>"
	  . $haut
	  . afficher_onglets_pages(
	  	array(
	  	'voir' => _T('onglet_contenu'),
	  	'props' => _T('onglet_proprietes'),
	  	'docs' => _T('onglet_documents'),
	  	'interactivite' => _T('onglet_interactivite'),
	  	'discuter' => _T('onglet_discuter')),
	  	array(
	    'props'=>$onglet_proprietes,
	    'voir'=>$onglet_contenu,
	    'docs'=>$onglet_documents,
	    'interactivite'=>$onglet_interactivite,
	    'discuter'=>_INTERFACE_ONGLETS?$onglet_discuter:""))
	  . "</div>"
	  . (_INTERFACE_ONGLETS?"":$onglet_discuter)
;
}

// http://doc.spip.org/@articles_documents
function articles_documents($type, $id)
{
	global $spip_lang_left, $spip_lang_right;

	// Joindre ?
	if  ($GLOBALS['meta']["documents_$type"]=='non'
	OR !autoriser('joindredocument', $type, $id))
		$res = '';
	else {
		$joindre = charger_fonction('joindre', 'inc');

		$res = $joindre(array(
			'cadre' => 'relief',
			'icone' => 'image-24.gif',
			'fonction' => 'creer.gif',
			'titre' => _T('titre_joindre_document'),
			'script' => 'articles',
			'args' => "id_article=$id",
			'id' => $id,
			'intitule' => _T('info_telecharger_ordinateur'),
			'mode' => 'document',
			'type' => 'article',
			'ancre' => '',
			'id_document' => 0,
			'iframe_script' => generer_url_ecrire("documenter","id_article=$id&type=$type",true)
		));

		// eviter le formulaire upload qui se promene sur la page
		// a cause des position:relative incompris de MSIE
		if ($GLOBALS['browser_name']!='MSIE') {
			$res = "\n<table style='float: $spip_lang_right' width='50%' cellpadding='0' cellspacing='0' border='0'>\n<tr><td style='text-align: $spip_lang_left;'>\n$res</td></tr></table>";
		}

		$res .= http_script('',"async_upload.js")
		  . http_script('$("form.form_upload").async_upload(async_upload_portfolio_documents);');
	}

	$documenter = charger_fonction('documenter', 'inc');

	$flag_editable = autoriser('modifier', $type, $id);

	return "<div id='portfolio'>" . $documenter($id, $type, 'portfolio') . "</div><br />"
	. "<div id='documents'>" . $documenter($id, $type, 'documents') . "</div>"
	. $res;
}

//
// Boites de configuration avancee
//

// http://doc.spip.org/@boites_de_config_articles
function boites_de_config_articles($id_article)
{
	if (autoriser('modererforum', 'article', $id_article)) {
		$regler_moderation = charger_fonction('regler_moderation', 'inc');
		$regler = $regler_moderation($id_article,"articles","id_article=$id_article") . '<br />';
	}

	$petitionner = charger_fonction('petitionner', 'inc');
	$petition = $petitionner($id_article,"articles","id_article=$id_article");

	$masque = $regler . $petition;

	if (!$masque) return '';

	$invite = "<b>"
	. _T('bouton_forum_petition')
	. aide('confforums')
	. "</b>";

	return
		cadre_depliable("forum-interne-24.gif",
		  $invite,
		  true,//$visible = strstr($masque, '<!-- visible -->')
		  $masque,
		  'forumpetition');
}

// http://doc.spip.org/@boite_article_virtuel
function boite_article_virtuel($id_article, $virtuel)
{
	if (!$virtuel
	AND $GLOBALS['meta']['articles_redirection'] != 'oui')
		return '';

	$invite = '<b>'
	._T('bouton_redirection')
	. '</b>'
	. aide ("artvirt");

	$virtualiser = charger_fonction('virtualiser', 'inc');

	return cadre_depliable("site-24.gif",
		$invite,
		$virtuel,
		$virtualiser($id_article, $virtuel, "articles", "id_article=$id_article"),
		'redirection');
}

// http://doc.spip.org/@bouton_modifier_articles
function bouton_modifier_articles($id_article, $id_rubrique, $flag_modif, $mode, $ip, $im, $align='')
{
	if ($flag_modif) {
		return icone_inline(_T('icone_modifier_article'), generer_url_ecrire("articles_edit","id_article=$id_article"), $ip, $im, $align, false)
		. "<span class='arial1 spip_small'>$mode</span>"
		. aide("artmodif");
	}
	else return icone_inline(_T('icone_modifier_article'), generer_url_ecrire("articles_edit","id_article=$id_article"), "article-24.gif", "edit.gif", $align);
}

// http://doc.spip.org/@afficher_corps_articles
function afficher_corps_articles($id_article, $virtuel, $row)
{
	$res = '';


//RERS     Alerte pour la rubrique OFFRES et DEMANDES  si aucun mot clé n'a été choisi
	  $rers_rub_offres = lire_config('rers/rers_rub_offres');
	  $rers_rub_demandes = lire_config('rers/rers_rub_demandes');
	  $id_rubrique = $row['id_rubrique'];
	  $id_article = $row['id_article'];
	$rersb = spip_fetch_array(spip_mysql_query("SELECT id_mot FROM spip_mots_articles WHERE id_article=$id_article"));
	$rersc = $rersb['id_mot'];
	if ($id_rubrique == $rers_rub_offres OR  $id_rubrique == $rers_rub_demandes) {
		if ($rersc){}
		else {
		$res .= "<p class='article_prop'> Pensez à sélectionner le domaine de savoir 
                             correspondant à votre fiche de savoirs. 
				(Une fois ce domaine de savoir choisi et confirmé à l'aide du bouton 'choisir', la couleur orange de la case disparaîtra)</p>";
		}
	}




	if ($row['statut'] == 'prop') {

		$res .= "<p class='article_prop'>"._T('text_article_propose_publication');
		if ($GLOBALS['meta']['forum_prive_objets'] != 'non')
			$res .= ' '._T('text_article_propose_publication_forum');
		$res.= "</p>";


	}

	if ($virtuel) {
		$res .= debut_boite_info(true)
		.  "\n<div style='text-align: center'>"
		. _T('info_renvoi_article')
		. " "
		.  propre("[->$virtuel]")
		. '</div>'
		.  fin_boite_info(true);
	}
	else {
		$type = 'article';
		$id_rubrique = $row['id_rubrique'];
		$contexte = array('id'=>$id_article,'id_rubrique'=>$id_rubrique);
		$fond = recuperer_fond("prive/contenu/$type",$contexte);
		// permettre aux plugin de faire des modifs ou des ajouts
		$fond = pipeline('afficher_contenu_objet',
			array(
			'args'=>array(
				'type'=>$type,
				'id_objet'=>$id_article,
				'contexte'=>$contexte),
			'data'=> ($fond)));
	
		$res .= "<div id='wysiwyg'>$fond</div>";
	}
	return $res;
}

?>
