Permet de gérer des évaluations fines (par exemple des évaluations de sites web, ou des évaluations de photos).
Les critères d'avaluation devraient pouvoir se rajouter dans une table et se modifier depuis l'interface privée.
Puis ils pourraient être sauvegardés.

Par exemple pour les sites :
- ergonomie
- designe
- accessibilité
- rapidité
- iroginalité
- fonctionnalités
- accord / adéquation avec le type de public

Pour des photos :
- originalité du sujet
- émotion ressentie (?)
- qualité de la composition
- exposition
- mise au point
- bougé

Pourrait compléter le plugin "notation" qui ne permet que de mettre une note.