<?php

//  Fichier de langue du plugin « Puce active pour les articles syndiqués»
// Attention aux caractères spéciaux HTML et aux guillements  : 
// é 		->	&eacute;
//l'article	->	l\'article


$GLOBALS[$GLOBALS['idx_lang']] = array(
	
// T
	'texte_statut_synd_dispo' => 'En attente',
	'texte_statut_synd_off' => 'Off',
	'texte_statut_synd_publie' => 'Publi&eacute;',
	'texte_statut_synd_poubelle' => 'Poubelle',

);

?>
