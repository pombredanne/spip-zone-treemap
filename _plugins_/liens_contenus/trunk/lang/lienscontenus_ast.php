<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'alerte_publie_contenant' => '¡Atención, esti conteníu ta espublizáu, pero contien enllaces a conteníos que nun tan!',
	'alerte_publie_contenant_ko' => 'Attention, ce contenu est publié, mais contient des liens vers des contenus qui n\'existent pas !', # NEW
	'aucun_objets_avec_lien_depuis_courant' => 'Esti conteníu nun contién dengún enllaz a otru conteníu.',
	'aucun_objets_avec_lien_vers_courant' => 'Dengún otru conteníu contién un enllaz a esti.',

	// C
	'confirmation_depublication' => '¡Atención, un conteníu espublizáu apunta pa esti, y verase afectáu si lu desespublces!nn¿Seguro que quies camuda-y l\'estau?', # MODIF
	'confirmation_publication' => '¡Atención, un conteníu al que apunta esti nun ta espublizáu!nn¿Seguro que quies camuda-y l\'estau?', # MODIF
	'confirmation_suppression' => '¡Atención, un conteníu espublizáu apunta a esti, y verase afectáu si lu desanicies!nn¿Realmente quies desanicialu?', # MODIF

	// I
	'inexistant' => 'inexistant (@id_objet@)', # NEW
	'information_element_contenu' => '¡Atención, otru conteníu apunta a esti!', # MODIF

	// L
	'legende_liens_faux_objets' => 'Los enllaces en bermeyu y tachaos indiquen conteníos enllazaos que nun esisten.',
	'liens_entre_contenus' => 'Liens entre contenus', # NEW

	// O
	'objets_avec_liens_depuis_courant' => 'Esti conteníu contién enllaces a estos:', # MODIF
	'objets_avec_liens_vers_courant' => 'Estos conteníos contienen enllaces a esti:', # MODIF

	// S
	'statut_poubelle' => 'Na basoria',
	'statut_prepa' => 'En preparación',
	'statut_prop' => 'Propuestu',
	'statut_publie' => 'Espublizáu',
	'statut_refuse' => 'Refugáu',

	// T
	'type_article' => '@titre@ (@id_objet@)', # NEW
	'type_article_inexistant' => 'Article inexistant (@id_objet@)', # NEW
	'type_auteur' => '@titre@', # NEW
	'type_auteur_inexistant' => 'Auteur inexistant (@id_objet@)', # NEW
	'type_breve' => '@titre@ (@id_objet@)', # NEW
	'type_breve_inexistant' => 'Brève inexistante (@id_objet@)', # NEW
	'type_document' => '@titre@ (@id_objet@)', # NEW
	'type_document_inexistant' => 'Document inexistant (@id_objet@)', # NEW
	'type_forum' => '@titre@ (@id_objet@)', # NEW
	'type_forum_inexistant' => 'Message inexistant (@id_objet@)', # NEW
	'type_modele' => 'Modèle "@id_objet@"', # NEW
	'type_modele_inexistant' => 'Modèle inexistant (@id_objet@)', # NEW
	'type_rubrique' => '@titre@ (@id_objet@)', # NEW
	'type_rubrique_inexistant' => 'Rubrique inexistante (@id_objet@)', # NEW
	'type_syndic' => '@titre@ (@id_objet@)', # NEW
	'type_syndic_inexistant' => 'Site inexistant (@id_objet@)' # NEW
);

?>
