<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'alerte_publie_contenant' => 'Atenţie, acest conţinut este publicat, dar are legături către conţinuturi care nu sunt (încă) publicate !',
	'alerte_publie_contenant_ko' => 'Attention, ce contenu est publié, mais contient des liens vers des contenus qui n\'existent pas !', # NEW
	'aucun_objets_avec_lien_depuis_courant' => 'Acest conţinut nu are legături către alte conţinuturi.',
	'aucun_objets_avec_lien_vers_courant' => 'Nici un conţinut nu are legături către acest conţinut.',

	// C
	'confirmation_depublication' => 'Atenţie, un conţinut publicat este legat de acest conţinut, şi va fi impactat dacă de-publicaţi ! Vreţi cu adevărat să schimbaţi status-ul ?', # MODIF
	'confirmation_publication' => 'Atenţie, un conţinut către care vă legaţi nu este publicat. Vreţi cu adevărat să schimbaţi status-ul ?', # MODIF
	'confirmation_suppression' => 'Atenţie, un conţinut publicat este legat de acesta şi va fi impactat dacă îl ştergeţi. Doriţi cu adevărat să îl ştergeţi ?', # MODIF

	// I
	'inexistant' => 'inexistant (@id_objet@)', # NEW
	'information_element_contenu' => 'Atenţie, un alt conţinut indică către acesta !', # MODIF

	// L
	'legende_liens_faux_objets' => 'Legăturile în roşu şi barate indică conţinuturi legate care nu există.',
	'liens_entre_contenus' => 'Liens entre contenus', # NEW

	// O
	'objets_avec_liens_depuis_courant' => 'Acest conţinut are legături către:', # MODIF
	'objets_avec_liens_vers_courant' => 'Aceste conţinuturi au legături către acesta:', # MODIF

	// S
	'statut_poubelle' => 'La coşul de gunoi',
	'statut_prepa' => 'În preparare',
	'statut_prop' => 'Propus',
	'statut_publie' => 'Publicat',
	'statut_refuse' => 'Refuzat',

	// T
	'type_article' => '@titre@ (@id_objet@)', # NEW
	'type_article_inexistant' => 'Article inexistant (@id_objet@)', # NEW
	'type_auteur' => '@titre@', # NEW
	'type_auteur_inexistant' => 'Auteur inexistant (@id_objet@)', # NEW
	'type_breve' => '@titre@ (@id_objet@)', # NEW
	'type_breve_inexistant' => 'Brève inexistante (@id_objet@)', # NEW
	'type_document' => '@titre@ (@id_objet@)', # NEW
	'type_document_inexistant' => 'Document inexistant (@id_objet@)', # NEW
	'type_forum' => '@titre@ (@id_objet@)', # NEW
	'type_forum_inexistant' => 'Message inexistant (@id_objet@)', # NEW
	'type_modele' => 'Modèle "@id_objet@"', # NEW
	'type_modele_inexistant' => 'Modèle inexistant (@id_objet@)', # NEW
	'type_rubrique' => '@titre@ (@id_objet@)', # NEW
	'type_rubrique_inexistant' => 'Rubrique inexistante (@id_objet@)', # NEW
	'type_syndic' => '@titre@ (@id_objet@)', # NEW
	'type_syndic_inexistant' => 'Site inexistant (@id_objet@)' # NEW
);

?>
