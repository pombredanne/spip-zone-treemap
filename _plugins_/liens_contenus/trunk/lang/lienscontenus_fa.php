<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'alerte_publie_contenant' => 'مراقب باشيد، اين مطلب منتشر مي‌شود، اما لينك‌هاي آن به مطالب ديگر نه!',
	'alerte_publie_contenant_ko' => 'Attention, ce contenu est publié, mais contient des liens vers des contenus qui n\'existent pas !', # NEW
	'aucun_objets_avec_lien_depuis_courant' => 'اين مطلب هيچ پيوندي به مطلب ديگر ندارد.',
	'aucun_objets_avec_lien_vers_courant' => 'مطلبي ديگري به اين مطلب پيوند ندارد.',

	// C
	'confirmation_depublication' => 'مراقب باشيد،‌ يك مطلب چاپ شده دست كم يك پيوند به اين مطلب دارد كه اگر وضعيت آن را تغيير دهيد تحت تأثير قرار خواهد گرفت!\\n\\n واقعاً مي‌خواهيد وضعيت آن را تغيير دهيد؟', # MODIF
	'confirmation_publication' => 'مراقب باشيد،‌اين مطلب دست كم به يك مطلب ديگر كه چاپ نشده پيوند دارد!\\n\\n واقعاً مي‌خواهيد وضعيت آن تغيير دهيد؟', # MODIF
	'confirmation_suppression' => 'مراقب باشيد، يك مطلب منتشر شده دست كم يك پيوند به اين يكي دارد و اگر حذفش كنيد متأثر خواهد شد!\\n\\n هنوز مي‌خواهيد حذفش كنيد؟ ', # MODIF

	// I
	'inexistant' => 'inexistant (@id_objet@)', # NEW
	'information_element_contenu' => 'مراقب باشيد، يك مطلب ديگر به اين مطلب پيوند دارد!', # MODIF

	// L
	'legende_liens_faux_objets' => 'پيوند‌هاي قرمز و باستاره پيوند مطلبي‌اند كه ديگر نيست!',
	'liens_entre_contenus' => 'Liens entre contenus', # NEW

	// O
	'objets_avec_liens_depuis_courant' => 'اين مطلب پيوند‌هايي به اين‌ها دارد:', # MODIF
	'objets_avec_liens_vers_courant' => 'اين مطلب‌ها پيوندهايي به اين يكي دارند:‌', # MODIF

	// S
	'statut_poubelle' => 'در سطل باطله',
	'statut_prepa' => 'در دست ويرايش ',
	'statut_prop' => 'پيشنهادي براي بررسي',
	'statut_publie' => 'منتشر شده',
	'statut_refuse' => 'ردي',

	// T
	'type_article' => '@titre@ (@id_objet@)', # NEW
	'type_article_inexistant' => 'Article inexistant (@id_objet@)', # NEW
	'type_auteur' => '@titre@', # NEW
	'type_auteur_inexistant' => 'Auteur inexistant (@id_objet@)', # NEW
	'type_breve' => '@titre@ (@id_objet@)', # NEW
	'type_breve_inexistant' => 'Brève inexistante (@id_objet@)', # NEW
	'type_document' => '@titre@ (@id_objet@)', # NEW
	'type_document_inexistant' => 'Document inexistant (@id_objet@)', # NEW
	'type_forum' => '@titre@ (@id_objet@)', # NEW
	'type_forum_inexistant' => 'Message inexistant (@id_objet@)', # NEW
	'type_modele' => 'Modèle "@id_objet@"', # NEW
	'type_modele_inexistant' => 'Modèle inexistant (@id_objet@)', # NEW
	'type_rubrique' => '@titre@ (@id_objet@)', # NEW
	'type_rubrique_inexistant' => 'Rubrique inexistante (@id_objet@)', # NEW
	'type_syndic' => '@titre@ (@id_objet@)', # NEW
	'type_syndic_inexistant' => 'Site inexistant (@id_objet@)' # NEW
);

?>
