<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'alerte_publie_contenant' => 'Dikkat, bu içerik yayınlanmış ama yayınlanmamış içeriklere bağlantıları var !',
	'alerte_publie_contenant_ko' => 'Attention, ce contenu est publié, mais contient des liens vers des contenus qui n\'existent pas !', # NEW
	'aucun_objets_avec_lien_depuis_courant' => 'Bu içeriğin başka hiçbir içeriğe bağlantısı yok.',
	'aucun_objets_avec_lien_vers_courant' => 'Başka hiçbir içeriğin bu içeriğe doğru bağlantısı yok.',

	// C
	'confirmation_depublication' => 'Dikkat, yayınlanmış bir içeriğin bu içeriğe doğru bir bağlantısı var ve eğer bunu yayından kaldırırsanız etkilenecekir !nnDurumu gerçekten değiştirmek istiyor musunuz ?', # MODIF
	'confirmation_publication' => 'Dikkat, bağlantı kurulan içerik yayınlanmamış !nnDurumu gerçekten değiştirmek istiyor musunuz ?', # MODIF
	'confirmation_suppression' => 'Dikkat, yayunlanmış bir içeriğin bu içeriğe doğru bir bağlantısı var ve eğer bunu yayından kaldırırsanız etkilenecekir!nnGerçekten silmek istiyor musunuz ?', # MODIF

	// I
	'inexistant' => 'inexistant (@id_objet@)', # NEW
	'information_element_contenu' => 'Dikkat, yayınlanmış bir içeriğin bu içeriğe doğru bir bağlantısı var !', # MODIF

	// L
	'legende_liens_faux_objets' => 'Kırmızı ve üstü çizili bağlantılar mevcut olmayan içeriklerdir.',
	'liens_entre_contenus' => 'Liens entre contenus', # NEW

	// O
	'objets_avec_liens_depuis_courant' => 'Bu içeriğin şu içeriklere doğru bağlantıları var :', # MODIF
	'objets_avec_liens_vers_courant' => 'Şu içeriklerin bu içeriğe doğru bağlantıları var :', # MODIF

	// S
	'statut_poubelle' => 'Çöpe',
	'statut_prepa' => 'Hazırlanıyor',
	'statut_prop' => 'Önerildi',
	'statut_publie' => 'Yayınlandı',
	'statut_refuse' => 'Reddedildi',

	// T
	'type_article' => '@titre@ (@id_objet@)', # NEW
	'type_article_inexistant' => 'Article inexistant (@id_objet@)', # NEW
	'type_auteur' => '@titre@', # NEW
	'type_auteur_inexistant' => 'Auteur inexistant (@id_objet@)', # NEW
	'type_breve' => '@titre@ (@id_objet@)', # NEW
	'type_breve_inexistant' => 'Brève inexistante (@id_objet@)', # NEW
	'type_document' => '@titre@ (@id_objet@)', # NEW
	'type_document_inexistant' => 'Document inexistant (@id_objet@)', # NEW
	'type_forum' => '@titre@ (@id_objet@)', # NEW
	'type_forum_inexistant' => 'Message inexistant (@id_objet@)', # NEW
	'type_modele' => 'Modèle "@id_objet@"', # NEW
	'type_modele_inexistant' => 'Modèle inexistant (@id_objet@)', # NEW
	'type_rubrique' => '@titre@ (@id_objet@)', # NEW
	'type_rubrique_inexistant' => 'Rubrique inexistante (@id_objet@)', # NEW
	'type_syndic' => '@titre@ (@id_objet@)', # NEW
	'type_syndic_inexistant' => 'Site inexistant (@id_objet@)' # NEW
);

?>
