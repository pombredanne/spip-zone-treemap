<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/prix?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_prix_ht' => 'Cena bez DPH',
	'label_prix_ttc' => 'Cena s DPH',
	'label_taxes' => 'DPH',
	'label_total_ht' => 'Celkom bez DPH',
	'label_total_ttc' => 'Celkom s DPH',

	// P
	'prix_ht' => '@prix@ bez DPH',
	'prix_ttc' => '@prix@ s DPH'
);

?>
