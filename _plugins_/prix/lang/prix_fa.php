<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/prix?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_prix_ht' => 'Prix HT',
	'label_prix_ttc' => 'Prix TTC',
	'label_taxes' => 'ماليات‌ها',
	'label_total_ht' => 'كلHT',
	'label_total_ttc' => 'كل ماليات گرفته شده (TTC)',

	// P
	'prix_ht' => '@prix@ HT',
	'prix_ttc' => '@prix@ TTC'
);

?>
