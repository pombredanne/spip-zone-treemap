<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/prix/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_prix_ht' => 'Prix HT',
	'label_prix_ttc' => 'Prix TTC',
	'label_taxes' => 'Taxes',
	'label_total_ht' => 'Total HT',
	'label_total_ttc' => 'Total TTC',

	// P
	'prix_ht' => '@prix@ HT',
	'prix_ttc' => '@prix@ TTC'
);

?>
