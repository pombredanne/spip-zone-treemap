<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

//A
//D
'de_l_annee' => 'of the year',
'depuis' => 'from',
//J
'jusqu_a' => 'to',
// L
'les_publications' => 'Publications',
'liste_de_publications' =>'Publications list',
// P
'publications' => 'Publications',
'pour' => 'for',
//R
//T
'tous_les_types_de_doc' => 'All types of documents'

);

?>