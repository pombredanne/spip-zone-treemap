<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

//A
//D
'de_l_annee' => "de l'ann&eacute;e",
'depuis' => 'depuis',
//J
'jusqu_a' => 'jusqu\'&agrave;',
// L
'les_publications' => 'Les publications',
'liste_de_publications' =>'Liste de publications',
// P
'publications' => 'Publications',
'pour' => 'pour',
//R
//T
'tous_les_types_de_doc' => 'Tous les types de documents'
);

?>