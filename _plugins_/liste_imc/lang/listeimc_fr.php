<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
  
	// C
	'cities'=> 'cities.html',

	// D
	'date_modification' => '@fichier@ a &eacute;t&eacute; modifi&eacute; le : @date@ .',  
	'description_plugin' => 'Ce plugin permet de r&eacute;cuperer p&eacute;riodiquement la liste des IMCs
	(Ind&eacute;pendant m&eacute;dia center) au format XML, et de le transformer en code HTML incluable dans le squelette afin de garder une liste &agrave; jour. Ce plugin s\'adresse principalement aux sites Indym&eacute;dia',
  
	// F
	'frequence_generation' => 'la fr&eacute;quence de g&eacute;n&eacute;ration de @fichier@ est : <b>g&eacute;n&eacute;r&eacute; toutes les @frequence@ heures.</b>',
	
	'fichier_generer' => 'Voir le fichier g&eacute;n&eacute;r&eacute;',
	'frequence' => 'Configuration la fr&eacute;quence',
  
	// G
	'gros_titre' => 'Page d\'administration du fichier cities.html',
	'generer_fichier' => 'G&eacute;n&eacute;rer le fichier cities.html maintenant (sans attendre la g&eacute;n&eacute;ration automatique).',
	'generer' => 'G&eacute;n&eacute;rer',
  
	// M 
	'modifier_frequence' => 'Modifier la fr&eacute;quence de g&eacute;n&eacute;ration du fichier cities.html (exprim&eacute;e en heures) :',
  
	// P
	'pas_de_fichier' => 'le fichier cities.html n\'existe pas. Utilisez le formulaire de g&eacute;n&eacute;ration pour le cr&eacute;er.',

	// T
	'titre_fichier' => 'Fichier g&eacute;n&eacute;r&eacute; par le plugin',
	'titre_frequence' => 'Fr&eacute;quence de g&eacute;n&eacute;ration'
  
);

?>