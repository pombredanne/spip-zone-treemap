<?php

/***************************************************************************\
 *  SPIPAL, Utilitaire de paiement en ligne pour SPIP                      *
 *                                                                         *
 *  Copyright (c) 2007 Thierry Schmit                                      *
 *  Copyright (c) 2011 Emmanuel Saint-James                                *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
//a
    'admin_titre_editer'            => 'Editer les versements',
    'admin_params_liste_versements' => 'S&eacute;lectionner les versements &agrave; &eacute;diter',
//c
    'compte_paypal' => 'compte_paypal',
    'configurer_avendre' => 'configurer AVendre',
//d
    'donner' => 'donner',
//e
    'euros'                          => 'euros',
    'exec_articles_titre'            => '&agrave; vendre ?',
    'exec_articles_vendre_ou_non'    => 'cet article est: ',
    'exec_article_a_vendre'          => '&agrave; vendre',
    'exec_article_a_votre_bon_coeur' => '&agrave; votre bon coeur',
    'exec_article_pas_vendre'        => 'gratuit',
//l
    'lang_lang'                      => 'en_US',
//m
    'menu_editer' => '&eacute;diter versements',
//p
    'paiement_paypal'             => 'url de paiment paypal',
//p
    'presentation' => 'Pr&eacute;sentation',
    'prix_unitaire'               => "prix unitaire",
//r
    'retour_paypal' => 'url de retour de paypal',
//s
    'style_paypal'  => 'style de la page PP',
//t
    'th_description'   => 'description',
    'th_prix_unitaire' => 'unit&eacute;',
    'th_qte'           => 'qt&eacute;',
    'th_ref'           => 'ref',
    'th_total'         => 'total',
    'th_tva'           => 'tva',
//v
    'vendre' => 'vendre',
);
?>
