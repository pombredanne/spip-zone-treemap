<?php

// This is a SPIP language file  --  Questo � un file di lingua di  SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'afficher_solution' 	=>'Visualizza la soluzione (in fondo alla pagina)',
'verifier' 		=> 'Controlla',
'solution'		=>'Soluzione',
'aucune_erreur'		=>'Nessun errore',
'titre'		=>'Schema di parole crociate',
'table_summary'		=>'Schema di parole crociate di @largeur@ colonne e @hauteur@ righe',
'ligne'			=>'riga @n@',
'colonne'		=>'colonna @n@',

'horizontalement' 	=> 'Orizzontali:',
'verticalement'		=> 'Verticali:',
'nombre_erreurs'	=> '@err@ errori',
'une_erreur'		=> 'Un errore',
'nombre_vides'		=> '@vid@ caselle bianche',
'une_vide'			=> 'Una casella bianca',
'bravo'			=> 'Bravo!'

);


?>