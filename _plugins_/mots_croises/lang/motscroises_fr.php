<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'afficher_solution' 	=>'Afficher la solution (en bas de page)',
'verifier' 		=> 'V&eacute;rifier',
'solution'		=>'Solution',
'aucune_erreur'		=>'Aucune erreur',
'titre'		=>'Grille de mots-crois&eacute;s',
'table_summary'		=>'Grille de mots-crois&eacute;s de @largeur@ colonnes sur @hauteur@ lignes',
'ligne'			=>'ligne @n@',
'colonne'		=>'colonne @n@',

'horizontalement' 	=> 'Horizontalement&nbsp;:',
'verticalement'		=> 'Verticalement&nbsp;:',
'nombre_erreurs'	=> '@err@ erreurs',
'une_erreur'		=> 'Une erreur',
'nombre_vides'		=> '@vid@ cases vides',
'une_vide'			=> 'Une case vide',
'bravo'			=> 'Bravo !'

);


?>