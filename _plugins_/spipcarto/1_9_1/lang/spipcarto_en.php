<?php
/*****************************************************************************\
* SPIP-CARTO, Solution de partage et d'elaboration d'information 
* (Carto)Graphique sous SPIP
*
* Copyright (c) 2005-2006
*
* Stephane Laurent, Franeois-Xavier Prunayre, Pierre Giraud, Jean-Claude 
* Moissinac et tous les membres du projet SPIP-CARTO V1 (Annie Danzart - Arnaud
* Fontaine - Arnaud Saint Leger - Benoit Veler - Christine Potier - Christophe 
* Betin - Daniel Faivre - David Delon - David Jonglez - Eric Guichard - Jacques
* Chatignoux - Julien Custot - Laurent Jegou - Mathieu Gehin - Michel Briand - 
* Mose - Olivier Frerot - Philippe Fournel - Thierry Joliveau)
* 
* voir : http://www.geolibre.net/article.php3?id_article=16
*
* Ce programme est un logiciel libre distribue sous licence GNU/GPL. 
* Pour plus de details voir le fichier COPYING.txt ou leaide en ligne.
* 
e -
This program is free software ; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation ; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY ; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program (COPYING.txt) ; if not, write to
the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
or check http://www.gnu.org/copyleft/gpl.html
e -
*
\***************************************************************************/
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'calque' => 'Layers ',

'cartes' => 'Maps',
'cartes_toutes' => 'All maps',
'carte_creer' => 'Create a new map',
'carte_nouvelle' => 'New map',
'carte_numero' => 'Map identifier :', 
'carte_apercu' => 'Map overview', 
'carte_map' => 'Image', 
'carte_lien' => 'Image with link',
'carte_logo' => 'Image with symbols',
'carte_svg' => 'SVG simple',
'carte_svgx' => 'SVG with zoom',
'carte_geosvgwms' => 'SVG with zoom and webservices (WMS)',
'carte_warning' => 'Warning :',
'carte_supp' => 'Remove current map', 
'carte_supp_confirm' => 'Are you sure to remove this map ?',
'carte_articles_use' => 'Articles using this map',
'carte_titre' => 'Map title', 
'carte_fond' => 'Background image',
'carte_fond_numero' => '(Spip image identifier)',
'carte_callage' => 'Bounding box', 
'carte_srs' => 'Coordinate system', 
'carte_objets' => 'Objects',
'carte_import' => 'You can import, modify or delete map objects.',
'carte_import_objet' => 'Import objects into current map',
'carte_supp_objets' => 'Remove all objects',
'carte_insert' => 'Insert a map',
'carte_insert_texte' => 'You can insert maps into your articles.<br/>Set focus on text, select a map in the list below and clic the shortcut to insert in your text.',
'carte_raccourci' => 'Copy the shortcut into your article to insert this form.',
'carte_loading' => 'Loading message',
'carte_point' => 'Point',
'carte_line' => 'Line',
'carte_polygon' => 'Polygon',
'carte_draw' => 'Draw ',
'config' => 'Spipcarto configuration ',
'configuration' => 'configuration',
'import_objet_sel_texte' => 'Link columns from your text file to object fields.',
'import_objet_sel_gpx' => 'Link columns from your GPX file to object fields.',
'import_texte' => 'Import text file (CSV)',
'import_no_fichier' => 'There\'s no file in the folder upload (use FTP).',
'import_gpx' => 'Import GPS file (GPX)',
'import_warning' => 'Warning, objects that you\'re going to import into your map have to be in the same coordinate system !',

'objet_nouvel' => 'New object',
'objet_nouvel_defaut' => '(\'New\' by default)',
'objet_add' => 'Add object', 
'objet_titre' => 'Title',
'objet_lien' => 'Hyperlink',
'objet_lien_default' => '(null by default)',
'objet_logo' => 'Symbol', 
'objet_geom' => 'Geometry', 
'objet_supp' => 'Remove object',
'objet_x' => 'X ',
'objet_y' => 'Y ',
'objet_coord_default' => '(0 by default)',
'objet_import' => 'Import objects.',
'objet_import_nombre' => 'Number of imported objects added to your map :'

);

?>