<?php
/*****************************************************************************\
* SPIP-CARTO, Solution de partage et d'elaboration d'information 
* (Carto)Graphique sous SPIP
*
* Copyright (c) 2005-2006
*
* Stephane Laurent, Franeois-Xavier Prunayre, Pierre Giraud, Jean-Claude 
* Moissinac et tous les membres du projet SPIP-CARTO V1 (Annie Danzart - Arnaud
* Fontaine - Arnaud Saint Leger - Benoit Veler - Christine Potier - Christophe 
* Betin - Daniel Faivre - David Delon - David Jonglez - Eric Guichard - Jacques
* Chatignoux - Julien Custot - Laurent Jegou - Mathieu Gehin - Michel Briand - 
* Mose - Olivier Frerot - Philippe Fournel - Thierry Joliveau)
* 
* voir : http://www.geolibre.net/article.php3?id_article=16
*
* Ce programme est un logiciel libre distribue sous licence GNU/GPL. 
* Pour plus de details voir le fichier COPYING.txt ou leaide en ligne.
* 
e -
This program is free software ; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation ; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY ; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program (COPYING.txt) ; if not, write to
the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
or check http://www.gnu.org/copyleft/gpl.html
e -
*
\***************************************************************************/
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'calque' => 'Calques ',

'cartes' => 'Cartes',
'cartes_toutes' => 'Toutes les cartes',
'carte_creer' => 'Cr&eacute;er une nouvelle carte',
'carte_nouvelle' => 'Nouvelle carte',
'carte_numero' => 'CARTE NUM&Eacute;RO :', 
'carte_apercu' => 'Apercu de la carte', 
'carte_map' => 'Image simple', 
'carte_lien' => 'Image avec liens',
'carte_logo' => 'Image avec logos',
'carte_svg' => 'SVG simple',
'carte_svgx' => 'SVG avec zoom',
'carte_geosvgwms' => 'SVG avec zoom et services web (WMS)',
'carte_warning' => 'Attention :',
'carte_supp' => 'Supprimer cette carte', 
'carte_supp_confirm' => 'Voulez-vous vraiment supprimer cette carte ?',
'carte_articles_use' => 'Articles utilisant cette carte',
'carte_titre' => 'Titre de la carte', 
'carte_fond' => 'Fond de carte',
'carte_fond_numero' => '(Num&eacute;ro de l\'image Spip)',
'carte_callage' => 'Callage de la carte', 
'carte_srs' => 'Syst&egrave;me de coordonn&eacute;es', 
'carte_objets' => 'Objets de la carte',
'carte_import' => 'Vous pouvez ici importer, modifier et supprimer les objets de la carte.',
'carte_import_objet' => 'Importer des objets dans la carte',
'carte_supp_objets' => 'Supprimer tous les objets de la carte',
'carte_dupl' => 'Dupliquer cette carte',
'carte_insert' => 'Ins&eacute;rer une carte',
'carte_insert_texte' => 'Vous pouvez ins&eacute;rer des cartes dans vos articles.<br/>Positionnez votre curseur dans le texte de l\'article, choisissez une carte dans la liste ci-dessous et cliquez sur un raccourci pour l\'ins&eacute;rer.',
'carte_raccourci' => 'Recopiez ce raccourci dans le texte de l\'article pour ins&eacute;rer ce formulaire.',
'carte_loading' => 'Chargement de la carte',
'carte_point' => 'Point',
'carte_line' => 'Ligne',
'carte_polygon' => 'Polygone',
'carte_draw' => 'Dessiner un objet',
'config' => 'Configuration de Spipcarto ',
'configuration' => 'configuration',
'import_objet_sel_texte' => 'S&eacute;lectionner les champs du fichier texte &agrave; importer dans les objets de votre carte.',
'import_objet_sel_gpx' => 'S&eacute;lectionner les champs du fichier GPX &agrave; importer dans les objets de votre carte.',
'import_texte' => 'Import fichier texte (CSV)',
'import_no_fichier' => 'Il n\'y a pas de fichier dans le r&eacute;pertoire upload (Utilisez le FTP).',
'import_gpx' => 'Import fichier GPS (GPX)',
'import_warning' => 'Attention, les objets que vous allez associer &agrave; la carte doivent etre dans la meme projection !',

'objet_nouvel' => 'Nouvel objet',
'objet_nouvel_defaut' => '(\'Nouvel Objet\' par d&eacute;faut)',
'objet_add' => 'Ajouter un objet', 
'objet_titre' => 'Titre de l\'objet',
'objet_lien' => 'Lien de l\'objet',
'objet_lien_default' => '(null par d&eacute;faut)',
'objet_logo' => 'Logo de l\'objet', 
'objet_geom' => 'G&eacute;ometrie', 
'objet_supp' => 'Supprimer cet objet',
'objet_supp_confirm' => 'Voulez-vous vraiment supprimer tous les objets de cette carte ?',
'objet_x' => 'Coordonn&eacute;es X ',
'objet_y' => 'Coordonn&eacute;es Y ',
'objet_coord_default' => '(0 par d&eacute;faut)',
'objet_import' => 'Importation des objets.',
'objet_import_nombre' => 'Nombre d\'objets ajout&eacute;s &agrave; votre carte :'
);


?>