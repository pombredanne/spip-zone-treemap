Auteur: Stephane LAURENT (Bill) bill@adequates.com
Licence: GPL

Description:

[fr]
version plugin de spipcarto
Installation:
- copier le repertoire dans le repertoire plugin de spip
- deplacer les fichier du repertoire /racine_spip a la racine du site
- activer le plugin
- configurer le plugin (installation en base et metas)


[en] ... 

Listes des fichiers:

[fr]
README.txt vous y etes,
_REGLES_DE_COMMIT.txt regles pour faire evoluer cette contrib,
TODO.txt ce qu'il reste a faire,
BUG.txt les bugs connus.

[en]
README.txt you are here <-,
_REGLES_DE_COMMIT.txt rules of contribution on this project,
TODO.txt what remains to do,
BUG.txt the known bugs.

http://www.geolibre.net/article.php3?id_article=16

