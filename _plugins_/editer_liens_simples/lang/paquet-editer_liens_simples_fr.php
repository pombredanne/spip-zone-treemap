<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
// E
	'editer_liens_simples_nom' => 'Éditer Liens Simples',
	'editer_liens_simples_description' => 'Fourni un formulaire et une API pour lier les objets
		sur les tables spip_xxx_yyy (et non spip_xxx_liens), directement inspiré de ce qui est fourni par SPIP
		pour ses tables de liens.',
	'editer_liens_simples_slogan' => 'API de liens pour les tables spip_xxx_yyy',
);

?>
