<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(
	'titre_page_kconf' => 'Configurer kconf',
	'titre_configuration_kconf' => 'Configuration de kconf',
	'titre_page_configuration' => 'Changer la configuration générale',
	'titre_configuration' => 'Configuration générale du site',
	'onglet_rubrique' => 'Rubriques',
	'onglet_article' => 'Articles',
	'configurer' => 'Configurer',
	'formulaire' => 'Formulaire de saisie',
	'icone_menu' => 'Configuration générale du site',
	//
	'manque_squelette' => 'Veuillez mettre des squelettes<br/> dans le dossier @dir@,<br/> et au besoin le créer.',
	'bouton_kconf' => 'Kconf',
	'bouton_kconf_admin' => 'Admin de Kconf',
	'public' => 'Branche: @objet@ et toute sa hiérarchie.',
	'protege' => 'Enfants: @objet@ et ses enfants.',
	'prive' => '@objet@: uniquement ici.',
	'change' => 'Valider',
	'nom_fichier' => 'Squelette',
	'actif' => 'Activé',
	'scope' => 'Portée',
	'probleme_dossier' => 'le dossier "@dossier@" n\'existe pas, il vous faut le créer',
	'pas_de_squelette' => 'le dossier "@dossier@" ne contient aucun squelette...',
);
?>