<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	//C
	'chemin_base_bucket' => 'Optionnel : le chemin de base &#224; l\'int&#233;rieur du bucket',
	'cle_publuque' => 'Votre clé publique Amazon S3',
	'cle_secrete' => 'Votre cl&#233; secr&#232;te Amazon S3',

	// E
	'envoyer_explication' => 'Pour envoyer vos fichiers sur Amazon S3 veuillez indiquer ci-dessous',
	'envoyer_s3' => 'Envoyer sur @provider@',	 
		
	//R
	'registre_cname_erreur' => 'n\'a pas d\'enregistrement',
		
	'url_s3' => 'L\'URL de vos fichiers',
	
);
?>
