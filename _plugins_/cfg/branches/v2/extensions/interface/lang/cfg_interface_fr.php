<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/_stable_/cfg/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configuration_cfg' => 'Configuration des extensions',
	'choisir_module_a_configurer' => 'Choisissez le module &agrave; configurer.',
	'configuration_modules' => 'Configuration des modules',
	'configurer' => 'Configurer',

	// D
	'description_cfg' => 'CFG facilite la cr&eacute;ation d\'interface de configuration pour les extensions de SPIP.',
	
	// E
	'erreur_compatibilite' => 'Installez le plugin de compatibilite (cfg_compat) pour lire cet ancien format de configuration CFG.',
);

?>
