<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/_stable_/cfg/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configuration_cfg' => 'Extensions configuration',
	'choisir_module_a_configurer' => 'Choose the module to configure.',
	'configuration_modules' => 'Modules configuration',
	'configurer' => 'Configure',

	// D
	'description_cfg' => 'CFG facilitates the creation of configuration interface for the extensions of SPIP.',
	
	// E
	'erreur_compatibilite' => 'Please install the compatibility plugin (cfg_compat) to read this old configuration format of CFG.',
);

?>
