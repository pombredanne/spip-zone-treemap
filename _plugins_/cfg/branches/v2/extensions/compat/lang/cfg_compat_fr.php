<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/_stable_/cfg/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_copie_fichier' => 'Impossible de copier le fichier @fichier@ &agrave; son emplacement d&eacute;finitif',
	'erreur_suppression_fichier' => 'Impossible de supprimer le fichier @fichier@.',
	'erreur_type_id' => 'Le champ @champ@ doit commencer par une lettre ou un soulign&eacute;',
	'erreur_type_idnum' => 'Le champ @champ@ doit &ecirc;tre numerique',
	'erreur_type_pwd' => 'Le champ @champ@ doit avoir plus de 5 caract&egrave;res',

	// N
	'nouveau' => 'Nouveau',
);

?>
