<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/_stable_/cfg/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_copie_fichier' => 'Impossible to copy the file @fichier@ to its final location',
	'erreur_suppression_fichier' => 'Impossible to delete the file @fichier@.',
	'erreur_type_id' => 'The @champ@ field must begin with a letter or an underscore',
	'erreur_type_idnum' => 'The @champ@ field should be digits',
	'erreur_type_pwd' => 'The @champ@ field  must be over 5 characters',

	// N
	'nouveau' => 'New',
);

?>
