<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(
	'titre_multidomaines' => 'Multidomaines',
	'label_url' => 'Url du secteur',
	'precisions_url' => "l'url doit être valide,ne contenir qu'un domaine et se terminer par un slash. ex: https://sous-dmomaine.domaine.com/",
	'label_squelette' => 'Dossier des squelettes',
	'precisions_squelette' => 'ex : squelettes',
	'label_editer_url' => 'Url par defaut',
	'precisions_editer_url' => "l'url doit être valide, complète et se terminer par un slash<br /> ex: https://sous-dmomaine.domaine.com/",
);
?>
