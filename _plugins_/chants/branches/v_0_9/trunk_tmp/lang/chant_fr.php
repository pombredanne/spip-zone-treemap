<?php
if (!defined('_ECRIRE_INC_VERSION')) return;
$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'aucun_chant' => 'Il n\'y a pas de chant à cette adresse',

	// C
	'chant' => 'Chant :',
	'chants' => 'Chants :',
	'chants_rubrique' => 'Chants de cette rubrique',
	
	// D
	'date_annee' => 'Année de sortie',
	
	// E
	'exporter' => 'Exporter le chant',
	'exporter_opensong' => 'Exporter le chant au format OpenSong',
	
	// I
	'icone_creer_chant' => "Ajouter un chant",
	'icone_ecrire_chant' => 'Écrire un nouveau chant',
	'icone_modifier_chant' => "Modifier ce chant",
	'icone_retour_chant' => 'Retour au chant',
	'info_aucun_chant' => "Aucun chant",
	'info_1_chant' => "Un chant",
	'info_nb_chants' => "@nb@ chants",
	'info_nombre_hymne' => 'Numéro',
	'info_numero_chant' => 'CHANT NUMÉRO :',
	'infos_complementaires' => 'Informations complémentaires',
	
	// L
	'label_alias' => 'Alias :',
	'label_capo' => 'Capo (de 1 à 5) :',
	'label_ccli' => 'N° CCLI :',
	'label_copyright' => 'Copyright :',
	'label_date_annee' => 'Année de sortie :',
	'label_ligne_principale' => 'Ligne principale :',
	'label_numero' => 'Numéro :',
	'label_paroles' => 'Paroles du chant :',
	'label_presentation' => 'Ordre des sections :',
	'label_signature' => 'Mesure :',
	'label_tempo' => 'Tempo :',
	'label_titre' => 'Titre :',
	'label_tonalite' => 'Tonalité',
	'logo_chant' => 'Logo du chant',
	
	// T
	'text_chant_propose_publication' => 'Chant proposé pour la publication.',
	'texte_alias' => 'Alias',
	'texte_changer_statut' => 'Ce chant est :',
	'texte_copyright' => 'Copyright',
	'texte_date_annee' => 'Année de sortie',
	'texte_travail_chant' => '@nom_auteur_modif@ a travaillé sur ce chant il y a @date_diff@ minutes',
	'titre_chant' => 'Chant',
	'titre_chants' => 'Chants',
	'titre_chants_rubrique' => 'Chants de la rubrique',
	'titre_langue_chant' => 'Langue du chant',
	'titre_logo_chant' => ' Logo du chant',
)

?>