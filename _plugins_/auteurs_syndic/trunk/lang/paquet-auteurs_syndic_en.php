<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-auteurs_syndic?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auteurs_syndic_description' => 'Adds the ability to add authors to syndicated sites',
	'auteurs_syndic_nom' => 'Authors for Web site',
	'auteurs_syndic_slogan' => 'Adds the ability to add authors to syndicated sites'
);

?>
