<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/auteurs_syndic/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auteurs_syndic_description' => 'Ajoute la possibilité d\'ajouter des auteurs aux sites syndiqués',
	'auteurs_syndic_nom' => 'Auteurs pour les sites syndiqués',
	'auteurs_syndic_slogan' => 'Ajoute la possibilité d\'ajouter des auteurs aux sites syndiqués'
);

?>
