<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-auteurs_syndic?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auteurs_syndic_description' => 'Umožňuje k syndikovaným stránkam pridávať autorov',
	'auteurs_syndic_nom' => 'Autori na syndikované stránky',
	'auteurs_syndic_slogan' => 'Umožňuje k syndikovaným stránkam pridávať autorov'
);

?>
