<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-auteurs_syndic?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auteurs_syndic_description' => 'Añade la posibilidad de agregar autores a los sitios sindicados',
	'auteurs_syndic_nom' => 'Autores para los sitios sindicados',
	'auteurs_syndic_slogan' => 'Añade la posibilidad de agregar autores a los sitios sindicados'
);

?>
