<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
									   'titre' => 'Gestion des types de documents',
									   'titre_page' => 'Types des documents',
									   'gros_titre' => 'Gerer les types de documents telechargeables',
									   'help' => 'Par d&eacute;faut, SPIP permet d\'associer que certain type de documents aux articles et rubriques. Tous les autres sont automatiquement mis dans un fichier zip pour des question de s&eacutes;curit&eacute;.

Cette page permet d\'ajouter des types de documents permis, de changer leur logo par d&eacute;faut, etc...',
									   'type' => 'Type',
									   'extension' => 'Extension',
									   'description' => 'Description',
									   'permission' => 'Permis?',
									   'mime' => 'Mime Type',
									   'nombre_documents' => 'Nombre de Documents',
									   'effacer' => 'Effacer',
									   'non' => 'non',
									   'embed' => 'embed',
									   'image' => 'image',
									   'inclus' => 'inclus'
);
?>
