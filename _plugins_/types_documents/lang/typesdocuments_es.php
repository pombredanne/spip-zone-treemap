<?php


$GLOBALS[$GLOBALS['idx_lang']] = array(
									   'titre' => 'Gesti&oacute; de los tipos de documentos',
									   'titre_page' => 'Tipos de documentos',
									   'gros_titre' => 'Administrar los tipos de documentos
admitidos para descarga',
									   'help' => 'De forma predeterminada, SPIP permite asociar
ciertos tipos de documentos en los art&iacute;culos y secciones. El resto de
documentos son colocados como archivos comprimidos (zip) por motivos de
seguridad.
		 
Esta p&aacute;gina permite a&ntilde;adir los tipos de documentos admisibles,
cambiar su logotipo predeterminado, etc...',
									   'type' => 'Tipo',
									   'extension' => 'Extensi&oacute;n',
									   'description' => 'Descripci&oacute;n',
									   'permission' => '&iquest;Permitido?',
									   'mime' => 'Tipo Mime',
									   'nombre_documents' => 'N&uacute;mero de Documentos',
									   'effacer' => 'Borrar',
									   'non' => 'no',
									   'embed' => 'integrado',
									   'image' => 'imagen',
									   'inclus' => 'incluidos'
									   );

?>
