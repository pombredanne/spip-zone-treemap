<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
									   'titre' => 'Gestion of document types',
									   'titre_page' => 'Documents types',
									   'gros_titre' => 'Manage uploadable document types',
									   'help' => 'By default, SPIP only allow a limited number of document types to be attached to articles or sections. The other ones being directly put into a zip archive for security.

This plugin allows you to manage the allowed types.',
									   'type' => 'Type',
									   'extension' => 'Extension',
									   'description' => 'Description',
									   'permission' => 'Allowed?',
									   'mime' => 'Mime Type',
									   'nombre_documents' => 'Number of Documents',
									   'effacer' => 'Remove',
									   'non' => 'no',
									   'embed' => 'embed',
									   'image' => 'image',
'inclus' => 'included'
);
?>
