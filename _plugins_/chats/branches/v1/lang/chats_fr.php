<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'action_ajouter' => 'Cr&eacute;er un chat',
	'action_modifier' => 'Modifier',
	'action_supprimer' => 'Supprimer',
	'action_voir' => 'Voir',
	
	// C
	'chats' => 'Chats',

	// D
	'description_chats' => 'Un chat est un petit animal poilu
		qui passe beaucoup de temps &agrave; dormir &ndash; et le reste
		du temps &agrave; manger&hellip;
		En g&eacute;n&eacute;ral, il poss&egrave;de 4 pattes
		pourvues de puissantes griffes douloureuses,
		miaule ou ronronne et aime particuli&egrave;rement
		faire souffrir les souris.',

	// E
	'explication_infos' => "
		D&eacute;crire le chat, son comportement, son histoire, des anecdotes,
		ses passe-temps, ses go&ucirc;ts&hellip;
	",
	
	// I
	'info_gauche_numero_chat' => 'Chat num&eacute;ro :',
	'info_modifier_chat' => 'Modifier un chat :',
	
	// L
	'label_actions' => 'Actions',
	'label_id' => 'Id',
	'label_infos' => 'Informations',
	'label_nom' => 'Nom',
	'label_race' => 'Race',
	'label_robe' => 'Robe',
	'label_annee_naissance' => 'Ann&eacute;e de naissance',
	'label_age' => '&Acirc;ge',
	'liste_des_chats' => 'Liste des chats',

	// N
	'nb_ans' => '@nb@ ans',

	// O
	'objet_chats' => 'Chats',
	
	// T
	'texte_nouveau_chat' => 'Sans nom',
	
);

?>
