<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'icone_creer_chat' => "Créer un chat",
	'icone_modifier_chat' => "Modifier ce chat",
	'info_aucun_chat' => "Aucun chat",
	'info_1_chat' => "Un chat",
	'info_nb_chats' => "@nb@ chats",


	
	// L
	'label_nom' => "Nom",
	'label_race' => "Race",
	'label_robe' => "Robe",
	'label_infos' => "Informations",
	'lien_ajouter_chat' => "Ajouter ce chat",
	'lien_retirer_chat' => "Retirer ce chat",
	'lien_retirer_chats' => "Retirer tous les chats",

	
	// T
	'titre_chat' => "Chat",
	'titre_chats' => "Chats",
	'titre_chats_rubrique' => "Chats de la rubrique",
	'titre_langue_chat' => "Langue de ce chat",
	'titre_logo_chat' => "Logo du chat",
	'texte_ajouter_chat' => "Ajouter un chat",
	'texte_changer_statut' => "Ce chat est :"


);

?>
