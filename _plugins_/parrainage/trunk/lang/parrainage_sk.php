<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/parrainage?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_filleul_confirmation' => 'Ak si tohto človeka pridáte do kontaktov, potom ho môžete pozvať.',

	// C
	'configuration_delai_sans_nouvelles_label' => 'Časový limit pre stav "Žiadne nové"',
	'configuration_invitation_obligatoire_label' => 'Vyžaduje sa pozvánka',
	'configuration_titre' => 'Nastavenia patronátu',

	// E
	'erreur_aucun_filleul' => 'Nevybrali ste žiadneho priateľa.',
	'erreur_invitation_invalide' => 'Vaša pozvánka sa nezhoduje so zadanou e-mailovou adresou',
	'erreur_invitation_obligatoire' => 'Na to, aby ste sa mohli zaregistrovať na tejto stránke, musíte mať pozvánku.',

	// F
	'filleul_actions' => 'Akcie',
	'filleul_ajouter' => 'Pridať',
	'filleul_email' => 'E-mailová adresa',
	'filleul_nom' => 'Meno',
	'filleul_statut' => 'Stav',

	// I
	'inscription_code_invitation_label' => 'Kód pozvánky',
	'invitation_message' => '@nom_parrain@ vás pozýva, aby ste sa pripojili k stránke @site@.',
	'invitation_sujet' => '@nom@ vás pozýva, aby ste sa pripojili k stránke @site@',
	'invitation_url' => 'Na prihlásenie navštívte túto adresu:',

	// P
	'parrainage_inviter' => 'Poslať pozvánku',
	'parrainage_message_aucun' => 'Nikoho z týchto ľudí netreba pozvať.',
	'parrainage_message_aucun_1' => 'Tento človek je už pozvaný alebo zaregistrovaný.',
	'parrainage_message_erreur' => 'Minimálne jedna pozvánka nebola odoslaná úspešne.',
	'parrainage_message_label' => 'Vaša správa',
	'parrainage_message_ok_pluriel' => '@nombre@ pozvánok bolo naprogramovaných, teraz sa posielajú.',
	'parrainage_message_ok_singulier' => 'Pozvánka bola naprogramovaná, teraz sa posiela.',
	'parrainage_supprimer_filleul' => 'Odstrániť tohto priateľa',
	'parrainage_supprimer_filleul_confirmation' => 'Určite chcete odstrániť @nom@ zo svojich kontaktov?',
	'plugin_nom' => 'Patronát',

	// S
	'statut_contact' => 'Kontakt',
	'statut_contact_explication' => 'Tento človek je vo vašom adresári. Môžete ho pozvať, aby sa pridal na stránku.',
	'statut_deja_inscrit' => 'Už zaregistrovaný',
	'statut_deja_inscrit_explication' => 'Tento človek je už zaregistrovaný na stránke, ale nie je váš priateľ!',
	'statut_en_cours' => 'Pozvánka sa posiela',
	'statut_en_cours_explication' => 'Vaša pozvánka sa posiela.',
	'statut_filleul' => 'Priateľ',
	'statut_filleul_depuis' => 'Priateľ od @date@',
	'statut_filleul_explication' => 'Vďaka vám sa tento človek zaregistroval na stránke: vy ste jeho patrón.',
	'statut_invite' => 'Pozvaný @duree@',
	'statut_invite_explication' => 'Nedávno ste tohto človeka požiadali.',
	'statut_sans_nouvelles' => 'Žiadni noví od @date@',
	'statut_sans_nouvelles_explication' => 'Tohto človeka ste pred nejakým časom pozvali, ale nikdy neprišiel.',
	'statut_visite' => 'Navštívil(a) stránku',
	'statut_visite_explication' => 'Tento človek už dostal vašu pozvánku, aby prišiel na stránku, ale nezaregistroval sa.'
);

?>
