<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-tabledata
// Langue: fr
// Date: 10-04-2012 08:11:46
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// T
	'tabledata_description' => 'Gestion des tables extra et autres.
_ Liste les tables \'extra\' et permet le choix
_ Liste les enregistrement d\'une table.
_ Ajouter/supprimer/editer des enregistrements dans une table.
_ Les pages sont dynamiques, elles s\'adaptent  la structure de la table.
_ Compatible fonctions sql portables MySQL, PostGreSQL, SQLite',
	'tabledata_slogan' => 'Gestion des tables extra et autres',
);
?>