<?php

// fichier : lang/tabledata_fr.php
// version : 2.1.1
// date : 10 mai 2009


// This is a SPIP language file  --  Ceci est un fichier langue de SPIP


if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	"export" => "Exporter la table",
    // I
    "insertion" => "insertion",
	
	// T
	"tabledata" => "TableData",

);

?>
