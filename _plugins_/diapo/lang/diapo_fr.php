<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'diapo'=>'Diaporama',
'txt_icodiapo'=>'Diaporama lecture/pause',
'txt_icoleft'=>'Vignettes &agrave; gauche',
'txt_icoright'=>'Vignettes &agrave; droite',
'txt_icocenter'=>'Vignettes en haut (grande image)',
'ico_diapo_play'=>'&gt;',
'ico_diapo_pause'=>'||',
'ico_left'=>'&nbsp;',
'ico_right'=>'&nbsp;',
'ico_center'=>'&nbsp;',
'loading'=>'Chargement en cours...'
);


?>