<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'diapo'=>'Diaporama',
'txt_icodiapo'=>'Diaporama play/pause',
'txt_icoleft'=>'Vorschaubilder links',
'txt_icoright'=>'Vorschaubilder rechts',
'txt_icocenter'=>'Vorschaubilder oben (Grosses Bild)',
'ico_diapo_play'=>'&gt;',
'ico_diapo_pause'=>'||',
'ico_left'=>'&nbsp;',
'ico_right'=>'&nbsp;',
'ico_center'=>'&nbsp;',
'loading'=>'Das Bild wird geladen...'
);


?>