<?php
/**
 * Plugin Actunite pour Spip 2.0
 * Licence GPL (c) 2009 - Ateliers CYM
 */

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'cfg_nom_plugin'				=> 'actunite',
	'cfg_titre_plugin'				=> 'Plugin Actunite',
	'cfg_nom_boite'					=> 'Formulaire de configuration du plugin "Actunite"',
	'cfg_help_boucle'				=> 'La boucle qui permet d\'extraire les articles du site est param&eacute;trable. On peut tout d\'abord sp&eacute;cifier la ou les rubriques desquelles seront extraites les articles (indiquer les num&eacute;ros de rubriques s&eacute;par&eacute;s par des virgules). Ensuite on peut indiquer si la s&eacute;lection des articles s\'effectue selon certains momts cl&eacute;s (idem, liste des id_mots s&eacute;par&eacute;s par des virgules). Enfin, on peut indiquer les &eacute;ventuels crit&egrave;res de tri pour la boucle (par date, par num titre, etc.).',
	'cfg_help_insertion' 			=> 'Choisissez l\'endroit du squelette o&ugrave; sera ins&eacute;r&eacute; le bloc Actunite. Vous pouvez indiquer tout &eacute;l&eacute;ment html qui compose votre page en sp&eacute;cifiant bien s\'il s\'agit d\'une ID ou d\'une CLASS.',
	'cfg_inf_insertion' 			=> 'Ins&#233;rer le bloc de <strong>Actunite</strong> :',
	'cfg_info_insertion'			=> 'Le bloc "Actunite" sera ins&eacute;r&eacute; :',
	'cfg_label_avant' 				=> 'avant',
	'cfg_label_apres' 				=> 'apr&egrave;s',	
	'cfg_label_dans' 				=> 'dans',
	'cfg_label_le_bloc'				=> '&nbsp;le bloc ',
	'cfg_legend_insertion'			=> 'Insertion',
	'cfg_label_critere_tri' 		=> 'Crit&egrave;res de tri :',
	'cfg_legend_insertion' 			=> 'Point d\'insertion',
	'cfg_label_mots_cle' 			=> 'Mots cl&eacute; :',
	'cfg_label_rubriques' 			=> 'Rubriques :',
	'cfg_legend_boucle' 			=> 'Param&egrave;tres de la boucle',
	'cfg_label_titre_insertion'		=> 'Position d\'insertion',
	'cfg_titre_descriptif'			=> 'Le plugin Actunite',
	'cfg_texte_descriptif'			=> 'Cette page de configuration du plugin <strong>Actunite</strong> vous permet de d&eacute;finir diff&eacute;rents param&egrave;tres de personnalisation.</p><p><a href="http://www.spip-contrib.net/Plugin-Acunite?var_mode=preview" class="spip_out">Doc en ligne, forum, FAQ sur SPIP-Contrib</a></p>',

);



?>