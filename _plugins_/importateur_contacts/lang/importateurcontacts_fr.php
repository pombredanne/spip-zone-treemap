<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/importateur_contacts/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer_fournisseur_active_avec_moteur' => 'Activé avec @moteur@',
	'configurer_fournisseur_desactive' => 'Désactivé',
	'configurer_titre' => 'Configurer les services d\'importation de contacts',

	// E
	'erreur_aucun_fournisseur' => 'Aucun service pouvant fournir des contacts n\'a été trouvé. Veuillez installer au moins une librairie d\'importation de contacts.',

	// I
	'importer_bouton_recuperer_contacts' => 'Récupérer mes contacts',
	'importer_fournisseur_label' => 'Choisissez le service chez qui récupérer vos contacts',
	'info_aucun_contact' => 'Aucun contact n\'a été trouvé pour ce compte.',
	'info_nb_contacts' => '@nb@ contacts ont été récupérés.',
	'info_nb_contacts_erreur' => '@nb@ contacts ont été récupérés mais n\'ont pu être utilisés.',
	'info_titre' => 'Importateur de contacts'
);

?>
