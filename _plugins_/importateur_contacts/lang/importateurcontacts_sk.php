<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/importateurcontacts?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer_fournisseur_active_avec_moteur' => 'Aktivovať cez @moteur@',
	'configurer_fournisseur_desactive' => 'Deaktivovať',
	'configurer_titre' => 'Nastaviť služby na nahrávanie kontaktov',

	// E
	'erreur_aucun_fournisseur' => 'Žiadna služba vám teraz nemôže zabezpečiť, aby ste našli svoje kontakty. Nainštalujte si, prosím, aspoň jednu knižnicu na nahrávanie kontaktov.',

	// I
	'importer_bouton_recuperer_contacts' => 'Získať kontakty',
	'importer_fournisseur_label' => 'Vyberte si službu, z ktorej chcete získať svoje kontakty',
	'info_aucun_contact' => 'Pre tento účet sa nenašiel žiaden kontakt.',
	'info_nb_contacts' => 'Získali ste @nb@ kontaktov.',
	'info_nb_contacts_erreur' => 'Získali ste @nb@ kontaktov, ale nedali sa použiť',
	'info_titre' => 'Nahrávač kontaktov'
);

?>
