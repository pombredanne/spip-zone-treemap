<?php
//
// lang/edittable_fr.php
//

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// E
'editer_la_table'=>'Mostra',
'editer_les_tables'=>'Accedi ai Dati',
'editer_enregistrement'=>'Mostra.',
'enregistrement_numero'=>'Record N�',
'mes_edittable'=>'Le mie Tabelle',
'cle_primaire'=>'Chiave Primaria',
'configuration_du_plugin_edittable'=>'configurazione di Editable',
'cacher_table_avec_prefix'=>'nascondere tabelle con prefisso',
'liste_des_prefix_a_cacher'=>'lista dei prefissi da nascondere',
'voir_table_spip'=>'mostra tabelle di spip',
'enregistrer'=>'Salva',
'retour_a_la_table'=>'torna alla tabella',
'valeur_courante'=>'valore attuale',
'edite_enregistrement'=>'modifica record',
'enregistrements_de_la_able'=>'Dati della Tabella',
'structure_de_la_table_courante'=>'Struttura della Tabella corrente',
'les_tables_de_la_base'=>'tabelle del DB',
'configurer_edittable'=>'Configura EditTable',
'les_table'=>'le tabelle'
);
?>
