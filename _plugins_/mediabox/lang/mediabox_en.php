<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// B
	'bouton_reinitialiser' => 'Reset',

	'titre_menu_box' => 'Media Box',
	'titre_page_configurer_box' => 'Configuration of the Media box',

	'explication_selecteur' => 'Specify the target element that will trigger the box. (jQuery CSS expression or extended)',
	'explication_selecteur_galerie' => 'Specify the target elements to group together into a gallery. (jQuery CSS expression or extended)',
	'explication_splash_url' => 'Enter the URL of the media to automatically display in a box at the first visit on the public site.',
	'explication_traiter_toutes_images' => 'Insert a box on all pictures ?',

	'label_apparence' => 'Appearance',
	'label_selecteur_commun' => 'In general',
	'label_selecteur_galerie' => 'In gallery mode',
	'label_traiter_toutes_images' => 'Pictures',
	'label_skin' => 'Visual skin',
	'label_aucun_style' => 'Do not use any default skin',
	'label_transition' => 'Transition between two views',
	'label_choix_transition_elastic' => 'Elastic',
	'label_choix_transition_fade' => 'Fade',
	'label_choix_transition_none' => 'Without transition effect',
	'label_speed' => 'Transition speed (ms)',
	'label_minwidth' => 'Min width (% or px)',
	'label_minheight' => 'Min height (% or px)',
	'label_maxwidth' => 'Max width (% or px)',
	'label_maxheight' => 'Max height (% or px)',
	'label_slideshow_speed' => 'Exhibition time of the slideshow images (ms)',
	'label_splash' => 'Splash Box',
	'label_splash_url' => 'URL to display',

	'boxstr_slideshowStart' => 'Slideshow',
	'boxstr_slideshowStop' => 'Stop',
	'boxstr_current' => '{current}/{total}',
	'boxstr_previous' => 'Previous',
	'boxstr_next' => 'Next',
	'boxstr_close' => 'Close',
	'boxstr_zoom' => 'Zoom',
	
);
?>
