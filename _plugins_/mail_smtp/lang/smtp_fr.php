<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'info_smtp' => 'Serveur Mail SMTP',
'texte_smtp' => 'Configurez les param&egrave;tres de connexion au serveur SMTP',
'smtp_host' => 'Host :',
'smtp_port' => 'Port :',
'smtp_auth' => 'Ce serveur necessite une authentification ',
'smtp_username' => 'Nom d&#8217;utilisateur :',
'smtp_password' => 'Mot de passe :',
'test_mail' => 'Un email va &ecirc;tre envoy&eacute; &agrave; @email@ pour tester la connexion avec le serveur SMTP.',
'test_mail_echec' => 'Echec de l&#8217;envoi du mail par le serveur SMTP',
'smtp_mail_test_sujet' => 'Test de connexion SMTP',
'smtp_mail_test_texte' => 'Ce mail a &eacute;t&eacute; envoy&eacute; par serveur SMTP avec les param&egrave;tres suivants :',
);


?>