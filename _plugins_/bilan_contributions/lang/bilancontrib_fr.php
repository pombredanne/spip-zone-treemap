<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'breves' => 'Brèves publiées',
	
	// D
	'derniere_connexion' => 'Dernière connexion',
	'documents_publies' => 'Documents publiés',
	
	// E
	'explication_id_secteur' => 'Choisir éventuellement le secteur sur lequel effectuer le bilan',
	'exporter_csv' => 'Exporter au format csv',
	
	// G
	'graph' => 'Filtre',
	
	// J
	'jamais_connecte' => 'Jamais connecté',
	
	// L
	'label_id_secteur' => 'Secteur',
	
	// N
	'nb_max' => '(10 max)',
	'nombre' => 'Nombre',
	'nom_plugin' => 'Bilan annuel des contributions',
	
	// O
	'objet' => 'Objet',
	
	// S
	'slogan' => 'En même temps, les chiffres, on peut leur faire dire ce qu\'on veut !',
	
	// T
	'taille' => 'Taille (Mo)',
	'titre_contrib_auteurs' => 'Contributions des auteurs - Articles publiés',
	'titre_contrib_objets' => 'Objets éditoriaux publiés',
	'total' => 'Total',
	
	// W
	'info_webmestre' => 'Webmestre' ,
);

?>
