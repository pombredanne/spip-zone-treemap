<?php

/**

* saveauto : plugin de sauvegarde automatique de la base de données de SPIP

*  

* Ce programme est un logiciel libre distribue sous licence GNU/GPL.

*  

**/

	$GLOBALS['i18n_medias_metas_fr'] = array(

			    // cfg

						 'medias_metas' => 'Métadonnées pour la médiathèque',
             'config' => 'Plugin Métadonnées pour la médiathèque: configuration',
             'help_titre' => 'Cette page vous permet de configurer les options du plugin.',
             'oui' => 'oui',
             'non' => 'non',
             'choix_meta' => 'Utiliser l\'affichage modifiées (dans la médiathèque)',
             'help_choix_meta' => '(Sinon, utilisation de l\'affichage officiel du plugin métadonnées photos)',
             'inclure_meta' => 'Inclure les metas dans',
             'articles' => 'les articles (dans l\'espace privé)',
             'mediatheque' => 'la médiathèque'
             
  );

?>
