<?php

// $LastChangedRevision$
// $LastChangedBy$
// $LastChangedDate$

$GLOBALS['i18n_exau_fr'] = array(

	'nom_plugin_complet' => "Export Auteurs (ExAu)"

	, 'exporter_auteurs' => "Exporter les auteurs"
	, 'exporter_visiteurs' => "Exporter les visiteurs"
	
	, 'cfg_presentation_exau' => "{{ExAu}} ajoute un bouton dans les pages d'administration des auteurs.<br />
		Ce bouton permet l'exportation rapide des auteurs.<br />
		Le r&#233;sultat appara&#238;t sous forme de liste, dans une nouvelle fen&#234;tre.<br />
		Vous pouvez personnaliser la présentation du résultat (la liste)
		en recopiant le fichier <strong>lister_auteurs.html</strong> dans votre r&#233;pertoire 
		de squelettes et en l'adaptant &#224; vos besoins.
		"
	, 'cfg_pos_bouton' => "Disponibilité du bouton"
	, 'cfg_bouton_visiteurs' => "Par d&#233;faut, le bouton <strong>Exporter les auteurs</strong> n'appara&#238;t que dans la page des visiteurs."
	, 'cfg_bouton_partout' => "En cochant cette case, le bouton <strong>Exporter les auteurs</strong> appara&#238;t dans toutes les pages de gestion des auteurs."
);
