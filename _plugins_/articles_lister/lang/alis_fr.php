<?php
/**
 * Pack langue francais
 * @see http://www.quesaco.org/plugin-spip-articles-lister
 */
 // $LastChangedBy$
 // $LastChangedDate$
 
$GLOBALS['i18n_alis_fr'] = array(
	
	'inventaire_articles_par_tri' => 'Inventaire des articles tri&#233;s par <em>@tri@</em>',

	'titre' => 'Titre',
	'editer_cet_article' => 'Editer cet article en espace priv&#233;',
	'date_publication' => 'Date de publication',
	'modification' => 'Modification',
	'retour_espace_prive' => 'Retour &#224; l&#39;espace priv&#233;',
	'nombre_visites' => 'Nombre de visites',
	'date_modification' => 'Date de dernière modification',
	'popularite' => 'Popularit&#233;',
	'long_desc' => 'Long. desc.',
	'exporter_csv' => 'Exporter au format CSV'
);