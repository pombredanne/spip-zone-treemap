<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

	//Fond cfg
	'methode_envoi' => 'M&eacute;thode d\'envoi de mail',
	'methode_envoi_texte' => '<p>*  La m&eacute;thode SMTP permettant de se connecter &agrave; un serveur de mail distant<br />* La m&eacute;thode "mail" lorsque le serveur de mail est local</p>',
	'serveur_smtp' => 'Configuration du serveur SMTP',
	'smtp_adresse' => 'Adresse du serveur :',
	'smtp_auth' => 'Le serveur n&eacute;cessite une autorisation?',
	'smtp_auth_non' => 'Non',
	'smtp_auth_yes' => 'Oui',
	'username' => 'Nom d\'utilisateur :',
	'password' => 'Mot de passe :',
	'default_mail_options' => 'Options par d&eacute;faut des mails envoy&eacute;s',
	'default_to' => '"To" par d&eacute;faut :',
	'default_reply_to' => 'Reply-To par d&eacute;faut :',
	'default_errors_to' => 'Errors-To par d&eacute;faut :',
	'default_return_path' => 'Return Path par d&eacute;faut :',
	'default_timeout' => 'Timeout (temps entre les mails) par d&eacute;faut :',
	
	// Contenu mail
	'avertissement_mail_texte' => '(Ce texte n\'est que la version textuelle du mail visible compl&ecirc;tement dans un logiciel compatible HTML)',
	
	'zzz' => 'zzz'
);
?>