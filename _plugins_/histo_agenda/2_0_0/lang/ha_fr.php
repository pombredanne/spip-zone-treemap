<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
				       "aide"=>"Ce plugin permet de visualiser les modifications de l'agenda entre deux dates.<br /><br />Tous les jours, l'ensemble des &eacute;v&eacute;nements li&eacute;s &agrave; l'agenda est sauvegard&eacute; (seuls les &eacute;v&eacute;nements du jour et futurs sont sauvegard&eacute;s). Ces informations sont accessibles ('Visualiser les &eacute;v&eacute;nements' en haut &agrave; gauche).<br /><br />Il est &eacute;galement possible de comparer les &eacute;v&eacute;nements entre deux dates donn&eacute;es ou bien avec les &eacute;v&eacute;nements pr&eacute;sents actuellement ('Visualiser les modifications').",
				       "config"=>"Configurer Historique agenda",
				       "dates_sauvegardes"=>"Date de sauvegarde :",
				       "titre"=>"Historique agenda",
				       "titre_aide"=>"Aide",
				       "titre_page"=>"Historique agenda",
				       "titre_visualisation"=>"Visualisation des &eacute;v&eacute;nements du ",
				       "titre_modification"=>"Visualisation des modifications entre ",
				       "titre_modification_et"=>" et ",
				       "modification_entre"=>"Modifications entre :",
				       "modification_et"=>"et :",

				       "sk_date_debut" => "Date d&eacute;but:",
				       "sk_date_fin" => "Date de fin:",
				       "sk_descriptif" => "Descriptif:",
				       "sk_evenements" => "Ev&eacute;nements attach&eacute;s:",
				       "sk_hierarchie" => "Hi&eacute;rarchie:",
				       "sk_horaire" => "Horaire:",
				       "sk_lieu" => "Lieu:",
				       "sk_mots" => "Mots:",
				       "sk_titre" => "Titre:",

				       "visualiser"=>"Visualiser",
				       "voir_evenements"=>"Visualiser les &eacute;v&eacute;nements",
				       "voir_modifications"=>"Visualiser les modifications"				       

    );

?>
