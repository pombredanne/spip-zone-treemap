<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'ticket_associer_projet' => 'Associer au projet',
	'tickets_associes' => 'Tickets associ&eacute;s',
);
?>