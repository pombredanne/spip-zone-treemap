<?php

// S&eacute;curit&eacute;
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	'couverture' => 'une couverture d\'ouvrage',
	'edited_by' => '&eacute;dit&eacute; par',
	'explication_authors' => 'Entrez les auteurs sous la forme <i>Nom, Pr&eacute;nom</i> en utilisant la virgule comme s&eacute;parateur. Utilisez le point-virgule pour s&eacute;parer plusieurs auteurs. Exemple&nbsp: <i>Einstein, Albert ; Descartes, Ren&eacute ; Groupe d\'auteurs</i>.',
	'explication_edition' => 'Par exemple&nbsp: <i>seconde &eacute;dition corrig&eacute;e et augment&eacute;</i>.',
	'explication_editors' => '&Agrave; ne pas confondre avec la maison d\'&eacute;dition. Pour les chapitres d\'ouvrage, indiquez ici les personnes ayant dirig&eacute; le le livre. La syntaxe est identique &agrave; celle des auteurs&nbsp;: utilisez la virgule pour s&eacute;parer le nom du pr&eacute;nom et le point-virgule pour s&eacute;parer plusieurs &eacute;diteurs.',
	'explication_pages' => 'Nombre total de pages (livres, rapports...) ou page de d&eacute;but et page de fin (articles, chapitres...). Dans ce second cas, utilisez le tiret (-) entre la page de d&eacute;but et la page de fin. Exemple&nbsp: <i>125-136</i>.',
	'explication_publisher' => 'S\'il y a plusieurs &eacute;diteurs, les s&eacute;parer par une virgule.',
	'export_format' => 'Télécharger la référence au format @format@',
	'in' => 'in',
	'item_book' => 'livre',
	'item_chapter' => 'chapitre de livre',
	'item_journal' => 'article scientifique',
	'item_report' => 'rapport',
	'item_thesis' => 'th&egrave;se',
	'item_web' => 'page web',
	'label_accessdate' => 'Date de consultation (AAAA/MM/JJ)&nbsp;:',
	'label_authors' => 'Auteurs&nbsp;:',
	'label_booktitle' => 'Titre du livre&nbsp;:',
	'label_doi' => 'DOI&nbsp;:',
	'label_edition' => '&Eacute;dition&nbsp;:',
	'label_editors' => '&Eacute;diteurs&nbsp;:',
	'label_id_document' => 'Num&eacute;ro du document &agrave; utiliser en couverture&nbsp;:',
	'label_institution' => 'Institution&nbsp;:',
	'label_isbn' => 'ISBN&nbsp;:',
	'label_issn' => 'ISSN&nbsp;:',
	'label_issue' => 'Num&eacute;ro&nbsp;:',
	'label_journal' => 'Nom de la revue&nbsp;:',
	'label_number' => 'Num&eacute;ro&nbsp;:',
	'label_pages' => 'Pages&nbsp;:',
	'label_place' => 'Ville&nbsp;:',
	'label_publisher' => 'Maison d\'&eacute;dition / &Eacute;diteur&nbsp;:',
	'label_report_type' => 'Type de rapport&nbsp;:',
	'label_series' => 'S&eacute;rie / Collection&nbsp;:',
	'label_site' => 'Titre du site web&nbsp;:',
	'label_taille' => 'Taille en pixels pour redimensionner l\'image (100 par d&eacute;faut)&nbsp;:',
	'label_thesis_type' => 'Type de th&egrave;se&nbsp;:',
	'label_title' => 'Titre&nbsp;:',
	'label_university' => 'Universit&eacute;&nbsp;:',
	'label_url' => 'Lien (URL)&nbsp;:',
	'label_variante' => 'Type de document&nbsp;:',
	'label_volume' => 'Volume&nbsp;:',
	'label_year' => 'Ann&eacute;e de publication&nbsp;:',
	'number' => 'num&eacute;ro',
	'page_consultee_le' => 'page consult&eacute;e le',
	'raccourci_pages' => 'p.',
	'reference_bibliographie' => 'une r&eacute;f&eacute;rence bibliographique',
	'series' => 'collection &laquo;&nbsp;@series@&nbsp;&raquo;',
	'volume' => 'Volume',

);

?>
