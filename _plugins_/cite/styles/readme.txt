Il y deux versions différentes du style SPIP-Cite.cls :
- dans le répertoire styles, il s'agit de la version à utiliser avec Zotero
- dans le répertoire csl, il s'agit d'une version embarquée pour ZotSpip. Il faut y remplacer &lt; par &amp;lt; pour que citeproc.php produise bien un &lt; (de même pour &gt;).
