<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'cog' => 'COG',
'aucun_article_accueil' => 'Aucun article',
'communes' => 'Communes',
'attacher_une_commune' => 'Attacher une commune',


'configuration' => 'Configuration',
'importer_les_donnee' => 'Importer les données',
'composition_verrouillee' => 'Cette composition est verrouillé par le webmestre',

'label_chemin_cog' => 'Dossier des données',
'label_chemin_cog_details' => 'Indiquez le chemin dans lequel les données sont stockées.',
'label_fichier' => 'Fichiers à charger :',
'liste_des_communes_attachees'=> 'Liste des communes attachées:',
'fichier_commune' => 'Fichiers commune, arrondissement, département, région',
'fichier_epci' => 'Fichier EPCI et ses relations avec communes',
'fichier_zauer' => 'Fichier ZAUER et ses relations avec communes',
'fichier_zone_emploi' => 'Fichiers EPCI et relation avec communes',
'fichier_introuvable' => 'Fichier introuvable',
'fichier_incorrect' => 'Fichier incorrect',
'fichiers_a_telecharger' => 'Fichiers à télécharger',
'choississez_le_fichier_a_telecharger' => 'Choississez le fichier à télécharger sur le serveur de l\'INSEE',
'choix_erronne' => 'Choix de la délimitation administrative erronné',

'importer_les_cog' => 'Importer les C.O.G.',

'importer' => 'Importer',

'bouton_importer' => 'Importer',
'ecraser_enregistrement' => 'Écraser les enregistrements existants',
'vider_la_table' => 'Vider la table avant l\'importation',

'bouton_ajouter' => 'Ajouter',
'bouton_supprimer' => 'Supprimer',
'bouton_telecharger' => 'Télécharger',

'liste_des_tables_du_cog'=>'Liste des tables du COG',
'codes_supplementaires'=>'Codes supplémentaires',
'bloc_cog'=>'Bloc de saisie COG',
'activer_bloc_cog'=>'Activer le bloc de saisie "Attacher à une commune"',
'explication_limitation_rubriques_cog'=>'Limiter l\'affichage du bloc de saisie COG à certaines rubriques',
'limitation_rubriques_cog'=>'Limitation du bloc de saisie COG',
'delimitations_supplementaires'=>'Délimitations supplémentaires',
'telecharger_les_cog_distant'=>'Télécharger les COG distant',
'fichiers'=>'Fichiers',
'texte_presentation_cog'=>'Le plugin COG permet ajouter facilement les codes officiels géographiques défini par l\'INSEE.',
'filtre_d_importation'=>'Filtrer les données',
'choississez_le_fichier_a_importer'=>'Choississez le type de délimitation à importer',

'decoupage_cantons'=>'Découpage de la commune en cantons',
'chef_lieu'=>'Chef lieu',
'region'=>'Région',
'departement'=>'Département',
'code'=>'Code',
'arrondissement'=>'Arrondissement',
'canton'=>'Canton',
'type_charniere'=>'Type de nom en clair',
'article_majuscule'=>'Article en majuscule',
'nom_majuscule'=>'Nom en majuscule',
'article'=>'Article',
'code_insee'=>'Code INSEE',
'nom'=>'Nom'


);


?>

