<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/jqueryui?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'afficher_calendrier' => 'Show calendar',

	// C
	'cfg_boite_jqueryui' => 'jQuery UI setup',
	'cfg_explication_plugins' => 'Choose here the plugins to insert in the website.',
	'cfg_explication_themes' => 'Choose the themes for the jQuery-UI look.',
	'cfg_lbl_plugins' => 'Plugins',
	'cfg_lbl_themes' => 'Theme',
	'cfg_no_css' => 'do not load jQuery-UI CSS',
	'cfg_titre_jqueryui' => 'jQuery UI',
	'cfg_val_complete' => 'The complete jQuery UI',
	'cfg_val_effects_blind' => 'Blind effect',
	'cfg_val_effects_bounce' => 'Bounce effect',
	'cfg_val_effects_clip' => 'Clip effect',
	'cfg_val_effects_core' => 'Core effects',
	'cfg_val_effects_drop' => 'Drop effect',
	'cfg_val_effects_explode' => 'Explode effect',
	'cfg_val_effects_fold' => 'Fold effect',
	'cfg_val_effects_highlight' => 'Highlight effect',
	'cfg_val_effects_pulsate' => 'Pulsate effect',
	'cfg_val_effects_scale' => 'Scale effect',
	'cfg_val_effects_shake' => 'Shake effect',
	'cfg_val_effects_slide' => 'Slide effect',
	'cfg_val_effects_transfer' => 'Transfer effect',
	'cfg_val_ui_accordion' => 'UI accordion',
	'cfg_val_ui_autocomplete' => 'UI autocomplete',
	'cfg_val_ui_button' => 'UI button',
	'cfg_val_ui_core' => 'Core UI',
	'cfg_val_ui_datepicker' => 'UI datepicker',
	'cfg_val_ui_dialog' => 'UI dialog',
	'cfg_val_ui_draggable' => 'UI draggable',
	'cfg_val_ui_droppable' => 'UI droppable',
	'cfg_val_ui_mouse' => 'UI mouse',
	'cfg_val_ui_position' => 'UI position',
	'cfg_val_ui_progressbar' => 'UI progressbar',
	'cfg_val_ui_resizable' => 'UI resizable',
	'cfg_val_ui_selectable' => 'UI selectable',
	'cfg_val_ui_slider' => 'UI slider',
	'cfg_val_ui_sortable' => 'UI sortable',
	'cfg_val_ui_tabs' => 'UI tabs',
	'cfg_val_ui_widget' => 'UI widget',

	// D
	'date_mois_10_abbr' => 'Oct',
	'date_mois_11_abbr' => 'Nov',
	'date_mois_12_abbr' => 'Dec',
	'date_mois_1_abbr' => 'Jan',
	'date_mois_2_abbr' => 'Feb',
	'date_mois_3_abbr' => 'Mar',
	'date_mois_4_abbr' => 'Apr',
	'date_mois_5_abbr' => 'May',
	'date_mois_6_abbr' => 'Jun',
	'date_mois_7_abbr' => 'Jul',
	'date_mois_8_abbr' => 'Aug',
	'date_mois_9_abbr' => 'Sep'
);

?>
