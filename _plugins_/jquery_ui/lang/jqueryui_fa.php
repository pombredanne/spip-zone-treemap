<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/jqueryui?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'afficher_calendrier' => 'نمايش تقويم',

	// C
	'cfg_boite_jqueryui' => 'پيكربندي جي‌كويري يو.آي (jQuery UI)',
	'cfg_explication_plugins' => 'لاگين‌ها را براي گنجاندن در سرصفحه‌هاي همگاني انتخاب كنيد.',
	'cfg_explication_themes' => 'تم‌ها را براي ظاهر جي‌كويري-يو.آي انتخاب كنيد.',
	'cfg_lbl_plugins' => 'پلاگين‌ها',
	'cfg_lbl_themes' => 'تم',
	'cfg_no_css' => 'شيوه‌‌‌ نامه آبشاري (سي.اس.اس) جي‌كويري-يو.آي بارگذاري نشده',
	'cfg_titre_jqueryui' => 'jQuery UI (جي‌كويري يو.آي)',
	'cfg_val_complete' => 'جي‌كويري يو.آي كامل ',
	'cfg_val_effects_blind' => 'افكت كور',
	'cfg_val_effects_bounce' => 'افكت بونس (لرزش كليد)',
	'cfg_val_effects_clip' => 'افكت كليپ',
	'cfg_val_effects_core' => 'افكت‌هاي اصلي',
	'cfg_val_effects_drop' => 'افكت قطره',
	'cfg_val_effects_explode' => 'افكت انفجار',
	'cfg_val_effects_fold' => 'افكت تاخوردگي ',
	'cfg_val_effects_highlight' => 'افكت هاي‌لايت',
	'cfg_val_effects_pulsate' => 'افكت ضربان
',
	'cfg_val_effects_scale' => 'افكت پولك',
	'cfg_val_effects_shake' => 'افكت لرزش',
	'cfg_val_effects_slide' => 'افكت اسلايد',
	'cfg_val_effects_transfer' => 'افكت ترانسفر',
	'cfg_val_ui_accordion' => 'يو.آي آكاردئون ',
	'cfg_val_ui_autocomplete' => 'يو.آي خودتكميل',
	'cfg_val_ui_button' => 'دكمه‌ي يو.آي',
	'cfg_val_ui_core' => 'يو.آي اصلي',
	'cfg_val_ui_datepicker' => 'يو.آي ديت‌ پيكر ',
	'cfg_val_ui_dialog' => 'يو.آي گفتگو',
	'cfg_val_ui_draggable' => 'يو.آي كشيدني ',
	'cfg_val_ui_droppable' => 'يو.آي ريختي ',
	'cfg_val_ui_mouse' => 'موس يو.آي',
	'cfg_val_ui_position' => 'وضعيت يو.آي',
	'cfg_val_ui_progressbar' => 'ميل رفت يو.آي',
	'cfg_val_ui_resizable' => 'يو.آي اندازه پذير ',
	'cfg_val_ui_selectable' => 'يو.آي گزينشي ',
	'cfg_val_ui_slider' => 'يو.آي لغزنده',
	'cfg_val_ui_sortable' => 'يو.آي مرتب شدني',
	'cfg_val_ui_tabs' => 'تب‌هاي يو.آي',
	'cfg_val_ui_widget' => 'ويدگت يو.آي',

	// D
	'date_mois_10_abbr' => 'اكتبر',
	'date_mois_11_abbr' => 'نوامبر',
	'date_mois_12_abbr' => 'دسامبر',
	'date_mois_1_abbr' => 'ژانويه',
	'date_mois_2_abbr' => 'فوريه',
	'date_mois_3_abbr' => 'مارس',
	'date_mois_4_abbr' => 'آوريل',
	'date_mois_5_abbr' => 'مه',
	'date_mois_6_abbr' => 'ژوئن',
	'date_mois_7_abbr' => 'جولاي',
	'date_mois_8_abbr' => 'اوت',
	'date_mois_9_abbr' => 'سپتامبر'
);

?>
