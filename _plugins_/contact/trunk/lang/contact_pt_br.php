<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/contact?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'alerte_effacement' => 'Confirme a exclusão dos dados de configuração de contato.',
	'aucun_message' => 'Nenhuma mensagem a ser exibida',

	// B
	'bouton_effacer' => 'Apagar',

	// C
	'champ_adresse' => 'Endereço',
	'champ_civi_explication' => 'Configuração avançada do campo tratamento',
	'champ_civi_mademoiselle_ok' => 'Incluir uma opção para "Senhorita" no campo tratamento',
	'champ_civilite' => 'Tratamento',
	'champ_code_postal' => 'Cep',
	'champ_etat' => 'Estado',
	'champ_impose' => 'campo obrigatório',
	'champ_infolettre' => 'Boletim',
	'champ_mail' => 'E-mail',
	'champ_nom' => 'Nome',
	'champ_obligatoire' => 'Obrigatório?',
	'champ_organisation' => 'Organização',
	'champ_pays' => 'País',
	'champ_portable' => 'Celular',
	'champ_prenom' => 'Primeiro nome',
	'champ_sujet' => 'Assunto',
	'champ_telephone' => 'Telefone',
	'champ_texte' => 'Texto',
	'champ_ville' => 'Cidade',
	'configuration_barre_forum' => 'Exibir a barra de ferramentas tipográficas nos fóruns.',
	'configuration_barre_impossible' => 'Outro plugin está bloqueando o uso do porta-caneta no espaó público - as configurações abaixo não terão nenhum efeito.',
	'configuration_barre_oui' => 'Exibir a barra de ferramentas tipográficas completa.',
	'configuration_champs_message' => 'Configuração do porta-caneta',
	'configuration_champs_message_explication' => 'Configuração da barra de ferramentas tipográficas associada ao texto da mensagem.',
	'configuration_description' => 'Permitir a configuração do formulário de contato.',
	'configuration_destinataires' => 'Destinatários',
	'configuration_destinataires_explication' => 'Escolha um ou mais autores que os visitantes poderão contatar. Se nada for definido, é o autor número 1 (o que instalou o site) que será usado.',
	'configuration_destinataires_plusieurs' => 'Permite que os visitantes escolham <b>diversos</b> destinatários entre a lista selecionada abaixo',
	'configuration_destinataires_plusieurs_et' => '... limitada pelos autores eventualmente transferidos ao formulário.',
	'configuration_destinataires_plusieurs_ou' => '... ou entre o grupo de autores eventualmente transferidos ao formulário',
	'configuration_destinataires_tous' => 'Incluir <b>todos</b> estes autores como destinatários da mensagem',
	'configuration_destinataires_tous_et' => '... incluir o grupo de autores eventualmente transferidos ao formulário',
	'configuration_destinataires_tous_ou' => '... ou apenas os eventualmente transferidos para o formulário',
	'configuration_destinataires_un' => 'Forçar os visitantes a escolher <b>apenas um</b> destinatário entre a lista selecionada abaixo',
	'configuration_destinataires_un_et' => '... limitáda pelos autores eventualmente transferidos para o formulário.',
	'configuration_destinataires_un_ou' => '... ou entre o grupo de autores eventualmente transferidos para o formulário',
	'configuration_intro' => 'Introdução',
	'configuration_intro_explication' => 'Escreva eventualmente um texto que apresente o formulário',
	'configuration_no_barre' => 'Não propor a barra de ferramentas tipográficas.',
	'configuration_no_previsualisation' => 'Exibir a barra de ferramentas tipográficas sem o botão de visualização.',
	'configuration_pj' => 'Anexo',
	'configuration_pj_autoriser' => 'Permitir o envio de anexos',
	'configuration_pj_facteur_absent' => 'Para usar esta função, você deve instalar o plugin Facteur.',
	'configuration_pj_nb_max' => 'Número máximo de anexos',
	'configuration_regle_liste_explication' => 'Escolha uma regra de cálculo da lista de potenciais destinatários e o método de seleção proposto aos visitantes.',
	'configuration_texteinfolettre' => 'Texto boletim',
	'configuration_texteinfolettre_explication' => 'Escreva o texto associado à caixa de seleção informativo', # NEW
	'consulter_memoire' => 'Ce message est enregistré sur votre site :', # NEW
	'courriel_de' => 'De', # NEW
	'courriel_pour' => 'Pour', # NEW

	// D
	'deplacement_possible' => 'Déplacement vertical par glissé-déposé', # NEW
	'description_menu_contact' => 'Élément de menu vers la page du formulaire de contact', # NEW
	'description_page-contact' => 'Acessible via <i>spip.php?page=contact</i>, cette page optionnelle permet de fournir un formulaire de contact (pensez à ajouter un lien dans vos menus).', # NEW

	// F
	'form_destinataire' => 'Choisissez un destinataire', # NEW
	'form_destinataires' => 'Choisissez les destinataires', # NEW
	'form_pj_ajouter_pluriel' => 'Ajouter des pièces jointes', # NEW
	'form_pj_ajouter_singulier' => 'Ajouter une pièce jointe', # NEW
	'form_pj_fichier_ajoute' => 'Le fichier a bien été ajouté : ', # NEW
	'form_pj_importer' => 'Importer un fichier', # NEW
	'form_pj_previsu_pluriel' => 'Pièces jointes', # NEW
	'form_pj_previsu_singulier' => 'Pièce jointe', # NEW
	'form_pj_supprimer' => 'Supprimer', # NEW
	'form_pj_titre' => 'Titre du fichier', # NEW
	'forum_attention_nbre_caracteres' => '<b>Attention !</b> votre message doit contenir au moins @nbre_caract@ caractères.', # NEW

	// H
	'horodatage' => 'Formulaire posté le : @horodatage@.', # NEW

	// I
	'inforigine' => 'Information transmise par le formulaire : ', # NEW
	'informez_moi' => 'J\'accepte de recevoir, par courrier électronique, des informations émanant de votre organisation.', # NEW
	'infos_collecte' => 'Informations collectées', # NEW
	'infos_supplementaires' => 'Informations supplémentaires', # NEW

	// M
	'madame' => 'Madame', # NEW
	'mademoiselle' => 'Mademoiselle', # NEW
	'modele_label_auteur' => 'Pré-sélectionner un auteur (facultatif) ?', # NEW
	'modele_nom_formulaire' => 'un formulaire de contact', # MODIF
	'monsieur' => 'Monsieur', # NEW
	'msg_accueil' => 'Vous trouverez ici les messages de contact que vous avez reçu.', # NEW
	'msg_contact' => 'Message de contact', # NEW
	'msg_contacts' => 'Messages de contact', # NEW
	'msg_expediteur' => 'Expéditeur : ', # NEW
	'msg_lus' => 'Messages déjà lus', # NEW
	'msg_messagerie' => 'Messagerie de contact', # NEW
	'msg_nouveaux' => 'Nouveaux messages', # NEW
	'msg_pas_nouveaux' => 'Vous n\'avez pas de nouveaux messages.', # NEW
	'msg_revenir_accueil' => 'Revenir à la liste de messages de contact reçus.', # NEW
	'msg_supprimer_message' => 'Supprimer le message', # NEW

	// N
	'noisette_label_afficher_titre_noisette' => 'Afficher un titre ?', # NEW
	'noisette_label_niveau_titre' => 'Choisir le niveau du titre', # NEW
	'noisette_label_titre_noisette' => 'Titre :', # NEW
	'noisette_label_titre_noisette_perso' => 'Si titre personnalisé :', # NEW
	'noisette_titre_perso' => 'Titre personnalisé', # NEW

	// P
	'par' => 'Par : ', # NEW
	'preselection' => 'Décocher tous les items de la liste de destinataires', # NEW

	// S
	'sauv_message' => 'Sauvegarder les messages de contact', # NEW
	'sauv_message_explication' => 'Permet d\'enregistrer les messages de contacts et de les afficher dans une messagerie dédiée.', # NEW
	'sauv_message_ok' => 'Sauvegarder les messages de contacts', # NEW
	'succes' => 'Merci, votre message a bien été envoyé.<br />Nous vous répondrons dès que possible.<br />L\'équipe de @equipe_site@', # NEW

	// T
	'titre' => 'Formulaire de contact', # NEW
	'titre_court' => 'Contact' # NEW
);

?>
