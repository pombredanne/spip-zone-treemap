<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'alerte_effacement' => 'Confirmez l\'effacement des données de configuration de contact.',
	'aucun_message' => 'Aucun message à afficher', # NEW

	// B
	'bouton_effacer' => 'Gommer', # NEW

	// C
	'champ_adresse' => 'Address', # MODIF
	'champ_civi_explication' => 'Réglages avancées du champ civilité', # NEW
	'champ_civi_mademoiselle_ok' => 'Proposer un choix « Mademoiselle » dans le champ civilit&eacute;', # NEW
	'champ_civilite' => 'Civility',
	'champ_code_postal' => 'Zip Code', # MODIF
	'champ_etat' => 'State',
	'champ_impose' => 'field imposed',
	'champ_infolettre' => 'Newsletter', # MODIF
	'champ_mail' => 'Mail', # MODIF
	'champ_nom' => 'Last Name', # MODIF
	'champ_obligatoire' => 'Mandatory ?', # MODIF
	'champ_organisation' => 'Organisation', # MODIF
	'champ_pays' => 'Country', # MODIF
	'champ_portable' => 'Mobile Phone Number', # MODIF
	'champ_prenom' => 'First Name',
	'champ_sujet' => 'Subject', # MODIF
	'champ_telephone' => 'Phone Number',
	'champ_texte' => 'Text', # MODIF
	'champ_ville' => 'City', # MODIF
	'configuration_barre_forum' => 'Afficher la barre typo des forums.', # NEW
	'configuration_barre_impossible' => 'Un autre plugin interdit l\'usage du porte-plume dans l\'espace public, les réglages ci-dessous seront sans effets.',
	'configuration_barre_oui' => 'View the complete bar typo.',
	'configuration_champs_message' => 'PenHolder Settings.',
	'configuration_champs_message_explication' => 'Typo toolbar settings, associated to the text\'s message.',
	'configuration_description' => 'Customize your contact form.', # MODIF
	'configuration_destinataires' => 'Recipients', # MODIF
	'configuration_destinataires_explication' => 'Choose one or more authors recipients your visitors may contact. When not defined, the first author will be used (the person who installed the website).',
	'configuration_destinataires_plusieurs' => 'Allow visitors to choose among <b>several</b> recipient authors',
	'configuration_destinataires_plusieurs_et' => 'Allow visitors to choose among <b>several</b> recipients from the selected list above andadd those from the group of authors eventually passed to the form.',
	'configuration_destinataires_plusieurs_ou' => 'Allow visitors to choose among <b>several</b> recipients from the selected list above or from the group of authors eventually passed to the form',
	'configuration_destinataires_tous' => 'Make <b>all</b> authors as contact email recipients', # MODIF
	'configuration_destinataires_tous_et' => 'Make <b>all</b> these authors as recipients of the email and add the group of authors passed to the form',
	'configuration_destinataires_tous_ou' => 'Make <b>all</b> these authors as recipients of the email or only those passed to the form',
	'configuration_destinataires_un' => 'Allow visitors to choose <b>a single</b> recipient author',
	'configuration_destinataires_un_et' => 'Force visitors to choose <b>a single</b> recipient from the selected list above andadd those from the group of authors eventually passed to the form.',
	'configuration_destinataires_un_ou' => 'Force visitors to choose <b>a single</b> recipient from the selected list above or from the group of authors eventually passed to the form',
	'configuration_intro' => 'Introduction', # MODIF
	'configuration_intro_explication' => 'You may here add a text that will introduce your contact form.',
	'configuration_no_barre' => 'Do not show typo toolbar.', # MODIF
	'configuration_no_previsualisation' => 'Show typo toolbar without preview.',
	'configuration_pj' => 'Attachement',
	'configuration_pj_autoriser' => 'Allow adding attachement',
	'configuration_pj_facteur_absent' => 'You have to install the Facteur plugin to use this functionality.', # MODIF
	'configuration_pj_nb_max' => 'Maximum number of attachments',
	'configuration_regle_liste_explication' => 'Select a calculation rule from the list of potential recipients and the selection method proposed to the visitors.',
	'configuration_texteinfolettre' => 'Texte infolettre', # NEW
	'configuration_texteinfolettre_explication' => 'Changez éventuellement le texte associé à la case à cocher infolettre.', # NEW
	'consulter_memoire' => 'This message is saved on your site:',
	'courriel_de' => 'From', # MODIF
	'courriel_pour' => 'To', # MODIF

	// D
	'deplacement_possible' => 'Vertical displacement by drag and drop',
	'description_menu_contact' => 'Élément de menu vers la page du formulaire de contact', # NEW
	'description_page-contact' => 'Acessible via <i>spip.php?page=contact</i>, cette page optionnelle permet de fournir un formulaire de contact (pensez à ajouter un lien dans vos menus).', # NEW

	// F
	'form_destinataire' => 'Choose a recipient', # MODIF
	'form_destinataires' => 'Choose recipients', # MODIF
	'form_pj_ajouter_pluriel' => 'Add attachements',
	'form_pj_ajouter_singulier' => 'Add attachement',
	'form_pj_fichier_ajoute' => 'The file has been added : ',
	'form_pj_importer' => 'Import a file', # MODIF
	'form_pj_previsu_pluriel' => 'Attachements',
	'form_pj_previsu_singulier' => 'Attachement',
	'form_pj_supprimer' => 'Delete', # MODIF
	'form_pj_titre' => 'File title', # MODIF
	'forum_attention_nbre_caracteres' => '<b>Warning!</b> Your message needs to be at least @nbre_caract@ characters in length.',

	// H
	'horodatage' => 'Form posted on: @horodatage@.', # MODIF

	// I
	'inforigine' => 'Information from the form: ', # MODIF
	'informez_moi' => 'I agree to receive, by e-mail, information from your organisation.',
	'infos_collecte' => 'Information collected',
	'infos_supplementaires' => 'Additional Infos',

	// M
	'madame' => 'Madam', # MODIF
	'mademoiselle' => 'Mademoiselle', # NEW
	'modele_label_auteur' => 'Pré-sélectionner un auteur (facultatif) ?', # NEW
	'modele_nom_formulaire' => 'le formulaire de contact avancé', # NEW
	'monsieur' => 'Mister', # MODIF
	'msg_accueil' => 'Here are messages from contacts you have received.',
	'msg_contact' => 'Message from contact', # MODIF
	'msg_expediteur' => 'Sender: ',
	'msg_lus' => 'Messages already read',
	'msg_messagerie' => 'Contact messaging', # MODIF
	'msg_nouveaux' => 'New messages', # MODIF
	'msg_pas_nouveaux' => 'You havn\'t new messages.', # MODIF
	'msg_revenir_accueil' => 'Return to the list of contact messages.',
	'msg_supprimer_message' => 'Delete this message', # MODIF

	// N
	'noisette_label_afficher_titre_noisette' => 'Afficher un titre ?', # NEW
	'noisette_label_titre_noisette' => 'Titre :', # NEW
	'noisette_label_titre_noisette_perso' => 'Si titre personnalisé :', # NEW
	'noisette_titre_perso' => 'Titre personnalisé', # NEW

	// P
	'par' => 'Par : ', # NEW
	'preselection' => 'Décocher tous les items de la liste de destinataires', # NEW

	// S
	'sauv_message' => 'Save contact messages', # MODIF
	'sauv_message_explication' => 'Allow to save messages from contacts, and display them in a dedicated interface.',
	'sauv_message_ok' => 'Save contact messages', # MODIF
	'succes' => 'Thank you, your message has been sent.<br />We will reply as soon as possible.<br />The team of @equipe_site@',

	// T
	'titre' => 'Contact Form', # MODIF
	'titre_court' => 'Contact' # NE
);

?>
