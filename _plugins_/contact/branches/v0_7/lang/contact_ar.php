<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'alerte_effacement' => 'تأكيد حذف معلومات إعداد الاتصال',
	'aucun_message' => 'لا توجد رسائل للعرض',

	// B
	'bouton_effacer' => 'محو',

	// C
	'champ_adresse' => 'العنوان',
	'champ_civi_explication' => 'الإعدادات المتقدمة لحقل الصفة',
	'champ_civi_mademoiselle_ok' => 'اقتراح خيار «آنسة» في حقل الصفة',
	'champ_civilite' => 'الصفة',
	'champ_code_postal' => 'الرمز البريدي',
	'champ_etat' => 'الولاية',
	'champ_impose' => 'حقل الزامي',
	'champ_infolettre' => 'النشرة الإخبارية',
	'champ_mail' => 'البريد الإلكتروني',
	'champ_nom' => 'اللقب',
	'champ_obligatoire' => 'إلزامية؟',
	'champ_organisation' => 'المنظمة',
	'champ_pays' => 'البلد',
	'champ_portable' => 'المحمول',
	'champ_prenom' => 'الاسم',
	'champ_sujet' => 'الموضوع',
	'champ_telephone' => 'الهاتف',
	'champ_texte' => 'النص',
	'champ_ville' => 'المدينة',
	'configuration_barre_forum' => 'Afficher la barre typo des forums.', # NEW
	'configuration_barre_impossible' => 'هناك ملحق آخر يمنع استخدام الريشة في الموقع العمومي. ستبقى الاعدادات أدناه دون اي مفعول.',
	'configuration_barre_oui' => 'عرض شريط كامل للتصحيح المطبعي.',
	'configuration_champs_message' => 'إعدادات حاملة الريشة.',
	'configuration_champs_message_explication' => 'إعدادات شريط للتصحيح المطبعي المرتبط بنص الرسالة.',
	'configuration_description' => 'السماح لإدارة إستمارة الإتصال.',
	'configuration_destinataires' => 'المستلمين',
	'configuration_destinataires_explication' => 'اختيار واحد أو أكثر من الناشرين بحيث يمكن للزوار الاتصال بهم. لا شيء محدد، ثم سيكون الناشر رقم 1  (ثبت  الموقع) سيتم استخدامه.',
	'configuration_destinataires_plusieurs' => 'يسمح للزوار اختيار <b>كثير</b> بين المستلمين',
	'configuration_destinataires_plusieurs_et' => 'يسمح للزوار اختيار <b>كثير</b> المستلمين من بين القائمة المحددة أعلاه ، و إضافة مجموعة المبعوثون للإستمارة.',
	'configuration_destinataires_plusieurs_ou' => 'يسمح للزوار اختيار <b>كثير</b> المستلمين من بين
 القائمة المحددة أعلاه ، أو من مجموعة المبعوثون للإستمارة',
	'configuration_destinataires_tous' => 'وضع <b>جميع</b> الناشرين كمستلمين  للبريد الالكتروني',
	'configuration_destinataires_tous_et' => 'وضع <b>جميع</b> هؤلاءالناشرين كمستلمين للبريدالالكتروني و إضافة مجموعة المبعوثون للإستمارة',
	'configuration_destinataires_tous_ou' => 'وضع <b>جميع</b> هؤلاءالناشرين كمستلمين للبريدالالكتروني أو فقط المبعوثون للإستمارة',
	'configuration_destinataires_un' => 'يسمح للزوار اختيار <b>واحد</b> بين المستلمين',
	'configuration_destinataires_un_et' => 'إجبار الزوار لاختيار <b>واحد</b> مستلم من القائمة المحددة أعلاه ، و إضافة مجموعة المبعوثون للإستمارة.',
	'configuration_destinataires_un_ou' => 'إجبار الزوار لاختيار <b>واحد</b> مستلم من القائمة المحددة أعلاه ، أو من مجموعة المبعوثون للإستمارة',
	'configuration_intro' => 'مقدمة',
	'configuration_intro_explication' => 'أدخل أي النص الذي سيعرض الإستمارة',
	'configuration_no_barre' => 'بدون شريط للتصحيح المطبعي .',
	'configuration_no_previsualisation' => 'عرض شريط للتصحيح المطبعي دون معاينة.',
	'configuration_pj' => 'المرفقات',
	'configuration_pj_autoriser' => 'السماح لإرسال المرفقات',
	'configuration_pj_facteur_absent' => 'لاستخدام هذه الميزة ، يجب تثبيت البرنامج المساعد Facteur.',
	'configuration_pj_nb_max' => 'الحد الأقصى لعدد المرفقات',
	'configuration_regle_liste_explication' => 'اختيار طريقة حساب قائمة المستلمين المحتملين وطريقة الاختيار المقترحة للزائر.',
	'configuration_texteinfolettre' => 'Texte infolettre', # NEW
	'configuration_texteinfolettre_explication' => 'Changez éventuellement le texte associé à la case à cocher infolettre.', # NEW
	'consulter_memoire' => 'يتم تسجيل هذه الرسالة على موقعك :',
	'courriel_de' => 'من',
	'courriel_pour' => 'لأجل',

	// D
	'deplacement_possible' => 'الحركة العمودية بواسطة السحب والإسقاط',
	'description_menu_contact' => 'عنصر القائمة إلى صفحة إستمارة الاتصال',
	'description_page-contact' => 'Acessible via <i>spip.php?page=contact</i>, cette page optionnelle permet de fournir un formulaire de contact (pensez à ajouter un lien dans vos menus).', # NEW

	// F
	'form_destinataire' => 'اختيار المستلم',
	'form_destinataires' => 'اختيار المستلمين',
	'form_pj_ajouter_pluriel' => 'إضافة مرفقات',
	'form_pj_ajouter_singulier' => 'إضافة مرفق',
	'form_pj_fichier_ajoute' => 'الملف قد أضيف : ',
	'form_pj_importer' => 'استيراد ملف',
	'form_pj_previsu_pluriel' => 'المرفقات',
	'form_pj_previsu_singulier' => 'المرفق',
	'form_pj_supprimer' => 'حذف',
	'form_pj_titre' => 'عنوان الملف',
	'forum_attention_nbre_caracteres' => '<b>إنتباه !</b> رسالتك يجب أن تحتوي على الأقل @nbre_caract@ الأحرف.',

	// H
	'horodatage' => 'إستمارة نشرت في : @horodatage@.',

	// I
	'inforigine' => 'المعلومات المرسلة من خلال الإستمارة : ',
	'informez_moi' => 'أوافق على استلام بواسطة البريد الإلكتروني ، معلومات عن منظمتكم.',
	'infos_collecte' => 'المعلومات التي يتم جمعها',
	'infos_supplementaires' => 'معلومات إضافية',

	// M
	'madame' => 'السيدة',
	'mademoiselle' => 'انسة',
	'modele_label_auteur' => 'Pré-sélectionner un auteur (facultatif) ?', # NEW
	'modele_nom_formulaire' => 'le formulaire de contact avancé', # NEW
	'monsieur' => 'السيد',
	'msg_accueil' => 'ستجد هنا رسائل الاتصال المتلقاة.',
	'msg_contact' => 'رسالة الاتصال',
	'msg_expediteur' => 'مرسل : ',
	'msg_lus' => 'رسائل مقروءة',
	'msg_messagerie' => 'بريد الاتصال',
	'msg_nouveaux' => 'رسائل جديدة',
	'msg_pas_nouveaux' => 'لا يوجد لديك رسائل جديدة.',
	'msg_revenir_accueil' => 'العودة إلى قائمة الرسائل المتلقاة.',
	'msg_supprimer_message' => 'حذف الرسالة',

	// N
	'noisette_label_afficher_titre_noisette' => 'Afficher un titre ?', # NEW
	'noisette_label_titre_noisette' => 'Titre :', # NEW
	'noisette_label_titre_noisette_perso' => 'Si titre personnalisé :', # NEW
	'noisette_titre_perso' => 'Titre personnalisé', # NEW

	// P
	'par' => 'Par : ', # NEW
	'preselection' => 'قم بإلغاء كافة العناصر الموجودة في قائمة المستلمين',

	// S
	'sauv_message' => 'حفظ رسائل الاتصالات',
	'sauv_message_explication' => 'تسمح لك لتسجيل رسائل الاتصالات من أجل عرضها في بريد مكرس لها.',
	'sauv_message_ok' => 'حفظ رسائل الاتصالات',
	'succes' => 'شكرا لك ، الرسالة قد أرسلت.<br />سوف نقوم بالرد في أقرب وقت ممكن.<br />مجموعة @equipe_site@',

	// T
	'titre' => 'إستمارة الإتصال',
	'titre_court' => 'الاتصال'
);

?>
