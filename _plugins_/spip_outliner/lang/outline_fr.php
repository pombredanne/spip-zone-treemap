<?php
/*
 * Spip SMS Liste
 * Gestion de liste de diffusion de SMS
 *
 * Auteur :
 * Cedric Morin
 * (c) 2007 - Distribue sous licence GNU/GPL
 *
 */

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// page tables_tous
'icone_creer_table' => "Cr&eacute;er une nouvel Outline",
'type_des_tables' => 'Outline',
'toutes_tables' => "Tous les Outlines",

// page donnees_tous
'icone_ajouter_donnees' => "Cr&eacute;er des lignes",
'telecharger_reponses' => "Exporter l'Outline en CSV",
'importer_donnees_csv' => "Importer un Outline en CSV",

// page donnees_edit
'lien_retirer_donnee_liee' => 'Retirer cette ligne',
'lien_retirer_donnee_liante' => 'Retirer cette ligne',
'articles_utilisant'=> "Articles li&eacute; a cette ligne",
'suivi_reponses' => "Voir les lignes",
'aucune_reponse' => "Aucune ligne",
'une_reponse' => "Une ligne",
'nombre_reponses' => "@nombre@ lignes",
'texte_donnee_statut' => "Statut de cette ligne",
'texte_statut_prepa' => "En cours de r&eacute;daction",
'texte_statut_prop' => "Propos&eacute;",
'texte_statut_publie' => "Publi&eacute;",
'texte_statut_poubelle' => "Supprim&eacute;",
'texte_statut_refuse' => "Refus&eacute;",

// page forms_edit
'champs_formulaire'=>"Colonnes de l'Outline",
'info_supprimer_formulaire' => "Voulez-vous vraiment supprimer cet Outline ?",
'info_supprimer_formulaire_reponses' => "Cet Outline contient des lignes. Voulez-vous vraiment le supprimer ?",

'formulaire' => "Outline",
'nouveau_formulaire' => "Nouvel Outline",
'supprimer_formulaire' => "Supprimer cet Outline",
'titre_formulaire' => "Titre de l'Outline",



);


?>
