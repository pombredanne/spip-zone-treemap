<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/mesfavoris?lang_cible=ru
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'add' => 'Добавить',
	'add_to' => 'Добавить в мою подборку',
	'ajoute_le' => 'Добавлено',

	// C
	'configurer' => 'конфигурировать',

	// I
	'info_nombre_favoris_objet' => 'Ajouté @nb@ fois en favori.', # Pas utile dans le plugin lui-même mais peut l'être dans les squelettes NEW

	// L
	'label_style_formulaire16' => 'Маленький (16px)',
	'label_style_formulaire24' => 'Средний (24px)',
	'label_style_formulaire32' => 'Большой (32px)',
	'legend_formulaire_public' => 'Публичный бланк',
	'legend_style_formulaire' => 'Стиль бланка',
	'login_first' => 'Извините, чтобы выполнить это действие, Вам надо зарегистрироваться',

	// M
	'mes_favoris' => 'Избранное',

	// R
	'remove' => 'Убрать',
	'remove_from' => 'Убрать из моей подборки',

	// S
	'ses_favoris' => 'Ses favoris', # NEW

	// V
	'vos_favoris' => 'Vos favoris' # Utile pour modifier le titre de l'inclusion NEW
);

?>
