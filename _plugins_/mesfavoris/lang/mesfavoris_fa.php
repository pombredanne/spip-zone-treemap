<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/mesfavoris?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'add' => 'افزودن',
	'add_to' => 'افزودن به گزينه‌هاي من',
	'ajoute_le' => 'افزوده به',

	// C
	'configurer' => 'پيكربندي',

	// I
	'info_nombre_favoris_objet' => 'Ajouté @nb@ fois en favori.', # Pas utile dans le plugin lui-même mais peut l'être dans les squelettes NEW

	// L
	'label_style_formulaire16' => 'كوچك (160 پيكسل)',
	'label_style_formulaire24' => 'متوسط (240 پيكسل)',
	'label_style_formulaire32' => 'بزرگ (320 پيكسل)',
	'legend_formulaire_public' => 'فرم همگاني',
	'legend_style_formulaire' => 'سبك فرم',
	'login_first' => 'متأسفانه اين كار مستلزم وصل شدن است، اول متصل شويد',

	// M
	'mes_favoris' => 'دوست داشتني‌هاي من',

	// R
	'remove' => 'درآوردن',
	'remove_from' => 'حذف از گزينه‌هاي من',

	// S
	'ses_favoris' => 'دوستداشتني‌هاي او ',

	// V
	'vos_favoris' => 'Vos favoris' # Utile pour modifier le titre de l'inclusion NEW
);

?>
