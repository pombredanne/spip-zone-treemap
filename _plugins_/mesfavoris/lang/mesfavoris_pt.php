<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/mesfavoris?lang_cible=pt
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'add' => 'Acrescentar',
	'add_to' => 'Acrescentar à minha seleção',
	'ajoute_le' => 'Acrescentado no',

	// C
	'configurer' => 'Configurar',

	// I
	'info_nombre_favoris_objet' => 'Ajouté @nb@ fois en favori.', # Pas utile dans le plugin lui-même mais peut l'être dans les squelettes NEW

	// L
	'label_style_formulaire16' => 'Pequeno (16px)',
	'label_style_formulaire24' => 'Médio (24px)',
	'label_style_formulaire32' => 'Grande (32px)',
	'legend_formulaire_public' => 'Formulário público',
	'legend_style_formulaire' => 'Estilo do formulário',
	'login_first' => 'Lamentamos mas a operação requer o seu registo, autentifique-se primeiro',

	// M
	'mes_favoris' => 'Os meus favoritos',

	// R
	'remove' => 'Retirar',
	'remove_from' => 'Retirar da minha seleção',

	// S
	'ses_favoris' => 'Ses favoris', # NEW

	// V
	'vos_favoris' => 'Vos favoris' # Utile pour modifier le titre de l'inclusion NEW
);

?>
