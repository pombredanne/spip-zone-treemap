<?php


	/**
	 * SPIP-Formulaires
	 *
	 * Copyright (c) 2006-2009
	 * Agence Artégo http://www.artego.fr
	 *  
	 * Ce programme est un logiciel libre distribue sous licence GNU/GPLv3.
	 * Pour plus de details voir http://www.gnu.org/licenses/gpl-3.0.html
	 *  
	 **/


	$GLOBALS[$GLOBALS['idx_lang']] = array(

		'un_internaute_a_valide_le_formulaire' => 'Un internaute a validé le formulaire ',
		'vous_avez_completer_le_formulaire' => 'Vous avez complèté le formulaire ',
		'types_fichier_autorises' => "Types de fichier autorisés",

		'nouveau_mot_de_passe' => "Nouveau mot de passe",
		'voici_votre_nouveau_mot_de_passe' => "Voici votre nouveau mot de passe",

		'merci' => 'Merci',
		'merci_note' => 'Les données de votre formulaire ont été sauvegardées.',
		'message_envoye' => "Message envoyé !",
		'bouton_valider' => "Valider",
		'valider' => "Valider",
		'retour' => "Retour",

		'controle_non_vide' => "Vous devez répondre à cette question",
		'controle_email' => "Le format de cet email n'est pas valide",
		'controle_email_applicant' => "Le format de cet email n'est pas valide ou cet email est déjà utilisé",
		'controle_url' => "Le format de ce lien n'est pas valide, il doit être de la forme http://...",
		'controle_nombre' => "Votre réponse doit être un nombre",
		'controle_date' => "Votre réponse doit être de la forme jj/mm/aaaa",
		'controle_fichier' => "Upload obligatoire. Vérifiez le type et la taille de votre fichier.",
		
		// formulaire login
		'login_formulaire' => "Identification",
		'vous_devez_etre_identifies' => "Vous devez être identifiés pour accéder à cette partie du site",
		'bad_pass' => "Le couple email / mot de passe que vous avez saisi est incorrect",
		'email' => "Votre email",
		'mdp' => "Votre mot de passe",
		'oubli_mdp' => "Mot de passe oublié ?",
		
		'cookie_ko' => "Votre identification a expiré. Vous devez accepter les cookies pour poursuivre.",
		
		// formulaire oubli
		'oubli_mdp' => "Récupérer un mot de passe oublié",
		'nouveau_mot_de_passe' => "Nouveau mot de passe",
		'email_inexistant' => "Cet email est mal formaté ou inconnu",
		'mdp_envoye' => "Mot de passe envoyé",
		
		// espace formulaire
		'bienvenue' => "Bienvenue",
		'deconnexion' => "Déconnexion",
		'espace_formulaire' => "Espace formulaire",
		'supprimer' => "Supprimer",
		'editer' => "Editer",
		'erreur_cookie' => "Votre navigateur doit accepter les cookies.",

		'Z' => 'ZZzZZzzz'

	);

?>