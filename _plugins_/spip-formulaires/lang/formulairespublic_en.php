<?php


	/**
	 * SPIP-Formulaires
	 *
	 * Copyright (c) 2006-2009
	 * Agence Artégo http://www.artego.fr
	 *  
	 * Ce programme est un logiciel libre distribue sous licence GNU/GPLv3.
	 * Pour plus de details voir http://www.gnu.org/licenses/gpl-3.0.html
	 *  
	 **/


	$GLOBALS[$GLOBALS['idx_lang']] = array(

		'un_internaute_a_valide_le_formulaire' => 'One user submitted the form',
		'types_fichier_autorises' => "Filetypes you can upload are",

		'nouveau_mot_de_passe' => "New password",
		'voici_votre_nouveau_mot_de_passe' => "Here is your new password",

		'merci' => 'Thank you',
		'merci_note' => 'Your application has been saved.',
		'message_envoye' => "Message sent !",
		'bouton_valider' => "Submit",
		'valider' => "Submit",
		'retour' => "Back",

		'controle_non_vide' => "You must answer this question",
		'controle_email' => "The email is invalid",
		'controle_email_applicant' => "The email is invalid or it is already being used",
		'controle_url' => "The link is not valid, its format must be http://...",
		'controle_nombre' => "Your answer must be a number",
		'controle_date' => "The date is invalid, its format must be dd/mm/yyyy",
		'controle_fichier' => "You must upload a file",
		
		// formulaire login
		'login_formulaire' => "Authentication",
		'vous_devez_etre_identifies' => "You must be authenticated to access this content",
		'bad_pass' => "Your login/password is incorrect",
		'email' => "Your email",
		'mdp' => "Your password",
		'oubli_mdp' => "Forgotten password ?",
		
		'cookie_ko' => "Your session has expired. Your browser must have cookies enabled to continue.",
		
		// formulaire oubli
		'oubli_mdp' => "Forgotten password",
		'nouveau_mot_de_passe' => "New password",
		'email_inexistant' => "This email is invalid or unknown",
		'mdp_envoye' => "Password sent",
		
		// espace applicant
		'bienvenue' => "Welcome",
		'deconnexion' => "Logout",
		'espace_formulaire' => "Form area",
		'supprimer' => "Delete",
		'editer' => "Edit",
		'erreur_cookie' => "Your browser must have cookies enabled.",

		'Z' => 'ZZzZZzzz'

	);

?>