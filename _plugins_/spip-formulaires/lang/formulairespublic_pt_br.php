<?php


	/**
	 * SPIP-Formulaires
	 *
	 * Copyright (c) 2006-2009
	 * Agence Artégo http://www.artego.fr
	 *  
	 * Ce programme est un logiciel libre distribue sous licence GNU/GPLv3.
	 * Pour plus de details voir http://www.gnu.org/licenses/gpl-3.0.html
	 *  
	 **/


	$GLOBALS[$GLOBALS['idx_lang']] = array(

		'un_internaute_a_valide_le_formulaire' => "Um internauta validou o formulário",

		'nouveau_mot_de_passe' => "Nova palavra-chave",
		'voici_votre_nouveau_mot_de_passe' => "Eis aqui a sua palavra-chave",

		'merci' => "Obrigado",
		'merci_note' => "Os dados do seu formulário foram registrados com sucesso",
		'message_envoye' => "Mensagem enviada !",
		'bouton_valider' => "Validar",
		'valider' => "Validar",
		'retour' => "Retornar",

		'controle_non_vide' => "Você deve responder a  essa pergunta",
		'controle_email' => "O formato desse e-mail não é válido",
		'controle_email_applicant' => "O formato desse e-mail não é válido ou o mesmo já está sendo utilizado",
		'controle_url' => "O formato desse link não é válido, ele deve ser no formato http://...",
		'controle_nombre' => "A sua resposta deve ser um número",
		'controle_date' => "A sua resposta deve ser da forma jj/mm/aaaa",
		'controle_fichier' => "Você deve fazer o upload do ficheiro",
		
		// formulaire login
		'login_formulaire' => "Identificação",
		'vous_devez_etre_identifies' => "Você deve ser identificado para acessar a essa página do site",
		'bad_pass' => "O vínculo entre o e-mail e a palavra-chave que você inseriu está incorreto",
		'email' => "E-mail",
		'mdp' => "Palavra-chave",
		'oubli_mdp' => "Esqueceu a sua palavra-chave ?",
		
		'cookie_ko' => "A sua identificação expirou. Você deve aceitar os cookies para continuar.",
		
		// formulaire oubli
		'oubli_mdp' => "Recuperar palavra-chave esquecida",
		'nouveau_mot_de_passe' => "Nova palavra-chave",
		'email_inexistant' => "Este e-mail está incorreto ou não existe",
		'mdp_envoye' => "palavra-chave enviada",
		
		// espace applicant
		'bienvenue' => "Bem-vindo",
		'deconnexion' => "Desconexão",
		
		'supprimer' => "Suprimir",

		'Z' => 'ZZzZZzzz'

	);

?>