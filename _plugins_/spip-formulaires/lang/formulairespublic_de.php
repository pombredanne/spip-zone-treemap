<?php


	/**
	 * SPIP-Formulaires
	 *
	 * Copyright (c) 2006-2009
	 * Agence Art�go http://www.artego.fr
	 *  
	 * Ce programme est un logiciel libre distribue sous licence GNU/GPLv3.
	 * Pour plus de details voir http://www.gnu.org/licenses/gpl-3.0.html
	 *  
	 **/


	$GLOBALS[$GLOBALS['idx_lang']] = array(

		'un_internaute_a_valide_le_formulaire' => 'Un internaute a valid� le formulaire ',

		'nouveau_mot_de_passe' => "Nouveau mot de passe",
		'voici_votre_nouveau_mot_de_passe' => "Voici votre nouveau mot de passe",

		'merci' => 'Merci',
		'merci_note' => 'Les donn�es de votre formulaire ont �t� sauvegard�es.',
		'message_envoye' => "Message sent !",
		'bouton_valider' => "schicken",
		'valider' => "schicken",
		'retour' => "Retour",

		'controle_non_vide' => "Vous devez r�pondre � cette question",
		'controle_email' => "Le format de cet email n'est pas valide",
		'controle_email_applicant' => "Le format de cet email n'est pas valide ou cet email est d�j� utilis�",
		'controle_url' => "Le format de ce lien n'est pas valide, il doit �tre de la forme http://...",
		'controle_nombre' => "Votre r�ponse doit �tre un nombre",
		'controle_date' => "Votre r�ponse doit �tre de la forme jj/mm/aaaa",
		'controle_fichier' => "Vous devez uploader un fichier",
		
		// formulaire login
		'login_formulaire' => "Identification",
		'vous_devez_etre_identifies' => "Vous devez �tre identifi�s pour acc�der � cette partie du site",
		'bad_pass' => "Le couple email / mot de passe que vous avez saisi est incorrect",
		'email' => "Votre email",
		'mdp' => "Votre mot de passe",
		'oubli_mdp' => "Mot de passe oubli� ?",
		
		'cookie_ko' => "Votre identification a expir�. Vous devez accepter les cookies pour poursuivre.",
		
		// formulaire oubli
		'oubli_mdp' => "R�cup�rer un mot de passe oubli�",
		'nouveau_mot_de_passe' => "Nouveau mot de passe",
		'email_inexistant' => "Cet email est mal format� ou inconnu",
		'mdp_envoye' => "Mot de passe envoy�",
		
		// espace applicant
		'bienvenue' => "Bienvenue",
		'deconnexion' => "D�connexion",
		
		'supprimer' => "Supprimer",

		'Z' => 'ZZzZZzzz'

	);

?>