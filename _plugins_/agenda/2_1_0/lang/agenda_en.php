<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'absence_prise_en_compte' => 'Your absence is recorded',
	'activite_editoriale' => 'Editorial activity',
	'afficher_calendrier' => 'Show the calendar',
	'agenda' => 'Agenda',
	'ajouter_un_evenement' => 'Add one event to this article',
	'ajouter_repetition' => 'Add repetitions',
	'ajouter_mots_cles' => 'Add keywords',
	'annee_precedente' => 'previous year',
	'annee_suivante' => 'next year',
	'aucun_evenement' => 'no event',
	'aucune_rubrique_mode_agenda' => 'By default, all sections can use the events. If you activate the agenda mode on one or more sections, event management will be limited in its subtree.',

	// B
	'bouton_annuler' => 'Cancel',
	'bouton_supprimer' => "Delete",
	
	// C
	'confirm_suppression_inscription' => "Are you sure you want to delete this registration?",
	'creer_evenement' => "Create an event",

	// E
	'erreur_article_manquant' => 'You should link to an article',
	'erreur_article_interdit' => 'You have no right to associate this event to this article',
	'erreur_date' => 'This date is incorrect',
	'erreur_heure' => 'This hour is incorrect',
	'erreur_date_corrigee' => 'The date has been corrected',
	'erreur_heure_corrigee' => 'The hour has been corrected',
	'erreur_date_avant_apres' => 'Please enter an end date after the date of beginning.',
	'evenement_adresse' => 'Address',
	'evenement_article' => 'Link to the article',
	'evenement_autres_occurences' => "Other occurences :",
	'evenement_date' => 'Date',
	'evenement_date_a' => 'to ',
	'evenement_date_au' => 'To ',
	'evenement_date_a_immediat' => 'at ',
	'evenement_date_de' => 'From ',
	'evenement_date_debut' => 'Starting date',
	'evenement_date_du' => 'From ',
	'evenement_date_fin' => 'Ending date',
	'evenement_descriptif' => 'Description',
	'evenement_horaire' => 'All day',
	'evenement_lieu' => 'Location',
	'evenement_titre' => 'Title',
	'evenement_repetitions' => 'Repetition',
	'evenements' => 'Events',
	'evenements_a_venir'=>'Next',
	'evenements_depuis_debut'=>'All',

	// F
	'fermer' => 'close',

	// G
	'groupes_mots'=>'Keywords groups',

	// I
	'icone_creer_evenement' => 'Generate a new event',
	'item_mots_cles_association_evenements' => 'events',
	'indiquez_votre_choix' => 'Indicate your choice',
	'info_aucun_evenement' => 'No event',
	'info_evenements' => 'Events',
	'info_nombre_evenements' => '@nb@ events',
	'info_un_evenement' => 'One event',
	'inscrits' => 'Registrations',

	// L
	'label_inscription' => 'Online registration',
	'label_places' => 'Limit the seats number',
	'label_reponse_jyparticipe' => 'I\'ll be there',
	'label_reponse_jyparticipe_peutetre' => 'Maybe I\'ll be there',
	'label_reponse_jyparticipe_pas' => 'I won\'t be there',
	'label_vous_inscrire' => 'Your participation',
	'lien_desinscrire' => 'Remove',
	'lien_retirer_evenement' => 'Deleted ',
	'liste_inscrits' => 'Registrations',

	// M
	'mois_precedent' => 'previous month',
	'mois_suivant' => 'next month',

	// N
	'nb_mots_clefs' => '@nb@&nbsp;keywords',
	'nb_repetitions' => '@nb@&nbsp;repetitions',

	// P
	'participation_prise_en_compte' => 'Your participation is recorded',
	'participation_incertaine_prise_en_compte' => 'Your possible participation is registered',
	'probleme_technique' => 'A technical problem occurred. Try again later.',

	// R
	'repetition_de' => "Repetition of",
	'rubriques' => 'Agenda sections',
	'rubrique_activer_agenda' => 'Activate the agenda for this section',
	'rubrique_dans_une_rubrique_mode_agenda' => 'This section allows you to use the events as it is in a section where agenda mode has been enabled',
	'rubrique_desactiver_agenda' => 'Disable agenda mode for this section',
	'rubrique_sans_gestion_evenement' => 'The agenda mode is not enabled for this section',
	'rubrique_mode_agenda' => 'The agenda mode is enabled for this section and its subtree',

	// S
	'sans_titre' => "(without title)",

	// T
	'telecharger' => 'Download',
	'texte_agenda' => 'AGENDA',
	'titre_cadre_ajouter_evenement' => 'Add one event',
	'titre_cadre_modifier_evenement' => 'Modify one event',
	'titre_sur_l_agenda' => "On agenda",
	'toutes_rubriques' => 'All',

	// U
	'un_mot_clef' => '1&nbsp;keyword',
	'une_repetition' => '1&nbsp;repetition',

	// V
	'voir_evenements_rubrique' => 'See this section\'s events',
);
?>