<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'bouton_annuler' => 'Anular',
'activite_editoriale' => 'Actividad editorial',
'ajouter_un_evenement' => 'A�adir un evento a �ste art�culo',
'texte_agenda' => 'AGENDA',
'titre_cadre_ajouter_evenement' => 'A�adir un evento',
'titre_cadre_modifier_evenement' => 'Modificar un evento',
'item_mots_cles_association_evenements' => 'a los eventos',
'info_un_evenement' => 'un evento,',
'info_nombre_evenements' => '@nb_evenements@ eventos,',
'info_evenements' => 'Eventos',
'evenements' => 'Eventos',
'evenement_titre' => 'Titulo',
'evenement_descriptif' => 'Descripci�n',
'evenement_lieu' => 'Lugar',
'evenement_date' => 'Fecha',
'evenement_date_de' => 'De ',
'evenement_date_du' => 'Del ',
'evenement_date_a' => 'a las ',
'evenement_date_au' => 'Hasta ',
'evenement_date_a_immediat' => 'a las ',
'evenement_date_debut' => 'Fecha de comienzo',
'evenement_date_fin' => 'Fecha de fin',
'lien_retirer_evenement' => 'Suprimir',
'evenement_horaire' => 'Horario',
'titre_sur_l_agenda' => "Sobre la agenda",
'icone_creer_evenement' => 'Creaci�n de un nuevo evento',
'evenement_repetitions' => 'Repeticiones',
'repetition_de' => "Repetici�n de",
'sans_titre' => "(con t�tulo)",
'evenement_autres_occurences' => "Otras ocurrencias :",
'bouton_supprimer' => "Suprimir",
);


?>