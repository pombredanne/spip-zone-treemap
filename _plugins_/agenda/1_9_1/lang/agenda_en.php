<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'bouton_annuler' => 'Escape',
'activite_editoriale' => 'Editorial activity',
'ajouter_un_evenement' => 'add one event to this article',
'texte_agenda' => 'DIARY',
'titre_cadre_ajouter_evenement' => 'Add one event',
'titre_cadre_modifier_evenement' => 'Modify one event',
'item_mots_cles_association_evenements' => 'to those events',
'info_un_evenement' => 'one event',
'info_nombre_evenements' => '@nb_evenements@ events',
'info_evenements' => 'Events',
'evenements' => 'Events',
'evenement_titre' => 'Title',
'evenement_descriptif' => 'Description',
'evenement_lieu' => 'Location',
'evenement_date' => 'Date',
'evenement_date_de' => 'From ',
'evenement_date_du' => 'From ',
'evenement_date_a' => 'to ',
'evenement_date_au' => 'To ',
'evenement_date_a_immediat' => 'at ',
'evenement_date_debut' => 'Starting date',
'evenement_date_fin' => 'Ending date',
'lien_retirer_evenement' => 'Deleted ',
'evenement_horaire' => 'Schedule',
'titre_sur_l_agenda' => "On diary",
'icone_creer_evenement' => 'Generate a new event',
'evenement_repetitions' => 'Repetition',
'repetition_de' => "Repetition of",
'sans_titre' => "(without title)",
'evenement_autres_occurences' => "Other occurences :",
'bouton_supprimer' => "Delete",
);


?>