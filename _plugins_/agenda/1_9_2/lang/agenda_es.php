<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'bouton_annuler' => 'Cancelar',
'activite_editoriale' => 'Actividad editorial',
'ajouter_un_evenement' => 'A&ntilde;adir un evento a &eacute;ste art&iacute;culo',
'texte_agenda' => 'AGENDA',
'titre_cadre_ajouter_evenement' => 'A&ntilde;adir un evento',
'titre_cadre_modifier_evenement' => 'Modificar un evento',
'item_mots_cles_association_evenements' => 'a los eventos',
'info_un_evenement' => 'un evento,',
'info_nombre_evenements' => '@nb_evenements@ eventos,',
'info_evenements' => 'Eventos',
'evenements' => 'Eventos',
'evenement_titre' => 'T&iacute;tulo',
'evenement_descriptif' => 'Descripci&oacute;n',
'evenement_lieu' => 'Lugar',
'evenement_date' => 'Fecha',
'evenement_date_de' => 'De ',
'evenement_date_du' => 'Del ',
'evenement_date_a' => 'a las ',
'evenement_date_au' => 'Hasta ',
'evenement_date_a_immediat' => 'a las ',
'evenement_date_debut' => 'Fecha de comienzo',
'evenement_date_fin' => 'Fecha de fin',
'lien_retirer_evenement' => 'Suprimir',
'evenement_horaire' => 'Horario',
'titre_sur_l_agenda' => "Sobre la agenda",
'icone_creer_evenement' => 'Creaci&oacute;n de un nuevo evento',
'evenement_repetitions' => 'Repeticiones',
'nb_repetitions' => '@nb@&nbsp;repeticiones',
'une_repetition' => '1&nbsp;repetici&oacute;n',
'repetition_de' => "Repetici&oacute;n de",
'nb_mots_clefs' => '@nb@&nbsp;palabras&nbsp;clave',
'un_mot_clef' => '1&nbsp;palabra&nbsp;clave',
'sans_titre' => "(con t&iacute;tulo)",
'evenement_autres_occurences' => "Otras ocurrencias:",
'bouton_supprimer' => "Suprimir",
);

?>
