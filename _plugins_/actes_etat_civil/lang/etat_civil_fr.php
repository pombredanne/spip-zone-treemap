<?php
// Ceci est un fichier langue de SPIP
$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
'actes_etat_civil' => 'Actes d\'état civil',
'acte_naissance' => 'Acte de Naissance',
'acte_etat_civil_1' => 'Copie intégrale d\'acte de naissance',	
'acte_etat_civil_2' => 'Extrait d\'acte de naissance avec filiation',
'acte_etat_civil_3' => 'Extrait d\'acte de naissance sans filiation',
'acte_etat_civil_4' => 'Extrait d\'acte de naissance plurilingue',

'acte_mariage' => 'Acte de mariage',
'acte_etat_civil_5' => 'Copie intégrale d\'acte de mariage',	
'acte_etat_civil_6' => 'Extrait d\'acte de mariage avec filiation',
'acte_etat_civil_7' => 'Extrait d\'acte de mariage sans filiation',
'acte_etat_civil_8' => 'Extrait d\'acte de mariage plurilingue',
 
'acte_deces' => 'Acte de décès',
'acte_etat_civil_9' => 'Copie intégrale d\'acte de décès',	
'acte_etat_civil_10' => 'Extrait d\'acte de décès plurilingue',

'absence_filiation_pere' => 'Absence de filiation du père',
'absence_filiation_mere' => 'Absence de filiation de la mère',

'adresse1' => 'Immeuble, bâtiment, résidence,…',
'adresse2' => 'Numéro, rue',
'adresse3' => 'Lieu-dit, boîte postale,…',

// aide
'aide_personne_nom' => 'Nom de la personne concernée (nom de jeune fille pour les femmes).',
'aide_personne_prenom' => 'Prénom de la personne concernée.',
'aide_personne_date_evenement' => 'Date sous la forme JJ/MM/AAAA.',
'aide_personne_commune_evenement' => 'La demande doit être faite dans la commune de l\'événement.',
'aide_personne_nom_pere' => 'Nom du père.',
'aide_personne_prenom_pere' => 'Prénom du père.',
'aide_personne_absence_pere' => 'Pour les personnes de père inconnu ou nées sous X cocher cette case.',
'aide_personne_nom_mere' => 'Nom de jeune fille de la mère pour les femmes mariées ou divorcées.',
'aide_personne_prenom_mere' => 'Prénom de la mère.',
'aide_personne_absence_mere' => 'Pour les personnes de mère inconnue ou nées sous X cocher cette case.',


// C
'caracteres_requis' => 'caractères requis !',
'chiffres_requis' => 'chiffres requis !',
'code_postal' => 'Code postal',
'confirmer_demande' => 'Confirmer la demande',
'confirmation_demande_objet' => '[Accusé de réception]',
'confirmation_demande_body' => 'Madame, Monsieur'."\n\n".
	'Nous accusons réception de votre demande d\'acte d\'état civil.'."\n".
	'Celui-ci vous sera adressé prochainement par voie postale.',
'commune_naissance' => 'Localité de naissance',
'commune_mariage' => 'Localité du mariage',
'commune_deces' => 'Localité du décès',

// D
'demande_acte_etat_civil' => 'Demande d\'acte d\'état civil',
'demande_de' => 'Demande de',
'demandeur' => 'Demandeur',
'date_naissance' => 'Date de naissance <em>(ex 15/3/1967)</em>',
'date_deces' => 'Date de décès <em>(ex 15/3/1967)</em>',
'date_mariage' => 'Date de mariage <em>(ex 15/3/1967)</em>',
'decede_le' => 'Décédé(e) le',
'date_incorrecte' => 'Date incorrecte !',

// E
'etoile' => '<span>*</span>',

'explication_service' => 'Toute personne majeure ou émancipée peut obtenir, si sa qualité le permet, une copie intégrale ou un extrait avec filiation d\'un acte d\'état civil en fournissant les éléments concernant la personne faisant l\'objet de la demande.',

'explications_type_acte' => 'Sélectionnez un type d\'acte dans la liste ci-dessous et indiquez éventuellement votre lien de parenté avec la personne concernée par l\'acte.<br /><strong>Important !</strong> L\'événement (naissance, mariage ou décès) faisant l\'objet de l\'acte demandé doit avoir été enregistré dans la commune de ',

'explications_personne_concerne_acte' => 'Indiquez les renseignements sur la personne concernée par l\'acte.<br /><em>(Nom = nom de jeune fille pour les femmes mariées ou divorcées)</em>.',

'explications_personne_concerne_mere' => 'Nom = nom de jeune fille pour les femmes mariées ou divorcées.',

'explications_demandeur' => 'Indiquez votre identité et vos coordonnées (adresse à laquelle les documents seront envoyés).',
'explications_validation' =>'Vérifier les renseignements et confirmez votre demande.',
'email_non_valide' => 'Adresse électronique non valide !',
'email' => 'Adresse électronique',
'envoi_via' => 'Envoi via',
'etape' => 'Étape',

// G
'generer_accuse_reception' => 'Pour envoyer un accusé de réception de la demande, cliquez ci-dessous.',

// I
'info_obligatoire' => 'Ce champ doit être renseigné !',
'indiquer_lien_parent' => 'Indiquez un lien de parenté !',
'indiquer_mail_ou_tel' => 'Indiquez votre adresse électronique ou votre téléphone !',

// L
'lien_parent' => 'Lien de parenté',
'lien_parent_0' => 'Sélectionnez un lien de parenté',
'lien_parent_1' => 'La personne concernée par l\'acte',
'lien_parent_2' => 'Son père ou sa mère',
'lien_parent_3' => 'Son conjoint ou sa conjointe',
'lien_parent_4' => 'Son fils ou sa fille',
'lien_parent_5' => 'Son frère ou sa soeur',
'lien_parent_6' => 'Son grand-père ou sa grand-mère',
'lien_parent_7' => 'Son petit-fils ou sa petite-fille',
'lien_parent_8' => 'Son représentant légal',
'lien_parent_9' => 'Son héritier',
'lien_parent_10' => 'Autre',

// M
'marie_le' => 'Mariée le',
'message_envoye' => 'Votre demande a été effectuée.<br />Si vous avez fourni une adresse électronique vous recevrez prochainement un accusé de réception.',
'mere' => 'Mère',
'modifier' => 'Modifier',

// N
'ne_le' => 'Né(e) le',
'nom' => 'Nom',

// O
'ou' => 'ou',
'objet_mel_prefix' => '[Demande acte état civil]',

// P
'pays' => 'Pays',
'par' => 'Par',
'prenom' => 'Prénom',
'personne_concerne_acte' => 'Personne concernée par l\'acte',
'personne_concerne_acte_pere' => 'Père de la personne concernée',
'personne_concerne_acte_mere' => 'Mère de la personne concernée',
'pere' => 'Père',

// R
'remarque' => 'Remarque',
'renseignement_obligatoire' => '= renseignement obligatoire.',
'renseignements_demandeur' => 'Renseignements sur le demandeur',

// S
'selectionner_acte' => 'Sélectionnez un type d\'acte !',
'suite' => 'Suite &gt;',

// T
'type_acte' => 'Type d\'acte',
'telephone' => 'Téléphone',
'termine' => 'Cliquez ici pour terminer.',

// V
'validation_demande' => 'Validation de la demande',
'ville' => 'Ville',

);
?>
