<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-persona
// Langue: fr
// Date: 02-07-2012 17:46:57
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// B
	'persona_description' => 'persona est une méthode d’authentification proposée par Mozilla. Elle repose sur l’emploi de l’email de l’auteur comme clé de connexion (et non pas d’une URL comme pour OpenID). Ce plugin implémente persona dans SPIP : connexion, création de compte, signature des messages de forum ou des pétitions…

A noter : persona est une technologie expérimentale, et le plugin persona pour SPIP est lui aussi expérimental.',
	'persona_slogan' => 'Authentification des visiteurs via persona',
);
?>