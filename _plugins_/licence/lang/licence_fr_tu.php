<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/licence?lang_cible=fr_tu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_boite_licence' => 'Configuration du plugin licence',
	'cfg_descr_licence' => 'Des licences pour tes articles',
	'cfg_lbl_licence_defaut' => 'Licence par défaut',
	'cfg_titre_licence' => 'Licence',

	// D
	'description_art_libre' => 'licence Art libre',
	'description_cc0' => 'Creative Common - Contenu libre de tout droit', # NEW
	'description_cc_by' => 'Creative Commons - Paternité',
	'description_cc_by_nc' => 'Creative Commons - Paternité Pas d\'Utilisation Commerciale',
	'description_cc_by_nc_nd' => 'Creative Commons - Paternité Pas d\'Utilisation Commerciale Pas de Modification',
	'description_cc_by_nc_sa' => 'Creative Commons - Paternité Pas d\'Utilisation Commerciale Partage des Conditions Initiales à l\'Identique', # MODIF
	'description_cc_by_nd' => 'Creative Commons - Paternité pas de modification',
	'description_cc_by_sa' => 'Creative Commons - Paternité Partage des Conditions Initiales à l\'Identique', # MODIF
	'description_copyright' => '© copyright auteur de l\'article',
	'description_gpl' => 'licence GPL', # MODIF
	'description_wtfpl' => 'Licence Publique Rien À Branler', # NEW

	// L
	'label_select_licence' => 'Choisis une licence',
	'lien_art_libre' => 'http://artlibre.org/',
	'lien_cc0' => 'http://vvlibri.org/fr/licence/cc0/10/fr/legalcode', # NEW
	'lien_cc_by' => 'http://creativecommons.org/licenses/by/3.0/deed.fr',
	'lien_cc_by_nc' => 'http://creativecommons.org/licenses/by-nc/3.0/deed.fr',
	'lien_cc_by_nc_nd' => 'http://creativecommons.org/licenses/by-nc-nd/3.0/deed.fr',
	'lien_cc_by_nc_sa' => 'http://creativecommons.org/licenses/by-nc-sa/3.0/deed.fr',
	'lien_cc_by_nd' => 'http://creativecommons.org/licenses/by-nd/3.0/deed.fr',
	'lien_cc_by_sa' => 'http://creativecommons.org/licenses/by-sa/3.0/deed.fr',
	'lien_gfdl' => 'http://www.gnu.org/licenses/fdl.html', # NEW
	'lien_gpl' => 'http://www.gnu.org/copyleft/gpl.html',
	'lien_whfpl' => 'http://sam.zoy.org/lprab/', # NEW

	// N
	'noisette_cacher_defaut' => 'Cacher lorsque la licence n\'est pas spécifiée ?', # NEW
	'noisette_hauteur_logo' => 'Hauteur maximale du logo (en pixels) :',
	'noisette_largeur_logo' => 'Largeur maximale du logo (en pixels) :',
	'noisette_lien' => 'Afficher le lien vers la description de la licence ?',
	'noisette_logo' => 'Afficher le logo de la licence ?',
	'noisette_nom_licence' => 'Afficher le nom de la licence ?',

	// S
	'sans_licence' => 'Sans licence',

	// T
	'titre_art_libre' => 'LAL',
	'titre_cc0' => 'CC0', # NEW
	'titre_cc_by' => 'CC by',
	'titre_cc_by_nc' => 'CC by-nc',
	'titre_cc_by_nc_nd' => 'CC by-nc-nd',
	'titre_cc_by_nc_sa' => 'CC by-nc-sa',
	'titre_cc_by_nd' => 'CC by-nd',
	'titre_cc_by_sa' => 'CC by-sa',
	'titre_copyright' => 'Copyright',
	'titre_gfdl' => 'GNU FDL', # NEW
	'titre_gpl' => 'Gnu GPL', # MODIF
	'titre_wtfpl' => 'LPRAB' # NEW
);

?>
