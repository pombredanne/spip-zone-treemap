<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/licence?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_boite_licence' => 'Configurazione del plugin licenza',
	'cfg_descr_licence' => 'Licenze per i vostri articoli',
	'cfg_lbl_licence_defaut' => 'Licenza di default',
	'cfg_titre_licence' => 'Licenza',

	// D
	'description_art_libre' => 'Licenza Arte Libera',
	'description_cc0' => 'Creative Common - Contenu libre de tout droit', # NEW
	'description_cc_by' => 'Creative Commons - Attribuzione',
	'description_cc_by_nc' => 'Creative Commons - Attribuzione Non uso commerciale',
	'description_cc_by_nc_nd' => 'Creative Commons - Attribuzione Non uso commerciale Non opere derivate',
	'description_cc_by_nc_sa' => 'Creative Commons - Attribuzione Non uso commerciale Condividi allo stesso modo', # MODIF
	'description_cc_by_nd' => 'Creative Commons - Attribuzione Non opere derivate',
	'description_cc_by_sa' => 'Creative Commons - Attribuzione Condividi allo stesso modo', # MODIF
	'description_copyright' => '© copyright autore dell\'articolo',
	'description_gpl' => 'licenza GPL', # MODIF
	'description_wtfpl' => 'Licence Publique Rien À Branler', # NEW

	// L
	'label_select_licence' => 'Scegliete una licenza',
	'lien_art_libre' => 'http://artlibre.org/',
	'lien_cc0' => 'http://vvlibri.org/fr/licence/cc0/10/fr/legalcode', # NEW
	'lien_cc_by' => 'http://creativecommons.org/licenses/by/3.0/deed.it',
	'lien_cc_by_nc' => 'http://creativecommons.org/licenses/by-nc/3.0/deed.it',
	'lien_cc_by_nc_nd' => 'http://creativecommons.org/licenses/by-nc-nd/3.0/deed.it',
	'lien_cc_by_nc_sa' => 'http://creativecommons.org/licenses/by-nc-sa/3.0/deed.it',
	'lien_cc_by_nd' => 'http://creativecommons.org/licenses/by-nd/3.0/deed.it',
	'lien_cc_by_sa' => 'http://creativecommons.org/licenses/by-sa/3.0/deed.it',
	'lien_gfdl' => 'http://www.gnu.org/licenses/fdl.html', # NEW
	'lien_gpl' => 'http://www.gnu.org/copyleft/gpl.html',
	'lien_whfpl' => 'http://sam.zoy.org/lprab/', # NEW

	// N
	'noisette_cacher_defaut' => 'Cacher lorsque la licence n\'est pas spécifiée ?', # NEW
	'noisette_hauteur_logo' => 'Hauteur maximale du logo (en pixels) :', # NEW
	'noisette_largeur_logo' => 'Largeur maximale du logo (en pixels) :', # NEW
	'noisette_lien' => 'Afficher le lien vers la description de la licence ?', # NEW
	'noisette_logo' => 'Afficher le logo de la licence ?', # NEW
	'noisette_nom_licence' => 'Afficher le nom de la licence ?', # NEW

	// S
	'sans_licence' => 'Senza licenza',

	// T
	'titre_art_libre' => 'LAL',
	'titre_cc0' => 'CC0', # NEW
	'titre_cc_by' => 'CC by',
	'titre_cc_by_nc' => 'CC by-nc',
	'titre_cc_by_nc_nd' => 'CC by-nc-nd',
	'titre_cc_by_nc_sa' => 'CC by-nc-sa',
	'titre_cc_by_nd' => 'CC by-nd',
	'titre_cc_by_sa' => 'CC by-sa',
	'titre_copyright' => 'Copyright',
	'titre_gfdl' => 'GNU FDL', # NEW
	'titre_gpl' => 'Gnu GPL', # MODIF
	'titre_wtfpl' => 'LPRAB' # NEW
);

?>
