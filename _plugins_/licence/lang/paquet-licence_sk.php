<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-licence?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'licence_description' => 'Umožňuje prepojiť licenciu na používanie s článkom alebo dokumentom',
	'licence_nom' => 'Licencia',
	'licence_slogan' => 'Licencia pre články a dokumenty'
);

?>
