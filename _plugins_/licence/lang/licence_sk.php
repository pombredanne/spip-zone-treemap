<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/licence?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_boite_licence' => 'Nastavenia zásuvného modulu Licencia',
	'cfg_descr_licence' => 'Licencie pre vaše články',
	'cfg_lbl_licence_defaut' => 'Predvolená licencia',
	'cfg_titre_licence' => 'Licencia',

	// D
	'description_art_libre' => 'Licencia Free Art',
	'description_cc0' => 'Creative Common – obsah oslobodený od všetkých práv',
	'description_cc_by' => 'Licencia Creative Commons - Attribution',
	'description_cc_by_nc' => 'Licencia Creative Commons - Attribution Non-Commercial',
	'description_cc_by_nc_nd' => 'Licencia Creative Commons - Attribution Non-Commercial No Derivatives',
	'description_cc_by_nc_sa' => 'Licencia Creative Commons - Attribution Non-Commercial Share Alike',
	'description_cc_by_nd' => 'Licencia Creative Commons - Attribution No Derivatives',
	'description_cc_by_sa' => 'Licencia Creative Commons - Attribution Share Alike',
	'description_copyright' => '© copyright autor článku',
	'description_gpl' => 'Licencia GNU/GPL',
	'description_wtfpl' => 'Verejná licencia žiadne porušenie',

	// L
	'label_select_licence' => 'Vyberte licenciu',
	'lien_art_libre' => 'http://artlibre.org/licence/lal/en',
	'lien_cc0' => 'http://vvlibri.org/en/licence/cc0/10/en/legalcode',
	'lien_cc_by' => 'http://creativecommons.org/licenses/by/3.0/legalcode',
	'lien_cc_by_nc' => 'http://creativecommons.org/licenses/by-nc/3.0/legalcode',
	'lien_cc_by_nc_nd' => 'http://creativecommons.org/licenses/by-nc-nd/3.0/legalcode',
	'lien_cc_by_nc_sa' => 'http://creativecommons.org/licenses/by-nc-sa/3.0/legalcode',
	'lien_cc_by_nd' => 'http://creativecommons.org/licenses/by-nd/3.0/legalcode',
	'lien_cc_by_sa' => 'http://creativecommons.org/licenses/by-sa/3.0/legalcode',
	'lien_gfdl' => 'http://www.gnu.org/licenses/fdl.html',
	'lien_gpl' => 'http://www.gnu.org/copyleft/gpl.html',
	'lien_whfpl' => 'http://sam.zoy.org/lprab/',

	// N
	'noisette_cacher_defaut' => 'Skryť, keď nie je vybratá licencia?',
	'noisette_hauteur_logo' => 'Maximálna výška loga (v pixeloch):',
	'noisette_largeur_logo' => 'Maximálna šírka loga (v pixeloch):',
	'noisette_lien' => 'Zobraziť odkaz na popis licencie?',
	'noisette_logo' => 'Zobraziť logo licencie?',
	'noisette_nom_licence' => 'Zobraziť názov licencie?',

	// S
	'sans_licence' => 'Žiadna licencia',

	// T
	'titre_art_libre' => 'FAL',
	'titre_cc0' => 'CC0',
	'titre_cc_by' => 'Licencia CC by',
	'titre_cc_by_nc' => 'Licencia CC by-nc',
	'titre_cc_by_nc_nd' => 'Licencia CC by-nc-nd',
	'titre_cc_by_nc_sa' => 'Licencia CC by-nc-sa',
	'titre_cc_by_nd' => 'Licencia CC by-nd',
	'titre_cc_by_sa' => 'Licencia CC by-sa',
	'titre_copyright' => 'Autorské práva',
	'titre_gfdl' => 'Licencia GNU FDL',
	'titre_gpl' => 'Licencia GNU GPL',
	'titre_wtfpl' => 'LPRAB'
);

?>
