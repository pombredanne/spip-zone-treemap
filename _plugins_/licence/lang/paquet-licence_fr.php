<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/licence/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'licence_description' => 'Permet de relier une licence d\'utilisation à un article ou un document',
	'licence_nom' => 'Licence',
	'licence_slogan' => 'Une Licence pour articles et documents'
);

?>
