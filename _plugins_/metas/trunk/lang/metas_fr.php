<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(

// Termes generiques
'title' => 'title',
'meta_description' => 'meta description',
'meta_keywords' => 'meta keywords',
'meta_canonical' => 'Url Canonique',

// Panneau de configuration
'configuration_metas' => 'M&eacute;tas',
'aide_ecrire_metas' => '<h4>M&eacute;tadonn&eacute;es</h4>
<p>Ici vous pouvez configurer les m&eacute;tadonn&eacute;es de votre site.</p>
<p>Pour en savoir plus sur ce plugin, consultez sa <a href="http://www.spip-contrib.net/?rubrique1250">documentation en&nbsp;ligne</a>.</p>',

// Metadonnees par defaut
'config_metas_defaut' => 'M&eacute;tadonn&eacute;es par defaut',
'title_defaut_explication' => 'Si vous ne souhaitez pas utiliser le <a href="?exec=configuration">nom de votre site</a>, indiquez un autre titre&nbsp;:',
'meta_description_defaut_explication' => 'Si vous ne souhaitez pas utiliser la <a href="?exec=configuration">description du site</a>, r&eacute;digez ici une autre description (en moins de 250 caract&egrave;res)&nbsp;:',
'meta_keywords_defaut_explication' => 'Ajoutez ici les mots-cl&eacute;s génériques du site, s&eacute;par&eacute;s par des virgules (1&nbsp;000&nbsp;caract&egrave;res maxi). Ils serviront de base pour remplir la balise idoine.',



// Metadonnees par objet
'config_metas_page' => 'M&eacute;tadonn&eacute;es',
'title_explication' => 'Si vous ne souhaitez pas utiliser le titre ci-dessus, indiquez-en un autre&nbsp;:',
'meta_description_explication' => 'Si vous ne souhaitez pas utiliser le descriptif automatique de SPIP, r&eacute;digez ici une courte description de la page (en moins de 250 caract&egrave;res)&nbsp;:',
'meta_keywords_explication' => 'Ajoutez vos mots-cl&eacute;s, s&eacute;par&eacute;s par des virgules (1&nbsp;000&nbsp;caract&egrave;res maxi)&nbsp;:',
'meta_canonical_explication' => 'Url Canonique complète. Dans la plupart des cas vous ne devez pas la renseigner' ,

// Mots importants
'config_mots_importants' => 'Configuration des mots importants',
'mots_importants_label' => 'Mots importants',
'mots_importants_explication' => '&Eacute;crivez ici, s&eacute;par&eacute;s par une virgule, les mots importants que vous souhaitez mettre en &eacute;vidence. Ils seront mis en &lt;strong&gt; sur le site. &Agrave;&nbsp;utiliser avec parcimonie.',

// Divers
'valider' => 'Enregistrer'

);
?>