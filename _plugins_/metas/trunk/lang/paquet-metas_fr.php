<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// M
    'metas_titre' => 'M&eacute;tas',
    'metas_description' => 'Gestion des m&eacute;tas <code>title</code>, <code>description</code> et <code>keywords</code> des articles et rubriques de SPIP, et mise en exergue de mots importants.',
	'metas_slogan' => 'Gestion des m&eacute;tas title,description et keywords'
);
?>