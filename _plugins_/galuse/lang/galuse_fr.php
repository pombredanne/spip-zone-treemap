<?php
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
    'admin_restreints' => 'Administrteurs restreints',
    'autorisation' => 'Autorisations',
    'autoriser_article' => 'Autoriser � joindres des images aux articles.',
    'autoriser_auteur' => 'Autoriser � joindres des images aux auteurs.',
    'autoriser_rubrique' => 'Autoriser � joindres des images aux rubriques.',
    'caracteristiques_images' => 'Caract&eacute;ristiques des images :',
    'compression_jpg' => 'Compression jpeg',
    'credits' => 'Cr�dits :',
    'envoyer' => 'Envoyer',
    'extensions' => 'Extensions',
    'extensions_supportees' => 'Extensions support&eacute;es',
    'fonctionnement' => 'Fonctionnement :',
    'galerie_vous_enregistrer' => 'Pour ajouter des photos &agrave; la galerie, vous devez vous enregistrer au pr&eacute;alable. Merci de vous connecter. Si vous n\'&ecirc;tes pas enregistr&eacute;, vous devez vous inscrire.',
    'hauteur_max' => 'Hauteur max.:',
    'hauteur_redim' => 'Hauteur :',
    'image' => 'Image',
    'ko' => 'kilo-octets',
    'largeur_max' => 'Largeur max.:',
    'largeur_redim' => 'Largeur :',
    'moderation' => 'Mod&eacute;ration &agrave; priori',
	'octets' => 'octets',
    'pas_droit' => 'Vous n\'avez pas les droits pour envoyer des images ici',
    'px' => 'px',
    'poids' => 'Poids max.:',
    'publi_imed' => 'Publication imm&eacute;diate',
    'qui_ajouter' => 'Qui peut ajouter des images ?',
    'redim_auto' => 'Redimensionnement automatique',
	'signature' => 'Plugin Photos :: Juin 2010
				:: Thom (sur la base du plugin de B. Blanzin)',
    'taille_max' => 'Taille max.:',
    'tout_monde' => 'Tout le monde',
    'types_images' => 'Type(s) d\'images autoris&eacute;(s) :',
    'visiteurs_enregistres' => 'Visiteurs enregistr&eacute;s',
    'webmestre' => 'Webmestre',
    'zone_abonnement' => 'Zone sur abonnement'
);

?>
