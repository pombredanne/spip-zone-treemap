<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'sur_le_web' => 'sur le web',

	// T
	'texte_date_publication_anterieure' => 'Date d\'apparition dans l\'agenda',

	// V
	'voir_en_ligne' => 'en ligne',
	'voir_rubrique' => '[hack] Voir la rubrique correspondante',
	'voir_syndic' => '[hack] Voir la fiche du site'

);


?>
