<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_associes' => 'Articles li&eacute;s',
	
	// B
	
	// D
	
	// E
	
	// F
	
	// I
	
	// L
	'label_parents' => 'Dans les rubriques',
	'label_autre_parent' => '&Eacute;galement dans la rubrique',
	'label_autres_parents' => '&Eacute;galement dans les rubriques',
	
	// M
	// P
	'parent_obligatoire' => 'Vous devez indiquer au moins une rubrique',
	
	// R
	'rubriques_associees' => 'Rubriques li&eacute;es',
	
	// T
	
	// U
	
	//

);
?>
