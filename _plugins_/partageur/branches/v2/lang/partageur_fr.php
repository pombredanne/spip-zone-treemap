<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

  // A
  'aucun_article' => 'aucun article &agrave; syndiquer actuellement',
  
  // C
  'creer_partage' => 'Importer des articles en partage',
  'creer_site_partage' => 'Ajouter un nouveau site partageur',
  'copie' => 'Partageur: Import d\'article',
  'cle_incorrecte' => 'Mauvaise clé d\'authentification',
  
  // D
  'description_partageur' => 'Vous pouvez dans cette section, définir les sites desquels vous voulez importer des articles.<br /><br />Ces sites doivent aussi avoir installés le plugin partageur',
  
  // E
  'erreur_aucun_site' => 'Aucun site partageur disponible',
  'erreur_rubrique_invalide' => ' Erreur: rubrique destination inconnue',
  'event_ok' => 'Ajout d\'un &eacute;v&eacute;nement ',
  
  // F
  'flux_inconnu' => 'Erreur: le site proposé ne semble pas avoir installé le plugin partageur. Le flux suivant est inaccessible:',
  'flux_doublon' => 'Erreur (doublon): le site est déjà inscrit dans les sites partageurs',
  
  // G
  'gestion_site_partage' => 'Sites partageurs',
  
  // I
  'install_table' => 'Cr&eacute;ation de la table spip_partageurs ',
  'imported_already' => 'Article d&eacute;j&agrave;  import&eacute;',
  'imported_new' => 'Nouvel article',

  
  // L
  'label_cle' => 'Clé d\'authentification',
  'label_titre' => 'Nom du site',
  'label_url' => 'Adresse du site',
  'label_effacer' => 'Effacer',
  
  // P
  'partageur' => 'Partageur',
  
  // R
  'retour_rubrique' => 'Revenir à la rubrique',
  
  
  // T
  'texte_nouveau_partageur' => 'Enregister un site en partage',
  
  // Z
  'zzzz' => 'zzzz',


);


?>