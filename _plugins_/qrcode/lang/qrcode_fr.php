<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'aide' => 'Utilisez votre mobile pour scanner ce code barre et y télécharger le document',
	
	// D
	'descriptif' => 'Les QRcodes sont des codes barres lisibles par divers appareils et en particulier par les téléphones portables. Ils
peuvent contenir diverses informations par exemple : des URL permettant de télécharger des fichiers sans avoir
à saisir manuellement l\'adresse de téléchargement.',

	// E
	'explication_css_aide' => 'Tous les qrcodes insérés auront cette classe',
	'explication_ecc_aide' => 'Ce pourcentage indique combien d\'erreurs peuvent être récupérées en cas d\'erreur de lecture du QRcode',
	'explication_remplace_css_id_aide' => 'Par défaut, c\'est&nbsp;: <code>.documents_joints</code>',
	'explication_style_aide' => 'Tous les qrcodes insérés auront ce style',
	'explication_taille_aide' => 'Chaque élément du QRcode fera la taille spécifiée.',

	// I
	'instructions' => 'Vous pouvez configurer comment les QRcodes apparaîtront dans vos articles.',

	// L
	'label_css' => 'Classe CSS : ',
	'label_ecc' => 'Correction d\'erreurs :',
	'label_remplace_css_id' => 'Identifiant CSS du conteneur commun des documents joints&nbsp;:',
	'label_style' => 'Style CSS :',
	'label_taille' => 'Taille :',
	'label_utiliser_pour_documents_joints' => 'Utiliser les QRcode pour permettre les téléchargements des documents joints',

	// O
	'option_non_configure' => 'Non configur&eacute;',
);

?>
