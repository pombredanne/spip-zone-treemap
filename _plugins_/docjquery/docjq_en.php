<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'les_types' => '		jQuery: "A jQuery object.",
		Object: "A simple Javascript object. For example, it could be a String or a Number.",
		String: "A string of characters.",
		Number: "A numeric valid.",
		Element: "The Javascript object representation of a DOM Element.",
		Map: "A Javascript object that contains key/value pairs in the form of properties and values.",
		"Array&lt;Element&gt;": "An Array of DOM Elements.",
		"Array&lt;String&gt;": "An Array of strings.",
		Function: "A reference to a Javascript function.",
		XMLHttpRequest: "An XMLHttpRequest object (referencing a HTTP request).",
		"&lt;Content&gt;": "A String (to generate HTML on-the-fly), a DOM Element, an Array of DOM Elements or a jQuery object"',

	'les_traductions' => 'Doc translations',
	'traduire_en' => 'Translate to',
	'visu_traduction_en' => 'Page showing jquery doc and its translation state in',
	's_identifier' => 'You have to be logged in to change translation content',
	'identifie' => 'Your identified and thus you can change translation content',

	'que_rouge' => 'only not or partially translated items',
	'que_orange' => 'only "in progress" ones',
	'que_vert' => 'only translated ones',

	'tout' => 'all items',
	'export' => 'resulting documentation',
	'autres_versions' => 'Other available versions',
	'comment_traduire' => 'How to translate',
);


?>
