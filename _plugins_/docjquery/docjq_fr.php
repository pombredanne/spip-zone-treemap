<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'les_types' => '		jQuery: "Un objet jQuery.",
		Object: "Un objet Javascript simple. Par exemple une chaîne ou un nombre.",
		String: "Une chaîne de caractères.",
		Number: "Un numérique valide.",
		Element: "La représentation en objet Javascript d\'un Element DOM.",
		Map: "Un objet Javascript qui contient des paires clé/valeur sous la forme de propriétés et de valeurs.",
		"Array&lt;Element&gt;": "Un tableau de Element DOM.",
		"Array&lt;String&gt;": "Un tableau de chaînes de caractères.",
		Function: "Une référence à une fonction Javascript.",
		XMLHttpRequest: "Un objet XMLHttpRequest (référençant une requête HTTP).",
		"&lt;Content&gt;": "Une chaîne de caractères (pour générer du HTML à la volée), un Element DOM, un tableau de Element DOM ou un objet jQuery."',

	'les_traductions' => 'Les traductions de la doc',
	'traduire_en' => 'Traduire vers',
	'visu_traduction_en' => 'Interface permettant de visualiser la documentation de jquery, et l\'état de sa traduction en',

	's_identifier' => 'Vous devez vous identifier si vous souhaitez modifier le contenu de la traduction',
	'identifie' => 'Vous êtes identifié et pouvez donc modifier le contenu de la traduction',
	
	'que_rouge' => 'que les éléments non traduits ou à revoir',
	'que_orange' => 'que ceux en cours de traduction',
	'que_vert' => 'que ceux traduits',

	'tout' => 'tous les éléments',
	'export' => 'la documentation résultante',
	'autres_versions' => 'Autres versions disponibles',
	'comment_traduire' => 'Comment traduire',
);


?>
