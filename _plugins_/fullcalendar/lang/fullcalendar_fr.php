<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/cfg/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'action_mot_add' => 'Ajout d&#39;une mot cl&#233; pour cet agenda',
	'action_mot_edit' => 'Mise &#224; jour du mot cl&#233; pour cet agenda',
	'action_rub_add' => 'Ajout d&#39;une rubrique source pour cet agenda',
	'action_rub_edit' => 'Mise &#224; jour de la rubriques source pour cet agenda',
	'action_key_add' => 'Ajout d&#39;une cl&#233; Google Agenda',
	'action_key_edit' => 'Mise &#224; jour de la cl&#233; google pour cet agenda',
	'add' => 'Ajouter',
	'aspect_ratio' => 'Ratio d&#39;aspect',
	// B
	'bouton_effacer' => 'Effacer',
	// C
	'calendar' => 'Calendrier',
	'calendars' => 'Calendriers',
	'calendar_add' => 'Ajout d&#39;un nouveau calendrier',
	'calendar_edit' => 'Modification d&#39;un calendrier',
	'calendar_del' => 'Efface le calendrier',
	'calendar_del_events' => 'Efface les &#233;v&#232;nements du calendrier',
	'center' => 'Centre',
	'column' => 'Colonnes',
	'cfg_boite_info' => '<h1>FullCalendar</h1>[Documentation->http://www.spip-contrib.net/FullCalendar]',
	'cfg_titre_accueil' => 'FullCalendar',
	'cfg_titre_boite' => 'Configuration de FullCalendar',
	'configuration' => 'Configuration',
	// D
	'days' => 'Jours',
	'default_info' => 'Mini calendrier par d&#233;faut',
	'delete_event' => 'Effacer cet &#233;v&#232;nement ?',
	// E
	'event' => '&#233;v&#232;nement',
	'event_add' => 'Ajout d&#39;un nouvel &#233;v&#232;nement',
	'event_del' => 'Ev&#232;nement effac&#233; !',
	'event_edit' => 'Modification d&#39;un &#233;v&#232;nement',
	'event_nuts' => 'Afficher les &#233;v&#232;nements &#224; venir et en cours',
	'event_style' => 'Style de l&#39;&#233;v&#232;nement ',
	'event_title' => 'Titre de l&#39;&#233;v&#232;nement',
	'event_start' => 'D&#233;but',
	'event_end' => 'Fin',
	'event_from' => 'Du',
	'event_to' => 'Au',
	'event_error' => 'Fin avant le d&#233;but ?',
	// F
	'fullcalendar' => 'FullCalendar',
	'flux' => 'Aperçu du flux',
	'font' => 'Police',
	// G
	'gestion' => 'Gestion',
	'gestion_evenements' => 'Gestion des &#233;v&#232;nements',
	'gestion_calendriers' => 'Gestion des calendriers',
	'gestion_styles' => 'Gestion des styles CSS pour les &#233;v&#232;nements',
	'general' => 'Apparence g&#233;n&#233;rale',
	'google_agenda' => 'Agenda Google',
	'google_agenda_info' => 'V&#233;rifier bien que votre agenda est <u>public</u> puis renseignez l&#39;ID dans le formulaire ci-contre.',
	'google_agenda_id' => 'ID de l&#39;agenda',
	// H
	'header' => 'En-tête',
	'help' => '[Aide]',
	// I 
	'in_skel' => 'en squelettes',
	// J
	// K
	'key_save' => 'Enregistrer ma cl&#233;',
	'key_update' => 'Modifier cette cl&#233;',
	// L
	'lien' => 'Lien de l&#39;&#233;v&#232;nement (?article4 ?rubrique2 ...)',
	'left' => 'Gauche',
	'linked_to' => 'Li&#233; &#224;',
	// M
	'max_height' => 'Hauteur maximale des noisettes',
	'month' => 'Mois',
	'mot_agenda' => 'Agenda par mot cl&#233;',
	'mot_agenda_info' => 'S&#233;lectionnez le mot cl&#233; &#224; lier aux articles qui serviront d&#39;&#233;v&#232;nements pour ce calendrier.',
	'mot_agenda_change' => 'Modifier ce mot cl&#233;',
	'mini_limit' => 'Nombre d&#39;&#233;v&#232;nements &#224; afficher',
	// N
	'noizette_sarka' => 'Noisette ',
	'non' => 'non',
	'no_style' => 'Vous n&#39;avez pas d&#233;finit de style pour vos &#233;v&#232;nements, il seront donc affich&#233;s avec les couleurs par d&#233;faut. Pour cr&#233;er un nouveau style d&#39;&#233;v&#232;nement <a href="?exec=fullcalendar_css">cliquez ici</a>',
	'no_event' => 'Aucun &#233;v&#232;nement dans ce calendrier!',
	// O
	'oui' => 'oui',
	// P
	'plugin_name' => 'jQuery FullCalendar v1.5.2 pour SPIP',
	'prevnext' => 'Reculer, avancer',
	// Q
	// R
	'right' => 'Droite',
	'rub_agenda' => 'Agenda d&#39;une rubrique',
	'rub_agenda_info' => 'S&#233;lectionnez la rubrique contenant les articles qui serviront d&#39;&#233;v&#232;nements pour ce calendrier.',
	'rub_agenda_change' => 'Modifier la rubrique source',
	'rub_agenda_source' => 'Secteur source pour les &#233;v&#232;nements de ce calendrier',
	// S
	'source_mysql' => 'Base de donn&#233;e MySQL locale',
	'source_google' => 'Google Agenda publique',
	'source_art' => 'Articles par mots cl&#233;s',
	'source_rub' => 'Rubrique',
	'source_select' => 'Source pour les &#233;v&#232;nements de ce calendrier',
	'styles' => 'styles',
	'style_title' => 'Les CSS de FullCalendar pour SPIP',
	'style_name' => 'Nom du style',
	'style_border' => 'Couleur des bordures',
	'style_background' => 'Couleur du fond',
	'style_text' => 'Couleur du texte',
	'style_add' => 'Ajout d&#39;un nouveau style',
	'style_edit' => 'Modification d&#39;un nouveau style',
	'style_del' => 'Efface le style',
	'style_welcome' => 'Vous n&#39;avez pas encore d&#233;finit de style !',
	'style_info' => 'Les d&#233;finition de styles permettent de modifier l&#39;apparence des &#233;v&#232;nements dans les agendas qui utilisent MySQL comme source de donn&#233;e.',
	'show_weekends' => 'Afficher les weekends',
	'shortcut' => 'Raccourci',
	// T
	'title' => 'Titre',
	'time' => 'Temps',
	'today' => 'Aller sur aujourd&#39;hui',
	'tous_evenements' => 'Tous les &#233;v&#232;nements de ce calendrier',
	// U
	'use_theme' => 'Utiliser le th&#232;me',
	'unknown_error' => 'Ce calendrier n&#39;existe plus ou erreur !',
	// V
	'view_default' => 'Vue par d&#233;faut',
	'view_month' => 'Vue mensuelle',
	'view_week' => 'Vue par semaines',
	'view_day' => 'Calendrier journalier',
	'view_mini' => 'Mini Calendrier',
	'votre_lien' => 'Votre lien',
	'vos_calendriers' => 'Vos calendriers',
	'vos_styles' => 'Vos styles',
	'vous_avez' => 'Vous avez',
	// W
	'welcome_title' => 'Bienvenu dans FullCalendar pour SPIP !',
	'welcome_text' => 'Commencez par cr&#233;er votre premier calendrier en utilisant le formulaire ci-contre.',
	'week_begin' => 'La semaine commence',
	'weeks' => 'semaines',
	'width' => 'Largeur',
	// X
	// Y
	'year_prev' => 'Reculer d&#39;un an',
	'year_next' => 'Avance d&#39;un an',
	// Z
);

?>
