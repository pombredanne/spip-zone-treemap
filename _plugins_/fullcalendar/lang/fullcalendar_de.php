<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/cfg/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'action_mot_add' => 'Schlagwort für diesen Kalender hinzufügen',
	'action_mot_edit' => 'Schlagwort für diesen Kalender bearbeiten',
	'action_rub_add' => 'Quellrubrik für diesen Kalender hinzufügen',
	'action_rub_edit' => 'Quellrubrik für diesen Kalender bearbeiten',
	'action_key_add' => 'Schlüssel für Google-Kalender hinzufügen',
	'action_key_edit' => 'Schlüssel für Google-Kalender bearbeiten',
	'add' => 'Ajouter',
	'aspect_ratio' => 'Seitenverhältnis',
	// B
	'bouton_effacer' => 'Löschen',
	// C
	'calendar' => 'Kalender',
	'calendars' => 'Kalender',
	'calendar_add' => 'Neuen Kalender hinzufügen',
	// 'calendar_edit' => 'Kalender bearbeiten',
	'calendar_del' => 'Kalender löschen',
	'calendar_del_events' => 'Kalendereinträge löschen',
	'center' => 'Zentrum',
	'column' => 'Spalten',
	'cfg_boite_info' => '<h1>FullCalendar</h1>[Dokumentation->http://www.spip-contrib.net/FullCalendar]',
	'cfg_titre_accueil' => 'FullCalendar',
	'cfg_titre_boite' => 'FullCalendar Konfiguration',
	'configuration' => 'Konfiguration',
	// D
	'days' => 'Tage',
	'default_info' => 'Standard Minikalender',
	'delete_event' => 'Kalendereintrag löschen?',
	// E
	'event' => 'Eintrag',
	'event_add' => 'Neuen Eintrag hinzufügen',
	'event_del' => 'Eintrag gelöscht!',
	'event_edit' => 'Eintrag bearbeiten',
	'event_nuts' => 'Aktuelle und zukünftige Einträge anzeigen',
	'event_style' => 'Style der Einträge ',
	'event_title' => 'Titel des Eintrags',
	'event_start' => 'Anfang',
	'event_end' => 'Ende',
	'event_from' => 'von',
	'event_to' => 'bis',
	'event_error' => 'Fin avant le d&#233;but ?',
	// F
	'fullcalendar' => 'FullCalendar',
	'flux' => 'Feed-Vorschau',
	'font' => 'Schriftart',
	// G
	'gestion' => 'Verwaltung',
	'gestion_evenements' => 'Verwaltung der Einträge',
	'gestion_calendriers' => 'Verwaltung der Kalender',
	'gestion_styles' => 'Verwaltung der CSS-Stile für Einträge',
	'general' => 'allgemeine Darstellung',
	'google_agenda' => 'Google-Kalender',
	'google_agenda_info' => 'Prüfen sie, ob ihr Kalender <u>öffentlich</u> ist und geben sie seine ID in das Formular ein.',
	'google_agenda_id' => 'Kalender-ID',
	// H
	'header' => 'Kopf',
	'help' => '[Hilfe]',
	// I 
	'in_skel' => 'als Skelette',
	// J
	// K
	'key_save' => 'Meinen Schlüssel speichern',
	'key_update' => 'Diesen Schlüssel ändern',
	// L
	'lien' => 'Link des Eintrags (?article4 ?rubrique2 ...)',
	'left' => 'Links',
	'linked_to' => 'Verlinkt mit',
	// M
	'max_height' => 'Maximalhöhe der Nüsse',
	'month' => 'Monat',
	'mot_agenda' => 'Kalender mit Schlagwort',
	'mot_agenda_info' => 'Wählen sie das Schlagwort, das Artikel zu Kalendereinträgen macht.',
	'mot_agenda_change' => 'Dieses Schlagwort ändern',
	'mini_limit' => 'Anzahl der dargestellten Einträge',
	// N
	'noizette_sarka' => 'Nuss ',
	'non' => 'nein',
	'no_style' => 'Sie haben keine Stilangaben für Ihren Kalender gemacht. Er wird deshalb in der Standardformatierung angezeigt. <a href="?exec=fullcalendar_css">Klicken sie hier</a>, um einen neuen Stil für den Kalender anzulegen.',
	'no_event' => 'Dieser Kalender hat keinen Eintrag!',
	// O
	'oui' => 'ja',
	// P
	'plugin_name' => 'jQuery FullCalendar v1.5.2 für SPIP',
	'prevnext' => 'zurück, vorwärts',
	// Q
	// R
	'right' => 'rechts',
	'rub_agenda' => 'Rubrik als Kalender',
	'rub_agenda_info' => 'Wählen sie die Rubrik, deren Artikel als Kalendereinträge genutzt werden sollen.',
	'rub_agenda_change' => 'Quellrubrik ändern',
	'rub_agenda_source' => 'Basisrubrik als Quelle für diesen Kalender',
	// S
	'source_mysql' => 'Lokale MySQL-Datenbank',
	'source_google' => 'Öffentlicher Google-Kalender',
	'source_art' => 'Artikel nach Schlagwort',
	'source_rub' => 'Rubrik',
	'source_select' => 'Datenquelle für diesen Kalender',
	'styles' => 'Stile',
	'style_title' => 'CSS von FullCalendar für SPIP',
	'style_name' => 'Stil-Name',
	'style_border' => 'Randfarbe',
	'style_background' => 'Hintergrundfarbe',
	'style_text' => 'Textfarbe',
	'style_add' => 'Neuen Stil hinzufügen',
	'style_edit' => 'Neuen Stil bearbeiten',
	'style_del' => 'Stil löschen',
	'style_welcome' => 'Sie haben noch keinen Stil angelegt!',
	'style_info' => 'Die Stile ermöglichen, das Aussehen von Kalendern zu steuern, welche die lokale MySQL-Daenbank nutzen.',
	'show_weekends' => 'Wochenenden anzeigen',
	'shortcut' => 'Shortcut',
	// T
	'title' => 'Titel',
	'time' => 'Zeit',
	'today' => 'Zum heutigen Tag',
	'tous_evenements' => 'Alle Einträge dieses Kalenders',
	// U
	'use_theme' => 'Thema benutzen',
	'unknown_error' => 'Dieser Kalender existiert nicht mehr oder es liegt ein Fähler vor!',
	// V
	'view_default' => 'Standardansicht',
	'view_month' => 'Monatsansicht',
	'view_week' => 'Wochenansicht',
	'view_day' => 'Tageskalender',
	'view_mini' => 'Minikalender',
	'votre_lien' => 'Ihr Link',
	'vos_calendriers' => 'Ihre Kalender',
	'vos_styles' => 'Ihre Stile',
	'vous_avez' => 'Sie haben',
	// W
	'welcome_title' => 'Willkommen beim FullCalendar für SPIP !',
	'welcome_text' => 'Legen sie ihren ersten Kalender mit Hilfe des Formulars an.',
	'week_begin' => 'Wochenanfang',
	'weeks' => 'Wochen',
	'width' => 'Breite',
	// X
	// Y
	'year_prev' => 'Ein Jahr zurück',
	'year_next' => 'Ein Jahr vorwärts',
	// Z
);

?>
