GeSHi for FCKeditor plugin by Richard Tuin
Email:	richardtuin @ gmail . com
Www:	http://www . richardtuin . com

Version 1.0

Geshi for FCKeditor is my implementation of the famous php Geshi library for FCKeditor. 
This FCKeditor plugin helps you to put scripting code colored in FCKeditor. 
It automatically syntax highlight / code color's the code you enter, and adds the generated code to FCKeditor.

=============
changelog
----------

05-05-2007
- Initial plugin creation and version 1.0 release