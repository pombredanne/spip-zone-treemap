FCKLang['GeshighlighterHighlightTitle'] = 'Insert Syntax Highlighted Code';
FCKLang['GeshighlighterSourceCode']     = 'Source code';
FCKLang['GeshighlighterLanguage']       = 'Language';
FCKLang['GeshighlighterShowLines']      = 'Show line numbers';
FCKLang['GeshighlighterWaitMsg']        = 'Waiting for geshi to respond...'; 