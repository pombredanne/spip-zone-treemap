FCKLang['GeshighlighterHighlightTitle'] = 'Gekleure code invoeren';
FCKLang['GeshighlighterSourceCode']     = 'Broncode';
FCKLang['GeshighlighterLanguage']       = 'Programmeertaal';
FCKLang['GeshighlighterShowLines']      = 'Laat regelnummers zien';
FCKLang['GeshighlighterWaitMsg']        = 'Wachten...';