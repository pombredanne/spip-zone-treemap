<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
 
'icone_modifier_article_spip' => 'Modifier cet article avec l\'&eacute;diteur spip',			 
'icone_modifier_article_fck' => 'Modifier cet article avec l\'&eacute;diteur wysiwyg',	  
'texte_editeur_standard' => 'Utiliser l\'&eacute;diteur standard de SPIP',
'texte_editeur_avance' => 'Utiliser l\'&eacute;diteur avanc&eacute;', 
'texte_editeur_avance_nvlefenetre' => 'Ouvrir dans une nouvelle fen&ecirc;tre',
'texte_modification_article_choix_editeur' => 'Attention, vous devez modifier l\'article avec l\'outil qui a permis sa cr&eacute;ation !'
);


?>