﻿/*
 * FCKeditor - The text editor for internet
 * Copyright (C) 2003-2006 Frederico Caldeira Knabben
 * 
 * Licensed under the terms of the GNU Lesser General Public License:
 * 		http://www.opensource.org/licenses/lgpl-license.php
 * 
 * For further information visit:
 * 		http://www.fckeditor.net/
 * 
 * "Support Open Source software. What about a donation today?"
 * 
 * File Name: fr.js
 * 	Placholder Italian language file.
 * 
 * File Authors:
 * 		Hubert Garrido (liane@users.sourceforge.net)
 */
 
// Fichier de langue française
FCKLang.HelppanelBtn = 'Aide';
FCKLang.HelppanelDlgTitle = 'Aide';