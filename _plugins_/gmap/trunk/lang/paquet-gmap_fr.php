<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'gmap_slogan' => "G&eacute;olocalisation et cartographie dans SPIP",

	'gmap_description' => "GMap permet de g&eacute;olocaliser les objets SPIP dans la partyie priv&eacute;e du site et de les repr&eacute;senter sur des cartes dans la partie publique.",

);

?>
