<?php
/*
 * GMap plugin
 * Insertion de carte Google Maps sur les �l�ments SPIP
 *
 * Auteur :
 * Fabrice ALBERT
 * (c) 2011 - licence GNU/GPL
 *
 * Param�trage de la carte dans l'espace public
 *
 */

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/gmap_config_utils');

// Param�trage de la carte selon ce qui est d�fini dans la partie priv�e
function mapimpl_gma3_public_parametre_carte_dist($viewport, $params, $profile = 'interface')
{
	// La clef � laquelle se trouvent les configurations d�pend du profile et de l'api
	if (!isset($profile))
		$profile = 'interface';
	$apiConfigKey = 'gmap_gma3_'.$profile;
	
	// Construction d'un tableau des vues autoris�es
	$types = array();
	if (gmap_lire_config($apiConfigKey, 'type_carte_plan', "oui") === "oui")
		$types[] = '"plan"';
	if (gmap_lire_config($apiConfigKey, 'type_carte_satellite', "oui") === "oui")
		$types[] = '"satellite"';
	if (gmap_lire_config($apiConfigKey, 'type_carte_mixte', "oui") === "oui")
		$types[] = '"mixte"';
	if (gmap_lire_config($apiConfigKey, 'type_carte_physic', "oui") === "oui")
		$types[] = '"physic"';
	$key = gmap_lire_config('gmap_api_gma3', 'key', "");
	if (strlen($key) > 0)
	{
		if (gmap_lire_config($apiConfigKey, 'type_carte_earth', "oui") === "oui")
			$types[] = '"earth"';
	}
	if (count($types) == 0)
		$types[] = '"mixte"';
		
	// Code javascript correspondant
	$code = '{ // Param�tres Google Maps V3
		viewLatitude: '.(isset($params['latitude']) ? $params['latitude'] : $viewport['latitude']).',
		viewLongitude: '.(isset($params['longitude']) ? $params['longitude'] : $viewport['longitude']).',
		viewZoom: '.(isset($params['zoom']) ? $params['zoom'] : $viewport['zoom']).',
		mapTypes: ['.implode(', ', $types).'],
		defaultMapType: "'.(isset($params['fond']) ? $params['fond'] : gmap_lire_config($apiConfigKey, 'type_defaut', "mixte")).'",
		styleBackgroundCommand: "'.(isset($params['ctrl_fond']) ? $params['ctrl_fond'] : gmap_lire_config($apiConfigKey, 'types_control_style', "menu")).'",
		positionBackgroundCommand: "'.gmap_lire_config($apiConfigKey, 'types_control_position', "TR").'",
		styleZoomCommand: "'.(isset($params['ctrl_zoom']) ? $params['ctrl_zoom'] : gmap_lire_config($apiConfigKey, 'zoom_control_style', "auto")).'",
		positionZoomCommand: "'.gmap_lire_config($apiConfigKey, 'zoom_control_position', "LT").'",
		stylePanCommand: "'.(isset($params['ctrl_pan']) ? $params['ctrl_pan'] : gmap_lire_config($apiConfigKey, 'pan_control_style', "large")).'",
		positionPanCommand: "'.gmap_lire_config($apiConfigKey, 'pan_control_position', "LT").'",
		styleScaleControl: "'.(isset($params['ctrl_scale']) ? $params['ctrl_scale'] : gmap_lire_config($apiConfigKey, 'scale_control_style', "none")).'",
		positionScaleControl: "'.gmap_lire_config($apiConfigKey, 'scale_control_position', "BL").'",
		styleStreetViewCommand: "'.(isset($params['ctrl_street']) ? $params['ctrl_street'] : gmap_lire_config($apiConfigKey, 'streetview_control_style', "default")).'",
		positionStreetViewCommand: "'.gmap_lire_config($apiConfigKey, 'streetview_control_position', "LT").'",
		styleRotationCommand: "'.(isset($params['ctrl_rotate']) ? $params['ctrl_rotate'] : gmap_lire_config($apiConfigKey, 'rotate_control_style', "none")).'",
		positionRotationCommand: "'.gmap_lire_config($apiConfigKey, 'rotate_control_position', "LT").'",
		styleOverviewControl: "'.(isset($params['ctrl_overview']) ? $params['ctrl_overview'] : gmap_lire_config($apiConfigKey, 'overview_control_style', "none")).'",
		enableDblClkZoom: '.(((isset($params['option_dblclk_zoom']) ? $params['option_dblclk_zoom'] : gmap_lire_config($apiConfigKey, 'allow_dblclk_zoom', "non")) === "oui") ? 'true' : 'false').',
		enableMapDragging: '.(((isset($params['option_drag']) ? $params['option_drag'] : gmap_lire_config($apiConfigKey, 'allow_map_dragging', "oui")) === "oui") ? 'true' : 'false').',
		enableWheelZoom: '.(((isset($params['option_wheel_zoom']) ? $params['option_wheel_zoom'] : gmap_lire_config($apiConfigKey, 'allow_wheel_zoom', "non")) === "oui") ? 'true' : 'false').',
		enableKeyboard: '.(((isset($params['option_keyboard']) ? $params['option_keyboard'] : gmap_lire_config($apiConfigKey, 'allow_keyboard', "non")) === "oui") ? 'true' : 'false').',
		infoWidthPercent: '.gmap_lire_config($apiConfigKey, 'info_width_percent', "65").',
		infoWidthAbsolute: '.gmap_lire_config($apiConfigKey, 'info_width_absolute', "300").',
		mergeInfoWindows: '.((gmap_lire_config($apiConfigKey, 'merge_infos', "non") === "oui") ? 'true' : 'false').'
	}';
	
	return $code;
}

?>