<?php
/*
 * Google Maps in SPIP plugin
 * Insertion de carte Google Maps sur les �l�ments SPIP
 *
 * Auteur :
 * Fabrice ALBERT
 * (c) 2009 - licence GNU/GPL
 *
 * Page de param�trage du plugin
 *
 */

include_spip('inc/presentation');
include_spip('inc/gmap_presentation');
include_spip('inc/gmap_config_utils');
include_spip('configuration/gmap_config_onglets');

if (!defined("_ECRIRE_INC_VERSION")) return;

// Choix de l'interface GIS
function gmap_formulaire_configuration_gis()
{
	$corps = "";
	
	// Texte explicatif
	$corps .= '
	<div class="texte"><p>'._T("gmap:configuration_gis_explic").'</p></div>';
	
	// Lire la configuration
	$apis = gmap_apis_connues();
	$apiSelected = gmap_lire_config('gmap_api', 'api', "gma3");

	// Param�trage de l'API utilis�e
	$corps .= '
		<div class="config_group">
			<select name="api_code" id="api_code" size="1">';
	foreach ($apis as $api => $infos)
	{
		$corps .= '
				<option value="'.$api.'"'.(!strcmp($apiSelected, $api) ? ' selected="selected"' : '').'>'.$infos['name'].'</option>';
	}
	$corps .= '
			</select>
			<p class="texte" id="api_code_desc"></p>
		</div>'."\n";

	// Script de mise � jour des explications
	$corps .= '<script type="text/javascript">'."\n".'	//<![CDATA['."\n";
	$corps .= '
function updateAPIDescription()
{
	var api = jQuery("#api_code").val();
	var desc = "";
	switch (api)
	{'."\n";
	foreach ($apis as $api => $infos)
	{
		$corps .= '
	case "'.$api.'" : desc = "'.$infos['explic'].'"; break;'."\n";
	}
	$corps .= '	default : desc = "'._T('gmap:gis_api_none_desc').'"; break;
	}
	jQuery("#api_code_desc").html(desc);
}
jQuery("#api_code").change(function() { updateAPIDescription(); });
jQuery(document).ready(function() { updateAPIDescription(); });
';
	$corps .= '	//]]>'."\n".'</script>'."\n";
		
	return gmap_formulaire_submit('configuration_gis', $corps, find_in_path('images/logo-config-gis.png'), _T('gmap:configuration_gis'));
}
function gmap_formulaire_configuration_gis_action()
{
	gmap_ecrire_config('gmap_api', 'api', _request('api_code'));
}

// Bo�tes d'information gauche
function boite_info_help()
{
	$flux = '';
	
	// D�but de la bo�te d'information
	$flux .= debut_boite_info(true);
	
	// Info globale
	$flux .= propre(_T('gmap:info_configuration_gmap'));
	
	// Lien sur l'aide
	$url = generer_url_ecrire('configurer_gmap_html').'&page=doc/parametrage#paramSystem';
	$flux .= propre('<a href="'.$url.'">'._T('gmap:info_configuration_help').'</a>');
	
	// Fin de la bo�te
	$flux .= fin_boite_info(true);
	
	return $flux;
}
function boite_info_important()
{
	$flux = '';

	if (gmap_est_actif())
	{
		// D�but de la bo�te d'information
		$flux .= debut_boite_info(true);
		
		// Affichage de l'API
		$api = gmap_lire_config('gmap_api', 'api', 'gma3');
		$apis = gmap_apis_connues();
		$api_desc = $apis[$api]['name'];
		$flux .= propre(_T('gmap:info_configuration_gmap_api').'<br />'.$api_desc);
		
		// Fin de la bo�te
		$flux .= fin_boite_info(true);
	}
	else
	{
		$flux .= debut_boite_alerte();
		$flux .= propre(_T('gmap:alerte_gmap_inactif'));
		$flux .= fin_boite_alerte();
	}
	
	return $flux;
}


// Page de configuration
function exec_configurer_gmap_dist($class = null)
{
	// v�rifier une nouvelle fois les autorisations
	if (!autoriser('webmestre'))
	{
		include_spip('inc/minipres');
		echo minipres();
		exit;
	}
	
	// Traitements du formulaire post pour ce qui n'est pas en Ajax
	if (_request('config') == 'configuration_gis')
		gmap_formulaire_configuration_gis_action();
	$api = gmap_lire_config('gmap_api', 'api', "gma3");
	if (_request('config') == 'configuration_api')
	{
		$faire_api = charger_fonction('faire_api', 'configuration');
		$faire_api();
	}
	else if (_request('config') == 'configuration_rubgeo')
	{
		$faire_rubgeo = charger_fonction('faire_rubgeo', 'configuration');
		$faire_rubgeo();
	}
	
	// Pipeline pour customiser
	pipeline('exec_init',array('args'=>array('exec'=>'configurer_gmap'),'data'=>''));
	
	// Affichages de SPIP
	$commencer_page = charger_fonction('commencer_page', 'inc');
	echo $commencer_page(_T('gmap:configuration_titre'), 'configurer_gmap', 'configurer_gmap');
	echo "<br /><br /><br />\n";
	$logo = '<img src="'.find_in_path('images/logo-config-title-big.png').'" alt="" style="vertical-align: center" />';
	echo gros_titre(_T('gmap:configuration_titre'), $logo, false);
	echo barre_onglets("configurer_gmap", "cg_main");
	echo debut_gauche('', true);
	
	// Informations sur la colonne gauche
	echo boite_info_help();
	echo boite_info_important();
	
	// Suite des affichages SPIP
	echo pipeline('affiche_gauche',array('args'=>array('exec'=>'configurer_gmap'),'data'=>''));
	echo creer_colonne_droite('', true);
	echo pipeline('affiche_droite',array('args'=>array('exec'=>'configurer_gmap'),'data'=>''));
	echo debut_droite("", true);
	
	// Configuration de l'API utilis�e
	// Cette partie n'est pas en ajax, parce que les autres param�tres en d�pendent
	echo gmap_formulaire_configuration_gis();
	
	// Selon l'API, autre param�trages
	// Cette partie n'est pas en ajax, parce que les autres param�tres en d�pendent
	$api_conf = charger_fonction('api', 'configuration', true);
	if ($api_conf)
		echo $api_conf();
		
	// Optimisations
	$optim = charger_fonction('optimisations', 'configuration', true);
	if ($optim)
		echo $optim();

	// pied de page SPIP
	echo fin_gauche() . fin_page();
}

?>
