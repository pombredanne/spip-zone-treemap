<?php
/*
 * Google Maps in SPIP plugin
 * Insertion de carte Google Maps sur les �l�ments SPIP
 *
 * Auteur :
 * Fabrice ALBERT
 * (c) 2009 - licence GNU/GPL
 *
 * Page de param�trage du plugin
 *
 */

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/gmap_presentation');
include_spip('inc/gmap_config_utils');

//
// Options des rubriques
//

function configuration_editparams_dist()
{
	$corps = "";
	
	// Si on a le r�sultat d'un traitement, l'afficher ici
	$corps .= gmap_decode_result("msg_result");

	// R�cup�rer les param�tres
	$sibling_same_parent = gmap_lire_config('gmap_edit_params', 'siblings_same_parent', "oui");
	$siblings_limit = gmap_lire_config('gmap_edit_params', 'siblings_limit', "5");
		
	// Voisins
	$corps .= '
<fieldset class="config_group">
	<legend>'._T('gmap:outil_siblings_nom').'</legend>
	<div class="padding"><div class="interior">
		<p><input type="checkbox" name="sibling_same_parent" id="sibling_same_parent" value="oui"'.(($sibling_same_parent==="oui")?'checked="checked"':'').' />&nbsp;<label for="sibling_same_parent">'._T('gmap:choix_siblings_same_parent').'</label></p>
		<p><label for="siblings_limit">'._T('gmap:choix_siblings_limit').'</label>&nbsp;<input type="text" name="siblings_limit" id="siblings_limit" value="'.$siblings_limit.'" /></p>
	</div></div>
</fieldset>';
	
	return gmap_formulaire_ajax('config_bloc_gmap', 'editparams', 'configurer_gmap', $corps,
		find_in_path('images/logo-config-editparams.png'),
		_T('gmap:configuration_edit_params'));
}
?>
