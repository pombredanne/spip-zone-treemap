<?php
/*
 * GMap plugin
 * Insertion de carte Google Maps sur les �l�ments SPIP
 *
 * Auteur :
 * Fabrice ALBERT
 * (c) 2011 - licence GNU/GPL
 *
 * Interface de configuration de l'API pour Google Maps v2
 *
 * Usage :
 * $faire_api = charger_fonction("faire_api", "mapimpl/$api/prive");
 * $faire_api();
 *
 */

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/gmap_config_utils');

// Enregistrement des param�tres pass�s dans la requ�te
function mapimpl_gma2_prive_faire_api_dist()
{
	gmap_ecrire_config('gmap_api_gma2', 'key', _request('cle_api'));
	$version = _request('api_version');
	if ($version == "2.")
		$version .= _request('num_other_version');
	gmap_ecrire_config('gmap_api_gma2', 'version', $version);
}

?>
