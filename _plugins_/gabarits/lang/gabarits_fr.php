<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'aucun_gabarit' => 'Aucun gabarit',

'bouton_supprimer' => 'Supprimer',

'cfg_explication_objets' => 'Sur quels objets souhaitez vous utiliser les gabarits ?',
'cfg_label_objets' => 'Types d\'objets',
'cfg_label_objets_article' => 'les articles',
'cfg_label_objets_breve' => 'les br&egrave;ves',
'cfg_label_objets_mot' => 'les mots-cl&eacute;s',
'cfg_label_objets_rubrique' => 'les rubriques',
'cfg_titre_page' => 'Configuration du plugin Gabarits',
'cree_le' => 'Créé le',

'gabarits' => 'Gabarits',
'gabarits_auteur' => 'Gabarits de :',

'gestion_gabarits' => 'Gestion des gabarits',

'icone_creer_gabarit' => 'Créer un nouveau gabarit',
'info_objet' => 'Objet',
'inserer_texte' => 'Ajouter au texte',

'label_autres_gabarits' => 'Gabarits publics',
'label_objet' => 'Objet',
'label_previsu_gabarits' => 'Pr&eacute;visualisation',
'label_select_gabarits' => 'S&eacute;lectionnez un gabarit',
'label_statut' => 'Statut',
'liste_tous' => 'Tous les gabarits',
'liste_vos' => 'Vos gabarits',
'label_vos_gabarits' => 'Vos gabarits',

'nombre_gabarits' => '@nb@ gabarits',
'numero_gabarit' => 'Gabarit num&eacute;ro',

'objet_type' => '@type@',

'par' => 'Par',

'statut_prive' => 'Priv&eacute;',
'statut_public' => 'Public',
'supprimer_confirmation' => 'Souhaitez vraiment supprimer ce gabarit ?',

'titre_bloc_gabarits' => 'Insérer le contenu d\'un gabarit',
'titre_cadre_ajouter_gabarit' => 'Ajouter un gabarit',
'titre_cadre_modifier_gabarit' => 'Modifier un gabarit',

'un_gabarit' => 'gabarit',

);


?>