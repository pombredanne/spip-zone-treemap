<?php

/*______________________________________________________________________________
 | Plugin SpipService 1.0 pour Spip 2.1                                           \
 | Copyright 2012 Sebastien Chandonay - Studio Lambda                            \
 |                                                                                |
 | SpipService est un logiciel libre : vous pouvez le redistribuer ou le          |
 | modifier selon les termes de la GNU General Public Licence tels que            |
 | publi�s par la Free Software Foundation : � votre choix, soit la               |
 | version 3 de la licence, soit une version ult�rieure quelle qu'elle            |
 | soit.                                                                          |
 |                                                                                |
 | SpipService est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE     |
 | GARANTIE ; sans m�me la garantie implicite de QUALIT� MARCHANDE ou             |
 | D'AD�QUATION � UNE UTILISATION PARTICULI�RE. Pour plus de d�tails,             |
 | reportez-vous � la GNU General Public License.                                 |
 |                                                                                |
 | Vous devez avoir re�u une copie de la GNU General Public License               |
 | avec SpipService. Si ce n'est pas le cas, consultez                            |
 | <http://www.gnu.org/licenses/>                                                 |
 ________________________________________________________________________________*/

define("_DIR_SPIPSERVICE_IMG_PACK", _DIR_PLUGIN_SPIPSERVICE."img_pack/");
define("_DIR_SPIPSERVICE_PRIVE", _DIR_PLUGIN_SPIPSERVICE."prive/");

// ------------------------------------------------------
//  DatePicker : Choix d'un th�me
//  - voir dans 'prive/css/datepicker/'
//  - plus de th�mes : http://jqueryui.com/themeroller/
// ------------------------------------------------------
define("THEME_DP_PRIVE", "sunny");
define("THEME_DP_PUBLIC", "cupertino");

define ('_DEBUG_SPIPSERVICE', true);

?>
