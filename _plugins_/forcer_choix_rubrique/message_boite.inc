<p>Vous devez obligatoirement s&eacute;lectionner une <strong>rubrique</strong> dans laquelle votre article sera class&eacute;. Sans cela, il n&rsquo;appara&icirc;tra pas dans le site. Le choix d&rsquo;une rubrique se fait en cliquant sur l&rsquo;ic&ocirc;ne <img src="/dist/images/loupe.png" alt="loupe"> qu&rsquo;on trouve apr&egrave;s la mention &laquo;&nbsp;&Agrave; l&rsquo;int&eacute;rieur de la rubrique&nbsp;&raquo;.
</p>
<p>
Pensez aussi &agrave; donner un <strong>titre</strong> &agrave; votre article.
</p>