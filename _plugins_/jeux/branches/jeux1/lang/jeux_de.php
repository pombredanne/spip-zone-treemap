<?php

// This is a SPIP language file

$GLOBALS[$GLOBALS['idx_lang']] = array(

'alphabet' =>	"ABCDEFGHIJKLMNOPQRSTUVWXYZ-",
'aucun_jeu'	=> 'Kein Spiel',
'votre_choix'    => "Ihre Wahl: ",
'votre_reponse' => "Ihre Antwort: ",
'corriger'        => "Korrigieren",
'verifier'         => '&#220;berpr&#252;fen',
'verifier_validite'     => 'Validit&#228;t &#252;berpr&#252;fen ',

'afficher_solution'     =>'L&#246;sung anzeigen',
'solution'        =>'L&#246;sung',
'reponse'        =>'Antwort',
'indices'        => 'Hinweise',
'score'            => 'Ergebnis:',

'point' => "&nbsp;Pkt",
'points' => "&nbsp;Pkte",

'ligne_n'        =>'Zeile @n@',
'colonne_n'        =>'Spalte @n@',

'reinitialiser'        => 'zur&uuml;cksetzen',
'recommencer'        => 'Neu starten',
'rejouer'        => 'Nochmals spielen',
'reset'        => 'zur&uuml;cksetzen',

'grille_valide'        =>'G&#252;ltiges Raster',
'grille_invalide'    =>'Ung&#252;ltiges Raster',
'aucune_erreur'        =>'Kein Fehler',
'une_erreur'        => 'Ein Fehler',
'n_erreurs'    => '@n@ Fehler',
'n_vides'        => '@n@ leere Zellen',
'une_vide'            => 'Eine leere Zelle',
'bravo'            => "Herzlichen Gl&#252;ckwunsch!",

'reponseJuste' => "Richtig!",
'reponseFausse' => "Falsch!",
'reponseNulle' => "Unbeantwortet!",
'bonneReponse' => "Die richtige Antwort ist:",
'Correction' => "L&#246;sung:",

'erreur_taille_grille'    => "Fehler: Ung&#252;ltige Gr&#246;�e",
'erreur_syntaxe'    => "Syntaxfehler",
'erreur_spip'    => "Diese Spip Version ist zu alt f&#252;r dieses Spiel!",
'erreur_scripts'    => "Skripte verboten: Dieses Spiel kann nicht ausgef&#252;hrt zerden!",

// espace prive

'jeux' => 'Spiele',
'jeux_' => 'Spiel&nbsp;:',
'jeu_numero' => 'Spiel Nummer @id@ (@nom@)',
'jeu_court' => 'Spiel n&deg;@id@ (@nom@)',
'jeu_numero_court' => 'ID',
'jeu_type' => 'Typ',
'jeu_titre' => 'Titel',
'auteur' => 'Autor',

'nouveau_jeu' => 'Neues Spiel',
'explication_jeu'=> 'Der interne Titel dient nur dazu, das Spiel im
	Redaktionsbereich zu bezeichnen.<br /><br />
	Der Inhalt des Spiels muss in der in der
	<a href="http://www.spip-contrib.net/Des-jeux-dans-vos-articles">
	Dokumentation</a> beschriebenen Syntax formuliert werden.
	<br /><br />
	Bei intern verwalteten Spielen sind die Tags &lt;jeux> und &lt;/jeux>
	&uuml;berfl&uuml;ssig.',
'modifier_jeu' => 'Spiel Nummer @id@ bearbeiten',
'modifier_ce_jeu' => 'Dieses Spiel bearbeiten',
'voir_jeu' => 'Spiel ansehen',
'liste_jeux' => 'Liste der Spiele',
'retourner_jeu' => 'Zur&uuml;ck zum Spiel',
'jeu_titre_prive' => 'Interner Titel',
'jeu_titre_public' => '&Ouml;ffentlicher Titel',
'jeu_titre_prive_' => 'Interner Titel:',
'jeu_titre_public_' => '&Ouml;ffentlicher Titel:',
'jeu_contenu' => 'Inhalt des Spiels',
'jeu_vide' => 'LEER',

'derniere_modif' => 'Letzte &Auml;nderung',
'resultat' => 'Ergebnis',
'resultats' => 'Ergebnisse',
'resultats_auteur' => 'Die Ergebnisse von @nom@',
'resultats_auteur_' => "Ergebnisse von Autor",
'voir_ses_resultats' => 'Seine Ergebnisse ansehen',
'resultats_jeu' => 'Ergebnisse von Spiel no. @id@ (@nom@)',
'resultats_jeu_' => 'Ergebnisse des Spiels',
'nb_resultats' => 'Anzahl Ergebnisse',
'gerer_resultats' => 'Ergebnisse verwalten',
'gerer_ses_resultats' => 'Eigene Ergebnisse verwalten',
'voir_resultats' => 'Ergebnisse ansehen',
'type_resultat' => 'Enregistrement des scores&nbsp;:',
'gerer_resultats_jeu' => 'Ergebnisse von Spiel no. @id@ (@nom@) verwalten',
'aucun_resultat' => 'Kein Ergebnis',

'supprimer_tout_tout' => 'ALLE Ergebnisse l�schen',
'supprimer_tout_jeu' => 'Effacer TOUS les r&eacute;sultats du jeu',
'supprimer_tout_auteur' => "ALLE Ergebnisse von Autor l&#8217; l&ouml;schen",
'supprimer_confirmer' => 'L&ouml;schen der Ergebnisse bet&auml;tigen',
'explication_supprimer_tout'=> 'Cette commande efface <i>tous</i> les r&eacute;sultats des diff&eacute;rents jeux&hellip;',
'explication_supprimer_jeu'=> 'Cette commande efface <i>tous</i> les r&eacute;sultats du jeu&hellip;',
'explication_supprimer_auteur'=> 'Dieser Befehl l&ouml;scht <i>alle</i> des Autors',
'confirmation' => 'Best&auml;tigen',
'confirmation_supprimer_tout' => 'Sind sie sicher, dass ALLE Ergebnisse gel&ouml;scht werden sollen?',
'confirmation_supprimer_jeu' => 'Sind sie sicher, dass ALLE Ergebnisse dieses Spiels gel&ouml;scht werden sollen?',
'confirmation_supprimer_auteur' => 'Sind sie sicher, dass ALLE Ergebnisse dieses Autors gel&ouml;scht werden sollen?',
'gerer_resultats_auteur' => 'Ergebnisse von @nom@ verwalten',
'infos_auteur'  => 'Informationen',
'gerer_resultats_tout'  => 'Ergebnisse aller Spiele verwalten',
'configurer_jeux' => 'Plugin Spiele konfigurieren',

'sans_titre_prive' => 'Ohne internen Titel',
'sans_type' => 'Ohne Typ',
'introuvable' => 'Kein Spiel vorhanden!',
'pas_de_jeu' => "Pardon, dieses Spiel existiert nicht!",
'pas_d_auteur' => "Pardon, dieser Autor ist nicht bekannt!",
'inserer_jeu' => "Spiele einf&uuml;gen",
'inserer_jeu_explication' =>"Sie k�nnen Spiele einf&uuml;gen, indem Sie &#60;jeuXXX&#62; schreiben,  XXX id die Nummer des SPiels",

'compacter_tout_tout' => 'ALlE Ergebnisse komprimieren',
'compacter_tout_jeu' => 'ALLE Ergebnisse des Spiels komprimieren',
'compacter_tout_auteur' => 'ALLE Ergebnisse des Autors komprimieren',
'explication_compacter_tout'=>'Dieser Befehl l&ouml;scht <i>alle</i> Ergebnisse der Spiele ausser der jeweils letzten Kombination Spiel/Autor',
'explication_compacter_jeu'=>'Dieser Befehl l&ouml;scht <i>alle</i> Ergebnisse dieses Spiels ausser dem jeweils letzten eines Autors',
'explication_compacter_auteur'=>"Cette commande  efface <i>tous</i> les r&eacute;sultats de cet auteur, sauf le dernier pour chaque jeu",
'confirmation_compacter_tout' => '&Sind sie sicher, dass ALLE Ergebnisse komprimiert werden sollen?',
'confirmation_compacter_auteur' => 'Sind sie sicher, dass ALLE Ergebnisse dieses Autors komprimiert werden sollen?',
'confirmation_compacter_jeu' => 'Sind sie sicher, dass ALLE Ergebnisse dieses Spiels komprimiert werden sollen?',
'compacter_confirmer' => 'Komprimieren der Ergebnisse best&auml;tigen',
'statut_jeu'  	=> 'Status des Spiels: ',
'titres_jeu'  	=> 'Titre(s) du jeu&nbsp;: ',
'configuration_interne' => 'Configuration interne&nbsp;: ',
'configuration_generale' => 'Configuration g&eacute;n&eacute;rale du plugin',

'resultat_defaut' => 'Par d&eacute;faut',
'resultat_aucun' => 'Aucun',
'resultat_premier' => 'Le premier',
'resultat_dernier' => 'Le dernier',
'resultat_meilleur' => 'Le meilleur',
'resultat_meilleurs' => 'Les meilleurs',
'resultat_tous' => 'Tous',
'resultat2_defaut' => 'Selon la configuration g&eacute;n&eacute;rale du plugin',
'resultat2_aucun' => 'Pas d\'enregistrement',
'resultat2_premier' => 'Pour chaque auteur, le premier r&eacute;sultat uniquement',
'resultat2_dernier' => 'Pour chaque auteur, le dernier r&eacute;sultat uniquement',
'resultat2_meilleur' => 'Pour chaque auteur, le meilleur r&eacute;sultat uniquement',
'resultat2_meilleurs' => 'L\'ensemble des meilleurs r&eacute;sultats',
'resultat2_tous' => 'Tous les r&eacute;sultats',
'titre_xml' => 'Des jeux dans vos articles',

'doc_jeux' => 'Allegemeine Dokumentation',
'doc_scores' => 'Dokumentation der Ergebnisse',

// Corbeille
  'jeux_corbeille_tous' => "@nb@ Spiele im M&uuml;lleimer",
  'jeux_corbeille_un' => "1 Spiel im M&uuml;lleimer",
  'jeux_tous' => 'Alle Spiele',

);


?>