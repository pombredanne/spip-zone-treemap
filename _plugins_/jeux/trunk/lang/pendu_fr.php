<?php
if (!defined("_ECRIRE_INC_VERSION")) return;
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'titre'		=>'Le jeu du Pendu',
'titre_court'		=>'Pendu',

'regle'		=>"R&egrave;gles du jeu : Il s'agit de deviner un mot qui vous est propos&eacute;.
_ Vous devez choisir une lettre &agrave; chaque essai. Si la lettre existe dans le mot, elle appara&icirc;t &agrave; la bonne place.
_ La t&ecirc;te, un bras, une jambe... Au bout de la six&egrave;me erreur, vous &ecirc;tes pendu !",

'cliquez'	=> "Cliquez sur la lettre que vous voulez proposer :",

// javascript
'fini'		=> 'Pendu(e) !\n\nIl fallait trouver : ',

);


?>