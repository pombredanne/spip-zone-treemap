<?php
if (!defined("_ECRIRE_INC_VERSION")) return;
// This is a SPIP language file  --  Questo &egrave; un file di lingua di  SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'titre'		=>'Schema di sudoku',
'titre_court'		=>'Sudoku',
'table_summary'		=>'Schema di sudoku di @largeur@ colonne e @hauteur@ righe',

'regle'		=> "Regola del gioco: partendo dalle cifre gi&agrave; inserite, completa lo schema in modo che ogni riga, colonna e quadrato di @largeur@ per @hauteur@ contenga una sola volta tutte le cifre da 1 a @max@."

);


?>