<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/jeux?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'afficher_solution' => 'Lösung anzeigen',
	'alphabet' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ-', # NEW
	'aucune_erreur' => 'Kein Fehler',
	'auteur' => 'Autor',

	// B
	'bonneReponse' => 'Die richtige Antwort ist:', # MODIF
	'bravo' => 'Herzlichen Glückwunsch!',

	// C
	'cfg_affichage' => 'L\'affichage du jeu', # NEW
	'cfg_correction' => 'La correction du jeu', # NEW
	'cfg_donner_indices' => 'Afficher des indices', # NEW
	'cfg_donner_reponse' => 'Afficher la réponse', # NEW
	'cfg_donner_solution' => 'Donner les bonnes réponses', # NEW
	'cfg_introduction' => 'Les paramètres regroupés ici correspondent à la configuration par défaut de l\'ensemble des jeux du site. Ils peuvent être surchargées au cas par cas grâce à la configuration interne des jeux (séparateur [config]).<br/><br/>Il est possible que certains jeux n\'utilisent pas certains des paramètres listés ci-dessous.', # NEW
	'cfg_scores' => 'Les scores du jeu', # NEW
	'cfg_type_resultat' => 'Enregistrement des scores :', # NEW
	'colonne_n' => 'Spalte @n@',
	'configuration_jeux' => 'Configurer les jeux', # NEW
	'correction' => 'Lösung:',
	'corriger' => 'Antwort',

	// E
	'erreur_scripts' => 'Skripte verboten: Dieses Spiel kann nicht ausgeführt zerden!', # MODIF
	'erreur_syntaxe' => 'Syntaxfehler',
	'erreur_taille_grille' => 'Fehler: Ungültige Grö',

	// G
	'grille_invalide' => 'Ungültiges Raster',
	'grille_valide' => 'Gültiges Raster',

	// I
	'indices' => 'Tipps zum Ausfüllen',

	// J
	'jeu_type' => 'Typ',
	'jeu_vide' => 'LEER',
	'jeux' => 'Spiele',

	// L
	'ligne_n' => 'Zeile @n@',

	// M
	'modules_dispos' => 'Modules disponibles', # NEW
	'multi_jeux' => 'Multi Jeux', # NEW

	// N
	'n_erreurs' => '@n@ Fehler',
	'n_vides' => '@n@ leere Zellen',
	'nouveau_jeu' => 'Neues Spiel',

	// O
	'options' => 'Options', # NEW

	// P
	'point' => ' Punkt', # MODIF
	'points' => ' Punkte', # MODIF

	// R
	'recommencer' => 'Neuer Versuch',
	'reinitialiser' => 'Neuer Versuch', # MODIF
	'rejouer' => 'Nochmals spielen',
	'reponse' => 'Antwort', # MODIF
	'reponseFausse' => 'Falsch!', # MODIF
	'reponseJuste' => 'Richtig!', # MODIF
	'reponseNulle' => 'Unbeantwortet!', # MODIF
	'resultat' => 'Ergebnis', # MODIF
	'resultat2_' => 'Non défini ?', # NEW
	'resultat2_aucun' => 'Pas d\'enregistrement',
	'resultat2_defaut' => 'Selon la configuration générale du plugin', # MODIF
	'resultat2_dernier' => 'Pour chaque auteur, le dernier résultat uniquement', # MODIF
	'resultat2_meilleur' => 'Pour chaque auteur, le meilleur résultat uniquement', # MODIF
	'resultat2_meilleurs' => 'L\'ensemble des meilleurs résultats', # MODIF
	'resultat2_premier' => 'Pour chaque auteur, le premier résultat uniquement', # MODIF
	'resultat2_tous' => 'Tous les résultats', # MODIF
	'resultat_aucun' => 'Aucun',
	'resultat_defaut' => 'Par défaut', # MODIF
	'resultat_dernier' => 'Le dernier',
	'resultat_meilleur' => 'Le meilleur',
	'resultat_meilleurs' => 'Les meilleurs',
	'resultat_premier' => 'Le premier',
	'resultat_tous' => 'Tous',
	'resultats' => 'Ergebnisse', # MODIF
	'resultats_auteur' => 'Die Ergebnisse von @nom@', # MODIF
	'resultats_jeu' => 'Ergebnisse von Spiel no. @id@ (@nom@)', # MODIF
	'resultats_jeux' => 'Résultats des jeux', # NEW

	// S
	'sans_titre_prive' => 'Ohne internen Titel', # MODIF
	'sans_type' => 'Ohne Typ',
	'score' => 'Ergebnis:', # MODIF
	'solution' => 'Lösung',

	// T
	'titre_page_configurer_jeux' => 'Configuration du plugin jeux', # NEW
	'total' => 'Total', # NEW
	'type_resultat' => 'Enregistrement des scores :', # MODIF

	// U
	'une_erreur' => 'Ein Fehler',
	'une_vide' => 'Eine leere Zelle',

	// V
	'verifier' => 'Überprüfen', # MODIF
	'verifier_validite' => 'Validität überprüfen ', # MODIF
	'voir_auteur' => 'Voir l\'auteur', # NEW
	'voir_jeu' => 'Spiel ansehen',
	'voir_resultats' => 'Ergebnisse ansehen', # MODIF
	'votre_choix' => 'Deine Antwort: ',
	'votre_reponse' => 'Deine Antwort: ' # MODIF
);

?>
