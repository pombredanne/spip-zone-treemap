<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/jeux?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'afficher_solution' => 'Show the solution',
	'alphabet' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ-',
	'aucune_erreur' => 'No error',
	'auteur' => 'Auteur',

	// B
	'bonneReponse' => 'The correct answer is:', # MODIF
	'bravo' => 'Congratulation !',

	// C
	'cfg_affichage' => 'L\'affichage du jeu',
	'cfg_correction' => 'La correction du jeu',
	'cfg_donner_indices' => 'Afficher des indices',
	'cfg_donner_reponse' => 'Afficher la réponse', # MODIF
	'cfg_donner_solution' => 'Donner les bonnes réponses', # MODIF
	'cfg_introduction' => 'Les paramètres regroupés ici correspondent à la configuration par défaut de l\'ensemble des jeux du site. Ils peuvent être surchargées au cas par cas grâce à la configuration interne des jeux (séparateur [config]).<br/><br/>Il est possible que certains jeux n\'utilisent pas certains des paramètres listés ci-dessous.', # MODIF
	'cfg_scores' => 'Les scores du jeu',
	'cfg_type_resultat' => 'Enregistrement des scores :', # MODIF
	'colonne_n' => 'column @n@',
	'configuration_jeux' => 'Configurer les jeux', # NEW
	'correction' => 'Correction:',
	'corriger' => 'Check',

	// E
	'erreur_scripts' => 'Scripts not allowed: you can not play this game here!', # MODIF
	'erreur_syntaxe' => 'Syntax error',
	'erreur_taille_grille' => 'Error: invalid size',

	// G
	'grille_invalide' => 'The grid is invalid',
	'grille_valide' => 'The grid is valid',

	// I
	'indices' => 'Clues',

	// J
	'jeu_type' => 'Type',
	'jeu_vide' => 'VIDE',
	'jeux' => 'Games',

	// L
	'ligne_n' => 'row @n@',

	// M
	'modules_dispos' => 'Modules disponibles', # NEW
	'multi_jeux' => 'Multi Jeux', # NEW

	// N
	'n_erreurs' => '@n@ errors',
	'n_vides' => '@n@ empty cells',
	'nouveau_jeu' => 'New game',

	// O
	'options' => 'Options', # NEW

	// P
	'point' => ' pt', # MODIF
	'points' => ' pts', # MODIF

	// R
	'recommencer' => 'Restart',
	'reinitialiser' => 'Reset', # MODIF
	'rejouer' => 'Rejouer',
	'reponse' => 'Answer', # MODIF
	'reponseFausse' => 'You were incorrect!', # MODIF
	'reponseJuste' => 'You were correct!', # MODIF
	'reponseNulle' => 'You didn\'t answer this question!', # MODIF
	'resultat' => 'Résultat', # MODIF
	'resultat2_' => 'Non défini ?', # MODIF
	'resultat2_aucun' => 'Pas d\'enregistrement',
	'resultat2_defaut' => 'Selon la configuration générale du plugin', # MODIF
	'resultat2_dernier' => 'Pour chaque auteur, le dernier résultat uniquement', # MODIF
	'resultat2_meilleur' => 'Pour chaque auteur, le meilleur résultat uniquement', # MODIF
	'resultat2_meilleurs' => 'L\'ensemble des meilleurs résultats', # MODIF
	'resultat2_premier' => 'Pour chaque auteur, le premier résultat uniquement', # MODIF
	'resultat2_tous' => 'Tous les résultats', # MODIF
	'resultat_aucun' => 'Aucun',
	'resultat_defaut' => 'Par défaut', # MODIF
	'resultat_dernier' => 'Le dernier',
	'resultat_meilleur' => 'Le meilleur',
	'resultat_meilleurs' => 'Les meilleurs',
	'resultat_premier' => 'Le premier',
	'resultat_tous' => 'Tous',
	'resultats' => 'Résultats', # MODIF
	'resultats_auteur' => 'Les résultats de @nom@', # MODIF
	'resultats_jeu' => 'Les résultats du jeu numéro @id@ (@nom@)', # MODIF
	'resultats_jeux' => 'Résultats des jeux', # NEW

	// S
	'sans_titre_prive' => 'Sans titre privé', # MODIF
	'sans_type' => 'Sans type',
	'score' => 'Score:', # MODIF
	'solution' => 'Solution',

	// T
	'titre_page_configurer_jeux' => 'Configuration du plugin jeux', # NEW
	'total' => 'Total', # NEW
	'type_resultat' => 'Type de résultats', # MODIF

	// U
	'une_erreur' => 'An error',
	'une_vide' => 'One empty cell',

	// V
	'verifier' => 'Check', # MODIF
	'verifier_validite' => 'Check the validity ', # MODIF
	'voir_auteur' => 'Voir l\'auteur', # NEW
	'voir_jeu' => 'Voir le jeu',
	'voir_resultats' => 'Voir les résultats', # MODIF
	'votre_choix' => 'Your answer: ',
	'votre_reponse' => 'Your answer: ' # MODIF
);

?>
