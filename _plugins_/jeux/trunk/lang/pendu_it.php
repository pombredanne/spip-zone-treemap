<?php
if (!defined("_ECRIRE_INC_VERSION")) return;
// This is a SPIP language file  --  Questo e un file di lingua di  SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'titre'		=> "Il gioco dell'impiccato",
'regle'		=> "Regole del gioco: si tratta di indovinare una parola che viene proposta.
_ Ad ogni tentativo devi scegliere una lettera. Se la lettera esiste nella parola, allora compare nel posto giusto.
_ La testa, un braccio, una gamba... Dopo il sesto errore vieni impiccato!",

'cliquez'	=> "Clicca sulla lettera che vuoi scegliere:",

// javascript
'fini'		=> 'Impiccato(a) !\n\nbisognava trovare: ',

);


?>