<?php
if (!defined("_ECRIRE_INC_VERSION")) return;
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'compacter'     =>'Compacter les résultats',
'confirmer_choix'=>'Il vous faut confirmer votre choix',
'pour_auteur'   =>'Pour cet auteur :',
'pour_jeu'      =>'Pour ce jeu :',
'pour_tous'     =>'Pour tout les résultats :',
'resultats_compactes'   =>'Résultats compactés !',
'resultats_supprimes'   =>'Résultats supprimés !',
'supprimer'     =>'Supprimer les résultats'

);
?>