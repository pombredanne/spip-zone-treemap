<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-eudock
// Langue: fr
// Date: 08-05-2012 08:55:50
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// E
	'eudock_description' => '{{Effet \'Dock\' à la mode \'Apple\' en javascript}}

Ce plugin ajoute une bibliothèque javascript permettant de créer un effet \'Dock\' sur une liste d\'objets images ({documents ou logos du site}).

Une documentation interne est disponible lorsque le plugin est actif sur la page publique [eudock_documentation->../?page=eudock_documentation].',
	'eudock_slogan' => 'Effet \'Dock\' à la mode \'Apple\' en javascript',
	'eudock_nom' => 'EU Dock',
);
?>