<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_encart' => "Ajouter un encart",

	// B
	'bouton_supprimer_encart' => "Supprimer",

	// E
	'editer_encart' => "Editer un encart",
	'nouvel_encart' => "Nouvel encart",

	// I
	'info_gauche_numero_encart' => "N&deg;",
	'info_encart_utilisee_par' => "Cet encart est utilis&eacute; par :",
	
	// L
	'label_titre' => 'Titre',
	'label_texte' => 'Texte',
	
	// M
	'message_aucun_encart' => 'Cet article ne poss&egrave;de aucun encart; pour le moment.',
	'modifier_encart' => 'Modifier',
	
	// S
	'sans_titre' => '(sans titre)',
	'supprimer_encart_question' => "&Ecirc;tes-vous s&ucirc;r de vouloir supprimer l'encart ?",

	// T
	'titre_bloc_encarts' => 'Encarts'
	
);

?>
