<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-ck?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'ck_description' => 'Simplicité, efficacité, légèreté.
_ Un couteau qui tient vraiment dans la poche en
une unique page de configuration pour les réglages cachés de SPIP.',
	'ck_nom' => 'Coltellino KISS',
	'ck_slogan' => 'Simplifier certains réglages SPIP'
);

?>
