<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/174?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_cache_taille_mini' => 'De grootte van de cache kan niet kleiner dan 10Mo zijn',
	'erreur_dossier_squelette_invalide' => 'Skelettendossier kan geen absolute path zijn noch <tt>../</tt> referentie bevatten',
	'explication_dossier_squelettes' => 'Je mag meerdere dossiers in volgorde aanduiden. Scheid ze met  \':\' . "<tt>squelettes</tt>" dossier wordt altijd ten laatste in acht genomen of hij bestaat.',
	'explication_image_seuil_document' => 'De geuploaden beelden kunnen vanaf een bepaalde breedte automatisch naar het document modus veranderd worden ',
	'explication_introduction_suite' => 'De opvolgingspunten worden door de baken <tt>#INTRODUCTION</tt> bijgevoegd wanneer ze een tekst verkort. Standaard is <tt> (...)</tt>',

	// L
	'label_cache_duree' => 'Duur van de cache (s)',
	'label_cache_duree_recherche' => 'Duur van de cache voor het zoeken',
	'label_cache_strategie' => 'Strategie van de cache',
	'label_cache_strategie_jamais' => 'Geen cache (deze optie wordt na 24u automatisch verwijderd)',
	'label_cache_strategie_normale' => 'Cache voor bepaalde tijd',
	'label_cache_strategie_permanent' => 'Cache voor onbepaalde tijd',
	'label_cache_taille' => 'Grootte van de cache (Mo)',
	'label_compacte_head_ecrire' => 'CSS en javascript altijd samenpersen',
	'label_derniere_modif_invalide' => 'De cache vernieuwen bij elke nieuwe publicatie',
	'label_docs_seuils' => 'De grootte van de documenten beperken bij het uploaden',
	'label_dossier_squelettes' => '<tt>Skeletten</tt> dossier ',
	'label_forcer_lang' => 'De taal van de url of van de bezoeker dwingen (<tt>$forcer_lang</ tt>) ',
	'label_image_seuil_document' => 'Breedte van de beelden in document modus',
	'label_imgs_seuils' => 'De grootte van de beelden beperken tijdens het downloaden',
	'label_inhiber_javascript_ecrire' => 'Javascript deactiveren in de artikels',
	'label_introduction_suite' => 'Opvolgpunten',
	'label_logo_seuils' => 'De grootte van logo\'s bij het uploaden beperken ',
	'label_longueur_login_mini' => 'Minimale lengt van de logins',
	'label_max_height' => 'Max hoogte (pixel)',
	'label_max_size' => 'Max gewicht (ko)',
	'label_max_width' => 'Max breedte (pixel)',
	'label_nb_objets_tranches' => 'Aantal objecten in de lijsten',
	'label_no_autobr' => 'Het in acht nemen van alineas (enig lijn terugkeer) in de tekst desactiveren',
	'label_no_set_html_base' => '<tt>&lt;base href="..."&gt;</tt> NIET automatisch bijvoegen',
	'label_options_ecrire_perfo' => 'Prestatie',
	'label_options_ecrire_secu' => 'Veiligheid',
	'label_options_skel' => 'Berekening van de pagina\'s ',
	'label_options_typo' => 'Verwerkingen van de teksten',
	'label_supprimer_numero' => 'Nummers van de titels automatisch afschaffen',
	'label_toujours_paragrapher' => 'Alle paragrafen met een <tt>&lt;p&gt;</tt> bebakenen (zelfs als er maar een paragraaf is in de tekst)',
	'legend_cache_controle' => 'Cachebeheer',
	'legend_espace_prive' => 'Privé ruimte ',
	'legend_image_documents' => 'Beelden en documenten ',
	'legend_site_public' => 'Publieke website',

	// M
	'message_ok' => 'Je configuratiekeuzen werden in het bestand <tt>@file@</tt> opgeslagen en zijn vanaf nu toegepast.',

	// T
	'texte_boite_info' => 'Een pagina om enkele verborgen instellingen van SPIP gemakkelijk te veranderen.

Of je een aantal afstellingen in het bestand <tt>config/mes_options.php</tt> bepaalt, dan zal dit formulier geen effect hebben op die afstellingen.

Wanner de configuratie van uw site klaar is kunt U de inhoud van het bestand <tt>tmp/ck_options.php</tt> in het bestand <tt>config/mes_options.php</tt> kopiëren en dan de plugin uitschakelen aangezien hij dan niet meer nuttig is.',
	'titre_page_couteau' => 'KISS Mes'
);

?>
