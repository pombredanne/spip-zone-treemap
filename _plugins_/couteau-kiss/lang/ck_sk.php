<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/174?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_cache_taille_mini' => 'Cache nemôže mať menšiu veľkosť ako 10 MB',
	'erreur_dossier_squelette_invalide' => 'Priečinok šablóny nemôže byť  absolútne umiestnenie ani nemôže obsahovať odkaz <tt>../</tt>',
	'explication_dossier_squelettes' => 'Môžete zadať viacero priečinkov a oddeliť ich ":", ktoré sa budú brať do úvahy po poradí. Priečinok s názvom "<tt>squelettes</tt>" sa vždy berie do úvahy ako posledný, ak existuje.',
	'explication_image_seuil_document' => 'Stiahnuté obrázky môžu byť automaticky presunuté do režimu dokumentu mimo predvolenej šírky',
	'explication_introduction_suite' => 'Nasledujúce body boli pridané cez tag <tt>#INTRODUCTION</tt> pri vystrihnutí textu. Podľa predvolených nastavení <tt> (...)</tt>',

	// L
	'label_cache_duree' => 'Trvanie cache (v s)',
	'label_cache_duree_recherche' => 'Trvanie cache vyhľadávania (v s)',
	'label_cache_strategie' => 'Stratégia cache',
	'label_cache_strategie_jamais' => 'Bez cache (táto možnosť bude zrušená po 24 hodinách)',
	'label_cache_strategie_normale' => 'Obmedzené trvanie chache',
	'label_cache_strategie_permanent' => 'Neobmedzené trvanie chache',
	'label_cache_taille' => 'Veľkosť cache (v MB)',
	'label_compacte_head_ecrire' => 'Vždy komprimovať CSS a javascript',
	'label_derniere_modif_invalide' => 'Aktualizovať cache vždy po novom publikovaní',
	'label_docs_seuils' => 'Obmedziť veľkosť dokumentov pri sťahovaní',
	'label_dossier_squelettes' => 'Priečinok <tt>squelettes</tt>',
	'label_forcer_lang' => 'Predvoliť jazyk internetovej adresy alebo návštevníka (<tt>$forcer_lang</tt>)',
	'label_image_seuil_document' => 'Šírka obrázkov v režime dokumentu',
	'label_imgs_seuils' => 'Obmedziť veľkosť obrázkov pri sťahovaní',
	'label_inhiber_javascript_ecrire' => 'Deaktivovať javascript v článkoch',
	'label_introduction_suite' => 'Nasledujúce body',
	'label_logo_seuils' => 'Obmedziť veľkosť log pri sťahovaní',
	'label_longueur_login_mini' => 'Minimálna dĺžka prihlasovacích mien',
	'label_max_height' => 'Maximálna výška (v pixeloch)',
	'label_max_size' => 'Maximálna veľkosť (kB)',
	'label_max_width' => 'Maximálna šírka (v pixeloch)',
	'label_nb_objets_tranches' => 'Počet objektov v zoznamoch',
	'label_no_autobr' => 'Deaktivovať vkladanie odsekov  do textu (prechod na nový riadok)',
	'label_no_set_html_base' => 'Žiadne automatické pridávanie <tt>&lt;základný odkaz href="..."&gt;</tt>',
	'label_options_ecrire_perfo' => 'Výkon',
	'label_options_ecrire_secu' => 'Zabezpečenie',
	'label_options_skel' => 'Počítanie stránok',
	'label_options_typo' => 'Spracovanie textov',
	'label_supprimer_numero' => 'Automaticky odstrániť čísla názvov',
	'label_toujours_paragrapher' => 'Všetky odseky dajte do <tt>&lt;p&gt;</tt> (dokonca aj text má jeden odsek)',
	'legend_cache_controle' => 'Ovládanie cache',
	'legend_espace_prive' => 'Súkromná stránka',
	'legend_image_documents' => 'Obrázky a dokumenty',
	'legend_site_public' => 'Verejne prístupná stránka',

	// M
	'message_ok' => 'Vaše nastavenia boli zohľadnené a uložené do súboru <tt>@file@</tt>. Teraz sa aplikujú.',

	// T
	'texte_boite_info' => 'Táto stránka vám umožňuje ľahko upraviť skryté nastavenia SPIPu.

Ak si niektoré nastavenia vynútite vo svojom súbore <tt>config/mes_options.php</tt>, tento formulár ich neovplyvní.

Keď skončíte s nastavovaním svojej stránky, môžete, ak chcete, skopírovať a prilepiť obsah súboru <tt>tmp/ck_options</tt> do <tt>config/mes_options.php</tt> pred odinštalovaním tohto zásuvného modulu, ktorý už nebudete potrebovať.',
	'titre_page_couteau' => 'Couteau KISS'
);

?>
