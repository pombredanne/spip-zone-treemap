<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-twidget?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(


	'explication_commun_fieldset' => "Configuration Nécessaire à l'utilisation du plugin Twitdget. Partie commune entre le widget profil et le widget recherche",
	'explication_recherche_fieldset' => "Configuration pour le widget de recherche",
	'explication_profil_fieldset' => "Configuration Nécessaire à l'utilisation du widget profil ",

	'label_search' => "Recherche. Terme recherché, l'usage de tweeter implique souvent de précéder le terme par un # ",
	'label_interval' => "Intervalle de temps après lequel la page tweeter est rechargée",
	'label_subject' => "Sujet de la fenêtre tweete",
	'label_title' => "Titre de la fenêtre",
	'label_width' => "Largeur de la fenêtre",
	'label_height' => "Hauteur de la fenêtre",
	'label_shell_background' => "Couleur de fond de la fenêtre tweeter",
	'label_shell_color' => "Couleur du texte",
	'label_tweets_background' => "Couleur fond tweet",
	'label_tweets_color' => "Couleur de fond des tweet",
	'label_tweets_link' => "Couleur des liens",
	'label_rpp' => "Nombre de résultats par page",
	'label_user' => "Utilisateur twitter (ne pas précéder du @)",

	'legend_commun_fieldset' => "Configuration Commune",
	'legend_recherche_fieldset' => "Recherche",
	'legend_profil_fieldset' => "Profil",


	// T
	'titre_config' => "Configuration du plugin Twidget",
	'titre_twidget' => "Twidget",
	'titre_configuration' => "Configuration du plugin Twidget"
);

?>
