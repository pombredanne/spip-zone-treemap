<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-twidget?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'twidget_description' => 'Twitter widget eenvoudig te installeren
_ Zet een inclusie in je skeletten om de widget te wijzen:
_ <code>#INCLURE{fond=inclure/twidget_profile,user=GusLeLapin}</code>
 of <code>#INCLURE{fond=inclure/twidget_search,search=#SPIP,title=\'Suivez twitter\',subject=\'SPIP\'}</code>

De plugin acteer als een  proxy om geen rechtstreekse verzoek van je bezoekers naar Twitter te provoceren en zo het sporen van hun activiteiten te voorkomen  (verdediging van de privacy).', # MODIF
	'twidget_slogan' => 'Twitter widget eenvoudig te installeren'
);

?>
