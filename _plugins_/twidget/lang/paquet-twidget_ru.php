<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-twidget?lang_cible=ru
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'twidget_description' => 'Виджет Twitter легко установить 
_ Добавьте код в шаблон для того чтобы отобразить виджет :
_ <code>#INCLURE{fond=inclure/twidget_profile,user=GusLeLapin}</code>
 или
<code>#INCLURE{fond=inclure/twidget_search,search=#SPIP,title=\'Suivez twitter\',subject=\'SPIP\'}</code>

Плагин работает по принципу прокси, таким образом, посетители не делают никаких запросов, и проследить их активность становится невозможным.', # MODIF
	'twidget_slogan' => 'Виджет Twitter легко установить '
);

?>
