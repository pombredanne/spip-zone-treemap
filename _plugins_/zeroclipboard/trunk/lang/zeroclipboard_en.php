<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/zeroclipboard?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'link_text_copied' => 'Copied',
	'link_text_copy' => 'Copy',
	'link_title_copied' => 'The content has been copied to clipboard',
	'link_title_copy' => 'Copy to clipboard'
);

?>
