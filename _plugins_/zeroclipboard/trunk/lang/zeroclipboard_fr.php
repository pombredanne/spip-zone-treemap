<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/zeroclipboard/trunk/lang
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'link_text_copied' => 'Copié',
	'link_text_copy' => 'Copier',
	'link_title_copied' => 'Le contenu est copié dans le presse-papier',
	'link_title_copy' => 'Copier dans le presse-papier'
);

?>
