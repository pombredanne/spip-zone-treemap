<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-zeroclipboard?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// Z
	'zeroclipboard_description' => 'Adds a "Copy" link before each element with the class <code>.copypaste</code> and place its contents into the clipboard when clicked',
	'zeroclipboard_nom' => 'Zeroclipboard',
	'zeroclipboard_slogan' => 'Easy copy / paste'
);

?>
