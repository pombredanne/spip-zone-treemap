<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-redirhttps?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// R
	'redirhttps_description' => 'Redirección hacía HTTPS cuando el contendio le necesita (espacio privado, login, páginas públicas con el usuario conectado)',
	'redirhttps_nom' => 'Redireciones HTTPS',
	'redirhttps_slogan' => 'Sécuriser l\'accès à certaines pages du site' # NEW
);

?>
