<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-redirhttps?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// R
	'redirhttps_description' => 'Presmerovanie na HTTPS, ak si to vyžaduje obsah (súkromná zóna, prihlásenie, používateľ prihlásený cez verejne prístupné stránky)',
	'redirhttps_nom' => 'Presmerovania HTTPS',
	'redirhttps_slogan' => 'Zabezpečiť prístup na určité stránky'
);

?>
