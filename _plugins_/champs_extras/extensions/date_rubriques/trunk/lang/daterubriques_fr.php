<?php
if (!defined("_ECRIRE_INC_VERSION")) return;
$GLOBALS[$GLOBALS['idx_lang']] = array(
	//A
	'aucun'=>'aucun',
	'aucune_rubrique_dans_secteur'=>'Il n\'y a aucune rubrique dans ce secteur',

	//C
	'configurer_date_rubriques'=>'Configurer les dates des rubriques',

	//B
	'bando_date_rubriques'=>'Dates des rubriques',
	'bouton_attribuer'=>'Attribuer',
	'bouton_previsualiser'=>'Prévisualiser',

	//D
	'date_label' => 'Date : ',
	'date_premier_article'=>'Attribuer à chaque rubrique d\'un secteur la date de son article le plus ancien',

	//E
	'entrer_id_secteur'=>'Entrer un numéro de secteur',
	'entrer_liste_secteurs'=>'Entrez les numéros des secteurs séparés par une virgule',
	'explication_secteurs' => 'Choisir le ou les secteurs pour lesquels rajouter ce champ supplémentaire. Ne rien sélectionner pour que le champ soit rajouté à toutes les rubriques du site.',

	//I
	'id_secteur'=>'id_secteur',
	'info_rubriques_secteur'=>'Rubriques du secteur @numero@',
	
	//L
	'label_secteurs' => 'Secteurs concernés',

	//M
	'mise_a_jour_rubriques'=>'Les dates des rubriques ont été mises a jour',

	//R
	'restreindre_secteurs'=>'Restreindre aux secteurs suivants',
	'rubriques_liste'=>'Dater les rubriques par lot',

	//V
	'valeur_incorrecte'=>'Ce n\'est pas vraiment un numéro!',
);
?>