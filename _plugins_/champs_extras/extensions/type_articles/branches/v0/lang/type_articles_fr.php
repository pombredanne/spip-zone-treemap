<?php
if (!defined("_ECRIRE_INC_VERSION")) return;
// Fichier ayant vocation à être surchargé depuis un autre plugin
$GLOBALS[$GLOBALS['idx_lang']] = array(
	// L
	'label_choix_type' => 'Type des articles :',

	// N
	'noisette_description' => 'Liste l\'ensemble des articles d\'un type spécifique du site, dans la même rubrique, dans une rubrique donnée...',
	'noisette_titre' => 'Liste des articles d\'un type spécifique',

	//T
	'titre' => "Type de l'article : ",
	'type_defaut'=>"Article",
	'type_1'=>"Nom du type 1",
	'type_2'=>"Nom du type 2"
);
?>