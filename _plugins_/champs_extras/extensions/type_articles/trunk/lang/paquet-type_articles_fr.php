<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-type_articles
// Langue: fr
// Date: 09-08-2012 19:23:58
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// T
	'type_articles_description' => 'Ajoute un champ "type" à l\'objet éditorial article. Les libellés des types possibles sont surchargeables dans votre fichier de langue',
	'type_articles_slogan' => 'Ajouter un champ "type" à vos articles',
);
?>