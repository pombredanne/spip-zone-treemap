<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
									   'titre_page' => 'Ordina gli @objets@',
									   'titre_tri_mots' => 'Ordinare gli @objets@ associati alla parola chiave @titre_mot@ del gruppo @type_mot@',
									   'tri_mots_help' => 'In questa pagina &egrave; possibile fare il drag&drop degli @objets@ per ordinarli come desiderato.<br>Questo ordine &egrave; relativo alla sola parola chiave @titre_mot@ del gruppo @type_mot@',
									   'voir' => 'vedi',
									   'ordonner' => 'Ordina gli @objets@ associati a:',
									   'rang' => 'posizione:',
									   'envoyer' => 'Salva il nuovo ordinamento'
);
?>
