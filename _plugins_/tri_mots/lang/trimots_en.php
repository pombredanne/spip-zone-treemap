<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
									   'titre_page' => 'Sort the @objets@',
									   'titre_tri_mots' => 'Sort the @objets@ of the keyword @titre_mot@ in group @type_mot@',
									   'tri_mots_help' => 'In this page, you can drag and drop the @objets@ to order them as you wish.<br>This ordering will be relative to the keyword @titre_mot@ of group @type_mot@',
									   'voir' => 'see',
									   'ordonner' => 'order the @objets@ relativelly to:',
									   'rang' => 'rank:',
									   'envoyer' => 'Save the new ordering'
);
?>
