<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'article_accueil' => 'Home article',
	'aucun_article_accueil' => 'No article',
	
	// L
	'label_id_article_accueil' => 'Home article',
	
	// R
	'rubrique_article_en_accueil' => 'Home article&nbsp;:',
);
?>