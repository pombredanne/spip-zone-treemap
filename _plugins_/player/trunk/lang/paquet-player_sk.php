<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-player?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'player_description' => 'Tento zásuvný modul umožňuje prehrávanie a zobrazenie zvukov alebo videí.
Pridáva údaje zo zariadení USB vo formáte .mp3.
Funguje na všetkých <code><docXX|player></code> vložených v texte, ako aj v šablónach.

-* v texte <code><docXX|player></code> zobrazí údaje zo zariadení USB (môžete si vybrať z viacerých zariadení),
-* v šablóne <code>#MODELE{playliste}</code> umožňuje zobraziť zoznam skladieb z posledných mp3',
	'player_slogan' => 'Prehrávanie zvukov a videí'
);

?>
