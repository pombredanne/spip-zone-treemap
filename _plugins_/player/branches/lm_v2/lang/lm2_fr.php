<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'cfg_descriptif_titre' => 'Configuration du plugin Lecteur multimedia',
	'cfg_explications_lecteur_son_inline' => 'Chaque fichier son dans les textes ayant comme attribut <code>rel="enclosure"</code> peuvent &ecirc;tre remplac&eacute;s par un lecteur sonore',
	'cfg_explications_lecteur_video' => 'Choisir parmi les lecteurs propos&eacute;s pour remplacer &lt;docX|video&gt;.',
	'cfg_label_aucun' => 'Aucun',
	'cfg_label_lecteur_audio' => 'Lecteur audio',
	'cfg_label_lecteur_son_inline' => 'Lecteur audio dans le texte',
	'cfg_label_lecteur_video' => 'Lecteur vid&eacute;o',
	'cfg_label_lecteur_video_lecteur' => 'Lecteur vid&eacute;o',
	'cfg_legende_documents_textes' => 'Lecteur pour les documents dans les textes',
	'cfg_lien_doc' => '<a href="http://multimedia.spip-zone.info" class="spip_out">Documentation</a>',

	// L
	'label_360animtransition' => 'Transition utilis&eacute; pour passer de l\'&eacute;tat "stop" &agrave; la lecture',
	'label_360animduration' => 'Dur&eacute;e de la transition (en ms)',
	'label_360loadringcolor' => 'Couleur de la taille du son d&eacute;j&agrave; charg&eacute;',
	'label_360playringcolor' => 'Couleur correspondant &agrave; la taille du son d&eacute;j&agrave; lu',
	'label_360backgroundringcolor' => 'Couleur de base du cercle de lecture',
	'label_neolao_video_bgcolor' => 'La couleur de fond',
	'label_neolao_video_bgcolor1' => 'La premi&egrave;re couleur du d&eacute;grad&eacute; du fond',
	'label_neolao_video_bgcolor2' => 'La seconde couleur du d&eacute;grad&eacute; du fond',
	'label_neolao_video_buffer' => 'Le nombre de seconde pour la m&eacute;moire tampon',
	'label_neolao_video_bufferbgcolor' => 'La couleur du fond du message du tampon',
	'label_neolao_video_buffercolor' => 'La couleur du texte du message du tampon',
	'label_neolao_video_buffermessage' => 'Le message de la m&eacute;moire tampon."_n_" indiquant le pourcentage',
	'label_neolao_video_buffershowbg' => '0 pour ne pas afficher le fond du message de tampon',
	'label_neolao_video_buttoncolor' => 'La couleur des boutons',
	'label_neolao_video_buttonovercolor' => 'La couleur des boutons au survol',
	'label_neolao_video_hauteur' => 'La hauteur (en px) par d&eacute;faut du lecteur',
	'label_neolao_video_largeur' => 'La largeur (en px) par d&eacute;faut du lecteur',
	'label_neolao_video_loadingcolor' => 'La couleur de la barre de chargement',
	'label_neolao_video_playeralpha' => 'La transparence du fond du lecteur (entre 0 et 100)',
	'label_neolao_video_playercolor' => 'La couleur du lecteur',
	'label_neolao_video_playertimeout' => 'Le d&eacute;lai en millisecondes avant que les boutons du lecteur ne se cachent (en mode autohide)',
	'label_neolao_video_showfullscreen' => 'Affichage du bouton plein &eacute;cran',
	'label_neolao_video_showloading' => 'Affichage du chargement',
	'label_neolao_video_showplayer' => 'Affichage de la barre des boutons',
	'label_neolao_video_showstop' => 'Afficher le bouton stop',
	'label_neolao_video_showtime' => 'Afficher les informations de dur&eacute;e',
	'label_neolao_video_showvolume' => 'Afficher la barre de volume',
	'label_neolao_video_slidercolor1' => 'La premi&egrave;re couleur du d&eacute;grad&eacute; de la barre',
	'label_neolao_video_slidercolor2' => 'La seconde couleur du d&eacute;grad&eacute; de la barre',
	'label_neolao_video_sliderovercolor' => 'La couleur de la barre au survol',
	'label_neolao_video_titlecolor' => 'La couleur du titre',
	'label_neolao_video_titlesize' => 'La taille de la police du titre',
	'label_neolao_video_type' => 'Le type du lecteur (en fonction du type certaines valeurs de cette configuration n\'auront aucun effet)',
	'legende_style_360player' => 'Styles du 360 player',
	'legende_style_neolao_video' => 'Styles du player Neolao Vid&eacute;o',
	'legende_style_players' => 'Styles du ou des lecteurs',

	// T
	'titre_liste_lecture' => 'Liste de lecture'

);

?>
