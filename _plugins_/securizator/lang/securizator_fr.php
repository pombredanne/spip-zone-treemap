<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

'htacces_invalide_1' => '\n\nDANGER\n\nLe r\xE9pertoire\n',
'htacces_invalide_2' => '\nest accessible depuis un navigateur.\n\nVous devez interdire l\'acc\xE8s \xE0 ce r\xE9pertoire, par exemple en y d\xE9posant un fichier .htaccess contenant la directive :\n\'deny from all\'.'

);
?>