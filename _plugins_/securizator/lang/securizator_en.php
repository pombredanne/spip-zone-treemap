<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

'htacces_invalide_1' => '\n\BEWARE\n\nThe directory\n',
'htacces_invalide_2' => '\nis accessible to whoever in a browser.\n\nYou must prohibit the access to this directory, by depositing a .htaccess file with the directive :\n\'deny from all\'.'

);
?>
