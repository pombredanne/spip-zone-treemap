<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-spipicious?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'spipicious_description' => 'Help authenticated users to add keywords/tags to objects from the public area.
_ Keywords are added to a group we can configure (\'{{- tags -}}\' by default)
_ Icon from [Pawel Kadysz->http://oneseventyseven.com/]',
	'spipicious_slogan' => 'Tag each objects'
);

?>
