<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/spipicious?lang_cible=ast
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'add_tags' => 'Amestar etiquetes',
	'aucun_tags' => 'Nun tien etiquetes asociáes.',
	'auteur_tag_usage' => 'Esti autor utiliza @nb@ veces esta etiqueta',

	// B
	'by' => 'Por',

	// C
	'cfg_description' => 'Esti complementu permite amestar fácilmente etiquetes a los artículos.', # MODIF

	// E
	'explication_ajout_tag' => 'P\'amestar delles etiquetes a la vez, utiliza el separador: "@separateur@".',
	'explication_suppression_tag' => 'Esbilla les etiquetes na llista darréu y valida pa desaniciales.',

	// I
	'identifier' => 'P\'amestar etiquetes a esti artículu, tienes d\'identificate',

	// L
	'label_tags' => 'Etiquetes:',
	'liste_auteurs_tag' => 'Esta etiqueta la utilicen:',

	// M
	'message_aucun_tags' => 'Nun tien etiquetes asociáes',
	'message_installation_activation' => 'Instalación de les tables y configuración predeterminada de spip.icio.us',
	'message_upgrade_database' => 'Anovamientu de la base de datos de spip.ici.ous versión @version@<br/>',

	// N
	'no_tags_associes' => 'Nun tienes una pallabra clave asociada',
	'nom_groupe_tag' => 'Grupu de pallabres clave asociáes',

	// S
	'spipicious' => 'Spip.icio.us',

	// T
	'tag_ajoute' => 'Amestasti la etiqueta:<br />@name@',
	'tag_deja_present' => 'Yá teníes amestao esta etiqueta.',
	'tag_supprime' => 'Desaniciasti la etiqueta:<br />@name@',
	'tags' => 'Etiquetes:',
	'tags_ajoutes' => 'Vienes d\'amestar delles etiquetes:<br />@name@', # MODIF
	'tags_supprimes' => 'Vienes de desaniciar delles etiquetes:<br />@name@', # MODIF
	'title_tag_utilise_nb' => 'Esta pallabra clave la usaron @nb@ persones.',
	'title_tag_utilise_un' => 'Esta pallabra clave la utilizó una persona.',
	'to' => 'con',
	'types_utilisateurs' => '¿Quién tien accesu al formulariu?', # MODIF

	// V
	'vos_tags' => 'Les tos etiquetes (cambéu)',

	// W
	'waiting' => 'gueta de les tos etiquetes'
);

?>
