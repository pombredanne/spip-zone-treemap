<?php

// lang/plom_fr.php


// $LastChangedRevision$
// $LastChangedBy$
// $LastChangedDate$
 
$GLOBALS['i18n_plom_fr'] = array(

	'mon_diplome' => "Mon dipl&#244;me"
	, 'obtenir_mon_diplome' => "Obtenir mon dipl&#244;me"
	, 'inconnu' => "Inconnu"
	
	, 'diplome_ligne_1' => "Les responsables de Spip-ville ont l&#39;immense honneur de d&#233;cerner ce jour le"
	, 'diplome_etudes_sup' => "Dipl&#244;me d'&#233;tudes sup&#233;rieures spipiennes"
	, 'diplome_ligne_2' => "&#224;"
	, 'diplome_ligne_3' => "Fait &#224; Spip-ville le "
	, 'diplome_ligne_4' => "pour en jouir avec les droits et pr&#233;rogatives qui y sont attach&#233;s."
	
	, 'pdf_sujet_defaut' => "Mon dipl&#244;me"
	, 'pdf_titre_defaut' => "Mon dipl&#244;me"
	
	, 'erreur_diplome_manquant_i' => "Votre dipl&#244;me est introuvable (code erreur #ID_@i@)"
	, 'erreur_fichier_s_manquant' => "Le fichier @s@ est introuvable."
	
);
