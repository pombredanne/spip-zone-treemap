<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_forum_longueur' => 'Tamanho das mensagens',
	'cfg_forum_longueur_maxi' => 'Tamanho máximo',
	'cfg_forum_longueur_mini' => 'Tamanho mínimo',
	'comment' => 'comentário',
	'comments' => 'comentários',
	'comments_h' => 'Seus comentários',

	// D
	'date_heure_a' => 'em',
	'date_jour_le' => 'Em',

	// F
	'forum_qui_etes_vous' => 'Quem é você?',

	// L
	'label_email' => 'E-mail (não divulgado)',
	'label_nom' => 'Nome',
	'label_url' => 'O seu website',

	// M
	'moderation_info' => 'Atenção, a sua mensagem só será exibida após ter sido relida e aprovada.',

	// P
	'permalink_to' => 'Link permanente para o comentário',

	// R
	'reponse_comment_modere' => 'O seu comentário foi gravado e está aguardando a releitura antes de ser publicado.',
	'reponse_comment_ok' => 'Obrigado pelo seu comentário!',

	// S
	'saisie_texte_info' => 'Este formulário aceita os atalhos de formatação SPIP <code>[->url] {{negrito}} {itálico} <quote> <code></code> e o código HTML <code><q> <del> <ins></code>. Para criar parágrafos, simplesmente deixe linhas em branco.',
	'saisie_texte_legend' => 'Inclua aqui o seu comentário',
	'submit1' => 'Visualizar',
	'submit2' => 'Confirmar o envio',

	// T
	'titre_comments' => 'Comentários'
);

?>
