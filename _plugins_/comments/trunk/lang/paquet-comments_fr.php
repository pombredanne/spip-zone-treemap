<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/comments/comments-200/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'comments_slogan' => 'Des commentaires, tout simplement',
	'comments_description' => 'ATTENTION, VERSION EN DEVELOPPEMENT, POUR SPIP&nbsp;3&nbsp;!<br />Affichage des messages en liste, façon commentaires de blog, avec formulaire simplifié. Commentaires microformatés, nomenclature homogène.',

);

?>
