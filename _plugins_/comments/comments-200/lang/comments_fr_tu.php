<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/comments?lang_cible=fr_tu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_forum_longueur' => 'Longueur des messages',
	'cfg_forum_longueur_maxi' => 'Longueur maximale',
	'cfg_forum_longueur_mini' => 'Longueur minimale',
	'cfg_option_aucun' => 'Aucun', # NEW
	'cfg_option_compteur' => 'Compteur (1.)', # NEW
	'cfg_option_diese' => 'Dièse (#)', # NEW
	'cfg_option_liste' => 'Liste de commentaires (à plat)', # NEW
	'cfg_option_picto' => 'Picto', # NEW
	'cfg_option_thread' => 'Enfilade de commentaires (en thread)', # NEW
	'cfg_option_thread_un' => 'Enfilade de commentaires (en thread) à un niveau', # NEW
	'cfg_permalien_label' => 'Apparence du lien permanent vers le commentaire', # NEW
	'cfg_permalien_legende' => 'Permalien', # NEW
	'cfg_type_label' => 'Présentation du fil de discussion', # NEW
	'cfg_type_legende' => 'Liste ou thread ?', # NEW
	'comment' => 'commentaire',
	'comments' => 'commentaires',
	'comments_h' => 'Tes commentaires',

	// D
	'date_heure_a' => 'à',
	'date_jour_le' => 'Le',

	// F
	'forum_attention_peu_caracteres' => '<b>Attention ! </b>ton message est trop court (@compte@ caractères) : pour pouvoir être enregistré, il ne doit pas être inférieur à @min@ caractères.', # NEW
	'forum_qui_etes_vous' => 'Qui es-tu ?',

	// L
	'label_email' => 'Courriel (non publié)',
	'label_nom' => 'Nom',
	'label_url' => 'Ton site web',

	// M
	'moderation_info' => 'Attention, ton message n\'apparaîtra qu\'après avoir été relu et approuvé.',

	// P
	'permalink_to' => 'Lien permanent vers le commentaire',

	// R
	'reponse_comment_modere' => 'Ton commentaire a bien été enregistré et est en attente de relecture avant publication.',
	'reponse_comment_ok' => 'Merci pour ton commentaire !',

	// S
	'saisie_texte_info' => 'Ce formulaire accepte les raccourcis SPIP <code>[-&gt;url] {{gras}} {italique} &lt;quote&gt; &lt;code&gt;</code> et le code HTML <code>&lt;q&gt; &lt;del&gt; &lt;ins&gt;</code>. Pour créer des paragraphes, laisse simplement des lignes vides.',
	'saisie_texte_legend' => 'Ajoute ton commentaire ici',
	'submit1' => 'Prévisualiser',
	'submit2' => 'Confirmer l\'envoi',

	// T
	'titre_comments' => 'Comments'
);

?>
