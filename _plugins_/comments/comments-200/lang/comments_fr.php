<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/comments/comments-200/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_forum_longueur' => 'Longueur des messages',
	'cfg_forum_longueur_maxi' => 'Longueur maximale',
	'cfg_forum_longueur_mini' => 'Longueur minimale',
	'cfg_option_aucun' => 'Aucun',
	'cfg_option_compteur' => 'Compteur (1.)',
	'cfg_option_diese' => 'Dièse (#)',
	'cfg_option_liste' => 'Liste de commentaires (à plat)',
	'cfg_option_picto' => 'Picto',
	'cfg_option_thread' => 'Enfilade de commentaires (en thread)',
	'cfg_option_thread_un' => 'Enfilade de commentaires (en thread) à un niveau',
	'cfg_permalien_label' => 'Apparence du lien permanent vers le commentaire',
	'cfg_permalien_legende' => 'Permalien',
	'cfg_type_label' => 'Présentation du fil de discussion',
	'cfg_type_legende' => 'Liste ou thread ?',
	'comment' => 'commentaire',
	'comments' => 'commentaires',
	'comments_h' => 'Vos commentaires',

	// D
	'date_heure_a' => 'à',
	'date_jour_le' => 'Le',

	// F
	'forum_attention_peu_caracteres' => '<b>Attention ! </b>votre message est trop court (@compte@ caractères) : pour pouvoir être enregistré, il ne doit pas être inférieur à @min@ caractères.',
	'forum_qui_etes_vous' => 'Qui êtes-vous ?',

	// L
	'label_email' => 'Courriel (non publié)',
	'label_nom' => 'Nom',
	'label_url' => 'Votre site web',

	// M
	'moderation_info' => 'Attention, votre message n\'apparaîtra qu\'après avoir été relu et approuvé.',

	// P
	'permalink_to' => 'Lien permanent vers le commentaire',

	// R
	'reponse_comment_modere' => 'Votre commentaire a bien été enregistré et est en attente de relecture avant publication.',
	'reponse_comment_ok' => 'Merci pour votre commentaire !',

	// S
	'saisie_texte_info' => 'Ce formulaire accepte les raccourcis SPIP <code>[-&gt;url] {{gras}} {italique} &lt;quote&gt; &lt;code&gt;</code> et le code HTML <code>&lt;q&gt; &lt;del&gt; &lt;ins&gt;</code>. Pour créer des paragraphes, laissez simplement des lignes vides.',
	'saisie_texte_legend' => 'Ajoutez votre commentaire ici',
	'submit1' => 'Prévisualiser',
	'submit2' => 'Confirmer l\'envoi',

	// T
	'titre_comments' => 'Comments'
);

?>
