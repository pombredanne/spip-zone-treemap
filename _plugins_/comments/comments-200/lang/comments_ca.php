<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/comments?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_forum_longueur' => 'Llargada dels missatges',
	'cfg_forum_longueur_maxi' => 'Llargada màxima',
	'cfg_forum_longueur_mini' => 'Llargada mínima',
	'cfg_option_aucun' => 'Aucun', # NEW
	'cfg_option_compteur' => 'Compteur (1.)', # NEW
	'cfg_option_diese' => 'Dièse (#)', # NEW
	'cfg_option_liste' => 'Liste de commentaires (à plat)', # NEW
	'cfg_option_picto' => 'Picto', # NEW
	'cfg_option_thread' => 'Enfilade de commentaires (en thread)', # NEW
	'cfg_option_thread_un' => 'Enfilade de commentaires (en thread) à un niveau', # NEW
	'cfg_permalien_label' => 'Apparence du lien permanent vers le commentaire', # NEW
	'cfg_permalien_legende' => 'Permalien', # NEW
	'cfg_type_label' => 'Présentation du fil de discussion', # NEW
	'cfg_type_legende' => 'Liste ou thread ?', # NEW
	'comment' => 'comentari',
	'comments' => 'comentaris',
	'comments_h' => 'Els teus comentaris',

	// D
	'date_heure_a' => 'a',
	'date_jour_le' => 'El',

	// F
	'forum_attention_peu_caracteres' => '<b>Attention ! </b>votre message est trop court (@compte@ caractères) : pour pouvoir être enregistré, il ne doit pas être inférieur à @min@ caractères.', # NEW
	'forum_qui_etes_vous' => 'Qui ets?',

	// L
	'label_email' => 'Correu electrònic (no publicat)',
	'label_nom' => 'Nom',
	'label_url' => 'El teu lloc Web',

	// M
	'moderation_info' => 'Atenció, el teu missatge no apareixerà fins que no hagi estat rellegit i aprovat. ',

	// P
	'permalink_to' => 'Enllaç permanent cap el comentari',

	// R
	'reponse_comment_modere' => 'El teu comentari s\'ha enregistrat correctament i està esperant la relectura abans de ser publicat.',
	'reponse_comment_ok' => 'Gràcies pel comentari!',

	// S
	'saisie_texte_info' => 'Aquest formulari accepta les dreceres SPIP <code>[-&gt;url] {{italique}} {italique} &lt;quote&gt; &lt;code&gt;</code> i el codi HTML <code>&lt;q&gt; &lt;del&gt; &lt;ins&gt;</code>. Per crear paràgrafs, deixeu simplement línies buides.',
	'saisie_texte_legend' => 'Afegeix el comentari aquí',
	'submit1' => 'Visualització prèvia',
	'submit2' => 'Confirmar l\'enviament',

	// T
	'titre_comments' => 'Comentaris'
);

?>
