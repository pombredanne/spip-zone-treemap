<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/comments?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_forum_longueur' => 'Dĺžka správ',
	'cfg_forum_longueur_maxi' => 'Maximálna dĺžka',
	'cfg_forum_longueur_mini' => 'Minimálna dĺžka',
	'cfg_option_aucun' => 'Žiadny',
	'cfg_option_compteur' => 'Počítadlo (1.)',
	'cfg_option_diese' => 'Krížik (#)',
	'cfg_option_liste' => 'Zoznam komentárov (jednoduchý)',
	'cfg_option_picto' => 'Pikto',
	'cfg_option_thread' => 'Rad komentárov (vo vlákne)',
	'cfg_option_thread_un' => 'Rad komentárov (vo vlákne) po úroveň',
	'cfg_permalien_label' => 'Vzhľad trvalého odkazu v komentári',
	'cfg_permalien_legende' => 'Trvalý odkaz',
	'cfg_type_label' => 'Spôsob zobrazenia priebehu diskusie',
	'cfg_type_legende' => 'Zoznam alebo vlákno?',
	'comment' => 'komentár',
	'comments' => 'komentárov',
	'comments_h' => 'Vaše komentáre',

	// D
	'date_heure_a' => 'o',
	'date_jour_le' => ' ',

	// F
	'forum_attention_peu_caracteres' => '<b>Pozor!</b> Váš príspevok je príliš krátky na to, aby mohol byť uložený (@compte@ znakov); musí mať minimálne @min@ znakov.',
	'forum_qui_etes_vous' => 'Kto ste?',

	// L
	'label_email' => 'E-mail (nezverejňuje sa)',
	'label_nom' => 'Meno',
	'label_url' => 'Vaša webová stránka',

	// M
	'moderation_info' => 'Upozornenie: Vaša správa sa zobrazí, až keď bude skontrolovaná a schválená.',

	// P
	'permalink_to' => 'Trvalý odkaz na komentár',

	// R
	'reponse_comment_modere' => 'Váš komentár bol uložený a čaká na kontrolu predtým, ako bude publikovaný.',
	'reponse_comment_ok' => 'Ďakujeme vám za komentár!',

	// S
	'saisie_texte_info' => 'Tento formulár akceptuje skratky SPIPU <code>[-&gt;urls] {{tučné}} {kurzíva} &lt;úvodzovky&gt; &lt;kód&gt;</code> a HTML kód <code>&lt;q&gt; &lt;del&gt; &lt;ins&gt;</code>. Ak chcete urobiť odsek, jednoducho vynechajte prázdne riadky.',
	'saisie_texte_legend' => 'Sem napíšte svoj komentár',
	'submit1' => 'Ukážka',
	'submit2' => 'Poslať',

	// T
	'titre_comments' => 'Komentáre'
);

?>
