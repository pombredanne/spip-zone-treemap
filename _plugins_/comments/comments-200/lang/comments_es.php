<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/comments?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_forum_longueur' => 'Longitud de los mensajes',
	'cfg_forum_longueur_maxi' => 'Longitud máxima',
	'cfg_forum_longueur_mini' => 'Longitud mínima',
	'cfg_option_aucun' => 'Aucun', # NEW
	'cfg_option_compteur' => 'Compteur (1.)', # NEW
	'cfg_option_diese' => 'Dièse (#)', # NEW
	'cfg_option_liste' => 'Liste de commentaires (à plat)', # NEW
	'cfg_option_picto' => 'Picto', # NEW
	'cfg_option_thread' => 'Enfilade de commentaires (en thread)', # NEW
	'cfg_option_thread_un' => 'Enfilade de commentaires (en thread) à un niveau', # NEW
	'cfg_permalien_label' => 'Apparence du lien permanent vers le commentaire', # NEW
	'cfg_permalien_legende' => 'Permalien', # NEW
	'cfg_type_label' => 'Présentation du fil de discussion', # NEW
	'cfg_type_legende' => 'Liste ou thread ?', # NEW
	'comment' => 'comentario',
	'comments' => 'comentarios',
	'comments_h' => 'Tus comentarios',

	// D
	'date_heure_a' => 'a',
	'date_jour_le' => 'El',

	// F
	'forum_attention_peu_caracteres' => '<b>Attention ! </b>votre message est trop court (@compte@ caractères) : pour pouvoir être enregistré, il ne doit pas être inférieur à @min@ caractères.', # NEW
	'forum_qui_etes_vous' => '¿Quién eres?',

	// L
	'label_email' => 'Email (no publicado)',
	'label_nom' => 'Nombre',
	'label_url' => 'Su sitio web',

	// M
	'moderation_info' => 'Aviso, su mensaje sólo será mostrado después de haber sido releído y aprobado.',

	// P
	'permalink_to' => 'Enlace permanente al comentario',

	// R
	'reponse_comment_modere' => 'Su comentario ha sido guardado y está a la espera de ser comprobado antes de que sea publicado.',
	'reponse_comment_ok' => '¡Gracias por su comentario!',

	// S
	'saisie_texte_info' => 'Este formulario acepta los atajos de SPIP, [-&gt;url] {{negrita}} {cursiva} &lt;quote&gt; &lt;code&gt;  y el código HTML. Para crear párrafos, dejen simplemente unas líneas vacías.',
	'saisie_texte_legend' => 'Añade tu comentario aquí',
	'submit1' => 'Previsualización',
	'submit2' => 'Confirmar el envio',

	// T
	'titre_comments' => 'Comments'
);

?>
