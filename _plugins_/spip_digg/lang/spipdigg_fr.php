<?php
//
// lang/spipdigg_fr.php
//

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajouter_un_digg'=>'Ajouter un Digg\'s',
// C
'configurer_les_digg'=>'Configurer Spip-Digg',
'ce_digg_est'=>'Ce Digg est',
// E
'en_preparation'=>'en pr&eacute;paration',
'enregistrer'=>'Enregistrer',
'editer_digg'=>'Editer le Digg',
// D
'descriptif_digg'=>'Descriptif :',
'dans_la_rubrique'=>'Dans la rubrique :',
'digg_numero'=>'Digg num&eacute;ro :',
// F
'forum_des_diggs'=>'Forum des Digg\'s',
// G
'gerer_les_diggs'=>'Gerer les Digg\'s',
// M
'mes_diggs'=>'Mes Digg\'s',
'mes_diggs_en_prepa'=>'Mes Digg\'s en pr&eacute;paration',
'mes_diggs_prop'=>'Mes Digg\'s en propos&eacute;s',
'modifier'=>'Modifier',
// P
'popose'=>'propos&eacute;',
'publie'=>'publi&eacute;',
// R
'refuse'=>'refus&eacute;',
// S
'stats_des_diggs'=> 'Statistiques des Digg\'s',
'supprimer_digg'=>'Supprimer un digg',
// T
'titre_digg'=>'Titre du Digg :',
// U
'url_digg'=>'Url du Digg :'
);
?>
