<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'icone'=>'Projets Didapages',
'configurer'=>'Configurer Didaspip',
'versionpsd' => "PSD 1.1",
'titrepsd' => "Petit Serveur pour Didapages",
'urlaccueil' => "http://www.fruitsdusavoir.org/psd/",
'dialoguehtaccess' => "Acc&egrave;s prot&eacute;g&eacute; ! Veuillez saisir vos codes d'acc&egrave;s.",
'defcodeadmin' => "D&eacute;finissez les codes d'acc&egrave;s &agrave; la partie administration.",
'utilisateur' => "Nom d'utilisateur",
'motdepasse' => "Mot de passe",
'enregistrer' => "Enregistrer",
'aideaccescours' => "Acc&egrave;s au cours",
'aidepasaccescours' => "Acc&egrave;s interdit, correction en cours",
'aidevoirlog' => "Voir/Cacher le d&eacute;tail des &eacute;v&egrave;nements",
'aidechangercateg' => "Changer la cat&eacute;gorie",
'aidedebloquertravail' => "Cliquez pour autoriser l'acc&egrave;s au livre",
'aidebloquertravail' => "Cliquez pour interdire l'acc&egrave;s au livre",
'aidedeconnecterapp' => "Cliquez pour deconnecter et annuler le travail en cours",
'aidesuivitravail' => "Suivi du travail de l'apprenant",
'aidesupprimertravail' => "Supprimer cette inscription",
'aidechangermessageapp' => "Changer le message d'accueil de cet apprenant",
'aidemodifierdonnees' => "Modifier le nom, prenom ou mot de passe",
'aidevoirinscriptions' => "Voir les inscriptions de cet apprenant",
'aidedebloquercourslibre' => "Cliquez pour que le cours soit en acc&egrave;s libre",
'aidebloquercourslibre' => "Cliquez pour que le cours ne soit pas en acc&egrave;s libre",
'aidemodifiercours' => "Modifier les donn&eacute;es de ce cours",
'aidesupprimercours' => "Supprimer ce cours",
'aidevoircours' => "Voir le contenu du cours",
'aidesupprimerprof' => "Supprimer ce compte",
'gestdidaspip' => "Gestion des projets Didapages",
'explication' => "* Champ obligatoire : Le nom doit &ecirc;tre uniquement compos&eacute; de caract&egrave;res alphanum&eacute;riques (pas d'espace, ni de tirets, ni underscores)",
'recommandation' => "<b>Rappel:</b> Pour un fonctionnement en ligne, les noms des fichiers m&eacute;dias du projet ne doivent en aucun cas comporter d'accent ou caract&egrave;res sp&eacute;ciaux !",
'msgaccueil' => "Bienvenue !",
'connexion' => "Connexion",
'coursacceslibre' => "Ressources en acc&egrave;s libre",
'utilisateurinconnu' => "Nom d'utilisateur inconnu !",
'motdepasseincorrect' => "Mot de passe incorrect !",
'accueil' => "Accueil",
'travail' => "Inscriptions et suivi du travail",
'apprenants' => "Gestion des apprenants",
'cours' => "Gestion des cours",
'configuration' => "Configuration",
'deconnexion' => "D&eacute;connexion",
'erreursaisie1' => "Le nom d'utilisateur doit avoir de 4 &agrave; 12 caract&egrave;res.",
'erreursaisie2' => "Le mot de passe doit avoir de 4 &agrave; 12 caract&egrave;res.",
'erreursaisie3' => "Le nom doit avoir de 1 &agrave; 20 caract&egrave;res.",
'erreursaisie4' => "Le pr&eacute;nom doit avoir de 1 &agrave; 20 caract&egrave;res.",
'erreursaisie5' => "Le nom de groupe doit avoir de 1 &agrave; 12 caract&egrave;res.",
'erreursaisie6' => "Seuls les caract&egrave;res a-z, A-Z, 0-9 et - sont autoris&eacute;s pour les noms d'utilisateur, de groupe, de cours et les mots de passe !",
'erreursaisie7' => "Ce nom d'utilisateur existe d&eacute;j&agrave;. Veuillez en choisir un autre.",
'erreurimport1' => "Vous ne pouvez pas importer un fichier d'une taille sup&eacute;rieure &agrave; ".ini_get("upload_max_filesize")." !",
'erreurimport2' => "Le fichier n'a &eacute;t&eacute; que partiellement re&ccedil;u, l'import a &eacute;chou&eacute; !",
'erreurimport3' => "Aucun fichier n'a &eacute;t&eacute; re&ccedil;u !",
'erreurimport4' => "Erreur cot&eacute; serveur : pas de dossier temporaire.",
'erreurimport5' => "Vous ne pouvez importer que les fichiers Zip correspondant aux cours cr&eacute;&eacute;s par Didapages (export MSP).",
'erreurimport6' => "Le nom du cours doit avoir de 1 &agrave; 12 caract&egrave;res (sans espace).",
'erreurimport7' => "Le titre du cours doit avoir de 1 &agrave; 100 caract&egrave;res.",
'erreurimport8' => "Ce nom de cours existe d&eacute;j&agrave;.",
'erreurimport9' => "Un probl&egrave;me est survenu lors de la d&eacute;compression du fichier Zip.",
'erreurimport10' => "Le cours a bien &eacute;t&eacute; import&eacute;, mais il contenait des fichiers, inhabituels pour un cours Didapages, qui ont &eacute;t&eacute; supprim&eacute;s par s&eacute;curit&eacute; : ",
'erreurtravail1' => "Erreur inconnue li&eacute;e au groupe ou &agrave; l'apprenant s&eacute;lectionn&eacute; !",
'erreurtravail2' => "Erreur inconnue li&eacute;e au cours s&eacute;lectionn&eacute; !",
'erreurtravail3' => "Cet apprenant est d&eacute;j&agrave; inscrit &agrave; ce cours !",
'erreurtravail4' => "Un ou plusieurs apprenants de ce groupe &eacute;taient d&eacute;j&agrave; inscrits &agrave; ce cours !",
'apptitrepage' => "GESTION DES APPRENANTS",
'apptitreinscript' => "Inscrire un nouvel apprenant",
'apptitreinscriptliste' => "Inscrire une liste d'apprenants",
'apptitreliste' => "Liste des apprenants",
'apptitremodifier' => "Modifier les donn&eacute;es de",
'appsupselection' => "Supprimer la s&eacute;lection",
'nom' => "Nom",
'prenom' => "Pr&eacute;nom",
'motdepasse' => "Mot de passe",
'modifier' => "Modifier",
'annuler' => "Annuler",
'groupe' => "Groupe",
'inscrire' => "Inscrire",
'tous' => "Tous",
'travail' => "Travail",
'aucunapprenant' => "Aucun apprenant n'est inscrit !",
'accueilapp' => "Message",
'messagedaccueil' => " Message d'accueil",
'fichiercsv' => "Fichier CSV &agrave; importer : ",
'consignecsv' => " Format du fichier CSV : 
<br/>\"nomUtilisateur1\",\"motDePasse1\",\"nom1\",\"prenom1\"
<br/>\"nomUtilisateur2\",\"motDePasse2\",\"nom2\",\"prenom2\"<br />",
'erreurimportcsv' => "La liste doit �tre un fichier CSV !",
'erreurlisteloginexiste' => "Ce nom d'utilisateur existe d&eacute;j&agrave; !",
'erreurlisteligne' => "Erreur ligne",
'erreurlisteformat' => "le format est incorrect !",
'courstitrepage' => "GESTION DES PROJETS",
'courstitreimport' => "Importer un nouveau projet",
'courstitremodif' => "Modifier un cours existant",
'courstitreliste' => "Liste des projets en ligne",
'taille' => "Taille",
'titre' => "Titre",
'code' => "Code &agrave; ins&eacute;rer dans l'article",
'acces' => "Acc&egrave;s",
'voir' => "Voir",
'suppr' => "Suppr.",
'importer' => "Importer",
'categorie' => "Cat&eacute;gorie",
'consigneimport' => "Projet Didapages &agrave; importer",
'ko' => "Ko",
'aucuncours' => "Aucun cours n'est install&eacute; !",
'attentionmodifcours' => '<b>--- Attention ! ---</b><br />Le travail de vos apprenants, ce sont les modifications qu\'ils apportent &agrave; un livre par rapport &agrave; son &eacute;tat initial. Le remplacement du cours actuel par un autre trop diff&eacute;rent peut donc provoquer de graves probl&egrave;mes lors de l\'acc&egrave;s &agrave; ce cours.
Si les changements apport&eacute;s sont mineurs (correction de fautes d\'orthographe, position d\'&eacute;lements...), il ne devrait pas y avoir de probl&egrave;me. Mais en cas de modifications importantes (insertion de page, de nouveaux &eacute;l&eacute;ments...), vous devez imp&eacute;rativement supprimer les inscriptions sur ce cours.<br />',
'travailtitreinscrire' => "Inscrire un apprenant ou un groupe &agrave; un cours",
'travailtitremodif' => "Modifier la cat&eacute;gorie d'une inscription &agrave; un cours",
'aucours' => "Au cours",
'voirinscriptions' => "Voir les inscriptions de",
'pour' => "pour",
'touslesapprenants' => "Tous les apprenants",
'touslescours' => "Tous les cours",
'aucuneinscription' => "Aucune inscription n'a &eacute;t&eacute; trouv&eacute;e !",
'log' => "Log",
"nomducours" => "Nom du cours",
"tempstravail" => "Temps de travail",
"score" => "Score",
"minutes" => "min",
"travpbconnect1" => " est connect&eacute; depuis ",
"travpbconnect2" => ". Vous devez le d&eacute;connecter pour acc&eacute;der au cours !",
"deconnexionde" => "D&eacute;connexion de ",
"par" => " par ",
"cat" => "Cat.",
'log1' => "Inscription au cours par",
'log2' => "Suivi du travail par",
'log3' => "Session de",
'log4' => "score de",
'logpbdeconnect' => "Fin d'une session non termin&eacute;e, d&eacute;marr&eacute;e le",
'logpbenregitrement' => "Echec d'une tentative d'enregistrement (apprenant d&eacute;connect&eacute;)",
'msgaccueilconf' => " Messages d'accueils ( texte simple ou HTML)",
'msgaccueilapp' => " Message par d&eacute;faut pour les apprenants",
'msgaccueilinvite' => " Message de la page d'identification",
'gereraccesprof'=> " Cr&eacute;er/supprimer des acc&egrave;s enseignant/formateur",
'aucunaccesprof'=> " Aucun enseignant/formateur inscrit",
'changerdonneesperso'=> " Changer vos donn&eacute;es personnelles",
'boitecateg'=>"Bo�te &agrave; cat&eacute;gories",
'ajouter'=>"Ajouter",
'supprimer' => "Supprimer",
'exemplecateg' =>"Exemples : Cours/Maths/G&eacute;om&eacute;trie ; A faire avant vendredi ; Astronomie/Syst&egrave;me solaire",
'voscours' => "Vos cours",
'correctionencours' => "Correction en cours..."
);
?>