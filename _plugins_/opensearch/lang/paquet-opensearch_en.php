<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-opensearch?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'opensearch_description' => 'Plugin which enables to offer the users to add a custom search engine related to your website on their browser.',
	'opensearch_slogan' => 'Votre site comme moteur de recherche' # NEW
);

?>
