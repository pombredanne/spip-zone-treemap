<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/opensearch?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_boite_opensearch' => 'پيكربندي پلاگين اوپن سرچ (OpenSearch)',
	'cfg_descr_opensearch' => 'اوپن سرچ: موتور جستجوگر سفارشي براي سايت شما.', # MODIF
	'cfg_inf_id_rubrique' => 'بخش مورد نظر براي جستچو را برگزينيد',
	'cfg_inf_page_recherche' => 'اسكلت براي حستجو در سايت را مشخص كنيد (مثلن: جستجو روي اسلكت recherche.html )',
	'cfg_lbl_id_rubrique' => 'بخش هدف',
	'cfg_lbl_page_recherche' => 'اسكلت جستجو',
	'cfg_option_tout' => 'تمام سايت',
	'cfg_titre_opensearch' => 'اوپن سرچ'
);

?>
