<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/opensearch/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'opensearch_description' => 'Plugin qui permet de proposer aux visiteurs de votre site d\'ajouter un moteur de recherche personnalisée à leur navigateur.',
	'opensearch_slogan' => 'Votre site comme moteur de recherche'
);

?>
