<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'inscription_mailing_list' => 'S\'inscrire &agrave; la newsletter',
	'mail_inscription_mailinglist' => 'Adresse email d\'inscription &agrave; la liste',
	'sujet_inscription_mailinglist' => 'Sujet du mail pour l\'inscription &agrave; la liste',
	'sujet_inscription_mailinglist_explication' => 'sujet de l\'email envoy&eacute; &agrave; la liste et correspondant &agrave; la requ&ecirc;te de la demande d\'inscription.',
	'mail_inscription_mailinglist_explication' => 'adresse email &agrave; laquelle la demande d\'inscription &agrave; la liste doit &ecirc;tre envoy&eacute;e',
);

?>
