<?php


	/**
	 * SPIP-Météo
	 *
	 * Copyright (c) 2006-2009
	 * Agence Artégo http://www.artego.fr
	 *  
	 * Ce programme est un logiciel libre distribue sous licence GNU/GPLv3.
	 * Pour plus de details voir http://www.gnu.org/licenses/gpl-3.0.html
	 *  
	 **/


	$GLOBALS[$GLOBALS['idx_lang']] = array(

		# partie publique
		'meteo' => "Météo",
		'meteo_pluie' => "Pluie",
		'meteo_orage' => "Orage",
		'meteo_neige' => "Neige",
		'meteo_verglas' => "Verglas",
		'meteo_brouillard' => "Brouillard",
		'meteo_vent' => "Vent",
		'meteo_nuages' => "Nuages",
		'meteo_lune' => "Pleine-lune",
		'meteo_lune-nuage' => "Pleine-lune et nuages éparses",
		'meteo_lune-nuages' => "Pleine-lune et nuages",
		'meteo_soleil' => "Soleil",
		'meteo_soleil-nuage' => "Soleil et nuages éparses",
		'meteo_soleil-nuages' => "Soleil et nuages",
		'meteo_inconnu' => "Inconnu",
		'meteo_NA' => "Inconnu",
		'temperature_inconnue' => "Température inconnue",

		'Z' => 'ZZzZZzzz'

	);

?>
