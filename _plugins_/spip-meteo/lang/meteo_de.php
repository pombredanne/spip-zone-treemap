<?php


	/**
	 * SPIP-Météo
	 *
	 * Copyright (c) 2006-2009
	 * Agence Artégo http://www.artego.fr
	 *  
	 * Ce programme est un logiciel libre distribue sous licence GNU/GPLv3.
	 * Pour plus de details voir http://www.gnu.org/licenses/gpl-3.0.html
	 *  
	 **/


	$GLOBALS[$GLOBALS['idx_lang']] = array(

		'meteo' => "Wetter",
		'meteo_pluie' => "Regen",
		'meteo_orage' => "Gewitter",
		'meteo_neige' => "Schnee",
		'meteo_verglas' => "Glatteis",
		'meteo_brouillard' => "Nebel",
		'meteo_vent' => "Wind",
		'meteo_nuages' => "Wolken",
		'meteo_lune' => "Vollmond",
		'meteo_lune-nuage' => "Vollmond und leicht bewölkt",
		'meteo_lune-nuages' => "Vollmond und bewölkt",
		'meteo_soleil' => "Sonne",
		'meteo_soleil-nuage' => "Sonne und leicht bewölkt",
		'meteo_soleil-nuages' => "Sonne und	bewölkt",
		'meteo_inconnu' => "Umbekannt",
		'meteo_NA' => "Umbekannt",
		'temperature_inconnue' => "Temperatur unbekannt",

		'Z' => 'ZZzZZzzz'
	);
		

?>