<?php


	/**
	 * SPIP-Météo
	 *
	 * Copyright (c) 2006-2009
	 * Agence Artégo http://www.artego.fr
	 *  
	 * Ce programme est un logiciel libre distribue sous licence GNU/GPLv3.
	 * Pour plus de details voir http://www.gnu.org/licenses/gpl-3.0.html
	 *  
	 **/


	$GLOBALS[$GLOBALS['idx_lang']] = array(

		'meteo' => "Weather",
		'meteo_pluie' => "Rainy",
		'meteo_orage' => "Stormy",
		'meteo_neige' => "Snowy",
		'meteo_verglas' => "Icy",
		'meteo_brouillard' => "Foggy",
		'meteo_vent' => "Windy",
		'meteo_nuages' => "Cloudy",
		'meteo_lune' => "Full moon",
		'meteo_lune-nuage' => "Full moon and sparsely cloudy",
		'meteo_lune-nuages' => "Full moon and cloudy",
		'meteo_soleil' => "Sunny",
		'meteo_soleil-nuage' => "Sunny and sparsely cloudy",
		'meteo_soleil-nuages' => "Sunny and cloudy",
		'meteo_inconnu' => "Unknown",
		'meteo_NA' => "Unknown",
		'temperature_inconnue' => "Unknown temperature",

		'Z' => 'ZZzZZzzz'

	);

?>
