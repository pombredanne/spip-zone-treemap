<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_envoyer' => 'Envoyer !',
	'bouton_tester_envoi' => 'Tester !',

	// I
	'info_tester_newsletter' => 'Tester avec un envoi par courriel',
	'info_envoyer_newsletter' => 'Envoyer l\'infolettre',

	// L
	'label_email_test' => 'à l\'adresse email',
	'label_liste' => 'aux abonnés de ',

);

?>