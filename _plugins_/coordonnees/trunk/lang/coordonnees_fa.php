<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/coordonnees?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'adresse_perso' => 'خانه',
	'adresse_pro' => 'پروفشنال (حرفه‌اي)',
	'adresses' => 'نشاني‌ها',
	'ajouter_adresse' => 'افزودن نشاني',
	'ajouter_email' => 'افزودن ايميل',
	'ajouter_telephone' => 'افزودن شماره‌ي تلفن',

	// C
	'configuration_coordonnees' => 'پيكربندي coordonnées)',
	'confirmer_suppression_adresse' => 'مي‌خواهيد اين نشاني را حذف كنيد؟',
	'confirmer_suppression_email' => 'آيا مي‌خواهيد اين ايميل را حذف كنيد؟',
	'confirmer_suppression_numero' => 'آيا مي‌خواهيد اين شماره تلفن را حذف كنيد؟',

	// E
	'editer_adresse' => 'ويرايش نشاني',
	'editer_email' => 'ويرايش يك ايميل',
	'editer_numero' => 'ويرايش شماره تلفن',
	'emails' => 'ايميل‌ها',
	'explication_objets_actifs' => 'كوتوردينه روي كدام موضوع‌هاي ويرايشي پيشنهاد مي‌شود؟ (؟)',
	'explication_type_email' => 'نوع مي‌تواند «شخصي» يا «حرفه‌اي» باشد.',

	// I
	'info_adresse_utilisee_par' => 'نشاني مورد استفاده‌ي: ',
	'info_email_utilise_par' => 'ايميل مورد استفاد‌ه‌ي :',
	'info_gauche_numero_adresse' => 'نشاني شماره‌ي ',
	'info_gauche_numero_email' => 'ايميل شماره‌ي',
	'info_gauche_numero_numero' => 'شماره تلفن شماره‌ي',
	'info_numero_utilise_par' => 'شماره تلفن مورد استفاده‌ي: ',
	'item_nouveau_numero' => 'شماره تلفن جديد',
	'item_nouvel_email' => 'ايميل جديد',
	'item_nouvelle_adresse' => 'نشاني جديد',

	// L
	'label_boite_postale' => 'صندوق پستي',
	'label_code_postal' => 'كد پستي',
	'label_complement' => 'اطلاعات تكميلي', # MODIF
	'label_email' => 'ايميل ',
	'label_numero' => 'شماره تلفن',
	'label_objets_actifs' => 'موضوع‌ها',
	'label_pays' => 'كشورها',
	'label_region' => 'Région', # NEW
	'label_telephone' => 'Téléphone', # NEW
	'label_titre' => 'تيتر',
	'label_type_adresse' => 'نوع نشاني',
	'label_type_email' => 'نوع ايميل ',
	'label_type_numero' => 'نوع شماره تلفن',
	'label_ville' => 'شهر',
	'label_voie' => 'نشاني', # MODIF

	// M
	'modifier_adresse' => 'ويرايش اين نشاني',
	'modifier_email' => 'ويرايش اين ايميل',
	'modifier_numero' => 'ويرايش اين شماره‌ي تلفن',

	// N
	'nouveau_numero' => 'شماره تلفن جديد',
	'nouvel_email' => 'ايميل جديد',
	'nouvelle_adresse' => 'نشاني جديد',
	'numeros' => 'شماره تلفن‌ها',

	// S
	'supprimer_adresse' => 'حذف اين نشاني',
	'supprimer_email' => 'حذف اين ايميل',
	'supprimer_numero' => 'حذف اين شماره تلفن',

	// T
	'titre_coordonnees' => 'دفتر تلفن',
	'type_adr_dom' => 'Résidentielle', # NEW
	'type_adr_home' => 'Personnelle', # NEW
	'type_adr_intl' => 'Étrangère', # NEW
	'type_adr_parcel' => 'Parcelle', # NEW
	'type_adr_postal' => 'Postale (en poste restante)', # NEW
	'type_adr_pref' => 'Principale', # NEW
	'type_adr_work' => 'Professionnelle', # NEW
	'type_email_internet' => 'Internet', # NEW
	'type_email_pref' => 'Préféré', # NEW
	'type_email_x400' => 'X.400', # NEW
	'type_mel_home' => 'Personnel', # NEW
	'type_mel_work' => 'Professionnel', # NEW
	'type_tel_bbs' => 'Service de messagerie', # NEW
	'type_tel_car' => 'Voiture', # NEW
	'type_tel_cell' => 'Portable', # NEW
	'type_tel_dsl' => 'box DSL', # NEW
	'type_tel_fax' => 'Télécopie', # NEW
	'type_tel_home' => 'Résidence', # NEW
	'type_tel_isdn' => 'RNIS ...ou DSL', # NEW
	'type_tel_modem' => 'MoDem informatique', # NEW
	'type_tel_msg' => 'à messagerie', # NEW
	'type_tel_pager' => 'Bipeur', # NEW
	'type_tel_pcs' => 'Service de communication personnel', # NEW
	'type_tel_pref' => 'Favori', # NEW
	'type_tel_text' => 'Texto', # NEW
	'type_tel_textphone' => 'Retranscripteur texte', # NEW
	'type_tel_video' => 'Visioconférence', # NEW
	'type_tel_voice' => 'Vocal', # NEW
	'type_tel_work' => 'Professionnel' # NEW
);

?>
