<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/coordonnees?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'adresse_perso' => 'Home',
	'adresse_pro' => 'Professional',
	'adresses' => 'Addresses',
	'ajouter_adresse' => 'Add an address',
	'ajouter_email' => 'Add an email',
	'ajouter_telephone' => 'Add a phone number', # MODIF

	// C
	'configuration_coordonnees' => 'Configuration of Address book',
	'confirmer_suppression_adresse' => 'Are you sure you want to detele this address ?',
	'confirmer_suppression_email' => 'Are you sure you want to detele this email ?',
	'confirmer_suppression_numero' => 'Are you sure you want to detele this phone number ?', # MODIF

	// E
	'editer_adresse' => 'Edit an address', # MODIF
	'editer_email' => 'Edit an email', # MODIF
	'editer_numero' => 'Edit a phone number', # MODIF
	'emails' => 'Emails',
	'explication_objets_actifs' => 'On which editorial objects should be provided contact information?',
	'explication_type_email' => 'Le type peut être \'perso\' ou \'pro\'.', # NEW

	// I
	'info_adresse_utilisee_par' => 'This address used by :', # MODIF
	'info_email_utilise_par' => 'Email used by :',
	'info_gauche_numero_adresse' => 'Address id',
	'info_gauche_numero_email' => 'Email id',
	'info_gauche_numero_numero' => 'Number id', # MODIF
	'info_numero_utilise_par' => 'This phone number is used by :', # MODIF
	'item_nouveau_numero' => 'New phone number',
	'item_nouvel_email' => 'New email',
	'item_nouvelle_adresse' => 'New address',

	// L
	'label_boite_postale' => 'Postal box',
	'label_code_postal' => 'Postal code',
	'label_complement' => 'Additional information', # MODIF
	'label_email' => 'Email',
	'label_numero' => 'Phone number', # MODIF
	'label_objets_actifs' => 'Objects',
	'label_pays' => 'Country',
	'label_region' => 'Région', # NEW
	'label_telephone' => 'Phone',
	'label_titre' => 'Title',
	'label_type_adresse' => 'Address type',
	'label_type_email' => 'Type of email',
	'label_type_numero' => 'Type of phone number', # MODIF
	'label_ville' => 'City',
	'label_voie' => 'Address', # MODIF

	// M
	'modifier_adresse' => 'Edit this address',
	'modifier_email' => 'Edit this email',
	'modifier_numero' => 'Edit this phone number', # MODIF

	// N
	'nouveau_numero' => 'New phone number', # MODIF
	'nouvel_email' => 'New email',
	'nouvelle_adresse' => 'New address',
	'numeros' => 'Phone numbers', # MODIF

	// S
	'supprimer_adresse' => 'Delette this address',
	'supprimer_email' => 'Delete this email',
	'supprimer_numero' => 'Delete this phone number', # MODIF

	// T
	'titre_coordonnees' => 'Address book', # MODIF
	'type_adr_dom' => 'Résidentielle', # NEW
	'type_adr_home' => 'Personnelle', # NEW
	'type_adr_intl' => 'Étrangère', # NEW
	'type_adr_parcel' => 'Parcelle', # NEW
	'type_adr_postal' => 'Postale (en poste restante)', # NEW
	'type_adr_pref' => 'Principale', # NEW
	'type_adr_work' => 'Professionnelle', # NEW
	'type_email_internet' => 'Internet', # NEW
	'type_email_pref' => 'Préféré', # NEW
	'type_email_x400' => 'X.400', # NEW
	'type_mel_home' => 'Personnel', # NEW
	'type_mel_work' => 'Professionnel', # NEW
	'type_tel_bbs' => 'Service de messagerie', # NEW
	'type_tel_car' => 'Voiture', # NEW
	'type_tel_cell' => 'Portable', # NEW
	'type_tel_dsl' => 'box DSL', # NEW
	'type_tel_fax' => 'Télécopie', # NEW
	'type_tel_home' => 'Résidence', # NEW
	'type_tel_isdn' => 'RNIS ...ou DSL', # NEW
	'type_tel_modem' => 'MoDem informatique', # NEW
	'type_tel_msg' => 'à messagerie', # NEW
	'type_tel_pager' => 'Bipeur', # NEW
	'type_tel_pcs' => 'Service de communication personnel', # NEW
	'type_tel_pref' => 'Favori', # NEW
	'type_tel_text' => 'Texto', # NEW
	'type_tel_textphone' => 'Retranscripteur texte', # NEW
	'type_tel_video' => 'Visioconférence', # NEW
	'type_tel_voice' => 'Vocal', # NEW
	'type_tel_work' => 'Professionnel' # NEW
);

?>
