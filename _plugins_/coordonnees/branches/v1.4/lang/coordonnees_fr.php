<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/coordonnees/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'adresses' => 'Adresses',
	'ajouter_adresse' => 'Ajouter une adresse',
	'ajouter_email' => 'Ajouter un email',
	'ajouter_telephone' => 'Ajouter un numéro',

	// C
	'configuration_coordonnees' => 'Configuration des coordonnées',
	'confirmer_suppression_adresse' => 'Voulez-vous vraiment supprimer cette adresse ?',
	'confirmer_suppression_email' => 'Voulez-vous vraiment supprimer ce courriel ?',
	'confirmer_suppression_numero' => 'Voulez-vous vraiment supprimer ce numéro ?',

	// E
	'editer_adresse' => 'Éditer une adresse',
	'editer_email' => 'Éditer un courriel',
	'editer_numero' => 'Éditer un numéro',
	'emails' => 'Courriels',
	'explication_objets_actifs' => 'Sur quels objets éditoriaux proposer les coordonnées ?',

	// I
	'info_adresse_utilisee_par' => 'Adresse utilisée par :',
	'info_email_utilise_par' => 'Courriel utilisé par :',
	'info_gauche_numero_adresse' => 'N° Adresse',
	'info_gauche_numero_email' => 'N° Email',
	'info_gauche_numero_numero' => 'N° Numéro',
	'info_numero_utilise_par' => 'Numéro utilisé par :',
	'item_nouveau_numero' => 'Nouveau numéro',
	'item_nouvel_email' => 'Nouveau courriel',
	'item_nouvelle_adresse' => 'Nouvelle adresse',

	// L
	'label_boite_postale' => 'Boîte Postale',
	'label_code_postal' => 'Code Postal',
	'label_complement' => 'Complément d\'adresse',
	'label_email' => 'Courriel',
	'label_numero' => 'Numéro',
	'label_objets_actifs' => 'Objets',
	'label_pays' => 'Pays',
	'label_region' => 'Région',
	'label_telephone' => 'Téléphone',
	'label_titre' => 'Titre',
	'label_type_adresse' => 'Type d\'adresse',
	'label_type_email' => 'Type de courriel',
	'label_type_numero' => 'Type de numéro',
	'label_ville' => 'Commune',
	'label_voie' => 'Adresse',

	// M
	'modifier_adresse' => 'Modifier cette adresse',
	'modifier_email' => 'Modifier ce courriel',
	'modifier_numero' => 'Modifier ce numéro',

	// N
	'nouveau_numero' => 'Nouveau numéro',
	'nouvel_email' => 'Nouveau courriel',
	'nouvelle_adresse' => 'Nouvelle adresse',
	'numeros' => 'Numéros',

	// S
	'supprimer_adresse' => 'Supprimer cette adresse',
	'supprimer_email' => 'Supprimer ce courriel',
	'supprimer_numero' => 'Supprimer ce numéro',

	// T
	// types (RFC2426) http://microformats.org/wiki/hcard-fr#adr_tel_email_types
	// traductions  http://www.alsacreations.com/tuto/lire/1223-microformats-composes.html#hcard
	'titre_coordonnees' => 'Coordonnées',
	'type_adr_dom' => 'R&eacute;sidentielle',
	'type_adr_home' => 'Personnelle',
	'type_adr_intl' => '&Eacute;trang&egrave;re',
	'type_adr_parcel' => 'Parcelle',
	'type_adr_postal' => 'Postale (en poste restante)',
	'type_adr_pref' => 'Principale',
	'type_adr_work' =>  'Professionnelle',
	'type_email_internet' => 'Internet', // user@domain
	'type_email_pref' => 'Préféré',
	'type_email_x400' => 'X.400',
	'type_mel_home' => 'Personnel',
	'type_mel_work' => 'Professionnel',
	'type_tel_bbs' => 'Service de messagerie',
	'type_tel_car' => 'Voiture',
	'type_tel_cell' => 'Portable', // a cote de "cell" on trouve "mobile" qui est une terminologie tres courante en Amerique du nord
	'type_tel_dsl' => 'box DSL', // bien que pas dans la norme, c'est de plus en plus en courant et j'arrive jamais a decider s'il faut utiliser "isdn" ou "modem" a la place
	'type_tel_fax' => 'Télécopie',
	'type_tel_home' => 'Résidence',
	'type_tel_isdn' => 'RNIS', // Réseau numérique à intégration de services
	'type_tel_modem' => 'MoDem informatique',
	'type_tel_msg' => 'Boîte vocale (répondeur)', // http://fr.wikipedia.org/wiki/R%C3%A9pondeur http://fr.wikipedia.org/wiki/Syst%C3%A8me_de_messagerie_vocale
	'type_tel_pager' => 'Téléavertisseur (bipeur)',
	'type_tel_pcs' => 'Service de communication personnel', // http://en.wikipedia.org/wiki/Personal_Communications_Service
	'type_tel_pref' => 'Favori',
	'type_tel_text' => 'Texto', // SMS sur cellulaires et messages sur bipeurs modernes
	'type_tel_textphone' => 'Retranscripteur texte', // pour non-entendants surtout : http://www.ricability.org.uk/consumer_reports/at_home/buying_a_textphone/what_are_textphones/ http://en.wikipedia.org/wiki/Telecommunications_device_for_the_deaf http://www.nokiaaccessibility.com/textphones.html
	'type_tel_video' => 'Visiophone (visioconférence)', // http://en.wikipedia.org/wiki/Videotelephony http://fr.wikipedia.org/wiki/Visiophonie http://fr.wikipedia.org/wiki/Visioconf%C3%A9rence
	'type_tel_voice' => 'Vocal',
	'type_tel_work' => 'Professionnel',

);

?>
