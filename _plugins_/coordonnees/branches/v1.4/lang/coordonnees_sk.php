<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'adresses' => 'Adresy',
	'ajouter_adresse' => 'Pridať adresu',
	'ajouter_email' => 'Pridať e-mail',
	'ajouter_telephone' => 'Pridať telefónne číslo',
	'articles' => 'Articles', # NEW
	'auteurs' => 'Auteurs', # NEW

	// C
	'configuration_coordonnees' => 'Configuration des coordonnées', # NEW
	'confirmer_suppression_adresse' => 'Voulez-vous vraiment supprimer cette adresse ?', # NEW
	'confirmer_suppression_email' => 'Voulez-vous vraiment supprimer ce courriel ?', # NEW
	'confirmer_suppression_numero' => 'Voulez-vous vraiment supprimer ce num&eacute;ro ?', # NEW

	// E
	'editer_adresse' => '&Eacute;diter une adresse', # NEW
	'editer_email' => '&Eacute;diter un courriel', # NEW
	'editer_numero' => '&Eacute;diter un num&eacute;ro', # NEW
	'emails' => 'Courriels', # NEW
	'explication_objets_actifs' => 'Sur quels objets éditoriaux proposer les coordonnées ?', # NEW
	'explication_type_email' => 'Le type peut être \'perso\' ou \'pro\'.', # NEW

	// I
	'info_adresse_utilisee_par' => 'Adresse utilis&eacute;e par :', # NEW
	'info_email_utilise_par' => 'Courriel utilis&eacute; par :', # NEW
	'info_gauche_numero_adresse' => 'N° Adresse', # NEW
	'info_gauche_numero_email' => 'N° Email', # NEW
	'info_gauche_numero_numero' => 'N° Num&eacute;ro', # NEW
	'info_numero_utilise_par' => 'Num&eacute;ro utilis&eacute; par :', # NEW
	'item_nouveau_numero' => 'Nouveau num&eacute;ro', # NEW
	'item_nouvel_email' => 'Nouveau courriel', # NEW
	'item_nouvelle_adresse' => 'Nouvelle adresse', # NEW

	// L
	'label_boite_postale' => 'Boîte Postale', # NEW
	'label_code_postal' => 'Code Postal', # NEW
	'label_complement' => 'Compl&eacute;ment', # NEW
	'label_email' => 'Courriel', # NEW
	'label_numero' => 'Num&eacute;ro', # NEW
	'label_objets_actifs' => 'Objets', # NEW
	'label_pays' => 'Pays', # NEW
	'label_titre' => 'Titre', # NEW
	'label_type_adresse' => 'Type d\'adresse', # NEW
	'label_type_email' => 'Type de courriel', # NEW
	'label_type_numero' => 'Type de num&eacute;ro', # NEW
	'label_ville' => 'Ville', # NEW
	'label_voie' => 'Adresse', # NEW

	// M
	'modifier_adresse' => 'Modifier cette adresse', # NEW
	'modifier_email' => 'Modifier ce courriel', # NEW
	'modifier_numero' => 'Modifier ce num&eacute;ro', # NEW

	// N
	'nouveau_numero' => 'Nouveau num&eacute;ro', # NEW
	'nouvel_email' => 'Nouveau courriel', # NEW
	'nouvelle_adresse' => 'Nouvelle adresse', # NEW
	'numeros' => 'Num&eacute;ros', # NEW

	// R
	'rubriques' => 'Rubriques', # NEW

	// S
	'supprimer_adresse' => 'Supprimer cette adresse', # NEW
	'supprimer_email' => 'Supprimer ce courriel', # NEW
	'supprimer_numero' => 'Supprimer ce num&eacute;ro', # NEW

	// T
	'titre_coordonnees' => 'Coordonn&eacute;es' # NEW
);

?>
