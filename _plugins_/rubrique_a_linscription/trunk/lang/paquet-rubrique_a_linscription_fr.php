<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-rubrique_a_linscription
// Langue: fr
// Date: 14-04-2012 20:40:15
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// R
	'rubrique_a_linscription_description' => 'Si le formulaire d\'inscription est passé avec l\'argument 0minirezo (<code>#FORMULAIRE_INSCRIPTION{0minirezo}</code>), une rubrique est automatiquement créée, où l\'auteur est admin restreint.',
	'rubrique_a_linscription_slogan' => 'Créer une rubrique à l\'inscription d\'un admin',
);
?>