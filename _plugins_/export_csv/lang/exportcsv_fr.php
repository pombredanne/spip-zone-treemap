<?php
/*##############################################################
 * ExportCSV
 * Export des articles / rubriques SPIP en fichiers CSV.
 *
 * Auteur :
 * St�phanie De Nada�
 * webdesigneuse.net
 * � 2008 - Distribu� sous licence GNU/GPL
 *
##############################################################*/


$GLOBALS[$GLOBALS['idx_lang']] = array(
// A
'apercu_data' => 'Aper&ccedil;u des articles extraits',
'aux_rub' => 'aux rubriques',
'aux_art' => 'aux articles',
'aide' => 'Aide en ligne',
'auteurs' => 'Auteurs',

// B

// C
'config' => 'Configuration',

// D
'description_cfg' => 'Ici vous pouvez configurer le plugin Export C.S.V.',
'del_cfg' => 'Tout effacer',
'descriptif' => 'Descriptif',
'date' => 'Date',

// E
'extract_data' => 'Extraire les donn&eacute;es',
'extraction_data' => 'Extraction des donn&eacute;es',
'extraction_data_back' => 'Aller &agrave; l\'extraction des donn&eacute;es',
'explications' => '<p><strong>Seuls les articles publi&eacute;s ou les signatures valid&eacute;es sont export&eacute;s</strong></p>
<ol class="exportcsv_ol"><li>Pour t&eacute;l&eacute;charger le fichier C.S.V :
		<ul><li>Pour les articles : cliquer sur &laquo;&nbsp;T&eacute;l&eacute;charger les articles&nbsp;&raquo; ci-dessus</li>
			<li>Pour les p&eacute;titions : cliquer sur le titre de l\'article concern&eacute;</li>
		</ul>
		</li>
		<li>Enregistrer le fichier sur votre ordinateur</li>
		<li>Lancer le tableur (OpenOffice ou Excel<sup>&reg;</sup>)</li>
		<li>Ouvrir le fichier t&eacute;l&eacute;charg&eacute; 
		<ul>
			<li>Avec OpenOffice, choisir comme s&eacute;paration le <strong>point-virgule</strong> et le <strong>texte par des guillemets</strong></li>
			<li>Avec Excel, choisir &laquo;&nbsp;Tous les fichiers&nbsp;&raquo; dans <em>Type de fichiers</em></li>
		</ul></li>
		</ol>',
'elements_a_extract' => '&Eacute;l&eacute;ments &agrave; extraire',
'erreur_lien_config' => '<p><a href="?exec=cfg&amp;cfg=exportcsv">Aller &agrave; la configuration</a></p>',
'erreur_admin_config' => '<p>Contactez un administrateur pour la configuration</p>',
'erreur_pas_de_config' => '<p>La configuration n\'est pas faite</p>',
'erreur_pas_de_rub' => '<p>Aucune rubrique n\'a &eacute;t&eacute; s&eacute;lectionn&eacute;e</p>',
'erreur_pas_de_champ' => '<p>Aucun &eacute;l&eacute;ment &agrave; afficher n\'a &eacute;t&eacute; s&eacute;lectionn&eacute;</p>',
'erreur_pet_id_article' => '<p>Aucune p&eacute;tition trouv&eacute;e. <br />Avez-vous s&eacute;lectionn&eacute; un article ?',

// F
// G
// H
// I
'info_config_rub' => 'S&eacute;lectionnez les groupes de mots-cl&eacute;s que vous voulez attribuer aux rubriques.<br /> ',
'info_config_art' => 'S&eacute;lectionnez les groupes de mots-cl&eacute;s que vous voulez attribuer aux articles.<br /> ',
'info_nb_lignes' => 'Lignes 0 &agrave; 100 sur un total de ',

// J K
// L
'lien_url' => 'Lien hypertexte : URL',
'lien_nom' => 'Lien hypertexte : Titre',

// M
'gmc_associes' => 'Groupes de mots-cl&eacute;s associ&eacute;s',
'mc_associes' => 'Mots-cl&eacute;s associ&eacute;s',
'mot_clef' => 'Mots-cl&eacute;s',

// N 
// O 
'ok_cfg' => 'Ok',

// P 
'pet_titre' => 'P&eacute;titions',
'pet_lien_extract' => 'Extraire les signatures',
// Q 

// R
'rub_a_extraire' => 'Articles &agrave; extraire depuis les rubriques',
'reset_cfg' => 'R&eacute;tablir',

// S
'publie' => 'Publi&eacute;e',
'prive' => 'Non publi&eacute;e',
'signature' => '<p><strong>Export C.S.V</strong></p> 
<p><small>Extraction des donn&eacute;es de SPIP pour tableur au format C.S.V</small>.</p>
<p>Plugin r&eacute;alis&eacute; par <a href="http://www.webdesigneuse.net/">St&eacute;phanie&nbsp;De&nbsp;Nada&iuml;</a>.</p>',

// T
'titre_page' => 'Export CSV',
'titre_gros_page' => 'Export CSV',
'telecharger_data' => 'T&eacute;l&eacute;charger les articles',
'titre' => 'Titre',
'toutes_selectionnees' => 'Toutes les sous-rubriques seront s&eacute;lectionn&eacute;es',

// U
// V
// W X Y Z
'z' => 'z'
);

?>
