<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/gis?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_gis' => 'No point',
	'aucun_objet' => 'No object',

	// B
	'bouton_lier' => 'Link this point',
	'bouton_supprimer_gis' => 'Delete this point permanently',
	'bouton_supprimer_lien' => 'Remove this link',

	// C
	'cfg_descr_gis' => 'Geographic Information System.<br /><a href="http://www.spip-contrib.net/3887" class="spip_out">Link to the documentation</a>.',
	'cfg_inf_adresse' => 'Displays additional address fields (country, city, state, address ...)',
	'cfg_inf_cloudmade' => 'This API needs a key you can create on <a href=\'@url@\' class="spip_out">the CloudMade website</a>.',
	'cfg_inf_geocoder' => 'Enable geocoder functions (search from an address, recovery of the address from the coordinates).',
	'cfg_inf_geolocaliser_user_html5' => 'If the user\'s browser allows it, its approximate geographic location is retrieved to give the default position when creating a new point.',
	'cfg_inf_google' => 'This API needs a key you can create on <a href=\'@url@\' class="spip_out">the GoogleMaps website</a>.',
	'cfg_inf_yandex' => 'This API needs a key you can create on <a href=\'@url@\' class="spip_out">the yandex website</a>.',
	'cfg_lbl_activer_objets' => 'Enable geotagging of content:',
	'cfg_lbl_adresse' => 'Show address fields',
	'cfg_lbl_api' => 'Geolocation API',
	'cfg_lbl_api_cloudmade' => 'CloudMade',
	'cfg_lbl_api_google' => 'Google Maps v2',
	'cfg_lbl_api_googlev3' => 'Google Maps v3',
	'cfg_lbl_api_key_cloudmade' => 'CloudMade API key',
	'cfg_lbl_api_key_google' => 'GoogleMaps API key',
	'cfg_lbl_api_key_yandex' => 'Yandex API key',
	'cfg_lbl_api_mapquest' => 'MapQuest',
	'cfg_lbl_api_microsoft' => 'Microsoft Bing',
	'cfg_lbl_api_openlayers' => 'OpenLayers',
	'cfg_lbl_api_ovi' => 'Ovi Nokia',
	'cfg_lbl_api_yandex' => 'Yandex',
	'cfg_lbl_geocoder' => 'Geocoder',
	'cfg_lbl_geolocaliser_user_html5' => 'Center the map on the location of the user at the creation step',
	'cfg_lbl_maptype' => 'Base map',
	'cfg_lbl_maptype_carte' => 'Map',
	'cfg_lbl_maptype_hybride' => 'Hybrid',
	'cfg_lbl_maptype_relief' => 'Relief',
	'cfg_lbl_maptype_satellite' => 'Satellite',
	'cfg_titre_gis' => 'GIS',

	// E
	'editer_gis_editer' => 'Edit this point',
	'editer_gis_explication' => 'This page lists the whole location-based points of the website.',
	'editer_gis_nouveau' => 'Create a new point',
	'editer_gis_titre' => 'The location-based points',
	'erreur_recherche_pas_resultats' => 'No point corresponds to the searched text.',
	'erreur_xmlrpc_lat_lon' => 'Latitude and longitude should be set as arguments',
	'explication_api_forcee' => 'The is imposed by another plugin or skeleton.',
	'explication_maptype_force' => 'The base map is imposed by another plugin or skeleton.',

	// F
	'formulaire_creer_gis' => 'Create a new location-based point :',
	'formulaire_modifier_gis' => 'Modify the location-based point :',

	// G
	'gis_pluriel' => 'Location-based points',
	'gis_singulier' => 'Location-based point',

	// I
	'icone_gis_tous' => 'Location-based points',
	'info_1_gis' => 'A location-based point',
	'info_1_objet_gis' => '1 object linked to that point',
	'info_aucun_gis' => 'No location-based point',
	'info_aucun_objet_gis' => 'No object linked to that point',
	'info_geolocalisation' => 'Géolocalisation', # NEW
	'info_id_objet' => 'N°',
	'info_liste_gis' => 'Location-based points',
	'info_nb_gis' => '@nb@ location-based points',
	'info_nb_objets_gis' => '@nb@ objects linked to that point',
	'info_numero_gis' => 'Point number',
	'info_objet' => 'Object',
	'info_recherche_gis_zero' => 'No result for « @cherche_gis@ ».',
	'info_supprimer_lien' => 'Unlink',
	'info_supprimer_liens' => 'Unlink all the points',
	'info_voir_fiche_objet' => 'Go to page',

	// L
	'label_adress' => 'Address',
	'label_code_postal' => 'Postal code',
	'label_pays' => 'Country',
	'label_rechercher_address' => 'Search for an address',
	'label_rechercher_point' => 'Search for a point',
	'label_region' => 'Region',
	'label_ville' => 'Town',
	'lat' => 'Latitude',
	'libelle_logo_gis' => 'POINT\\\'S LOGO',
	'lien_ajouter_gis' => 'Add this point',
	'lon' => 'Longitude',

	// T
	'texte_ajouter_gis' => 'Add a location-based point',
	'texte_creer_associer_gis' => 'Create and link a location-based point',
	'texte_creer_gis' => 'Create a location-based point',
	'texte_modifier_gis' => 'Modify the location-based point',
	'texte_voir_gis' => 'Show the location-based point',
	'titre_bloc_creer_point' => 'Link a new point',
	'titre_bloc_points_lies' => 'Linked points',
	'titre_bloc_rechercher_point' => 'Search for a point',
	'titre_nombre_utilisation' => 'One use',
	'titre_nombre_utilisations' => '@nb@ uses',
	'titre_nouveau_point' => 'New point',
	'titre_objet' => 'Title',

	// Z
	'zoom' => 'Zoom'
);

?>
