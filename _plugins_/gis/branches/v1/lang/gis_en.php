<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP nomme admin_lang genere le NOW()
// langue / language = fr
// Traduction -- Pierre FICHES <pierre.fiches@free.fr>
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
//elements de formulaire pour editer les coordonnees
'clave_engadida' => 'Added key: ',
'clic_mapa' => 'Clic on the map to change coordinates',
'clic_desplegar' => 'unfold',
'configurar_gis' => 'Configure Gis Plugin',
'conseguir' => '(obtain)',
'cambiar' => 'change coordinates',
'boton_actualizar' => 'update',
'default_geoloc' => 'Default position of maps:'
);

?>
