<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP nomme admin_lang genere le NOW()
// langue / language = fr
// Traduction -- Pierre FICHES <pierre.fiches@free.fr>
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	//elements du formulaire CFG
	
	"activer_geocoding" => "Activer le geocoding",
	"api_carte" => "API des cartes",
	"api_carte_geomap" => "API Google Maps",
	"api_carte_openlayer" => "API Open Layers",
	"explication_geocoding" => "R&eacute;cup&eacute;rer automatiquement les informations des points (pays, ville, adresse, etc).",
	"explication_pages_prive" => "Choisissez les rubriques dans lesquelles le plugin sera disponible.",
	"explication_swfobject" => "D&eacute;sactiver l'insertion du script swf_object dans les pages publiques (si vous n'utilisez pas le lecteur de son dans les infobulles).",
	"desactiver_swfobject" => "D&eacute;sactiver swf_object",
	"formats_documents" => "Format des documents",
	"geocoding" => "Geocoding",
	"pages_public" => "Partie publique",
	"pages_prive" => "Partie priv&eacute;e",
	"parametres_formulaire" => "Param&egrave;tres du formulaire public",
	"rubrique_cible" => "Rubrique cible",
	"statut_articles" => "Statut des articles",
	
	//elements de formulaire pour editer les coordonnees
	
	"boton_actualizar" => "Actualiser",
	"bouton_supprimer" => "Supprimer",
	"cambiar" => "changer les coordonn&eacute;es",
	"clic_mapa" => "Cliquer sur la carte pour changer les coordonn&eacute;es",
	"clic_desplegar" => "d&eacute;plier",
	"coord" => "Coordonn&eacute;es",
	"coord_enregistre" => "Coordonn&eacute;es enregistr&eacute;es",
	"coord_maj" => "Coordonn&eacute;es mises &agrave; jour",
	"label_address" => "Rechercher",
	"label_ville" => "Ville",
	"label_pays" => "Pays",
	
	//elements du formulaire public
	
	"bouton_documents" => "Valider",
	"document_joint" => "Documents joints",
	"document_supprime" => "Document(s) supprim&eacute;(s)",
	"erreur_ajout_article" => "Erreur lors de l'ajout de l'article",
	"erreur_copie_impossible" => "Impossible de copier le document",
	"erreur_formats_acceptes" => "Formats accept&#233;s : @formats@.",
	"erreur_titre" => "Titre obligatoire",
	"erreur_texte" => "Veuillez entrer un texte",
	"erreur_upload" => "Erreur lors du chargement du fichier",
	"ok_formulaire_soumis" => "Votre article a &eacute;t&eacute; envoy&eacute; sur le site.",
	
	"configurar_gis" => "Configurer le Plugin Gis",
);

?>