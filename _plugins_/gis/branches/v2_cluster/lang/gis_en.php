<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_gis' => 'No point',
	'aucun_objet' => 'No object',

	// B
	'bouton_lier' => 'Link this point',
	'bouton_supprimer_gis' => 'Delete this point permanently',
	'bouton_supprimer_lien' => 'Remove this link',

	// C
	'cfg_descr_gis' => 'Geographic Information System.<br />Link to the documentation.',
	'cfg_inf_adresse' => 'Displays additional address fields (country, city, state, address ...)',
	'cfg_inf_cloudmade' => 'This API needs a key you can create on <a href=\'@url@\'>the CloudMade website</a>.',
	'cfg_inf_geocoder' => 'Enable search function in form.', # MODIF
	'cfg_inf_geolocaliser_user_html5' => 'If the user\'s browser allows it, its approximate geographic location is retrieved to give the default position when creating a new point.',
	'cfg_inf_google' => 'This API needs a key you can create on <a href=\'@url@\'>the GoogleMaps website</a>.',
	'cfg_lbl_adresse' => 'Show address fields',
	'cfg_lbl_api' => 'Geolocation API',
	'cfg_lbl_api_cloudmade' => 'CloudMade',
	'cfg_lbl_api_google' => 'Google Maps v2',
	'cfg_lbl_api_googlev3' => 'Google Maps v3',
	'cfg_lbl_api_key_cloudmade' => 'CloudMade API key',
	'cfg_lbl_api_key_google' => 'GoogleMaps API key',
	'cfg_lbl_api_mapquest' => 'MapQuest',
	'cfg_lbl_api_microsoft' => 'Microsoft Bing',
	'cfg_lbl_api_multimap' => 'MultiMap',
	'cfg_lbl_api_openlayers' => 'OpenLayers',
	'cfg_lbl_api_openspace' => 'OpenSpace',
	'cfg_lbl_api_yahoo' => 'Yahoo',
	'cfg_lbl_geocoder' => 'Geocoder',
	'cfg_lbl_geolocaliser_user_html5' => 'At the creation step, center the map on the location of the user',
	'cfg_titre_gis' => 'GIS',

	// E
	'editer_gis_editer' => 'Edit this point',
	'editer_gis_explication' => 'This page lists the whole geolocated points of the website.',
	'editer_gis_nouveau' => 'Create a new point',
	'editer_gis_titre' => 'The geolocated points',
	'erreur_recherche_pas_resultats' => 'No point corresponds to the searched text.',

	// F
	'formulaire_creer_gis' => 'Create a new geolocated point :',
	'formulaire_modifier_gis' => 'Modify the geolocated point :',

	// I
	'icone_gis_tous' => 'Geolocated points',
	'info_id_objet' => 'N&deg;',
	'info_liste_gis' => 'Points géolocalisés', # NEW
	'info_liste_gis_objet' => 'Linked points to this object',
	'info_liste_objets_gis' => 'Objects attached to this point',
	'info_numero_gis' => 'Point number',
	'info_objet' => 'Object',
	'info_supprimer_lien' => 'Detach',
	'info_voir_fiche_objet' => 'Go to page',

	// L
	'label_adress' => 'Addresse',
	'label_code_postal' => 'Postal code',
	'label_pays' => 'Country',
	'label_rechercher_address' => 'Search for an address',
	'label_rechercher_point' => 'Search for a point',
	'label_region' => 'Region',
	'label_ville' => 'Town',
	'lat' => 'Latitude',
	'libelle_logo_gis' => 'POINT\\\'S LOGO',
	'lon' => 'Longitude',

	// T
	'titre_bloc_creer_point' => 'Link a new point',
	'titre_bloc_points_lies' => 'Linked points',
	'titre_bloc_rechercher_point' => 'Search for a point',
	'titre_nombre_utilisation' => 'One use',
	'titre_nombre_utilisations' => '@nb@ uses',

	// Z
	'zoom' => 'Zoom'
);

?>
