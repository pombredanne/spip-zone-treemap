<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-gis?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'gis_description' => 'Provee la estructura y las interfaces para crear, manejar y visualizar puntos geolocalizados sobre mapas. Estás mapas puedes ser recuperadas desde varios proveedores gracias a la libreria Mapstraction.', # MODIF
	'gis_slogan' => 'Sistema de información geográfica'
);

?>
