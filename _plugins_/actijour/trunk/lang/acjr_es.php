<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// actijour 1.52 - --> todo 1.55 -->
// Traducción ES, JSJ, Mayo 2007

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
'abrv_administrateur' => 'Admin.',
'abrv_redacteur' => 'redac.',
'abrv_visiteur' => 'Visit.',
'activite_du_jour' => 'Actividad del d&iacute;a',
'afficher_stats_art' => 'Mostrar estad&iacute;sticas del Art&iacute;culo',
'article_inexistant' => ':: <i>Art&iacute;culo inexistente</i> ::',
'articles_crees_modifies' => 'Articles cr&eacute;&eacute;s/modifi&eacute;s', ## 1.55
'articles_modifies_crees_jour' => 'Articles modifi&eacute;s et/ou cr&eacute;&eacute;s le @jour@', ## 1.55
'aucun' => '&nbsp;Ning&uacute;n&nbsp;',
'aucun_article_cree' => 'Aucun article cr&eacute;&eacute;/modifi&eacute; ce jour', ## 1.55
'aucun_article_visite' => 'Ning&uacute;n Art&iacute;culo visitado a esa hora',
'aucun_auteur_en_ligne' => 'Aqu&iacute; nadie se mueve desde hace 15 mn !<br />Aparte de ti!',
'aucune_moment' => 'Nadie de momento!',
'auteurs_connections' => 'Connect&eacute;s du jour*', ## 1.55
'auteurs_en_ligne' => 'desde hace unos 15 mn, estaban en l&iacute;gnea:',

// B
'bargraph_trimestre_popup' => 'Bargraph del trimestre en Popup',

// C
'configuration_commune' => 'Configuration commune',
'configuration_perso' => 'Configuration personnelle de : @nom@',
'conf_nbl_art' => 'Nombre de lignes par tranche, du tableau des articles visit&eacute;s du jour.',
'conf_nbl_aut' => 'Nombre d\'auteurs affich&eacute;s dans le tableau 
					"Derni&egrave;res Connexions".',
'conf_nbl_mensuel' => 'Nombre de mois affich&eacute;s dans le tableau des visites mensuelles (jauges).',
'conf_nbl_topgen' => 'Page TopTen. Transformez le tableau "Topten g&eacute;n&eacute;ral" en Top-15, 20..<br />
						Entrez le nombre d\'articles &agrave; afficher.',
'conf_nbl_topmois' => 'Page TopTen. Transformez le tableau "Topten sur 30 jours" en Top-15, 20..<br />
						Entrez le nombre d\'articles &agrave; afficher.',
'conf_nbl_topsem' => 'Page TopTen. Transformez le tableau "Topten sur 8 jours" en Top-15, 20..<br />
						Entrez le nombre d\'articles &agrave; afficher.',
'conf_ordon_milieu' => 'Changez l\'ordre d\'affichage des blocs de la colonne du milieu 
						sur la page principale : "Ce jour".<br />
						- tableau des articles : <b>1</b><br />
						- tableau des rubriques : <b>2</b><br />
						- tableau des visites sur 8 jours : <b>3</b><br />
						- liste des liens entrants : <b>4</b><br />
						Entrez les num&eacute;ros de blocs s&eacute;par&eacute; par une virgule
						(ex. : 1,2,3,4).', ## 1.53

// D
'date_jour_maxi_vis' => 'El @date_max@ : @visites_max@ vis.',
'date_maj' => 'Date M&agrave;J', ## 1.55
'date_publication_dpt' => 'Date publication : ', ## 1.55
'date_redaction_dpt' => 'R&eacute;dac ant&eacute;rieure : ', ## 1.55
'date_serveur_mysql' => 'Date serveur MySQL', ## 1.55
'date_serveur_php' => 'Date serveur PHP', ## 1.55
'depuis_date_visites_pg' => 'Depuis @heure@ @date@, @nb_visite@ Visites 
							sur @nb_articles@ articles 
							(voir Pr&eacute;visions).', ## 1.53
'depuis_date_visites_prev' => 'Depuis @heure@ @date@, @nb_visite@ visites 
							sur @nb_articles@ articles.', ## 1.53
'depuis_le_prim_jour' => 'Desde el <b>@prim_jour_stats@</b>',
'dernieres_connections' => '&Uacute;ltimas Conexiones*',


// E
'entete_tableau_art_jour' => '<b>@nb_art_visites_jour@ Art&iacute;culos visitados</b>, 
								o "apuntados"<b> @aff_date_now@</b>',
'entete_tableau_art_hier' => '<b>@nb_art_visites_jour@ Art&iacute;culos visitados</b>, 
								o "apuntados", <b>ayer @aff_date_now@</b>',
'entete_tableau_mois' => '<i><b>Para informaci&oacute;n ... en cifras !</b></i><br />Las visitas de @nb_mois@ meses.',

// F
'forum' => 'Foros', ## 1.55

// G
'global_vis_hier' => 'Ayer: <b>@global_jour@</b>',
'global_vis_jour' => 'D&iacute;a: <b>@global_jour@</b>',
'global_vis_global' => 'Global: <b>@global_stats@</b>',
'graph_article_dpt' => 'Graph de l\'article : ', ## 1.55
'graph_trimestre' => 'Gr&aacute;fico del trimestre:',
'grosse_journee_' => 'Gran d&iacute;a ...',

// H
'huit_derniers_jours' => 'Los 8 &uacute;ltimos d&iacute;as ...',
'haut_page' => 'Haut de page',

// I
'info_colonnes_topten' => 'Info columnas tablas:<br />
							A - Num. art&iacute;culo.<br />
							B - T&iacute;tulo art&iacute;culo.<br />
							C - Total visitas del periodo.<br />
							D - M&aacute;ximo visitas (1 d&iacute;a) del periodo.<br />
							E - Total visitas del art&iacute;culo.',
'info_dernieres_connections' => '*Les @nb_aut@ derni&egrave;res connexions 
								parmis les Admins et R&eacute;dacs du site', ## m 1.55
'info_page_actijour_prev' => 'Visites et nombre d\'articles non encore trait&eacute;s
								par SPIP dans la BDD', ## 1.53

// J
'jour' => 'D&iacute;a',
'jour_affiche_dpt' => 'Jour affich&eacute; :', ## 1.55

// L
'liens_entrants_jour' => 'Enlaces entrantes del d&iacute;a',

// M
'message' => '&nbsp;mensaje&nbsp;', ## 1.55
'messages' => '&nbsp;mensajes&nbsp;', ## 1.55
'mise_a_jour' => 'Actualizaci&oacute;n',
'mois_pipe' => 'Mes |',
'moyenne_c' => 'Media.',
'moyenne_mois' => 'Media/mes',

// N
'nombre_art' => 'N&uacute;m. Art.',
'nombre_visites_' => 'N&uacute;mero de Visitas ..',
'numero_' => 'n&uacute;mero: &nbsp;',
'numero_court' => 'N&deg;',

// O
'onglet_actijour_hier' => 'Ayer',
'onglet_actijour_pg' => 'Ce jour', ## m 1.55
'onglet_actijour_prev' => 'pr&eacute;visions',
'onglet_actijour_top' => 'TopTen',
'onglet_actijour_conf' => 'Config', // short word
'onglet_actijour_connect' => 'Connect&eacute;s', ## 1.55
'onglet_connect_0minirezo' => 'Administrateurs', ## 1.55
'onglet_connect_1comite' => 'R&eacute;dacteurs', ## 1.55
'onglet_connect_6forum' => 'Visiteurs', ## 1.55
'onglet_connect_tous' => 'Tous', ## 1.55

// P
'pages_article_vues' => 'P&aacute;ginas \'Art&iacute;culo\' Vistas',
'pages_art_cumul_jour' => 'd&iacute;a: <b>@cumul_vis_art_jour@</b> p.',
'pages_art_moyenne_jour' => 'Son <b>@moy_pages_jour@</b> p./visita',
'pages_global_cumul_jour' => 'Global: <b>@global_pages_stats@</b> p.',
'pages_global_moyenne_jour' => 'Son <b>@moy_pag_vis@</b> p./visita',
'page_phpinfo' => 'P&aacute;gina phpinfo',
'pied_tableau_mois' => '* no toma en cuenta la jornada actual.',
'popularite' => 'Poblac.',
'popup_date_debut_stats' => 'Stat. depuis le @date_deb_fr@', ## 1.55
'popup_date_edit_art' => 'Art. Edit&eacute; le @date_edit@', ## 1.55
'popup_date_redac_art' => '&nbsp;- Redac. le @date_redac@', ## 1.55

//R
'repartition_visites_secteurs' => 'Visitas por Sectores del D&iacute;a 
									(p&aacute;ginas art&iacute;culos vistas)',

// S
's' => 's',
'signature_plugin' => '<b>Actividad del D&iacute;a - @version@</b><br />(10/2004 - 06/2009)<br />
						Peque&ntilde;o gadget ... para ver las visitas del sitio.<br />
						Por Scoty - <a href=\'http://www.koakidi.com\'>koakidi.com</a><br />
						<br />Esto no es indispensable !<br />pero, en fin ... !',
'signatures_petitions' => 'Firmas de Peticiones',
'soit_nbre_jours' => 'Son <b>@nb_jours_stats@ d&iacute;as</b>',
'soit_moyenne_par_jour' => 'Son (bruto) <b>@moy_global_stats@</b> visitas/d',
'stats_actives_' => 'Estad&iacute;sticas activas ...',

// T
'telechargements_dpt' => 'Descargas :',
'text_bouton_afficher' => 'Afficher', ## 1.55
'title_vers_page_graph' => 'Hacia la p&aacute;gina de estad&iacute;sticas gr&aacute;ficas de SPIP',
'title_vers_popup_graph' => 'Stat gr&aacute;fica en Popup',
'titre_actijour' => 'Actividad<br />del d&iacute;a',
'titre_article' => 'T&iacute;tulo art&iacute;culo',
'top_ten_article_8_j' => 'TopTen art&iacute;culos en 8 d&iacute;as',
'top_ten_article_30_j' => 'TopTen art&iacute;culos en 30 d&iacute;as',
'top_ten_article_gen' => 'TopTen Art&iacute;culos general',
'total_visites' => 'TT Vis.',
'tous_date_connections' => 'Derni&egrave;re connexion des "Auteurs"', ## 1.55

// V
'visites_jour' => 'v. d&iacute;a', // hacer corto - faire court
'visites' => 'Visitas',
'voir' => 'ver',
'voir_details' => 'Voir d&eacute;tails', ## 1.55
'voir_plugin' => 'Ver p&aacute;ge : ',
'voir_suivi_forums' => 'Ver p&iacute;gina Seguimiento Foros',
'voir_suivi_petitions' => 'Ver p&iacute;gina Seguimiento Peticiones',

// Z
'z' => 'z'

);

?>
