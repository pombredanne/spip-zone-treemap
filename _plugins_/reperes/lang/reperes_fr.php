<?php
/**
 * Plugin Reperes pour Spip 2.0
 * Licence GPL (c) 2009 - Ateliers CYM
 */

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'annuler' => 'Annuler',
	
	// C
	'cfg_alt_supprimer_repere'	=> 'Supprimer ce rep&egrave;re',
	'cfg_lbl_insertion' 		=> 'Point d\'insertion',
	'cfg_inf_insertion' 		=> 'Ins&#233;rer le bloc de <strong>rep&egrave;res</strong> :',
	'cfg_help_insertion' 		=> 'Choisissez l\'endroit du squelette o&ugrave; sera ins&eacute;r&eacute; le bloc contenant les rep&egrave;res. Vous pouvez indiquer tout &eacute;l&eacute;ment html qui compose votre page en sp&eacute;cifiant bien s\'il s\'agit d\'une ID ou d\'une CLASS.',
	'cfg_lbl_bloc'				=> 'le bloc ',
	'cfg_lbl_avant' 			=> 'avant',
	'cfg_lbl_dans' 				=> 'dans',
	'cfg_lbl_apres' 			=> 'apr&egrave;s',	
	'cfg_lbl_ajouter' 			=> 'Ajouter un rep&egrave;re',
	'cfg_help_ajouter' 			=> 'Vous pouvez ajouter un rep&egrave;re et sp&eacute;cifiant ses caract&eacute;riqtiques. L\'identifiant est ajout&eacute; automatiquement.<br /><br />Vous devez s&eacute;lectionner une <em>orientation</em> (verticale ou horizontale) puis indiquer une <em>position</em> et une <em>couleur</em> en h&eacute;xad&eacute;cimal (sous la forme &quot;#3a0f44&quot;) ou selon son nom web (yellow, cyan, purple...).<br /><br />La position s\'indique en pixels (ou pourcentage) &agrave; partir du bord gauche du contenant si l\'orientation est verticale, ou du bord haut du contenant si l\'orientation est horizontale.',
	'cfg_lbl_existant' 			=> 'Rep&egrave;res existants',
	'cfg_help_existant'			=> 'Voici la liste des rep&egrave;res d&eacute;j&agrave; param&eacute;tr&eacute;s. Utilisez le bouton "supprimer" pour supprimer un rep&egrave;re &eacute;xistant.',
	
	'configurer_insertion' => 'Point d\'insertion',

	// D
	'deja_modifie' => 'D&eacute;j&agrave; modifi&eacute;',
	'donnees_mal_formatees' => 'Donn&eacute;es mal format&eacute;es',

	// E
	'editer' => '&Eacute;diter',
	'editer_@type@_@id@' => '&Eacute;diter @type@ @id@ en entier',
	'editer_tout' => '&Eacute;diter tout',

	// M
	'modifie_par_ailleurs' => 'Modifi&eacute; par ailleurs',

	// N
	'non_autorise' => 'Non autoris&eacute;',
	'non_implemente' => 'Non impl&eacute;ment&eacute;',

	// P
	'pas_de_modification' => 'Pas de modification',
	'pas_de_valeur' => 'Pas de valeur',

	// S
	'sauvegarder' => 'Sauvegarder les modifications ?',
	'svp_copier_coller' => 'D&eacute;sol&eacute;, copiez/collez vos changements et recommencez',

	// V
	'veuillez_patienter' => 'Veuillez patienter...'
);

?>
