<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'code_postal' => 'Code postal',
'code_postaux' => 'Code postaux',



'importer_les_cp' => 'Importer les codes postaux',
'integrer_les_cp' => 'Intégrer les codes postaux',
'telecharger_les_cp' => 'Télécharger les codes postaux',
'telecharger_integrer_cp' => 'Télécharger et intégrer les codes postaux',

'importer' => 'Importer',

'bouton_importer' => 'Importer',
'bouton_telecharger' => 'Télécharger',
'fichiers_a_telecharger' => 'Fichiers à télécharger',
'ecraser_enregistrement' => '&Eacute;craser les enregistrements existants',
'vider_la_table' => 'Vider la table avant l\'importation',
'label_chemin_cp' => 'Dossier des données',
'label_chemin_cp_details' => 'Indiquez le chemin dans lequel les données téléchargées sont stockées.',
'filtrer_les_donnees' => 'Filtrer les données',
'relier_avec_communes' => 'Relier les codes postaux aux communes (Plugin COG)',
'configuration' => 'Configuration',

'choississez_le_fichier_a_telecharger' => 'Choississez le fichier à télécharger à partir du serveur Geonames',

'liste_des_tables_cp'=>'Liste des tables'


);


?>

