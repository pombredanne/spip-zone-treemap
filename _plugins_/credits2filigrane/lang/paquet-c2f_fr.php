<?php
/**
 * Plugin Credits en filigrane
 * Licence GPL3 (c) 2012 cy_altern
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
	'c2f_description' => 'Générer automatiquement un filigrane sur les documents images en utilisant le contenu du champ "Crédits"
_ Pour enlever le filigrane d\'une image, mettre "0" dans le champ "Cr&#233;dits".
_ Nécessite le plugin Mediathèque.',
	'c2f_slogan' => 'Générer un filigrane sur les images en utilisant les "Crédits"',
);
?>