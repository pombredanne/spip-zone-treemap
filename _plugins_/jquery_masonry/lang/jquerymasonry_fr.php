<?php
// Securite
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'configurer_explication' => 'Consultez le <a href="http://masonry.desandro.com/" target="_blank">site du plugin jQuery Masonry</a> pour une démonstration de ses possibilités.',
	'configurer_titre' => 'Configurer jQuery Masonry',

	// E
	'explication_animation' => 'Animer le repositionnement des blocs lors d\'un redimensionnement du container ?',
	'explication_container' => 'Classe ou id du container des éléments à traiter (saisir par exemple .maconner)',
	'explication_items' => 'Classe ou id des blocs à agencer (saisir par exemple .briques)',
	'explication_largeur' => 'Largeur des colonnes (en pixels)',
	'explication_marge' => 'Taille des marges autour des blocs (en pixels)',
	'explication_multicolonne' => 'Arrangement des blocs de type multi-colonne ? Si oui, il vous faut forcer la largeur des blocs dans votre feuille de style.',

	// L
	'label_animation' => 'Animation',
	'label_container' => 'Container',
	'label_items' => 'Items',
	'label_largeur' => 'Largeur',
	'label_marge' => 'Marge',
	'label_multicolonne' => 'Multi-colonne',
	'label_nombre' => 'Nombre d\'éléments',
	'legend_jquerymasonry_nombre' => 'Nombre d\'éléments à traiter',
	'legend_jquerymasonry' => 'Paramètrage @numero@',

	// T
	'titre_menu' => 'jQuery Masonry',

);
?>
