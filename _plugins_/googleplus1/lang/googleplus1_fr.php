<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	//C	
	'cfg_en_bas' => 'En bas de chaque page',
	'cfg_exemples' => 'Exemples (sur les squelettes dist) :',
	'cfg_explication_boite_info' => 'Ici vous pouvez choisir la taille et la position du bouton google+1 dans vos pages.',
	'cfg_inserer' => 'Ins&#233;rer le bloc google+1 en-dessous du bloc suivant :',
	'cfg_insertion' => 'Insertion dans les pages :',
	'cfg_selector' => 'S&#233;lecteur :',
	'cfg_sous_bloc' => 'Sous le bloc de titre des pages article',
	'cfg_sous_div' => 'Sous la div nomm&#233;e id=google+1',
	'cfg_titre_boite_info' => 'Configuration du plugin Google+1',
	'cfg_titre_google+1' => 'Google+1',
	'cfg_taille' => 'Choisir la taille du bouton :',
	'cfg_taillepetit' => 'Petit (15 px)',
	'cfg_taillemoyen' => 'Moyen (20 px)',
	'cfg_taillestandard' => 'Standard (24 px)',
	'cfg_taillegrand' => 'Grand (60 px)',
	'cfg_titre_googleplus1' => 'Google+1',
);
?>
