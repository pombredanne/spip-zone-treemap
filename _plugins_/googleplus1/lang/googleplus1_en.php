<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	//C	
	'cfg_en_bas' => 'At the bottom of each page',
	'cfg_exemples' => 'Examples (on the basic skeletons) :',
	'cfg_explication_boite_info' => 'You can choose here the size and the position of the google+1 button in your pages.',
	'cfg_inserer' => 'Insert the google+1 button after the following block :',
	'cfg_insertion' => 'Insertion in the pages :',
	'cfg_selector' => 'Selector :',
	'cfg_sous_bloc' => 'Under the title of the article pages',
	'cfg_sous_div' => 'Under the div "id=google+1"',
	'cfg_titre_boite_info' => 'Configuration of the Google+1 plugin',
	'cfg_titre_google+1' => 'Google+1',
	'cfg_taille' => 'Choose the button size :',
	'cfg_taillepetit' => 'Small (15 px)',
	'cfg_taillemoyen' => 'Medium (20 px)',
	'cfg_taillestandard' => 'Standard (24 px)',
	'cfg_taillegrand' => 'Tall (60 px)',
	'cfg_titre_googleplus1' => 'Google+1',
);
?>
