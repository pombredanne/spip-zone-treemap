<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-media
// Langue: fr
// Date: 03-07-2012 15:14:22
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// M
	'media_description' => 'Les modèles <doc>, <img> et <emb> produisent chacun un résultat différent et ce résultat, pour les images, dépend du fait qu’elle soit dans le portfolio ou non. Ce plugin propose une nouvelle série de modèles ayant un comportement unifié et indépendant du mode des images. Les modèles existants (doc, emb, img) ne sont pas modifiés afin d’assurer la rétrocompatibilité.',
	'media_slogan' => 'Modèles alternatifs pour l\'insertion des documents',
);
?>