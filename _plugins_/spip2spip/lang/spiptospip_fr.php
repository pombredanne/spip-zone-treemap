<?php

// This is a SPIP module file  --  Ceci est un fichier module de SPIP

//$GLOBALS['i18n_spip2spip_fr'] = array(
$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
'article_license' => '<br />Licence de l\'article:',
'action_syndic' => 'syndiquer manuellement',
'action_delete' => 'effacer',
'aucun_article' => 'aucun article &agrave; syndiquer actuellement',

// B
'back' => 'retour',

// C
'copy_spip2spip' => 'Spip2Spip: Copie SPIP &agrave; SPIP',
'config_spip2spip' => 'Configurer',

// F
'form_s2s_1' => 'Titre du site',
'form_s2s_2' => 'URL du fil au format SPIP2SPIP',
'form_s2s_3' => 'Ajouter ce site',

// H
'how_to' => 'Pensez &agrave; bien attribuer les mots-cl&eacute;s du groupe - spip2spip - [<a href=\'?exec=mots_tous\'>Voir le groupe</a>]<br /><br />
- attribuez les mots cl&eacute;s de ce groupe aux <strong>articles</strong> dont vous voulez envoyer le contenu vers les autres spip2spip<br /><br />
- attribuez les mots cl&eacute;s de ce groupe aux <strong>rubriques</strong> dans lequelles vous voulez importer les articles spip2spip li&eacute;s &agrave; une th&eacute;matique donn&eacute;e.',

// I
'intro_spip2spip' => '<strong>SPIP 2 SPIP</strong><br /><p>Permet de recopier automatiquement des articles d\'un SPIP &agrave; l\'autre.</p>',
'install_spip2spip' => 'Installation des tables de spip2spip',
'install_spip2spip_1' => 'Cr&eacute;ation de la table SQL',
'install_spip2spip_2' => 'Ajout des flux backends',
'install_spip2spip_groupe_mot' => 'Cr&eacute;ation du groupe de mots cl&eacute;s - spip2spip -',
'install_spip2spip_4' => 'groupe spip2spip pour d&eacute;signer les articles et rubriques a synchroniser.',
'install_spip2spip_5' => '{{mode d&#039;emploi:}}
-* attribuez les mots cl&eacute;s de ce groupe aux {{articles}} que vous voulez envoyer vers les sites utilisant spip2spip.
-* attribuez les mots cl&eacute;s de ce groupe aux {{rubriques}} dans lequelles vous voulez importer les articles spip2spip sur cette th&eacute;matique',
'install_spip2spip_99' => '<p>Installation de SPIP2SPIP compl&egrave;te !</p><a href=\'?exec=spip2spip\'>Retourner sur l\'interface principale de SPIP2SPIP</a>',
'imported_already' => 'Article d&eacute;j&agrave; import&eacute;',
'imported_new' => 'Nouvel article',
'imported_update' => 'Article mis &agrave; jour  ',
'event_ok' => 'Ajout d\'un &eacute;v&eacute;nement ',
'imported_view' => 'Consulter l\'article import&eacute;',
'installed' => 'spip2spip est install&eacute;. cette page ne sert plus &agrave; rien</p>',

// L
'last_syndic' => 'Derni&egrave;re syndication',

// N
'no_target' => 'aucune rubrique li&eacute;e &agrave; ce mot cl&eacute;',
'not_installed' => 'spip2spip n\'est pas encore install&eacute;.<p><a href=\'?exec=spip2spip_install\'>installer spip2spip</a></p>',

// O
'origin_url' => 'L\'adresse originale de cet article est',

// S
'site_manage' => 'Gestion des sites &agrave; synchroniser',
'site_available' => 'Sites inscrits',
'site_add' => 'Ajouter un nouveau site',

// T
'titre' => 'Spip2Spip',

);
?>