<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'assigne_a'	=> 'Assigné à : ',
	'aucun_ticket_trouve'	=> 'Aucun ticket trouvé',
	'bouton_enregistrer' => 'Enregistrer',
	'bouton_fermer' => 'Fermer',
	'erreur_infos_manquantes' => 'Il manque des informations obligatoires.',
	'erreur_manque_titre' => 'Un bref résumé est requis.',
	'erreur_manque_texte' => 'Une description même succincte est attendue.',

	'label_assigner_a' => 'Assigner à',
	'label_composant_creer' => 'Ou nom du nouveau composant',
	'label_composant_relier' => 'Relier à un composant existant',
	'label_description'	=> 'Description',
	'label_titre_resume' => 'Titre/Résumé',
	'les_tickets' => 'Tous les tickets &agrave; faire',
		
	'rapporte_par'	=> 'Rapport&eacute; par : ',
	
	// N
	'nom_plugin' => 'Le plugin Pr&eacute;\'Prod',

	// S
	'sinscrire' => "S'inscrire",
	'severite_bloquant' => 'Bloquant',
	'severite_important' => 'Important',
	'severite_normal' => 'Normal',
	'severite_peu_important' => 'Peu important',
	'statut_mis_a_jour' => 'Statut mis &agrave; jour',
	'statut_ferme' => 'Fermé',
	'statut_ferme_long' => 'Tous les tickets ferm&eacute;s',
	'statut_inchange' => 'Le statut n\'a pas &eacute;t&eacute; modifi&eacute;.',
	'statut_ouvert' => 'Ouvert et discuté',
	'statut_redac' => 'En cours de r&eacute;daction',
	'statut_resolu' => 'Résolu',
	'statut_resolu_long' => 'Tous les tickets r&eacute;solus',
	'succes_ticket_ajoute' => "Nouveau ticket enregistré (id @id@).",
	'succes_ticket_modifie' => "Ticket (id @id@) modifié.",
	'syndiquer_ticket' => 'Syndiquer le ticket&nbsp;:',
	'syndiquer_tickets' => 'Syndiquer les tickets du site',

	// T
	'ticket' => 'Ticket',
	'ticket_enregistre' => 'Ticket enregistr&eacute;',
	'tickets' => 'Tickets',
	'tickets_autorisations' => 'Les autorisations',
	'tickets_derniers_commentaires' => 'Les derniers commentaires',
	'tickets_general' => 'Général',
	'tickets_page'	=> 'Tous les tickets relatifs à cette page:',
	'tickets_sur_inscription' => "
		L'&eacute;criture des tickets ou commentaires n'est
		possible qu'aux personnes identifi&eacute;es.
	",
	'tickets_traites' => 'Tous les tickets trait&eacute;s',
	'titre' => 'Tickets, suivi de bugs',
	'titre_ajouter_ticket' => 'Ajouter un nouveau ticket',
	'titre_apercu'	=> 'aperçu',
	'titre_modifier_ticket'	=> "Modifier le ticket @id@",
	'titre_identification' => 'Identification',
	'titre_liste' => 'Liste des tickets',
	'tous_tickets_ouverts' => 'Tous les tickets ouverts',
	'type_amelioration' => 'Amélioration',
	'type_amelioration_long' => 'Les tickets demandant une am&eacute;lioration',
	'type_probleme' => 'Problème',
	'type_probleme_long' => 'Les probl&egrave;mes &agrave; r&eacute;soudre',
	'type_tache' => 'Tâche',
	'type_tache_long' => 'Les t&acirc;ches &agrave; accomplir',

);


?>
