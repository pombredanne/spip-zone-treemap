<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tra?lang_cible=eu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_traduction' => 'Itzulpen bat gehitu',
	'avis_rubrique_source' => 'Vous devez d\'abord traduire toutes les rubrique parentes', # NEW

	// R
	'rubrique_reference' => '(Erreferentziazko atala)',

	// T
	'trad_delier_rubrique' => 'Ne plus lier cette rubrique à ces traductions' # NEW
);

?>
