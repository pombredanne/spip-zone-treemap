<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tra?lang_cible=ast
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_traduction' => 'Amestar una traducción',
	'avis_rubrique_source' => 'Vous devez d\'abord traduire toutes les rubrique parentes', # NEW

	// R
	'rubrique_reference' => '(seición de referencia)',

	// T
	'trad_delier_rubrique' => 'Ne plus lier cette rubrique à ces traductions' # NEW
);

?>
