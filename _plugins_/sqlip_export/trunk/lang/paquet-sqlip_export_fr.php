<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

        'prefix_description' => 'Appliqu&#233; sur un site install&#233; en SQLite, permet de t&#233;l&#233;charger un fichier mysql-dump.csv importable sur un site install&#233; en MySQL. Il utilise un squelette et les it&#233;rateurs pour produire ce fichier contenant les d&#233;finitions de tables et les contenus du site en SQLite. Le tout se passe en CHARSET UTF-8.

Pour lancer le t&#233;l&#233;chargement, le webmestre doit appeler [->../spip.php?page=sqlite-mysql]',
        'prefix_slogan' => 'Importez une base SQLite dans MySQL'
);
?>
