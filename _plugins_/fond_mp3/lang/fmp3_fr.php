<?php

// lang/fmp3_fr.php

// $LastChangedRevision$
// $LastChangedBy$
// $LastChangedDate$

$GLOBALS['i18n_fmp3_fr'] = array(

	'portfolio_fmp3' => "Illustration sonore"
	
	, 'fmp3_aide' => "<strong>Fond MP3</strong> est un plugin pour SPIP 
		compos&#233; de plusieurs scripts.<br /><br />
		Les divers scripts ont une licence d'utilisation sp&#233;cifique.<br /><br />
		Avant d'utiliser <strong>Fond MP3</strong> sur votre site, consultez 
		<a href='http://www.sean-o.com/jquery/jmp3/'>jMP3: Sean O's javaScript MP3 Player / jQuery plugin</a>
		"
	, 'fmp3_aide_install' => "<p>Bienvenue dans le monde de <strong>Fond MP3</strong>.</p>
		<p class='verdana2'>Pour valider les diff&eacute;rentes options de <strong>Fond MP3</strong>, rendez-vous 
		<a href='@url_config@'>sur la page de configuration</a>.</p>"
	, 'pas_acces_a_la_page' => "L'acc&#232;s &#224; cette page ne vous est pas autoris&#233;e."
	
	, 'configuration_fmp3' => "Configuration de l'illustration sonore"
	, 'configurer_fond_sonore' => "Configurer le fond sonore"
	
	, 'info_replier' => "Replier"
	, 'info_titre_article' => "Son de l'article"
	, 'info_titre_rubrique' => "Son de la rubrique"
	, 'info_titre_site' => "Son du site"
	, 'info_telecharger_nouveau_son' => "T&eacute;l&eacute;charger un nouveau son&nbsp;:"
	, 'info_installer_sons_dossier' => "Installer des sons dans le dossier @upload@ pour pouvoir les s&#233;lectionner ici."

	, 'error_format_incorrect' => "Format incorrect. "
	, 'error_reception' => "Erreur de r&#233;ception de fichier. Consulter le journal de spip. "
	, 'error_ecriture' => "Erreur lors de la copie. "
	, 'error_fichier_manquant' => "Veuillez s&#233;lectionner un fichier avant de valider."
	
	, 'autoStart_label' => "Lecture automatique."
	, 'repeatPlay_label' => "Boucler."
	, 'songVolume_label' => "Volume du son (0 &#224; 100). "
	, 'backColor_label' => "Couleur de fond du bouton."
	, 'frontColor_label' => "Couleur de premier plan du bouton."
	
	, 'inherit_label' => "H&#233;ritage. En s&#233;lectionnant cette option, si un article n'a pas d'illustration sonore, 
		c'est celle de la rubrique qui prend sa place. Si une rubrique n'a pas d'illustration sonore, 
		c'est celle du site qui prend sa place."
	
	, 'par_defaut' => "(Par défaut : @default@)."

	, 'reset' => "Reset"
	, 'reset_title' => "R&#233;tablir les r&#233;glages par d&#233;faut"

	, 'voir_journal' => "Voir le journal"
	, 'titre_page_voir_journal' => "Journal du plugin (log)"
	, 'recharger_journal' => "Recharger le journal"

	, 'mode_debug_actif' => "Mode debug actif"	
);

?>