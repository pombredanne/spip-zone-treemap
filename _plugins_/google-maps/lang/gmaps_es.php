<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'gmaps'	=>	'Google Maps',
'apikey_button' =>	'cambiar la llave Google Maps',
'infos_droite' =>	'<b>A&ntilde;adir un punto :</b><br />haga clic sobre la carta al emplazamiento del nuevo punto.<br /><br />
					<b>Colocar un punto :</b><br />Mantenga un clic sobre el punto que quiere colocar y soltalo al lugar que desea.<br /><br />
					<b>Editar la ventana :</b><br />Haga un doble-clic sobre el punto que desea editar y elige <i>Ok</i> para registrar.<br /><br />
					<b>Suprimir un punto :</b><br />Haga un doble-clic sobre el y selecciona el bot&oacute;n <i>Suprimir</i>.',

'generateur_balise' => 'generador de tag',

'alert_cancel' => 'Quiere cerrar esta ventana y perder sus cambios ?',
'alert_delete' => 'Quiere suprimir este punto ?',

'boite_fenetre_titre' => 'proriedades del cuadro',
'boite_fenetre_alignement' => 'Alineaci&oacute;n :',
'boite_fenetre_alignement_left' => 'izquierda',
'boite_fenetre_alignement_none' => 'ninguno',
'boite_fenetre_alignement_right' => 'derecha',
'boite_fenetre_largeur' => 'anchura :',
'boite_fenetre_hauteur' => 'altura :',

'boite_points_titre' => 'Puntos a figar',
'boite_points_type' => 'figar :',
'boite_points_type_tous' => 'Todos los puntos',
'boite_points_type_select' => 'Los puntos seleccionados',
'boite_points_type_rubrique' => 'los de los art&iacute;culos y sus r&uacute;bricas de la r&uacute;brica <b>xx</b>.',

'bouton_ok' => 'Ok',
'bouton_cancel' => 'Anular',
'bouton_delete' => 'Suprimir',

'code_insert' => '<b>C&oacute;digo a insertar :</b>',

'lirelasuite' =>	'Ver m&aacute;s',

'please_configure_key'	=>	'[TO TRANSLATE]Please configure your key'
);


?>
