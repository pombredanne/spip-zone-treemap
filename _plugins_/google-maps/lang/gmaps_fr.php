<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'gmaps'	=>	'Google Maps',
'apikey_button' =>	'Modifier la clef Google Maps',
'infos_droite' =>	'<b>Ajouter un point :</b><br />Cliquer sur la carte &agrave; l\'emplacement du nouveau point.<br /><br />
					<b>D&eacute;placer un point :</b><br />Faire un clic maintenu sur le point &agrave; d&eacute;placer et le relacher &agrave; l\'emplacement souhait&eacute;.<br /><br />
					<b>&Eacute;diter la bulle :</b><br />Faire un double-clic sur le point &agrave; &eacute;diter et choisir <i>Ok</i> pour enregistrer.<br /><br />
					<b>Supprimer un point :</b><br />Faire un double-clic dessus et choisir le bouton <i>Supprimer</i>.',

'generateur_balise' => 'G&eacute;n&eacute;rateur de balise',

'alert_cancel' => 'Voulez-vous fermer cette bulle et perdre les modifications ?',
'alert_delete' => 'Voulez-vous supprimer ce point ?',

'boite_fenetre_titre' => 'Propri&eacute;t&eacute;s du cadre',
'boite_fenetre_alignement' => 'Alignement :',
'boite_fenetre_alignement_left' => 'gauche',
'boite_fenetre_alignement_none' => 'aucun',
'boite_fenetre_alignement_right' => 'droite',
'boite_fenetre_largeur' => 'Largeur :',
'boite_fenetre_hauteur' => 'Hauteur :',

'boite_points_titre' => 'Points &agrave; afficher',
'boite_points_type' => 'Afficher :',
'boite_points_type_tous' => 'Tous les points',
'boite_points_type_select' => 'Les points s&eacute;l&eacute;ctionn&eacute;s',
'boite_points_type_rubrique' => 'Ceux des articles et sous rubriques de la rubrique <b>xx</b>.',

'bouton_ok' => 'Ok',
'bouton_cancel' => 'Annuler',
'bouton_delete' => 'Supprimer',

'code_insert' => '<b>Code &agrave; ins&eacute;rer :</b>',

'lirelasuite' =>	'En savoir plus',

'please_configure_key'	=>	'Veuillez configurer votre clef GoogleMaps.'
);


?>
