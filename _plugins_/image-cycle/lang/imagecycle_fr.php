<?php

	$GLOBALS[$GLOBALS['idx_lang']] = array(
		'titre'				=>	'titre',
		'legende'			=>	'configuration du plugin',
		
		'origineimage'		=>	'origine de l\'image',
		'aleatoire'			=>	'images al&eacute;atoires issues de toutes les images du site',
		'article'			=>	'images d\'un article',
		'renvart'			=>	'renvoi vers l\'article',
		'idarticle'			=>	'choix de l\'article',
		'renvdoc'			=>	'renvoi vers le document (compatible thickbox)',
		'nombre'			=>	'nombre d\'image &acute; afficher',
		
		'paraimage'			=>	'Param&egrave;tres de l\'image',
		'tailleimage'		=>	'Taille de l\'image',
		'taillecadre'		=>	'Taille du cadre',
		'largeur'			=>	'largeur',
		'hauteur'			=>	'hauteur',
		
		'paraanim'			=>	'Param&egrave;tres de l\'animation',
		'couleur'			=>	'couleur du cadre',
		'vitesse'			=>	'vitesse de transition',
		'duree'				=>	'dur&eacute;e d\'affichage de la photo',
		'effet'				=>	'effet d&eacute;sir&eacute;',
		'couleurcadre'		=>	'Couleur du cadre',
		'fond'				=>	'Couleur du fond',
		'cadre'				=>	'Couleur de la bordure',
		'taillefixe'		=>	'Taille fixe (avec d&eacute;formation de l\'image)',
		'description'		=>	'<p>Bas&eacute; sur le script jquery image cycle.</p>
								Affiches une noisette d\'images al&eacute;atoires',
		'description2'		=>	'Configuration de la deuxi&egrave;me noisette pour un autre affichage dans le site',					
								


);
?>