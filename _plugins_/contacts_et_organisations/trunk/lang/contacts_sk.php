<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/contacts?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_contact' => 'Žiaden kontakt tu nie je!',
	'aucune_organisation' => 'Žiadna organizácia tu nie je!',
	'auteur_lie' => 'Id prepojeného autora',

	// B
	'bouton_contacts' => 'Kontakty',
	'bouton_contacts_organisations' => 'Osobné kontakty a spoločnosti',
	'bouton_organisations' => 'Spoločnosti',
	'bouton_rechercher' => 'Vyhľadať',
	'bouton_repertoire' => 'Adresár',

	// C
	'cfg_activer_squelettes_publics_zpip1' => 'Verejné šablóny ZPIPu v1',
	'cfg_activer_squelettes_publics_zpip1_explication' => 'Aktivovať verejné šablóny pre ZPIP vo verzii 1,
		ktoré umožňujú prehliadať kontakty na ľudí a spoločnosti na verejne prístupnej stránke?',
	'cfg_afficher_infos_sur_auteurs' => 'Podrobné zobrazenie autorov?',
	'cfg_afficher_infos_sur_auteurs_explication' => 'Zobraziť kontaktné údaje spoločnosti
		alebo aj na autorov v súkromnej zóne?',
	'cfg_associer_aux_auteurs' => 'Prepojiť s autormi?',
	'cfg_associer_aux_auteurs_explication' => 'Umožňuje prepájať kontakty na ľudí a spoločnosti 
		s autormi. Pridá formulár na prepojenie autora na stránky s kontaktmi na ľudí alebo spoločnosti,
		a aj opačne pridá formulár na prepojenie kontaktov na ľudí alebo spoločnosti so stránku s autormi.',
	'cfg_lier_organisations_rubriques' => 'Prepojiť s rubrikami',
	'cfg_lier_organisations_rubriques_explication' => 'Umožniť prepájať spoločnosti s rubrikami?
		Táto funkcia zobrazí oddeľovač spoločností podľa rubrík a oddeľovač rubrík podľa spoločností.',
	'changer' => 'Zmeniť',
	'chercher_contact' => 'Vyhľadať',
	'chercher_organisation' => 'Vyhľadať',
	'chercher_statut' => 'Stav',
	'confirmer_delier_contact' => 'Určite chcete zrušiť prepojenie tejto organizácie s týmto kontaktom?',
	'confirmer_delier_organisation' => 'Určite chcete zrušiť prepojenie tohto kontaktu s touto organizáciou?',
	'confirmer_delier_organisation_rubrique' => 'Určite chcete zrušiť prepojenie tejto organizácie s touto rubrikou?',
	'confirmer_supprimer_contact' => 'Určite chcete zmazať všetky údaje o tomto kontakte?',
	'confirmer_supprimer_organisation' => 'Určite chcete zmazať všetky údaje o tejto organizácii?',
	'contact' => 'Kontakt',
	'contact_ajouter' => 'Pridať kontakt',
	'contact_ajouter_lien' => 'Pridať tento kontakt',
	'contact_associe_a_auteur_numero' => 'Prepojiť s autorom číslo',
	'contact_associer_a_auteur' => 'Prepojiť s autorom',
	'contact_aucun' => 'Žiaden kontakt',
	'contact_creer' => 'Vytvoriť kontakt',
	'contact_creer_associer' => 'Vytvoriť kontakt a prepojiť',
	'contact_editer' => 'Upraviť tento kontakt',
	'contact_logo' => 'Logo kontaktu',
	'contact_nouveau_titre' => 'Nový kontakt',
	'contact_numero' => 'Kontakt číslo',
	'contact_retirer_lien' => 'Odstrániť kontakt',
	'contact_retirer_tous_lien' => 'Odstrániť všetky kontakty',
	'contact_un' => 'Jeden kontakt',
	'contact_voir' => 'Zobraziť',
	'contacts' => 'Kontakty',
	'contacts_nb' => '@nb@ kontaktov',
	'creer_auteur_contact' => 'Vytvoriť nového autora a prepojiť ho s týmto kontaktom',
	'creer_auteur_organisation' => 'Vytvoriť nového autora a prepojiť ho s touto organizáciou',

	// D
	'definir_auteur_comme_contact' => 'Nastaviť ako kontakt',
	'definir_auteur_comme_organisation' => 'Definovať ako organizáciu',
	'delier_cet_auteur' => 'Zrušiť prepojenie',
	'delier_contact' => 'Zrušiť prepojenie',
	'delier_organisation' => 'Zrušiť prepojenie',

	// E
	'est_un_contact' => 'Tento autor je nastavený ako kontakt',
	'est_une_organisation' => 'Tento autor je nastavený ako organizácia',
	'explication_activite' => 'Činnosť organizácie: mimovládna organizácia, vzdelávanie, publikovanie a pod.',
	'explication_contacts_ou_organisations' => 'Môžete tohto autora nastaviť ako kontakt alebo ako organizáciu.  Ďalšie polia potom môžete vyplniť na stránke Upraviť autora.',
	'explication_identification' => 'Identifikácia organizácie: napr. DIČ.',
	'explication_statut_juridique' => 'spoločnosť, organizácia, združenie a pod.',
	'explication_supprimer_contact' => 'Vymazaním tohto kontaktu vymažete všetky ďalšie údaje, ktoré boli vyplnené na stránke autora.',
	'explication_supprimer_organisation' => 'Odstránením tejto organizácie odstránite všetky ďalšie údaje, ktoré boli vyplnené na autorskej stránke.',
	'explications_page_contacts' => 'Stránka sa rekonštruuje.<br /><br />Plánované akcie:<ul>
<li>zobraziť všetky kontakty,</li><li>zmeniť autorov v kontaktoch,</li><li>nahrať kontakty,</li><li>li>...</li></ul><br />Ďakujeme vám za podnety na  <a href="http://www.spip-contrib.net/Plugin-Contacts-Organisations#pagination_comments-list">diskuskom fóre;-)</ a>',
	'explications_page_organisations' => 'Stránka sa rekonštruuje. <br /> <br /> Plánované akcie: 
 <ul>  <li> zobraziť všetky organizácie, </ li> upraviť autorov v organizáciách, </ li> nahrať organizácie, </ li> ...</ li> </ ul> <br / > Ďakujeme vám za podnety na <a href="http://www.spip-contrib.net/Plugin-Contacts-Organisations#pagination_comments-list"> diskusnom fóre;-) </ a>',

	// I
	'info_contacts_organisation' => 'Kontakty organizácie',
	'info_nb_contacts' => 'Prepojené kontakty',
	'info_organisation_appartenance' => 'Organizácia',
	'info_organisations_appartenance' => 'Členstvo v organizáciách',
	'info_organisations_filles' => 'Podradené organizácie',
	'info_organisations_meres' => 'Nadradené organizácie',
	'info_tous_contacts' => 'Všetky kontakty',
	'info_toutes_organisations' => 'Všetky organizácie',
	'infos_contacts_ou_organisations' => 'Kontakty a organizácie',

	// L
	'label_activite' => 'Aktivita',
	'label_civilite' => 'Pohlavie',
	'label_date_creation' => 'Dátum vytvorenia',
	'label_date_naissance' => 'Dátum narodenia',
	'label_descriptif' => 'Popis',
	'label_email' => 'E-mail',
	'label_fonction' => 'Funkcia',
	'label_identification' => 'Identifikácia',
	'label_nom' => 'Meno',
	'label_nom_organisation' => 'Organizácia',
	'label_organisation' => 'Prepojená organizácia',
	'label_organisation_parente' => 'Nadradená organizácia',
	'label_prenom' => 'Krstné meno',
	'label_prenom_nom' => 'Meno a priezvisko',
	'label_pseudo' => 'Prezývka',
	'label_recherche_auteurs' => 'Vyhľadať v autoroch',
	'label_recherche_contacts' => 'Vyhľadať v kontaktoch',
	'label_recherche_organisations' => 'Vyhľadať v organizáciách',
	'label_statut_juridique' => 'Právne postavenie',
	'label_telephone' => 'Telefón',
	'label_type_liaison' => 'Odkaz',
	'lier_ce_contact' => 'Pripojiť tento kontakt',
	'lier_cet_auteur' => 'Odkaz',
	'lier_cette_organisation' => 'Pripojiť k tejto organizácii',
	'lier_contact' => 'Pripojiť kontakt',
	'lier_organisation' => 'Pripojiť k organizácii',
	'liste_contacts' => 'Zoznam všetkých kontaktov v databáze',
	'liste_organisations' => 'Zoznam všetkých organizácií v databáze',

	// N
	'nb_contact' => '1 kontakt',
	'nb_contacts' => '@nb@ kontaktov',
	'nom_contact' => 'Meno',
	'nom_organisation' => 'Názov',

	// O
	'organisation' => 'Spoločnosť',
	'organisation_ajouter' => 'Pridať spoločnosť',
	'organisation_ajouter_lien' => 'Pridať túto spoločnosť',
	'organisation_associe_a_auteur_numero' => 'Prepojené s autorom číslo',
	'organisation_associer_a_auteur' => 'Prepojiť s autorom',
	'organisation_aucun' => 'Žiadna spoločnosť',
	'organisation_creer' => 'Zapísať organizáciu',
	'organisation_creer_associer' => 'Vytvoriť a vytvoriť prepojiť so spoločnosťou',
	'organisation_creer_fille' => 'Zapísať podradenú organizáciu',
	'organisation_editer' => 'Upraviť túto organizáciu',
	'organisation_logo' => 'Logo spoločnosti',
	'organisation_nouveau_titre' => 'Nová spoločnosť',
	'organisation_numero' => 'Organizácia číslo',
	'organisation_retirer_lien' => 'Odstrániť spoločnosť',
	'organisation_retirer_tous_lien' => 'Odstrániť všetky spoločnosti',
	'organisation_un' => 'Jedna organizácia',
	'organisation_voir' => 'Zobraziť',
	'organisations' => 'Organizácie',
	'organisations_nb' => '@nb@ organizácií',

	// P
	'prenom' => 'Krstné meno',

	// R
	'recherche_de' => 'Vyhľadať "@recherche@"',
	'rechercher' => 'Vyhľadať',

	// S
	'statut_juridique' => 'Právna forma',
	'supprimer_contact' => 'Odstrániť tento kontakt',
	'supprimer_organisation' => 'Odstrániť túto organizáciu',

	// T
	'titre_contact' => 'Kontaktné údaje',
	'titre_organisation' => 'Informácie o organizácii',
	'titre_page_configurer_contacts_et_organisations' => 'Nastaviť Kontakty a spoločnosti',
	'titre_page_contacts' => 'Riadenie kontaktov',
	'titre_page_organisations' => 'Riadenie organizácií',
	'titre_page_repertoire' => 'Adresár',
	'titre_parametrages' => 'Nastavenie parametrov',
	'tous' => 'Všetko'
);

?>
