<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/contacts?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_contact' => 'ارتباطي برقرار نيست!',
	'aucune_organisation' => 'سازماني موجود نيست!',
	'auteur_lie' => 'Id de l\'auteur lié', # NEW

	// B
	'bouton_contacts' => 'تماس‌ها',
	'bouton_contacts_organisations' => 'تماس‌ها & سازمان‌ها',
	'bouton_organisations' => 'سازمان‌ها',
	'bouton_rechercher' => 'جستجو',
	'bouton_repertoire' => 'دايركتوري',

	// C
	'cfg_activer_squelettes_publics_zpip1' => 'Squelettes publics ZPIP v1', # NEW
	'cfg_activer_squelettes_publics_zpip1_explication' => 'Activer les squelettes publics pour ZPIP en version 1,
		permettant le parcourir les contacts et organisations dans l\'espace public ?', # NEW
	'cfg_afficher_infos_sur_auteurs' => 'Affichage détaillé des auteurs ?', # NEW
	'cfg_afficher_infos_sur_auteurs_explication' => 'Afficher les infos de contact ou d\'organisation
		également sur les pages auteurs dans l\'espace privé ?', # NEW
	'cfg_associer_aux_auteurs' => 'Associer aux auteurs ?', # NEW
	'cfg_associer_aux_auteurs_explication' => 'Permettre d\'associer des contacts ou organisations
		aux auteurs. Cela ajoute un formulaire pour associer un auteur sur les pages contact ou organisation,
		et inversement cela ajoute un formulaire pour lier un contact ou une organisation sur les pages des auteurs.', # NEW
	'cfg_lier_organisations_rubriques' => 'Lier aux rubriques', # NEW
	'cfg_lier_organisations_rubriques_explication' => 'Permettre de lier les organisations aux rubriques ?
		Cela affiche le sélecteur d\'organisations sur les rubriques ainsi que le sélecteur de rubriques sur les organisations.', # NEW
	'changer' => 'تغيير',
	'chercher_contact' => 'جستجو',
	'chercher_organisation' => 'جستجو',
	'chercher_statut' => 'رتبه',
	'confirmer_delier_contact' => 'مطمئن هستيد كه مي‌خواهيد پيوند اين سازمان و اين ارتباط را قطع كنيد؟',
	'confirmer_delier_organisation' => 'مطمئن هستيد كه مي‌‌خواهيد پيوند با اين سازمان را قطع كنيد؟',
	'confirmer_delier_organisation_rubrique' => 'مطمئن هستيد كه مي‌‌خواهيد ارتباط اين سازمان با اين بخش را قطع كنيد؟‌',
	'confirmer_supprimer_contact' => 'مطمئن هستيد كه مي‌خواهيد تمام اطلاعات مربوط يه اين ارتباط را حذف كنيد؟‌',
	'confirmer_supprimer_organisation' => 'مطمئن هستيد كه مي‌خواهيد تمام اطلاعات مربوط به اين سازمان را حذف كنيد؟‌',
	'contact' => 'يك تماس', # MODIF
	'contact_ajouter' => 'Ajouter un contact', # NEW
	'contact_ajouter_lien' => 'Ajouter ce contact', # NEW
	'contact_associe_a_auteur_numero' => 'پيوند به نويسنده‌ي شماره‌ي ',
	'contact_associer_a_auteur' => 'پيوند به يك نويسنده',
	'contact_aucun' => 'Aucun contact', # NEW
	'contact_creer' => 'ايجاد يك تماس',
	'contact_creer_associer' => 'Créer et associer un contact', # NEW
	'contact_editer' => 'ويرايش اين تماس',
	'contact_logo' => 'Logo de ce contact', # NEW
	'contact_nouveau_titre' => 'Nouveau contact', # NEW
	'contact_numero' => 'تماس شماره‌ي',
	'contact_retirer_lien' => 'Retirer le contact', # NEW
	'contact_retirer_tous_lien' => 'Retirer tous les contacts', # NEW
	'contact_un' => 'Un contact', # NEW
	'contact_voir' => 'بنگريد',
	'contacts' => 'تماس‌ها',
	'contacts_nb' => '@nb@ تماس ',
	'creer_auteur_contact' => 'ايجاد يك نويسنده و پيوند او با اين تماس ',
	'creer_auteur_organisation' => 'ايجاد يك نويسنده و پيوند او به اين سازمان',

	// D
	'definir_auteur_comme_contact' => 'تعريف به عنوان تماس ',
	'definir_auteur_comme_organisation' => 'تعريف به عنوان سازمان',
	'delier_cet_auteur' => 'قطع',
	'delier_contact' => 'Désassocier', # NEW
	'delier_organisation' => 'Désassocier', # NEW

	// E
	'est_un_contact' => 'اين نويسنده به عنوان يك تماس تعريف مي‌شود.',
	'est_une_organisation' => 'اين نويسنده به عنوان يك سازمان تعريف مي‌شود.',
	'explication_activite' => 'فعاليت سازمان:‌ بشردوستانه، فناوري اطلاعات، چاپ . . . ',
	'explication_contacts_ou_organisations' => 'مي‌توانيد اين نويسنده را به عنوان يك تماس يا يك سازمان تعريف كنيد.ميدان‌هاي اضافي مي‌توانند بعداً از صفحه‌ي تكميلي اطلاعات نويسنده پر شوند.',
	'explication_identification' => 'هويت سازمان:‌ ',
	'explication_statut_juridique' => 'سازمان، انجمن، شركت . . . ',
	'explication_supprimer_contact' => 'حذف اين تماس تمام اطلاعات اضافي را كه در صفحه‌ي نويسنده پر شده حذف مي‌كند. ',
	'explication_supprimer_organisation' => 'حذف اين سازمان تمام اطلاعات اضافي را كه در صفحه‌ي نويسنده پر شده حذف خواهد كرد. ',
	'explications_page_contacts' => 'Page en cours de développement. <br /><br />Actions envisagées :<ul>
	<li>voir tous les contacts</li><li>transformer les auteurs en contacts</li><li>importer des contacts</li><li>...</li></ul><br />Merci pour vos suggestions sur <a href="http://www.spip-contrib.net/Plugin-Contacts-Organisations#pagination_comments-list">le forum</a> ;-)', # NEW
	'explications_page_organisations' => 'Page en cours de développement. <br /><br />Actions envisagées :<ul>
	<li>voir toutes les organisations</li><li>transformer des auteurs en organisations</li><li>importer des organsations</li><li>...</li></ul><br />Merci pour vos suggestions sur <a href="http://www.spip-contrib.net/Plugin-Contacts-Organisations#pagination_comments-list">le forum</a> ;-)', # NEW

	// I
	'info_contacts_organisation' => 'تماس‌هاي سازمان',
	'info_nb_contacts' => 'تماس‌هاي پيوند شده',
	'info_organisation_appartenance' => 'تعلق سازمان',
	'info_organisations_appartenance' => 'تعلق سازمان‌ها',
	'info_organisations_filles' => 'پرونده‌هاي سازمان‌ها',
	'info_organisations_meres' => 'سازمان‌هاي مادر',
	'info_tous_contacts' => 'تمام تماس‌ها',
	'info_toutes_organisations' => 'تمام سازمان‌ها',
	'infos_contacts_ou_organisations' => 'تماس‌ها & سازمان‌‌ها',

	// L
	'label_activite' => 'فعاليت',
	'label_civilite' => 'ادب',
	'label_date_creation' => 'تاريخ ايجاد',
	'label_date_naissance' => 'تاريخ تولد',
	'label_descriptif' => 'شرح',
	'label_email' => 'ايميل',
	'label_fonction' => 'كاركرد',
	'label_identification' => 'هويت',
	'label_nom' => 'نام',
	'label_nom_organisation' => 'سازمان',
	'label_organisation' => 'سازمان وابسته',
	'label_organisation_parente' => 'سازمان مادر',
	'label_prenom' => 'اسم كوچك',
	'label_prenom_nom' => 'اسم كوچك + اسم فاميل',
	'label_pseudo' => 'اسم مستعار',
	'label_recherche_auteurs' => 'جستجو در ميان نويسندگان',
	'label_recherche_contacts' => 'جستجو در ميان تماس‌ها',
	'label_recherche_organisations' => 'جستجو در ميان سازمان‌ها',
	'label_statut_juridique' => 'وضعيت',
	'label_telephone' => 'تلفن.',
	'label_type_liaison' => 'ارتباط',
	'lier_ce_contact' => 'الحاق اين تماس',
	'lier_cet_auteur' => 'پيوند',
	'lier_cette_organisation' => 'الحاق به اين سازمان',
	'lier_contact' => 'الحاق به اين تماس ',
	'lier_organisation' => 'الحاق به يك سازمان',
	'liste_contacts' => 'ليست تمام تماس‌ها',
	'liste_organisations' => 'ليست تمام سازمان‌ها',

	// N
	'nb_contact' => 'تماس 1',
	'nb_contacts' => '@nb@ تماس ',
	'nom_contact' => 'Nom', # NEW
	'nom_organisation' => 'Nom', # NEW

	// O
	'organisation' => 'Organisation', # NEW
	'organisation_ajouter' => 'Ajouter une organisation', # NEW
	'organisation_ajouter_lien' => 'Ajouter cette organisation', # NEW
	'organisation_associe_a_auteur_numero' => 'ارتباط با شماره‌ي نويسنده',
	'organisation_associer_a_auteur' => 'ارتباط با يك نويسنده',
	'organisation_aucun' => 'Aucune organisation', # NEW
	'organisation_creer' => 'ايجاد يك سازمان',
	'organisation_creer_associer' => 'Créer et associer une organisation', # NEW
	'organisation_creer_fille' => 'ايجاد يك پرونده‌ي سازمان',
	'organisation_editer' => 'ويرايش اين سازمان',
	'organisation_logo' => 'Logo de l\'organisation', # NEW
	'organisation_nouveau_titre' => 'Nouvelle organisation', # NEW
	'organisation_numero' => 'شماره‌ي سازمان',
	'organisation_retirer_lien' => 'Retirer l\'organisation', # NEW
	'organisation_retirer_tous_lien' => 'Retirer toutes les organisations', # NEW
	'organisation_un' => 'يك سازمان',
	'organisation_voir' => 'بنگريد',
	'organisations' => 'سازمان‌ها',
	'organisations_nb' => '@nb@ سازمان',

	// P
	'prenom' => 'Prénom', # NEW

	// R
	'recherche_de' => ' جستجوي «@recherche@»',
	'rechercher' => 'جستجو',

	// S
	'statut_juridique' => 'Statut juridique', # NEW
	'supprimer_contact' => 'حذف اين تماس ',
	'supprimer_organisation' => 'حذف اين سازمان',

	// T
	'titre_contact' => 'حزئيات تماس',
	'titre_organisation' => 'جزئيات سازمان',
	'titre_page_configurer_contacts_et_organisations' => 'Configurer Contacts & Organisations', # NEW
	'titre_page_contacts' => 'مديريت تماس‌ها',
	'titre_page_organisations' => 'مديريت سازمان‌ها',
	'titre_page_repertoire' => 'ديركتور',
	'titre_parametrages' => 'Paramétrages', # NEW
	'tous' => 'Tous' # NEW
);

?>
