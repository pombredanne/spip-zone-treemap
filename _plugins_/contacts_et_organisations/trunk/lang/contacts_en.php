<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/contacts?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_contact' => 'There is no contact !',
	'aucune_organisation' => 'There is no organization',
	'auteur_lie' => 'Id de l\'auteur lié', # NEW

	// B
	'bouton_contacts' => 'Contacts',
	'bouton_contacts_organisations' => 'Contacts & Organizations',
	'bouton_organisations' => 'Organisations',
	'bouton_rechercher' => 'Search',
	'bouton_repertoire' => 'Directory',

	// C
	'cfg_activer_squelettes_publics_zpip1' => 'Squelettes publics ZPIP v1', # NEW
	'cfg_activer_squelettes_publics_zpip1_explication' => 'Activer les squelettes publics pour ZPIP en version 1,
		permettant le parcourir les contacts et organisations dans l\'espace public ?', # NEW
	'cfg_afficher_infos_sur_auteurs' => 'Affichage détaillé des auteurs ?', # NEW
	'cfg_afficher_infos_sur_auteurs_explication' => 'Afficher les infos de contact ou d\'organisation
		également sur les pages auteurs dans l\'espace privé ?', # NEW
	'cfg_associer_aux_auteurs' => 'Associer aux auteurs ?', # NEW
	'cfg_associer_aux_auteurs_explication' => 'Permettre d\'associer des contacts ou organisations
		aux auteurs. Cela ajoute un formulaire pour associer un auteur sur les pages contact ou organisation,
		et inversement cela ajoute un formulaire pour lier un contact ou une organisation sur les pages des auteurs.', # NEW
	'cfg_lier_organisations_rubriques' => 'Lier aux rubriques', # NEW
	'cfg_lier_organisations_rubriques_explication' => 'Permettre de lier les organisations aux rubriques ?
		Cela affiche le sélecteur d\'organisations sur les rubriques ainsi que le sélecteur de rubriques sur les organisations.', # NEW
	'changer' => 'Change',
	'chercher_contact' => 'Search',
	'chercher_organisation' => 'Search',
	'chercher_statut' => 'Status',
	'confirmer_delier_contact' => 'Are you sure you want to unlink this organisation from this contact?',
	'confirmer_delier_organisation' => 'Are you sure you want to unlink this contact from this organisation?',
	'confirmer_delier_organisation_rubrique' => 'Are you sure you want to unlink this organization from this section?',
	'confirmer_supprimer_contact' => 'Ares you sure you want to delete all information about this contact?',
	'confirmer_supprimer_organisation' => 'Are you sure you want to delete all the information about this organisation?',
	'contact' => 'One contact', # MODIF
	'contact_ajouter' => 'Ajouter un contact', # NEW
	'contact_ajouter_lien' => 'Ajouter ce contact', # NEW
	'contact_associe_a_auteur_numero' => 'Link to author number',
	'contact_associer_a_auteur' => 'Link to an author',
	'contact_aucun' => 'Aucun contact', # NEW
	'contact_creer' => 'Create a contact',
	'contact_creer_associer' => 'Créer et associer un contact', # NEW
	'contact_editer' => 'Edit this contact',
	'contact_logo' => 'Logo de ce contact', # NEW
	'contact_nouveau_titre' => 'Nouveau contact', # NEW
	'contact_numero' => 'Contact number',
	'contact_retirer_lien' => 'Retirer le contact', # NEW
	'contact_retirer_tous_lien' => 'Retirer tous les contacts', # NEW
	'contact_un' => 'Un contact', # NEW
	'contact_voir' => 'See',
	'contacts' => 'Contacts',
	'contacts_nb' => '@nb@ contacts',
	'creer_auteur_contact' => 'Create a new author and link it to this contact',
	'creer_auteur_organisation' => 'Create a new author and link it to this organization',

	// D
	'definir_auteur_comme_contact' => 'Set as contact',
	'definir_auteur_comme_organisation' => 'Set as organisation',
	'delier_cet_auteur' => 'Unlink',
	'delier_contact' => 'Désassocier', # NEW
	'delier_organisation' => 'Désassocier', # NEW

	// E
	'est_un_contact' => 'This author is set as a contact.',
	'est_une_organisation' => 'This author is set as an organization.',
	'explication_activite' => 'Activity of the organization : NGO, education, edition...',
	'explication_contacts_ou_organisations' => 'You can set this author as a contact or as an organization.Vous pouvez définir cet auteur
  Additional fields can be then filled from the author modification page.',
	'explication_identification' => 'Organisation identification : VAT, etc.',
	'explication_statut_juridique' => 'company, organisation, ...',
	'explication_supprimer_contact' => 'Deleting this contact will remove all the additional informations which have been filled on the author page.',
	'explication_supprimer_organisation' => 'Deleting this organization will remove all the additional informations which have been filled on the author page.',
	'explications_page_contacts' => 'Page en cours de développement. <br /><br />Actions envisagées :<ul>
 <li>voir tous les contacts</li><li>transformer les auteurs en contacts</li><li>importer des contacts</li><li>...</li></ul><br />Merci pour vos suggestions sur <a href="http://www.spip-contrib.net/Plugin-Contacts-Organisations#pagination_comments-list">le forum</a> ;-)', # NEW
	'explications_page_organisations' => 'Page en cours de développement. <br /><br />Actions envisagées :<ul>
 <li>voir toutes les organisations</li><li>transformer des auteurs en organisations</li><li>importer des organsations</li><li>...</li></ul><br />Merci pour vos suggestions sur <a href="http://www.spip-contrib.net/Plugin-Contacts-Organisations#pagination_comments-list">le forum</a> ;-)', # NEW

	// I
	'info_contacts_organisation' => 'Organisation\'s contacts',
	'info_nb_contacts' => 'Linked contacts',
	'info_organisation_appartenance' => 'Belonging Organisation',
	'info_organisations_appartenance' => 'Membership organizations',
	'info_organisations_filles' => 'Children organizations',
	'info_organisations_meres' => 'Parents organizations',
	'info_tous_contacts' => 'All the contacts',
	'info_toutes_organisations' => 'All the organisations',
	'infos_contacts_ou_organisations' => 'Contacts & Organisations',

	// L
	'label_activite' => 'Activity',
	'label_civilite' => 'Gender',
	'label_date_creation' => 'Date of creation',
	'label_date_naissance' => 'Date of birth',
	'label_descriptif' => 'Description',
	'label_email' => 'Email',
	'label_fonction' => 'Function',
	'label_identification' => 'Identification',
	'label_nom' => 'Name',
	'label_nom_organisation' => 'Organisation',
	'label_organisation' => 'Linked organisation',
	'label_organisation_parente' => 'Parent organization',
	'label_prenom' => 'First name',
	'label_prenom_nom' => 'First + last name',
	'label_pseudo' => 'Pseudo',
	'label_recherche_auteurs' => 'Search in authors',
	'label_recherche_contacts' => 'Search in contacts',
	'label_recherche_organisations' => 'Search in organisations',
	'label_statut_juridique' => 'Status',
	'label_telephone' => 'Phone',
	'label_type_liaison' => 'Link',
	'lier_ce_contact' => 'Attach this contact',
	'lier_cet_auteur' => 'Link',
	'lier_cette_organisation' => 'Attach to this organisation',
	'lier_contact' => 'Attach a contact',
	'lier_organisation' => 'Attach to an organisation',
	'liste_contacts' => 'List of all contacts in the database',
	'liste_organisations' => 'List of all organisations in the database',

	// N
	'nb_contact' => '1 contact',
	'nb_contacts' => '@nb@ contacts',
	'nom_contact' => 'Nom', # NEW
	'nom_organisation' => 'Nom', # NEW

	// O
	'organisation' => 'Organisation', # NEW
	'organisation_ajouter' => 'Ajouter une organisation', # NEW
	'organisation_ajouter_lien' => 'Ajouter cette organisation', # NEW
	'organisation_associe_a_auteur_numero' => 'Linked to author number',
	'organisation_associer_a_auteur' => 'Link to an author',
	'organisation_aucun' => 'Aucune organisation', # NEW
	'organisation_creer' => 'Create an organization',
	'organisation_creer_associer' => 'Créer et associer une organisation', # NEW
	'organisation_creer_fille' => 'Create a child organization',
	'organisation_editer' => 'Edit this organizatino',
	'organisation_logo' => 'Logo de l\'organisation', # NEW
	'organisation_nouveau_titre' => 'Nouvelle organisation', # NEW
	'organisation_numero' => 'Organization number',
	'organisation_retirer_lien' => 'Retirer l\'organisation', # NEW
	'organisation_retirer_tous_lien' => 'Retirer toutes les organisations', # NEW
	'organisation_un' => 'One organization',
	'organisation_voir' => 'See',
	'organisations' => 'Organisations',
	'organisations_nb' => '@nb@ organizations',

	// P
	'prenom' => 'Prénom', # NEW

	// R
	'recherche_de' => 'Search for "@recherche@"',
	'rechercher' => 'Search',

	// S
	'statut_juridique' => 'Statut juridique', # NEW
	'supprimer_contact' => 'Delete this contact',
	'supprimer_organisation' => 'Delete this organisation',

	// T
	'titre_contact' => 'Contact details',
	'titre_organisation' => 'Organization details',
	'titre_page_configurer_contacts_et_organisations' => 'Configurer Contacts & Organisations', # NEW
	'titre_page_contacts' => 'Contacts management',
	'titre_page_organisations' => 'Organisations management',
	'titre_page_repertoire' => 'Directory',
	'titre_parametrages' => 'Paramétrages', # NEW
	'tous' => 'Tous' # NEW
);

?>
