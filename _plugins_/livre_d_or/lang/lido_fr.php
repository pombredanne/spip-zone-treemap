<?php

	// lang/lido_fr.php
	
	// $LastChangedRevision$
	// $LastChangedBy$
	// $LastChangedDate$

$GLOBALS['i18n_'._LIDO_PREFIX.'_fr'] = array(

  'livre_dor' => "Livre d&acute;or"

, 'configuration_livre_dor' => "Configuration du livre d&acute;or"
, 'configuration_livre_dor_desc' => "Les pages du livre d&acute;or peuvent &ecirc;tre sauvegard&eacute;es sous forme d'articles 
   ou de br&egrave;ves. Elles seront post&eacute;es dans la rubrique pr&eacute;cis&eacute;e 
   ci-dessous. <br />
	Si vous choisissez de sauvegarder les pages sous forme d'articles, vous pouvez &eacute;galement attribuer un auteur aux pages du livre 
   d'or (conseill&eacute;)."
, 'breves_non_activees' => "L'utilisation des br&egrave;ves est d&eacute;sactiv&eacute;e pour ce site. 
   Si vous d&eacute;sirez archiver les pages du livre d&acute;or sous forme de br&egrave;ves, 
   vous devez activer leur utilisation <a href='/ecrire/?exec=configuration'>via 
   l&acute;interface de configuration du site."
, 'configurer_article_breve' => "Article ou br&egrave;ve"
, 'configurer_article_breve_desc_' => "Sauvegarder les pages sous forme : "
, 'd_article' => "d&acute;articles"
, 'de_breve' => "de br&egrave;ves."
, 'pas_de_rubriques' => "Votre site n&acute;a pour l'instant aucune rubrique. Cr&eacute;ez une 
   rubrique avant de configurer le livre d&acute;or."
, 'selectionnez_la_rubrique_' => "S&eacute;lectionnez la rubrique de destination des pages du livre d&acute;or : "
, 'rubrique_destination' => "Rubrique de destination"
, 'mode_publication' => "Mode de publication du livre d&acute;or"
, 'prevenir_moderateur' => "Pr&eacute;venir le mod&eacute;rateur"
, 'prevenir_moderateur_desc' => "Lorsqu&acute;une nouvelle page est propos&eacute;e &agrave; publication, le mod&eacute;rateur 
   peut en &ecirc;tre averti par e-mail."
, 'ne_pas_prevenir' => "Ne pas pr&eacute;venir le mod&eacute;rateur"
, 'prevenir' => "Pr&eacute;venir le mod&eacute;rateur..."
, 'indiquez_email_' => "Indiquez ici son adresse e-mail : "
, 'indiquez_tag_' => "Le tag ci-dessous sera inclus automatiquement en d&eacute;but du sujet du mail : "
, 'attribuer_auteur' => "Attribuer les pages du livre d&acute;or"
, 'attribuer_auteur_desc' => "Vous pouvez attribuer les pages du livre d&acute;or &agrave; un auteur."
, 'auteur_aucun' => "aucun"

, 'votre_commentaire_' => "Ajoutez votre commentaire au livre d&acute;or : "
, 'entrez_texte' => "Entrez votre texte dans ce champ : "
, 'votre_signature_' => "Votre signature : "
, 'previsualiser_commentaire' => "Pr&eacute;visualiser mon commentaire"
, 'desole' => "D&eacute;sol&eacute;. "
, 'texte_trop_long' => "Votre texte est trop long. "
, 'texte_trop_court' => "Votre texte est trop court. "
, 'merci_corriger' => "Merci de le corriger avant envoi."
, 'commentaire_envoye' => "Votre commentaire vient d'&ecirc;tre envoy&eacute; au livre d&acute;or.<br />"
, 'commentaire_modere' => "Il sera valid&eacute; prochainement.<br />"
, 'commentaire_merci' => "Merci de votre participation."
, 'commentaire_poste' => "Un commentaire vient d'&ecirc;tre post&eacute; dans le livre d'or du site @nom_site@."
, 'commentaire_a_valider' => "Vous devez le valider pour le faire appara&icirc;tre en espace public du site."
, 'commentaire_contenu_' => "Contenu du commentaire : "

); //

?>