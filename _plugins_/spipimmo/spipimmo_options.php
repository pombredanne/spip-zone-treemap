<?php
/**
* Plugin SPIP-Immo
*
* @author: CALV V3
* @author: Pierre KUHN V4
*
* Copyright (c) 2007-12
* Logiciel distribue sous licence GPL.
*
**/

	//Repertoire IMG PACK
	define("_DIR_PLUGIN_SPIPIMMO_IMG_PACK", _DIR_PLUGIN_SPIPIMMO."img_pack/");

	//R�pertoire des vignettes
	define("_SPIPIMMO_REP_VIGNETTES", _DIR_IMG  . "cache100xXXX/");

	//Nombre de caract�re pour le num�ro d'un dossier
	define("_SPIPIMMO_DOSSIER_NBCAR", 7);

	//Nombre de r�sultat par page (A ne pas modifier)
	define("_SPIPIMMO_PAGE_NBRES", 10);
?>
