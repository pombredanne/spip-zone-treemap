<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'spipimmo_description' => 'Plugin à destination des agences immobilières souhaitant publier facilement leurs annonces en ligne.',
	'spipimmo_slogan' => 'Publier vos annonces immobilières en ligne.',
);
?>
