<?php

if (!defined('_ECRIRE_INC_VERSION')) return;
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
$GLOBALS[$GLOBALS['idx_lang']] = array(

	//A
	'acces_handicape' => 'Accès handicapé',
	'adresse' => 'Adresse',
	'annee_construction' => 'Année de construction',
	'annonce' => 'Annonce',
	'ascenseur' => 'Ascenseur',
	//C
	'charge' => 'Charge',
	'code_postal' => 'Code postal',
	'creer_annonce' => 'Créer une annonce',
	'creer_proprietaire' => 'Créer un propriétaire',
	//D
	'dpe' => 'DPE',
	'date_annonce' => 'Date de l\'annonce',
	'date_dispo' => 'Date de disponibilité',
	'date_modif' => 'Date de modification',
	'depot_garantie' => 'Dépot de garantie',
	//E
	'etage' => 'Etage',
	'etage_code' => 'Etage code',
	'exclusif' => 'Exclusif',
	//H
	'honoraires' => 'Honoraires',
	//L
	'liste_annonces' => 'Liste des annonces',
	'location' => 'Location',
	//N
	'nature_chauffage' => 'Nature de chauffage',
	'nbr_balcon' => 'Nombre de balcon',
	'nbr_cave' => 'Nombre de cave',
	'nbr_chambre' => 'Nombre de chambre',
	'nbr_etage' => 'Nombre d\'étage',
	'nbr_garage' => 'Nombre de garage',
	'nbr_mur_mitoyen' => 'Nombre de mur mitoyen',
	'nbr_park_ext' => 'Nombre de parking extérieur',
	'nbr_park_int' => 'Nombre de parking intérieur',
	'nbr_piece' => 'Nombre de pièce',
	'nbr_sdb' => 'Nombre de salle de bain',
	'nbr_sde' => 'Nombre de salle d\'eau',
	'nbr_terrasse' => 'Nombre de terrasse',
	'nbr_wc' => 'Nombre de WC',
	'negociateur' => 'Négociateur',
	'numero_mandat' => 'Numéro de mandat',
	//P
	'piscine' => 'Piscine',
	'prestige' => 'Prestige',
	'privilège' => 'Privilège',
	'prix_loyer' => 'Prix loyer',
	'proximite' => 'Proximité',
	'publier' => 'Publier',
	'quartier' => 'Quartier',
	//R
	'residence' => 'Résidence',
	//S
	'secteur' => 'Secteur',
	'semi_privilege' => 'Semi-privilège',
	'spipimmo' => 'SPIP-Immo',
	'surface_facade' => 'Surface de facade',
	'surface_habitable' => 'Surface habitable',
	'surface_sejour' => 'Surface séjour',
	'surface_terrain' => 'Surface terrain',
	'surface_total' => 'Surface total',
	//T
	'taxe_fonciere' => 'Taxe foncière',
	'taxe_habitation' => 'Taxe d\'habitation',
	'transport' => 'Transport',
	'travaux' => 'Travaux',
	'type_chauffage' => 'Type de chauffage',
	'type_cuisine' => 'Type de cuisine',
	'type_mandat' => 'Type de mandat',
	'type_offre' => 'Type d\'offre',
	'type_sous_sol' => 'Type de sous sol',
	//V
	'vente' => 'Vente',
	'vente_location' => 'Vente/Location',
	'ville' => 'Ville',

);

?>
