<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'albums_nom' => "Albums",
	'albums_slogan' => "Grouper vos documents sous forme d'albums",
	'albums_description' => "<ins>ATTENTION, VERSION EN DEVELOPPEMENT !</ins>. 
	Veillez à consulter <a href='http://contrib.spip.net/Albums-v2-DEV'>la documentation provisoire</a> avant installation ou mise à jour.

	Ce plugin ajoute un nouvel objet 'album' que l'on peut lier à n'importe quel autre objet éditorial.
	Les albums sont des conteneurs pour documents, ils vous permettent par exemple de séparer
	les documents d'un articles en plusieurs groupes, de les insérer au fil du texte,
	ou indépendamment dans vos squelettes."

);

?>
