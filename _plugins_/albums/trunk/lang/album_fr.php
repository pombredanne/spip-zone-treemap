<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'avis_suppression_album'	=> "L'album va être supprimé, cette action est irréversible. Êtes vous sûr ?",

	// B
	'bouton_dissocier'		=> "Dissocier",
	'bouton_associer'		=> "Associer",
	'bouton_supprimer'		=> "Supprimer définitivement",

	// C
	'cfg_titre_albums'		=> "Albums",
	'c_albums_info'			=> "Gestion des albums",
	'c_albums_info_texte'		=> 
		"<div class='albums'>
		Vous pouvez sélectionner les albums selon plusieurs critères :
		<ul>
		<li><strong>1er rang:</strong> objets auxquels les albums sont liés.</li>
		<li><strong>2ème rang:</strong>
			<ul>
			<li>statuts des albums</li>
			<li>vus = albums insérés dans un texte.</li>
			<li>inutilisés = albums sans lien avec un objet éditorial.</li>
			</ul>
		</li>
		<li><strong>3ème rang:</strong> types de médias contenus (plusieurs choix simultanés possibles).</li>
		</ul>
		<p>Les albums ont 3 statuts : non publié (prepa), publié et à la poubelle. La puce à gauche permet de le changer rapidement.
		Si un album est à la poubelle, vide et inutilisé, une icône apparaît à droite qui permet de le supprimer définitivement.</p>
		<p><a href='http://contrib.spip.net/Albums-v2'>Documentation en ligne</a></p>
		</div>",

	// E
	'explication_affichage_modele'		=> "La pérennité de ces options n'est pas garantie !",

	// F
	'filtre_tous'				=> "Tous",
	'filtre_vus'				=> "Vus",
	'filtre_non_vus'			=> "Non vus",
	'filtre_albums_vides'			=> "Albums vides",
	'filtre_albums_non_vides'		=> "Albums non vides",

	// H
	'heading_logo'				=> "Logo",
	'heading_id'				=> "ID",
	'heading_infos'				=> "Infos",
	'heading_titre'				=> "Titre",
	'heading_contenu'			=> "Contenu",
	'heading_liaison'			=> "Liaison",
	'heading_descriptif'			=> "Description",

	// I
	'icone_ajouter_album'			=> "Ajouter un album",
	'icone_creer_album'			=> "Créer un nouvel album",
	'icone_associer_album'			=> "Associer un album existant",
	'icone_modifier_album'			=> "Modifier cet album",
	'icone_delier_album'			=> "Enlever cet album",
	'icone_fermer'				=> "Fermer",
	'info_aucun_album'			=> "Aucun album",
	'info_aucun_album_supplementaire'	=> "Aucun album supplémentaire",
	'info_aucun_album_criteres'		=> "Aucun album ne correspond à ces critères",
	'info_1_album'				=> "1 album",
	'info_nb_albums'			=> "@nb@ albums",
	'info_album_vide'			=> "Album vide",
	'info_statut_modele'			=> "Album non publié",
	'info_documents_joints_albums'		=> "Veillez à activer l'ajout de documents-joints aux albums",
	'info_aucun_album_vide'			=> "Aucun album vide",
	'info_dans_objet'			=> "dans des",
	'info_association'			=> "Associé à",
	'info_auteur'				=> "Vous êtes l'auteur",
	'info_nouvel_album' => 'Nouvel album',
	'info_recherche_album_zero'		=> "Aucun résultat pour « @cherche_album@ »",
	'info_resultat_recherche_aucun'		=> 'Aucun résultat',
	'info_resultat_recherche_un'		=> '1 résultat',
	'info_resultat_recherche_plus'		=> '@nb@ résultats',

	// L
	'label_fieldset_objets_albums'		=> "Objets auxquels les albums peuvent être associés",
	'label_activer_album_objets'		=> "Activer les albums pour les contenus :",
	'label_fieldset_champs'			=> "Champs du formulaire d'édition",
	'label_afficher_descriptif'		=> "Descriptif",
	'label_case_utiliser_titre_defaut'	=> "Par défaut, assigner le titre de l'objet parent",
	'label_utiliser_titre_defaut'		=> "Titre",
	'label_fieldset_affichage_modele'	=> "Modèles : informations optionnelles",
	'label_vue_icones'			=> "Vue Aperçu",
	'label_vue_liste'			=> "Vue Liste",
	'label_vue_icones_titre'		=> "Titre",
	'label_vue_liste_icone'			=> "Icône",
	'label_vue_liste_mimetype'		=> "Mimetype",
	'label_vue_liste_poids'			=> "Poids",
	'label_vue_liste_dimensions'		=> "Dimensions <small>(images)</small>",
	'label_titre'				=> "Titre",
	'label_categorie'			=> "Catégorie",
	'label_descriptif'			=> "Descriptif",

	// O
	'onglet_tous'				=> "Tous",
	'onglet_creer_album'			=> "Créer",
	'onglet_associer_album'			=> "Associer",

	// T
	'titre_album'				=> "Album",
	'titre_albums'				=> "Albums",
	'titre_page_configurer_albums'		=> "Configurer les Albums",
	'titre_logo_album'			=> "Logo",
	'texte_statut_prepa'			=> "non publié",
	'texte_statut_publie'			=> "publié en ligne",
	'texte_statut_poubelle'			=> "à la poubelle",
	'texte_creer_album'			=> "Créer un nouvel album",
	'texte_ajouter_album'			=> "Ajouter un album",
	'texte_associer_album'			=> "Associer un album existant",
	'texte_modifier'			=> "Modifier",
	'texte_changer_statut'			=> "Modifier le statut",

	// U
	'un_image'				=> "image",
	'un_audio'				=> "bande-son",
	'un_video'				=> "vidéo",
	'un_file'				=> "document"

);

?>
