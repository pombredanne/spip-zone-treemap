<?php
/**
 * @name 		JavascriptPopup_lang_FR
 * @author 		Piero Wbmstr <piero.wbmstr@gmail.com>
 */

$GLOBALS[$GLOBALS['idx_lang']] = array(

// Description
	'nom' => 'Balise &#035;POPUP',

// CFG
	'titre_descr_cfg' => 'Configuration de l\'outil "Popup"',
	'descr_cfg' => 'Documentation du plugin pour plus d\'infos : [spip-contrib.net/?article3573->http://www.spip-contrib.net/?article3573]',
	'legend_cfg_balise' => 'Concernant la fen&ecirc;tre externe',
	'skel_defaut' => 'Squelette par d&eacute;faut utilis&eacute; pour afficher le contenu de la fen&ecirc;tre',
	'skel_defaut_comment' => 'Il s\'agit initialement du squelette "popup_defaut.html" pr&eacute;sent &agrave; la racine du plugin.',
	'width_and_height' => 'Taille de la fen&ecirc;te',
	'width' => 'Largeur (en pixels)',
	'height' => 'Hauteur (en pixels)',
	'titre_popup' => 'Nom JavaScript de la fen&ecirc;tre',
	'titre_popup_comment' => 'Vous pourrez par la suite utiliser ce nom pour la d&eacute;signer dans vos scripts ("window.popup").',
	'options_popup' => 'Options JavaScript pass&eacute;es &agrave; la nouvelle fen&ecirc;tre',
	'options_popup_comment' => 'Indiquez ici un tableau d\'options qui seront pass&eacute;es &agrave; la nouvelle fen&ecirc;tre sous la forme d\'un tableau du genre : "variable1: valeur1, variable2: valeur2, ..." (ex.: "location: 0, scrollbars: 1").',

// Popup
	'popup_titre' => 'Bo&#238;te de dialogue',
	'btn_imprimer' => 'Imprimer',
	'btn_imprimer_ttl' => 'Imprimer cette page',
	'btn_fermer_fenetre' => 'Fermer',
	'btn_fermer_fenetre_ttl' => 'Fermer cette fen&#234;tre',
	'btn_haut_page' => 'Haut de page',
	'btn_haut_page_ttl' => 'Retour en haut de page',
	'nouvelle_fenetre' => '[Nouvelle fen&#234;tre]',
	'retour_fenetre' => '[Retour en fen&#234;tre principale]',

	// E
	// editor
	'editor' => 'Editeur de lien popup',
	'editor_titre_inserer_lien_popup' => 'Insérer un lien popup',
	'editor_lien' => 'Lien à insérer (URL ou objet SPIP)',
	'editor_texte' => 'Texte du lien',
	'editor_skel' => 'Squelette de page SPIP',
	'editor_titre' => 'Titre du lien (au passage de la souris)',
	'editor_erreur_entree_obligatoire' => 'Cette entrée est obligatoire !',
	'editor_enregistrer' => 'Enregistrer',

	// I
	'inserer_lien_popup' => 'Insérer un lien en fenêtre popup',
);
?>