Plugin "Popup" pour SPIP 2.0+
Lame pour le plugin Couteau Suisse
------------------------------------------

INSTALLATION

Pour installer la lame, copiez tous les fichiers contenus dans le dossier o� se trouve ce README.txt dans votre dossier de squelettes.
(En gros, ne cr�ez pas de dossier "popup_lame_cs" suppl�mentaire)

La lame "Balise #POPUP" sera alors automatiquement d�tect�e par le Couteau Suisse dans la rubrique "Balises, filtres, crit�res".
A vous ensuite de la configurer et de l'utiliser !
 

------------------------------------------

Plugin Couteau Suisse : http://www.spip-contrib.net/Le-Couteau-Suisse

Plugin Popup : http://www.spip-contrib.net/?article3573
