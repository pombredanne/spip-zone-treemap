<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-crud?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'crud_description' => 'C(r)UD Interface for SPIP that can be used via an action defined by its URL or calling it directly.',
	'crud_slogan' => 'Interface to create, update and delete an object'
);

?>
