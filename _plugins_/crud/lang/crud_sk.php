<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/crud?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_action_erronee' => 'CRUD: chyba akcie @action@',
	'erreur_action_inconnue_table' => 'CRUD: tabuľka @table@ nepozná akciu @action@',
	'erreur_article_inconnue' => 'CRUD: článok @id@ neexistuje',
	'erreur_creation' => 'CRUD: objekt typu "@objet@" sa nedá vytvoriť (Skontrolujte si svoje práva)',
	'erreur_info_obligatoire' => 'CRUD: Pole @info@ je povinné',
	'erreur_objet_inexistant' => 'CRUD: objekt @objet@ #@id_objet@ neexistuje',
	'erreur_rubrique_inconnue' => 'CRUD: rubrika @id@ neexistuje',
	'erreur_suppression' => 'CRUD: chyba pri vymazávaní objektu "@objet@" #@id_objet@ (Skontrolujte si svoje práva)',
	'erreur_table_erronee' => 'CRUD: chyba tabuľky @table@',
	'erreur_table_inconnue' => 'CRUD: systém nepozná tabuľku @table@',
	'erreur_update' => 'CRUD: chyba pri aktualizácii objektu "@objet@" #@id@ (Skontrolujte si svoje práva)'
);

?>
