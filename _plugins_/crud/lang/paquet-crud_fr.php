<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/crud/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'crud_description' => 'Une interface C(r)UD pour SPIP qui peut s\'utiliser par l\'intermédiaire d\'une action définie par son url ou par appel direct.',
	'crud_slogan' => 'Interface de création, mise à jour et suppression d\'un objet'
);

?>
