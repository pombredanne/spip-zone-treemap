<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-crud?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'crud_description' => 'Rozhranie C(r)UD pre SPIP, ktoré môžete využívať prostredníctvom akcie, ktorá je definovaná cez svoju url alebo priamo prostredníctvom dialógového okna.',
	'crud_slogan' => 'Rozhranie na vytváranie, aktualizovanie a vymazávanie objektu'
);

?>
