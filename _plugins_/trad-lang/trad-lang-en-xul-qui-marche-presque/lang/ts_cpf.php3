<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// 0
'0_URL' => 'http://listes.rezo.net/mailman/listinfo/spip-dev',
'0_langue' => 'Kr&eacute;ol r&eacute;yon&eacute; [CPF]',
'0_liste' => 'spip-cpf@rezo.net',
'0_mainteneur' => 'pascale@rezo.net',


// B
'bouton_administrer' => 'Po f&eacute; lo kolonaz (fr)',
'bouton_ajouter' => 'Azout&eacute;',
'bouton_annuler' => 'Anil&eacute;',
'bouton_chercher' => 'Pou rod',
'bouton_continuer' => 'Kontiny&eacute; >>>',
'bouton_modifier' => 'Pou sanz&eacute;',
'bouton_restaurer' => 'Artourn sovgard',
'bouton_revenir2' => 'Arnir bat-ary&egrave;r',
'bouton_revenir_2' => 'Arnir bat-ary&egrave;r',
'bouton_telecharger' => 'Po t&eacute;l&eacute;sarz&eacute; >>',
'bouton_traduire' => 'Po tradwi >>',
'bouton_valider' => 'Po konfirm&eacute;',


// C
'confirmer_effacer' => 'Po konfirm&eacute; ifo &eacute;fas lo s&egrave;n-la: @chaine@',


// D
'dans' => 'andan',


// I
'item_admin' => 'KOMANDER',
'item_conflit' => 'DISKORD',
'item_date' => 'Bann dat',
'item_effacer' => 'Pou &eacute;fas&eacute;',
'item_langue_cible' => 'Lalang po tradwi : ',
'item_langue_origine' => 'Lalang lorizin :',
'item_modifie' => 'SANZE',
'item_non_traduit' => 'POKOR TRADWI',
'item_nouveau' => 'Nouvo',
'item_nouveau_code' => 'o nouvo kodlang :',
'item_tous' => 'Tout lansanm',
'item_traduit' => 'tradwi',


// L
'lien_bilan' => 'Bilan bann tradiksyon akty&egrave;l.',
'lien_code_langue' => 'Kodlang-la l&eacute; pa bon. Ifo li n&eacute;na d&eacute; l&egrave;t andan omwin (kod ISO-631).',
'lien_confirm_export' => 'Pou konfirm&eacute; l&eacute;spor lo fisy&eacute; i mars ast&egrave;r (s&eacute;dir lo krazman lo fisy&eacute; @fichier@)',
'lien_export' => 'Po &eacute;sport&eacute; otomatikman lo fisy&eacute; akty&egrave;l',
'lien_page_depart' => 'Ronir si lapaz-ak&egrave;y ?',
'lien_proportion' => '<MODIF> Rapor bann s&egrave;n afis&eacute;',
'lien_recharger_page' => 'Rosarz lapaz.',
'lien_retour' => 'Arnir',
'lien_revenir_traduction' => 'Arnir si lapaz tradiksyon',
'lien_sauvegarder' => 'Sovgard/artourn lo fisy&eacute; akty&egrave;l.',
'lien_telecharger' => '[Po t&eacute;l&eacute;sarz]',
'lien_traduction_module' => 'Modil ',
'lien_traduction_vers' => 'ziska ',


// S
'sel_langue_cible' => 'Lalang po tradwi',
'sel_langue_origine' => 'Lalang lorizin',


// T
'texte_admin' => '<b>KOLONAZ POU </b>',
'texte_avis_doc' => 'Po tradwi SPIP, anon lir <a href=\'doc_trad_lang.html\'> ladokimantasyon nout zoutiy tradikt&egrave;r</a> &eacute;pi anon klik anba t&egrave;rla',
'texte_avis_enregistrer' => 'Ifo <a href=\'spip_login.php3?var_url=trad_lang.php3\'>anrozistr aou.</a>',
'texte_consulter' => 'Po konsilt&eacute;',
'texte_consulter_brut' => 'Po konsilt&eacute; S&egrave;k ',
'texte_contact' => 'Ekri anou : ',
'texte_creation_langue_impossible' => 'Dabor: <P>Lakr&eacute;asyon inn nouvo lang l&eacute; pa posib ast&egrave;r : akoz bann fisy&eacute; \'@dir_lang@\' et \'@dir_bak@\'  l&eacute; pa ouv&egrave;r po l&eacute;kritir.<P> kank ou lora ran azot ouv&egrave;r komsa, ou sra kav',
'texte_creation_nouvelle_langue_impossible' => 'Lakr&eacute;asyon lo nouvo lang-la l&eacute; pa posib ast&egrave;r. Lo fisy&eacute; ',
'texte_erreur' => 'KANAR',
'texte_erreur_acces' => ' <b>Tansyon pangar: </b>l&eacute; pa posib &eacute;kri dann fisy&eacute;-la<tt>@fichier_lang@</tt>. M&egrave;rsi kontrol&eacute; zot drwa laks&eacute;.',
'texte_existe_deja' => 'i ekzizt d&eacute;za.',
'texte_explication_langue_cible' => '<MODIF> Po lalang po tradwi : ifo ou i indik si lalang-la l&eacute; inn loang i ekzizt d&eacute;za andan nout lint&egrave;rfas, oubyinsa ifo kr&eacute; ali andan inn nouvo fisy&eacute;. (L&eacute; posib koz ansanm lo swa lo kod lalang si lalist  spip-trad(a)rezo.net ; ou p&eacute; osi rann aou si  lo sit <a href=http://www.ethnologue.com/site_search.asp>www.ethnologue.com</a>)<p>Si ou v&eacute; inn bilan pr&eacute;si si tout bann tradiksyon nou l&eacute; apr&eacute; men&eacute; ( o r&eacute;kip&egrave;r bann fisy&eacute;-lang dann forma s&egrave;k ), m&egrave;rsi klik&eacute; si <A HREF=\'./trad_lang.php3?module=@module@&amp;etape=bilan\'><b>ici</b><',
'texte_export_impossible' => 'L&eacute; pa posib &eacute;sport&eacute; lo fisy&eacute;. G&egrave;t ou n&eacute;na byin tout bann drwa pou l&eacute;kritir si lo fisy&eacute; @cible@',
'texte_fichier' => 'Fisy&eacute; : ',
'texte_fichier2' => 'li dwa &egrave;t ouv&egrave;r po l&eacute;kritir. ',
'texte_fichier3' => 'Kank lop&eacute;rasyon-la lora finn mars&eacute;, ou sra kav  ',
'texte_fichier4' => 'Rosarz lapaz-la.',
'texte_fichier5' => 'Lo fisy&eacute; ',
'texte_filtre' => 'Filtr (rod)',
'texte_interface' => 'Lint&egrave;rfas latradiksyon : ',
'texte_interface2' => 'Lint&egrave;rfas latradiksyon',
'texte_langue' => 'Lang :',
'texte_langue_cible' => ' <b>Lalang po tradwi</b> sa l&eacute; lalang sak ou v&eacute; donn inn nouv&egrave;l versyon lo modil swazi.',
'texte_langue_origine' => '<b>Lalang lorizin</b> sa l&eacute; lalang ou sort po ',
'texte_langue_origine2' => 'l&eacute; apr&eacute; tradwi.',
'texte_langues_differentes' => 'Gard byin ! Ifo lalang po tradwi i swa dif&eacute;ran lalang lorizin.',
'texte_modifier' => 'Po sanz',
'texte_module' => 'lo modil po tradwi. ',
'texte_module_traduire' => 'Lo modil po tradwi :',
'texte_non_traduit' => 'pokor tradwi ',
'texte_operation_desactivee' => 'Lop&eacute;rasyon-la l&eacute; d&eacute;gr&eacute;n&eacute;. Domann moun si lalist trad-spip pou sovgard lo fisy&eacute;...',
'texte_operation_impossible' => 'Lop&eacute;rasyon-la l&eacute; pokor posib. Kank n&eacute;na inn krwa si lo kar&eacute; \'swazi lansanm\' ,<br> ifo ou f&eacute; zw&eacute; inn lop&eacute;rasyon lo tip \'Konsilt&eacute;\'.',
'texte_pas_de_reponse' => '... n&eacute;na pwinn okinn r&eacute;pons',
'texte_preleminaire' => 'Dabor : ',
'texte_preleminaire2' => 'Dabor : ',
'texte_recapitulatif' => 'Ramas bilan bann tradiksyon',
'texte_restauration_fichier_langue' => 'Fin&egrave;t-la i p&egrave;rm&eacute; aou sovgard o artounr lo fisy&eacute; lalang ou l&eacute; apr&eacute; travay&eacute; ast&egrave;r. ',
'texte_restauration_impossible' => 'l&eacute; imposib artourn lo sovgard lo fisy&eacute;-la',
'texte_restaurer' => 'Po artoun inn fisy&eacute; :',
'texte_saisie_informations' => 'Pou &eacute;kri bann zinformasyon si latradiksyon',
'texte_saisie_informations2' => '<MODIF> Bann\'<b>@circ@</b>\' i ropr&eacute;zant bann z&eacute;&nbsp;pas ou p&eacute; pa koup&eacute; (z&eacute;spas ins&eacute;kab). (&amp;nbsp; andan HTML).',
'texte_saisie_informations3' => '<MODIF> Bann \'<b>@dollar@</b>\' i ropr&eacute;zant bann sotlaliny.',
'texte_sauvegarde' => 'Linterfas po latradiksyon, Sovgard/artounaz pou lo fisy&eacute;',
'texte_sauvegarde_courant' => 'Kopi po lasovgard lo fisy&eacute; akty&egrave;l :',
'texte_sauvegarde_impossible' => 'l&eacute; pa posib sovgard lo fisy&eacute;-la',
'texte_sauvegarder' => 'Po sovgard&eacute;',
'texte_selection_langue' => 'M&eacute;rsi swazi inn lang anba t&egrave;rla, pou afis inn fisy&eacute; po lalang d&eacute;za o pokor tradwui : 
 ',
'texte_selectionner' => 'Ou dwa swazi :',
'texte_selectionner_version' => 'Swazi inn v&egrave;rsyon lo fisy&eacute; &eacute;pi apiy lo bouton kot&eacute; la.',
'texte_seul_admin' => 'In kont komand&egrave;r s&egrave;lman i p&eacute; nir si l&eacute;tap-la.',
'texte_total_chaine' => 'Nom bann s&egrave;n :',
'texte_total_chaine_conflit' => 'Nob bann s&egrave;n l&eacute; an diskord :',
'texte_total_chaine_modifie' => 'Nob bann s&egrave;n ifo sanz ast&egrave;r :',
'texte_total_chaine_non_traduite' => 'Nonb bann s&egrave;n pokor tradwi :',
'texte_total_chaine_traduite' => 'Nob bann s&egrave;n d&eacute;za tradwi :',
'texte_tout_selectionner' => 'Swazi lansanm',
'texte_type_operation' => 'Tip lop&eacute;rasyon-la',
'titre_traduction' => 'Bann tradiksyon',
'titre_traduction_de' => 'Tradiksyon de',
'type_messages' => 'Tip bann mod&eacute;kri'

);


?>
