<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// 0
'0_URL' => 'http://listes.rezo.net/mailman/listinfo/spip-dev',
'0_langue' => 'Nederlands [nl]',
'0_liste' => 'spip-dev@rezo.net',
'0_mainteneur' => 'spip-dev@rezo.net',


// B
'bouton_administrer' => 'Beheren (nl)',
'bouton_ajouter' => 'Toevoegen',
'bouton_annuler' => 'Annuleren',
'bouton_chercher' => 'Zoeken',
'bouton_continuer' => 'Doorgaan >>>',
'bouton_modifier' => 'Wijzigen',
'bouton_restaurer' => 'Terugzetten',
'bouton_revenir2' => 'Terugkeren',
'bouton_revenir_2' => 'Teruggaan',
'bouton_telecharger' => 'Opladen >>',
'bouton_traduire' => 'Vertalen >>',
'bouton_valider' => 'Goedkeuren',


// C
'confirmer_effacer' => 'Bevestig het wissen van het veld : @chaine@',


// D
'dans' => 'in',


// I
'item_admin' => 'ADMIN',
'item_conflit' => 'NIET LANGER GEBRUIKT',
'item_date' => 'Data',
'item_effacer' => 'Wissen',
'item_langue_cible' => 'Doeltaal :',
'item_langue_origine' => 'brontaal :',
'item_modifie' => 'GEWIJZIGD',
'item_non_traduit' => 'ONVERTAALD',
'item_nouveau' => 'Nieuw',
'item_nouveau_code' => 'of nieuwe taalcode :',
'item_tous' => 'Alles',
'item_traduit' => 'vertaald',


// L
'lien_bilan' => 'Balans van de lopende vertalingen.',
'lien_code_langue' => 'Ongeldige taalcode. De taalcode dient minstens uit 2 letters te bestaan (code ISO-631).',
'lien_confirm_export' => 'De uitvoer van het huidige bestand bevestigen (m.a.w. het wissen van het bestand @fichier@)',
'lien_export' => 'Het huidige bestand automatisch uitvoeren.',
'lien_page_depart' => 'Terugkeren naar de bewaarpagina ?',
'lien_proportion' => 'Aandeel van de getoonde velden',
'lien_recharger_page' => 'Pagina herladen.',
'lien_retour' => 'Terugkeer',
'lien_revenir_traduction' => 'Terugkeren naar de vertaalpagina',
'lien_sauvegarder' => 'Bewaren/terugzetten van het huidige bestand.',
'lien_telecharger' => '[Opladen]',
'lien_traduction_module' => 'Module ',
'lien_traduction_vers' => ' naar ',


// S
'sel_langue_cible' => 'doeltaal',
'sel_langue_origine' => 'brontaal',


// T
'texte_admin' => '<b>BEHEER VAN </b>',
'texte_avis_doc' => 'Om SPIP te vertalen, lees je best eerst <a href=\'doc_trad_lang.html\'> de documentatie van de vetaalmoudle</a>. Klik vervolgens hieronder.',
'texte_avis_enregistrer' => 'Je dient je te <a href=\'spip_login.php3?var_url=trad_lang.php3\'>registreren.</a>',
'texte_consulter' => 'Raadplegen',
'texte_consulter_brut' => 'Brutovorm weergeven',
'texte_contact' => 'Contact : ',
'texte_creation_langue_impossible' => 'Vooraf : <P>Onmogelijk om de nieuwe taal aan te maken : op de bestanden \'@dir_lang@\' et \'@dir_bak@\' moeten schrijfrechten zijn.<P>Als deze bewerking gelukt is, kan je  ',
'texte_creation_nouvelle_langue_impossible' => 'Onmogelijk om de nieuwe taal aan te maken. Het bestand',
'texte_erreur' => 'FOUT',
'texte_erreur_acces' => '<b>Opgelet : </b>onmogelijk om in het bestand<tt>@fichier_lang@</tt> te schrijven. Controleer de toegangsrechten.',
'texte_existe_deja' => 'bestaat reeds.',
'texte_explication_langue_cible' => 'Als doeltaal kies je een reeds bestaande taal of een nieuwe taal : in dat laatste geval zal een nieuw bestand aangemaakt worden. (Over de te gebruiken taalcode kan gesproken worden op de lijst   spip-trad(a)rezo.net, ofwel kijk je op <a href=http://www.ethnologue.com/site_search.asp>www.ethnologue.com</a>)<p>Voor een gedetailleerd overzicht van de lopende vertalingen (of om de taalfolders in bruto vorm binnen te halen), klik <A HREF=\'./trad_lang.php3?module=@module@&amp;etape=bilan\'><b>hier</b></a>.',
'texte_export_impossible' => 'Onmogelijk het bestand te exportern. Verifieer de schrijfrechten van het bestand @cible@',
'texte_fichier' => 'Bestand : ',
'texte_fichier2' => 'moet schrijfrechten hebben.',
'texte_fichier3' => 'Als deze bewerking voltooid is, kan je',
'texte_fichier4' => 'Deze pagina herladen.',
'texte_fichier5' => 'Bestand',
'texte_filtre' => 'Filter (zoeken)',
'texte_interface' => 'Vertaalmodule : ',
'texte_interface2' => 'Vertaalmodule',
'texte_langue' => 'Taal :',
'texte_langue_cible' => '<b>De doeltaal</b> is de taal naar dewelke je vertaalt.',
'texte_langue_origine' => '<b>De brontaal</b> is de taal van waaruit je',
'texte_langue_origine2' => 'vertaalt (vrijwel zeker het Frans).',
'texte_langues_differentes' => 'Doel- en brontaal dienen verschillend te zijn.',
'texte_modifier' => 'Wijzigen',
'texte_module' => 'Te vertalen module. ',
'texte_module_traduire' => 'Te vertalen module :',
'texte_non_traduit' => 'onvertaald',
'texte_operation_desactivee' => 'Deze bewerking is niet geactiveerd. Vraag op de trad-spip lijst om het bestand terug te zetten....',
'texte_operation_impossible' => 'Bewerking is onmogelijk. Wanneer het vakje \'alles selecteren\' aangekruisd is,<br> dien je bewerkingen van het type \'Raadplegen\' te doen.',
'texte_pas_de_reponse' => '.... geen antwoord',
'texte_preleminaire' => 'Vooraf : ',
'texte_preleminaire2' => 'Vooraf :',
'texte_recapitulatif' => 'Hernemen vertalingen',
'texte_restauration_fichier_langue' => 'In dit venster kan je het taalbestand waarop je aan het werken bent, bewaren of terugzetten.',
'texte_restauration_impossible' => 'onmogelijk om het bestand terug te zetten',
'texte_restaurer' => 'Een bestand terugzetten :',
'texte_saisie_informations' => 'Bewaren van de vertaalinformatie',
'texte_saisie_informations2' => 'De \'<b>@circ@</b>\' stellen een harde spatie voor. (zoals &amp;nbsp; bij HTML).',
'texte_saisie_informations3' => 'De \'<b>@dollar@</b>\' tekens stellen een lijnsprong voor (harde return).',
'texte_sauvegarde' => 'Vertaalmodule, Bewaren/Terugzetten van het bestand',
'texte_sauvegarde_courant' => 'Reservekopie van het huidige bestand :',
'texte_sauvegarde_impossible' => 'onmogelijk het bestand te bewaren',
'texte_sauvegarder' => 'Bewaren',
'texte_selection_langue' => 'Om een bestand in een vertaalde taal (of waarvan de vertaling aan de gang is) weer te geven, gelieve hier de taal te kiezen : ',
'texte_selectionner' => 'Je dient te kiezen :',
'texte_selectionner_version' => 'Kies een versie van het bestand en klik vervolgens op de knop hiertegenover.',
'texte_seul_admin' => 'Enkel een beheerdersaccount kan deze stap uitvoeren.',
'texte_total_chaine' => 'Aantal reeksen :',
'texte_total_chaine_conflit' => 'Aantal ongebruikte reeksen :',
'texte_total_chaine_modifie' => 'Aantal reeksen voor update :',
'texte_total_chaine_non_traduite' => 'Aantal onvertaalde reeksen :',
'texte_total_chaine_traduite' => 'Aantal vertaalde reeksen :',
'texte_tout_selectionner' => 'Alles selecteren',
'texte_type_operation' => 'Type bewerking',
'titre_traduction' => 'Vertalingen',
'titre_traduction_de' => 'Vertaling van',
'type_messages' => 'Type berichten'

);


?>
