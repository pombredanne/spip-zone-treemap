<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// 0
'0_URL' => 'http://listes.rezo.net/mailman/listinfo/spip-dev',
'0_langue' => 'Polski [pl]',
'0_liste' => 'spip-dev@rezo.net',
'0_mainteneur' => 'd.paszkiewicz@ekstenso.com',


// B
'bouton_administrer' => 'Zarz&#261;dzaj (pl)',
'bouton_ajouter' => 'Dodaj',
'bouton_annuler' => 'Anuluj',
'bouton_chercher' => 'Szukaj',
'bouton_continuer' => 'T&#322;umacz >>>',
'bouton_modifier' => 'Zmie&#324;',
'bouton_restaurer' => 'Przywr&oacute;&#263;',
'bouton_revenir2' => 'Powr&oacute;t',
'bouton_revenir_2' => 'Powr&oacute;t',
'bouton_telecharger' => '&#346;ci&#261;gnij >>',
'bouton_traduire' => 'T&#322;umacz >>',
'bouton_valider' => 'Zatwierd&#378;',


// C
'confirmer_effacer' => 'Potwierd&#378; usuni&#281;cie ci&#261;gu znakowego : @chaine@',


// D
'dans' => 'w',


// I
'item_admin' => 'ADMIN',
'item_conflit' => 'PLUS_UTILISE',
'item_date' => ' Daty',
'item_effacer' => 'Wyczy&#347;&#263;',
'item_langue_cible' => 'J&#281;zyk docelowy :',
'item_langue_origine' => 'J&#281;zyk &#378;r&oacute;d&#322;owy :',
'item_modifie' => 'ZMODYFIKOWANY',
'item_non_traduit' => 'NIE PRZET&#321;UMACZONY',
'item_nouveau' => 'Nowy',
'item_nouveau_code' => 'lub nowy kod j&#281;zyka :',
'item_tous' => 'Wszystko',
'item_traduit' => 'przet&#322;umaczony',


// L
'lien_bilan' => 'Bilans aktualnych prac przek&#322;adowych.',
'lien_code_langue' => 'Kod j&#281;zyka nie jest prawid&#322;owy. Powinien zawiera&#263; co najmniej dwie litery (ISO-631)',
'lien_confirm_export' => 'Potwierd&#378; eksport tego pliku (np. nadpisanie pliku @fichier@)',
'lien_export' => 'Automatycznie wyeksportowa&#263; aktualne pliki.',
'lien_page_depart' => 'Czy powr&oacute;ci&#263; na stron&#281; g&#322;&oacute;wn&#261; ?',
'lien_proportion' => 'Proporcje wy&#347;wietlanych ci&#261;g&oacute;w znakowych',
'lien_recharger_page' => 'Od&#347;wie&#380;y&#263; stron&#281;.',
'lien_retour' => 'Powr&oacute;t',
'lien_revenir_traduction' => 'Powr&oacute;t do interfejsu t&#322;umacze&#324;',
'lien_sauvegarder' => 'Zapisa&#263;/Odtworzy&#263; aktualne pliki.',
'lien_telecharger' => '[&#346;ci&#261;gnij]',
'lien_traduction_module' => 'Modu&#322;',
'lien_traduction_vers' => ' na ',


// S
'sel_langue_cible' => 'J&#281;zyk docelowy',
'sel_langue_origine' => 'J&#281;zyk &#378;r&oacute;d&#322;owy',


// T
'texte_admin' => '<b>ADMINISTRACJA</b>',
'texte_avis_doc' => 'Aby t&#322;umaczy&#263; SPIP, przeczytaj <a href=\'doc_trad_lang.html\'> dokumentacj&#281; na temat narz&#281;dzia s&#322;u&#380;&#261;cego do t&#322;umaczenia</a> nast&#281;pnie kliknij poni&#380;ej',
'texte_avis_enregistrer' => 'Musisz si&#281; <a href=\'spip_login.php3?var_url=trad_lang.php3\'>zarejestrowa&#263;.</a>',
'texte_consulter' => 'Konsultuj',
'texte_consulter_brut' => 'Zobacz kod',
'texte_contact' => 'Kontakt : ',
'texte_creation_langue_impossible' => 'Wst&#281;p: <P>Stworzenie nowego j&#281;zyka niemo&#380;liwe : pliki \'@dir_lang@\' i \'@dir_bak@\' powinny by&#263; zapisywalne.<P> Kiedy wprowadzisz zmiany, b&#281;dziesz m&oacute;g&#322;',
'texte_creation_nouvelle_langue_impossible' => 'Storzenie nowego j&#281;zyka jest niemo&#380;liwe. Pliki',
'texte_erreur' => 'B&#321;&#260;D',
'texte_erreur_acces' => '<b>Uwaga : </b>nie mo&#380;na zapisa&#263; w pliku <tt>@fichier_lang@</tt>. Sprawd&#378; prawo dost&#281;pu.',
'texte_existe_deja' => ' ju&#380; istnieje.',
'texte_explication_langue_cible' => 'Je&#347;li chodzi o j&#281;zyk docelowy, powiniene&#347; wybra&#263; czy chcesz edytowa&#263; plik j&#281;zykowy ju&#380; istniej&#261;cy czy chcecie stworzy&#263; nowy: w tym przypadku mo&#380;ecie stworzyc nowy. (na temat wyboru kodu j&#281;zykowego mo&#380;emy podyskutowa&#263; na naszej li&#347;cie dyskusyjnej <a href=http://www.ethnologue.com/site_search.asp>www.ethnologue.com</a>)<p>Je&#347;li chcesz zobaczy&#263; szczeg&oacute;&#322;owy bilans aktualnych przek&#322;ad&oacute;w lub &#347;ci&#261;gn&#261;&#263; &#378;r&oacute;d&#322;owe pliki j&#281;zykowe, kliknij <A HREF=\'./trad_lang.php?module=@module@&amp;etape=bilan\'><b>tutaj</b></a>.',
'texte_export_impossible' => 'Eksport pliku niemo&#380;liwy. Zweryfikuj prawa dost&#281;pu/zapisu plik&oacute;w @cible@',
'texte_fichier' => 'Plik : ',
'texte_fichier2' => 'plik musi by&#263; zapisywalny.',
'texte_fichier3' => 'Kiedy ta operacja zostanie wykonana b&#281;dziesz m&oacute;g&#322;',
'texte_fichier4' => 'Prze&#322;aduj t&#261; stron&#281; .',
'texte_fichier5' => 'Pliki',
'texte_filtre' => 'Filtr (przeszukaj)',
'texte_interface' => 'Interfejs przek&#322;adu :',
'texte_interface2' => 'Interfejs przek&#322;adu',
'texte_langue' => 'J&#281;zyk :',
'texte_langue_cible' => '<b>J&#281;zyk docelowy</b> jest j&#281;zykiem, na kt&oacute;ry t&#322;umaczysz.',
'texte_langue_origine' => '<b>J&#281;zyk &#378;r&oacute;d&#322;owy</b> jest j&#281;zykiem, z kt&oacute;rego t&#322;umaczysz',
'texte_langue_origine2' => ' (zapewne z francuskiego).',
'texte_langues_differentes' => 'J&#281;zyk docelowy i j&#281;zyk &#378;r&oacute;d&#322;owy musz&#261; by&#263; r&oacute;&#380;ne.',
'texte_modifier' => 'Zmie&#324;',
'texte_module' => 'modu&#322; do przet&#322;umaczenia.',
'texte_module_traduire' => 'Modu&#322; do t&#322;umaczenia :',
'texte_non_traduit' => 'nie przet&#322;umaczony',
'texte_operation_desactivee' => 'Ta operacja jest nieaktywna. Zwr&oacute;&#263;cie si&#281; na li&#347;cie <b>trad-spip</b> o przywr&oacute;cenie plik&oacute;w...',
'texte_operation_impossible' => 'Operacja niemo&#380;liwa. Kiedy kratka \'wybierz wszystko\' jest zaznaczona,<br>trzeba wykona&#263; operacj&#281; \'Sprawd&#378;\'.',
'texte_pas_de_reponse' => '... brak odpowiedzi',
'texte_preleminaire' => 'Wst&#281;p :',
'texte_preleminaire2' => 'Wst&#281;p :',
'texte_recapitulatif' => 'Podsumowanie przek&#322;ad&oacute;w',
'texte_restauration_fichier_langue' => 'W tym oknie mo&#380;esz zapisa&#263; lub odtworzy&#263; plik j&#281;zykowy, nad kt&oacute;rym pracujesz.',
'texte_restauration_impossible' => 'odtworzenie pliku jest niemo&#380;liwe',
'texte_restaurer' => 'Odtw&oacute;rz plik :',
'texte_saisie_informations' => 'Zapisywanie informacji o przek&#322;adzie',
'texte_saisie_informations2' => '\'<b>@circ@</b>\' - to niemo&#380;liwe do pomini&#281;cia spacje. (tak jak: &amp;nbsp; w HTML-u).',
'texte_saisie_informations3' => '\'<b>@dollar@</b>\' oznacza obowi&#261;zkowy przeskok do nast&#281;pnej linii.',
'texte_sauvegarde' => 'Iterfejs przek&#322;adu Zapisywanie/Odzysk plik&oacute;w',
'texte_sauvegarde_courant' => 'Kopia zapasowa aktualnego pliku :',
'texte_sauvegarde_impossible' => 'zapis pliku jest niemo&#380;liwy',
'texte_sauvegarder' => 'Zapisz',
'texte_selection_langue' => 'Aby wy&#347;wietli&#263; plik t&#322;umaczonego j&#281;zyka wybierz j&#281;zyk :',
'texte_selectionner' => 'Musisz wybra&#263; :',
'texte_selectionner_version' => 'Wybierz werjs&#281; pliku, nast&#281;pnie naci&#347;nij przycisk.',
'texte_seul_admin' => 'Jedynie administrator mo&#380;e uzyskac dost&#281;p do tego etapu.',
'texte_total_chaine' => 'Ca&#322;kowita liczba ci&#261;g&oacute;w znakowych :',
'texte_total_chaine_conflit' => 'Liczba nieu&#380;ywanych ju&#380; ci&#261;g&oacute;w znakowych :',
'texte_total_chaine_modifie' => 'Liczba ciag&oacute;w znakowych, kt&oacute;re nale&#380;y uaktualni&#263; :',
'texte_total_chaine_non_traduite' => 'Liczba nieprzet&#322;umaczonych ci&#261;g&oacute;w znakowych:',
'texte_total_chaine_traduite' => 'Liczba przet&#322;umaczonych ci&#261;g&oacute;w znakowych :',
'texte_tout_selectionner' => 'Wybierz wszystko',
'texte_type_operation' => 'Rodzaj operacji',
'titre_traduction' => 'Przek&#322;ady',
'titre_traduction_de' => 'Przek&#322;ad z ',
'type_messages' => 'Rodzaj wiadomo&#347;ci'

);


?>
