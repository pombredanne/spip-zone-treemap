<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/trad-lang/trad-lang_spip2/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'tradlang_description' => 'Un plugin pour gérer directement les fichiers de langues depuis SPIP.',
	'tradlang_slogan' => 'Gérer les fichiers de langue'
);

?>
