<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-tradlang?lang_cible=ar
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'tradlang_description' => 'ملحق لإداراة ملفات اللغات مباشرة من SPIP.',
	'tradlang_slogan' => 'إداراة ملفات اللغات'
);

?>
