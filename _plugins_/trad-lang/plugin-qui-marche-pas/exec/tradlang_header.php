<?
/*

    This file is part of Trad-Lang.

    Trad-Lang is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Trad-Lang is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Trad-Lang; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Copyright 2003 
        Florent Jugla <florent.jugla@eledo.com>, 
        Philippe Rivi�re <fil@rezo.net>

*/
?>


<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html lang='fr' dir='ltr'>
<head>
<title>[Mon site SPIP] Trad-Lang</title>
<link rel="stylesheet" type="text/css" href="img_pack/style_prive_defaut.css" >
<link rel="stylesheet" type="text/css" href="http://localhost/SPIP-v1-9-1/spip.php?page=style_prive&amp;couleur_claire=C0CAD4&amp;couleur_foncee=85909A&amp;ltr=left" >
<link rel="stylesheet" type="text/css" href="../dist/agenda.css" >
<link rel="stylesheet" type="text/css" href="img_pack/spip_style_print.css" media="print" >
<link rel="stylesheet" type="text/css" href="img_pack/spip_style_invisible.css" >
<link rel="shortcut icon" href="http://localhost/SPIP-v1-9-1/dist/favicon.ico" >
<link rel='alternate' type='application/rss+xml' title="Mon site SPIP" href='http://localhost/SPIP-v1-9-1/spip.php?page=backend' >
<link rel='help' type='text/html' title="Aide" href='http://localhost/SPIP-v1-9-1/ecrire/?exec=aide_index&amp;var_lang=fr' >
<link rel='alternate' type='application/rss+xml' title="Mon site SPIP (br&egrave;ves)" href='http://localhost/SPIP-v1-9-1/spip.php?page=backend-breves' >
<script type="text/javascript" src="img_pack/layer.js"></script>
<script type="text/javascript"><!--

var ajax_image_searching = '<div style="float: right;"><img src="http://localhost/SPIP-v1-9-1/ecrire/img_pack/searching.gif" /><\/div>';
var admin = 1
var stat = 1
var largeur_icone = 84
var  bug_offsetwidth = 1
var confirm_changer_statut = 'Attention, vous avez demand\u00e9 \u00e0 changer le statut de cet \u00e9l\u00e9ment. Souhaitez-vous continuer?';

//--></script>

<script type="text/javascript" src="img_pack/presentation.js"></script>

</head>
<body 
link='#3B5063'
vlink='#6D8499'
alink='#6D8499'
bgcolor='#f8f7f3' text='#000000' 
topmargin='0' leftmargin='0' marginwidth='0' marginheight='0' frameborder='0' onLoad="verifForm();">

<div style="margin:10px;">