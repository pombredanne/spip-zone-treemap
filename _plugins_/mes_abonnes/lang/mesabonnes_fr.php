<?php
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

    // B
    'bye' => 'L\'adresse @email@ a été retirée de notre liste de diffusion, merci.',
		'bouton_ok' => 'OK',
    
    // E
    'export_abonnes' => 'Exporter les abonnés au format',
    'export_abonnes_csv' => "CSV (Format complet)",
    'export_abonnes_csv_bulk' => "CSV (Format MaxBulk Mailer)",
    'export_abonnes_compte' => '@compte@ abonnés actuellement enregistrés',
    'export_abonnes_rien' => 'Aucun abonné enregistré actuellement',

    // M
    'mes_abonnes' => 'Mes abonnés',         
    'merci' => 'L\'adresse @email@ a été ajoutée à notre liste de diffusion, merci.',
    
    // S
    'subscribe' => 'Abonnement',
    
    // U
    'unsubscribe' => 'Désabonnement'
    
    

); 

?>
