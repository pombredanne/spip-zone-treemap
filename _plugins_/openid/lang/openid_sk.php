<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/openid?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_librairies' => 'Knižnice na prihlásenie cez OpenID sa nedajú nájsť',
	'erreur_openid' => 'Zdá sa, že táto adresa OpenID nie je platná',
	'erreur_openid_info_manquantes' => 'Poskytovateľ vášho OpenID neposkytol všetky potrebné údaje',

	// F
	'form_forum_indiquer_openid' => 'Na tejto stránke sa môžete zaregistrovať uvedením identifikátora OpenID.',
	'form_forum_openid' => 'Prihlásenie cez OpenID',
	'form_login_openid' => 'Môžete použiť aj OpenID (<a href="http://sk.wikipedia.org/wiki/OpenID" target="_blank">pomoc</a>)',
	'form_login_openid_inconnu' => 'Táto adresa OpenID je neznáma. Skontrolujte ju, alebo si ju uložte do svojho profilu.',
	'form_login_openid_ok' => 'Tento identifikátor využíva OpenID.',
	'form_login_openid_pass' => 'Použiť heslo',
	'form_login_statut_nouveau' => 'Pri prvom prihlásení musíte použiť heslo, ktoré ste dostali e-mailom',
	'form_pet_votre_openid' => 'Vaša adresa OpenID',

	// O
	'openid' => 'OpenID',

	// U
	'utilisateur_inconnu' => 'Neznámy používateľ na tejto stránke',

	// V
	'verif_refusee' => 'Potvrdenie zamietnuté'
);

?>
