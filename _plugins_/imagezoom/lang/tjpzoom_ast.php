<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tjpzoom?lang_cible=ast
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'descriptif' => 'Esta páxina permite configurar l\'estilu de la lente. Pue declarase un estilu personalizáu; ver <a href="http://plugins.spip.net/?page=article&id_article=48">la documentación en llinia</a>.', # MODIF

	// E
	'example' => 'Resultau gráficu',

	// N
	'nostyle' => 'Ensin Estilu',

	// O
	'other' => 'Otru',

	// S
	'size_vignette' => 'Tamañu Másimu de la Viñeta',
	'style' => 'Estilu de la Lente',

	// T
	'titre' => 'Iguar la Lente'
);

?>
