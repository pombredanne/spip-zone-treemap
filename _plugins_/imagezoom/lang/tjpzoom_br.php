<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'descriptif' => '<NEW>Dre ar bajenn-mañ e c\'haller kefluniañ stil ar werenn-greskiñ. Gallout a c\'haller disklêriañ ur stil personelaet; gwelet <a href="http://plugins.spip.net/?page=article&id_article=48">an teulioù skoazell enlinenn</a>.', # MODIF

	// E
	'example' => '<NEW>Disoc\'h grafek',

	// N
	'nostyle' => '<NEW>Distil',

	// O
	'other' => '<NEW>Traoù all',

	// S
	'size_vignette' => '<NEW>Ledander brasañ ar vignetenn',
	'style' => '<NEW>Stil ar werenn-greskiñ',

	// T
	'titre' => '<NEW>Kefluniadur ar werenn-greskiñ'
);

?>
