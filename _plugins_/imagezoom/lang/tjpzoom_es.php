<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tjpzoom?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'descriptif' => 'Esta página permite configurar el estilo de la lupa. Puede declararse un estilo personalizado; ver <a href="http://plugins.spip.net/?page=article&id_article=48">la documentación en línea</a>.', # MODIF

	// E
	'example' => 'Resultado gráfico',

	// N
	'nostyle' => 'Sin Estilo',

	// O
	'other' => 'Otro',

	// S
	'size_vignette' => 'Tamaño Máximo de la Viñeta',
	'style' => 'Estilo de la Lupa',

	// T
	'titre' => 'Configuración de la Lupa'
);

?>
