<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tjpzoom?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'descriptif' => 'Deze bladzijde maakt het mogelijk om de stijl van het vergrootglas te configureren. Een verpersoonlijkte stijl kan verklaard worden; <a href= " http://plugins.spip.net/?page=article&id_article=48 ">  zien de on-line documentatie</a>. Webmestres SPIP',

	// E
	'example' => 'Grafisch resultaat',

	// N
	'nostyle' => 'Zonder Stijl',

	// O
	'other' => 'Ander',

	// S
	'size_vignette' => 'Maximumbreedte van de zegel',
	'style' => 'Stijl van het Vergrootglas',

	// T
	'titre' => 'Configuratie van het Vergrootglas'
);

?>
