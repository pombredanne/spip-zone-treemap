<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tjpzoom?lang_cible=ro
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'descriptif' => 'Această pagină permite configurarea stilului lupei. Un stil personalizat poate fi declarat; vedeţi <a href="http://plugins.spip.net/?page=article&id_article=48">documentaţia on-line</a>.', # MODIF

	// E
	'example' => 'Rezultat grafic',

	// N
	'nostyle' => 'Fără stil',

	// O
	'other' => 'Altul',

	// S
	'size_vignette' => 'Mărimea maximă a vinietei',
	'style' => 'Stilul lupei',

	// T
	'titre' => 'Configuraţia lupei'
);

?>
