<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tjpzoom?lang_cible=fr_tu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'descriptif' => 'Cette page permet de configurer le style de la loupe. Un style personnalisé peut être déclaré ; voir <a href="http://plugins.spip.net/?page=article&id_article=48">la documentation en ligne</a>.', # MODIF

	// E
	'example' => 'Résultat graphique',

	// N
	'nostyle' => 'Sans Style',

	// O
	'other' => 'Autre',

	// S
	'size_vignette' => 'Largeur Maximale de la Vignette',
	'style' => 'Style de la Loupe',

	// T
	'titre' => 'Configuration de la Loupe'
);

?>
