<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tjpzoom?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'descriptif' => 'Esta página permite configurar o estilo da lupa. Um estilo personalizado pode ser declarado; ver <a href="http://plugins.spip.net/?page=article&id_article=48">a documentação online</a>.', # MODIF

	// E
	'example' => 'Resultado gráfico',

	// N
	'nostyle' => 'Sem Estilo',

	// O
	'other' => 'Outro',

	// S
	'size_vignette' => 'Largura Máxima da Miniatura',
	'style' => 'Estilo da Lupa',

	// T
	'titre' => 'Configuração da Lupa'
);

?>
