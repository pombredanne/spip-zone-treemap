<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tjpzoom?lang_cible=tr
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'descriptif' => 'Bu sayfa büyütecin stilini konfigüre etmeye yarar. Kişiselleştirilmiş bir stil tanımlanabilir; <a href="http://plugins.spip.net/?page=article&id_article=48">çevrimiçi makaleye bkz </a>.', # MODIF

	// E
	'example' => 'Grafik sonuç',

	// N
	'nostyle' => 'Stilsiz',

	// O
	'other' => 'Başka',

	// S
	'size_vignette' => 'İkonun Maksimum Genişliği',
	'style' => 'Büyüteç Stili',

	// T
	'titre' => 'Büyüteç Düzenlemesi'
);

?>
