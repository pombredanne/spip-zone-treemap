<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tjpzoom?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'descriptif' => 'اين صفحه به شما اجازه مي‌دهد شيوه‌ي ذره‌بين را پيكربندي كنيد. مي‌توان يك شيوه‌ي شخصي را پيكر‌بندي كرد: بنگريد به <a href="http://plugins.spip.net/?page=article&id_article=48"> سندهاي وصل‌خط</a>.', # MODIF

	// E
	'example' => 'نتيجه‌ي گرافيكي ',

	// N
	'nostyle' => 'بي‌‌شيوه',

	// O
	'other' => 'ديگر',

	// S
	'size_vignette' => 'بيشترين پهناي ريزنقش ',
	'style' => 'شيوه‌ي ذره‌بين ',

	// T
	'titre' => 'پيكربندي ذره‌بين'
);

?>
