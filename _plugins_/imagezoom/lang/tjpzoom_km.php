<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tjpzoom?lang_cible=km
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'descriptif' => 'ទំព័រនេះ អនុញ្ញាតអ្នក កំណត់ទំរង់ រចនាបថនៃឧបករពង្រីក។ រចនាបថផ្ទាល់ខ្លួន អាចត្រូវបានកំណត់ទំរង់៖ មើល <a href="http://plugins.spip.net/?page=article&id_article=48">កំរងឯកសារ លើបណ្តាញ</a>។', # MODIF

	// E
	'example' => 'លទ្ធផល ជាក្រាភិក',

	// N
	'nostyle' => 'គ្មានរចនាបថ',

	// O
	'other' => 'ដទៃ',

	// S
	'size_vignette' => 'ទទឹងអតិបរិមា នៃកូនរូបភាព',
	'style' => 'រចនាបថ នៃឧបករពង្រីក',

	// T
	'titre' => 'ការកំណត់ទំរង់ នៃឧបករពង្រីក'
);

?>
