<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tjpzoom?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'descriptif' => 'Aquesta pàgina permet configurar l\'estil de la lupa. Es pot declarar un estil personalitzat; veure <a href="http://plugins.spip.net/?page=article&id_article=48">la documentació en línia</a>.', # MODIF

	// E
	'example' => 'Resultat gràfic',

	// N
	'nostyle' => 'Sense estil',

	// O
	'other' => 'Altre',

	// S
	'size_vignette' => 'Amplada Màxima de la Vinyeta',
	'style' => 'Estil de la Lupa',

	// T
	'titre' => 'Configuració de la Lupa'
);

?>
