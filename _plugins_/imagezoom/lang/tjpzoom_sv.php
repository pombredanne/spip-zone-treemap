<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'descriptif' => 'Cette page permet de configurer le style de la loupe. Un style personalisé peut être déclaré; voir <a href="http://plugins.spip.net/?page=article&amp;id_article=48">la documentation en ligne</a>.', # NEW

	// E
	'example' => 'Résultat graphique', # NEW

	// N
	'nostyle' => 'Sans Style', # NEW

	// O
	'other' => 'Annan',

	// S
	'size_vignette' => 'Largeur Maximale de la Vignette', # NEW
	'style' => 'Style de la Loupe', # NEW

	// T
	'titre' => 'Configuration de la Loupe' # NEW
);

?>
