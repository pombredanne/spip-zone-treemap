<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tjpzoom?lang_cible=lb
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'descriptif' => 'Dës Säit erlaabt de Style vun der Loupe anzestëllen. E personaliséierte Style kann gemaach gin; liest <a href="http://plugins.spip.net/?page=article&id_article=48">d\'Dokumentatioun</a>.', # MODIF

	// E
	'example' => 'Graphëscht Resultat',

	// N
	'nostyle' => 'Ouni Style',

	// O
	'other' => 'Anerer',

	// S
	'size_vignette' => 'Maximal-Breed vun der Vignette',
	'style' => 'Style vun der Loupe',

	// T
	'titre' => 'Astellen vun der Loupe'
);

?>
