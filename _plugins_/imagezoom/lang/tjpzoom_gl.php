<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tjpzoom?lang_cible=gl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'descriptif' => 'Esta páxina permite configurar o estilo da lupa. Pódese indicar un estilo personalizado; vexa <a href="http://plugins.spip.net/?page=article&id_article=48">a documentación en liña</a>.', # MODIF

	// E
	'example' => 'Resultado gráfico',

	// N
	'nostyle' => 'Sen estilo',

	// O
	'other' => 'Outro',

	// S
	'size_vignette' => 'Largura máxima da viñeta',
	'style' => 'Estilo da lupa',

	// T
	'titre' => 'Configuración da lupa'
);

?>
