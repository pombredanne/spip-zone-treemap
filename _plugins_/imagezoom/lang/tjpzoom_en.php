<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tjpzoom?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'descriptif' => 'This page allows you to configure the style of the magnifier. A personal style can be configured: see the <a href="http://plugins.spip.net/?page=article&id_article=48">online documentation</a>.', # MODIF

	// E
	'example' => 'Graphical result',

	// N
	'nostyle' => 'No style',

	// O
	'other' => 'Other',

	// S
	'size_vignette' => 'Maximum width of the thumbnail',
	'style' => 'Style of the Magnifier',

	// T
	'titre' => 'Configuration of the Magnifier'
);

?>
