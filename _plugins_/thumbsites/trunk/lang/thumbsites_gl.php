<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_copier_comme_logo' => 'Définir comme logo', # NEW
	'bouton_effacer' => 'Desfacer',
	'bouton_rafraichir' => 'Rafraichir la vignette', # NEW
	'bouton_valider' => 'Validar',
	'bulle_bouton_effacer' => 'Desfacer os resultados de volver aos valores de partida',
	'bulle_bouton_valider' => 'Validar as saídas',

	// C
	'cfg_descriptif' => 'Cette page vous permez de configurer le plugin Thumbsites, et, en particulier, de choisir le serveur de vignettes. Pour en savoir plus, consultez la <a href="http://www.spip-contrib.net/?article2584">documentation sur contrib</a>', # NEW
	'cfg_inf_apercite_parametres' => 'Veuillez saisir vos données personnelles:', # NEW
	'cfg_inf_apercite_presentation' => '<a href="http://www.apercite.fr">apercite.fr</a> propose un service gratuit qui ne nécessite pas d\'identification.', # NEW
	'cfg_inf_girafa_description' => 'Les vignettes générées seront au maximum de 160 x 120 pixels (cette valeur peut être modifiée sur le site de Girafa)', # NEW
	'cfg_inf_girafa_parametres' => 'Veuillez saisir vos données personnelles:', # NEW
	'cfg_inf_girafa_presentation' => 'Pour utiliser ce service vous devez être inscrit sur le site <a href="http://www.girafa.com">Girafa</a>. Il existe une version gratuite si vous souhaitez moins de 2000 captures par jour.', # NEW
	'cfg_inf_miwin_parametres' => 'Veuillez saisir vos données personnelles:', # NEW
	'cfg_inf_miwin_presentation' => 'Pour utiliser ce service vous devez mettre un lien vers le site <a href="http://thumbs.miwim.fr">Miwin</a>. La présence de ce lien est vérifié périodiquement par un script.', # NEW
	'cfg_inf_rotothumb_parametres' => 'Veuillez saisir vos données personnelles:', # NEW
	'cfg_inf_rotothumb_presentation' => 'Pour utiliser ce service vous ne devez pas vous enregistrer mais vous devez mettre un lien vers le site <a href="http://www.robothumb.com">Robothumb</a>. La présence de ce lien est vérifié périodiquement par leur soin.', # NEW
	'cfg_inf_thumbshots_de_description' => 'Les vignettes générées seront de 120 x 90 pixels.', # NEW
	'cfg_inf_thumbshots_de_presentation' => '<a href="http://www.thumbshots.de">Thumbshots.de</a> propose un service équivalent à Thumbshots.com. Bien que les services soient identiques, ce sont bien 2 sites distincts.', # NEW
	'cfg_inf_thumbshots_description' => 'Les vignettes générées seront de 120 x 90 pixels.', # NEW
	'cfg_inf_thumbshots_presentation' => '<a href="http://www.thumbshots.com">Thumbshots.com</a> propose un service gratuit qui ne nécessite pas d\'identification.', # NEW
	'cfg_inf_websnapr_parametres' => 'Veuillez saisir vos données personnelles:', # NEW
	'cfg_inf_websnapr_presentation' => 'Pour utiliser ce service vous devez être inscrit sur le site <a href="http://www.websnapr.com">Websnapr</a>. La version gratuite permet de traiter de 250.000 captures de site par mois, à un rythme de 80 captures par heure.', # NEW
	'cfg_itm_serveur_apercite' => 'Apercite.fr', # NEW
	'cfg_itm_serveur_girafa' => 'Girafa', # NEW
	'cfg_itm_serveur_miwin' => 'Miwin', # NEW
	'cfg_itm_serveur_robothumb' => 'Robothumb', # NEW
	'cfg_itm_serveur_thumbshots' => 'Thumbshots.com', # NEW
	'cfg_itm_serveur_thumbshots_de' => 'Thumbshots.de', # NEW
	'cfg_itm_serveur_websnapr' => 'Websnapr', # NEW
	'cfg_itm_taille_vignette' => '@taille@ pixels', # NEW
	'cfg_lbl_choix_serveur' => 'Choisissez le serveur de vignettes', # NEW
	'cfg_lbl_cle' => 'Votre clé', # NEW
	'cfg_lbl_duree_cache' => 'Durée du cache (en jours)', # NEW
	'cfg_lbl_identifiant' => 'Votre identifiant', # NEW
	'cfg_lbl_signature' => 'Votre signature', # NEW
	'cfg_lbl_taille_vignette' => 'Taille des vignettes', # NEW
	'cfg_lgd_cache' => 'Cache', # NEW
	'cfg_lgd_choix_serveur' => 'Serveur', # NEW

	// T
	'titre_thumbshot_site' => 'VIÑETA DO SITIO'
);

?>
