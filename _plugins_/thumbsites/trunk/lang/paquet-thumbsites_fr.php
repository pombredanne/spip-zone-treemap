<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-thumbsites
// Langue: fr
// Date: 08-04-2012 19:09:00
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
	
// T
	'thumbsites_description' => 'Thumbsites fournit un système de capture d\'écran de site générée par des serveurs dédiés. Le plugin propose des balises, des filtres et des modèles pour afficher la vignette d\'un site donné. Ce plugin gère aussi un cache qui permet de pallier les indisponibilités fréquentes des serveurs et d\'accélérer les affichages. Une configuration est disponible dans l\'espace privé.',
	'thumbsites_slogan' => 'Une vignette pour les objets sites',
);
?>