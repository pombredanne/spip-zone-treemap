<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/thumbsites?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_copier_comme_logo' => 'Définir comme logo', # NEW
	'bouton_rafraichir' => 'Rafraichir la vignette', # NEW

	// C
	'cfg_descriptif' => 'Esta página permite configurar o plugin Thumbsites e, em particular, escolher o servidor de ícones. Para saber mais, consulte a <a href="http://www.spip-contrib.net/?article2584">documentação sobre contrib</a>',
	'cfg_inf_apercite_presentation' => '<a href="http://www.apercite.fr">apercite.fr</a> propose un service gratuit qui ne nécessite pas d\'identification.', # NEW
	'cfg_inf_choix_serveur' => 'Choisissez le serveur qui fournira les vignettes de vos sites référencés et compléter éventuellement son paramétrage.', # NEW
	'cfg_inf_miwim_presentation' => 'Pour utiliser ce service vous devez mettre un lien vers le site <a href="http://thumbs.miwim.fr">Miwim</a>. La présence de ce lien est vérifié périodiquement par un script.', # NEW
	'cfg_inf_rotothumb_presentation' => 'Para usar este serviço, você não precisa se registrar mas deverá incluir um link para o site <a href="http://www.robothumb.com">Robothumb</a>. A presença deste link é verificada periodicamente por eles.',
	'cfg_inf_thumbshots_de_presentation' => '<a href="http://www.thumbshots.de">Thumbshots.de</a> propõe um serviço equivalente ao do Thumbshots.com. Embora os serviços sejam identicos, são dois sites distintos.',
	'cfg_inf_thumbshots_presentation' => '<a href="http://www.thumbshots.com">Thumbshots.com</a> propõe um serviço gratuito que não exige identificação.',
	'cfg_inf_websnapr_presentation' => 'Para usar este serviço você deve se registrar no site <a href="http://www.websnapr.com">Websnapr</a>. A versão gratuita permite tratar 250.000 capturas de sites por mês, num ritmo de 80 capturas por hora.',
	'cfg_itm_serveur_apercite' => 'Apercite.fr', # NEW
	'cfg_itm_serveur_miwim' => 'Miwim', # NEW
	'cfg_itm_serveur_robothumb' => 'Robothumb',
	'cfg_itm_serveur_thumbshots' => 'Thumbshots.com',
	'cfg_itm_serveur_thumbshots_de' => 'Thumbshots.de',
	'cfg_itm_serveur_websnapr' => 'Websnapr',
	'cfg_itm_taille_vignette' => '@taille@ pixels',
	'cfg_lbl_cle' => 'Sua chave',
	'cfg_lbl_duree_cache' => 'Durée du cache (en jours)', # NEW
	'cfg_lbl_serveur' => 'Serveur', # NEW
	'cfg_lbl_taille_vignette' => 'Tamanho dos ícones',
	'cfg_lbl_usage' => 'Conditions d\'utilisation', # NEW
	'cfg_lgd_cache' => 'Cache', # NEW
	'cfg_lgd_choix_serveur' => 'Servidor', # MODIF
	'cfg_titre_form' => 'Configurer Thumbsites', # NEW
	'cfg_titre_page' => 'Thumbsites', # NEW

	// T
	'titre_thumbshot_site' => 'ÍCONE DO SITE'
);

?>
