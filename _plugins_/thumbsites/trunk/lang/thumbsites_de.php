<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/thumbsites?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_copier_comme_logo' => 'Définir comme logo', # NEW
	'bouton_rafraichir' => 'Rafraichir la vignette', # NEW

	// C
	'cfg_descriptif' => 'Auf dieser Seite können sie das Plugin Thumbsites konfigurieren und den Thumbnailserver einstellen. Weitere Informationen: <a href="http://www.spip-contrib.net/?article2584">Dokumentation auf Spip-Contrib</a>',
	'cfg_inf_apercite_presentation' => '<a href="http://www.apercite.fr">apercite.fr</a> propose un service gratuit qui ne nécessite pas d\'identification.', # NEW
	'cfg_inf_choix_serveur' => 'Choisissez le serveur qui fournira les vignettes de vos sites référencés et compléter éventuellement son paramétrage.', # NEW
	'cfg_inf_miwim_presentation' => 'Pour utiliser ce service vous devez mettre un lien vers le site <a href="http://thumbs.miwim.fr">Miwim</a>. La présence de ce lien est vérifié périodiquement par un script.', # NEW
	'cfg_inf_rotothumb_presentation' => 'Um diesen Dienst zu nutzen, brauchen sie sich nicht anzumelden, abe sie müssen einen Link zur Site <a href="http://www.robothumb.com">Robothumb</a> setzen. Die Existenz dieses Links wird vom Betreiber überprüft.',
	'cfg_inf_thumbshots_de_presentation' => '<a href="http://www.thumbshots.de">Thumbshots.de</a> bietet einen Dienst, der Thumbshots.com entspricht. Auch wenn die Dienste identisch sind, handelt es sich doch um zwei Websites.',
	'cfg_inf_thumbshots_presentation' => '<a href="http://www.thumbshots.com">Thumbshots.com</a> bietet einen kostenlosen Dienst ohne Anmeldung.',
	'cfg_inf_websnapr_presentation' => 'Um diesen Dienst zu nutzen, müssen sie sich bei <a href="http://www.websnapr.com">Websnapr</a> anmelden. Die kostenlose Version erlaubt 250.000 Screenshots pro Monat und schafft bis zu 80 neue Screenshots pro Stunde.',
	'cfg_itm_serveur_apercite' => 'Apercite.fr', # NEW
	'cfg_itm_serveur_miwim' => 'Miwim', # NEW
	'cfg_itm_serveur_robothumb' => 'Robothumb',
	'cfg_itm_serveur_thumbshots' => 'Thumbshots.com',
	'cfg_itm_serveur_thumbshots_de' => 'Thumbshots.de',
	'cfg_itm_serveur_websnapr' => 'Websnapr',
	'cfg_itm_taille_vignette' => '@taille@ pixels',
	'cfg_lbl_cle' => 'Ihr Schlüssel',
	'cfg_lbl_duree_cache' => 'Durée du cache (en jours)', # NEW
	'cfg_lbl_serveur' => 'Serveur', # NEW
	'cfg_lbl_taille_vignette' => 'Größe der Thumbnails',
	'cfg_lbl_usage' => 'Conditions d\'utilisation', # NEW
	'cfg_lgd_cache' => 'Cache', # NEW
	'cfg_lgd_choix_serveur' => 'Server', # MODIF
	'cfg_titre_form' => 'Configurer Thumbsites', # NEW
	'cfg_titre_page' => 'Thumbsites', # NEW

	// T
	'titre_thumbshot_site' => 'THUMBNAIL DER SITE'
);

?>
