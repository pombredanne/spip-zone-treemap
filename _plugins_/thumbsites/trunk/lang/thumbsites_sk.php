<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/thumbsites?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_copier_comme_logo' => 'Nastaviť ako logo',
	'bouton_rafraichir' => 'Obnoviť miniatúru',

	// C
	'cfg_descriptif' => 'Táto stránka vám umožňuje nastaviť zásuvný modul Thumbsites a najmä vybrať si server pre miniatúry. Ak chcete zistiť viac informácií, pozrite si <a href="http://www.spip-contrib.net/?article2584">documentation sur contrib</a>dokumentáciu na contribe</a>',
	'cfg_inf_apercite_presentation' => '<a href="http://www.apercite.fr">apercite.fr</ a> ponúka bezplatnú službu, ktorá si nevyžaduje identifikáciu.',
	'cfg_inf_choix_serveur' => 'Vyberte si server, čo poskytuje miniatúry stránok, na ktoré odkazujete, a prípadne vyplňte jeho parametre ',
	'cfg_inf_miwim_presentation' => 'Na to, aby ste mohli využívať túto službu, musíte pridať odkaz na stránku <a href="http://thumbs.miwim.fr">Miwim.</a> Existenciu tohto odkazu pravidelne kontroluje skript.',
	'cfg_inf_rotothumb_presentation' => 'Ak chcete využívať túto službu, nemusíte sa zaregistrovať, ale musíte vytvoriť odkaz na stránku <a href="http://www.robothumb.com">Robothumb.</a> Pokračujúca existencia tohto odkazu sa pravidelne overuje skriptami.',
	'cfg_inf_thumbshots_de_presentation' => '<a href="http://www.thumbshots.de">Thumbshots.de</a> ponúka podobnú službu ako Thumbshots.com. Napriek tomu, že sú tieto služby rovnaké, sú to dve rôzne stránky.',
	'cfg_inf_thumbshots_presentation' => '<a href="http://www.thumbshots.com">Thumbshots.com</a> ponúka bezplatnú službu, ktorá si nevyžaduje registráciu.',
	'cfg_inf_websnapr_presentation' => 'Ak chcete využívať túto službu, musíte sa zaregistrovať na stránke <a href="http://www.websnapr.com">Websnapr.</a> Bezplatná verzia umožňuje spracovanie 250 000 snímok obrazovky zo stránky za mesiac maximálnou rýchlosťou 80 snímok za hodinu.',
	'cfg_itm_serveur_apercite' => 'Apercite.fr',
	'cfg_itm_serveur_miwim' => 'Miwim',
	'cfg_itm_serveur_robothumb' => 'Robothumb',
	'cfg_itm_serveur_thumbshots' => 'Thumbshots.com',
	'cfg_itm_serveur_thumbshots_de' => 'Thumbshots.de',
	'cfg_itm_serveur_websnapr' => 'Websnapr',
	'cfg_itm_taille_vignette' => '@taille@ pixelov',
	'cfg_lbl_cle' => 'Váš kľúč',
	'cfg_lbl_duree_cache' => 'Trvanie cache (v dňoch)',
	'cfg_lbl_serveur' => 'Server',
	'cfg_lbl_taille_vignette' => 'Veľkosť miniatúr',
	'cfg_lbl_usage' => 'Podmienky používania',
	'cfg_lgd_cache' => 'Cache',
	'cfg_lgd_choix_serveur' => 'Služba',
	'cfg_titre_form' => 'Nastaviť Thumbsites',
	'cfg_titre_page' => 'Thumbsites',

	// T
	'titre_thumbshot_site' => 'MINIATÚRA STRÁNKY'
);

?>
