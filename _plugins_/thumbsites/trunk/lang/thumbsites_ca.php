<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/thumbsites?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_copier_comme_logo' => 'Définir comme logo', # NEW
	'bouton_rafraichir' => 'Rafraichir la vignette', # NEW

	// C
	'cfg_descriptif' => 'Aquesta pàgina us permet configurar el plugin Thumbsites, i, en particular, escollir el servidor de vinyetes. Per saber-ne més, consulteu la <a href="http://www.spip-contrib.net/?article2584">documentació a contrib</a>',
	'cfg_inf_apercite_presentation' => '<a href="http://www.apercite.fr">apercite.fr</a> propose un service gratuit qui ne nécessite pas d\'identification.', # NEW
	'cfg_inf_choix_serveur' => 'Choisissez le serveur qui fournira les vignettes de vos sites référencés et compléter éventuellement son paramétrage.', # NEW
	'cfg_inf_miwim_presentation' => 'Pour utiliser ce service vous devez mettre un lien vers le site <a href="http://thumbs.miwim.fr">Miwim</a>. La présence de ce lien est vérifié périodiquement par un script.', # NEW
	'cfg_inf_rotothumb_presentation' => 'Per utilitzar aquest servei heu de registrar-vos però haureu de posar un enllaç cap al lloc <a href="http://www.robothumb.com">Robothumb</a>. La presència d\'aquest vincle és verificada periòdicament per la seva cura. ',
	'cfg_inf_thumbshots_de_presentation' => '<a href="http://www.thumbshots.de">Thumbshots.de</a> proposa un servei equivalent a Thumbshots.com. Encara que els serveis siguin idèntics, són 2 llocs ben diferents.',
	'cfg_inf_thumbshots_presentation' => '<a href="http://www.thumbshots.com">Thumbshots.com</a> proposa un servei gratuït que no necessita identificació.',
	'cfg_inf_websnapr_presentation' => 'Per utilitzar aquest servei heu d\'estar registrat al lloc <a href="http://www.websnapr.com">Websnapr</a>. La versió gratuïta permet processar unes 250.000 captures del lloc per mes, a un ritme de 80 captures per hora.',
	'cfg_itm_serveur_apercite' => 'Apercite.fr', # NEW
	'cfg_itm_serveur_miwim' => 'Miwim', # NEW
	'cfg_itm_serveur_robothumb' => 'Robothumb',
	'cfg_itm_serveur_thumbshots' => 'Thumbshots.com',
	'cfg_itm_serveur_thumbshots_de' => 'Thumbshots.de',
	'cfg_itm_serveur_websnapr' => 'Websnapr',
	'cfg_itm_taille_vignette' => '@taille@ píxels',
	'cfg_lbl_cle' => 'La vostra clau',
	'cfg_lbl_duree_cache' => 'Durée du cache (en jours)', # NEW
	'cfg_lbl_serveur' => 'Serveur', # NEW
	'cfg_lbl_taille_vignette' => 'Mida de les vinyetes ',
	'cfg_lbl_usage' => 'Conditions d\'utilisation', # NEW
	'cfg_lgd_cache' => 'Cache', # NEW
	'cfg_lgd_choix_serveur' => 'Servidor', # MODIF
	'cfg_titre_form' => 'Configurer Thumbsites', # NEW
	'cfg_titre_page' => 'Thumbsites', # NEW

	// T
	'titre_thumbshot_site' => 'VINYETA DEL LLOC'
);

?>
