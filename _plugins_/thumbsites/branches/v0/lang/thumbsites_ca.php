<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/thumbsites?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_copier_comme_logo' => 'Définir comme logo', # NEW
	'bouton_effacer' => 'Esborrar',
	'bouton_rafraichir' => 'Rafraichir la vignette', # NEW
	'bouton_valider' => 'Validar',
	'bulle_bouton_effacer' => 'Esborrar les entrades i tornar als valors inicials',
	'bulle_bouton_valider' => 'Validar les dades',

	// C
	'cfg_descriptif' => 'Aquesta pàgina us permet configurar el plugin Thumbsites, i, en particular, escollir el servidor de vinyetes. Per saber-ne més, consulteu la <a href="http://www.spip-contrib.net/?article2584">documentació a contrib</a>',
	'cfg_inf_apercite_parametres' => 'Veuillez saisir vos données personnelles:', # NEW
	'cfg_inf_apercite_presentation' => '<a href="http://www.apercite.fr">apercite.fr</a> propose un service gratuit qui ne nécessite pas d\'identification.', # NEW
	'cfg_inf_girafa_description' => 'Les vinyetes generades seran com a molt de 160 x 120 píxels (aquest valor es pot modificar al lloc de Girafa)',
	'cfg_inf_girafa_parametres' => 'Introduïu les vostres dades personals:',
	'cfg_inf_girafa_presentation' => 'Per utilitzar aquest servei haureu d\'estar registrats al lloc <a href="http://www.girafa.com">Girafa</a>. Si voleu fer menys de 2000 captures per dia, hi ha una versió gratuïta.',
	'cfg_inf_miwin_parametres' => 'Introduïu les vostres dades personals:',
	'cfg_inf_miwin_presentation' => 'Per utilitzar aquest servei, heu de posar un enllaç cap al lloc <a href="http://thumbs.miwim.fr">Miwin</a>. La presència d\'aquest enllaç es verifica periòdicament mitjançant un script.',
	'cfg_inf_rotothumb_parametres' => 'Introduïu les vostres dades personals:',
	'cfg_inf_rotothumb_presentation' => 'Per utilitzar aquest servei heu de registrar-vos però haureu de posar un enllaç cap al lloc <a href="http://www.robothumb.com">Robothumb</a>. La presència d\'aquest vincle és verificada periòdicament per la seva cura. ',
	'cfg_inf_thumbshots_de_description' => 'Les vinyetes generades seran de 120 x 90 píxels.',
	'cfg_inf_thumbshots_de_presentation' => '<a href="http://www.thumbshots.de">Thumbshots.de</a> proposa un servei equivalent a Thumbshots.com. Encara que els serveis siguin idèntics, són 2 llocs ben diferents.',
	'cfg_inf_thumbshots_description' => 'Les vinyetes generades seran de 120 x 90 píxels.',
	'cfg_inf_thumbshots_presentation' => '<a href="http://www.thumbshots.com">Thumbshots.com</a> proposa un servei gratuït que no necessita identificació.',
	'cfg_inf_websnapr_parametres' => 'Vulgueu introduir les vostres dades personals:',
	'cfg_inf_websnapr_presentation' => 'Per utilitzar aquest servei heu d\'estar registrat al lloc <a href="http://www.websnapr.com">Websnapr</a>. La versió gratuïta permet processar unes 250.000 captures del lloc per mes, a un ritme de 80 captures per hora.',
	'cfg_itm_serveur_apercite' => 'Apercite.fr', # NEW
	'cfg_itm_serveur_girafa' => 'Girafa',
	'cfg_itm_serveur_miwin' => 'Miwin',
	'cfg_itm_serveur_robothumb' => 'Robothumb',
	'cfg_itm_serveur_thumbshots' => 'Thumbshots.com',
	'cfg_itm_serveur_thumbshots_de' => 'Thumbshots.de',
	'cfg_itm_serveur_websnapr' => 'Websnapr',
	'cfg_itm_taille_vignette' => '@taille@ píxels',
	'cfg_lbl_choix_serveur' => 'Escolliu el servidor de vinyetes',
	'cfg_lbl_cle' => 'La vostra clau',
	'cfg_lbl_duree_cache' => 'Durée du cache (en jours)', # NEW
	'cfg_lbl_identifiant' => 'El vostre identificador ',
	'cfg_lbl_signature' => 'La vostra signatura',
	'cfg_lbl_taille_vignette' => 'Mida de les vinyetes ',
	'cfg_lgd_cache' => 'Cache', # NEW
	'cfg_lgd_choix_serveur' => 'Servidor',

	// T
	'titre_thumbshot_site' => 'VINYETA DEL LLOC'
);

?>
