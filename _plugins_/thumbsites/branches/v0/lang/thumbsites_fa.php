<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/thumbsites?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_copier_comme_logo' => 'Définir comme logo', # NEW
	'bouton_effacer' => 'حذف ',
	'bouton_rafraichir' => 'تازه‌سازي صورتك',
	'bouton_valider' => 'تأييد ',
	'bulle_bouton_effacer' => 'حذف كردن اقلام و بازگشت به مقادير اوليه',
	'bulle_bouton_valider' => 'تأييد اقلام',

	// C
	'cfg_descriptif' => 'اين صفحه به شما اجازه مي‌دهد تا پلاگين تام‌سايتز (Thumbsites) را پيكربندي كنيد، و به ويژه، سرور ريزنش‌ها را انتخاب كنيد. براي اطلاعات بيشتر به اينجا مراجعه كنيد: <a href="http://www.spip-contrib.net/?article2584">documentation sur contrib</a>',
	'cfg_inf_apercite_parametres' => 'مي‌توانيد داده‌هاي شخصي را وارد كنيد:',
	'cfg_inf_apercite_presentation' => '<a href="http://www.apercite.fr">apercite.fr</a> خدمات رايگاني را پيشنهاد مي‌دهد كه نيازي به شناسايي ندارد.',
	'cfg_inf_girafa_description' => 'ريزنقش‌هاي خلق شده حداكثر 160x120 پيكسل خواهند بود(اين مقادرمي‌تواند در سايت Girafa اصلاح شود)',
	'cfg_inf_girafa_parametres' => 'لطفاً داده‌هاي شخصي خود را وارد كنيد',
	'cfg_inf_girafa_presentation' => 'براي استفاده از اين خدمت بايد در سايت <a href="http://www.girafa.com">Girafa</a> ثبت نام كنيد. اگر روزانه به كمتر از 200 كاپچر (گيراندازي) احتياج داشته باشيد نسخه‌ي رايگان نيز وجود دارد.',
	'cfg_inf_miwin_parametres' => 'لطفاً داده‌هاي شخصي خود را وارد كنيد',
	'cfg_inf_miwin_presentation' => 'براي استفاده از اين خدمت بايد يك پيوند با سايت  <a href="http://thumbs.miwim.fr">Miwin</a> ايجاد كنيد. تداوم اين پيوند به صورت دوره‌اي به وسيله‌ي يك نويسان (اسكريپت) بررسي مي‌شود.   ',
	'cfg_inf_rotothumb_parametres' => 'لطفاً داده‌هاي شخصي خود را وارد كنيد.',
	'cfg_inf_rotothumb_presentation' => 'براي استفاده از اين خدمت لازم نيست ثبت نام كنيد اما بايد يك پيوند با سايت <a href="http://www.robothumb.com">Robothumb</a> برقرار كنيد. تدوام اين پيوند به صورت دوره‌اي توسط سرور‌هاي خودشان بررسي مي‌شود. ',
	'cfg_inf_thumbshots_de_description' => 'ريزنقش‌هاي ايجاد شده 120 در 90 پيكسل خواهند بود.',
	'cfg_inf_thumbshots_de_presentation' => '<a href="http://www.thumbshots.de">Thumbshots.de</a> خدمتي معادل سايت  Thumbshots.comارايه مي‌دهد. هرچند اين دو خدمت يكي هستند اما عملاً دو سايت متمايز مي‌باشند. ',
	'cfg_inf_thumbshots_description' => 'ريزنقش‌هاي ايجاد شده 120 در 90 پيكسل خواهند بود.',
	'cfg_inf_thumbshots_presentation' => '<a href="http://www.thumbshots.com">Thumbshots.com</a> خدمتي رايگان ارايه مي‌دهد كه به شناسايي نيازي نيست. ',
	'cfg_inf_websnapr_parametres' => 'لطفاً داده‌هاي شخصي خود را وارد كنيد:',
	'cfg_inf_websnapr_presentation' => 'براي استفاده از اين خدمت بايد در سايت <a href="http://www.websnapr.com">Websnapr</a> ثبت نام كنيد. نسخه‌ي رايگان مي‌تواند 250 هزار كاپچبر (گيراندازي) سايت را در طول يك ماه و با آهنگ 80 كاپچر در ساعت پردازش كند. ',
	'cfg_itm_serveur_apercite' => 'Apercite.fr',
	'cfg_itm_serveur_girafa' => 'Girafa',
	'cfg_itm_serveur_miwin' => 'Miwin',
	'cfg_itm_serveur_robothumb' => 'Robothumb',
	'cfg_itm_serveur_thumbshots' => 'Thumbshots.com',
	'cfg_itm_serveur_thumbshots_de' => 'Thumbshots.de',
	'cfg_itm_serveur_websnapr' => 'Websnapr',
	'cfg_itm_taille_vignette' => '@taille@ پيكسل',
	'cfg_lbl_choix_serveur' => 'سرور ريزنقش‌ها را انتخاب كنيد',
	'cfg_lbl_cle' => 'كليد شما',
	'cfg_lbl_duree_cache' => 'دوره‌ي حافظه‌ي نزديك (به روز)',
	'cfg_lbl_identifiant' => 'شناسه‌ي شما',
	'cfg_lbl_signature' => 'امضاي شما',
	'cfg_lbl_taille_vignette' => 'اندازه‌ي ريزنقش‌ها',
	'cfg_lgd_cache' => 'حافظه‌‌ي نزديك',
	'cfg_lgd_choix_serveur' => 'سرور',

	// T
	'titre_thumbshot_site' => 'ريزنقش سايت'
);

?>
