<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/thumbsites?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_copier_comme_logo' => 'Définir comme logo', # NEW
	'bouton_effacer' => 'Limpar',
	'bouton_rafraichir' => 'Rafraichir la vignette', # NEW
	'bouton_valider' => 'Validar',
	'bulle_bouton_effacer' => 'Limpar as entradas e voltar aos valores iniciais',
	'bulle_bouton_valider' => 'Validar as entradas',

	// C
	'cfg_descriptif' => 'Esta página permite configurar o plugin Thumbsites e, em particular, escolher o servidor de ícones. Para saber mais, consulte a <a href="http://www.spip-contrib.net/?article2584">documentação sobre contrib</a>',
	'cfg_inf_apercite_parametres' => 'Veuillez saisir vos données personnelles:', # NEW
	'cfg_inf_apercite_presentation' => '<a href="http://www.apercite.fr">apercite.fr</a> propose un service gratuit qui ne nécessite pas d\'identification.', # NEW
	'cfg_inf_girafa_description' => 'Os ícone gerados terão no máximo 160 x 120 pixels (este valor pode ser alterado no site do Girafa)',
	'cfg_inf_girafa_parametres' => 'Por favor, informe os seus dados pessoais:',
	'cfg_inf_girafa_presentation' => 'Para usar este serviço, voc6e deve se inscrever no site <a href="http://www.girafa.com">Girafa</a>. Existe uma versão gratuita se você for usar menos de 2000 capturas por dia.',
	'cfg_inf_miwin_parametres' => 'Por favor, informe os seus dados pessoais:',
	'cfg_inf_miwin_presentation' => 'Para usar este serviço, voc6e deve incluir um link para o site <a href="http://thumbs.miwim.fr">Miwin</a>. A presença deste link é verificada periodicamente por um script.',
	'cfg_inf_rotothumb_parametres' => 'Por favor, informe os seus dados pessoais:',
	'cfg_inf_rotothumb_presentation' => 'Para usar este serviço, você não precisa se registrar mas deverá incluir um link para o site <a href="http://www.robothumb.com">Robothumb</a>. A presença deste link é verificada periodicamente por eles.',
	'cfg_inf_thumbshots_de_description' => 'Os ícones gerados terão 120 x 90 pixels.',
	'cfg_inf_thumbshots_de_presentation' => '<a href="http://www.thumbshots.de">Thumbshots.de</a> propõe um serviço equivalente ao do Thumbshots.com. Embora os serviços sejam identicos, são dois sites distintos.',
	'cfg_inf_thumbshots_description' => 'Os ícone gerados terão 120 x 90 pixels.',
	'cfg_inf_thumbshots_presentation' => '<a href="http://www.thumbshots.com">Thumbshots.com</a> propõe um serviço gratuito que não exige identificação.',
	'cfg_inf_websnapr_parametres' => 'Por favor, informe os seus dados pessoais:',
	'cfg_inf_websnapr_presentation' => 'Para usar este serviço você deve se registrar no site <a href="http://www.websnapr.com">Websnapr</a>. A versão gratuita permite tratar 250.000 capturas de sites por mês, num ritmo de 80 capturas por hora.',
	'cfg_itm_serveur_apercite' => 'Apercite.fr', # NEW
	'cfg_itm_serveur_girafa' => 'Girafa',
	'cfg_itm_serveur_miwin' => 'Miwin',
	'cfg_itm_serveur_robothumb' => 'Robothumb',
	'cfg_itm_serveur_thumbshots' => 'Thumbshots.com',
	'cfg_itm_serveur_thumbshots_de' => 'Thumbshots.de',
	'cfg_itm_serveur_websnapr' => 'Websnapr',
	'cfg_itm_taille_vignette' => '@taille@ pixels',
	'cfg_lbl_choix_serveur' => 'Escolha o servidor de ícones',
	'cfg_lbl_cle' => 'Sua chave',
	'cfg_lbl_duree_cache' => 'Durée du cache (en jours)', # NEW
	'cfg_lbl_identifiant' => 'Sua identificação',
	'cfg_lbl_signature' => 'Sua assinatura',
	'cfg_lbl_taille_vignette' => 'Tamanho dos ícones',
	'cfg_lgd_cache' => 'Cache', # NEW
	'cfg_lgd_choix_serveur' => 'Servidor',

	// T
	'titre_thumbshot_site' => 'ÍCONE DO SITE'
);

?>
