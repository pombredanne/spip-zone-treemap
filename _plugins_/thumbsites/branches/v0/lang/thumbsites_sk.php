<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/thumbsites?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_copier_comme_logo' => 'Nastaviť ako logo',
	'bouton_effacer' => 'Odstrániť',
	'bouton_rafraichir' => 'Obnoviť miniatúru',
	'bouton_valider' => 'Potvrdiť',
	'bulle_bouton_effacer' => 'Odstrániť vstupy a vrátiť sa k pôvodným hodnotám',
	'bulle_bouton_valider' => 'Potvrdiť vstupy',

	// C
	'cfg_descriptif' => 'Táto stránka vám umožňuje nastaviť zásuvný modul Thumbsites a najmä vybrať si server pre miniatúry. Ak chcete zistiť viac informácií, pozrite si <a href="http://www.spip-contrib.net/?article2584">documentation sur contrib</a>dokumentáciu na contribe</a>',
	'cfg_inf_apercite_parametres' => 'Prosím, zadajte svoje osobné údaje:',
	'cfg_inf_apercite_presentation' => '<a href="http://www.apercite.fr">apercite.fr</ a> ponúka bezplatnú službu, ktorá si nevyžaduje identifikáciu.',
	'cfg_inf_girafa_description' => 'Vytvorená miniatúra bude mať maximálnu veľkosť 160 x 120 pixelov (tieto hodnoty môžete zmeniť na stránke Girafy)',
	'cfg_inf_girafa_parametres' => 'Prosím, zadajte svoje osobné údaje:',
	'cfg_inf_girafa_presentation' => 'Ak chcete využívať túto službu, musíte sa zaregistrovať na stránke <a href="http://www.girafa.com">Girafa.</a> Existuje bezplatná verzia, ak potrebujete menej ako  2 000 snímok obrazovky denne.',
	'cfg_inf_miwin_parametres' => 'Prosím, zadajte svoje osobné údaje:',
	'cfg_inf_miwin_presentation' => 'Ak chcete využívať túto službu, musíte vytvoriť odkaz na stránku <a href="http://thumbs.miwim.fr">Miwin.</a> Pokračujúca existencia tohto odkazu  sa periodicky overuje skriptom.',
	'cfg_inf_rotothumb_parametres' => 'Prosím, zadajte svoje osobné údaje:',
	'cfg_inf_rotothumb_presentation' => 'Ak chcete využívať túto službu, nemusíte sa zaregistrovať, ale musíte vytvoriť odkaz na stránku <a href="http://www.robothumb.com">Robothumb.</a> Pokračujúca existencia tohto odkazu sa pravidelne overuje skriptami.',
	'cfg_inf_thumbshots_de_description' => 'Vygenerované miniatúry budú mať 120 x 90 pixelov.',
	'cfg_inf_thumbshots_de_presentation' => '<a href="http://www.thumbshots.de">Thumbshots.de</a> ponúka podobnú službu ako Thumbshots.com. Napriek tomu, že sú tieto služby rovnaké, sú to dve rôzne stránky.',
	'cfg_inf_thumbshots_description' => 'Vygenerované miniatúry budú mať 120 x 90 pixelov.',
	'cfg_inf_thumbshots_presentation' => '<a href="http://www.thumbshots.com">Thumbshots.com</a> ponúka bezplatnú službu, ktorá si nevyžaduje registráciu.',
	'cfg_inf_websnapr_parametres' => 'Prosím, zadajte svoje osobné údaje:',
	'cfg_inf_websnapr_presentation' => 'Ak chcete využívať túto službu, musíte sa zaregistrovať na stránke <a href="http://www.websnapr.com">Websnapr.</a> Bezplatná verzia umožňuje spracovanie 250 000 snímok obrazovky zo stránky za mesiac maximálnou rýchlosťou 80 snímok za hodinu.',
	'cfg_itm_serveur_apercite' => 'Apercite.fr',
	'cfg_itm_serveur_girafa' => 'Girafa',
	'cfg_itm_serveur_miwin' => 'Miwin',
	'cfg_itm_serveur_robothumb' => 'Robothumb',
	'cfg_itm_serveur_thumbshots' => 'Thumbshots.com',
	'cfg_itm_serveur_thumbshots_de' => 'Thumbshots.de',
	'cfg_itm_serveur_websnapr' => 'Websnapr',
	'cfg_itm_taille_vignette' => '@taille@ pixelov',
	'cfg_lbl_choix_serveur' => 'Vyberte server miniatúr',
	'cfg_lbl_cle' => 'Váš kľúč',
	'cfg_lbl_duree_cache' => 'Trvanie cache (v dňoch)',
	'cfg_lbl_identifiant' => 'Váš identifikátor',
	'cfg_lbl_signature' => 'Váš podpis',
	'cfg_lbl_taille_vignette' => 'Veľkosť miniatúr',
	'cfg_lgd_cache' => 'Cache',
	'cfg_lgd_choix_serveur' => 'Server',

	// T
	'titre_thumbshot_site' => 'MINIATÚRA STRÁNKY'
);

?>
