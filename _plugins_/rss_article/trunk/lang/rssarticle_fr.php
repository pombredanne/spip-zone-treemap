<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

  // A
  'article_origine' => 'Cet article est repris du site', 
  'activer_recopie_intro' => 'Flux RSS en Articles',
  'activer_recopie' => 'Copier les articles issus de ce flux RSS en articles SPIP',

  // C
  'citer_source' => 'Citer la source',
  'citer_source_oui' => 'Citer l\'URL de l\'article d\'origine dans l\'article import&eacute;',
  'configuration_rssarticle' => 'Flux RSS en articles',
  'copie_logo' => 'Recopier le logo du site comme logo d\'article',
  'cron_interval' => 'Fréquence de la copie des flux RSS en articles',
  'cron_interval_timer' => 'Intervalle en seconde ',

  // S
  'statut_article_importe' => 'Statut des articles import&eacute;s',
  'suivi_syndic' => 'Suivi de la syndication',
  'site_maj' => 'Option enregistrée',
  
  // E
  'email_alerte' => 'Pr&eacute;venir par email &agrave; chaque nouvelle syndication en articles ?',
  'email_alerte_email' => 'Si oui, sur quel email ? ',
  
  // I
  'install_rssarticle' => 'Cr&eacute;ation de la table spip_articles_syndic',
  
  // M
  'maj_manuelle' => 'La copie manuelle des derniers flux RSS en articles a été effectuée',
  'maj_recharge' => 'Relancer la copie manuelle',
  'mode' => 'Mode de fonctionnement',
  'mode_auto' => 'Mode automatique: tous les sites r&eacute;f&eacute;renc&eacute;s sont recopi&eacute;s en articles',
  'mode_manuel' => 'Mode manuel: vous selectionnez manuellement les sites r&eacute;f&eacute;renc&eacute;s qui doivent être recopi&eacute;s en articles',
  
  // T
  'titre_page_configurer_rssarticle' => 'Copie RSS en articles'
  
);


?>
