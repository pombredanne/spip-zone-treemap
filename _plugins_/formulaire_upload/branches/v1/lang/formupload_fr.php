<?php

	$GLOBALS[$GLOBALS['idx_lang']] = array(
	'cfg_boite_formupload' => 'Configuration du plugin Formulaire upload',
	'cfg_descr_formupload' => 'Ce plugin permet d\'avoir un formulaire public associant des documents aux &eacute;l&eacute;ments de SPIP.',
	'cfg_lbl_files_accepted' => 'Types de fichiers accept&eacute;s',
	'cfg_lbl_insert_code' => 'Code d\'insertion',
	'cfg_lbl_files_number' => 'Nombre de fichiers accept&eacute;s',
	'cfg_inf_files_accepted' => 'Mettre les extensions s&eacute;par&eacute;es par des pipes ex: pdf|txt|odt ou gif|jpg|png',
	'cfg_inf_insert_code' => 'Afficher le code d\'insertion des documents (si on a un autre formulaire pour cr&eacute;er des articles par exemple)',
	'cfg_inf_files_number' => 'L\'utilisateur ne pourra uploader que ce nombre de fichiers au maximum',
	'cfg_titre_formupload' => 'Formulaire upload',
	'choosefiles' => 'Choisir les fichiers &agrave; ajouter&nbsp;:',
  'erreur_copie_impossible' => 'Erreur: copie impossible',
	'nodocs' => 'Pas encore de document sur la zone de t&eacute;l&eacute;chargement',
	'insert_code' => 'Code &agrave; ins&eacute;rer dans la zone de texte&nbsp;:',
	'doc_delete' => 'Supprimer ce document',
	'doc_left' => 'Document flottant &agrave; gauche',
	'doc_center' => 'Document centr&eacute;',
	'doc_right' => 'Document flottant &agrave; droite',
	'img_left' => 'Image flottante &agrave; gauche',
	'img_center' => 'Image centr&eacute;e',
	'img_right' => 'Image flottante &agrave; droite',
	'msg_doc_added' => 'Document(s) ajout&eacute;(s)',
	'msg_doc_deleted' => 'Document(s) supprim&eacute;(s)',
	'msg_fichier_doublon' => 'Vous avez déjà sélectionné ce fichier.',
	'msg_format_non_autorise' => 'Format de fichier non autorisé.',
	'msg_nothing_to_do' => 'Rien &agrave; faire',
	'remove' => 'supprimer',
	'yourfiles' => 'Vos fichiers',
	'Z' => 'ZZzZZzzz'

);

?>