<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/pages?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucune_page' => 'There are no pages at the moment.',

	// C
	'convertir_article' => 'Convert to an article',
	'convertir_page' => 'Convert to a page',
	'creer_page' => 'Create a new page',

	// I
	'input_page' => 'une_page',

	// M
	'modifier_page' => 'Edit page:',

	// P
	'pages_uniques' => 'Unique pages',

	// T
	'titre_page' => 'Page',
	'toutes_les_pages' => 'All pages'
);

?>
