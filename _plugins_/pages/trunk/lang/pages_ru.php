<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/pages?lang_cible=ru
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucune_page' => 'Пока нет страниц.',

	// C
	'convertir_article' => 'Преобразовать в статью',
	'convertir_page' => 'Преобразовать в страницу',
	'creer_page' => 'Новая страница',

	// I
	'input_page' => 'une_page',

	// M
	'modifier_page' => 'Изменить страницу:',

	// P
	'pages_uniques' => 'Отдельные страницы',

	// T
	'titre_page' => 'Страница',
	'toutes_les_pages' => 'Все страницы'
);

?>
