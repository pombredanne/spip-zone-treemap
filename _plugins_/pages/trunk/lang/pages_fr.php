<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/pages/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucune_page' => 'Il n\'y a aucune page pour l\'instant.',

	// C
	'convertir_article' => 'Convertir en article',
	'convertir_page' => 'Convertir en page',
	'creer_page' => 'Créer une nouvelle page',

	// I
	'input_page' => 'une_page',

	// M
	'modifier_page' => 'Modifier la page :',

	// P
	'pages_uniques' => 'Pages uniques',

	// T
	'titre_page' => 'Page',
	'toutes_les_pages' => 'Toutes les pages'
);

?>
