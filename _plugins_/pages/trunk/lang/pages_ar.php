<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/pages?lang_cible=ar
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucune_page' => 'لا توجد صفحات في هذه اللحظة.',

	// C
	'convertir_article' => 'تحويل الى مقال',
	'convertir_page' => 'تحويل الى صفحة',
	'creer_page' => 'إنشاء صفحة جديدة',

	// I
	'input_page' => 'une_page',

	// M
	'modifier_page' => 'تغيير الصفحة :',

	// P
	'pages_uniques' => 'صفحات فريدة',

	// T
	'titre_page' => 'صفحة',
	'toutes_les_pages' => 'كل الصفحات'
);

?>
