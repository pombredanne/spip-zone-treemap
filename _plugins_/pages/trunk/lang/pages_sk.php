<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/pages?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucune_page' => 'Momentálne žiadne stránky neexistujú.',

	// C
	'convertir_article' => 'Zmeniť na článok',
	'convertir_page' => 'Zmeniť na stránku',
	'creer_page' => 'Vytvoriť novú stránku',

	// I
	'input_page' => 'une_page',

	// M
	'modifier_page' => 'Upraviť stránku:',

	// P
	'pages_uniques' => 'Jedinečné stránky',

	// T
	'titre_page' => 'Stránka',
	'toutes_les_pages' => 'Všetky stránky'
);

?>
