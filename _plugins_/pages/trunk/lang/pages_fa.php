<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/pages?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucune_page' => ' براي الأن هيچ صفحه‌ي نيست',

	// C
	'convertir_article' => 'تبديل به يك مقاله',
	'convertir_page' => 'تبديل به يك صفحه',
	'creer_page' => 'آفرينش يك صفحه‌ي نو',

	// I
	'input_page' => 'une_page',

	// M
	'modifier_page' => 'اصلاح صفحه: ',

	// P
	'pages_uniques' => 'صفحه‌هاي تك',

	// T
	'titre_page' => 'صفحه',
	'toutes_les_pages' => 'تمام صفه‌ها'
);

?>
