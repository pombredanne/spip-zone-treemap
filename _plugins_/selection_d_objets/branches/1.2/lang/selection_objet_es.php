<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/selection_objet?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'affichage_section' => 'Mostrar en:',
	'ajouter_lien_objet' => 'Seleccionar este objeto',

	// C
	'configurations' => 'Configuraciones',

	// E
	'explication_selection_rubrique_dest' => 'Las secciones dónde podrán ser listados los objetos semeccionados',

	// I
	'icone_creer_selection_objet' => 'Seleccionar un objet',
	'icone_modifier_selection_objet' => 'Modificar este objeto seleccionado',
	'info_1_selection_objet' => 'Un objeto seleccionado',
	'info_aucun_selection_objet' => 'Ningún objeto seleccionado',
	'info_nb_selection_objets' => '@nb@ objetos seleccionados',

	// L
	'label_descriptif' => 'Descriptivo',
	'label_id_objet' => 'Id objeto',
	'label_id_objet_dest' => 'Id objeto destino',
	'label_objet' => 'Objeto',
	'label_objet_dest' => 'Objeto destino',
	'label_ordre' => 'Orden',
	'label_titre' => 'Título',
	'label_url' => 'Url',

	// O
	'objet_destination_choisis' => 'Destinación/ones seleccionada(s):',

	// R
	'racine' => 'Raíz',
	'retirer_lien_objet' => 'Retirar este objeto seleccionado',
	'retirer_tous_liens_selection_objets' => 'Retirar todas las selecciones de objetos',
	'rubriques_choisis' => 'Secciones seleccionadas:',

	// S
	'selection_objet' => 'Selección',
	'selection_objet_titre' => 'Selección de objetos',
	'selection_objets_pour' => 'Selección objetos para:',
	'selection_rubrique_dest' => 'Sección/ones de destino',
	'selection_rubrique_objet' => 'Objetos que pueden ser seleccionados',

	// T
	'texte_ajouter_objet' => 'Seleccionar un objeto',
	'texte_changer_statut_objet' => 'Este objeto seleccionado está:',
	'texte_creer_asselection_objetcier_objet' => 'Créer et associer un objet', # NEW
	'titre_langue_objet' => 'Idioma de este objeto seccionado',
	'titre_logo_selection_objet' => 'Logo de este objeto seleccionado',
	'titre_selection_objets' => 'Objeto seleccionado',
	'titre_selection_objets_rubrique' => 'Objetos seleccionados de la sección'
);

?>
