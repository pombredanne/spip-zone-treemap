<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/selection_d_objets/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'affichage_section' => 'Afficher dans :',
	'ajouter_lien_objet' => 'Sélectionner cet objet',

	// C
	'configurations' => 'Configurations',

	// E
	'explication_selection_rubrique_dest' => 'Les rubriques ou pourront être affichés les objets sélectionnés',

	// I
	'icone_creer_selection_objet' => 'Sélectionner un objet',
	'icone_modifier_selection_objet' => 'Modifier cet objet sélectionné',
	'info_1_selection_objet' => 'Un objet sélectionné',
	'info_aucun_selection_objet' => 'Aucun objet sélectionné',
	'info_nb_selection_objets' => '@nb@ objets sélectionnés',

	// L
	'label_descriptif' => 'Descriptif',
	'label_id_objet' => 'Id objet',
	'label_id_objet_dest' => 'Id objet dest',
	'label_objet' => 'Objet',
	'label_objet_dest' => 'Objet dest',
	'label_ordre' => 'Ordre',
	'label_titre' => 'Titre',
	'label_url' => 'Url',

	// O
	'objet_destination_choisis' => 'Destination(s) choisie(s) :',

	// R
	'racine' => 'Racine',
	'retirer_lien_objet' => 'Retirer cet objet de la sélection',
	'retirer_tous_liens_selection_objets' => 'Retirer toutes les sélection d\'objets',
	'rubriques_choisis' => 'Rubriques choisies :',

	// S
	'selection_objet' => 'Sélection',
	'selection_objet_titre' => 'Selection d\'objets',
	'selection_objets_pour' => 'Sélection objets pour',
	'selection_rubrique_dest' => 'Rubrique(s) de destination',
	'selection_rubrique_objet' => 'Objets qui peuvent être sélectionnés',

	// T
	'texte_ajouter_objet' => 'Sélectionner un objet',
	'texte_changer_statut_objet' => 'Cet  objet sélectionné est :',
	'texte_creer_asselection_objetcier_objet' => 'Créer et associer un objet',
	'titre_langue_objet' => 'Langue de cet objet sélectionné',
	'titre_logo_selection_objet' => 'Logo de cet objet sélectionné',
	'titre_selection_objets' => 'Objet sélectionné',
	'titre_selection_objets_rubrique' => 'Objets sélectionnés de la rubrique'
);

?>
