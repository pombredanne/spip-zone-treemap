<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP nommé  genere le NOW()
// langue / language = fr

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// a
	'affichage_section' => 'Afficher dans :',
	
	// c
	'choix_favoris' => 'Afficher comme favoris :',
	
	// o
	'oui' => 'OUI',
	
	// n
	'non' => 'NON',
	
	//r
	'racine' => 'Racine',
	'rubriques_choisis' => 'Rubriques choisis :',
	
	//s
	'selection_article' => 'Selection d\'articles',

);

?>