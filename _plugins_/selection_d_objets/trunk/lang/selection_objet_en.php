<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/selection_objet?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'affichage_section' => 'Display in:',
	'ajouter_lien_objet' => 'Select this object:',

	// C
	'configurations' => 'Configurations',

	// E
	'explication_selection_rubrique_dest' => 'Les rubriques ou pourront être Sections where the selected objects can be displayed',

	// I
	'icone_creer_selection_objet' => 'Select an object',
	'icone_modifier_selection_objet' => 'Modify this selected object',
	'info_1_selection_objet' => 'One selected object',
	'info_aucun_selection_objet' => 'No selected object',
	'info_nb_selection_objets' => '@nb@ selected objects',

	// L
	'label_descriptif' => 'Description',
	'label_id_objet' => 'Object ID',
	'label_id_objet_dest' => 'Dest Object ID',
	'label_objet' => 'Object',
	'label_objet_dest' => 'Dest Object',
	'label_ordre' => 'Rank',
	'label_titre' => 'Title',
	'label_url' => 'Url',

	// O
	'objet_destination_choisis' => 'Selected destination(s):',

	// R
	'racine' => 'Root',
	'retirer_lien_objet' => 'Remove this selected object',
	'retirer_tous_liens_selection_objets' => 'Remove all the selected objects',
	'rubriques_choisis' => 'Selected sections:',

	// S
	'selection_objet' => 'Selection',
	'selection_objet_titre' => 'Object Selection',
	'selection_objets_pour' => 'Object selection for',
	'selection_rubrique_dest' => 'Destination Section',
	'selection_rubrique_objet' => 'Objects that can be selected',

	// T
	'texte_ajouter_objet' => 'Select an object',
	'texte_changer_statut_objet' => 'This Selected Object is:',
	'texte_creer_asselection_objetcier_objet' => 'Create and link an object',
	'titre_langue_objet' => 'Language of this Selected Object',
	'titre_logo_selection_objet' => 'Logo of this Selected Object',
	'titre_selection_objets' => 'Selected Object',
	'titre_selection_objets_rubrique' => 'Selected Objects of the section'
);

?>
