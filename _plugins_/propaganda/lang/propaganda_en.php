<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP nomme admin_lang genere le NOW()
// langue / language = en
// Traduction -- Daniel Vinar Ulriksen <dani@belvil.net>

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'autoriser_tout_le_monde' => 'Allow any visitor of the website to send a postcard',
	'autoriser_uniquement_inscrits' => 'Force visitors to register themselves in the site to be able to send a postcard',		
	
	// B
	'bonjour' => 'Hello,',
	
	// C
	'carte_envoyee' => 'Postcard sent',
	'cfg_titre_configuration' => 'Set up of the "Propaganda" plugin',
	'champ_obligatoire' => 'This field is mandatory',
	'champ_trop_court' => 'This field must be more than @taille@ characters length',
	'choose_image' => 'Choose a postcard image',
	'connexion_obligatoire' => 'You must be connected to the site to send an e-card.',
	'consulter_carte' => 'You can see it at the following URL address:',
	'credits' => 'Graphical credits: ',

	// D
	'destinataire' => 'Recipient(s) :',
	'droit_envoi' => 'Who can send an e-postcard',

	// E
	'email_destinataire' => 'Send the postcard to',
	'emmetteur' => 'Sender:',
	'emmetteur_envoi' => 'sends you this e-postcard',
	'envoi_nouvelle_carte' => 'Send another postcard',
	
	// L
	'label_documents_traduction' => 'Use the documents of the linked translations',
	
	// M
	'merci_de_visite' => 'Thank you for your visit...',
	
	// S
	'send_ecard' => 'Send an E-card',
	'son_message' => 'The message within the e-card is :',
	
	// U
	'untel_envoi_carte' => 'sent you an e-postcard.',

	// V
	'votre_carte' => 'Your e-card :',

	// X
	'xxx' => 'xxx'

);

?>
