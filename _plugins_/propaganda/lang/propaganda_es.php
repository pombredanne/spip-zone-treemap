<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP nomme admin_lang genere le NOW()
// langue / language = es
// Traduction -- Pierre FICHES <pierre.fiches@free.fr>

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'autoriser_tout_le_monde' => 'Autorizar cualquier visitante del sitio a mandar una postal',
	'autoriser_uniquement_inscrits' => 'Exigir de los visitantes que se inscriban y autentifiquen en el sitio para mandar una postal.',
	
	// B
	'bonjour' => 'Hola,',
	
	// C
	'carte_envoyee' => 'Postal enviada',
	'choose_image' => 'Elige una imagen',
	'consulter_carte' => 'La puede consultar en la direcci&oacute;n siguiente:',
	'credits' => 'Cr&eacute;ditos iconogr&aacute;ficos: ',
				
	// D
	'destinataire' => 'Destinatari@(s) :',
	'droit_envoi' => 'Quien tiene derecho de enviar una postal',

	// E
	'email_destinataire' => 'La direcci&oacute;n de correo del destinatario',
	'emmetteur' => 'Remitente:',
	'emmetteur_envoi' => 'te env&iacute;a esta postal electr&oacute;nica',
	'envoi_nouvelle_carte' => 'Enviar otra postal',
	
	// M
	'merci_de_visite' => 'Gracias por su visita...',
	
	// S
	'send_ecard' => '<NEW>Send an E-card',
	'son_message' => 'Su mensaje acompa&ntilde;a esta postal',
		
	// U
	'untel_envoi_carte' => 'le acaba de enviar una postal electr&oacute;nica.',

	// V
	'votre_carte' => 'Su postal:',
	
	// X
	'xxx' => 'xxx'

);

?>
