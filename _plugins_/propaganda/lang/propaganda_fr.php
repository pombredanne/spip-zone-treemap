<?php

// This is a SPIP language file  --  Ceci est le fichier langue de SPIP du plugin Propaganda
// langue / language = fr

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'autoriser_tout_le_monde' => 'Autoriser tout visiteur du site &agrave; envoyer une carte',
	'autoriser_uniquement_inscrits' => 'Exiger des visiteurs qu\'ils s\'inscrivent dans le site et s\'authentifient pour envoyer une carte',
	
	// B
	'bonjour' => 'Bonjour,',

	// C
	'carte_envoyee' => 'Carte postale envoy&eacute;e',
	'connexion_obligatoire' => 'Vous devez &ecirc;tre connect&eacute; pour envoyer une carte.',
	'cfg_titre_configuration' => 'Configuration du plugin "Propaganda"',
	'champ_obligatoire' => 'Ce champ est obligatoire',
	'champ_trop_court' => 'Ce champ doit &ecirc;tre plus long que @taille@ caract&egrave;res',
	'choose_image' => 'Choisissez une image',
	'consulter_carte' => 'Vous pouvez la consulter &agrave; l\'adresse :',
	'credits' => 'Cr&eacute;dits iconographiques : ',

	// D
	'destinataire' => 'Destinataire(s) :',
	'droit_envoi' => 'Qui a le droit d\'envoyer une carte',
	
	// E
	'email_destinataire' => 'L\'email du destinataire',
	'emmetteur' => 'Emmetteur :',
	'emmetteur_envoi' => 'vous a fait parvenir cette carte &eacute;lectronique',
	'envoi_nouvelle_carte' => 'Envoyer une autre carte postale',

	// L
	'label_documents_traduction' => 'Utiliser les documents des traductions li&eacute;es',
	
	// M
	'merci_de_visite' => 'Merci de votre visite...',
	
	// S
	'send_ecard' => 'Envoyer une carte &eacute;lectronique',
	'son_message' => 'Le message accompagnant cette carte est :',	
	
	// U
	'untel_envoi_carte' => 'vous a envoy&eacute; une carte &eacute;lectronique.',

	// V
	'votre_carte' => 'Votre carte :',
	
	// X
	'xxx' => 'xxx'

);

?>
