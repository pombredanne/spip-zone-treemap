<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'importer' 	=>"Importer massivement des documents distants",
'explicatif'	=>"Notez ici la liste des documents distants que vous souhaitez importer, chaque document distant est séparé par un point virgule",
'attribuer'	=>'Attribués à',
'retourner'	=>'Y retourner',
'pasdroits'	=>'Vous ne pouvez pas joindre des documents à cet objet',
'alt_vignette'	=>'Ajouter les vignettes à partir de l\'upload local',
'vignette'	=>'Vignettes locales',
'completer'	=>"Vous n'avez pas complétez le formulaire"
);



?>
