<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(
	'affichage' => 'affichage',
	'calendriers' => 'Calendriers',
	'cal_par_mois' => 'calendrier mensuel',
	'navigation' => 'navigation',
	'par_mois' => 'par mois',
	'par_annee' => 'par ann&eacute;e',
	'premier_jour_culturel' => 'premier jour culturel',
	'desc_pjc_auto' => '(automatique : le dimanche pour les pays anglo-saxons et le japon, le lundi sinon)',
	'jour_semaine_abbr' => 'jour de la semaine',
	'mot_entier' => 'mot complet',
	'abreviation' => 'abr&eacute;viation',
	'initiale' => 'initiale',
	'une_lettre' => 'premi&egrave;re lettre',
	'deux_lettres' => 'deux lettres',
	'trois_lettres' => 'trois lettres',
	'revenir_aujourdhui' => 'retour &agrave; la date courante',
	'texte_precedent' => 'texte "pr&eacute;c&eacute;dent"',
	'texte_suivant' => '"suivant"',
	'desc_texte_prec_suiv' => '(texte du lien vers la p&eacute;riode pr&eacute;c&eacute;dente / suivante &ndash; \'&#171;\' / \'&#187;\' par d&eacute;faut)'
);
?>
