<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
//encodage des caractères = ê = &ecirc; - é = &eacute; - à = &agrave; - è = &egrave;
// format : ' ' => " ",

$GLOBALS[$GLOBALS['idx_lang']] = array(
'accents' => "é=&eacute;, è=&egrave;, ê=&ecirc;, à=&agrave;, ù= î=, °=&#176;, ",
'article_num' => "Article n &#176;",

'boite_info' => "<i>Informations :</i>",

'clic_editer' => "Cliquez ici pour &eacute;diter",

'descriptif' => "<strong>Descriptif : </strong>",
'discipline' => "<strong>Discipline : </strong>",

'editer_mentions' => "Editer les mentions l&eacute;gales",
'effet' => "<strong>Effet de transition actuel : </strong>",
'eva_mentions' => "Gestion des mentions l&eacute;gales",

'page_publique' => "Aller &agrave; la page sommaire",
'presentation' => "Ce plugin permet de g&eacute;rer les renseignements &agrave; compl&eacute;ter pour les mentions l&eacute;gales<br/>",
'probleme_base' => "Il y a un probl&egrave;me dans votre base de donn&eacute;es.<br/>",

'lcen' => "En vertu de la loi pour la confiance dans l'&eacute;conomie num&eacute;rique (LCEN) du 21 juin 2004, vous devez compl&eacute;ter les mentions suivantes pour ce site : <br />",
'lienoff1' => "<a href='http://eva-web.edres74.net' target='_blank'>http://eva-web.edres74.net</a><br/>",
'lienoff2' => "<a href='' target='_blank' ></a><br/>",

'modif_renseignements' => "Modifier ces renseignements",
'mot_titre' => "<strong>Nom du mot-cl&eacute; : </strong>",
'mot_descriptif' => "<strong>Descriptif : </strong>",

'pausevitesse' => "<strong>Dur&eacute;e de pause entre chaque articles (1000=1seconde) : </strong>",

'siteoff1' => "<strong>Site officiel eva-web :</strong><br/>",
'siteoff2' => "<strong></strong><br/>",

'texte_article' => "<strong>Texte de l'article : </strong>",
'texte_descriptif' => "<br />Ce plugin vous permet de publier une s&eacute;rie d'articles dans un bloc avec des effets d'affichage.
							<br />Liste des effets : Fade In-Out ,Slide Up-Down, Left To Right<br />",
'texte_integration' => "<br /><strong>Avec des squelettes autres que eva-web 4 :</strong><br />
	 Vous devez ins&eacute;rer la ligne <code><INCLURE {fond=articles_news}{ajax}></code> dans votre squelette &agrave; l'endroit où doit apparaître le bloc de news<br/><br />
	 <strong>Avec eva-web 4 :</strong><br />
	 Rendez-vous dans l'onglet <i>structure</i> du plugin eva-habillage.<br />
	 Dans le deuxi&egrave;me bloc intitul&eacute; <i>Choisir la position des blocs dans les pages</i>,
	 allez &agrave; la ligne <i>Ins&eacute;rer un squelette personnel de type bloc dans une des pages...</i>.<br />
	 A l'&eacute;tape 2, dans la zone de texte <i>Nom du squelette</i>, tapez <i>articles_news</i>, puis validez.<br /><br/>
	 ",
'titre' => "<strong>Titre du bloc de news : </strong>",
'titre_boite_principale' => "Param&egrave;trage du plugin :",
'titre_presentation' => "<strong>Pr&eacute;sentation :</strong><br/>",
'titre_page' => "EVA NEWS : D&eacute;filement automatique d'articles sous forme de news",

'vitesse' => "<strong>Vitesse entre les transitions (1000=1seconde) : </strong>",

'quitter' => "Quitter ce formulaire",

//Variables pour les paramêtres
'titre' => "<strong>titre : </strong>",
'idwebmaster' => "<strong>Num&eacute;ro du compte SPIP du Webmaster : </strong>",
'fonction' => "<strong>Fonction du webmaster : </strong>",
'directeur' => "<strong>Directeur de publication : </strong>",
'responsable' => "<strong>Responsable d'&eacute;dition : </strong>",
'hebergeur' => "<strong>H&eacute;bergeur du site : </strong>",
'etab' => "<strong>Nom de l'&eacute;tablissement, &eacute;cole, association, etc. : </strong>",
'idweb' => "<strong>Num&eacute;ro du compte SPIP du webmestre : </strong>",
'adresse' => "<strong>Adresse de l'h&eacute;bergeur : </strong>",

);
?>