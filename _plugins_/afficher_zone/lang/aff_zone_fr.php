<?php
// fichier de langue du PLUGIN aff_zone - FR

$GLOBALS[$GLOBALS['idx_lang']] = array(
// A
"activez_cfg" => "Vous devez avoir install&eacute; et activ&eacute; le plugin CFG pour que ce plugin focntionne",
"aff_zone" => "trier/classer/afficher les plugins &agrave; partir du flux RSS des paquets de la zone",
"affichage_plugins" => "Affichage des plugins",
"afficher_attribues" => "Afficher les plugins attribu&eacute;s",
"attribution_mots_cles"=> "Classement des plugins",

// C
"categorie_afficher" => "Choix des cat&eacute;gories",
"choisir" => "Choisir",
"choix_categorie" => "Choix de la cat&eacute;gorie",
"choix_groupe_statuts" => "Choisir le groupe de mots cl&eacute;s pour les status",
"choix_groupes_categories" => "Choisir les groupes de mots cl&eacute;s des cat&eacute;gories de plugins",
"choix_plugins" => "Choix des plugins",
"choix_statut" => "Choix du statut",
"clicker_afficher" => "afficher les d&eacute;tails du plugin",

// D
"descriptif_rss" => "Description du flux RSS",

// E
"erreur_enregistrement" => "erreur d'enregistrement",
"erreur_parametres_ajax" => "Erreur dans les param&egrave;tres envoy&eacute;s",
"erreur_suppression" => "erreur de suppression",

// F
"fm_lien" => "choix du type de lien dans les cartes Freemind",
"fm_lien_txt" => "choisissez quel type de lien vous d�sirez associer &agrave; un plugin dans les cartes Freemind",
"fm_lien_zone" => "lien sur la page Trac du plugin",
"fm_lien_zip" => "lien sur le zip du plugin",

// H
"help_info" => "Choisissez le mot cl&eacute; correspondant &agrave; une cat&eacute;gorie puis cochez les plugins correspondants.<br /><br />
                <strong>Validez avant de changer de cat&eacute;gorie.</strong>",

// L
"lien_config" => "Configuration du plugin",
"lien_rss" => "flux RSS",
"lien_freemind" => "carte freemind",

// M
"masquer_attribues" => "Masquer les plugins attribu&eacute;s",
"mode_affichage" => "Mode d'affichage HTML du plugin",
"mode_compact" => "affichage compact: les d&eacute;tails sont masqu&eacute;s, il faut cliquer sur le titre du plugin pour les afficher",
"mode_complet" => "affichage complet: les d&eacute;tails sont affich&eacute;s",
"module_titre" => "afficher la Zone",

// P
"page_zone" => "sur la Zone",
"plugin_info" => "Plugin Afficher la Zone",
"plugins" => "Plugins",

// R
"rss_presentation" => "Pr&eacute;sentation du flux RSS des plugins s&eacute;lectionn&eacute;s",

// S
"selection_plugins_rss" => "Les plugins selectionnes par ",
"statut" => "Statut",

// T
"telecharger_zip" => "t&eacute;l&eacute;charger le zip",
"telecharger" => "t&eacute;l&eacute;charger",
"titre_choisir_statut" => "choisissez un statut",
"titre_page" => "Classement des plugins",
"titre_rss" => "Titre du flux RSS",
"titre_statut" => "plugins ayant le statut",
"tout" => "tout",

// V
"version" => "Version: ",
"valider" => "Valider",
"voir_en_ligne" => "Voir en ligne",


// �viter de g�rer la , sur la derni�re ligne
"zozo" => "Ze sert � rien!"
);


?>
