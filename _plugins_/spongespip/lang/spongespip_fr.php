<?php

// This is a SPIP module file  --  Ceci est un fichier module de SPIP

$GLOBALS['i18n_spongespip_fr'] = array(

//_ 
'titre_page_spongespip' => 'spongespip',

//Barre d'onglets
'graphiques' => 'Graphiques',
'pages_vues' => 'Pages vues',
'hotes' => 'H&ocirc;tes',
'referents' => 'R&eacute;f&eacute;rents',
'plateformes' => 'Plateformes',
'mots_cles' => 'Mots-cl&eacute;s',

//D
'chaine_derniers_visiteurs' => 'Derniers visiteurs',

//H
'hote' => 'H&ocirc;tes'
);

?>
