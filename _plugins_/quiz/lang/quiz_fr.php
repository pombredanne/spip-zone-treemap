<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

	//A
	'ajouter_reponse' => 'Ajouter une r&eacute;ponse',
	'ajouter_reponse_confirme' => 'Ajouter une r&eacute;ponse &agrave; la question',
	'ajouter_question' => 'Ajouter une question &agrave; ce quiz',
	'article_pas_question' => 'Cet article n\'est pas une question de quiz. Pour le transformer en question, ajoutez-lui simplement une r&eacute;ponse.',
	
	//B
	'bouton_ajouter_reponse' => 'Ajouter une r&eacute;ponse',
	'bouton_ajouter_question' => 'Ajouter une question',
	'bouton_supprimer_reponse' => 'X',
	'bouton_exporter_txt' => 'Cr&eacute;er fichier txt',
	
	//C
	'corrige' => 'Corrig&eacute;',
	'cliquez_editer_question' => 'Double-cliquez pour &eacute;diter cette question',
	
	//E
	'editer_nouveau' => 'Cr&eacute;er un quiz',
	'edition_question' => 'Edition d\'une question de quiz',
	'erreur_article_manquant' => 'Le num&eacute;ro de l\'article n\'est pas pass&eacute; dans le formulaire.',
	
	//I
	'indiquer_reponse_juste' => 'Indiquer la r&eacute;ponse juste',
	
	// N
	'nom_bouton_plugin' => 'Les quiz',
	
	//O
	'oui' => 'Oui',

	//P
	'page_tous_explication' => 'Voici la liste de tous les quiz du site, c\'est &agrave; dire toutes les rubriques contenant des articles de type r&eacute;ponse.',
	'pas_reponse_article' => 'Pas encore de r&eacute;ponse pour l\'article ',
	'pas_reponse_question' => 'Pas encore de r&eacute;ponse pour la question ',
	
	//Q
	'quiz' => 'Quiz',
	'question_ajoutee_ok' => 'Article correctement transform&eacute; en question !',
	'question_ajoutee_erreur' => 'Erreur lors de la transformation de l\'article en question',

	//R 
	'reponses_possibles' => 'Les r&eacute;ponses possibles &agrave; cette question :',
	'reponse_juste' => 'La r&eacute;ponse juste est la r&eacute;ponse N&deg;',
	
	//S
	'supprimer_reponse_confirme' => 'Supprimer la r&eacute;ponse ',
	
	//T
	'transformer_question' => 'Transformer cet article en question de quiz.',
	'tous_les_quiz' => 'Tous les quiz du site',
	

);

?>