<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	'en_savoir_plus' => 'to know +',
	'partager'=>'share it',
	'abonnement_lettre'=>'To get last publications by e-mail, subscribe !',
	'use_opensearch'=>'Done search with your navigator on @nom_site@ as other diff&eacute;rents search engines!',
	'rendu_code'=>'This code rendering : ',
	'restez_informes_avec'=>'Keep informed of rendez-vous using',
	'confort_w3c'=>'For all confortable visit, this website is done with <a href="http://validator.w3.org/check?uri=@url_site_spip@">W3C norms</a>. Enjoy! and use <a href="htp://www.mozilla-europe.org/fr/firefox/">a good navigator respecting standards !</a>',
	'use_rss'=>'Use agregator as <a href="http://rssowl.org/">RRSOwl</a>, 
	or simply your navigator if you see in navigation barre RSS icon.'
	);

?>
