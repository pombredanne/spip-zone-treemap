<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'bouton_arreter' => 'Arr&ecirc;ter les tests',
'tester_10' => '10 Tests',
'tester_tout' => 'Tester Tout!',
'erreur' => 'Erreur',
'une_erreur' => 'Une erreur',
'erreurs' => '@erreurs@ erreurs',
'page_valide' => 'Page Valide',
'reset_all' => 'Tout R&eacute;initialiser',
'reset' => 'R&eacute;initialiser',
'tester_page' => 'Tester cette page',
'titre_formulaire_choix_validateur' => "Verifier la conformit&eacute; avec les validateurs&nbsp;:",
'titre_page' => 'Validation Site Xhtml&amp;Accessibilit&eacute;',
'titre_tableau_conformite' => 'Conformit&eacute; du site',
'titre_conformite_page' => 'Conformit&eacute; de la page :',
'resultat_pages_conformes' => "@nb@ sur @tot@ pages conformes selon le validateur @nom@",
'resultat_pages_completement_conformes' => "@nb@ sur @tot@ pages compl&egrave;tement conformes",
);


?>