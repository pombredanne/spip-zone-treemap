<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/cfg/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'autoplay' => 'Autoplay',
	// B
	'bouton_effacer' => 'Delete',
	// C
	'controls' => 'Controls',
	'controlnav' => 'Show pagination',
	'cfg_title_fluxslider' => 'Flux Slider',
	'cfg_boite_fluxslider' => 'Flux Slider configuration form.<br /><br />[Documentation->http://www.spip-contrib.net/Flux-Slider-pour-SPIP]',
	// D
	'delay' => 'Animation speed',
	'default' => 'Default article',
	'default' => 'Default article',
	'default_info' => 'Default article used by the snippet [#MODELE{nivoslider}] as default article id to use...<br/>',
	'directionnav' => 'Show Next &amp; Prev arrows',
	// E
	// F
	// G
	'general' => 'General Appearance',
	// H
	'height' => 'Height',
	// I
	'images' => 'Images',
	'image_align' => 'Crop',
	'image_backcolor' => 'Background color',
	// J
	// K
	// L
	// M
	// N
	'non' => 'no',
	// O
	'oui' => 'yes',
	// P
	'portfolio' => 'Slide portfolio',
	// Q
	// R
	// S
	// T
	// U
	'usethumbs' => 'Show document titles (captions)',
	// V
	// W
	'width' => 'Width',
	// X
	// Y
	// Z
);

?>
