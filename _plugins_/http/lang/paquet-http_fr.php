<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
'http_nom' => 'Serveur HTTP abstrait',
'http_description' => 'Ce plugin fournit une API pour que d\'autres plugins puissent implémenter plus facilement des services utilisant les méthodes HTTP.',
'http_slogan' => 'Gestion abstraite des méthodes HTTP.',
);

?>
