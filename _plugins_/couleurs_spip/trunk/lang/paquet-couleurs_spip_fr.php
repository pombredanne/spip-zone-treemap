<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'couleurs_spip_description' => 'Ce plugin permet de modifier l\'apparence de parties de texte. Pour mettre certains mots en couleur ou modifier leur taille ou toute autre caractéristique typographique, il suffit d\'entourer ceux-ci de balises de type <<code>cs_rouge</code>><cs_rouge>mon texte rouge</cs><<code>/cs</code>>. 
	En ajoutant des styles dans le fichier css/couleurs_spip.css - le recopier dans ce cas dans le dossier squelettes utilisé -, on peut ajouter autant de balise qu\'on le désire.',
	'couleurs_spip_slogan' => 'Des textes en couleur',
);
?>