<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

    'images_actives:'=>'Images Actives',
    'pas_d_images_actives'=>'Pas de document associé',
    'chargement'=>'Chargement',
    'sans_titre'=>'Pas d\'image active ici',
);

?>
