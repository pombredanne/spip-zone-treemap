<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'action_ajouter' => 'Cr&eacute;er un produit',
	'action_modifier' => 'Modifier',
	'action_supprimer' => 'Supprimer',
	'action_voir' => 'Voir',
	'add_to_cart' => 'Ajouter au panier',
	
	// B
	'buy' => 'Acheter',
	
	// C
	
	'c_dineromail' => 'DineroMail',
	'c_email' => 'Email',
	'c_google_checkout' => 'Google Checkout',
	'c_paypal' => 'PayPal',
	'cart_configuration' => 'Configuration du panier',
	'cart_headers' => 'Ent&ecirc;te du panier',
	'cart_headers_explication' => 'Editez les champs du panier. Voir <a href="http://simplecartjs.com/documentation.html">Cart formatting configuration page</a> pour d&eacute;tails',
	'checkout' => 'Finaliser l\'achat',
	'checkout2email' => 'Envoyer une commande par email',
	'checkout2email_explication' => 'La commande est envoy&eacute;e &agrave; l\' adresse suivante. On n\'utilise pas de moyen de paiement.',
	'checkout_methods' => 'Moyens de paiement',
	'checkout_to' => 'Pay&eacute; par',
	
	// D
	'description_produits' => 'D&eacute;crivez  vos produits et inserez une image!',
	'description' => 'Un panier d\'achat simple en javascript',
	'dineromail_country' => 'Pays',
	'dineromail_country_argentine' => 'Argentine',
	'dineromail_country_brazil' => 'Bresil',
	'dineromail_country_chile' => 'Chili',
	'dineromail_country_explication' => 'Choisir le pays ou &agrave; &eacute;t&eacute; enregistr&eacute; le compte DineroMail.',
	'dineromail_country_mexico' => 'Mexique',
	'dineromail_currency' => 'Monnaie de la transaction',
	'dineromail_currency_explication' => 'Type de la monnaie de la transaction. Il n\'y a pas de conversion de ce type. Les prix doivent &ecirc;tre indiqu&eacute;s dans la monnaie s&eacute;lectionn&eacute;e',
	'dineromail_currency_local' => 'Monnaie locale',
	'dineromail_currency_usd' => 'Dollars am&eacute;ricains',
	'dineromail_header_image' => 'Image de l\'en-t&ecirc;te',
	'dineromail_header_image_explication' => 'URL absolue du logo &agrave; montrer dans l\'en-t&ecirc;te de DineroMail (jpg o gif, 150px x 50px)',
	'dineromail_merchant_id' => 'Num&eacute;ro de compte',
	'dineromail_merchant_id_explication' => 'Num&eacute;ro d\'identification de votre compte DineroMail sans l\'indicateur digital ',
	'dineromail_payments_methods' => 'Moyen de paiement DineroMail',
	'dineromail_payments_methods_explication' => 'Chaine de texte qui d&eacute;finit les moyens de paiement autoris&eacute;s. Laisser en blanc pour toutes les disponibilit&eacute;s dans le pays.',
	'dineromail_seller_name' => 'Nom du vendeur',
	'dineromail_seller_name_explication' => 'L&eacute;gende que le vendeur vut montrer dans l\'en-t&ecirc;te',
	'dineromail_see' => 'Voir',
	
	// E
	'explication_infos' => "
		Juste pour permettre un classement de vos produits&hellip;
	",
	'empty' => 'Vider',
	'error_url' => 'URL achat erronn&eacute;',
	'error_url_explication' => 'URL o&ugrave; se redirige l\'acheteur en cas de transaction erronn&eacute;e',
	'envoi_groupe' => 'Frais de transport unique &agrave; toute la commande?',
	
	//F
   'final_total' => 'Total',
   'frais_port' => 'Frais de port',
   
   // G
	'google_merchant_id' => 'Id de l\'identification du marchand',
	'google_merchant_id_explication' => 'Num&eacute;ro d\'identification de votre compte marchand Google',
	
	// H
	'header_name' => 'Nom',
	'header_price' => 'Prix',
	'header_quantity' => 'Quantit&eacute;',
	'header_total' => 'Total',
	
	// I
	'info_gauche_numero_produit' => 'Produit num&eacute;ro :',
	'info_modifier_produit' => 'Modifier un produit :',
	
	// L
	'label_actions' => 'Actions',
	'label_id' => 'Id',
	'label_infos' => 'R&eacute;f&eacute;rence du produit',
	'label_nom' => 'Nom',
	'label_descriptif' => 'Petit descriptif du produit.',
	'label_texte' => 'Texte',
	'label_prix_ttc' => 'Prix TTC du produit.',
	'label_prix' => 'Prix',
	'liste_des_produits' => 'Liste des produits',

	
	

	// O
	
	'objet_produits' => 'Produits',
	'ok_url' => 'URL achat r&eacute;ussi',
	'ok_url_explication' => 'URL o&ugrave; sera redirig&eacute; l\'acheteur en cas de transaction r&eacute;ussie.',
	'other_parameters' => 'Autres param&egrave;tres',
	
	//P
	
	'produits' => 'Produits',
	'paypal_account' => 'Compte PayPal',
	'paypal_account_explication' => 'Si vous avez un compte PayPal, enregistrez l\'email de votre compte.',
	'pending_url' => 'URL achat en suspend',
	'pending_url_explication' => 'URL de redirection pour l\'acheteur en cas de transaction suspendue.',
	'prix' => 'Prix de l\'objet',
	
	//R
	'reference' => 'R&eacute;f&eacute;rence de l\'objet',
	'repondre_produit' => 'D&eacute;posez un avis sur ce produit.',
	
	//S
	'shipping_cost' => 'Co&ucirc;t d\'envoi',
	'shipping_flat_rate' => 'Taxe fixe de l\'envoi',
	'shipping_flat_rate_explication' => 'Ajouter une taxe fixe &agrave; l\'ordre complet',
	'shipping_quantity_rate' => 'Co&ucirc;t de l\'envoi par quantit&eacute;',
	'shipping_quantity_rate_explication' => 'Ajouter un montant fixe pour chaque item',
	'shipping_total_rate' => 'Co&ucirc;t d\'envoi en pourcentage de la somme totale',
	'shipping_total_rate_explication' => 'Ajouter un co&ucirc;t d\'envoi proportionnel au co&ucirc;t de la commande',
	'subtotal' => 'Sous-total',
	'stock' => 'Stock actuel',
	
	// T
	'texte_nouveau_produit' => 'Sans nom',
	'tax_and_shipping' => 'Frais et co&ucirc;t d\'envoi',
	'tax_cost' => 'Frais',
	'tax_rate' => 'Taxe imposable',
	'tax_rate_explication' => 'Taux imposable. Exemple: 0.19 de frais de TVA',
	'title' => 'Ma petite boutique',
	
    // Y
	'your_cart' => 'Votre panier',
	//'repondre_breve' => 'D&eacute;posez un avis sur ce produit.',
	
	
	
	
);
?>