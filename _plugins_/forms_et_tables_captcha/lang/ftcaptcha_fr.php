<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(
'captcha_configuration' => 'configuration',
'captcha_config' => 'Configuration plugin captcha.',
'captcha_niveau_access' => 'Valeur num&eacute;rique d\'accessibilit&eacute; du captcha.',
'captcha_personnal_words' => 'Tableau de mots personnels.',
'info_captcha_personnal_words' => 'Ajoutez des mots qui pourront &ecirc;tre utilis&eacute;s par le captcha.',
'captcha_personnal_question' => 'Tableau de questions personnelles.',
'info_captcha_personnal_question' => 'Ajoutez des questions/r&eacute;ponses qui pourront &ecirc;tre utilis&eacute;s par le captcha.',
'captcha_question_type' => 'Tableau de types de question.',
'info_captcha_niveau_access' => 'Veuillez s&eacute;lectionnez la valeur num&eacute;rique d\'accessibilit&eacute; du captcha suivant le niveau d\'accessibilit&eacute; voulu pour votre site.',
'niveau_acc_1' => 'Captcha sous forme de texte accessible',
'niveau_acc_2' => 'Chiffres sous forme d\'images PNG',
'niveau_acc_3' => 'Image GD brouill&eacute;e',
'anti_spam'=>'captcha'
);


?>