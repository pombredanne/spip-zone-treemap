<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// a
'ajouter_un_produit'=>'Ajouter un produit',

// c
'configurer_boutique'=>'Configurer Spip-Boutique',
'creer_categorie'=>'Cr&eacute;er une cat&eacute;gorie',
'creer_un_produit'=>'Cr&eacute;er un produit',

// d
'dans_la_categorie'=>'Dans la cat&eacute;gorie :',
'descriptif_de_la_categorie'=>'Descriptif de la cat&eacute;gorie',
'descriptif_du_produit'=>'Descriptif du produit',

// e
'editer_une_categorie'=>'Edition d\'une cat&eacute;gorie',
'eur'=>'&euro;',

// g
'gerer_les_categories'=>'Gestion des cat&eacute;gories',
'gerer_les_produits'=>'Gestion des produits',

// i
'icone_creer_produit'=>'Cr&eacute;er un produit',
'icone_creer_categorie'=>'Cr&eacute;er une cat&eacute;gorie',

// l
'les_cadis'=>'Les caddies',
'les_categories'=>'Les cat&eacute;gories de produit',
'les_clients'=>'Les clients',
'les_forum_de_boutique'=>'Discussion sur les produits',
'les_produits'=>'Les produits',

// m
'modifier_la_categorie'=>'Modifier la cat&eacute;gorie',

// p
'pour_cent'=>'%',
'prix_achat'=>'Prix d\'achat',
'prix_vente'=>'Prix de vente',

// r
'racine_de_la_boutique'=>'Racine de la boutique',
'retour'=>'Retour',

// t
'texte_de_la_categorie'=>'Texte&nbsp;:',
'texte_du_produit'=>'Texte',
'titre_de_la_categorie'=>'Titre de la cat&eacute;gorie :',
'titre_du_produit'=>'Titre du produit',
'tva'=>'Tva',

// u
'url_de_reference'=>'Url de r&eacute;f&eacute;rence',

);
?>
