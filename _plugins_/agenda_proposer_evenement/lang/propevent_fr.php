<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'bouton_proposer' => 'Proposer&nbsp;!',

'cfg_titre_propevent' => 'Proposition d\'&eacute;v&eacute;nements',
'cfg_descr_propevent' => 'Configurez la proposition d\'&eacute;v&eacute;nements par les visiteurs

Chaque proposition d\'&eacute;v&eacute;nement g&eacute;n&egrave;rera un article avec le statut configur&eacute; ci-contre.

L\'article sera rang&eacute; dans la rubrique que vous configurez, ou une de ses sous rubriques imm&eacute;diate au choix du contributeur,
si vous activez le choix de ces sous rubriques comme th&eacute;matique.

Chaque groupe de mot cl&eacute; s&eacute;l&eacute;ctionn&eacute; ici permettra &agrave; l\'internaute de choisir des mots cl&eacute;s dans ce groupe, au moyen
d\'un select si le groupe est &agrave; choix unique, ou de cases &agrave; chocher sinon.
L\'intitul&eacute; de la saisie dans le formulaire reprendra le titre du groupe de mots cl&eacute;s.
',

'desactiver_contributions' => 'desactiver le formulaire de proposition',

'evenement_propose' => 'Ev&eacute;nement propos&eacute;',
'email_confirm_texte_1' => 'Vous avez propos&eacute; un &eacute;v&eacute;nement sur le site @url_site@ et nous vous en remercions.',
'email_confirm_texte_2' => 'Votre proposition a &eacute;t&eacute; enregistr&eacute;e et va &ecirc;tre relue par un mod&eacute;rateur avant publication.',
'email_confirm_texte_2b' => 'Votre proposition a &eacute;t&eacute; publi&eacute;e, mais pourra &ecirc;tre modifi&eacute;e apr&egrave;s relecture par un mod&eacute;rateur.',
'email_confirm_rappel_saisie' => 'Les informations que vous avez envoy&eacute;es sont rappel&eacute;es ci-dessous.',
'email_modo_confirm_texte_1' => '&Eacute;v&eacute;nement propos&eacute; sur le site @url_site@.',
'email_modo_confirm_rappel_saisie' => 'Informations envoy&eacute;es rappel&eacute;es ci-dessous.',
'email_voir_en_ligne' => 'Relisez la proposition sur le site ',

'erreur_telephone' => 'Ce num&eacute;ro de t&eacute;l&eacute;phone n\'est pas correct',
'erreurs_verifier' => 'Votre saisie comporte des erreurs. V&eacute;rifiez les champs erron&eacute;s indiqu&eacute;s ci-dessous.',

'erreur_no_bot' => 'Si vous n\'&ecirc;tes pas un robot, faites attention &agrave; votre saisie',
'erreur_categorie_interdite' => 'Ce th&egrave;me n\'est pas autoris&eacute;',
'erreur_mots_cles_interdits' => 'Certains choix ne sont pas autoris&eacute;s. Verifiez votre saisie.',
'erreur_enregistrement_article_impossible' => 'Une erreur est survenue, votre proposition n\'a pu &ecirc;tre enregistr&eacute;e (article)',
'erreur_enregistrement_evenement_impossible' => 'Une erreur est survenue, votre proposition n\'a pu &ecirc;tre enregistr&eacute;e  (evenement)',

'label_email_moderateur' => 'Adresse email pour la mod&eacute;ration des &eacute;v&eacute;nements',
'label_etat_contribution' => 'Statut des contributions',
'label_groupes' => 'Groupes de mots-cl&eacute;s propos&eacute;s',
'label_rubrique' => 'Rubrique pour les stockage des &eacute;v&eacute;nements',
'label_proposer_thematique' => 'Proposer les sous rubriques comme th&eacute;matique de l\'&eacute;v&eacute;nement (les &eacute;v&eacute;nements seront alors r&eacute;partis dans ces sous rubriques)',

'label_qui_etes_vous' => 'Vous',
'label_votre_nom' => 'Nom',
'label_votre_prenom' => 'Pr&eacute;nom',
'label_votre_email' => 'Couriel',
'label_votre_telephone' => 'T&eacute;l&eacute;phone',

'label_evenement_propose' => 'Votre &eacute;v&eacute;nement',
'label_proposer_descriptif' => 'R&eacute;sum&eacute;',
'label_proposer_texte' => 'Description d&eacute;taill&eacute;e',
'label_id_categorie' => 'Th&egrave;me',

'message_aucun_groupe_mot_evenement' => 'Aucun groupe n\'est configur&eacute; pour &ecirc;tre associ&eacute; aux &eacute;v&eacute;nements',

'ok_proposition_publiee_moderation_posteriori' => 'Votre &eacute;v&eacute;nement a &eacute;t&eacute; publi&eacute; mais pourra &ecirc;tre corrig&eacute; par un mod&eacute;rateur',
'ok_proposition_attente_moderation' => 'Votre &eacute;v&eacute;nement a bien &eacute;t&eacute; enregistr&eacute; et sera relu par un mod&eacute;rateur avant publication',
);


?>