<?php
/**
 * Pack langue francais
 *
 */
 // $LastChangedRevision$
 // $LastChangedBy$
 // $LastChangedDate$
 
$GLOBALS['i18n_ausu_fr'] = array(

	'supprimer_auteurs' => 'Supprimer les auteurs',
	'introduction' => '<span class="important">Attention</span>:
		La suppression est effective et irr&#233;cup&#233;rable.<br />
		Effectuer une sauvegarde de la base avant utilisation de
		ce formulaire.',
	'supprimer' => 'Supprimer',
	'confirmer' => 'Confirmer',
	'message_confirmer_' => 'Etes-vous certain de vouloir supprimer ',
	'cet_auteur' => 'cet auteur.',
	'ces_auteurs' => 'ces auteurs.',
	'action_realisee' => 'Action r&#233;alis&#233;e.',
	'un_article' => '1 article',
	'nb_articles' => '@nb@ articles',
	'pagination_' => 'Pagination : ',
	'total_un_auteur' => 'Total: 1 auteur',
	'total_nb_auteur' => 'Total: @nb@ auteurs',
	'tout_cocher' => 'Tout cocher',
	'tout_decocher' => 'Tout d&#233;cocher',
	'tous' => 'Tous'
);