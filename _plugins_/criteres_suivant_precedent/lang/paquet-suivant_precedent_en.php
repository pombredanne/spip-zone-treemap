<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-suivant_precedent
// Langue: en
// Date: 06-04-2012 16:03:45
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'suivant_precedent_description' => 'This plugin is an experiment allowing to find in a given loop, the next or previous item. This usage is primarily useful when creating navigation by category or by topic (keywords).',
	'suivant_precedent_nom' => 'Next / Previous criteria',
	'suivant_precedent_slogan' => 'Adds loops criteria <code>{suivant}</code> and <code>{precedent}</code>',
);
?>