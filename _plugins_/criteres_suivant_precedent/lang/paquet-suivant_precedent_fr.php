<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-suivant_precedent
// Langue: fr
// Date: 06-04-2012 16:03:45
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'suivant_precedent_description' => 'Ce plugin est une expérimentation permettant de trouver, dans une boucle donnée, l’élément suivant ou l’élément précédent. Cet usage est principalement utile pour créer des navigations par rubrique ou par thème (mots-clés).',
	'suivant_precedent_nom' => 'Critères Suivant / Précédent',
	'suivant_precedent_slogan' => 'Ajoute des critères de boucles <code>{suivant}</code> et <code>{precedent}</code>',
);
?>