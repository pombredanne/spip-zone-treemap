<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
    'editer' => '&Eacute;diter le contenu',
    'titre_page' => 'Pr&eacute;paration du cahier de texte "Bonbon !"',
    'info' => '<strong>Informations :</strong>',
    'soustitre' => 'ajout des classes et des mati&egrave;res et la rubrique du cahier de texte, 
    					mise &agrave; jour des fiches des classes et des profs',
    'precaution' => 'Avant d\'ex&eacute;cuter cette page en cliquant sur le bouton, 
    						faites une sauvegarde de votre base de donn&eacute;es. En cas de probl&egrave;me 
    						vous devrez r&eacute;utiliser cette sauvegarde.',
    'consignes' => 'Vous allez installer dans votre Spip les mots-cl&eacute;s, rubriques et autres 
    						&eacute;lements utilis&eacute;s par le cahier de texte sous Spip. Ces mots-cl&eacute;s sont 
    						la liste des classes, les listes de mati&egrave;res, quelques autres mots-cl&eacute;s 
    						de gestion. Il faut &Eacute;diter le fichier "cahier-de-texte-installer-mots-cles.html" 
    						vers la ligne n°46 pour personnaliser les classes et mati&egrave;res. Puis, 
    						il sera rajout&eacute; une rubrique qui contiendra les entr&eacute;es du cahier de 
    						texte sous forme d\'articles. Cette rubrique contiendra aussi des 
    						sous-rubriques. Les noms de ces rubriques devront &ecirc;tre conserv&eacute;s. 
    						Vous pouvez compl&eacute;ter le nom de la rubrique racine du cahier de texte 
    						nomm&eacute;e "Cahier de texte en-ligne" (avec un num&eacute;ro par exemple), mais 
    						devez imp&eacute;rativement garder tout le contenu de son titre actuel, sinon 
    						le cahier de texte ne fonctionnera plus.',
    'maj' => 'Si vous mettez &agrave; jour votre cahier de texte, attention : l\'op&eacute;ration peut &ecirc;tre 
    				tr&egrave;s lente et pendre beaucoup des ressources du serveur de base de donn&eacute;es. 
    				Veuillez &agrave; ne faire cette mise &agrave; jour que pendant des p&eacute;riodes de faible 
    				fr&eacute;quentation du cahier de texte (en g&eacute;n&eacute;ral, tr&egrave;s tôt le matin ou tr&egrave;s tard 
    				le soir).',
    'presentation' => '"Bonbon !" est un cahier de texte qui s\'installe &agrave; cot&eacute; de SPIP 
    						(et notamment EVA-WEB) dont il utilise les ressources (articles, 
    						mots-cl&eacute;s, rubriques).',
    'titre_boite_principale' => 'Installation - Configuration',
    'conf1' => 'Vous pouvez personnaliser les classes, sous groupes et mati&egrave;res:<br />',
    'conf2' => '- Chaque &eacute;lement (classe, groupe, mati&egrave;re) est s&eacute;par&eacute; du suivant par une virgule.<br \>
		- les &eacute;lements ne peuvent contenir de virgules ou de guillemets droits (type: "). Vous pouvez utiliser des guillemets français: «»<br \>
		- veuillez &agrave; ce qu\'il n\'y ait pas d\'espace avant ou apr&egrave;s les virgules qui s&eacute;parent les &eacute;l&eacute;ments.<br \>
		- V&eacute;rifiez que la liste des classes, des groupes, des mati&egrave;res sont bien commenc&eacute;s et finis par des guillemets.<br \>',
    'modif_renseignements' => 'Modifier les param&egrave;tres',
    'classes' => '<strong>Liste des classes : </strong>',
    'groupes' => '<strong>Liste des groupes : </strong>',
    'matieres' => '<strong>Liste des mati&egrave;res : </strong>',
    'installation_bouton' => 'Lancez l\'installation',   
    'installation' => 'L\'installation ex&#233;cutera un script de pr&#233;paration ou de mise &#224; jour du cahier de texte. Son ex&#233;cution peut-&#234;tre longue et charger le serveur de base de donn&#233;es.',
    );
?>
