<?php

	if (!defined("_ECRIRE_INC_VERSION")) return;

	$GLOBALS[$GLOBALS['idx_lang']] = array(
		// A
		'add' => 'Ajouter un Scenari',
		'available' => 'Scenari disponibles',
		// B
		// C
		// D
		// E
		'empty' => 'Aucun scenari trouvé !',
		'extract' => 'Répertoire pour l\'extraction',
		'extractfail' => 'Echec de l\'extraction !',
		'extractok' => 'Extraction terminée dans ',
		// F
		// G
		// H
		// I
		// J
		// K
		// L
		// M
		// N
		// O
		// P
		// Q
		// R
		// S
		'scenari' => 'Scenari',
		// T
		'titre1' => 'Scenari',
		'titre2' => 'Scenari pour SPIP',
		// U
		'uploadok' => 'Upload termin&eacute; avec succ&eacute;s.',
		'uploadfail' => 'Echec de l\'upload !',
		// V
		// W
		// X
		// Y
		// Z
		'zip' => 'Archive Zip'
	);

?>
