<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-menus?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'menus_description' => 'Menüs bequem im Redaktionssystem gestalten.', # MODIF
	'menus_nom' => 'Menus', # NEW
	'menus_slogan' => 'Menüs bequem im Redaktionssystem gestalten.',
	'menus_titre' => 'Menüs'
);

?>
