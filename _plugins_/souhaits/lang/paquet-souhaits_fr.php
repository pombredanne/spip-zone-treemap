<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'souhaits_description' => 'Ce plugin permet à chaque utilisateur de lister des choses qu\'il aimerait avoir, par exemple pour inspirer les personnes qui voudraient lui offrir un cadeau. Cela peut servir à une liste de naissance, de mariage, d\'anniversaire, de noël, etc.',
	'souhaits_nom' => 'À vos souhaits',
	'souhaits_slogan' => 'Lister des choses qu\'on aimerait avoir',
);

?>