<?php

	// lang/barrac_fr.php
	
	// $LastChangedRevision$
	// $LastChangedBy$
	// $LastChangedDate$

$GLOBALS['i18n_'._BARRAC_PREFIX.'_fr'] = array(

	'accessibilite' => "Accessibilit&eacute;"
, 'configuration_barre_accessibilite' => "Configuration de la barre d'accessibilit&eacute;"
, 'configurer_barrac' => "Configurer la barre d'accessibilit&eacute;"
, 'configurer_barrac_desc' => "Vous pouvez d&eacute;finir ici la position 
	et la taille de la barre d'accessibilit&eacute;.
	Les r&eacute;glages sont imm&eacute;diatement appliqu&eacute;s et le cache est 
	vid&eacute; afin de permettre la reconstruction des pages."
, 'configurer_activation_boutons' => "Activation des boutons : "
, 'configurer_activation_bouton' => "Activation de : "
, 'configurer_position_boutons' => " Position de la barre des boutons 
	&agrave; l'&eacute;cran : "
, 'configurer_position_tl' => "Positionner la barre en haut, &agrave; 
	gauche de l'&eacute;cran"
, 'configurer_position_tr' => "Positionner la barre en haut, &agrave; 
	droite de l'&eacute;cran"
, 'configurer_position_bl' => "Positionner la barre en bas, &agrave; gauche 
	de l'&eacute;cran"
, 'configurer_position_br' => "Positionner la barre en bas, &agrave; droite 
	de l'&eacute;cran"
, 'configurer_flip_boutons' => "Rotation des boutons et du fond : "
, 'configurer_position_fixed' => "Attachement de la barre : "
, 'configurer_position_fixed_desc' => "Par d&eacute;faut, la barre est 
	attach&eacute;e sur le document
	et d&eacute;filera avec lui lorsque vous d&eacute;placez l'ascenseur. 
	Vous pouvez fixer la barre sur la fen&ecirc;tre pour &eacute;viter ce d&eacute;filement 
	(attention aux effets de bord parfois ind&eacute;sirables)."
, 'configurer_position_fixer' => "Fixer la position de la barre sur la 
	fen&ecirc;tre."
, 'configurer_flip_pointer' => "Pointeur toujours vers le centre de l'&eacute;cran."
, 'configurer_flip_horizontal' => "Miroir horizontal du fond si la barre 
	est positionn&eacute;e &agrave; gauche de l'&eacute;cran."
, 'configurer_flip_vertical' => "Miroir vertical du fond si la barre est 
	positionn&eacute;e en bas de l'&eacute;cran."
, 'configurer_flip_contextuel' => "Rotation contextuel (&agrave; n'appliquer 
	que pour les fonds d&eacute;bordants)"
, 'configurer_presentation_boutons' => "Pr&eacute;sentation de la barre 
	des boutons : "
, 'configurer_presenter_h' => "Pr&eacute;sentation horizontale"
, 'configurer_presenter_v' => "Pr&eacute;sentation verticale"
, 'configurer_espace_taille_boutons' => "Taille et espacement des boutons 
	(en pixels) : "
, 'configurer_espace_boutons' => "Espace entre les boutons : "
, 'configurer_taille_boutons' => "Taille des boutons : "

, 'configurer_mobile_no_display' => "Ne pas afficher sur les mobiles"
, 'configurer_mobile_no_display_desc' => "La présence de la barre d'accessibilité n'est pas toujours souhaitable sur 
	les téléphones mobiles et autres ordinateurs de petite taille. Cochez l'option ci-dessous pour cacher
	la barre sur ces appareils (expérimental. Seuls les compatibles WebKit sont pris en charge)."

, 'configurer_familles' => "Famille de boutons : "
, 'configurer_familles_defaut' => "Il n'y a qu'une seule famille de boutons 
	disponible. Elle sera utilis&eacute;e par d&eacute;faut.
	Consultez - si besoin - la documentation (voir lien sur la gauche de l'&eacute;cran) 
	pour ajouter un famille de boutons."
, 'configurer_familles_choisir' => "Choisissez une famille de boutons 
	dans la liste : "
, 'configurer_options_desc' => "Les options de r&eacute;glages permettent 
	d'optimiser l'accessibilit&eacute; de vos pages.
	La barre d'accessibilit&eacute; ne corrige pas un site mal construit et ne permet 
	pas de rendre accessible un site
	qui ne l'est pas.<br />
	Les premiers r&eacute;glages de la barre d'accessibilit&eacute; sont bas&eacute;s 
	sur les squelettes SPIP fournis dans la distribution.
	Vous pouvez &eacute;galement construire vos propres feuilles de styles (CSS) pour 
	les modes propos&eacute;s
	et en demander l'import automatique ici.
	La conception de feuilles de style personnalis&eacute;es est d'ailleurs largement 
	conseill&eacute;e afin de rendre votre site le plus accessible possible.
	Vous trouverez dans le r&eacute;pertoire du plugin quelques fichiers .css en exemple.
	"
, 'configurer_pointeur' => "Pointeur du contenu de la page"
, 'configurer_pointeur_desc' => "Le pointeur permet au visiteur de positionner 
	son curseur directement sur le contenu de la page.
	En g&eacute;n&eacute;ral, il s'agit du d&eacute;but de l'article. 
	Si vous utilisez les squelettes originaux de SPIP, il est inutile de modifier 
	ce champ. Sinon, indiquez ici le nom de l'ancre
	qui d&eacute;bute l'article de vos squelettes."
, 'configurer_pointeur_ancre' => "Nom (id) de votre ancre de d&eacute;but 
	de contenu : "
, 'configurer_selectionner_css' => "Vous pouvez &eacute;galement s&eacute;lectionner 
	un fichier CSS pr&eacute;sent sur votre serveur."
, 'configurer_indiquez_fichier' => "Indiquez ici le nom du fichier CSS 
	: "
, 'configurer_grossir' => "Taille des caract&egrave;res"
, 'configurer_grossir_desc' => "Vous pouvez grossir globalement la taille 
	des caract&egrave;res (font-size du &lt;body&gt;)."
, 'configurer_grossir_global' => "Grossissement global (%) : "
, 'configurer_grossir_cssfile' => "Grossissement d&eacute;fini dans le 
	fichier CSS : "
, 'configurer_espacer' => "Espacer les liens"
, 'configurer_espacer_desc' => "Cette option vous permet d'espacer les 
	liens (&lt;a&gt;). Pas d&eacute;faut, cet espacement est de 
	2ex (largeur moyenne de deux caract&egrave;res) &agrave; gauche et &agrave; droite 
	du lien."
, 'configurer_espacer_global' => "Espacer globalement les liens."
, 'configurer_encadrer' => "Encadrer les paragraphes"
, 'configurer_encadrer_desc' => "Cette option vous permet d'encadrer les 
	titres et les paragraphes (class='.titre' et class='.texte'). 
	L'&eacute;paisseur du cadre est de 12 pixels.
	La marge int&eacute;rieure est de 12 pixels."
, 'configurer_encadrer_global' => "Encadrer globalement les paragraphes."
, 'configurer_inverser' => "Inverser les couleurs"
, 'configurer_inverser_desc' => "Cette option utilise par d&eacute;faut 
	une fonction qui n'est reconnue que sous IE.
	Elle vous permet d'inverser globalement les couleurs.<br />
	Pour &eacute;tendre cette fonctionnalit&eacute; &agrave; tous les navigateurs, 
	il est conseill&eacute; de mettre en place un fichier CSS."
, 'configurer_inverser_global' => "Inverser globalement les couleurs."
, 'configurer_valider_desc' => "Valider cette configuration : "

// titre des boutons
, 'aller_au_contenu' => "Aller directement au contenu de la page"
, 'grossir_taille_caracteres' => "Grossir la taille des caract&egrave;res"
, 'reduire_taille_caracteres' => "R&eacute;duire la taille des caract&egrave;res"
, 'espacer_blocs' => "Espacer les liens dans les blocs de texte"
, 'retablir_blocs' => "R&eacute;tablir les espaces des liens entre blocs"
, 'encadrer_paragraphes' => "Encadrer les paragraphes"
, 'decadrer_paragraphes' => "Retirer les encadrements forc&eacute;s"
, 'inverser_couleurs' => "Inverser les couleurs"
, 'retrouver_couleurs' => "Revenir aux couleurs d'origine"
, 'erreur_installation' => "Erreur d'installation. Un ou plusieurs fichiers 
	sont manquants. SVP, r&eacute;installez le plugin."
, 'nav_caracteres_grossir' => "Grossir la taille des caract&egrave;res"
, 'nav_espacer_liens' => "Espacer les liens"
, 'nav_encadrer_liens' => "Encadrer les paragraphes"

);

?>
