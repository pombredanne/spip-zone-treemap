<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Module: relecture
// Langue: fr

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// B
	'bouton_ajouter_commentaire' => 'Ajouter un commentaire',
	'bouton_ajouter_relecteur' => 'Ajouter un relecteur',
	'bouton_configurer_relecture' => 'Administrer la relecture',
	'bouton_moderer' => 'Modérer',
	'bouton_modifier' => 'Modifier',
	'bouton_modifier_commentaire' => 'Modifier le commentaire',
	'bouton_modifier_relecture' => 'Modifier la relecture',
	'bouton_ouvrir_relecture' => 'Ouvrir un cycle de relecture',
	'bouton_participer_relecture' => 'Participer à la relecture',
	'bouton_saisir_commentaire' => 'Saisir le texte de votre commentaire',
	'bouton_voir_relecture' => 'Voir',
	'bouton_voir_relectures' => 'Afficher l\'historique des relectures',

// E
	'explication_ajout_commentaire' => 'Pour ajouter des commentaires à cet élément cliquez sur le bouton de la barre d\'outils. Au préalable, si vous souhaitez associer ce commentaire à un emplacement ou à une portion du texte veillez à positionner correctement votre curseur dans le texte.',

// I
	'info_1_commentaire_ferme' => '1 commentaire fermé',
	'info_1_commentaire' => '1 commentaire déposé',
	'info_1_relecture' => '1 relecture',
	'info_aucun_commentaire_ferme' => 'Aucun commentaire fermé',
	'info_aucun_commentaire' => 'Aucun commentaire déposé',
	'info_aucun_relecture' => 'aucune relecture',
	'info_commenter_relecture' => 'Ajouter des commentaires sur cet élément jusqu\'au @date@',
	'info_nb_commentaires_fermes' => '@nb@ commentaires fermés',
	'info_nb_commentaires' => '@nb@ commentaires déposés',
	'info_nb_relectures' => '@nb@ relectures',
	'info_numero_relecture' => 'relecture numéro',
	'info_ouverture_relecture' => 'Ouvert le @date@ sur la révision @revision@ de l\'article',
	'info_periode_commentaire_fermee' => 'L\'ajout de commentaire n\'est plus possible depuis le @date@',
	'info_periode_commentaire_ouverte' => 'Commentaires autorisés jusqu\'au @date@',

// L
	'label_action' => ' ',
	'label_article_chapo' => 'Chapeau',
	'label_article_descr' => 'Descriptif',
	'label_article_ps' => 'Post-Scriptum',
	'label_article_relu' => 'Article concerné',
	'label_article_texte' => 'Texte',
	'label_auteur_commentaire' => 'Rédigé par @auteur@',
	'label_commentaires_acceptes' => 'Acceptés',
	'label_commentaires_ouverts' => 'Ouverts',
	'label_commentaires_poubelle' => 'Supprimés',
	'label_commentaires_refuses' => 'Refusés',
	'label_commentaire_statut' => 'Statut du commentaire',
	'label_commentaire_texte' => 'Texte du commentaire',
	'label_commentaire_reponse' => 'Réponse au commentaire',
	'label_date_cloture_commentaire' => 'Fermé le',
	'label_date_cloture_relecture' => 'Date',
	'label_date_modif' => 'Date',
	'label_date_fin_commentaire_abrege' => 'Fin :',
	'label_date_fin_commentaire' => 'Date de fin des commentaires',
	'label_date_ouverture_commentaire' => 'Ouvert le',
	'label_date_ouverture_relecture' => 'Date',
	'label_date_statut' => 'Date',
	'label_description' => 'Description',
	'label_element' => 'Concerne',
	'label_emetteur' => 'Emis par',
	'label_nb_commentaires_a_traiter' => 'À traiter',
	'label_revision_cloture' => 'Révision',
	'label_revision_ouverture_abrege' => 'Rév :',
	'label_revision_ouverture' => 'Révision',
	'label_statut' => 'Statut',

// R
	'resume_table_relectures' => 'Liste des relectures',
	'resume_table_commentaires' => 'Liste des commentaires',

// T
	'texte_commentaire_accepte' => 'Accepté',
	'texte_commentaire_ouvert' => 'Ouvert',
	'texte_commentaire_refuse' => 'Non retenu',
	'texte_commentaire_poubelle' => 'Supprimé',
	'texte_instituer_relecture' => 'Cette relecture est :',
	'texte_relecture_fermee' => 'Fermée',
	'texte_relecture_ouverte' => 'Ouverte',
	'titre_boite_accueil_relectures' => 'Vous êtes concernés par les relectures ci-dessous.',
	'titre_boite_cloture' => 'Clôture',
	'titre_boite_ouverture' => 'Ouverture',
	'titre_boite_relecture_article' => 'Relecture en cours',
	'titre_boite_statistiques_commentaires' => 'STATISTIQUES SUR CET ELEMENT :',
	'titre_boite_textes_article' => 'TEXTES DE L\'ARTICLE EN RÉVISION @revision@',
	'titre_commentaire' => 'Commentaire',
	'titre_commentaire_accepte' => 'Commentaire accepté',
	'titre_commentaire_ouvert' => 'Commentaire ouvert',
	'titre_commentaire_refuse' => 'Commentaire non retenu',
	'titre_commentaire_poubelle' => 'Commentaire supprimé',
	'titre_commentaires' => 'Commentaires',
	'titre_liste_autres_relectures' => 'Autres relectures de l\'article',
	'titre_liste_commentaires' => 'Commentaires déjà émis pour cette relecture',
	'titre_liste_organisations_relectures' => 'Relectures que vous administrez',
	'titre_liste_participations_relectures' => 'Relectures auxquelles vous participez',
	'titre_liste_relecteurs' => 'Relecteurs',
	'titre_menu_commentaires' => 'Commentaires déjà émis',
	'titre_relecture_fermee' => 'Relecture fermée',
	'titre_relecture_ouverte' => 'Relecture ouverte',
	'titre_relecture' => 'Relecture',
	'titre_relectures' => 'Relectures',

);
?>
