<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'lilyspip_plugin' => 'Plugin Lilyspip',
'parametrage_lilyspip' => 'Param&eacute;trages',
'info_message' => "Cette page permet de sp&eacute;cifier l'adresse du serveur Lilypond pour l'insertion de notations musicales.",
'adresse_serveur' => 'Adresse du serveur Lilypond (http://www.site.net/lilyserv.php) :',
'previsualisation' => 'Pr&eacute;visualisation :',
);
?>