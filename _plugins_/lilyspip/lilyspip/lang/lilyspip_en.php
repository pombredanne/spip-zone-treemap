<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'lilyspip_plugin' => 'Plugin Lilyspip',
'parametrage_lilyspip' => 'Settings for Lilypond server',
'info_message' => 'This page lets you specify Lilypond server address to embed music notation in all text.',
'adresse_serveur' => 'Server address',
'previsualisation' => 'Previsualisation :',
);
?>