<?php


	$GLOBALS[$GLOBALS['idx_lang']] = array(
		'ajouter' => "Ajouter",
		'erreur_url_edit' => 'Erreur: URL incorrecte ou d&eacute;j&agrave; pr&eacute;sente',
		'formatage' => 'Format des URLs', 
		'filtre' => 'Filtre  (fonction php: strtolower, ucfirst,....)',
		'info_urledit' => 'Attribuer une URL',
		'longueur_min' => 'Longueur minimum',
		'longueur_max' => 'Longueur maximum',
		'separateur' => 'S&eacute;parateur', 		
    	'supprimer' => 'Supprimer',  		
		'titre_urledit' => 'GESTION DES URLS',
		'urls_editables' => 'URLS &eacute;ditables',
		'version_patchee' => 'Titre-de-l-article -Rubrique- (version patch&eacute;e urledit)',
		
	);

?>
