<?php


	/**
	 * SPIP-Mémos : plugin de gestion de lettres d'information
	 *
	 * Copyright (c) 2006
	 * Agence Artégo http://www.artego.fr
	 *  
	 * Ce programme est un logiciel libre distribue sous licence GNU/GPL.
	 * Pour plus de details voir le fichier COPYING.txt.
	 *  
	 **/


	$GLOBALS[$GLOBALS['idx_lang']] = array(


		'memos' => "Notas de serviço",
		'configuration_identifiant_site' => "Configuração do identificante do site",
		'identifiant_site' => "Identificante do site :",
		'valider' => "Validar",
		'titre_boite_memos' => "Notas de serviço",
		'titre_boite_alertes' => "Alertas da Artégo",

		'Z' => 'ZZzZZzzz'

	);

?>