<?php


	/**
	 * SPIP-Lettres : plugin de gestion de lettres d'information
	 *
	 * Copyright (c) 2006
	 * Agence Artégo http://www.artego.fr
	 *  
	 * Ce programme est un logiciel libre distribue sous licence GNU/GPL.
	 * Pour plus de details voir le fichier COPYING.txt.
	 *  
	 **/


	$GLOBALS[$GLOBALS['idx_lang']] = array(


		'memos' => "Mémos",
		'configuration' => "Configuration",
		'spip_memos_serveur' => "Adresse du site",
		'spip_memos_client' => "Identifiant du client",
		'spip_memos_fond' => "Fond des mémos utilisé sur le serveur",
		'valider' => "Valider",
		'titre_boite_memos' => "MEMOS&nbsp;&nbsp;&nbsp;",
		'titre_boite_alertes' => "ALERTES&nbsp;&nbsp;&nbsp;",

		'Z' => 'ZZzZZzzz'

	);

?>