<?php
// Ceci est un fichier langue de SPIP,
// qui permet de personnaliser (ou traduire) 
// les textes de squelettes d'EVA.
$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'accueil_site' => 'Accueil',
'articles' => 'Articles',
'aujourd_hui' => 'Aujourd&#8217;hui',
'article_precedent' => 'Article pr&eacute;c&eacute;dent',
'article_suivant' => 'Article suivant',
'autres_breves' => 'Autres br&egrave;ves',
'' => '',

// B
'breves' => 'Br&egrave;ves',
'' => '',

// C
'contributions_forum' => 'contribution(s) au forum',
'copyright' => 'Ce site utilise <a href="http://www.spip.net/fr">(#SPIP_VERSION)</a> et utilise le squelette <a href="http://spip-edu.edres74.net/eva/">EVA-Web 2.3</a>',
'' => '',

// D
'derniers_articles' => 'Derniers articles',
'dernieres_breves' => 'Derni&egrave;res br&egrave;ves',
'dernier_article' => 'Dernier article',
'documents' => 'Documents',
'desole_page_indisponible' => '<h3>D&eacute;sol&eacute;, la page que vous demandez n&#8217;existe pas, n&#8217;est pas encore publi&eacute;e ou ne l&#8217;est plus.</h3>Utilisez la bo&icirc;te de recherche ci-dessous pour trouver la page voulue.',
'' => '',

// E
'en_reponse' => 'En r&eacute;ponse &agrave; :',
'evenements_a_venir' => '&eacute;v&eacute;nements &agrave; venir',
'aucun_evenements_a_venir' => 'Il n&#8217;y a aucun &eacute;v&eacute;nement &agrave; venir.',
'evenements_passes' => '&eacute;v&eacute;nements pass&eacute;s',
'aucun_evenements_passes' => 'Il n&#8217;y a aucun &eacute;v&eacute;nement pass&eacute;s.',
'' => '',

// F
'forum_article' => 'Forum de l&#8217;article :',
'forum_breve' => 'Forum de la br&egrave;ve :',
'fermer' => 'Fermer',
'' => '',

// I
'inscription' => 'Inscription au site',
'image_suivante' => 'Image suivante',
'' => '',

// L
'le' => 'Le :',
'lien_premier_article' => 'Lancer le diaporama',
'lire_suite' => 'Lire la suite',
'' => '',

// M
'meme_rubrique' => 'Dans la m&ecirc;me rubrique',	
'mots_clefs' => 'Mots-cl&eacute;s',
'mots_clefs_meme_groupe' => 'Mots-cl&eacute;s dans le m&ecirc;me groupe',
'mise_a_jour' => 'Mise &agrave; jour:',
'' => '',

// N
'notes_bas_page' => 'Notes',
'nouveautes_web' => 'Derniers sites r&eacute;f&eacute;renc&eacute;s',
'' => '',

// P
'par' => 'Par :',
'plein_ecran' => 'Voir les photos en taille r&eacute;elle',
'premier_article' => 'Premier article',
'petition' => 'P&eacute;tition',
'post_scriptum' => 'Post-scriptum',
'presentation_doc_aide' => 'Pr&eacute;sentation<br />Documentation<br />Aide aux r&eacute;dacteurs',
'page_indisponible' => 'Page indisponible - Erreur 404',
'' => '',

// S
'syndiquer_breves' => 'Syndiquer les br&egrave;ves du site',
'syndiquer_breves_rubrique' => 'Syndiquer les br&egrave;ves de cette rubrique',
'syndiquer_rubrique' => 'Syndiquer cette rubrique',
'syndiquer_site' => 'Syndiquer tout le site',
'' => '',

// R
'repondre_article' => 'R&eacute;pondre &agrave; cet article',	
'repondre_breve' => 'R&eacute;pondre &agrave; cette br&egrave;ve',	
'repondre_forum' => 'R&eacute;pondre sur un forum',
'referencer_site' => 'R&eacute;f&eacute;rencer un site',
'rechercher_sur_le_site' => 'Rechercher sur le site :',
'' => '',

// T
'themes' => 'Th&egrave;mes :',
'' => '',

// V
'voir_en_ligne' => 'Voir en ligne :',
'vous_etes_ici' => 'Vous &ecirc;tes ici :',
'autres_versions' => 'Autres versions&nbsp;:',
'' => '',

// W
'webmaster' => 'Webmestre',
'' => '',



);
?>
