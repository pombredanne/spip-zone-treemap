<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'Het adres <@var_email@> zal na verificatie aan de lijst <@liste@> worden toegevoegd. Gelieve het bericht te beantwoorden dat je werd toegestuurd.',

	// C
	'confirm' => 'Een vraag voor bevestiging is gestuurd naar <@var_email@>.',

	// D
	'deja' => '&lt;@var_email@&gt; : je bent al ingeschreven op de lijst &lt;@liste@&gt;.',
	'desabo' => 'Uitschrijving is voltooid.',

	// F
	'fermer' => 'sluiten',

	// I
	'inscription' => 'Inschrijving bij <@liste@>',

	// M
	'mail_removed' => '

Het adres <@var_email@> is van de lijst @liste@ geschrapt.

Bij problemen of indien je de uitschrijving niet hebt gevraagd, kan je <@responsable@> contacteren.

Bedankt en tot ziens.
',
	'merci' => 'Bedankt.',

	// P
	'pasabo' => 'Het adres <@var_email@> is niet ingeschreven op de lijst <@liste@>.',
	'patientez' => 'Even geduld...',

	// Q
	'quitter' => 'Uitschrijving',

	// S
	'subject_removed' => 'Je adres is geschrapt van de lijst @liste@.',

	// T
	'titrefenetre' => 'Inschrijving',

	// V
	'veuillez' => 'Gelieve je adres op te geven.',
	'votreemail' => 'Je e-mail :'
);

?>
