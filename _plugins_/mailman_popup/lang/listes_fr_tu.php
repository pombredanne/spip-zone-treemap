<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=fr_tu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'L\'adresse &lt;@var_email@&gt; sera ajoutée à la liste &lt;@liste@&gt; après vérification. Merci de répondre au message qui vient de t\'être envoyé.',

	// C
	'confirm' => 'Une demande de confirmation a été adressée à &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt; : tu es déjà inscrit(e) à la liste &lt;@liste@&gt;.',
	'desabo' => 'Désabonnement effectué.',

	// F
	'fermer' => 'fermer',

	// I
	'inscription' => 'Inscription à &lt;@liste@&gt;',

	// M
	'mail_removed' => '

L\'adresse <@var_email@> a été supprimée de la liste @liste@.

En cas de problème, ou si vous n\'avez pas demandé ce désabonnement,
veuillez écrire à <@responsable@>.

Au revoir, et merci.
',
	'merci' => 'Merci.',

	// P
	'pasabo' => 'L\'adresse &lt;@var_email@&gt; n\'est pas abonnée à la liste &lt;@liste@&gt;.',
	'patientez' => 'Patiente stp...',

	// Q
	'quitter' => 'Désabonnement',

	// S
	'subject_removed' => 'Ton adresse a été supprimée de la liste @liste@.',

	// T
	'titrefenetre' => 'Inscription',

	// V
	'veuillez' => 'Précise ton adresse.',
	'votreemail' => 'Ton email :'
);

?>
