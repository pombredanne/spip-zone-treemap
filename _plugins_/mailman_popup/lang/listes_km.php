<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=km
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'L\'adresse &lt;@var_email@&gt; sera ajoutée à la liste &lt;@liste@&gt; après vérification. Merci de répondre au message qui vient de vous être envoyé.', # NEW

	// C
	'confirm' => 'Une demande de confirmation a été adressée à &lt;@var_email@&gt;.', # NEW

	// D
	'deja' => '&lt;@var_email@&gt; : vous êtes déjà inscrit(e) à la liste &lt;@liste@&gt;.', # NEW
	'desabo' => 'Désabonnement effectué.', # NEW

	// F
	'fermer' => 'បិទ',

	// I
	'inscription' => 'ការចុះឈ្មោះ ទៅ &lt;@liste@&gt;',

	// M
	'mail_removed' => '

L\'adresse <@var_email@> a été supprimée de la liste @liste@.

En cas de problème, ou si vous n\'avez pas demandé ce désabonnement,
veuillez écrire à <@responsable@>.

Au revoir, et merci.
', # NEW
	'merci' => 'អរគុណ។',

	// P
	'pasabo' => 'L\'adresse &lt;@var_email@&gt; n\'est pas abonnée à la liste &lt;@liste@&gt;.', # NEW
	'patientez' => 'សូមព្យាយាម...',

	// Q
	'quitter' => 'Désabonnement', # NEW

	// S
	'subject_removed' => 'អាសយដ្ឋាន របស់អ្នក ត្រូវបានលុបចេញហើយ ពី បញ្ជី @liste@។',

	// T
	'titrefenetre' => 'ការចុះឈ្មោះ',

	// V
	'veuillez' => 'សូមបញ្ជាក់ អាសយដ្ឋាន របស់អ្នក។',
	'votreemail' => 'អ៊ីមែវល៍ របស់អ្នក ៖'
);

?>
