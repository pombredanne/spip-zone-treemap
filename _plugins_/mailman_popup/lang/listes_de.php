<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'Die Adresse &lt;@var_email@&gt; wird der Liste &lt;@liste@&gt; nach einer Überprüfung hinzugefügt. Bitte beantworten Sie die Mail, welche Ihnen zur Überprüfung Ihrer Adresse geschickt wurde.',

	// C
	'confirm' => 'Die Bitte um Bestätigung wurde an &lt;@var_email@&gt; geschickt.',

	// D
	'deja' => '&lt;@var_email@&gt; : Sie sind bereits Mitglied der Liste &lt;@liste@&gt;.',
	'desabo' => 'Abmeldung erfolgreich.',

	// F
	'fermer' => 'schließen',

	// I
	'inscription' => 'Abonnieren von <@liste@>',

	// M
	'mail_removed' => '

Die Adresse <@var_email@> wurde von der Liste @liste@ glöscht.

Falls ein Problem auftritt oder Sie die Liste nicht abbestellt haben, schreiben Sie bitte an <@responsable@>.

Vielen Dank und auf Wiedersehen!
',
	'merci' => 'Danke.',

	// P
	'pasabo' => 'Die Adresse &lt;@var_email@&gt; gehört nicht  zu den Empfängern der Liste &lt;@liste@&gt;.',
	'patientez' => 'Bitte warten...',

	// Q
	'quitter' => 'Abbestellen',

	// S
	'subject_removed' => 'Ihre Adresse wurde aus der Liste @liste@ gelöscht.',

	// T
	'titrefenetre' => 'Abonnieren',

	// V
	'veuillez' => 'Bitte geben Sie Ihre Adresse an.',
	'votreemail' => 'Ihre E-Mail Adresse:'
);

?>
