<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'The address <@var_email@> will be added to the list <@liste@> once it has been checked. Please reply to the message which has just been sent to you.',

	// C
	'confirm' => 'Na <@var_email@> bola poslaná žiadosť o potvrdenie.',

	// D
	'deja' => '<@var_email@>: Do konferencie <@liste@> ste sa už zaregistrovali.',
	'desabo' => 'Registrácia zrušená.',

	// F
	'fermer' => 'Zatvoriť',

	// I
	'inscription' => 'Registrácia do <@liste@>',

	// M
	'mail_removed' => '

Adresa <@var_email@> bola odstránená z konferencie @liste@.

Ak sa vyskytol problém, alebo ak ste o toto zrušenie nepožiadali,
prosím, napíšte na <@responsable@>.

Ďakujeme a dovidenia.
',
	'merci' => 'Ďakujeme.',

	// P
	'pasabo' => 'Adresa <@var_email@> nie je zaregistrovaná v konferencii <@liste@>.',
	'patientez' => 'Prosím počkajte...',

	// Q
	'quitter' => 'Zrušenie',

	// S
	'subject_removed' => 'Vaša adresa bola odstránená z konferencie @liste@.',

	// T
	'titrefenetre' => 'Zaregistrovať sa',

	// V
	'veuillez' => 'Prosím, zadajte svoju adresu',
	'votreemail' => 'Váš e-mail:'
);

?>
