<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=bg
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'Адресът &lt;@var_email@&gt; ще бъде добавен към пощенския списък &lt;@liste@&gt; след неговата проверка. Моля да отговорите на съобщението, което току-що Ви бе изпратено.',

	// C
	'confirm' => 'Заявка за потвърждение бе изпратена на &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt;: Вече сте абонирани за пощенския списък &lt;@liste@&gt;.',
	'desabo' => 'Абонаментът е заличен.',

	// F
	'fermer' => 'затваряне',

	// I
	'inscription' => 'Абонамент за &lt;@liste@&gt;',

	// M
	'mail_removed' => '

Адресът &lt;@var_email@&gt; бе премахнат от пощенския списък @liste@.

Ако има проблем или ако не сте дали заявка за такъв отказ
пишете на: &lt;@responsable@&gt;.

Довиждане и благодаря.
',
	'merci' => 'Благодаря.',

	// P
	'pasabo' => 'Адресът &lt;@var_email@&gt; не е абониран за пощенския списък &lt;@liste@&gt;.',
	'patientez' => 'Изчакайте...',

	// Q
	'quitter' => 'Отказване',

	// S
	'subject_removed' => 'Адресът Ви бе премахнат от пощенския списък @liste@.',

	// T
	'titrefenetre' => 'Записване',

	// V
	'veuillez' => 'Напишете адреса си.',
	'votreemail' => 'Електронен адрес:'
);

?>
