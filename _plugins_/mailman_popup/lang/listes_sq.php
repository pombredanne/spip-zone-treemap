<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=sq
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'Adresa &lt;@var_email@&gt; do të shtohet në listën &lt;@liste@&gt; sapo të shqyrtohet. Ju lutemi, përgjigjiuni mesazhit të konfirmimit që do ju dërgohet me email.',

	// C
	'confirm' => 'Kërkesa për konfirmim u dërgua në adresën  &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt; : kjo adresë ishte e pranishme në listën &lt;@liste@&gt;.',
	'desabo' => 'Fshirja nga lista u krye.',

	// F
	'fermer' => 'mbyll',

	// I
	'inscription' => 'Regjistrim në listën &lt;@liste@&gt;',

	// M
	'mail_removed' => '

Adresa <@var_email@> u fshi nga lista @liste@.

Në rast se ka probleme apo nëse kjo fshirje ishte e padëshiruar,
shkruani tek <@responsable@>.

Faleminderit.
',
	'merci' => 'Faleminderit.',

	// P
	'pasabo' => 'Adresa &lt;@var_email@&gt; nuk është e pranishme në listën &lt;@liste@&gt;.',
	'patientez' => 'Ju lutem prisni...',

	// Q
	'quitter' => 'Fshirje nga lista',

	// S
	'subject_removed' => 'Adresa juaj është fshirë nga lista @liste@.',

	// T
	'titrefenetre' => 'Regjistrim',

	// V
	'veuillez' => 'Vendosni adresën tuaj email.',
	'votreemail' => 'Adresa email:'
);

?>
