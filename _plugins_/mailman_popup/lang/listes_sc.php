<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=sc
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'S\'indiritzu <@var_email@> at a esser annantu a s\'elencu <@liste@> pustis verificadu. Risponde a su messazu de cunfirma chi t\'at a esser imbiadu, pro praghere.',

	// C
	'confirm' => 'Sa dimanda de cunfirma est istada imbiada a <@var_email@>.',

	// D
	'deja' => '&lt;@var_email@&gt; : ses zai marcadu in sa lista &lt;@liste@&gt;.',
	'desabo' => 'Sa cantzellatzione dae sa lista est istada fata.',

	// F
	'fermer' => 'cunza',

	// I
	'inscription' => 'Iscritzione a <@liste@>',

	// M
	'mail_removed' => '

S\'indiritzu <@var_email@> est istadu cantzelladu dae sa  lista @liste@.

In caso di problemi, oppure se tale cancellazione non è stata richiesta,
scrivere a <@responsable@>.

Grazie.
',
	'merci' => 'Gràtzias.',

	// P
	'pasabo' => 'S\'indiritzu <@var_email@> non resurtat abonadu a sa lista <@liste@>.',
	'patientez' => 'Iseta pro praghere...', # MODIF

	// Q
	'quitter' => 'Cantzellatzione dae sa lista',

	// S
	'subject_removed' => 'S\'indiritzu tuo nch\'est istadu cantzelladu dae sa lista @liste@.',

	// T
	'titrefenetre' => 'Iscritzione', # MODIF

	// V
	'veuillez' => 'Pone s\'indiritzu tuo de posta eletrònica.',
	'votreemail' => 'Indiritzu de post.e.:'
);

?>
