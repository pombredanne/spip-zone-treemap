<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=oc_ni
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'L\'adreça &lt;@var_email@&gt; serà ajustada à la tièra &lt;@liste@&gt; après verificacion. Mercé de respoandre au messatge que vene de v\'èstre mandat.',

	// C
	'confirm' => 'Une demanda de confirmacion es estada mandada à &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt; : siètz ja inscrich sus la tièra &lt;@liste@&gt;.',
	'desabo' => 'Desabonament efetuat.',

	// F
	'fermer' => 'plegar',

	// I
	'inscription' => 'Inscripcion à &lt;@liste@&gt;',

	// M
	'mail_removed' => '

L\'adreça &lt;@var_email@&gt; es estada suprimida de la tièra @liste@.

En cas de problema, ò s\'avetz pas demandat aquest desabonament,
vorgatz escriure à &lt;@responsable@&gt;.

À ben lèu, e mercé.
',
	'merci' => 'Mercé.',

	// P
	'pasabo' => 'L\'adreça &lt;@var_email@&gt; es pas abonada à la tièra &lt;@liste@&gt;.',
	'patientez' => 'Vorgatz esperar...',

	// Q
	'quitter' => 'Desabonament',

	// S
	'subject_removed' => 'La voastra adreça es estada suprimida de la tièra @liste@.',

	// T
	'titrefenetre' => 'Inscripcion',

	// V
	'veuillez' => 'Vorgatz precisar la voastra adreça.',
	'votreemail' => 'Lo voastre e-mail :'
);

?>
