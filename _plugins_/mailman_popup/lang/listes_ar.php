<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=ar
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'ستتم اضافة العنوان &lt;@var_email@&gt; الى القائمة البريدية &lt;@liste@&gt; بعد التدقيق. الرجاء الرد على الرسالة التي استلمتها.',

	// C
	'confirm' => 'تم ارسال طلب تثبيت الى &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt;: انت مسجل مسبقاً في القائمة البريدية &lt;@liste@&gt;.',
	'desabo' => 'تم الغاء الاشتراك.',

	// F
	'fermer' => 'اقفال',

	// I
	'inscription' => 'التسجيل في &lt;@liste@&gt;',

	// M
	'mail_removed' => '

تم حذف العنوان &lt;@var_email@&gt; من القائمة البريدية @liste@.

في حال حصول مشكلة، او اذا لم تكن قد طلبت الغاء الاشتراك،
الرجاء الكتابة الى &lt;@responsable@&gt;.

شكراً والسلام.
',
	'merci' => 'شكراً.',

	// P
	'pasabo' => 'العنوان &lt;@var_email@&gt; ليس مشتركاً في القائمة البريدية &lt;@liste@&gt;. ',
	'patientez' => 'الرجاء الانتظار...',

	// Q
	'quitter' => 'الغاء الاشتراك',

	// S
	'subject_removed' => 'تم حذف عنوانك من القائمة البريدية @liste@.',

	// T
	'titrefenetre' => 'تسجيل',

	// V
	'veuillez' => 'الرجاء ادخال عنوانك.',
	'votreemail' => 'عنوانك البريدي:'
);

?>
