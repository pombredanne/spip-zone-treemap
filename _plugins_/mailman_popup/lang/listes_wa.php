<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=wa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'L\' adresse &lt;@var_email@&gt; serè radjoutêye al copinreye &lt;@liste@&gt; après verifiaedje. Gråces di responde a l\' emile ki vs a stî evoyî.',

	// C
	'confirm' => 'Ene dimande d\' acertinaedje a stî evoyeye a &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt;: vos estoz ddja abouné(êye) al copinreye &lt;@liste@&gt;.',
	'desabo' => 'Dizabounaedje fwait.',

	// F
	'fermer' => 'clôre',

	// I
	'inscription' => 'Abounmint a <@liste@>',

	// M
	'mail_removed' => '

L\' adresse <@var_email@> a stî disfacêye del copinreye @liste@.

S\' i gn a-st on problinme, ou si vos n\' avîz nén dmandé l\' dizabounmint,
sicrijhoz a <@responsable@>.

Ki ça vos våye bén, et gråces.
',
	'merci' => 'Gråces.',

	// P
	'pasabo' => 'L\' adresse &lt;@var_email@&gt; n\' est nén abounêye al copinreye &lt;@liste@&gt;.',
	'patientez' => 'Tårdjîz ene miete s\' i vs plait...',

	// Q
	'quitter' => 'Si dizabouner',

	// S
	'subject_removed' => 'L\' adresse da vosse a stî disfacêye del copinreye @liste@.',

	// T
	'titrefenetre' => 'Edjîstraedje',

	// V
	'veuillez' => 'Dinez voste adresse emile s\' i vs plait.',
	'votreemail' => 'L\' emile da vosse:'
);

?>
