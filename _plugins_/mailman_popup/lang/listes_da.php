<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=da
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'Adressen &lt;@var_email@&gt; vil blive føjet til listen &lt;@liste@&gt; når den er blevet kontrolleret. Vær venlig at besvare den besked, som netop er blevet sendt til dig.',

	// C
	'confirm' => 'En anmodning om bekræftelse er blevet sendt til &lt;@var_email@&gt;.',

	// D
	'deja' => ' &lt;@var_email@&gt;: Du abonnerer allerede på listen &lt;@liste@&gt;.',
	'desabo' => 'Abonnementet er opsagt.',

	// F
	'fermer' => 'luk',

	// I
	'inscription' => 'Abonnement på &lt;@liste@&gt;',

	// M
	'mail_removed' => '

Adressen &lt;@var_email@&gt; er slettet fra listen @liste@.

Hvis der er et problem, eller hvis du ikke har bedt om denne opsigelsen
så skriv venligst til &lt;@responsable@&gt;.

Farvel og tak.
',
	'merci' => 'Fak.',

	// P
	'pasabo' => 'Adressen &lt;@var_email@&gt; abonnerer ikke på listen &lt;@liste@&gt;.',
	'patientez' => 'Vent venligst...',

	// Q
	'quitter' => 'Opsigelse',

	// S
	'subject_removed' => 'Din adresse er fjernet fra listen @liste@.',

	// T
	'titrefenetre' => 'Abonner',

	// V
	'veuillez' => 'Oplys venligst din adresse.',
	'votreemail' => 'Din email:'
);

?>
