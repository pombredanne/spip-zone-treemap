<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'La dirección &lt;@var_email@&gt; será agregada a la lista &lt;@liste@&gt; luego de ser verificada. Se ruega responder al mensaje que acaba de ser enviado.',

	// C
	'confirm' => 'Un pedido de confirmación fue enviado a  &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt;: Ya estás inscrita o inscrito a la lista &lt;@liste@&gt;.',
	'desabo' => 'Desuscripción efectuada.',

	// F
	'fermer' => 'cerrar',

	// I
	'inscription' => 'Inscripción a &lt;@liste@&gt;',

	// M
	'mail_removed' => '

Se suprimió la dirección <@var_email@> de la lista @liste@.

En caso de problema, o si no fuiste tú quien pidió esta cancelación de suscripción,
no dudes en contactar <@responsable@>.

Hasta luego, y gracias.
',
	'merci' => 'Gracias.',

	// P
	'pasabo' => 'La dirección &lt;@var_email@&gt; no está suscrita a la lista &lt;@liste@&gt;.',
	'patientez' => 'Un momento, por favor...',

	// Q
	'quitter' => 'Desuscripción',

	// S
	'subject_removed' => 'Se suprimió tu dirección de la lista @liste@.',

	// T
	'titrefenetre' => 'Inscripción',

	// V
	'veuillez' => 'Por favor indica tu dirección.',
	'votreemail' => 'Tu e-milio:'
);

?>
