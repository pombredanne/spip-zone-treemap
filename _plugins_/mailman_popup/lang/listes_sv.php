<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=sv
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'Adressen &lt;@var_email@&gt; kommer att läggas till listan &lt;@liste@&gt; när den har blivit kontrollerad. Var god och svara på meddelandet som just skickats till dig.',

	// C
	'confirm' => 'En begäran om bekräftelse har skickats till &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt;: Du prenumerar redan på &lt;@liste@&gt; listan.',
	'desabo' => 'Prenumeration avbruten.',

	// F
	'fermer' => 'stäng',

	// I
	'inscription' => 'Prenumerera på &lt;@liste@&gt;',

	// M
	'mail_removed' => '

Adressen &lt;@var_email@&gt; har tagits bort som prenumerant från @liste@ listan.
Om det orsakar problem, eller om du inte begärt detta: skriv till &lt;@responsable@&gt;.
Hejdå och tack.
',
	'merci' => 'Tack',

	// P
	'pasabo' => 'Adressen &lt;@var_email@&gt; är inte anmäld som prenumerant på &lt;@liste@&gt; listan.',
	'patientez' => 'Vänta...',

	// Q
	'quitter' => 'Avsluta',

	// S
	'subject_removed' => 'Din adress har tagits bort från @liste@ listan.',

	// T
	'titrefenetre' => 'Prenumerera',

	// V
	'veuillez' => 'Ange din adress.',
	'votreemail' => 'Din e-post:'
);

?>
