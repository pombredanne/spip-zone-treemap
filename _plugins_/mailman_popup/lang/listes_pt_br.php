<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'O endereço &lt;@var_email@&gt; será incluído na lista &lt;@liste@&gt; após verificação. Por favor, responda à mensagem que acabou de lhe ser enviada.',

	// C
	'confirm' => 'Um pedido de confirmação foi enviado para o endereço &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt;: você já está inscrito(a) na lista &lt;@liste@&gt;.',
	'desabo' => 'Cancelamento efetuado.',

	// F
	'fermer' => 'fechar',

	// I
	'inscription' => 'Inscrição na &lt;@liste@&gt;',

	// M
	'mail_removed' => '

O endereço <@var_email@> foi removido da lista @liste@.

Em caso de problemas, ou se você não pediu este cancelamento,
por favor, escreva para <@responsable@>.

Adeus e obrigado.
',
	'merci' => 'Obrigado.',

	// P
	'pasabo' => 'O endereço &lt;@var_email@&gt; não está cadastrado na lista &lt;@liste@&gt;.',
	'patientez' => 'Por favor, aguarde...',

	// Q
	'quitter' => 'Desinscrição',

	// S
	'subject_removed' => 'O seu endereço foi suprimido da lista  @liste@.',

	// T
	'titrefenetre' => 'Inscrição',

	// V
	'veuillez' => 'Por favor, confirme o seu endereço.',
	'votreemail' => 'Seu e-mail:'
);

?>
