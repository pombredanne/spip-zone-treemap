<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=oc_ni_la
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'L\'adreiça &lt;@var_email@&gt; s\'apondrà a la lista &lt;@liste@&gt; après verificacion. Mercé de respòndre au messatge que venèm de vos mandar.',

	// C
	'confirm' => 'Una demanda de confirmacion s\'es adreiçada a &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt; : ja siatz inscrich(a) a la lista &lt;@liste@&gt;.',
	'desabo' => 'Desabonament fach.',

	// F
	'fermer' => 'barrar',

	// I
	'inscription' => 'Inscripcion a <@liste@>',

	// M
	'mail_removed' => '

L\'adreiça <@var_email@> es estada suprimida de la lista @liste@.

En cas de problèma, ò se non demandat minga d\'abonament,
vorgatz escriure a <@responsable@>.

Adieu-siatz e mercé.
',
	'merci' => 'Mercé.',

	// P
	'pasabo' => 'L\'adreiça &lt;@var_email@&gt; non es abonada a la lista &lt;@liste@&gt;.',
	'patientez' => 'Mercé de pacientar...',

	// Q
	'quitter' => 'Desabonament',

	// S
	'subject_removed' => 'S\'es suprimit la vòstra adreiça de la lista @liste@.',

	// T
	'titrefenetre' => 'Inscripcion',

	// V
	'veuillez' => 'Vorgatz precisar la vòstra adreiça.',
	'votreemail' => 'Lo vòstre e-mail:'
);

?>
