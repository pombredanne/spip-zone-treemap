<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=cpf_hat
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'Ladres &lt;@var_email@&gt; ka ajoute nan lis sa-a &lt;@liste@&gt; aprè li ki verifye. Mèsi w reponn mesaj ki ap voye a w.',

	// C
	'confirm' => 'Yon domann w konfime enskripsyon-an ki ap voye w sou ladrès &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt; : w se deja enskri sou lis-la &lt;@liste@&gt;.',
	'desabo' => 'Enskripsyon w te fin rete.',

	// F
	'fermer' => 'fèmen ',

	// I
	'inscription' => 'Enskripsyon sou <@liste@>',

	// M
	'mail_removed' => '

Ladres <@var_email@> te sipwime nan lis @liste@.

Si w genyen pwoblèm, osnon rete enskripsyon-an ki pap domann w,
tanpri kontak <@responsable@>.

Mèsi a w.
',
	'merci' => 'Mèsi.',

	// P
	'pasabo' => 'Ladres <@var_email@> ki pap enskri sou lis la <@liste@>.',
	'patientez' => 'Tanpri atann...',

	// Q
	'quitter' => 'Rete lenskripsyon',

	// S
	'subject_removed' => 'Ladrès w ki te sipwime sou lis-la @liste@.',

	// T
	'titrefenetre' => 'Enskripsyon',

	// V
	'veuillez' => 'Tanpri pweziz ladrès w.',
	'votreemail' => 'Limel a w :'
);

?>
