<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=gl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'O enderezo &lt;@var_email@&gt; será engadido á lista &lt;@liste@&gt; despois da verificación. Agradeceremos que responda á mensaxe que acaba de lle ser enviada.',

	// C
	'confirm' => 'Unha solicitude de confirmación foi dirixida a &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt; : xa está subscrito/a(e) á lista &lt;@liste@&gt;.',
	'desabo' => 'Desubscrición efectuada.',

	// F
	'fermer' => 'pechar',

	// I
	'inscription' => 'Subscrición a &lt;@liste@&gt;',

	// M
	'mail_removed' => '

O enderezo <@var_email@> foi suprimido da lista @liste@.

En caso de problema, ou se vostede non ten solicitado a desubscrición,
escríballe a  <@responsable@>.

Deica logo, e gracias.
',
	'merci' => 'Gracias.',

	// P
	'pasabo' => 'O enderezo &lt;@var_email@&gt; non está subscrito á lista &lt;@liste@&gt;.',
	'patientez' => 'Teña un pouco de paciencia...',

	// Q
	'quitter' => 'Desubscrición',

	// S
	'subject_removed' => 'O seu enderezo foi suprimido da lista',

	// T
	'titrefenetre' => 'Inscrición',

	// V
	'veuillez' => 'Precise o seu enderezo',
	'votreemail' => 'O seu correo :'
);

?>
