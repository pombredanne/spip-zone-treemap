<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=hu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'A cím &lt;@var_email@&gt; kerül a &lt;@liste@&gt; listába ellenőrzés után. Előre köszöjuk, hogy válaszol a most küldött üzenetre.',

	// C
	'confirm' => 'Egy megerősítési kérelem lett küldve ennek: &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt; : már szerepel a listában &lt;@liste@&gt;.',
	'desabo' => 'Kiíratkozás megtörtént.',

	// F
	'fermer' => 'bezárás',

	// I
	'inscription' => 'Beíratkozás a listában &lt;@liste@&gt;',

	// M
	'mail_removed' => '

A cim <@var_email@> lett törölve a listából @liste@.

En cas de problème, ou si vous n\'avez pas demandé ce désabonnement,
veuillez écrire à <@responsable@>.

Au revoir, et merci.
',
	'merci' => 'Köszönjük.',

	// P
	'pasabo' => 'A cím &lt;@var_email@&gt; nem szerepel a listában &lt;@liste@&gt;.',
	'patientez' => 'Egy kis türelmet kérünk...',

	// Q
	'quitter' => 'Kiíratkozás',

	// S
	'subject_removed' => 'Az Ön címe kikerült a listából @liste@.',

	// T
	'titrefenetre' => 'Beíratkozás',

	// V
	'veuillez' => 'Legyen szíves pontosítani az Ön címét.',
	'votreemail' => 'Az Ön e-mailje :'
);

?>
