<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=ro
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'Adresa &lt;@var_email@&gt; va fi adăugată la lista &lt;@liste@&gt; după verificare. Vă rugăm să răspundeţi la mesajul care tocmai v-a fost trimis.',

	// C
	'confirm' => 'O cerere de confirmare a fost adresată la &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt; : sunteţi deja înscris(ă) pe lista &lt;@liste@&gt;.',
	'desabo' => 'Dezabonare efectuată.',

	// F
	'fermer' => 'închideţi',

	// I
	'inscription' => 'Înscriere la &lt;@liste@&gt;',

	// M
	'mail_removed' => '

Adresa <@var_email@> a fost ştearsă de pe lista @liste@.

În caz de probleme, sau dacă nu aţi cerut această dezabonare,
vă rugăm să vă adresaţi la <@responsable@>.

La revedere şi vă mulţumim.
',
	'merci' => 'Mulţumesc.',

	// P
	'pasabo' => 'Adresa &lt;@var_email@&gt; nu este abonată la lista &lt;@liste@&gt;.',
	'patientez' => 'Vă rugăm să aşteptaţi un moment...',

	// Q
	'quitter' => 'Dezabonare',

	// S
	'subject_removed' => 'Adresa dumneavoastră a fost ştearsă de pe lista @liste@.',

	// T
	'titrefenetre' => 'Înscriere',

	// V
	'veuillez' => 'Vă rugăm să precizaţi adresa dumneavoastră.',
	'votreemail' => 'Email-ul dumneavoastră :'
);

?>
