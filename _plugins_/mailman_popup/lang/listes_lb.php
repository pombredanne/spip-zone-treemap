<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=lb
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'D\'Adress &lt;@var_email@&gt; gëtt op d\'Lëscht &lt;@liste@&gt; no enger Kontroll abonnéiert. Äntwert weg op de Message deen Iech geschéckt ginn ass.',

	// C
	'confirm' => 'Eng Konfirmatiouns-Demande ass un d\'Adress &lt;@var_email@&gt; geschéckt ginn.',

	// D
	'deja' => '&lt;@var_email@&gt; : dir sidd schon un der Lëscht &lt;@liste@&gt; abonnéiert.',
	'desabo' => 'Annuléiert.',

	// F
	'fermer' => 'zoumaachen',

	// I
	'inscription' => 'Aschreiwung op d\'Lëscht &lt;@liste@&gt;',

	// M
	'mail_removed' => '

D\'Adress <@var_email@> ass vun der Lëscht @liste@ geläscht ginn.

Wann dat e Problem ass oder wann dier dës Annulatioun nët gefrot hut,
schreiwt weg un <@responsable@>.

Au revoir a merci.
',
	'merci' => 'Merci.',

	// P
	'pasabo' => 'D\'Adress &lt;@var_email@&gt; ass nët un der Lëscht &lt;@liste@&gt; abonnéiert.',
	'patientez' => 'Waart weg...',

	// Q
	'quitter' => 'Ofmelden',

	// S
	'subject_removed' => 'Är Adress ass vun der Lëscht @liste@ geläscht ginn.',

	// T
	'titrefenetre' => 'Aschreiwen',

	// V
	'veuillez' => 'Gidd weg är Adress un.',
	'votreemail' => 'Är Email-Adress:'
);

?>
