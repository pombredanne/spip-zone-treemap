<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=cs
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'Adresa &lt;@var_email@&gt; bude zkontrolována a poté přidána do skupiny &lt;@liste@&gt;. Byla vám zaslána zpráva. Prosím, odpovězte na ni.',

	// C
	'confirm' => 'Na adresu &lt;@var_email@&gt; byla zaslána potvrzovací zpráva.',

	// D
	'deja' => '&lt;@var_email@&gt;: Do skupiny &lt;@liste@&gt; jste již přihlášeni.',
	'desabo' => 'Přihlášení bylo zrušeno.',

	// F
	'fermer' => 'zavřít',

	// I
	'inscription' => 'Přihlášení do &lt;@liste@&gt;',

	// M
	'mail_removed' => '

Adresa &lt;@var_email@&gt; byla odebrána ze skupiny @liste@.

V případě potíží, resp. pokud jste o zrušení nežádali, obraťte se prosím na &lt;@responsable@&gt;.

Děkujeme a zdravíme.
',
	'merci' => 'Děkujeme.',

	// P
	'pasabo' => 'Adresa &lt;@var_email@&gt; není do skupiny &lt;@liste@&gt; přihlášena.',
	'patientez' => 'Vyčkejte prosím ...',

	// Q
	'quitter' => 'Zrušení',

	// S
	'subject_removed' => 'Adresa byla odebrána ze skupiny @liste@.',

	// T
	'titrefenetre' => 'Přihlášení',

	// V
	'veuillez' => 'Zadejte prosím adresu.',
	'votreemail' => 'Váš e-mail:'
);

?>
