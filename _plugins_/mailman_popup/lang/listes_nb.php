<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=nb
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'Adressen &lt;@var_email@&gt; vil bli lagt til epostlisten &lt;@liste@&gt; så fort den er kontrollert. Vennligst svar på den eposten som er sendt deg.',

	// C
	'confirm' => 'En bekreftselsesforespørsel er sendt til &lt;@var_email@&gt;.',

	// D
	'deja' => ' &lt;@var_email@&gt;: Du har allerede meldt deg på epostlisten &lt;@liste@&gt;.',
	'desabo' => 'Påmelding avbrutt.',

	// F
	'fermer' => 'steng',

	// I
	'inscription' => 'Påmelding til &lt;@liste@&gt;',

	// M
	'mail_removed' => '

Adressen &lt;@var_email@&gt; er fjernet fra epostlisten @liste@.

Hvis du er et problem eller hvis du ikke har bedt om å meldes av
vennligst skriv til &lt;@responsable@&gt;.

Takk, ha en fin dag.
',
	'merci' => 'Takk.',

	// P
	'pasabo' => 'Adressen &lt;@var_email@&gt; er ikke påmeldt epostlisten &lt;@liste@&gt;.',
	'patientez' => 'Veuillez patienter...', # NEW

	// Q
	'quitter' => 'Avmelding',

	// S
	'subject_removed' => 'Adressen din er meldt av epostlisten @liste@.',

	// T
	'titrefenetre' => ' Inscription', # NEW

	// V
	'veuillez' => 'Oppgi din adresse.',
	'votreemail' => 'Din epost:'
);

?>
