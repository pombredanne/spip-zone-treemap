<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'The address <@var_email@> will be added to the list <@liste@> once it has been checked. Please reply to the message which has just been sent to you.',

	// C
	'confirm' => 'A confirmation request has been sent to <@var_email@>.',

	// D
	'deja' => '<@var_email@>: You are already subcribed to the <@liste@> list.',
	'desabo' => 'Subscription cancelled.',

	// F
	'fermer' => 'close',

	// I
	'inscription' => 'Subscription to <@liste@>',

	// M
	'mail_removed' => '

The address <@var_email@> has been removed from the @liste@ list.

If there is a problem, or if you did not request this cancellation
please write to <@responsable@>.

Goodbye and thanks.
',
	'merci' => 'Thank you.',

	// P
	'pasabo' => 'The address <@var_email@> is not subscribed the <@liste@> list.',
	'patientez' => 'Please wait...',

	// Q
	'quitter' => 'Cancellation',

	// S
	'subject_removed' => 'Your address has been removed from the @liste@ list.',

	// T
	'titrefenetre' => 'Subscribe',

	// V
	'veuillez' => 'Please give your address.',
	'votreemail' => 'Your email:'
);

?>
