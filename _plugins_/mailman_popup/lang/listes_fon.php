<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=fon
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'nyikɔ &lt;@var_email@&gt ; na gɔna ɖiɖèmɛ ɔ &lt;@liste@&gt, nu mi vɔ lɛ kpɔn fo ɔ. Mi nu mi na yi gbè nu wɛn é sɛ ɖo mi é ',

	// C
	'confirm' => 'Yé ɖalɔwemamɛ bo  sɛ  ɖo &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt;: Mi ko na nyikɔ mi tɔn &lt;@liste@&gt;.',
	'desabo' => 'ɖiɖetɔn sin agun ɔ mɛ é ko sɔ gbè',

	// F
	'fermer' => 'Su',

	// I
	'inscription' => 'nyikɔ ni na ɖo &lt;@liste@&gt;',

	// M
	'mail_removed' => 'nyikɔ <@var_email@> sunsun sin agun ɔ mɛ @liste@.Nu nɖé jɔ ɔ , kabi nu mi ma byɔ ɖiɖetɔn si mɛ a ,Mi kɛnklin bo wlan <@gân@>.O ɖabɔ, mi kuɖéwu.',
	'merci' => 'mi kudewu.',

	// P
	'pasabo' => 'nyikɔ &lt;@var_emailu@&gt; é lo agun ɔ mɛ a  &lt;@liste@&gt;.',
	'patientez' => 'Mi kɛnklin bo kuhun ',

	// Q
	'quitter' => 'ɖiɖetɔnsimɛ ',

	// S
	'subject_removed' => 'Yé sunsun nyikɔ mitɔn sin agun ɔ mɛ @liste@.',

	// T
	'titrefenetre' => 'nyikɔ ni na ',

	// V
	'veuillez' => 'Mi kɛnklɛn mi lɛ vɔ tɛɖɛ nyikɔ mi tɔn ji ',
	'votreemail' => 'e-mailu mi tɔn '
);

?>
