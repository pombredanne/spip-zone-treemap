<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=eu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'Egiaztatu ondonan <@var_email@> helbidea <@liste@> zerrendan gehitua izanen da. Igorria izan zaizun mezuari erantzutea eskertzen zaitugu.',

	// C
	'confirm' => 'Egiaztatze eskaera bat helbide honetara igorria izan da :<@var_email@>.',

	// D
	'deja' => '&lt;@var_email@&gt; : &lt;@liste@&gt; zerrendan jadanik harpidetua zira.',
	'desabo' => 'Harpidetza ezeztatua da.',

	// F
	'fermer' => 'hetsi',

	// I
	'inscription' => '<@liste@> zerrendara harpidetza',

	// M
	'mail_removed' => '

<@var_email@> helbidea @liste@ zerrendatik kendua izan da.

Arazo bat balitz edo ez badezu zu honek harpidetza hori ezeztatzea eskatu,
<@responsable@>-i idaztea eskertzen dizugu.

Agur eta milesker.
',
	'merci' => 'Milesker.',

	// P
	'pasabo' => '<@var_email@> helbidea ez da <@liste@> zerrendari harpidetua.',
	'patientez' => 'Igurikatu mesedez...',

	// Q
	'quitter' => 'Harpidetza ezeztatu',

	// S
	'subject_removed' => 'Zure helbidea @liste@ zerrendatik kendua izan da.',

	// T
	'titrefenetre' => 'Harpidetza',

	// V
	'veuillez' => 'Zure helbidea zehaztea eskertzen dizugu.',
	'votreemail' => 'Zure emaila :'
);

?>
