<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => '@liste@: آدرس ايميل &lt;@var_email@&gt; به ليست اضافه شد',

	// C
	'confirm' => '@var_email@   : يك درخواست تأئيد به آدرس
زير فرستاده شده',

	// D
	'deja' => '&lt;@var_email@&gt; : @liste@:شما قبلأ ثبت نام كرده ايد',
	'desabo' => 'شما ديگر مشترك نيستيد',

	// F
	'fermer' => 'ببنديد',

	// I
	'inscription' => 'براي اشتراک به  ;@liste@',

	// M
	'mail_removed' => '@liste@ : آدرس شما از ليست ايميل مشتركين زير حذف شد',
	'merci' => 'متشكريم',

	// P
	'pasabo' => 'نميباشد @liste@ در ليست مشتركين 
@var_email@آدرس',
	'patientez' => 'صبر كنيد',

	// Q
	'quitter' => 'قطع اشتراک',

	// S
	'subject_removed' => '@liste@ : آدرس شما از ليست ايميل مشتركين زير حذف شد',

	// T
	'titrefenetre' => 'Inscription',

	// V
	'veuillez' => 'خواهشمند است كه آدرس ايميلتان را
مشخص كنيد',
	'votreemail' => 'آدرس الکترونيکي'
);

?>
