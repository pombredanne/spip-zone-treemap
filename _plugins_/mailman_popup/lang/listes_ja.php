<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=ja
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'それがチェックされれば、アドレス  &lt;@var_email@&gt; はリスト  &lt;@liste@&gt; に追加されるでしょう。今あなたに送ったメッセージに返信してください。',

	// C
	'confirm' => '確認要請を &lt;@var_email@&gt; へ送りました。',

	// D
	'deja' => '&lt;@var_email@&gt;: あなたは既に &lt;@liste@&gt; リストへ購読予約しています。',
	'desabo' => '定期購読をキャンセルしました。',

	// F
	'fermer' => '閉じる',

	// I
	'inscription' => '定期購読 &lt;@liste@&gt;',

	// M
	'mail_removed' => '

アドレス &lt;@var_email@&gt; は、リスト@liste@ から取り除かれました。

問題がある、キャンセルを要求してなかったのなら
&lt;@responsable@&gt; へ書き込んで下さい。

さよなら、そしてありがとう。
',
	'merci' => 'Thank you.',

	// P
	'pasabo' => 'アドレス &lt;@var_email@&gt; は、 &lt;@liste@&gt; リストを予約購読していない。',
	'patientez' => 'しばらくお待ち下さい...',

	// Q
	'quitter' => 'キャンセル',

	// S
	'subject_removed' => 'あなたのアドレスは、@liste@ リストから削除されていました。',

	// T
	'titrefenetre' => '予約購読',

	// V
	'veuillez' => 'あなたのアドレスを与えてください。',
	'votreemail' => 'あなたのemailアドレス:'
);

?>
