<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=pl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'Adres &lt;@var_email@&gt; zostanie dodany do listy &lt;@liste@&gt; po koniecznej weryfikacji. Prosimy o odpowiedź na wiadomość, która właśnie została do Ciebie wysłana.',

	// C
	'confirm' => 'Zapytanie o potwierdzenie zostało zaadresowane do &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt; : jesteś już zapisany(a) na listę &lt;@liste@&gt;.',
	'desabo' => 'Adres usunięty z powodzeniem.',

	// F
	'fermer' => 'zamknij',

	// I
	'inscription' => 'Zapis na listę &lt;@liste@&gt;',

	// M
	'mail_removed' => '

Adres <@var_email@> został usunięty z listy @liste@.

W przypadku jakichś problemów, lub jeśli nie prosiłeś o usunięcie,
prosimy o kontakt z odpowiedzialnym <@responsable@>.

Dziękujemy, miłego dnia.
',
	'merci' => 'Dziękujemy.',

	// P
	'pasabo' => 'Adresu &lt;@var_email@&gt; nie ma na liście &lt;@liste@&gt;.',
	'patientez' => 'Prosimy o chwilę cierpliwości...',

	// Q
	'quitter' => 'Usunięcie',

	// S
	'subject_removed' => 'Twój adres został usunięty z listy @liste@.',

	// T
	'titrefenetre' => 'Zapisz się',

	// V
	'veuillez' => 'Prosimy o podanie adresu.',
	'votreemail' => 'Twój e-mail :'
);

?>
