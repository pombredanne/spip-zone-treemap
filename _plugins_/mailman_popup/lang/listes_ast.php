<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=ast
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'La direción &lt;@var_email@&gt; va axuntase a la llista &lt;@liste@&gt; llueu de verificase. Por favor, responde al corréu que vamos unviate de secute.',

	// C
	'confirm' => 'Unvióse un corréu de confirmación a &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt; : ya ta apuntáu(a) a la llista &lt;@liste@&gt;.',
	'desabo' => 'Fechu el desapuntamientu.',

	// F
	'fermer' => 'pesllar',

	// I
	'inscription' => 'Apuntase a <@liste@>',

	// M
	'mail_removed' => '

La direción <@var_email@> quitose de la llista @liste@.

En casu de problemes, o si non pediste que se quitara,
escribe a <@responsable@>.

Alón, y gracies.
',
	'merci' => 'Gracies.',

	// P
	'pasabo' => 'La direción &lt;@var_email@&gt; nun ta apuntada la llista &lt;@liste@&gt;.',
	'patientez' => 'Un momentín...',

	// Q
	'quitter' => 'Borrase',

	// S
	'subject_removed' => 'la to direción borrose de la llista @liste@.',

	// T
	'titrefenetre' => 'Apuntase',

	// V
	'veuillez' => 'Tienes que poner la to direción.',
	'votreemail' => 'El to email:'
);

?>
