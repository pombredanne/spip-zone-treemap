<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=fi
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'Osoite &lt;@var_email@&gt; lisätään listaan &lt;@liste@&gt; kunhan se on varmennettu. Vastaa viestiin, joka on juuri lähetetty sinulle.',

	// C
	'confirm' => 'Varmennuspyyntö on lähetetty osoitteseen &lt;@var_email@&gt;.',

	// D
	'deja' => '<@var_email@>: olet jo listan <@liste@> tilaajana.',
	'desabo' => 'Tilaus peruttu.',

	// F
	'fermer' => 'sulje',

	// I
	'inscription' => 'Tilaus listaan <@liste@>',

	// M
	'mail_removed' => '

Osoite &lt;@var_email@&gt; on poistettu listalta @liste@.

Jos sinulla on ongelmia, tai et pyytänyt peruutusta niin ota yhteys osoitteseen
&lt;@responsable@&gt;.

Kiitos ja näkemiin.
',
	'merci' => 'Kiitos.',

	// P
	'pasabo' => 'Osoite <@var_email@> ei ole <@liste@> listan tilaaja.',
	'patientez' => 'Odota...',

	// Q
	'quitter' => 'Peruutus',

	// S
	'subject_removed' => 'Osoitteesi on poistettu listalta @liste@.',

	// T
	'titrefenetre' => 'Tilaa',

	// V
	'veuillez' => 'Anna osoitteesi.',
	'votreemail' => 'Sinun sähköposti:'
);

?>
