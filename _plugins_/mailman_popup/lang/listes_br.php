<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'Ouzhpennet e vo ar chomlec\'h &lt;@var_email@&gt; d\'al listenn &lt;@liste@&gt; goude bezañ bet gwiriet. Trugarez da respont d\'ar c\'hemennadenn a zo o paouez bezañ kaset deoc\'h.',

	// C
	'confirm' => 'Kaset ez eus bet ur goulenn-gwiriañ da &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt; : c\'hoazh oc\'h ensrkivet d\'al listenn &lt;@liste@&gt;.',
	'desabo' => 'Tennet eo bet ho chomlec\'h.',

	// F
	'fermer' => 'serriñ',

	// I
	'inscription' => 'Enrollañ da &lt;@liste@&gt;',

	// M
	'mail_removed' => '

Tennet eo bet ar chomlec\'h <@var_email@> eus al listenn @liste@.

Ma sav ur gudenn, pe ma n\'ho peus ket goulennet kement-mañ, 
skrivit da <@responsable@>.

Kenavo, ha trugarez.
',
	'merci' => 'Trugarez.',

	// P
	'pasabo' => 'N\\emañ ket enrollet ar chomlec\'h &lt;@var_email@&gt; war al listenn &lt;@liste@&gt;.',
	'patientez' => 'Gortozit mar-plij...',

	// Q
	'quitter' => 'Kuitaat',

	// S
	'subject_removed' => 'Tennet eo bet ho chomlec\'h ouzh al listenn @liste@.',

	// T
	'titrefenetre' => 'Enrollañ',

	// V
	'veuillez' => 'Resisait ho chomlec\'h.',
	'votreemail' => 'Ho postel :'
);

?>
