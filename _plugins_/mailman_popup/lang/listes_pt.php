<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/listes?lang_cible=pt
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajoutee' => 'O endereço &lt;@var_email@&gt; será acrescentado à lista &lt;@liste@&gt; após confirmação. Por favor responda à mensagem que acaba de lhe ser enviada.',

	// C
	'confirm' => 'Um pedido de confirmação foi dirigido a &lt;@var_email@&gt;.',

	// D
	'deja' => '&lt;@var_email@&gt; : já está inscrito(a) na lista &lt;@liste@&gt;.',
	'desabo' => 'Fim da assinatura efectuado.',

	// F
	'fermer' => 'fechar',

	// I
	'inscription' => 'Inscrição a &lt;@liste@&gt;',

	// M
	'mail_removed' => '

O endereço <@var_email@> foi retirado da lista @liste@.

Se houver problema, ou se não pediu esse fim de assinatura,
favor escrever a <@responsable@>.

Adeus, e obrigado.
',
	'merci' => 'Obrigado',

	// P
	'pasabo' => 'O endereço &lt;@var_email@&gt; não assinou a lista  &lt;@liste@&gt;.',
	'patientez' => 'Aguarde, por favor...',

	// Q
	'quitter' => 'Fim da assinatura',

	// S
	'subject_removed' => 'O seu endereço foi retirado da lista @liste@.',

	// T
	'titrefenetre' => 'Inscrição',

	// V
	'veuillez' => 'Favor indicar o seu endereço.',
	'votreemail' => 'O seu email :'
);

?>
