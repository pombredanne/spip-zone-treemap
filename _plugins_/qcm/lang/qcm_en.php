<?php

// This is a SPIP language file

$GLOBALS[$GLOBALS['idx_lang']] = array(

'qcm_titre' => "Here is an MCQ",

'qcm_reponseJuste' => " You were correct!",
'qcm_reponseFausse' => " You were incorrect!",
'qcm_reponseNulle' => "You didn't answer this question!",
'qcm_reinitialiser' => "Restart",
'qcm_introReponse' => "Your answer: ",
'qcm_bravo' => "Congratulation !",

'qcm_corriger' => "Check",
'qcm_score' => "Score:",

'qcm_point' => "&nbsp;pt",
'qcm_points' => "&nbsp;pts"

);


?>
