<?php

// This is a SPIP language file  --  Questo � un file lingua di SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'qcm_titre' => "Un questionario a scelta multipla",

'qcm_reponseJuste' => "La risposta � esatta",
'qcm_reponseFausse' => "La risposta � sbagliata",
'qcm_reponseNulle' => "Non hai ancora risposto",
'qcm_reinitialiser' => "Ricarica il questionario",
'qcm_introReponse' => "La tua risposta: ",
'qcm_bravo' => "Complimenti!",

'qcm_corriger' => "Controlla",
'qcm_score' => "Punteggio:",

'qcm_point' => "punto",
'qcm_points' => "punti"

);


?>

