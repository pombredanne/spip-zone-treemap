<?php 
if (!defined("_ECRIRE_INC_VERSION")) return;
$GLOBALS[$GLOBALS['idx_lang']] = array(
	"ajouter_lien" => "Ajouter un lien",

	"chp_auteur" => "Champ auteur",
	"chp_ldap" => "Champ LDAP",

	"et_groupe" => "et le(s) groupe(s)",

	"lien_ldap" => "Lier memberOf=",
	"liste_act1" => "Gerer les informations",
	"liste_act2" => "Gerer memberOf",
	

	"supprimer" => "Supprimer",

	"tbl_auteur" => "Table auteurs",
	"tbl_auteur_elargi" => "Table auteurs_elargis",
	"titre_page" => "LDAPLUS"
);
?>