<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/boutonstexte?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'augmenter_police' => 'Podmienky zväčšenia',

	// B
	'boutonstexte_nom' => 'Tlačidlá v texte',

	// D
	'diminuer_police' => 'Podmienky zmenšenia',
	'donnees_techniques' => 'Technické dáta sa nemenia',

	// L
	'label_selector' => 'Výraz CSS alebo rozšírený výraz jQuery',
	'label_skin' => 'Vzhľad tlačidiel',
	'label_txtBackSpip' => 'Text pre návrat',
	'label_txtOnly' => 'Iba text *',
	'label_txtSizeDown' => 'Zmenšiť text *',
	'label_txtSizeUp' => 'Zväčšiť text *',
	'legend_apparence' => 'Vzhľad',

	// R
	'retour_a_spip' => 'Vrátiť sa na úplnú stránku',

	// S
	'selecteur' => 'Vyberte text, ktorý sa má upraviť',

	// T
	'texte_seulement' => 'Iba text',
	'textes_icones' => 'Text bublinkovej nápovede'
);

?>
