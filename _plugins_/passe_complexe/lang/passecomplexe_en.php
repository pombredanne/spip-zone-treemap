<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(

'tres_faible' => 'Very Weak',
'faible' => 'Weak',
'moyen' => 'Medium',
'fort' => 'Strong',
'tres_fort' => 'Very Strong',
'nb_mini' => 'Minimum number of characters : ',
'simple' => 'Password Too Simple!',
'court' => 'Too Short',
'common' => 'pass,word,password,sex,god,liverpool,letmein,qwerty,monkey',
'cfg_common' => 'Common words to avoid in passwords. <small>(Separated by commas)</small>:',
'cfg_titre' => 'Password Strength Tester',
'cfg_descriptif' => 'This plugin tests the &quot;strength&quot; of the passwords chosen by the authors. <br/> The test is based on multiple criterion like the length of the password, if it contains special characters, etc. <br/> You can also provide a list of words that shouldn\'t be used in the passwords.',
'cfg_longueur' => 'Password minimum length : '

);

?>
