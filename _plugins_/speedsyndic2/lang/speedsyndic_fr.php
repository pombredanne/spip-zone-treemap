<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
'erreur' => 'Erreur : ',
'frequence' => 'Fr&eacute;quence de rafraichissement : (en secondes)',
'frequence_num' => 'Veuillez entrer un nombre entier > 30 pour la fr&eacute;quence.',
'frequence_trente' =>  'La fr&eacute;quence ne peut pas  &ecirc;tre inf&eacute;rieure &agrave; 30.',
'syndiclist' => 'Sites &agrave; speedsyndiquer : ',
);

?>