<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
									   'mots_partout' => 'Mots Partout',
									   'titre_page' => 'Assignation de mots clefs',
									   'ajouter' => 'ajouter',
									   'enlever' => 'enlever',
									   'voir' => 'voir',
									   'cacher' => 'cacher',
									   'limite' => 'limitation',
									   'aucune' => 'aucune',
									   'action' => 'Action',
									   'stricte' => 'stricte',
									   'select' => 'selection',
									   'pas_de_documents' => 'Il n&#39;y a aucun objet avec ces caract&eacute;ristiques',
									   'choses' => 'Ajouter des mots clefs sur:',
									   'dejamotgroupe' => 'il y a d&eacute;j&agrave; un mot de ce groupe (@groupe@) sur l&#39;objet @chose@.',
									   'ATTENTION' => 'ATTENTION',
									   'action_help' => 'Ex&eacute;cuter les actions selection&eacute;es sur les @chose@ selection&eacute;s',
									   'tagmachine' => 'liste des mots &agrave; ajouter',
									   'par' => 'par',
									   'installer' => 'configurer la base de donn&eacute;es pour avoir des mots sur les',
									   'toutinstalle' => 'La base de donn&eacute;es est d&eacute;j&agrave; configur&eacute;e pour mettre des mots partout.',

									   'info_articles' => 'Articles',
									   'info_breves' => 'Br&egrave;ves',
									   'info_rubriques' => 'Rubriques',
									   'info_syndic' => 'Syndic',
									   'info_syndic_articles' => 'Articles syndiqu&eacute;s',
									   'info_auteurs' => 'Auteurs',
									   'info_documents' => 'Documents',
									   'info_messages' => 'Messages',

									   'item_mots_cles_association_auteurs' => 'aux auteurs',
									   'item_mots_cles_association_documents' => 'aux documents',
									   'item_mots_cles_association_messages' => 'aux messages',
									   'item_mots_cles_association_articles' => 'aux articles',
									   'item_mots_cles_association_rubriques' => 'aux rubriques',
									   'item_mots_cles_association_breves' => 'aux breves',
									   'item_mots_cles_association_syndic' => 'aux sites syndiqu&eacute;s',
									   'item_mots_cles_association_syndic_articles' => 'aux articles de sites syndiqu&eacute;s',
									   'item_mots_cles_association_evenements' => 'aux &eacute;v&eacute;nements',

									   'info_auteurs_lies_mot' => 'Les auteurs li&eacute;s &agrave; ce mot clef',

									  'icone_creation_sous_groupe_mots'=>'Cr&eacute;er un sous groupe de mot-cl&eacute;s',
									  'choix_mots_clefs'=>'Associer les mots clefs :',
									  'liste_mots_clefs'=>'Mots clefs d&eacute;j&agrave; associ&eacute;s',
									  'supprimer_mot_clef'=>'cocher et valider pour supprimer le mot clef',

									  'libelle_breves'=>"Breves",
									  'libelle_documents'=>"Documents",
									  'libelle_messages'=>"Messages",
									  'libelle_evenements'=>"Evenements",
									  'libelle_auteurs'=>"Auteurs",
									  'libelle_syndic_articles'=>"Articles syndiqu&eacute;s",
									  'libelle_groupes_mots'=>"Groupes de mots",
									  'groupe_mot_parent'=>"S&eacute;lectionnez le groupe de mot parent",
									  'groupe_racine'=>"Aucun - groupe de mots clefs &agrave; la racine"

									   );

