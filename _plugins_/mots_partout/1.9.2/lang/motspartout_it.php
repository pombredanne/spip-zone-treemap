<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
									   'mots_partout' => 'Parole Chiave per tutto',
									   'titre_page' => 'Aggiungere parole chiave',
									   'ajouter' => 'aggiungere',
									   'enlever' => 'cancella',
									   'voir' => 'vedere',
									   'cacher' => 'excludere',
									   'limite' => 'limitazione',
									   'aucune' => 'nessuna',
									   'action' => 'Azione',
									   'stricte' => 'rigorosa',
									   'select' => 'selezione',
									   'pas_de_documents' => 'Non ce niente con questi caratteristiche',
									   'choses' => 'Aggiungere parole chiave sopra:',
									   'dejamotgroupe' => 'Ce gia un parole chiave de questo gruppo (@groupe@) sul oggetto @chose@.',
									   'ATTENTION' => 'ATTENZIONE',
									   'action_help' => 'Compiere i actioni selectionati suli @chose@ selectionati',
									   'tagmachine' => 'Parole chiave da aggiungere',
									   'par' => 'nb. risultati',		
									   'installer' => 'configurare la basa per i parole chiave su',
									   'toutinstalle' => 'La basa &eacute; gia configurata.',
									'icone_creation_sous_groupe_mots'=>'generare un nuovo sottogruppo'
									   );

?>