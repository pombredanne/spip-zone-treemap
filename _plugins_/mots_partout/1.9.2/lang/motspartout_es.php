<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
									   'mots_partout' => 'Claves omnipresentes',
									   'titre_page' => 'Asignaci&oacute; claves',
									   'ajouter' => 'agregar',
									   'enlever' => 'suprimir',
									   'voir' => 'ver',
									   'cacher' => 'esconder',
									   'limite' => 'limitaci&oacute;n',
									   'aucune' => 'ninguna',
									   'action' => 'Acci&oacute;n',
									   'stricte' => 'estricta',
									   'select' => 'selecci&oacute;n',
									   'pas_de_documents' => 'No hay tal objeto',
									   'choses' => 'Agregar palabra clave a:',
									   'dejamotgroupe' => 'Ya hay una palabla para este grupo (@groupe@) en el objeto @chose@.',
									   'ATTENTION' => 'ATENCI&Oacute;N',
									   'action_help' => 'Ejecutar las acciones selectionnadas en las @chose@ seleccionadas',
									   'tagmachine' => 'Lista de palabras claves',
									   'par' => 'por',
									   'installer' => 'Configurar la base de datos para afectar palabras claves a:',
									   'toutinstalle' => 'La base de datos ya est&aacute; configurada para tener palabras claves en todos lados.',
									   
									   'info_articles' => 'Art&iacute;culos',
									   'info_breves' => 'Noticias',
									   'info_rubriques' => 'Secciones',
									   'info_syndic' => 'Syndicar',
									   'info_auteurs' => 'Autore/as',
									   'info_documents' => 'Documentos',
									   'info_messages' => 'Mesnajes',

									   'item_mots_cles_association_auteurs' => 'autore/as',
									   'item_mots_cles_association_documents' => 'documentos',
									   'item_mots_cles_association_messages' => 'messnajes',
									   'item_mots_cles_association_articles' => 'art&iacute;culos',
									   'item_mots_cles_association_rubriques' => 'secciones',
									   'item_mots_cles_association_breves' => 'news items',
									   'item_mots_cles_association_syndic' => 'sitios web syndicados',
									'icone_creation_sous_groupe_mots'=>'crear un nuevo grupo en este grupo'
									  );

?>