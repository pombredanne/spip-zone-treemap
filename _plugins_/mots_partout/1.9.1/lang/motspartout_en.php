<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
									   'mots_partout' => 'Keywords Everywhere',
									   'titre_page' => 'Keywords assignation',
									   'ajouter' => 'add',
									   'enlever' => 'remove',
									   'voir' => 'see',
									   'cacher' => 'hide',
									   'limite' => 'limitation',
									   'aucune' => 'none',
									   'action' => 'Action',
									   'stricte' => 'strict',
									   'select' => 'select',
									   'pas_de_documents' => 'There is no such object',
									   'choses' => 'Add keywords on:',
									   'dejamotgroupe' => 'There is already a word from this group (@groupe@) on the object @chose@.',
									   'ATTENTION' => 'WARNING',
									   'action_help' => 'Perfom the actions selected on the selected @chose@',
									   'tagmachine' => 'Keywords to add',
									   'par' => 'by',
									   'installer' => 'Configure the database for keywords on:',
									   'toutinstalle' => 'The database is already fully configured to have keywords everywhere.',
									   
									   'info_articles' => 'Articles',
									   'info_breves' => 'News',
									   'info_rubriques' => 'Sections',
									   'info_syndic' => 'Syndic',
									   'info_auteurs' => 'Authors',
									   'info_documents' => 'Documents',
									   'info_messages' => 'Messages',

									   'item_mots_cles_association_auteurs' => 'authors',
									   'item_mots_cles_association_documents' => 'documents',
									   'item_mots_cles_association_messages' => 'messages',
									   'item_mots_cles_association_articles' => 'articles',
									   'item_mots_cles_association_rubriques' => 'sections',
									   'item_mots_cles_association_breves' => 'news items',
									   'item_mots_cles_association_syndic' => 'syndicated web sites',
									  );

?>
