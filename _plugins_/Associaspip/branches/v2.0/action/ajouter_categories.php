<?php
	/**
	* Plugin Association
	*
	* Copyright (c) 2007
	* Bernard Blazin & Fran�ois de Montlivault
	* http://www.plugandspip.com 
	* Ce programme est un logiciel libre distribue sous licence GNU/GPL.
	* Pour plus de details voir le fichier COPYING.txt.
	*  
	**/
if (!defined("_ECRIRE_INC_VERSION")) return;

function action_ajouter_categories() {
		
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$securiser_action();

	$libelle = _request('libelle');
	$valeur = _request('valeur');
	$duree = _request('duree');
	$cotisation = _request('cotisation');
	$commentaires = _request('commentaires');

	categories_insert($cotisation, $valeur, $duree, $libelle, $commentaires);
}

function categories_insert($cotisation, $valeur, $duree, $libelle, $commentaires)
{
	include_spip('base/association');		
	$id_categorie = sql_insertq('spip_asso_categories', array(
		'duree' => $duree,
		'libelle' => $libelle,
		'cotisation' => $cotisation,
		'valeur' => $valeur,
		'commentaires' => $commentaires));
}
?>
