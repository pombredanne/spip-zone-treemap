<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

# Titres globaux
	'titre_gestion_pour_association' => 'Gestion pour Association',
	'titre_relance' => 'Renouvellement de votre cotisation',
	'titre_menu_gestion_association' => 'Gestion Association',

# Navigation
 'menu2_titre_gestion_membres' => 'Gestion des membres',
 'menu2_titre_ajouter_membre' => 'Ajouter un membre',
  'menu2_titre_ajouter_abonne' => 'Ajouter un abonn&eacute;',
 'menu2_titre_relances_cotisations' => 'Relances des cotisations',
 'menu2_titre_relances_abo' => 'Relances',
 'menu2_titre_gestion_dons' => 'Gestion des dons',
 'menu2_titre_ventes_asso' => 'Ventes associatives',
 'menu2_titre_ventes_abo' => 'Ventes',
 'abonnements' => 'Abonnements',
 'menu2_titre_gestion_activites' => 'Gestion des activit&eacute;s',
 'menu2_titre_livres_comptes' => 'Livres de comptes',

# Boutons globaux
	'bouton_retour' => 'Retour',
	'bouton_confirmer' => 'Confirmer',
	'bouton_modifier' => 'Modifier',
	'bouton_envoyer' => 'Envoyer',
	'bouton_soumettre' => 'Soumettre',
	'bouton_supprimer' => 'Supprimer',
	
	'categories_de_cotisations' => 'Cat&eacute;gories de cotisations',
	'configuration' => 'Configuration',
	'effacer_les_tables' => 'Effacer les tables',
	'gestion_association' => 'Gestion d\'une Association',
	'gestion_des_banques' => 'Gestion des banques',
	'gestion_de_lassoc' => 'Gestion de l\'association',
	'ID' => 'ID',
	'info_doc' => '<p>Ce plugin vous permet de g&eacute;rer une petite association en ligne.</p> <p>Vous pouvez ainsi  visualiser, ajouter et modifier des membres actifs, lancer des mails de masse pour les relances de cotisations, g&eacute;rer des dons et des ventes associatives, et tenir une livre de comptes.</p>',
	'message_relance' => '
Bonjour,
		
Votre adh&eacute;sion est arriv&eacute;e &agrave; &eacute;ch&eacute;ance.
Si vous souhaitez continuer l\'aventure en notre compagnie, n\'oubliez pas de reconduire celle-ci.
Vous pouvez nous faire parvenir votre r&egrave;glement &agrave; votre convenance (ch&egrave;que, mandat  ou virement ).

Le bureau de l\'association.

Merci de ne pas r&eacute;pondre directement &agrave; ce message automatique 
	',
	'profil_de_lassociation' => 'Profil de l\'association',
	
# Adherents
 # Titres
	'adherent_titre_action_membres_actifs' => 'Action sur les membres actifs',
	'adherent_titre_modifier_membre' => 'Modifier un membre actif',
	'adherent_titre_ajout_adherent' => 'Ajout d\'adh&eacute;rent',
	'adherent_titre_ajouter_membre_actif' => 'Ajouter des membres actifs',
	'adherent_titre_ajouter_membre' => 'Ajouter un membre',
	'adherent_titre_fiche_signaletique' => 'Fiche signal&eacute;tique membre',
	'adherent_titre_fiche_signaletique_id' => 'Fiche signal&eacute;tique #@id@',
	'adherent_titre_historique_cotisations' => 'Historique des cotisations',
	'adherent_titre_historique_activites' => 'Historique des activit&eacute;s',
	'adherent_titre_liste_actifs' => 'Tous les membres actifs',

	# Libelle
	'adherent_libelle_reference_interne_abrev' => 'R&eacute;f. int.',
	'adherent_libelle_reference_interne' => 'R&eacute;f&eacute;rence interne',
	'adherent_libelle_numero' => 'Num&eacute;ro',
	'adherent_libelle_id' => 'ID',
	'adherent_libelle_photo' => 'Photo',
	'adherent_libelle_nom' => 'Nom',
	'adherent_libelle_prenom' => 'Pr&eacute;nom',
	'adherent_libelle_sexe' => 'Civilit&eacute;',
	'adherent_libelle_date_naissance' => 'Date de naissance',
	'adherent_libelle_categorie' => 'Cat&eacute;gorie',
	'adherent_libelle_fonction' => 'Fonction',
	'adherent_libelle_email' => 'Email',
	'adherent_libelle_adresse' => 'Adresse',
	'adherent_libelle_rue' => 'Rue',
	'adherent_libelle_num_rue' => 'N&deg;',
	'adherent_libelle_ville' => 'Ville',
	'adherent_libelle_codepostal' => 'Code Postal',
	'adherent_libelle_portable' => 'Portable',
	'adherent_libelle_telephone' => 'T&eacute;l&eacute;phone',
	'adherent_libelle_profession' => 'Profession',
	'adherent_libelle_societe' => 'Soci&eacute;t&eacute;',
	'adherent_libelle_secteur' => 'Secteur',
	'adherent_libelle_accord' => 'Accord de publication',
	'adherent_libelle_utilisateur1' => 'Utilisateur 1',
	'adherent_libelle_utilisateur2' => 'Utilisateur 2',
	'adherent_libelle_utilisateur3' => 'Utilisateur 3',
	'adherent_libelle_utilisateur4' => 'Utilisateur 4',
	'adherent_libelle_validite' => 'Validit&eacute;',
	'adherent_libelle_date_validite' => 'Date limite de validit&eacute;',
	'adherent_libelle_remarques' => 'Remarques',
	'adherent_libelle_identifiant' => 'Identifiant',
	'adherent_libelle_visiteur_spip' => 'Visiteur SPIP',
	'adherent_libelle_statut' => 'Statut de cotisation',
	'adherent_libelle_categorie_choix' => 'Choisissez une cat&eacute;gorie de cotisation',

	'adherent_libelle_statut_ok' => '&Agrave; jour',
	'adherent_libelle_statut_echu' => '&Agrave &eacute;ch&eacute;ance',
	'adherent_libelle_statut_relance' => 'Relanc&eacute;',
	'adherent_libelle_statut_sorti' => 'D&eacute;sactiv&eacute;',
	'adherent_libelle_statut_prospect' => 'Prospect',
	

	'adherent_libelle_oui' => 'oui',
	'adherent_libelle_non' => 'non',
	'adherent_libelle_homme' => 'H',
	'adherent_libelle_femme' => 'F',
	'adherent_libelle_masculin' => 'Monsieur',
	'adherent_libelle_feminin' => 'Madame',

	# En-tetes
	'adherent_entete_date' => 'Date',
	'adherent_entete_id' => 'ID',
	'adherent_entete_livre' => 'Livre',
	'adherent_entete_paiement' => 'Paiement',
	'adherent_entete_justification' => 'Justification',
	'adherent_entete_journal' => 'Journal',
	'adherent_entete_activite' => 'Activit&eacute;',
	'adherent_entete_lieu' => 'Lieu',
	'adherent_entete_inscrits' => 'Inscrits',
	'adherent_entete_action' => 'Action',
	'adherent_entete_notes' => 'Notes',
	'adherent_entete_tous' => 'Tous',
	'adherent_entete_supprimer_abrev' => 'Sup.',

	'adherent_entete_statut' => 'Statut',
	'adherent_entete_statut_defaut' => 'Actifs',
	'adherent_entete_statut_ok' => '&Agrave; jour',
	'adherent_entete_statut_echu' => '&Agrave relancer',
	'adherent_entete_statut_relance' => 'Relanc&eacute;s',
	'adherent_entete_statut_sorti' => 'D&eacute;sactiv&eacute;s',
	'adherent_entete_statut_erreur_bank' => 'Paiement refus&eacute;',
	'adherent_entete_statut_prospect' => 'Prospects',
	'adherent_entete_statut_tous' => 'Tous',

	# Bouton
	'adherent_bouton_confirmer' => 'Confirmer',
	'adherent_bouton_modifier' => 'Modifier',
	'adherent_bouton_envoyer' => 'Envoyer',
	'adherent_bouton_modifier_membre' => 'Modifier le membre',
	'adherent_bouton_maj_operation' => 'Mettre &agrave; jour l\'op&eacute;ration',
	'adherent_bouton_maj_inscription' => 'Mettre &agrave; jour l\'inscription',

	# Label
	'adherent_label_modifier_visiteur' => 'Modifier le visiteur',
	'adherent_label_envoyer_courrier' => 'Envoyer un courrier',
	'adherent_label_ajouter_cotisation' => 'Ajouter une cotisation',
	'adherent_label_modifier_membre' => 'Modifier membre',
	'adherent_label_voir_membre' => 'Voir le membre',

	# Message
	'adherent_message_ajout_adherent' => '@prenom@ @nom@ a &eacute;t&eacute; ajout&eacute; dans le fichier',
	'adherent_message_ajout_adherent_suite' => 'et enregistr&eacute; comme visiteur',
	'adherent_message_email_invalide' => 'L\'email n\'est pas valide !',
	'adherent_message_maj_adherent' => 'Les donn&eacute;es de @prenom@ @nom@ ont &eacute;t&eacute; mises &agrave; jour !',
	'adherent_message_confirmer_suppression' => 'Vous vous appr&ecirc;tez &agrave; effacer les membres',
	'adherent_message_suppression_faite' => 'Suppression effectu&eacute;e !',
	# Liste
	'adherent_liste_legende' => 'En bleu : Relanc&eacute; | En rose : A &eacute;ch&eacute;ance | En vert : A jour<br> En brun : D&eacute;sactiv&eacute; | En jaune paille : Prospect',
	'adherent_liste_nombre_adherents' => 'Nombre d\'adh&eacute;rents : @total@',
	'adherent_liste_total_cotisations' => 'Total des cotisations : @total@ &euro;',

# Activites
 # Titres
	'activite_titre_action_sur_inscriptions' => 'Action sur les inscriptions',
	'activite_titre_mise_a_jour_inscriptions' => 'Mise &agrave; jour des inscriptions',
	'activite_titre_ajouter_inscriptions' => 'Ajouter des inscriptions',
	'activite_titre_toutes_activites' => 'Toutes les activit&eacute;s',
	'activite_titre_inscriptions_activites' => 'Inscriptions aux activit&eacute;s',

 # Sous-titres
	'activite_mise_a_jour_inscription' => 'Mettre &agrave; jour une inscription',
	'activite_ajouter_inscription' => 'Ajouter une inscription',
	
# Libelle
	'activite_libelle_inscription' => 'Inscription n&deg;',
	'activite_libelle_date' => 'Date',
	'activite_libelle_nomcomplet' => 'Nom complet',
	'activite_libelle_adherent' => 'N&deg; d\'adh&eacute;rent',
	'activite_libelle_invitation' => ' -- Invitation ext&eacute;rieure -- ',
	'activite_libelle_accompagne_de' => 'Je serai accompagn&eacute; de',
	'activite_libelle_membres' => 'Noms des participants membres',
	'activite_libelle_non_membres' => 'Noms des participants non membres',
	'activite_libelle_nombre_inscrit' => 'Nombre total d\'inscrits',
	'activite_libelle_email' => 'Email',
	'activite_libelle_telephone' => 'T&eacute;l&eacute;phone',
	'activite_libelle_adresse_complete' => 'Adresse compl&egrave;te',
	'activite_libelle_montant_inscription' => 'Montant de l\'inscription (en &euro;)',
	'activite_libelle_date_paiement' => 'Date de paiement (AAAA-MM-JJ)',
	'activite_libelle_mode_paiement' => 'Mode de paiement',
	'activite_libelle_statut' => 'Statut',
	'activite_libelle_commentaires' => 'Commentaires',

 # En-tete
	'activite_entete_id' => 'ID',
	'activite_entete_date' => 'Date',
	'activite_entete_heure' => 'Heure',
	'activite_entete_intitule' => 'Intitul&eacute;',
	'activite_entete_lieu' => 'Lieu',
	'activite_entete_action' => 'Action',
	'activite_entete_toutes' => 'Toutes',
	'activite_entete_validees' => 'Valid&eacute;es',
	'activite_entete_nom' => 'Nom',
	'activite_entete_adherent' => 'Adh&eacute;rent',
	'activite_entete_inscrits' => 'Nbre',
	'activite_entete_montant' => 'Montant',
	'activite_entete_commentaire' => 'Commentaire',

 # Bouton
	'activite_bouton_ajouter' => 'Ajouter',
	'activite_bouton_envoyer' => 'Envoyer',
	'activite_bouton_confirmer' => 'Confirmer',
	'activite_bouton_supprimer' => 'Supprimer',
	'activite_bouton_modifier_article' => 'Modifier l\'article',
	'activite_bouton_ajouter_inscription' => 'Ajouter une inscription',
	'activite_bouton_voir_liste_inscriptions' => 'Voir la liste des inscriptions',
 'activite_bouton_maj_inscription' => 'Mettre &agrave; jour l\'inscription',

	# Liste
	'activite_liste_legende' => 'En bleu : Inscription non valid&eacute;e | En vert : Inscription valid&eacute;e',
	'activite_liste_nombre_inscrits' => 'Nombre d\'inscrits : @total@',
	'activite_liste_total_participations' => 'Total des participations : @total@ &euro;',

	# Message
	'activite_justification_compte_inscription' => 'Inscription n&deg; @id_activite@ - @nom@',
	'activite_message_ajout_inscription' => 'L\'inscription de @nom@ a &eacute;t&eacute; enregistr&eacute;e pour un montant de @montant@ &euro;',
	'activite_message_maj_inscription' => 'L\'inscription de @nom@ a &eacute;t&eacute; mise &agrave; jour',
	'activite_message_confirmation_supprimer' => 'Vous vous appr&ecirc;tez &agrave; effacer @nombre@ inscription@pluriel@ !',
	'activite_message_suppression' => 'Suppression effectu&eacute;e !',
	'activite_message_sujet' => 'Inscription activit&eacute;',
	'activite_message_confirmation_inscription'=>'
Bonjour,

Nous venons d\'enregistrer pour vous l\'inscription suivante:

Activit&eacute;: @activite@
Date: @date@
Lieu: @lieu@

De: @nom@
N&deg; d\'adh&eacute;rent: @id_adherent@
Accompagn&eacute; de
	Membres: @membres@
	Non-membres: @non_membres@
Nombre total d\'inscrits: @inscrits@

Cette inscription ne sera d&eacute;finitive qu\'apr&egrave;s v&eacute;rification et dans la mesure o&ugrave;, sauf stipulation contraire, le montant de @montant@ euros nous est parvenu.

Dans cette attente et dans l\'attente de vous retrouver, nous vous adressons nos salutations les meilleures.

L\'&eacute;quipe @nomasso@
	',
	'activite_message_webmaster'=>'
De: @nom@
Activit&eacute;: @activite@
Nombre: @inscrits@
Commentaire: @commentaire@
	',	
	'date_du_jour' => 'Nous sommes le '.date('d/m/Y'),
	'date_du_jour_heure' => 'Nous sommes le '.date('d/m/Y').' et il est '.date('H:i'),
	
	#Votre association
	'votre_asso' => 'Votre association',
	'president' => 'President',
	'votre_equipe' => 'Votre &eacute;quipe',
	'donnees_perso' => 'Donn&eacute;es Personnelles',
	'donnees_internes' => 'Donn&eacute;es Internes'
	
);
?>