<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(

// Pour l'affichage
'titre_marquepages' => 'Bookmarks',
'titre_marquepages_de' => '@auteur@\'s bookmarks',
'mes_marquepages' => 'My bookmarks',
'tous_les_marquepages' => 'All bookmarks',
'historique' => 'Website history',
'aucun_marquepages' => 'There is no bookmarks.',
'nombre_marquepages' => '@nombre@ bookmarks',
'avec_etiquettes' => 'with tags',
'contenant' => 'containing @recherche@',
'poste_le' => 'post on',
'et' => 'and',
'autres' => 'other(s)',
'vous' => 'you',
'supprimer' => 'Delete',
'copier' => 'Copy',
'enlever' => 'Remove',
'confirmer' => 'Are you really want to delete this bookmark ?',
'de' => 'of',
'bookmarklet' => 'Bookmarklet',
'bookmarklet_lien' => 'Add to @site@',
'bookmarklet_explication' => 'Move this bookmarklet in your browser\'s bookmarks and click on it every time you want to add a bookmark on @site@.',
'importer' => 'Import',
'importer_explication' => 'Export your bookmarks from where they are saved now (Firefox, delicious, scuttle, etc), then upload the file here.',

// Pour la configuration
'configuration_titre' => 'Bookmarks configuration',
'configuration_qui' => 'Who can add bookmarks ?',
'configuration_rubrique' => 'Bookmarks section',
'configuration_rubrique_explication' => 'Choose a section where bookmarks will be saved by default.',
'configuration_titremp' => 'Bookmarks interface title',
'configuration_titremp_explication' => 'You can customize the interface title, otherwise the title of the SPIP will be used.',

// Erreurs
'erreur_importation' => 'Error on file upload.',
'erreur_importation_mal_passee' => 'Importation failed. Please retry.',
'erreur_importation_ok' => 'All bookmarks have been imported.',
'erreur_type_inconnu' => 'Impossible to find the type of uploaded file.',

// Pour le formulaire de saisi
'ajouter' => 'Add a bookmark',
'a' => 'to',
'modifier' => 'Modify a bookmark',
'url' => 'URL',
'titre' => 'Title',
'description' => 'Description',
'visibilite' => 'Visibility;',
'visibilite_public' => 'Public bookmark',
'visibilite_prive' => 'Private bookmark',
'pas_le_droit' => 'You are not authorized. You have to log in with an authorized account.',
'se_deconnecter' => 'You can log out by clicking here.',
'enregistre' => 'The bookmark have been successfully saved.',
'modifie' => 'The bookmark have been successfully modified.',
'erreur' => 'An error occured.',
'revenir' => 'Come back from where you came',
'nouveau' => 'Add a new bookmark'

);

?>
