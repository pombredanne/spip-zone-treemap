<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_encart' => 'Ajouter cet encart',
	
	// C
	'champ_hauteur_explication' => 'Hauteur en pixels',
	'champ_hauteur_label' => 'Hauteur',
	'champ_identifiant_erreur_existe_deja' => 'Un autre encart utilise déjà cet identifiant.',
	'champ_identifiant_explication' => 'Un identifiant textuel unique qui permettra d’appeler l’encart plus facilement.',
	'champ_identifiant_label' => 'Identifiant',
	'champ_largeur_explication' => 'Largeur en pixels',
	'champ_largeur_label' => 'Largeur',
	'champ_titre_label' => 'Titre',
	'champ_type_choix_image_label' => 'Image',
	'champ_type_choix_texte_label' => 'Texte',
	'champ_type_label' => 'Type d’affichage',
	
	// I
	'icone_creer_encart' => 'Créer un encart',
	'icone_modifier_encart' => 'Modifier cet encart',
	'info_1_encart' => 'Un encart',
	'info_aucun_encart' => 'Aucun encart',
	'info_encarts_auteur' => 'Les encarts de cet auteur',
	'info_nb_encarts' => '@nb@ encarts',
	'info_nb_campagnes_total' => 'Publicités publiées (total)',

	// R
	'retirer_lien_encart' => 'Retirer cet encart',
	'retirer_tous_liens_encarts' => 'Retirer tous les encarts',

	// T
	'texte_ajouter_encart' => 'Ajouter un encart',
	'texte_changer_statut_encart' => 'Cet encart est :',
	'texte_creer_associer_encart' => 'Créer et associer un encart',
	'titre_encart' => 'Encart',
	'titre_encarts' => 'Encarts',
	'titre_encarts_menu' => 'Encarts publicitaires',
	'titre_encarts_rubrique' => 'Encarts de la rubrique',
	'titre_langue_encart' => 'Langue de cet encart',
	'titre_logo_encart' => 'Logo de cet encart',
);

?>
