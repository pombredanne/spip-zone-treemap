<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_annonceur' => 'Ajouter ce annonceur',
	
	// C
	'champ_id_auteur_label' => 'Utilisateur associé',
	'champ_nom_label' => 'Nom',
	
	// I
	'icone_creer_annonceur' => 'Créer un annonceur',
	'icone_modifier_annonceur' => 'Modifier ce annonceur',
	'info_1_annonceur' => 'Un annonceur',
	'info_annonceurs_auteur' => 'Les annonceurs de cet auteur',
	'info_aucun_annonceur' => 'Aucun annonceur',
	'info_nb_annonceurs' => '@nb@ annonceurs',

	// R
	'retirer_lien_annonceur' => 'Retirer ce annonceur',
	'retirer_tous_liens_annonceurs' => 'Retirer tous les annonceurs',
	
	// S
	'statistiques_annonceur' => 'Statistiques globale de l’annonceur',

	// T
	'texte_ajouter_annonceur' => 'Ajouter un annonceur',
	'texte_changer_statut_annonceur' => 'Ce annonceur est :',
	'texte_creer_associer_annonceur' => 'Créer et associer un annonceur',
	'titre_annonceur' => 'Annonceur',
	'titre_annonceurs' => 'Annonceurs',
	'titre_annonceurs_rubrique' => 'Annonceurs de la rubrique',
	'titre_langue_annonceur' => 'Langue de ce annonceur',
	'titre_logo_annonceur' => 'Logo de ce annonceur',
);

?>
