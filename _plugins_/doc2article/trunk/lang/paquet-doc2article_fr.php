<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-doc2article
// Langue: fr
// Date: 28-06-2012 10:49:24
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// D
	'doc2article_description' => 'Importer l\'ensemble des fichiers du répertoire tmp/upload et créer un article par fichier automatiquement.',
	'doc2article_slogan' => 'Importer l\'ensemble des fichiers du répertoire tmp/upload et créer un article par fichier automatiquement',
);
?>