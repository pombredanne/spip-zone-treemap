<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// langue / language = en
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'clave_engadida' => 'Added key: ',
	'configuration' => 'Configuration',
	'conseguir' => '(obtain)',
	
	// E
	'erreur_api_browser' => 'Sorry, the Google Maps API is not compatible with this browser',
	
	// D
	'default_geoloc' => 'Default maps position:',
	
	// M
	'miseajour' => 'Update'
	

);

?>
