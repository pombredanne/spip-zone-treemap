<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// langue / language = es
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'api_version' => 'Version de la API',
	'clave_engadida' => 'Clave a&ntilde;adida: ',
	'cle' => 'Clave',
	'configuration' => 'Configuraci&oacute;n',
	'conseguir' => '(conseguir)',
	'default_geoloc' => 'Posici&oacute;n por defecto de los mapas :',
	'desactiver_custom_control' => 'Desactivar custom_control.js',
	'explication_api_version' => 'API Version <a href="http://code.google.com/apis/maps/documentation/index.html#API_Updates" class="spip_out">info</a>:',
	'explication_cle' => 'Google Map API Key : <a href="http://www.google.com/apis/maps/signup.html" class="spip_out">Obtenir une cl&eacute;</a>',
	'explication_custom_control' => 'Desactivar los controles personalizados del mapa :',
	'latitude' => 'Latitud',
	'longitude' => 'Longitud',
	'miseajour' => 'Mise &agrave; jour',
	'pages_public' => 'P&aacute;ginas p&uacute;blicas',
	'rechercher' => 'Buscar',
	'zoom' => 'Zoom',
	'configuration_titre' => 'Configuration del plugin GoogleMap Api',
	'compacte' => 'Compactar',
	'explication_compacte' => 'Evitar o compactado do javascript',
	'non' => 'no',
	'oui' => 'si'
	
);

?>