<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-socialtags
// Langue: en
// Date: 16-04-2012 21:31:34
// Items: 1

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'socialtags_slogan' => 'Activate the social link sharing network buttons.',
);
?>