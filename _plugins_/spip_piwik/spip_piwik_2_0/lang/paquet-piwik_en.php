<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-piwik?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'piwik_description' => 'Adds the Piwik statistics script (web traffic analyzer) on the pages of the site.',
	'piwik_slogan' => 'Link [SPIP->http://www.spip.net] and [Piwik->http://www.piwik.org]'
);

?>
