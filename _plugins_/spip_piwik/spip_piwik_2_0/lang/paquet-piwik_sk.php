<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-piwik?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'piwik_description' => 'Na stránky webu pridá skript so štatistikami Piwik (analyzátor návštevnosti webu).',
	'piwik_slogan' => 'Rozhranie Spip a Piwik'
);

?>
