<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'article_num' => "Article n &#176;",

'descriptif' => "<strong>Descriptif : </strong>",
'discipline' => "<strong>Discipline : </strong>",

'eva_ecriture' => "Gestion des polices de caractères int&eacute;gr&eacute;es dans ce plugin",

'gros_titre_page' => "Gestion des polices de caractères",

'presentation' => "Ce plugin permet, lors de la rédaction d'un article, de pouvoir modifier la 
						police de caractère d’une partie de texte.<br/>",
						
'probleme_base' => "Il y a un problème dans votre base de donn&eacute;es.<br/>",

'lienoff1' => "<a href='http://eva-web.edres74.net' target='_blank'>http://eva-web.edres74.net</a><br/>",
'lienoff2' => "<a href='' target='_blank' ></a><br/>",

'mot_descriptif' => "<strong>Descriptif : </strong>",

'siteoff1' => "<strong>Site officiel eva-web :</strong><br/>",
'siteoff2' => "<strong></strong><br/>",

'texte_article' => "<strong>Texte de l'article : </strong>",
'titre_boite_principale' => "Descriptif du plugin :",
'titre_presentation' => "<strong>Pr&eacute;sentation :</strong><br/>",

'' => "",

);
?>