<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	
	
	// K
	'kaye_titre' => 'kaye',
	
	// C
	'cfg_label_choix_joursemaine' => 'Jours de la semaines à afficher :',
	'cfg_mercredi' => 'Mercredi :',
	'cfg_samedi' => 'Samedi :',
	'cfg_mercredi_explication' => 'affiche l\'onglet mercredi',
	'cfg_titre_affichage_public' => 'Affichage public du cahier de texte',
	'cfg_titre_parametrages' => 'Paramétrages',
	'cfg_vue' => 'Mode d\'affichage :',
	
	// O
	'onglets_explication' => "Cliquer sur les onglets pour accéder au contenu.",

	// T
	'titre_page_configurer_kaye' => 'Configuration du cahier de texte',
	'titre_public' => 'Cahier de texte',
);

?>