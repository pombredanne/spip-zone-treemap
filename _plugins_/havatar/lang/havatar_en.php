<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'havatar_info' => 'Your avatar will be fetched from the URL of the website you indicated in your author profile, provided that it uses <a rel="nofollow external" href="http://microformats.org/" hreflang="en" rel="nofollow">microformats</a>.',
'havatar_info_forum' => 'Your avatar will be fetched from your website, provided it uses <a rel="nofollow external" href="http://microformats.org/" hreflang="en" rel="nofollow">microformats</a>.',
'titre_havatar_auteur' => 'AUTHOR\'S HAVATAR',

);

?>
