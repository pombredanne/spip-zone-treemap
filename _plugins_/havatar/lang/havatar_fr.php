<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'havatar_info' => 'Votre avatar sera r&eacute;cup&eacute;r&eacute; d\'apr&egrave;s l\'URL que vous avez indiqu&eacute;e dans votre profil, &agrave; condition que celui-ci utilise les <a rel="nofollow external" href="http://microformats.org/" rel="nofollow">microformats</a>.',
'havatar_info_forum' => 'Votre avatar sera r&eacute;cup&eacute;r&eacute; sur votre site, &agrave; condition que celui-ci utilise les <a rel="nofollow external" href="http://microformats.org/" rel="nofollow">microformats</a>.',
'titre_havatar_auteur' => 'HAVATAR DE L\'AUTEUR',

);

?>
