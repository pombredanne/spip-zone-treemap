<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/_stable_/crayons/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'agrandir' => 'Agrandir l&apos;image',
	'appareil' => 'Pris avec:',

	// C
	'chargement' => 'Chargement...',
	'commentaires' => 'Commentaires :',
	
	// D

	// E

	// M
	'miniatures' => 'Miniatures',

	// N

	// P

	// S

	// V
);

?>
