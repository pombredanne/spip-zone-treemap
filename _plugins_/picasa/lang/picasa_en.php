<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'agrandir' => 'Open image',
	'appareil' => 'Taken with:',

	// C
	'chargement' => 'Loading...',
	'commentaires' => 'Comments :',
	
	// D

	// E

	// M
	'miniatures' => 'Pictures',

	// N

	// P

	// S

	// V
);

?>
