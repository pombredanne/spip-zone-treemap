PLUGIN IMPRIMIR DOCUMENTO PARA SPIP 2

A veces una persona que visita nuestra web quiere una copia del contenido de uno de nuestros documentos, art�culo o breve, sin cabeceras ni elementos de navegaci�n de la web. Es lo que se suele conocer como "versi�n imprimir".Con este plugin puedes instalar en tu sitio Spip un enlace que cumpla esa funci�n.
El plugin "Imprimir documento" para SPIP 2 y siguientes crea un enlace que permite imprimir el contenido del art�culo o breve sin las cabeceras, men�s y dem�s elementos de la web.Al cliquear en el enlace aparece una ventana pop-up que visualiza la versi�n para imprimir y lanza el di�logo de impresi�n.INSTALACI�N

- Descomprime el archivo "imprimir_documento_2.zip".- Pon la carpeta "imprimir_documento_2" dentro de la carpeta plugins de tu instalaci�n de SPIP. Si no existe crea una y ll�mala "plugins".- En el espacio privado cliquea en Configuraci�n del sitio > Gesti�n de los plugins.- Marca la casilla de Imprimir documento para activarlo.- Donde quieras que aparezca (en los esqueletos article.html o breve.html) escribe #IMPRIMIR_DOCUMENTO** (con los dos asteriscos)- HechoPERSONALIZARPara personalizar la tipograf�a del enlace puedes colocar la baliza #IMPRIMIR_DOCUMENTO** entre <span style="font-size:65%">...</span>, por ejemplo, y en style puedes poner tipo de letra, tama�o, color,... o puedes crear una clase en tu hoja de estilos y pon�rsela,...Para modificar el icono del sobre pon el que tu quieras (a poder ser en formato gif) en la carpeta del plugin imprimir_documento, y ll�malo "impresora.gif"Para otras modificaciones abre el archivo baliza_imprimir_documento.php y retoca lo que quieras. Por ejemplo, el 700 y el 470 son el ancho y el alto de la ventana que se abre.Para modificar el esqueleto que se env�a modifica imprimir_articulo.html o imprimir_breve.html

==========================================================================
NOTA: Los esqueletos empleados (imprimir_articulo.html e imprimir_breve.html) son bastante viejos y estar�a bien reformarlos quitando las tablas y respetando las normas de estilo, pero de momento pueden servir ya que funcinan aceptablemente. T� mism@.CopyLeft 2006 joseluis@digital77.com


PLUGIN IMPRIMER UN DOCUMENT POUR SPIP 2

Un internaute qui visite un site peut vouloir se procurer une copie de l'un de nos documents, article ou br�ve,
d�barrass� de son habillage ou de ses �l�ments de navigation. C'est ce qu'on a l'habitude d'appeller "version imprimable".

C'est ce que r�alise ce plugin, � installer sur votre site sous SPIP.

Le plugin "Imprimer un document" pour SPIP 2 et suivants met � disposition un squelette qui permet d'imprimer le contenu de l'article ou de la br�ve sans habillage, men�s et dem � s des �l�ments du web.

En cliquant sur le lien, une pop-up appara�t qui permet de visauliser la version imprimable avec une ic�ne pour lancer l'impression.

INSTALLATION

- D�compresser l'arhive "imprimir_documento_2.zip".
- Mettre le dossier "imprimir_documento_2" � l'int�rieur du dossier plugins de SPIP (le cr�er s'il n'existe pas).
- Dans l'espace priv� cliquer dans Configuration > Gestion des plugins, et activer celui-ci.
 