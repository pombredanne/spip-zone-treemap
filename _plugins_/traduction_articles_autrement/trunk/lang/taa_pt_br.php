<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/taa?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'affichage_liste_compacte_desactive' => 'Désactivet l\'affichage compacte des liste des articles', # NEW

	// C
	'cfg_limiter_secteur' => 'Ne pas afficher la barre de langue dans le secteur :', # NEW
	'cfg_utiliser_langues' => 'Utiliser les langues suivantes :', # NEW
	'changer_langue' => 'Alterar o idioma da matéria',

	// F
	'fermer' => 'Fechar',

	// L
	'lier_traduction' => 'Anexar uma tradução',

	// M
	'modifier_options_langue' => 'opções',

	// N
	'necesite_afficher_objet' => 'Para poder usar esta função você precisa instalar o plugin',

	// O
	'options' => 'Alterar as opções de idioma',
	'oui' => 'Oui', # NEW

	// R
	'rang' => 'Classificar',

	// T
	'titre_plugin' => 'Tradução das matérias diferentemente',
	'traductions' => 'Traduções', # MODIF

	// V
	'voir_traductions' => 'Ver os detalhes das traduções'
);

?>
