<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/taa?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'affichage_liste_compacte_desactive' => 'Désactivet l\'affichage compacte des liste des articles', # NEW

	// C
	'cfg_limiter_secteur' => 'Ne pas afficher la barre de langue dans le secteur :', # NEW
	'cfg_utiliser_langues' => 'Utiliser les langues suivantes :', # NEW
	'changer_langue' => 'Canviar la llengua de l\'article',

	// F
	'fermer' => 'Tancar',

	// L
	'lier_traduction' => 'Vincular una traducció',

	// M
	'modifier_options_langue' => 'opcions',

	// N
	'necesite_afficher_objet' => 'Per poder utiiltzar aquesta funció heu d\'instal·lar el connector',

	// O
	'options' => 'Opcions', # MODIF
	'oui' => 'Oui', # NEW

	// R
	'rang' => 'Rang', # NEW

	// T
	'titre_plugin' => 'Traducció d\'articles d\'una altra manera',
	'traductions' => 'Traduccions', # MODIF

	// V
	'voir_traductions' => 'Veure el detall de les traduccions'
);

?>
