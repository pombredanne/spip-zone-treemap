<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/taa?lang_cible=ast
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'affichage_liste_compacte_desactive' => 'Désactivet l\'affichage compacte des liste des articles', # NEW

	// C
	'cfg_limiter_secteur' => 'Ne pas afficher la barre de langue dans le secteur :', # NEW
	'cfg_utiliser_langues' => 'Utiliser les langues suivantes :', # NEW
	'changer_langue' => 'Camudar la llingua del artículu',

	// F
	'fermer' => 'Zarrar',

	// L
	'lier_traduction' => 'Axuntar una traducción',

	// M
	'modifier_options_langue' => 'opciones',

	// N
	'necesite_afficher_objet' => 'Pa poder utilizar esta función tienes d\'instalar el complementu',

	// O
	'options' => 'Camudar les opciones de llingua',
	'oui' => 'Oui', # NEW

	// R
	'rang' => 'Rang', # NEW

	// T
	'titre_plugin' => 'Traducción d\'artículos d\'otra miente',
	'traductions' => 'Traducciones', # MODIF

	// V
	'voir_traductions' => 'Ver el detalle de les traducciones'
);

?>
