<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/taa?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'affichage_liste_compacte_desactive' => 'غيرفعال‌سازي نمايش فشرده‌ي فهرست مقاله‌ها را ', # MODIF

	// C
	'cfg_limiter_secteur' => 'Ne pas afficher la barre de langue dans le secteur :', # NEW
	'cfg_utiliser_langues' => 'Utiliser les langues suivantes :', # NEW
	'changer_langue' => 'تغيير زبان مقاله',

	// F
	'fermer' => 'بستن',

	// L
	'lier_traduction' => 'پيوست يك ترجمه',

	// M
	'modifier_options_langue' => 'گزينه‌ها',

	// N
	'necesite_afficher_objet' => 'اين كاركرد مستلزم نصب پلاگين است',

	// O
	'options' => 'اصلاح گزينه‌هاي زبان ',
	'oui' => 'Oui', # NEW

	// R
	'rang' => 'رتبه',

	// T
	'titre_plugin' => 'ترجمه‌ي متفاوت متقالات',
	'traductions' => 'ترجمه‌ها',

	// V
	'voir_traductions' => 'بنگريد به جزئيات ترجمه‌ها'
);

?>
