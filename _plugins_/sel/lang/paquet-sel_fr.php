<?php
     if (!defined('_ECRIRE_INC_VERSION')) return;
     $GLOBALS[$GLOBALS['idx_lang']] = array(
		'sel_nom' => "SEL (Syst&egrave;me d'&eacute;change local)",
		'sel_slogan' => "Gerez votre SEL en toute simplicité",
		'sel_description' => "		
			Ce plugin est destin&eacute; &agrave; la gestion int&eacute;grale d'un ou plusieurs SEL (Syst&egrave;me d'&eacute;change local).
			Que ce soit un SEl qui travaille sur des &eacute;changes simples, des &eacute;changes d'objets, des &eacute;changes de savoir, un SEL de stages, un SEL d\'h&eacute;bergement ou tout autre syst&egrave;me.
			Que ce soit un SEL qui fonctionne avec un carnet d'&eacute;change, un syst&egrave;me de JEU.
			Il est destin&eacute; &agrave; tout type de SEL, de s&eacute;liste et de joueur.
			La publication en ligne des annonces, avec cr&eacute;ation, changement ou suppression est pr&eacute;vue.
			La publication autonome du catalogue, &agrave; partir des annonces en ligne, est pr&eacute;vue.
			La gestion en ligne des &eacute;changes avec gestion autonome par les deux &eacute;changeurs, est pr&eacute;vue.
			La gestion en ligne des adh&eacute;sions, leur renouvellement, leur caducit&eacute;, est pr&eacute;vue.	
		",
     );
?>
