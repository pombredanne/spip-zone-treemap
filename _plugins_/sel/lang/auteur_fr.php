<?php
	 if (!defined('_ECRIRE_INC_VERSION')) return;
     $GLOBALS[$GLOBALS['idx_lang']] = array(

		//C
		'correspondant_auteur_organisations' => "Correspondant-e des ateliers / organisations suivant-e-s :",
		
		// E
		'entree_adresse1' => "Votre adresse (numéro de voie, voie...)",
		'entree_adresse1_2' => "Adresse (numéro de voie, voie...)",
		'entree_adresse2' => "Votre adresse complémentaire (bâtiment, résidence, lieu-dit...)",
		'entree_adresse2_2' => "Adresse complémentaire (bâtiment, résidence, lieu-dit...)",
		'entree_code_postal' => "Votre code postal",
		'entree_code_postal_2' => "Code postal",
		'entree_ville' => "Votre ville",
		'entree_ville_2' => "Ville",
		'entree_pays' => "Votre pays",
		'entree_pays_2' => "Pays",
		'entree_tel1' => "Votre téléphone",
		'entree_tel1_2' => "Téléphone",
		'entree_tel2' => "Votre 2e téléphone",
		'entree_tel2_2' => "2e Téléphone",
		'entree_commentaires' => "Commentaires",
		
		//F
		'form_erreur_information_obligatoire' => 'Cette information est obligatoire',
		
		//I
		'info_coordonnees' => "Coordonnées",
		'info_admin_statuer_webmestre' => "Donner à cet administrateur les droits de webmestre, co-admnistrateur global du catalogue",

		//M
		'membre_auteur_sel' => "Membre du / des SEL(s) suivant(s) :",
     );
?>