<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'afficher_tickets' => 'Show tickets',
	'assignes_a' => 'Assigned to',
	'assigne_a' => 'Assigned to',
	'assignation_attribuee_a' => 'The ticket has been assigned to @nom@.',
	'assignation_mail_titre' => 'Change of ticket assignment',
	'assignation_modifiee' => 'Assignment Update',
	'assignation_non_modifiee' => 'Assignment not updated',
	'assignation_supprimee' => 'The assignment of this ticket has ben deleted.',

	// C
	'champ_assigner' => 'Assign to',
	'champ_composant' => 'Component',
	'champ_date' => 'Date',
	'champ_date_modif' => 'Modified on',
	'champ_description' => 'Description of the ticket',
	'champ_exemple' => 'Example',
	'champ_id' => 'Number',
	'champ_importance' => 'Importance',
	'champ_jalon' => 'Milestone',
	'champ_nouveau_commentaire' => 'New comment',
	'champ_projet' => 'Project',
	'champ_severite' => 'Severity',
	'champ_titre' => 'Abstract',
	'champ_titre_ticket' => 'Title of the bug report',
	'champ_type' => 'Type',
	'champ_statut' => 'Status',
	'champ_url_exemple' => 'Example URL',
	'champ_version' => 'Version',
	'changement_statut_mail' => 'The status of this ticket has been changed from "@ancien@" to "@nouveau@".',
	'classement_assigne' => 'Tickets by assignment',
	'classement_asuivre' => 'Your tickets to follow',
	'classement_jalon' => 'Tickets by milestone',
	'classement_termine' => 'Closed bug reports',
    'classement_tous' => 'All the tickets',
	'classement_type' => 'Bug reports by type',
	'commenter_ticket' => 'Comment this bug report',
	'creer_ticket' => 'Create a bug report',

	// D
	'date_creation_auteur' => 'Ticket created on <strong>@date@</strong> by <strong>@nom@</strong>',

	// E
	'erreur_texte_longueur_mini' => 'The minimum length of text is @nb@ characters.',
	'erreur_verifier_formulaire' => 'Verify your form',
	'explication_description_ticket' => 'Describe as precisely as possible the need or problem.
	Indicate in particular if it occurs consistently or occasionally.
	If it is a display problem, specify with what browser you are experiencing it.',
	'explication_url_exemple' => 'Enter here the URL of a page covered by this ticket.',
	'explication_redaction' => 'When you\'re finished writing your ticket, select the status &laquo;open and discussed&raquo;.',

	// F
	'forum_message' => 'Your message',
	'forum_sans_previsu' => 'Warning&nbsp;: pas de pr&eacute;visualisation&nbsp;; votre message est publi&eacute; imm&eacute;diatement.',

	// I
	'info_numero_ticket' => 'TICKET NUMBER&nbsp;:',
	'icone_modifier_ticket' => 'Modify this ticket',
	'icone_retour_ticket' => 'Back to the ticket',

	// M
	'mail_texte_message_auto' => 'This is an automatic message : please don\'t answer.',
	'message_automatique' => 'This is an automatic message : please don\'t answer.',
	'message_poste_par' => 'Message posted by',
	'message_le' => 'on @date@',
	'message_publie' => 'Your message has been published',

	// N
	'no_assignation' => 'No one',
	'non_assignes' => 'Not owned',
	'nouveau_commentaire_mail' => 'New comment on ticket',
	'nouveau_ticket' => 'New ticket',

	// P
	'page_titre' => 'Tickets, bug tracking system',

	// R
	'revenir_gestion' => 'Back to the bug reports management',

	// S
	'severite_bloquant' => 'critical',
	'severite_important' => 'major',
	'severite_normal' => 'normal',
	'severite_peu_important' => 'trivial',
	'statut_mis_a_jour' => 'Status updated',
	'statut_ferme' => 'closed',
	'statut_ferme_long' => 'All the closed bug reports',
	'statut_inchange' => 'The status has been modified.',
	'statut_ouvert' => 'opened and discussed',
	'statut_redac' => 'editing in progress',
	'statut_resolu' => 'resolved',
	'statut_resolu_long' => 'All the resolved bug reports',

	// T
	'ticket' => 'Bug report',
	'ticket_enregistre' => 'The ticket has been saved.',
	'tickets' => 'Bug reports',
	'titre' => 'Tickets, bug tracking',
	'titre_liste' => 'List of the bug reports',
	'tous_tickets_ouverts' => 'All opened tickets',
	'type_amelioration' => 'improvements',
	'type_amelioration_long' => 'The improvements tickets',
	'type_probleme' => 'problem',
	'type_probleme_long' => 'Problems to resolve',
	'type_tache' => 'to do',
	'type_tache_long' => 'Current tasks',

	// V
	'vos_tickets_assignes' => 'Tickets assigned to you',
	'vos_tickets_en_cours' => 'Your tickets being editing',

	// Z
	'z' => 'zzz'
);


?>
