<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
'a' => '&agrave;',
'accedez_compte' => 'Acc&eacute;dez &agrave; votre compte',
'accueil_site' => 'Accueil',
'actualites' => 'Actualit&eacute;s',
'adress_opac' => 'Adresse de l\'OPAC',
'ajouter_tag' => 'Etiquetez cet ouvrage',
'annee_publication' => 'Ann&eacute;e de publication',
'auteur' => 'Auteurs',
'auteurs' => 'Auteurs',
'author_comment' => 'Commentaire',
'author_date' => 'Date',
'author_lieu' => 'Lieu',
'author_pays' => 'Pays',
'author_ville' => 'Ville',
'author_web' => 'Site internet',
'autres_lecteurs' => 'Les abonn&eacute;s qui ont emprunt&eacute; ce document ont &eacute;galement emprunt&eacute;',

// B
'bonjour' => 'Bonjour',

// C
'catalogue' => 'Catalogue',
'catalogue' => 'Catalogue',
'changer_logo' => 'Votre photo',
'code_barre_abbr' => 'CB',
'code_barre' => 'Code barre',
'collection' => 'Collection',
'collection_issn' => 'ISSN',
'collections' => 'Collections',
'collection_web' => 'Site internet',
'collectivite' => 'Collectivité',
'collectivite_autre' => 'Autre collectivité',
'comment_ca_fonctionne' => 'Comment &ccedil;a fonctionne ?',
'compte_lecteur' => 'Votre compte de lecteur',
'conferences' =>'P.U.L.P.',
'configurer_pmb' =>'Configuration de SPIP-PMB',
'configurer_rpc_json' =>'JSON-RPC (plus rapide, choisir par d&eacute;faut)',
'configurer_rpc_soap' =>'SOAP',
'config_pmb_partout' => 'Afficher la colonne de PMB partout sur le site ?',
'config_pmb_partout_explication' => 'PMB peut surcharger les fichiers de Zpip pour
	afficher sa colonne quelque soit la page du site. Par défaut sa colonne n\'est
	affichée que pour les pages concernant PMB. Mettre oui ici, l\'affichera partout (ou presque).',
'config_pmb_logo' => 'Logo de PMB',
'config_pmb_logo_explication' => 'Afficher le logo de PMB dans la colonne ?',
'cote' => 'Cote',

// D
'date' => 'Date',
'decouvrir' => 'd&eacute;couvrir',
'derniers_articles' => 'Actualit&eacute;s',
'disponiblite_abbr' => 'Disp.',
'disponiblite' => 'Disponibilit&eacute;',
'donner_avis' => 'Donnez votre avis',

// E
'ecrire_a' => 'Écrire à',
'editeur' => 'Éditeur',
'editeurs' => 'Éditeurs',
'edition' => 'Mention d\'édition',
'en_savoir_plus' => 'en&nbsp;savoir&nbsp;+',
'exemplaires' => 'Exemplaires',
'format' =>'Format',
'importance' => 'Importance',
'info_recherche_avancee' => 'Vous pouvez lancer une recherche portant sur un ou plusieurs mots<br />(titre, auteur, &eacute;diteur, ...)',
'isbn' => 'ISBN/ISSN/EAN',
'jsonrpc' => 'Adresse du service JSON-RPC',
'lire_la_suite' => 'Lire la suite',
'localisation_abbr' => 'Loc.',
'localisation' => 'Localisation',

// M
'message_compte_lecteur' => 'Prochainement, vous disposerez d\'une int\351gration compl\350te\nde votre compte lecteur sur ce site. \n\nEn attendant, vous allez \352tre redirig\351s vers l\'interface standard.',
'message_recherche_avancee' => 'Prochainement, vous disposerez d\'une int\351gration compl\350te\nde la recherche avanc\351e sur ce site. \n\nEn attendant, vous allez \352tre redirig\351s vers l\'interface standard.',
'mon_compte' => 'Mon compte',
'mots_cles' => 'Mots clés',

// N
'newsletter' => 'Newsletter',
'notices_consultees' => 'Notices consult&eacute;es',
'note_generale' => 'Note générale',
'note_contenu' => 'Note de contenu',
'nouveautes' => 'Nouveaut&eacute;s du catalogue',
'numero' => 'Num&eacute;ro',

// O
'ouvrages' => 'Ouvrages',
'ouvrages_trouves' => 'ouvrages trouv&eacute;s',
'ouvrage_trouve' => 'ouvrage trouv&eacute;',

// P
'parametrage_options' => 'Options du plugin SPIP-PMB',
'parametrage_catalogue' => 'Param&eacute;trage du catalogue',
'parametrage_plugin' => 'Param&eacute;trage du plugin',
'pas_d_ouvrages_trouves' => 'Aucun ouvrage trouv&eacute; dans le catalogue',
'pmb_login' => 'Identifiant',
'pmb_motdepasse' => 'Mot de passe',
'presentation' => 'Pr&eacute;sentation',
'prets_en_cours' => 'Pr&ecirc;ts en cours',
'prets_en_retard' => 'Pr&ecirc;ts en retard',
'prix' => 'Prix',
'publisher_address1' => 'Adresse',
'publisher_address2' => 'Adresse (suite)',
'publisher_city' => 'Ville',
'publisher_country' => 'Pays',
'publisher_web' => 'Site internet',
'publisher_zipcode' => 'CP',

// R
'rang' => 'Rang',
'recherche_avancee' => 'Recherche avanc&eacute;e',
'recherche_catalogue' => 'Dans le catalogue',
'recherche' => 'Recherche',
'rechercher_dans_catalogue' => 'Rechercher dans le catalogue',
'recherche_dans' => 'Recherche dans',
'recherche_portail_web' => 'Dans le site',
'reservation_ko' => 'Votre r&eacute;servation &agrave; n\'a pas pu &ecirc;tre prise en compte.',
'reservation_ok' => 'Votre r&eacute;servation &agrave; &eacute;t&eacute; prise en compte.',
'reservations' => 'R&eacute;servations',
'reserver_ouvrage' => 'R&eacute;servez cet ouvrage',
'resultats_dans_catalogue' => 'R&eacute;sultats dans le catalogue',
'resultats_dans_site' => 'R&eacute;sultats dans le site',
'resultats_precedents' => 'R&eacute;sultats pr&eacute;c&eacute;dents',
'resultats' => 'R&eacute;sultats',
'resultats_suivants' => 'R&eacute;sultats suivants',
'retour_recherche' => '&#91;Retour aux r&eacute;sultats de la recherche&#93;',
'retour' => 'Retour',
'rpc_type' => 'Type de serveur',
'rubriques' => 'Qui sommes-nous ?',

// S
'section' => 'Section',
'se_deconnecter' => 'Se d&eacute;connecter',
'serie' => 'S&eacute;rie',
'session_expire' => 'votre session a expir&eacute;',
'session_expire' => 'Votre session de lecteur a expir&eacute;. Veuillez vous d&eacute;connecter, puis vous identifier de nouveau.',
'support' => 'Support',
'sur' => 'sur',
'source' => 'Lien sur cette ressource',
'sous_collection' => 'Sous collection',

// T
'titre_lien_pmb' => 'PMB, un SIGB enti&egrave;rement libre',
'titre' => 'Titre',
'tous_coupsdecoeur' => 'toutes les critiques',
'toute_actualite' => 'toute l\'actualit&eacute;',
'toutes_nouveautes' => 'toutes les nouveaut&eacute;s',
'type' => 'Type',

// U
'unite_materielle' => 'Unité matérielle', // fait partie de...
'url' => 'Adresse',
'vous_etes_identifies' => 'Vous &ecirc;tes identifi&eacute;',
'wsdl' => 'Adresse du WSDL SOAP',

);
?>
