<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-pmb
// Langue: fr
// Date: 06-12-2011 11:50:22
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// P
	'pmb_description' => 'Ce portail web pour [PMB->http://www.pmbservices.fr]
	contient un squelette prêt à l’emploi compatible avec les thèmes ZPIP.
	L’interface avec le catalogue pmb s’effectue par les webservices pmb disponibles
	à partir de la version 3.3.

	PMB est un Sigb (Système Intégré de Gestion de Bibliothèque) libre.',
	
	'pmb_nom' => 'SPIP-PMB',
	'pmb_slogan' => 'Portail Web pour le logiciel PMB',
);
?>
