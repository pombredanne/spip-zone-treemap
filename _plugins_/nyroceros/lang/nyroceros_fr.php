<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	//C
	'cfg_boite_nyroceros' => 'Configuration du plugin Nyroceros',
	'cfg_descr_nyroceros' => 'Nyroceros : Plugin jQuery Nyromodal pour SPIP',
	'cfg_inf_bgcolor' => 'Modifier la couleur de fond de la fen&ecirc;tre',
	'cfg_inf_selecteur_commun' => 'Indiquez la cible des &eacute;l&eacute;ments qui d&eacute;clencheront Nyromodal. (Expression CSS ou &eacute;tendue jQuery)',
	'cfg_inf_selecteur_galerie' => 'Indiquez la cible des &eacute;l&eacute;ments qui d&eacute;clencheront une galerie avec Nyromodal . (Expression CSS ou &eacute;tendue jQuery)',
	'cfg_inf_traiter_toutes_images' => 'Traiter toutes les images avec Nyroceros ?',
	'cfg_inf_preload' => 'Pr&eacute;charger les images des galeries photo',
	'cfg_inf_installer_diapo_auto' => 'Installer le disporama automatique',
	
	'cfg_lbl_bgcolor' => 'Couleur de fond',
	'cfg_lbl_selecteur_commun' => 'En g&eacute;n&eacute;ral',
	'cfg_lbl_selecteur_galerie' => 'En galerie',
	'cfg_lbl_installer_diapo_auto' => 'Diaporama automatique',
	'cfg_titre_nyroceros' => 'Nyroceros',
	'cfg_lbl_traiter_toutes_images' => 'Images',
	'cfg_lbl_preload' => 'Pr&eacute;chargement'	
	
);
?>
