<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(


      //A
      'adresse_liste' => 'Adresse de la liste',
      'action' => 'Gérer',
      'auteur_listes' => 'Gestion des listes SYMPA',
      'abonnes_liste' => 'Abonnés à la liste',
      'ajouter_abonne' => 'Ajouter ',
      'ajouter_abonne_warning' => 'Ajouter un abonné à cette liste.',
    'abonnes_liste_pas_droit' => 'Vous n\'avez pas les droits pour gérer cette liste. Essayez la liste test...',
'abonnement_ok' => 'Abonnement réussi !',
'abonnement_erreur' => 'Abonnement échoué. Contacter le webmestre.',

//B
'btn_abonnement_lists' => 'Abonner l\'auteur aux listes sélectionnées.',
'btn_desabonnement_lists' => 'Désabonner l\'auteur aux listes sélectionnées.',
'btn_desabonnement_emails' => 'Abonner les emails sélectionnés.',
'btn_desabonnement_emails' => 'Désabonner les emails sélectionnés.',
	// C
	'configurer_soapsympa' => 'Spip ml SYMPA',
	'choisir_liste' => 'Vous devez choisir une liste.',
	
	//D
	'description_liste' => 'Description de la liste',
'desabonnement_ok' => 'désabonnement réussi !',
'desabonnement_erreur' => 'Désabonnement échoué. Contacter le webmestre.',

	// E
	'edition_soapsympa' => 'Gérer listes SYMPA',
	'enregistrement_reussi' => 'Enregistrement réussi !',
	'email_proprietaire' => 'Email du propriétaire des listes',
'emails_liste' => 'Courriels',
	'mot_de_passe' => 'Mot de passe',
	'identifiant' => 'Identifiant',
	'nom_domaine' => 'Nom de domaine des listes',
	'serveur_wsdl' => 'URL du serveur wsdl',

	// I
	// L
	'listes_abonne' => 'Listes auxquelles l\'auteur est abonné',
	'listes_nonabonne' => 'Autres listes',
	
	'liste_des_listes' => 'Liste des listes',
	'liste_total_abonnes' => 'Nombre total d\'abonnés',

	

	// M
	'message_confirmation_a' => 'Abonnements effectué',
	'message_confirmation_unique_a' => 'Abonnement effectué',
	'message_confirmation_d' => 'Désabonnement effectué ',
	'message_confirmation_unique_d' => 'Désabonnement effectué',
	// N
	

	// O
	

	// R
	'rubriques' => 'Rubriques',

	// S
	'statut' => 'Statut de l\'auteur',
	'signoff' => 'Signoff',
	'subscribe' => 'Subscribe',

	// T
	'votre_email' => 'Saisissez courriel',
	'voir_abonnes' => 'Voir les abonnés'

);

?>