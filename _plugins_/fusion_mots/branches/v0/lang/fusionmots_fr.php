<?php
/** Fichier de langue de SPIP **/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	'erreur_par'		=>'Choisissez un mot de remplacement',
	'erreur_remplacer'	=>'Choisissez au moins un mot à remplacer',
	'explication'		=>'Sur cette page vous pouvez fusionner des mots-clefs. Choisissez à gauche les mots-clefs à remplacer par celui choisi à droite.',
	'explication_intro'	=>'Fusionner plusieurs mots et leurs utilisations.',
	'choisir_groupe'	=>'Choisissez le groupe de mots dont vous souhaitez fusionner des mots', 
	'tous_groupes'		=>'Tous les groupes',
	'fusionmots'		=>"Fusionner des mots-clef",
	'par'				=>'Par',
	'remplacer'			=>'Remplacer le(s) mot(s)-clef',
	
);

?>