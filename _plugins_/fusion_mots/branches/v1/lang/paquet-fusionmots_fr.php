<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Module: paquet-fusionmots
// Langue: fr

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// F
	'fusionmots_description' => 'Permet aux administrateurs de remplacer en série des mots-clés par d\'autres, pour en diminuer le nombre et éliminer les redondances.',
	'fusionmots_slogan' => 'Vous avez trop de mots ? Fusionnez les !',
);
?>