Pistes pour l'avenir de ce plugin :

- proposer le selecteur generique d�s lors qu'il y a plus d'une centaine de motcl�s dans le groupe

- permettre de trier par anciennet� de cr�ation l'affichage des mots dans la colonne de gauche. Ce serait particuli�rement utile, puisque notamment avec les plugins spipicious ou etiquette, on voudra prioritairement s'occuper des motcl�s r�cemment cr��s pour �viter les doublons.