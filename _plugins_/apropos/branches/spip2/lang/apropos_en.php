<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'les_plugins' => 'It use these plugins:',

	// S
	'site_utilise' => 'This site use a <a href="http://en.wikipedia.org/wiki/Content_management_system" title="Definition of a Content management system on Wikipedia">CMS</a>. It is developed and works with <a href="http://www.spip.net/rubrique25.html" title="The official site of SPIP">SPIP</a>, version',
	'spip_contrib' => 'For more informations about these plugins, go on the site <a href="http://www.spip-contrib.net/-English-" title="A lot of SPIP users are maintaining the external contributions site SPIP-CONTRIB: it is a mine of information and solutions for various problems.">http://www.spip-contrib.net</a>.',
	
);

?>
