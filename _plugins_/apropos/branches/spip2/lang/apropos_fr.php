<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/cfg/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	

	// P
	'les_plugins' => 'Il utilise les plugins suivants :',

	// S
	'site_utilise' => 'Ce site utilise un <a href="http://fr.wikipedia.org/wiki/Systeme_de_gestion_de_contenu" title="D&eacute;finition de CMS sur Wikipedia">CMS</a>. Il est &eacute;labor&eacute; et tourne avec <a href="http://www.spip.net/fr" title="Aller sur le site officiel de SPIP">SPIP</a> version ',
	'spip_contrib' => 'Pour plus d\'informations sur ces plugins, rendez vous sur le site <a href="http://www.spip-contrib.net/" title="L&rsquo;espace des contributions externes, qui recense l&rsquo;ensemble des fonctionnalit&eacute;s suppl&eacute;mentaires mises &agrave; disposition &agrave; la communaut&eacute; par les utilisateurs de SPIP.">http://www.spip-contrib.net</a>.',
	
	// T
	
	'titre'			=> 'À propos'

);

?>
