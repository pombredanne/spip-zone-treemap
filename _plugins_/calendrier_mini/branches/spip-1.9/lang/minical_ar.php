<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'date_jour_abbr_1' => 'أحد',
	'date_jour_abbr_2' => 'اثنين',
	'date_jour_abbr_3' => 'ثلثاء',
	'date_jour_abbr_4' => 'أربعاء',
	'date_jour_abbr_5' => 'خميس',
	'date_jour_abbr_6' => 'جمعة',
	'date_jour_abbr_7' => 'سبت'
);

?>
