<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/paquet-calendriermini?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'calendriermini_description' => 'Umožňuje používať tag #CALENDRIER_MINI.
_ Tento tag zobrazí vzhľad kalendára bez bodiek, a preto je kompatibilný so štýlmi z tohto blogovacieho systému.
_ Pomáha ďalším prvkom, ako sú tagy, kritériá, šablóny a i.
_ Ikona [Tempest->http://tempest.deviantart.com/] pod licenciou CC BY-NC-ND.',
	'calendriermini_slogan' => 'Umožňuje používať tag #CALENDRIER_MINI'
);

?>
