<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/calendrier_mini/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'calendriermini_description' => 'Permet l\'utilisation d\'une balise #CALENDRIER_MINI.
_ Cette balise affiche un calendrier au design issu de dotclear et donc compatible avec les styles issus de ce système de blog.
_ Lui sont adjoints d\'autre éléments, tels que balises, critères, modèles...
_ Icone de [Tempest->http://tempest.deviantart.com/] sous licence CC BY-NC-ND.',
	'calendriermini_slogan' => 'Permet l\'utilisation d\'une balise #CALENDRIER_MINI'
);

?>
