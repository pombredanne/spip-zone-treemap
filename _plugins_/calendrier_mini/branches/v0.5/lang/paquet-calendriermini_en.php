<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/paquet-calendriermini?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'calendriermini_description' => 'Allow to use a #CALENDRIER_MINI tag.
_ This tag displays a calendar designed as dotclear\'s one and therefore compatible with the styles from this blog system.
_ Other tools are added, such as tags, criteria, models...
_ Icon from [Tempest->http://tempest.deviantart.com/] under CC BY-NC-ND license.',
	'calendriermini_slogan' => 'Allow to use a #CALENDRIER_MINI tag'
);

?>
