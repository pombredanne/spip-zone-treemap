<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/minical?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'date_jour_abbr_1' => 'So',
	'date_jour_abbr_2' => 'Mo',
	'date_jour_abbr_3' => 'Di',
	'date_jour_abbr_4' => 'Mi',
	'date_jour_abbr_5' => 'Do',
	'date_jour_abbr_6' => 'Fr',
	'date_jour_abbr_7' => 'Sa'
);

?>
