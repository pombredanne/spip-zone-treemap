<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org/tradlang_module/minical?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'date_jour_abbr_1' => 'ned.',
	'date_jour_abbr_2' => 'pon.',
	'date_jour_abbr_3' => 'uto.',
	'date_jour_abbr_4' => 'str.',
	'date_jour_abbr_5' => 'štv.',
	'date_jour_abbr_6' => 'pia.',
	'date_jour_abbr_7' => 'sob.'
);

?>
