<?php 
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array (
	'ajouter' => 'Ajouter',
	'ajouter_lien' => 'Ajouter un lien entre ',
	'aj_zone' => 'Ajouter une zone',
	'aj_auteur' => 'Ajouter des auteurs',
	'conf_suppr' => 'Confirmer la suppression de :',

	'email' => 'Email',
	'et_groupe' => 'et le groupe',

	'gerer_auteur_txt1' => 'Lier',
	'gerer_auteur_txt2' => 'au groupe',
	'gerer_auteur_txt3' => 'Liste des groupes auxquels appartient l\'auteur',
	'gerer_auteur_txt4' => 'n\'appartient a aucun groupe',	
	
	'gerer_groupe_titre' => 'Gerer les groupes',

	'gerer_liens' => 'Gerer les liens',

	'id' => 'Id',

	'liste_actions_act1' => 'Gerer les groupes',
	'liste_actions_act2' => 'Liste des auteurs',
	'liste_actions_titre' => 'Liste des actions',

	'liste_groupes_nom' => 'Nom',
	'liste_auteurs_gerer' => 'Gerer auteur',

	'liste_auteurs_groupe' =>'Liste des auteurs',
	'liste_auteurs_groupe_txt1' =>'Liste des auteurs appartenant a',
	'login' => 'Login',

	'mod' => 'Modifier',
	'mod_ok' => 'Modification effectuee',

	'nom' => 'Nom',
	'nom_groupe' => 'Nom :',
	
	'pas_nom' => 'Le champ nom de doit pas etre vide',

	'suppr' => 'Supprimer',
	'suppr_zone' => 'Supprimer la zone',
	'titre_page' => 'Groupes',

	'zones' => 'Zones : '
)

?>