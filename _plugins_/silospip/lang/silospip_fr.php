<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/_dev_/silospip/lang
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_silo' => 'Cr&eacute;ation de sites SPIP en libre-service',

	// C
	'champ_obligatoire' => 'Ce champs ne peut &ecirc;tre vide.',
	'creer_site' => 'Cr&eacute;er ce site',
	'changer_nom_ou_dom' => 'Choisissez un autre nom ou un autre domaine.',


	// D
	'descriptif_site' => 'Descriptif du site',
	'domaine_site' => 'Choisis un domaine pour le site&nbsp;:',
	'donnees_site_valides' => 'Veuillez confirmer la cr&eacute;ation du site&nbsp;:',

	// E
	'explication_silo' => 'Les visiteurs qui s\'inscrire pourront cr&eacute;er leur propre site SPIP',

	// G
	'gen_champ_obligatoire' => 'Certains champs obligatoires n\'ont pas &eacute;t&eacute; renseign&eacute;s',
	'gen_chars_invalides_nom' => 'Le nom du site contient des caract&egrave;res invalides',
	'gen_site_existe' => 'Ce site existe d&eacute;j&agrave;',

	// L
	'lang_site' => 'Langue principale du site&nbsp;:',

	// N
	'nom_site' => 'Nom du site&nbsp;:',

	// M
	'modifier_donnees' => 'Modifier ces donn&ees',

	// S
	'site_a_creer' => 'Votre site sera cr&eacute;&eacute; avec les donn&eacute;es suivantes&nbsp;:',
	'site_existe' => 'Le site @url@ existe d&eacute;j&agrave;.',

	// T
	'titre_site' => 'Titre du site&nbsp;:',
	
	// U
	'url_site' => 'Adresse URL du site&nbsp;:'

);

?>
