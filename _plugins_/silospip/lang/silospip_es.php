<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/_dev_/silospip/lang
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_silo' => 'Creaci&oacute;n de sitios SPIP en libre servicio',
	
	// C
	'champ_obligatoire' => 'Este campo no puede quedar vac&iacute;o.',
	'creer_site' => 'Crear este sitio',
	'changer_nom_ou_dom' => 'Elije otro nombre u otro dominio.',

	// D
	'descriptif_site' => 'Descripci&oacute;n del sitio:',
	'domaine_site' => 'Escoge un dominio para el sitio:',
	'donnees_site_valides' => 'Por favor, confirmar de la creaci&oacute;n del sitio:',

	// E
	'explication_silo' => 'Los visitantes que se inscriben podr&aacute;n activar su propio sitio SPIP',

	// G
	'gen_champ_obligatoire' => 'No fueron llenados ciertos campos que son obligatorios',
	'gen_chars_invalides_nom' => 'El nombre del sitio contiene caracteres no autorizados',
	'gen_site_existe' => 'Este sitio ya existe',

	// L
	'lang_site' => 'Idioma principal del sitio:',

	// N
	'nom_site' => 'Nombre del sitio:',

	// M
	'modifier_donnees' => 'Modificar estos datos',

	// S
	'site_a_creer' => 'Tu sitio ser&aacute; creado con los siguientes datos:',
	'site_existe' => 'El sitio @url@ ya existe.',

	// T
	'titre_site' => 'T&iacute;tulo del sitio:',
	
	// U
	'url_site' => 'Direcci&oacute;n URL del sitio:'
	
);

?>
