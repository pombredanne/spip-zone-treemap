<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/mes_fichiers/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'mes_fichiers_description' => 'Ce plugin fabrique un fichier d\'archives contenant les données de personnalisation du site comme le dernier dump de sauvegarde de la base, les dossiers des squelettes personnalisés, le dossier des images...
Le fichier d\'archives est créé dans <code>tmp/mes_fichiers/</code> et contient dans son nom la date de création.

Il est aussi possible de lancer une sauvegarde régulière automatique de tous les fichiers de personnalisation combinée à un nettoyage des archives obsolètes et de notifier toutes les actions.',
	'mes_fichiers_nom' => 'Mes fichiers',
	'mes_fichiers_slogan' => 'Archiver ses fichiers et dossiers de personnalisation'
);

?>
