<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-mes_fichiers?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'mes_fichiers_description' => 'This plugin builds an archive file containing the customisation datas of the site like the latest database dump, the templates, the images folder...
The archive file is created in <code>tmp/mes_fichiers/</code> and its filename includes the creation date.

It is also possible to initiate an automatic backup of all customisation files combined with deletion of obsolete archive files and to notify each action.',
	'mes_fichiers_nom' => 'My files',
	'mes_fichiers_slogan' => 'Backup your customisation files and folders'
);

?>
