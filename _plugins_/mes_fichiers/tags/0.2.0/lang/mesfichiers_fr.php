<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// I
'info_sauvegarde_reussi_01' => 'Sauvegarde en progression.',
'info_sauvegarde_reussi_02' => 'La base a &eacute;t&eacute; sauvegard&eacute;e dans @archive@. Vous pouvez',
'info_sauvegarde_reussi_03' => 'poursuivre par la cr&eacute;ation',
'info_sauvegarde_reussi_04' => 'du fichier @fichier@.',

// T
'texte_admin_tech_01' => 'Cette option vous permet de sauvegarder le contenu de la base dans un fichier qui sera stock&eacute; dans le r&eacute;pertoire @dossier@. Elle y ajoutera aussi l\'int&eacute;gralit&eacute; du r&eacute;pertoire @img@, qui contient les images et les documents utilis&eacute;s dans les articles et les rubriques, ainsi que vos dossier @squelettes@ et fichier de configuration @options@, s\'ils existent.',
'texte_admin_tech_02' => 'Attention: cette sauvegarde ne pourra &ecirc;tre restaur&eacute;e QUE dans un site install&eacute; sous la m&ecirc;me version de SPIP. Il ne faut donc surtout pas &laquo;&nbsp;vider la base&nbsp;&raquo; en esp&eacute;rant r&eacute;installer la sauvegarde apr&egrave;s une mise &agrave; jour... Consultez <a href="http://www.spip.net/fr_article1489.html">la documentation de SPIP</a> ou celle <a href="http://www.spip-contrib.net/Mes-fichiers">du plugin mes_fichiers.zip</a>.',
'texte_admin_tech_03' => 'Le fichier est sauvegard&eacute; au format zip.',
'texte_sauvegarde_compressee' => 'Il ne semble pas possible d\'utiliser pleinement cette fonction. Nous vous conseillons de d&eacute;sactiver ce plugin ou de contacter votre administrateur pour qu\'il active la compression gz. La sauvegarde sera faite dans le fichier non compress&eacute; @fichier@ qui ne contiendra que la base donn&eacute;es.',

);


?>
