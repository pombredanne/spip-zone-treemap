<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// B
'bouton_mes_fichiers' => 'Backup my files',
'bouton_sauver' => 'Backup',
'bouton_telecharger' => 'Download',
'bouton_tout_cocher' => 'Select all',
'bouton_tout_decocher' => 'Deselect all',
'bouton_voir' => 'See',
'bulle_bouton_voir' => 'See the content of the archive',

// C
'colonne_date' => 'Date',
'colonne_nom' => 'Name',
'colonne_taille_octets' => 'Size',
'colonne_actions' => '&nbsp;',

// E
'erreur_aucun_fichier_sauver' => 'There is no file to backup',
'erreur_droits_insuffisants' => 'You don\'t have sufficient rights to perform a backup.',
'erreur_repertoire_trop_grand' => 'This folder exceed the size limit of @taille_max@ MB and can\'t be saved.',
'explication_cfg_notif_mail' => 'These emails are added to the one of the webmaster. They must be separated by commas ",".',

// I
'info_sauver' => 'This option makes an archive file containing the customization datas of the site as the last database dump, the skeleton(s) file(s), the images folder ...
	The archive file is created in <i>tmp/mes_fichiers/</i> and is called <i>@prefixe@_aaaammjj_hhmmss.zip</i>.
	The full list of files and folders can be saved is displayed below :',
'info_telecharger' => 'This option allows you to download one of the archives files available from the list displayed below :',

// J
'jours' => 'days',

// L
'label_cfg_duree_sauvegarde' => 'Preservation period of the backups',
'label_cfg_frequence' => 'Backup frequency',
'label_cfg_taille_max_rep' => 'Maximum size of directories that can be saved (en MB)',
'label_cfg_notif_active' => 'Enable notifications',
'label_cfg_notif_mail' => 'Email addresses to notify',
'label_cfg_prefixe' => 'Prefix used for the filename',
'label_cfg_sauvegarde_reguliere' => 'Make a periodic backup',
'legende_cfg_notification' => 'Notifications',
'legende_cfg_sauvegarde_reguliere' => 'Make periodic backups',


// M
'message_cleaner_sujet' => 'Deleting backups',
'message_notif_cleaner_intro' => 'The action of automatic deletion of obsolete backup files (whose date is after @frequence@ days) was performed successfully. The following files have been deleted : ',
'message_rien_a_sauver' => 'No files or folders to back up.',
'message_rien_a_telecharger' => 'No backup available to download.',
'message_rien_a_restaurer' => 'No backups available for restoration.',
'message_sauvegarde_ok' => 'The archive file has been created.',
'message_sauvegarde_nok' => 'Backup error. The archive file has not been created.',
'message_notif_sauver_intro' => 'New backup of your files is available for your site. It was created by @auteur@.',
'message_sauver_sujet' => 'Backup files',
'message_telechargement_ok' => 'The archive file has been downloaded.',
'message_telechargement_nok' => 'Error downloading.',
'message_zip_propriete_nok' => 'No property is available on this archive.',
'message_zip_sans_contenu' => 'No information is available on the contents of this archive.',
'message_zip_auteur_indetermine' => 'Undetermined',
'message_zip_auteur_cron' => 'Cron job',

// R
'resume_table_telecharger' => 'List of archives you can download',
'resume_zip_auteur' => 'Created by',
'resume_zip_statut' => 'Status',
'resume_zip_compteur' => 'Files / folders archived',
'resume_zip_contenu' => 'Summary of the content',


// T
'titre_page_navigateur' => 'My files',
'titre_page' => 'Save my customization files',
'titre_boite_sauver' => 'Backup my files',
'titre_boite_telecharger' => 'Download a backup',

);


?>
