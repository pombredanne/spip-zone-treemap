<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'titre_aide_surtitre' => 'Fonction du champ surtitre',
'titre_aide_titre' => 'Fonction du champ titre',
'titre_aide_soustitre' => 'Fonction du champ soustitre',
'titre_aide_descriptif' => 'Fonction du champ descriptif',
'titre_aide_texte' => 'Fonction du champ texte',
'titre_aide_ps' => 'Fonction du champ PS',
'aide_surtitre' => '{{{Sur-titre de l\'article}}}

Peut &ecirc;tre utiliser pour pr&eacute;ciser le titre en lui fournissant un contexte pour l\'introduire.',
'aide_titre' => '{{{Titre de l\'article}}}

Peut commencer par num&eacute;ro point espace puis le titre pour ordonner les articles par num&eacute;ro :
_ Exemple : 10. Un titre class&eacute;
_ Ne pas h&eacute;siter &agrave; num&eacute; de 10 en 10 pour pouvoir ins&eacute;rer une article en deux plus tard !

{{Un titre doit &ecirc;tre explicite et court !}}',
'aide_soustitre' => '{{{Sous-titre de l\'article}}}

Peut &ecirc;tre utiliser pour pr&eacute;ciser le titre en lui fournissant un compl&eacute;ment d\'information.',
'aide_descriptif' => '{{{Descriptif de l\'article}}}

Il sera utilis&eacute; pour tous les liens vers l\'article dans le site (plan du site, dans la m&ecirc;me rubrique, quoi de neuf...) comme bulle d\'aide.
_ Cela permet de pr&eacute;ciser le sens du titre.

Si le Chapo n\'est pas rempli, alors, c\'est le descriptif rapide qui sera affich&eacute; &agrave; sa place.

De plus, ce texte est index&eacute; par les moteurs de recherche ce qui ne peut qu\'am&eacute;liorer le r&eacute;f&eacute;rencement du site.',
'aide_texte' => 'La partie principale de la page.

N\'oubliez pas que la mise en forme doit &ecirc;tre au service du {{sens du texte}} et non de votre go&ucirc;t artistique personnel !',
'aide_ps' => '&Agrave; utiliser pour donner un compl&eacute;ment d\'information sur l\'article dans un cadre plac&eacute; &agrave; la fin de celui-ci.',
);
?>
