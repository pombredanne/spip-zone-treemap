<?php
/*
+--------------------------------------------+
| DW2 2.13 (02/2007) - SPIP 1.9.1
+--------------------------------------------+
| H. AROUX . Scoty . koakidi.com
| Script certifi� KOAK2.0 strict, mais si !
+--------------------------------------------+
| This is a SPIP frontoffice language file
| Ceci est un fichier langue de SPIP espace public
+--------------------------------------------+
| Translated into Inconsistent English by Andy Mountain. 
| Be Cool, be Good, be Me ! Type F1 for help !" (02/2007) 
+--------------------------------------------+
*/
# for your translation : #n  => new || #m => modified || #del => delete

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
'catalogue' => 'Catalogue',

// D
'droits_insuffisant' => 'You don\'t have enough rights to access this file.',


// E
'etre_identifier_pour_telecharger' => 'To download this document you must be registered and logged in.',
'echec_acces_fichier' => 'File access failed.',


// F
'close' => '',


// I
'info_erreur_doc_out' => 'The requested document\'s ID has changed,<br />
							has been temporarily removed<br />
							or<br />does not exist any more on this site !<br />
							See the following page :<br />',


// Z
'z' => 'z'
);
?>
