<?php
/*
+--------------------------------------------+
| DW2 2.13 (02/2007) - SPIP 1.9.1
+--------------------------------------------+
| H. AROUX . Scoty . koakidi.com
| Script certifi� KOAK2.0 strict, mais si !
+--------------------------------------------+
| This is a SPIP frontoffice language file
| Ceci est un fichier langue de SPIP espace public
+--------------------------------------------+
| Traduit de l'Idiot Moderne par Scoty (02/2007)
+--------------------------------------------+
*/
# for your translation : #n  => new || #m => modified || #del => delete

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
'catalogue' => 'Catalogue',


// D
'droits_insuffisant' => 'Vous n\'avez pas les droits suffisants pour acc&eacute;der &agrave; ce fichier',


// E
'etre_identifier_pour_telecharger' => 'Pour t&eacute;l&eacute;charger ce document 
										vous devez &ecirc;tre inscrit et vous identifier.',
'echec_acces_fichier' => 'Echec d\'acc&egrave;s au fichier.',


// F
'fermer' => '',


// I
'info_erreur_doc_out' => 'Le document demand&eacute; &agrave; chang&eacute; d\'identifiant<br />
							ou a &eacute;t&eacute; retir&eacute; pour maintenance<br />
							ou<br />n\'existe plus sur notre site !<br />
							Consultez la page suivante :<br />', // see in dw_fr.php


// Z
'z' => 'z'
);
?>
