<?php
/*
+--------------------------------------------+
| DW2 2.14 (03/2007) - SPIP 1.9.2
+--------------------------------------------+
| H. AROUX . Scoty . koakidi.com
| Script certifi� KOAK2.0 strict, mais si !
+--------------------------------------------+
| This is a SPIP language file
| Ceci est un fichier langue de SPIP
+--------------------------------------------+
| Traduit de l'Idiot Moderne par Scoty (02/2007)
+--------------------------------------------+
*/
# for your translation : #n  => new || #m => modified || #del => delete

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
'abonne_s' => 'Inscrits',
'abrv_ko' => 'ko', // abreviation kilo-octet
'abrv_mo' => 'mo', // abreviation mega-octet
'abrv_oc' => 'oc', // abreviation octet (j'invente !)
'accueil' => 'Accueil',
'acc_dw2_st' => 'Suivi T&eacute;l&eacute;chargement',
'acc_dw2_dd' => 'Docs D&eacute;localis&eacute;s',
'acces_serv_enreg' => 'l\'acc&egrave;s &agrave; ce serveur est enregistr&eacute; !',
'actuellement' => 'Actuellement',
'administration' => 'ADMINISTRATION',
'adresse_mail_bad' => '<b>Attention : </b> adresse email non conforme !',
'affect_repert_hote' => 'Affecter un autre r&eacute;pertoire &agrave; cet H&ocirc;te',
'affiche_serveur' => 'Afficher ce serveur',
'aide' => 'AIDE en ligne',
'ajouter' => 'Ajouter',
'ajout_doc' => 'Ajouter Documents',
'ajout_images_1_1' => 'Ajout Images 1/1',
'ajout_manuel' => 'Ajout manuel',
'ajout_par_article' => 'Ajout par Article',
'ajouter_select' => 'Ajouter la s&eacute;lection',
'ajout_tout' => 'Tout Ajouter',
'annuler' => 'Annuler',
'archives' => 'Archives',
'archiver_tout' => 'Archiver',
'archiver_fiche' => 'Archiver cette Fiche',
'article' => 'article',
'associer' => 'Associer', // bouton
'assoc_du_fichier_cat' => 'Association du fichier<br />[ @nomfich@ ]<br />dans le Catalogue.',
'attention_info' => 'ATTENTION, info d\'usage',
'attention' => 'ATTENTION',
'aucun_doc_archive' => 'Aucun Document archiv&eacute; !',
'aucun_doc_num' => 'Aucun Document avec ce num&eacute;ro !', // exception javascript caract' accentue
'aucun_doc_type_img' => 'Aucun document de type jpg, png ou gif sur ce site',
'aucun_fich_assoc' => 'Aucun fichier associ&eacute;',
'aucun_fichier_export' => 'Aucun fichier &agrave; exporter',
'aucun_serv_enreg' => 'Aucun Serveur n\'est enregistr&eacute; !',
'aucun_telech_moment' => 'Pas de T&eacute;l&eacute;chargements pour le moment !',
'aucun_telech_periode' => 'Pas de T&eacute;l&eacute;chargements pour la p&eacute;riode demand&eacute;e !',
'au_webmaster' => 'Au webmaster :',
'autres' => 'Autres',


// C
'catalogue' => 'Catalogue',
'catalogue_dw2' => 'Catalogue DW2',
'categorie' => 'Cat&eacute;gorie',
'categories' => 'Cat&eacute;gories',
'categories_exist' => 'Cat&eacute;gories existantes',
'cata_images' => 'Catalogue Images',
'cat_images_de' => 'Catalogue Images de @nom_site_spip@',
'cfg_anti_triche_val_oui' => 'oui',
'cfg_anti_triche_val_non' => 'non',
'cfg_type_categorie_val_secteur' => 'secteur',
'cfg_type_categorie_val_rubrique' => 'rubrique',
'cfg_mode_enregistre_doc_val_manuel' => 'manuel',
'cfg_mode_enregistre_doc_val_auto' => 'auto',
'cfg_mode_affiche_images_val_1' => 'lignes',
'cfg_mode_affiche_images_val_2' => 'vignettes',
'cfg_avis_maj_val_oui' => 'oui',
'cfg_avis_maj_val_non' => 'non',
'cfg_mode_restreint_val_oui' => 'oui',
'cfg_mode_restreint_val_non' => 'non',
'cfg_criteres_auto_doc_val_1' => 'jpg',
'cfg_criteres_auto_doc_val_2' => 'png',
'cfg_criteres_auto_doc_val_3' => 'gif',
'cfg_forcer_url_dw2_val_oui' => 'oui',#n2.13
'cfg_forcer_url_dw2_val_non' => 'non',#n2.13
'champs_modif' => 'Modifier ...',
'changer_assoc_fichier_serveur' => 'Changer association Fichier-Serveur',
'changer_statut_masse' => 'Changer le statut en masse',
'cette_adresse' => 'Cette adresse :',
'chemin' => 'Chemin',
'choix_article_liste' => '--- Choisir un article de la liste ---',
'compression_fichier' => 'Compression fichier ?',
'compteur' => 'Compteur',
'conten_repert_img' => 'Contenu du R&eacute;pertoire local IMG/',
'contient_nb_fich' => 'contient : @nb_k@ fichier',
'contient_nb_fich_s' => 'contient : @nb_k@ fichiers',
'controle_sur_serv' => 'Contr&ocirc;le de pr&eacute;sence sur le serveur :',
'crea_fichier_impossible_droits' => 'Impossible de cr&eacute;er @nom_fichier@,
									v&eacute;rifiez les droits d\'&eacute;criture du
									r&eacute;pertoire @rep_bases@.',
'criteres_recherche_art_doc' => 'Crit&egrave;res de recherche des Articles/Rubriques<br />
								contenant des Documents',


// D
'dans_rub' => 'Dans la Rubrique',
'date_crea' => 'Date de cr&eacute;ation',
'date_heure' => '@jour@/@mois@/@annee@ - @heure@h@minutes@',
'date_verif_avis_maj_plugin' => 'Derni&egrave;re v&eacute;rification de Mise &agrave; Jour de DW2,
								le : @datemaj@',
'doc_de_article' => 'De l\'article',
'doc_de_breve' => 'De la br&egrave;ve',
'deplace_doc' => 'D&eacute;placer ce Document',
'dernier_en_date' => 'Derniers en Date',
'dernier_telech' => 'Dernier t&eacute;l&eacute;chargement',
'desactive' => 'D&eacute;sactiv&eacute;',
'destination_doc' => 'Destination du Document',
'descriptif' => 'Descriptif',
'destination_doc' => 'Destination : ',
'doc_dans_archive' => '<b>@nb_archive@<b/> Fiche-Document Archiv&eacute;e',
'doc_dans_archive_s' => '<b>@nb_archive@</b> Fiches-Document Archiv&eacute;es',
'doc_dans_cat' => '&nbsp;<b>@nb_actif@</b> Document dans le Catalogue',
'doc_dans_cat_s' => '&nbsp;<b>@nb_actif@</b> Documents dans le Catalogue',
'doc_dans_cat_depuis' => 'Documents enregistr&eacute;s dans le Catalogue depuis @jours_affiche_nouv@ jours',
'doc_de_rubrique' => 'Doc. de rubrique',
'doc_pas_dans_cat' => 'Ces Documents ne sont pas dans le Catalogue',
'doc_pas_dans_spip' => 'Les Documents suivants ne sont plus dans SPIP',
'doc_spip_n' => 'Document Spip n&deg;',
'doc_assoc' => '@nb_k@ Document associ&eacute;',
'doc_assoc_s' => '@nb_k@ Documents associ&eacute;s',
'doc_delocalises' => 'Documents D&eacute;localis&eacute;s',
'doc_lie_trt_descrip' => 'Titre et Descriptif du Document',
'doc_sans_origine' => 'Ce Document n\'est li&eacute; &aacute; aucun article, rubrique ...',
'doctype' => 'Article/Rubrique', // define of 'doctype' : article/section
'document' => 'Document',
'document_s' => 'Documents',
'document_export' => '&nbsp;@nb_doc@ Document exportable',
'document_export_s' => '&nbsp;@nb_doc@ Documents exportables',
'donnees_corrupt' => 'Probl&egrave;me avec les donn&eacute;es de @tablename@, corruption possible !',
'du_catalogue' => 'du Catalogue',
'dupliquer' => 'Dupliquer',
'duplic_serv' => 'Dupliquer ce Serveur',


// E
'echec_acces_fichier' => 'Echec acc&egrave;s au fichier.',
'echec_mail_save' => 'Echec &agrave; l\'envoi de @nom_fichier@ &agrave; @dest_save@',
'editer_serveur' => 'Editer serveur',
'efface_fiche' => 'EFFACER cette fiche du catalogue DW2',
'effacer' => 'EFFACER',
'effacer_fichier_local' => ' ... et effacer le fichier du r&eacute;pertoire LOCAL ?',
'affacer_serv' => 'Effacer ce Serveur',
'enreg_dans_cat' => 'Enregistr&eacute; dans le catalogue le ',
'enreg_dans_cat_et_nbr_jours' => 'Enregistr&eacute; dans le catalogue le @datecrea@ [ @nbrjour@ jours ]',
'enreg_doc' => 'Enregistrement du Document',
'enreg_lien_dans_cat' => 'Enregistrement de la Fiche dans le Catalogue',
'entree_cat' => 'Date Entr&eacute;e',
'envoi_sauvegarde_mail' => 'Envoi de la Sauvegarde par mail ?',
'evolution_telech' => '&Eacute;volution des T&eacute;l&eacute;chargements',
'exist_pas_sur_serv' => 'N\'existe pas/plus physiquement sur ce serveur !',
'export' => 'EXPORT',
'exporter' => 'EXPORTER',
'export_vers_serv' => 'Export vers ce serveur',


// F
'fichier' => 'Fichier',
'fichier_s' => 'Fichiers',
'fichier_nombre' => '@nb_k@ Fichier',
'fichier_nombre_s' => '@nb_k@ Fichiers',
'fichier_declar_sur_serv' => 'Actuellement, ce fichier est d&eacute;clar&eacute; sur le serveur :',
'fichiers_save_dans_repert' => 'Fichiers de Sauvegarde dans @rep_bases@',
'fichier_sur_serv_courant' => 'Le fichier est &eacute;galement pr&eacute;sent sur le serveur courant :',
'fonctions' => 'fonctions',


// H
'hote_ftp' => 'H&ocirc;te ftp',


// I
'image' => 'Image',
'import' => 'IMPORT',
'import_depuis_serv' => 'Import Virtuel depuis ce serveur',
'import_virtuel' => 'IMPORT Virtuel',
'info_change_statut_inverser' => 'Inverser : rendre actif la selection',
'info_change_statut_masse' => 'Archiver des Fiches de Documents - mode par d&eacute;faut.<br />
								Saisir les ID des Documents, s&eacute;par&eacute;s par des virgules (xxx,yyy,zzz).',
'info_erreur_doc_out' => 'Le document demand&eacute; &agrave; chang&eacute; d\'identifiant<br />
							ou a &eacute;t&eacute; retir&eacute; pour maintenance<br />
							ou<br />n\'existe plus sur notre site !<br />
							Consultez la page suivante :<br />',
'info_save_fonction' => 'Cette page vous permet de cr&eacute;er des fichiers de sauvegarde des tables de DW2.
						Fichiers exploitables dans PhpMyAdmin. Les fichiers sont effac&eacute; au-del&agrave;
						de la p&eacute;riode fix&eacute;e. Liste des fichiers en pied de page.
						Lors de la cr&eacute;ation, le fichier peut &ecirc;tre envoy&eacute; par mail, voir ci-dessous.',
'info_save_param' => 'L\'Administratur du site peut modifier les deux param&egrave;tres ci-dessus,
						directement dans le fichier : dw2_save_tbl.php.',
'info_save_non_compress' => '<b>Attention :</b> la sauvegarde ne pourra pas &ecirc;tre compress&eacute;e
							car cette fonctionnalit&eacute; n\'est pas disponible sur votre serveur.',
'inst_conf' => 'Configuration',
'integre_doc_dw' => 'Int&eacute;gr&eacute;',
'intitule_serveur' => 'Intitul&eacute; du Serveur',


// J
'jour_s' => 'jours',


// L
'la_fiche' => 'La fiche',
'les_deux' => 'Les deux',
'lien_telecharg' => 'Lien de t&eacute;l&eacute;chargement',
'liste_tables_base_impossible' => 'Impossible de lister les tables de la base @base@,
									corrigez le nom de votre base',
'login' => 'Login',


// M
'mail_save_msg_1' => 'Sauvegarde de DW2 effectu&eacute;e avec succ&egrave;s.', 	// execption mail caract accent
'mail_save_msg_base' => 'Base : @base@',							// execption mail caract accent
'mail_save_msg_serveur' => 'Serveur : @serveur@',					// execption mail caract accent
'mail_save_msg_admin' => 'Fait par : @admin@.',						// execption mail caract accent
'mail_save_sujet' => 'Sauvegarde tables DW2, @nom_site_spip@',		// execption mail caract accent
'maj_etat' => 'Etat :',
'maj_evolution_dw' => 'Mise &agrave; jour DW2',
'maj_taille_fichier' => 'V&eacute;rifier taille du fichier',
'maj_version' => 'version',
'message_config_01' => 'Attention ! Vous avez s&eacute;lectionn&eacute; :<br />
						Mode Restreint = oui || Forcer URL DOCUMENT = non<br />
						Assurez-vous que vos squelettes soient conforme &agrave; ce choix.<br />
						(Utilisation des Balises #URL_DOC_OUT/#URL_DOCUMENT)',#n2.13
'mess_err_1' => 'Attention aucun champ ne peut &ecirc;tre vide, sauf le champ optionnel !',
'mess_err_2' => 'Enlever "/" en fin de ligne "URL Site"',
'mess_err_3' => 'Enlever "/" en d&eacute;but de ligne "R&eacute;pertoires',
'mess_err_4' => 'Ajouter "/" en fin de ligne "R&eacute;pertoires"',
'mess_err_5' => 'Enlever "/" en fin de ligne H&ocirc;te ftp !',
'mess_err_6' => 'Erreur syntaxe sur la ligne "URL Site"',
'mess_err_7' => 'Vous n\'avez pas modifi&eacute le r&eacute;pertoire !',
'mess_err_ftpechec' => 'Echec connexion FTP : contact serveur refus&eacute;, verifier H&ocirc;te.',
'mess_err_loginechec' => 'Echec connexion FTP : mauvais login/mot-de-passe.',
'mess_err_repertechec' => 'Echec connexion FTP : acc&egrave;s au serveur, mais mauvais r&eacute;pertoire.',
'mess_export_fichier' => 'Fichier @fichier@ export&eacute;. Version locale effac&eacute;',
'mode_doc_spip' => 'Mode',
'mode_doc_spip_document' => 'Mode : Document',
'mode_doc_spip_image' => 'Mode : Image',
'modif_serv' => 'Modifier ce Serveur',
'mot_passe' => 'Mot-de-Passe',
'mode_ligne' => 'Mode ligne',
'mode_vignette' => 'Mode vignette',
'modifier' => 'Modifier',
'modif_assoc_vers_serv' => 'Modifer l\'association vers ce Serveur .. ',
'modif_fich_trt' => 'MODIFICATION de la Fiche',
'modif_nom_categ' => 'Modifier le Nom de la Cat&eacute;gorie',
'modif_titre_descriptif' => 'Modifier Titre et Descriptif',
'moyenne_jour' => 'Moyenne/j',
'moyenne_jours' => 'Moyenne jour',


// N
'nbre_docs' => 'Nbre Docs',
'nbr_docs_nbr_telech' => '[ @nligne@ ] Documents, pour [ @tt_compt@ ] t&eacute;l&eacute;chargements.',
'nom_fiche' => 'Nom de la fiche',
'non' => 'non',
'nouveau_nom' => 'Nouveau Nom',
'nouveau_serveur' => 'Nouveau Serveur',
'numero' => 'Num&eacute;ro',
'num_dblpt' => 'N&deg; : ',


// O
'oui' => 'oui',
'outils' => 'Outils',
'ouvr_page_import' => 'Ouvrir Page Import de ce Serveur',
'ouvrir_serv' => 'Ouvrir le serveur : ',


// P
'pas_dans_spip' => 'Doc inexistant dans SPIP',
'presente_detail_doc' => 'Fichier, taille, titre et descriptif',
'periode_date_debut' => 'Jour &agrave; afficher (d&eacute;but p&eacute;riode) :',
'periode_date_fin' => '... Fin p&eacute;riode :',
'premiere_date_stats_site' => 'Les Statistiques DW2 commencent le @prem_date@',


// R
'recherche_art_de_rub' => 'Rechercher les articles de cette rubrique',
'rejete_doc_dw2' => 'Ignor&eacute;',
'repertoire' => 'R&eacute;pertoire',
'repertoire_s' => 'R&eacute;pertoires',
'repertoire_nombre' => '@nb_k@ R&eacute;pertoire',
'repertoire_nombre_s' => '@nb_k@ R&eacute;pertoires',
'repert_hote' => 'R&eacute;pertoire H&ocirc;te',
'repert_img' => 'R&eacute;pertoire IMG',
'repert_taille' => 'Taille du R&eacute;pertoire : ',
'repert_save_existe_pas' => 'Le r&eacute;pertoire @rep_bases@ n\'existe pas, corrigez ce param&egrave;tre !',
'repert_stock_save' => 'R&eacute;pertoire de stockage :',
'repert_vide' => 'R&eacute;pertoire vide !',
'rest_dependance_aucune' => 'Aucune d&eacute;pendance',
'rest_dependance_detail' => '@maitre_p@ - @titre_maitre_p@<br />Restriction : ',
'rest_dependance_direct_sup' => 'D&eacute;pendance niveau sup&eacute;rieur :',
'rest_des_secteurs' => 'Restriction des Secteurs',
'rest_etat_tous_secteurs' => 'Tous les secteurs sont restreint pour : ',
'rest_etat_table_restrict' => 'Etat de la table des Restrictions',
'rest_page_hierarchie' => 'Hi&eacute;rarchie Restrictions',
'rest_page_table' => 'Etat Table Restriction',
'rest_telecharge_pour' => 'Restreindre le t&eacute;l&eacute;chargement de documents pour :',
'rest_titre_formulaire' => 'Restriction de t&eacute;l&eacute;chargement',
'rest_tous_secteur_0' => 'Tous les Secteurs sont non-restreint (Personne).<br />
							Vous pouvez faire le m&eacute;nage dans la table !',
'restreint' => 'Hi&eacute;rarchie Restreint',
'restreint_val_0' => 'Personne',
'restreint_val_1' => 'Visiteur',
'restreint_val_2' => 'R&eacute;dacteur',
'restreint_val_3' => 'Administrateur',
'resultat_sauvegarde' => 'R&eacute;sultat de la Sauvegarde',
'retour_fiche_catalogue' => 'R&eacute;tablir cette Fiche-document dans le catalogue actif',
'rub_sect' => 'Rubrique-Secteur',
'rubrique' => 'rubrique',
'rub_sans_art' => '--- Rubrique sans article ---',


// S
'saisir_nouv_serv_ftp' => 'Saisir un nouveau Serveur FTP ... ',
'sauvegarde' => 'Sauvegarde',
'sauvegarde_tables_dw' => 'Sauvegarde des tables DW2',
'sauvegardes_obsolete_delais' => 'Sauvegardes obsol&egrave;tes apr&egrave;s :',
'sauvegarde_effectuee' => 'Sauvegarde de DW2 effectu&eacute;e avec succ&egrave;s.',
'secteur' => 'secteur',
'select_autre_serv' => 'S&eacute;lectionnez un autre serveur',
'select_doc' => 'S&eacute;lectionnez un des documents :',
'selection' => 'S&eacute;lection',
'serveur' => 'Serveur',
'serv_hote_chemin' => 'Serveur : H&ocirc;te / Chemin',
'serv_info_chemdist' => '( sous/repertoire/ )',
'serv_info_hote' => '( ftp.monsite.net )',
'serv_info_hostdir' => '( Optionnel : /web/ )',
'serv_info_port' => '(Port par d&eacute;faut : 21)',
'serv_info_sitedist' => '( http://www.monsite.net )',
'serv_sans_fich' => ' ne contient aucun fichier',
'signature' => '<b>DW2 v. @version@</b><br />
		Ces pages sont destin&eacute;es au suivi des Documents
		que vous proposez en t&eacute;l&eacute;chargement sur ce site.<br /><br />
		Par Scoty, <a href=\'http://www.koakidi.com\'>koakidi.com</a><br />
		(12/2004 - 02/2007).',
'soumettre' => 'Soumettre',
'statistiques' => 'Stastistiques',
'statistiques_auteurs' => 'Stats Inscrits',
'stats_generales_titre' => 'Statistiques G&eacute;n&eacute;rales',
'stats_periode_titre' => 'Statistiques P&eacute;riode',
'stats_abonnes_les_docs' => 'Statistiques Inscrits - Les documents',
'stats_abonnes_les_visiteurs' => 'Statistiques Inscrits - Les visiteurs',
'stats_abonnes_personne' => 'Personne !',
'stats_date_jour' => 'Statistiques du @deuz_date@',
'stats_date_prem_deux' => 'Statistiques du @premiere_date@ au @deuz_date@',
'stats_de_periode' => 'Stats de p&eacute;riode',
'stats_des_docs' => 'Stats des documents',
'stats_des_visiteurs' => 'Stats des visiteurs',
'stats_du_jour' => 'Stats du jour',
'stats_info_totaux' => '@tot_auteur@ Visiteurs Inscrits pour @tot_telech@ t&eacute;l&eacute;chargements
						de @tot_fichier@ fichiers.',
'statut_non_publie' => 'non publi&eacute;', // sens : article status not "publie"
'suite' => 'suite >>>', //bouton
'supprimer_doc_a_date' => 'Documents enregistr&eacute;s &agrave; compter du :',
'supprimer_doc_du_catalogue' => 'Supprimer documents du Catalogue',
'supprimer_doc_du_catalogue_info' => 'Supprimer les documents du Catalogue DW2, selon les crit&egrave;res suivants :',


// T
'taille' => 'Taille',
'taille_fichier_avant' => 'La taille du fichier &agrave; &eacute;t&eacute; modifi&eacute;e,<br />avant : ',
'telechargements' => 't&eacute;l&eacute;chargements',
'telech_du_jour' => 'T&eacute;l&eacute;chargements du jour',
'telech_du_jour_nombre' => 'T&eacute;l&eacute;chargements du jour : @nbr_tt@',
'telech_jour_par' => 'T&eacute;l&eacute;chargements du jour par @nb_visit@ visiteur',
'telech_jour_par_s' => 'T&eacute;l&eacute;chargements du jour par @nb_visit@ visiteurs',
'telech_fichier' => 't&eacute;l&eacute;charger ce fichier',
'type_spip_extension' => 'Type extension',
'titre' => 'Titre',
'titre_page_admin' => 'DW2 - Suivi des t&eacute;l&eacute;chargements',
'titre_page_deloc' => 'DW2 - Documents D&eacute;localis&eacute;s',
'titre_page_outils' => 'DW2 - Outils',
'title_aide_01' => 'Installation et Fonctions',
'top' => 'Haut de page',
'total_compteurs' => 'Total Compteurs',
'total_doc_pour_ajout' => '@nbr_lignes_tableau@ documents montr&eacute;s sur @total@ propos&eacute;s',
'total_type' => 'Total par type',
'totaux_grand' => 'Total compteur',
'totaux_periode' => 'P&eacute;riode*',
'totaux_periode_info' => '*P&eacute;riode : Nombre de t&eacute;l&eacute;chargement
						du fichier pour la p&eacute;riode.',
'tous' => 'Tous',
'tout_le_catalogue' => 'Tout le catalogue',
'txt_ajout_rien' => 'Aucun Document &agrave; inscrire au Catalogue,<br />
					selon vos crit&egrave;res de configuration :',
'txt_ajout_01' => '@nbr_doc_enreg@ Document ajout&eacute;, voir le Catalogue',
'txt_ajout_01_s' => '@nbr_doc_enreg@ Documents ajout&eacute;s, voir le Catalogue',
'txt_ajout_03' => 'Par d&eacute;faut, DW2 prend :<br />le nom de fichier du Document, comme "Nom" de Fiche,<br />
				le Titre du <b>@type_categorie@</b> d\'appartenance, comme intitul&eacute; de "Cat&eacute;gorie".<br />
				Vous pourrez modifier ces derniers ult&eacute;rieurement.<br />Voir l\'Aide.',
'txt_ajout_05' => 'Ajouter tous ces Documents',
'txt_ajout_titre_page' => 'Ajout de documents au Catalogue de DW2',
// this 2 text-sequences are not use but ... maybe !
'txt_anc_version1' => 'Vous venez d\'installer une ancienne version de DW2 : v.',
'txt_anc_version2' => 'Voyez l\'Aide, car la version du fichier de configuration est',
'txt_cat_aucun' => 'Aucun Document n\'est enregistr&eacute; dans le catalogue !',
'txt_cat_trt' => 'CATALOGUE DES DOCUMENTS',
'txt_categ_trt' => 'STATISTIQUES DES CATEGORIES',
'txt_categ_01' => 'Pour tous les Documents concern&eacute;s !',
'txt_dd_intro_gauche' => 'Ce module de DW2 permet...<br /><br />
		<b>l\'Import virtuel</b> : faire appara&icirc;tre dans SPIP (et DW2)
		un Document dont le fichier est stock&eacute; sur un de vos Serveurs distants ;<br /><br />
		<b>l\'Export</b> : d&eacute;placer un Fichier pour le stocker sur un serveur distant.<br />
		Le Fichier &eacute;tant d&eacute;j&agrave; r&eacute;pertori&eacute; par DW2.',
'txt_defico_import_1' => 'Cliquez pour importer.',
'txt_defico_import_2' => 'DW2 ne g&egrave;re pas les fichiers .jpg / .gif / .png.',
'txt_defico_import_3' => 'Extension inconnue de SPIP.',
'txt_defico_import_4' => 'Association du fichier dans le Catalogue DW2, ( doublons ) :',
'txt_defico_import_5' => 'Associ&eacute; &agrave; autre Serveur. Clic pour voir et modifier l\'association.',
'txt_defico_import_6' => 'Associ&eacute; &agrave; cet H&ocirc;te, autre R&eacute;pertoire. Clic pour voir et modifier l\'association.',
'txt_defico_import_7' => 'Associ&eacute; en Local. Clic pour voir et modifier l\'association.',
'txt_defico_import_8' => 'ici ! ',
'txt_defico_import_9' => 'Un fichier portant ce nom est indiqu&eacute; en Archive.',
'txt_defico_import_10' => 'Un fichier portant ce nom est indiqu&eacute; comme \'Distant\' dans Spip.',
'txt_deplace_doc' => 'Inscrivez dans le champ le num&eacute;ro de l\'Article ou de la Rubrique
					de destination du Document puis validez.<br />
					Cette action va modifer les tables de SPIP :<br />
					- Supprimer le document de l\'article(rubrique) d\'origine.<br />
					- Lier le document au nouvel article(rubrique).<br />
					Dw2 tiendra compte, bien-s&ucirc;r, de cette modification.',
'txt_duplic_serv' => 'L\'objectif de cette page est de vous permettre de saisir un nouveau
			r&eacute;pertoire d\'Import/Export pour cet H&ocirc;te.<br />
			Seul le champ "R&eacute;pertoires", est disponible en saisie.<br />
			Les contr&ocirc;les de saisie et le test de connexion se feront dans les m&ecirc;mes
			conditions que pour une cr&eacute;ation de Serveur',
'txt_info_vignette' => 'Cette ic&ocirc;ne indique l\'existence d\'une miniature associ&eacute;e.<br />
						Type \' -s \' calcul&eacute;e par SPIP < 1.8.<br />OU<br />
						Vignette personnalis&eacute;e.',
'txt_install_01' => 'Configuration de DW2 - @vers_loc@',
'txt_install_02' => 'Anti-triche : ',
'txt_install_03' => 'Nombre de lignes des tableaux : ',
'txt_install_04' => 'Format des logos de Serveurs : ',
'txt_install_05' => 'Fixer le format des logos de Serveurs.<br />
					(Voir Docs D&eacute;localis&eacute;s)',  // see : 'acc_dw2_dd'
'txt_install_07' => 'Titre Cat&eacute;gorie bas&eacute; sur :',
'txt_install_08' => 'Fixer l\'origine des Titres de Cat&eacute;gorie.<br />
					Secteur : rubrique racine de spip.<br />
					Rubrique : la rubrique-parent du document.',
'txt_install_09' => 'Installation de DW2 - @vers_loc@',
'txt_install_10' => 'Mise &agrave; jour des tables .. fait !',
'txt_install_11' => 'Activer le mode Anti-triche = un clic compt&eacute; par IP pour 24 heures.<br />
					(Evites la comptabilisation de clics redondants du visiteur,
					pour cette p&eacute;riode !).',
'txt_install_14' => 'Indiquer le nombre de lignes affich&eacute;es dans les tableaux du Catalogue,
					des statistiques, catalogue images ...',
'txt_install_15' => 'Pour finir l\'installation, validez ce formulaire ...',
'txt_install_16' => 'Cr&eacute;ation des tables dans la base .. fait !',
'txt_install_17' => 'Mode d\'enregistrement des nouveaux Documents : ',
'txt_install_18' => 'Si Mode "Auto", afficher les derni&egrave;res entr&eacute;es (en jours) : ',
'txt_install_19' => 'Mode d\'enregistrement des nouveaux Documents dans le Catalogue :<br />
					"Manuel", vous validez l\'enregistrement depuis la fonction "ajouter documents" de DW2.<br />
					"Auto", SPIP d&eacute;clenche l\'enregistrement automatiquement.',
'txt_install_21' => 'En mode "Auto", DW2 affichera sur sa page d\'Accueil, les derniers enregistrements
					de documents dans son Catalogue, depuis ce nombre de jours.',
'txt_install_22' => 'Par d&eacute;faut, DW2 pr&eacute;sente le catalogue des images :<br />
					- Ligne : tableau des noms de fichier image.<br />
					- Vignette : tableau des miniatures de fichier image.<br />
					(voir page "ajout images 1/1" [Ajouts Documents])',
'txt_install_23' => 'Catalogue Images, afficher :',
'txt_install_26' => 'Squelette du Catalogue Public :',
'txt_install_27' => 'Saisissez le nom du squelette "Catalogue Public", sans l\'extension (.html).<br />
					(voir la documentation)',
'txt_install_28' => 'Mode restreint :',
'txt_install_29' => 'Ce mode permet de restreindre (zone public) le t&eacute;l&eacute;chargement des documents
					aux visiteurs, r&eacute;dacteurs ou administrateurs du site.<br />
					(voir la documentation)',
'txt_install_30' => 'Inclure les documents (jpg, png, gif) : ',
'txt_install_31' => 'Forcer DW2 &agrave; enregistrer dans son Catalogue, les Documents images
					(spip, mode : document) de type jpg, png, gif.<br />
					Le mode d\'enregistrements des documents doit &ecirc;tre valid&eacute; en "Auto".',
'txt_install_32' => 'Forcer URL DOCUMENT : ',#n2.13
'txt_install_33' => 'Cette option permet d\'intercepter les balises #URL_DOCUMENT de vos squelettes
					et forcer celles-ci &agrave; produire un lien du type "spip.php?action=dw2_out&id=xxx".
					La balise #URL_DOC_OUT, propre &agrave; DW2, reste toujours fonctionnelle.',#n2.13
'txt_modif_01' => 'Pour corriger le nom de la fiche, la cat&eacute;gorie<br />
					ou la valeur du compteur : pr&eacute;-incr&eacute;mentation',
'txt_nouv_serveur' => 'Avant d\'effectuer une nouvelle saisie,<br />
						consultez votre administrateur r&eacute;seau ...<br />
						ou les param&egrave;tres transmis par l\'h&eacute;bergeur du serveur cible.',
'types_fich_cat' => 'TYPES de FICHIERS du CATALOGUE',


// U
'url_site' => 'URL Site',


// V
'valider' => 'valider',
'validez' => 'Validez ...',
'vers_serveur' => 'Vers le serveur : ',
'visiteurs_abonnes' => 'Visiteurs Inscrits',
'voir' => 'voir', // for this word .. make short : 4 or 5 letters or less
'voir_article' => 'Voir cet article',
'voir_breve' => 'Voir cette br&egrave;ve',
'voir_fiche' => 'Voir la Fiche',
'voir_rubrique' => 'Voir cette rubrique',
'vos_doc' => 'Vos documents',

// Z, it's good to be the last
'z' => 'z'

);
?>
