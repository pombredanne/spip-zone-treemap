DW2
(appel� par les intimes 'Didubyatoo')
Ca ne veut toujours rien dire, mais c'est pas grave.

---------------------------------------------------------
Consignes d'installation de DW2 version 2.14 pour spip 1.9.2
---------------------------------------------------------
du 9/03/2007


Attention, cette version reste encore EN TEST !!

-------------------------------------------------------------
version 2.14 ?
Cette version est la 2.13(1.9.1) port�e pour spip 1.9.2
Et ne re�oit aucune nouveaut�.
Hormis : fichier .sql de sauvegarde des tables DW2 passe de ecrire/data/ � tmp/dump/ !

Pour la 2.13 les nouv. �taient :
1 - Les tables DW2 sont d�sormais pr�fix�es en : 'spip_' (contrib. de Skedus),
	permet assujetissement � la global $table_prefix .. (ex. 1 BDD -> 2 SPIPs)
2 - Suppression du fichier de config (dw2_config.php en racine du plugin),
	la configuration passe dans une nouvelle table spip_dw2_config.
	2.13 peut simuler la r�cup�ration du fichier de config d'une ancienne version
	pour ceux qui l'aurait malencontreusement �cras� !
3 - Mode "Restreint" .. Restrictions du t�l�chargement des documents :
	. sur tout le site, sur rubrique, article et/ou document.
4 - Compl�ment au statistiques : recherche sur p�riode.
5 - Nouvelle Balise #DW2_ALERTE permet d'afficher 3 messages d'erreurs/alertes :
	document inexistant, s'enregistrer et acc�s refus� (mode "restreint").
et + ...
-------------------------------------------------------------


Passer de DW2 2.13 � 2.14 
-------------------------------------------------------------
Ecraser plugins/dw2/ par le contenu de cette archive !


Passer de DW2 2.1x � 2.14
-------------------------------------------------------------
1. Dans Spip, sur la page de "Gestion des Plugins", d�sactiver DW2 (*)

2. Supprimer int�gralement tous fichiers DW2 d'une pr�c�dente version :
	- repertoire dw2/ du repert. plugins/
	Si pr�c�dente version : 2.1 beta x ... :
	- "dw2_out.php" ou "dw_out.php" en racine de votre site.
	- dw2_catalogue.php et dw2_cat_style.css (en racine ou votre dossier squelettes)

	Conserver (pour la derni�re fois) le fichier de param�tre : dw2_config.php
	que vous placerez � nouveau dans plugins/dw2/ � la phase suivante !!!

3. extraire de l'archive le repertoire du plugin ( dw2/ ), � placer dans : ../plugins/ !!!
	Replacer le fichier dw2_config.php � la racine de DW2.

4. Dans le backoffice, r�activer le plugin, dans la "gestion des plugins".

5. Cliquer sur l�icone de DW2 qui apparait dans le sous-menu "statistiques" de SPIP.

6. DW2 d�tecte une ancienne version (d'o� l'utilit� de conserver dw2_config.php, pour la derni�re fois)
   et pr�sente la page "installation". Valider le formulaire.

* .. il est toujours prudent - voir n�cessaire - de d�sactiver un plugin lors d'une mise � jour,
	afin que SPIP r��crive les fichiers du "pipeline", en prennant compte d'�ventuelles nouveaut�es !



Passer de DW2 2.0x � 2.14
-------------------------------------------------------------
1. Supprimer tous fichiers DW2 d�une version ant�rieur, dans ecrire/ (dw2_qqchose.php) 
   + ecrire/lang/dw_en et dw_fr.php ;
   + (en racine ou dossier squelettes) => dw2_out.php, dw2_catalogue.php et dw2_cat_style.css
   sauf : ecrire/dw2_config.php,
   que vous devez conserver (pour la derni�re fois) ; voir le point 3.

   ET supprimer tous filtres, fonctions (DW2) qui �tait dans mes_fonctions.php et mes_otpions.php !!

2. Extraire de l�archive le r�pertoire "dw2" et placer ce dernier dans plugins/

3. D�placer ecrire/dw2_config.php vers la racine du plugins (soit : ../plugins/dw2/)

4. Dans le backoffice de SPIP, activer le plugin DW2.

5. Cliquer sur l�icone de DW2 qui apparait dans le sous-menu "statistiques" de SPIP.

6. DW2 d�tecte une ancienne version (d'o� l'utilit� de conserver dw2_config.php)
   et pr�sente la page "installation". Valider le formulaire.

  
Premi�re installation de DW2
-------------------------------------------------------------
1. Extraire de l�archive le r�pertoire "dw2" et placer ce dernier dans plugins/ : ../plugins/dw2/

2. Dans le backoffice de SPIP, activer le plugin DW2.

3. Cliquer sur l�icone de DW2 qui apparait dans le sous-menu "statistiques" de SPIP.

4. Valider le formulaire de configuration.
	... avant de poser vos questions, lisez l'Aide en ligne !
	Le lien est rappel� sur toutes les pages de DW2.




-------------------------------------------------------------
POUR TOUS ..............
-------------------------------------------------------------

	Votre fichier HTML de "catalogue public" (pour les anciens utilisateurs versions < 2.1x) est � modifier, 
	�videment, puisque dw2_catalogue.php n'existe plus !
	Remplacez l'"<INCLURE>" par l'exemple de Boucle propos� dans le fichier : modele_catalogue_dw2.html.
	Modifiez, testez � votre guise cette pr�sentation du catalogue.
	Pour l'usage des Balises disponible, vous pouvez vous reporter � l'Aide en ligne,
	chapitre Balises.
	Cet exemple fait appel aux Balises du plugin : DEREBLOC. A t�l�charger sur KOAKIDI.com.




-------------------------------------------------------------
Nouveaut�s 2.1x ...
-------------------------------------------------------------
2.13/2.14 ..............................
voir en t�te du fichier.

2.12 ..............................
C'�tait juste une mise � jour pour spip1.9.1

2.11 ..............................
Fini le catalogue public en php ...
Voir ci-dessus ...

- TOUT est dans le plugin !

- possibilit� de "forcer" la mise en archive d'une fiche/document.
Ainsi le document n'apparait plus dans le catalogue de DW2 (public et priv�)
Et bien-s�r on peut � tout moment inverser le statut et rendre � nouveau "actif" le document.
Cette action ne "supprime" pas les documents dans leurs articles respectif, mais rend inactif le compteur.

- Nouveau bouton : OUTILS
Il rassemble dans le m�me popup :
	Changer statut de fiche-Document en masse. (nouveau).
	Lister le r�pertoire IMG/
	Page Titre&Documents (pour modifier Titre et Descriptif).

- Avis de mise-�-jour DW2.
Option param�trable dans Configuration (oui/non), pour se tenir au courant des �volutions de DW2.
.... details :
Le script charge depuis le serveur koakidi.com un fichier xml.
Ce dernier lui fournis les infos de la derni�re version en cours d'�laboration/stabilis�e.
Cette requete est faites uniquement par la page d'accueil de DW2, et y affiche les infos recuillis.

- Mise � jour globale du module "Import/Export". Diverses am�lioration/simplification dans le traitement.
	M�rite certainement plus. A suivre !


Et voil� !

D�avance, merci pour vos retour d'usage !


SCOTY


-------------------------------------------------------------
Contact
-------------------------------------------------------------
Pour toutes question concernant ce plugin
ou si vous avez des id�es, suggestions, traductions ...,
n'h�sitez pas � me contacter.

Forum koakidi.com : http://www.koakidi.com/spip.php?rubrique89
ou mieux ... 
mail : scoty@koakidi.com

