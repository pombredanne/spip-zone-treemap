<?php
/*
+--------------------------------------------+
| DW2 2.14 (03/2007) - SPIP 1.9.2
+--------------------------------------------+
| H. AROUX . Scoty . koakidi.com
| Script certifi� KOAK2.0 strict, mais si !
+--------------------------------------------+
| fonctions : 
| .. ajout de documents 
| .. origine
| .. enreg auto des docs
+--------------------------------------------+
*/

//
// retourne la Cat�gorie d'un doc selon choix config
function select_categorie_doc($rub_select) {
	$quer_cat=sql_select("titre","spip_rubriques","id_rubrique = $rub_select");
	while ($row_cat=sql_fetch($quer_cat)) {
		if(!function_exists('typo')) { include_spip("inc/texte"); }
		$categorie=typo(supprimer_numero($row_cat['titre']));
	}
	return $categorie;
}


//
// Retourne tableau du proprio 
function origine_doc($id_doc) {
	$requete=sql_select("sd.id_document, sl.objet, sl.id_objet ",
						"spip_documents AS sd, spip_documents_liens AS sl",
						"sd.id_document=sl.id_document AND sd.id_document = $id_doc");
	if (sql_count($requete)) {
		$lg=sql_fetch($requete);
		$iddoctype=$lg['id_objet'];
		$doctype=$lg['objet'];
		$statut='1'; // a priori plus g�r� en 2.0 ? a verifier
	}
	return $origine_doc=array($doctype,$iddoctype,$statut);
}


//
// Ajout des Documents dans le Catalogue de DW2
function ajout_doc_catalogue($doc,$typecat='', $retour='') {	
	
	// si hors appel calc_inclus_auto_doc :
	if(!$typecat) {
		$typecat = $GLOBALS['dw2_param']['type_categorie'];
	}
	
	// origine du doc 
	// on l'a repasse ici because : cal_inclus_auto_doc
	$origine=origine_doc($doc);
	$doctype=$origine[0];
	$iddoctype=$origine[1];

	// si en statut 'publie' OK .. on enregistre
	if($origine[2]=='1') {
		$resul=sql_select("id_document, fichier, distant","spip_documents","id_document=$doc");
		$ro=sql_fetch($resul);
		$distant=$ro['distant'];
		$id_doc=$ro['id_document'];
		
		if($distant=='oui') {
			$url=$ro['fichier'];
			$heberge='distant';
		} else {
			$url='/'.$ro['fichier'];
		}
		
		$nomfichier=substr(strrchr($url,'/'), 1);
			
		// trouver categorie
		$rs_cat=sql_select("id_secteur, id_rubrique","spip_".$doctype."s","id_".$doctype."=".$iddoctype);
		$ro_cat=sql_fetch($rs_cat);
		$idsect=$ro_cat['id_secteur'];
		$idrub=$ro_cat['id_rubrique'];

		if ($typecat=="secteur")
			{ $class_cat=$idsect; }
		else
			{ $class_cat=$idrub; }
			
		// enregistre le Doc
		//sql_query("INSERT INTO spip_dw2_doc (id_document, nom, url, total, dateur, doctype, id_doctype, categorie, date_crea) 
		//	VALUES('$id_doc','$nomfichier','$url','0','','$doctype','$iddoctype','".select_categorie_doc($class_cat)."',NOW())");

		sql_insertq("spip_dw2_doc", array(
								'id_document' => $id_doc, 
								'nom' => $nomfichier, 
								'url' => $url,
								//'total' => '0',
								//'dateur' => '',
								'doctype' => $doctype,
								'id_doctype' => $iddoctype,
								'categorie' => select_categorie_doc($class_cat),
								'heberge' => $herberge ? $heberge : 'local',
								'date_crea' => "NOW()",
								) );
	}
	/* dans linsertq
	if($heberge) {
		//sql_query("UPDATE spip_dw2_doc SET heberge='$heberge' WHERE id_document = $id_doc");
	}
	*/	
	if ($retour=="oui") { return $nomfichier; }
}


//
// Enreg. des docs spip vers dw2 en auto
// appel depuis dw2_mesoptions (a chaque hit backoffice !)
# rev. h.02/02/07 ( ++ criteres d'enregistrement des docs)
function calc_inclus_auto_doc($arg='',$typecat) {
	if($arg=='') {
		$where="AND sd.extension NOT IN ('jpg','png','gif') ";
	}
	else {
		$crit=explode(',',$arg);
		if(count($crit)==1) {
			$where="AND (sd.extension NOT IN ('jpg','png','gif') OR sd.extension = '$arg')";
		}
		elseif(count($crit)==2) {
			$where="AND (sd.extension NOT IN ('jpg','png','gif') OR sd.extension IN ('$crit[0]','$crit[1]'))";
		}
		else {
			$where="";
		}
	}
	$result=sql_select("sd.id_document",
						"spip_documents sd LEFT JOIN spip_dw2_doc dw ON sd.id_document = dw.id_document ",
						"sd.mode = 'document' $where AND dw.id_document IS NULL");

	if(sql_count($result)) {
		while ($row=sql_fetch($result)) {
			$doc=$row['id_document'];
			ajout_doc_catalogue($doc,$typecat);
		}
	}
}

?>