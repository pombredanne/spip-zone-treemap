<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/_stable_/noie/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

'avatar_info' => 'Para ense&ntilde;ar su avatar de Jabber aqu&iacute;, a&ntilde;ada un bot Omnipresence (por ejemplo <a href="xmpp:presence.jabberfr.org">el JID presence.jabberfr.org</a> en su lista de contactos.',
'titre_avatar_auteur' => 'AVATAR JABBER DEL AUTOR O AUTORA',
'serveur_omnipresence_nom_champ' => 'Servidor Omnipr&eacute;sence',
'serveur_omnipresence_precisions' => 'Si a&ntilde;adieron un servidor Omnipr&eacute;sence a su lista de contactos, indique su direcci&oacute;n HTTP aqu&iacute;.',
'serveur_omnipresence_defaut' => 'Servidor Omnipr&eacute;sence por omisi&oacute;n',
'serveur_omnipresence_defaut_explication' => 'Indique aqu&iacute; la direcci&oacute;n HTTP del servidor Omnipr&eacute;sence que hay que usar para los usuarios que no tienen ninguno configurado en sus preferencias. Si no pone nada, se usar&aacute; <em>@defaut@</em>.',
);

?>
