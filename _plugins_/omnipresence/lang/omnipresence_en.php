<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/_stable_/noie/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

'avatar_info' => 'In order to display your Jabber avatar here, please add an Omnipresence bot (e.g. <a href="xmpp:presence.jabberfr.org">JID \'presence.jabberfr.org\'</a> to your buddy list.',
'titre_avatar_auteur' => 'AUTHOR\'s JABBER AVATAR',
'serveur_omnipresence_nom_champ' => 'Omnipresence server',
'serveur_omnipresence_precisions' => 'If an Omnipresence server is subscribed to your presence, please give its HTTP URL here.',
'serveur_omnipresence_defaut' => 'Default Omnipresence server',
'serveur_omnipresence_defaut_explication' => 'Please give below the HTTP URL of the Omnipresence server that will be used for users who didn\'t give one in their personal preferences. The default value is <em>@defaut@</em>.',
);

?>
