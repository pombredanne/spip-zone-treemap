<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/_stable_/noie/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

'avatar_info' => 'Pour afficher votre avatar Jabber ici, ajoutez un bot Omnipresence (par exemple <a href="xmpp:presence.jabberfr.org">le JID presence.jabberfr.org</a> à vos contacts.',
'titre_avatar_auteur' => 'AVATAR JABBER DE L\'AUTEUR',
'serveur_omnipresence_nom_champ' => 'Serveur Omnipr&eacute;sence',
'serveur_omnipresence_precisions' => 'Si vous avez ajout&eacute; un serveur Omnipr&eacute;sence &agrave; vos contacts, indiquez son adresse HTTP ici.',
'serveur_omnipresence_defaut' => 'Serveur Omnipr&eacute;sence par d&eacute;faut',
'serveur_omnipresence_defaut_explication' => 'Indiquez ici l\'adresse HTTP du serveur Omnipr&eacute;sence utilis&eacute; pour les utilisateurs qui n\'en ont pas indiqu&eacute; dans leurs pr&eacute;f&eacute;rences. Par d&eacute;faut, c\'est <em>@defaut@</em> qui est utilis&eacute;.',
);

?>
