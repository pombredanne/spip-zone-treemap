<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'api_call_title' => 'Appel de l\'API',
'api_result_html' => 'Résultat HTML',
'api_result_xml' => 'Résultat XML',
'api_result_json' => 'Résultat JSON',

'liste_urls_exemples' => 'Quelques exemples',
'label_url'=>'URL de la page',
'bouton_tester'=>'oEmbed!',
'titre_lien_vers_cette_page' => 'Lien vers cette page !',

);


?>