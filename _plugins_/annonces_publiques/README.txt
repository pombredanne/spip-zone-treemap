README.txt
----------

Version 1.3 - 2008-12-28

-* Compatibilite SPIP 2.0.0

Version 1.2 - 2008-02-15

-* Rubriques => Calendriers

Version 1.1 - 2008-02-04

-* filtres/balises => duree, jolie_dates (credits erational et le gars cerdic)
-* Champ LIEU

Version 1.0 - 2008-02-01

-* Boucle (ANNONCES)
-* Criteres a_venir, en_cours, revolus
-* Noisette microformat hCalendar

BUGS:
-----

-* s'arrurer qu'une annonce ne sort pas des creneaux correspondant aux criteres a_venir, en_cours et revolus.

TODO:
-----

Selection annonce publique/privee
Mots-Cles => Categories
Repetitions -> RRULE, EXRULE, RDATE, EXDATE (RFC iCal)
Squelettes annonce(rdz), ical, atom et rss