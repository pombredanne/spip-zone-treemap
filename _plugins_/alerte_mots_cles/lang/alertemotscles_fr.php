<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'alertemotscles' => 'Alerte mots cl&#233;',
'cfg_onglet' => 'Alerte/Mots',

'cfg_titre' => 'Alerte Mots Cl&#233; auteur',
'cfg_texte' => '[Cf. documentation->http://www.spip-contrib.net]',

'cfg_boite_titre' => 'R&#233;glage des notifications',
'cfg_form_titre' => 'Alerte par mots cl&#233; auteur',
'cfg_form_texte' => 'Les &#233;v&#233;nements suivants peuvent d&#233;clencher une notification par email selon les mots cl&#233;s associ&#233;s.',

'article_publie' => 'Article publi&#233;.',
'breve_publie' =>'Br&#232;ve publi&#233;e.',
'site_publie' => 'Site publi&#233;.',


'notification_breve_publie_subject' =>'Br&#232;ve publi&#233;e : @titre@',
'notification_breve_publie_body' =>'La br&#232;ve "@titre@" a &#233;t&#233; publi&#233;e avec les mots cl&#233;s suivants :
 @mots@

@texte@

voir en ligne : @lien_public@'


);


?>