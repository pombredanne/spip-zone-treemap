<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'mailjet_title' => 'Mailjet - Configuration',
'info_mailjet_enabled' => 'Envoi pris en charge par Mailjet',
'label_mailjet_enabled_1' => 'Activer la prise en charge des mails par Mailjet',
'mailjet_titre_email_test' => 'Mail de test de l\'envoi par Mailjet',
'message_envoi_test' => 'Un email de test a été envoyé à @email@',
'bouton_enregistrer_et_tester' => 'Enregistrer et tester',
'label_mailjet_username' => 'Clé API :',
'label_mailjet_password' => 'Clé secrète :',
'explications_api_mailjet' => 'Les informations pour le paramétrage de Mailjet sont disponibles dans votre compte Mailjet <a href="https://fr.mailjet.com/account/api_keys">https://fr.mailjet.com/account/api_keys</a>',
'configuration_acces_interdit' => 'Vous n\'avez pas le droit d\'accèder à cette configuration',
'mj_global_settings' => 'Paramètres généraux',
'mj_error_autoconfig' => 'Impossible de determiner la configuration Host/Port<br />Erreur @no@ - @str@',
'mj_autoconfig_host_port' => 'La detection automatique a déterminé le Host @host@ et port @port@',
);


?>