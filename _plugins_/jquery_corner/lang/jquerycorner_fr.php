<?php
// Sécurité
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'configurer_explication' => 'Consultez le <a href="http://jquery.malsup.com/corner/" target="_blank">site du plugin jQuery Corner</a> pour une démonstration de ses possibilités.',
	'configurer_titre' => 'Configurer jQuery Corner',

	// E
	'explication_element' => 'Classe ou id des éléments à traiter (saisir par exemple .coinsarrondis)',
	'explication_param' => 'Paramètres à appliquer aux angles (saisir par exemple bevel pour des coins chanfreinés). Pour voir une démo des paramètres, visitez le site indiqué ci-contre.',

	// L
	'label_element' => 'Eléments',
	'label_nombre' => 'Nombre d\'éléments',
	'label_param' => 'Paramètres',
	'legend_jquerycorner_nombre' => 'Nombre d\'éléments à traiter',
	'legend_jquerycorner' => 'Paramètrage @numero@',

	// T
	'titre_menu' => 'jQuery Corner',
);
?>
