<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'ajouter_calendrier_reservation' => 'Ajouter un calendrier de r&eacute;servation',
	'ajouter_calendrier' => 'Ajouter le calendrier',
	'affichage_vertical' => 'Affichage vertical',
	'affichage_horizontal' => 'Affichage horizontal',
	'aucun_calendrier' => 'Aucun calendrier pour le moment',
	
	// C
	'configuration_plugin_resa' => 'Configuration du plugin r&eacute;sa',
	
	// F
	'form_msg_champs_obligatoires' => 'Champs obligatoires',
	'form_msg_champ_obligatoire' => 'Ce champ est obligatoire',
	'form_msg_calendrier_ajoute' => 'Le calendrier a &eacute;t&eacute; ajout&eacute; avec succ&egrave;s',
	
	// Jours
	'lundi' => 'Lundi',
	'mardi' => 'Mardi',
	'mercredi' => 'Mercredi',
	'jeudi' => 'Jeudi',
	'vendredi' => 'Vendredi',
	'samedi' => 'Samedi',
	'dimanche' => 'Dimanche',
	
	// L
	'liste_calendriers' => 'Liste des calendriers',
	'label_article' => 'Article',
	'label_voir_reservations' => 'Voir les r&eacute;servations',
	'label_supprimer' => 'Supprimer',
	
	// M
	'msg_identifiant_manquant' => 'L\'identifiant est manquant.',
	'msg_calendrier_supprime' => 'Le calendrier a &eacute;t&eacute; supprim&eacute; avec succ&egrave;s',
	'msg_descriptif_config' => 'Ici, vous pouvez configurer le calendrier qui est affich&eacute; sur les pages de r&eacute;servation.<br />Vous pouvez choisir le mois duquel doit commencer le calendrier, et le nombre de mois &agrave; afficher.',
	
	// Mois
	'mois_depart' => 'Mois de d&eacute;part :',
	'mois_a_afficher' => 'Mois &agrave; afficher :',
	'mois_courant' => 'Mois courant',
	'janvier' => 'Janvier',
	'fevrier' => 'F&eacute;vrier',
	'mars' => 'Mars',
	'avril' => 'Avril',
	'mai' => 'Mai',
	'juin' => 'Juin',
	'juillet' => 'Juillet',
	'aout' => 'Ao&ucirc;t',
	'septembre' => 'Septembre',
	'octobre' => 'Octobre',
	'novembre' => 'Novembre',
	'decembre' => 'D&eacute;cembre',
	
	// N
	'navigation' => 'Navigation',
	
	// O
	'ok' => 'Ok',
	
	// S
	'supprimer_calendrier_reservation' => 'Supprimer un calendrier de r&eacute;servation',
	'supprimer' => 'Supprimer',
	'supprimer_calendrier' => 'Supprimer le calendrier',
	
	// T
	'type_affichage_mois' => 'Type d\'affichage des mois :',
	
	// V
	'voir_reservations' => 'Voir les r&eacute;servations',
	'voir_calendrier' => 'Voir le calendrier'
) ;
