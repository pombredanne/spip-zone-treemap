<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'composition' => 'Composition',
	'compositions' => 'Compositions',
	'composition_defaut' => 'Composition par d&eacute;faut',
	'composition_heritee' => 'composition h&eacute;rit&eacute;e',
	'composition_verrouillee' => 'Cette composition est verrouill&eacute; par le webmestre',
	'composition_utilisee' => 'Utilise&nbsp;:',

	// D
	'des_utilisations' => '@nb@ utilisations',

	// H
	'heritages' => 'Cette composition d&eacute;finit des compositions par d&eacute;faut pour les objets suivants&nbsp;:',

	// L
	'label_branche_verrouillee' => 'Les compositions de cette branche sont verrouill&eacute;es.',
	'label_chemin_compositions' => 'Dossier des compositions',
	'label_chemin_compositions_details' => 'Indiquez le chemin dans lequel seront recherch&eacute;s les squelettes de composition.',
	'label_composition' => 'Type de composition',
	'label_composition_branche_lock' => 'Verrouiller la composition de tous les objets de la branche',
	'label_composition_explication' => 'Vous &ecirc;tes Webmestre, vous pouvez',
	'label_composition_lock' => 'Verrouiller la composition',
	'label_composition_rubrique' => 'Composition des rubriques',
	'label_information' => 'Information',
	'label_masquer_formulaire' => 'Masquer le formulaire',
	'label_masquer_formulaire_composition' => 'Masquer le formulaire de choix d\'une composition lorsque l\'utilisateur n\'a pas les droits de la modifier.',
	'label_pas_de_composition' => 'Aucune composition',
	'label_styliser' => 'S&eacute;lection des squelettes',
	'label_styliser_auto' => 'Ne pas utiliser la s&eacute;lection automatique. La s&eacute;lection est prise en charge par mes squelettes.',
	'label_tout_verrouiller' => 'Tout verrouiller',
	'label_toutes_verrouilles' => 'Toutes les compositions sont verrouill&eacute;es.',
	'label_verrouiller_toutes_compositions' => 'Verrouiller toutes les compositions (seuls les webmasters pourront les modifier).',

	// U
	'une_utilisation' => '1 utilisation',
);
?>