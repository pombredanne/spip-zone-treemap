<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/compositions?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'composition' => 'Composición',
	'composition_defaut' => 'composition par défaut', # NEW
	'composition_heritee' => 'héritée', # NEW
	'composition_utilisee' => 'Composition :', # NEW
	'composition_verrouillee' => 'Esta composición esta cerrada por el webmaster',
	'compositions' => 'Composiciones',

	// D
	'des_utilisations' => '@nb@ utilizaciones',

	// H
	'heritages' => 'Cette composition définit des compositions par défaut pour les objets suivants :', # NEW

	// L
	'label_activer_composition_objets' => 'Utiliser les compositions sur les objets', # NEW
	'label_branche_verrouillee' => 'Les compositions de cette branche sont verrouillées.', # NEW
	'label_chemin_compositions' => 'Carpeta de las composiciones',
	'label_chemin_compositions_details' => 'Indicar el camino en cual buscar los esqueletos de composición.',
	'label_composition' => 'Tipo de composición',
	'label_composition_branche_lock' => 'Verrouiller la composition de tous les objets de la branche', # NEW
	'label_composition_explication' => 'Estas Webmaster, puedes',
	'label_composition_lock' => 'Cerrar la composición',
	'label_composition_rubrique' => 'Composición de las secciones',
	'label_information' => 'Information', # NEW
	'label_masquer_formulaire' => 'Masquer le formulaire', # NEW
	'label_masquer_formulaire_composition' => 'Masquer le formulaire de choix d\'une composition lorsque l\'utilisateur n\'a pas les droits de la modifier.', # NEW
	'label_pas_de_composition' => ' Ninguna composición',
	'label_styliser' => 'Selección de los esqueletos',
	'label_styliser_auto' => 'No utilizar la selección automatica. La selección esta manejada por mis esqueletos.',
	'label_tout_verrouiller' => 'Tout verrouiller', # NEW
	'label_toutes_verrouilles' => 'Toutes les compositions sont verrouillées.', # NEW
	'label_verrouiller_toutes_compositions' => 'Verrouiller toutes les compositions (seuls les webmestres pourront les modifier).', # NEW

	// U
	'une_utilisation' => '1 utilización'
);

?>
