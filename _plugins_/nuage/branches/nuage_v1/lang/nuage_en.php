<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_descr_nuage' => 'Configuration of the tags cloud plugin',
	'cfg_explication_score_min' => 'This value is used to filter the number of keywords displayed in the cloud: from 0 to 1 (0 shows all the keywords).',
	'cfg_label_score_min' => 'Minimal score',
	'cfg_titre_nuage' => 'Cloud'

);

?>
