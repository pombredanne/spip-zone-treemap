<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_descr_nuage' => 'Configuration du plugin nuage de mots-cl&eacute;s',
	'cfg_explication_score_min' => 'Cette valeur permet de filtrer le nombre de mots affich&eacute;s dans le nuage : de 0 à 1 (0 affiche tous les mots).',
	'cfg_label_score_min' => 'Score minimum',
	'cfg_titre_nuage' => 'Nuage'

);

?>
