<?php

// This is a SPIP-forum module file  --  Ceci est un fichier module de SPIP-forum
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// N
'noisette_titre_defaut' => 'Tags',
'noisette_nom_noisette' => 'Nuage',
'noisette_description' => 'Affiche un nuage des mots cl&eacute;s',
'noisette_label_afficher_titre' => 'Afficher le titre&nbsp;:',
'noisette_titre_noisette' => 'Titre&nbsp;:',
'noisette_label_niveau_titre' => 'Choisir le niveau du titre',

);

?>
