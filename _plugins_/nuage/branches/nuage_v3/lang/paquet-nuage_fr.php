<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-nuage
// Langue: fr
// Date: 21-11-2011 14:58:26
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// N
	'nuage_description' => 'Ce plugin permet d’afficher les mots clés en vrac, en faisant varier la taille de la police en fonction du nombre d\'articles associés au mot-clé. En d\'autres termes, spip permet de créer des « tags » !

	Plusieurs modèles disponibles :
-* article_nuage : affiche les mots clés de l\'article la taille variant en fonction de la fréquence des mots dans les articles du site
-* rubrique_nuage : affiche les mots clés des articles de la branche, la taille variant en fonction de la fréquence des mots dans les articles de cette branche
-* nuage_popularite : affiche les mots clés du site (ou d\'un groupe), la taille variant en fonction de sa popularité relative (somme des popularité des articles ayant ce mot)',
	'nuage_slogan' => 'Afficher les mots-clés en faisant varier la taille de la police',
);
?>