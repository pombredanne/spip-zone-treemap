<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'explications_page_dafill' => 'Page en cours de d&eacute;veloppement',
	
	//S
	'selectionnez_theme' => 'Si vous voulez, vous pouvez choisir un nouveau th&egrave;me pour DayFill : ',
	
	// T
	'titre_page_dayfill' => 'D&eacute;file - Gestionnaire de temps',
	'theme_actuel' => 'Th&egrave;me actuel',
	'theme' => 'Th&egrave;me',

);

?>