<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/zpip?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accueil' => 'Accueil', # NEW
	'adapte_de' => 'adapté de', # MODIF

	// C
	'commentaire' => 'comentario',
	'commentaires' => 'comentarios',
	'conception_graphique_par' => 'Diseño gráfico (c)',

	// D
	'date_forum' => 'Le @date@ à @heure@', # MODIF

	// I
	'info_1_commentaire' => '1 commentaire', # NEW
	'info_nb_commentaires' => '@nb@ commentaires', # NEW

	// L
	'lire_la_suite' => 'Seguir leyendo',
	'lire_la_suite_de' => '',

	// P
	'pagination_next' => 'Suivant »', # MODIF
	'pagination_pages' => 'Pages', # NEW
	'pagination_prev' => '« Précédent', # MODIF
	'personaliser_nav' => 'Personnaliser ce menu', # NEW

	// S
	'sous_licence' => 'bajo Licencia',

	// Z
	'zapl_loading' => 'Chargement en cours...', # NEW
	'zapl_reload_off' => 'Cliquer ici si la page reste incomplète (ou activer le javascript dans votre navigateur)' # MODIF
);

?>
