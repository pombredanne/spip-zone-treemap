<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/zpip?lang_cible=ar
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accueil' => 'Accueil',
	'adapte_de' => 'adapté de',

	// C
	'commentaire' => 'commentaire',
	'commentaires' => 'commentaires',
	'conception_graphique_par' => 'Habillage visuel ©',

	// D
	'date_forum' => 'Le @date@ à @heure@',

	// I
	'info_1_commentaire' => '1 commentaire', # NEW
	'info_nb_commentaires' => '@nb@ commentaires', # NEW

	// L
	'lire_la_suite' => 'Lire la suite',
	'lire_la_suite_de' => ' de ',

	// P
	'pagination_next' => 'Suivant »',
	'pagination_pages' => 'Pages',
	'pagination_prev' => '« Précédent',
	'personaliser_nav' => 'Personnaliser ce menu',

	// S
	'sous_licence' => 'sous Licence',

	// Z
	'zapl_loading' => 'Chargement en cours...',
	'zapl_reload_off' => 'Cliquer ici si la page reste incomplète (ou activer le javascript dans votre navigateur)'
);

?>
