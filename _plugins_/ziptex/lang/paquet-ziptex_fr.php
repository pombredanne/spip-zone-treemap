<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-ziptex
// Langue: fr
// Date: 08-01-2012 19:32:07
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// Z
	'ziptex_description' => 'Il s\'agit d\'une API permettant de regrouper en ZIP le résultat d\'une série de squelettes visant à produire à des fichiers .tex.',
	'ziptex_slogan' => 'Empaqueter vos .tex !',
);
?>