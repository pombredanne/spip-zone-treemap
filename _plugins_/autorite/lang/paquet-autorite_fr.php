<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/autorite/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'autorite_description' => 'Ce plugin permet de configurer des autorisations différentes de celles par défaut.',
	'autorite_nom' => 'Autorité',
	'autorite_slogan' => 'Quelques réglages d\'autorisations (ajoutez les vôtres !)'
);

?>
