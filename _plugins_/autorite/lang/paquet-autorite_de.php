<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-autorite?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'autorite_description' => 'Dieses Plugin ermöglicht es Ihnen, unterschiedliche Berechtigungen als die Standard-Konfiguration.',
	'autorite_nom' => 'Autorität',
	'autorite_slogan' => 'Feineinstellungen der Benutzerrechte (füge eigene hinzu!)'
);

?>
