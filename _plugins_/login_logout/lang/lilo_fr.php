<?php

	// lang/lilo_fr.php
	
	// $LastChangedRevision$
	// $LastChangedBy$
	// $LastChangedDate$

$GLOBALS['i18n_'._LILO_PREFIX.'_fr'] = array(

	'lilo_nom' => "Login-Logout"
	
, 'configuration_login_logout' => "Configuration Login-Logout"
, 'configuration_login_logout_desc' => "
	Les modifications apportées ici sont activées immédiatement.
	Il est inutile de vider le cache pour valider la configuration, sauf à l'installation du plugin 
	et à l'insertion de la balise #STATUT_PUBLIC."

, 'configurer_login_prive' => "Page login"
, 'configurer_login_prive_desc' => "La page login est appelée pour accéder à l'espace privé."

, 'configurer_login_prive_voir_logo' => "Logo auteur"
, 'configurer_login_prive_voir_logo_desc' => "Afficher le logo auteur si identifié. Si le logo n'existe pas, 
	le logo silouhette sera utilisé."

, 'configurer_login_voir_erreur' => "Message d'erreur"
, 'configurer_login_voir_erreur_desc' => "Afficher les messages d'erreur (mot-de-passe erroné, login inconnu...)."

, 'configurer_login_autocomplete' => "Désactiver l'autocomplétion"
, 'configurer_login_autocomplete_desc' => "Demander au butineur de ne pas compléter le champ login
	(attention: compatible FFX 3.0+, IE 5.0+, Safari 1.0+).
	"

, 'configurer_login_session_remember' => "Cookie de correspondance"
, 'configurer_login_session_remember_desc' => "Forcer le cookie de correspondance."

, 'configurer_statut_css_perso' => "Feuille de style (css)"
, 'configurer_statut_css_perso_desc' => "Personnaliser la feuille de style de la boite statut
	(recopiez le fichier <em>lilo_public.css</em> dans votre dossier squelettes et adaptez-le à votre site)."

, 'configurer_statut' => "Boite de statut"
, 'configurer_statut_desc' => "La boite de statut apparaît en espace public si le visiteur/auteur est authentifié."

, 'configurer_statut_sans_anim' => "Effet plier/déplier"
, 'configurer_statut_sans_anim_desc' => "Ne pas animer la boite de statut."

, 'configurer_statut_voir_logo' => "Logo auteur"
, 'configurer_statut_voir_logo_desc' => "Afficher le logo auteur dans la boite de statut."

, 'configurer_statut_voir_btn_admins' => "Boutons administrateurs"
, 'configurer_statut_voir_btn_admins_desc' => "Insérer les boutons administrateurs dans la boite de statut."

, 'configurer_statut_transparent' => "Apparence"
, 'configurer_statut_transparent_desc' => "Boite semi-transparente (75%)."

, 'configurer_statut_position' => "Position de la boite statut dans l'écran"
, 'lilo_statut_position_tl' => "En haut à gauche"
, 'lilo_statut_position_tr' => "En haut à droite"
, 'lilo_statut_position_bl' => "En bas à gauche"
, 'lilo_statut_position_br' => "En bas à droite"

, 'configurer_statut_fixed' => "Mode de positionnement"
, 'configurer_statut_fixed_desc' => "La boite reste fixe lors du défilement de la fenêtre."

, 'configurer_statut_bgcolor' => "Couleur de fond"
, 'configurer_statut_couleur_000' => "Noir"
, 'configurer_statut_couleur_f00' => "Rouge"
, 'configurer_statut_couleur_f0f' => "Fushia"
, 'configurer_statut_couleur_0c0' => "Vert"
, 'configurer_statut_couleur_00f' => "Bleu"
, 'configurer_statut_couleur_666' => "Gris"

, 'javascript_manquant' => "Javascript n&acute;est pas activ&eacute;.<br />
	Svp, activez Javascript sur votre navigateur et rechargez cette page. "
, 'jquery_manquant' => "jQuery n&acute;est pas activ&eacute;.<br />
	Svp, contactez le responsable du site en lui signalant cette erreur. "
, 'jquery_manquant_prive' => "Vous devez activer jQuery pour configurer ce plugin."
, 'jquery_manquant_public' => "jQuery n&acute;est pas activ&eacute; dans l'espace public.
	Vous ne pourrez pas vous connecter en utilisant la page de login.
	Vous devez activer jQuery pour utiliser ce plugin."

); //

?>