<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/_stable_/cfg/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'installed' => 'SPIP-Listes-Cleaner a bien �t� install�! ',
	'uninstalled' => 'SPIP-Listes-Cleaner a bien �t� d�install�! ',

	'spip_listes_cleaner_name' => 'Spip Listes Cleaner',
	'spip_listes_cleaner_dsc' => 'Configuration de Spip Listes Cleaner',
	'config_email_server' => 'Configuration du serveur de mail :',
	'server_address' => 'Adresse du serveur :',
	'server_type' => 'Type de serveur :',
	'server_security' => 'Securit&eacute; :',
	'server_security_option' => 'Option de s&eacute;curit&eacute; :',
	'server_mailbox' => 'Boite mail :',
	'server_username' => 'Nom d\'utilisateur :',
	'server_password' => 'Mot de passe :',
	
	'server_address_help' => 'exemple : pop.monserveur.com:110',
	'server_mailbox_help' => 'habituellement \'INBOX\'',
	
	'options' => 'Options :',
	'option_delete_bounce' => 'Supprimer les bounces mails du serveur mail :',
	'option_delete_bounce_yes' => 'Oui',
	'option_delete_bounce_no' => 'Non',
	'option_delete_row' => 'Methode de supression des auteurs :',
	'option_delete_row_definitive' => 'Compl&ecirc;te et d&eacute;finitive',
	'option_delete_row_5poubelle' => 'Marquer les auteurs "&agrave; la poubelle"',
	
	'statistics' => 'Statistiques (nombre de mails supprim&eacute;) :',
	'nb_deleted_mails' => 'Au total :',
	'nb_deleted_mails_last_export' => 'Depuis le dernier export supprim&eacute; :',
	
	'export' => 'Export :',
	'export_download' => 'Exporter les mails supprim&eacute; au format CSV :',
	'export_reset' => 'Supprimer tous les mails de l\'export :',
	'export_download_button' => 'Exporter',
	'export_reset_button' => 'Supprimer',
);

?>
