<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aecrit' => '<MODIF>napsal/a',
	'annonce' => 'Ozn&aacute;men&iacute;',
	'anonyme' => 'Anonym',
	'avatar' => 'Avatar',

	// C
	'citer' => 'Citovat',
	'creer_forum' => 'Vytvo&#345;it novou diskusn&iacute; skupinu',

	// D
	'dans_forum' => 'v diskusn&iacute; skupin&#283;',
	'dernier' => '<MODIF>Posledn&iacute;<br />',

	// E
	'ecrirea' => '<MODIF>Napsat e-mail pro',
	'effacer' => 'Odstranit',
	'email' => 'E-mail',
	'etplus' => '... a v&iacute;ce ...',

	// F
	'fil_deplace' => 'P&#345;esunout diskusi',
	'forum_ferme' => '<MODIF>Tato diskusn&iacute; skupina je ukon&#269;ena',
	'forum_ferme_texte' => '<MODIF>Tato diskusn&iacute; skupina skon&#269;ila. Dal&#353;&iacute; p&#345;&iacute;sp&#283;vky do n&iacute; nelze zas&iacute;lat.',
	'forum_ouvrir' => 'Otev&#345;&iacute;t diskusn&iacute; skupinu',

	// H
	'haut_page' => 'Na za&#269;&aacute;tek str&aacute;nky',

	// I
	'icone_ferme' => 'Zav&#345;&iacute;t',
	'info' => 'Informace',
	'info_confirmer_passe' => 'Potvrdit nov&eacute; heslo:',
	'info_plus_cinq_car' => 'v&iacute;ce ne&#382; 5 znak&#367;',

	// L
	'le' => 'Dne',
	'login' => 'P&#345;ipojen&iacute;',

	// M
	'maintenance' => '<MODIF>U dan&yacute;ch diskusn&iacute;ch skupin nezapome&#328;te zru&#353;it <br />"Uzav&#345;en&iacute; kv&#367;li &uacute;dr&#382;b&#283;"<br />.',
	'messages' => '<MODIF>Zpr&aacute;vy',
	'messages_derniers' => 'Posledn&iacute; zpr&aacute;vy',
	'messages_voir_dernier' => 'Zobrazit posledn&iacute; zpr&aacute;vu',
	'moderateurs' => 'Moder&aacute;tor/moder&aacute;to&#345;i',

	// N
	'no_message' => 'Vyhled&aacute;vac&iacute;m krit&eacute;ri&iacute;m nedopov&iacute;d&aacute; &#382;&aacute;dn&yacute; p&#345;edm&#283;t, resp. zpr&aacute;va.',

	// P
	'poste_valide' => 'Zpr&aacute;vy ke schv&aacute;len&iacute; ...',
	'profil' => '<MODIF>Profil ',

	// R
	'recherche_elargie' => 'Roz&#353;&iacute;&#345;en&eacute; hled&aacute;n&iacute;',
	'reglement' => '<MODIF>Spr&aacute;vci a moder&aacute;to&#345;i t&eacute;to diskusn&iacute; skupiny se sna&#382;&iacute; co nejrychleji odstra&#328;ovat nebo m&#283;nit ur&aacute;&#382;liv&eacute;, resp. sporn&eacute; zpr&aacute;vy. V&#353;echny v&#353;ak kontrolovat nemohou. Proto berete na v&#283;dom&iacute;, &#382;e zpr&aacute;vy v t&#283;chto diskusn&iacute;ch skupin&aacute;ch vyjad&#345;uj&iacute; n&aacute;zory sv&yacute;ch autor&#367; a nikoli spr&aacute;vc&#367;, moder&aacute;tor&#367; nebo spr&aacute;vc&#367; webu (s v&yacute;jimkou zpr&aacute;v, kter&eacute; tito ode&#353;lou). Proto za n&#283; tak&eacute; spr&aacute;vci, moder&aacute;to&#345;i, resp. spr&aacute;vci webu nenesou &#382;&aacute;dnou odpov&#283;dnost.</p>

<p>Souhlas&iacute;te, &#382;e nebudete zve&#345;ej&#328;ovat zpr&aacute;vy obsahuj&iacute;c&iacute; ur&aacute;&#382;ky, obsc&eacute;nnosti, sprost&aacute; a hrub&aacute; slova, pomluvy, v&yacute;hru&#382;ky, sexu&aacute;ln&iacute; nar&aacute;&#382;ky nebo jak&eacute;koli jin&eacute; zpr&aacute;vy, kter&eacute; by byly v rozporu s platn&yacute;mi z&aacute;kony. Pokud tak u&#269;in&iacute;te, m&#367;&#382;ete b&yacute;t okam&#382;it&#283; a nav&#382;dy vylou&#269;eni (a v&aacute;&#353; poskytovatel p&#345;&iacute;stupu k internetu o tom bude informov&aacute;n). Za &uacute;&#269;elem dodr&#382;ov&aacute;n&iacute; t&#283;chto podm&iacute;nek se ukl&aacute;daj&iacute; IP adresy v&#353;ech zpr&aacute;v. Souhlas&iacute;te s t&iacute;m, &#382;e spr&aacute;vce webu, spr&aacute;vce nebo moder&aacute;tor diskusn&iacute; skupiny maj&iacute; kdykoli pr&aacute;vo odstranit, zm&#283;nit, p&#345;esunout nebo ukon&#269;it jak&eacute;koli t&eacute;ma. Jako&#382;to u&#382;ivatel&eacute; souhlas&iacute;te s t&iacute;m, &#382;e ve&#353;ker&eacute; &uacute;daje, kter&eacute; d&aacute;le uvedete, budou ulo&#382;eny v datab&aacute;zi. Bez va&#353;eho souhlasu v&#353;ak tyto &uacute;daje nebudou sd&#283;leny &#382;&aacute;dn&eacute; jin&eacute; fyzick&eacute; &#269;i pr&aacute;vnick&eacute; osob&#283;. Spr&aacute;vce webu, spr&aacute;vce ani moder&aacute;to&#345;i nenesou &#382;&aacute;dnou odpov&#283;dnost za jak&yacute;koli pokus o neopr&aacute;vn&#283;n&eacute; zneu&#382;it&iacute; t&#283;chto &uacute;daj&#367;.</p>

<p>Tato diskusn&iacute; skupina pou&#382;&iacute;v&aacute; pro ukl&aacute;d&aacute;n&iacute; &uacute;daj&#367; ve va&#353;em po&#269;&iacute;ta&#269;i soubory cookies. Tyto soubory v&#353;ak neobsahuj&iacute; &#382;&aacute;dn&eacute; ingformace, kter&eacute; zad&aacute;te n&iacute;&#382;e. Jsou ur&#269;eny pouze pro zv&yacute;&#353;en&iacute; u&#382;ivatelsk&eacute;ho pohodl&iacute;. Elektronick&aacute; adresa se pou&#382;&iacute;v&aacute; pouze pro potvrzen&iacute; &uacute;daj&#367; o registraci a hesla (a tak&eacute; pro p&#345;&iacute;pad zasl&aacute;n&iacute; nov&eacute;ho hesla, pokud sv&eacute; heslo zapomenete).</p><p>P&#345;ihl&aacute;&#353;en&iacute;m potvrzuj&euml;te sv&#367;j souhlas s v&yacute;&#353;e uveden&yacute;mi pravidly.</p>',
	'repondre' => 'Odpov&#283;d&#283;t',
	'retour_forum' => 'Zp&#283;t na hlavn&iacute; str&aacute;nku diskusn&iacute; skupiny',

	// S
	'secteur_forum' => '<MODIF>OBLASTI � DISKUSN&Iacute; SKUPINY',
	'selection_efface' => '<MODIF>Odstranit v&yacute;b&#283;r ...',
	'sinscrire' => '<MODIF>p&#345;ihl&aacute;sit se',
	'statut' => 'Status',
	'sujet' => '<MODIF>P&#345;edm&#283;t&nbsp;',
	'sujet_auteur' => 'Autor',
	'sujet_clos_texte' => 'Tento p&#345;edm&#283;t je ukon&#269;en. Dal&#353;&iacute; zpr&aacute;vy k n&#283;mu nem&#367;&#382;ete zas&iacute;lat.',
	'sujet_clos_titre' => 'Skon&#269;en&yacute; p&#345;edm&#283;t',
	'sujet_nombre' => 'Po&#269;et p&#345;edm&#283;t&#367;',
	'sujet_nouveau' => 'Nov&yacute; p&#345;edm&#283;t',
	'sujet_rejete' => 'Odm&iacute;tnut&yacute; p&#345;edm&#283;t',
	'sujet_valide' => 'P&#345;edm&#283;t ke schv&aacute;len&iacute;',
	'sujets' => 'P&#345;edm&#283;ty',
	'sujets_aucun' => 'V t&eacute;to diskusn&iacute; skupin&#283; prozat&iacute;m neexistuje &#382;&aacute;dn&yacute; p&#345;edm&#283;t',

	// T
	'tous_forums' => 'V&#353;echny diskusn&iacute; skupiny',

	// V
	'voir' => 'ZOBRAZIT'
);

?>
