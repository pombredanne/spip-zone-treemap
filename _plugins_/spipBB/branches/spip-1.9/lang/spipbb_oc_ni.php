<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aecrit' => '<MODIF>escriugu&egrave;t&nbsp;',
	'annonce' => 'Anon&ccedil;a',
	'anonyme' => 'Anonime',
	'avatar' => 'Avatar',

	// C
	'citer' => 'Citar',
	'creer_forum' => 'Crear un F&ograve;ro n&ograve;u',

	// D
	'dans_forum' => 'dins lo f&ograve;ro',
	'dernier' => '<MODIF>&nbsp;Darri&egrave;r<br />',

	// E
	'ecrirea' => '<MODIF>Escriure un email &agrave;;',
	'effacer' => 'Escafar',
	'email' => 'Email',
	'etplus' => '... e mai ...',

	// F
	'fil_deplace' => 'Despla&ccedil;ar aqueu fiu',
	'forum_ferme' => '<MODIF>Aquest f&ograve;ro es desactivat',
	'forum_ferme_texte' => '<MODIF>Aquest f&ograve;ro es desactivat, e se p&ograve;u pas plus postar. ',
	'forum_ouvrir' => 'Durbir aquest F&ograve;ro',

	// H
	'haut_page' => 'Aut de p&agrave;gina',

	// I
	'icone_ferme' => 'Barrar',
	'info' => 'Informacions',
	'info_confirmer_passe' => 'Confirmar aqueu m&ograve;t de santa Clara n&ograve;u&nbsp;:',
	'info_plus_cinq_car' => 'mai de 5 caract&egrave;rs',

	// L
	'le' => 'Lo',
	'login' => 'Connexion',

	// M
	'maintenance' => '<MODIF>Oblidetz pas de quitar<br />"Barradura per manten&eacute;ncia",<br />suls Forums concernits.',
	'messages' => '<MODIF>Messatges',
	'messages_derniers' => 'Darri&egrave;rs Messatges',
	'messages_voir_dernier' => 'Veire lo darri&egrave;r messatge',
	'moderateurs' => 'Moderaire(s)',

	// N
	'no_message' => 'Minga de subj&egrave;cte &ograve; messatge corespond pas ai voastres crit&egrave;ris de rec&egrave;rca',

	// P
	'poste_valide' => 'Messatge(s) de validar ...',
	'profil' => '<MODIF>Profiu de',

	// R
	'recherche_elargie' => 'C&egrave;rca alargada',
	'reglement' => '<MODIF><p>Los administrators e moderaires d\'aquel forum far&agrave;n lor possible per suprimir o editar totes los messatges qu\'an un caract&egrave;r repreensible tan l&egrave;u coma possible. Pasmens, lor es impossible d\'espepissar cada message. Admet&egrave;tz doncas que totes los messatges postats sus aqueles forums exprimisson lo vejaire de sos autors, e non pas lo dels administrators, o moderaires, o webm&egrave;stres (levat los messatges qu\'eles-meteisses post&egrave;ron) e per consequent que ne p&ograve;don pas &egrave;sser tenguts per responsables.</p>

<p>Vos engatjats de postar pas de messatges injurioses, obsc&egrave;ns, vulgars, difamat&ograve;ris, amena&ccedil;ants, sexuals ni cap d\'autre message que violari&aacute; las leis en vigor. O fagu&egrave;ssetz vos poiri&aacute; menar a &egrave;sser f&ograve;rabandit sulpic e de longa (e v&ograve;stre provesidor d\'acc&egrave;s internet ne seri&aacute; assabentat). L\'adrei&ccedil;a IP de cada messatge s\'enregistra, per fins d\'ajudar de far respectar aquelas condicions. Bailatz v&ograve;stre ac&ograve;rdi sul fach que lo webm&egrave;stre, l\'administrator e los moderaires d\'aquel forum an lo drech de suprimir, editar, despa&ccedil;ar o clavar quin subj&egrave;cte de  discussion que si&aacute; a cada moment. Coma usanci&egrave;rs, s&egrave;s d\'ac&ograve;rdi sul fach que totas las informacions que balharetz &ccedil;ai apr&egrave;p se servar&agrave;n dins una basa de donadas. Pasmens, aquelas informacions se divulgar&agrave;n pas a cap de persona o societat sens v&ograve;stre ac&ograve;rdi. Lo webm&egrave;stre, l\'administrator, e los moderaires p&ograve;don pas &egrave;sser tenguts per responsables en cap qu\'un piratatge informatic mene a l\'acc&egrave;s a aquelas donadas.</p>

<p>Aquel forum emplega los cookies per servar d\'informacions sus v&ograve;stre ordenador. Los cookies caber&agrave;n pas cap d\'information qu\'aur&egrave;tz intrat &ccedil;ai apr&egrave;p, servisson sonque per melhorar lo conf&ograve;rt d\'utilizacion. L\'adrei&ccedil;a eletronica es utilizada sonque per confirmar los detalhs de v&ograve;stre enregistrament e v&ograve;stre mot de santa Clara  (e tanben per vos mandar un mot de santa Clara n&ograve;u en cas que l\'oblidetz).</p><p>En vos enregistrar, vos portatz garent del fach d\'escriure en ac&ograve;rdi amb lo reglament &ccedil;ai subre.</p>',
	'repondre' => 'Respoandre',
	'retour_forum' => 'Tornar &agrave; l\'acu&egrave;lh dau f&ograve;ro',

	// S
	'secteur_forum' => '<MODIF>SECTORS FORUMS',
	'selection_efface' => '<MODIF>Escafar la seleccion ...',
	'sinscrire' => '<MODIF>si registrar',
	'statut' => 'Estatut',
	'sujet' => '<MODIF>Subj&egrave;cte&nbsp;',
	'sujet_auteur' => 'Autor',
	'sujet_clos_texte' => 'Aquest subj&egrave;cte es barrat, pod&egrave;tz pas plus li postar.',
	'sujet_clos_titre' => 'Subj&egrave;cte Barrat',
	'sujet_nombre' => 'Nombre de Subj&egrave;ctes',
	'sujet_nouveau' => 'Subj&egrave;cte n&ograve;u',
	'sujet_rejete' => 'Subj&egrave;cte refudat;',
	'sujet_valide' => 'Subj&egrave;cte de validar',
	'sujets' => 'Subj&egrave;ctes',
	'sujets_aucun' => 'Minga de subj&egrave;ctes encara dins aqueu f&ograve;ro',

	// T
	'tous_forums' => 'Toi lu f&ograve;ros',

	// V
	'voir' => 'VEIRE'
);

?>
