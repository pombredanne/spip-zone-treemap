<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aecrit' => '<MODIF>ha scritto&nbsp;',
	'annonce' => 'Annuncio',
	'anonyme' => 'Anonimo',
	'avatar' => 'Avatar',

	// C
	'citer' => 'Cita',
	'creer_forum' => 'Crea Nuovo Forum',

	// D
	'dans_forum' => 'nel forum',
	'dernier' => '<MODIF>&nbsp;Ultimo<br />',

	// E
	'ecrirea' => '<MODIF>Scrivi un Email a',
	'effacer' => 'Cancella',
	'email' => 'E-mail',
	'etplus' => '... e oltre ...',

	// F
	'fil_deplace' => 'Sposta questo thread',
	'forum_ferme' => '<MODIF>Questo forum &egrave; disattivato',
	'forum_ferme_texte' => '<MODIF>Questo forum &egrave; disattivato. Non &egrave; pi&ugrave; possibile postare messaggi.',
	'forum_ouvrir' => 'Apri questo Forum',

	// H
	'haut_page' => 'Inizio pagina',

	// I
	'icone_ferme' => 'Chiudi',
	'info' => 'Informazioni',
	'info_confirmer_passe' => 'Conferma la password:',
	'info_plus_cinq_car' => 'pi&ugrave; di 5 caratteri',

	// L
	'le' => 'Il',
	'login' => 'Connessione',

	// M
	'maintenance' => '<MODIF>Non dimenticarsi di eliminare<br />"Chiusura per manutenzione",<br />sui Forum interessati.',
	'messages' => '<MODIF>Messaggi',
	'messages_derniers' => 'Messaggi Recenti',
	'messages_voir_dernier' => 'Vedi l\'ultimo messaggio',
	'moderateurs' => 'Moderatore/i',

	// N
	'no_message' => 'Non &egrave; stato trovato alcun argomento o messaggio corrispondente ai criteri di ricerca inseriti',

	// P
	'poste_valide' => 'Messaggio/i da convalidare ...',
	'profil' => '<MODIF>Profilo di',

	// R
	'recherche_elargie' => 'Ricerca avanzata',
	'reglement' => '<MODIF><p>Le amministratrici e le moderatrici di questo forum censureranno (cancellando o modificando) tutti i messaggi con contenuto riprovevole nel pi&ugrave; breve tempo possibile. Tuttavia, &egrave; impossibile leggere tutti i messaggi. Pertanto, tutti i messaggi pubblicati su questi forum esprimono unicamente l\'opinione e le idee delle rispettive autrici, e non delle amministratrici o moderatrici o webmistress (eccetto per i messaggi scritti dalle stesse) e quindi non possono essere ritenute responsabili.</p>

<p>Si accetta di non inviare messaggi ingiuriosi, osceni, volgari, diffamatori, minacciosi, a sfondo sessuale o di qualsiasi altra natura che costituisce reato per le leggi vigenti. L\'eventuale invio di messaggi contrari a quanto detto pu&ograve; causare l\'immediata e permanente sospensione dei diritti di accesso  (e il proprio provider Internet ne verr&agrave; informato). L\'indirizzo IP di ogni singolo messaggio viene registrato al fine di facilitare il rispetto di tali termini. Si accetta che la webmistress, l\'amministratrice e le moderatrici di questo forum hanno il diritto di eliminare, modificare, spostare o chiudere qualsiasi argomento di discussione, in qualsiasi momento. In qualit&agrave; di utente si accetta che tutte le informazioni fornite di seguito siano registrate in una banca dati. Tuttavia, queste informazioni non verranno cedute a terzi se non previo consenso da parte vostra. La webmistress, l\'amministratrice e le moderatrici non possono essere ritenute responsabili nel caso che un tentativo di pirateria informatica conduca all\'accesso dei dati personali.</p>

<p>Questo forum utilizza i cookie per registrare informazioni sul computer. Questi cookie non conterranno alcuna delle informazioni che verranno richieste successivamente, ma servono esclusivamente a rendere pi&ugrave; confortevole la navigazione. L\'indirizzo e-mail viene utilizzato solamente al fine di confermare i dettagli della propria iscrizione e della password scelta (nonch&eacute; per inviare una nuova password nel caso la prima venga dimenticata).</p><p>Accettando l\'iscrizione si accettano altres&igrave; i termini del presente accordo.</p>',
	'repondre' => 'Rispondi',
	'retour_forum' => 'Torna alla home del forum',

	// S
	'secteur_forum' => '<MODIF>SETTORI FORUM',
	'selection_efface' => '<MODIF>Cancella la selezione ...',
	'sinscrire' => '<MODIF>registrarsi',
	'statut' => 'Status',
	'sujet' => '<MODIF>Argomento&nbsp;',
	'sujet_auteur' => 'Autore',
	'sujet_clos_texte' => 'Questa discussione &egrave; stata chiusa e non &egrave; possibile inviare altri messaggi.',
	'sujet_clos_titre' => 'Argomento Chiuso',
	'sujet_nombre' => 'Numero di Argomenti',
	'sujet_nouveau' => 'Nuovo argomento',
	'sujet_rejete' => 'Argomento rifiutato',
	'sujet_valide' => 'Argomento da convalidare',
	'sujets' => 'Argomenti',
	'sujets_aucun' => 'Nessuna discussione in questo forum attualmente',

	// T
	'tous_forums' => 'Tutti i forum',

	// V
	'voir' => 'VEDI'
);

?>
