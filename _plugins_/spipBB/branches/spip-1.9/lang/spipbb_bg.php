<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aecrit' => '<MODIF>&#1085;&#1072;&#1087;&#1080;&#1089;&#1072;&#1085;&#1086;',
	'annonce' => '&#1053;&#1086;&#1074;&#1080;&#1085;&#1072;',
	'anonyme' => '&#1040;&#1085;&#1086;&#1085;&#1080;&#1084;&#1077;&#1085;',
	'avatar' => '&#1040;&#1074;&#1072;&#1090;&#1072;&#1088;',

	// C
	'citer' => '&#1062;&#1080;&#1090;&#1072;&#1090;',
	'creer_forum' => '&#1057;&#1098;&#1079;&#1076;&#1072;&#1074;&#1072;&#1085;&#1077; &#1085;&#1072; &#1085;&#1086;&#1074; &#1092;&#1086;&#1088;&#1091;&#1084;',

	// D
	'dans_forum' => '&#1074;&#1098;&#1090;&#1088;&#1077; &#1074;&#1098;&#1074; &#1092;&#1086;&#1088;&#1091;&#1084;&#1072;',
	'dernier' => '<MODIF>&nbsp;&#1055;&#1086;&#1089;&#1083;&#1077;&#1076;&#1077;&#1085;<br />',

	// E
	'ecrirea' => '<MODIF>&#1055;&#1080;&#1089;&#1072;&#1085;&#1077; &#1085;&#1072; &#1087;&#1080;&#1089;&#1084;&#1086; &#1085;&#1072;:',
	'effacer' => '&#1048;&#1079;&#1090;&#1088;&#1080;&#1074;&#1072;&#1085;&#1077;',
	'email' => '&#1045;&#1083;&#1077;&#1082;&#1090;&#1088;&#1086;&#1085;&#1077;&#1085; &#1072;&#1076;&#1088;&#1077;&#1089;',
	'etplus' => '... &#1080; &#1086;&#1097;&#1077; ...',

	// F
	'fil_deplace' => '&#1055;&#1088;&#1077;&#1084;&#1077;&#1089;&#1090;&#1074;&#1072;&#1085;&#1077; &#1085;&#1072; &#1085;&#1080;&#1096;&#1082;&#1072;&#1090;&#1072; &#1085;&#1072; &#1088;&#1072;&#1079;&#1075;&#1086;&#1074;&#1086;&#1088;&#1072;',
	'forum_ferme' => '<MODIF>&#1060;&#1086;&#1088;&#1091;&#1084;&#1098;&#1090; &#1077; &#1076;&#1077;&#1072;&#1082;&#1090;&#1080;&#1074;&#1080;&#1088;&#1072;&#1085;',
	'forum_ferme_texte' => '<MODIF>&#1058;&#1086;&#1079;&#1080; &#1092;&#1086;&#1088;&#1091;&#1084; &#1077; &#1076;&#1077;&#1072;&#1082;&#1090;&#1080;&#1074;&#1080;&#1088;&#1072;&#1085;. &#1053;&#1077; &#1084;&#1086;&#1078;&#1077;&#1090;&#1077; &#1076;&#1072; &#1087;&#1091;&#1073;&#1083;&#1080;&#1082;&#1091;&#1074;&#1072;&#1090;&#1077; &#1074; &#1085;&#1077;&#1075;&#1086;.',
	'forum_ouvrir' => '&#1054;&#1090;&#1074;&#1072;&#1088;&#1103;&#1085;&#1077; &#1085;&#1072; &#1092;&#1086;&#1088;&#1091;&#1084;&#1072;',

	// H
	'haut_page' => '&#1043;&#1086;&#1088;&#1077;',

	// I
	'icone_ferme' => '&#1047;&#1072;&#1090;&#1074;&#1072;&#1088;&#1103;&#1085;&#1077;',
	'info' => '&#1048;&#1085;&#1092;&#1086;&#1088;&#1084;&#1072;&#1094;&#1080;&#1103;',
	'info_confirmer_passe' => '&#1055;&#1086;&#1090;&#1074;&#1098;&#1088;&#1078;&#1076;&#1072;&#1074;&#1072;&#1085;&#1077; &#1085;&#1072; &#1085;&#1086;&#1074;&#1072;&#1090;&#1072; &#1087;&#1072;&#1088;&#1086;&#1083;&#1072;:',
	'info_plus_cinq_car' => '&#1087;&#1086;&#1074;&#1077;&#1095;&#1077; &#1086;&#1090; 5 &#1079;&#1085;&#1072;&#1082;&#1072;',

	// L
	'le' => '',
	'login' => '&#1057;&#1074;&#1098;&#1088;&#1079;&#1074;&#1072;&#1085;&#1077;',

	// M
	'maintenance' => '<MODIF>&#1053;&#1077; &#1079;&#1072;&#1073;&#1088;&#1072;&#1074;&#1103;&#1081;&#1090;&#1077; &#1076;&#1072; &#1084;&#1072;&#1093;&#1085;&#1077;&#1090;&#1077;<br />"&#1047;&#1072;&#1090;&#1074;&#1086;&#1088;&#1077;&#1085; &#1079;&#1072; &#1087;&#1086;&#1076;&#1076;&#1088;&#1098;&#1078;&#1082;&#1072;",<br />&#1074; &#1089;&#1098;&#1086;&#1090;&#1074;&#1077;&#1090;&#1085;&#1080;&#1090;&#1077; &#1092;&#1086;&#1088;&#1091;&#1084;&#1080;.',
	'messages' => '<MODIF>&#1057;&#1098;&#1086;&#1073;&#1097;&#1077;&#1085;&#1080;&#1103;',
	'messages_derniers' => '&#1053;&#1072;&#1081;-&#1085;&#1086;&#1074;&#1080;&#1090;&#1077; &#1089;&#1098;&#1086;&#1073;&#1097;&#1077;&#1085;&#1080;&#1103;',
	'messages_voir_dernier' => '&#1055;&#1088;&#1077;&#1075;&#1083;&#1077;&#1076; &#1085;&#1072; &#1087;&#1086;&#1089;&#1083;&#1077;&#1076;&#1085;&#1086;&#1090;&#1086; &#1089;&#1098;&#1086;&#1073;&#1097;&#1077;&#1085;&#1080;&#1077;',
	'moderateurs' => '<NEW>Mod&eacute;rateur(s)',

	// N
	'no_message' => '&#1053;&#1080;&#1082;&#1072;&#1082;&#1074;&#1080; &#1090;&#1077;&#1084;&#1080; &#1080;&#1083;&#1080; &#1089;&#1098;&#1086;&#1073;&#1097;&#1077;&#1085;&#1080;&#1103; &#1085;&#1077; &#1073;&#1103;&#1093;&#1072; &#1085;&#1072;&#1084;&#1077;&#1088;&#1077;&#1085;&#1080; &#1089;&#1087;&#1086;&#1088;&#1077;&#1076; &#1082;&#1088;&#1080;&#1090;&#1077;&#1088;&#1080;&#1080;&#1090;&#1077;, &#1082;&#1086;&#1080;&#1090;&#1086; &#1089;&#1090;&#1077; &#1091;&#1082;&#1072;&#1079;&#1072;&#1083;&#1080;',

	// P
	'poste_valide' => '&#1055;&#1091;&#1073;&#1083;&#1080;&#1082;&#1072;&#1094;&#1080;&#1103;(&#1080;) &#1079;&#1072; &#1086;&#1076;&#1086;&#1073;&#1088;&#1077;&#1085;&#1080;&#1077; ...',
	'profil' => '<MODIF>&#1055;&#1088;&#1086;&#1092;&#1080;&#1083; &#1085;&#1072;',

	// R
	'recherche_elargie' => '&#1056;&#1072;&#1079;&#1096;&#1080;&#1088;&#1077;&#1085;&#1086; &#1090;&#1098;&#1088;&#1089;&#1077;&#1085;&#1077;',
	'reglement' => '<MODIF><p>Les administrateurs et mod&eacute;rateurs de ce forum s\'efforceront de supprimer ou &eacute;diter tous les messages &agrave; caract&egrave;re r&eacute;pr&eacute;hensible aussi rapidement que possible. Toutefois, il leur est impossible de passer en revue tous les messages. Vous admettez donc que tous les messages post&eacute;s sur ces forums expriment la vue et opinion de leurs auteurs respectifs, et non pas des administrateurs, ou mod&eacute;rateurs, ou webmestres (except&eacute; les messages post&eacute;s par eux-m&ecirc;me) et par cons&eacute;quent ne peuvent pas &ecirc;tre tenus pour responsables.</p>

<p>Vous consentez &agrave; ne pas poster de messages injurieux, obsc&egrave;nes, vulgaires, diffamatoires, mena&ccedil;ants, sexuels ou tout autre message qui violerait les lois applicables. Le faire peut vous conduire &agrave; &ecirc;tre banni imm&eacute;diatement de fa&ccedil;on permanente (et votre fournisseur d\'acc&egrave;s &agrave; internet en sera inform&eacute;). L\'adresse IP de chaque message est enregistr&eacute;e afin d\'aider &agrave; faire respecter ces conditions. Vous &ecirc;tes d\'accord sur le fait que le webmestre, l\'administrateur et les mod&eacute;rateurs de ce forum ont le droit de supprimer, &eacute;diter, d&eacute;placer ou verrouiller n\'importe quel sujet de discussion &agrave; tout moment. En tant qu\'utilisateur, vous &ecirc;tes d\'accord sur le fait que toutes les informations que vous donnerez ci-apr&egrave;s seront stock&eacute;es dans une base de donn&eacute;es. Cependant, ces informations ne seront divulgu&eacute;es &agrave; aucune tierce personne ou soci&eacute;t&eacute; sans votre accord. Le webmestre, l\'administrateur, et les mod&eacute;rateurs ne peuvent pas &ecirc;tre tenus pour responsables si une tentative de piratage informatique conduit &agrave; l\'acc&egrave;s de ces donn&eacute;es.</p>

<p>Ce forum utilise les cookies pour stocker des informations sur votre ordinateur. Ces cookies ne contiendront aucune information que vous aurez entr&eacute; ci-apr&egrave;s, ils servent uniquement &agrave; am&eacute;liorer le confort d\'utilisation. L\'adresse e-mail est uniquement utilis&eacute;e afin de confirmer les d&eacute;tails de votre enregistrement ainsi que votre mot de passe (et aussi pour vous envoyer un nouveau mot de passe dans la cas o&ugrave; vous l\'oublieriez).</p><p>En vous enregistrant, vous vous portez garant du fait d\'&ecirc;tre en accord avec le r&egrave;glement ci-dessus.</p>',
	'repondre' => '&#1054;&#1090;&#1075;&#1086;&#1074;&#1086;&#1088;',
	'retour_forum' => '&#1042;&#1088;&#1098;&#1097;&#1072;&#1085;&#1077; &#1074; &#1085;&#1072;&#1095;&#1072;&#1083;&#1086;&#1090;&#1086; &#1085;&#1072; &#1092;&#1086;&#1088;&#1091;&#1084;&#1072;',

	// S
	'secteur_forum' => '<MODIF>&#1057;&#1045;&#1050;&#1058;&#1054;&#1056; &#1060;&#1054;&#1056;&#1059;&#1052;&#1048;',
	'selection_efface' => '<MODIF>&#1048;&#1079;&#1090;&#1088;&#1080;&#1074;&#1072;&#1085;&#1077; &#1085;&#1072; &#1080;&#1079;&#1073;&#1086;&#1088;&#1072; ...',
	'sinscrire' => '<MODIF>&#1079;&#1072;&#1087;&#1080;&#1089;&#1074;&#1072;&#1085;&#1077;',
	'statut' => '&#1057;&#1090;&#1072;&#1090;&#1091;&#1090;',
	'sujet' => '<MODIF>&#1058;&#1077;&#1084;&#1072;',
	'sujet_auteur' => '&#1040;&#1074;&#1090;&#1086;&#1088;',
	'sujet_clos_texte' => '&#1058;&#1077;&#1084;&#1072;&#1090;&#1072; &#1077; &#1087;&#1088;&#1080;&#1082;&#1083;&#1102;&#1095;&#1077;&#1085;&#1072;, &#1085;&#1077; &#1084;&#1086;&#1078;&#1077;&#1090;&#1077; &#1076;&#1072; &#1087;&#1091;&#1073;&#1083;&#1080;&#1082;&#1091;&#1074;&#1072;&#1090;&#1077;.',
	'sujet_clos_titre' => '&#1055;&#1088;&#1080;&#1082;&#1083;&#1102;&#1095;&#1077;&#1085;&#1080; &#1090;&#1077;&#1084;&#1080;',
	'sujet_nombre' => '&#1041;&#1088;&#1086;&#1081; &#1085;&#1072; &#1090;&#1077;&#1084;&#1080;&#1090;&#1077;',
	'sujet_nouveau' => '&#1053;&#1086;&#1074;&#1072; &#1090;&#1077;&#1084;&#1072;',
	'sujet_rejete' => '&#1054;&#1090;&#1093;&#1074;&#1098;&#1088;&#1083;&#1077;&#1085;&#1072; &#1090;&#1077;&#1084;&#1072;',
	'sujet_valide' => '&#1058;&#1077;&#1084;&#1072; &#1079;&#1072; &#1086;&#1076;&#1086;&#1073;&#1088;&#1077;&#1085;&#1080;&#1077;',
	'sujets' => '&#1058;&#1077;&#1084;&#1080;',
	'sujets_aucun' => '&#1047;&#1072; &#1084;&#1086;&#1084;&#1077;&#1085;&#1090;&#1072; &#1085;&#1103;&#1084;&#1072; &#1090;&#1077;&#1084;&#1080; &#1074;&#1098;&#1074; &#1092;&#1086;&#1088;&#1091;&#1084;&#1072;',

	// T
	'tous_forums' => '&#1042;&#1089;&#1080;&#1095;&#1082;&#1080; &#1092;&#1086;&#1088;&#1091;&#1084;&#1080;',

	// V
	'voir' => '&#1055;&#1056;&#1045;&#1043;&#1051;&#1045;&#1044;'
);

?>
