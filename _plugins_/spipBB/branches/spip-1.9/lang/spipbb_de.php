<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aecrit' => '<MODIF>hat geschrieben&nbsp;',
	'annonce' => 'Ank&uuml;ndigung',
	'anonyme' => 'Anonym',
	'avatar' => 'Avatar',

	// C
	'citer' => 'Zitieren',
	'creer_forum' => 'Neues Forum anlegen',

	// D
	'dans_forum' => 'im Forum',
	'dernier' => '<MODIF>Letzter<br />',

	// E
	'ecrirea' => '<MODIF>E-Mail schreiben an:&nbsp;',
	'effacer' => 'L&ouml;schen',
	'email' => 'E-Mail',
	'etplus' => '... mehr ...',

	// F
	'fil_deplace' => 'Diskussion verschieben',
	'forum_ferme' => '<MODIF>Dieses Forum ist desaktiviert',
	'forum_ferme_texte' => '<MODIF>Dieses Forum ist desaktiviert. Sie k&ouml;nnen nicht in ihm schreiben.',
	'forum_ouvrir' => 'Forum &ouml;ffnen',

	// H
	'haut_page' => 'Nach oben',

	// I
	'icone_ferme' => 'Schliessen',
	'info' => 'Informationen',
	'info_confirmer_passe' => 'Neues Passwort best&auml;tigen:',
	'info_plus_cinq_car' => 'mehr als 5 Zeichen',

	// L
	'le' => 'Am',
	'login' => 'Verbinden',

	// M
	'maintenance' => '<MODIF>Bitte vergessen sie nicht den Hinweis<br />"wegen Wartung geschlossen",<br /> in den betreffenden Foren zu entfernen.',
	'messages' => '<MODIF>Nachrichten',
	'messages_derniers' => 'Letzte Nachrichten',
	'messages_voir_dernier' => 'Letzte Nachricht anzeigen',
	'moderateurs' => 'Moderator(en)',

	// N
	'no_message' => 'Keine Nachricht oder Thema entspricht ihrem Suchbegriff',

	// P
	'poste_valide' => 'Beitr&auml;ge best&auml;tigen ...',
	'profil' => '<MODIF>Profil von',

	// R
	'recherche_elargie' => 'Erweiterte Suche',
	'reglement' => '<MODIF>Die Administratoren und Moderatoren dieses Forums bem&uuml;hen sich, alle Beitr&auml;ge mit widerrechtlichem Inhalt unverz&uuml;glich zu l&ouml;schen oder zu &uuml;berarbeiten. Es ist jedoch unm&ouml;glich, jede Nachricht einzeln zu pr&uuml;fen. Als Leser erkennen Sie an, da&szlig; die Beitr&auml;ge in diesen Foren ausschlie&szlig;lich den Standpunkt der jeweiligen Autoren wiedergeben und nicht den der Administratoren und Moderatoren (selbstverst&auml;ndlich betrifft das nicht die von ihnen selbst verfa&szlig;ten Beitr&auml;ge), und sie f&uuml;r den Inhalt von Beitr&auml;gen dritter nicht verantwortlich sind.</p>

<p>Sie erkl&auml;ren sich damit einverstanden, keine beleidigenden, obz&ouml;nen, vulg&auml;ren, diffamierenden, verleumdenden, sexuellen oder sonstigen gegen geltendes Recht versto&szlig;enden Beitr&auml;ge zu ver&ouml;ffentlichen. Bei Versto&szlig; gegen diese Richtlinien werden wir sie von der Nutzung der Foren f&uuml;r eine bestimmte Zeit oder auf Dauer ausschlie&szlig;en und ihren Internetprovider &uuml;ber die Verst&ouml;&szlig;e informieren. Die IP-Adressen der Nutzer werden gespeichert, um diese Richtlinien durchsetzen zu k&ouml;nnen. Sie erkl&auml;ren sich damit einverstanden, da&szlig; Webmaster, Administrator und Moderatoren dieses Forum berechtigt sind, Beitr&auml;ge jederzeit zu l&ouml;schen, zu &auml;ndern und zu verschieben sowie jedes Thema zu sperren. Sie erkl&auml;ren sich damit einverstanden, da&szlig; alle von ihnen gegebenen Informationen in einer Datenbank gespeichert werden. Diese Informationen werden keiner dritten Person oder Firma ohne ihre ausdr&uuml;ckliche Zustimmung zug&auml;nglich gemacht. Sie erkennen an, da&szlig; Webmaster, Administrator und Moderatoren nicht f&uuml;r die Folgen eines eventuellen Hackerangriffs oder Datendiebstahls verantwortlich sind, durch den die gespeicherten Daten in andere H&auml;nde gelangen.</p>

<p>Dieses Forum verwendet Cookies um Informationen auf ihrem Computer zu speichern. Diese Cookies enthalten keine Informationen &uuml;ber sie und dienen ausschlie&szlig;lich der komfortabeln Nutzung der Foren. Ihre E-Mail Adresse wird ausschlie&szlig;lich zur &Uuml;bermittlung ihres Benutzernamens und Pa&szlig;worts verwendet (und zur &Uuml;bermittlung eines neuen Pa&szlig;worts im Fall, da&szlig; sie das urspr&uuml;ngliche vergessen haben).</p><p>Durch ihre Anmeldung best&auml;tigen sie die Anerkenntnis der obigen Regeln.</p>',
	'repondre' => 'Antworten',
	'retour_forum' => 'Zur&uuml;ck zur Startseite des Forums',

	// S
	'secteur_forum' => '<MODIF>HAUPTRUBRIKEN DER FOREN',
	'selection_efface' => '<MODIF>Auswahl l&ouml;schen ...',
	'sinscrire' => '<MODIF>sich einschreiben',
	'statut' => 'Status',
	'sujet' => '<MODIF>Thema',
	'sujet_auteur' => 'Autor',
	'sujet_clos_texte' => 'Dieses Thema ist abgeschlossen. Sie k&ouml;nnen keine Beitr&auml;ge posten.',
	'sujet_clos_titre' => 'Abgeschlossenes Thema',
	'sujet_nombre' => 'Anzahl der Themen',
	'sujet_nouveau' => 'Neues Thema',
	'sujet_rejete' => 'Abgelehntes Thema',
	'sujet_valide' => 'Themen &uuml;berpr&uuml;fen',
	'sujets' => 'Themen',
	'sujets_aucun' => 'Dieses Forum hat noch keine Themen',

	// T
	'tous_forums' => 'Alle Foren',

	// V
	'voir' => 'ANZEIGEN'
);

?>
