<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'admin_action_01_configuration' => 'Configurare',
	'admin_action_02_etat' => 'Starea forum-urilor',
	'admin_action_ZZ_debug' => 'Debugging',
	'admin_action_effacer' => 'Mesaje refuzate',
	'admin_action_fromphorum' => 'Import din Phorum',
	'admin_action_fromphpbb' => 'Import din PhpBB',
	'admin_action_gere_ban' => 'Gestiunea interzicerilor',
	'admin_action_gestion' => 'Gestiune',
	'admin_action_inscrits' => 'Membri',
	'admin_action_swconfig' => 'Configurare',
	'admin_action_swforum' => 'Post-uri marcate',
	'admin_action_swlog' => 'Log de spam',
	'admin_action_swwords' => 'Gestiune cuvinte',
	'admin_afficher_bouton_alerte_abus' => 'Afi&#351;a&#355;i butoanele de alert&#259; abuzuri',
	'admin_affichier_bouton_rss' => 'Afi&#351;ati butoanele RSS',
	'admin_age_forum' => 'V&acirc;rsta forum-ului ani/luni',
	'admin_avatar_affiche' => 'Accepta&#355;i &#351;i afi&#351;a&#355;i avatar-urile (da &icirc;n mod implicit la prima instalare)',
	'admin_avatar_taille_contact' => 'M&#259;rimea avatar-urilor (&icirc;n pixeli) pe pagina de contact',
	'admin_avatar_taille_profil' => 'M&#259;rimea avatar-urilor (&icirc;n pixeli) pe pagina de profil',
	'admin_avatar_taille_sujet' => 'M&#259;rimea avatar-urilor (&icirc;n pixeli) pe pagina subiecte',
	'admin_average_posts' => 'Media de mesaje/zi',
	'admin_average_users' => 'Media de &icirc;nscrieri/zi',
	'admin_ban_email' => 'Gestiune de adrese email interzise',
	'admin_ban_email_info' => 'Pentru a specifica mai mult de o adres&#259; email, separa&#355;i-le prin virgule. Pentru a specifica un joker pentru numele de utilizator, utiliza&#355;i *: de exemplu *@hotmail.com',
	'admin_ban_email_none' => 'Nici o adres&#259; interzis&#259;',
	'admin_ban_ip' => 'Gestiune adrese IP interzise',
	'admin_ban_ip_info' => 'Pentru a specifica mai multe IP-uri sau nume de servere diferite, separa&#355;i-le prin virgule. Pentru a specifica o plaj&#259; de adrese IP, separa&#355;i &icirc;nceputul &#351;i sf&acirc;r&#351;itul cu o cratim&#259; (-), pentru a specifica un joker folosi&#355;i o stea (*)',
	'admin_ban_ip_none' => 'Nici o adres&#259; interzis&#259;',
	'admin_ban_user' => 'Gestiune login-uri interzise',
	'admin_ban_user_info' => 'Pute&#355;i interzice mai mul&#355;i utilizatori deodat&#259; prin folosirea tastei Ctrl sau Maj &icirc;mpreun&#259; cu mouse-ul sau tastatura',
	'admin_ban_user_none' => 'Nici un utilizator',
	'admin_cat_01_general' => 'Administrare',
	'admin_cat_outils' => 'Scule',
	'admin_cat_spam' => 'Anti Spam',
	'admin_config_prerequis' => 'Necesare',
	'admin_config_spam_words' => 'Configurare anti-spam',
	'admin_config_spipbb' => 'Activarea lui SpipBB',
	'admin_config_spipbb_info' => 'Ap&#259;sa&#355;i pe Da pentru a activa SpipBB',
	'admin_config_tables' => 'Configurarea tabelelor lui SpipBB',
	'admin_config_tables_erreur' => '<MODIF>Problem&#259; cu tabelele lui SpipBB : @tables_erreur@ sunt incorecte (tabelele @tables_ok@ par a fi corecte).
 Consulta&#355;i [documenta&#355;ia de la Spip-Contrib->http://www.spip-contrib.net/SpipBB-le-forum] sau [suportul pentru spipbb.spip-zone->http://spipbb.spip-zone.info/spip.php?article11]',
	'admin_config_tables_ok' => 'Tabelele lui SpipBB sunt corect instalate (@tables_ok@)',
	'admin_date_ouverture' => 'Data deschiderii',
	'admin_debug_log' => 'Fi&#351;ier de log @log_name@',
	'admin_debug_metas' => 'META-uri SpipBB',
	'admin_form_action' => 'Ac&#355;iune',
	'admin_form_creer_categorie' => 'Crea&#355;i o categorie',
	'admin_form_creer_forum' => 'Crea&#355;i un forum',
	'admin_form_deplacer' => 'Deplasa&#355;i',
	'admin_form_descendre' => 'Cobor&acirc;&#355;i',
	'admin_form_editer' => 'Edita&#355;i',
	'admin_form_messages' => '&nbsp;',
	'admin_form_monter' => 'Monta&#355;i',
	'admin_form_sujets' => '&nbsp;',
	'admin_forums_affiche_membre_defaut' => 'Vre&#355;i s&#259; afi&#351;a&#355;i membrii &icirc;n lista de membrii, chiar &#351;i atunci c&acirc;nd nu au f&#259;cut aceast&#259; alegere?<br />[ Nu &icirc;n mod implicit ]',
	'admin_forums_configuration' => 'Configurare SpipBB',
	'admin_forums_configuration_avatar' => 'Gestinue avatar-uri, reglaj general',
	'admin_forums_configuration_options' => 'Op&#355;iuni SpipBB',
	'admin_forums_log_level' => 'Alegere de nivel de log-uri produse de SpipBB.<br />[ 3 (maximum)- implicit ]',
	'admin_forums_log_level_0' => 'Nici un log',
	'admin_forums_log_level_1' => 'Un pic de log-uri',
	'admin_forums_log_level_2' => 'Multe log-uri',
	'admin_forums_log_level_3' => 'Enorm de multe log-uri',
	'admin_id_mjsc' => 'Num&#259;r',
	'admin_infos' => 'SpipBB - Administrare - Recapitulare',
	'admin_interface' => 'Op&#355;iuni de interfa&#355;&#259;',
	'admin_nombre_lignes_messages' => 'Num&#259;rul de linii de mesaj',
	'admin_plugin_requis_erreur' => 'Plug-in necesar care lipse&#351;te. Activa&#355;i-l !',
	'admin_plugin_requis_erreur_balisesession' => 'Instala&#355;i plug-in-ul Balise SESSION &#351;i activa&#355;i-l ! [Documentaie aici->http://www.spip-contrib.net/?article1224], [Arhiv&#259; ZIP aici->http://files.spip.org/spip-zone/balise_session.zip].',
	'admin_plugin_requis_erreur_cfg' => 'Instala&#355;i plug-in-ul CFG &#351;i activa&#355;i-l ! [Documenta&#355;ie aici->http://www.spip-contrib.net/?article1605], [Arhiv&#259; ZIP aici->http://files.spip.org/spip-zone/cfg.zip].',
	'admin_plugin_requis_erreur_s' => 'Plug-in-uri necesare lipsesc. Activa&#355;i-le !',
	'admin_plugin_requis_ok' => 'Plug-in(-uri) instalat(e) &#351;i activ(e)',
	'admin_plugin_requis_ok_cfg' => '[Plug-in CFG->http://www.spip-contrib.net/?article1605] : furnizeaz&#259; func&#355;ii &#351;i balize.',
	'admin_sous_titre' => 'Accesa&#355;i panoul de administrare forum-uri cu SpipBB',
	'admin_spip_config_forums' => 'Configura&#355;ie SPIP&nbsp;:',
	'admin_spip_forums_ok' => 'Forum-urile publice sunt activate cu bine.',
	'admin_spip_forums_warn' => '<p>{{Aten&#355;ie}} : forum-urile dumneavoastr&#259; sunt dezactivate &icirc;n mod implicit, v&#259; este recomandat s&#259; folosi&#355;i publicarea imediat&#259;: [vede&#355;i aici->@config_contenu@].</p><p>&Icirc;n caz contrar ve&#355;i fi nevoit s&#259; face&#355;i aceast&#259; activitate articol cu articol.</p>',
	'admin_spip_mots_cles_ok' => 'Cuvintele cheie sunt activate cu bine.',
	'admin_spip_mots_cles_warn' => '<p>{{Aten&#355;ie}} : Cuvintele cheie nu sunt activate &icirc;n SPIP, nu ve&#355;i putea folosi func&#355;iile avansate asociate.</p><p>V&#259; recomand&#259;m s&#259; le activa&#355;i : [vede&#355;i aici->@configuration@].</p>',
	'admin_spip_mots_forums_ok' => 'Cuvintele cheie asociate forum-urilor au fost activate cu bine.',
	'admin_spip_mots_forums_warn' => '<p>{{Aten&#355;ie}} : Cuvintele cheie &icirc;n forum-urile site-ului public nu sunt activate &icirc;n SPIP, nu ve&#355;i putea folosi func&#355;iile avansate asociate.</p><p>V&#259; recomand&#259;m s&#259; permite&#355;i folosirea lor: [vede&#355;i aici->@configuration@].</p>',
	'admin_spipbb_release' => 'Versiune SpipBB',
	'admin_statistique' => 'Informa&#355;ie',
	'admin_surtitre' => 'Gestiona&#355;i forum-urile',
	'admin_temps_deplacement' => 'Timp necesar &icirc;nainte de deplasare de c&#259;tre un administrator',
	'admin_titre' => 'Administrare SpipBB',
	'admin_titre_page_spipbb_admin' => 'Gestiunea forum-urilor',
	'admin_titre_page_spipbb_admin_anti_spam_config' => 'Configurare general&#259; a filtr&#259;rii spam-ului',
	'admin_titre_page_spipbb_admin_anti_spam_forum' => 'Post-uri marcate',
	'admin_titre_page_spipbb_admin_anti_spam_log' => 'Log-ul spam-ului',
	'admin_titre_page_spipbb_admin_anti_spam_words' => 'Filtrare cuvinte',
	'admin_titre_page_spipbb_admin_debug' => 'Debugging',
	'admin_titre_page_spipbb_admin_etat' => 'SpipBB - Administrare - Recapitulare',
	'admin_titre_page_spipbb_admin_gere_ban' => 'Gestiune interziceri',
	'admin_titre_page_spipbb_admin_migre' => 'Importul @nom_base@',
	'admin_titre_page_spipbb_configuration' => 'Configurare SpipBB',
	'admin_titre_page_spipbb_effacer' => 'Gestiunea mesajelor refuzate',
	'admin_titre_page_spipbb_inscrits' => 'Gestiune membri',
	'admin_total_posts' => 'Num&#259;r total de mesaje',
	'admin_total_users' => 'Num&#259;r de membri',
	'admin_total_users_online' => 'Membri online',
	'admin_unban_email_info' => 'Pute&#355;i permite mai multe adrese dintr-o dat&#259; folosing combina&#355;ia Ctrl sau Maj cu mouse-ul sau cu tastatura',
	'admin_unban_ip_info' => 'Pute&#355;i permite mai multe adrese dintr-o dat&#259; folosing combina&#355;ia Ctrl sau Maj cu mouse-ul sau cu tastatura',
	'admin_unban_user_info' => 'Pute&#355;i permite mai mul&#355;i utilizatori dintr-o dat&#259; folosing combina&#355;ia Ctrl sau Maj cu mouse-ul sau cu tastatura',
	'admin_valeur' => 'Valoare',
	'aecrit' => 'a scris:',
	'alerter_abus' => 'Semnala&#355;i acest mesaj ca fiind abuziv/injurios',
	'alerter_sujet' => 'Mesaj abuziv',
	'alerter_texte' => 'V&#259; atragem aten&#355;ia la mesajul urm&#259;tor:',
	'annonce' => 'Anun&#355;',
	'annonce_dpt' => 'Anun&#355;:',
	'anonyme' => 'Anonim',
	'auteur' => 'Autor',
	'avatar' => 'Avatar',

	// B
	'bouton_select_all' => 'Selec&#355;iona&#355;i tot',
	'bouton_unselect_all' => 'Deselec&#355;iona&#355;i tot',

	// C
	'champs_obligatoires' => 'C&acirc;mpurile marcate cu o stelu&#355;&#259; (*) sunt obligatorii.',
	'chercher' => 'C&#259;uta&#355;i',
	'choix_mots_annonce' => 'Face&#355;i un anun&#355;',
	'choix_mots_ferme' => 'Pentru a &icirc;nchide un fir',
	'choix_mots_postit' => 'Pune&#355;i &icirc;n Post-It',
	'choix_mots_selection' => 'Grupul de cuvinte trebuie s&#259; con&#355;in&#259; trei cuvinte cheie. &Icirc;n mod normal, plug-in-ul le-a creat &icirc;n momentul instal&#259;rii sale. SpipBB utilizeaz&#259; &icirc;n general cuvintele {ferme}, {annonce} &#351;i {postit}, dar pute&#355;i alege &#351;i alte cuvinte.',
	'choix_rubrique_selection' => 'Selec&#355;iona&#355;i un sector care va fi baza forum-urilor dumneavoastr&#259;. &Icirc;n&#259;untru, fiecare sub-rubric&#259; va fi un grup de forum-uri; fiecare articol publicat va deschide un forum.',
	'choix_squelettes' => 'Pute&#355;i s&#259; alege&#355;i &#351;i altele, dar fi&#351;ierele care &icirc;nlocuiesc groupeforum.html et filforum.html trebuie s&#259; existe !',
	'citer' => 'Cita&#355;i',
	'col_avatar' => 'Avatar',
	'col_date_crea' => 'Dat&#259; &icirc;nscriere',
	'col_marquer' => 'Marca&#355;i',
	'col_signature' => 'Semn&#259;tur&#259;',
	'config_affiche_champ_extra' => '<MODIF>Afi&#351;a&#355;i c&acirc;mpul: @nom_champ@',
	'config_affiche_extra' => 'Afi&#351;a&#355;i aceste c&acirc;mpuri &icirc;n schelete',
	'config_champs_auteur' => 'C&acirc;mpuri SpipBB',
	'config_champs_auteurs_plus' => 'Gestiune c&acirc;mpuri autori suplimentari',
	'config_champs_requis' => 'C&acirc;mpurile necesare lui SpipBB',
	'config_choix_mots' => 'Alege&#355;i grupul de cuvinte cheie',
	'config_choix_rubrique' => 'Alege&#355;i rubrica con&#355;in&acirc;nd forum-urile SpipBB',
	'config_choix_squelettes' => 'Alege&#355;i scheletele utilizate',
	'config_orig_extra' => 'Ce suport va fi folosit pentru c&acirc;mpurile suplimentare',
	'config_orig_extra_info' => 'Informa&#355;ii c&acirc;mpuri EXTRA sau alt&#259; tabel&#259;, tabel&#259; auteurs_profils.',
	'config_spipbb' => 'Configura&#355;ie de baz&#259; a SpipBB pentru a permite func&#355;ionarea forum-urilor cu acest plug-in.',
	'contacter' => 'Contacta&#355;i',
	'contacter_dpt' => 'Contacta&#355;i:',
	'creer_categorie' => 'Crea&#355;i Categorie Nou&#259;',
	'creer_forum' => 'Crea&#355;i un Nou Forum',

	// D
	'dans_forum' => '&icirc;n forum-ul',
	'deconnexion_' => 'Deconectare',
	'deplacer' => 'Deplasa&#355;i',
	'deplacer_confirmer' => 'Confirma&#355;i deplasarea',
	'deplacer_dans_dpt' => 'De deplasat &icirc;n forum-ul:',
	'deplacer_sujet_dpt' => 'Deplasarea lui:',
	'deplacer_vide' => 'Nu exist&#259; nici un alt forum: deplasare imposibil&#259;.',
	'dernier' => '&nbsp;Ultimul',
	'dernier_membre' => 'Ultimul membru &icirc;nregistrat',
	'derniers_messages' => 'Ultimele mesaje',
	'diviser' => 'Diviza&#355;i',
	'diviser_confirmer' => 'Confirma&#355;i separarea mesajelor',
	'diviser_dans_dpt' => 'De pus &icirc;n forum&nbsp;:',

	// E
	'ecrirea' => '<MODIF>Scrie&#355;i mail lui',
	'effacer' => '&#350;terge&#355;i',
	'email' => 'E-mail',
	'etplus' => '... &#351;&icirc; mai mult ...',

	// F
	'fil_deplace' => 'Deplasa&#355;i acest fir de discu&#355;ie',
	'forum_ferme' => '<MODIF>Acest forum este dezactivat',
	'forum_ferme_texte' => '<MODIF>Acest forum este dezactivat. Nu mai pute&#355;i contribui aici.',
	'forum_ouvrir' => 'Deschide&#355;i acest Forum',

	// H
	'haut_page' => '&Icirc;n susul paginii',

	// I
	'icone_ferme' => '&Icirc;nchide&#355;i',
	'info' => 'Informa&#355;ii',
	'info_confirmer_passe' => 'Confirma&#355;i aceast&#259; nou&#259; parol&#259;&nbsp;:',
	'info_plus_cinq_car' => 'mai mult de 5 caractere',

	// L
	'le' => '&nbsp; &nbsp;',
	'login' => 'Conectare',

	// M
	'maintenance' => '<MODIF>Nu uita&#355;i s&#259; &icirc;nl&#259;tura&#355;i op&#355;iunea <br />"&Icirc;nchidere pentru mentenan&#355;&#259;",<br /> &icirc;n forum-urile implicate.',
	'messages' => '<MODIF>Mesaje',
	'messages_derniers' => 'Ultimele mesaje',
	'messages_voir_dernier' => 'Vede&#355;i ultimul mesaj',
	'moderateurs' => 'Moderator(i)',

	// N
	'no_message' => 'Nici un subiect sau mesaj nu corespunde criteriilor de c&#259;utare indicate',

	// P
	'poste_valide' => 'Trimitere de validat ...',
	'profil' => '<MODIF>Profil de',

	// R
	'recherche_elargie' => 'C&#259;utare avansat&#259;',
	'reglement' => '<MODIF><p>Administratorii &#351;i moderatorii acestui forum &icirc;&#351;i dau silin&#355;a s&#259; &#351;tearg&#259; sau s&#259; modifice toate mesajele cu un carater nepotrivit &icirc;ntr-un timp c&acirc;t mai scurt. Cu toate acestea, este imposibil s&#259; revad&#259; toate mesajele. Admite&#355;i, a&#351;adar, c&#259; mesajele trimise pe aceste forum-uri exprim&#259; punctul de vedere &#351;i opinia autorilor lor respectivi, iar nu cea a administratorilor sau moderatorilor sau webmaster-ilor (cu excep&#355;ia celor trimise de ei &icirc;n&#351;i&#351;i) &#351;i c&#259; ace&#351;tia nu pot fi &#355;inu&#355;i responsabili, &icirc;n consecin&#355;&#259;.</p>
<p>Consim&#355;i&#355;i s&#259; nu trimite&#355;i mesaje injurioase, obscene, vulgare, difamatorii sau orice alt mesaj care ar &icirc;nc&#259;lca legile &icirc;n vigoare. &Icirc;nc&#259;lcarea acestui consemn v&#259; poate aduca la situa&#355;ia de a fi alungat imediat &#351;i de manier&#259; permanent&#259; c&acirc;t &#351;i la informarea furnizorului dumneavoastr&#259; de acces internet pe baza adresei IP &icirc;nregistrat&#259; tocmai pentru a face respectat&#259; aceste reguli. Sunte&#355;i de acord c&#259; webmaster-ul, administratorul sau moderatorul acestui forum au dreptul de a &#351;terge, edita, deplasa, bloca orice subiect de discu&#355;ie &icirc;n orice moment. Ca &#351;i utilizator sunte&#355;i de acord ca toate informa&#355;iile pe care le ve&#355;i furniza vor fi stocate &icirc;ntr-o baz&#259; de date. Aceste informa&#355;ii vor r&#259;m&acirc;ne confiden&#355;iale, &#351;i nu vor fi furnizate nici unei alte persoane f&#259;r&#259; acordul dumneavoastr&#259; prealabil.. Webmaster-ul, administratorul sau moderatorul nu vor fi &#355;inu&#355;i responsabili dac&#259; o eventual&#259; tentativ&#259; de piraterie informatic&#259; reu&#351;e&#351;te s&#259; acceseze aceste informa&#355;ii.</p>
<p>Acest forum utilizeaz&#259; cookies pentru a stoca informa&#355;ii pe calculatorul dumneavoastr&#259;. Aceste cookies nu vor con&#355;ine nici o informa&#355;ie introdus&#259; de dumneavoastr&#259;, &#351;i vor fi utilizate numai pentru a spori confortul dumneavoastr&#259; &icirc;n timpul utiliz&#259;rii acestui forum. Adresa de email este folosit&#259; doar pentru a confirma detaliile trimiterilor dumneavoastr&#259; ca &#351;i pentru a v&#259; trimite parola (&icirc;n cazul &icirc;n care, de exemplu, a&#355;i uitat-o).</p><p>&Icirc;nregistr&acirc;ndu-v&#259;, declara&#355;i astfel acordul dumneavoastr&#259; cu regulamentul de mai sus.</p>',
	'repondre' => 'R&#259;spunde&#355;i',
	'retour_forum' => '&Icirc;ntoarcere la &icirc;nceptul forum-ului',

	// S
	'secteur_forum' => '<MODIF>SECTOARE FORUM-URI',
	'selection_efface' => '<MODIF>&#350;terge&#355;i selec&#355;ia ...',
	'sinscrire' => '<MODIF>&icirc;nregistra&#355;i-v&#259;',
	'statut' => 'Statut',
	'sujet' => '<MODIF>Subiect&nbsp;',
	'sujet_auteur' => 'Autor',
	'sujet_clos_texte' => 'Acest subiect este &icirc;nchis, nu mai pute&#355;i contribui.',
	'sujet_clos_titre' => 'Subiect &icirc;nchis',
	'sujet_nombre' => 'Num&#259;r de Subiecte',
	'sujet_nouveau' => 'Subiect nou',
	'sujet_rejete' => 'Subiect refuzat',
	'sujet_valide' => 'Subiect acceptat',
	'sujets' => 'Subiecte',
	'sujets_aucun' => 'Nici un subiect disponibil, pentru moment, &icirc;n acest forum ',

	// T
	'tous_forums' => 'Toate forum-urile',

	// V
	'voir' => 'VEDE&#354;I'
);

?>
