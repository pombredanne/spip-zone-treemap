<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_propos_auteur_dpt' => 'Tout à propos de :', # NEW
	'admin_action_01_configuration' => 'Configurare',
	'admin_action_02_etat' => 'Starea forum-urilor',
	'admin_action_ZZ_debug' => 'Debugging',
	'admin_action_effacer' => 'Mesaje refuzate',
	'admin_action_fromphorum' => 'Import din Phorum',
	'admin_action_fromphpbb' => 'Import din PhpBB',
	'admin_action_gere_ban' => 'Gestiunea interzicerilor',
	'admin_action_gestion' => 'Gestiune',
	'admin_action_inscrits' => 'Membri',
	'admin_action_swconfig' => 'Configurare',
	'admin_action_swforum' => 'Post-uri marcate',
	'admin_action_swlog' => 'Log de spam',
	'admin_action_swwords' => 'Gestiune cuvinte',
	'admin_afficher_bouton_alerte_abus' => 'Afişaţi butoanele de alertă abuzuri',
	'admin_afficher_bouton_rss' => 'Afişati butoanele RSS',
	'admin_age_forum' => 'Vârsta forum-ului ani/luni',
	'admin_avatar_affiche' => 'Acceptaţi şi afişaţi avatar-urile (da în mod implicit la prima instalare)',
	'admin_avatar_taille_contact' => 'Mărimea avatar-urilor (în pixeli) pe pagina de contact',
	'admin_avatar_taille_profil' => 'Mărimea avatar-urilor (în pixeli) pe pagina de profil',
	'admin_avatar_taille_sujet' => 'Mărimea avatar-urilor (în pixeli) pe pagina subiecte',
	'admin_average_posts' => 'Media de mesaje/zi',
	'admin_average_users' => 'Media de înscrieri/zi',
	'admin_ban_email' => 'Gestiune de adrese email interzise',
	'admin_ban_email_info' => 'Pentru a specifica mai mult de o adresă email, separaţi-le prin virgule. Pentru a specifica un joker pentru numele de utilizator, utilizaţi *: de exemplu *@hotmail.com',
	'admin_ban_email_none' => 'Nici o adresă interzisă',
	'admin_ban_ip' => 'Gestiune adrese IP interzise',
	'admin_ban_ip_info' => 'Pentru a specifica mai multe IP-uri sau nume de servere diferite, separaţi-le prin virgule. Pentru a specifica o plajă de adrese IP, separaţi începutul şi sfârşitul cu o cratimă (-), pentru a specifica un joker folosiţi o stea (*)',
	'admin_ban_ip_none' => 'Nici o adresă interzisă',
	'admin_ban_user' => 'Gestiune login-uri interzise',
	'admin_ban_user_info' => 'Puteţi interzice mai mulţi utilizatori deodată prin folosirea tastei Ctrl sau Maj împreună cu mouse-ul sau tastatura',
	'admin_ban_user_none' => 'Nici un utilizator',
	'admin_cat_01_general' => 'Administrare',
	'admin_cat_outils' => 'Scule',
	'admin_cat_spam' => 'Anti Spam',
	'admin_config_prerequis' => 'Necesare',
	'admin_config_spam_words' => 'Configurare anti-spam',
	'admin_config_spipbb' => 'Activarea lui SpipBB',
	'admin_config_spipbb_info' => 'Apăsaţi pe Da pentru a activa SpipBB',
	'admin_config_tables' => 'Configurarea tabelelor lui SpipBB',
	'admin_config_tables_erreur' => 'Problemă cu tabelele lui SpipBB : @tables_erreur@ sunt incorecte (tabelele @tables_ok@ par a fi corecte).
 Consultaţi [documentaţia de la Spip-Contrib->http://www.spip-contrib.net/SpipBB-le-forum] sau [suportul pentru spipbb.spip-zone->http://spipbb.spip-zone.info/spip.php?article11]', # MODIF
	'admin_config_tables_ok' => 'Tabelele lui SpipBB sunt corect instalate (@tables_ok@)',
	'admin_date_ouverture' => 'Data deschiderii',
	'admin_debug_log' => 'Fişier de log @log_name@',
	'admin_debug_metas' => 'META-uri SpipBB',
	'admin_form_action' => 'Acţiune',
	'admin_form_creer_categorie' => 'Creaţi o categorie',
	'admin_form_creer_forum' => 'Creaţi un forum',
	'admin_form_deplacer' => 'Deplasaţi',
	'admin_form_descendre' => 'Coborâţi',
	'admin_form_editer' => 'Editaţi',
	'admin_form_messages' => ' ',
	'admin_form_monter' => 'Montaţi',
	'admin_form_sujets' => ' ',
	'admin_forums_affiche_membre_defaut' => 'Vreţi să afişaţi membrii în lista de membrii, chiar şi atunci când nu au făcut această alegere?<br />[ Nu în mod implicit ]',
	'admin_forums_configuration' => 'Configurare SpipBB',
	'admin_forums_configuration_avatar' => 'Gestinue avatar-uri, reglaj general',
	'admin_forums_configuration_options' => 'Opţiuni SpipBB',
	'admin_forums_log_level' => 'Alegere de nivel de log-uri produse de SpipBB.<br />[ 3 (maximum)- implicit ]',
	'admin_forums_log_level_0' => 'Nici un log',
	'admin_forums_log_level_1' => 'Un pic de log-uri',
	'admin_forums_log_level_2' => 'Multe log-uri',
	'admin_forums_log_level_3' => 'Enorm de multe log-uri',
	'admin_id_mjsc' => 'Număr',
	'admin_infos' => 'SpipBB - Administrare - Recapitulare',
	'admin_interface' => 'Opţiuni de interfaţă',
	'admin_nombre_lignes_messages' => 'Numărul de linii de mesaj',
	'admin_plugin_requis_erreur' => 'Plug-in necesar care lipseşte. Activaţi-l !',
	'admin_plugin_requis_erreur_balisesession' => 'Instalaţi plug-in-ul Balise SESSION şi activaţi-l ! [Documentaie aici->http://www.spip-contrib.net/?article1224], [Arhivă ZIP aici->http://files.spip.org/spip-zone/balise_session.zip].',
	'admin_plugin_requis_erreur_cfg' => 'Instalaţi plug-in-ul CFG şi activaţi-l ! [Documentaţie aici->http://www.spip-contrib.net/?article1605], [Arhivă ZIP aici->http://files.spip.org/spip-zone/cfg.zip].',
	'admin_plugin_requis_erreur_s' => 'Plug-in-uri necesare lipsesc. Activaţi-le !',
	'admin_plugin_requis_ok' => 'Plug-in(-uri) instalat(e) şi activ(e)',
	'admin_plugin_requis_ok_balisesession' => '[Plugin BALISE_SESSION->http://www.spip-contrib.net/?article1224] : fourni les informations sur les visiteurs authentifié.', # NEW
	'admin_plugin_requis_ok_cfg' => '[Plug-in CFG->http://www.spip-contrib.net/?article1605] : furnizează funcţii şi balize.',
	'admin_sous_titre' => 'Accesaţi panoul de administrare forum-uri cu SpipBB',
	'admin_spip_config_forums' => 'Configuraţie SPIP :',
	'admin_spip_forums_ok' => 'Forum-urile publice sunt activate cu bine.',
	'admin_spip_forums_warn' => '<p>{{Atenţie}} : forum-urile dumneavoastră sunt dezactivate în mod implicit, vă este recomandat să folosiţi publicarea imediată: [vedeţi aici->@config_contenu@].</p><p>În caz contrar veţi fi nevoit să faceţi această activitate articol cu articol.</p>',
	'admin_spip_mots_cles_ok' => 'Cuvintele cheie sunt activate cu bine.',
	'admin_spip_mots_cles_warn' => '<p>{{Atenţie}} : Cuvintele cheie nu sunt activate în SPIP, nu veţi putea folosi funcţiile avansate asociate.</p><p>Vă recomandăm să le activaţi : [vedeţi aici->@configuration@].</p>',
	'admin_spip_mots_forums_ok' => 'Cuvintele cheie asociate forum-urilor au fost activate cu bine.',
	'admin_spip_mots_forums_warn' => '<p>{{Atenţie}} : Cuvintele cheie în forum-urile site-ului public nu sunt activate în SPIP, nu veţi putea folosi funcţiile avansate asociate.</p><p>Vă recomandăm să permiteţi folosirea lor: [vedeţi aici->@configuration@].</p>',
	'admin_spipbb_release' => 'Versiune SpipBB',
	'admin_statistique' => 'Informaţie',
	'admin_surtitre' => 'Gestionaţi forum-urile',
	'admin_temps_deplacement' => 'Timp necesar înainte de deplasare de către un administrator',
	'admin_titre' => 'Administrare SpipBB',
	'admin_titre_page_spipbb_admin' => 'Gestiunea forum-urilor',
	'admin_titre_page_spipbb_admin_anti_spam_config' => 'Configurare generală a filtrării spam-ului',
	'admin_titre_page_spipbb_admin_anti_spam_forum' => 'Post-uri marcate',
	'admin_titre_page_spipbb_admin_anti_spam_log' => 'Log-ul spam-ului',
	'admin_titre_page_spipbb_admin_anti_spam_words' => 'Filtrare cuvinte',
	'admin_titre_page_spipbb_admin_debug' => 'Debugging',
	'admin_titre_page_spipbb_admin_etat' => 'SpipBB - Administrare - Recapitulare',
	'admin_titre_page_spipbb_admin_gere_ban' => 'Gestiune interziceri',
	'admin_titre_page_spipbb_admin_migre' => 'Importul @nom_base@',
	'admin_titre_page_spipbb_configuration' => 'Configurare SpipBB',
	'admin_titre_page_spipbb_effacer' => 'Gestiunea mesajelor refuzate',
	'admin_titre_page_spipbb_inscrits' => 'Gestiune membri',
	'admin_titre_page_spipbb_sujet' => 'Édition d\'un fil', # NEW
	'admin_total_posts' => 'Număr total de mesaje',
	'admin_total_users' => 'Număr de membri',
	'admin_total_users_online' => 'Membri online',
	'admin_unban_email_info' => 'Puteţi permite mai multe adrese dintr-o dată folosing combinaţia Ctrl sau Maj cu mouse-ul sau cu tastatura',
	'admin_unban_ip_info' => 'Puteţi permite mai multe adrese dintr-o dată folosing combinaţia Ctrl sau Maj cu mouse-ul sau cu tastatura',
	'admin_unban_user_info' => 'Puteţi permite mai mulţi utilizatori dintr-o dată folosing combinaţia Ctrl sau Maj cu mouse-ul sau cu tastatura',
	'admin_valeur' => 'Valoare',
	'aecrit' => 'a scris:',
	'alerter_abus' => 'Semnalaţi acest mesaj ca fiind abuziv/injurios',
	'alerter_sujet' => 'Mesaj abuziv',
	'alerter_texte' => 'Vă atragem atenţia la mesajul următor:',
	'annonce' => 'Anunţ',
	'annonce_dpt' => 'Anunţ:',
	'anonyme' => 'Anonim',
	'auteur' => 'Autor',
	'avatar' => 'Avatar',

	// B
	'bouton_select_all' => 'Selecţionaţi tot',
	'bouton_speciaux_sur_skels' => 'Configurer les boutons spécifiques sur les squelettes publics', # NEW
	'bouton_unselect_all' => 'Deselecţionaţi tot',

	// C
	'champs_obligatoires' => 'Câmpurile marcate cu o steluţă (*) sunt obligatorii.',
	'chercher' => 'Căutaţi',
	'choix_mots_annonce' => 'Faceţi un anunţ',
	'choix_mots_creation' => 'Si vous voulez créer <strong>automatiquement</strong> les mot-clés dédiés à SpipBB, appuyez sur ce bouton. Ces mot-clefs peuvent être modifiés ou supprimés ultérieurement...', # NEW
	'choix_mots_creation_submit' => 'Configuration auto des mots-clefs', # NEW
	'choix_mots_ferme' => 'Pentru a închide un fir',
	'choix_mots_postit' => 'Puneţi în Post-It',
	'choix_mots_selection' => 'Grupul de cuvinte trebuie să conţină trei cuvinte cheie. În mod normal, plug-in-ul le-a creat în momentul instalării sale. SpipBB utilizează în general cuvintele {ferme}, {annonce} şi {postit}, dar puteţi alege şi alte cuvinte.',
	'choix_rubrique_creation' => 'Si vous voulez créer <strong>automatiquement</strong> le secteur contenant les forums SpipBB et un premier forum vide, appuyez sur ce bouton. Ce forum et la hiérarchie créés peuvent être modifiés ou supprimés ultérieurement...', # NEW
	'choix_rubrique_creation_submit' => 'Configuration auto du secteur', # NEW
	'choix_rubrique_selection' => 'Selecţionaţi un sector care va fi baza forum-urilor dumneavoastră. Înăuntru, fiecare sub-rubrică va fi un grup de forum-uri; fiecare articol publicat va deschide un forum.',
	'choix_squelettes' => 'Puteţi să alegeţi şi altele, dar fişierele care înlocuiesc groupeforum.html et filforum.html trebuie să existe !',
	'citer' => 'Citaţi',
	'cocher' => 'cocher', # NEW
	'col_avatar' => 'Avatar',
	'col_date_crea' => 'Dată înscriere',
	'col_marquer' => 'Marcaţi',
	'col_signature' => 'Semnătură',
	'config_affiche_champ_extra' => 'Afişaţi câmpul: @nom_champ@', # MODIF
	'config_affiche_extra' => 'Afişaţi aceste câmpuri în schelete',
	'config_champs_auteur' => 'Câmpuri SpipBB',
	'config_champs_auteurs_plus' => 'Gestiune câmpuri autori suplimentari',
	'config_champs_requis' => 'Câmpurile necesare lui SpipBB',
	'config_choix_mots' => 'Alegeţi grupul de cuvinte cheie',
	'config_choix_rubrique' => 'Alegeţi rubrica conţinând forum-urile SpipBB',
	'config_choix_squelettes' => 'Alegeţi scheletele utilizate',
	'config_orig_extra' => 'Ce suport va fi folosit pentru câmpurile suplimentare',
	'config_orig_extra_info' => 'Informaţii câmpuri EXTRA sau altă tabelă, tabelă auteurs_profils.',
	'config_spipbb' => 'Configuraţie de bază a SpipBB pentru a permite funcţionarea forum-urilor cu acest plug-in.',
	'contacter' => 'Contactaţi',
	'contacter_dpt' => 'Contactaţi:',
	'creer_categorie' => 'Creaţi Categorie Nouă',
	'creer_forum' => 'Creaţi un Nou Forum',

	// D
	'dans_forum' => 'în forum-ul',
	'deconnexion_' => 'Deconectare',
	'deplacer' => 'Deplasaţi',
	'deplacer_confirmer' => 'Confirmaţi deplasarea',
	'deplacer_dans_dpt' => 'De deplasat în forum-ul:',
	'deplacer_sujet_dpt' => 'Deplasarea lui:',
	'deplacer_vide' => 'Nu există nici un alt forum: deplasare imposibilă.',
	'dernier' => ' Ultimul',
	'dernier_membre' => 'Ultimul membru înregistrat',
	'derniers_messages' => 'Ultimele mesaje',
	'diviser' => 'Divizaţi',
	'diviser_confirmer' => 'Confirmaţi separarea mesajelor',
	'diviser_dans_dpt' => 'De pus în forum :',
	'diviser_expliquer' => 'A l\'aide du formulaire ci-dessous, vous pourrez séparer ce fil en deux, soit : en sélectionnant les messages individuellement; soit en choissant le message à partir duquel il faut les diviser en deux.', # NEW
	'diviser_selection_dpt' => 'Sélection :', # NEW
	'diviser_separer_choisis' => 'Séparer les messages sélectionnés', # NEW
	'diviser_separer_suite' => 'Séparer à partir du message sélectionné', # NEW
	'diviser_vide' => 'Pas d\'autre forum : division impossible.', # NEW

	// E
	'ecrirea' => 'Scrieţi mail lui',
	'effacer' => 'Ştergeţi',
	'email' => 'E-mail',
	'en_ligne' => 'Qui est en ligne ?', # NEW
	'en_rep_sujet_' => ' ::: Sujet : ', # NEW
	'en_reponse_a' => 'En réponse au message', # NEW
	'etplus' => '... şî mai mult ...',
	'extra_avatar_saisie_url' => 'URL de votre avatar (http://... ...)', # NEW
	'extra_avatar_saisie_url_info' => 'URL de l\'avatar du visiteur', # NEW
	'extra_date_crea' => 'Date de premiere saisie profil SpipBB', # NEW
	'extra_date_crea_info' => 'Date de premiere saisie profil SpipBB', # NEW
	'extra_emploi' => 'Emploi', # NEW
	'extra_localisation' => 'Localisation', # NEW
	'extra_loisirs' => 'Loisirs', # NEW
	'extra_nom_aim' => 'Contacts chat (AIM)', # NEW
	'extra_nom_msnm' => 'Contacts chat (MSN Messenger)', # NEW
	'extra_nom_yahoo' => 'Contacts chat (Yahoo)', # NEW
	'extra_numero_icq' => 'Contacts chat (ICQ)', # NEW
	'extra_refus_suivi_thread' => '(refus suivi) Ne pas modifier !', # NEW
	'extra_refus_suivi_thread_info' => 'Liste des threads pour lesquels on ne souhaite plus recevoir de notification', # NEW
	'extra_signature_saisie_texte' => 'Saisir ici le texte de votre signature', # NEW
	'extra_signature_saisie_texte_info' => 'Court texte de signature des messages', # NEW
	'extra_visible_annuaire' => 'Apparaitre dans la liste des Inscrits (publique)', # NEW
	'extra_visible_annuaire_info' => 'Permet de refuser l\'affichage dans l\'annuaire des inscrits en zone publique', # NEW

	// F
	'fiche_contact' => 'Fiche Contact', # NEW
	'fil_annonce_annonce' => 'Passer le Sujet en Annonce', # NEW
	'fil_annonce_desannonce' => 'Supprimer le mode Annonce', # NEW
	'fil_deplace' => 'Deplasaţi acest fir de discuţie',
	'filtrer' => 'Filtrer', # NEW
	'forum' => 'Forums', # NEW
	'forum_annonce_annonce' => 'Poser le marquage d\'annonce', # NEW
	'forum_annonce_desannonce' => 'Supprimer le marquage d\'annonce', # NEW
	'forum_dpt' => 'Forum : ', # NEW
	'forum_ferme' => 'Acest forum este dezactivat',
	'forum_ferme_texte' => 'Acest forum este dezactivat. Nu mai puteţi contribui aici.',
	'forum_maintenance' => 'Ce forum est fermé pour maintenance', # NEW
	'forum_ouvrir' => 'Deschideţi acest Forum',
	'forums_categories' => 'Divers', # NEW
	'forums_spipbb' => 'Forums SpipBB', # NEW
	'forums_titre' => 'Mon premier forum créé', # NEW
	'fromphpbb_erreur_db_phpbb_config' => 'Impossible de lire la configuration dans la base phpBB', # NEW
	'fromphpbb_migre_categories' => 'Import des catégories', # NEW
	'fromphpbb_migre_categories_dans_rub_dpt' => 'Implantation des forums dans la rubrique :', # NEW
	'fromphpbb_migre_categories_forum' => 'Forum', # NEW
	'fromphpbb_migre_categories_groupe' => 'Groupe', # NEW
	'fromphpbb_migre_categories_impossible' => 'Impossible de récupérer les catégories', # NEW
	'fromphpbb_migre_categories_kw_ann_dpt' => 'Les annonces recevront le mot-clef :', # NEW
	'fromphpbb_migre_categories_kw_ferme_dpt' => 'Les sujets clos recevront le mot-clef :', # NEW
	'fromphpbb_migre_categories_kw_postit_dpt' => 'Les post its recevront le mot-clef :', # NEW
	'fromphpbb_migre_existe_dpt' => 'existe :', # NEW
	'fromphpbb_migre_thread' => 'Import des topics et des posts', # NEW
	'fromphpbb_migre_thread_ajout' => 'Ajout thread', # NEW
	'fromphpbb_migre_thread_annonce' => 'Annonce', # NEW
	'fromphpbb_migre_thread_existe_dpt' => 'Forum existe :', # NEW
	'fromphpbb_migre_thread_ferme' => 'Fermé', # NEW
	'fromphpbb_migre_thread_impossible_dpt' => 'Impossible de récupérer les posts :', # NEW
	'fromphpbb_migre_thread_postit' => 'Post-it', # NEW
	'fromphpbb_migre_thread_total_dpt' => 'Nombre total de topics et de posts ajoutés :', # NEW
	'fromphpbb_migre_utilisateurs' => 'Import des utilisateurs', # NEW
	'fromphpbb_migre_utilisateurs_admin_restreint_add' => 'Ajout admin restreint', # NEW
	'fromphpbb_migre_utilisateurs_admin_restreint_already' => 'Deja admin restreint', # NEW
	'fromphpbb_migre_utilisateurs_impossible' => 'Impossible de récupérer les utilisateurs', # NEW
	'fromphpbb_migre_utilisateurs_total_dpt' => 'Nombre total d\'utilisateurs ajoutés :', # NEW

	// H
	'haut_page' => 'În susul paginii',

	// I
	'icone_ferme' => 'Închideţi',
	'import_base' => 'Nom de la base :', # NEW
	'import_choix_test' => 'Réaliser un import de test (choix par défaut) :', # NEW
	'import_choix_test_titre' => 'Import à blanc ou réel', # NEW
	'import_erreur_db' => 'Impossible de se connecter à la base @nom_base@', # NEW
	'import_erreur_db_config' => 'Impossible de lire la configuration dans la base @nom_base@', # NEW
	'import_erreur_db_rappel_connexion' => 'Impossible de se reconnecter à la base @nom_base@', # NEW
	'import_erreur_db_spip' => 'Impossible de se connecter à la base SPIP', # NEW
	'import_erreur_forums' => 'Impossible de recuperer les forums', # NEW
	'import_fichier' => 'Fichier de configuration @nom_base@ trouvé :', # NEW
	'import_host' => 'Nom/adresse du serveur', # NEW
	'import_login' => 'Identifiant :', # NEW
	'import_parametres_base' => 'Choisissez soit le chemin vers le fichier de configuration de @nom_base@, soit de renseigner les paramètres d\'accès à la base contenant les forums de @nom_base@ :', # NEW
	'import_parametres_rubrique' => 'Choisissez la rubrique dans laquelle seront importés les forums de @nom_base@', # NEW
	'import_parametres_titre' => 'Informations sur la base @nom_base@', # NEW
	'import_password' => 'Mot de passe :', # NEW
	'import_prefix' => 'Préfixe des tables :', # NEW
	'import_racine' => 'Chemin vers @nom_base@ (avatars) :', # NEW
	'import_table' => 'Table de configuration @nom_base@ trouvée :', # NEW
	'import_titre' => 'Import d\'un forum @nom_base@', # NEW
	'import_titre_etape' => 'Import d\'un forum  @nom_base@ - étape', # NEW
	'info' => 'Informaţii',
	'info_annonce_ferme' => 'Etat Annonce / Fermer', # NEW
	'info_confirmer_passe' => 'Confirmaţi această nouă parolă :',
	'info_ferme' => 'Etat Fermé', # NEW
	'info_inscription_invalide' => 'Inscription impossible', # NEW
	'info_plus_cinq_car' => 'mai mult de 5 caractere',
	'infos_refus_suivi_sujet' => 'Ne plus suivre les sujets', # NEW
	'infos_suivi_forum_par_inscription' => 'Suivi du forum par inscription', # NEW
	'inscription' => 'Inscription', # NEW
	'inscrit_le' => 'Inscrit le', # NEW
	'inscrit_le_dpt' => 'Inscrit le :', # NEW
	'inscrit_s' => 'Inscrits', # NEW
	'ip_adresse_autres' => 'Autres adresses IP à partir desquelles cet auteur a posté', # NEW
	'ip_adresse_membres' => 'Membres ayant posté de cette adresse IP', # NEW
	'ip_adresse_post' => 'Adresse IP de ce message', # NEW
	'ip_informations' => 'Informations sur une adresse IP et un auteur', # NEW

	// L
	'le' => '   ',
	'liste_des_messages' => 'Liste des messages', # NEW
	'liste_inscrits' => 'Liste des membres', # NEW
	'login' => 'Conectare',

	// M
	'maintenance' => 'Nu uitaţi să înlăturaţi opţiunea <br />"Închidere pentru mentenanţă",<br /> în forum-urile implicate.', # MODIF
	'maintenance_fermer' => 'a fermé l\'article/forum :', # NEW
	'maintenance_pour' => 'pour MAINTENANCE.', # NEW
	'membres_en_ligne' => 'membres en ligne', # NEW
	'membres_inscrits' => 'membres inscrits', # NEW
	'membres_les_plus_actifs' => 'Membres les plus actifs', # NEW
	'message' => 'Message', # NEW
	'message_s' => 'Messages', # NEW
	'message_s_dpt' => 'Messages : ', # NEW
	'messages' => 'Mesaje',
	'messages_anonymes' => 'rendre anonymes', # NEW
	'messages_derniers' => 'Ultimele mesaje',
	'messages_laisser_nom' => 'laisser le nom', # NEW
	'messages_supprimer_titre_dpt' => 'Pour les messages :', # NEW
	'messages_supprimer_tous' => 'les supprimer', # NEW
	'messages_voir_dernier' => 'Vedeţi ultimul mesaj',
	'messages_voir_dernier_s' => 'Voir les derniers messages', # NEW
	'moderateur' => 'Modérateur', # NEW
	'moderateur_dpt' => 'Modérateur : ', # NEW
	'moderateurs' => 'Moderator(i)',
	'moderateurs_dpt' => 'Modérateurs : ', # NEW
	'modif_parametre' => 'Modifiez vos paramètres', # NEW
	'mot_annonce' => 'Annonce
_ Une annonce est située en tête de forum sur toutes les pages.', # NEW
	'mot_ferme' => 'Fermé
-* Lorsqu\'un article-forum a ce mot-clef, seul les modérateurs peuvent y ajouter des messages.
-* Lorsqu\'un sujet de forum est ferm&ecute;, seuls les modérateurs peuvent y ajouter des réponses.', # NEW
	'mot_groupe_moderation' => 'Goupe de mot-clefs utilisé pour la modération de SpipBB', # NEW
	'mot_postit' => 'Postit
_ Un postit est situé en dessous des annonces, avant les messages ordinaires. Il n\'appara&icirc;t qu\'une seule fois dans la liste.', # NEW

	// N
	'no_message' => 'Nici un subiect sau mesaj nu corespunde criteriilor de căutare indicate',
	'nom_util' => 'Nom d\'utilisateur', # NEW
	'non' => 'Non', # NEW

	// O
	'ordre_croissant' => 'Croissant', # NEW
	'ordre_decroissant' => 'Décroissant', # NEW
	'ordre_dpt' => 'Ordre :', # NEW
	'oui' => 'Oui', # NEW

	// P
	'pagine_page_' => ' .. page ', # NEW
	'pagine_post_' => ' réponse', # NEW
	'pagine_post_s' => ' réponses', # NEW
	'pagine_sujet_' => ' sujet', # NEW
	'pagine_sujet_s' => ' sujets', # NEW
	'par_' => 'par ', # NEW
	'plugin_auteur' => 'La SpipBB Team : [voir la liste des contributeurs sur Spip-contrib->http://www.spip-contrib.net/Plugin-SpipBB#contributeurs]', # NEW
	'plugin_description' => 'Le plugin SpipBB permet :
-* De gérer de façon centralisée les forums de SPIP (interface privée),
-* D\'utiliser un secteur comme base d\'un groupe de forums comme les «Bulletin Board» tels que phpBB. Dans ce secteur, les sous-rubriques sont des groupes de forums, les articles des forums, chaque message dans le forum d\'un article y démarre un thread.

{{Consultez :}}
-* •[l\'aide et support sur spipbb.spip-zone.info->http://spipbb.free.fr/spip.php?article11],
-* •[La documentation sur Spip-contrib->http://www.spip-contrib.net/SpipBB-le-forum].

_ {{Plugin spipbb en cours de developpement. Vous l\'utilisez à vos risques et périls}}

_ [Accès au panneau d\'administration-> .?exec=spipbb_configuration]', # NEW
	'plugin_licence' => 'Distribué sous licence GPL', # NEW
	'plugin_lien' => '[Consulter la documentation du plugin sur Spip-contrib->http://www.spip-contrib.net/SpipBB-le-forum]', # NEW
	'plugin_mauvaise_version' => 'Cette version du plugin n\'est pas compatible avec votre version de SPIP !', # NEW
	'plugin_nom' => 'SpipBB : Gestion des forums de SPIP', #  Pour faciliter les traductions de plugin.xml NEW
	'post_aucun_pt' => 'aucun !', # NEW
	'post_efface_lui' => 'Ce sujet comprend @nbr_post@ message(s). Effacés avec lui !\\n', # NEW
	'post_ip' => 'Messages posté à partie de l\'adresse IP', # NEW
	'post_propose' => 'Message proposé', # NEW
	'post_rejete' => 'Message rejeté', # NEW
	'post_titre' => ' ::: Titre : ', # NEW
	'post_verifier_sujet' => 'Vérifier ce sujet', # NEW
	'poste_valide' => 'Trimitere de validat ...',
	'poster_date_' => 'Posté le : ', # NEW
	'poster_message' => 'Poster un message', # NEW
	'postit' => 'Postit', # NEW
	'postit_dpt' => 'Postit : ', # NEW
	'posts_effaces' => 'Messages effacés !', # NEW
	'posts_refuses' => 'Messages refusés, à effacer !', # NEW
	'previsualisation' => 'Prévisualisation', # NEW
	'profil' => 'Profil de', # MODIF

	// R
	'raison_clic' => 'cliquez ici', # NEW
	'raison_texte' => 'Pour en connaitre la raison', # NEW
	'recherche' => 'Recherche', # NEW
	'recherche_elargie' => 'Căutare avansată',
	'redige_post' => 'Ecrire message', # NEW
	'reglement' => '<p>Administratorii şi moderatorii acestui forum îşi dau silinţa să şteargă sau să modifice toate mesajele cu un carater nepotrivit într-un timp cât mai scurt. Cu toate acestea, este imposibil să revadă toate mesajele. Admiteţi, aşadar, că mesajele trimise pe aceste forum-uri exprimă punctul de vedere şi opinia autorilor lor respectivi, iar nu cea a administratorilor sau moderatorilor sau webmaster-ilor (cu excepţia celor trimise de ei înşişi) şi că aceştia nu pot fi ţinuţi responsabili, în consecinţă.</p>
<p>Consimţiţi să nu trimiteţi mesaje injurioase, obscene, vulgare, difamatorii sau orice alt mesaj care ar încălca legile în vigoare. Încălcarea acestui consemn vă poate aduca la situaţia de a fi alungat imediat şi de manieră permanentă cât şi la informarea furnizorului dumneavoastră de acces internet pe baza adresei IP înregistrată tocmai pentru a face respectată aceste reguli. Sunteţi de acord că webmaster-ul, administratorul sau moderatorul acestui forum au dreptul de a şterge, edita, deplasa, bloca orice subiect de discuţie în orice moment. Ca şi utilizator sunteţi de acord ca toate informaţiile pe care le veţi furniza vor fi stocate într-o bază de date. Aceste informaţii vor rămâne confidenţiale, şi nu vor fi furnizate nici unei alte persoane fără acordul dumneavoastră prealabil.. Webmaster-ul, administratorul sau moderatorul nu vor fi ţinuţi responsabili dacă o eventuală tentativă de piraterie informatică reuşeşte să acceseze aceste informaţii.</p>
<p>Acest forum utilizează cookies pentru a stoca informaţii pe calculatorul dumneavoastră. Aceste cookies nu vor conţine nici o informaţie introdusă de dumneavoastră, şi vor fi utilizate numai pentru a spori confortul dumneavoastră în timpul utilizării acestui forum. Adresa de email este folosită doar pentru a confirma detaliile trimiterilor dumneavoastră ca şi pentru a vă trimite parola (în cazul în care, de exemplu, aţi uitat-o).</p><p>Înregistrându-vă, declaraţi astfel acordul dumneavoastră cu regulamentul de mai sus.</p>', # MODIF
	'repondre' => 'Răspundeţi',
	'reponse_s_' => 'Réponses', # NEW
	'resultat_s_pour_' => ' résultats pour ', # NEW
	'retour_forum' => 'Întoarcere la începtul forum-ului',

	// S
	's_abonner_a' => 'RSS . S\'abonner à : ', # NEW
	'secteur_forum' => 'SECTOARE FORUM-URI', # MODIF
	'selection_efface' => 'Ştergeţi selecţia ...',
	'selection_tri_dpt' => 'Sélectionner la méthode de tri :', # NEW
	'sign_admin' => '{{Cette page est uniquement accessible aux responsables du site.}}<p>Elle donne accès à la configuration du plugin «{{<a href="http://www.spip-contrib.net/Plugin-SpipBB#contributeurs" class="copyright">SpipBB</a>}}» ainsi qu\'à la gestion des forums du site.</p><p>Version : @version@ @distant@</p><p>Consultez :
_ • [La documentation sur Spip-Contrib->http://www.spip-contrib.net/?article2460]
_ • [L\'aide et support sur spipbb.spip-zone.info->http://spipbb.free.fr/spip.php?article11]</p>@reinit@', # NEW
	'sign_maj' => '<br />Version plus récente disponible : @version@', # NEW
	'sign_ok' => 'à jour.', # NEW
	'sign_reinit' => '<p>Ré-initialisation :
_ • [de tout le plugin->@plugin@]</p>', # NEW
	'sign_tempo' => 'Réalisé avec <a href="http://www.spip-contrib.net/Plugin-SpipBB#contributeurs" class="copyright">SpipBB</a>', # NEW
	'signature' => 'Signature', # NEW
	'sinscrire' => 'înregistraţi-vă', # MODIF
	'site_propose' => 'Site proposé par @auteur_post@', # NEW
	'site_web' => 'Site web', # NEW
	'squelette_filforum' => 'Base de squelette pour les fils de discussions :', # NEW
	'squelette_groupeforum' => 'Base de squelette pour les groupes de discussions :', # NEW
	'statut' => 'Statut',
	'statut_admin' => 'Administrateur', # NEW
	'statut_redac' => 'Rédacteur', # NEW
	'statut_visit' => 'Membre', # NEW
	'sujet' => 'Subiect ', # MODIF
	'sujet_auteur' => 'Autor',
	'sujet_clos_texte' => 'Acest subiect este închis, nu mai puteţi contribui.',
	'sujet_clos_titre' => 'Subiect închis',
	'sujet_dpt' => 'Sujet : ', # NEW
	'sujet_ferme' => 'Sujet : fermé', # NEW
	'sujet_nombre' => 'Număr de Subiecte',
	'sujet_nouveau' => 'Subiect nou',
	'sujet_rejete' => 'Subiect refuzat',
	'sujet_repondre' => 'Répondre au sujet', # NEW
	'sujet_s' => 'Sujets', # NEW
	'sujet_valide' => 'Subiect acceptat',
	'sujets' => 'Subiecte',
	'sujets_aucun' => 'Nici un subiect disponibil, pentru moment, în acest forum ',
	'support_extra_normal' => 'extra', # NEW
	'support_extra_table' => 'table', # NEW
	'supprimer' => 'Supprimer', # NEW
	'sw_admin_can_spam' => 'Les admins sont autorisés', # NEW
	'sw_admin_no_spam' => 'Pas de spam', # NEW
	'sw_ban_ip_titre' => 'Bannir l\'IP en même temps', # NEW
	'sw_config_exceptions' => 'Vous pouvez activer des exceptions pour des utilisateurs privilégiés ici. Ils pourront quand même publier avec des mots bannis.', # NEW
	'sw_config_exceptions_titre' => 'Gestion des exceptions', # NEW
	'sw_config_generale' => 'Configuration actuelle du filtrage :', # NEW
	'sw_config_generale_titre' => 'Configuration générale du filtrage du spam', # NEW
	'sw_config_warning' => 'Vous pouvez choisir le texte du MP envoyé si vous activez cette option (maxi 255 caractères).', # NEW
	'sw_config_warning_titre' => 'Configuration des avertissements par message privé', # NEW
	'sw_disable_sw_titre' => '<strong>Active le filtrage</strong><br />Si vous devez vous passer du filtrage,<br />cliquez sur Non', # NEW
	'sw_modo_can_spam' => 'Les modérateurs sont autorisés', # NEW
	'sw_nb_spam_ban_titre' => 'Nombre de spams avant banissement', # NEW
	'sw_pm_spam_warning_message' => 'Ceci est un avertissement. Vous avez essayé de poster un message analysé comme du spam sur ce site web. Merci d\'éviter de recommencer.', # NEW
	'sw_pm_spam_warning_titre' => 'Attention.', # NEW
	'sw_send_pm_warning' => '<strong>Envoie un MP à l\'utilisateur</strong><br />lorsqu\'il poste un message avec un mot interdit', # NEW
	'sw_spam_forum_titre' => 'Gestion des messages de spam', # NEW
	'sw_spam_titre' => 'Filtrage du spam', # NEW
	'sw_spam_words_action' => 'A partir de cette page, vous pouvez ajouter, éditer et supprimer des mots associés à du spam. Le caractère (*) est accepté dans le mot. Par exemple : {{*tes*}} capturera {détestable}, {{tes*}} capturera {tester}, {{*tes}} capturera {portes}.', # NEW
	'sw_spam_words_mass_add' => 'Copier-coller ou saisir vos mots dans cette zone. Séparer chaque mot par une virgule, deux points ou un retour à la ligne.', # NEW
	'sw_spam_words_titre' => 'Filtrage de mots', # NEW
	'sw_spam_words_url_add' => 'Saisir l\'URL d\'un fichier contenant une liste de mots formatée comme ci-dessus. Exemple : http://spipbb.free.fr/IMG/csv/spamwordlist.csv .', # NEW
	'sw_warning_from_admin' => 'Choisir l\'admin auteur du message envoyé', # NEW
	'sw_warning_pm_message' => 'Texte du message privé', # NEW
	'sw_warning_pm_titre' => 'Sujet du message privé', # NEW
	'sw_word' => 'Mot', # NEW

	// T
	'title_ferme' => 'Fermer le forum/article', # NEW
	'title_libere' => 'Réouvrir le forum/article', # NEW
	'title_libere_maintenance' => 'Libérer le verrou de Maintenance', # NEW
	'title_maintenance' => 'Fermer le forum/article pour Maintenance', # NEW
	'title_sujet_ferme' => 'Fermer ce sujet', # NEW
	'title_sujet_libere' => 'Réouvrir ce sujet', # NEW
	'titre_spipbb' => 'SpipBB', # NEW
	'total_membres' => 'Nous avons un total de ', # NEW
	'total_messages_membres' => 'Nos membres ont posté un total de ', # NEW
	'tous' => 'Tous', # NEW
	'tous_forums' => 'Toate forum-urile',
	'trier' => 'Trier', # NEW
	'trouver_messages_auteur_dpt' => 'Trouver tous les messages de :', # NEW

	// V
	'visible_annuaire_forum' => 'Apparaitre dans la liste des Inscrits', # NEW
	'visites' => 'Vu', # NEW
	'voir' => 'VEDEŢI',
	'votre_bio' => 'Courte biographie en quelques mots.', # NEW
	'votre_email' => 'Votre adresse email', # NEW
	'votre_nouveau_passe' => 'Nouveau mot de passe', # NEW
	'votre_signature' => 'Votre signature', # NEW
	'votre_site' => 'Le nom de votre site', # NEW
	'votre_url_avatar' => 'URL de votre Avatar (http://...)', # NEW
	'votre_url_site' => 'L\'adresse (URL) de votre site' # NEW
);

?>
