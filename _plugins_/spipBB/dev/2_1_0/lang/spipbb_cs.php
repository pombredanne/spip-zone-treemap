<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_propos_auteur_dpt' => 'Tout à propos de :', # NEW
	'admin_action_01_configuration' => 'Configuration', # NEW
	'admin_action_02_etat' => 'Etat des forums', # NEW
	'admin_action_ZZ_debug' => 'Debogage', # NEW
	'admin_action_effacer' => 'Messages rejetés', # NEW
	'admin_action_fromphorum' => 'Import de Phorum', # NEW
	'admin_action_fromphpbb' => 'Import de PhpBB', # NEW
	'admin_action_gere_ban' => 'Gestion des bans', # NEW
	'admin_action_gestion' => 'Gestion', # NEW
	'admin_action_inscrits' => 'Membres', # NEW
	'admin_action_swconfig' => 'Configuration', # NEW
	'admin_action_swforum' => 'Posts marqués', # NEW
	'admin_action_swlog' => 'Log du spam', # NEW
	'admin_action_swwords' => 'Gestion des mots', # NEW
	'admin_afficher_bouton_alerte_abus' => 'Afficher les boutons alerte Abus', # NEW
	'admin_afficher_bouton_rss' => 'Afficher les boutons RSS', # NEW
	'admin_age_forum' => 'Age du forum Ans/mois', # NEW
	'admin_avatar_affiche' => 'Accepter et afficher les avatars (oui par défaut en prem install)', # NEW
	'admin_avatar_taille_contact' => 'Taille des avatars (en pixels) sur page contact', # NEW
	'admin_avatar_taille_profil' => 'Taille des avatars (en pixels) sur page profil', # NEW
	'admin_avatar_taille_sujet' => 'Taille des avatars (en pixels) sur page sujets', # NEW
	'admin_average_posts' => 'Moyenne de messages/jour', # NEW
	'admin_average_users' => 'Moyenne d\'inscriptions/jour', # NEW
	'admin_ban_email' => 'Gestion des adresses email bannies', # NEW
	'admin_ban_email_info' => 'Pour spécifier plus d\'une adresse e-mail, séparez-les par des virgules. Pour spécifier un joker pour le nom d\'utilisateur, utilisez * ; par exemple *@hotmail.com', # NEW
	'admin_ban_email_none' => 'Aucune adresse bannie', # NEW
	'admin_ban_ip' => 'Gestion des adresses IP bannies', # NEW
	'admin_ban_ip_info' => 'Pour spécifier plusieurs IP ou noms de serveurs différents, séparez-les par des virgules. Pour spécifier un intervalle d\'adresses IP, séparez le début et la fin avec un trait d\'union (-), pour spécifier un joker, utilisez une étoile (*)', # NEW
	'admin_ban_ip_none' => 'Aucune adresse bannie', # NEW
	'admin_ban_user' => 'Gestion des login bannis', # NEW
	'admin_ban_user_info' => 'Vous pouvez bannir plusieurs utilisateurs en une fois en utilisant la combinaison CTRL ou MAJ avec la souris ou le clavier', # NEW
	'admin_ban_user_none' => 'Aucun utilisateur', # NEW
	'admin_cat_01_general' => 'Administration', # NEW
	'admin_cat_outils' => 'Outils', # NEW
	'admin_cat_spam' => 'Anti Spam', # NEW
	'admin_config_prerequis' => 'Prérequis', # NEW
	'admin_config_spam_words' => 'Configuration de l\'anti-spam', # NEW
	'admin_config_spipbb' => 'Activation de SpipBB', # NEW
	'admin_config_spipbb_info' => 'Cliquer sur Oui pour activer SpipBB', # NEW
	'admin_config_tables' => 'Configuration des tables de SpipBB', # NEW
	'admin_config_tables_erreur' => 'Problème avec les tables de SpipBB : @tables_erreur@ sont incorrectes (les tables @tables_ok@ semblent correctes).
Consultez la [documentation sur Spip-Contrib->http://www.spip-contrib.net/SpipBB-le-forum] ou le [support sur spipbb.spip-zone->http://spipbb.free.fr/spip.php?article11]', # NEW
	'admin_config_tables_ok' => 'Les tables de SpipBB sont correctement installées (@tables_ok@)', # NEW
	'admin_date_ouverture' => 'Date d\'ouverture', # NEW
	'admin_debug_log' => 'Fichier de log @log_name@', # NEW
	'admin_debug_metas' => 'SpipBB METAs', # NEW
	'admin_form_action' => 'Action', # NEW
	'admin_form_creer_categorie' => 'Créer une catégorie', # NEW
	'admin_form_creer_forum' => 'Créer un forum', # NEW
	'admin_form_deplacer' => 'Déplacer', # NEW
	'admin_form_descendre' => 'Descendre', # NEW
	'admin_form_editer' => 'Editer', # NEW
	'admin_form_messages' => ' ', # NEW
	'admin_form_monter' => 'Monter', # NEW
	'admin_form_sujets' => ' ', # NEW
	'admin_forums_affiche_membre_defaut' => 'Voulez-vous afficher les membres dans la liste de membres lorsqu\'il n\'ont pas fait de choix ?<br />[ Non par défaut ]', # NEW
	'admin_forums_configuration' => 'Configuration de SpipBB', # NEW
	'admin_forums_configuration_avatar' => 'Gestion des avatars, réglage général', # NEW
	'admin_forums_configuration_options' => 'Options de SpipBB', # NEW
	'admin_forums_log_level' => 'Choix du niveau de logs produites par SpipBB.<br />[ 3 (maximum)- Par défaut ]', # NEW
	'admin_forums_log_level_0' => 'Pas de logs', # NEW
	'admin_forums_log_level_1' => 'Un peu de logs', # NEW
	'admin_forums_log_level_2' => 'Beaucoup de logs', # NEW
	'admin_forums_log_level_3' => 'Enormément de logs', # NEW
	'admin_id_mjsc' => 'N°', # NEW
	'admin_infos' => 'SpipBB - Administration - Récapitulatif', # NEW
	'admin_interface' => 'Options de l\'interface', # NEW
	'admin_nombre_lignes_messages' => 'Nombre de lignes de messages', # NEW
	'admin_plugin_requis_erreur' => 'Le plugin requis suivant manque. Activez-le !', # NEW
	'admin_plugin_requis_erreur_balisesession' => 'Installez le plugin Balise SESSION et activez le ! [Documentation ici->http://www.spip-contrib.net/?article1224], [Archive ZIP là->http://files.spip.org/spip-zone/balise_session.zip].', # NEW
	'admin_plugin_requis_erreur_cfg' => 'Installez le plugin CFG et activez le ! [Documentation ici->http://www.spip-contrib.net/?article1605], [Archive ZIP là->http://files.spip.org/spip-zone/cfg.zip].', # NEW
	'admin_plugin_requis_erreur_s' => 'Les plugins requis suivants manquent. Activez-les !', # NEW
	'admin_plugin_requis_ok' => 'Plugin(s)  installé(s) et actif(s) :', # NEW
	'admin_plugin_requis_ok_balisesession' => '[Plugin BALISE_SESSION->http://www.spip-contrib.net/?article1224] : fourni les informations sur les visiteurs authentifié.', # NEW
	'admin_plugin_requis_ok_cfg' => '[Plugin CFG->http://www.spip-contrib.net/?article1605] : fourni des fonctions et des balises.', # NEW
	'admin_sous_titre' => 'Accèder au panneau d\'administration des forums avec SpipBB', # NEW
	'admin_spip_config_forums' => 'Configuration de SPIP :', # NEW
	'admin_spip_forums_ok' => 'Les forums publics sont bien activés.', # NEW
	'admin_spip_forums_warn' => '<p>{{Attention}} : vos forums sont désactivés par défaut, il vous est recommandé d\'utiliser la publication immédiate : [voir ici->@config_contenu@].</p><p>Sinon vous devrez les activer articles par articles.</p>', # NEW
	'admin_spip_mots_cles_ok' => 'Les mot-clefs sont bien activés.', # NEW
	'admin_spip_mots_cles_warn' => '<p>{{Attention}} : Les mots-clés sont pas actifs dans SPIP, vous ne pourrez pas utiliser les fonctions avancées associées.</p><p>Il vous est recommandé de les activer : [voir ici->@configuration@].</p>', # NEW
	'admin_spip_mots_forums_ok' => 'Les mot-clefs associés aux forums sont bien activés.', # NEW
	'admin_spip_mots_forums_warn' => '<p>{{Attention}} : Les mots-clés dans les forums du site public sont pas actifs dans SPIP, vous ne pourrez pas utiliser les fonctions avancées associées.</p><p>Il vous est recommandé de permettre leur utilisation : [voir ici->@configuration@].</p>', # NEW
	'admin_spipbb_release' => 'Version de SpipBB', # NEW
	'admin_statistique' => 'Information', # NEW
	'admin_surtitre' => 'Gérer les forums', # NEW
	'admin_temps_deplacement' => 'Temps requis avant déplacement par un admin', # NEW
	'admin_titre' => 'Administration SpipBB', # NEW
	'admin_titre_page_spipbb_admin' => 'Gestion des forums', # NEW
	'admin_titre_page_spipbb_admin_anti_spam_config' => 'Configuration générale du filtrage du spam', # NEW
	'admin_titre_page_spipbb_admin_anti_spam_forum' => 'Posts marqués', # NEW
	'admin_titre_page_spipbb_admin_anti_spam_log' => 'Log du spam', # NEW
	'admin_titre_page_spipbb_admin_anti_spam_words' => 'Filtrage de mots', # NEW
	'admin_titre_page_spipbb_admin_debug' => 'Debogage', # NEW
	'admin_titre_page_spipbb_admin_etat' => 'SpipBB - Administration - Récapitulatif', # NEW
	'admin_titre_page_spipbb_admin_gere_ban' => 'Gestion du banissement', # NEW
	'admin_titre_page_spipbb_admin_migre' => 'Import de @nom_base@', # NEW
	'admin_titre_page_spipbb_configuration' => 'Configuration de SpipBB', # NEW
	'admin_titre_page_spipbb_effacer' => 'Gestion des messages rejetés', # NEW
	'admin_titre_page_spipbb_inscrits' => 'Gestion des membres', # NEW
	'admin_titre_page_spipbb_sujet' => 'Édition d\'un fil', # NEW
	'admin_total_posts' => 'Nombre total de messages', # NEW
	'admin_total_users' => 'Nombre de membres', # NEW
	'admin_total_users_online' => 'Membres en ligne', # NEW
	'admin_unban_email_info' => 'Vous pouvez débannir plusieurs adresses en une fois en utilisant la combinaison CTRL ou MAJ avec la souris ou le clavier', # NEW
	'admin_unban_ip_info' => 'Vous pouvez débannir plusieurs adresses en une fois en utilisant la combinaison CTRL ou MAJ avec la souris ou le clavier', # NEW
	'admin_unban_user_info' => 'Vous pouvez débannir plusieurs utilisateurs en une fois en utilisant la combinaison CTRL ou MAJ avec la souris ou le clavier', # NEW
	'admin_valeur' => 'Valeur', # NEW
	'aecrit' => 'napsal/a',
	'alerter_abus' => 'Signaler ce message comme abusif/injurieux...', # NEW
	'alerter_sujet' => 'Message abusif', # NEW
	'alerter_texte' => 'Nous attirons votre attention sur le message suivant :', # NEW
	'annonce' => 'Oznámení',
	'annonce_dpt' => 'Annonce : ', # NEW
	'anonyme' => 'Anonym',
	'auteur' => 'Auteur', # NEW
	'avatar' => 'Avatar',

	// B
	'bouton_select_all' => 'Tout sélectionner', # NEW
	'bouton_speciaux_sur_skels' => 'Configurer les boutons spécifiques sur les squelettes publics', # NEW
	'bouton_unselect_all' => 'Tout dé-sélectionner', # NEW

	// C
	'champs_obligatoires' => 'Les champs marqués d\'une * sont obligatoires.', # NEW
	'chercher' => 'Chercher', # NEW
	'choix_mots_annonce' => 'Faire une annonce', # NEW
	'choix_mots_creation' => 'Si vous voulez créer <strong>automatiquement</strong> les mot-clés dédiés à SpipBB, appuyez sur ce bouton. Ces mot-clefs peuvent être modifiés ou supprimés ultérieurement...', # NEW
	'choix_mots_creation_submit' => 'Configuration auto des mots-clefs', # NEW
	'choix_mots_ferme' => 'Pour fermer un fil', # NEW
	'choix_mots_postit' => 'Mettre en postit', # NEW
	'choix_mots_selection' => 'Le groupe de mot doit contenir trois mot-clefs. Normalement, le plugin les a créé au moment de son installation. SpipBB utilise en général les mots {ferme}, {annonce} et {postit}, mais vous pouvez en choisir d\'autres.', # NEW
	'choix_rubrique_creation' => 'Si vous voulez créer <strong>automatiquement</strong> le secteur contenant les forums SpipBB et un premier forum vide, appuyez sur ce bouton. Ce forum et la hiérarchie créés peuvent être modifiés ou supprimés ultérieurement...', # NEW
	'choix_rubrique_creation_submit' => 'Configuration auto du secteur', # NEW
	'choix_rubrique_selection' => 'Sélectionner un secteur qui sera la base de vos forums. Dedans, chaque sous-rubrique sera un groupe de forums, chaque article publié ouvrira un forum.', # NEW
	'choix_squelettes' => 'Vous pouvez en choisir d\'autres, mais les fichiers qui remplacent groupeforum.html et filforum.html doivent exister !', # NEW
	'citer' => 'Citovat',
	'cocher' => 'cocher', # NEW
	'col_avatar' => 'Avatar', # NEW
	'col_date_crea' => 'Date inscription', # NEW
	'col_marquer' => 'Marquer', # NEW
	'col_signature' => 'Signature', # NEW
	'config_affiche_champ_extra' => 'Afficher le champ : <b>@nom_champ@</b>', # NEW
	'config_affiche_extra' => 'Afficher ces champs dans les squelettes', # NEW
	'config_champs_auteur' => 'Champs SPIPBB', # NEW
	'config_champs_auteurs_plus' => 'Gestion champs auteurs supplémentaires', # NEW
	'config_champs_requis' => 'Les champs nécessaires à SpipBB', # NEW
	'config_choix_mots' => 'Choisir le groupe de mot-clés', # NEW
	'config_choix_rubrique' => 'Choisir la rubrique contenant les forums spipBB', # NEW
	'config_choix_squelettes' => 'Choisir les squelettes utilisés', # NEW
	'config_orig_extra' => 'Quel support utiliser pour les champs supplémentaires', # NEW
	'config_orig_extra_info' => 'Infos champs EXTRA ou autre table, table auteurs_profils.', # NEW
	'config_spipbb' => 'Configuration de base de spipBB pour permettre le fonctionnement des forums avec ce plugin.', # NEW
	'contacter' => 'Contacter', # NEW
	'contacter_dpt' => 'Contacter : ', # NEW
	'creer_categorie' => 'Créer Nouvelle Catégorie', # NEW
	'creer_forum' => 'Vytvořit novou diskusní skupinu',

	// D
	'dans_forum' => 'v diskusní skupině',
	'deconnexion_' => 'Déconnexion ', # NEW
	'deplacer' => 'Déplacer', # NEW
	'deplacer_confirmer' => 'Confirmer le déplacement', # NEW
	'deplacer_dans_dpt' => 'À deplacer dans le forum :', # NEW
	'deplacer_sujet_dpt' => 'Déplacement de :', # NEW
	'deplacer_vide' => 'Pas d\'autre forum : déplacement impossible.', # NEW
	'dernier' => 'Poslední<br />',
	'dernier_membre' => 'Dernier membre enregistré : ', # NEW
	'derniers_messages' => 'Derniers Messages', # NEW
	'diviser' => 'Diviser', # NEW
	'diviser_confirmer' => 'Confirmer la séparation des messages', # NEW
	'diviser_dans_dpt' => 'À mettre dans le forum :', # NEW
	'diviser_expliquer' => 'A l\'aide du formulaire ci-dessous, vous pourrez séparer ce fil en deux, soit : en sélectionnant les messages individuellement; soit en choissant le message à partir duquel il faut les diviser en deux.', # NEW
	'diviser_selection_dpt' => 'Sélection :', # NEW
	'diviser_separer_choisis' => 'Séparer les messages sélectionnés', # NEW
	'diviser_separer_suite' => 'Séparer à partir du message sélectionné', # NEW
	'diviser_vide' => 'Pas d\'autre forum : division impossible.', # NEW

	// E
	'ecrirea' => 'Napsat e-mail pro',
	'effacer' => 'Odstranit',
	'email' => 'E-mail',
	'en_ligne' => 'Qui est en ligne ?', # NEW
	'en_rep_sujet_' => ' ::: Sujet : ', # NEW
	'en_reponse_a' => 'En réponse au message', # NEW
	'etplus' => '... a více ...',
	'extra_avatar_saisie_url' => 'URL de votre avatar (http://... ...)', # NEW
	'extra_avatar_saisie_url_info' => 'URL de l\'avatar du visiteur', # NEW
	'extra_date_crea' => 'Date de premiere saisie profil SpipBB', # NEW
	'extra_date_crea_info' => 'Date de premiere saisie profil SpipBB', # NEW
	'extra_emploi' => 'Emploi', # NEW
	'extra_localisation' => 'Localisation', # NEW
	'extra_loisirs' => 'Loisirs', # NEW
	'extra_nom_aim' => 'Contacts chat (AIM)', # NEW
	'extra_nom_msnm' => 'Contacts chat (MSN Messenger)', # NEW
	'extra_nom_yahoo' => 'Contacts chat (Yahoo)', # NEW
	'extra_numero_icq' => 'Contacts chat (ICQ)', # NEW
	'extra_refus_suivi_thread' => '(refus suivi) Ne pas modifier !', # NEW
	'extra_refus_suivi_thread_info' => 'Liste des threads pour lesquels on ne souhaite plus recevoir de notification', # NEW
	'extra_signature_saisie_texte' => 'Saisir ici le texte de votre signature', # NEW
	'extra_signature_saisie_texte_info' => 'Court texte de signature des messages', # NEW
	'extra_visible_annuaire' => 'Apparaitre dans la liste des Inscrits (publique)', # NEW
	'extra_visible_annuaire_info' => 'Permet de refuser l\'affichage dans l\'annuaire des inscrits en zone publique', # NEW

	// F
	'fiche_contact' => 'Fiche Contact', # NEW
	'fil_annonce_annonce' => 'Passer le Sujet en Annonce', # NEW
	'fil_annonce_desannonce' => 'Supprimer le mode Annonce', # NEW
	'fil_deplace' => 'Přesunout diskusi',
	'filtrer' => 'Filtrer', # NEW
	'forum' => 'Forums', # NEW
	'forum_annonce_annonce' => 'Poser le marquage d\'annonce', # NEW
	'forum_annonce_desannonce' => 'Supprimer le marquage d\'annonce', # NEW
	'forum_dpt' => 'Forum : ', # NEW
	'forum_ferme' => 'Tato diskusní skupina je ukončena',
	'forum_ferme_texte' => 'Tato diskusní skupina skončila. Další příspěvky do ní nelze zasílat.',
	'forum_maintenance' => 'Ce forum est fermé pour maintenance', # NEW
	'forum_ouvrir' => 'Otevřít diskusní skupinu',
	'forums_categories' => 'Divers', # NEW
	'forums_spipbb' => 'Forums SpipBB', # NEW
	'forums_titre' => 'Mon premier forum créé', # NEW
	'fromphpbb_erreur_db_phpbb_config' => 'Impossible de lire la configuration dans la base phpBB', # NEW
	'fromphpbb_migre_categories' => 'Import des catégories', # NEW
	'fromphpbb_migre_categories_dans_rub_dpt' => 'Implantation des forums dans la rubrique :', # NEW
	'fromphpbb_migre_categories_forum' => 'Forum', # NEW
	'fromphpbb_migre_categories_groupe' => 'Groupe', # NEW
	'fromphpbb_migre_categories_impossible' => 'Impossible de récupérer les catégories', # NEW
	'fromphpbb_migre_categories_kw_ann_dpt' => 'Les annonces recevront le mot-clef :', # NEW
	'fromphpbb_migre_categories_kw_ferme_dpt' => 'Les sujets clos recevront le mot-clef :', # NEW
	'fromphpbb_migre_categories_kw_postit_dpt' => 'Les post its recevront le mot-clef :', # NEW
	'fromphpbb_migre_existe_dpt' => 'existe :', # NEW
	'fromphpbb_migre_thread' => 'Import des topics et des posts', # NEW
	'fromphpbb_migre_thread_ajout' => 'Ajout thread', # NEW
	'fromphpbb_migre_thread_annonce' => 'Annonce', # NEW
	'fromphpbb_migre_thread_existe_dpt' => 'Forum existe :', # NEW
	'fromphpbb_migre_thread_ferme' => 'Fermé', # NEW
	'fromphpbb_migre_thread_impossible_dpt' => 'Impossible de récupérer les posts :', # NEW
	'fromphpbb_migre_thread_postit' => 'Post-it', # NEW
	'fromphpbb_migre_thread_total_dpt' => 'Nombre total de topics et de posts ajoutés :', # NEW
	'fromphpbb_migre_utilisateurs' => 'Import des utilisateurs', # NEW
	'fromphpbb_migre_utilisateurs_admin_restreint_add' => 'Ajout admin restreint', # NEW
	'fromphpbb_migre_utilisateurs_admin_restreint_already' => 'Deja admin restreint', # NEW
	'fromphpbb_migre_utilisateurs_impossible' => 'Impossible de récupérer les utilisateurs', # NEW
	'fromphpbb_migre_utilisateurs_total_dpt' => 'Nombre total d\'utilisateurs ajoutés :', # NEW

	// H
	'haut_page' => 'Na začátek stránky',

	// I
	'icone_ferme' => 'Zavřít',
	'import_base' => 'Nom de la base :', # NEW
	'import_choix_test' => 'Réaliser un import de test (choix par défaut) :', # NEW
	'import_choix_test_titre' => 'Import à blanc ou réel', # NEW
	'import_erreur_db' => 'Impossible de se connecter à la base @nom_base@', # NEW
	'import_erreur_db_config' => 'Impossible de lire la configuration dans la base @nom_base@', # NEW
	'import_erreur_db_rappel_connexion' => 'Impossible de se reconnecter à la base @nom_base@', # NEW
	'import_erreur_db_spip' => 'Impossible de se connecter à la base SPIP', # NEW
	'import_erreur_forums' => 'Impossible de recuperer les forums', # NEW
	'import_fichier' => 'Fichier de configuration @nom_base@ trouvé :', # NEW
	'import_host' => 'Nom/adresse du serveur', # NEW
	'import_login' => 'Identifiant :', # NEW
	'import_parametres_base' => 'Choisissez soit le chemin vers le fichier de configuration de @nom_base@, soit de renseigner les paramètres d\'accès à la base contenant les forums de @nom_base@ :', # NEW
	'import_parametres_rubrique' => 'Choisissez la rubrique dans laquelle seront importés les forums de @nom_base@', # NEW
	'import_parametres_titre' => 'Informations sur la base @nom_base@', # NEW
	'import_password' => 'Mot de passe :', # NEW
	'import_prefix' => 'Préfixe des tables :', # NEW
	'import_racine' => 'Chemin vers @nom_base@ (avatars) :', # NEW
	'import_table' => 'Table de configuration @nom_base@ trouvée :', # NEW
	'import_titre' => 'Import d\'un forum @nom_base@', # NEW
	'import_titre_etape' => 'Import d\'un forum  @nom_base@ - étape', # NEW
	'info' => 'Informace',
	'info_annonce_ferme' => 'Etat Annonce / Fermer', # NEW
	'info_confirmer_passe' => 'Potvrdit nové heslo:',
	'info_ferme' => 'Etat Fermé', # NEW
	'info_inscription_invalide' => 'Inscription impossible', # NEW
	'info_plus_cinq_car' => 'více než 5 znaků',
	'infos_refus_suivi_sujet' => 'Ne plus suivre les sujets', # NEW
	'infos_suivi_forum_par_inscription' => 'Suivi du forum par inscription', # NEW
	'inscription' => 'Inscription', # NEW
	'inscrit_le' => 'Inscrit le', # NEW
	'inscrit_le_dpt' => 'Inscrit le :', # NEW
	'inscrit_s' => 'Inscrits', # NEW
	'ip_adresse_autres' => 'Autres adresses IP à partir desquelles cet auteur a posté', # NEW
	'ip_adresse_membres' => 'Membres ayant posté de cette adresse IP', # NEW
	'ip_adresse_post' => 'Adresse IP de ce message', # NEW
	'ip_informations' => 'Informations sur une adresse IP et un auteur', # NEW

	// L
	'le' => 'Dne',
	'liste_des_messages' => 'Liste des messages', # NEW
	'liste_inscrits' => 'Liste des membres', # NEW
	'login' => 'Připojení',

	// M
	'maintenance' => 'U daných diskusních skupin nezapomeňte zrušit <br />"Uzavření kvůli údržbě"<br />.', # MODIF
	'maintenance_fermer' => 'a fermé l\'article/forum :', # NEW
	'maintenance_pour' => 'pour MAINTENANCE.', # NEW
	'membres_en_ligne' => 'membres en ligne', # NEW
	'membres_inscrits' => 'membres inscrits', # NEW
	'membres_les_plus_actifs' => 'Membres les plus actifs', # NEW
	'message' => 'Message', # NEW
	'message_s' => 'Messages', # NEW
	'message_s_dpt' => 'Messages : ', # NEW
	'messages' => 'Zprávy',
	'messages_anonymes' => 'rendre anonymes', # NEW
	'messages_derniers' => 'Poslední zprávy',
	'messages_laisser_nom' => 'laisser le nom', # NEW
	'messages_supprimer_titre_dpt' => 'Pour les messages :', # NEW
	'messages_supprimer_tous' => 'les supprimer', # NEW
	'messages_voir_dernier' => 'Zobrazit poslední zprávu',
	'messages_voir_dernier_s' => 'Voir les derniers messages', # NEW
	'moderateur' => 'Modérateur', # NEW
	'moderateur_dpt' => 'Modérateur : ', # NEW
	'moderateurs' => 'Moderátor/moderátoři',
	'moderateurs_dpt' => 'Modérateurs : ', # NEW
	'modif_parametre' => 'Modifiez vos paramètres', # NEW
	'mot_annonce' => 'Annonce
_ Une annonce est située en tête de forum sur toutes les pages.', # NEW
	'mot_ferme' => 'Fermé
-* Lorsqu\'un article-forum a ce mot-clef, seul les modérateurs peuvent y ajouter des messages.
-* Lorsqu\'un sujet de forum est ferm&ecute;, seuls les modérateurs peuvent y ajouter des réponses.', # NEW
	'mot_groupe_moderation' => 'Goupe de mot-clefs utilisé pour la modération de SpipBB', # NEW
	'mot_postit' => 'Postit
_ Un postit est situé en dessous des annonces, avant les messages ordinaires. Il n\'appara&icirc;t qu\'une seule fois dans la liste.', # NEW

	// N
	'no_message' => 'Vyhledávacím kritériím nedopovídá žádný předmět, resp. zpráva.',
	'nom_util' => 'Nom d\'utilisateur', # NEW
	'non' => 'Non', # NEW

	// O
	'ordre_croissant' => 'Croissant', # NEW
	'ordre_decroissant' => 'Décroissant', # NEW
	'ordre_dpt' => 'Ordre :', # NEW
	'oui' => 'Oui', # NEW

	// P
	'pagine_page_' => ' .. page ', # NEW
	'pagine_post_' => ' réponse', # NEW
	'pagine_post_s' => ' réponses', # NEW
	'pagine_sujet_' => ' sujet', # NEW
	'pagine_sujet_s' => ' sujets', # NEW
	'par_' => 'par ', # NEW
	'plugin_auteur' => 'La SpipBB Team : [voir la liste des contributeurs sur Spip-contrib->http://www.spip-contrib.net/Plugin-SpipBB#contributeurs]', # NEW
	'plugin_description' => 'Le plugin SpipBB permet :
-* De gérer de façon centralisée les forums de SPIP (interface privée),
-* D\'utiliser un secteur comme base d\'un groupe de forums comme les «Bulletin Board» tels que phpBB. Dans ce secteur, les sous-rubriques sont des groupes de forums, les articles des forums, chaque message dans le forum d\'un article y démarre un thread.

{{Consultez :}}
-* •[l\'aide et support sur spipbb.spip-zone.info->http://spipbb.free.fr/spip.php?article11],
-* •[La documentation sur Spip-contrib->http://www.spip-contrib.net/SpipBB-le-forum].

_ {{Plugin spipbb en cours de developpement. Vous l\'utilisez à vos risques et périls}}

_ [Accès au panneau d\'administration-> .?exec=spipbb_configuration]', # NEW
	'plugin_licence' => 'Distribué sous licence GPL', # NEW
	'plugin_lien' => '[Consulter la documentation du plugin sur Spip-contrib->http://www.spip-contrib.net/SpipBB-le-forum]', # NEW
	'plugin_mauvaise_version' => 'Cette version du plugin n\'est pas compatible avec votre version de SPIP !', # NEW
	'plugin_nom' => 'SpipBB : Gestion des forums de SPIP', #  Pour faciliter les traductions de plugin.xml NEW
	'post_aucun_pt' => 'aucun !', # NEW
	'post_efface_lui' => 'Ce sujet comprend @nbr_post@ message(s). Effacés avec lui !\\n', # NEW
	'post_ip' => 'Messages posté à partie de l\'adresse IP', # NEW
	'post_propose' => 'Message proposé', # NEW
	'post_rejete' => 'Message rejeté', # NEW
	'post_titre' => ' ::: Titre : ', # NEW
	'post_verifier_sujet' => 'Vérifier ce sujet', # NEW
	'poste_valide' => 'Zprávy ke schválení ...',
	'poster_date_' => 'Posté le : ', # NEW
	'poster_message' => 'Poster un message', # NEW
	'postit' => 'Postit', # NEW
	'postit_dpt' => 'Postit : ', # NEW
	'posts_effaces' => 'Messages effacés !', # NEW
	'posts_refuses' => 'Messages refusés, à effacer !', # NEW
	'previsualisation' => 'Prévisualisation', # NEW
	'profil' => 'Profil ', # MODIF

	// R
	'raison_clic' => 'cliquez ici', # NEW
	'raison_texte' => 'Pour en connaitre la raison', # NEW
	'recherche' => 'Recherche', # NEW
	'recherche_elargie' => 'Rozšířené hledání',
	'redige_post' => 'Ecrire message', # NEW
	'reglement' => 'Správci a moderátoři této diskusní skupiny se snaží co nejrychleji odstraňovat nebo měnit urážlivé, resp. sporné zprávy. Všechny však kontrolovat nemohou. Proto berete na vědomí, že zprávy v těchto diskusních skupinách vyjadřují názory svých autorů a nikoli správců, moderátorů nebo správců webu (s výjimkou zpráv, které tito odešlou). Proto za ně také správci, moderátoři, resp. správci webu nenesou žádnou odpovědnost.</p>

<p>Souhlasíte, že nebudete zveřejňovat zprávy obsahující urážky, obscénnosti, sprostá a hrubá slova, pomluvy, výhružky, sexuální narážky nebo jakékoli jiné zprávy, které by byly v rozporu s platnými zákony. Pokud tak učiníte, můžete být okamžitě a navždy vyloučeni (a váš poskytovatel přístupu k internetu o tom bude informován). Za účelem dodržování těchto podmínek se ukládají IP adresy všech zpráv. Souhlasíte s tím, že správce webu, správce nebo moderátor diskusní skupiny mají kdykoli právo odstranit, změnit, přesunout nebo ukončit jakékoli téma. Jakožto uživatelé souhlasíte s tím, že veškeré údaje, které dále uvedete, budou uloženy v databázi. Bez vašeho souhlasu však tyto údaje nebudou sděleny žádné jiné fyzické či právnické osobě. Správce webu, správce ani moderátoři nenesou žádnou odpovědnost za jakýkoli pokus o neoprávněné zneužití těchto údajů.</p>

<p>Tato diskusní skupina používá pro ukládání údajů ve vašem počítači soubory cookies. Tyto soubory však neobsahují žádné ingformace, které zadáte níže. Jsou určeny pouze pro zvýšení uživatelského pohodlí. Elektronická adresa se používá pouze pro potvrzení údajů o registraci a hesla (a také pro případ zaslání nového hesla, pokud své heslo zapomenete).</p><p>Přihlášením potvrzujëte svůj souhlas s výše uvedenými pravidly.</p>', # MODIF
	'repondre' => 'Odpovědět',
	'reponse_s_' => 'Réponses', # NEW
	'resultat_s_pour_' => ' résultats pour ', # NEW
	'retour_forum' => 'Zpět na hlavní stránku diskusní skupiny',

	// S
	's_abonner_a' => 'RSS . S\'abonner à : ', # NEW
	'secteur_forum' => 'OBLASTI – DISKUSN&Iacute; SKUPINY', # MODIF
	'selection_efface' => 'Odstranit výběr ...',
	'selection_tri_dpt' => 'Sélectionner la méthode de tri :', # NEW
	'sign_admin' => '{{Cette page est uniquement accessible aux responsables du site.}}<p>Elle donne accès à la configuration du plugin «{{<a href="http://www.spip-contrib.net/Plugin-SpipBB#contributeurs" class="copyright">SpipBB</a>}}» ainsi qu\'à la gestion des forums du site.</p><p>Version : @version@ @distant@</p><p>Consultez :
_ • [La documentation sur Spip-Contrib->http://www.spip-contrib.net/?article2460]
_ • [L\'aide et support sur spipbb.spip-zone.info->http://spipbb.free.fr/spip.php?article11]</p>@reinit@', # NEW
	'sign_maj' => '<br />Version plus récente disponible : @version@', # NEW
	'sign_ok' => 'à jour.', # NEW
	'sign_reinit' => '<p>Ré-initialisation :
_ • [de tout le plugin->@plugin@]</p>', # NEW
	'sign_tempo' => 'Réalisé avec <a href="http://www.spip-contrib.net/Plugin-SpipBB#contributeurs" class="copyright">SpipBB</a>', # NEW
	'signature' => 'Signature', # NEW
	'sinscrire' => 'přihlásit se', # MODIF
	'site_propose' => 'Site proposé par @auteur_post@', # NEW
	'site_web' => 'Site web', # NEW
	'squelette_filforum' => 'Base de squelette pour les fils de discussions :', # NEW
	'squelette_groupeforum' => 'Base de squelette pour les groupes de discussions :', # NEW
	'statut' => 'Status',
	'statut_admin' => 'Administrateur', # NEW
	'statut_redac' => 'Rédacteur', # NEW
	'statut_visit' => 'Membre', # NEW
	'sujet' => 'Předmět ', # MODIF
	'sujet_auteur' => 'Autor',
	'sujet_clos_texte' => 'Tento předmět je ukončen. Další zprávy k němu nemůžete zasílat.',
	'sujet_clos_titre' => 'Skončený předmět',
	'sujet_dpt' => 'Sujet : ', # NEW
	'sujet_ferme' => 'Sujet : fermé', # NEW
	'sujet_nombre' => 'Počet předmětů',
	'sujet_nouveau' => 'Nový předmět',
	'sujet_rejete' => 'Odmítnutý předmět',
	'sujet_repondre' => 'Répondre au sujet', # NEW
	'sujet_s' => 'Sujets', # NEW
	'sujet_valide' => 'Předmět ke schválení',
	'sujets' => 'Předměty',
	'sujets_aucun' => 'V této diskusní skupině prozatím neexistuje žádný předmět',
	'support_extra_normal' => 'extra', # NEW
	'support_extra_table' => 'table', # NEW
	'supprimer' => 'Supprimer', # NEW
	'sw_admin_can_spam' => 'Les admins sont autorisés', # NEW
	'sw_admin_no_spam' => 'Pas de spam', # NEW
	'sw_ban_ip_titre' => 'Bannir l\'IP en même temps', # NEW
	'sw_config_exceptions' => 'Vous pouvez activer des exceptions pour des utilisateurs privilégiés ici. Ils pourront quand même publier avec des mots bannis.', # NEW
	'sw_config_exceptions_titre' => 'Gestion des exceptions', # NEW
	'sw_config_generale' => 'Configuration actuelle du filtrage :', # NEW
	'sw_config_generale_titre' => 'Configuration générale du filtrage du spam', # NEW
	'sw_config_warning' => 'Vous pouvez choisir le texte du MP envoyé si vous activez cette option (maxi 255 caractères).', # NEW
	'sw_config_warning_titre' => 'Configuration des avertissements par message privé', # NEW
	'sw_disable_sw_titre' => '<strong>Active le filtrage</strong><br />Si vous devez vous passer du filtrage,<br />cliquez sur Non', # NEW
	'sw_modo_can_spam' => 'Les modérateurs sont autorisés', # NEW
	'sw_nb_spam_ban_titre' => 'Nombre de spams avant banissement', # NEW
	'sw_pm_spam_warning_message' => 'Ceci est un avertissement. Vous avez essayé de poster un message analysé comme du spam sur ce site web. Merci d\'éviter de recommencer.', # NEW
	'sw_pm_spam_warning_titre' => 'Attention.', # NEW
	'sw_send_pm_warning' => '<strong>Envoie un MP à l\'utilisateur</strong><br />lorsqu\'il poste un message avec un mot interdit', # NEW
	'sw_spam_forum_titre' => 'Gestion des messages de spam', # NEW
	'sw_spam_titre' => 'Filtrage du spam', # NEW
	'sw_spam_words_action' => 'A partir de cette page, vous pouvez ajouter, éditer et supprimer des mots associés à du spam. Le caractère (*) est accepté dans le mot. Par exemple : {{*tes*}} capturera {détestable}, {{tes*}} capturera {tester}, {{*tes}} capturera {portes}.', # NEW
	'sw_spam_words_mass_add' => 'Copier-coller ou saisir vos mots dans cette zone. Séparer chaque mot par une virgule, deux points ou un retour à la ligne.', # NEW
	'sw_spam_words_titre' => 'Filtrage de mots', # NEW
	'sw_spam_words_url_add' => 'Saisir l\'URL d\'un fichier contenant une liste de mots formatée comme ci-dessus. Exemple : http://spipbb.free.fr/IMG/csv/spamwordlist.csv .', # NEW
	'sw_warning_from_admin' => 'Choisir l\'admin auteur du message envoyé', # NEW
	'sw_warning_pm_message' => 'Texte du message privé', # NEW
	'sw_warning_pm_titre' => 'Sujet du message privé', # NEW
	'sw_word' => 'Mot', # NEW

	// T
	'title_ferme' => 'Fermer le forum/article', # NEW
	'title_libere' => 'Réouvrir le forum/article', # NEW
	'title_libere_maintenance' => 'Libérer le verrou de Maintenance', # NEW
	'title_maintenance' => 'Fermer le forum/article pour Maintenance', # NEW
	'title_sujet_ferme' => 'Fermer ce sujet', # NEW
	'title_sujet_libere' => 'Réouvrir ce sujet', # NEW
	'titre_spipbb' => 'SpipBB', # NEW
	'total_membres' => 'Nous avons un total de ', # NEW
	'total_messages_membres' => 'Nos membres ont posté un total de ', # NEW
	'tous' => 'Tous', # NEW
	'tous_forums' => 'Všechny diskusní skupiny',
	'trier' => 'Trier', # NEW
	'trouver_messages_auteur_dpt' => 'Trouver tous les messages de :', # NEW

	// V
	'visible_annuaire_forum' => 'Apparaitre dans la liste des Inscrits', # NEW
	'visites' => 'Vu', # NEW
	'voir' => 'ZOBRAZIT',
	'votre_bio' => 'Courte biographie en quelques mots.', # NEW
	'votre_email' => 'Votre adresse email', # NEW
	'votre_nouveau_passe' => 'Nouveau mot de passe', # NEW
	'votre_signature' => 'Votre signature', # NEW
	'votre_site' => 'Le nom de votre site', # NEW
	'votre_url_avatar' => 'URL de votre Avatar (http://...)', # NEW
	'votre_url_site' => 'L\'adresse (URL) de votre site' # NEW
);

?>
