<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/spipbb?lang_cible=km
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_propos_auteur_dpt' => 'រាល់របស់ អំពី៖',
	'admin_action_01_configuration' => 'កំណត់ទំរង់',
	'admin_action_02_etat' => 'ស្ថានភាព នៃទីប្រជុំមតិ',
	'admin_action_ZZ_debug' => 'ស្រាយកំហុស',
	'admin_action_effacer' => 'Rejected posts', # NEW
	'admin_action_fromphorum' => 'នាំចូល ពីទីប្រជុំមតិ',
	'admin_action_fromphpbb' => 'នាំចូល ពី PhpBB',
	'admin_action_gere_ban' => 'ការគ្រប់គ្រង បញ្ជីហាម',
	'admin_action_gestion' => 'ការគ្រប់គ្រង',
	'admin_action_inscrits' => 'សមាជិក',
	'admin_action_swconfig' => 'កំណត់ទំរង់',
	'admin_action_swforum' => 'ប្រកាស មានទង់',
	'admin_action_swlog' => 'កំណត់ហេតុ វិចារឥតបានការ',
	'admin_action_swwords' => 'គ្រប់គ្រង ពាក្យ',
	'admin_afficher_bouton_alerte_abus' => 'បង្ហាញ ប្រអប់រំលឹកការបំពាន',
	'admin_afficher_bouton_rss' => 'បង្ហាញ ប្រអប់ RSS',
	'admin_age_forum' => 'Forum age Years/months', # NEW
	'admin_avatar_affiche' => 'យល់ព្រម និងបង្ហាញ អាវើថរ (លំនាំដើម ជា យល់ព្រម បន្ទាប់ការតំលើង)',
	'admin_avatar_taille_contact' => 'ទំហំអាវើថរ (ជា pixels) លើទំព័រទំនាក់ទំនង',
	'admin_avatar_taille_profil' => 'ទំហំអាវើថរ (ជា pixels) លើ ទំព័រពត៌មានផ្ទាល់ខ្លួន',
	'admin_avatar_taille_sujet' => 'ទំហំអាវើថរ (ជា pixels) លើ ទំព័រប្រធានបទ',
	'admin_average_posts' => 'Daily posts average', # NEW
	'admin_average_users' => 'Daily inscription average', # NEW
	'admin_ban_email' => 'Banned email admin', # NEW
	'admin_ban_email_info' => 'To write more than one email, separate them using commas. To use a joker for a username use * ; eg *@hotmail.com', # NEW
	'admin_ban_email_none' => 'គ្មានអាសយដ្ឋាន ត្រូវបានហាម',
	'admin_ban_ip' => 'Banned IP admin', # NEW
	'admin_ban_ip_info' => 'To write more than one IP or server names, separate them using commas. To specify IP intervals, separate the biginning and the end with a dash -, to specify a joker use a star *', # NEW
	'admin_ban_ip_none' => 'គ្មានអាសយដ្ឋាន ត្រូវបានហាម',
	'admin_ban_user' => 'Banned logins admin', # NEW
	'admin_ban_user_info' => 'You can ban more than one user at a time using CTRL or UP combined with the mouse or the keyboard', # NEW
	'admin_ban_user_none' => 'គ្មានការពិនិត្យចូល ត្រូវបានហាម',
	'admin_cat_01_general' => 'ការអភិបាល',
	'admin_cat_outils' => 'ឧបករ',
	'admin_cat_spam' => 'ពាក្យឥតបានការ',
	'admin_config_prerequis' => 'ការតំរូវអោយដាក់',
	'admin_config_spam_words' => 'Anti-spam admin', # NEW
	'admin_config_spipbb' => 'អនុញ្ញាត SpipBB',
	'admin_config_spipbb_info' => 'ជ្រើសរើស យល់ព្រម សំរាប់អនុញ្ញាត SpipBB',
	'admin_config_tables' => 'ការកំណត់ទំរង់ នៃតារាង SpipBB',
	'admin_config_tables_erreur' => 'Problem with SpipBB tables: @tables_erreur@ are incorrect (the tables @tables_ok@ seem to be all right).
 Refer to the [documentation on Spip-Contrib->http://www.spip-contrib.net/SpipBB-le-forum] or [support on spipbb.spip-zone->http://spipbb.spip-zone.info/spip.php?article11]', # NEW
	'admin_config_tables_ok' => 'The SpipBB database tables are installed correctly (@tables_ok@)', # NEW
	'admin_date_ouverture' => 'Openning date', # NEW
	'admin_debug_log' => 'Log file @log_name@', # NEW
	'admin_debug_metas' => 'SpipBB METAs',
	'admin_form_action' => 'សកម្មភាព',
	'admin_form_creer_categorie' => 'បង្កើត មួយចំណាត់ក្រុម',
	'admin_form_creer_forum' => 'បង្កើត មួយទីប្រជុំមតិ',
	'admin_form_deplacer' => 'ប្តូរទីតាំង',
	'admin_form_descendre' => 'ទៅក្រោម',
	'admin_form_editer' => 'កែប្រែ',
	'admin_form_messages' => ' ',
	'admin_form_monter' => 'ទៅលើ',
	'admin_form_sujets' => ' ',
	'admin_forums_affiche_membre_defaut' => 'Do you want to enable report the member names in the members list when they did not make their own choice ?<br />[ Default No ]', # NEW
	'admin_forums_configuration' => 'កំណត់ទំរង់ SpipBB',
	'admin_forums_configuration_avatar' => 'កំណត់ទំរង់ទូទៅ នៃអាវើថរ',
	'admin_forums_configuration_options' => 'ជំរើស SpipBB',
	'admin_forums_log_level' => 'Choice of the SpipBB log level.<br />[ Default 3 (maximum) ]', # NEW
	'admin_forums_log_level_0' => 'គ្មានកំណត់ហេតុ',
	'admin_forums_log_level_1' => 'មួយចំនួនកំណត់ហេតុ',
	'admin_forums_log_level_2' => 'ច្រើនកំណត់ហេតុ',
	'admin_forums_log_level_3' => 'Very verbose logs', # NEW
	'admin_id_mjsc' => '#',
	'admin_infos' => 'SpipBB - អ្នកអភិបាល - សង្ខេប',
	'admin_interface' => 'ជំរើសផ្ទាំងប្រទាក់',
	'admin_nombre_lignes_messages' => 'Number of lines of messages', # NEW
	'admin_plugin_requis_erreur' => 'The following required plugin is missing. Activate it!', # NEW
	'admin_plugin_requis_erreur_balisesession' => 'Install le "Balise SESSION" plugin  and activate it! [Documentation here->http://www.spip-contrib.net/?article1224], [ZIP file here->http://files.spip.org/spip-zone/balise_session.zip].', # NEW
	'admin_plugin_requis_erreur_cfg' => 'Install the CFG plugin and activate it![Documentation here->http://www.spip-contrib.net/?article1605], [Zip file here->http://files.spip.org/spip-zone/cfg.zip].', # NEW
	'admin_plugin_requis_erreur_s' => 'The following required plugins are missing. Activate them!', # NEW
	'admin_plugin_requis_ok' => 'Installed and activated plugin(s):', # NEW
	'admin_plugin_requis_ok_balisesession' => '[Plugin BALISE_SESSION->http://www.spip-contrib.net/?article1224] : gives information on visitors who are logged in.', # NEW
	'admin_plugin_requis_ok_cfg' => '[Plugin CFG->http://www.spip-contrib.net/?article1605] : provides tags and functions.', # NEW
	'admin_sous_titre' => 'Go to the SpipBB forums admin panel', # NEW
	'admin_spip_config_forums' => 'ការកំណត់ទំរង់ SPIP៖',
	'admin_spip_forums_ok' => 'The public forums are enabled.', # NEW
	'admin_spip_forums_warn' => '<p>{{Beware}} : By default, Your forums are not activated. The recommended setting is to use the automatic activation ([see here->@config_contenu@]).</p><p>Otherwise, you will have to activate them article by article.</p>', # NEW
	'admin_spip_mots_cles_ok' => 'អនុញ្ញាត ពាក្យគន្លឹះ',
	'admin_spip_mots_cles_warn' => '<p>{{Beware}} : The keywords are not activated in SPIP, you won\'t be able to use them for the advanced features.</p><p>It is recommended to activate them. ([see here->@configuration@]).</p>', # NEW
	'admin_spip_mots_forums_ok' => 'អនុញ្ញាត ពាក្យគន្លឹះទីប្រជុំមតិ',
	'admin_spip_mots_forums_warn' => '<p>{{Beware}} : The keywords for public forums are not activated in SPIP, you won\'t be able to use them for the relevant advanced features.</p><p>It is recommended to activate them. ([see here->@configuration@]).</p>', # NEW
	'admin_spipbb_release' => 'SpipBB relase', # NEW
	'admin_statistique' => 'ពត៌មាន',
	'admin_surtitre' => 'ការគ្រប់គ្រងទីប្រជុំមតិ',
	'admin_temps_deplacement' => 'Amount of time before an admin can move', # NEW
	'admin_titre' => 'ការអភិបាល SpipBB',
	'admin_titre_page_spipbb_admin' => 'អ្នកអភិបាលទីប្រជុំមតិ',
	'admin_titre_page_spipbb_admin_anti_spam_config' => 'Spam filter general admin', # NEW
	'admin_titre_page_spipbb_admin_anti_spam_forum' => 'ប្រកាសត្រូវបានចំណាំ',
	'admin_titre_page_spipbb_admin_anti_spam_log' => 'កំណត់ហេតុ នៃវិចារឥតបានការ ',
	'admin_titre_page_spipbb_admin_anti_spam_words' => 'ឧបករតំរង ពាក្យ',
	'admin_titre_page_spipbb_admin_debug' => 'ស្រាយកំហុស',
	'admin_titre_page_spipbb_admin_etat' => 'SpipBB - អ្នកអភិបាល - សង្ខេប',
	'admin_titre_page_spipbb_admin_gere_ban' => 'Ban admin', # NEW
	'admin_titre_page_spipbb_admin_migre' => 'Migration of @nom_base@', # NEW
	'admin_titre_page_spipbb_configuration' => 'ការកំណត់ទំរង់ SpipBB',
	'admin_titre_page_spipbb_effacer' => 'Admin blocked posts', # NEW
	'admin_titre_page_spipbb_inscrits' => 'Members admin', # NEW
	'admin_titre_page_spipbb_sujet' => 'Edit a thread', # NEW
	'admin_total_posts' => 'Total number of posts', # NEW
	'admin_total_users' => 'Number of members', # NEW
	'admin_total_users_online' => 'Online members', # NEW
	'admin_unban_email_info' => 'You can reinstate multiple addresses at once using combined CTRL and UP with mouse or keyboard', # NEW
	'admin_unban_ip_info' => 'You can reinstate multiple addresses at once using combined CTRL and UP with mouse or keyboard', # NEW
	'admin_unban_user_info' => 'You can reinstate multiple users at once using combined CTRL and UP with mouse or keyboard', # NEW
	'admin_valeur' => 'តំលៃ',
	'aecrit' => 'បានសរសេរ៖',
	'alerter_abus' => 'រាយការសារនេះ ជាបំពាន',
	'alerter_sujet' => 'Post reported as abusive', # NEW
	'alerter_texte' => 'Please take a look at this post:', # NEW
	'annonce' => 'ជំនូនដំណឹង',
	'annonce_dpt' => 'ជំនូនដំណឹង៖',
	'anonyme' => 'អនាមិក',
	'auteur' => 'អ្នកនិពន្ធ',
	'avatar' => 'អាវើថរ',

	// B
	'bouton_select_all' => 'ជ្រើសយក ទាំងអស់',
	'bouton_speciaux_sur_skels' => 'Configure specific buttons on the public templates', # NEW
	'bouton_unselect_all' => 'លែងជ្រើសយក ទាំងអស់',

	// C
	'champs_obligatoires' => 'Fields marked with an * are compulsory.', # NEW
	'chercher' => 'ស្វែងរក',
	'choix_mots_annonce' => 'Make an announce', # NEW
	'choix_mots_creation' => 'If you want to create the keywords dedicated to SpipBB  <strong>automatically</strong>B, click this button. These keywords can be modified or deleted later...', # NEW
	'choix_mots_creation_submit' => 'Automatic keywords configuration', # NEW
	'choix_mots_ferme' => 'To close a thread', # NEW
	'choix_mots_postit' => 'កំណត់ មមារញឹក',
	'choix_mots_selection' => 'This keyword group should at least contain 3 keywords. Usually, the plugin will create them while installing. SpipBB needs - in general - the {ferme} (closed), {annonce} (announce) and {postit} (postit) keywords, but you are allowed to choose another one.', # NEW
	'choix_rubrique_creation' => 'If you want to create the main section containing the SpipBB forums and the first empty forum <strong>automatically</strong>, click this button. This forum is the created hierarchy can be modified or deleted later...', # NEW
	'choix_rubrique_creation_submit' => 'Main section automatic configuration', # NEW
	'choix_rubrique_selection' => 'Select the section that will host the base of your forums. Inside, each sub-section will be a forum group, each published article will open a new forum.', # NEW
	'choix_squelettes' => 'You are allowed to used other templates, but the selected files replacing groupeforum.html and filforum.html must already exist!', # NEW
	'citer' => 'Quote', # NEW
	'cocher' => 'cocher', # NEW
	'col_avatar' => 'អាវើថរ',
	'col_date_crea' => 'ថ្ងៃខែឆ្នាំ ចុះឈ្មោះ',
	'col_marquer' => 'ចំណាំ',
	'col_signature' => 'ហត្ថលេខា',
	'config_affiche_champ_extra' => 'បង្ហាញវាល៖ <b>@nom_champ@</b>',
	'config_affiche_extra' => 'បង្ហាញ វាលខាងក្រោម ក្នុងគំរូខ្នាត',
	'config_champs_auteur' => 'វាល SPIPBB',
	'config_champs_auteurs_plus' => 'Author extra fields admin', # NEW
	'config_champs_requis' => 'វាល តំរូវអោយដាក់ ដោយ SpipBB',
	'config_choix_mots' => 'Choose the keyword group', # NEW
	'config_choix_rubrique' => 'Choose the section used by spipBB forums', # NEW
	'config_choix_squelettes' => 'ជ្រើសយក គំរូខ្នាត',
	'config_orig_extra' => 'Which source used to store the extra fields', # NEW
	'config_orig_extra_info' => 'Infos EXTRA fields or other db table, table auteurs_profils.', # NEW
	'config_spipbb' => 'Basic SpipBB config in order to use the forums with this plugin.', # NEW
	'contacter' => 'ទាក់ទង',
	'contacter_dpt' => 'ទាក់ទង៖',
	'creer_categorie' => 'បង្កើត មួយចំណាត់ក្រុម',
	'creer_forum' => 'បង្កើត មួយទីប្រជុំមតិ',

	// D
	'dans_forum' => 'ក្នុង ទីប្រជុំមតិ',
	'deconnexion_' => 'ពិនិត្យចេញ',
	'deplacer' => 'ប្តូរទីតាំង',
	'deplacer_confirmer' => 'បញ្ជាក់ទទួលស្គាល់ បណ្តូរទីតាំង',
	'deplacer_dans_dpt' => 'ប្តូរទីតាំង ទៅទីប្រជុំមតិ៖',
	'deplacer_sujet_dpt' => 'Moving of:', # NEW
	'deplacer_vide' => 'Not any other forum left: moving this thread is impossible!', # NEW
	'dernier' => ' Last', # NEW
	'dernier_membre' => 'Last registered member: ', # NEW
	'derniers_messages' => 'សារចុងក្រោយ',
	'diviser' => 'Split', # NEW
	'diviser_confirmer' => 'Confirm the topics splitting', # NEW
	'diviser_dans_dpt' => 'Into this Forum:', # NEW
	'diviser_expliquer' => 'Using the below form you can split a topic in two, either by selecting the posts individually or by splitting at a selected post.', # NEW
	'diviser_selection_dpt' => 'Select:', # NEW
	'diviser_separer_choisis' => 'Split selected posts', # NEW
	'diviser_separer_suite' => 'Split from selected posts', # NEW
	'diviser_vide' => 'Not any other forum left: spliting this thread is impossible!', # NEW

	// E
	'ecrirea' => 'Send an email to', # NEW
	'effacer' => 'Delete', # NEW
	'email' => 'អ៊ីមែវល៍',
	'en_ligne' => 'Who\'s online?', # NEW
	'en_rep_sujet_' => ' ::: Topic : ', # NEW
	'en_reponse_a' => 'Answering the message', # NEW
	'etplus' => '...and more...', # NEW
	'extra_avatar_saisie_url' => 'Your avatar\'s URL (http://... ...)', # NEW
	'extra_avatar_saisie_url_info' => 'Visitor\'s avatar URL', # NEW
	'extra_date_crea' => 'Registration date', # NEW
	'extra_date_crea_info' => 'SpipBB profile registration date', # NEW
	'extra_emploi' => 'Job', # NEW
	'extra_localisation' => 'Localization', # NEW
	'extra_loisirs' => 'Hobbies', # NEW
	'extra_nom_aim' => 'Chat id (AIM)', # NEW
	'extra_nom_msnm' => 'Chat id (MSN Messenger)', # NEW
	'extra_nom_yahoo' => 'Chat id (Yahoo)', # NEW
	'extra_numero_icq' => 'Chat id (ICQ)', # NEW
	'extra_refus_suivi_thread' => '(don\'t follow). Do not modify!', # NEW
	'extra_refus_suivi_thread_info' => 'Threads you want not to follow any more', # NEW
	'extra_signature_saisie_texte' => 'Fill your signature here', # NEW
	'extra_signature_saisie_texte_info' => 'Short signature', # NEW
	'extra_visible_annuaire' => 'Appear in the (public) members\' list', # NEW
	'extra_visible_annuaire_info' => 'Allow you not to appear in the public members\' list', # NEW

	// F
	'fiche_contact' => 'Contact form', # NEW
	'fil_annonce_annonce' => 'Move this topic into Announcement', # NEW
	'fil_annonce_desannonce' => 'Remove the Announcement mode', # NEW
	'fil_deplace' => 'Move this thread', # NEW
	'filtrer' => 'Filter', # NEW
	'forum' => 'Forums', # NEW
	'forum_annonce_annonce' => 'Mark as announce', # NEW
	'forum_annonce_desannonce' => 'Remove the announce mark', # NEW
	'forum_dpt' => 'Forum : ', # NEW
	'forum_ferme' => 'This forum is disabled', # NEW
	'forum_ferme_texte' => 'This forum is disabled. You cannot post to it anymore.', # NEW
	'forum_maintenance' => 'This forum is close for maintenance', # NEW
	'forum_ouvrir' => 'Open this Forum', # NEW
	'forums_categories' => 'Miscellaneous', # NEW
	'forums_spipbb' => 'SpipBB forums', # NEW
	'forums_titre' => 'My first forum', # NEW
	'fromphpbb_erreur_db_phpbb_config' => 'Impossible to read config value in phpBB database', # NEW
	'fromphpbb_migre_categories' => 'Categories migration', # NEW
	'fromphpbb_migre_categories_dans_rub_dpt' => 'Implanting the forums into the sector:', # NEW
	'fromphpbb_migre_categories_forum' => 'Forum', # NEW
	'fromphpbb_migre_categories_groupe' => 'Group', # NEW
	'fromphpbb_migre_categories_impossible' => 'Impossible to migrate the categories', # NEW
	'fromphpbb_migre_categories_kw_ann_dpt' => 'The announce will use the keyword:', # NEW
	'fromphpbb_migre_categories_kw_ferme_dpt' => 'The closed topics will use the keyword:', # NEW
	'fromphpbb_migre_categories_kw_postit_dpt' => 'The postits will use the keyword:', # NEW
	'fromphpbb_migre_existe_dpt' => 'exist:', # NEW
	'fromphpbb_migre_thread' => 'Migration of topics and posts', # NEW
	'fromphpbb_migre_thread_ajout' => 'Adding thread', # NEW
	'fromphpbb_migre_thread_annonce' => 'Announce', # NEW
	'fromphpbb_migre_thread_existe_dpt' => 'Forum already exist:', # NEW
	'fromphpbb_migre_thread_ferme' => 'Closed', # NEW
	'fromphpbb_migre_thread_impossible_dpt' => 'Impossible to migrate the topics:', # NEW
	'fromphpbb_migre_thread_postit' => 'Post-it', # NEW
	'fromphpbb_migre_thread_total_dpt' => 'Total number of topics and posts added:', # NEW
	'fromphpbb_migre_utilisateurs' => 'Migrate the members', # NEW
	'fromphpbb_migre_utilisateurs_admin_restreint_add' => 'Add restricted admin', # NEW
	'fromphpbb_migre_utilisateurs_admin_restreint_already' => 'Already restricted admin', # NEW
	'fromphpbb_migre_utilisateurs_impossible' => 'Impossible to migrate the members', # NEW
	'fromphpbb_migre_utilisateurs_total_dpt' => 'Total number of members added:', # NEW

	// H
	'haut_page' => 'Top of page', # NEW

	// I
	'icone_ferme' => 'បិទ',
	'import_base' => 'ឈ្មោះមូលដ្ឋានទិន្នន័យ៖',
	'import_choix_test' => 'Make a blank test (default):', # NEW
	'import_choix_test_titre' => 'Blank or real migration', # NEW
	'import_erreur_db' => 'Impossible to connect to @nom_base@ database', # NEW
	'import_erreur_db_config' => 'Impossible to read the configuration of @nom_base@', # NEW
	'import_erreur_db_rappel_connexion' => 'Impossible to reconnect tp @nom_base@ database', # NEW
	'import_erreur_db_spip' => 'គ្មានអាចភ្ជាប់ ទៅ មូលដ្ឋានទិន្នន័យ របស់ SPIP',
	'import_erreur_forums' => 'Impossible to migrate the forums', # NEW
	'import_fichier' => 'រកឃើញ ឯកសារកំណត់ទំរង់ @nom_base@ ៖',
	'import_host' => 'អាសយដ្ឋាន/ឈ្មោះ នៃខំព្យូរើបំរើសេវា',
	'import_login' => 'ពិនិត្យចូល៖',
	'import_parametres_base' => 'Please choose the path to the config file of @nom_base@, or the database parameters of @nom_base@:', # NEW
	'import_parametres_rubrique' => 'Please choose the sector into which the @nom_base@ forums will be migrated', # NEW
	'import_parametres_titre' => 'ពត៌មាន អំពី @nom_base@',
	'import_password' => 'ពាក្យសំងាត់៖',
	'import_prefix' => 'Database tables prefix:', # NEW
	'import_racine' => 'Path to @nom_base@ (avatars):', # NEW
	'import_table' => 'រកឃើញ តារាងកំណត់ទំរង់ នៃ @nom_base@៖',
	'import_titre' => 'Migration of @nom_base@ forum', # NEW
	'import_titre_etape' => 'Migration of @nom_base@ forum - step', # NEW
	'info' => 'ពត៌មាន',
	'info_annonce_ferme' => 'State Announce/Closed', # NEW
	'info_confirmer_passe' => 'បញ្ជាក់ទទួលស្គាល់ ពាក្យសំងាត់ថ្មីនេះ៖',
	'info_ferme' => 'State closed', # NEW
	'info_inscription_invalide' => 'គ្មានអាចចុះឈ្មោះ',
	'info_plus_cinq_car' => 'ច្រើនជាង 5 អក្សរ',
	'infos_refus_suivi_sujet' => 'Don\'t follow these threads', # NEW
	'infos_suivi_forum_par_inscription' => 'Follow the thread by registration', # NEW
	'inscription' => 'ចុះឈ្មោះ',
	'inscrit_le' => 'បានចុះឈ្មោះ លើ',
	'inscrit_le_dpt' => 'បានចុះឈ្មោះ លើ',
	'inscrit_s' => 'បានចុះឈ្មោះ',
	'ip_adresse_autres' => 'Other IP addresses this user has posted from', # NEW
	'ip_adresse_membres' => 'Users posting from this IP address', # NEW
	'ip_adresse_post' => 'អាសយដ្ឋាន IP សំរាប់ប្រកាសនេះ',
	'ip_informations' => 'ពត៌មាន នៃ IP',

	// L
	'le' => 'The', # NEW
	'liste_des_messages' => 'បញ្ជីប្រកាស',
	'liste_inscrits' => 'បញ្ជីសមាជិក',
	'login' => 'បញ្ជាប់',

	// M
	'maintenance' => 'តំហែទាំ',
	'maintenance_fermer' => 'បានបិទ ដំណឹង/ទីប្រជុំមតិ៖',
	'maintenance_pour' => 'សំរាប់ តំហែទាំ។',
	'membres_en_ligne' => 'សមាជិក លើបណ្តាញ',
	'membres_inscrits' => 'សមាជិកបានចុះឈ្មោះ',
	'membres_les_plus_actifs' => 'សមាជិកសកម្មបំផុត',
	'message' => 'សារ',
	'message_s' => 'សារ',
	'message_s_dpt' => 'សារ៖',
	'messages' => 'ចំលើយ',
	'messages_anonymes' => 'anonymize', # NEW
	'messages_derniers' => 'សារ ចុងក្រោយបំផុត',
	'messages_laisser_nom' => 'ដាក់ឈ្មោះ',
	'messages_supprimer_titre_dpt' => 'សំរាប់ប្រកាស៖',
	'messages_supprimer_tous' => 'លុបចេញ',
	'messages_voir_dernier' => 'ផ្លោះទៅ សារចុងក្រោយ',
	'messages_voir_dernier_s' => 'តាមដាន សារចុងក្រោយ',
	'moderateur' => 'អ្នកសំរបសំរួល',
	'moderateur_dpt' => 'អ្នកសំរបសំរួល៖',
	'moderateurs' => 'អ្នកសំរបសំរួល',
	'moderateurs_dpt' => 'ពួកអ្នកសំរបសំរួល៖',
	'modif_parametre' => 'Change your parameters', # NEW
	'mot_annonce' => 'Announcement
 _ An announcement is placed at the head of the forum on all pages.', # NEW
	'mot_ferme' => 'Closed
 -* when an article-forum uses this keyword, only moderators can post messages.
 -* when a forum topic is closed, only the moderators will be able to post messages.', # NEW
	'mot_groupe_moderation' => 'Keywords group used for SpipBB moderation', # NEW
	'mot_postit' => 'Post-it
 _ A post-it is placed underneath announcements, before ordinary messages. It only appears once in the list.', # NEW

	// N
	'no_message' => 'No message by that search criterium', # NEW
	'nom_util' => 'ឈ្មោះសមាជិក',
	'non' => 'ទេ',

	// O
	'ordre_croissant' => 'Increasing', # NEW
	'ordre_decroissant' => 'Decreasing', # NEW
	'ordre_dpt' => 'លំដាប់៖',
	'oui' => 'បាទ/ចាស',

	// P
	'pagine_page_' => ' .. ទំព័រ',
	'pagine_post_' => 'ឆ្លើយតប',
	'pagine_post_s' => 'ចំលើយតប',
	'pagine_sujet_' => 'ប្រធានបទ',
	'pagine_sujet_s' => 'ប្រធានបទ',
	'par_' => 'ដោយ',
	'plugin_auteur' => 'The SpipBB Team: [See the list of contributors on Spip-contrib->http://www.spip-contrib.net/Plugin-SpipBB#contributeurs]', # NEW
	'plugin_description' => 'The SpipBB plugin provides the following features:
-* centralizes the forum management in SPIP (in the private area),
-* turns a main section (sector) of the site into a group of forum, "Bulletin Board" style, similar to phpBB. In this sector, sub-sections are used as forum groups, articles are dedicated forums where threads are made of messages posted to an article.

{{Please check:}}
-* [help and support on spipbb.spip-zone.info->http://spipbb.spip-zone.info/spip.php?article11],
-* [the documentation on Spip-contrib->http://www.spip-contrib.net/SpipBB-le-forum].

_ {{The SpipBB plugin is still being developed. You use it at your own risk.}}

_ [Access to the management panel-> .?exec=spipbb_configuration]', # NEW
	'plugin_licence' => 'Distributed under the GPL licence', # NEW
	'plugin_lien' => '[See the documentation of the plugin from Spip-contrib->http://www.spip-contrib.net/SpipBB-le-forum]', # NEW
	'plugin_mauvaise_version' => 'This version of the plugin cannot be used with your version of SPIP!', # NEW
	'plugin_nom' => 'SpipBB: Management of SPIP forums', # NEW
	'post_aucun_pt' => 'គ្មានមួយណា !',
	'post_efface_lui' => 'This topic contains @
nbr_post@ message(s). Deleted along with it!\\n', # NEW
	'post_ip' => 'ប្រកាស ត្រូវបានផ្ញើ ពីអាសយដ្ឋាន IP',
	'post_propose' => 'Suggested post', # NEW
	'post_rejete' => 'បានបដិសេធ ប្រកាស',
	'post_titre' => ' ::: Title: ', # NEW
	'post_verifier_sujet' => 'ផ្ទៀង ប្រកាសនេះ',
	'poste_valide' => 'ប្រកាស ត្រូវផ្ទៀង...',
	'poster_date_' => 'បានដាក់ប្រកាស៖',
	'poster_message' => 'ដាក់ប្រកាស មួយប្រធានបទ',
	'postit' => 'Postit', # NEW
	'postit_dpt' => 'Postit: ', # NEW
	'posts_effaces' => 'ប្រធានបទ ត្រូវបានលុបចេញ!',
	'posts_refuses' => 'Blocked topics to delete!', # NEW
	'previsualisation' => 'មើលមុន',
	'profil' => 'ពត៌មានផ្ទាល់ខ្លួន',

	// R
	'raison_clic' => 'ចុចទីនេះ',
	'raison_texte' => 'ដើម្បីដឹង ហេតុភេទ',
	'recherche' => 'ស្វែងរក',
	'recherche_elargie' => 'ស្វែងរក កំរិតខ្ពស់',
	'redige_post' => 'សរសេរ មួយប្រធានបទ​',
	'reglement' => '<p>The administrators and moderators of this forum will 
 endeavour to delete or edit all the messages with offending content
 as quickly as possible. However, it is impossible to check all the
 messages.You agree that all the messages posted to these forums 
 reflect the opinions of their respective authors and not the 
 opinions of the administrators, moderators or Webmasters (except the messages they post themselves) and consequently 
 cannot be held responsible or liable.</p>
 <p>You agree not to post messages containing
 abusive, illegal, sexually or racially objectionable, defamatory or 
 harassing language of any sort. Offenders may find themselves permanently 
 banned (and their ISP informed). The IP address of each 
 message is recorded in order to help uphold these regulations. You 
 agree that the Webmaster, the administrator and the moderators of 
 this forum have the right to delete, edit, move or lock any topic 
 of discussion at any moment. As a user, you agree that the 
 information you provide below will be stored in a database. 
 However, this information will not be disclosed to any third party 
 or company without your prior consent. The Webmaster, the 
 administrator and the moderators cannot be held liable if a 
 hacking attempt succeeds in accessing this data.</p>
 <p>This forum will log information via cookies stored in your 
 computer. These cookies will not contain any information that you 
 have entered below, their only goal is to enhance the user 
 experience.The e-mail address will be used only to confirm the 
 details of your registration and your password (and also to send 
 you a new password should you forget yours).</p>
 <p>By registering, you guarantee your agreement with the above 
 regulations.</p>', # NEW
	'repondre' => 'ឆ្លើយតប',
	'reponse_s_' => 'ចំលើយតប',
	'resultat_s_pour_' => 'លទ្ធផល សំរាប់',
	'retour_forum' => 'ត្រលប់ទៅ ទំព័រដើម នៃទីប្រជុំមតិ',

	// S
	's_abonner_a' => 'RSS: subscribe to this thread', # NEW
	'secteur_forum' => 'ឫស',
	'selection_efface' => 'បានលុបចេញ ជំរើស... ',
	'selection_tri_dpt' => 'ជ្រើសរើស វិធីរៀបលំដាប់៖',
	'sign_admin' => '{{This page can only be seen by the site owner.}}<p>It provides access to the plugin configuration of «{{<a href="http://www.spip-contrib.net/Plugin-SpipBB#contributeurs" class="copyright">SpipBB</a>}}» as well as to the forum management of your site.</p><p>Version : @version@ @distant@</p><p>See :
_ • [The documentation of Spip-Contrib->http://www.spip-contrib.net/?article2460]
_ • [Help and support on spipbb.spip-zone.info->http://spipbb.spip-zone.info/spip.php?article11]</p>@reinit@', # NEW
	'sign_maj' => '<br />ការបន្ទាន់សម័យ មានស្រាប់៖ @version@',
	'sign_ok' => 'ទាន់សម័យ',
	'sign_reinit' => '<p>Reset:
 _ • [the whole plugin->@plugin@]</p>', # NEW
	'sign_tempo' => 'Build with <a href="http://www.spip-contrib.net/Plugin-SpipBB#contributeurs" class="copyright">SpipBB</a>', # NEW
	'signature' => 'ហត្ថលេខា',
	'sinscrire' => 'ចុះឈ្មោះ',
	'site_propose' => 'Proposed Website', # NEW
	'site_web' => 'វ៉ែបសៃថ៍',
	'squelette_filforum' => 'Skeleton base for threads:', # NEW
	'squelette_groupeforum' => 'Skeleton base for forum groups:', # NEW
	'statut' => 'ស្ថានភាព',
	'statut_admin' => 'អ្នកអភិបាល',
	'statut_redac' => 'អ្នកសំរបសំរួល',
	'statut_visit' => 'សមាជិក',
	'sujet' => 'ប្រធានបទ',
	'sujet_auteur' => 'អ្នកនិពន្ធ',
	'sujet_clos_texte' => 'ប្រធានបទនេះ បានត្រូវបិទ, អ្នកគ្មានអាច ដាក់ប្រកាស ទៅវា។',
	'sujet_clos_titre' => 'Topic closed', # NEW
	'sujet_dpt' => 'ប្រធានបទ៖',
	'sujet_ferme' => 'ប្រធានបទ​៖ បានបិទ',
	'sujet_nombre' => 'លេខប្រធានបទ',
	'sujet_nouveau' => 'ប្រធានបទថ្មី',
	'sujet_rejete' => 'Rejected topic', # NEW
	'sujet_repondre' => 'ឆ្លើយតប',
	'sujet_s' => 'ប្រធានបទ',
	'sujet_valide' => 'ប្រធានបទ ត្រូវបញ្ជាក់ទទួលស្គាល់',
	'sujets' => 'ប្រធានបទ',
	'sujets_aucun' => 'No topics in this forum for the time being', # NEW
	'support_extra_normal' => 'ពិសេស',
	'support_extra_table' => 'តារាង',
	'supprimer' => 'លុបចេញ',
	'sw_admin_can_spam' => 'អនុញ្ញាតអ្នកអភិបាល ដាក់ប្រកាស ពាក្យឥតបានការ',
	'sw_admin_no_spam' => 'គ្មានវិចារឥតបានការ',
	'sw_ban_ip_titre' => 'ហាមអាសយដ្ឋាន IP ក៏មិនអី?',
	'sw_config_exceptions' => 'You can set exceptions for privileged members here. The members who meet these criteria will be permitted to post spam words.', # NEW
	'sw_config_exceptions_titre' => 'ការលើកលែង',
	'sw_config_generale' => 'Currently activated spam words:', # NEW
	'sw_config_generale_titre' => 'General spam filtering settings', # NEW
	'sw_config_warning' => 'Here you can define the text to PM your users if you choose to warn them via PM when they posts a spam word (max. 255 characters).', # NEW
	'sw_config_warning_titre' => 'Private message warning settings', # NEW
	'sw_disable_sw_titre' => '<strong>Enable Spam words filter</strong><br />If you need to do without this filter,<br />then click "No".', # NEW
	'sw_modo_can_spam' => 'Allow moderators to post spam words', # NEW
	'sw_nb_spam_ban_titre' => 'Number of offenses before user is automatically banned', # NEW
	'sw_pm_spam_warning_message' => 'This is a warning. You have tried to post a word that is defined as spam on this website. Please stop.', # NEW
	'sw_pm_spam_warning_titre' => 'ប្រយត្ន។',
	'sw_send_pm_warning' => '<strong>Send a PM to the user</strong> to warn them when they submit a post containing a prohibited word', # NEW
	'sw_spam_forum_titre' => 'Manage flagged posts', # NEW
	'sw_spam_titre' => 'Spam filtering', # NEW
	'sw_spam_words_action' => 'From this control panel you can add, edit, and remove spam words. Wildcards (*) are accepted in the word field. For example, *test* will match detestable, test* would match testing, *test would match detest.', # NEW
	'sw_spam_words_mass_add' => 'Paste or type your spam words lists into the text area. Separate each spam word by either a comma, semi-colon, or line-break', # NEW
	'sw_spam_words_titre' => 'Spam words filtering', # NEW
	'sw_spam_words_url_add' => 'Type the URL of a file containing a list of word formatted in the style above. Example: http://spipbb.spip-zone.info/IMG/csv/spamwordlist.csv .', # NEW
	'sw_warning_from_admin' => 'Select the administrator that is listed as the Private Message sender', # NEW
	'sw_warning_pm_message' => 'អត្ថបទ សារឯកជន',
	'sw_warning_pm_titre' => 'ប្រធានបទ សារឯកជន',
	'sw_word' => 'ពាក្យ',

	// T
	'title_ferme' => 'បិទ ទីប្រជុំមតិ/ដំណឹង',
	'title_libere' => 'បើកឡើងវិញ ទីប្រជុំមតិ/ដំណឹង​',
	'title_libere_maintenance' => 'Remove the Maintenance lock', # NEW
	'title_maintenance' => 'Close the forum/news for Maintenance', # NEW
	'title_sujet_ferme' => 'បិទប្រធានបទនេះ',
	'title_sujet_libere' => 'បើកឡើងវិញ ប្រធានបទនេះ',
	'titre_spipbb' => 'SpipBB',
	'total_membres' => 'យើងមាន សរុប',
	'total_messages_membres' => 'សមាជិកយើង បានដាក់ប្រកាស សរុប',
	'tous' => 'ទាំងអស់',
	'tous_forums' => 'គ្រប់ទីប្រជុំមតិ',
	'trier' => 'រៀបលំដាប់',
	'trouver_messages_auteur_dpt' => 'រកមើល គ្រប់សារ ពី៖',

	// V
	'visible_annuaire_forum' => 'លេចចេញ លើបញ្ជីសមាជិក',
	'visites' => 'ចំណូលមើល',
	'voir' => 'មើល',
	'votre_bio' => 'ជីវប្រវត្តិ សង្ខេប។',
	'votre_email' => 'អ៊ីមែវល៍ របស់អ្នក',
	'votre_nouveau_passe' => 'ពាក្យសំងាត់ថ្មី',
	'votre_signature' => 'ហត្ថលេខា របស់អ្នក៖',
	'votre_site' => 'ឈ្មោះវ៉ែបសៃថ៍ របស់អ្នក',
	'votre_url_avatar' => 'Your avatar\'s URL(http://...)', # NEW
	'votre_url_site' => 'អាសយដ្ឋានវ៉ែបសៃថ៍ របស់អ្នក'
);

?>
