<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/spipbb?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_propos_auteur_dpt' => 'همه چيز در باره‌ي :',
	'admin_action_01_configuration' => 'پيكربندي',
	'admin_action_02_etat' => 'وضعيت سخنگاه‌ها',
	'admin_action_ZZ_debug' => 'خطازدايي',
	'admin_action_effacer' => 'پيام‌هاي ردي ',
	'admin_action_fromphorum' => 'وارد كردن فاروم',
	'admin_action_fromphpbb' => 'وارد كردن PhpBB',
	'admin_action_gere_ban' => 'مديريت فهرست ممنوع',
	'admin_action_gestion' => 'مديريت',
	'admin_action_inscrits' => 'اعضاء',
	'admin_action_swconfig' => 'پيكربندي',
	'admin_action_swforum' => 'پست‌هاي مارك‌دار',
	'admin_action_swlog' => 'لاگ اسپم',
	'admin_action_swwords' => 'مديريت كلمات',
	'admin_afficher_bouton_alerte_abus' => 'نشان دادن دكمه‌ي هشدار سوءاستفاده',
	'admin_afficher_bouton_rss' => 'نمايش دكمه‌هاي آر.اس‌.اس',
	'admin_age_forum' => 'سن سخنگاه سال/ماه',
	'admin_avatar_affiche' => 'پذيرش و نمايش آواتارها (پيش‌گزينه پس از نصب آري است)',
	'admin_avatar_taille_contact' => 'اندازه‌ي آوتارها (به پيكسل) در صفحه تماس ',
	'admin_avatar_taille_profil' => 'اندازه‌ي آواتارها در صفحه‌ي شرح وضعيت (پروفايل)',
	'admin_avatar_taille_sujet' => 'اندازه‌ي آواتارها (به پيكسل) در صفحه‌ي سرفصل‌ها',
	'admin_average_posts' => 'ميانگين پست‌هاي روزانه',
	'admin_average_users' => 'ميانگين نوشته‌هاي روزانه',
	'admin_ban_email' => 'ادمين ايميل‌هاي ممنوعه',
	'admin_ban_email_info' => 'در نوشتن بيش از يك نشاني ايميل، با كاما جداسازي كنيد. از ژوكر براي يوزرنيم استفاده كنيد؛ به عنوان نمونه: *@hotmail.com',
	'admin_ban_email_none' => 'بدون نشاني ممنوع',
	'admin_ban_ip' => 'مديريت نشاني‌‌هاي آي.پي ممنوعه',
	'admin_ban_ip_info' => 'در نوشتن بيش از يك آي.پي يا سرور، آن‌ها را كاما جدا كنيد. براي تعيين فواصل آي.پي، اولي و آخري را با دش (-) از هم جدا كنيد. براي مشخص سازي ژوكر از ستاره (*) استفاه كنيد.',
	'admin_ban_ip_none' => 'هيچ نشاني ممنوعه',
	'admin_ban_user' => 'ادمين لاگين‌هاي ممنوع',
	'admin_ban_user_info' => 'مي‌توانيد با استفاده از CTRL يا UPدر تركيب با موس يا صفحه‌كليد بيش از يك كاربر را در آن واحد ممنوع كنيد',
	'admin_ban_user_none' => 'هيچ كاربري موجود نيست',
	'admin_cat_01_general' => 'مديريت',
	'admin_cat_outils' => 'ابزارها',
	'admin_cat_spam' => 'ضد اسپم',
	'admin_config_prerequis' => 'الزام‌ها',
	'admin_config_spam_words' => 'پيكربندي ضد اسپم',
	'admin_config_spipbb' => 'فعال‌سازي اسپيپ بي‌بي(SpipBB)',
	'admin_config_spipbb_info' => 'براي فعال‌سازي اسپيپ بي‌بي بله را كليك كنيد',
	'admin_config_tables' => 'پيكربندي جدول‌هاي اسپيپ بي‌بي ',
	'admin_config_tables_erreur' => 'مشكل در جدول‌هاي اسپيپ بي‌بي:@tables_erreur@ نادرست هستند (جدول‌هاي @tables_ok@ درست به نظر مي‌رسند).
 به [documentation on Spip-Contrib->http://www.spip-contrib.net/SpipBB-le-forum] or  مراجعه كنيد.[support on spipbb.spip-zone->http://spipbb.spip-zone.info/spip.php?article11]',
	'admin_config_tables_ok' => 'جدول‌هاي پايگاه‌داده‌هاي اسپيپ بي‌بي درست نصب شده (@tables_ok@)',
	'admin_date_ouverture' => 'تاريخ گشايش',
	'admin_debug_log' => 'فايل لاگ @log_name@ ',
	'admin_debug_metas' => 'اسپيپ بي‌بي متاس (SpipBB METAs)',
	'admin_form_action' => 'حركت',
	'admin_form_creer_categorie' => 'ايجاد يك مقوله',
	'admin_form_creer_forum' => 'ايجاد يك فرم',
	'admin_form_deplacer' => 'جا به جايي',
	'admin_form_descendre' => 'پائين',
	'admin_form_editer' => 'ويرايش ',
	'admin_form_messages' => ' ',
	'admin_form_monter' => 'بالا',
	'admin_form_sujets' => ' ',
	'admin_forums_affiche_membre_defaut' => 'آيا مي‌خواهيد نمايش نام اعضاء مندرچ در فهرست اعضاء را در حاليكه خودشان انتخاب نكرده‌اند، نمايش دهيد؟ <br />[پيش‌گزيده نه] ',
	'admin_forums_configuration' => 'پيكربندي اسپيپ بي‌بي',
	'admin_forums_configuration_avatar' => 'پيكربندي كلي آواتارها ',
	'admin_forums_configuration_options' => 'گزينه‌هاي اسپيپ بي‌بي ',
	'admin_forums_log_level' => 'گزينش سطح لاگ‌هاي توليدي اسپيپ بي‌بي.<br/> [پيش‌گزيده - 3 (بيشترين)]',
	'admin_forums_log_level_0' => 'بدون ورودي',
	'admin_forums_log_level_1' => 'ورودي‌هاي معدود',
	'admin_forums_log_level_2' => 'ورودي‌هاي فراوان',
	'admin_forums_log_level_3' => 'ورودي‌هاي خيلي زياد',
	'admin_id_mjsc' => '#',
	'admin_infos' => 'اسپيپ‌ بي‌بي - ادمين - خلاصه',
	'admin_interface' => 'گزينه‌هاي ميانجي همگاني',
	'admin_nombre_lignes_messages' => 'تعداد سطر‌هاي پيام',
	'admin_plugin_requis_erreur' => 'پلاگين مورد نياز زير موجود نيست. فعالش كنيد!',
	'admin_plugin_requis_erreur_balisesession' => 'پلاگين  Balise SESSION فعال كنيد! [مستندات آن اينجاست->http://www.spip-contrib.net/?article1224], [Archive ZIP là->http://files.spip.org/spip-zone/balise_session.zip].',
	'admin_plugin_requis_erreur_cfg' => 'پلاگين CFG را نصب و فعال كنيد!  [مستنداتش اينجاست->http://www.spip-contrib.net/?article1605], [Archive ZIP là->http://files.spip.org/spip-zone/cfg.zip].',
	'admin_plugin_requis_erreur_s' => 'پلاگين‌هاي مورد نياز زير موجود نيستند. فعالشان كنيد!',
	'admin_plugin_requis_ok' => 'پلاگين‌هاي نصب و فعال شده: ',
	'admin_plugin_requis_ok_balisesession' => '[پلاگين BALISE_SESSION->http://www.spip-contrib.net/?article1224] : اطلاعاتي در مورد بازديد‌كنندگاني كه وارد شده‌اند ارايه مي‌دهد.',
	'admin_plugin_requis_ok_cfg' => '[پلاگين CFG->http://www.spip-contrib.net/?article1605] : كاركردها و برچسب‌ها را تأمين مي‌كند.',
	'admin_sous_titre' => 'برو به پنل سخنگاه‌هاي ادمين اسپيپ‌ بي‌بي',
	'admin_spip_config_forums' => 'پيكربندي اسپيپ :',
	'admin_spip_forums_ok' => 'سخنگاه‌هاي همگاني فعال هستند.',
	'admin_spip_forums_warn' => '<p>{{توجه}} : سخنگاه‌هاي شما به صورت پيش‌گزيده غيرفعال‌اند. تنظيمات پيشنهادي استفاده‌ي از فعال‌سازي خودكار آن‌هاست.  [ينجا را بنگريد->@config_contenu@].</p><p>Sinon vous devrez les activer articles par articles.</p>',
	'admin_spip_mots_cles_ok' => 'كليدواژه‌ها فعال‌اند.',
	'admin_spip_mots_cles_warn' => '<p>{{توجه}} :كليدواژه‌ها در اسپيپ فعال نيستند.نمي‌توانيد آن‌ها را براي خصوصيات پيشرفته مورد استفاده قرار دهيد.</p><p> پيشنهاد مي‌شود آن‌ها را فعال سازيد: [بنگريد به اينجا->@configuration@].</p>',
	'admin_spip_mots_forums_ok' => 'كليد‌واژه‌هاي مربوط به سخنگاه‌ها فعال‌اند.',
	'admin_spip_mots_forums_warn' => '<p>{{توجه}} : كليدواژه‌هاي مربوط به سخنگاه‌هاي همگاني در اسپيپ فعال نيستند، نمي‌توانيد آن‌ها را براي خصوصيات پيشرفته مورد استفاده قرار دهيد. </p><p> فعال‌سازي آن‌ها توصيه مي‌شود: [بنگريد به اينجا->@configuration@].</p>',
	'admin_spipbb_release' => 'نسخه‌ي اسپيپ بي‌بي ',
	'admin_statistique' => 'اطلاعات',
	'admin_surtitre' => 'مديريت سخنگاه‌ها',
	'admin_temps_deplacement' => 'زمان لازم پيش از حذف يك ادمين',
	'admin_titre' => 'ادمين اسپيپ بي‌بي',
	'admin_titre_page_spipbb_admin' => 'ادمين سخنگاه‌ها',
	'admin_titre_page_spipbb_admin_anti_spam_config' => 'پيكربندي كلي فيلتر اسپم',
	'admin_titre_page_spipbb_admin_anti_spam_forum' => 'پست‌هاي نشان‌دار',
	'admin_titre_page_spipbb_admin_anti_spam_log' => 'ورودي اسپم‌',
	'admin_titre_page_spipbb_admin_anti_spam_words' => 'فيلترسازي واژه‌ها',
	'admin_titre_page_spipbb_admin_debug' => 'خطازدايي',
	'admin_titre_page_spipbb_admin_etat' => 'اسپيپ بي‌بي - ادمين - خلاصه',
	'admin_titre_page_spipbb_admin_gere_ban' => 'ادمين ممنوعيت',
	'admin_titre_page_spipbb_admin_migre' => 'وارداتِ @nom_base@',
	'admin_titre_page_spipbb_configuration' => 'پيكربندي اسپيپ بي‌بي',
	'admin_titre_page_spipbb_effacer' => 'ادمين پست‌هاي مسدود شده',
	'admin_titre_page_spipbb_inscrits' => 'ادمين اعضاء',
	'admin_titre_page_spipbb_sujet' => 'ويرايش يك رشته',
	'admin_total_posts' => 'تعداد كل پيام‌ها',
	'admin_total_users' => 'تعداد اعضاء',
	'admin_total_users_online' => 'اعضاء برخط',
	'admin_unban_email_info' => 'با استفاده از سي.تي.آر.ال و ام.اي.جي تركيبي مي‌توانيد يكباره تعدادي نشاني را به حالت اول برگردانيد',
	'admin_unban_ip_info' => 'با استفاده از سي.تي.آر.ال يا ام.اي.جي مي‌توانيد به يكباره تعداد نشاني را با موش‌واره و صفحه‌ كليد به حالت اول بگردانيد',
	'admin_unban_user_info' => 'با استفاده از سي.تي.آر.ال يا ام.اي.جي با موش‌واره يا صفحه كليد مي‌توانيد يك باره تعدادي كاربر را به حالت اول برگردانيد',
	'admin_valeur' => 'ارزش',
	'aecrit' => 'نوشته: ',
	'alerter_abus' => 'اين پيام را به اين عنوان كه فحش است گزارش دهيد  ',
	'alerter_sujet' => 'پيام دشنام',
	'alerter_texte' => 'نگاهي به اين پيام بياندازيد:‌',
	'annonce' => 'اطلاعيه',
	'annonce_dpt' => 'اطلاعيه:',
	'anonyme' => 'ناشناس',
	'auteur' => 'نويسنده',
	'avatar' => 'اواتار',

	// B
	'bouton_select_all' => 'گزينش همه',
	'bouton_speciaux_sur_skels' => 'پيكربندي دكمه‌هاي اختصاصي روي اسكلت‌هاي همگاني',
	'bouton_unselect_all' => 'نامزد كردن همه',

	// C
	'champs_obligatoires' => 'ميدان‌هاي نشاني‌گذاري شده با * اجباري هستند. ',
	'chercher' => 'جستجو',
	'choix_mots_annonce' => 'صدور اطلاعيه',
	'choix_mots_creation' => 'اگر مي‌خواهيد <strong> به صورت خودكار</strong> كليدواژه‌هاي اختصاصي براي اسپيپ بي‌بي ايجاد كنيد، روي اين دكمه كليك كنيد. اين كليد‌واژه بعداً مي‌توانند اصلاح يا حذف شوند... ',
	'choix_mots_creation_submit' => 'پيكربندي خودكار كليدواژه‌ها',
	'choix_mots_ferme' => 'براي بستن يك رشته',
	'choix_mots_postit' => 'چسبانكي سازي',
	'choix_mots_selection' => 'اين گروه كليدواژه دست كم بايد سه كليدواژه داشته باشد. معمولاً اين پلاگين هنگام نصب آن‌ها را مي‌سازد. اسپيپ بي‌بي به طور كلي از كليد‌واژه‌هاي {ferme} (بسته), {annonce}(آگهي) و  {postit} (چسبانكي) استفاده مي‌كند، اما شما مي‌توانيد كليدواژه‌هاي ديگري انتخاب كنيد.',
	'choix_rubrique_creation' => 'اگر مي‌خواهيد <strong> به صورت خودكار</strong> بخشي شامل سخنگاه‌هاي اسپيپ بي‌بي و نخستين سخنگاه خالي را ايجاد نيد، اين دكمه را كليك كنيد.اين سخنگاه و سلسله مراتب ايجاد شد بعداً مي‌تواند اصللاح يا حذف كرد...',
	'choix_rubrique_creation_submit' => 'پيكربندي خودكار بخش',
	'choix_rubrique_selection' => 'انتخاب يك بخش كه ميزبان اساس سخنگاه‌هاي شما خواهد شد. در اين بخش، هر زيربخش يك گروه سخنگاه قرار خواهد گرفت، هر مقاله‌ي منتشر شده يك سخنگاه جديد را باز خواهد كرد.',
	'choix_squelettes' => 'مي‌توانيد اسكلت‌هاي ديگري انتخاب كنيد، اما فايل‌هايي كه قرار است جايگزين فايل‌هاي  groupeforum.html و filforum.html شوند بايد ايجاد شده باشند!',
	'citer' => 'نقل قول',
	'cocher' => 'تيك',
	'col_avatar' => 'اواتار',
	'col_date_crea' => 'تاريخ ثبت ',
	'col_marquer' => 'نشان',
	'col_signature' => 'امضاء',
	'config_affiche_champ_extra' => 'نمايش ميدان: <b>@nom_champ@</b>',
	'config_affiche_extra' => 'ميدان‌هاي زير را در اسكلت‌ها نشان دادن',
	'config_champs_auteur' => 'ميدان‌هاي اسپيپ بي‌بي',
	'config_champs_auteurs_plus' => 'ادمين ميدان‌هاي نويسندگان تكميلي',
	'config_champs_requis' => 'ميدان‌هاي مورد نياز اسپيپ بي‌بي',
	'config_choix_mots' => 'انتخاب گروه كليد‌واژه‌ها',
	'config_choix_rubrique' => 'انتخاب بخش مورد استفاده براي سخنگاه‌هاي اسپيپ بي‌بي',
	'config_choix_squelettes' => 'انتخاب اسكلت‌ها',
	'config_orig_extra' => 'منبع مورد استفاده براي ميدان‌هاي اضافي',
	'config_orig_extra_info' => ' ميدان‌هاي EXTRA Infos  يا ديگر جدول پايگاه‌داده‌ها، جدول,auteurs_profils.',
	'config_spipbb' => 'پيكربندي اساس اسپيپ بي‌بي براي مورد استفاده قرار دادن سخنگاه‌ها با اين پلاگين.',
	'contacter' => 'تماس',
	'contacter_dpt' => 'تماس: ',
	'creer_categorie' => 'ايجاد يك مقوله‌ي جديد',
	'creer_forum' => 'ايجاد يك سخنگاه جديد',

	// D
	'dans_forum' => 'داخل سخنگاه',
	'deconnexion_' => 'قطع تماس',
	'deplacer' => 'جابه‌چايي',
	'deplacer_confirmer' => 'تأييد جابه‌جايي',
	'deplacer_dans_dpt' => 'جابه‌جايي در سخنگاه:',
	'deplacer_sujet_dpt' => 'جابه‌چاييِ:',
	'deplacer_vide' => 'باقي نماندن هيچ سخنگاه ديگر: جابه‌جايي اين رشته ممكن نسيت.',
	'dernier' => 'آخرين',
	'dernier_membre' => 'آخرين عضو ثبت‌‌ نام كرده:‌',
	'derniers_messages' => 'آخرين پيام‌ها',
	'diviser' => 'تقسيم',
	'diviser_confirmer' => 'تأييد تقسيم پيام‌ها',
	'diviser_dans_dpt' => 'قرار دادن در اين سخنگاه:',
	'diviser_expliquer' => 'با استفاده از فرم زير مي‌توانيد يك مطلب را دو قسمت كنيد، هم از طريق انتخاب پست‌ها به صورت جداگانه يا با تقسيم يك پست انتخاب شده. ',
	'diviser_selection_dpt' => 'گزينش:',
	'diviser_separer_choisis' => 'تقسيم پست‌هاي انتخاب شده',
	'diviser_separer_suite' => 'تقسيم از پيام انتخاب شده',
	'diviser_vide' => 'سخنگاه ديگري باقي نمانده: تقسيم اين رشته ممكن نيست.',

	// E
	'ecrirea' => 'ارسال يك ايميل به ',
	'effacer' => 'حذف',
	'email' => 'ايميل',
	'en_ligne' => 'چه كسي برخط است؟‌',
	'en_rep_sujet_' => ':::موضوع : ',
	'en_reponse_a' => 'پاسخ به يك پيام',
	'etplus' => ' ... و بيشتر ...',
	'extra_avatar_saisie_url' => 'يوآرال اواتار شما (http://...)',
	'extra_avatar_saisie_url_info' => 'يوآرال اواتار بازديدكننده',
	'extra_date_crea' => 'داده‌هاي ثبت نامه',
	'extra_date_crea_info' => 'داده‌هاي ثبت نام اسپيپ بي‌بي',
	'extra_emploi' => 'شغل',
	'extra_localisation' => 'اقامت',
	'extra_loisirs' => 'دلبستگي‌ها',
	'extra_nom_aim' => 'شناسه‌ي تماس‌ها (اي.آي.ام)',
	'extra_nom_msnm' => 'شناسه تماس‌ها (ام.اس.ان پيام‌)',
	'extra_nom_yahoo' => 'شناسه تماس‌ها (ياهو)',
	'extra_numero_icq' => 'شناسه تماس‌ها (آي.سي.كيو)',
	'extra_refus_suivi_thread' => '(منع پيگيري) اصلاح نكنيد!',
	'extra_refus_suivi_thread_info' => 'رشته‌هايي كه نمي‌خواهيد پيگيري كنيد',
	'extra_signature_saisie_texte' => 'امضاء خود را اينجا بگذاريد',
	'extra_signature_saisie_texte_info' => 'متن كوتاه براي امضاء پيام‌ها',
	'extra_visible_annuaire' => 'نمايش در فهرست (همگاني)اعضاء',
	'extra_visible_annuaire_info' => 'اجازه دهد كه شما در ليست همگاني اعضاء ظاهر نشويد',

	// F
	'fiche_contact' => 'فرم تماس ',
	'fil_annonce_annonce' => 'انتقال اين موضوع به اطلاعيه',
	'fil_annonce_desannonce' => 'حذف حالت آگهي',
	'fil_deplace' => 'حذف اين رشته',
	'filtrer' => 'فيلتر',
	'forum' => 'سخنگاه‌ها',
	'forum_annonce_annonce' => 'علامت آگهي زدن ',
	'forum_annonce_desannonce' => 'حذف علامت آگهي',
	'forum_dpt' => 'سخنگاه:',
	'forum_ferme' => 'اين سخنگاه بسته شده',
	'forum_ferme_texte' => 'اين سخنگاه بسته شده. نمي‌توانيد پيام به آن پست كنيد.',
	'forum_maintenance' => 'اين سخنگاه در حال حاضر بسته است',
	'forum_ouvrir' => 'بازگشايي اين سخنگاه',
	'forums_categories' => 'متفرقه',
	'forums_spipbb' => 'سخنگاه‌هاي اسپيپ بي‌بي',
	'forums_titre' => 'نخستين سخنگاه ايجاد شده‌ي من',
	'fromphpbb_erreur_db_phpbb_config' => 'قرائت مقدار پيكربندي در پايگاه‌داده‌هاي اسپيپ بي‌بي ممكن نيست',
	'fromphpbb_migre_categories' => 'انتقال مقوله‌ها',
	'fromphpbb_migre_categories_dans_rub_dpt' => 'كاشت سخنگاه‌ها در اين بخش: ',
	'fromphpbb_migre_categories_forum' => 'سخنگاه',
	'fromphpbb_migre_categories_groupe' => 'گروه',
	'fromphpbb_migre_categories_impossible' => 'انتقال مقوله‌ها ممكن نيست',
	'fromphpbb_migre_categories_kw_ann_dpt' => 'آگهي‌ها اين كليدواژه را استفاده خواهند كرد:',
	'fromphpbb_migre_categories_kw_ferme_dpt' => 'موضوع‌هاي بسته شده اين كليد وارژه را استفاده مي‌كنند:‌',
	'fromphpbb_migre_categories_kw_postit_dpt' => 'پست‌ها اين كليدواژه را استفاده مي‌كنند:',
	'fromphpbb_migre_existe_dpt' => 'موجود:',
	'fromphpbb_migre_thread' => 'انتقال موضوع‌ها و پست‌ها',
	'fromphpbb_migre_thread_ajout' => 'افزودن رشته',
	'fromphpbb_migre_thread_annonce' => 'آگهي',
	'fromphpbb_migre_thread_existe_dpt' => 'سخنگاه موجود:',
	'fromphpbb_migre_thread_ferme' => 'بسته',
	'fromphpbb_migre_thread_impossible_dpt' => 'انتقال موضوع‌ها غيرممكن است: ',
	'fromphpbb_migre_thread_postit' => 'اطلاعيه',
	'fromphpbb_migre_thread_total_dpt' => 'تعداد كل موضوع‌ها و پست‌هاي افزوده شده:',
	'fromphpbb_migre_utilisateurs' => 'انتقال كاربران',
	'fromphpbb_migre_utilisateurs_admin_restreint_add' => 'افزودن ادمين محدود',
	'fromphpbb_migre_utilisateurs_admin_restreint_already' => 'ادمين‌هاي محدود موجود',
	'fromphpbb_migre_utilisateurs_impossible' => 'انتقال كاربران ممنوع است',
	'fromphpbb_migre_utilisateurs_total_dpt' => 'كل كاربران افزوده شده:',

	// H
	'haut_page' => 'بالاي صفحه',

	// I
	'icone_ferme' => 'بستن',
	'import_base' => 'نام پايگاه داده‌ها:',
	'import_choix_test' => 'آزمايش جاي خالي (پيش‌گزيده):',
	'import_choix_test_titre' => 'انتقال خالي يا واقعي',
	'import_erreur_db' => 'اتصال به پايگاه داده‌‌هاي @nom_base@ ممكن نيست',
	'import_erreur_db_config' => 'قرائت پيكربندي پايگاه‌داده‌هاي@nom_base@  ممكن نسيت',
	'import_erreur_db_rappel_connexion' => 'اتصال دوباره به پايگاه‌داده‌هاي @nom_base@ ممكن نيست',
	'import_erreur_db_spip' => 'اتصال به پايگاه داده‌هاي اسپيپ ممكن نيست',
	'import_erreur_forums' => 'انتقال سخنگاه‌ها ممكن نسيت',
	'import_fichier' => 'پرونده پيكربندي  @nom_base@  يافت شد:',
	'import_host' => 'نام/نشاني سرور',
	'import_login' => 'لاگين:‌',
	'import_parametres_base' => 'لطفاً مسير پرونده پيكربندي @nom_base@، يا پارامتر‌هاي پيكربندي را انتخاب كنيد:',
	'import_parametres_rubrique' => 'بخشي را كه در آن سخنگاه‌هاي @nom_base@ منتقل مي‌شود انتخاب كنيد',
	'import_parametres_titre' => 'اطلاعات مربوط به پايگاه @nom_base@',
	'import_password' => 'گذر واژه:',
	'import_prefix' => 'پيشوند جدول‌ها: ',
	'import_racine' => 'مسير به @nom_base@ (اواتارها): ',
	'import_table' => 'جدول پيكربندي @nom_base@ يافت شده: ',
	'import_titre' => 'انتقال سخنگاه‍ @nom_base@',
	'import_titre_etape' => 'انتقال سخنگاه@nom_base@ - مرحله ',
	'info' => 'اطلاعات',
	'info_annonce_ferme' => 'حالت آگهي/بسته',
	'info_confirmer_passe' => 'تأييد اين گذرواژه‌ي جديد: ',
	'info_ferme' => 'وضعيت بسته',
	'info_inscription_invalide' => 'تبت نامه ممكن نيست',
	'info_plus_cinq_car' => 'بيش از 5 كاراكتر',
	'infos_refus_suivi_sujet' => 'اين رشته‌ها را دنبال نكنيد',
	'infos_suivi_forum_par_inscription' => 'با ثبت نامه اين سخنگاه را دنبال كنيد',
	'inscription' => 'ثبت نام',
	'inscrit_le' => 'ثبت نام در',
	'inscrit_le_dpt' => 'ثبت نام در:‌',
	'inscrit_s' => 'ثبت نام شده',
	'ip_adresse_autres' => 'نشاني آي.پي هاي ديگري كه اين كاربر با آن‌ها پست كرده ',
	'ip_adresse_membres' => 'كاربران ديگري كه با اين آي.پي پست كرده‌اند',
	'ip_adresse_post' => 'نشاني آي.پي اين پيام ',
	'ip_informations' => 'اطلاعات يك نشاني آي.پي و نشاني‌هاي ديگر',

	// L
	'le' => 'اين',
	'liste_des_messages' => 'فهرست پيام‌ها',
	'liste_inscrits' => 'فهرست اعضاء',
	'login' => 'اتصال',

	// M
	'maintenance' => 'نگهداري',
	'maintenance_fermer' => 'مقاله/سخنگاه بسته:',
	'maintenance_pour' => 'براي نگهداري.',
	'membres_en_ligne' => 'اعضاء برخط',
	'membres_inscrits' => 'اعضاء ثبت نام شده',
	'membres_les_plus_actifs' => 'فعال‌ترين عضوها',
	'message' => 'پيام',
	'message_s' => 'پيام‌ها',
	'message_s_dpt' => 'پيام‌ها:‌',
	'messages' => 'پاسخ‌ها',
	'messages_anonymes' => 'ناشناس',
	'messages_derniers' => 'آخرين پيام',
	'messages_laisser_nom' => 'نام‌گذاري',
	'messages_supprimer_titre_dpt' => 'براي پيام‌ها:',
	'messages_supprimer_tous' => 'حذف',
	'messages_voir_dernier' => 'ديدن آخرين پيام ',
	'messages_voir_dernier_s' => 'ديدن آخرين پيام‌ها',
	'moderateur' => 'هماهنگ كننده',
	'moderateur_dpt' => 'هماهنگ كننده:',
	'moderateurs' => 'هماهنگ كننده (ها)',
	'moderateurs_dpt' => 'هماهنگ كننده‌ها:',
	'modif_parametre' => 'پارامترهاي خود را اصلاح كنيد',
	'mot_annonce' => 'اطلاعيه
_ يك اطلاعيه بالاي سخنگاه در تمام صفحه‌ها‌يقرار مي‌گيرد.',
	'mot_ferme' => 'بسته
وقتي يك سخنگاه-مقاله از اين كليدواژه استفاده كند، فقط هماهنگ كننده مي‌تواند پيام پست كند.
-* وقتي يك موضوع سخنگاه بسته باشد فقط هماهنگ كننده مي‌تواند يك پيام پست كند.',
	'mot_groupe_moderation' => 'گروه كليدواژه‌هاي مورد استفاده براي هماهنگ‌سازي اسپيپ بي‌بي',
	'mot_postit' => 'Postit
_ Un postit est situé en dessous des annonces, avant les messages ordinaires. Il n\'apparaît qu\'une seule fois dans la liste.', # NEW

	// N
	'no_message' => 'Aucun sujet ou message ne correspond à vos critères de recherche', # NEW
	'nom_util' => 'Nom d\'utilisateur', # NEW
	'non' => 'Non', # NEW

	// O
	'ordre_croissant' => 'Croissant', # NEW
	'ordre_decroissant' => 'Décroissant', # NEW
	'ordre_dpt' => 'Ordre :', # NEW
	'oui' => 'Oui', # NEW

	// P
	'pagine_page_' => ' .. page ', # NEW
	'pagine_post_' => ' réponse', # NEW
	'pagine_post_s' => ' réponses', # NEW
	'pagine_sujet_' => ' sujet', # NEW
	'pagine_sujet_s' => ' sujets', # NEW
	'par_' => 'par ', # NEW
	'plugin_auteur' => 'La SpipBB Team : [voir la liste des contributeurs sur Spip-contrib->http://www.spip-contrib.net/Plugin-SpipBB#contributeurs]', # NEW
	'plugin_description' => 'Le plugin SpipBB permet :
-* De gérer de façon centralisée les forums de SPIP (interface privée),
-* D\'utiliser un secteur comme base d\'un groupe de forums comme les «Bulletin Board» tels que phpBB. Dans ce secteur, les sous-rubriques sont des groupes de forums, les articles des forums, chaque message dans le forum d\'un article y démarre un thread.

{{Consultez :}}
-* •[l\'aide et support sur spipbb.spip-zone.info->http://spipbb.free.fr/spip.php?article11],
-* •[La documentation sur Spip-contrib->http://www.spip-contrib.net/SpipBB-le-forum].

_ {{Plugin spipbb en cours de developpement. Vous l\'utilisez à vos risques et périls}}

_ [Accès au panneau d\'administration-> .?exec=spipbb_configuration]', # NEW
	'plugin_licence' => 'Distribué sous licence GPL', # NEW
	'plugin_lien' => '[Consulter la documentation du plugin sur Spip-contrib->http://www.spip-contrib.net/SpipBB-le-forum]', # NEW
	'plugin_mauvaise_version' => 'Cette version du plugin n\'est pas compatible avec votre version de SPIP !', # NEW
	'plugin_nom' => 'SpipBB : Gestion des forums de SPIP', # NEW
	'post_aucun_pt' => 'aucun !', # NEW
	'post_efface_lui' => 'Ce sujet comprend @nbr_post@ message(s). Effacés avec lui !\\n', # NEW
	'post_ip' => 'Messages posté à partie de l\'adresse IP', # NEW
	'post_propose' => 'Message proposé', # NEW
	'post_rejete' => 'Message rejeté', # NEW
	'post_titre' => ' ::: Titre : ', # NEW
	'post_verifier_sujet' => 'Vérifier ce sujet', # NEW
	'poste_valide' => 'Post(s) à valider ...', # NEW
	'poster_date_' => 'Posté le : ', # NEW
	'poster_message' => 'Poster un message', # NEW
	'postit' => 'Postit', # NEW
	'postit_dpt' => 'Postit : ', # NEW
	'posts_effaces' => 'Messages effacés !', # NEW
	'posts_refuses' => 'Messages refusés, à effacer !', # NEW
	'previsualisation' => 'Prévisualisation', # NEW
	'profil' => 'Profil', # NEW

	// R
	'raison_clic' => 'cliquez ici', # NEW
	'raison_texte' => 'Pour en connaitre la raison', # NEW
	'recherche' => 'Recherche', # NEW
	'recherche_elargie' => 'Recherche élargie', # NEW
	'redige_post' => 'Ecrire message', # NEW
	'reglement' => '<p>Les administrateurs et modérateurs de ce forum s\'efforceront de supprimer
				ou éditer tous les messages à caractère répréhensible
				aussi rapidement que possible. Toutefois, il leur est impossible de passer en revue tous
				les messages. Vous admettez donc que tous les messages postés sur ces forums
				expriment la vue et opinion de leurs auteurs respectifs, et non pas des administrateurs,
				ou modérateurs, ou webmestres (excepté les messages postés par
				eux-même) et par conséquent ne peuvent pas être tenus pour responsables.</p>
				<p>Vous consentez à ne pas poster de messages injurieux, obscènes,
				vulgaires, diffamatoires, menaçants, sexuels ou tout autre message qui violerait
				les lois applicables. Le faire peut vous conduire à être banni
				immédiatement de façon permanente (et votre fournisseur d\'accès
				à internet en sera informé). L\'adresse IP de chaque message est
				enregistrée afin d\'aider à faire respecter ces conditions.
				Vous êtes d\'accord sur le fait que le webmestre, l\'administrateur
				et les modérateurs de ce forum ont le droit de supprimer, éditer,
				déplacer ou verrouiller n\'importe quel sujet de discussion à tout moment.
				En tant qu\'utilisateur, vous êtes d\'accord sur le fait que toutes les informations
				que vous donnerez ci-après seront stockées dans une base de données.
				Cependant, ces informations ne seront divulguées à aucune tierce personne
				ou société sans votre accord. Le webmestre, l\'administrateur,
				et les modérateurs ne peuvent pas être tenus pour responsables si une
				tentative de piratage informatique conduit à l\'accès de ces données.</p>
				<p>Ce forum utilise les cookies pour stocker des informations sur votre ordinateur.
				Ces cookies ne contiendront aucune information que vous aurez entré ci-après,
				ils servent uniquement à améliorer le confort d\'utilisation.
				L\'adresse e-mail est uniquement utilisée afin de confirmer les détails
				de votre enregistrement ainsi que votre mot de passe (et aussi pour vous envoyer un nouveau
				mot de passe dans la cas où vous l\'oublieriez).</p>
				<p>En vous enregistrant, vous vous portez garant du fait d\'être en accord avec le
				règlement ci-dessus.</p>', # NEW
	'repondre' => 'Répondre', # NEW
	'reponse_s_' => 'Réponses', # NEW
	'resultat_s_pour_' => ' résultats pour ', # NEW
	'retour_forum' => 'Retour à l\'accueil du forum', # NEW

	// S
	's_abonner_a' => 'RSS . S\'abonner à : ', # NEW
	'secteur_forum' => 'RACINE', # NEW
	'selection_efface' => 'Effacer la sélection .. ', # NEW
	'selection_tri_dpt' => 'Sélectionner la méthode de tri :', # NEW
	'sign_admin' => '{{Cette page est uniquement accessible aux responsables du site.}}<p>Elle donne accès à la configuration du plugin «{{<a href="http://www.spip-contrib.net/Plugin-SpipBB#contributeurs" class="copyright">SpipBB</a>}}» ainsi qu\'à la gestion des forums du site.</p><p>Version : @version@ @distant@</p><p>Consultez :
_ • [La documentation sur Spip-Contrib->http://www.spip-contrib.net/?article2460]
_ • [L\'aide et support sur spipbb.spip-zone.info->http://spipbb.free.fr/spip.php?article11]</p>@reinit@', # NEW
	'sign_maj' => '<br />Version plus récente disponible : @version@', # NEW
	'sign_ok' => 'à jour.', # NEW
	'sign_reinit' => '<p>Ré-initialisation :
_ • [de tout le plugin->@plugin@]</p>', # NEW
	'sign_tempo' => 'Réalisé avec <a href="http://www.spip-contrib.net/Plugin-SpipBB#contributeurs" class="copyright">SpipBB</a>', # NEW
	'signature' => 'Signature', # NEW
	'sinscrire' => 'S\'inscrire', # NEW
	'site_propose' => 'Site proposé par @auteur_post@', # NEW
	'site_web' => 'Site web', # NEW
	'squelette_filforum' => 'Base de squelette pour les fils de discussions :', # NEW
	'squelette_groupeforum' => 'Base de squelette pour les groupes de discussions :', # NEW
	'statut' => 'Statut', # NEW
	'statut_admin' => 'Administrateur', # NEW
	'statut_redac' => 'Rédacteur', # NEW
	'statut_visit' => 'Membre', # NEW
	'sujet' => 'Sujet', # NEW
	'sujet_auteur' => 'Auteur', # NEW
	'sujet_clos_texte' => 'Ce sujet est clos, vous ne pouvez pas y poster.', # NEW
	'sujet_clos_titre' => 'Sujet Clos', # NEW
	'sujet_dpt' => 'Sujet : ', # NEW
	'sujet_ferme' => 'Sujet : fermé', # NEW
	'sujet_nombre' => 'Nombre de Sujets', # NEW
	'sujet_nouveau' => 'Nouveau sujet', # NEW
	'sujet_rejete' => 'Sujet rejeté', # NEW
	'sujet_repondre' => 'Répondre au sujet', # NEW
	'sujet_s' => 'Sujets', # NEW
	'sujet_valide' => 'Sujet à valider', # NEW
	'sujets' => 'Sujets', # NEW
	'sujets_aucun' => 'Pas de sujet dans ce forum pour l\'instant', # NEW
	'support_extra_normal' => 'extra', # NEW
	'support_extra_table' => 'table', # NEW
	'supprimer' => 'Supprimer', # NEW
	'sw_admin_can_spam' => 'Les admins sont autorisés', # NEW
	'sw_admin_no_spam' => 'Pas de spam', # NEW
	'sw_ban_ip_titre' => 'Bannir l\'IP en même temps', # NEW
	'sw_config_exceptions' => 'Vous pouvez activer des exceptions pour des utilisateurs privilégiés ici. Ils pourront quand même publier avec des mots bannis.', # NEW
	'sw_config_exceptions_titre' => 'Gestion des exceptions', # NEW
	'sw_config_generale' => 'Configuration actuelle du filtrage :', # NEW
	'sw_config_generale_titre' => 'Configuration générale du filtrage du spam', # NEW
	'sw_config_warning' => 'Vous pouvez choisir le texte du MP envoyé si vous activez cette option (maxi 255 caractères).', # NEW
	'sw_config_warning_titre' => 'Configuration des avertissements par message privé', # NEW
	'sw_disable_sw_titre' => '<strong>Active le filtrage</strong><br />Si vous devez vous passer du filtrage,<br />cliquez sur Non', # NEW
	'sw_modo_can_spam' => 'Les modérateurs sont autorisés', # NEW
	'sw_nb_spam_ban_titre' => 'Nombre de spams avant banissement', # NEW
	'sw_pm_spam_warning_message' => 'Ceci est un avertissement. Vous avez essayé de poster un message analysé comme du spam sur ce site web. Merci d\'éviter de recommencer.', # NEW
	'sw_pm_spam_warning_titre' => 'Attention.', # NEW
	'sw_send_pm_warning' => '<strong>Envoie un MP à l\'utilisateur</strong><br />lorsqu\'il poste un message avec un mot interdit', # NEW
	'sw_spam_forum_titre' => 'Gestion des messages de spam', # NEW
	'sw_spam_titre' => 'Filtrage du spam', # NEW
	'sw_spam_words_action' => 'A partir de cette page, vous pouvez ajouter, éditer et supprimer des mots associés à du spam. Le caractère (*) est accepté dans le mot. Par exemple : {{*tes*}} capturera {détestable}, {{tes*}} capturera {tester}, {{*tes}} capturera {portes}.', # NEW
	'sw_spam_words_mass_add' => 'Copier-coller ou saisir vos mots dans cette zone. Séparer chaque mot par une virgule, deux points ou un retour à la ligne.', # NEW
	'sw_spam_words_titre' => 'Filtrage de mots', # NEW
	'sw_spam_words_url_add' => 'Saisir l\'URL d\'un fichier contenant une liste de mots formatée comme ci-dessus. Exemple : http://spipbb.free.fr/IMG/csv/spamwordlist.csv .', # NEW
	'sw_warning_from_admin' => 'Choisir l\'admin auteur du message envoyé', # NEW
	'sw_warning_pm_message' => 'Texte du message privé', # NEW
	'sw_warning_pm_titre' => 'Sujet du message privé', # NEW
	'sw_word' => 'Mot', # NEW

	// T
	'title_ferme' => 'Fermer le forum/article', # NEW
	'title_libere' => 'Réouvrir le forum/article', # NEW
	'title_libere_maintenance' => 'Libérer le verrou de Maintenance', # NEW
	'title_maintenance' => 'Fermer le forum/article pour Maintenance', # NEW
	'title_sujet_ferme' => 'Fermer ce sujet', # NEW
	'title_sujet_libere' => 'Réouvrir ce sujet', # NEW
	'titre_spipbb' => 'SpipBB', # NEW
	'total_membres' => 'Nous avons un total de ', # NEW
	'total_messages_membres' => 'Nos membres ont posté un total de ', # NEW
	'tous' => 'Tous', # NEW
	'tous_forums' => 'Tous les forums', # NEW
	'trier' => 'Trier', # NEW
	'trouver_messages_auteur_dpt' => 'Trouver tous les messages de :', # NEW

	// V
	'visible_annuaire_forum' => 'Apparaitre dans la liste des Inscrits', # NEW
	'visites' => 'Vu', # NEW
	'voir' => 'VOIR', # NEW
	'votre_bio' => 'Courte biographie en quelques mots.', # NEW
	'votre_email' => 'Votre adresse email', # NEW
	'votre_nouveau_passe' => 'Nouveau mot de passe', # NEW
	'votre_signature' => 'Votre signature', # NEW
	'votre_site' => 'Le nom de votre site', # NEW
	'votre_url_avatar' => 'URL de votre Avatar (http://...)', # NEW
	'votre_url_site' => 'L\'adresse (URL) de votre site' # NEW
);

?>
