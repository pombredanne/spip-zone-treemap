<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'action_ajouter' => 'Cr&eacute;er une actualit&eacute;',
	'action_modifier' => 'Modifier',
	'action_supprimer' => 'Supprimer',
	'action_voir' => 'Voir',
	
	// C
	'actuss' => 'Actualit&eacute;s',

	// D
	'description_actus' => 'Saisissez votre actualit&eacute;', //  ?????????

	// E
	'explication_descriptif' => "
		Descriptif de l'actualit&eacute; : Apparait dans les listes d'actualit&eacute;s
	",
	'explication_chapo' => "
			Chapeau de l'actualit&eacute; : Apparait, mis en avant, dans la fiche de l'actualit&eacute; 
	",
	'explication_texte' => "
			Texte de l'actualit&eacute; : contenu textuel de l'actualit&eacute;
	",
	// G
	'gestion_objets'=>"Gestion des objets suppl&eacute;mentaires",
	
	// I
	'info_gauche_numero_actu' => 'Actualit&eacute; num&eacute;ro :',
	'info_objets_configuration' => 'Saisissez le nom du nouvel objet a g&eacute;rer :', //icone_creer_sous_rubrique
	'icone_creer_objet' => 'Cr&eacute;er un nouveau',
	'item_nouveau'=>'Nouvel objet',
	'info_modifier_objet'=>'Saisissez votre  @objet@',

	// L
	'label_objet'=>"Choisissez le nom de l'objet a gérer",
	'label_actions' => 'Actions',
	'label_id' => 'Id',
	'label_infos' => 'Informations',
	'label_titre' => 'Titre',
	'label_chapo' => 'Chapeau',
	'label_descriptif' => 'Descriptif',
	'label_texte' => 'Texte',
	'liste_des_actus' => 'Liste des actualit&eacute;s',
	'logo'=>"Logo",

	// N
	'nom_objet_supplementaire'=>"nom_objet_supplementaire",

	// O
	'objet' => 'Objet',
	
	// T
	'texte_nouvelle_actu' => 'Pas de titre',
	'titre_liste'=>'Liste des objets associ&eacute;s : ',
	'titre_actu_refusee'=>'Actualit&eacute;s refus&eacute;e',
	'titre_logo'=>'Logo',
  //texte_objets_configuration
  //titre_page_objet_edit
  'texte_nouvel_objet'=>'Remplissez les différentes zones de saisie puis validez'

	
);

?>
