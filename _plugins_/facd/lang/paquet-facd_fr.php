<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/facd/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'facd_description' => 'Gestion de file d\'attente pour la conversion de documents.',
	'facd_slogan' => 'Gestion de file d\'attente pour la conversion de documents'
);

?>
