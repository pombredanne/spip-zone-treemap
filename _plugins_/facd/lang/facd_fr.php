<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/facd/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_relancer_tout' => 'Reconvertir tous les documents',
	'bouton_relancer_tout_format' => 'Reconvertir tous les documents au format @format@',
	'bouton_relancer_tout_format_message' => 'Êtes vous sûr de vouloir relancer l\'encodage de tous les fichiers au format @format@.',
	'bouton_relancer_tout_message' => 'Êtes vous sûr de vouloir relancer l\'encodage de tous les fichiers',

	// E
	'erreur_document_plus_disponible' => 'Ce document n\'est plus disponible sur le site',
	'explication_file_attente' => 'Cette page liste les documents présents dans la file d\'attente. Ceux-ci seront convertis automatiquement par CRON.',

	// I
	'info_document_conversion' => 'Ce document est en cours de conversion',
	'info_document_conversion_erreur' => 'La conversion de ce document a échoué',
	'info_document_dans_file_attente' => 'En attente de conversion',
	'info_relancer_erreurs' => 'Relancer toutes les conversions en erreur',
	'info_statut_conversion_en_cours' => 'En cours',
	'info_statut_conversion_erreur' => 'En erreur',
	'info_statut_conversion_non' => 'En attente',
	'info_statut_conversion_oui' => 'Converti',
	'info_tous_docs_facd' => 'Documents de la file d\'attente',
	'info_voir_log_erreur' => 'Voir le log d\'erreur',

	// L
	'label_relancer_conversion' => 'Relancer la conversion',
	'lien_convertir_document' => 'Convertir ce document',
	'lien_recharger' => 'Recharger',
	'liste_attente_aucun' => 'Aucun document dans la file d\'attente.',
	'liste_attente_tous' => 'Documents présents dans la file d\'attente',
	'liste_convert_aucun' => 'Aucun document converti',
	'liste_convert_tous' => 'Documents convertis',

	// T
	'thead_date' => 'Date',
	'thead_duree' => 'Durée',
	'thead_duree_conversion' => 'Durée de conversion',
	'thead_extension' => 'Format',
	'thead_fonction' => 'Fonction utilisée',
	'thead_id' => 'ID',
	'thead_id_auteur' => 'Auteur',
	'thead_id_document' => 'Original',
	'thead_nombre' => 'Nombre',
	'thead_statut' => 'Statut',
	'titre_log_conversion' => 'Contenu du log de conversion #@id@',
	'titre_page_file' => 'File d\'attente du module de conversion de medias',
	'titre_page_file_convertis_jour' => 'Conversions par date',
	'titre_page_file_menu' => 'File d\'attente de conversion',

	// V
	'version_encodee_de' => 'Ce document est une conversion du document @id_orig@'
);

?>
