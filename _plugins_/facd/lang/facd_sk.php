<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/facd?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_relancer_tout' => 'Prekonvertovať všetky dokumenty',
	'bouton_relancer_tout_format' => 'Prekonvertovať všetky dokumenty vo formáte @format@',
	'bouton_relancer_tout_format_message' => 'Určite chcete znova spustiť zakódovanie všetkých súborov vo formáte @format@?',
	'bouton_relancer_tout_message' => 'Určite chcete znova spustiť zakódovanie všetkých súborov',

	// E
	'erreur_document_plus_disponible' => 'Tento dokument už viac na stránke nie je dostupný',
	'explication_file_attente' => 'Na tejto stránke je zoznam dokumentov, ktoré sa nachádzajú v rade. CRON ich automaticky skonvertuje.',

	// I
	'info_document_conversion' => 'Tento dokument sa konvertuje',
	'info_document_conversion_erreur' => 'Konverzia tohto dokumentu sa nepodarila',
	'info_document_dans_file_attente' => 'Čaká na konverziu',
	'info_relancer_erreurs' => 'Znova spustiť všetky nevydarené konverzie',
	'info_statut_conversion_en_cours' => 'Spracúva sa',
	'info_statut_conversion_erreur' => 'Chybný',
	'info_statut_conversion_non' => 'Čaká',
	'info_statut_conversion_oui' => 'Konvertovaný',
	'info_tous_docs_facd' => 'Dokumenty v rade',
	'info_voir_log_erreur' => 'Zobraziť protokol s chybami',

	// L
	'label_relancer_conversion' => 'Spustiť konverziu',
	'lien_convertir_document' => 'Konvertovať tento dokument',
	'lien_recharger' => 'Obnoviť',
	'liste_attente_aucun' => 'Žiaden dokument v rade.',
	'liste_attente_tous' => 'Dokumenty v rade',
	'liste_convert_aucun' => 'Žiaden konvertovaný dokument',
	'liste_convert_tous' => 'Skonvertované dokumenty',

	// T
	'thead_date' => 'Dátum',
	'thead_duree' => 'Trvanie',
	'thead_duree_conversion' => 'Trvanie konverzie',
	'thead_extension' => 'Formát',
	'thead_fonction' => 'Použitá funkcia',
	'thead_id' => 'P. č.',
	'thead_id_auteur' => 'Autor',
	'thead_id_document' => 'Originál',
	'thead_nombre' => 'Počet',
	'thead_statut' => 'Stav',
	'titre_log_conversion' => 'Obsah protokolu konverzie #@id@',
	'titre_page_file' => 'Rad modulu na konverziu médií',
	'titre_page_file_convertis_jour' => 'Konverzie podľa dátumu',
	'titre_page_file_menu' => 'Rad na konverziu',

	// V
	'version_encodee_de' => 'Tento súbor je konverziou dokumentu @id_orig@'
);

?>
