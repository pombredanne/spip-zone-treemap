<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/openinviter?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'champ_email_label' => 'Vaša e-mailová adresa @– poskytovateľ@',
	'champ_password_explication' => 'Nebudeme si ho pamätať.',
	'champ_password_label' => 'Heslo',

	// E
	'erreur_authentification' => 'Nepodarilo sa overiť vašu totožnosť. Skontrolujte adresu a heslo a skúste to znova.',
	'erreur_generale' => 'Obnoveniu vašich kontaktov bráni vnútorná chyba.'
);

?>
