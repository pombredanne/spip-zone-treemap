<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/openinviter?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'champ_email_label' => 'Your email @fournisseur@',
	'champ_password_explication' => 'It won\'t be stored.',
	'champ_password_label' => 'Password',

	// E
	'erreur_authentification' => 'Authentication failed. Check the address and the password and try again.',
	'erreur_generale' => 'An internal error prevents the recovery of your contacts.'
);

?>
