<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/openinviter/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'champ_email_label' => 'Votre adresse email @fournisseur@',
	'champ_password_explication' => 'Il ne sera pas gardé en mémoire.',
	'champ_password_label' => 'Mot de passe',

	// E
	'erreur_authentification' => 'L\'authentification a échoué. Vérifiez l\'adresse et le mot de passe et réessayez.',
	'erreur_generale' => 'Une erreur interne empêche la récupération de vos contacts.'
);

?>
