<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_droits_auteur' => 'Aggiungimi i diritti di accesso in questa zona',
	'aucune_zone' => 'Aucune zone', # NEW
	'auteurs' => 'autori',

	// B
	'bouton_configurer_acces' => 'Configuration des acc&#232;s .htaccess', # NEW
	'bouton_creer_la_zone' => 'Crea la nuova zona',

	// C
	'colonne_id' => 'Num',
	'confirmer_ajouter_auteurs' => 'Êtes vous sûr de vouloir ajouter cet auteur à la zone ?', # NEW
	'confirmer_retirer_auteur_zone' => 'Êtes vous sûr de vouloir retirer cet auteur de la zone ?', # NEW
	'confirmer_retirer_auteurs' => 'Êtes vous sûr de vouloir retirer tous les auteurs de cette zone ?', # NEW
	'confirmer_retirer_rubrique_zone' => 'Êtes vous sûr de vouloir retirer cette rubrique de cette zone ?', # NEW
	'confirmer_supprimer_zone' => 'Êtes vous sûr de vouloir supprimer cette zone ?', # NEW
	'creer_zone' => 'Crea una nuova zona',

	// D
	'descriptif' => 'Descrizione',

	// E
	'explication_creer_htaccess' => 'Cette option interdit la lecture des documents joints si le texte auquel ils se rattachent n\'est pas publi&eacute', # NEW

	// I
	'icone_menu_config' => 'Accesso limitato',
	'icone_supprimer_zone' => 'Elimina questa zona',
	'info_1_zone' => '1 zone', # NEW
	'info_acces_restreint' => 'L\'accès à cette page est restreint. Identifiez vous pour y accéder', # NEW
	'info_ajouter_auteur' => 'Ajouter cet auteur', # NEW
	'info_ajouter_auteurs' => 'Ajouter tous les auteurs', # NEW
	'info_ajouter_zones' => 'Aggiungi tutte le zone',
	'info_aucun_acces' => 'Aucun accès autorisé', # NEW
	'info_aucun_auteur' => 'Aucun auteur dans la zone', # NEW
	'info_aucune_zone' => 'Aucune zone', # NEW
	'info_auteurs_lies_zone' => 'Gli autori che possono accedere a questa zona',
	'info_lien_action_proteger' => 'Prot&eacute;ger l\'acc&egrave;s &agrave; cette rubrique.', # NEW
	'info_nb_zones' => '@nb@ zones', # NEW
	'info_page' => 'Questa pagina permette di gestire le zone di accesso limitato del proprio sito ',
	'info_retirer_auteurs' => 'Retirer tous les auteurs', # NEW
	'info_retirer_zone' => 'Togli la zona',
	'info_retirer_zones' => 'Togli tutte le zone',
	'info_rubrique_dans_zone' => 'Cette rubrique fait partie de la zone :', # NEW
	'info_rubrique_dans_zones' => 'Cette rubrique fait partie des zones :', # NEW

	// L
	'label_creer_htaccess' => 'Acc&egrave;s aux document joints par leur URL', # NEW
	'label_creer_htaccess_non' => 'autoriser la lecture', # NEW
	'label_creer_htaccess_oui' => 'interdire la lecture', # NEW

	// M
	'modifier_zone' => 'Modifier la zone', # NEW

	// P
	'page_zones_acces' => 'Accesso limitato',
	'par_titre' => 'Par titre', # NEW
	'privee' => 'Privata',
	'publique' => 'Pubblica',

	// R
	'rubriques' => 'rubriche',
	'rubriques_zones_acces' => 'Rubriche della zona',

	// S
	'selectionner_une_zone' => 'Seleziona una zona',

	// T
	'texte_ajouter_zone' => 'Ajouter une zone', # NEW
	'texte_creer_associer_zone' => 'Créer et associer une zone', # NEW
	'titre' => 'Titolo',
	'titre_ajouter_zone' => 'Unisci la zona',
	'titre_boite_protegee_non' => 'Acc&egrave;s non prot&eacute;g&eacute;', # NEW
	'titre_boite_protegee_oui' => 'Acc&egrave;s prot&eacute;g&eacute;', # NEW
	'titre_cadre_modifier_zone' => 'Modifier une zone', # NEW
	'titre_page_config' => 'Configuration des accès', # NEW
	'titre_table' => 'Tutte le zone di accesso',
	'titre_zone_acces' => 'Zone d\'accès restreint', # NEW
	'titre_zones_acces' => 'Zone ad accesso limitato',
	'toutes' => 'Toutes', # NEW

	// V
	'voir_toutes' => 'Vedi tutte le zone',

	// Z
	'zone_numero' => 'ZONA NUMERO:',
	'zone_restreinte_espace_prive' => 'Limitare l\'accesso a questa zona nell\'area riservata',
	'zone_restreinte_publique' => 'Limitare l\'accesso a questa zona nell\'area pubblica'
);

?>
