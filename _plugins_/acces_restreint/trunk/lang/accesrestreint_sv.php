<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_droits_auteur' => 'M\'ajouter les droits d\'accès à cette zone', # NEW
	'aucune_zone' => 'Aucune zone', # NEW
	'auteurs' => 'auteurs', # NEW

	// B
	'bouton_configurer_acces' => 'Configuration des acc&#232;s .htaccess', # NEW
	'bouton_creer_la_zone' => 'Créer la nouvelle zone', # NEW

	// C
	'colonne_id' => 'Num', # NEW
	'confirmer_ajouter_auteurs' => 'Êtes vous sûr de vouloir ajouter cet auteur à la zone ?', # NEW
	'confirmer_retirer_auteur_zone' => 'Êtes vous sûr de vouloir retirer cet auteur de la zone ?', # NEW
	'confirmer_retirer_auteurs' => 'Êtes vous sûr de vouloir retirer tous les auteurs de cette zone ?', # NEW
	'confirmer_retirer_rubrique_zone' => 'Êtes vous sûr de vouloir retirer cette rubrique de cette zone ?', # NEW
	'confirmer_supprimer_zone' => 'Êtes vous sûr de vouloir supprimer cette zone ?', # NEW
	'creer_zone' => 'Créer une nouvelle zone', # NEW

	// D
	'descriptif' => 'Descriptif', # NEW

	// E
	'explication_creer_htaccess' => 'Cette option interdit la lecture des documents joints si le texte auquel ils se rattachent n\'est pas publi&eacute', # NEW

	// I
	'icone_menu_config' => 'Accès Restreint', # NEW
	'icone_supprimer_zone' => 'Supprimer cette zone', # NEW
	'info_1_zone' => '1 zone', # NEW
	'info_acces_restreint' => 'L\'accès à cette page est restreint. Identifiez vous pour y accéder', # NEW
	'info_ajouter_auteur' => 'Ajouter cet auteur', # NEW
	'info_ajouter_auteurs' => 'Ajouter tous les auteurs', # NEW
	'info_ajouter_zones' => 'Ajouter toutes les zones', # NEW
	'info_aucun_acces' => 'Aucun accès autorisé', # NEW
	'info_aucun_auteur' => 'Aucun auteur dans la zone', # NEW
	'info_aucune_zone' => 'Aucune zone', # NEW
	'info_auteurs_lies_zone' => 'Les auteurs ayant accès à cette zone', # NEW
	'info_lien_action_proteger' => 'Prot&eacute;ger l\'acc&egrave;s &agrave; cette rubrique.', # NEW
	'info_nb_zones' => '@nb@ zones', # NEW
	'info_page' => 'Cette page vous permet de gérer les zones d\'accès restreint de votre site', # NEW
	'info_retirer_auteurs' => 'Retirer tous les auteurs', # NEW
	'info_retirer_zone' => 'Enlever de la zone', # NEW
	'info_retirer_zones' => 'Enlever de toutes les zones', # NEW
	'info_rubrique_dans_zone' => 'Cette rubrique fait partie de la zone :', # NEW
	'info_rubrique_dans_zones' => 'Cette rubrique fait partie des zones :', # NEW

	// L
	'label_creer_htaccess' => 'Acc&egrave;s aux document joints par leur URL', # NEW
	'label_creer_htaccess_non' => 'autoriser la lecture', # NEW
	'label_creer_htaccess_oui' => 'interdire la lecture', # NEW

	// M
	'modifier_zone' => 'Modifier la zone', # NEW

	// P
	'page_zones_acces' => 'Accès Restreint', # NEW
	'par_titre' => 'Par titre', # NEW
	'privee' => 'Privat',
	'publique' => 'Publik',

	// R
	'rubriques' => 'rubriques', # NEW
	'rubriques_zones_acces' => 'Rubriques de la zone', # NEW

	// S
	'selectionner_une_zone' => 'Sélectionner une zone', # NEW

	// T
	'texte_ajouter_zone' => 'Ajouter une zone', # NEW
	'texte_creer_associer_zone' => 'Créer et associer une zone', # NEW
	'titre' => 'Titre', # NEW
	'titre_ajouter_zone' => 'Rejoindre la zone', # NEW
	'titre_boite_protegee_non' => 'Acc&egrave;s non prot&eacute;g&eacute;', # NEW
	'titre_boite_protegee_oui' => 'Acc&egrave;s prot&eacute;g&eacute;', # NEW
	'titre_cadre_modifier_zone' => 'Modifier une zone', # NEW
	'titre_page_config' => 'Configuration des accès', # NEW
	'titre_table' => 'Toutes les zones d\'accès', # NEW
	'titre_zone_acces' => 'Zone d\'accès restreint', # NEW
	'titre_zones_acces' => 'Zones d\'accès restreint', # NEW
	'toutes' => 'Toutes', # NEW

	// V
	'voir_toutes' => 'Voir toutes les zones', # NEW

	// Z
	'zone_numero' => 'ZONE NUMÉRO :', # NEW
	'zone_restreinte_espace_prive' => 'Restreindre l\'accès à cette zone dans l\'espace privé', # NEW
	'zone_restreinte_publique' => 'Restreindre l\'accès à cette zone dans la partie publique' # NEW
);

?>
