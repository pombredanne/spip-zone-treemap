<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/accesrestreint?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_droits_auteur' => 'Afegir-me els drets d\'accés a aquesta zona',
	'aucune_zone' => 'Cap zona',
	'auteurs' => 'autors',

	// B
	'bouton_configurer_acces' => 'Configuration des accès .htaccess', # NEW
	'bouton_creer_la_zone' => 'Crear la nova zona',

	// C
	'colonne_id' => 'Núm',
	'confirmer_ajouter_auteurs' => 'Esteu segurs de voler afegir aquest autor a la zona?',
	'confirmer_retirer_auteur_zone' => 'Estàs segur que vols retirar aquest autor de la zona?',
	'confirmer_retirer_auteurs' => 'Estàs segur que vols retirar tots els autors d\'aquesta zona?',
	'confirmer_retirer_rubrique_zone' => 'Êtes vous sûr de vouloir retirer cette rubrique de cette zone ?', # NEW
	'confirmer_supprimer_zone' => 'Estàs segur que vols suprimir aquesta zona?',
	'creer_zone' => 'Crear una nova zona',

	// D
	'descriptif' => 'Descripció',

	// E
	'explication_creer_htaccess' => 'Cette option interdit la lecture des documents joints si le texte auquel ils se rattachent n\'est pas publi&eacute', # NEW

	// I
	'icone_menu_config' => 'Accés Restringit ',
	'icone_supprimer_zone' => 'Suprimir aquesta zona',
	'info_1_zone' => '1 zone', # NEW
	'info_acces_restreint' => 'L\'accés a aquesta pàgina està restringit. Us heu d\'identificar per accedir-hi',
	'info_ajouter_auteur' => 'Afegir aquest autor',
	'info_ajouter_auteurs' => 'Afegir tots els autors',
	'info_ajouter_zones' => 'Afegir totes les zones',
	'info_aucun_acces' => 'Cap accés autoritzat',
	'info_aucun_auteur' => 'Cap autor a la zona',
	'info_aucune_zone' => 'Cap zona',
	'info_auteurs_lies_zone' => 'Els autors que tenen accés a aquesta zona',
	'info_lien_action_proteger' => 'Protéger l\'accès à cette rubrique.', # NEW
	'info_nb_zones' => '@nb@ zones', # NEW
	'info_page' => 'Aquesta pàgina permet gestionar les zones d\'accés restringit del vostre lloc',
	'info_retirer_auteurs' => 'Retirar tots els autors',
	'info_retirer_zone' => 'Treure de la zona',
	'info_retirer_zones' => 'Treure de totes les zones',
	'info_rubrique_dans_zone' => 'Cette rubrique fait partie de la zone :', # NEW
	'info_rubrique_dans_zones' => 'Cette rubrique fait partie des zones :', # NEW

	// L
	'label_creer_htaccess' => 'Accès aux document joints par leur URL', # NEW
	'label_creer_htaccess_non' => 'autoriser la lecture', # NEW
	'label_creer_htaccess_oui' => 'interdire la lecture', # NEW

	// M
	'modifier_zone' => 'Modificar la zona',

	// P
	'page_zones_acces' => 'Accés Restringit',
	'par_titre' => 'Per títol',
	'privee' => 'Privat',
	'publique' => 'Públic',

	// R
	'rubriques' => 'seccions',
	'rubriques_zones_acces' => 'Seccions de la zona',

	// S
	'selectionner_une_zone' => 'Seleccionar una zona',

	// T
	'texte_ajouter_zone' => 'Ajouter une zone', # NEW
	'texte_creer_associer_zone' => 'Créer et associer une zone', # NEW
	'titre' => 'Títol',
	'titre_ajouter_zone' => 'Tornar a la zona',
	'titre_boite_protegee_non' => 'Accès non protégé', # NEW
	'titre_boite_protegee_oui' => 'Accès protégé', # NEW
	'titre_cadre_modifier_zone' => 'Modificar una zona',
	'titre_page_config' => 'Configuració de l\'accés',
	'titre_table' => 'Totes les zones d\'accés',
	'titre_zone_acces' => 'Zone d\'accès restreint', # NEW
	'titre_zones_acces' => 'Zones d\'accés restringit',
	'toutes' => 'Totes',

	// V
	'voir_toutes' => 'Veure totes les zones',

	// Z
	'zone_numero' => 'ZONA NÚMERO :',
	'zone_restreinte_espace_prive' => 'Restringir l\'accés a aquesta zona a l\'espai privat',
	'zone_restreinte_publique' => 'Restringir l\'accés a aquesta zona a la part pública'
);

?>
