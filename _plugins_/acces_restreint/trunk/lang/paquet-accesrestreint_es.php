<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-accesrestreint
// Langue: es
// Date: 18-10-2011 17:20:21
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
	'accesrestreint_description' => '_ Cada zona contiene secciones.
_ Los autores podrán ser asociados a ciertas zonas restringidas para tener derechos de acceso a ellas.
_ Todos los bucles nativos de SPIP quedan modificados para filtrar los resultados en función de los derechos del visitante.',
	'accesrestreint_nom' => 'Acceso restringido',
	'accesrestreint_slogan' => 'Gestión de zonas de acceso restringido',
);
?>