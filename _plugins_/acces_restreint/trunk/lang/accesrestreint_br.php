<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/accesrestreint?lang_cible=br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_droits_auteur' => 'Ouzhpennañ gwirioù moned din-me evit an takad-mañ',
	'aucune_zone' => 'Aucune zone', # NEW
	'auteurs' => 'skridaozerien',

	// B
	'bouton_configurer_acces' => 'Configuration des accès .htaccess', # NEW
	'bouton_creer_la_zone' => 'Krouiñ an takad nevez',

	// C
	'colonne_id' => 'Niv.',
	'confirmer_ajouter_auteurs' => 'Êtes vous sûr de vouloir ajouter cet auteur à la zone ?', # NEW
	'confirmer_retirer_auteur_zone' => 'Ha sur oc\'h e fell deoc\'h lemel kuit an aozer-mañ eus an takad ?',
	'confirmer_retirer_auteurs' => 'Ha sur oc\'h e fell deoc\'h lemel kuit an holl aozerien eus an takad ?',
	'confirmer_retirer_rubrique_zone' => 'Êtes vous sûr de vouloir retirer cette rubrique de cette zone ?', # NEW
	'confirmer_supprimer_zone' => 'Ha sur oc\'h e fell deoc\'h diverkañ an takad ?',
	'creer_zone' => 'Krouiñ un takad nevez',

	// D
	'descriptif' => 'Deskrivadur',

	// E
	'explication_creer_htaccess' => 'Cette option interdit la lecture des documents joints si le texte auquel ils se rattachent n\'est pas publi&eacute', # NEW

	// I
	'icone_menu_config' => 'Moned bevennet',
	'icone_supprimer_zone' => 'Diverkañ an takad-mañ',
	'info_1_zone' => '1 zone', # NEW
	'info_acces_restreint' => 'L\'accès à cette page est restreint. Identifiez vous pour y accéder', # NEW
	'info_ajouter_auteur' => 'Ouzhpennañ an aozer-mañ',
	'info_ajouter_auteurs' => 'Ouzhpennañ an holl aozerien',
	'info_ajouter_zones' => 'Ouzhpennañ an holl dakadoù',
	'info_aucun_acces' => 'Moned ebet aotreet',
	'info_aucun_auteur' => 'Aozer ebet en takad',
	'info_aucune_zone' => 'Takad ebet',
	'info_auteurs_lies_zone' => 'Ar skridaozerien ganto ur moned war-zu an takad-mañ',
	'info_lien_action_proteger' => 'Protéger l\'accès à cette rubrique.', # NEW
	'info_nb_zones' => '@nb@ zones', # NEW
	'info_page' => 'Gant ar bajenn-mañ e c\'hellit merañ takadoù eus ho lec\'hienn bevennet ar moned outo.',
	'info_retirer_auteurs' => 'Lemel kuit an holl aozerien',
	'info_retirer_zone' => 'Lemel kuit eus an takad',
	'info_retirer_zones' => 'Lemel kuit eus an holl dakadoù',
	'info_rubrique_dans_zone' => 'Cette rubrique fait partie de la zone :', # NEW
	'info_rubrique_dans_zones' => 'Cette rubrique fait partie des zones :', # NEW

	// L
	'label_creer_htaccess' => 'Accès aux document joints par leur URL', # NEW
	'label_creer_htaccess_non' => 'autoriser la lecture', # NEW
	'label_creer_htaccess_oui' => 'interdire la lecture', # NEW

	// M
	'modifier_zone' => 'Kemmañ an takad',

	// P
	'page_zones_acces' => 'Moned bevennet',
	'par_titre' => 'Dre ditloù',
	'privee' => 'Prevez',
	'publique' => 'Foran',

	// R
	'rubriques' => 'rubrikennoù',
	'rubriques_zones_acces' => 'Rubrikennoù an takad',

	// S
	'selectionner_une_zone' => 'Diuzañ un takad',

	// T
	'texte_ajouter_zone' => 'Ajouter une zone', # NEW
	'texte_creer_associer_zone' => 'Créer et associer une zone', # NEW
	'titre' => 'Titl',
	'titre_ajouter_zone' => 'Tizhout an takad',
	'titre_boite_protegee_non' => 'Accès non protégé', # NEW
	'titre_boite_protegee_oui' => 'Accès protégé', # NEW
	'titre_cadre_modifier_zone' => 'Kemmañ un takad',
	'titre_page_config' => 'Configuration des accès', # NEW
	'titre_table' => 'An holl dakadoù moned',
	'titre_zone_acces' => 'Zone d\'accès restreint', # NEW
	'titre_zones_acces' => 'Takad bevennet ar moned outañ',
	'toutes' => 'An holl',

	// V
	'voir_toutes' => 'Gwelet an holl dakadoù',

	// Z
	'zone_numero' => 'TAKAD NIVERENN :',
	'zone_restreinte_espace_prive' => 'Bevenniñ ar moned ouzh an takad-mañ el lodenn brevez',
	'zone_restreinte_publique' => 'Bevenniñ ar moned ouzh an takad-mañ el lodenn foran'
);

?>
