<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/accesrestreint?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_droits_auteur' => 'Añadirme derechos de acceso para esta zona',
	'aucune_zone' => 'Aucune zone', # NEW
	'auteurs' => 'autores',

	// B
	'bouton_configurer_acces' => 'Configuration des accès .htaccess', # NEW
	'bouton_creer_la_zone' => 'Crear la nueva zona',

	// C
	'colonne_id' => 'Nº',
	'confirmer_ajouter_auteurs' => 'Êtes vous sûr de vouloir ajouter cet auteur à la zone ?', # NEW
	'confirmer_retirer_auteur_zone' => '¿Estás seguro de que quieres retirar este autor de la zona?',
	'confirmer_retirer_auteurs' => '¿Estás seguro de que quieres retirar todos los autores de esta zona?',
	'confirmer_retirer_rubrique_zone' => 'Êtes vous sûr de vouloir retirer cette rubrique de cette zone ?', # NEW
	'confirmer_supprimer_zone' => '¿Quiere realmente suprimir esta zona?',
	'creer_zone' => 'Crear una zona nueva',

	// D
	'descriptif' => 'Descripción',

	// E
	'explication_creer_htaccess' => 'Cette option interdit la lecture des documents joints si le texte auquel ils se rattachent n\'est pas publi&eacute', # NEW

	// I
	'icone_menu_config' => 'Acceso restringido',
	'icone_supprimer_zone' => 'Suprimir esta zona',
	'info_1_zone' => '1 zone', # NEW
	'info_acces_restreint' => 'L\'accès à cette page est restreint. Identifiez vous pour y accéder', # NEW
	'info_ajouter_auteur' => 'Añadir este autor',
	'info_ajouter_auteurs' => 'Añadir todos los autores',
	'info_ajouter_zones' => 'Añadir todas las zonas',
	'info_aucun_acces' => 'Ningún acceso autorizado',
	'info_aucun_auteur' => 'Ningún autor en la zona',
	'info_aucune_zone' => 'Ninguna zona',
	'info_auteurs_lies_zone' => 'Los autores con acceso a esta zona',
	'info_lien_action_proteger' => 'Protéger l\'accès à cette rubrique.', # NEW
	'info_nb_zones' => '@nb@ zones', # NEW
	'info_page' => 'Esta página permite administrar las zonas de acceso restringido en el sitio',
	'info_retirer_auteurs' => 'Retirar todos los autores',
	'info_retirer_zone' => 'Eliminar de la zona',
	'info_retirer_zones' => 'Eliminar de todas las zonas',
	'info_rubrique_dans_zone' => 'Cette rubrique fait partie de la zone :', # NEW
	'info_rubrique_dans_zones' => 'Cette rubrique fait partie des zones :', # NEW

	// L
	'label_creer_htaccess' => 'Accès aux document joints par leur URL', # NEW
	'label_creer_htaccess_non' => 'autoriser la lecture', # NEW
	'label_creer_htaccess_oui' => 'interdire la lecture', # NEW

	// M
	'modifier_zone' => 'Modificar la zona',

	// P
	'page_zones_acces' => 'Acceso Restringido',
	'par_titre' => 'Por título',
	'privee' => 'Privado',
	'publique' => 'Público',

	// R
	'rubriques' => 'secciones',
	'rubriques_zones_acces' => 'Secciones de la zona',

	// S
	'selectionner_une_zone' => 'Seleccionar una zona',

	// T
	'texte_ajouter_zone' => 'Ajouter une zone', # NEW
	'texte_creer_associer_zone' => 'Créer et associer une zone', # NEW
	'titre' => 'Título',
	'titre_ajouter_zone' => 'Añadir la zona',
	'titre_boite_protegee_non' => 'Accès non protégé', # NEW
	'titre_boite_protegee_oui' => 'Accès protégé', # NEW
	'titre_cadre_modifier_zone' => 'Modificar una zona',
	'titre_page_config' => 'Configuration des accès', # NEW
	'titre_table' => 'Todas las zonas de acceso restringido',
	'titre_zone_acces' => 'Zone d\'accès restreint', # NEW
	'titre_zones_acces' => 'Zonas de acceso restringido',
	'toutes' => 'Todas',

	// V
	'voir_toutes' => 'Ver todas las zonas',

	// Z
	'zone_numero' => 'ZONA NÚMERO :',
	'zone_restreinte_espace_prive' => 'Restringir el acceso a esta zona en el espacio privado',
	'zone_restreinte_publique' => 'Restringir el acceso a esta zona en la parte pública'
);

?>
