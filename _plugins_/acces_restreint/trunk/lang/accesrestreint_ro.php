<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/accesrestreint?lang_cible=ro
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_droits_auteur' => 'Adaugă-mi drepturile de acces la această zonă',
	'aucune_zone' => 'Aucune zone', # NEW
	'auteurs' => 'autori',

	// B
	'bouton_configurer_acces' => 'Configuration des accès .htaccess', # NEW
	'bouton_creer_la_zone' => 'Creaţi noua zonă',

	// C
	'colonne_id' => 'Num',
	'confirmer_ajouter_auteurs' => 'Êtes vous sûr de vouloir ajouter cet auteur à la zone ?', # NEW
	'confirmer_retirer_auteur_zone' => 'Sunteţi sigur ca vreţi să scoateţi autorul din această zonă ?',
	'confirmer_retirer_auteurs' => 'Sunteţi sigur că vreţi să retrageţi toţi autorii din această zonă ?',
	'confirmer_retirer_rubrique_zone' => 'Êtes vous sûr de vouloir retirer cette rubrique de cette zone ?', # NEW
	'confirmer_supprimer_zone' => 'Sunteţi sigur că vreţi să suprimaţi această zonă ?',
	'creer_zone' => 'Creaţi o zonă nouă',

	// D
	'descriptif' => 'Descriere',

	// E
	'explication_creer_htaccess' => 'Cette option interdit la lecture des documents joints si le texte auquel ils se rattachent n\'est pas publi&eacute', # NEW

	// I
	'icone_menu_config' => 'Acces Restrâns',
	'icone_supprimer_zone' => 'Ştergeţi această zonă',
	'info_1_zone' => '1 zone', # NEW
	'info_acces_restreint' => 'L\'accès à cette page est restreint. Identifiez vous pour y accéder', # NEW
	'info_ajouter_auteur' => 'Adăugaţi acest autor',
	'info_ajouter_auteurs' => 'Adăugaţi toţi autorii',
	'info_ajouter_zones' => 'Adăugaţi toate zonele',
	'info_aucun_acces' => 'Nici un acces autorizat',
	'info_aucun_auteur' => 'Nici un autor în această zonă',
	'info_aucune_zone' => 'Nici o zonă',
	'info_auteurs_lies_zone' => 'Autorii care au acces la această zonă',
	'info_lien_action_proteger' => 'Protéger l\'accès à cette rubrique.', # NEW
	'info_nb_zones' => '@nb@ zones', # NEW
	'info_page' => 'Această pagină vă permite să gestionaţi zonele de acces restrâns din cadrul site-ului dumneavostră',
	'info_retirer_auteurs' => 'Retrageţi toţi autorii',
	'info_retirer_zone' => 'Scoateţi din zonă',
	'info_retirer_zones' => 'Scoateţi din toate zonele',
	'info_rubrique_dans_zone' => 'Cette rubrique fait partie de la zone :', # NEW
	'info_rubrique_dans_zones' => 'Cette rubrique fait partie des zones :', # NEW

	// L
	'label_creer_htaccess' => 'Accès aux document joints par leur URL', # NEW
	'label_creer_htaccess_non' => 'autoriser la lecture', # NEW
	'label_creer_htaccess_oui' => 'interdire la lecture', # NEW

	// M
	'modifier_zone' => 'Modificaţi zona',

	// P
	'page_zones_acces' => 'Acces Restrâns',
	'par_titre' => 'După titlu',
	'privee' => 'Privată',
	'publique' => 'Publică',

	// R
	'rubriques' => 'rubrici',
	'rubriques_zones_acces' => 'Rubricile zonei',

	// S
	'selectionner_une_zone' => 'Selecţionaţi o zonă',

	// T
	'texte_ajouter_zone' => 'Ajouter une zone', # NEW
	'texte_creer_associer_zone' => 'Créer et associer une zone', # NEW
	'titre' => 'Titlu',
	'titre_ajouter_zone' => 'Regăsiţi zona',
	'titre_boite_protegee_non' => 'Accès non protégé', # NEW
	'titre_boite_protegee_oui' => 'Accès protégé', # NEW
	'titre_cadre_modifier_zone' => 'Modificaţi o zonă',
	'titre_page_config' => 'Configuration des accès', # NEW
	'titre_table' => 'Toate zonele de acces',
	'titre_zone_acces' => 'Zone d\'accès restreint', # NEW
	'titre_zones_acces' => 'Zonele de acces restrâns',
	'toutes' => 'Toate',

	// V
	'voir_toutes' => 'Vedeţi toate zonele',

	// Z
	'zone_numero' => 'ZONA NUMĂRUL :',
	'zone_restreinte_espace_prive' => 'Restrângeţi accesul la această zonă în cadrul spaţiului privat',
	'zone_restreinte_publique' => 'Restrângeţi accesul la această zonă în cadrul părţii publice'
);

?>
