<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/accesrestreint?lang_cible=gl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_droits_auteur' => 'Engadir os dereitos de acceso a esta zona',
	'aucune_zone' => 'Aucune zone', # NEW
	'auteurs' => 'autores',

	// B
	'bouton_configurer_acces' => 'Configuration des accès .htaccess', # NEW
	'bouton_creer_la_zone' => 'Crear a nova zona',

	// C
	'colonne_id' => 'Núm.',
	'confirmer_ajouter_auteurs' => 'Êtes vous sûr de vouloir ajouter cet auteur à la zone ?', # NEW
	'confirmer_retirer_auteur_zone' => 'Está vostede seguro de querer retirar este autor da zona?',
	'confirmer_retirer_auteurs' => 'Está vostede seguro de querer retirar a todos os autores desta zona?',
	'confirmer_retirer_rubrique_zone' => 'Êtes vous sûr de vouloir retirer cette rubrique de cette zone ?', # NEW
	'confirmer_supprimer_zone' => 'Está seguro de querer suprimir esta zona?',
	'creer_zone' => 'Crear unha nova zona',

	// D
	'descriptif' => 'Descritivo',

	// E
	'explication_creer_htaccess' => 'Cette option interdit la lecture des documents joints si le texte auquel ils se rattachent n\'est pas publi&eacute', # NEW

	// I
	'icone_menu_config' => 'Acceso restrinxido',
	'icone_supprimer_zone' => 'Suprimir esta zona',
	'info_1_zone' => '1 zone', # NEW
	'info_acces_restreint' => 'L\'accès à cette page est restreint. Identifiez vous pour y accéder', # NEW
	'info_ajouter_auteur' => 'Engadir este autor',
	'info_ajouter_auteurs' => 'Engadir todos os autores',
	'info_ajouter_zones' => 'Engadir todas as zonas',
	'info_aucun_acces' => 'Non está autorizado ningún acceso',
	'info_aucun_auteur' => 'Non hai ningún autor na zona',
	'info_aucune_zone' => 'Ningunha zona',
	'info_auteurs_lies_zone' => 'Os autores que teñen acceso a esta zona',
	'info_lien_action_proteger' => 'Protéger l\'accès à cette rubrique.', # NEW
	'info_nb_zones' => '@nb@ zones', # NEW
	'info_page' => 'Esta páxina permite xestionar as zonas de acceso restrinxido do seu web',
	'info_retirer_auteurs' => 'Retirar a todos os autores',
	'info_retirer_zone' => 'Retirar da zona',
	'info_retirer_zones' => 'Retirar de todas as zonas',
	'info_rubrique_dans_zone' => 'Cette rubrique fait partie de la zone :', # NEW
	'info_rubrique_dans_zones' => 'Cette rubrique fait partie des zones :', # NEW

	// L
	'label_creer_htaccess' => 'Accès aux document joints par leur URL', # NEW
	'label_creer_htaccess_non' => 'autoriser la lecture', # NEW
	'label_creer_htaccess_oui' => 'interdire la lecture', # NEW

	// M
	'modifier_zone' => 'Modificar a zona',

	// P
	'page_zones_acces' => 'Acceso restrinxido',
	'par_titre' => 'Por título',
	'privee' => 'Privado',
	'publique' => 'Público',

	// R
	'rubriques' => 'seccións',
	'rubriques_zones_acces' => 'Seccións da zona',

	// S
	'selectionner_une_zone' => 'Seleccionar unha zona',

	// T
	'texte_ajouter_zone' => 'Ajouter une zone', # NEW
	'texte_creer_associer_zone' => 'Créer et associer une zone', # NEW
	'titre' => 'Título',
	'titre_ajouter_zone' => 'Reconectar a zona',
	'titre_boite_protegee_non' => 'Accès non protégé', # NEW
	'titre_boite_protegee_oui' => 'Accès protégé', # NEW
	'titre_cadre_modifier_zone' => 'Modificar unha zona',
	'titre_page_config' => 'Configuration des accès', # NEW
	'titre_table' => 'Todas as zonas de acceso',
	'titre_zone_acces' => 'Zone d\'accès restreint', # NEW
	'titre_zones_acces' => 'Zonas de acceso restrinxido',
	'toutes' => 'Todas',

	// V
	'voir_toutes' => 'Ver todas as zonas',

	// Z
	'zone_numero' => 'ZONA NÚMERO :',
	'zone_restreinte_espace_prive' => 'Restrinxir o acceso a esta zona no espazo privado',
	'zone_restreinte_publique' => 'Restrinxir o acceso a esta zona na parte pública'
);

?>
