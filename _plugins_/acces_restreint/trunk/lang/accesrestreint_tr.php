<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/accesrestreint?lang_cible=tr
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_droits_auteur' => 'Bana, bu bölgeye erişim haklarını ver',
	'aucune_zone' => 'لا توجد أية منطقة', # NEW
	'auteurs' => 'yazar',

	// B
	'bouton_configurer_acces' => 'Configuration des accès .htaccess', # NEW
	'bouton_creer_la_zone' => 'Yeni bölgeyi oluştur',

	// C
	'colonne_id' => 'Sayı',
	'confirmer_ajouter_auteurs' => 'هل انت متأكد من إضافة هذا المؤلف الى المنطقة؟', # NEW
	'confirmer_retirer_auteur_zone' => 'Bu yazarı bu bölümden (Zone) çekmek istediğinizden emin misiniz ?',
	'confirmer_retirer_auteurs' => 'Tüm yazarları bu bölümden (Zone) çekmek istediğinizden emin misiniz ?',
	'confirmer_retirer_rubrique_zone' => 'Êtes vous sûr de vouloir retirer cette rubrique de cette zone ?', # NEW
	'confirmer_supprimer_zone' => 'Bu bölgeyi silmek istediğinizden emin misiniz ?',
	'creer_zone' => 'Yeni bir bölge oluştur',

	// D
	'descriptif' => 'Tanımlayıcı',

	// E
	'explication_creer_htaccess' => 'Cette option interdit la lecture des documents joints si le texte auquel ils se rattachent n\'est pas publi&eacute', # NEW

	// I
	'icone_menu_config' => 'Kısıtlı Erişim',
	'icone_supprimer_zone' => 'Bu bölgeyi sil',
	'info_1_zone' => '1 zone', # NEW
	'info_acces_restreint' => 'الوصول الى هذه الصفحة محصور. عليك التعريف بنفسك للوصول اليها', # NEW
	'info_ajouter_auteur' => 'Bu yazarı ekle',
	'info_ajouter_auteurs' => 'Tüm yazarları ekle',
	'info_ajouter_zones' => 'Tüm bölgeleri ekle',
	'info_aucun_acces' => 'Hiçbir erişime izin yok',
	'info_aucun_auteur' => 'Bölümde (Zone) hiç yazar yok',
	'info_aucune_zone' => 'Hiç bölüm (Zone) yok',
	'info_auteurs_lies_zone' => 'Bu bölgeye erişim hakkı olan yazarlar',
	'info_lien_action_proteger' => 'Protéger l\'accès à cette rubrique.', # NEW
	'info_nb_zones' => '@nb@ zones', # NEW
	'info_page' => 'Bu sayfa, sitenizin kısıtlanmış bölgelerini yönetmenizi sağlar',
	'info_retirer_auteurs' => 'Tüm yazarları sil',
	'info_retirer_zone' => 'Bölgeden kaldır',
	'info_retirer_zones' => 'Tüm bölgelerden kaldır',
	'info_rubrique_dans_zone' => 'Cette rubrique fait partie de la zone :', # NEW
	'info_rubrique_dans_zones' => 'Cette rubrique fait partie des zones :', # NEW

	// L
	'label_creer_htaccess' => 'Accès aux document joints par leur URL', # NEW
	'label_creer_htaccess_non' => 'autoriser la lecture', # NEW
	'label_creer_htaccess_oui' => 'interdire la lecture', # NEW

	// M
	'modifier_zone' => 'Bölümü (Zone) değiştir',

	// P
	'page_zones_acces' => 'Kısıtlı Erişim',
	'par_titre' => 'Başlığa göre',
	'privee' => 'Özel',
	'publique' => 'Kamusal',

	// R
	'rubriques' => 'başlıklar',
	'rubriques_zones_acces' => 'Bölge başlıkları',

	// S
	'selectionner_une_zone' => 'Bir bölge seç',

	// T
	'texte_ajouter_zone' => 'Ajouter une zone', # NEW
	'texte_creer_associer_zone' => 'Créer et associer une zone', # NEW
	'titre' => 'Başlık',
	'titre_ajouter_zone' => 'Bölgeye gir',
	'titre_boite_protegee_non' => 'Accès non protégé', # NEW
	'titre_boite_protegee_oui' => 'Accès protégé', # NEW
	'titre_cadre_modifier_zone' => 'Bir bölümü (Zone) değiştir',
	'titre_page_config' => 'إعداد الدخول', # NEW
	'titre_table' => 'Tüm erişim bölgeleri',
	'titre_zone_acces' => 'Zone d\'accès restreint', # NEW
	'titre_zones_acces' => 'Kısıtlı erişim bölgeleri',
	'toutes' => 'Hepsi',

	// V
	'voir_toutes' => 'Tüm bölgeleri göster',

	// Z
	'zone_numero' => 'ZONE No :',
	'zone_restreinte_espace_prive' => 'Özel alanda bu bölgeye erişimi kısıtla',
	'zone_restreinte_publique' => 'Kamusal alanda bu bölgeye erişimi kısıtla'
);

?>
