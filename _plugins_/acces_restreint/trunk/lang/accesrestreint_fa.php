<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/accesrestreint?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_droits_auteur' => 'حق دسترسي به اين عرصه را به من بدهيد',
	'aucune_zone' => 'هيچ منطقه',
	'auteurs' => 'نويسندگان',

	// B
	'bouton_configurer_acces' => 'Configuration des accès .htaccess', # NEW
	'bouton_creer_la_zone' => 'ايجاد منطقه‌ي جديد',

	// C
	'colonne_id' => 'شماره',
	'confirmer_ajouter_auteurs' => 'آيا مطمئن هستيد كه مي‌‌خواهيد اين نويسنده را به اين منطقه اضافه كنيد؟‌',
	'confirmer_retirer_auteur_zone' => 'آي مطمئن هستيد كه مي‌خواهيد اين نويسنده را از اين منطقه حذف كنيد؟',
	'confirmer_retirer_auteurs' => 'آيا مطمئن هستيد كه مي‌خواهيد تمام اين نويسندگان را از اين منطقه حذف كنيد؟',
	'confirmer_retirer_rubrique_zone' => 'Êtes vous sûr de vouloir retirer cette rubrique de cette zone ?', # NEW
	'confirmer_supprimer_zone' => 'آيا مطمئن‌ هستيد كه مي‌‌خواهيد اين منطقه را حذف كنيد؟',
	'creer_zone' => 'ايجاد يك منطقه‌ي جديد',

	// D
	'descriptif' => 'توصيف ',

	// E
	'explication_creer_htaccess' => 'Cette option interdit la lecture des documents joints si le texte auquel ils se rattachent n\'est pas publi&eacute', # NEW

	// I
	'icone_menu_config' => 'دسترسي محدود',
	'icone_supprimer_zone' => 'حذف اين منطقه',
	'info_1_zone' => '1 zone', # NEW
	'info_acces_restreint' => 'دسترسي به اين صفحه محدود است. لطفاً براي دسترسي به آن خودتان را معرفي كنيد. ',
	'info_ajouter_auteur' => 'اين نويسنده را اضافه كنيد',
	'info_ajouter_auteurs' => 'تمام نويسندگان را اضافه كنيد',
	'info_ajouter_zones' => 'تمام منطقه‌ها را اضافه كنيد',
	'info_aucun_acces' => 'دسترسي مجاز نيست',
	'info_aucun_auteur' => 'نويسنده‌‌اي در منطقه نيست',
	'info_aucune_zone' => 'هيچ منطقه',
	'info_auteurs_lies_zone' => 'نويسندگان داراي دسترسي به اين منطقه',
	'info_lien_action_proteger' => 'عدم دسترسي به اين بخش. ',
	'info_nb_zones' => '@nb@ zones', # NEW
	'info_page' => 'اين صفحه به شما اجازه مي‌دهد مناطق محدود سايت خود را اداره كنيد',
	'info_retirer_auteurs' => 'حذف تمام نويسندگان',
	'info_retirer_zone' => 'حذف منطقه',
	'info_retirer_zones' => 'حذف تمام منطقه‌ها',
	'info_rubrique_dans_zone' => 'اين بخش در اين منطقه قرار دارد:',
	'info_rubrique_dans_zones' => 'اين بخش در اين مناطق قرار دارد:',

	// L
	'label_creer_htaccess' => 'Accès aux document joints par leur URL', # NEW
	'label_creer_htaccess_non' => 'autoriser la lecture', # NEW
	'label_creer_htaccess_oui' => 'interdire la lecture', # NEW

	// M
	'modifier_zone' => 'اصلاح منطقه',

	// P
	'page_zones_acces' => 'دسترسي محدود',
	'par_titre' => 'با تيتر ',
	'privee' => 'خصوصي',
	'publique' => 'همگاني',

	// R
	'rubriques' => 'بخش‌ها',
	'rubriques_zones_acces' => 'بخش‌هاي منطقه',

	// S
	'selectionner_une_zone' => 'انتخاب يك منطقه',

	// T
	'texte_ajouter_zone' => 'Ajouter une zone', # NEW
	'texte_creer_associer_zone' => 'Créer et associer une zone', # NEW
	'titre' => 'تيتر ',
	'titre_ajouter_zone' => 'افزودن به منطقه‌ي محدود',
	'titre_boite_protegee_non' => 'دسترسي بدون منع',
	'titre_boite_protegee_oui' => 'دسترسي ممنوع‌ شده',
	'titre_cadre_modifier_zone' => 'اصلاح يك منطقه',
	'titre_page_config' => 'پيكربندي دسترسي‌ها',
	'titre_table' => 'تمام منطقه‌هاي محدود',
	'titre_zone_acces' => 'Zone d\'accès restreint', # NEW
	'titre_zones_acces' => 'منطقه‌هاي محدود',
	'toutes' => 'همه',

	// V
	'voir_toutes' => 'ديدن تمام منطقه‌ها',

	// Z
	'zone_numero' => 'منطقه‌ي شماره‌ي: ',
	'zone_restreinte_espace_prive' => 'دسترسي محدود به اين منطقه در قسمت شخصي',
	'zone_restreinte_publique' => 'دسترسي محدود به اين منطقه در سايت همگاني'
);

?>
