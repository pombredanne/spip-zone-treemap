<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-accesrestreint
// Langue: en
// Date: 18-10-2011 17:20:21
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
	'accesrestreint_description' => '_ Each area contains sections.
_ Authors can be associated with these areas to gain access to them.
_ All SPIP\'s loops are loaded with the necessary tools to filter the results according to the visitors\' access priviliges.',
	'accesrestreint_nom' => 'Restricted Access',
);
?>