<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/accesrestreint?lang_cible=ar
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_droits_auteur' => 'إعطائي حق الدخول الى هذه المنطقة',
	'aucune_zone' => 'لا توجد أية منطقة',
	'auteurs' => 'المؤلفين',

	// B
	'bouton_configurer_acces' => 'Configuration des accès .htaccess', # NEW
	'bouton_creer_la_zone' => 'إنشاء المنطقة الجديدة',

	// C
	'colonne_id' => 'الرقم',
	'confirmer_ajouter_auteurs' => 'هل انت متأكد من إضافة هذا المؤلف الى المنطقة؟',
	'confirmer_retirer_auteur_zone' => 'هل تريد فعلاً سحب هذا المؤلف من المنطقة؟',
	'confirmer_retirer_auteurs' => 'هل تريد فعلاً سحب جميع المؤلفين من المنطقة؟',
	'confirmer_retirer_rubrique_zone' => 'Êtes vous sûr de vouloir retirer cette rubrique de cette zone ?', # NEW
	'confirmer_supprimer_zone' => 'هل تريد فعلاً حذف هذه المنطقة؟',
	'creer_zone' => 'إنشاء منطقة جديدة',

	// D
	'descriptif' => 'الوصف',

	// E
	'explication_creer_htaccess' => 'Cette option interdit la lecture des documents joints si le texte auquel ils se rattachent n\'est pas publi&eacute', # NEW

	// I
	'icone_menu_config' => 'دخول محصور',
	'icone_supprimer_zone' => 'حذف هذه المنطقة',
	'info_1_zone' => '1 zone', # NEW
	'info_acces_restreint' => 'الوصول الى هذه الصفحة محصور. عليك التعريف بنفسك للوصول اليها',
	'info_ajouter_auteur' => 'إضافة مؤلف',
	'info_ajouter_auteurs' => 'إضافة جميع المؤلفين',
	'info_ajouter_zones' => 'إضافة كل المناطق',
	'info_aucun_acces' => 'لا يوجد أي دخول مسموح',
	'info_aucun_auteur' => 'لا يوجد أي مؤلف في المنطقة',
	'info_aucune_zone' => 'لا توجد أية منطقة',
	'info_auteurs_lies_zone' => 'المؤلفون المسموح لهم الدخول الى هذه المنطقة',
	'info_lien_action_proteger' => 'حماية الوصول الى هذا القسم.',
	'info_nb_zones' => '@nb@ zones', # NEW
	'info_page' => 'تتيح لك هذه الصفحة إدارة مناطق الدخول المحصور في الموقع',
	'info_retirer_auteurs' => 'سحب جميع المؤلفين',
	'info_retirer_zone' => 'سحب من المنطقة',
	'info_retirer_zones' => 'إلغاء كل المناطق',
	'info_rubrique_dans_zone' => 'هذا القسم هو ضمن المنطقة:',
	'info_rubrique_dans_zones' => 'هذا القسم هو ضمن المناطق:',

	// L
	'label_creer_htaccess' => 'Accès aux document joints par leur URL', # NEW
	'label_creer_htaccess_non' => 'autoriser la lecture', # NEW
	'label_creer_htaccess_oui' => 'interdire la lecture', # NEW

	// M
	'modifier_zone' => 'تعديل المنطقة',

	// P
	'page_zones_acces' => 'دخول محصور',
	'par_titre' => 'حسب الاسم',
	'privee' => 'الخاص',
	'publique' => 'العمومي',

	// R
	'rubriques' => 'أقسام',
	'rubriques_zones_acces' => 'أقسام المنطقة',

	// S
	'selectionner_une_zone' => 'تحديد منطقة',

	// T
	'texte_ajouter_zone' => 'Ajouter une zone', # NEW
	'texte_creer_associer_zone' => 'Créer et associer une zone', # NEW
	'titre' => 'الاسم',
	'titre_ajouter_zone' => 'الربط بالمنطقة',
	'titre_boite_protegee_non' => 'وصول غير محمي',
	'titre_boite_protegee_oui' => 'وصول محمي',
	'titre_cadre_modifier_zone' => 'تعديل منطقة',
	'titre_page_config' => 'إعداد الدخول',
	'titre_table' => 'كل مناطق الدخول',
	'titre_zone_acces' => 'Zone d\'accès restreint', # NEW
	'titre_zones_acces' => 'مناطق الدخول المحصور',
	'toutes' => 'الكل',

	// V
	'voir_toutes' => 'عرض كل المناطق',

	// Z
	'zone_numero' => '<strong>منطقة رقم</strong>',
	'zone_restreinte_espace_prive' => 'حصر الدخول الى هذه المنطقة في المجال الخاص',
	'zone_restreinte_publique' => 'حصر الدخول الى هذه المنطقة في الموقع العمومي'
);

?>
