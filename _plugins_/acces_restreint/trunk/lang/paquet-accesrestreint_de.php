<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier produit par PlugOnet
// Module: paquet-accesrestreint
// Langue: de
// Date: 18-10-2011 17:20:21
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
	'accesrestreint_description' => '_ Jeder Bereich enthält Rubriken.
_ Einzelnen Autoren können Zugangsrechte für Bereiche zugeordnet werden.
_ ALle Standardschleifen (Abfragen) von SPIP werden überladen, und um die Überprüfung der Rechte des jeweiligen Sitebesuchers ergänzt.',
	'accesrestreint_nom' => 'Zugangskontrolle',
	'accesrestreint_slogan' => 'Verwaltung von Bereichen mit Zugangskontrolle',
);
?>