<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/noizetier?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'apercu' => 'Preview',

	// B
	'bloc_sans_noisette' => 'This block doesn\'t contain nuts.',

	// C
	'choisir_noisette' => 'Choose the nut you want to add:',
	'compositions_non_installe' => '<b>Plugin Compositions:</b> this plugin isn\'t installed on your site. It\'s not necessary to the correct working of the nuts. However, when it\'s activated, you can create compositions directly inside your Nuts Manager.',

	// D
	'description_bloc_contenu' => 'Main content of each page.',
	'description_bloc_extra' => 'Contextual extra information for each page.',
	'description_bloc_navigation' => 'Navigation information specific to each page.',
	'description_bloctexte' => 'The title is optional. For the text, you can use SPIP typographical shortcuts.',

	// E
	'editer_composition' => 'Edit this composition',
	'editer_composition_heritages' => 'Define heritages',
	'editer_configurer_page' => 'Configure the nuts of this page',
	'editer_exporter_configuration' => 'Export the configuration',
	'editer_importer_configuration' => 'Import a configuration.',
	'editer_noizetier_explication' => 'Select the page where you want to configure the nuts.',
	'editer_noizetier_titre' => 'Manage the nuts',
	'editer_nouvelle_page' => 'Create a new page / composition',
	'erreur_aucune_noisette_selectionnee' => 'You must select a nut!',
	'erreur_doit_choisir_noisette' => 'You have to choose a nut.',
	'erreur_mise_a_jour' => 'An error occurred while updating the database.',
	'explication_glisser_deposer' => 'You can add a nut or order them by drag and drop.',
	'explication_heritages_composition' => 'Here you can set the compositions that will be inherited by the objects of the branch.',
	'explication_noizetier_css' => 'You can add some additional CSS classes to the nut.',
	'explication_raccourcis_typo' => 'You can use the SPIP typographical shortcuts.',

	// F
	'formulaire_ajouter_noisette' => 'Add a nut',
	'formulaire_composition' => 'Identifier of composition',
	'formulaire_composition_explication' => 'Specify a unique keyword (lowercase, no spaces, no dashes (-) and without accents) to identify the composition. <br />For example: <i>mycompo</i>.',
	'formulaire_composition_mise_a_jour' => 'Composition updated',
	'formulaire_configurer_bloc' => 'Configure the block:',
	'formulaire_configurer_page' => 'Configure the page:',
	'formulaire_deplacer_bas' => 'Move down',
	'formulaire_deplacer_haut' => 'Move up',
	'formulaire_description' => 'Description',
	'formulaire_description_explication' => 'You can use SPIP usual shortcuts, especially the &lt;multi&gt; tag.',
	'formulaire_erreur_format_identifiant' => 'The identifier can only contain lowercase letters without accents, numbers and the "_" (underscore) character.',
	'formulaire_icon' => 'Icon',
	'formulaire_icon_explication' => 'You can enter the relative path to an icon (for example : <i>images/list-item-contenus.png</i>).',
	'formulaire_identifiant_deja_pris' => 'This identifier already exists!',
	'formulaire_import_compos' => 'Import the compositions of the Nuts Manager',
	'formulaire_import_fusion' => 'Merge with the current configuration',
	'formulaire_import_remplacer' => 'Replace the current configuration',
	'formulaire_liste_compos_config' => 'This configuration file defines the following Nuts Manager compositions:',
	'formulaire_liste_pages_config' => 'This configuration file defines
nuts on the following pages:',
	'formulaire_modifier_composition' => 'Edit this composition:',
	'formulaire_modifier_composition_heritages' => 'Edit the heritages',
	'formulaire_modifier_noisette' => 'Edit this nut',
	'formulaire_modifier_page' => 'Edit this page',
	'formulaire_noisette_sans_parametre' => 'This nut has no parameters.',
	'formulaire_nom' => 'Title',
	'formulaire_nom_explication' => 'You can use the &lt;multi&gt; tag.',
	'formulaire_nouvelle_composition' => 'New composition',
	'formulaire_obligatoire' => 'Required field',
	'formulaire_supprimer_noisette' => 'Delete this nuts',
	'formulaire_supprimer_noisettes_page' => 'Delete the nuts of this page',
	'formulaire_supprimer_page' => 'Delete this page',
	'formulaire_type' => 'Page type',
	'formulaire_type_explication' => 'Indicate on which object does this composition apply or if you wish to create a specific page.',
	'formulaire_type_import' => 'Import type',
	'formulaire_type_import_explication' => 'You can merge the configuration file with your actual configuration (the nuts of each page will be added to your already defined nuts) or you can replace your configuration by this one.',

	// I
	'icone_introuvable' => 'Icon not found!',
	'ieconfig_ne_pas_importer' => 'Do not import',
	'ieconfig_noizetier_export_explication' => 'Will export the configuration of the nuts and compositions of the Nuts Manager.',
	'ieconfig_noizetier_export_option' => 'Included in the export?',
	'ieconfig_non_installe' => '<b>Configuration Import/Export Plugin:</b> this plugin isn\'t installed on your site. It is not necessarry to the correct working of the Nuts Manager. However, when it\'s activated, you can  export and import some nuts configurations into the Nuts Manager.',
	'ieconfig_probleme_import_config' => 'A problem occured while importing the Nuts Manager configuration.',
	'info_composition' => 'COMPOSITION:',
	'info_page' => 'PAGE:',
	'installation_tables' => 'Installed tables of the Nuts Manager Plugin.<br />',
	'item_titre_perso' => 'custom title',

	// L
	'label_afficher_titre_noisette' => 'Display a title of nut?',
	'label_niveau_titre' => 'Title level:',
	'label_noizetier_css' => 'CSS classes:',
	'label_texte' => 'Text:',
	'label_titre' => 'Title:',
	'label_titre_noisette' => 'Title of the nut:',
	'label_titre_noisette_perso' => 'Custom title:',
	'liste_icones' => 'Icons list',
	'liste_pages' => 'List of the pages',

	// M
	'masquer' => 'Hide',
	'mode_noisettes' => 'Edit the nuts',
	'modif_en_cours' => 'Ongoing changes',
	'modifier_dans_prive' => 'Modifier dans l\'espace privé', # NEW

	// N
	'ne_pas_definir_d_heritage' => 'Do not define heritage',
	'noisette_numero' => 'nut number :',
	'noisettes_composition' => 'Specific nuts to the composition <i>@composition@</i>:',
	'noisettes_disponibles' => 'Nuts available',
	'noisettes_page' => 'Specific nuts to the page <i>@type@</i>:',
	'noisettes_toutes_pages' => 'Common nuts to all pages:',
	'noizetier' => 'Nuts Manager',
	'nom_bloc_contenu' => 'Content',
	'nom_bloc_extra' => 'Extra',
	'nom_bloc_navigation' => 'Navigation',
	'nom_bloctexte' => 'Block of free text',
	'non' => 'No',
	'notice_enregistrer_rang' => 'Clic on Save to store the nuts order.',

	// O
	'operation_annulee' => 'Operation canceled.',
	'oui' => 'Yes',

	// P
	'page' => 'Page',
	'page_autonome' => 'Specific page',
	'probleme_droits' => 'You don\'t have the permission to make this change.',

	// Q
	'quitter_mode_noisettes' => 'Quit editing nuts',

	// R
	'retour' => 'Back',

	// S
	'suggestions' => 'Suggestions',

	// W
	'warning_noisette_plus_disponible' => 'WARNING: this nut is no longer available.',
	'warning_noisette_plus_disponible_details' => 'The template of the nut (<i>@squelette@</i>) is no longer available. It may be a nut requiring a plugin that you have disabled or uninstalled.'
);

?>
