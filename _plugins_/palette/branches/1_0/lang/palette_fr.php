<?php
// Ceci est un fichier langue de SPIP
$GLOBALS[$GLOBALS['idx_lang']] = array( // Espace ecrire
  'palette' => "Palette",
  'fermer' => 'Fermer la palette',
  'palette_public' => "Active Palette sur le site public",
  'palette_ecrire' => "Active Palette pour l'espace ecrire",
);

?>