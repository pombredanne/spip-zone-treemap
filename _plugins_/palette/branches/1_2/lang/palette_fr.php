<?php
// Ceci est un fichier langue de SPIP
$GLOBALS[$GLOBALS['idx_lang']] = array( // Espace ecrire
	'palette' => "Palette",
	'fermer' => 'Fermer la palette',
	'palette_public' => "Active Palette sur le site public",
	'palette_ecrire' => "Active Palette pour l'espace ecrire",
	'config_titre' => '<h4>Configuration du plugin Palette</h4>',
	'config_desc' => '<p>Palette permet d\'ajouter automatiquement un s&eacute;lecteur de couleurs &agrave; un champ input, simplement en lui donnant la classe <strong>palette</strong>:</p>',
	'config_exemple' => 'Exemple : ',
	
);

?>