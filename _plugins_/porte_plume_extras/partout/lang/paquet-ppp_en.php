<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-ppp?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'ppp_description' => 'Quill everywhere enables the editing toolbar on other text fields (standfirst, description, ps)',
	'ppp_nom' => 'Quill everywhere',
	'ppp_slogan' => 'Display Quill on most input fields'
);

?>
