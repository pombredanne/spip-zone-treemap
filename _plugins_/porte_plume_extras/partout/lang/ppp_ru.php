<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ppp?lang_cible=ru
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_mettre_a_jour' => 'Обновить',

	// C
	'cfg_attention' => 'Внимание',
	'cfg_description' => 'en fonction de vos squelettes, veillez à n\'activer le Porte Plume que sur des champs pour lesquels l\'utilisation des raccourcis n\'engendrera pas d\'erreur xhtml.', # NEW
	'cfg_titre' => 'Porte Plume Partout',

	// E
	'explication_personnalisation' => 'Indiquez la cible des éléments qui utiliseront la barre typographique (Expression CSS ou étendue jQuery).', # NEW

	// L
	'label_hauteur_champ' => 'Demi-hauteur de l\'écran', # NEW
	'label_personnalisation' => 'Sélecteur personnalisé', # NEW
	'legend_barre_typo' => 'Добавить панель инструментов для :',
	'legend_hauteur_champ' => 'Hauteur du champ texte pour les articles', # NEW
	'legend_personnalisation' => 'Персонализация',

	// S
	'supprimer' => 'Сбросить в настройки по умолчанию'
);

?>
