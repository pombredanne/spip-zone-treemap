<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/porte_plume_extras/codes/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_activer_barre_edition' => 'Barre d\'édition',
	'label_activer_barre_forum' => 'Barre de forum',
	'label_outils_actifs' => 'Outils actifs',

	// O
	'outil_inserer_cadre_css' => 'Insérer un code preformaté CSS (cadre)',
	'outil_inserer_cadre_php' => 'Insérer un code preformaté PHP (cadre)',
	'outil_inserer_cadre_spip' => 'Insérer un code preformaté SPIP (cadre)',
	'outil_inserer_cadre_xml' => 'Insérer un code preformaté XML (cadre)',
	'outil_inserer_kbd' => 'Insérer une entrée clavier (kdb)',
	'outil_inserer_lien_trac' => 'Insérer un lien vers le trac de SPIP',
	'outil_inserer_pre' => 'Insérer un code preformaté (pre)',
	'outil_inserer_samp' => 'Insérer une sortie de code (samp)',
	'outil_inserer_var' => 'Insérer une variable (var)',

	// P
	'pp_codes' => 'Codes informatiques pour Porte Plume',

	// T
	'titre_activer_extension_sur' => 'Activer sur quelles barres d\'outils ?',
	'titre_configurer_pp_codes' => 'Configurer l\'extension codes informatiques pour Porte Plume'
);

?>
