<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-pp_codes?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pp_codes_description' => 'Ovládanie tlačidiel na vkladanie počítačového kódu prostredníctvom panelu úprav Porte Plume.
Pridáva aj skratku <code>[->ecrire/inc_versions.php#trac].</code>',
	'pp_codes_nom' => 'Počítačové kódy pre Porte Plume',
	'pp_codes_slogan' => 'Pridá tlačidlá na panel nástrojov Porte Plume na správu počítačových kódov'
);

?>
