<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/pp_codes?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_activer_barre_edition' => 'Site edit toolbar',
	'label_activer_barre_forum' => 'Forum toolbar',
	'label_outils_actifs' => 'Active tools',

	// O
	'outil_inserer_cadre_css' => 'Insert a CSS preformatted code (cadre)',
	'outil_inserer_cadre_php' => 'Insert tags for a PHP class frame segment (cadre)',
	'outil_inserer_cadre_spip' => 'Insert tags for a SPIP class frame segment (cadre)',
	'outil_inserer_cadre_xml' => 'Insert a preformatted XML code (cadre)',
	'outil_inserer_kbd' => 'Insert tags for keyboard sequence (kdb)',
	'outil_inserer_lien_trac' => 'Insert a link to SPIP\'s trac repository',
	'outil_inserer_pre' => 'Insert tags for preformatted text (pre)',
	'outil_inserer_samp' => 'Insert tags for an escaped sample (samp)',
	'outil_inserer_var' => 'Insert tags for a variable (var)',

	// P
	'pp_codes' => 'Computer codes for Porte Plume',

	// T
	'titre_activer_extension_sur' => 'Activate on which toolbars?',
	'titre_configurer_pp_codes' => 'Configurer the Porte Plume extension for special display codes'
);

?>
