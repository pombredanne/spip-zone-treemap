<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/porte_plume_extras/codes/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pp_codes_description' => 'Gestion de boutons pour insérer du code informatique via la barre d\'édition Porte Plume.
Ajoute aussi un raccourci <code>[->ecrire/inc_versions.php#trac]</code>.',
	'pp_codes_nom' => 'Codes Informatiques pour Porte Plume',
	'pp_codes_slogan' => 'Ajoute des boutons à la barre d\'outils du Porte Plume pour gérer des codes informatiques'
);

?>
