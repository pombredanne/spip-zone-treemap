<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-pp_codes?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pp_codes_description' => 'Buttons management to insert computer code with the Quill toolbar.
Adds a shortcut <code>[->ecrire/inc_versions.php#trac]</code>.',
	'pp_codes_nom' => 'Computer codes for Quill',
	'pp_codes_slogan' => 'Adds buttons to the Quill toolbar to handle computer code'
);

?>
