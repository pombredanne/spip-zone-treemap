<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(

	//O
	'outil_inserer_code' => "Ins&eacute;rer un code informatique (code)",
	'outil_inserer_cadre_spip' => "Ins&eacute;rer un code preformat&eacute; SPIP (cadre)",
	'outil_inserer_cadre_php' => "Ins&eacute;rer un code preformat&eacute; PHP (cadre)",
	'outil_inserer_pre' => "Ins&eacute;rer un code preformat&eacute; (pre)",
	'outil_inserer_var' => "Ins&eacute;rer une variable (var)",
	'outil_inserer_samp' => "Ins&eacute;rer une sortie de code (samp)",
	'outil_inserer_kbd' => "Ins&eacute;rer une entr&eacute;e clavier (kdb)",
	'outil_inserer_lien_trac' => "Ins&eacute;rer un lien vers le trac de SPIP",

	//P
	'pp_codes' => "Codes informatiques pour Porte Plume",
	
	//C
	'cfg_description_pp_codes' => "Configurer l'extension codes informatiques pour Porte Plume",
	'cfg_activer_extension_sur' => "Activer sur quelles barres d'outils ?",
	'cfg_activer_barre_edition' => "Barre d'&eacute;dition",
	'cfg_activer_barre_forum' => "Barre de forum",
);
?>
