<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(

	//O
	'outil_inserer_code' => "Insert tags for included code",
	'outil_inserer_cadre_spip' => "Insert tags for a SPIP class frame segment (cadre)",
	'outil_inserer_cadre_php' => "Insert tags for a PHP class frame segment (cadre)",
	'outil_inserer_pre' => "Insert tags for preformatted text (pre)",
	'outil_inserer_var' => "Insert tags for a variable (var)",
	'outil_inserer_samp' => "Insert tags for an escaped sample (samp)",
	'outil_inserer_kbd' => "Insert tags for keyboard sequence (kdb)",
	'outil_inserer_lien_trac' => "Insert a link to SPIP's trac repository",

	//P
	'pp_codes' => "Computer codes for Porte Plume",
	
	//C
	'cfg_description_pp_codes' => "Configurer the Porte Plume extension for special display codes",
	'cfg_activer_extension_sur' => "Activate on which toolbars?",
	'cfg_activer_barre_edition' => "Site edit toolbar",
	'cfg_activer_barre_forum' => "Forum toolbar",
);
?>
