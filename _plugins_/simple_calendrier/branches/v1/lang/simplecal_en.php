<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
// A
	'a_venir' => 'Coming',
    'auteurs_titre' => 'Author(s)',
    'auteur_numero' => 'N&deg; author',
    'auteur_ajouter' => 'Add',
    'auteur_retirer' => 'Remove',
    'auteur_aucun' => 'No author !',
    'auteur_msg_num_manquant' => "Author nb. is missing !",
    'auteur_msg_num_incorrect' => "Author nb. is incorrect !",
    'auteur_msg_id_inexistant' => "Auteur nb.@id_auteur@ is missing !",
    'auteur_msg_id_dejala' => "Nb.@id_auteur@ is already an author !",
    'auteur_msg_id_pasauteur' => "Nb.@id_auteur@ is not an author for this event !",
    'auteur_msg_erreur' => 'Error',
    'auteur_msg_ajout' => 'Author added !',
    'auteur_msg_retrait' => 'Author removed !',
    

// B
	'bouton_entree' => 'Events',
    'bouton_admin' => 'Event',

// C
    'calendrier' => "calendar",
    'corbeille_tous' => "@nb@ events in garbage",
    'corbeille_un' => "1 event in garbage",
    'config_titre' => "Simple calendrier",
    'config_refobj_oui' => "Enable text by reference",
    'config_refobj_non' => "Disable text by reference",
    'config_refobj_explication' => "Enable 'text by reference' to allow articles/breves to be in calendar.",
    'config_themes_explication' => "You can choose a theme (color set) for the 'little calendar' in public area.",
    'config_autorisation_explication' => "You can choose if event creation are enabled for editors.",
    'config_rubrique_explication' => "If you want to manage a simple calendar for your site, let this option disabled. It is possible to use keywords for categorised your events.<br />But, if you want to manage multiple calendar, and display distinct events for each folder, you can enable it.",
    'config_themepublic' => "Public area theme",
    'config_minicalendrier' => "little calendar",
    'config_blocreference' => "References",
    'config_blocthemes' => "Themes",
    'config_blocautorisation' => "Autorisations",
    'config_blocrubrique' => 'Folders affectation',
    'config_autorisation_redac_non' => 'Administrators only.',
    'config_autorisation_redac_oui' => 'Administrators and editors',
    'config_rubrique_partout' => 'Yes, create events in folder.',
    'config_rubrique_secteur' => 'Yes, create events in 1st level folder (sector).',
    'config_rubrique_non' => 'No, it makes no sens for me.',
    'config_champs' => 'Events content',
    'config_champs_explication' => 'Based on the layout chosen for your site, you can decide which elements in the following list should be available.',
    

// D
    'description_plugin' => 'Calendar',
    'date' => 'Date',
    'dates' => 'Dates',
    'date_du_au' => 'from @date_debut@ to @date_fin@',
    'date_le' => '@date@',
    'date_jusque' => 'to @date@',
    'date_de_debut' => 'First date',
    'date_de_fin' => 'Last date',
    'descriptif' => 'Description',
    'date_picto_title' => 'Select date with datepicker...',
    'date_janvier_abbr' => 'Jan.',
    'date_fevrier_abbr' => 'Feb.',
    'date_mars_abbr' => 'Mar.',
    'date_avril_abbr' => 'Apr.',
    'date_mai_abbr' => 'May',
    'date_juin_abbr' => 'Jun.',
    'date_juillet_abbr' => 'Jul.',
    'date_aout_abbr' => 'Aug.',
    'date_septembre_abbr' => 'Sep.',
    'date_octobre_abbr' => 'Oct.',
    'date_novembre_abbr' => 'Nov.',
    'date_decembre_abbr' => 'Dec.',
    'date_precedent' => 'Previous',
    'date_suivant' => 'Next',
    'date_lundi_abbr' => 'Mo',
    'date_mardi_abbr' => 'Tu',
    'date_mercredi_abbr' => 'We',
    'date_jeudi_abbr' => 'Th',
    'date_vendredi_abbr' => 'Fr',
    'date_samedi_abbr' => 'Sa',
    'date_dimanche_abbr' => 'Su',
    'demo_aucun' => 'Aucun !',
    'demo_les_evenements_critere' => 'Events',
    'demo_description' => "A demonstration to see how to use EVENEMENTS loop, with various criteria.",
    'demo_voir' => 'See',
    
    
// E
    'explication_titre' => 'Event Label',
    'explication_ref' => 'Reference to an article or news (ex : article23 ; breve45)',
    'explication_texte' => 'Text for event description.',
    'entree_evenement_publie' => 'Status of the event :',
    'enregistrer_dabord_une_fois' => "It's possible ! <br /><br />But first, please save this page one time...",
    
// F
    'filtres' => 'filters',
    'filtres_rubrique_concernee' => 'concerned folder',
    
// H
    'html_title' => 'Calendar',

// I
    'info_titre' => 'Title',
    'info_date_debut' => 'First date',
    'info_date_fin' => 'Last date',
    'info_lieu' => 'Place',
    'info_descriptif_rapide' => 'Brief description',
    'info_ref_1pos' => 'Text : by reference (1\'st possibility)',
    'info_ref' => 'Text : by reference',
    'info_texte' => 'Text',
    'info_texte_2possibilites' => 'Text : specific (2\'nd possibility)',
    'info_gauche_numero_evenement' => 'EVENT NUMBER',
    'info_statut_encours' => "In progress",
	'info_statut_proposee' => "Submitted for publication",
	'info_statut_validee' => 'Published',
    'info_statut_refusee' => 'Rejected',
	'info_statut_poubelle' => 'In the dustbin',
    'icone_modifier_evenement' => 'Edit this event',
    'info_en_cours' => "[in progress]",
    'info_a_valider' => "[pending]",
    'item_mots_cles_association_evenements' => 'events',
    'info_evenement_libelle' => 'Events',
    'inconnu' => 'unknown',
    'info_evenements_liees_mot' => 'Events associated with this keyword',
    'info_1_evenement' => 'event',
    'info_n_evenements' => 'events',

// L
    'liste_evenements_prop' => "Events submitted for publication.",
    'liste_evenements_a_valider' => "Events items to be validated.",
    'liste_evenements_publie' => "Events published.",
    'liste_evenements_prepa' => "Events in progress.",
    'liste_evenements_refuse' => "Events rejected.",
    'liste_evenements_poubelle' => 'Event in the dustbin',
    'liste_evenements_auteur' => "This author's events",
    'logo_evenement' => "LOGO EVENT",
    'lieu' => 'Place',
    'liste_des_evenements' => 'Events list',
    'liste_des_evenements_rubrique' => 'Events list for folder',

// M
    'multiples_auteurs' => 'Multi auteurs',
    
// R
    'raccourcis_ecrire_evenement' => 'Write a new event',
    'raccourcis_tous_evenements' => 'All events',
    'raccourcis_tous_evenements_rubrique' => 'Events for same folder',
    'raccourcis_retour' => 'Back',
    'raccourcis_ajouter_date' => 'Add a date',
    'raccourcis_liste_evenements_rubrique' => 'Events list for this folder',
    'raccourcis_demo' => 'D&eacute;mo...',
    'reference_objet' => 'see',
    'retour_accueil' => 'Back home',
    'retour_rubrique' => 'Back to folder',
    

// T
    'texte_modifier_evenement' => 'Edit this event',
    'titre_nouvel_evenement' => 'New event',
    'terme_evenement' => "event",
    'terme_evenements' => "events",
    'titre_evenement_preparation' => 'Event in progress',
    'titre_evenement_propose' => 'Event submitted for publication',
    'titre_evenement_publie' => 'Event published',
    'titre_evenement_refuse' => 'Event rejected',
    'titre_evenement_poubelle' => 'Event in the dustbin',
    'texte_issu_autre_objet' => 'Text related to other content',
    'texte' => 'Text',
    'titre_evenements' => 'Events',
    'tous' => 'All',
    'titre_boite_refobj' => 'in calendar',
    'titre_boite_rubrique' => 'Calendar',
    
// V
    'validation_titre' => 'Title is required !',
    'validation_date_debut' => 'First date is required.',
    'validation_date_format' => 'Date format must be : dd/mm/yyyy',
    'validation_refobj_format' => 'Format must be : [type][id]. ex : article23 ; breve45',
    'validation_rubrique' => 'Folder is required.',
    'validation_corriger_svp' => 'Please correct the mistakes...',
    'validation_type_nexiste_pas' => "@type@ n&deg; @id_objet@ doesn't exist !",
    
);

?>
