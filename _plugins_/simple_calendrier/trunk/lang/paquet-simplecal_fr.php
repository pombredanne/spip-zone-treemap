<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'simplecal_description' => 'Un simple calendrier pour planifier des évènements.',
	'simplecal_slogan' => 'Planifier simplement vos évènements.',
);
?>