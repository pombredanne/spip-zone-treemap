<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/edition_directe/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_edition_directe' => 'Activer l\'édition directe pour :',
	'activer_edition_directe_objet' => 'Activer l\'édition directe pour l\'objet ',

	// D
	'desactiver_edition_directe_objet' => 'Désactiver l\'édition directe pour l\'objet ',

	// T
	'titre_plugin' => 'Edition directe',

	// V
	'vider_cache' => 'Veuillez vider le cache'
);

?>
