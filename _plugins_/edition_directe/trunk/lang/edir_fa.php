<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/edir?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_edition_directe' => 'فعال سازي ويرايش مستقيم براي: ',
	'activer_edition_directe_objet' => 'Activer l\'édition directe pour l\'objet ', # NEW

	// D
	'desactiver_edition_directe_objet' => 'Désactiver l\'édition directe pour l\'objet ', # NEW

	// T
	'titre_plugin' => 'ويرايش مستقيم',

	// V
	'vider_cache' => 'لطفاً حافظه‌ي دم‌دستي را خالي كنيد'
);

?>
