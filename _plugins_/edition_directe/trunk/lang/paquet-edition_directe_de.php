<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-edition_directe?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'edition_directe_description' => 'Ermöglicht es Spip Objekete im Backoffice direkt auf von deren Seite aus zu bearbeiten. Die gewünschten Objekte müssen zuerst via cfg aktiviert werden',
	'edition_directe_slogan' => 'Ermöglicht es Spip Objekete direkt auf von deren Seite her zu bearbeiten'
);

?>
