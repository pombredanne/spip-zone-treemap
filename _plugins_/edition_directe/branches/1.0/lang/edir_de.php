<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_edition_directe' => 'Direkt Edition für :',
	'article' => 'Artikel',
	'auteur' => 'Autoren',

	// B
	'breve' => 'Meldungen',

	// G
	'groupe_mots' => 'Schlagwortgruppe',

	// M
	'mot' => 'Schlagwort',

	// R
	'rubrique' => 'Rubriken',

	// S
	'site' => 'Websites',

	// T
	'titre_plugin' => 'Direkte Edition',

	// V
	'vider_cache' => 'Leeren Sie bitte die Sitecache'
);

?>
