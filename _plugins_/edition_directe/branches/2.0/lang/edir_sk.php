<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_edition_directe' => 'Aktivovať priame úpravy pre:',
	'article' => 'Články',
	'auteur' => 'Autori',

	// B
	'breve' => 'Novinky',

	// G
	'groupe_mots' => 'Skupiny kľúčových slov',

	// M
	'mot' => 'Kľúčové slová',

	// R
	'rubrique' => 'Rubriky',

	// S
	'site' => 'Stránky',

	// T
	'titre_plugin' => 'Priama editácia',

	// V
	'vider_cache' => 'Prosím, vyprázdnite cache'
);

?>
