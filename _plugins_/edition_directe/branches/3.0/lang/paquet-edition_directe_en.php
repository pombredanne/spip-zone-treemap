<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-edition_directe?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'edition_directe_description' => 'Permits direct editing of the spip objects from their page. By default only articles are in direct mode, you can ad other objects in the plugins configuration panel',
	'edition_directe_slogan' => 'Permits direct editing of the spip objects from their page'
);

?>
