<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
   'conf_base'    => 'Base PhpBB',
   'conf_comment' => 'Forum des commentaires',
   'conf_continue'   => 'Cliquez ici pour continuer la configuration du plugin',
   'conf_http'    => 'Adresse internet du site du forum (en relatif par rapport à spip ou en absolu)',
   'conf_param'   => 'Indiquez les paramètres de la base de données avant de continuer',
   'conf_prefix'  => 'Pr&eacute;fixe des tables PhpBB',
   'conf_robot'   => 'Utilisateur-robot (cet utilisateur sera le posteur du topic)',
   'conf_temp'    => 'Forum temporaire (o&ugrave; les topics sont d&eacute;plac&eacute;s si le statut de l\'article n\'est plus "publi&eacute;")',
   'conf_titre'   => 'Configuration g&eacute;n&eacute;rale',
   'description'  => 'Configuration du plugin qui g&egrave;re les commentaires sur votre forum phpBB',
   'titre'        => 'Commentaires PhpBB',
   'maj_ok_texte' => 'Le forum phpBB a &eacute;t&eacute; mis &agrave; jour avec l\'ensemble des articles de SPIP',
   'maj_lien'     => 'Mettre &agrave; jour le forum avec tous les articles existants',
   'phpbb_texte'  => 'Lire l\'article',
   'public_voir_forum'  => 'Voir tous les messages du forum sur cet article'
);
?>