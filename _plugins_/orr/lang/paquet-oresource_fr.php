<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'oresource_description' => 'Adaptation du célèbre GRR pour spip',
	'oresource_nom' => 'ORR',
	'oresource_slogan' => 'Organisation de réservations de ressources',
);

?>