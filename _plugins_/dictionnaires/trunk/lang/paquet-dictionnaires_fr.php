<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-dictionnaires
// Langue: fr
// Date: 09-02-2012 14:32:07
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// D
	'dictionnaires_description' => 'Permet la création de dictionnaires regroupant des définitions. Les termes définis sont automatiquement détectés dans les textes du site et il est alors possible de les insérer dans le HTML du texte de manière accessible.',
	'dictionnaires_slogan' => 'Créer des dictionnaires',
	'dictionnaires_nom' => 'Dictionnaires',
);
?>
