<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(

// formulaire inscription2
'coord_livraison' => 'Delivery coordinates',
'adresse_livraison' => 'Address (delivery)',

// Formulaire profil : controleurs et vues
'contacts_livraison' => 'Delivery contact',
'vos_contacts_livraison' => 'Your delivery contacts',
'votre_adresse_livraison' => 'Your delivery address',

// A
'acces_non_autorise'=>'We are sorry,<br />you\'re not allowed to access this section.',

// C
'categorie_numero' => 'Category number:',
'creer_nouvelle_categorie' => 'Create a new category',
'creer_un_ref_automatiquement' => 'Create one automatically',
'colisage' => 'Packing',
'contacts_livraison' => 'Delivery contact', 
'coord_livraison' => 'Delivery coordinates', 

// D
'date_de_mise_en_vente' => 'Date put on sale',
'date_de_retrait_de_vente' => 'Date of collection',
'descriptif'=>'Description:',
'descriptif_echoppe' => 'Echoppe is an administration plugin for online shopping . It tries to be as generalist as possible.',
'derniere_modification' => 'Last modifications',

// E
'echoppe'=>'Echoppe, the plugin.',
'editer_categorie' => 'Edit',
'editer_version'=>'Edit the version',
'edition_de_cetegorie'=>'Category edition',
'edition_langue' => 'Language edition',
'edition_de_produit' => 'Product edition',
'edition_donnees_globales' => 'Global datas edition',

// G
'gerer_echoppe' => 'Administer Echoppe',

// H
'hauteur' => 'Height',

// L
'langue_editee' => 'Language edition',
'largeur' => 'Width',
'les_categories' => 'The categories',
'les_produits' => 'The products',
'longeur' => 'Length',

// N
'nouveau_produit' => 'Create a product',
'nouvelle_cetegorie' => 'New category',

// P
'par_defaut' => 'default (setting)',
'pas_de_categorie_ici' => 'No category here',
'pas_de_produit_ici' => 'No product at this adress',
'pas_encore_cree' => 'Doesn\'t exist yet.',
'pas_encore_modifie' => 'Never modified;',
'prix_base' => 'Basic price excluding VAT',
'poids' => 'Weight',
'ps' => 'Remark',

// Q
'quantite_minimum' => 'Minimum quantity',

// R
'reference_interne' => 'Reference number',
'retour' => 'Back',


// T
'texte'=>'Text :',
'titre_categorie' => 'Category title:',
'titre_produit' => 'Product title:',
'tva' => 'VAT',

// U
'unites' => 'Units',

// V 
'vos_contacts_livraison' => 'Your delivery contacts', 
'votre_adresse_livraison' => 'Your delivery address', 

// X
'xxx' => 'xxx'

);


?>
