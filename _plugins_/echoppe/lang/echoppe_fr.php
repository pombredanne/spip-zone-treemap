<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(

// formulaire inscription2
'coord_livraison' => 'Coordonn&eacute;es de livraison',
'adresse_livraison' => 'Adresse (livraison)',

// Formulaire profil : controleurs et vues
'contacts_livraison' => 'Contacts de livraison',
'vos_contacts_livraison' => 'Vos contacts de livraison',
'votre_adresse_livraison' => 'Votre adresse de livraison',

// A
'acces_non_autorise'=>'D&eacute;sol&eacute;,<br />vous n\'avez pas le droit d\'acc&eacute;der &agrave; cette partie.', 
'acces_rapide_aux_produits' => 'Acc&egrave;s rapide aux produits',
'adresse_du_beneficiaire' => 'Adresse du B&eacute;n&eacute;ficiaire',
'adresse_du_depot' => 'Adresse du d&eacute;p&ocirc;t',
'adresse_livraison' => 'Adresse (livraison)', // formulaire inscription2 
'ajouter' => 'Ajouter',
'ajouter_un_depot' => 'Ajouter un d&eacute;p&ocirc;t',
'ajouter_un_prestataire_de_paiement' => 'Ajouter un prestataire de paiement',
'ajouter_une_option' => 'Ajouter une option',
'article' => 'L\'article',
'article_ajoute' => 'a &eacute;t&eacute; ajout&eacute; &agrave; votre panier',

// C
'c_est_termine_vorte_panier_a_bien_ete_enregistre_et_est_en_cours_de_traiment' => 'Votre panier a bien &eacute;t&eacute; valid&eacute;! Vous allez recevoir un e-mail r&eacute;capitulatif de votre commande. Notre &eacute;quipe la traite dans les plus brefs d&eacute;lais.',
'categorie_numero' => 'Cat&eacute;gorie num&eacute;ro :',
'centimetre_racourcci' => 'cm.',
'chemin_categorie' => 'Chemin de la cat&eacute;gorie',
'chez_le_fournisseur' => 'Chez le fournisseur',
'creer_nouvelle_categorie' => 'Cr&eacute;er une cat&eacute;gorie',
'creer_nouvelle_sous_categorie' => 'Cr&eacute;er une sous-cat&eacute;gorie',
'creer_un_ref_automatiquement' => 'En cr&eacute;er un automatiquement',
'code_postal_du_beneficiaire' => 'Code postal du b&eacute;n&eacute;ficiaire',
'code_postal_du_depot' => 'Code postal',
'colisage' => 'Frais de colisage',
'confguration_d_echoppe' => 'Configuration d\'Echoppe',
'configuration_generale' => 'Configuration g&eacute;n&eacute;rale',
'contacts_livraison' => 'Contacts de livraison', 
'coordonees_du_beneficiaire' => 'Coordonn&eacute;es du b&eacute;n&eacute;ficiaire',
'coordonnees_societe' => 'Coordonn&eacute;es soci&eacute;t&eacute; (facultatif)',
'coordonnees_livraison' => 'Coordonn&eacute;es de livraison (facultatif)',
'coord_livraison' => 'Coordonn&eacute;es de livraison', // formulaire inscription2 
'coordonnees_livraison_detail' => 'Si vous ne remplissez pas cette partie, votre commande sera envoy&eacute;e aux coordonn&eacute;es renseign&eacute;es ci-dessus.',
'connection' => 'Connectez-vous',
'commande' => 'Commande',
'commentaire' => 'Communication structur&eacute;',
'continuer_achats' => 'Continuer mes achats',

// D
'dans_votre_panier' =>'dans votre panier pour un montant de',
'dans_la_categorie' => 'Dans la cat&eacute;gorie',
'date_de_mise_en_vente' => 'Date de mise en vente',
'date_de_retrait_de_vente' => 'Date de retrait de la vente',
'denomination_du_beneficiaire' => 'D&eacute;nomination du b&eacute;n&eacute;ficiaire',
'descriptif'=>'Descriptif :',
'descriptif_echoppe' => '<strong>Echoppe est un plugin de gestion de boutique en ligne.</strong><br />Il vous permet de g&eacute;rer les diff&eacute;rents &eacute;tapes n&eacute;cessaires &agrave; la vente en ligne de produits.',
'description_du_depot' => 'Description',
'description_du_prestataire' => 'Description du prestataire',
'detail_du_stock' => 'D&eacute;tail du stock',
'derniere_modification' => 'Derni&egrave;re modification',
'duree_de_vie_des_paniers_temporaires' => 'Dur&eacute;e de vie des paniers temporaires',

// E
'echoppe'=>'Echoppe',
'echoppe_info_panier' => 'Gestion du panier',
'editer'=>'&Eacute;diter',
'editer_donnees'=>'&Eacute;diter mes donn&eacute;es',
'editer_categorie' => '&Eacute;diter la cat&eacute;gorie',
'editer_version'=>'&Eacute;diter la version',
'editer_panier'=>'&Eacute;diter mon panier',
'editer_le_panier'=>'Panier',
'edition_d_un_depot'=>'&Eacute;dition d\'un d&eacute;p&ocirc;t',
'edition_d_un_prestataire' => '&Eacute;dition d\'un prestataire',
'edition_de' => '&Eacute;dition de',
'edition_de_cetegorie'=>'&Eacute;dition d\'une cat&eacute;gorie',
'edition_de_la_version'=>'&Eacute;dition de la version',
'edition_de_produit' => '&Eacute;dition d\'un produit',
'edition_donnees_globales' => '&Eacute;dition des donn&eacute;es globales',
'elements_a_utiliser_dans_les_produits' => '&Eacute;l&eacute;ments &agrave; utiliser dans les produits',
'email_du_beneficiaire' => 'Email du b&eacute;n&eacute;ficiaire',
'email_du_depot' => 'E-Mail',
'email_pour_confirmation_panier' => 'Email des destinataires qui recevront les paniers valid&eacute;s',
'en_stock' => 'en stock',
'enregistrement_et_connexion' => 'Coordonn&eacute;es',
'erreur_de_validation_panier_vous_devez_etre_loge' => 'Erreur de validation. Vous devez &ecirc;tre enregistr&eacute; pour valider votre panier.',
'etat_des_stocks' => '&Eacute;tat des stocks',
'etat_du_produit' => '&Eacute;tat du produit',
'etape_suivante' => 'Etape suivante &raquo;',
'euro' => '&euro;',
'explication_du_systeme_par_virement' => 'Voici les informations n&eacute;cessaires pour effectuer le virement afin de payer votre commande. Une fois le paiement re&ccedil;u sur notre compte, nous vous en informerons par e-mail et passerons &agrave; l\'&eacute;tape d\'envoi de la commande.',
'etat_des_stocks_du_produit' => 'Stocks',

// F
'fax_du_depot' => 'Fax',
'fichier_de_definition' => 'Fichier de d&eacute;finition',
'fichier_introuvable' => 'Le fichier est introuvable',
'fichier_pas_une_image' => 'Le fichier n\'est pas une image',
'fin' => 'Validation commande',

// G
'gerer_echoppe' => 'Echoppe',
'gerer_les_depots' => 'G&eacute;rer les d&eacute;p&ocirc;ts',
'gerer_les_paniers' => 'G&eacute;rer les paniers',
'gerer_les_prestataire_paiement' => 'G&eacute;rer les prestataires de paiement',
'gramme_racourcci' => 'gr.',

// H
'hauteur' => 'Hauteur',

// I
'inconnu' => 'inconnu',
'info_depot' => '<strong>Gestion des d&eacute;p&ocirc;ts</strong><br />Vous pouvez r&eacute;partir votre stock de produits en plusieurs d&eacute;p&ocirc;ts. Il en faut au minimum 1.',
'info_paniers' => '<strong>G&eacute;rez les paniers des visiteurs</strong><br />Chaque cr&eacute;ation de panier dans la partie publique du site est visible ici et est class&eacute; selon son statut.',
'info_prestataire_paiement' => '<strong>G&eacute;rez les prestataires de paiement</strong><br />Attention, cette partie n&eacute;cessite la cr&eacute;ation de mod&egrave;les de paiement sur base des recommandations des prestataires (PayPal, ...).',
'ignorer_date_de_retrait_de_vente' => 'Ignorer la date de retrait de la vente',
'ignorer_date_mise_en_vente' => 'Ignorer la date de mise en vente',
'impossible_copier_dans'=>'Impossible de copier le fichier dans ',
'impossible_d_effectuer_le_paiement_echoppe_n_est_pas_completement_configure' => 'Impossible de continuer le paiement, Erreur syst&egrave;me.',
'impossible_d_effectuer_le_paiement_vos_infos_de_livraison_sont_manquantes_ou_incomplete_cliquez_ici_pour_les_completer' => 'Impossible d\'effectuer le paiement. Vos informations de livraison sont maquantes ou incompl&egrave;tes. <a href="@url_profile@" >Cliquez ici</a> pour les compl&eacute;ter.',
'informations_client'=>'Informations du client',
'information_de_l_option' => 'informations sur l\'option', 
'information_technique_du_produit'=>'Informations techniques',
'info_perso'=>'Informations personnelles',
'info_pass'=>'Informations de connexion',
'info_livraison'=>'Informations de livraison',
'info_pro'=>'Informations professionnelles',

//J
'jours' => 'jours',
'0_pour_ne_rien_effacer' => 'Mettre 0 pour ne rien &eacute;ffacer',


// L
'langue_editee' => '&Eacute;dition de la langue', // A verifier car en doublons (mattheoh)
'largeur' => 'Largeur',
'le_panier' => 'Le panier',
'le_panier_est_vide' => '<strong>Votre panier est vide !</strong>',
'les_categories' => 'Les cat&eacute;gories',
'les_depots' => 'Les d&eacute;p&ocirc;ts',
'les_options_du_produit' => 'Les options de ce produit',
'les_produits' => 'Les produits',
'les_produits_de_cette_categories' => 'Les produits de cette cat&eacute;gorie',
'les_sous_categories' => 'Les sous-cat&eacute;gories',
'logo_de_la_categorie' => 'Logo de la cat&eacute;gorie',
'logo_du_produit' => 'Logo du produit',
'longeur' => 'Longueur',

// M
'mail_recap_debut' => 'Bonjour,

Voici le r�capitulatif de votre commande sur ',
'mail_recap_fin' => 'D�s la confirmation du paiement re�ue, nous traiterons votre commande.

Merci de votre confiance,

L\'�quipe de ',
'mes_paniers' => 'Mes paniers',
'mes_coordonnes' => 'Mes coordonn&eacute;es',
'mettre_a_jour' => 'Mettre &agrave; jour',
'mettre_le_panier_a_jour' => 'Actualiser le panier',
'mon_compte_client' => 'Mon compte client',
'mon_panier_avant_validation' => 'Mon panier avant validation',
'montant_total_du_panier' => 'Montant total du panier',
'modification_du_panier_non_permise' => 'Modification du panier non permise !',
'modification_du_panier_ok' => 'Panier modifi&eacute; avec succ&egrave;s !', 
'modifier_mon_panier' => 'Modifier mon panier', 
'modifier' => 'Modifer',

// N
'ne_le' => 'N&eacute;(e) le',
'nom_de_la_nouvelle_option' => 'Nom de la nouvelle option',
'nom_du_beneficiaire' => 'Nom du b&eacute;n&eacute;ficiaire',
'numero_de_compte_du_beneficiaire' => 'Num&eacute;ro de compte du b&eacute;n&eacute;ficiaire',
'numero_du_depot' => 'Num&eacute;ro',
'numero_tva_livraison' => 'Num&eacute;ro de TVA',
'nombre_de_chiffre_apres_la_virgule' => 'Nombre maximum de chiffres apr&egrave;s la virgule',
'nouveau_depot' => 'Nouveau d&eacute;p&ocirc;t',
'nouveau_prestataire' => 'Nouveau prestataire',
'nouveau_produit' => 'Cr&eacute;er un produit',
'nouvelle_categorie' => 'Nouvelle cat&eacute;gorie',
'nouveau_client' => 'Nouveau client ?',
'nouveau_client_details' => 'Remplissez le formulaire ci-dessous. Les champs marqu&eacute;s d\'une * sont obligatoires.<br/>Une fois le formulaire compl&eacute;t&eacute; et envoy&eacute;, vous recevrez un email confirmant votre inscription et contenant votre mot de passe.',
'numero_tva' => 'N&deg; de TVA',

//O
'options_du_produit' => 'Options',

// P
'passer_commande' => 'Passer ma commande',
'paiement' => 'Paiement',
'paiement_du_panier' => 'Paiement de mon panier',
'panier' => 'Panier',
'panier_du_client' => 'Panier du client',
'paniers_en_attente' => 'Paniers temporaires',
'paniers_paye' => 'Paniers pay&eacute;s',
'paniers_reserve' => 'Paniers r&eacute;serv&eacute;s',
'paniers_traite' => 'Paniers trait&eacute;s',
'paniers_valide' => 'Paniers valid&eacute;s',
'paniers_envoye' => 'Paniers envoy&eacute;s',
'paniers_perso' => 'Votre panier',
'paniers_perso_details' => 'Voici les produits que vous avez s&eacute;lectionn&eacute;s. Vous pouvez modifier le contenu de votre panier et ensuite le valider pour passer commande.',
'par_defaut' => 'Par d&eacute;faut',
'payer_le_panier' => 'Payer le panier',
'produit_dans_ce_panier' => 'Nombre de produit(s) dans ce panier',
'pas_d_options_pour_le_produit' => 'Pas d\'option pour ce produit',
'pas_de_depot_ici' => 'Pas de d&eacute;p&ocirc;t ici',
'pas_de_categorie_ici' => 'Pas de cat&eacute;gorie ici',
'pas_de_num_de_ref' => 'Aucune',
'pas_de_produit_ici' => 'Pas de produit &agrave; cette adresse.',
'pas_de_titre' => 'Pas de titre',
'pas_encore_de_categorie' => 'Pas encore de cat&eacute;gorie',
'pas_encore_de_depots' => 'Il n\'y a pas encore de d&eacute;p&ocirc;t',
'pas_encore_cree' => 'N\'existe pas encore',
'pas_encore_modifie' => 'Jamais modifi&eacute;',
'pays_du_beneficiaire' => 'Pays du b&eacute;n&eacute;ficiaire',
'pays_du_depot' => 'Pays',
'prenom_du_beneficiaire' => 'Pr&eacute;nom du b&eacute;n&eacute;ficiaire',
'prepa' => 'en pr&eacute;paration',
'prix_base' => 'Prix de base HTVA',
'prix_ttc' => 'Prix TTC',
'produit_non_disponible' => 'Produit non disponible',
'propose' => 'propos&eacute;',
'pour_vous_inscrire_cliquez_ici' => 'Pour vous inscrire, cliquez ici.',
'poids' => 'Poids',
'publie' => 'publi&eacute;',
'poubelle' => '&agrave; la poubelle',
'ps' => 'Remarque',
'payer_commande' => 'Paiement de  votre commande',
'panier_encours'=> 'Votre panier est en cours de paiement. Vous ne pouvez plus ajouter de produits &agrave; ce stade ',

// Q
'quantite_minimum' => 'Quantit&eacute; minimum',
'quantite' => 'Quantit&eacute;',

// R
'reference_interne' => 'Num&eacute;ro de r&eacute;f&eacute;rence',
'remettre_a_zero' => 'Remettre &agrave; z&eacute;ro',
'retour' => 'Retour',
'retour_a_la_categorie' => 'Retour &agrave; la cat&eacute;gorie',
'retour_au_panier' => 'Retour au panier',
'retour_au_produit' => 'Retour au produit',
'retour_au_sommaire' => 'Retour &agrave; l\'accueil',
'recapitulatif' => 'Voici un r&eacute;capitulatif de votre commande avant paiement :',

// S
'separer_par_des_v_pour_en_mettre_plusieurs' => 'S&eacute;parez chaque email par des virgules \',\' pour en indiquer plusieurs',
'si_vous_avez_deja_un_compte_vous_pouvez_vous_connecter_ci_dessous' => 'Si vous avez d&eacute;j&agrave; un compte, vous pouvez vous connecter ci-dessous.',
'squelette_du_panier' => 'Squelette du panier',
'statut_du_panier' => 'Statut du panier',
'stock_dans_le_depot' => 'Stock dans le d&eacute;p&ocirc;t',
'stock_total' => 'Stock TOTAL',
'selectionner' => 'S&eacute;lectionner',
'selectionner_details' => 'Veuillez s&eacute;lectionner un mode de paiement dans la liste ci-dessous :',
'secteur_d_activite' => 'Secteur d\'activit&eacute;',
'statut_temporaire' => 'temporaire',
'statut_reserve' => 'r&eacute;serv&eacute;',
'statut_valide' => 'valid&eacute;',
'statut_paye' => 'pay&eacute;',
'statut_traite' => 'trait&eacute;',
'statut_envoye' => 'envoy&eacute;',
'statistiques_du_produit' => 'Statistiques',
'statut_du_prestataire' => 'Statut',

// T
'taux_de_tva_par_defaut' => 'Taux de T.V.A. par d&eacute;faut',
'texte'=>'Texte :',
'telecharger_un_nouveau_logo' => 'T&eacute;l&eacute;charger un nouveau logo',
'telephone_du_depot' => 'T&eacute;l&eacute;phone',
'titre_categorie' => 'Titre de la cat&eacute;gorie :',
'titre_ou_texte_de_l_option' => 'Titre/Texte de l\'option',
'information_de_l_option' => 'informations dur l\'option',
'titre_produit' => 'Titre du produit :',
'titre_du_depot' => 'Titre du d&eacute;p&ocirc;t',
'titre_du_prestataire' => 'Titre',
'tva' => 'TVA',
'total' => 'Total',

// U
'unites' => 'Unit&eacute;(s)',
'unite_dans_le_depot' => 'unit&eacute;(s) dans le d&eacute;p&ocirc;t de',
'utiliser_date_mise_et_retrait_en_ligne' => 'Utiliser les dates de mise en et hors ligne',

// V
'valider_le_panier' => 'Valider le panier',
'valider_les_modifications' => 'Valider les modifications',
'valider_mon_panier' => 'Valider mon panier &raquo;',
'version_du_prestataire' => 'Vesrion du prestataire',
'Ville_du_beneficiaire' => 'Code postal du b&eacute;n&eacute;ficiaire',
'ville_du_depot' => 'Ville',
'visualisation_d_un_depot' => 'Visualisation d\'un d&eacute;p&ocirc;t',
'visualisation_d_un_panier' => 'Visualisation d\'un panier',
'visualisation_d_un_prestataire_de_paiement' => 'Visualisation d\'un prestataire de paiement',
'visualisation_d_un_produit' => 'Visualisation de',
'visualisation_des_paniers' => 'Visualisation des paniers',
'vos_contacts_livraison' => 'Vos contacts de livraison', 
'votre_adresse_livraison' => 'Votre adresse de livraison', 
'votre_saisie_contient_des_erreurs' => 'Votre saisie contient des erreurs !',
'vous_n_avez_encore_aucun_panier_chez_nous' => 'Vous n\'avez encore aucun panier chez nous.',
'vousavez' => 'Vous avez',
// X
'xxx' => 'xxx'

);


?>
