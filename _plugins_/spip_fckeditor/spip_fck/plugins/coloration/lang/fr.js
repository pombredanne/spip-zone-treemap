/*
 * Fichier de langage pour le plugin Geshi. Frank SAURET
 */

FCKLang.ColorationTitre = 'Coloration syntaxique.';
FCKLang.ColorationBulle = 'Insérer un code pour le colorer.';
FCKLang.ColorationInserer = 'Collez votre code source ici :';
FCKLang.ColorationCodeSource = 'Code source.';
FCKLang.ColorationLangage = 'Langage : ';
FCKLang.ColorationNumerodeLigne = ' Afficher les numéros de ligne.';
FCKLang.ColorationPatience = 'Patientez, Geshi bosse pour vous...';
FCKLang.ColorationTabulation = 'Taille de la tabulation : '; 
FCKLang.ColorationPremiernumero = 'Premier numéro de ligne : '; 
FCKLang.ColorationTxtAdresse = 'Le site officiel de Geshi.'; 
FCKLang.ColorationTxtLicence = 'Geshi est sous licence GPL.';
FCKLang.ColorationNumeroNormaux = 'Normaux'; 
FCKLang.ColorationNumeroColore = 'Lignes colorées'; 
FCKLang.ColorationPasDeNumero = 'Pas de numéro';  
FCKLang.ColorationInterLigne = 'Interligne :'; 
