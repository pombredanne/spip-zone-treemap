<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ajaxform?lang_cible=ast
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_fichier_recu' => 'Tienes de seleicionar un ficheru.',

	// C
	'changer_document' => 'Camudar el documentu',
	'confirmer_suppression' => 'El desaniciu ta fechu.',

	// D
	'document_ajoute' => 'S\'amestó el documentu.',

	// E
	'erreur_ajout_document' => 'Hebo un error al amestar el documentu.',

	// L
	'logo' => 'Logo',
	'logo_maj' => 'El logo s\'anovó.',
	'logo_survol' => 'Logo de pasu del mur',

	// M
	'mauvaise_reception' => '¡Ui! Error na receición de ficheru. Vuelvi a tentalo...'
);

?>
