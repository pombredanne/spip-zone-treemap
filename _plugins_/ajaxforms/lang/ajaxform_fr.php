<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/ajaxforms/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_fichier_recu' => 'Vous devez sélectionner un fichier.',

	// C
	'changer_document' => 'Changer le document',
	'confirmer_suppression' => 'La suppression a été effectuée.',

	// D
	'document_ajoute' => 'Le document a été ajouté.',

	// E
	'erreur_ajout_document' => 'Une erreur est survenue lors de l\'ajout du document.',

	// L
	'logo' => 'Logo',
	'logo_maj' => 'Le logo a été mis à jour.',
	'logo_survol' => 'Logo survolé',

	// M
	'mauvaise_reception' => 'Oups ! Erreur dans la réception du fichier. Réessayez...'
);

?>
