<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ajaxform?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_fichier_recu' => 'بايد يك پرونده انتخاب كنيد.',

	// C
	'changer_document' => 'تغيير سند',
	'confirmer_suppression' => 'حذف كامل شده',

	// D
	'document_ajoute' => 'سند افزوده شده',

	// E
	'erreur_ajout_document' => 'هنگام افزودن سند يك خطا رخ داده.',

	// L
	'logo' => 'لوگو',
	'logo_maj' => 'لوگو روزآمد شده.',
	'logo_survol' => 'لوگو پرپرزن',

	// M
	'mauvaise_reception' => 'خطا در دريافت فايل. دوباره لطفاً...'
);

?>
