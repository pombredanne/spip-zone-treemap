<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ajaxform?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_fichier_recu' => 'You must select a file.',

	// C
	'changer_document' => 'Change the document',
	'confirmer_suppression' => 'The deletion has been completed.',

	// D
	'document_ajoute' => 'The document has been added.',

	// E
	'erreur_ajout_document' => 'An error has occurred while adding the document.',

	// L
	'logo' => 'Logo',
	'logo_maj' => 'The logo has been updated.',
	'logo_survol' => 'Hover logo',

	// M
	'mauvaise_reception' => 'Oops! An error occurred when receiving the file. Please try again ...'
);

?>
