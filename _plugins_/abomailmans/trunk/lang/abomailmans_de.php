<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/abomailmans?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'abomailman' => 'Liste de diffusion ou discussions', # NEW
	'abomailmans' => 'Listes de diffusion ou discussions', # NEW
	'abonne' => 'Ich abonniere',
	'activation' => 'Activation', # NEW
	'active' => 'Aktiv',
	'aucune_langue' => 'Keine ANgabn',

	// B
	'bouton_listes_diffusion' => 'Mailinglisten',
	'btn_abonnement' => 'Abonnieren',
	'btn_desabonnement' => 'Abbestellen',

	// C
	'choisir_liste' => 'Sie müssen eine Liste wählen.',
	'contenu_date' => 'Inhalt ab diesem Datum', # MODIF
	'creation_droits_insuffisants' => 'Sie haben kein Recht dazu ...',

	// D
	'desactive' => 'Abgeschaltet',
	'destinataire' => 'Destinataire', # NEW

	// E
	'email' => 'E-Mail',
	'email_abonnement' => 'Ihre E-Mail Adresse',
	'email_envoye' => 'Die E-Mail wurde an die Mailingliste verschickt: @liste@.',
	'email_oublie' => 'Sie haben ihre E-Mail Adresse vergessen',
	'emailliste_abomailman' => 'E-Mail Adresse der Liste',
	'emailliste_abosympa' => 'E-Mail Adresse des Sympa-Administrators',
	'emailliste_subscribe' => 'Subscribe',
	'emailliste_unsubscribe' => 'Unsubscribe', # MODIF
	'emails_a_renseigner' => 'Emails à renseigner', # NEW
	'envoi_apercu' => 'Vorschau',
	'envoi_confirmer' => 'Bestätigen und absenden',
	'envoi_liste_parametres' => 'Parameterliste',
	'envoi_parametres' => 'Paramétres', # NEW
	'envoi_regulier' => 'Automatics mail',
	'envoi_regulier_info' => 'Keep empty if you do not want to send automatically',
	'envoi_regulier_tous_les' => 'Send each',
	'envoi_vers' => 'send to',
	'envoyer_courier' => 'Mail senden',
	'envoyer_courier_liste' => 'Mail an diese Mailingliste senden:',
	'envoyer_mailmans' => 'Modell und Inhalt auswählen', # MODIF
	'erreur_email_liste_oublie' => 'Die E-Mail Adresse ist obligatorisch.',
	'erreur_nobot' => 'Votre inscription n\' a pu être effectué à cause d\'un problème technique', # NEW
	'explication_email_subscribe' => 'Email for subscribe, something like <code>suffix+subscribe@exemple.org</code>',
	'explication_email_sympa' => 'Wenn dieses Feld ausgefüllt ist, wird von einer Sympa-Liste ausgegangen,
							anderenfalls von einer Mailman-Liste.',
	'explication_email_unsubscribe' => 'Email for unsubscribe',

	// I
	'icone_ajouter_liste' => 'Neue Liste hinzufügen',
	'icone_envoyer_mail_liste' => 'Mail aus dem Inhalt dieser Website generieren und an die Buchstaben senden',
	'icone_modifier_abomailman' => 'Modifier la liste', # NEW
	'icone_retour_abomailman' => 'Retour à la liste', # NEW
	'info_abomailman_aucun' => 'Aucune liste', # NEW
	'info_abomailmans_1' => 'Une liste', # NEW
	'info_abomailmans_nb' => '@nb@ listes', # NEW
	'info_sisympa' => '[Obligatorisch bei Sympa-Listen]',
	'insciption_listes_legende' => 'Mailing-Listen Abonnements', # MODIF
	'inscription_lettres_legende' => 'Abonnement bei Newslettern<br />und Diskussionslisten',

	// J
	'je_m_abonne' => 'Markieren um Abonnement zu bestellen oder zu kündigen.',

	// L
	'label_etat_liste' => 'Status der Liste',
	'label_type_abo' => 'Type', # NEW
	'label_type_ml' => 'Liste de discussion', # NEW
	'label_type_news' => 'Liste de diffusion', # NEW
	'langue_liste' => 'Sprache der Liste',
	'legende_inscription_ml' => 'Inscription à la liste de discussion', # NEW
	'legende_inscription_news' => 'Inscription à la liste de diffusion', # NEW
	'legende_inscriptions_ml' => 'Inscription aux listes de discussion', # NEW
	'legende_inscriptions_news' => 'Inscription aux listes de diffusion', # NEW
	'les_listes_mailmans' => 'Bekannte Mailman-Listen',
	'lire_article' => 'Artikel lesen',
	'liste_creee' => 'Die Liste Nummer @id@ (@titre@) wurde angelegt.',
	'liste_non_existante' => 'Die Liste existiert nich oder wurde entfernt.',
	'liste_oublie' => 'Sie habe vergessen, eine Liste auszuwählen!',
	'liste_supprimee' => 'Die Liste Nummer @id@ (@titre@) wurde gelöscht.',
	'liste_updatee' => 'Die Liste Nummer  @id@ (@titre@) wurde aktualisiert.',

	// M
	'message' => 'Einleitungstext vor den Inhalten ihrer Website',
	'message_confirm_suite' => 'Um ihren Auftrag zu bestätigen beantworten sie bitte die Bestätigungsmail, die sie erhalten werden..',
	'message_confirmation_a' => 'Aboanfragen an folgende Listen wurden gesendet:',
	'message_confirmation_d' => 'Stornierungsaufträge an folgende Listen wurden gesendet. ',
	'message_confirmation_unique_a' => 'Eine Aboanfragen an folgende Liste wurden gesendet:',
	'message_confirmation_unique_d' => 'Stornierungsauftrag an folgende Liste wurden gesendet. ',
	'mot' => 'Artikel zu diesem Schlagwort auflisten',

	// N
	'nom' => 'Name und Vorname (freiwillige Angabe)',
	'nouveau_abomailman' => 'Nouvelle liste de diffusion', # NEW

	// P
	'pas_template_txt' => 'Il n\'y a pas de version texte pour ce modèle', # NEW
	'periodicite' => ' days.',
	'prenom' => 'Vorname',
	'previsu_html' => 'html', # NEW
	'previsu_txt' => 'texte', # NEW

	// R
	'rubrique' => 'Artikel der Rubrik auflisten',

	// S
	'souhaite_rester' => 'Ich möchte auf dem Laufenden bleiben',
	'sujet' => 'Thema der Mail',
	'sujet_obligatoire' => 'Es muss ein Thema angegeben werden.',
	'suppression_definitive' => 'Suppression définitive !', # NEW
	'supprimer' => 'Löschen',
	'sympa_message_confirmation' => 'Eine Bestätigungsmail wurde an folgende Adresse gesendet: ', # MODIF

	// T
	'template' => 'Modell und Inhalte auswählen',
	'template_defaut' => 'Default template',
	'template_defaut_info' => 'Si le fichier modele_choisi.txt.html existe, la newsletter sera envoyée en mode html + texte. Sinon seule la version html sera expédiée.', # NEW
	'texte_descriptif' => 'Descriptif', # NEW
	'titre_abomailman' => 'Bezeichnung der Liste',
	'titre_liste_obligatoire' => 'Die Liste muss eine Bezeichnung erhalten',
	'toute_liste' => 'ALle Mailinglisten', # MODIF

	// V
	'verifier_formulaire' => 'Überprüfen sie den Inhalt des Formulars.',
	'veut_s_abonner' => 'want to subscribe',
	'veut_se_desabonner' => 'want to unsubscribe',
	'voir_modele_depuis' => 'Voir un exemple du modéle avec', # NEW
	'votre_email' => 'Ihre E-Mail'
);

?>
