<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// $Id: abomailmans_fr.php 26976 2009-03-01 00:33:38Z kent1@arscenic.info $
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'abomailmans' => 'Mailmans',
	'abonne' => 'I subscribe',
	'active' => 'Active',
	'activation' => 'Activation',
	'aucune_langue' => 'None specified',

	// B
	'bouton_listes_diffusion' => 'Mailing lists',
	'btn_abonnement' => 'Subscribe',
	'btn_desabonnement' => 'Unsubscribe',

	// C
	'choisir_liste' => 'You should choose a list.',
	'contenu_date' => 'Content since this date',
	'creation_droits_insuffisants' => 'Your rights are insufficients',

	// D
	'desactive' => 'Disabled',
	'destinataire' => 'Recipient',

	// E
	'email' => 'Email address',
	'emailliste_abomailman' => 'The email address of the list',
	'emailliste_abosympa' => 'The email address of the Sympa admin',
	'emailliste_subscribe' => 'Subscribe',
	'emailliste_unsubscribe' => 'Unsubscribe',
	'emails_a_renseigner' => 'Email addresses to provide',
	'email_abonnement' => 'Your email address',
	'email_envoye' => 'The email has been sent to the list : @liste@.',
	'email_oublie' => 'You forgot your email address',
	'envoi_apercu' => 'Previsualization',
	'envoi_confirmer' => 'Confirm and send',
	'envoi_liste_parametres' => 'List of parameters [Facultatif]',
	'envoi_parametres' => 'Settings',
	'envoi_regulier' => 'Automatics mail',
	'envoi_regulier_info' => 'Leave empty if you do not want to send automatically',
	'envoi_regulier_tous_les' => 'Send each',
	'envoi_vers'=> 'send to',
	'envoyer_courier' => 'Send an email',
	'envoyer_courier_liste'	=> 'Send this content to this list :',
	'envoyer_mailmans' => 'Select the template and his content',
	'erreur_email_liste_oublie' => 'The email address of the list is mandatory',
	'explication_email_sympa' => 'If this field is filled in, the list is considered as a "Sympa" list, if not, as a "Mailman" list.',
	'explication_email_subscribe' => 'Email for subscribe, something like <code>suffix+subscribe@exemple.org</code>',
	'explication_email_unsubscribe' => 'Email for unsubscribe',

	// I
	'icone_ajouter_liste' => 'Add a new list',
	'icone_envoyer_mail_liste' => 'Send an e-mail to the lists from the content of this site',
	'info_sisympa' => '[Mandatory for a Sympa list]',
	'insciption_listes_legende'	=> 'Subscription to the mailing lists',
	'inscription_lettres_legende' => 'Subscription to the newsletters<br />and mailing lists',

	// J
	'je_m_abonne' => 'Check to confirm the subscription or unsubscription.',

	// L
	'label_etat_liste' => 'Status of the list',
	'label_type_abo' => 'Type',
	'label_type_ml' => 'Mailing-list',
	'label_type_news' => 'Newsletter',
	'legende_inscription_ml' => 'Subscription to the mailing list',
	'legende_inscription_news' => 'Subscription to the newsletter',
	'legende_inscriptions_ml' => 'Subscription to the mailing lists',
	'legende_inscriptions_news' => 'Subscription to the newsletters',
	'langue_liste' => 'Language of the list',
	'les_listes_mailmans' => 'The Mailman\'s or Sympa\'s lists filled',
	'lire_article' => 'Read the article',
	'liste_creee' => 'The list number @id@ (@titre@) has been created.',
	'liste_non_existante' => 'The asked list doesn\'t exist or has been deleted',
	'liste_oublie' => 'You forgot to check a list.',
	'liste_supprimee' => 'The list number @id@ (@titre@) has been deleted.',
	'liste_updatee' => 'The list number @id@ (@titre@) has been updated.',

	// M
	'message' => 'Introduction of your email, before the site\'s content',
	'message_confirmation_a' => 'A subscription request to the following lists has been sent :',
	'message_confirmation_unique_a' => 'A subscription request to the following list has been sent :',
	'message_confirmation_d' => 'An unsubscribing request from the lists below has been sent. ',
	'message_confirmation_unique_d' => 'An unsubscribing request from the list below has been sent. ',
	'message_confirm_suite' => 'To validate your request, please reply to the confirmation email that you will receive.',
	'mot' => 'And list the articles linked to the keyword',

	// N
	'nom' => 'Name and first name (optional)',

	// P
	'periodicite' => ' days.',
	'prenom' => 'First name',

	// R
	'rubrique' => 'And list the articles of the section',

	// S
	'souhaite_rester' => 'I wish to keep informed',
	'sujet' => 'Subject of the mail',
	'sujet_obligatoire' => 'The subject is mandatory.',
	'supprimer' => 'Delete',
	'suppression_definitive' => 'Permanent removal !',
	'sympa_message_confirmation' => 'A validation email has been sent to the address : ',

	// T
	'template' => 'Choose a template and his content',
	'template_defaut' => 'Default template',
	'texte_descriptif' =>'Description',
	'titre_abomailman' => 'Title of the list',
	'titre_liste_obligatoire' => 'The title of the list is mandatory',
	'toute_liste' => 'All mailing lists',

	// V
	'veut_s_abonner' => 'want to subscribe',
	'veut_se_desabonner' => 'want to unsubscribe',
	'verifier_formulaire' => 'Please verify the filling of the form.',
	'voir_modele_depuis' =>'See an example of the template with',
	'votre_email' => 'Your email',

);

?>