<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP nommé  genere le NOW()
// langue / language = fr

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'action'=>'Action',
	'actif'=>'Actif',
	'active'=>'Active',	
	'activer'=>'Activer',		
	'ajouter_projet'=>'Ajouter un projet',
	'ajouter_tache'=>'Ajouter une tâche',		
	
	// C
	'chef_projet'=>'Chef du projet',
	
	// D
	'date_debut'=>'Date début',
	'date_fin_estimee'=>'Date fin estimée',	
	'desactiver'=>'Désactiver',
	'duree'=>'Durée',						
	'duree_estimee'=>'Durée estimée',	
	'duree_reelle'=>'Durée réelle',		
	
	// E
	'editer_projet'=>'Editer le projet',		
	'editer_tache'=>'Editer la tâche',			
	'erreur_date'=>'Votre date fin estimée doit être postérieure à celle du début',	
	'explication_heure'=>'En heures',	
			
	//G
	'gestion_projets'=>'Gestion de Projets',	
	
	// I
	'infos_obligatoires'=>'Informations obligatoires',
	
	//M
	'montant_estime'=>'Montant estimée',
	'montant_heure'=>'Montant par heure',
	'montant_reel'=>'Montant réel',		
	
	//N
	'nom'=>'Nom',
	'numero_projet'=>'PROJET NUMÉRO',	
	'numero_tache'=>'TACHE NUMÉRO',				
			
		
	//O
	'options_avancees'=>'Options avancées',		
	
	//P
	'participants'=>'Participants',	
	'participants_projet'=>'Les Participants du projet',			
	'projet'=>'Projet',	
	'projets'=>'Projets',
			
	//R
	'retour'=>'Retour :',
		
	// S
	'start'=>'Start',
	'stop'=>'Stop',	
	
	//T
	'taches_projet'=>'Les tâches tu projet',
	


);
?>