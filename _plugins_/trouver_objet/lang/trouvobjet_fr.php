<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'trouver_articles_avec_mot' => 'Rechercher des articles avec un mot-clef',
	'bouton_chercher' => 'Chercher',
	'bouton_trouver'=>'Trouver',
	'choisir_groupe'=>'Choisissez un groupe de mots',
	'choisir_rubrique'=>'Choisissez une rubrique',
	'trouver_mot_in'=>'Rechercher un mot dans <strong>@titre@</strong>',
	'trouver_article_in'=>'Rechercher un article dans <strong>@titre@</strong>',
	'pas_de_identifiant'=>'Nom inconnu',


);

?>
