<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
'nom' => 'Navigation AJAX',
'trouver_doc' => 'Pour de l\'aide sur la configuration de ce plugin, rendez-vous sur ',
'conf_plugin' => 'Configuration du plugin :',
'pages_titre' => 'Les types de page qui seront charg&eacute;es en ajax :',
'pages_expli' => 'une liste de types de page separ&eacute;s par des espaces. p.ex: "sommaire article rubrique"',
'ajax_divs_titre' => 'Les ids des div &agrave; charger en ajax :',
'ajax_divs_expli' => 'une liste d\'id separ&eacute;s par des espaces. p.ex: "contenu extra"',
'loc_divs_titre' => 'Les ids des div &agrave; recharger en cas de changement de langue :',
'loc_divs_expli' => 'une liste d\'id separ&eacute;s par des espaces. p.ex: "menu navigation"',
'html4' => 'Activer les urls hash pour les vieux navigateurs (exp&eacute;rimental) :',
'use_modern_lib' => 'Utiliser la librairie Modernizr fournie avec le plugin : ',
'use_history_lib' => 'Utiliser la libraire History.js fournie avec le plugin : ',
'auto_replace_divs' => 'Remplacer les divs automatiquement : ',
);

?>