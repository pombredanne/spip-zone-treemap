<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-sjcycle
// Langue: fr
// Date: 04-01-2012 19:07:25
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'sjcycle_description' => 'Diaporama paramétrable pour Spip basé sur le plugin [jQuery Cycle->http://malsup.com/jquery/cycle/]. Le diaporama peut s\'insérer dans le texte de vos articles. Le plugin fournit également des modèles permettant par exemple d\'ajouter un diaporama aléatoire d\'images ou un diaporama des sites syndiqués dans un squelette...',
	'sjcycle_slogan' => 'Un diaporama paramétrable pour Spip',
);
?>