<?php

// FICHIER DE TRADUCTION DES PAGES PRIVEES � actualites ! �
// Pour les caract�res sp�ciaux HTML :
// Attention � �chapper les guillements par backspace : "l'apostrophe" -> "l\'apostrophe"
// Info : pour l'espace insecable : &nbsp;

// Pour la traduction de la partie publique voir local_**.php

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
// A
	'alt_img_logo' => 'Logo du plugin actualit&eacute;!',
	'apercu_chez' => ' chez ',
	'apercu_de' => 'De ',
	'apercu_le' => ' le ',
	'apercu_lien_actualite' => '&Agrave; voir sur&nbsp;',
	'apercu_informations' => 'Plus d\'informations sur&nbsp;',
	'apercu_publie' => 'Publi&eacute;',
// C
	'cfg_descriptif' => '<h4>Configurer le plugin actualit&eacute;s !',
	'cfg_titre' => 'Des actualit&eacute;s !',
	'cfg_soustitre1' => 'Les objets actifs',
	'cfg_soustitre2' => 'Les champs optionnels',
	'cfg_concernant_actualite' => 'Concernant les actualit&eacute;s',
	'cfg_label_type' => 'Type :',
	'cfg_label_actualite' => 'Les actualit&eacute;s :',
	'cfg_zero_objet' => '<b>Aucun objet actualit&eacute; n\'est actuellement activ&eacute;.</b><br />Pour rem&eacute;dier &agrave ce probl&egrave;me, rendez-vous sur la page de <a href="?exec=cfg&cfg=actualites">configuration</a>.',
	'choisir_rubrique_associee' => 'Choisissez la rubrique associ&eacute;e',

// D
	'description_plugin' => 'Ajouter des actualit&eacute;s plut&ocirc;t que des articles !',

// E
	'entree_actualite_publiee' => 'Cette actualit&eacute; doit-elle &ecirc;tre publi&eacute;e ?',
	'etiquette_formulaire_actualite' => 'Modifier l\'actualit&eacute; :',
	'explication_date' => 'La date doit &ecirc;tre au format \'aaaa-mm-jj\'.',

// H
	'html_title' => 'actualites',

// I
	'icone_creer_objet' => 'R&eacute;diger une actualit&eacute;',
	'icone_modifier_actualite' => 'Modifier l\'actualit&eacute;',
	'icone_precedent_actualite' => 'actualit&eacute; pr&eacute;c&eacute;dente',
	'icone_suivant_actualite' => 'actualit&eacute; suivante',
	'info_actualite_publiee' => 'Cette actualit&eacute; doit-elle &ecirc;tre publi&eacute;e ?',
	'info_auteur' => 'Auteur(s) :',
	'info_date' => 'Date&nbsp;:',
	'info_descriptif' => 'Descriptif :',
	'info_editeur' => '&Eacute;diteur :',
	'info_gauche_numero_actualite' => 'ACTUALIT&Eacute; NUM&Eacute;RO',
	'info_langpub' => 'Langue de publication :',
	'info_legend_source' => 'Source',
	'info_legend_statut' => 'Statut',
	'info_lien' => 'Lien : ',
	'info_titre' => 'Titre :',
	'info_vu_libelle_actualite' => 'Actualit&eacute;s',
	'item_actualite_proposee' => 'actualit&eacute; propos&eacute;e',
	'item_actualite_refusee' => 'NON - actualit&eacute; refus&eacute;e',
	'item_actualite_validee' => 'OUI - actualit&eacute; valid&eacute;e',
	'item_actualite_poubelle' => '&agrave; la poubelle',
	'item_mots_cles_association_actualites' => 'aux actualit&eacute;s',

// L
	'liste_actualites' => 'Les actualit&eacute;s',
	'logo' => 'Logo actualit&eacute;',
	'logo_ajouter' => 'T&eacute;l&eacute;charger un nouveau logo :',

// N
	'naviguer_titre' => 'actualit&eacute;s',
	'nouvelle_actualite' => 'Nouvelle actualit&eacute;',

// R
	'raccourcis_actualite' => 'Nouvelle actualit&eacute;',
	// Pour les flux RSS, on preferera indiquer les caracteres speciaux en code ISO plutot
	// que HTML. Sinon, ca plante...
	'rss_apercu_lien_actualite' => '&#192;&#160;voir&#160;sur&#160;',
	'rss_apercu_publie' => 'Publi&#233;',
	'rss_apercu_du' => 'du&#160;',
	'rss_apercu_informations' => 'Plus&#160;d\'informations&#160;sur&#160;',
	'rss_objet_actualite' => 'actualite',

// S
	'spiplistes_info_actualites' => 'info_actualites',

// T
	'titre_enattente_actualites' => 'Les actualit&eacute;s propos&eacute;es',
	'titre_nouvelle_actualite' => 'Nouvelle actualit&eacute;',
	'titre_page_actualites_edit' => '&Eacute;diter une actualit&eacute;',
	'titre_cartouche_accueil_actualites' => 'actualit&eacute;s !',
	'titre_logo' => 'Logo actualit&eacute;',

// Compatibilite plugin Corbeille
	'corbeille_actualites_tous' => "@nb@ actualit&eacute;s dans la corbeille",
	'corbeille_actualites_un' => "1 actualit&eacute; dans la corbeille"
);

?>
