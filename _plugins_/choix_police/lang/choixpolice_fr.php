<?php
// fichier de langue fr pour le plugin choix_police

$GLOBALS['i18n_choixpolice_fr'] = array(
// A
'alignement_vertical' => "Alignement vertical",

// C
'choix_police' => "Choix de la police",
'configuration_parametres_par_defaut' => "Configuration des param&egrave;tres par d&eacute;faut",
'couleur_typo' => "Couleur",

// D
'descriptif_configuration' => "Les param&egrave;tres d&eacute;finis dans cette page sont ceux qui seront appliqu&eacute;s par d&eacute;faut.",

// E
'explication_alignement_vertical' => "Alignement vertical de l’image typo (valeurs possibles : bottom, baseline, middle, top)",
'explication_couleur_typo' => "Couleur du texte (de la forme #EE0000, avec ou sans #)",
'explication_largeur_typo' => "Largeur occup&eacute;e par l’image avant retour à la ligne (sans unit&eacute;)",
'explication_marge_horizontale' => "Marge à droite et à gauche de l’image (avec unit&eacute; : px ou em au choix)",
'explication_marge_verticale' => "Marge au-dessus et dessous de l’image (avec unit&eacute; : px ou em au choix)",
'explication_palette' => "Pour choisir une couleur: cliquez d'abord sur le cercle puis sur le carr&eacute;",
'explication_padding_typo' => "Espacement autour du texte pour les polices qui d&eacute;bordent (sans unit&eacute;)",
'explication_styles_css' => "Ajustements CSS des images g&eacute;n&eacute;r&eacute;es",
'explication_supprimer_accents' => "Pour les polices qui ne g&egrave;rent pas les accents, remplacer les caract&egrave;res accentu&eacute;s",
'explication_taille_typo' => "Taille de la police (sans unit&eacute;)",

// L
'largeur_typo' => "Largeur maximum",

// M
'marge_horizontale' => "Marge horizontale",
'marge_verticale' => "Marge verticale",

// P
'padding_typo' => "Padding",
'parametrage_typo' => "Param&egrave;tres de la police",

// S
'style_css_image_typo' => "Styles suppl&eacute;mentaires des images typo",
'supprimer_accents' => "Supprimer les accents",

// T
'taille_typo' => "Taille",
'titre_choix_police' => "Choix Police",
'titre_descriptif_configuration' => "Param&eacute;trage du plugin Choix Police"


);

?>
