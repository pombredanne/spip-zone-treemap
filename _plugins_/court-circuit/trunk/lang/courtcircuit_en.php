<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/courtcircuit?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aide_en_ligne' => 'Online help',

	// C
	'configurer_courtcircuit' => 'Configure the rules of sections short circuit',
	'courtcircuit' => 'Short circuit',

	// E
	'explication_liens_rubriques' => 'Change the URL of redirected sections directly into the skeletons?',
	'explication_regles' => 'The different rules below are tested in that order. If no rule defines a redirection, then the section will be displayed normally.',
	'explication_restreindre_langue' => 'Si cette option est activée, seuls les articles de la langue active seront pris en compte pour le calcul de la rediretion. Cette option n\'est utile que si vos rubriques contiennent des articles de différentes langues. Ne pas utiliser si votre site est organisé en secteurs de langue ou si vous utilisez des champs multi.', # NEW
	'explication_sousrubrique' => 'Browse the first subsection (sort by title number and date)? Redirection rules will be tested again in this subsection.',
	'explication_variantes_squelettes' => 'Example: skeletons of type rubrique-2.html or rubrique=3.html.',

	// I
	'item_appliquer_redirections' => 'Apply the redirection rules',
	'item_jamais_rediriger' => 'Never redirect',
	'item_ne_pas_rediriger' => 'Do not redirect',
	'item_rediriger_sur_article' => 'Redirect on this article',

	// L
	'label_article_accueil' => 'Home article of the section',
	'label_composition_rubrique' => 'Section with composition',
	'label_exceptions' => 'Exceptions',
	'label_liens' => 'URL of sections',
	'label_liens_rubriques' => 'Act on the #URL_RUBRIQUE tag?',
	'label_plus_recent' => 'The most recent article of the section',
	'label_plus_recent_branche' => 'The most recent article of the branch',
	'label_rang_un' => 'First article (numbered articles)',
	'label_regles' => 'Redirection rules of the sections',
	'label_restreindre_langue' => 'Ne prendre en compte que les articles de la langue ?', # NEW
	'label_sousrubrique' => 'Subsections',
	'label_un_article' => 'The only article of the section',
	'label_variantes_squelettes' => 'Section with alternative skeletons'
);

?>
