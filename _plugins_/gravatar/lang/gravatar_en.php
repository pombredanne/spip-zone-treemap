<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/gravatar?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_descr_gravatar' => 'Setup the Gravatar\'s display',
	'cfg_titre_gravatar' => 'Gravatars',

	// E
	'explication_image_defaut' => 'For those who don\'t have any avatar, use :',

	// G
	'gravatar_info' => 'To show your avatar here, register it first on <a href="http://www.gravatar.com/" rel="external nofollow" class="spip_out">gravatar.com</a> (free and painless).',
	'gravatar_info_forum' => 'To show your avatar with your message, register it first on <a href="http://www.gravatar.com/" rel="external nofollow" class="spip_out">gravatar.com</a> (free et painless) and don\'t forget to indicate your Email addresse here.',

	// L
	'label_image_defaut' => 'Default image',
	'label_image_defaut_404' => 'no image',
	'label_image_defaut_gravatar' => 'the Gravatar\'s logo',
	'label_image_defaut_identicon' => '<i>Identicon</i> (generated from the Email address)',
	'label_image_defaut_monsterid' => '<i>MonsterID</i> (generated from the Email address)',
	'label_image_defaut_png' => '<tt>images/gravatar.png</tt>',
	'label_image_defaut_wavatar' => '<i>Wavatar</i> (generated from the Email address)',
	'label_taille' => 'Gravatar\'s size',

	// T
	'titre_gravatar_auteur' => 'Gravatar'
);

?>
