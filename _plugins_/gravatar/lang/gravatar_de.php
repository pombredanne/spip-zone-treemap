<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/gravatar?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_descr_gravatar' => 'Benutzen Sie das Gravatar-Rendering',
	'cfg_titre_gravatar' => 'Gravatars',

	// E
	'explication_image_defaut' => 'Für alle, die kein Gravatar haben:',

	// G
	'gravatar_info' => 'Um Ihren Avatar hier anzeigen zu lassen, registrieren Sie sich erst hier <a href="http://www.gravatar.com/" rel="external nofollow" class="spip_out">gravatar.com</a> (kostenlos und einfach).',
	'gravatar_info_forum' => 'Um Ihren Avatar hier anzeigen zu lassen, registrieren Sie sich erst hier <a href="http://www.gravatar.com/" rel="external nofollow" hreflang="en" class="spip_out">gravatar.com</a> (kostenlos und einfach). Vergessen Sie nicht, hier Ihre E-Mail-Adresse einzutragen.',

	// L
	'label_image_defaut' => 'Standardbild',
	'label_image_defaut_404' => 'Kein Bild',
	'label_image_defaut_gravatar' => 'Gravatar logo',
	'label_image_defaut_identicon' => '<i>Identicon</i> (erzeugt von der E-Mail-Adresse)',
	'label_image_defaut_monsterid' => '<i>MonsterID</i> (erzeugt von der E-Mail-Adresse)',
	'label_image_defaut_png' => '<tt>images/gravatar.png</tt>',
	'label_image_defaut_wavatar' => '<i>Wavatar</i> (erzeugt von der E-Mail-Adresse)',
	'label_taille' => 'Größe des Gravatars',

	// T
	'titre_gravatar_auteur' => 'Gravatar'
);

?>
