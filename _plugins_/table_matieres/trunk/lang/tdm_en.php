<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tdm?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'automatique' => 'Is the table of contents automatically generated?',
	'automatique_desription' => 'Disable this option if you place yourself
		the <code>#TABLE_MATIERES</code> tag in your skeletons.',

	// C
	'config' => 'Settings',

	// E
	'explication_longueur' => 'Sets the maximum number of characters that can consitute each anchor.',
	'explication_min' => 'Defines the minimum number of subheadings of a text from which a table of contents will be displayed.',
	'explication_separateur' => 'Defines the character that will be inserted between each word to replace spaces.',

	// L
	'longueur' => 'Length:',

	// M
	'min_intertitres' => 'Minimum of subheadings:',

	// R
	'retour_table_matiere' => 'back to the table of contents',

	// S
	'separateur' => 'Separator:',

	// T
	'table_matiere' => 'Table of contents',

	// Z
	'zbug_champ_tdm_hors_boucle_articles' => 'Field #TABLE_MATIERES outside loop ARTICLES'
);

?>
