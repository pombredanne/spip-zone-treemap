<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/table_matieres/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'automatique' => 'La table des matières est-elle générée automatiquement ?',
	'automatique_desription' => 'Désactivez cette option si vous placez vous-même
		la balise <code>#TABLE_MATIERES</code> dans vos squelettes.',

	// C
	'config' => 'Paramètres',

	// E
	'explication_longueur' => 'Définit le nombre de caractères maximum que consituera chaque ancres.',
	'explication_min' => 'Définit le nombre minimal d\'intertitres d\'un texte à partir duquel une table des matières sera affichée.',
	'explication_separateur' => 'Définit le caractère qui s\'intercalera entre chaque mot pour remplacer les espaces.',

	// L
	'longueur' => 'Longueur :',

	// M
	'min_intertitres' => 'Intertitres minimum ',

	// R
	'retour_table_matiere' => 'Retour à la table des matières',

	// S
	'separateur' => 'Séparateur :',

	// T
	'table_matiere' => 'Table des matières',

	// Z
	'zbug_champ_tdm_hors_boucle_articles' => 'Champ #TABLE_MATIERES hors boucles ARTICLES'
);

?>
