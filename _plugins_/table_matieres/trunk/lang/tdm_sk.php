<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tdm?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'automatique' => 'Chcete, aby obsah vytváral automaticky?',
	'automatique_desription' => 'Túto možnosť deaktivujete, ak do šablón dáte 
		tag <code>#TABLE_MATIERES.</code>',

	// C
	'config' => 'Parametre',

	// E
	'explication_longueur' => 'Nastaví maximálny počet znakov pre každú kotvu.',
	'explication_min' => 'Nastaví minimálny počet nadpisov v texte, z ktorých sa vytvorí obsah.',
	'explication_separateur' => 'Určí znak, ktorý sa vloží medzi slová namiesto medzier.',

	// L
	'longueur' => 'Dĺžka:',

	// M
	'min_intertitres' => 'Minimálny počet nadpisov',

	// R
	'retour_table_matiere' => 'Vrátiť sa na obsah',

	// S
	'separateur' => 'Oddeľovač:',

	// T
	'table_matiere' => 'Obsah',

	// Z
	'zbug_champ_tdm_hors_boucle_articles' => 'Pole #TABLE_MATIERES mimo slučiek ARTICLES (=Články)'
);

?>
