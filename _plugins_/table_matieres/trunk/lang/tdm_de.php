<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tdm?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'automatique' => 'Inhaltsverzeichnis automatisch erstellen?',
	'automatique_desription' => 'Bitte deaktivieren, falls sie den
	SPIP-Tag <code>#TABLE_MATIERES</code> direkt in ihre
	SPIP-Skelette einfügen.',

	// C
	'config' => 'Parameter',

	// E
	'explication_longueur' => 'Legt die maximale Zeichenzahl für jeden Anker fest.',
	'explication_min' => 'Legit die Mindestanzahl der Zwischentitel fest, ab der ein Inhaltsverzeichnis eingeblendet wird.',
	'explication_separateur' => 'Legt das Zeichen fest, daß anstelle von Leerzeichen zwischen Worte eigefügt wird.',

	// L
	'longueur' => 'Länge:',

	// M
	'min_intertitres' => 'Mindestanzahl Zwischentitel',

	// R
	'retour_table_matiere' => 'Zurück zum Inhaltsverzeichnis',

	// S
	'separateur' => 'Trennzeichen:',

	// T
	'table_matiere' => 'Inhaltsverzeichnis',

	// Z
	'zbug_champ_tdm_hors_boucle_articles' => 'Feld #TABLE_MATIERES außerhalb der Schleife ARTICLES'
);

?>
