<?php
/* dd
*
* Auteur : phil
*  
* Ce programme est un logiciel libre distribue sous licence GNU/GPL.
*  
**/

	$GLOBALS[$GLOBALS['idx_lang']] = array(
//			'dd' => 'dd',
			'titre_dump_download' => 'T&eacute;l&eacute;charger le dump de sauvegarde',
			'titre_admin_dump_download' => 'T&eacute;l&eacute;charger le dump de sauvegarde',
			'info_gauche_admin_dump_download' => '<strong>Information</strong><br/>Cette page vous permet de t&eacute;l&eacute;charger le fichier dump de votre derni&egrave;re sauvegarde sur votre ordinateur.<br/><br/>Pour cette op&eacute;ration, le fichier est copi&eacute; dans votre r&eacute;pertoire IMG, le fichier est alors accessible depuis le web, il est donc fortement conseill&eacute; de supprimer le fichier apr&egrave;s le t&eacute;l&eacute;chargement',
			'texte_dump_download' => 'T&eacute;l&eacute;chargement',
			'texte_download_dump' => 'T&eacute;l&eacute;charger la sauvegarde sur votre ordinateur',
			'icone_menu_config' => 'Download Dump',
			'texte_supprime_download' => 'Supprimer le fichier apr&egrave;s t&eacute;l&eacute;chargement',
			'texte_download_dump_supprimer' => 'Supprimer le fichier apr&egrave;s t&eacute;l&eacute;chargement',
			'entree_nom_fichier_supprimer' => 'Cliquer sur le fichier pour le supprimer',
			'entree_nom_fichier' => 'Cliquer sur le fichier pour le t&eacute;l&eacute;charger'
			
	);
?>
