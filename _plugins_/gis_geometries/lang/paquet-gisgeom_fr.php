<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-gisgeom
// Langue: fr
// Date: 06-08-2012 18:20:02
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// G
	'gisgeom_description' => 'Ce plugin ajoute la prise en charge des formes géométriques à GIS. Il utilise les fonctions spatiales de MySQL disponibles à partir de la version 4.1.',
	'gisgeom_slogan' => 'Prise en charge formes géométriques dans GIS',
);
?>