<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/crayons/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

'herbier_dans' => 'Dans l\'Herbier de&nbsp;<acronym title="Syst&egrave;me de Publication pour Internet">SPIP</acronym>',
'herbier' => 'L\'Herbier de&nbsp;<acronym title="Syst&egrave;me de Publication pour Internet">SPIP</acronym>'

);

?>
