<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// 2
	'2pts' => '@objet@&nbsp;:',

	// A
	'attente_confirmation' => 'Onay bekliyor',

	// D
	'date_court' => '@jour@/@mois@/@annee@',
	'derniers_connectes' => 'Son ba&#287;lananlar :',

	// E
	'explique_spam' => 'Dikkat : yask bir komut, karakter veya s&ouml;zc&uuml;k kulland&#305;n&#305;z.<br /><br />L&uuml;tfen geri d&ouml;n&uuml;n !',

	// L
	'lutte_spam' => 'SPAM\'a kar&#351;&#305; sava&#351;',

	// N
	'nom_forum' => 'L&uuml;tfen isminizi belirtiniz !',
	'non_confirmes' => 'Onaylanmam&#305;&#351; :',

	// O
	'objet_article' => 'Makale',
	'objet_articles' => 'Makaleler',
	'objet_auteur' => 'Yazar',
	'objet_auteurs' => 'Yazarlar',
	'objet_breve' => 'K&#305;sa haber',
	'objet_breves' => 'K&#305;sa haberler',
	'objet_mot' => 'S&ouml;zc&uuml;k',
	'objet_mots' => 'S&ouml;zc&uuml;kler',
	'objet_petition' => 'Dilek&ccedil;e',
	'objet_petitions' => 'Dilek&ccedil;e',
	'objet_rubrique' => 'Ba&#351;l&#305;k',
	'objet_rubriques' => 'Ba&#351;l&#305;klar',
	'objet_syndic' => 'Site',
	'objet_syndics' => 'Siteler',

	// P
	'page_debut' => '&#304;lk sayfa',
	'page_fin' => 'Son sayfa',
	'page_lien' => 'Sayfa @page@ : @title@',
	'page_precedente' => '&Ouml;nceki sayfa',
	'page_suivante' => 'Sonraki sayfa',
	'plugin_xml' => 'SPIP sitenizin y&ouml;netilmesine geli&#351;tirecek bir &ccedil;ok k&uuml;&ccedil;&uuml;k ve yararl&#305; yeni i&#351;levi tek bir eklentide toplar.

Bu gere&ccedil;lerin her biri kullan&#305;c&#305; taraf&#305;ndan aktive edilebilir ve pasif hale getirilebilir [eklentinin y&ouml;netim sayfas&#305;->./?exec=admin_couteau_suisse] ve baz&#305; de&#287;i&#351;kenleri kontrol edilebilir : {{Configuration}}\'a t&#305;klay&#305;n&#305;z ve {{&#304;svi&ccedil;re &Ccedil;ak&#305;s&#305;}} ba&#351;l&#305;&#287;&#305;n&#305; se&ccedil;iniz.

Mevcut kategoriler &#351;unlard&#305;r : Y&ouml;netim, Tipografik Geli&#351;tirme, Tipografik K&#305;sayollar, Kamusal g&ouml;sterim, Komutlar, filtreler, kriterler.

Bu eklentide favori gere&ccedil;lerinizi bulacaks&#305;n&#305;z : {Numaray&#305; sil}, {URL formatlar&#305;}, {Tipografik t&#305;rnaklar}, {G&uuml;zel i&#351;aretler}, {Makaleleriniz i&ccedil;in i&ccedil;indekiler}, {G&uuml;len suratlar}, {SPAM\'e kar&#351;&#305; sava&#351;}, {G&uuml;zel URLler}, {SPIP ve d&#305;&#351; ba&#287;lar}, {Anonim forum yasaklama}, {Sayfa ve sekmelere ay&#305;rma}, vb.

Eklentinin belgeleme makalelerini inceleyiniz : [spip-contrib.net->http://www.spip-contrib.net/Le-Couteau-Suisse].

Uyumluluk : SPIP 1.91',

	// S
	'smileys_dispos' => 'Sunulan g&uuml;len suratlar (smileys) :',
	'sommaire_page' => '&nbsp;s@page@',
	'sommaire_page_long' => '&nbsp;sayfa&nbsp;@page@',
	'sommaire_titre' => '&Ouml;zet',
	'stats_auteur' => '@icon@ @nom@, &nbsp;@date@',
	'stats_date' => '@jour@/@mois@/@annee@&nbsp;, &nbsp;@h@h@m@',

	// T
	'texte_formatspip' => 'SPIP FORMATINDA OR&#304;J&#304;NAL MET&#304;N',
	'textes_formatspip' => 'SPIP FORMATINDA OR&#304;J&#304;NAL MET&#304;NLER',
	'titre' => '&#304;svi&ccedil;re &Ccedil;ak&#305;s&#305;',
	'tri_auteurs' => 'YAZAR SIRASI',
	'tri_descendre' => 'Bu yazar&#305; alta indir',
	'tri_monter' => 'Bu yazar&#305; yukar&#305; &ccedil;&#305;kart',

	// U
	'urls_propres_erreur' => 'Bu format a&#351;a&#287;&#305;da belirtilen &ouml;zel URL\'leri dikkate almaz.',
	'urls_propres_format' => 'URL\'lerin &#351;u anki format&#305; : {{&laquo;&nbsp;@format@&nbsp;&raquo;}}. [<span>[Configuration->@url@]</span>]',
	'urls_propres_lien' => 'Kamusal eri&#351;im ba&#287;&#305;',
	'urls_propres_objet' => 'Burada veritaban&#305;nda depolanm&#305;&#351; &ouml;zel URL\'ler listelenmi&#351;tir b&ouml;ylece ziyaret&ccedil;iler sitenizde gezinebilir.',
	'urls_propres_titre' => '&Ouml;ZEL URL\'LER',

	// V
	'variable_vide' => '(Bo&#351;)',
	'visiteurs_connectes' => 'Ba&#287;l&#305; ziyaret&ccedil;iler : @nb@'
);

?>
