<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// 2
	'2pts' => '@objet@&nbsp;:',

	// A
	'attente_confirmation' => 'Esperant confirmaci&oacute;',

	// C
	'configurer' => 'Configurar el Ganivet Su&iacute;s',
	'connections' => 'Connexions',

	// D
	'date_court' => '@jour@/@mois@/@annee@',
	'derniers_connectes' => 'Darrers connectats:',

	// E
	'explique_spam' => 'Atenci&oacute;: heu utilitzat etiquestes, car&agrave;cters o paraules prohibides.<br /><br />Torneu enrere!',

	// L
	'lutte_spam' => 'Lluita contra l\'SPAM',

	// N
	'nom_forum' => 'Gr&agrave;cies per especificar el vostre nom!',
	'non_confirmes' => 'No confirmats:',

	// O
	'objet_article' => 'Article',
	'objet_articles' => 'Articles',
	'objet_auteur' => 'Autor',
	'objet_auteurs' => 'Autors',
	'objet_breve' => 'Breu',
	'objet_breves' => 'Breus',
	'objet_mot' => 'Paraula',
	'objet_mots' => 'Paraules',
	'objet_petition' => 'Petici&oacute;',
	'objet_petitions' => 'Peticions',
	'objet_rubrique' => 'Secci&oacute;',
	'objet_rubriques' => 'Seccions',
	'objet_syndic' => 'Lloc',
	'objet_syndics' => 'Llocs',

	// P
	'page_debut' => 'Primera p&agrave;gina',
	'page_fin' => '&Uacute;ltima p&agrave;gina',
	'page_lien' => 'P&agrave;gina @page@ : @title@',
	'page_precedente' => 'P&agrave;gina enrere',
	'page_suivante' => 'P&aacute;gina endavant',
	'plugin_xml' => 'Reuneix en un sol plugin una llista de petites noves funcionalitats i utilitats que milloren la gesti&oacute; del vostre lloc SPIP.

Cadascuna d\'aquestes eines es pot activar o no per l\'usuari a [la p&agrave;gina d\'administraci&oacute; del plugin->./?exec=admin_couteau_suisse] i gestionar un cert nombre de variables: fer un clic a {{Configuraci&oacute;}}, despr&eacute;s escollir la pestanya {{El Ganivet Su&iacute;s}}.

Les categories disponibles s&oacute;n: Administraci&oacute;, Millores tipogr&agrave;fiques, Dreceres tipogr&agrave;fiques, Presentaci&oacute; p&uacute;blica, Etiquetes, filtres, criteris.

Descobriu en aquest plugin les vostres eines utilitats favorites: {Suprimir el n&uacute;mero}, {Format dels URLs}, {Exponents tipogr&agrave;fics}, {Cometes tipogr&agrave;fiques}, {Bonics s&iacute;mbols gr&agrave;fics}, {Lluita contra l\'SPAM}, {Encriptaci&oacute; del correu electr&ograve;nic}, {Belles URLs}, {SPIP i els enlla&ccedil;os... externs}, {Emoticones}, {Un sumari pels vostres articles}, {Tallar en p&agrave;gines i pestanyes}, etc., etc.

No dubteu a consultar els articles de la documentaci&oacute; del plugin publicats a: [spip-contrib.net->http://www.spip-contrib.net/Le-Couteau-Suisse].

Compatibilitat: SPIP v1.91',

	// S
	'smileys_dispos' => 'Cares atractives disponibles:',
	'sommaire_page' => ',&nbsp;p@page@',
	'sommaire_page_long' => ',&nbsp;p&agrave;gina&nbsp;@page@',
	'sommaire_titre' => 'Sumari',
	'stats_auteur' => '@icon@ @nom@, el&nbsp;@date@',
	'stats_date' => '@jour@/@mois@/@annee@&nbsp;a&nbsp;@h@h@m@',

	// T
	'texte_formatspip' => 'TEXT ORIGINAL EN FORMAT SPIP',
	'textes_formatspip' => 'TEXTOS ORIGINALS EN FORMAT SPIP',
	'titre' => 'El Ganivet Su&iacute;s',
	'tri_auteurs' => 'ORDRE DELS AUTORS',
	'tri_descendre' => 'Baixar aquest autor',
	'tri_monter' => 'Pujar aquest autors',

	// U
	'urls_propres_erreur' => 'Aquest format no t&eacute; en compte els URLs propis llistats m&eacute;s avall. ',
	'urls_propres_format' => 'L\'actual format dels URLs &eacute;s: {{&laquo;&nbsp;@format@&nbsp;&raquo;}}. [<span>[Configuraci&oacute;->@url@]</span>]',
	'urls_propres_lien' => 'Enlla&ccedil; p&uacute;blic d\'acc&eacute;s',
	'urls_propres_objet' => 'Aqu&iacute; es llisten els URLs propis emmagatzemats a la base gr&agrave;cies a les que els visitants podran navegar pel vostre lloc. ',
	'urls_propres_titre' => 'URLS PR&Ograve;PIES',

	// V
	'variable_vide' => '(Buit)',
	'visiteurs_connectes' => 'Visitants connectats: @nb@',

	// W
	'webmestres' => 'Webmestres SPIP'
);

?>
