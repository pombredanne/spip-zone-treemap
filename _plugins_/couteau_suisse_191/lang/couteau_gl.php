<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// 2
	'2pts' => '@objet@&nbsp;:',

	// A
	'attente_confirmation' => 'En espera de confirmaci&oacute;n',

	// C
	'configurer' => 'Configurar A Navalla Su&iacute;za',
	'connections' => 'Conexi&oacute;ns',

	// D
	'date_court' => '@jour@/@mois@/@annee@',
	'derniers_connectes' => '&Uacute;ltimos conectados :',

	// E
	'explique_spam' => 'Atenci&oacute;n : vostede utilizou balizas, caracteres ou palabras prohibidas.<br /><br />Volva atr&aacute;s !',

	// L
	'lutte_spam' => 'Loita contra o SPAM',

	// N
	'nom_forum' => 'Grazas por especificar o seu nome !',
	'non_confirmes' => 'Non confirmados :',

	// O
	'objet_article' => 'Artigo',
	'objet_articles' => 'Artigos',
	'objet_auteur' => 'Autor',
	'objet_auteurs' => 'Autores',
	'objet_breve' => 'Breve',
	'objet_breves' => 'Breves',
	'objet_mot' => 'Palabra',
	'objet_mots' => 'Palabras',
	'objet_petition' => 'Pedimento',
	'objet_petitions' => 'Pedimentos',
	'objet_rubrique' => 'Secci&oacute;n',
	'objet_rubriques' => 'Secci&oacute;ns',
	'objet_syndic' => 'Web',
	'objet_syndics' => 'Webs',

	// P
	'page_debut' => 'Primeira p&aacute;xina',
	'page_fin' => 'Derradeira p&aacute;xina',
	'page_lien' => 'P&aacute;xina @page@ : @title@',
	'page_precedente' => 'P&aacute;xina anterior',
	'page_suivante' => 'P&aacute;xina seguinte',
	'plugin_xml' => 'Re&uacute;ne nun s&oacute; m&oacute;dulo unha lista de pequenas funcionalidades novas e &uacute;tiles que melloran a xesti&oacute;n do seu web SPIP.

Cada unha destas ferramentas pode ser activada ou non polo usuario en  [p&aacute;xina de administraci&oacute;n do m&oacute;dulo->./?exec=admin_couteau_suisse] e xestionar un certo n&uacute;mero de variables : Premer en {{Configuraci&oacute;n}}, logo escoller o separador {{A Navalla Su&iacute;za}}.

As categor&iacute;as dispo&ntilde;ibles son : Administraci&oacute;n, Melloras tipogr&aacute;ficas, Atallos tipogr&aacute;ficos, Presentaci&oacute;n p&uacute;blica, Balizas, Filtros, Criterios.

Descubra neste m&oacute;dulos as s&uacute;as ferramentas favoritas : {Suprimir o num&eacute;ro}, {Formato das URL}, {Super&iacute;ndices  tipogr&aacute;ficos}, {V&iacute;rgulas tipogr&aacute;ficas}, {Vi&ntilde;etas fermosas}, {Riso&ntilde;os}, {Mailcrypt}, {SPIP e as ligaz&oacute;ns... externas}, {Ning&uacute;n foro an&oacute;nimo}, {Un sumario para os seus artigos}, {Partir en p&aacute;xinas e separadores}, etc., etc.

Non dubide en consultar os artigos de documentaci&oacute;n do m&oacute;dulo publicados en  : [spip-contrib.net->http://www.spip-contrib.net/Le-Couteau-Suisse].

Compatibilidade : SPIP todas as versi&oacute;ns 1.91',

	// S
	'smileys_dispos' => 'Cari&ntilde;as (emoticon) dispo&ntilde;ibles :',
	'sommaire_page' => ',&nbsp;p@page@',
	'sommaire_page_long' => ',&nbsp;page&nbsp;@page@',
	'sommaire_titre' => 'Sumario',
	'stats_auteur' => '@icon@ @nom@, o&nbsp;@date@',
	'stats_date' => '@jour@/@mois@/@annee@&nbsp;&aacute;s&nbsp;@h@h@m@',

	// T
	'texte_formatspip' => 'TEXTO ORIXINAL EN FORMATO SPIP',
	'textes_formatspip' => 'TEXTOS ORIXINAIS EN FORMATO SPIP',
	'titre' => 'A navalla su&iacute;za',
	'tri_auteurs' => 'ORDE DE AUTORES',
	'tri_descendre' => 'Baixar este autor',
	'tri_monter' => 'Montar este autor',

	// U
	'urls_propres_erreur' => 'Este formato non toma en conta os URL propios listados a seguir.',
	'urls_propres_format' => 'O formato actual dos URL &eacute; : {{&laquo;&nbsp;@format@&nbsp;&raquo;}}. [<span>[Configuraci&oacute;n->@url@]</span>]',
	'urls_propres_lien' => 'Ligaz&oacute;n p&uacute;blica de acceso',
	'urls_propres_objet' => 'Son listado aqu&iacute; os URL propios gardados merc&eacute; aos cales os visitantes poder&aacute;n navegar no seu web.',
	'urls_propres_titre' => 'URL PROPIOS',

	// V
	'variable_vide' => '(Baleiro)',
	'visiteurs_connectes' => 'Visitantes conectados: @nb@',

	// W
	'webmestres' => 'Webmasters SPIP'
);

?>
