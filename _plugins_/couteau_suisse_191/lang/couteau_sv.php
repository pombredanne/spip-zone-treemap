<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// 2
	'2pts' => '<NEW>@objet@&nbsp;:',

	// A
	'attente_confirmation' => 'v&auml;ntar p&aring; bekr&auml;ftelse',

	// D
	'derniers_connectes' => 'Senast inloggad:',

	// E
	'explique_spam' => 'Varning : du har anv&auml;nt f&ouml;rbjudna nyckelord, bokst&auml;ver eller ord. <br /><br />Starta om!',

	// L
	'lutte_spam' => 'Stoppa SPAM',

	// N
	'nom_forum' => 'Skriv ditt namn!',
	'non_confirmes' => 'Obekr&auml;ftad:',

	// O
	'objet_article' => '<NEW>Article',
	'objet_articles' => '<NEW>Articles',
	'objet_auteur' => '<NEW>Auteur',
	'objet_auteurs' => '<NEW>Auteurs',
	'objet_breve' => '<NEW>Br&egrave;ve',
	'objet_breves' => '<NEW>Br&egrave;ves',
	'objet_mot' => '<NEW>Mot',
	'objet_mots' => '<NEW>Mots',
	'objet_rubrique' => '<NEW>Rubrique',
	'objet_rubriques' => '<NEW>Rubriques',
	'objet_syndic' => '<NEW>Site',
	'objet_syndics' => '<NEW>Sites',

	// P
	'page_debut' => 'F&ouml;rsta sidan',
	'page_fin' => 'Sista sidan',
	'page_lien' => 'sidan @page@: @title@',
	'page_precedente' => 'F&ouml;reg&aring;ende sida',
	'page_suivante' => 'N&auml;sta sida',
	'plugin_xml' => '<MODIF>Samlar m&aring;nga sm&aring; anv&auml;ndbara verktyg f&ouml;r att hantera din SPIP-sajt i en enda plugin.

Varje verktyg kan aktiveras av anv&auml;ndaren i [administrationssidan->./?exec=admin_couteau_suisse]. F&ouml;r att st&auml;lla in olika val, klicka p&aring;  {{Konfiguration}} och v&auml;lj fliken f&ouml;r {{Schweizisk arm&eacute;kniv }}.

Tillg&auml;ngliga kategorier &auml;r: Administration, Typografiska f&ouml;rb&auml;ttringar, Typografiska genv&auml;gar, Visa p&aring; den publika sidan, Nyckelord, Filter ..

Find your favourite tools in this plugin: {Superscript}, {Curly inverted commas}, {Beautiful bullets}, {A summary for your articles}, {Smileys}, {Delete the number}, {Fine URLs}, {SPIP and external links}, {No anonymous forums}, {Division in pages and tab}, etc., etc.

See the documentation articles at: [spip-contrib.net->http://www.spip-contrib.net/Le-Couteau-Suisse].

Compatibility: SPIP 1.91',

	// S
	'smileys_dispos' => 'Tillg&auml;ngliga smileys:',
	'sommaire_page' => ',&nbsp;p@page@',
	'sommaire_page_long' => ',&nbsp;page&nbsp;@page@',
	'sommaire_titre' => 'Inneh&aring;ll',
	'stats_auteur' => '@icon@ @nom@, @date@',
	'stats_date' => '@jour@/@mois@/@annee@&nbsp;kl&nbsp;@h@:@m@',

	// T
	'texte_formatspip' => 'ORIGINALTEXT I SPIP-FORMAT',
	'textes_formatspip' => 'ORIGINALTEXTER I SPIP-FORMAT',
	'titre' => 'Schweizisk arm&eacute;kniv',

	// U
	'urls_propres_erreur' => '<NEW>Ce format ne prend pas compte les URLs propres list&eacute;es ci-dessous.',
	'urls_propres_format' => '<NEW>Le format actuel des URLs est : {{&laquo;&nbsp;@format@&nbsp;&raquo;}}. [<span>[Configuration->@url@]</span>]',
	'urls_propres_lien' => '<NEW>Lien public d\'acc&egrave;s',
	'urls_propres_objet' => '<NEW>Sont list&eacute;es ici les URLs propres stock&eacute;es en base gr&acirc;ce auxquelles les visiteurs pourront naviguer sur votre site.',
	'urls_propres_titre' => '<NEW>URLS PROPRES',

	// V
	'variable_vide' => '(Tom)',
	'visiteurs_connectes' => 'Inloggade bes&ouml;kare: @nb@'
);

?>
