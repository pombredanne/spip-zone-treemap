<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// 2
	'2pts' => '@objet@:',

	// A
	'attente_confirmation' => 'Auf Best&auml;tigung warten',

	// C
	'configurer' => 'Schweizer Taschenmesser konfigurieren',
	'connections' => 'Verbindungen',

	// D
	'date_court' => '@jour@/@mois@/@annee@',
	'derniers_connectes' => 'Letzte Verbindungen:',

	// E
	'explique_spam' => 'Achtung: Sie haben reservierte Tags, Zeichen oder Worte verwendet.<br /><br />Bitte kehren Sie zum letzten Schritt zur&uuml;ck!',

	// L
	'lutte_spam' => 'SPAM-Bek&auml;mpfung',

	// N
	'nom_forum' => 'Danke, dass Sie Ihren Namen angegeben haben!',
	'non_confirmes' => 'Nicht best&auml;tigt:',

	// O
	'objet_article' => 'Artikel',
	'objet_articles' => 'Artikel',
	'objet_auteur' => 'Autor',
	'objet_auteurs' => 'Autoren',
	'objet_breve' => 'Meldung',
	'objet_breves' => 'Meldungen',
	'objet_mot' => 'Schlagwort',
	'objet_mots' => 'Schlagworte',
	'objet_petition' => 'Petition',
	'objet_petitions' => 'Petitionen',
	'objet_rubrique' => 'Rubrik',
	'objet_rubriques' => 'Rubriken',
	'objet_syndic' => 'Website',
	'objet_syndics' => 'Websites',

	// P
	'page_debut' => 'Erste Seite',
	'page_fin' => 'Letzte Seite',
	'page_lien' => 'Seite @page@: @title@',
	'page_precedente' => 'Vorige Seite',
	'page_suivante' => 'N&auml;chste Seite',
	'plugin_xml' => 'Zusammenfassung einiger Zusatzfunktionen f&uuml;r eine bessere Verwaltung Ihrer SPIP Website.

Jedes dieser Werkzeuge kann vom Administrator auf der |Seite zur Verwaltung des Plugins->./?exec=admin_couteau_suisse] aktiviert oder abgeschaltet und konfiguriert werden: Klicken Sie auf {{Konfiguration}}, und w&auml;hlen Sie dann {{Schweizer Taschenmesser}}.

Sie k&ouml;nnen folgende Einstellungen vornehmen: Verwaltung, typografische Verbesserungen, typografische K&uuml;rzel, Anzeigeoptionen der Website, SPIP-Tags, Filter, Kriterien.

W&auml;hlen Sie die f&uuml;r Sie wichtigsten Funktionen: {Ziffern entfernen}, {Typ der URLs}, {typografische Hochstellungen}, {typografische Anf&uuml;hrungszeichen}, {sch&ouml;ne Listen-Punkte}, {SPAM Bek&auml;mpfung}, {MailCrypt}, {&auml;sthetische URLs},  {SPIP und externe Links}, {Smileys}, {Inhaltsverzeichnis f&uuml;r Artikel},  {Aufteilung in Seiten und Reiter}, etc. pp.

Bitte lesen Sie auch die Artikel auf : [spip-contrib.net->http://www.spip-contrib.net/Le-Couteau-Suisse].

Kompatibel mit: SPIP 1.91',

	// S
	'smileys_dispos' => 'Verf&uuml;gbare Smileys:',
	'sommaire_page' => ',&nbsp;S@page@',
	'sommaire_page_long' => ',&nbsp;Seite&nbsp;@page@',
	'sommaire_titre' => 'Inhalt',
	'stats_auteur' => '@icon@ @nom@, am&nbsp;@date@',
	'stats_date' => '@jour@.@mois@.@annee@&nbsp;um&nbsp;@h@h@m@',

	// T
	'texte_formatspip' => 'ORIGINALTEXT IM SPIP-FORMAT',
	'textes_formatspip' => 'ORIGINALTEXTE IM SPIP-FORMAT',
	'titre' => 'Schweizer Taschenmesser',
	'tri_auteurs' => 'REIHENFOLGE DER AUTOREN',
	'tri_descendre' => 'Autor nach unten',
	'tri_monter' => 'Autor nach oben',

	// U
	'urls_propres_erreur' => 'Dieses Format ber&uuml;cksichtigt die angepassten URLs weiter unten nicht.',
	'urls_propres_format' => 'Das Format der URLs ist: {{&laquo;&nbsp;@format@&nbsp;&raquo;}}. [<span>[Konfiguration->@url@]</span>]',
	'urls_propres_lien' => '&Ouml;ffentlicher Zugangslink',
	'urls_propres_objet' => 'Mit diesen in der Datenbank gespeicherten URLs k&ouml;nnen die Besucher Ihre Website ansteuern.',
	'urls_propres_titre' => 'ANGEPASSTE URLS',

	// V
	'variable_vide' => '(Leer)',
	'visiteurs_connectes' => 'Angemeldete Besucher: @nb@',

	// W
	'webmestres' => 'SPIP Webmaster'
);

?>
