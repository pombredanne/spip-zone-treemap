<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
'active' => 'Activ&eacute;',
'aucun' => 'Aucun copyright',

// B
'bottom' => 'Bas de l\'image',

// C
'center' => 'Centre',
'cfg_except_admin' => 'Les administrateurs',
'cfg_except_no' => 'Aucune',
'cfg_except_redact' => 'Les r&eacute;dacteurs et administrateurs',
'cfg_logo_album' => 'Logo SPIP-Album',
'cfg_nbre_vignette' => 'Nombre de vignettes par d&eacute;faut',
'cfg_nbre_vignette_label' => 'D&eacute;finit le nombre de photographies affich&eacute;es dans les galeries, diaporamas ou carousel 3D',
'cfg_noclic' => 'Blocage du clic droit de la souris',
'cfg_noclic_except' => 'Blocage non applicable &agrave;',
'cfg_noclic_presentation' => 'Cette option permet de rendre impossible l\'appel au menu contextuel et donc de faire facilement une copie du contenu de votre site',
'cfg_noclic_titre' => 'Blocage du menu contextuel',
'cfg_param_img' => 'Configuration des param&egrave;tres d\'affichage des images',
'cfg_param_img_presentation' => 'Permet de fixer les param&egrave;tres d\'affichage par d&eacute;faut de vos images dans les articles. Vous maintenez ainsi l\'homog&eacute;n&eacute;isation du contenu de vos articles sans que vos r&eacute;dacteurs aient &agrave; se rappeler des diff&eacute;rents param&egrave;tres &agrave; int&eacute;grer.',
'cfg_senstri' => 'Sens du tri par d&eacute;faut',
'cfg_senstri_label' => 'D&eacute;finit le sens du tri des photographies affich&eacute;es dans les galeries, diaporamas ou carousel 3D.<br /><strong>Nota Bene :</strong> Dans les galeries, le tri se fait en fonction de <b>num titre</b> et de la <b>date</b>. Dans les diaporamas et les carousel 3D, le tri se fait sur la <b>date</b> seule',
'cfg_taille' => 'Taille par d&eacute;faut des images',
'cfg_taille_label' => 'D&eacute;finit la taille par d&eacute;faut des images affich&eacute;es ou des zones d\'affichage comme par exemple pour le <strong>carousel 3D</strong>',
'cfg_titre' => 'Configuration du plugin SPIP-Album',
'cfg_watermark' => 'Configuration de l\'option Watermark',
'clic_photo' => 'Cliquer sur la photo pour la visualiser dans sa taille originale.',
'cliquez_ici_pour_telecharger_l_image' => 'Cliquez ici pour t&eacute;l&eacute;charger l\'image',
'close' => 'fermer',
'croissant' => 'Croissant',

// D
'decroissant' => 'D&eacute;croissant',
'desactive' => 'D&eacute;sactiv&eacute;',
'dimension' => 'Dimension',

// E

// F
'fermer_fenetre' => 'Fermer cette fen&ecirc;tre',
'from' => 'sur',

//H
'head_debut' => 'Debut de la partie reservee a SPIP-Album',
'head_fin' => 'Fin de la partie reservee a SPIP-Album',

// I
'image' => 'Copyright sous forme d\'image',

// L
'lance_diapo' => 'Lancer le diaporama',
'left' => 'Gauche',

// M
'melange_puzzle' => 'M&eacute;langer le puzzle',

// N
'no' => 'Non',
'nojava' => 'Votre navigateur n\'est pas compatible Java !',

// O

// P
'photos' => 'Photos',
'publie_le' => 'Publi&eacute; le',
'precedent' => 'Pr&eacute;c&eacute;dent',

// R
'resoudre_puzzle' => 'R&eacute;soudre le puzzle',
'right' => 'Droite',

// S
'Showing_image' => 'Image num&eacute;ro',
'suivant' => 'Suivant',

// T
'taille' => 'Taille',
'text' => 'Copyright sous forme de texte',
'top' => 'Haut de l\'image',

// U

// V

// W
'watermark_alignh' => 'Alignement horizontal',
'watermark_alignv' => 'Alignement vertical',
'watermark_color' => 'Couleur du texte<br />(<i>En Hexad&eacute;cimal</i>)',
'watermark_image' => 'Fichier image de copyright',
'watermark_font' => 'Taille de police du texte',
'watermark_margin' => 'Excentrage &agrave; partir du bord',
'watermark_opacity' => 'Opacit&eacute; de l\'image copyright',
'watermark_shadow' => 'Ombrage du texte',
'watermark_text' => 'Texte utilis&eacute; pour le copyright',
'watermark_type' => 'Type de marquage de copyright',
'watermark_presentation' => 'L\'option <strong>WaterMark</strong> permet d\'int&eacute;grer un texte ou une image de copyright au sein d\'une image publi&eacute;e, afin d\'en &eacute;viter le vol',

// Y
'yes' => 'Oui',

// Z
'z' => ''

);

?>