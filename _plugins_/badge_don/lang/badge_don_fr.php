<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'label_url_don' => 'Adresse de la page de dons',
	'titre_badge_don' => 'Badge pour dons',
	'titre_lien' => 'Soutenir par un don'
	
);

?>
