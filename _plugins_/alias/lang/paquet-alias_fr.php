<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-alias
// Langue: fr
// Date: 11-03-2012 15:32:42
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
	'alias_description' => 'Il est assez fréquent de vouloir afficher une contenu identique en plusieurs endroits d’une arborescence. Même si ce genre de pratique n’est pas souhaitable, et si elle doit toujours nous amener à nous poser la question de la pertinence de notre rubriquage, elle n’en demeure pas moins nécessaire dans certains cas. Ce plugin permet donc de créer rapidement des Alias d’article, pour afficher un contenu identique en plusieurs endroits d’une arborescence.',
	'alias_nom' => 'Alias Articles',
	'alias_slogan' => 'Creer des Alias d\'articles',
);
?>