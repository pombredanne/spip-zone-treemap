<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_newsletter' => 'Ajouter cette infolettre',


	// E
	'explication_html_page_vide' => 'Permet d\'afficher dans le navigateur une version différente de celle envoyée par email (pour des raisons techniques ou éditoriales). Laissez le vide si vous voulez afficher la même version dans l\'email et dans le navigateur.',
	'explication_baked' => 'Édition avancée&nbsp;: vous pouvez modifier directement le HTML et le texte de l\'infolettre. Elle ne sera plus générée automatiquement. Si vous repassez en édition normale vos modifications seront perdues.',
	'explication_baked_publie' => 'Cette infolettre est figée&nbsp;: vous pouvez corriger son HTML ou son texte mais vous ne pouvez plus modifier son éditorial ni la regénérer à partir de son modèle.',

	// I
	'icone_creer_newsletter' => 'Créer une infolettre',
	'icone_modifier_newsletter' => 'Modifier cette infolettre',
	'info_1_newsletter' => 'Une infolettre',
	'info_aucun_newsletter' => 'Aucune infolettre',
	'info_nb_newsletters' => '@nb@ infolettres',
	'info_newsletters_auteur' => 'Les infolettres de cet auteur',
	'info_newsletter_generer' => 'Générer l\'infolettre (Version HTML et version texte)',
	'info_email_not_displaying' => 'Cet email ne s\'affiche pas correctement&nbsp;?',
	'info_email_voir_en_ligne' => 'Voir cette Infolettre dans votre navigateur',
	'info_test_sujet' => 'TEST',
	'info_test_envoye' => 'La newsletter a été envoyée en test à @email@',

	'info_preview_version_html_email' => 'Version HTML email',
	'info_preview_version_texte_email' => 'Version Texte email',
	'info_preview_version_html_page' => 'Version HTML en ligne',
	'info_preview_version_html_page_noiframe' => '(afficher sans iframe)',

	// L
	'label_chapo' => 'Chapeau',
	'label_date_redac' => 'Date de rédaction',
	'label_texte' => 'Texte',
	'label_titre' => 'Titre',
	'label_patron' => 'Modèle',
	'label_html_email' => 'Version HTML',
	'label_texte_email' => 'Version Texte',
	'label_html_page' => 'Version HTML affichée dans le navigateur',
	'label_baked_1' => 'Edition avancée de l\'infolettre',
	'label_masquer_fond' => 'Masquer les modèles d\'infolettre :',
	'label_selection_articles' => 'Selection d\'articles',

	// R
	'retirer_lien_newsletter' => 'Retirer cette infolettre',
	'retirer_tous_liens_newsletters' => 'Retirer toutes les infolettres',

	// T
	'texte_ajouter_newsletter' => 'Ajouter une infolettre',
	'texte_changer_statut_newsletter' => 'Cette infolettre est :',
	'texte_creer_associer_newsletter' => 'Créer et associer une infolettre',
	'titre_langue_newsletter' => 'Langue de cette infolettre',
	'titre_logo_newsletter' => 'Logo de cette infolettre',
	'titre_newsletter' => 'Infolettre',
	'titre_newsletters' => 'Infolettres',
	'titre_newsletters_rubrique' => 'Infolettres de la rubrique',
);

?>