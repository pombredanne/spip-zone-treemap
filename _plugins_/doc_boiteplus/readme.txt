 * Doc boite plus pour spip
 * Licensed under the GPL License:
 * http://www.gnu.org/licenses/gpl.html
 * auteur alm elastick.net
 
Une photo dans le dossier demo de ce plugin permet d'avoir un aper�u du rendu final,

Le plugin Doc boite plus permet d'ajouter une boite de mots clefs aux documents lors de l'affichage du portfolio
sur la page /ecrire/?exec=articles&id_article=x

Vous devez avoir activ� le plugin "des mots clefs partout" qui creera les tables necessaires pour g�rer les mots clefs sur les documents
voir http://www.spip-contrib.net/Des-mots-clefs-partout

Ensuite, vous devez veiller � ne pas mettre trop de groupes de mots clefs sur les documents
Pour ma part je l'utilise pour 2 groupes de mots departement et ville qui contiennent respectivement 5 et 1200 mots
Dans le cas ou vous avez beaucoup de mots dans un groupe (comme ici pour ville) 
n'activez pas l'option 'choisir un seul mot dans ce groupe' car la liste serait trop lourde � l'affichage (TODO)

N'hesitez pas � faire des remarques sur la zone spip
Des galeries photos (walma?) modulables suivant le mot clef devraient suivre pour montrer la pertinence de ce plugin ;D

