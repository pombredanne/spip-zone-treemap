<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-numero
// Langue: fr
// Date: 25-04-2012 12:25:32
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// N
	'numero_description' => 'Ce plugin permet d\'un clic de numéroter/re-numéroter/dé-numéroter tous les articles ou sous-rubriques d\'une rubrique.',
	'numero_slogan' => 'Gérer facilement la numérotation des articles et rubriques',
);
?>
