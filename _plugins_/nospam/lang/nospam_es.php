<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/nospam?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_attributs_html_interdits' => 'Il n\'est pas permis d\'utiliser les attributs html <tt>class</tt> ou <tt>style</tt>', # NEW
	'erreur_jeton' => 'No se puede tener en cuenta su mensaje. ¡Gracias a presentar de nuevo!',
	'erreur_spam' => 'No se puede tener en cuenta su mensaje!',
	'erreur_spam_doublon' => 'Un mensaje idéntico ya existe!',
	'erreur_spam_ip' => 'También muchos comentarios en detrimento de la calidad!',
	'erreur_url_deja_spammee' => 'Ce message contient des liens suspects qui ressemblent à du SPAM. Merci de les retirer.', # NEW

	// F
	'forum_saisie_texte_info' => 'Pour la mise en forme de votre message, ce formulaire n\'accepte que les raccourcis SPIP <code>[-&gt;url] {{gras}} {italique} &lt;quote&gt; &lt;code&gt;</code> &lt;cadre&gt;</cadre> et le code HTML <code>&lt;q&gt; &lt;del&gt; &lt;ins&gt;</code>. Pour créer des paragraphes, laissez simplement des lignes vides.' # NEW
);

?>
