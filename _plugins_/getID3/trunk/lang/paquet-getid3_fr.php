<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/getID3/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'getid3_description' => 'Lire et écrire les tags audio sur les documents de type audio
_ [->http://getid3.sourceforge.net/]',
	'getid3_slogan' => 'Lire et écrire les tags audio'
);

?>
