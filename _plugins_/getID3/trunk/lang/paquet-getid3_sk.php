<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/paquet-getid3?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'getid3_description' => 'Čítanie a zápis audio značiek do zvukových súborov
_ [->http://getid3.sourceforge.net/]',
	'getid3_slogan' => 'Čítanie a zápis audio značiek'
);

?>
