<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/csvimport?lang_cible=gl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'administrer_tables' => 'Administrar as táboas',
	'ajouter_donnees' => 'Engadir os datos',
	'ajouter_table' => 'Engadir á táboa',
	'aucune_donnee' => 'Non hai ningún dato dentro do ficheiro',
	'aucune_table_declaree' => 'Ningunha táboa foi declarada para a importación CSV',
	'avertissement_ajout' => 'Os datos do ficheiro CSV vanse engadir á táboa "@table@" como se ilustra a seguir.', # MODIF
	'avertissement_remplacement' => 'Esta operación supón a supresión de todos os datos presentes na táboa. Os datos do ficheiro inseriranse a seguir:',

	// C
	'caracteres_separation' => 'Carácter de separación',
	'champs_csv' => 'Campos do ficheiro CSV',
	'champs_table' => 'Campos da táboa', # MODIF
	'confirmation_ajout_base' => 'Le CSV a été correctement ajouté à la base !', # NEW
	'correspondance_incomplete' => 'As correspondencias CSV-Táboa son incompletas',
	'correspondance_indefinie' => 'As correspondencias entre CSV-Táboa non están definidas',
	'csvimport' => 'Importar un CSV',

	// D
	'delimiteur_indefini' => 'Delimitador non definido',
	'description_table_introuvable' => 'Non se pode encontrar a descrición da táboa',

	// E
	'erreurs_ajout_base' => 'Houbo @nb@ erros logo de engadilos na base.',
	'etape' => '(Étape @step@ sur 3)', # NEW
	'export_classique' => 'CSV clásico (,)',
	'export_excel' => 'CSV para Excel (;)',
	'export_format' => 'Formato do ficheiro:',
	'export_table' => 'Exportación da táboa: @table@',
	'export_tabulation' => 'CSV con tabulación',
	'exporter' => 'Exportar',
	'extrait_CSV_importe' => 'Extrait du fichier CSV importé : ', # NEW
	'extrait_table' => 'Extrait de la table "@nom_table@" : ', # NEW

	// F
	'fichier_absent' => 'Ficheiro ausente',
	'fichier_choisir' => 'Ficheiro CSV para importar',
	'fichier_vide' => 'Ficheiro baleiro',

	// I
	'import_csv' => 'Importar o CSV : @table@', # MODIF
	'import_export_tables' => 'Importar / Exportar nas táboas',

	// L
	'ligne_entete' => '1<sup>ro</sup> liña de cabeceira',
	'lignes_table' => 'Hai @nb_resultats@ liñas na táboa "@table@".',
	'lignes_totales' => '@nb@ liñas en total.',
	'lignes_totales_csv' => '@nb@ liñas en total no ficheiro CSV.',

	// N
	'nb_enregistrements' => '@nb@ rexistros',
	'noms_colonnes_CSV' => 'Noms des colonnes (attendus) du fichier CSV à importer : ', # NEW

	// P
	'pas_importer' => 'Non importar',
	'premieres_lignes' => '@nb@ primeiras liñas do ficheiro.',
	'previsualisation_CSV_integre' => 'Prévisualisation des @nb@ premières lignes du fichier CSV intégrées à la table : ', # NEW
	'probleme_chargement_fichier' => 'Problema logo da carga do ficheiro',
	'probleme_chargement_fichier_erreur' => 'Problema logo da carga do ficheiro (erro @erreur@).',
	'probleme_inextricable' => 'Problema inextricable...',

	// R
	'remplacer_toute_table' => 'Substituír toda a táboa',

	// T
	'table_vide' => 'A táboa "@table@" está baleira.',
	'tables_declarees' => 'Táboas declaradas',
	'tables_presentes' => 'Táboas presentes na táboa',
	'tout_remplacer' => 'Substituír todo',

	// Z
	'z' => 'zzz'
);

?>
