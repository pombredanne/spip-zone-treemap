<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'administrer_tables' => 'អភិបាល ពួកតារាង',
	'ajouter_donnees' => 'បន្ថែមទិន្នន័យ',
	'ajouter_table' => 'បន្ថែម ទៅតារាង',
	'aucune_donnee' => 'គ្មានទិន្នន័យ ក្នុងឯកសារ។',
	'aucune_table_declaree' => 'Aucune table n\'est déclarée pour l\'import CSV', # NEW
	'avertissement_ajout' => 'Les données du fichier CSV vont être ajoutées à la table "@table@" comme illustré ci-dessous.', # MODIF
	'avertissement_remplacement' => 'Cette opération va entraîner la suppression de toutes les données présentes dans la table. Les données du fichier seront insérées comme ci-dessous :', # NEW

	// C
	'caracteres_separation' => 'Caractère de séparation', # NEW
	'champs_csv' => 'Champs du fichier CSV', # NEW
	'champs_table' => 'វាល នៃតារាង', # MODIF
	'confirmation_ajout_base' => 'Le CSV a été correctement ajouté à la base !', # NEW
	'correspondance_incomplete' => 'Correspondances CSV-Table incomplètes', # NEW
	'correspondance_indefinie' => 'Correspondances CSV-Table non définies', # NEW
	'csvimport' => 'នាំចូល CSV',

	// D
	'delimiteur_indefini' => 'Délimiteur non défini', # NEW
	'description_table_introuvable' => 'Description de la table introuvable', # NEW

	// E
	'erreurs_ajout_base' => 'Il y a eu @nb@ erreurs lors de l\'ajout dans la base.', # NEW
	'etape' => '(&Eacute;tape @step@ sur 3)', # NEW
	'export_classique' => 'CSV classique (,)', # NEW
	'export_excel' => 'CSV pour Excel (;)', # NEW
	'export_format' => 'ទ្រង់ទ្រាយ នៃឯកសារ ៖',
	'export_table' => 'ការនាំចេញ នៃតារាង៖ @table@',
	'export_tabulation' => 'CSV avec tabulations', # NEW
	'exporter' => 'នាំចេញ',
	'extrait_CSV_importe' => 'Extrait du fichier CSV importé : ', # NEW
	'extrait_table' => 'Extrait de la table "@nom_table@" : ', # NEW

	// F
	'fichier_absent' => 'ឯកសារអវត្តមាន',
	'fichier_choisir' => 'ឯកសារ CSV ត្រូវនាំចូល',
	'fichier_vide' => 'ឯកសារទទេ',

	// I
	'import_csv' => 'នាំចូល CSV ៖ @table@', # MODIF
	'import_export_tables' => 'នាំចូល / នាំចេញ ក្នុងតារាង',

	// L
	'ligne_entete' => '1<sup>ère</sup> ligne d\'en-tête', # NEW
	'lignes_table' => 'Il y a @nb_resultats@ lignes dans la table "@table@".', # NEW
	'lignes_totales' => 'សរុប @nb@ បន្ទាត់។',
	'lignes_totales_csv' => 'សរុប @nb@ បន្ទាត់ ក្នុងឯកសារ CSV។',

	// N
	'nb_enregistrements' => '@nb@ enregistrements', # NEW
	'noms_colonnes_CSV' => 'Noms des colonnes (attendus) du fichier CSV à importer : ', # NEW

	// P
	'pas_importer' => 'គ្មានត្រូវនាំចូល',
	'premieres_lignes' => '@nb@ បន្ទាត់ដំបូង នៃឯកសារ។',
	'previsualisation_CSV_integre' => 'Prévisualisation des @nb@ premières lignes du fichier CSV intégrées à la table : ', # NEW
	'probleme_chargement_fichier' => 'បញ្ហា ពេលទាញយកឯកសារ',
	'probleme_chargement_fichier_erreur' => 'បញ្ហា ពេលទាញយកឯកសារ (កំហុស @erreur@)។',
	'probleme_inextricable' => 'Problème inextricable...', # NEW

	// R
	'remplacer_toute_table' => 'Remplacer toute la table', # NEW

	// T
	'table_vide' => 'តារាង "@table@" ទទេ។',
	'tables_declarees' => 'Tables déclarées', # NEW
	'tables_presentes' => 'Tables présentes dans la base', # NEW
	'tout_remplacer' => 'ជំនួសទាំងអស់',

	// Z
	'z' => 'zzz'
);

?>
