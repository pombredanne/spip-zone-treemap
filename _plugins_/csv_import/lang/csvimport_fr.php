<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/csv_import/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'administrer_tables' => 'Administrer les tables',
	'ajouter_donnees' => 'Ajouter les données',
	'ajouter_table' => 'Ajouter à la table',
	'aucune_donnee' => 'Il n\'y a aucune donnée dans le fichier.',
	'aucune_table_declaree' => 'Aucune table n\'est déclarée pour l\'import CSV',
	'avertissement_ajout' => 'Les données du fichier CSV vont être ajoutées à la table "@table@" comme illustré ci-dessus.',
	'avertissement_remplacement' => 'Cette opération va entraîner la suppression de toutes les données présentes dans la table. Les données du fichier seront insérées comme ci-dessous :',

	// C
	'caracteres_separation' => 'Caractère de séparation',
	'champs_csv' => 'Champs du fichier CSV',
	'champs_table' => 'Champs de la table : "libellé (clé)"',
	'confirmation_ajout_base' => 'Le CSV a été correctement ajouté à la base !',
	'correspondance_incomplete' => 'Correspondances CSV-Table incomplètes',
	'correspondance_indefinie' => 'Correspondances CSV-Table non définies',
	'csvimport' => 'Import CSV',

	// D
	'delimiteur_indefini' => 'Délimiteur non défini',
	'description_table_introuvable' => 'Description de la table introuvable',

	// E
	'erreurs_ajout_base' => 'Il y a eu @nb@ erreurs lors de l\'ajout dans la base.',
	'etape' => '(Étape @step@ sur 3)',
	'export_classique' => 'CSV classique (,)',
	'export_excel' => 'CSV pour Excel (;)',
	'export_format' => 'Format du fichier :',
	'export_table' => 'Export de la table : @table@',
	'export_tabulation' => 'CSV avec tabulations',
	'exporter' => 'Exporter',
	'extrait_CSV_importe' => 'Extrait du fichier CSV importé : ',
	'extrait_table' => 'Extrait de la table "@nom_table@" : ',

	// F
	'fichier_absent' => 'Fichier absent',
	'fichier_choisir' => 'Fichier CSV à importer',
	'fichier_vide' => 'Fichier vide',

	// I
	'import_csv' => 'Import CSV : "@table@"',
	'import_export_tables' => 'Import / Export dans les tables',

	// L
	'ligne_entete' => '1<sup>ère</sup> ligne d\'en-tête',
	'lignes_table' => 'Il y a @nb_resultats@ lignes dans la table "@table@".',
	'lignes_totales' => '@nb@ lignes au total.',
	'lignes_totales_csv' => '@nb@ lignes au total dans le fichier CSV.',

	// N
	'nb_enregistrements' => '@nb@ enregistrements',
	'noms_colonnes_CSV' => 'Noms des colonnes (attendus) du fichier CSV à importer : ',

	// P
	'pas_importer' => 'Ne pas importer',
	'premieres_lignes' => '@nb@ premières lignes du fichier.',
	'previsualisation_CSV_integre' => 'Prévisualisation des @nb@ premières lignes du fichier CSV intégrées à la table : ',
	'probleme_chargement_fichier' => 'Problème lors du chargement du fichier',
	'probleme_chargement_fichier_erreur' => 'Problème lors du chargement du fichier (erreur @erreur@).',
	'probleme_inextricable' => 'Problème inextricable...',

	// R
	'remplacer_toute_table' => 'Remplacer toute la table',

	// T
	'table_vide' => 'La table "@table@" est vide.',
	'tables_declarees' => 'Tables déclarées',
	'tables_presentes' => 'Tables présentes dans la base',
	'tout_remplacer' => 'Tout remplacer',

	// Z
	'z' => 'zzz'
);

?>
