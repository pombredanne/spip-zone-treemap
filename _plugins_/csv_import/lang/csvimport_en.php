<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/csvimport?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'administrer_tables' => 'Manage the tables',
	'ajouter_donnees' => 'Add data',
	'ajouter_table' => 'Add to the table',
	'aucune_donnee' => 'There is no data in the file.',
	'aucune_table_declaree' => 'There is no table declared for the CSV import',
	'avertissement_ajout' => 'The data in the CSV file will be added to the "@table@" table as illustrated below.',
	'avertissement_remplacement' => 'This operation will involve deleting all existing data in the table. The data from the file will be inserted as shown below:',

	// C
	'caracteres_separation' => 'Separator character',
	'champs_csv' => 'Fields in the CSV file',
	'champs_table' => 'Fields of the table: "label (key)"',
	'confirmation_ajout_base' => 'The CSV file has been successfully added to the database!',
	'correspondance_incomplete' => 'The CSV-Table correspondences are incomplete',
	'correspondance_indefinie' => 'The CSV-Table correspondences are undefined',
	'csvimport' => 'CSV Import',

	// D
	'delimiteur_indefini' => 'The delimiter is undefined',
	'description_table_introuvable' => 'The table description can not be found',

	// E
	'erreurs_ajout_base' => 'There were @nb@ insertion errors into the database.',
	'etape' => '(Step @step@ on 3)',
	'export_classique' => 'standard CSV (,)',
	'export_excel' => 'Excel CSV (;)',
	'export_format' => 'File format:',
	'export_table' => 'Export of the table: @table@',
	'export_tabulation' => 'Tab-separated CSV',
	'exporter' => 'Export',
	'extrait_CSV_importe' => 'Excerpt from the imported CSV file:',
	'extrait_table' => 'Extract from the table "@nom_table@" : ',

	// F
	'fichier_absent' => 'Missing file',
	'fichier_choisir' => 'CSV file to import',
	'fichier_vide' => 'Empty file',

	// I
	'import_csv' => 'CSV Import: "@table@"',
	'import_export_tables' => 'Import / Export for tables',

	// L
	'ligne_entete' => '1<sup>st</sup> row as headers',
	'lignes_table' => 'There are @nb_resultats@ rows in the "@table@" table.',
	'lignes_totales' => '@nb@ rows in total.',
	'lignes_totales_csv' => '@nb@ rows in total in the CSV file.',

	// N
	'nb_enregistrements' => '@nb@ records saved',
	'noms_colonnes_CSV' => 'Column names (expected) from the CSV file to import:',

	// P
	'pas_importer' => 'Do not import',
	'premieres_lignes' => '@nb@ first rows in the file.',
	'previsualisation_CSV_integre' => 'Preview of the @nb@ first lines of the CSV files imported to the table: ',
	'probleme_chargement_fichier' => 'Problem loading the file',
	'probleme_chargement_fichier_erreur' => 'Problem loading the file (@erreur@ error).',
	'probleme_inextricable' => 'Unrecognised error...',

	// R
	'remplacer_toute_table' => 'Replace the whole table',

	// T
	'table_vide' => 'The "@table@" table is empty.',
	'tables_declarees' => 'Tables declared',
	'tables_presentes' => 'Tables existing in the database',
	'tout_remplacer' => 'Replace all',

	// Z
	'z' => 'zzz'
);

?>
