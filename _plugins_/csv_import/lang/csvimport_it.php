<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/csvimport?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'administrer_tables' => 'Amministra le tabelle',
	'ajouter_donnees' => 'Aggiungi dati',
	'ajouter_table' => 'Aggiungi alla tabella',
	'aucune_donnee' => 'Il file non contiene alcun dato.',
	'aucune_table_declaree' => 'Nessuna tabella dichiarata per l\'importazione CSV',
	'avertissement_ajout' => 'I dati del file CSV saranno aggiunti alla tabella "@table@" come mostrato di seguito.', # MODIF
	'avertissement_remplacement' => 'Questa operazione sopprimerà tutti i dati presenti nella tabella. I dati del file saranno inseriti come mostrato di seguito:',

	// C
	'caracteres_separation' => 'Carattere di separazione',
	'champs_csv' => 'Campi del file CSV',
	'champs_table' => 'Campi della tabella', # MODIF
	'confirmation_ajout_base' => 'Le CSV a été correctement ajouté à la base !', # NEW
	'correspondance_incomplete' => 'Mappa CSV-Tabella incompleta',
	'correspondance_indefinie' => 'Mappa CSV-Tabella non definita',
	'csvimport' => 'Importazione CSV',

	// D
	'delimiteur_indefini' => 'Delimitatore non definito',
	'description_table_introuvable' => 'Descrizione della tabella non trovata',

	// E
	'erreurs_ajout_base' => 'Si sono verificati @nb@ errori durante l\'inserimento in database.',
	'etape' => '(Étape @step@ sur 3)', # NEW
	'export_classique' => 'CSV classico (,)',
	'export_excel' => 'CSV per Excel (;)',
	'export_format' => 'Formato del file:',
	'export_table' => 'Esportazione della tabella: @table@',
	'export_tabulation' => 'CSV con tabulazioni',
	'exporter' => 'Esportare',
	'extrait_CSV_importe' => 'Extrait du fichier CSV importé : ', # NEW
	'extrait_table' => 'Extrait de la table "@nom_table@" : ', # NEW

	// F
	'fichier_absent' => 'File non trovato',
	'fichier_choisir' => 'File CSV da importare',
	'fichier_vide' => 'File vuoto',

	// I
	'import_csv' => 'Importazione CSV : @table@', # MODIF
	'import_export_tables' => 'Importazione / Esportazione nelle tabelle',

	// L
	'ligne_entete' => '1<sup>a</sup> riga d\'intestazione',
	'lignes_table' => 'Ci sono @nb_resultats@ righe nella tabella "@table@".',
	'lignes_totales' => '@nb@ righe totali.',
	'lignes_totales_csv' => '@nb@ righe totali nel file CSV.',

	// N
	'nb_enregistrements' => '@nb@ inserimenti',
	'noms_colonnes_CSV' => 'Noms des colonnes (attendus) du fichier CSV à importer : ', # NEW

	// P
	'pas_importer' => 'Non importare',
	'premieres_lignes' => '@nb@ prime righe del file.',
	'previsualisation_CSV_integre' => 'Prévisualisation des @nb@ premières lignes du fichier CSV intégrées à la table : ', # NEW
	'probleme_chargement_fichier' => 'Problema durante il caricamento del file',
	'probleme_chargement_fichier_erreur' => 'Problema durante il caricamento del file (errore @erreur@).',
	'probleme_inextricable' => 'Errore fatale...',

	// R
	'remplacer_toute_table' => 'Sostituisci tutta la tabella',

	// T
	'table_vide' => 'La tabella "@table@" è vuota.',
	'tables_declarees' => 'Tabelle dichiarate',
	'tables_presentes' => 'Tabelle presenti in database',
	'tout_remplacer' => 'Sostituire tutto',

	// Z
	'z' => 'zzz'
);

?>
