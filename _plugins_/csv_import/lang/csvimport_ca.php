<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/csvimport?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'administrer_tables' => 'Administrar les taules',
	'ajouter_donnees' => 'Afegir dades ',
	'ajouter_table' => 'Afegir a la taula',
	'aucune_donnee' => 'No hi ha dades a l\'arxiu.',
	'aucune_table_declaree' => 'Cap taula declarada per la importació CSV',
	'avertissement_ajout' => 'Les dades de l\'arxiu CSV seran afegides a la taula "@table@" tal i com es mostra a continuació.', # MODIF
	'avertissement_remplacement' => 'Aquesta operació suposarà la supressió de totes les dades que hi ha a la taula. Les dades del fitxer s\'inseriran com més avall:  ',

	// C
	'caracteres_separation' => 'Caràcter separador',
	'champs_csv' => 'Camps de l\'arxiu CSV',
	'champs_table' => 'Camps de la taula', # MODIF
	'confirmation_ajout_base' => 'Le CSV a été correctement ajouté à la base !', # NEW
	'correspondance_incomplete' => 'Correspondències CSV-Taula incompletes',
	'correspondance_indefinie' => 'Correspondències CSV-Table no definides',
	'csvimport' => 'Importa CSV',

	// D
	'delimiteur_indefini' => 'Delimitador sense definir',
	'description_table_introuvable' => 'Descripció de la taula introbable',

	// E
	'erreurs_ajout_base' => 'Hi ha hagut @nb@ errors quan s\'ha afegit a la base. ',
	'etape' => '(Étape @step@ sur 3)', # NEW
	'export_classique' => 'CSV clàssic (,)',
	'export_excel' => 'CSV per Excel (;)',
	'export_format' => 'Format de l\'arxiu:',
	'export_table' => 'Exportar la taula: @table@',
	'export_tabulation' => 'CSV amb tabulacions',
	'exporter' => 'Exportar',
	'extrait_CSV_importe' => 'Extrait du fichier CSV importé : ', # NEW
	'extrait_table' => 'Extrait de la table "@nom_table@" : ', # NEW

	// F
	'fichier_absent' => 'Arxiu absent',
	'fichier_choisir' => 'Arxiu CSV a importar',
	'fichier_vide' => 'Arxiu buit',

	// I
	'import_csv' => 'Importa CSV : @table@', # MODIF
	'import_export_tables' => 'Importar / Exportar a les taules',

	// L
	'ligne_entete' => '1a. línia de capçalera',
	'lignes_table' => 'Hi ha @nb_resultats@ línies a la taula "@table@".',
	'lignes_totales' => '@nb@ línies en total.',
	'lignes_totales_csv' => '@nb@ línies en total a l\'arxiu CSV.',

	// N
	'nb_enregistrements' => '@nb@ registres',
	'noms_colonnes_CSV' => 'Noms des colonnes (attendus) du fichier CSV à importer : ', # NEW

	// P
	'pas_importer' => 'No importar',
	'premieres_lignes' => '<@nb@ primeres línies de l\'arxiu.',
	'previsualisation_CSV_integre' => 'Prévisualisation des @nb@ premières lignes du fichier CSV intégrées à la table : ', # NEW
	'probleme_chargement_fichier' => 'Problema durant la càrrega de l\'arxiu',
	'probleme_chargement_fichier_erreur' => 'Problema durant la càrrega de l\'arxiu (error @erreur@).',
	'probleme_inextricable' => 'Problema inextricable...',

	// R
	'remplacer_toute_table' => 'Substituir tota la taula',

	// T
	'table_vide' => 'La taula "@table@" està buida.',
	'tables_declarees' => 'Taules declarades',
	'tables_presentes' => 'Taules presents a la base',
	'tout_remplacer' => 'Reemplaçar-ho tot ',

	// Z
	'z' => 'zzz'
);

?>
