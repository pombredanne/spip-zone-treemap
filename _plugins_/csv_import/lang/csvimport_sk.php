<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/csvimport?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'administrer_tables' => 'Riadiť tabuľky',
	'ajouter_donnees' => 'Pridať údaje',
	'ajouter_table' => 'Pridať do tabuľky',
	'aucune_donnee' => 'V súbore nie sú žiadne údaje.',
	'aucune_table_declaree' => 'Na nahrávanie do CSV nie je deklarovaná žiadna tabuľka',
	'avertissement_ajout' => 'Údaje v súbore CSV budú pridané do tabuľky "@table@", ako je to opísané nižšie.',
	'avertissement_remplacement' => 'Súčasťou tejto operácie bude odstránenie všetkých existujúcich údajov v tabuľke. Údaje zo súboru budú vložené, ako je to uvedené nižšie:',

	// C
	'caracteres_separation' => 'Znak oddeľovača',
	'champs_csv' => 'Polia v súbore CSV',
	'champs_table' => 'Polia v tabuľke',
	'confirmation_ajout_base' => 'CSV bolo úspešne pridané do databázy!',
	'correspondance_incomplete' => 'Súvzťažnosti v tabuľkách CSV sú neúplné.',
	'correspondance_indefinie' => 'Súvzťažnosti v tabuľkách CSV nie sú definované.',
	'csvimport' => 'Nahrávanie CSV',

	// D
	'delimiteur_indefini' => 'Oddeľovač nie je definovaný',
	'description_table_introuvable' => 'Popis tabuľky sa nedá nájsť',

	// E
	'erreurs_ajout_base' => 'Pri vkladaní do databázy vzniklo @nb@ chýb.',
	'etape' => '(@step@. krok z 3)',
	'export_classique' => 'štadardný CSV (,)',
	'export_excel' => 'CSV Excelu (;)',
	'export_format' => 'Formát súboru:',
	'export_table' => 'Export tabuľky: @table@',
	'export_tabulation' => 'CSV oddelený tabulátorom',
	'exporter' => 'Exportovať',
	'extrait_CSV_importe' => 'Extrahovať z nahratého súboru CSV:',
	'extrait_table' => 'Vypísať z tabuľky "@nom_table@":',

	// F
	'fichier_absent' => 'Súbor chýba',
	'fichier_choisir' => 'Súbor CSV, ktorý sa má nahrať',
	'fichier_vide' => 'Prázdny súbor',

	// I
	'import_csv' => 'Nahrať CSV: @table@',
	'import_export_tables' => 'Nahrávanie/export tabuliek',

	// L
	'ligne_entete' => '1. riadok ako hlavičku',
	'lignes_table' => 'V tabuľke "@table@" je @nb_resultats@.',
	'lignes_totales' => '@nb@ riadkov spolu.',
	'lignes_totales_csv' => '@nb@ riadkov v súbore CSV spolu.',

	// N
	'nb_enregistrements' => '@nb@ uložených záznamov',
	'noms_colonnes_CSV' => '(Očakávané) názvy stĺpcov v súbore CSV, ktorý sa má nahrať:',

	// P
	'pas_importer' => 'Nenahrávať',
	'premieres_lignes' => '@nb@ prvých riadkov v súbore.',
	'previsualisation_CSV_integre' => 'Ukážka prvých @nb@ riadkov zo súboru CSV začleneného do tabuľky:',
	'probleme_chargement_fichier' => 'Problém pri spúšťaní súboru',
	'probleme_chargement_fichier_erreur' => 'Problém pri spúšťaní súboru (chyba @erreur@).',
	'probleme_inextricable' => 'Nerozoznaná chyba.',

	// R
	'remplacer_toute_table' => 'Nahradiť celú tabuľku',

	// T
	'table_vide' => 'Tabuľka "@table@" je prázdna.',
	'tables_declarees' => 'Tabuľky deklarované',
	'tables_presentes' => 'Tabuľky existujúce v databáze',
	'tout_remplacer' => 'Nahradiť všetko',

	// Z
	'z' => 'zzz'
);

?>
