<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/csvimport?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'administrer_tables' => 'مديريت جدول‌ها',
	'ajouter_donnees' => 'افزودن داده‌ها',
	'ajouter_table' => 'افزودن به جدول',
	'aucune_donnee' => 'هيچ داده آي در پرونده موجود نيست.',
	'aucune_table_declaree' => 'هيچ جدول اعلام شده‌اي براي واردات سي.اس.وي (CSV) موجود نيست.',
	'avertissement_ajout' => 'داده‌هاي پرونده‌ي سي.اس.وي به جدول "@table@" طبق تصوير زير افزوده خواهد شد.', # MODIF
	'avertissement_remplacement' => 'اين عمليات مستلزم حذف تمام داده‌هاي اين جدول خواهد بود. داده‌هاي اين پرونده اينطور كه نشان داده شده گنجانده خواهد شد:',

	// C
	'caracteres_separation' => 'كارآكتر جداساز',
	'champs_csv' => 'ميدان‌هاي پرونده‌ي سي.اس.وي (CSV)',
	'champs_table' => 'ميدان‌هاي جدول', # MODIF
	'confirmation_ajout_base' => 'Le CSV a été correctement ajouté à la base !', # NEW
	'correspondance_incomplete' => 'تطابقات جدول-سي.اس.وي ناكامل است',
	'correspondance_indefinie' => 'تطابقات جدول-سي.اس.وي تعريف نشده',
	'csvimport' => 'واردسازي سي.اس.وي',

	// D
	'delimiteur_indefini' => 'جداساز تعريف نشده',
	'description_table_introuvable' => 'توصيف جدول يافت نمي‌شود',

	// E
	'erreurs_ajout_base' => 'در گنجاندن داده‌هاي جدول تعداد @nb@ خطا رخ داده است',
	'etape' => '(Étape @step@ sur 3)', # NEW
	'export_classique' => 'سي.اس.وي استاندارد (،)',
	'export_excel' => 'سي.وي.اس براي اكسل (؛)',
	'export_format' => 'فرمت پرونده :‌',
	'export_table' => 'صادرسازي جدول: @table@',
	'export_tabulation' => 'سي.اس.وي با تب‌ جداساز',
	'exporter' => 'صادرسازي',
	'extrait_CSV_importe' => 'Extrait du fichier CSV importé : ', # NEW
	'extrait_table' => 'Extrait de la table "@nom_table@" : ', # NEW

	// F
	'fichier_absent' => 'پرونده مفقود',
	'fichier_choisir' => 'پرونده‌ي سي.اس.وي براي صادرسازي',
	'fichier_vide' => 'پرونده خالي',

	// I
	'import_csv' => 'صادرسازي سي.وي.اس : @table@', # MODIF
	'import_export_tables' => 'صادر/واردسازي براي جدول‌ها',

	// L
	'ligne_entete' => 'رديف <sup>اول</sup> براي سرصفحه',
	'lignes_table' => 'در جدول"@table@"اينقدر رديف وجود دارد:  @nb_resultats@',
	'lignes_totales' => '@nb@ دريف در كل ',
	'lignes_totales_csv' => 'در كل @nb@ رديف در پرونده سي.اس.وي',

	// N
	'nb_enregistrements' => '@nb@ ركورد ضبط شده',
	'noms_colonnes_CSV' => 'Noms des colonnes (attendus) du fichier CSV à importer : ', # NEW

	// P
	'pas_importer' => 'صادر نسازي ',
	'premieres_lignes' => '@nb@ رديف‌ نخست از پرونده',
	'previsualisation_CSV_integre' => 'Prévisualisation des @nb@ premières lignes du fichier CSV intégrées à la table : ', # NEW
	'probleme_chargement_fichier' => 'مشكل در بارگذاري پرونده',
	'probleme_chargement_fichier_erreur' => 'مشكل در بارگذاري پرونده (خطاي @erreur@).',
	'probleme_inextricable' => 'مشكل بي‌سابقه...',

	// R
	'remplacer_toute_table' => 'جايگزين‌سازي كل جدول',

	// T
	'table_vide' => 'جدول"@table@" خالي است.',
	'tables_declarees' => 'جدول‌هاي اعلام شده',
	'tables_presentes' => 'جدول‌هاي موجود در پايگاه داده‌ها',
	'tout_remplacer' => 'جايگزين‌سازي همه',

	// Z
	'z' => 'zzz'
);

?>
