<?php
/***************************************************************************\
 * Plugin Nouvelle Version pour Spip 2.0
 * Licence GPL (c) 2011
 * Modération de la nouvelle version d'un article
 *
\***************************************************************************/

// Ceci est un fichier langue de SPIP  --  This is a SPIP language file

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'icone_dupliquer' => 'Dupliquer la rubrique',
	'se_rendre_sur_la_version' => "Voir la nouvelle version en cours d'édition",
	'se_rendre_sur_l_original' => "Voir la version publiée",
	'message_annuler' => 'Annuler',
	'message_confirmer' => 'Confirmer',	
	
	'operation_executee' => "L'op&eacute;ration a bien &eacute;t&eacute; ex&eacute;cut&eacute;e.",
	'operation_annulee' => "L'op&eacute;ration a &eacute;t&eacute; annul&eacute;e.",
	'operation_retour_ok' => "Se rendre dans la rubrique copi&eacute;e.",
	'operation_retour_ko' => "Retour aux rubriques.",

	'icone_dupliquer_article' => "Nouvelle version de l'article",
	'icone_remplacer_article' => "Publier cette version",


	'operation_retour_ok_article' => "Se rendre dans l'article dupliqu&eacute;.",
	'operation_retour_ok_article_publi' => "Se rendre dans l'article publi&eacute;.",

	'operation_retour_ko_article' => "Retour aux articles."
);
