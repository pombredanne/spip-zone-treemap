<?php 

// Fichier generer par le plugin Atelier

if(!defined("_ECRIRE_INC_VERSION")) return; 

$GLOBALS[$GLOBALS['idx_lang']] = array (

	
	// A

	'action' => 'Actions',
	'administration' => 'Administration',
	'ajouter_page_cfg' => 'Ajouter une page CFG',
	'ajouter_tache' => 'Ajouter une t&acirc;che',
	'atelier' => 'Atelier',
	'atelier_lang' => 'Gestion des fichiers langue',
	'aucun_projet' => 'Aucun projet selectionn&eacute;. Vous devez appeler cette page en lui donnant un num&eacute;ro de projet.',

	// B

	'bouton_add_projet' => 'Ajouter les fichiers selectionn&eacute;s au projet',
	'bouton_ajouter_version' => 'Ajouter cette version au projet',
	'bouton_checkout_projet' => 'Lancer un checkout du projet',
	'bouton_commit_projet' => 'Commiter votre projet',
	'bouton_creer_fichier_lang' => 'Cr&eacute;er un fichier langue',
	'bouton_creer_objet' => 'Cr&eacute;er cet objet',
	'bouton_creer_repertoire' => 'Cr&eacute;er le r&eacute;pertoire du plugin',
	'bouton_creer_repertoire_lang' => 'Cr&eacute;er le r&eacute;pertoire lang',
	'bouton_enregistrer' => 'Enregistrer',
	'bouton_enregistrer_fichier' => 'Enregistrer le fichier',
	'bouton_generer_todo' => 'G&eacute;n&eacute;rer le fichier TODO.txt',
	'bouton_installer_base' => 'Installer la base de donn&eacute;e',
	'bouton_supprimer' => 'Supprimer',
	'bouton_update_projet' => 'Lancer un update du projet',

	// C

	'cfg' => 'CFG : moteur de configuration',
	'charger_pipelines' => 'charger_pipelines.php',
	'charger_plugins_fonctions' => 'charger_plugins_fonctions.php',
	'charger_plugins_options' => 'charger_plugins_options.php',
	'choisissez_lang' => 'Choisissez la langue du fichier : ',
	'code_retour' => '- Code retour : ',
	'commande' => '- Commande : ',
	'commentaire' => 'Commentaire',
	'commit_sans_commentaire' => 'Votre commit a &eacute;t&eacute; annul&eacute; pour cause de commentaire manquant. Veuilliez remplir le champs "commentaire" avec les explications relatives &agrave; votre commit avant de valider le formulaire.',
	'commit_svn' => 'Mettre &agrave; jour la zone &agrave; partir de votre copie de travail. Cette action va provoquer une nouvelle version du projet sur la zone &agrave; partir de vos modifications. N\'oubliez surtout pas de remplir le champs "commentaire" afin d\'expliquer la nature de vos modifications.',
	'contenu_repertoire_lang' => 'Contenu du r&eacute;pertoire "lang". Cliquez sur l\'un des fichiers pour le modifier ou le visualiser.',
	'creer_arbo' => 'Cr&eacute;er l\'arborescence de votre projet ?',
	'creer_projet' => 'Cr&eacute;er le projet ?',
	'creer_repertoire_lang' => 'Cr&eacute;er le r&eacute;pertoire "lang"',

	// D

	'dev' => 'Dev',
	'documentation' => 'Documentation',
	'documentation_code' => 'Documentation du code SPIP',
	'droit_insuffisant' => 'Vous n\'avez pas les droits necessaires pour ecrire dans le repertoire plugin ...<br />Veuilliez modifier les droits de ce r&eacute;pertoire.',

	// E

	'editer_action_txt' => 'Editer action.txt',
	'editer_exec_txt' => 'Editer exec.txt',
	'editer_inc_txt' => 'Editer inc.txt',
	'erreur_commande' => 'Erreur commande<br />',
	'erreur_deja_present' => 'Erreur : Plugin d&eacute;j&agrave; present ! checkout impossible<br />',
	'erreur_nom_manquant' => 'Erreur : Nom manquant ! checkout impossible<br />',
	'erreur_svn_pas_installe' => 'Subversion n\'est pas install&eacute;',
	'etat_projet' => 'Etat du projet : ',
	'explication_ajouter_lang' => 'Remplissez les champs ci-dessous pour ajouter une d&eacute;finition &agrave; votre fichier langue.',
	'explication_creer_fichier_lang' => 'Vous pouvez cr&eacute;er un nouveau fichier langue. Celui-ci sera ajout&eacute; &agrave; votre r&eacute;pertoire "lang". La liste des fichiers d&eacute;j&agrave; existants est situ&eacute;e dans la colonne de gauche.',
	'explication_editer_lang' => 'Utilisez les champs ci-dessous pour modifier une d&eacute;finition. Pour supprimer une d&eacute;finition, mettez la &agrave; vide, puis validez.',
	'explication_objets' => 'Ce formulaire ne prend en compte que les objets de l\'espace priv&eacute;e de SPIP. Il permet de cr&eacute;er un fichier "exec", "inc" ou "action".',
	'explication_rep_lang' => 'Le r&eacute;pertoire lang contient tous les fichiers langue de votre plugin.',
	'explication_status_svn' => '<ul><li>M : fichier modifier localement</li><li>D : supprim&eacute;</li><li>A : ajout&eacute;</li></ul>',

	// F

	'fichier_enregistrer' => 'le fichier a &eacute;t&eacute; enregist&eacute;',

	// G

	'gerer_versions' => 'Gestion des versions',

	// I

	'importer_projet_zone' => 'Importer un projet de la zone',
	'installer_base' => 'Le plugin Atelier utilise trois tables suppl&eacute;mentaires dans la base de donn&eacute;e, il faut donc installer ces tables pour pouvoir utiliser le plugin Atelier. Vous pouver &agrave; tout moment supprimer ces nouvelles tables en utilisant l\'action "Supprimer le plugin Atelier".',
	'installer_svn' => 'Subversion ne semble pas &ecirc;tre install&eacute; sur votre syst&egrave;me. Pour b&eacute;n&eacute;ficier des options offerts par subversion, installez-le.',

	// L

	'lang' => 'Internationalisation',
	'licence' => 'Distribu&eacute; sous licence GNU/GPL',
	'liste_projets' => 'Liste des projets',
	'liste_taches' => 'Liste des t&acirc;ches',
	'liste_taches_fermees' => 'Liste des t&acirc;ches ferm&eacute;es',
	'liste_taches_ouvertes' => 'Liste des t&acirc;ches ouvertes',

	// M

	'meta_cache' => 'meta_cache.txt',
	'modifier_plugin_xml' => 'Le fichier plugin.xml contient toutes les informations relatives &agrave; votre plugin. Il permet &agrave; SPIP de comprendre la structure de votre plugin et ainsi de l\'ins&eacute;rer dans son fonctionnement.',
	'modifier_projet' => 'Modifier le projet',
	'modifier_tache' => 'Modifier la t&acirc;che',

	// N

	'nom_projet' => 'Nom du projet : ',
	'nouveau_projet' => 'Nouveau projet',
	'nouveau_tache' => 'Nouvelle t&acirc;che',

	// O

	'objets' => 'Cr&eacute;er un objet SPIP',

	// P

	'page_lang' => 'Gestion des fichiers langue',
	'page_principale' => 'Page principale',
	'page_svn' => 'Outils Subversion',
	'pas_definition' => 'Ce fichier langue ne contient aucunes d&eacute;finitions ...',
	'plugin' => 'Plugin',
	'plugin_auteur' => 'Auteur',
	'plugin_description' => 'Description',
	'plugin_etat' => 'Etat',
	'plugin_expl_auteur' => 'Le nom de l\'auteur et &eacute;ventuellement son adresse email',
	'plugin_expl_description' => 'La description de votre plugin en quelques lignes.',
	'plugin_expl_etat' => 'L\'&eacute;tat actuel de votre plugin. Il peut &ecirc;tre soit "dev", soit "test", soit "stable".',
	'plugin_expl_fonctions' => 'Indiquez i&ccedil;i le nom de votre fichier fonction. G&eacute;n&eacute;ralement, il est de la forme prefixe_fonctions.php',
	'plugin_expl_lien' => 'Liens vers la documentation et/ou votre site.',
	'plugin_expl_new_necessite' => 'Si cela est necessaire, vous pouvez ajouter une d&eacute;pendance &agrave; votre plugin. Spip refusera d\'activer votre plugin tant que cette d&eacute;pendance n\'est pas satisfaite.',
	'plugin_expl_new_pipeline' => 'Un pipeline permet &agrave; votre plugin d\'inter-agir avec SPIP ou avec d\'autres plugins.',
	'plugin_expl_nom' => 'Le nom de votre plugin',
	'plugin_expl_options' => 'indiquez i&ccedil;i le nom de votre fichier option. G&eacute;n&eacute;ralement, il est de la forme prefixe_options.php',
	'plugin_expl_prefixe' => 'Le pr&eacute;fixe correspond &agrave; l\'identifiant de votre plugin. Les caract&egrave;res sp&eacute;ciaux sont interdits, de m&ecirc;me que les espaces.',
	'plugin_expl_version' => 'La version actuelle de votre plugin',
	'plugin_fonctions' => 'Fonctions',
	'plugin_lien' => 'Lien',
	'plugin_new_necessite' => 'Ajouter une d&eacute;pendance',
	'plugin_new_pipeline' => 'Ajouter un pipeline',
	'plugin_nom' => 'Nom',
	'plugin_options' => 'Options',
	'plugin_prefixe' => 'Pr&eacute;fixe',
	'plugin_version' => 'Version',
	'plugin_xml' => 'plugin.xml',
	'plugins_xml' => 'plugins_xml.cache',
	'presentation' => 'Le plugin atelier se veut &ecirc;tre un framework de developpement pour spip. Il a pour but de simplifier le developpement d\'outils tel qu\'un plugin ou un squelette',
	'prevenir_suppression' => 'Attention, l\'op&eacute;ration que vous vous apprettez &agrave; effectuer supprimera d&eacute;finitivement les tables de la base de donn&eacute;e utilis&eacute; par le plugin Atelier.',
	'prevenir_suppression_projet' => 'Attention, vous vous appretez &agrave; suprimmer un projet. Cette action supprimera d&eacute;finitivement tous les fichiers relatifs &agrave; ce projet. Etes vous r&eacute;&eacute;lement certain de ce que vous faites ?',
	'projet_ajoute' => 'Projet ajout&eacute; ...',
	'projet_importer_svn' => 'Projet import&eacute; par svn',
	'projet_svn' => 'Importer un projet de la "zone". Cette action va provoquer un "checkout" du projet et cr&eacute;er une copie de travail dans votre r&eacute;pertoire plugins. Si vous souhaitez cr&eacute;er un projet du m&ecirc;me nom dans l\'atelier, cochez la case "cr&eacute;er le projet ?".',
	'projets' => 'Projets',

	// R

	'raccourcis' => 'Raccourcis',
	'repertoire' => 'Repertoire',
	'repertoire_inexistant' => 'Votre plugin ne semble pas posseder de r&eacute;pertoire de travail dans le r&eacute;pertoire "./plugins" de SPIP.',
	'retour_atelier' => 'Retour &agrave; l\'atelier',
	'revenir_projet' => 'Retour au projet',
	'revenir_roadmap' => 'Revenir &agrave; la feuille de route',

	// S

	'squelette' => 'Squelette',
	'stable' => 'Stable',
	'supprimer_atelier' => 'Supprimer le plugin Atelier',
	'supprimer_projet' => 'Supprimer le projet',
	'svn' => 'Subversion',

	// T

	'tache' => 'T&acirc;che',
	'test' => 'Test',
	'texte_descriptif' => 'Descriptif',
	'texte_etat' => 'Etat',
	'texte_id_projet' => 'ID',
	'texte_id_tache' => 'ID',
	'texte_prefixe' => 'Prefixe',
	'texte_prefixe_obligatoire' => 'Prefixe de votre projet [Obligatoire]',
	'texte_titre' => 'Titre',
	'texte_type' => 'Type',
	'titre' => 'Atelier',
	'titre_edit_choix_auteur' => 'Affectez un auteur &agrave; cette t&acirc;che',
	'titre_edit_choix_etat' => 'Choisissez l\'etat de votre t&acirc;che',
	'titre_edit_choix_projet' => 'Choisissez votre projet',
	'titre_edit_choix_type' => 'Choisissez le type de projet',
	'titre_edit_choix_urgence' => 'Choix de l\'urgence',
	'titre_edit_choix_version' => 'D&ucirc; pour la version',
	'titre_fichiers_temporaires' => 'Fichiers temporaires',
	'titre_infos' => 'Informations',
	'titre_objets' => 'Cr&eacute;ation d\'objets',
	'titre_plugin_xml' => 'Gestion du fichier plugin.xml',
	'titre_projet_choix_auteur' => 'Choississez les auteurs participants &agrave; ce projet.',
	'titre_projets' => 'Lecture d\'un projet',
	'titre_projets_edit' => 'Edition d\'un projet',
	'titre_roadmap' => 'Feuille de route',
	'titre_supprimer_atelier' => 'Suppression de l\'atelier',
	'titre_supprimer_projet' => 'Suppresion du projet',
	'titre_taches' => 'Lecture d\'une t&acirc;che',
	'titre_taches_edit' => 'Edition d\'une t&acirc;che',
	'titre_versions' => 'Gestion des versions',
	'type_projet' => 'Type de projet : ',

	// U

	'update_svn' => 'Mettre &agrave; jour votre copie de travail &agrave; partir de la "zone". Cette action va provoquer un "update" du projet et mettre &agrave; jour les diff&eacute;rents fichiers de votre projet.',

	// V

	'version' => 'Version',
	'voir_feuille_de_route' => 'Voir la feuille de route',
	'voir_metas' => 'Visualiser les metas',


);

?>
