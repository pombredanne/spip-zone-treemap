<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/acces_restreint/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

    // D
    'description_page' => "Voici les 3 derniers tickets en provenance du site @site@. Pour toute question voir http://contrib.spip.net/Plugin tickets",
    // R
    'resumes' => "Résumés",
	// S
	'severite' => "Sévérité",
    'sites_suivis' => 'Sites suivis',
    'statut' => 'Statut',
    // T
    'titre_page' => "Les tickets du site ",
    'type' => "Type",

    );

?>
