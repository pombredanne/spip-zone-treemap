<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'nom' => 'Edito',
'description' => 'Editorial. Un article mis en évidence selon un mot-clé.',
'help' => 'Le mot-clé dépend de la langue. En français, c\'est: "_Edito". Le mot-clé est défini dans le fichier de langue du composant Edito.',

'FondImageRepeatX' => 'R&eacute;p&egrave;te l\'image de fond en X',
'FondImageRepeatY' => 'R&eacute;p&egrave;te l\'image de fond en Y',
'FondAlign' => 'Alignement de l\'image de fond',
'FondVerticalAlign' => 'Alignement vertical de l\'image de fond',
'LegendBordColor' => 'Bordure du titre',
'NbLettres' => 'Lettres avant coupure',
'Legend' => 'Titre du composant ("Editorial")',
'AlignInfosArticle' => 'Auteurs & dates'
);
?>
