<?php
// This is a SPIP language file
// ACS component specific

$GLOBALS[$GLOBALS['idx_lang']] = array(
'keyword' => '_Edito', /* Keyword to set an article as Editorial */
'edito' => 'Editorial',
'previous' => 'Previous editorials',
'pause' => 'Pause',
'suivant' => 'Next',
'precedent' => 'Previous',
);
?>
