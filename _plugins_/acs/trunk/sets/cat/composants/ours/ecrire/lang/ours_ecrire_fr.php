<?php
// Ceci est un fichier langue de SPIP-ACS

$GLOBALS[$GLOBALS['idx_lang']] = array(

'nom' => 'Ours',
'description' => 'Informations sur le site.',
'info' => 'Si une rubrique a le mot-cl&eacute; _Ours, un lien vers cette rubrique est affich&eacute;.',
'help' => 'Une rubrique avec le mot-cl&eacute; _Ours sert à rassembler vos informations &eacute;diteur.<br />Le lien vers les articles refus&eacute;s sert à rendre public les choix &eacute;ditoriaux.<br />L\'ours affiche en plus un lien vers les articles propos&eacute;s quand le composant "Democratie" est activ&eacute;.',

'Text' => 'Texte',
'Link' => 'Liens',
'LinkHover' => 'Au survol',
'Custom' => 'Pied de page personnalis&eacute; : ',

'liens' => 'Liens affich&eacute;s : ',
'LienCopyright' => 'Copyright',
'LienResume' => 'R&eacute;sum&eacute; en mode texte',
'LienRefus' => 'Articles refus&eacute;s',
'LienACS' => 'ACS',
'LienSPIP' => 'SPIP',
'LienRSS' => 'RSS',
'Stats' => 'Statistiques du site'
);
?>
