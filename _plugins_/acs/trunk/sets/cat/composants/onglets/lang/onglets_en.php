<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// ACS component specific - spécifique au composant ACS

$GLOBALS[$GLOBALS['idx_lang']] = array(
'sommaire' => 'A&nbsp;la&nbsp;Une',
'_sommaire' => 'A la Une breaking news',
'resume' => 'Top&nbsp;10',
'_resume' => 'Most popular articles and last messages',
'plan' => 'Site&nbsp;map',
'_plan' => 'All the site on one page',
'sites' => 'Links',
'_sites' => 'Other sites to discover',
'forums' => 'Forums',
'_forums' => 'Reactions, ideas, suggestions',
'publier' => 'Publish',
'_publier' => 'Publish on'
);
?>
