<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// ACS component specific - spécifique au composant ACS

$GLOBALS[$GLOBALS['idx_lang']] = array(
'sommaire' => 'A&nbsp;la&nbsp;Une',
'_sommaire' => 'A la Une',
'resume' => 'Top&nbsp;10',
'_resume' => 'Les articles les plus populaires et les derniers messages',
'plan' => 'Plan',
'_plan' => 'Tout le site sur une page',
'sites' => 'Liens',
'_sites' => 'Autres sites à découvrir',
'forums' => 'Forums',
'_forums' => 'Réactions, propositions',
'publier' => 'Publier',
'_publier' => 'Publier sur'
);
?>
