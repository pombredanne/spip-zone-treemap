<?php
// Ceci est un fichier langue de SPIP-ACS

$GLOBALS[$GLOBALS['idx_lang']] = array(

'nom' => 'Mail',
'description' => 'Formulaire d\'envoi de mail',
'info' => 'Affiche un formulaire d\'envoi d\'un mail &agrave; l\'auteur-e.',
'help' => 'Utilise le formulaire SPIP #FORMULAIRE_ECRIRE_AUTEUR.',

'TitreFond' => 'Titre'
);
?>
