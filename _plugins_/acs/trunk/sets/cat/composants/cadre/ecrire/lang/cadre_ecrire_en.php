<?php
// This is a SPIP-ACS language file  --  Ceci est un fichier langue de SPIP-ACS

$GLOBALS[$GLOBALS['idx_lang']] = array(

'nom' => 'Cadre',
'description' => 'Box and container for other components.',
'help' => 'Cadre define a box AND contain other components.',

'marge' => 'Margin',
'orientation' => 'Orientation',
'vertical' => 'vertical',
'horizontal' => 'horizontal',
'style' => 'Css style',
'PaddingLeft' => 'left',
'PaddingRight' => 'right',
'PaddingTop' => 'top',
'PaddingBottom' => 'bottom',
'WidthHelp' => 'Keep empty to get 100%'
);
?>
