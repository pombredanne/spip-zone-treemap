<?php
// This is a SPIP-ACS language file  --  Ceci est un fichier langue de SPIP-ACS

$GLOBALS[$GLOBALS['idx_lang']] = array(

'nom' => 'NavKeys',
'NavKeys' => 'Navigation by keywords menu.',
'info' => 'Display a menu composed with the logo of selected keywords or with optionnal icons.',
'help' => '',

'BordHover' => 'Hover',
'MarginHelp' => 'Margin between navigation button and its border.',
'p1' => 'Image&nbsp;1',
'p2' => 'Image&nbsp;2',
'p3' => 'Image&nbsp;3',
'p4' => 'Image&nbsp;4',
'p5' => 'Image&nbsp;5',
'p6' => 'Image&nbsp;6',
'p7' => 'Image&nbsp;7',
'p8' => 'Image&nbsp;8'
);
?>