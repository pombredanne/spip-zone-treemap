<?php
// This is a SPIP-ACS language file  --  Ceci est un fichier langue de SPIP-ACS

$GLOBALS[$GLOBALS['idx_lang']] = array(

'nom' => 'NavKeys',
'description' => 'Menu de navigation par mots-clés.',
'info' => 'Affiche un menu composé des logos de mot-clés sélectionnés ou de pictogrammes optionnels.',
'help' => '',

'BordHoverColor' => 'Au survol',
'MarginHelp' => 'Marge entre le bouton de navigation et sa bordure.',
'p1' => 'Image&nbsp;1',
'p2' => 'Image&nbsp;2',
'p3' => 'Image&nbsp;3',
'p4' => 'Image&nbsp;4',
'p5' => 'Image&nbsp;5',
'p6' => 'Image&nbsp;6',
'p7' => 'Image&nbsp;7',
'p8' => 'Image&nbsp;8'
);
?>