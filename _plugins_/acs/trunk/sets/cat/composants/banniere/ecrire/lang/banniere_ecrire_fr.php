<?php
// This is a SPIP-ACS language file  -  Ceci est un fichier langue de SPIP-ACS

$GLOBALS[$GLOBALS['idx_lang']] = array(

'nom' => 'Banni&egrave;re',
'description' => 'Banni&egrave;re qui change au survol, et logo du site, en option.',
'help' => 'Une banni&egrave;re mesure normalement 468x60 pixels. Au survol du pointeur, la bannière affiche alatoirement l\'une des images disponibles. Le nom du site est affich&eacute;  si aucune image n\'a &eacute;t&eacute; t&eacute;l&eacute;charg&eacute;e sur le serveur.<br />
Un lien vers une page particuli&egrave;re peut être affich&eacute; en option dans la banni&egrave;re, &eacute;ventuellement avec une image: c\'est le "lien extra".',

'Image' => 'Banni&egrave;re',
'FondImage' => 'Arri&egrave;re-plan',
'FondImageRepeatX' => 'Répèter l\'image en X',
'FondImageRepeatY' => 'Répèter l\'image en Y',
'FondAlign' => _T('acs:align'),
'FondVerticalAlign' => _T('acs:valign'),
'Hauteur' => 'Hauteur',
'Logo' => 'Afficher le logo du site',
'LogoAlign' => _T('acs:align'),
'TextColor'=>'Couleur texte',
'TextColorOver'=>'Au survol',
'extra' => 'Lien extra',
'ExtraLink' => 'URL',
'ExtraLabel' => 'Texte',
'ExtraImg' => 'Image',
'ExtraTop' => 'Top',
);
?>
