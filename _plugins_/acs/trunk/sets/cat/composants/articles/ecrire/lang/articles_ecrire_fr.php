<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'nom' => 'Articles',
'description' => 'Listes d\'articles, pour rubriques, une, r&eacute;sum&eacute;, plan ...',
'info' => 'Apparence des listes d\'articles d\'une rubrique, de la Une, du r&eacute;sum&eacute, du plan, associés à un mot-clé,...',
'help' => '
',

'Bord' => 'Bord sup&eacute;rieur',
'MargeBas' => 'Marge inf&eacute;rieure',
'NbLettres' => 'Nb de lettres avant coupure',
'LogoTailleMax' => 'Taille maxi du logo',
'Pagination' => 'Nombre d\'articles par page'
);
?>