<?php
// This is a SPIP-ACS language file  --  Ceci est un fichier langue de SPIP-ACS

$GLOBALS[$GLOBALS['idx_lang']] = array(

'nom' => 'Login',
'description' => 'Connection',
'info' => 'Formulaire de connection au site.',
'help' => 'Affiche soit un formulaire de connection soit les infos personnelles de l\'utilisateur connect&eacute;'
);
?>