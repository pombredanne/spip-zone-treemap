<?php
// This is a SPIP-ACS language file  --  Ceci est un fichier langue de SPIP-ACS

$GLOBALS[$GLOBALS['idx_lang']] = array(

'nom' => 'Login',
'description' => 'Connection',
'info' => 'Connection form',
'help' => 'Display a connection form or personal informations for connected users.'
);
?>