Les sous-dossiers cat et dist utilisent la règle "contrôle strict"  : 

« nul ne peut intervenir sur les fichiers de cette branche sans avoir au préalable envoyé 
un patch au format diff -pu aux responsables de la branche, et reçu de l'un d'eux un "OK" formel. »

Commit d'autres modèles ACS:
Tout administrateur d'une rubrique de documentation d'un modèle ACS sur le site http://acs.geomaticien.org peut
committer les changements du modèle qu'il administre dans son sous-dossier au nom de ce modèle.