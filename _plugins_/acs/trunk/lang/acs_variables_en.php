<?php
#              ACS
#          (Plugin Spip)
#     http://acs.geomaticien.org
#
# Copyright Daniel FAIVRE, 2007-2012
# Copyleft: licence GPL - Cf. LICENCES.txt

/**
 * Generic translations for common variables names, used BOTH in private area
 * AND by pencils (crayons of components)
 */
$GLOBALS[$GLOBALS['idx_lang']] = array(
  'use' => 'Use',

  'oui' => 'Yes',
  'non' => 'No',

	'fond' => 'Background',
	'titrefond' => 'Title&nbsp;background',

  'bordcolor' => 'Border',
  'bordlargeur' => 'border width',
  'bordstyle' => 'border style',
	'bordrond' => 'Rounded corner',
  'parent' => 'default value',
  'none' => 'no border. Equivalent to border-width: 0',
  'solid' => 'solid',
  'dashed' => 'dashed',
  'dotted' => 'dotted',
  'double' => 'double',
  'groove' => 'groove',
  'ridge' => 'ridge',
  'inset' => 'inset',
  'outset' => 'outset',

  'align' => 'Alignment',
	'valign' => 'vertical alignment',
	'margin' => 'Margin',
  'padding' => 'Padding',
	'left' => 'left',
	'center' => 'center',
	'right' => 'right',
	'top' => 'top',
	'bottom' => 'bottom',

	'font' => 'Font(s)',
	'fontsize' => 'Size',
	'fontfamily' => 'Font family',
	'text' => ' Text',
	'link' => 'Link',
	'linkhover' => 'Link hover',

	'key' => 'Keyword',

	'shadow' => 'Shadow',
  'shadowsize' => 'Size',
  'shadowblur' => 'Blur',

	'Nom' => 'Name',
	'nb' => 'Number',

	'vertical' => 'vertical',
	'horizontal' => 'horizontal'
);
?>
