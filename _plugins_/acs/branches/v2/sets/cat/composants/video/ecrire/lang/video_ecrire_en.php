<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'nom' => 'Video',
'description' => 'Video player.',
'info' => 'To embed the player in an article, add filter player to tag &lt;doc123&gt; of a flv document.<br /><br />
Examples:<br />
&lt;doc8|player&gt;<br />
&lt;doc123|player|autostart=yes&gt;<br />
&lt;doc123|player|w=640|h=480&gt;',
'help' => 'Preview display nothing since any video is uploaded on the site. Use html5 when possible and Flash player instead when not.<br />
Display thumbnail instead of title when player is loaded if the video document have a thumbnail.<br />
Video formats:<br />
- .flv : Flash video<br />
- .mp4 : video/mp4 codecs=avc1.42E01E, mp4a<br />
- .ogv : video/ogg codecs=theora, vorbis<br />
- .webm : video/webm codecs=vp8, vorbis
, ',

'mp3bgcolor' => 'Background',
'mp3Boutons' => 'Buttons',
'mp3BoutonsHover' => 'Over',
'flvbgcolor1' => 'Background up',
'flvbgcolor2' => 'Background bottom',
'Icons' => 'Icons',
'IconsHover' => 'Over',

'Download' => 'Download link'
);
?>