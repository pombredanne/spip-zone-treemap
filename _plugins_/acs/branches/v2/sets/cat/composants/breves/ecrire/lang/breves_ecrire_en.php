<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// ACS component specific - spécifique au composant ACS

$GLOBALS[$GLOBALS['idx_lang']] = array(
'nom' => 'News',
'description' => 'Block of last news.',
'help' => 'Usable only if Spip news are activated.',

'TitreFond' => 'Title',
'Nb' => 'Number'

);
?>
