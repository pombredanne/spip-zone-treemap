<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'nom' => 'Encart',
'description' => 'Blocks of last published articles or of last published articles in section(s) with selected keyword.',

'Type' => 'Type',
'TypeArticles' => 'articles',
'TypeRubriques' => 'sections',

'SouligneRub' => 'underline section',
'SouligneRubHeight' => 'Section underline height'
);
?>
