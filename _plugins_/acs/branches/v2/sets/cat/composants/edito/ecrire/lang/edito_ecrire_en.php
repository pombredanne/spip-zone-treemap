<?php
// This is a SPIP language file

$GLOBALS[$GLOBALS['idx_lang']] = array(

'nom' => 'Editorial',
'description' => 'An article &quot;a la Une&quot; according to a keyword.',
'help' => 'Keyword depends on lang. In english, it\'s "_Edito". Keyword is defined in component Edito lang\'s file.',

'FondImageRepeatX' => 'Background image repeat X',
'FondImageRepeatY' => 'Background image repeat Y',
'FondAlign' => 'Background image align',
'FondVerticalAlign' => 'Background image vertical align',
'LegendBordColor' => 'Title border',
'NbLettres' => 'Letters before cut',
'Legend' => 'Component title ("Editorial")',
'AlignInfosArticle' => 'Authors & dates'
);
?>
