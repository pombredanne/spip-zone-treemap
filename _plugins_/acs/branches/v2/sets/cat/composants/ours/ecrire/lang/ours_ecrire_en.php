<?php
// This is a SPIP-ACS language file

$GLOBALS[$GLOBALS['idx_lang']] = array(

'nom' => 'Ours',
'description' => 'Website informations.',
'info' => 'If a rubric has the keyword _Ours, a link to these rubric is displayed.',
'help' => 'A rubric with keyword _Ours is used to group your editor\'s informations.<br />Link to refused articles make publics eitorial choices.<br />Ours display also a link to proposed articles when "Democratie" component is used.',

'Text' => 'Text',
'Link' => 'Links',
'LinkHover' => 'Over',
'Custom' => 'Custom footer text : ',

'liens' => 'Displayed links : ',
'LienCopyright' => 'Copyright',
'LienResume' => 'Text abstract',
'LienRefus' => 'Refused texts',
'LienACS' => 'ACS',
'LienSPIP' => 'SPIP',
'LienRSS' => 'RSS',
'Stats' => 'Website statistics'
);
?>