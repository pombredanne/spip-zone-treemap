<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// ACS component specific - sp&eacute;cifique au composant ACS

$GLOBALS[$GLOBALS['idx_lang']] = array(
'stop' => 'Stop',
'play' => 'Lecture / pause',
'prev' => 'Pr&eacute;c&eacute;dent',
'next' => 'Suivant',

'audio' => 'Audio',
'download' => 'T&eacute;l&eacute;charger (Podcast)',
'article' => 'Lire',
'lire_article' => 'Lire l\'article contenant ce fichier audio',
'err_liste_vide' => 'Liste de lecture vide. Aucun document mp3 trouv&eacute;.'
);
?>
