<?php
// This is a SPIP-ACS language file  --  Ceci est un fichier langue de SPIP-ACS

$GLOBALS[$GLOBALS['idx_lang']] = array(

'nom' => 'Cadre',
'description' => 'Cadre et conteneur pour d\'autres composants.',
'help' => 'Cadre permet de définir des propriétés pour un cadre et d\'y glisser d\'autres composants.',

'marge' => 'Marge sur le bord',
'Orientation' => 'Orientation',
'OrientationVertical' => 'verticale',
'OrientationHorizontal' => 'horizontale',
'style' => 'Style CSS',
'PaddingLeft' => 'gauche',
'PaddingRight' => 'droit',
'PaddingTop' => 'haut',
'PaddingBottom' => 'bas',
'WidthHelp' => 'Laisser vide pour une largeur de 100%'
);
?>
