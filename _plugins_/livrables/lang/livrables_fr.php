<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'item_mots_cles_association_composant' => 'aux composants d\'un projet',
	'item_mots_cles_association_livrables' => 'aux livrables d\'un projet',
	'action' => 'Action',
	'ajouter_livrable' => 'Ajouter un livrable',
	
	// C
	'composant' => 'Composant',
	'composants' => 'Composants',
	'confirmer_supprimer_composant' => '&Ecirc;tes-vous sur de vouloir supprimer ce composant ?',
	'confirmer_supprimer_livrable' => '&Ecirc;tes-vous sur de vouloir supprimer ce livrable ?',

	// D
	'descriptif' => 'Descriptif',
	
	// E
	'editer_livrable' => 'Editer un livrable',
	'erreur_echec_enregistrement' => 'Une erreur s\'est produite pendant l\'enregistrement du livrable.',
	'erreur_manque_descriptif' => 'Il manque le descriptif du livrable.',
	'erreur_manque_projet' => 'Il manque le projet du livrable.',
	'erreur_manque_titre' => 'Il manque le titre du livrable.',
	'erreur_manque_url' => 'Il manque l\'url du livrable.',
	'erreur_meme_livrable' => 'Erreur : c\'est le même livrable (modification impossible).',
	"explication_statut_non_livre"	=> "Pas encore fabriqu&eacute;, livraison non effectu&eacute;e.",
	"explication_statut_non_vue"	=> "Livrable non vérifié; peut-etre pas livré ou simplement pas encore vu.",
	"explication_statut_alerte"	=> "Alerte, difficult&eacute;s rencontr&eacute;es.",
	"explication_statut_dev"	=> "En cours de d&eacute;veloppement; la recette n'est pas encore demand&eacute;e.",
	"explication_statut_test"	=> "Recette en cours ou demand&eacute;e.",
	"explication_statut_avec_bugs"	=> "Livraison effectu&eacute; mais bugs &agrave; corriger.",
	"explication_statut_prod"	=> "Livraison effectu&eacute;, en production.",
	"explication_statut_accepte"	=> "Valid&eacute;, accept&eacute;, recett&eacute;.",
	"explication_statut_abandonne"	=> "D&eacute;veloppement arr&ecirc;t&eacute; ou abandonn&eacute;.",
	
	// I
	'info_tous_les_composants'	=> 'Tous les composants',
	'info_tous_les_livrables'	=> 'Tous les livrables',
	
	// L
	'label_descriptif' 			=> 'Descriptif du livrable',
	'label_livrable' 			=> 'ID',
	'label_projet' 				=> 'Projet',
	'label_projet_lot' 			=> 'Projet/Lot',
	'label_titre' 				=> 'Titre du livrable',
	'label_tickets_total' 		=> 'Total Tickets', // 'Total des tickets du livrable',
	'label_tickets_finis' 		=> 'Tickets Finis', // 'Nombe de tickets termin&eacute;s',
	'label_tickets_en_cours' 	=> 'En cours', // 'Nombre de tickets restant à traiter',
	'label_type'				=> 'Type page', // 'Type de livrable : page ou objet editorial',
	'label_objet'				=> 'Objet &eacute;dit.', // 'Adresse du livrable',
	'label_compo'				=> 'Nom compo.', // 'Adresse du livrable',
	'label_url'					=> 'URL de test', // 'Adresse du livrable',
	'label_ref'					=> 'URL de r&eacute;f.', // 'Adresse de la reference ou modele qui permettra de construire le livrable',
	'label_statut_client' 		=> 'Statut Client',
	'label_statut_atelier' 		=> 'Statut Atelier',
	'libelle_compo_neant'		=> 'Dist', // 'Par d&eacute;faut', // 'Dist (aucune composition).',
	"libelle_statut_non_livre"	=> "Non livr&eacute;",
	"libelle_statut_non_vue"	=> "Pas encore v&eacute;rifi&eacute;",
	"libelle_statut_alerte"		=> "Alerte",
	"libelle_statut_dev"		=> "En cours de d&eacute;veloppement",
	"libelle_statut_test"		=> "&Agrave; tester",
	"libelle_statut_avec_bugs"	=> "Bugs &agrave; corriger",
	"libelle_statut_prod"		=> "En production",
	"libelle_statut_accepte"	=> "Livr&eacute; et accept&eacute;",
	"libelle_statut_abandonne"	=> "Abandonn&eacute;",
	'libelle_type_page'			=> "Page",
	'libelle_type_rubrique'		=> "Rubrique",
	'libelle_type_article'		=> "Article",
	'libelle_type_auteur'		=> "Auteur",
	'libelle_type_vide'			=> "-",
	'libelle_type_neant	'		=> "N&eacute;ant",
	'lier_livrable' 			=> 'Lier à un livrable',
	'livrable' 					=> 'Livrable',
	'livrable_non_trouve' 		=> 'Livrable non trouvé ou inexistant',
	'livrables' 				=> 'Livrables',

	// N
	'nom_bouton_plugin' => 'Livrables',
	
	// O
	'objet'	=> 'Objet',
	'objet_composant' => 'Composant',
	'objet_livrable' => 'Livrable',

	// S
	'selectionner_livrable' => 'Sélectionner un livrable',
	'succes_enregistrement' => 'Le livrable @id@ a bien été créé.',
	'succes_modification' => 'Le livrable @id@ a bien été modifié.',
	'supprimer' => 'Supprimer ce livrable',
	
	// T
	'titre' => 'Titre',
	'type' => 'Type',
	
	// U
	
	// V
	'voir_composant' 	=> 'D&eacute;tails',
	'voir_modele'		=> 'Voir une r&eacute;f&eacute;rence du livrable',
	'voir_en_ligne'		=> 'Voir en ligne',
	'voir_livrable' 	=> 'D&eacute;tails',

);

?>
