<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

  // B
  'BBcode_SPIP' => 'BBcode (PhpBB) vers SPIP',

  // C
  'convertir_titre' => 'Convertisseur de formats',
  'convertir_desc' => 'Permet de convertir des formats divers (wiki, docx, pdf, phpBB) en format SPIP',
  'convertir' => 'Convertir',
  'convertir_en' => 'R&eacute;sultat de la conversion: ',
  'convertir_utf' => 'convertir en UTF-8',
  
  // D
  'DOCX_SPIP' => 'DOCX (Word 2007) vers SPIP',
  'DotClear_SPIP'=> 'DotClear vers SPIP', 
  
  // E
  'erreur_extracteur' => 'Erreur : l\'extracteur n\'a pas fonctionn&eacute;',
 
  // F
  'from' => 'De ',
  
  // H
  'html_SPIP' => 'HTML vers SPIP',
    
  // M
  'MediaWiki_SPIP' => 'Wiki (MediaWiki) vers SPIP', 
  'MoinWiki_SPIP' => 'Wiki (MoinWiki) vers SPIP',
  
  // P
  'options' => 'Options :',
  
  // S
  'SLA_SPIP' => 'SLA (Scribus) vers SPIP',
  'SPIP_txt' => 'SPIP vers texte brut',
  'SPIP_mediawiki' => 'SPIP vers Wiki (MediaWiki)',
  
  // T
  'texte_a_convertir' => 'Votre texte &agrave; convertir :',
  'texte_a_copier' => 'Copiez-le ci-dessous :',
  'texte_fichier' => 'ou choisissez un fichier :',
    
  
  // U 
  'unknown_format' => 'Erreur: format inconnu',
  
  // X
  'XTG_SPIP' => 'XTG (XPressTags) vers SPIP' 

);

?>