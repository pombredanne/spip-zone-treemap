<?php

// FICHIER DE TRADUCTION DES PAGES PRIVEES « VU ! »
// Pour les caractères spéciaux HTML :
// Attention à échapper les guillements par backspace : "l'apostrophe" -> "l\'apostrophe"
// Info : pour l'espace insecable : &nbsp;

// Pour la traduction de la partie publique voir local_**.php

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
// A
	'alt_img_logo' => 'Logo du plugin Vu!',
	'apercu_chez' => ' chez ',
	'apercu_de' => 'De ',
	'apercu_le' => ' le ',
	'apercu_lien_annonce' => '&Agrave; voir sur&nbsp;',
	'apercu_lien_evenement' => ' organisation&nbsp;',
	'apercu_informations' => 'Plus d\'informations sur&nbsp;',
	'apercu_peremption' => 'Annonce valable jusqu\'au ',
	'apercu_publie' => 'Publi&eacute;',
	'apercu_source' => 'source : ',

// C
	'cfg_descriptif' => '<h4>Configurer le plugin Vu !</h4> <p>Vous pouvez ici choisir quels sont les types d\'objets actifs ainsi que les champs optionnels &agrave; utiliser pour chacun d\'entre eux.</p><p><b>Par défaut</b>, tous les objets et tous les champs optionnels sont activ&eacute;s.</p><p><b>Nota : </b>d&eacute;sactiver un objet ou un champ n\'a aucune incidence sur le contenu de la base de donn&eacute;e. On se contente simplement de retirer les formulaires superflux de l\'interface priv&eacute;e. Par cons&eacute;quent, quelque soit le param&eacute;trage choisi le contenu reste disponible depuis l\'interface publique, si le squelette le permet.</p><p><a href="http://www.spip-contrib.net/Vu-la-documentation" class="spip_out">Documentation</a>',
	'cfg_titre' => 'Vu !',
	'cfg_soustitre1' => 'Les objets actifs',
	'cfg_soustitre2' => 'Les champs optionnels',
	'cfg_explication1' => 'Le plugin <i>Vu!</i> propose (actuellement) trois types d\'objets pour relayer de l\'information : les annonces, les &eacute;v&eacute;nements et les publications. Vous avez ici la possibilit&eacute; de choisir quels objets seront utilis&eacute;s sur votre site.',
	'cfg_explication2' => 'En plus des champs obligatoires (requis pour une utilisation minimale de chacun des objets <i>Vu!</i>), quelques champs optionnels sont ici mis &agrave; disposition afin que vous puissiez adapter plus finement le plugin &agrave; votre site.',
	'cfg_concernant_annonce' => 'Concernant les annonces',
	'cfg_concernant_evenement' => 'Concernant les événements',	
	'cfg_concernant_publication' => 'Concernant les publications',
	'cfg_label_type' => 'Type :',
	'cfg_label_descriptif' => 'Descriptif :',
	'cfg_label_source' => 'Source :',
	'cfg_label_langue' => 'Langue :',
	'cfg_label_annonce' => 'Les annonces :',
	'cfg_label_evenement' => 'Les &eacute;v&eacute;nements :',
	'cfg_label_publication' => 'Les publications :',
	'cfg_zero_objet' => '<b>Aucun objet de veille n\'est actuellement activ&eacute;.</b><br />Pour rem&eacute;dier &agrave ce probl&egrave;me, rendez-vous sur la page de <a href="?exec=cfg&cfg=vu">configuration</a>.',


// D
	'description_plugin' => 'Relayez l\'information plutôt que de la (re)publier !',

// E
	'entree_annonce_publiee' => 'Cette annonce doit-elle &ecirc;tre publi&eacute;e ?',
	'entree_evenement_publie' => 'Cet &eacute;v&eacute;nement doit-il &ecirc;tre publi&eacute; ?',
	'entree_publication_publiee' => 'Cette publication doit-elle &ecirc;tre publi&eacute;e ?',
	'etiquette_formulaire_annonce' => 'Modifier l\'annonce :',
	'etiquette_formulaire_evenement' => 'Modifier l\'&eacute;v&eacute;nement&nbsp;:',
	'etiquette_formulaire_publication' => 'Modifier la publication&nbsp;:',
	'explication_date' => 'La date doit &ecirc;tre au format \'aaaa-mm-jj\'.',

// H
	'html_title' => 'Veille',

// I
	'icone_modifier_annonce' => 'Modifier l\'annonce',
	'icone_modifier_evenement' => 'Modifier l\'&eacute;v&eacute;nement',
	'icone_modifier_publication' => 'Modifier la publication',
	'icone_precedent_annonce' => 'Annonce pr&eacute;c&eacute;dente',
	'icone_precedent_evenement' => '&Eacute;v&eacute;nement pr&eacute;c&eacute;dent',
	'icone_precedent_publication' => 'Publication pr&eacute;c&eacute;dente',
	'icone_suivant_annonce' => 'Annonce suivante',
	'icone_suivant_evenement' => '&Eacute;v&eacute;nement suivant',
	'icone_suivant_publication' => 'Publication suivante',
	'info_annonce_publiee' => 'Cette annonce doit-elle &ecirc;tre publi&eacute;e ?',
	'info_annonceur' => 'Annonceur :',
	'info_auteur' => 'Auteur(s) :',
	'info_date' => 'Date&nbsp;:',
	'info_date_evenement' => 'Date de l\'&eacute;v&eacute;nement&nbsp;:',
	'info_descriptif' => 'Descriptif :',
	'info_editeur' => '&Eacute;diteur :',
	'info_evenement_publie' => 'Cette &eacute;v&eacute;nement doit-il &ecirc;tre publi&eacute;&nbsp;?',
	'info_gauche_numero_annonce' => 'ANNONCE NUM&Eacute;RO',
	'info_gauche_numero_evenement' => '&Eacute;V&Eacute;NEMENT NUM&Eacute;RO',
	'info_gauche_numero_publication' => 'PUBLICATION NUM&Eacute;RO',
	'info_langpub' => 'Langue de publication :',
	'info_legend_source' => 'Source',
	'info_legend_statut' => 'Statut',
	'info_lien' => 'Lien : ',
	'info_lieu' => 'Lieu :',
	'info_lieu_evenement' => 'Lieu de l\'&eacute;v&eacute;nement&nbsp;:',
	'info_organisateur' => 'Organisateur :',
	'info_peremption' => 'Date de p&eacute;remption&nbsp;:',
	'info_publication_publiee' => 'Cette publication doit-elle &ecirc;tre publi&eacute;e&nbsp;?',
	'info_source' => 'Source : ',
	'info_source_nom' => 'Nom :',
	'info_source_lien' => 'Lien :',
	'info_titre' => 'Titre :',
	'info_type' => 'Type :',
	'info_vu_annonce' => 'Annonces',
	'info_vu_libelle_annonce' => 'Annonces relay&eacute;es',
	'info_vu_libelle_evenement' => '&Eacute;v&eacute;nements relay&eacute;s',
	'info_vu_libelle_publication' => 'Publications relay&eacute;es',
	'item_annonce_proposee' => 'Annonce propos&eacute;e',
	'item_annonce_refusee' => 'NON - Annonce refus&eacute;e',
	'item_annonce_validee' => 'OUI - Annonce valid&eacute;e',
	'item_annonce_poubelle' => '&agrave; la poubelle',
	'item_evenement_propose' => '&Eacute;v&eacute;nement propos&eacute;',
	'item_evenement_refuse' => 'NON - &Eacute;v&eacute;nement refus&eacute;',
	'item_evenement_valide' => 'OUI - &Eacute;v&eacute;nement valid&eacute;',
	'item_evenement_poubelle' => '&agrave; la poubelle',
	'item_mots_cles_association_annonces' => 'aux annonces relay&eacute;es (Vu!)',
	'item_mots_cles_association_evenements' => 'aux &eacute;v&eacute;nements relay&eacute;s (Vu!)',
	'item_mots_cles_association_publications' => 'aux publications relay&eacute;es (Vu!)',
	'item_publication_proposee' => 'Publication propos&eacute;e',
	'item_publication_refusee' => 'NON - Publication refus&eacute;e',
	'item_publication_validee' => 'OUI - Publication valid&eacute;e',
	'item_publication_poubelle' => '&agrave; la poubelle',

// L
	'liste_annonces' => 'Annonces',
	'liste_evenements' => '&Eacute;v&eacute;nements',
	'liste_publications' => 'Publications',

// N
	'naviguer_titre' => 'Veille',

// R
	'raccourcis_annonce' => 'Nouvelle annonce',
	'raccourcis_evenement' => 'Nouvel &eacute;v&eacute;nement',
	'raccourcis_publication' => 'Nouvelle publication',
	// Pour les flux RSS, on preferera indiquer les caracteres speciaux en code ISO plutot
	// que HTML. Sinon, ca plante...
	'rss_apercu_lien_annonce' => '&#192;&#160;voir&#160;sur&#160;',
	'rss_apercu_source' => 'source&#160;:&#160;',
	'rss_apercu_peremption' => 'valable&#160;jusqu\'au&#160;',
	'rss_apercu_publie' => 'Publi&#233;',
	'rss_apercu_lien_evenement' => '&#160;organisation&#160;',
	'rss_apercu_du' => 'du&#160;',
	'rss_apercu_informations' => 'Plus&#160;d\'informations&#160;sur&#160;',
	'rss_objet_annonce' => 'Annonce',
	'rss_objet_evenement' => '&#201;v&#233;nement',
	'rss_objet_publication' => 'Publication',

// S
	'spiplistes_info_veille' => 'info_veille',

// T
	'titre_enattente_annonces' => 'Annonces propos&eacute;es (Vu !)',
	'titre_enattente_evenements' => '&Eacute;v&eacute;nements propos&eacute;s (Vu !)',
	'titre_enattente_publications' => 'Publications propos&eacute;es (Vu !)',
	'titre_nouvelle_evenement' => 'Nouvel &eacute;v&eacute;nement',
	'titre_nouvelle_annonce' => 'Nouvelle annonce',
	'titre_nouvelle_publication' => 'Nouvelle publication',
	'titre_page_annonces_edit' => '&Eacute;diter une annonce',
	'titre_page_evenements_edit' => '&Eacute;diter un &eacute;v&eacute;nement',
	'titre_page_publications_edit' => '&Eacute;diter une publication',
	'titre_cartouche_accueil_annonces' => 'Annonces (Vu !)',
	'titre_cartouche_accueil_evenements' => '&Eacute;v&eacute;nements (Vu !)',
	'titre_cartouche_accueil_publications' => 'Publications (Vu !)',


// Compatibilite plugin Corbeille
	'corbeille_annonces_tous' => "@nb@ annonces (Vu !) dans la corbeille",
	'corbeille_annonces_un' => "1 annonce (Vu !) dans la corbeille",
	'corbeille_evenements_tous' => "@nb@ &eacute;v&eacute;nements (Vu !) dans la corbeille",
	'corbeille_evenements_n' => "1 &eacute;v&eacute;nement (Vu !) dans la corbeille",
	'corbeille_publications_tous' => "@nb@ publications (Vu !) dans la corbeille",
	'corbeille_publications_un' => "1 publication (Vu !) dans la corbeille",
);

?>
