<?php

// FICHIER DE TRADUCTION DES PAGES PRIVEES « VU ! »
// Pour les caractères spéciaux HTML :
// Attention à échapper les guillements par backspace : "l'apostrophe" -> "l\'apostrophe"
// Info : pour l'espace insecable : &nbsp;

// Pour la traduction de la partie publique voir local_**.php

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
// A
	'alt_img_logo' => 'Logo du plugin Vu!',
	'apercu_chez' => ' chez ',
	'apercu_de' => 'De ',
	'apercu_le' => ' le ',
	'apercu_lien_annonce' => '&Agrave; voir sur&nbsp;',
	'apercu_lien_evenement' => ' organisation&nbsp;',
	'apercu_informations' => 'Plus d\'informations sur&nbsp;',
	'apercu_peremption' => 'Annonce valable jusqu\'au ',
	'apercu_publie' => 'Publi&eacute;',
	'apercu_source' => 'source : ',

// D
	'description_plugin' => 'Relayez l\'information plutôt que de la (re)publier !',

// E
	'entree_annonce_publiee' => 'Cette annonce doit-elle &ecirc;tre publi&eacute;e ?',
	'entree_evenement_publie' => 'Cet &eacute;v&eacute;nement doit-il &ecirc;tre publi&eacute; ?',
	'entree_publication_publiee' => 'Cette publication doit-elle &ecirc;tre publi&eacute;e ?',
	'etiquette_formulaire_annonce' => 'Modifier l\'annonce :',
	'etiquette_formulaire_evenement' => 'Modifier l\'&eacute;v&eacute;nement&nbsp;:',
	'etiquette_formulaire_publication' => 'Modifier la publication&nbsp;:',
	'explication_date' => 'La date doit &ecirc;tre au format \'aaaa-mm-jj\'.',

// H
	'html_title' => 'Veille',

// I
	'icone_modifier_annonce' => 'Modifier l\'annonce',
	'icone_modifier_evenement' => 'Modifier l\'&eacute;v&eacute;nement',
	'icone_modifier_publication' => 'Modifier la publication',
	'icone_precedent_annonce' => 'Annonce pr&eacute;c&eacute;dente',
	'icone_precedent_evenement' => '&Eacute;v&eacute;nement pr&eacute;c&eacute;dent',
	'icone_precedent_publication' => 'Publication pr&eacute;c&eacute;dente',
	'icone_suivant_annonce' => 'Annonce suivante',
	'icone_suivant_evenement' => '&Eacute;v&eacute;nement suivant',
	'icone_suivant_publication' => 'Publication suivante',
	'info_annonce_publiee' => 'Cette annonce doit-elle &ecirc;tre publi&eacute;e ?',
	'info_annonceur' => 'Annonceur :',
	'info_auteur' => 'Auteur(s) :',
	'info_date' => 'Date&nbsp;:',
	'info_date_evenement' => 'Date de l\'&eacute;v&eacute;nement&nbsp;:',
	'info_descriptif' => 'Descriptif :',
	'info_editeur' => '&Eacute;diteur :',
	'info_evenement_publie' => 'Cette &eacute;v&eacute;nement doit-il &ecirc;tre publi&eacute;&nbsp;?',
	'info_gauche_numero_annonce' => 'ANNONCE NUM&Eacute;RO',
	'info_gauche_numero_evenement' => '&Eacute;V&Eacute;NEMENT NUM&Eacute;RO',
	'info_gauche_numero_publication' => 'PUBLICATION NUM&Eacute;RO',
	'info_langpub' => 'Langue de publication :',
	'info_legend_source' => 'Source',
	'info_legend_statut' => 'Statut',
	'info_lien' => 'Lien : ',
	'info_lieu' => 'Lieu :',
	'info_lieu_evenement' => 'Lieu de l\'&eacute;v&eacute;nement&nbsp;:',
	'info_organisateur' => 'Organisateur :',
	'info_peremption' => 'Date de p&eacute;remption&nbsp;:',
	'info_publication_publiee' => 'Cette publication doit-elle &ecirc;tre publi&eacute;e&nbsp;?',
	'info_source' => 'Source : ',
	'info_source_nom' => 'Nom :',
	'info_source_lien' => 'Lien :',
	'info_titre' => 'Titre :',
	'info_type' => 'Type :',
	'info_vu_annonce' => 'Annonces',
	'info_vu_libelle_annonce' => 'Annonces relay&eacute;es',
	'info_vu_libelle_evenement' => '&Eacute;v&eacute;nements relay&eacute;s',
	'info_vu_libelle_publication' => 'Publications relay&eacute;es',
	'item_annonce_proposee' => 'Annonce propos&eacute;e',
	'item_annonce_refusee' => 'NON - Annonce refus&eacute;e',
	'item_annonce_validee' => 'OUI - Annonce valid&eacute;e',
	'item_annonce_poubelle' => '&agrave; la poubelle',
	'item_evenement_propose' => '&Eacute;v&eacute;nement propos&eacute;',
	'item_evenement_refuse' => 'NON - &Eacute;v&eacute;nement refus&eacute;',
	'item_evenement_valide' => 'OUI - &Eacute;v&eacute;nement valid&eacute;',
	'item_evenement_poubelle' => '&agrave; la poubelle',
	'item_mots_cles_association_annonces' => 'aux annonces relay&eacute;es (Vu!)',
	'item_mots_cles_association_evenements' => 'aux &eacute;v&eacute;nements relay&eacute;s (Vu!)',
	'item_mots_cles_association_publications' => 'aux publications relay&eacute;es (Vu!)',
	'item_publication_proposee' => 'Publication propos&eacute;e',
	'item_publication_refusee' => 'NON - Publication refus&eacute;e',
	'item_publication_validee' => 'OUI - Publication valid&eacute;e',
	'item_publication_poubelle' => '&agrave; la poubelle',

// L
	'liste_annonces' => 'Annonces',
	'liste_evenements' => '&Eacute;v&eacute;nements',
	'liste_publications' => 'Publications',

// N
	'naviguer_titre' => 'Veille',

// R
	'raccourcis_annonce' => 'Nouvelle annonce',
	'raccourcis_evenement' => 'Nouvel &eacute;v&eacute;nement',
	'raccourcis_publication' => 'Nouvelle publication',

// S
	'spiplistes_info_veille' => 'info_veille',

// T
	'titre_nouvelle_evenement' => 'Nouvel &eacute;v&eacute;nement',
	'titre_nouvelle_annonce' => 'Nouvelle annonce',
	'titre_nouvelle_publication' => 'Nouvelle publication',
	'titre_page_annonces_edit' => '&Eacute;diter une annonce',
	'titre_page_evenements_edit' => '&Eacute;diter un &eacute;v&eacute;nement',
	'titre_page_publications_edit' => '&Eacute;diter une publication',


// Compatibilite plugin Corbeille
	'corbeille_annonces_tous' => "@nb@ annonces (Vu !) dans la corbeille",
	'corbeille_annonces_un' => "1 annonce (Vu !) dans la corbeille",
	'corbeille_evenements_tous' => "@nb@ &eacute;v&eacute;nements (Vu !) dans la corbeille",
	'corbeille_evenements_n' => "1 &eacute;v&eacute;nement (Vu !) dans la corbeille",
	'corbeille_publications_tous' => "@nb@ publications (Vu !) dans la corbeille",
	'corbeille_publications_un' => "1 publication (Vu !) dans la corbeille",
);

?>
