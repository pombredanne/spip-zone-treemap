<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Traduit de l'Idiot Moderne par Scoty (06/2007)

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
'admin_de_pt' => 'Admin de ..',
'admin' => 'Admin',
'admin_restreints_' => 'Admins (restreints)',
'admin_s' => 'Admins',
'arborescence_' => 'Arborescence ',
'arbre_rubriques' => 'Arbre Rubriques',
'article' => 'Article',
'article_publies' => 'Articles publi&eacute;s',
'article_c' => 'Art.', ## 2.5
'article_s' => 'Articles',
'auucn_mot_sur_site' => 'Aucun Mot-clef cr�� sur ce site !', ## 2.5
'auteurs_enregistre_s' => 'Auteurs enregistr&eacute;s',
'auteur_s' => 'Auteurs',
'autre_s' => 'Autres',

// B
'breve_s' => 'Br&egrave;ves',

// C
'credits' => 'Tableau de Bord 2.6<br />
				(spip 1.9.2) Mise en plugin :<br />
				juin 07 - <a href="http://www.koakidi.com/">Scoty . koakidi.com</a><br />
				Sur une base de Marabeh (2003).<br /><br />
				"Validation sites" :<br />d\'apr&egrave;s une id&eacute;e de Matthieu Onfray et scripts de 
				<a href="http://www.categorizator.org/">Categorizator</a>.',
'cree_le' => 'Cr&eacute;&eacute; le',

// D
'document_s' => 'Documents',
'document_s_' => 'Documents ',

// E
'efface_s' => 'Effac&eacute;s',
'enreg_table_metas' => '@nb_metas@ enregistrements dans la table @prefix@_meta.', ## 2.6
'enregistrement_s' => 'Enregistrements',
'espace_disque' => 'Espace disque',
'espace_disque_var' => 'Espace disque : @taille@',

// F
'fichier' => 'Fichier',
'fichier_distant' => 'Fichier distant',
'forum' => 'Forum', ## 2.5
'forum_s' => 'Forums',

// G
'groupe' => 'Groupe',
'groupe_id_' => 'Groupe @id_groupe@ ',
'groupes_mot_s_' => 'Groupes Mots : ',

// I
'ici'=> 'ici',
'id_mjsc' => 'ID',
'impossible_trouver_spip' => 'Impossible de trouver les fichiers SPIP',
'info_mesure_espace_disque' => '(mesure &agrave; 5% pr&egrave;s en fonction du formatage du serveur)',
'interne_s' => 'Internes',
'intitule' => 'Intitul&eacute;',

// L
'larg_x_haut' => 'Larg x Haut',
'leg_art_nbsp' => 'Art. :&nbsp;',
'ligne_s_' => 'Lignes ',
'liste_article_s' => 'Liste Articles',
'liste_auteur_s' => 'Liste Auteurs',
'liste_breve_s' => 'Liste Br&egrave;ves',
'liste_document_s' => 'Liste Documents',
'liste_mot_s' => 'Liste Mots',
'liste_petition_s' => 'Liste P&eacute;titions',
'liste_rubrique_s' => 'Liste Rubriques',
'listes_sites_sur_' => 'Listes des sites sur ',
'liste_site_s' => 'Liste Sites (validator)',

// M
'message_s' => 'Messages',
'message_validation' => 'Message du test de validation',
'mode' => 'Mode',
'mode_document' => 'Mode Document',
'mode_vignette' => 'Mode Vignette',
'mot_clef_s' => 'Mots-clefs',
'msg_pas connection' => 'Impossible de se connecter',
'msg_url_non_conforme' => 'URL non conforme !',
'msg_page_site' => 'Page du site',
'msg_champ_incomplet' => 'Champ URL incomplet (vide !)',
'msg_delais_depasse' => 'D&eacute;lais d&eacute;pass&eacute;, revenir !',
'msg_ok' => 'OK',
'msg_page_vide' => 'Page vide',
'msg_contenu_partiel' => 'Contenu partiel de la page',
'msg_deplacee_definitif' => 'Page d&eacute;plac&eacute;e d&eacute;finitivement',
'msg_deplacee_tempo' => 'Page d&eacute;plac&eacute;e momentan&eacute;ment',
'msg_erreur_requete_http' => 'Erreur dans la requ&ecirc;te HTTP',
'msg_authentif_requise' => 'Authentification requise',
'msg_acces_payant' => 'Acc&egrave;s &agrave; la page : payant',
'msg_acces_refuse' => 'Acc&egrave;s &agrave; la page : refus&eacute;',
'msg_inexistante' => 'Page inexistante',
'msg_erreur_interne_serveur' => 'Erreur interne au serveur',
'msg_erreur_passerelle' => 'Erreur Passerelle Serveur',
'msg_erreur_code' => 'Erreur non trait&eacute;e, num&eacute;ro : @code@ !',

// N
'nom' => 'Nom',
'nombre' => 'Nombre',
'non' => 'non',
'non_def' => 'non d&eacute;fini',

// O
'obligatoire_c' => 'Oblig.',
'origine' => 'Origine',
'oui' => 'oui',

// P
'parent' => 'Parent',
'pas_elem_sur_site' => 'Pas d\'&eacute;l&eacute;ment de ce type sur le site !',
'pas_doc_sur_site' => 'Pas de Document sur le site !',
'pas_mot_cle_sur_site' => 'Aucun Mot-clef cr&eacute;&eacute; sur ce site !',
'pas_petitions_sur_site' => 'Pas de p&eacute;titions sur ce site !',
'petition_s' => 'P&eacute;titions',
'propose_s' => 'Propos&eacute;s',
'public_s' => 'Publics',
'publiee_s' => 'Publi&eacute;es',
'publiee_s_non' => 'Non publi&eacute;es',

// R
'redacteur_c' => 'R&eacute;dac.', ## 2.5
'redacteur_s' => 'R&eacute;dacteurs',
'repertoire' => 'R&eacute;pertoire',
'rubrique' => 'Rubrique',
'rubrique_c' => 'Rub.',
'rubrique_s' => 'Rubriques',

// S
'secteur' => 'Secteur',
'signature_s' => 'Signatures',
'site' => 'Site',
'sites_references' => 'Sites R&eacute;f&eacute;renc&eacute;s',
'statut' => 'Statut',
'statut_pt' => 'Statut ..',
'syndic_c' => 'Syndic.', ## 2.5
'syndique' => 'Syndiqu&eacute;',

// T
'table' => 'Table',
'table_metas' => 'Table des metas', ## 2.6
'tables_non_spip' => ' : Tables non SPIP (Plugins ...)',
'taille' => 'Taille',
'taille_base' => 'Taille Base',
'taille_base_donnees' => 'Taille Base de donn&eacute;es : @taille@',
'taille_tables_plug' => 'Dont tables des plugins (...) : @taille_plug@', ## 2.5
'titre' => 'Titre',
'titre_plugin' => 'Tableau de bord',
'total' => 'Total',
'total_signatures_' => 'Total Signatures : ',
'toutes_rubriques' => 'Toutes rubriques.',
'tri_par_date' => 'Trier par date',## 2.5
'tri_par_groupe' => 'Trier par groupe', ## 2.5
'tri_par_id' => 'Trier par ID', ## 2.5
'tri_par_nom' => 'Trier par nom',## 2.5
'tri_par_parent' => 'Trier par Parent',## 2.5
'tri_par_titre' => 'Trier par titre', ## 2.5
'type' => 'Type',
'types_de_docs_' => 'Types de Documents ',
'types_pages' => 'Types de pages',

// U
'unique' => 'Unique',
'url_sans_nom' => '<i>-- lien sans nom</i>',

// V
'verifier_sites_valides' => 'V&eacute;rifier la validit&eacute; des sites de la listes',
'vignette_associee' => 'Vignette associ&eacute;e',
'visiteur_s' => 'Visiteurs',

// Z
'z' => 'z'
);
?>
