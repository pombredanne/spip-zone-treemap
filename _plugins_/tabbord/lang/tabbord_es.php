<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Traduit de l'Idiot Moderne par Scoty (06/2007)
// Traducci�n Espa�ol, JSJ, 1-5-07

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
'admin_de_pt' => 'Admin de ..',
'admin' => 'Admin',
'admin_restreints_' => 'Admins (restringidos)',
'admin_s' => 'Admins',
'arborescence_' => 'Arborescencia ',
'arbre_rubriques' => 'Arbol de Secciones',
'article' => 'Art&iacute;culo',
'article_publies' => 'Art&iacute;cles publicados',
'article_c' => 'Art.', ## 2.5
'article_s' => 'Art&iacute;culos',
'aucun_mot_sur_site' => 'no palabra clave creada en este sitio !', ## 2.5
'auteurs_enregistre_s' => 'Auteres registrados',
'auteur_s' => 'Autores',
'autre_s' => 'Otros',

// B
'breve_s' => 'Breves',

// C
'credits' => 'Panel de Control (Tableau de Bord) 2.6<br />
				(spip 1.9.2) Puesta en plugin :<br />
				junio 07 - <a href="http://www.koakidi.com/">Scoty . koakidi.com</a><br />
				Sobre una base de Marabeh (2003).<br /><br />
				"Validaci&oacute;n de sitios" :<br />a partir de una idea de Matthieu Onfray y scripts de 
				<a href="http://www.categorizator.org/">Categorizador</a>.',
'cree_le' => 'Creado el',

// D
'document_s' => 'Documentos',
'document_s_' => 'Documentos ',

// E
'efface_s' => 'Borrados',
'enreg_table_metas' => '@nb_metas@ registros en la tabla @prefix@_meta.', ## 2.6
'enregistrement_s' => 'Registros',
'espace_disque' => 'Espacio en disco',
'espace_disque_var' => 'Espacio en disco: @taille@',

// F
'fichier' => 'Fichero',
'fichier_distant' => 'Fichero distante',
'forum' => 'Forum', ## 2.5
'forum_s' => 'Forums',

// G
'groupe' => 'Grupo',
'groupe_id_' => 'Grupo @id_groupe@ ',
'groupes_mot_s_' => 'Grupos Palabras: ',

// I
'ici'=> 'aq&iacute;',
'id_mjsc' => 'ID',
'impossible_trouver_spip' => 'Imposible encontrar los archivos SPIP',
'info_mesure_espace_disque' => '(medida mas o menos 5% en funci&oacute;n del formato del servidor)',
'interne_s' => 'Internos',
'intitule' => 'Sin t&iacute;tulo',

// L
'larg_x_haut' => 'Ancho x Alto',
'leg_art_nbsp' => 'Art.:&nbsp;',
'ligne_s_' => 'L&iacute;neas ',
'liste_article_s' => 'Lista de Art&iacute;culos',
'liste_auteur_s' => 'Lista de Autores',
'liste_breve_s' => 'Lista de Breves',
'liste_document_s' => 'Lista Documentos',
'liste_mot_s' => 'Lista de Palabras clave',
'liste_petition_s' => 'Lista de Peticiones',
'liste_rubrique_s' => 'Lista de Secciones',
'listes_sites_sur_' => 'Listas de sitios sobre ',
'liste_site_s' => 'Lista de Sitios (validador)',

// M
'message_s' => 'Mensajes',
'message_validation' => 'Mensaje de prueba de validaci&oacute;n',
'mode' => 'Modo',
'mode_document' => 'Modo Documento',
'mode_vignette' => 'Modo Vi&ntilde;eta',
'mot_clef_s' => 'Palabras Clave',
'msg_pas connection' => 'Imposible conectarse',
'msg_url_non_conforme' => 'URL no conforme!',
'msg_page_site' => 'P&aacute;gina del sitio',
'msg_champ_incomplet' => 'Campo URL incompleto (vac&iacute;o!)',
'msg_delais_depasse' => 'Espera agotada, volver!',
'msg_ok' => 'OK',
'msg_page_vide' => 'P&aacute;gina vac&iacute;a',
'msg_contenu_partiel' => 'Contenido parcial de la p&aacute;gina',
'msg_deplacee_definitif' => 'P&aacute;gina definitivamente',
'msg_deplacee_tempo' => 'P&aacute;gina desplazada moment&aacute;neamente',
'msg_erreur_requete_http' => 'Error en la petici&oacute;n HTTP',
'msg_authentif_requise' => 'Necesaria la Autentificaci&oacute;n',
'msg_acces_payant' => 'Acceso a la p&aacute;gina: de pago',
'msg_acces_refuse' => 'Acceso a la p&aacute;gina: rechazado',
'msg_inexistante' => 'P&aacute;gina inexistente',
'msg_erreur_interne_serveur' => 'Error interno en el servidor',
'msg_erreur_passerelle' => 'Error pasarela del Servidor',
'msg_erreur_code' => 'Error no tratado, n&uacute;mero: @code@!',

// N
'nom' => 'Nombre',
'nombre' => 'N&uacute;mero',
'non' => 'no',
'non_def' => 'indefinido',

// O
'obligatoire_c' => 'Obligat.', ## 2.5
'origine' => 'Origen',
'oui' => 'si',

// P
'parent' => 'Padre',
'pas_elem_sur_site' => 'No hay elementos de este tipo en este sitio!',
'pas_doc_sur_site' => 'No hay documentos en este sitio!',
'pas_mot_cle_sur_site' => 'Ninguna palabra clave en este sitio!',
'pas_petitions_sur_site' => 'No hay peticiones en este sitio!',
'petition_s' => 'Peticiones',
'propose_s' => 'Propuestoss',
'public_s' => 'P&uacute;blicados',
'publiee_s' => 'Publicados',
'publiee_s_non' => 'Sin publicar',

// R
'redacteur_c' => 'Redac.', ## 2.5
'redacteur_s' => 'Redactores',
'repertoire' => 'Carpeta',
'rubrique' => 'Secci&oacute;n',
'rubrique_c' => 'Secc.', ## 2.5
'rubrique_s' => 'Secciones',

// S
'secteur' => 'Sector',
'signature_s' => 'Firmas',
'site' => 'Sitio',
'sites_references' => 'Sitios Referenciados',
'statut' => 'Estatuto',
'statut_pt' => 'Estatuto ...',
'syndic_c' => 'Sindic.', ## 2.5
'syndique' => 'Sindicado',

// T
'table' => 'Tabla',
'table_meas' => 'Tabla de metas', ## 2.6
'tables_non_spip' => ': Tablas no SPIP (Plugins ...)',
'taille' => 'Tama&ntilde;o',
'taille_base' => 'Tama&ntilde;o Base',
'taille_base_donnees' => 'Tama&ntilde;o Base de datos: @taille@',
'taille_tables_plug' => 'Dont tables des plugins (...) : @taille_plug@', ## 2.5
'titre' => 'T&iacute;tulo',
'titre_plugin' => 'Panel de Control',
'total' => 'Total',
'total_signatures_' => 'Total Firmas: ',
'toutes_rubriques' => 'Todas las secciones.',
'tri_par_date' => 'Clasificar por fecha',## 2.5
'tri_par_groupe' => 'Clasificar por grupo', ## 2.5
'tri_par_id' => 'Clasificar por ID', ## 2.5
'tri_par_nom' => 'Clasificar por nombre',## 2.5
'tri_par_parent' => 'Clasificar por padre',## 2.5
'tri_par_titre' => 'Clasificar par t&iacute;tulo', ## 2.5
'type' => 'Tipo',
'types_de_docs_' => 'Tipos de documentos ',
'types_pages' => 'Tipos de p&aacute;ginas',

// U
'unique' => 'S&oacute;lo', ## 2.5
'url_sans_nom' => '<i>-- enlace sin nombre</i>',

// V
'verifier_sites_valides' => 'Verificar la validez de los sitios de la lista',
'vignette_associee' => 'Vi&ntilde;eta asociada',
'visiteur_s' => 'Visitantes',

// Z
'z' => 'z'
);
?>
