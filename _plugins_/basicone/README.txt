BASICONE
un jeu d'icônes pour l'espace privé de SPIP3
http://www.spip-contrib.net/basicone-icones-du-theme-prive

Ce projet est soumis aux mêmes règles de contribution que les projets de la zone, c'est la règle par defaut qui s'applique : ouvert a toute modification mineure qui ameliore le projet, et appel a discussion avant toute proposition d'amélioration majeure.
N'hesitez pas à contacter le contributeur concerné ou a en parler sur la mailing liste en cas de doute.
Ici le mécanisme du plugin n'attend aucune évolution. Le jeu d'icônes oui.

Précision sur les images png pour ceux qui voudrait *compléter* le jeu :
- rester très sobre et minimal, prendre appui sur l'existant...
- n'utiliser que le noir et les transparences du png
- 2 pourcentages de transparence utilisé : 30% (dans le jeu d'icônes en général) et 70% (pour le bandeau)
- 3 tailles pour chaque image : 16, 24 et 32 px
- lorsqu'on y est obligé par l'interface SPIP : ajuster l'image à un format de 12px
- les 24px dans le contenu des pages sont stylées pour mettre un fond blanc et une bordure grise
- toujours prendre exemple sur le thème par défaut /extension/themes/prive/spip/images (basicone est une dérivation du pack spip3)
- les sources svg sont sur spip-contrib : http://www.spip-contrib.net/IMG/zip/basicone_svg-0.0.9.zip

L'espoir est que les images pourraient resservir dans des interfaces très différentes, on ajoute des styles et hop !


- pour compléter les informations : http://www.spip-contrib.net/Doc-SPIP3-theme-prive