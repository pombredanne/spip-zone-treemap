<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/boussole/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'boussole_description' => 'Installez ce plugin sur votre site et vous aurez accès, dans vos squelettes, à l\'ensemble des sites de la Galaxie SPIP en utilisant
les modèles, les noisettes ou la page Z proposés.
_ Pour les bricoleurs, vous pouvez aussi utiliser ce plugin pour afficher votre propre liste de sites et concevoir vos propres affichages.',
	'boussole_nom' => 'Boussole',
	'boussole_slogan' => 'Les bonnes adresses de la galaxie SPIP ou d\'ailleurs !'
);

?>
