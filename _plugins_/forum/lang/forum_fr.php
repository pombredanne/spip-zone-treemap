<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'aucun_message_forum' => 'Aucun message de forum',

	// I
	'icone_bruler_message' => "Signaler comme Spam",
	'icone_bruler_messages' => "Signaler comme Spam",
	'icone_legitimer_message' => "Signaler comme licite",
	'icone_supprimer_messages' => 'Supprimer',
	'icone_valider_messages' => 'Valider',
	'icone_valider_repondre_message' => 'Valider &amp; R&eacute;pondre &agrave; ce message',
	'info_liens_texte' => 'Lien(s) contenu(s) dans le texte du message',
	'info_liens_titre' => 'Lien(s) contenu(s) dans le titre du message',
	'interface_onglets' => 'Interface avec onglets',
	'interface_formulaire' => 'Interface formulaire',
	'info_1_message_forum' => '1 message de forum',
	'info_nb_messages_forum' => '@nb@ messages de forum',

	// L
	'label_afficher' => 'Afficher&nbsp;:',
	'label_nb_messages' => 'messages',
	'label_selectionner' => 'S&eacute;lectionner&nbsp;:',

	// M
	'message_marque_comme_spam' => 'Un message marqu&eacute; comme spam',
	'message_publie' => 'Un message publi&eacute;',
	'message_rien_a_faire' => 'Aucun forum n\'a &eacute;t&eacute; s&eacute;lectionn&eacute;',
	'message_supprime' => 'Un message supprim&eacute;',
	'messages_aucun' => 'Aucun',
	'messages_marques_comme_spam' => '@nb@ messages marqu&eacute;s comme spam',
	'messages_meme_auteur' => 'Tous les messages de cet auteur',
	'messages_meme_email' => 'Tous les messages de cet email',
	'messages_meme_ip' => 'Tous les messages de cette IP',
	'messages_off' => 'Supprim&eacute;s',
	'messages_perso' => 'Personnels',
	'messages_prive' => 'Priv&eacute;s',
	'messages_privoff' => 'Supprim&eacute;s',
	'messages_privrac' => 'G&eacute;n&eacute;raux',
	'messages_privadm' => 'Administrateurs',
	'messages_prop' => 'Propos&eacute;s',
	'messages_publie' => 'Publi&eacute;s',
	'messages_publies' => '@nb@ messages publi&eacute;s',
	'messages_spam' => 'Spam',
	'messages_supprimes' => '@nb@ messages supprim&eacute;s',
	'messages_tous' => 'Tous',

	// S
	'statut_prop' => 'Propos&eacute;',
	'statut_publie' => 'Publi&eacute;',
	'statut_spam' => 'Spam',
	'statut_off' => 'Supprim&eacute;',

	// T
	'texte_en_cours_validation'=> 'Les articles, br&egrave;ves, forums ci dessous sont propos&eacute;s &agrave; la publication.',
	'tout_voir' => 'Voir tous les messages',
	'texte_messages_publics' => 'Messages publics sur&nbsp;:',

	// V
	'voir_messages_objet' => 'voir les messages'

);


?>