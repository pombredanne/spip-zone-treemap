<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

'aucun_post_forum' => 'No post forum',
'info_1_message_forum' => '1 post forum',
'info_nb_messages_forum' => '@nb@ forum posts',

'icone_bruler_post' => "Report as Spam",
'icone_valider_repondre_post' => 'Confirm & Reply to this post',

'icone_valider_posts' => 'Confirm these posts',
'icone_bruler_posts' => "Report as Spam",
'icone_legitimer_post' => "Report as lawful",
'icone_supprimer_posts' => 'Refuse these posts',

'posts_meme_auteur' => 'All posts by this author',
'posts_meme_email' => 'All posts in this email',
'posts_meme_ip' => 'All posts of this IP',

// statuts
'posts_tous' => 'All',
'posts_publie' => 'Published',
'posts_prop' => 'Proposed',
'posts_spam' => 'Spam',
'posts_off' => 'Refused',

'posts_prive' => 'Private',
'posts_privoff' => 'Refused',
'posts_privrac' => 'General',
'posts_privadm' => 'Administrators',

'posts_perso' => 'Personal',

'statut_prop' => 'Proposed',
'statut_publie' => 'Published',
'statut_spam' => 'Spam',
'statut_off' => 'Refused',

'tout_voir' => 'See all posts',
);


?>