<?php


	/**
	 * SPIP-Lettres
	 *
	 * Copyright (c) 2006-2009
	 * Agence Artégo http://www.artego.fr
	 *  
	 * Ce programme est un logiciel libre distribue sous licence GNU/GPLv3.
	 * Pour plus de details voir http://www.gnu.org/licenses/gpl-3.0.html
	 *  
	 **/
if (!defined("_ECRIRE_INC_VERSION")) return;


	$GLOBALS[$GLOBALS['idx_lang']] = array(

		// formulaire lettres
		'lettre_information' => "Newsletter",
		'lettres_information' => "Newsletters",
		'themes' => "Topics",
		'racine' => "Website's root",
		'tout_le_site' => "This website's activity",
		'email' => "Email",
		'nom' => "Name",
		'format' => "Format",
		'format_html' => "HTML",
		'format_texte' => "Text",
		'format_mixte' => "Mixte",
		'action' => "Action",
		'abonnement' => "Subscribe",
		'desabonnement' => "Unsubscribe",
		'changement_format' => "Change format",
		'valider' => "Submit",
		'email_ko' => "Invalid format",
		'choix_ko' => "Mandatory field",
		'vous_devez_choisir_un_theme' => "You must choose a topic",
		'vous_n_etes_pas_abonnes' => "You're not subscribed",
		
		// formulaire lettres messages
		'validation_abonnements_succes' => "You successfully subscribed.",
		'validation_abonnements_erreur' => "Your subscriptions failed.",
		'validation_desabonnements_succes' => "You successfully unsubscribed.",
		'validation_desabonnements_erreur' => "Your unsubscriptions failed.",
		'validation_changement_format_succes' => "Your newsletter's format has successfully been changed.",
		'validation_changement_format_erreur' => "Your newsletter's format couldn't be changed.",
		'envoi_abonnements_succes' => "You will receive an email to confirm your subscriptions.",
		'envoi_abonnements_erreur' => "The confirmation email couldn't be sent.",
		'envoi_desabonnements_succes' => "You will receive an email to confirm your unsubscriptions.",
		'envoi_desabonnements_erreur' => "The confirmation email couln't be sent.",
		'envoi_changement_format_succes' => "You will receive an email to confirm this change.",
		'envoi_changement_format_erreur' => "The confirmation email couldn't be sent.",
		'retour' => 'Retour',

		// lettres
		'probleme_lecture_lettre' => "Click here if you have trouble reading this email",
		'se_desabonner' => "Click here to unsubscribe",

		// notifications
		'validation_abonnements' => "Confirm your subscriptions",
		'vous_avez_demande_a_vous_abonner' => "On ".$GLOBALS['meta']['nom_site'].", you asked to subscribe the following topics :",
		'validation_desabonnements' => "Confirm your unsubscriptions",
		'vous_avez_demande_a_vous_desabonner' => "On ".$GLOBALS['meta']['nom_site'].", you asked to unsubscribe the following topics :",
		'validation_changement_format' => "Confirm change of format",
		'vous_avez_demande_a_changer_format' => "On ".$GLOBALS['meta']['nom_site'].", you asked to change the format of the newsletter you receive.",
		'confirmation' => "Confirmation link",
		'cliquez_ici_pour_confirmer' => "Click here to confirm",
		'suppression_abonne' => "Deletion of subscriber",
		'a_ete_supprime' => "was removed from the database. Please sync your files.",
		
		'Z' => 'ZZzZZzzz'

	);


?>