<?php
/**
 * SPIP-Lettres
 *
 * Copyright (c) 2006-2009
 * Agence Artégo http://www.artego.fr
 *
 * Ce programme est un logiciel libre distribue sous licence GNU/GPLv3.
 * Pour plus de details voir http://www.gnu.org/licenses/gpl-3.0.html
 *
 **/

define('_DIR_LETTRES', _DIR_IMG.'lettres/');

// libelle pour le logo des lettres
$GLOBALS['logo_libelles']['id_lettre'] = _T('lettresprive:logo_lettre');
#define('_DEBUG_BLOCK_QUEUE',true);

include_spip ("inc/choisir_thematique");
?>
