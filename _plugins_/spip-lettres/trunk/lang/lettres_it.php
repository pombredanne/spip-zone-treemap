<?php


	/**
	 * SPIP-Lettres
	 *
	 * Copyright (c) 2006-2009
	 * Agence ArtÃ©go http://www.artego.fr
	 *  
	 * Ce programme est un logiciel libre distribue sous licence GNU/GPLv3.
	 * Pour plus de details voir http://www.gnu.org/licenses/gpl-3.0.html
	 *  
	 *  
	 * fichier de langue italienne traduit par Objectif Sciences
	 * http://asso.objectif-sciences.com
	 *  
	 *  
	 **/


	$GLOBALS[$GLOBALS['idx_lang']] = array(

		// formulaire lettres
		'lettre_information' => "Lettera d'informazione",
		'lettres_information' => "Lettere d'informazione",
		'themes' => "Temi",
		'racine' => "Radice del sito",
		'tout_le_site' => "Tutto il sito",
		'email' => "Mail",
		'nom' => "Cognome",
		'format' => "Formato",
		'format_html' => "HTML",
		'format_texte' => "Testo",
		'format_mixte' => "Misto",
		'action' => "Azione",
		'abonnement' => "Abbonamento",
		'desabonnement' => "Disdetta di abbonamento",
		'changement_format' => "Cambio di formato",
		'valider' => "Convalidare",
		
		// formulaire lettres messages
		'validation_abonnements_succes' => "I vostri abbonamenti sono stati convalidati.",
		'validation_abonnements_erreur' => "I vostri abbonamenti non sono potuti essere convalidati.",
		'validation_desabonnements_succes' => "Le vostre disdette di abbonamento sono state convalidate.",
		'validation_desabonnements_erreur' => "Le vostre disdette di abbonamento non sono potuti essere convalidate.",
		'validation_changement_format_succes' => "Il vostro cambio di formato è appena stato preso in conto.",
		'validation_changement_format_erreur' => "Il vostro cambio di formato non è potuto essere preso in conto.",
		'envoi_abonnements_succes' => "Sta per ricevere una mail per confermare i suoi abbonamenti.",
		'envoi_abonnements_erreur' => "La mail di conferma dei suoi abbonamenti non è potuta essere mandata.",
		'envoi_desabonnements_succes' => "Sta per ricevere una mail per confermare le sue disdette di abbonamento.",
		'envoi_desabonnements_erreur' => "La mail di conferma delle sue disdette di abbonamento non è potuta essere mandata.",
		'envoi_changement_format_succes' => "Sta per ricevere una mail per confermare il suo cambio di formato.",
		'envoi_changement_format_erreur' => "La mail di conferma del suo cambio di formato non è potuta essere mandata.",
		'retour' => "Indietro",

		// lettres
		'probleme_lecture_lettre' => "Cliccare qui se la lettere d'informazione non si affige corettamente",
		'se_desabonner' => "Cliccare qui per disdire il suo abbonamento",

		// notifications
		'validation_abonnements' => "Convalida dei suoi abbonamenti",
		'vous_avez_demande_a_vous_abonner' => "Su ".$GLOBALS['meta']['nom_site'].", ha chiesto l'abbonamento per i temi seguenti :",
		'validation_desabonnements' => "Convalida delle sue disdette di abbonamento",
		'vous_avez_demande_a_vous_desabonner' => "Su ".$GLOBALS['meta']['nom_site'].", ha chiesto la disdetta di abbonamento dei temi seguenti :",
		'validation_changement_format' => "Convalida del suo cambio di formato",
		'vous_avez_demande_a_changer_format' => "Su ".$GLOBALS['meta']['nom_site'].", ha chiesto il cambio del formato delle lettere d'informazione che riceve.",
		'confirmation' => "Conferma",
		'cliquez_ici_pour_confirmer' => "Cliccare qui per confermare",
		'suppression_abonne' => "Soppressione di un abbonato",
		'a_ete_supprime' => "E stato tolto dalla base dati. La prego di sincronizzare con il suo file cliente.",

		'Z' => 'ZZzZZzzz'

	);


?>