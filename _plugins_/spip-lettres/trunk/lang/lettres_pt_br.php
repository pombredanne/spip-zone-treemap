<?php


	/**
	 * SPIP-Lettres
	 *
	 * Copyright (c) 2006-2009
	 * Agence Artégo http://www.artego.fr
	 *  
	 * Ce programme est un logiciel libre distribue sous licence GNU/GPLv3.
	 * Pour plus de details voir http://www.gnu.org/licenses/gpl-3.0.html
	 *  
	 **/


	$GLOBALS[$GLOBALS['idx_lang']] = array(

		// formulaire lettres
		'lettre_information' => "Informativo",
		'lettres_information' => "Informativos",
		'themes' => "Temas",
		'racine' => "Raíz do site",
		'tout_le_site' => "Todo o site",
		'email' => "E-mail",
		'nom' => "Nome",
		'format' => "Formato",
		'format_html' => "HTML",
		'format_texte' => "Texto",
		'format_mixte' => "Misto",
		'action' => "Ação",
		'abonnement' => "Dados",
		'desabonnement' => "Suprimir dados",
		'changement_format' => "Mudança de formato",
		'valider' => "Validar",
		
		// formulaire lettres messages
		'validation_abonnements_succes' => "Os seus dados foram validados.",
		'validation_abonnements_erreur' => "Os seus dados não poderam ser validados.",
		'validation_desabonnements_succes' => "A sua solicitação para supressão de dados foi validada.",
		'validation_desabonnements_erreur' => "A sua solicitação para supressão de dados não pôde ser validada.",
		'validation_changement_format_succes' => "A mudança de formato foi registrada.",
		'validation_changement_format_erreur' => "A mudança de formato não pôde ser registrada.",
		'envoi_abonnements_succes' => "Você receberá um e-mail para confirmar os seus dados.",
		'envoi_abonnements_erreur' => "O e-mail de confirmação de dados não pôde ser enviado.",
		'envoi_desabonnements_succes' => "Você receberá um e-mail para confimar a sua solicitação de supressão de dados.",
		'envoi_desabonnements_erreur' => "O e-mail de confirmação para supressão de dados não pôde ser enviado.",
		'envoi_changement_format_succes' => "Você receberá um e-mail para confimar a sua mudança de formato.",
		'envoi_changement_format_erreur' => "O e-mail de confirmação da mudança de formato não pôde ser enviada.",
		'retour' => 'Retour',

		// lettres
		'probleme_lecture_lettre' => "Clique aqui se o informativo não se afixar corretamente",
		'se_desabonner' => "Clique aqui para suprimir os seus dados",

		// notifications
		'validation_abonnements' => "Validação de dados",
		'vous_avez_demande_a_vous_abonner' => "Sobre ".$GLOBALS['meta']['nom_site'].", você solicitou receber os temas seguintes :",
		'validation_desabonnements' => "Validação da supressão de dados",
		'vous_avez_demande_a_vous_desabonner' => "Sobre ".$GLOBALS['meta']['nom_site'].", você solicitou suprimir os temas seguintes :",
		'validation_changement_format' => "Validação da mudança de formato",
		'vous_avez_demande_a_changer_format' => "Sobre ".$GLOBALS['meta']['nom_site'].", você solicitou a mudança de formato dos informativos destinados ao seu e-mail.",
		'confirmation' => "Confirmação",
		'cliquez_ici_pour_confirmer' => "Clique aqui para confirmar",
		'suppression_abonne' => "Supressão de um registro",
		'a_ete_supprime' => "Foi suprimido da base de cadastros. Atualize a sua ficha de cliente.",

		'Z' => 'ZZzZZzzz'

	);


?>