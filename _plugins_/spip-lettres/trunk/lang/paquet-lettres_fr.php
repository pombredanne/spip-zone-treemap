<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-lettres
// Langue: fr
// Date: 18-01-2012 14:09:00
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// L
	'lettres_description' => 'Gestion de lettres d\'information par mail : plusieurs thematiques pour les abonnements, import-export de liste d\'abonnes, construction des lettres par squelettes personnalisables, suivi des envois en temps reel, statistiques d\'envois et de clics',
	'lettres_slogan' => 'Gestion de lettres d\'information',
);
?>