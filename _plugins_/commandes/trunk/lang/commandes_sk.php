<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/commandes?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'action_facture' => 'Faktúra',
	'action_modifier' => 'Zmeniť',
	'action_supprimer' => 'Odstrániť',
	'adresse_facturation' => 'Fakturačná adresa',
	'adresse_livraison' => 'Doručovacia adresa',

	// B
	'bonjour' => 'Dobrý deň',

	// C
	'commande_client' => 'Zákazník',
	'commande_date' => 'Dátum',
	'commande_date_paiement' => 'Dátum platby',
	'commande_editer' => 'Upraviť objednávku',
	'commande_modifier' => 'Zmeniť objednávku:',
	'commande_montant' => 'Množstvo',
	'commande_nouvelle' => 'Nová objednávka',
	'commande_numero' => 'Objednávka č. ',
	'commande_reference' => 'Číslo',
	'commande_reference_numero' => 'Č. objednávky ',
	'commande_statut' => 'Štádium',
	'commandes_titre' => 'Objednávky',
	'configurer_titre' => 'Nastaviť zásuvný modul Objednávka',
	'confirmer_supprimer_commande' => 'Môžete potvrdiť odstránenie tejto objednávky?',
	'contact_label' => 'Kontakt:',
	'contenu' => 'Obsah',

	// D
	'date_commande_label' => 'Dátum vytvorenia:',
	'date_envoi_label' => 'Dátum odoslania:',
	'date_maj_label' => 'Dátum aktualizácie:',
	'date_paiement_label' => 'Dátum platby:',
	'designation' => 'Označenie',
	'details_commande' => 'Podrobnosti objednávky:',

	// E
	'etat' => 'Stav',

	// F
	'facture_date' => 'Dátum: <span>@date@</span>',
	'facture_num' => 'Faktúra č. <span>@num@</span>',
	'facture_titre' => 'Faktúra',

	// I
	'info_numero_commande' => 'OBJEDNÁVKA ČÍSLO:',
	'info_toutes_commandes' => 'Všetky objednávky',

	// L
	'label_recherche' => 'Vyhľadávať',

	// M
	'merci_de_votre_commande' => 'Vašu objednávku sme zaregistrovali a ceníme si Vašu dôveru.',
	'modifier_commande_statut' => 'Táto objednávka je:',
	'montant' => 'Množstvo',

	// N
	'nom_bouton_plugin' => 'Objednávky',
	'notifications_activer_explication' => 'Posielať e-mailom oznamy o príkaze?',
	'notifications_activer_label' => 'Aktivovať',
	'notifications_cfg_titre' => 'Oznamy',
	'notifications_client_explication' => 'Posielať zákazníkovi oznamy?',
	'notifications_client_label' => 'Zákazník',
	'notifications_expediteur_administrateur_label' => 'Vybrať administrátora:',
	'notifications_expediteur_choix_administrateur' => 'administrátor',
	'notifications_expediteur_choix_email' => 'e-mail',
	'notifications_expediteur_choix_facteur' => 'rovnaký ako zásuvný modul Facteur',
	'notifications_expediteur_choix_webmaster' => 'webmaster',
	'notifications_expediteur_email_label' => 'Zadajte e-mailovú adresu:',
	'notifications_expediteur_explication' => 'Vyberte odosielateľa oznamov pre predávajúceho a kupujúceho',
	'notifications_expediteur_label' => 'Odosielateľ',
	'notifications_expediteur_webmaster_label' => 'Vybrať webmastera:',
	'notifications_explication' => 'Oznamy sa používajú na posielanie e-mailov po zmenách v stave spracovania objednávky: čaká, vybavuje sa, odoslaná, čiastočne zaplatená, zaplatená, vrátená, čiastočne vrátiť. Táto funkcia si vyžaduje zásuvný modul Podrobné oznamy.',
	'notifications_parametres' => 'Parametre oznamov',
	'notifications_quand_explication' => 'Pri akých zmenách stavu poslať oznam?',
	'notifications_quand_label' => 'Spustenie',
	'notifications_vendeur_administrateur_label' => 'Vyberte jedného administrátora alebo viacerých:',
	'notifications_vendeur_choix_administrateur' => 'jeden administrátor alebo viacerí',
	'notifications_vendeur_choix_email' => 'jeden e-mail alebo viacero e-mailov',
	'notifications_vendeur_choix_webmaster' => 'jeden webmaster alebo viacerí',
	'notifications_vendeur_email_explication' => 'Zadajte jednu e-mailovú adresu alebo viac e-mailových adries a oddeľte ich čiarkami:',
	'notifications_vendeur_email_label' => 'E-mail(y) :',
	'notifications_vendeur_explication' => 'Vyberte príjemcu (-ov) oznamov na odoslanie predávajúcemu ',
	'notifications_vendeur_label' => 'Predávajúci',
	'notifications_vendeur_webmaster_label' => 'Vyberte jedného webmastera alebo viacerých:',

	// P
	'parametres_cfg_titre' => 'Parametre',
	'parametres_duree_vie_explication' => 'Zadajte čas platnosti objednávky v tomto štádiu spracovania (v hodinách):',
	'parametres_duree_vie_label' => 'Čas platnosti',
	'passer_la_commande' => 'Zadať objednávku',

	// R
	'recapitulatif' => 'Zhrnutie objednávky:',
	'reference' => 'Značka',
	'reference_label' => 'Značka:',
	'reference_ref' => 'Značka @ref@',

	// S
	'simuler' => 'Simulovať zmenu stavu',
	'statut_attente' => 'Čaká',
	'statut_encours' => 'Vybavuje sa',
	'statut_envoye' => 'Odoslaná',
	'statut_erreur' => 'Chyba',
	'statut_label' => 'Stav:',
	'statut_partiel' => 'Čiastočne zaplatená',
	'statut_paye' => 'Zaplatená',
	'statut_retour' => 'Vrátená',
	'statut_retour_partiel' => 'Čiastočne vrátiť',
	'supprimer' => 'Odstrániť',

	// T
	'texte_changer_statut_commande' => 'Táto objednávka je:',

	// U
	'une_commande_de' => 'Objednávka: ',
	'une_commande_sur' => 'Vaša objednávka – @nom@',

	// V
	'votre_commande_sur' => '@nom@: Vaša objednávka'
);

?>
