<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'commandes_declinaisons_description' => 'Permet de g&#233;rer des commandes.',
	'commandes_declinaisons_nom' => 'Commandes',
	'commandes_declinaisons_slogan' => 'G&#233;rer des commandes',
);

?>