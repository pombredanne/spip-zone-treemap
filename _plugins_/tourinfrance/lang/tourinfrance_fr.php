<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/_stable_/acces_restreint/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'action_modifier' => 'Modifier',
	'action_supprimer' => 'Supprimer',
	'action_voir' => 'Voir les offres',
	
	// B
	
	// C

	// D
	'description_tourinfrance' => 'Importez les flux Tourinsoft d&eacute;sir&eacute;s. Les offres seront trait&eacute;es en articles SPIP.',
	
	// E
	'editer_flux_nouveau' => 'Ajouter un nouveau flux',
	
	// F
	'form_flux_vide' => 'Le flux propos&eacute; est vide ou ne correspond pas aux crit&egrave;res Tourinfrance.',
	'form_info_obligatoire' => 'Cette information est obligatoire !',
	'form_message_traiter_erreur' => 'Une erreur s\'est produite lors de l\'ajout.',
	'form_message_traiter_ok' => 'Flux modifi&eacute; avec succ&egrave;s.',
	'form_message_verifier_erreur' => 'Une erreur est pr&eacute;sente dans votre saisie.',
	'form_nouveau_derniere_etape' => 'Derni&egrave;re &eacute;tape : Veuillez proc&eacute;der au mappage des donn&eacute;es avec pr&eacute;caution, pour terminer l\'ajout de votre Flux.',

	// I
	'info_ajouter_nouveau' => 'Ajouter un nouveau flux :',
	'info_gauche_numero_flux' => 'Flux num&eacute;ro :',
	'info_modifier_flux' => 'Modifier un flux :',

	// L
	'label_actions' => 'Actions',
	'label_nom' => 'Nom',
	'label_url' => 'URL',
	'liste_offres' => 'Listes des offres du flux :',

	// M
	'mappage' => 'Mappage :',
	'message_ajoute' => 'AJOUT&Eacute;',
	'message_fichier_introuvable' => 'fichier non trouv&eacute;.',
	'message_flux_maj_aucune' => 'Aucune modification apport&eacute;e.',
	'message_flux_propose' => 'Flux propos&eacute; &agrave; la base de donn&eacute;es !',
	'message_modifie' => 'MODIFI&Eacute;',
	
	// O
	
	// P

	// R

	// S
	'soustitre_tourinfrance' => 'Importez des offres Tourinsoft.',

	// T
	'texte_nouveau_flux' => 'Sans nom',
	'titre_tourinfrance' => 'Tourinfrance'

	// V

	// Z
);

?>
