<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/bandeau/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'icone_accueil' => 'Accueil',
	'icone_administration' => 'Maintenance',
	'icone_configuration' => 'Configuration',
	'icone_edition' => 'Édition',
	'icone_langage' => 'Ma langue',
	'icone_mes_infos' => 'Mes informations',
	'icone_preferences' => 'Mes préférences',
	'icone_reactions' => 'Activité',
	'icone_squelette' => 'Squelettes',
	'icone_suivi_edito' => 'Publication',
	'icone_suivi_publication' => 'Suivi de la publication',
	'icone_visiter_site' => 'Voir le site public',

	// L
	'label_adresses_secondaires' => 'Adresses ALIAS du site public',
	'label_bando_outils' => 'Barre d\'outils',
	'label_bando_outils_afficher' => 'Afficher les outils',
	'label_bando_outils_masquer' => 'Masquer les outils',
	'label_choix_langue' => 'Selectionnez votre langue',
	'label_slogan_site' => 'Slogan du site',
	'label_taille_ecran' => 'Largeur de l\'ecran',
	'label_texte_et_icones_navigation' => 'Menu de navigation',
	'label_texte_et_icones_page' => 'Affichage dans la page',

	// T
	'texte_redirection_adresses_secondaires' => 'Les Alias sont des URL alternatives qui permettent aussi d\'accèder à votre site.
		Ces adresses seront redirigées automatiquement vers l\'adresse principale. Indiquez une adresse par ligne',
	'titre_config_langage' => 'Configurer la langue',
	'titre_configurer_preferences' => 'Configurer vos préférences',
	'titre_identite_site' => 'Identité du site',
	'titre_infos_perso' => 'Mes informations personnelles'
);

?>
