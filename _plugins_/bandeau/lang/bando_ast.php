<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/bando?lang_cible=ast
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'icone_accueil' => 'Entamu',
	'icone_administration' => 'Mantenimientu',
	'icone_configuration' => 'Configuración',
	'icone_edition' => 'Edición',
	'icone_langage' => 'La mio llingua',
	'icone_mes_infos' => 'La mio información',
	'icone_preferences' => 'Les mios preferencies',
	'icone_reactions' => 'Actividá',
	'icone_squelette' => 'Cadarmes',
	'icone_suivi_edito' => 'Publicación',
	'icone_suivi_publication' => 'Siguimientu de la publicación',
	'icone_visiter_site' => 'Ver el sitiu públicu',

	// L
	'label_adresses_secondaires' => 'Direiciones ALIAS del sitiu públicu',
	'label_bando_outils' => 'Barra de ferramientes',
	'label_bando_outils_afficher' => 'Amosar les ferramientes',
	'label_bando_outils_masquer' => 'Anubrir les ferramientes',
	'label_choix_langue' => 'Escueye la to llingua',
	'label_slogan_site' => 'Eslogan del sitiu',
	'label_taille_ecran' => 'Tamañu de la pantalla',
	'label_texte_et_icones_navigation' => 'Menú de navegación',
	'label_texte_et_icones_page' => 'Posición na páxina',

	// T
	'texte_redirection_adresses_secondaires' => 'Los Alias son URL alternatives que tamién permiten l\'accesu al to sitiu.
  Estes direiciones se van redirixir automáticamente a la direición principal. Indica una direición por llinia',
	'titre_config_langage' => 'Configurar la llingua',
	'titre_configurer_preferences' => 'Configurar les tos preferencies',
	'titre_identite_site' => 'Identidá del sitiu',
	'titre_infos_perso' => 'La mio información personal'
);

?>
