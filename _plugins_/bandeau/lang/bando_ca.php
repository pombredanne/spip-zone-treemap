<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/bando?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'icone_accueil' => 'Inici',
	'icone_administration' => 'Manteniment',
	'icone_configuration' => 'Configuració',
	'icone_edition' => 'Edició',
	'icone_langage' => 'La meva llengua ',
	'icone_mes_infos' => 'Les meves informacions',
	'icone_preferences' => 'Les meves preferències',
	'icone_reactions' => 'Activitat',
	'icone_squelette' => 'Esquelets',
	'icone_suivi_edito' => 'Publicació',
	'icone_suivi_publication' => 'Seguiment de la publicació',
	'icone_visiter_site' => 'Veure el lloc públic',

	// L
	'label_adresses_secondaires' => 'Adreces ÀLIES del lloc públic',
	'label_bando_outils' => 'Barra d\'eines',
	'label_bando_outils_afficher' => 'Mostrar les eines',
	'label_bando_outils_masquer' => 'Amagar les eines',
	'label_choix_langue' => 'Seleccioneu la vostra llengua',
	'label_slogan_site' => 'Eslògan del lloc',
	'label_taille_ecran' => 'Amplada de la pantalla',
	'label_texte_et_icones_navigation' => 'Menú de navegació',
	'label_texte_et_icones_page' => 'Visualització a la pàgina',

	// T
	'texte_redirection_adresses_secondaires' => 'Els Àlies són URL alternatives que també permeten accedir al vostre lloc.
  Aquestes adreces seran redirigides automàticament cap a l\'adreça principal. Indiqueu una adreça per línia ',
	'titre_config_langage' => 'Configurar la llengua',
	'titre_configurer_preferences' => 'Configurar les teves preferències',
	'titre_identite_site' => 'Identitat del lloc',
	'titre_infos_perso' => 'Mes informations personnelles' # NEW
);

?>
