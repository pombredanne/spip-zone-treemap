<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/bando?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'icone_accueil' => 'Úvodná stránka',
	'icone_administration' => 'Údržba',
	'icone_configuration' => 'Konfigurácia',
	'icone_edition' => 'Úpravy',
	'icone_langage' => 'Môj jazyk',
	'icone_mes_infos' => 'Moje údaje',
	'icone_preferences' => 'My preferences',
	'icone_reactions' => 'Aktivita',
	'icone_squelette' => 'Šablóny',
	'icone_suivi_edito' => 'Publikovanie',
	'icone_suivi_publication' => 'Sledovať publikovanie',
	'icone_visiter_site' => 'Zobraziť verejne prístupnú stránku',

	// L
	'label_adresses_secondaires' => 'Sekundárne adresy verejne prístupnej stránky',
	'label_bando_outils' => 'Panel s nástrojmi',
	'label_bando_outils_afficher' => 'Zobraziť nástroje',
	'label_bando_outils_masquer' => 'Schovať nástroje',
	'label_choix_langue' => 'Vyberte si jazyk',
	'label_slogan_site' => 'Slogan stránky',
	'label_taille_ecran' => 'Šírka obrazovky',
	'label_texte_et_icones_navigation' => 'Menu navigácie',
	'label_texte_et_icones_page' => 'Zobrazenie stránky',

	// T
	'texte_redirection_adresses_secondaires' => 'Sekundárne adresy sú alternatívne adresy, ktoré môžete použiť pri prístupe na svoju stránku.
  Tieto adresy budú automaticky presmerované na vašu hlavnú adresu. Na každý riadok zadajte jednu adresu ',
	'titre_config_langage' => 'Nastaviť jazyk',
	'titre_configurer_preferences' => 'Nastavte si svoje predvoľby',
	'titre_identite_site' => 'Identita stránky',
	'titre_infos_perso' => 'Moje osobné údaje'
);

?>
