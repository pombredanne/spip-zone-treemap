<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/bando?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'icone_accueil' => 'Start',
	'icone_administration' => 'Wartung',
	'icone_configuration' => 'Konfiguration',
	'icone_edition' => 'Redaktion',
	'icone_langage' => 'Meine Sprache',
	'icone_mes_infos' => 'Mein Profil',
	'icone_preferences' => 'Meine Einstellungen',
	'icone_reactions' => 'Aktivität verfolgen',
	'icone_squelette' => 'Skelette',
	'icone_suivi_edito' => 'Veröffentlichung',
	'icone_suivi_publication' => 'Veröffentlichung verfolgen',
	'icone_visiter_site' => 'Website ansehen',

	// L
	'label_adresses_secondaires' => 'Adresses ALIAS du site public', # NEW
	'label_bando_outils' => 'Barre d\'outils', # NEW
	'label_bando_outils_afficher' => 'Afficher les outils', # NEW
	'label_bando_outils_masquer' => 'Masquer les outils', # NEW
	'label_choix_langue' => 'Ihre Sprache wählen',
	'label_slogan_site' => 'Slogan der Website',
	'label_taille_ecran' => 'Bildschirmbreite',
	'label_texte_et_icones_navigation' => 'Navigationsmenü',
	'label_texte_et_icones_page' => 'Auf der Seite anzeigen',

	// T
	'texte_redirection_adresses_secondaires' => 'Les Alias sont des URL alternatives qui permettent aussi d\'accèder à votre site.
		Ces adresses seront redirigées automatiquement vers l\'adresse principale. Indiquez une adresse par ligne', # NEW
	'titre_config_langage' => 'Spracheinstellung',
	'titre_configurer_preferences' => 'Persönliche Einstellungen',
	'titre_identite_site' => 'Identität der Site',
	'titre_infos_perso' => 'Meine persönlichen Informationen'
);

?>
