<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/bando?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'icone_accueil' => 'Launch pad',
	'icone_administration' => 'Maintenance',
	'icone_configuration' => 'Configuration',
	'icone_edition' => 'Site edit',
	'icone_langage' => 'My language',
	'icone_mes_infos' => 'My informations',
	'icone_preferences' => 'My preferences',
	'icone_reactions' => 'Activity',
	'icone_squelette' => 'Templates',
	'icone_suivi_edito' => 'Publication',
	'icone_suivi_publication' => 'To follow the publication',
	'icone_visiter_site' => 'Visit',

	// L
	'label_adresses_secondaires' => 'ALIAS addresses for the public site',
	'label_bando_outils' => 'Toolbar',
	'label_bando_outils_afficher' => 'Show tools',
	'label_bando_outils_masquer' => 'Hide tools',
	'label_choix_langue' => 'Manage your language ',
	'label_slogan_site' => 'Slogan',
	'label_taille_ecran' => 'Display',
	'label_texte_et_icones_navigation' => 'Navigation menu',
	'label_texte_et_icones_page' => 'Page display',

	// T
	'texte_redirection_adresses_secondaires' => 'The ALIAS addresses are alternative URLs that can also be used for accessing your site.
  These addresses will be automatically redirected to the main address. Enter one address per line',
	'titre_config_langage' => 'Configurer la langue',
	'titre_configurer_preferences' => 'Configure your preferences',
	'titre_identite_site' => 'Site\'s identity',
	'titre_infos_perso' => 'My personal details'
);

?>
