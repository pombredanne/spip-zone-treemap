<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/bando?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'icone_accueil' => 'Mis tareas',
	'icone_administration' => 'Mantenimiento',
	'icone_configuration' => 'Configuración',
	'icone_edition' => 'Edición',
	'icone_langage' => 'Mi lengua',
	'icone_mes_infos' => 'Mis informaciones',
	'icone_preferences' => 'Mis preferencias',
	'icone_reactions' => 'Actividad',
	'icone_squelette' => 'Skelette',
	'icone_suivi_edito' => 'Publicación',
	'icone_suivi_publication' => 'Seguir la publicación',
	'icone_visiter_site' => 'Ver el sitio público',

	// L
	'label_adresses_secondaires' => 'Direcciones ALIAS del sitio público',
	'label_bando_outils' => 'Barra de herramientas',
	'label_bando_outils_afficher' => 'Mostrar las herramientas',
	'label_bando_outils_masquer' => 'Esconder las herramientas',
	'label_choix_langue' => 'Seleccine su lengua',
	'label_slogan_site' => 'Slogan del sitio',
	'label_taille_ecran' => 'Largo de la pantalla',
	'label_texte_et_icones_navigation' => 'Menu de navigaión',
	'label_texte_et_icones_page' => 'Afichar en la página',

	// T
	'texte_redirection_adresses_secondaires' => 'Los Alias son URLs alternativas que también permiten acceder a su sitio.
  Estas direcciones son automaticamente redirigidas hacia la dirección principal. Indicar una dirección por linea.',
	'titre_config_langage' => 'Configurar la lengua',
	'titre_configurer_preferences' => 'Configurar sus preferencias',
	'titre_identite_site' => 'Identidad del sitio',
	'titre_infos_perso' => 'Mes informations personnelles' # NEW
);

?>
