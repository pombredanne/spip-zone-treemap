<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/bando?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'icone_accueil' => 'Página principal',
	'icone_administration' => 'Manutenção',
	'icone_configuration' => 'Configuração',
	'icone_edition' => 'Edição',
	'icone_langage' => 'Meu idioma',
	'icone_mes_infos' => 'Minhas informações',
	'icone_preferences' => 'Minhas preferências',
	'icone_reactions' => 'Atividade',
	'icone_squelette' => 'Gabaritos',
	'icone_suivi_edito' => 'Publicação',
	'icone_suivi_publication' => 'Acompanhamento da publicação',
	'icone_visiter_site' => 'Visualizar o site público',

	// L
	'label_adresses_secondaires' => 'Endereços ALIAS do site público',
	'label_bando_outils' => 'Barra de ferramentas',
	'label_bando_outils_afficher' => 'Exibir as ferramentas',
	'label_bando_outils_masquer' => 'Ocultar as ferramentas',
	'label_choix_langue' => 'Selecione o seu idioma',
	'label_slogan_site' => 'Slogan do site',
	'label_taille_ecran' => 'Largura da tela',
	'label_texte_et_icones_navigation' => 'Menu de navegação',
	'label_texte_et_icones_page' => 'Exibição na página',

	// T
	'texte_redirection_adresses_secondaires' => 'Os Alias são URL alternativas que permitem também acessar o seu site.
Estes endereços serão redirecionados automaticamente para o endereço principal. Informe um endereço por linha',
	'titre_config_langage' => 'Configurar o idioma',
	'titre_configurer_preferences' => 'Configurar as suas preferências',
	'titre_identite_site' => 'Identidade do site',
	'titre_infos_perso' => 'Minhas informações pessoais'
);

?>
