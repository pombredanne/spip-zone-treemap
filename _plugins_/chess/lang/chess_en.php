<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'info_documents'=>'Documents',
'titre' => 'Chess and Mat',
'liste_details' => 'Details',
'liste_parties' => 'Games',
'liste_fichiers_pgn' => 'PGN Files',
'voir_parties' => 'Check the games of this file',
'voir_details' => 'Check the details of this game',
'pgn_dispo' => 'Available pgn files:',
'parties_voir' => 'View',
'parties_blanc' => 'White',
'parties_noir' => 'Black',
'parties_resultat' => 'Res.',
'details_jouer' => 'Play',
'details_stop' => 'Stop',
'details_info' => 'Games info.',
'details_options' => 'Board options',
'details_options_inverser' => 'Flip board',
'details_options_lecture' => 'when automatic play is launch, on comments:',
'details_options_lecture_arret' => ' stop the play.',
'details_options_lecture_continuer' => ' continue to play after ',
'details_options_lecture_contsec' => ' seconds <strong>(Default : 3)</strong>',
'details_options_charset' => 'Charset des fichiers PGN:',
'details_options_pieces' => 'Choose chess set:',
'details_options_taille' => 'Board size',
'details_options_taille_petit' => 'Small',
'details_options_taille_moyen' => 'Medium',
'details_options_taille_grand' => 'Large',
'details_options_coldroite' => 'Largeur de la colonne de droite (en pixels, 230 par d&eacute;faut) :',
'details_options_habillage' => 'Chess set',
'details_options_vitesse' => 'Automatic play game speed : one move every ',
'details_options_vitsec' => 'secondes <strong>(default : 0.1)</strong>',
'details_info_blanc' => 'White:',
'details_info_noir' => 'Black:',
'details_info_resultat' => 'Result:',
'details_info_evennement' => 'Event:',
'details_info_date' => 'Date:',
'details_info_round' => 'Round:',
'details_info_vide' => '...',
'erreur_config' => '<strong>You need to validate the options to display the chess plugin, <a href="@url@">click here</a> to access to the configuration&apos;s page.</strong>',
);
?>
