<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'titre_page_config_walma' => "Configuration of plugin Walma",
'more_info' => 'More information', 
'copidlibre' => "CopID free non commercial &copy; 07/2007 Alm &amp; Walk Galerie ",
'copid_walma' => "CopID libre non marchand",
'pour' => "for",
'taille_image' => "Image size",
'diaporama' => "Slideshow",
'change_langue'=> "Passer en langue fran&#231;aise",
'clic_fermer'=> "clic to close",
'gauche'=> "Left",
'droite'=> "Right", 
'mosaiclic'=>"Mosa&#239;clic",
'no_result'=> "No result!",
'images'=> "images",
'image'=> "image",
'tout_portfolios'=>"All portfolios",
'ce_portfolio'=>"This portfolio",
);


?>