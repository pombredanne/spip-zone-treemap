<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(

//A

	'ajouter_mos_parents' => 'Mots-cl&eacute;s parents (ajouter/retirer)',

//B
//C
//D
	'deplacement_possible' => 'Vous pouvez d&eacute;placer les n&oelig;uds du graphe en les glissant d&eacute;posant',
//E
	'enfants_du_mot_ou_des_mots' => 'Enfants de tous les mots-cl&eacute;s (qui en ont !)',
//F
//G
	'graphe_site' => 'Graphe de tous les mots-cl&eacute;s hi&eacute;rachis&eacute;s',
//H
	'hierarchies_courantes' => 'Hi&eacute;rarchies des mots-cl&eacute;s parents courantes&nbsp;:',
	'hierachie_guidee_par' => 'Fil d\'Ariane guid&eacute; par le mot &laquo;&nbsp;@titre@&nbsp;&raquo;&nbsp;:',
	'hierarchies_mot' => 'Diff&eacute;rentes hi&eacute;rarchies d\'un mot-cl&eacute; pass&eacute; en param&egrave;tre',
//I
	'item_mots_cles_association_mots' => 'aux mots-cl&eacute;s',
	'objet_mots' => 'Mots-cl&eacute;s',
//J
//K
//L
//M
//N
//O
//P
	'parents_du_mot_ou_des_mots' => 'Parents de tous les mots-cl&eacute;s (qui en ont !)',
	'pas_de_parents' => 'Ce mot-cl&eacute; n\'a pas de mot-cl&eacute; parent',
	'plus_d_info_sur' => 'Plus d\'informations sur &laquo;&nbsp;@titre@&nbsp;&raquo;',
//Q
//R

	'retirer_mots_parents' => 'Retirer les mots-cl&eacute;s d&eacute;coch&eacute;s',

//S
	'sommet' => 'Sommet',
	'sous_niveau' => 'Sous-niveau @niv@',
//T
//U
//V
//W
//X
//Y
//Z

);
?>