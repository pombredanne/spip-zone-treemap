<?php


	/**
	 * SPIP-Météo : prévisions météo dans vos squelettes
	 *
	 * Copyright (c) 2006
	 * Agence Artégo http://www.artego.fr
	 *  
	 * Ce programme est un logiciel libre distribue sous licence GNU/GPL.
	 * Pour plus de details voir le fichier COPYING.txt.
	 *  
	 **/


	$GLOBALS[$GLOBALS['idx_lang']] = array(

		# partie publique
		'meteo' => "Météo",
		'meteo_pluie' => "Pluie",
		'meteo_orage' => "Orage",
		'meteo_neige' => "Neige",
		'meteo_verglas' => "Verglas",
		'meteo_brouillard' => "Brouillard",
		'meteo_vent' => "Vent",
		'meteo_nuages' => "Nuages",
		'meteo_lune' => "Pleine-lune",
		'meteo_lune-nuage' => "Pleine-lune et nuages éparses",
		'meteo_lune-nuages' => "Pleine-lune et nuages",
		'meteo_soleil' => "Soleil",
		'meteo_soleil-nuage' => "Soleil et nuages éparses",
		'meteo_soleil-nuages' => "Soleil et nuages",
		'meteo_inconnu' => "Inconnu",
		'meteo_NA' => "Inconnu",
		'temperature_inconnue' => "Température inconnue",

		'ajouter_une_meteo' => "Ajouter une météo",
		'numero_meteo' => "M&Eacute;T&Eacute;O NUM&Eacute;RO",
		'liste_des_meteos' => "Liste des météos",
		'nouvelle_ville' => "Nouvelle ville",
		'editer_meteo' => "Editer les réglages d'une météo",
		'ville' => "Ville",
		'ville_note' => "Entrez le nom de la ville, sans accents. Vous devez être connectés à internet pour récupérer le code météo via le formulaire suivant.",
		'chercher' => "Chercher",
		'code_ville' => "Code de la ville",
		'enregistrer' => "Enregistrer",
		'retour_liste_meteo' => "Retour à la liste des météos",
		'modifier_meteo' => "Modifier les réglages de cette météo",
		'action' => "Actions sur cette météo",
		'action_aucune' => "Aucune",
		'action_poubelle' => "Supprimer",
		'changer' => "Changer",
		'texte_probleme_recuperation_flux' => "Vérifier la connexion à internet car le plugin n'arrive pas à récupérer le flux météo depuis Weather.com",
		'probleme_de_recuperation_du_flux' => "Impossible de récupérer le flux météo",
		'previsions_meteo' => 'Prévision météo',
		'date_derniere_maj' => "Dernière mise à jour",
		'en_ligne' => "En ligne",
		'en_erreur' => "En erreur",
		'meteos_trouvees' => 'Météos trouvées',
		

		'Z' => 'ZZzZZzzz'

	);

?>
