<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/todo/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer_bando' => 'Choses à faire',
	'configurer_titre' => 'Configurer les listes de choses à faire',

	// L
	'label_statut' => 'Statut',
	'label_titre' => 'Titre',

	// S
	'statut_afaire' => 'À faire',
	'statut_encours' => 'En cours',
	'statut_termine' => 'Terminé',

	// T
	'tri_cle' => 'Remettre dans l\'ordre',
	'tri_statut' => 'Trier par statut',
	'tri_titre' => 'Trier par titre'
);

?>
