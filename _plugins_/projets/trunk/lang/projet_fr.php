<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_projet' => 'Ajouter ce projet',

	// E
	'explication_actif' => 'Super-statut du projet',
	'explication_date_debut' => 'Date de démarrage du projet',
	'explication_date_livraison' => 'Date de livraison effective du projet',
	'explication_date_livraison_prevue' => 'Date à laquelle le projet doit être livré',
	'explication_descriptif' => 'Quelques explications complémentaires, commentaires...',
	'explication_enjeux' => 'Décrire les enjeux, s\'il y en a',
	'explication_id_parent' => 'Sentez-vous libre de sélectionner un projet parent...',
	'explication_id_projets_cadre' => 'Vous pouvez préciser le cadre du projet en le sélectionnant dans cette liste',
	'explication_id_projets_categorie' => 'Vous pouvez péciser la catégorie du projet (en général le type de prestation réalisée dans le cadre de ce projet)',
	'explication_methode' => 'Quelques mots sur la méthode recommandée pour réaliser le projet',
	'explication_nb_heures_reelles' => 'Nombre d\'heures réellement passées sur le projet',
	'explication_objectif' => 'Les objectifs du projet, en quelques mots...',
	'explication_url_site' => 'Vous pouvez indiquer une url permettant de voir le projet en ligne',

	// I
	'icone_creer_projet' => 'Créer un projet',
	'icone_modifier_projet' => 'Modifier ce projet',
	'info_1_projet' => 'Un projet',
	'info_aucun_projet' => 'Aucun projet',
	'info_nb_projets' => '@nb@ projets',
	'info_projets_auteur' => 'Les projets de cet auteur',

	// L
	'label_actif' => 'Actif',
	'label_date_debut' => 'Date de démarrage',
	'label_date_livraison' => 'Date livraison effective',
	'label_date_livraison_prevue' => 'Date livraison prévue',
	'label_descriptif' => 'Descriptif du projet',
	'label_enjeux' => 'Enjeux',
	'label_id_parent' => 'Projet parent',
	'label_id_projets_cadre' => 'Cadre du projet',
	'label_id_projets_categorie' => 'Catégorie du projet',
	'label_methode' => 'Méthode',
	'label_nb_heures_estimees' => 'Nb heures estimees',
	'label_nb_heures_reelles' => 'Nombre d\'heures réel',
	'label_nom' => 'Nom du projet',
	'label_objectif' => 'Objectif',
	'label_url_site' => 'Voir en ligne',

	// R
	'retirer_lien_projet' => 'Retirer ce projet',
	'retirer_tous_liens_projets' => 'Retirer tous les projets',

	// T
	'texte_ajouter_projet' => 'Ajouter un projet',
	'texte_changer_statut_projet' => 'Ce projet est :',
	'texte_creer_associer_projet' => 'Créer et associer un projet',
	'texte_statut_preparation'    => "En préparation",
	'texte_statut_redaction'      => "Cahier des charges en cours",
	'texte_statut_elabore'        => "Projet élaboré",
	'texte_statut_chiffrage'      => "Proposition en cours",
	'texte_statut_propose'        => "Proposé",
	'texte_statut_accepte'        => "Accepté",
	'texte_statut_accord'         => "Accord de fabrication",
	'texte_statut_fabrication'    => "En fabrication",
	'texte_statut_fabrique'       => "Pret à livrer",
	'texte_statut_test'           => "Tests en cours",
	'texte_statut_recette'        => "Recette en cours",
	'texte_statut_production'     => "En production",
	'texte_statut_cloture'        => "Cloturé",
	'texte_statut_arrete'         => "Arrêté",
	'texte_statut_abandonne'      => "Abandonné",
	'texte_statut_poubelle'       => "Poubelle",
	'titre_langue_projet'         => 'Langue de ce projet',
	'titre_logo_projet'           => 'Logo de ce projet',
	'titre_projet'                => 'Projet',
	'titre_projets'               => 'Projets',
	'titre_projets_rubrique'      => 'Projets de la rubrique',

);

?>
