<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'projets_description' => 'Crée une base de données de projets, permettant de définir les objectifs, les enjeux, ... d\'un projet.

Ce plugin est un élément autonome de SPIPMINE.',
	'projets_nom' => 'projets',
	'projets_slogan' => 'Gerer des projets avec SPIP',
);

?>