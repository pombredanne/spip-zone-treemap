<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_projets_categorie' => 'Ajouter cette catégorie de projet',

	// E
	'explication_descriptif' => 'Description plus précise de la catégorie de projet',
	'explication_titre' => 'Donner un titre à cette catégorie de projet',

	// I
	'icone_creer_projets_categorie' => 'Créer une catégorie de projet',
	'icone_modifier_projets_categorie' => 'Modifier cette catégorie de projet',
	'info_1_projets_categorie' => 'Une catégorie de projet',
	'info_aucun_projets_categorie' => 'Aucune catégorie de projet',
	'info_nb_projets_categories' => '@nb@ catégories de projets',
	'info_projets_categories_auteur' => 'Les catégories de projets de cet auteur',

	// L
	'label_descriptif' => 'Descriptif de la catégorie de projet',
	'label_titre' => 'Titre de la catégorie',

	// R
	'retirer_lien_projets_categorie' => 'Retirer cette catégorie de projet',
	'retirer_tous_liens_projets_categories' => 'Retirer toutes les catégories de projets',

	// T
	'texte_ajouter_projets_categorie' => 'Ajouter une catégorie de projet',
	'texte_changer_statut_projets_categorie' => 'Cette catégorie de projet est :',
	'texte_creer_associer_projets_categorie' => 'Créer et associer une catégorie de projet',
	'titre_langue_projets_categorie' => 'Langue de cette catégorie de projet',
	'titre_logo_projets_categorie' => 'Logo de cette catégorie de projet',
	'titre_projets_categorie' => 'Catégorie de projet',
	'titre_projets_categories' => 'Catégories de projets',
	'titre_projets_categories_rubrique' => 'Catégories de projets de la rubrique',
);

?>