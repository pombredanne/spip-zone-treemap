<?php
/**
 * Plugin SPIP-Projet
 * Licence GPL
 * Eric Lupinacci, Quentin Drouet
 *
 * Ficher de langue: [fr]
 *
 */

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'associer_au_projet' => 'ASSOCIER AU PROJET :',
	'aucun' => 'Aucun',
	'aucun_projet' => 'Aucun projet &agrave; cette adresse',

	// B
	'bouton_creer_projet' => 'Cr&eacute;er un projet',
	'bulle_modifier_projet' => 'Modifier le projet',
	'bulle_puce_publie' => 'publi&eacute; en ligne',
	'bulle_puce_preparation' => 'en cours de pr&eacute;paration',
	'bulle_puce_ferme' => 'ferm&eacute;',

	// C
	'cfg_titre_projet' => 'SPIP-Projet',
	'cfg_descr_projet' => 'Cette page de configuration permet de choisir les param&egrave;tres de base pour tous les projets.',
	'cfg_boite_projet' => 'Configuration de SPIP-Projet',
	'cfg_msg_webmestre_nok' => 'Seuls les webmestres sont autoris&eacute;s &agrave; voir cette page',
	'cfg_lgd_autorisation_creer' => 'Autorisation &#171;Cr&eacute;er un projet&#187;',
	'cfg_lgd_autorisation_modifier' => 'Autorisation &#171;Modifier un projet&#187;',
	'cfg_lgd_autorisation_voir' => 'Autorisation &#171;Voir un projet&#187;',
	'cfg_inf_type_autorisation' => 'Si vous choisissez par statut ou par auteur, il vous sera demand&eacute; ci-dessous votre s&eacute;lection de statuts ou d\'auteurs.',
	'cfg_lbl_type_autorisation' => 'M&eacute;thode d\'autorisation',
	'cfg_lbl_autorisation_webmestre' => 'Autoriser les webmestres uniquement',
	'cfg_lbl_autorisation_statuts' => 'Autoriser par statut d\'auteurs',
	'cfg_lbl_autorisation_auteurs' => 'Autoriser par liste d\'auteurs',
	'cfg_lbl_statuts_auteurs' => 'Statuts possibles',
	'cfg_lbl_ligatures' => 'Objets pouvant &ecirc;tre li&eacute;s avec un projet',
	'cfg_lbl_liste_auteurs' => 'Auteurs du site',


	// D

	// E

	// F

	// I
	'icone_ecrire_projet' => 'Cr&eacute;er un nouveau projet',
	'icone_modifier_projet' => 'Modifier ce projet',
	'icone_retour_projet' => 'Retour au projet',
	'info_nouveau_projet' => 'Nouveau projet',
	'info_numero_projet' => 'PROJET NUM&Eacute;RO :',

	// L
	'label_selecteur_projet' => 'Associer au projet',
	'label_choisir_projet' => 'Associer l\'article &agrave; un projet',
	'logo_projet' => 'LOGO DU PROJET',

	// M
	'message_objet_associe' => 'Le projet a &eacute;t&eacute; associ&eacute;',

	// N

	// P
	'projet_autorisations' => 'Configuration des autorisations',
	'projet_ligatures' => 'Configuration des ligatures',
	'projet' => 'projet',
	'projets' => 'projets',

	// R

	// S

	// T
	'texte_modifier_projet' => 'Modifier le projet&nbsp;:',
	'texte_projet_statut' => 'Ce projet est :',
	'titre_contenu_projets_page' => 'Les projets que vous suivez',
	'titre_page_projets_page' => 'Vos projets',
	'titre_formulaire_associer' => 'PROJETS',
	'titre_table_vos_contributions' => 'Les projets auxquels vous contribuez',
	'titre_table_vos_projets_en_preparation' => 'Vos projets en pr&eacute;paration',
	'titre_table_vos_projets_proposes' => 'Vos projets en attente de validation',
	'titre_table_vos_projets_fermes' => 'Vos projets clos',
	'titre_table_vos_projets_publies' => 'Vos projets publi&eacute;s',

	// U
	'upgrade_base_version' => 'Mise &agrave; jour des tables du plugin "Projet" en version @version@.',

	// V
	'voir_projets' => 'Projets',


	// Z
);


?>