<?php

// Sécurité
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'anythingslider' => 'AnythingSlider',
	'configurer_anythingslider' => 'Configurer AnythingSlider',
	'explication_configurer_anythingslider' => 'Choisissez les éléments à insérer dans l\'en-tête des pages publiques (voir la <a href="https://github.com/ProLoser/AnythingSlider/wiki">documentation du script</a>).',
	'label_scripts' => 'Scripts optionnels',
	'label_themes' => 'Thèmes',
);

?>