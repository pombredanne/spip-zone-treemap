<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-anythingslider
// Langue: fr
// Date: 27-11-2012 18:34:56
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
	'anythingslider_description' => 'Ce plugin permet d\'insérer facilement le script jQuery [AnythingSlider->https://github.com/CSS-Tricks/AnythingSlider] dans votre SPIP pour l’utiliser dans vos squelettes. Il peut être également être utilisé comme ressource pour d\'autres plugins.',
	'anythingslider_slogan' => 'Faire défiler du contenu dans vos squelettes',
);
?>