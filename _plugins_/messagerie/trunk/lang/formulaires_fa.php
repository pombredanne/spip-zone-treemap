<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/formulaires?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'email_invalide' => 'اين نشاني ايميل اعتبار ندارد',
	'erreur_enregistrement' => 'هنگام ثبت نام <b>@nom@</b> خطايي رخ داده ',

	// I
	'info_obligatoire' => '*',
	'info_obligatoire_legende' => 'ميدان الزامي',
	'info_obligatoire_rappel' => 'اين اطلاعات الزامي است!',

	// V
	'valeur_incorrecte' => 'اين مقدار درست نيست'
);

?>
