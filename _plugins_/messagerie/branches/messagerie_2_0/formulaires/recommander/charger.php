<?php
/*
 * Plugin messagerie
 * Licence GPL
 * (c) 2008 C.Morin Yterium
 *
 */


/**
 * Chargement des valeurs par defaut de #FORMULAIRE_RECOMMANDER
 *
 * @return array
 */
function formulaires_recommander_charger_dist(){
	$valeurs = array('destinataire'=>'','destinataires'=>'','texte'=>'');

	return $valeurs;
}

?>