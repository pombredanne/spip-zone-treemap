<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/messagerie/branches/messagerie_2_0/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'email_invalide' => 'Cette adresse mail n\'est pas valide',
	'erreur_enregistrement' => 'Une erreur est survenue lors de l\'enregistrement',

	// I
	'info_obligatoire' => '*',
	'info_obligatoire_legende' => 'champs obligatoires',
	'info_obligatoire_rappel' => 'Cette information est obligatoire !',

	// V
	'valeur_incorrecte' => 'Cette valeur n\'est pas correcte'
);

?>
