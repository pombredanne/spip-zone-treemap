<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/recommander?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_destinataire' => 'Nombre',
	'label_envoyer' => 'Recomendar',
	'label_texte' => 'Añadir mensaje',

	// R
	'recommander_a_un_ami' => 'Recomendar a un amigo o amiga',

	// X
	'x_vous_recommande_le_vin' => '@nom_lien@ te recomiendo @titre_vin@'
);

?>
