<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/messagerie?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_message_envoye' => 'No has enviado ningún mensaje',
	'aucun_message_recu' => 'Por ahora no se han recibido mensajes',

	// L
	'label_selection' => 'Seleccionar',

	// M
	'mail_sujet_envoyermessage' => '[@nom_site@] Tienes un nuevo mensaje',
	'mail_texte_envoyermessage' => '@nom@ has enviado un nuevo mensaje (@url_site@) !',

	// R
	'replier' => 'Reenviar este mensaje',
	'repondre_message' => 'Contestar este mensaje',

	// T
	'texte_des_nouveaux_messages' => 'Tienes <b>@nb@</b> nuevos mensajes',
	'texte_signature_email' => '
----
Correo enviado por @nom_site@ (@url_site@)',
	'texte_un_nouveau_message' => 'Tienes <b>un</b> nuevo mensaje',
	'titre_message_general' => 'Aviso general'
);

?>
