<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ecrire_message?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_un_ami' => 'Escribir un mensaje',

	// E
	'envoye_a' => 'A ',
	'envoyer_un_message' => 'enviar un mensaje',

	// L
	'label_destinataire' => 'Para',
	'label_effacer' => 'eliminar',
	'label_envoyer' => 'Enviar',
	'label_marquer_lus' => 'marcar como leído',
	'label_marquer_non_lus' => 'marcar como no leído',
	'label_objet' => 'Título',
	'label_texte_message' => 'Mensaje',

	// M
	'message_envoye' => 'Mensaje enviado',
	'message_envoye_erreur' => 'Ha ocurrido un error, el mensaje no se ha enviado',

	// S
	'selection_aucun' => 'ninguno',
	'selection_lus' => 'leídos',
	'selection_non_lus' => 'no leídos',
	'selection_selectionner' => 'seleccionar',
	'selection_tous' => 'todos'
);

?>
