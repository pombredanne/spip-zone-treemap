<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/ecrire_message?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_un_ami' => 'Napísať správu',

	// E
	'envoye_a' => 'Adresát',
	'envoyer_un_message' => 'poslať správu',

	// L
	'label_destinataire' => 'Príjemca',
	'label_effacer' => 'vymazať',
	'label_envoyer' => 'Poslať',
	'label_marquer_lus' => 'označiť ako prečítanú',
	'label_marquer_non_lus' => 'označiť ako neprečítanú',
	'label_objet' => 'Predmet',
	'label_texte_message' => 'Správa',

	// M
	'message_envoye' => 'Vaša správa bola úspešne odoslaná',
	'message_envoye_erreur' => 'Vyskytla sa chyba, vaša správa nebola odoslaná',

	// S
	'selection_aucun' => 'žiadna',
	'selection_lus' => 'prečítané',
	'selection_non_lus' => 'neprečítané',
	'selection_selectionner' => 'vybrať',
	'selection_tous' => 'všetky'
);

?>
