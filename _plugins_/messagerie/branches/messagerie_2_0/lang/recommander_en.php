<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/recommander?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_destinataire' => 'His name',
	'label_envoyer' => 'Recommend!',
	'label_texte' => 'Add a message',

	// R
	'recommander_a_un_ami' => 'Recommend this wine to a friend',

	// X
	'x_vous_recommande_le_vin' => '@nom_lien@ recommend the wine @titre_vin@'
);

?>
