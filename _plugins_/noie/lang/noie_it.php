<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/noie?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'noie_info' => 'Per navigare meglio su questo sito su tutto il Web, ti consigliamo di <strong>aggiornare il tuo <a href="http://www.microsoft.com/italy/windows/internet-explorer/">Internet Explorer</a></string> o di provare un altro browser popolare come <a href="http://www.mozilla-europe.org/it/firefox/">Firefox</a>, <a href="http://www.google.it/chrome/">Chrome</a>, <a href="http://www.opera.com/browser/">Opera</a> o <a href="http://www.apple.com/it/safari/">Safari</a>.', # MODIF
	'noie_titre' => 'Attenzione, il tuo Internet Explorer non è aggiornato!',

	// T
	'toocool_alt' => 'Too Cool for IE',
	'toocool_info' => 'Il sito che stai tentando di vedere è certificato "Too Cool for Internet Explorer".
<br />Per favore, prendi un momento di tempo e prova uno di questi grandi browsers: <a href="http://www.w3junkies.com/toocool/index.php?language=it">http://www.w3junkies.com/toocool/</a>',
	'toocool_titre' => 'Too Cool for Internet Explorer'
);

?>
