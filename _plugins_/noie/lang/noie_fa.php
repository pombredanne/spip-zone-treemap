<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/noie?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'noie_info' => 'براي گذر از ميان صفحات اين سايت و اينترنت به شما توصيه مي‌كنيم اينترنت اكسپلورر خود را روزآمد سازيد<a href="http://www.microsoft.com/france/windows/internet-explorer/">Internet Explorer</a></strong> يا از مرورگر ديگري مانند اين‌ها استفاده نماييد <a href="http://www.mozilla-europe.org/fr/firefox/">Firefox</a>, <a href="http://www.google.fr/chrome">Chrome</a>, <a href="http://www.opera.com/">Opera</a> ou <a href="http://www.apple.com/fr/safari/">Safari</a>.', # MODIF
	'noie_titre' => 'توجه! اينترنت اكسپلورر شما روزآمد نيست!',

	// T
	'toocool_alt' => 'خيلي مناسب براي آي.اي(اينترنت اكسپلورر) ',
	'toocool_info' => 'سايتي كه سعي داريد ببينيد «خيلي خوب براي آي.اي» تشخيص داده شده است.
<br />لطفاً كمي وقت بگذاريد و اين مرورگرهاي بزرگ را امتحان كنيد: <a href="http://www.w3junkies.com/toocool/index.php?language=fr">http://www.w3junkies.com/toocool/</a>',
	'toocool_titre' => 'خيلي مناسب براي اينترنت اكسپلورر'
);

?>
