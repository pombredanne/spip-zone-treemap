<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/noie?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'noie_info' => 'To navigate through this site and the rest of the Internet with better results, we recommend that you <strong>update your <a href="http://www.microsoft.com/windows/downloads/default.aspx">Internet Explorer</a></strong> or  try another popular browser such as <a href="http://www.mozilla-europe.org/en/firefox/">Firefox</a>, <a href="http://www.google.com/chrome?hl=en">Chrome</a>, <a href="http://www.opera.com/">Opera</a> or <a href="http://www.apple.com/safari/download/">Safari</a>.', # MODIF
	'noie_titre' => 'Warning: your version of Internet Explorer is not up to date!',

	// T
	'toocool_alt' => 'Too Cool for IE',
	'toocool_info' => 'The site you are trying to view has been deemed "Too Cool for Internet Explorer".
<br />Please take a moment to try one of these great browsers: <a href="http://www.w3junkies.com/toocool/index.php?language=en">http://www.w3junkies.com/toocool/</a>',
	'toocool_titre' => 'Too Cool for Internet Explorer'
);

?>
