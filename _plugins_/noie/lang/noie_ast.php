<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/noie?lang_cible=ast
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'noie_info' => 'Pa navegar de forma más afayadiza per esti sitiu y pol restu de la Web, encamentamos <strong>anovar el to <a href="http://www.microsoft.com/france/windows/internet-explorer/">Internet Explorer</a></strong> o probar otru navegador popular como <a href="http://www.mozilla.com/en-US/firefox/all.html">Firefox</a>, <a href="http://www.google.com/chrome">Chrome</a>, <a href="http://www.opera.com/">Opera</a> o <a href="http://www.apple.com/safari/">Safari</a>.', # MODIF
	'noie_titre' => 'Atención, ¡el to Internet Explorer nun ta anováu!',

	// T
	'toocool_alt' => 'Gayaspero enforma pa IE',
	'toocool_info' => 'El sitiu que tentas amosar xulgose como «demasiao bono pa Internet Explorer ».
<br />Si quies, tómate un momentu pa prebar algún d\'estos escelentes ñavegadores: <a href="http://www.w3junkies.com/toocool/index.php?language=es">http://www.w3junkies.com/toocool/</a>',
	'toocool_titre' => 'Gayaspero enforma pa Internet Explorer'
);

?>
