<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/noie/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'noie_info' => 'Pour naviguer de façon plus satisfaisante sur ce site et le reste du Web, nous vous recommandons d\'<strong>actualiser votre <a href="http://www.microsoft.com/france/windows/internet-explorer/">Internet Explorer</a></strong> ou d\'essayer un autre navigateur populaire comme <a href="http://www.mozilla-europe.org/fr/firefox/">Firefox</a>, <a href="http://www.google.fr/chrome">Chrome</a> ou <a href="http://www.opera.com/">Opera</a>.',
	'noie_titre' => 'Attention, votre Internet Explorer n\'est pas à jour !',

	// T
	'toocool_alt' => 'Trop cool pour IE',
	'toocool_info' => 'Le site que vous tentez d\'afficher a été jugé « trop bien pour Internet Explorer ».
<br />Prenez, s\'il vous plaît, un moment afin d\'essayer l\'un de ces excellents navigateurs : <a href="http://www.w3junkies.com/toocool/index.php?language=fr">http://www.w3junkies.com/toocool/</a>',
	'toocool_titre' => 'Trop Cool pour Internet Explorer'
);

?>
