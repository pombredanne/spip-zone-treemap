<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/noie?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'noie_info' => 'Para navegar de manera satisfactoria en este sitio web, le recomendamos utilizar un navegador <strong>conforme a los estándares internacionales</strong>, que este vela en adoptar. Le sugerimos probar otros navegadores populares, como <a href="http://www.mozilla-europe.org/es/firefox/">Firefox</a>, <a href="http://www.google.es/chrome">Chrome</a>, <a href="http://www.opera.com/">Opera</a> o <a href="http://www.apple.com/rd/safari/">Safari</a>, o al menos pasar su <a href="http://www.microsoft.com/latam/windows/internet-explorer/">Internet Explorer</a>a versiones menos incompatibles. ', # MODIF
	'noie_titre' => '¡OjO: Su Internet Explorer es incompatible con las normas internacionales!',

	// T
	'toocool_alt' => 'Demasiado Cool para IE',
	'toocool_info' => 'El sitio que estás tratando de ver ha sido juzgado "Demasiado Cool Para Internet Explorer".
<br />Por favor tómate el tiempo de probar uno de éstos geniales navegadores : <a href="http://www.w3junkies.com/toocool/index.php?language=es">http://www.w3junkies.com/toocool/</a>',
	'toocool_titre' => 'Demasiado Cool para Internet Explorer'
);

?>
