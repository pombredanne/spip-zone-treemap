<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'noie_info' => 'Ji gali iškraipyti lankomus puslapius, gali neveikti kai kurios svetaini&u funkcijos. Be to, naudojant pasenusia naršykle zymiai padid?ja rizika tapti virusu ir piktavališku interneto svetainiu auka.
<br />Rekomenduojame atnaujinti ja iki <a href="http://www.microsoft.com/windows/downloads/ie/getitnow.mspx">Internet Explorer 7</a> arba <a href="http://www.microsoft.com/windows/Internet-explorer/beta/default.aspx">Internet Explorer 8 (beta)</a> versijos.
<br />Atnaujinimas nieko nekainuoja. Darbe tai gali padaryti kompiuteriniu sistemu administratorius.
<br />Taip pat siùlome išbandyti puikias Internet Explorer alternatyvas: populiaruj? <a href="http://getfirefox.com">Firefox</a>, spar&ciaja <a href="http://www.opera.com">Opera</a> arba <a href="http://www.apple.com/safari/download/">Safari</a>.',
	'noie_titre' => 'Ar zinai, kad naudoji pasenusia naršykle?',

	// T
	'toocool_alt' => 'Trop cool pour IE', # NEW
	'toocool_info' => 'Le site que vous tentez d\'afficher a été jugé « trop bien pour Internet Explorer ».
<br />Prenez, s\'il vous plaît, un moment afin d\'essayer l\'un de ces excellents navigateurs : <a href="http://www.w3junkies.com/toocool/index.php?language=fr">http://www.w3junkies.com/toocool/</a>', # NEW
	'toocool_titre' => 'Trop Cool pour Internet Explorer' # NEW
);

?>
