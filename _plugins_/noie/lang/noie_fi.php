<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'noie_info' => 'Sinun kannattaa päivittää selaimesi uudempaan versioon, jotta saisit parhaan mahdollisen käyttökokemuksen näiltä sivuilta, .
<br /><a href="http://www.microsoft.com/finland/windows/downloads/ie/getitnow.mspx">Hae Internet Explorer 7 täältä</a>.
<br />Voit käyttää myös muita suosittuja selaimia, kuten <a href="http://www.opera.com">Opera</a>, <a href="http://www.mozilla-europe.org/fi/firefox/">Firefox</a>, <a href="http://www.apple.com/fi/safari/">Safari</a> tai <a href="http://www.google.com/chrome/index.html?hl=fi&brand=CHMG&utm_source=fi-hpp&utm_medium=hpp&utm_campaign=fi">Crome</a>.
<br />Nykyisen selaimesi tekniikka ja standardit ovat vanhentuneet vuosia sitten ja tämä heikentää tietoturvaasi!',
	'noie_titre' => 'Käytät Internet Explorerin vanhaa versiota',

	// T
	'toocool_alt' => 'Trop cool pour IE', # NEW
	'toocool_info' => 'Le site que vous tentez d\'afficher a été jugé « trop bien pour Internet Explorer ».
<br />Prenez, s\'il vous plaît, un moment afin d\'essayer l\'un de ces excellents navigateurs : <a href="http://www.w3junkies.com/toocool/index.php?language=fr">http://www.w3junkies.com/toocool/</a>', # NEW
	'toocool_titre' => 'Trop Cool pour Internet Explorer' # NEW
);

?>
