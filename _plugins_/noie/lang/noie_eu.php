<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/noie?lang_cible=eu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'noie_info' => 'Gune honetaz eta Internetez hobeki gozatzeko zure <a href="http://www.microsoft.com/france/windows/internet-explorer/">Internet Explorer</a> eguneratzea gomendatzen dizugu, edo nabigatzaile arrakastatsuago bat baliatzea proposatzen dizugu, <a href="http://www.mozilla.com/eu/firefox/">Firefox</a>, <a href="http://www.google.fr/chrome">Chrome</a>, <a href="http://www.opera.com/">Opera</a> edo <a href="http://www.apple.com/fr/safari/">Safari</a> bezalakoa.', # MODIF
	'noie_titre' => 'Kontuz zure Internet Explorer ez da eguneratua !',

	// T
	'toocool_alt' => 'Hobekiegi IErendako',
	'toocool_info' => 'Bistarazi nahi duzun gunea "Hobekiegi Internet Explorerendako" sailkatua izan da.<br />Otoi, nabigatzaila bikain horietarik bat saiatzeko astia har dezazue : <a href="http://www.w3junkies.com/toocool/index.php?language=fr">http://www.w3junkies.com/toocool/</a>',
	'toocool_titre' => 'Hobekiegi Internet Explorerendako'
);

?>
