<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'noie_info' => 'För att få en bättre och säkrare upplevelse på nätet rekommenderar vi att du <a href="http://www.microsoft.com/sverige/windows/downloads/ie/getitnow.mspx">hämtar en nyare version av Internet Explorer</a>. Uppgraderingen är kostnadsfri.
Sitter du på jobb och inte har kontroll över din dator själv bör du kontakta din IT-ansvarige.
<br />Vi kan också varmt rekommendera dig att prova någon av följade alternativa webbläsare <a href="http://www.opera.com">Opera</a>, <a href="http://mozilla.com">FireFox</a> eller <a href="http://www.apple.com/safari/download/">Safari</a>
<br /><a href="http://labs.finn.no/blog/finn-anbefaler-ie6-brukere-a-oppgradere-sin-nettleser">Den här uppmaningen har sitt ursprung i Norge och på en av deras största sajter, finn.no, kan du läsa om varför du bör uppgradera.</a>',
	'noie_titre' => 'Tips från oss: Du har en gammal version av webbläsaren Internet Explorer.',

	// T
	'toocool_alt' => 'Trop cool pour IE', # NEW
	'toocool_info' => 'Le site que vous tentez d\'afficher a été jugé « trop bien pour Internet Explorer ».
<br />Prenez, s\'il vous plaît, un moment afin d\'essayer l\'un de ces excellents navigateurs : <a href="http://www.w3junkies.com/toocool/index.php?language=fr">http://www.w3junkies.com/toocool/</a>', # NEW
	'toocool_titre' => 'Trop Cool pour Internet Explorer' # NEW
);

?>
