<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'noie_info' => 'For å få en best mulig opplevelse av å bruke våre nettsider, kan du gratis <a href="http://www.microsoft.com/norge/windows/downloads/ie/getitnow.mspx">hente en nyere versjon av Internet Explorer</a>. Bruker du en jobb-PC bør du kontakte IT-ansvarlig.
<br /><a href="http://labs.finn.no/blog/finn-anbefaler-ie6-brukere-a-oppgradere-sin-nettleser">På FINN labs kan du lese mer om hvorfor vi anbefaler denne oppgraderingen.</a>',
	'noie_titre' => 'Tips fra oss: Du har en eldre versjon av nettleseren Internet Explorer.',

	// T
	'toocool_alt' => 'Trop cool pour IE', # NEW
	'toocool_info' => 'Le site que vous tentez d\'afficher a été jugé « trop bien pour Internet Explorer ».
<br />Prenez, s\'il vous plaît, un moment afin d\'essayer l\'un de ces excellents navigateurs : <a href="http://www.w3junkies.com/toocool/index.php?language=fr">http://www.w3junkies.com/toocool/</a>', # NEW
	'toocool_titre' => 'Trop Cool pour Internet Explorer' # NEW
);

?>
