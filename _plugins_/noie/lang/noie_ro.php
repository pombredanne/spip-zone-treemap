<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/noie?lang_cible=ro
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'noie_info' => 'Pour naviguer de façon plus satisfaisante sur ce site et le reste du Web, nous vous recommandons d\'<strong>actualiser votre <a href="http://www.microsoft.com/france/windows/internet-explorer/">Internet Explorer</a></strong> ou d\'essayer un autre navigateur populaire comme <a href="http://www.mozilla-europe.org/fr/firefox/">Firefox</a>, <a href="http://www.google.fr/chrome">Chrome</a>, <a href="http://www.opera.com/">Opera</a> ou <a href="http://www.apple.com/fr/safari/">Safari</a>.', # NEW
	'noie_titre' => 'Attention, votre Internet Explorer n\'est pas à jour !', # NEW

	// T
	'toocool_alt' => 'Prea cool pentru IE',
	'toocool_info' => 'Site-ul pe care încercaţi să îl afişati a fost judecat ca fiind « prea bun pentru Internet Explorer ».
<br />Vă rugăm să petreceţi un moment pentru a încerca unul dintre aceste excelente navigatoare de web: <a href="http://www.w3junkies.com/toocool/">http://www.w3junkies.com/toocool/</a>',
	'toocool_titre' => 'Prea Cool pentru Internet Explorer'
);

?>
