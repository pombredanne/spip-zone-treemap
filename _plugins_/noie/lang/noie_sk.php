<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/noie?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'noie_info' => 'Na lepšie prehliadanie tejto stránky a celého internetu vám odporúčame, aby ste <strong>si aktualizovali <a href="http://www.microsoft.com/slovakia/windows/internet-explorer/">Internet Explorer</a></strong> alebo vyskúšali  iný prehliadač, ako populárny <a href="http://www.mozilla-europe.org/fr/firefox/">Firefox,</a> <a href="http://www.google.fr/chrome">Chrome</a> alebo <a href="http://www.opera.com/">Operu.</a>',
	'noie_titre' => 'Pozor, váš Internet Explorer nie je aktuálny!',

	// T
	'toocool_alt' => 'Príliš kúl pre IE',
	'toocool_info' => 'Stránka, ktorú sa snažíte zobraziť, bola označená ako Príliš kúl pre Internet Explorer.
<br />Prosím, doprajte si chvíľu na vyskúšanie jedného z týchto skvelých prehliadačov: <a href="http://www.w3junkies.com/toocool/index.php?language=en">http://www.w3junkies.com/toocool/</a>',
	'toocool_titre' => 'Príliš kúl pre Internet Explorer'
);

?>
