<?php

// This is a SPIP-forum module file  --  Ceci est un fichier module de SPIP-forum

$GLOBALS[$GLOBALS['idx_lang']] = array(

//A
'ajouter_commentaire' => 'Ajouter un commentaire',
'archive' => 'Archive', // Non utilise
'archives' => 'Archives',
'attime' => '&agrave;',
'aucune_archive' => 'Aucune archive', // Non utilise
'aucune_categorie' => 'Aucune cat&eacute;gorie', // Non utilise
'aucun_commentaire' => 'Aucun commentaire', // Non utilise
'aucune_langue' => 'Aucune langue', // Non utilise
'aucun_lien' => 'Auncun lien', // Non utilise
'aucun_trackback' => 'Aucun trackback', // Non utilise

//B
'boss' => 'Administrateur', // Oubli

//C
'calendrier' => 'Calendrier', // Non utilise
'categorie' => 'Cat&eacute;gorie', // Non utilise
'categories' => 'Cat&eacute;gories',
'commentaire' => 'commentaire',
'commentaires' => 'commentaires',
'commentaires_pour' => 'commentaires pour',
'commentaires_fermes' => 'Les commentaires pour ce billet sont ferm&eacute;s',
'credits' => '<a href="http://www.spip.net/fr">motoris&eacute; par SPIP</a>,
<a href="http://www.spip-contrib.net/Spip-Clear">propuls&eacute; par Spip.Clear</a>',

//D
'date_jour_abbr_1' => 'dim', // Non utilise
'date_jour_abbr_2' => 'lun', // Non utilise
'date_jour_abbr_3' => 'mar', // Non utilise
'date_jour_abbr_4' => 'mer', // Non utilise
'date_jour_abbr_5' => 'jeu', // Non utilise
'date_jour_abbr_6' => 'ven', // Non utilise
'date_jour_abbr_7' => 'sam', // Non utilise
'de' => 'de', // Non utilise

//E

//F
'fil_rss' => 'fil rss',
'fil_rss_commentaires' => 'fil rss commentaires',
'fil_atom' => 'fil atom',
'fil_atom_commentaires' => 'fil atom commentaires',

//G
'go_main' => 'Aller au contenu',
'go_sidebar' => 'Aller au menu',
'go_search' => 'Aller &agrave; la recherche',

//H
'hebergement' => 'h&eacute;bergement par',

//I
'info_navigation' => 'Choisissez l\'emplacement de la bande de navigation', // CFG
'info_choix_theme' => 'Choisissez le th&egrave;me que vous souhaitez utiliser pour votre blog', // CFG

//L
'label_navigation_droite' => 'Bande de navigation &agrave; droite (valeur par d&eacute;faut)', // CFG
'label_navigation_gauche' => 'Bande de navigation &agrave; gauche', // CFG
'label_theme_defaut' => 'Th&egrave;me par d&eacute;faut (type DotClear)', // CFG
'label_theme_perso' => 'Th&egrave;me perso (&agrave; d&eacute;finir ci-dessous)', // CFG
'label_nom_theme' => 'Saisissez le nom du th&egrave;me perso (correspond &agrave xxxx du sous-r&eacute;pertoire squelettes/themes/xxxx/', // CFG
'langue' => 'Langue', // Non utilise
'langues' => 'Langues',
'legend_layout' => 'Pr&eacute;sentation', // CFG
'legend_theme' => 'Th&egrave;me', // CFG
'lien' => 'Lien', // Non utilise
'liens' => 'Liens',

//O
'oksearch' => 'ok',

//P
'permalink_pour' => 'Lien permanent vers',

//R
'resultat_recherche' => 'R&eacute;sultats pour votre recherche de',
'rss_pour' => 'fil RSS des commentaires de',

//S
'selection' => '&agrave; retenir',
'site_comment' => 'site',
'syndication' => 'Syndication',

//T
'texte_erreur1' => 'La recherche de',
'texte_erreur2' => 'ne donne aucun r&eacute;sultat.',
'titre_erreur' => 'Erreur :',
'trackback' => 'trackback', // Non utilise
'trackbacks' => 'trackbacks', // Non utilise
'trackbacks_pour_faire' => 'Pour faire un trackback sur ce billet', // Non utilise
'trackbacks_fermes' => 'Les trackbacks pour ce billet sont ferm&eacute;s.', // Non utilise

//U
'un_seul_mot' => 'un seul mot', // Oubli

);

?>
