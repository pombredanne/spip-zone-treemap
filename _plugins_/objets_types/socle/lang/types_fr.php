<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'choisir_type_article'	=> 'Choisir le type de cet article&nbsp;:',
	'choisir_type_rubrique' => 'Choisir le type de cette rubrique&nbsp;:',
	'normal_article'		=> 'Article standard',
	'normal_rubrique'		=> 'Rubrique ordinaire',
	'titre_typer_article'	=> 'TYPE DE L\'ARTICLE',
	'titre_typer_rubrique'	=> 'TYPE DE LA RUBRIQUE'
);

?>
