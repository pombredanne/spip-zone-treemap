<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2008                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return; // securiser
if (!function_exists('generer_url_article')) { // si la place n'est pas prise

####### modifications possibles dans ecrire/mes_options
# on peut indiquer '.html' pour faire joli
define ('_terminaison_urls_page', '');
# ci-dessous, ce qu'on veut ou presque (de preference pas de '/')
# attention toutefois seuls '' et '=' figurent dans les modes de compatibilite
define ('_separateur_urls_page', '');
# on peut indiquer '' si on a installe le .htaccess
define ('_debut_urls_page', get_spip_script('./').'?');
#######

function fabriquer_liste_types() {
	global $types;
	static $liste = '';
	static $correspondance = array();
	if($liste == '') {
		foreach($types as $objet => $type)
			if(is_array($type))
				foreach($type as $_type) {
					$liste .= '|'.$_type;
					$correspondance[$_type] = ($objet == 'secteur') ? 'rubrique' : $objet;
				}
			else {
				$liste .= '|'.$type;
				$correspondance[$type] = ($objet == 'secteur') ? 'rubrique' : $objet;
			}
	}
	return array($liste, $correspondance);
}

function recuperer_type($objet, $id) {
	include_spip('base/abstract_sql');
	$id_objet = id_table_objet($objet);
	$table_objet = table_objet_sql($objet);
	$type = sql_getfetsel(_TYPE, $table_objet, $id_objet.'='.intval($id));
	return $type ? $type : $objet;
}

// http://doc.spip.org/@composer_url_page
function composer_url_page($page,$id, $args='', $ancre='') {
	$url = _debut_urls_page . $page . _separateur_urls_page
	  . $id . _terminaison_urls_page;
	if ($args) $args = strpos($url,'?') ? "&$args" : "?$args";
	return $url . $args . ($ancre ? "#$ancre" : '');
}

// http://doc.spip.org/@generer_url_article
function generer_url_article($id_article, $args='', $ancre='') {
	return composer_url_page(recuperer_type('article', $id_article), $id_article, $args, $ancre);
}

// http://doc.spip.org/@generer_url_rubrique
function generer_url_rubrique($id_rubrique, $args='', $ancre='') {
	return composer_url_page(recuperer_type('rubrique', $id_rubrique), $id_rubrique, $args, $ancre);
}

// http://doc.spip.org/@generer_url_breve
function generer_url_breve($id_breve, $args='', $ancre='') {
	return composer_url_page('breve', $id_breve, $args, $ancre);
}

// http://doc.spip.org/@generer_url_mot
function generer_url_mot($id_mot, $args='', $ancre='') {
	return composer_url_page('mot', $id_mot, $args, $ancre);
}

// http://doc.spip.org/@generer_url_site
function generer_url_site($id_syndic, $args='', $ancre='') {
	return composer_url_page('site', $id_syndic, $args, $ancre);
}

// http://doc.spip.org/@generer_url_auteur
function generer_url_auteur($id_auteur, $args='', $ancre='') {
	return composer_url_page('auteur', $id_auteur, $args, $ancre);
}

// http://doc.spip.org/@generer_url_document
function generer_url_document($id_document, $args='', $ancre='') {
	include_spip('inc/documents');
	return generer_url_document_dist($id_document);
}

// retrouve le fond et les parametres d'une URL abregee
// http://doc.spip.org/@urls_page_dist
function urls_type_dist(&$fond, $url) {
	global $contexte;

	list($liste, $correspondance) = fabriquer_liste_types();

	// Ce bloc gere les urls page et la compatibilite avec les "urls standard"
	if ($fond=='sommaire'){
		if (preg_match(
		',^[^?]*[?/](article|rubrique|breve|mot|site|auteur'.$liste.')(?:\.php3?)?.*?([0-9]+),',
		$url, $regs)) {
			$fond = $regs[1];
			$id_objet = 'id_'.$correspondance[$fond];
			if ($regs[1] == 'site') {
				if (!isset($contexte['id_syndic']))
					$contexte['id_syndic'] = $regs[2];
			} else {
				if (!isset($contexte[$id_objet]))
					$contexte[$id_objet] = $regs[2];
			}
	
			return;
		}
		/* Compatibilite urls-page avec formulaire en get !!! */
		else if (preg_match(
			',[?/&](article|breve|rubrique|mot|auteur|site'.$liste.')[=]?([0-9]+),',
			$url, $regs)) {
			$fond = $regs[1];
			if ($regs[1] == 'site') {
				if (!isset($contexte['id_syndic']))
					$contexte['id_syndic'] = $regs[2];
			} else {
				if (!isset($contexte[$id_objet]))
					$contexte[$id_objet] = $regs[2];
			}
			return;
		}
		/* Fin compatibilite urls-page */
	}


	/*
	 * Le bloc qui suit sert a faciliter les transitions depuis
	 * le mode 'urls-propres' vers les modes 'urls-standard/page' et 'url-html'
	 * Il est inutile de le recopier si vous personnalisez vos URLs
	 * et votre .htaccess
	 */
	// Si on est revenu en mode page, mais c'est une ancienne url_propre
	// on ne redirige pas, on assume le nouveau contexte (si possible)
	if (
		 (isset($_SERVER['REDIRECT_url_propre']) AND $url_propre = $_SERVER['REDIRECT_url_propre'])
	OR (isset($_ENV['url_propre']) AND $url_propre = $_ENV['url_propre'])
	AND preg_match(',^(article|breve|rubrique|mot|auteur|site)$,', $fond)) {
	  $url_propre = (preg_replace('/^[_+-]{0,2}(.*?)[_+-]{0,2}(\.html)?$/',
			'$1', $url_propre));
		$r = sql_fetsel("id_objet", "spip_urls", "url=" . _q($url_propre));
		if ($r)	$contexte[id_table_objet($fond)] = $r['id_objet'];
	}

	/* Fin du bloc compatibilite url-propres */
}

//
// URLs des forums
//

// http://doc.spip.org/@generer_url_forum
function generer_url_forum($id_forum, $args='', $ancre='') {
	include_spip('inc/forum');
	return generer_url_forum_dist($id_forum, $args, $ancre);
}
 }
?>
