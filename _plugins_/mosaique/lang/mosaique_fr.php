<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/saisies/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'mosaique_titre' => 'Mosa&iuml;que',
	'mosaique_new_article' => 'Il est n&eacute;cessaire d\'enregistrer l\'article une premi&egrave;re fois pour pouvoir utiliser la mosa&iuml;que.',
	'mosaique_popup_txt' => 'Triez les vignettes en les faisant glisser avec votre souris.',
	'mosaique_aucun_doc' => 'D&eacute;posez au moins deux images dans le portfolio pour pouvoir utiliser la mosa&iuml;que.',
	'mosaique_trier_docs' => 'Trier les images du portfolio',
	'mosaique_notice' => 'L\'ordre des images de la mosa&iuml;que a chang&eacute;. Il est n&eacute;cessaire d\'enregistrer l\'article pour conserver ces modifications.',
	'mosaique_annuler' => 'Annuler',
	'mosaique_ok' => 'OK',
	'mosaique_enregistrer' => 'Enregistrer l\'ordre'
);
?>