<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/saisies/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'mosaique_slogan' => 'Tri intuitif du portfolio.',
	'mosaique_description' => 'Ce plugin propose de trier les documents associ&eacute;s aux articles (via la page d\'&eacute;dition de l\'article) par simple glisser-d&eacute;poser des vignettes d\'une mosaique.'
);
?>