<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;
$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
'class_css_par_defaut' => 'Classe css par défaut',
// D
'dimension_maxi' => 'Hauteur ou largeur maximale des images',

// E
'enregistrer' => 'Enregistrer',
'explications' => 'Dimensions maximales des images et des vignettes en pixels sans unités (ex : 200). Ceci n\'est pris en compte que si ces tailles ne sont pas précisées dans le modèle d\'inclusion',

// S
'supprimer_valeurs' => 'Réinitialiser',

// T
'taille_vignettes' => 'Taille des vignettes',
);
?>