<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'afficher_tickets' => 'Show bug reports',
	'assignation_attribuee_a' => 'The bug report has been assigned to @nom@.',
	'assignation_mail_titre' => 'Change of bug report assignment',
	'assignation_modifiee' => 'Assignment Update',
	'assignation_non_modifiee' => 'Assignment not updated',
	'assignation_supprimee' => 'The assignment of this bug report has ben deleted.',
	'assigne_a' => 'Assigned to:',
	'assignes_a' => 'Assigned to',
	'autres_tickets_ouverts' => 'Other opened bug reports',

	// C
	'cfg_bouton_radio_desactiver_forum_public' => "Disable the use of public forums.",
	'cfg_form_tickets_autorisations' => 'Setting up permissions',
	'cfg_form_tickets_general' => 'General setup',
	'cfg_descr_tickets' => 'Configuration of the Bug reports plugin for SPIP',
	'cfg_explication_cacher_moteurs' => 'Hide the bug reports from the search engines asking them to avoid their indexation when they are shown in public space.',
	'cfg_explication_composants' => 'Separate the components with the character ":".',
	'cfg_explication_jalons' => 'Separate the milestones with the character ":".',
	'cfg_explication_projets' => 'Separate the projects with the character ":".',
	'cfg_explication_readonly' => 'This part of the configuration is already set somewhere else.',
	'cfg_explication_versions' => 'Separate the versions with the character ":".',
	'cfg_inf_type_autorisation' => 'If you choose by status or by author, you will be prompted below your selection of status or authors.',
	'cfg_lbl_autorisation_auteurs' => '	Allow list of authors',
	'cfg_lbl_autorisation_statuts' => 'Allow by authors status',
	'cfg_lbl_autorisation_webmestre' => 'Allow webmasters only',
	'cfg_lbl_cacher_moteurs' => 'Hide from search engines',
	'cfg_lbl_case_joindre_fichiers' => 'Allow to add one or several files to the bug reports',
	'cfg_lbl_case_notification_publique' => 'Be notified in public rather than private space (Requires Zpip)',
	'cfg_lbl_case_selecteur_navigateur' => 'Having a browser selector in the bug report form',
	'cfg_lbl_composants' => 'Components',
	'cfg_lbl_desactiver_public' => 'Disable public access',
	'cfg_lbl_forums_publics' => 'Comments on bug reports',
	'cfg_lbl_jalons' => 'Milestones',
	'cfg_lbl_joindre_fichiers' => 'Add one or several files',
	'cfg_lbl_liste_auteurs' => 'Authors of the website',
	'cfg_lbl_notification_publique' => 'Public notification',
	'cfg_lbl_projets' => 'Projects',
	'cfg_lbl_selecteur_navigateur' => 'Browser selector',
	'cfg_lbl_statuts_auteurs' => 'Possible status',
	'cfg_lbl_type_autorisation' => 'Authorization method',
	'cfg_lbl_versions' => 'Versions',
	'cfg_lgd_autorisation_assigner' => 'Assign bug reports',
	'cfg_lgd_autorisation_commenter' => 'Comment bug reports',
	'cfg_lgd_autorisation_ecrire' => 'Write bug reports',
	'cfg_lgd_autorisation_modifier' => 'Modify bug reports',
	'cfg_lgd_champs_optionnels' => 'Optional fields',
	'cfg_lgd_champs_options_autres' => 'Other options',
	'cfg_lgd_notifs_forums' => 'Forums and notifications',
	'cfg_titre_tickets' => 'Bug reports',
	'champ_assigner' => 'Assign to:',
	'champ_composant' => 'Component:',
	'champ_createur' => 'Created by:',
	'champ_date' => 'Date:',
	'champ_date_debut' => 'From:',
	'champ_date_fin' => 'To:',
	'champ_date_modif' => 'Modified on',
	'champ_description' => 'Description of the bug report',
	'champ_exemple' => 'Example:',
	'champ_fichier' => 'Add a file',
	'champ_id' => 'Number',
	'champ_id_auteur' => 'Author:',
	'champ_id_assigne' => 'Assigned to:',
	'champ_importance' => 'Severity:',
	'champ_jalon' => 'Milestone:',
	'champ_maj' => 'Update:',
	'champ_maj_long' => 'Updated date',
	'champ_navigateur' => 'Browser:',
	'champ_nouveau_commentaire' => 'New comment',
	'champ_projet' => 'Project:',
	'champ_recherche' => 'Search:',
	'champ_severite' => 'Severity:',
	'champ_statut' => 'Status:',
	'champ_sticked' => 'Sticked',
	'champ_texte' => 'Text',
	'champ_titre' => 'Abstract',
	'champ_titre_ticket' => 'Title of the bug report',
	'champ_type' => 'Type:',
	'champ_url_exemple' => 'Example URL',
	'champ_version' => 'Version:',
	'changement_statut_mail' => 'The status of this bug report has been changed from "@ancien@" to "@nouveau@".',
	'classement_assigne' => 'Bug reports by assignment',
	'classement_asuivre' => 'Your bug reports to follow',
	'classement_jalon' => 'Bug reports by milestone',
	'classement_termine' => 'Closed bug reports',
	'classement_type' => 'Bug reports by type',
	'commentaire' => 'comment',
	'commentaires' => 'comments',
	'commentaire_aucun' => 'No comment',
	'commenter_ticket' => 'Comment this bug report',
	'creer_ticket' => 'Create a bug report',

	// D
	'date_creation_auteur' => 'Bug report created on <strong>@date@</strong> by <strong>@nom@</strong>',

	// E
	'erreur_texte_longueur_mini' => 'The minimum length of text is @nb@ characters.',
	'erreur_verifier_formulaire' => 'Verify your form',
	'explication_champ_sticked' => 'Sticked bug reports are allways shown first, not depending on their status.',
	'explication_description_ticket' => 'Describe as precisely as possible the need or problem.
	Indicate in particular if it occurs consistently or occasionally.
	If it is a display problem, specify with what browser you are experiencing it.',
	'explication_description_ticket_ss_nav' => 'Describe as precisely as possible the need or problem.
	Indicate in particular if it occurs consistently or occasionally.',
	'explication_fichier' => 'Add a file to your bug report.',
	'explication_redaction' => 'When you\'re finished writing your bug report, select the status &laquo;open and discussed&raquo;.',
	'explication_url_exemple' => 'Enter here the URL of a page covered by this bug report.',

	// F
	'forum_message' => 'Your message',
	'forum_sans_previsu' => 'Warning: pas de pr&eacute;visualisation&nbsp;; votre message est publi&eacute; imm&eacute;diatement.',

	// I
	'info_commentaire' => 'Comment #@id@ :',
	'info_document_ajoute' => 'Added:',
	'info_liste_tickets' => 'Bug reports',
	'info_numero_ticket' => 'BUG REPORT NUMBER:',
	'info_tickets_ouvert' => 'opened and discussed',
	'info_tickets_redac' => 'being edited',
	'info_tickets' => 'Bug reports',
	'icone_modifier_ticket' => 'Modify this bug report',
	'icone_retour_ticket' => 'Back to the bug report',

	// L
	'label_paginer_par' => 'Paginate by&nbsp;:',
	'lien_filtrer' => 'Filter bug reports',
	'lien_supprimer_filtres' => 'Delete all filters',
	
	// M
	'mail_texte_message_auto' => 'This is an automatic message: please don\'t answer.',
	'message_aucun_ticket_recherche' => 'No bug report match your criteria',
	'message_automatique' => 'This is an automatic message: please don\'t answer.',
	'message_le' => 'on @date@',
	'message_poste_par' => 'Message posted by',
	'message_publie' => 'Your message has been published',

	// N
	'no_assignation' => 'No one',
	'non_assignes' => 'Not owned',
	'nouveau_commentaire_mail' => 'New comment on bug report',
	'nouveau_ticket' => 'New bug report',

	// O
	'option_navigateur_autre' => 'Other',
	'option_navigateur_tous' => 'All browsers',
	
	// P
	'page_titre' => 'Bug reports, bug tracking system',

	// R
	'revenir_gestion' => 'Back to the bug reports management',

	// S
	'sinscrire' => "Subscribe",
	'severite_bloquant' => 'Critical',
	'severite_important' => 'Major',
	'severite_normal' => 'Normal',
	'severite_peu_important' => 'Trivial',
	'statut_mis_a_jour' => 'Status updated',
	'statut_ferme' => 'Closed',
	'statut_ferme_long' => 'All the closed bug reports',
	'statut_inchange' => 'The status has been modified.',
	'statut_ouvert' => 'Opened and discussed',
	'statut_poubelle' => 'In the dustbin',
	'statut_redac' => 'Editing in progress',
	'statut_resolu' => 'Resolved',
	'statut_resolu_long' => 'All the resolved bug reports',
	'suivre_tickets_assignes_a' => 'Bug reports assigned to @nom@',
	'suivre_tickets_comments' => 'Tracking of the bug report comments',
	'suivre_tickets_comments_rss' => 'Follow these comments by RSS',
	'suivre_tickets_de' => 'Bug reports of @nom@',
	'suivre_tickets_id' => 'Tracking of the bug report #@id@ : @titre@',
	'suivre_tickets_statut' => 'Bug reports having the statut(s):',
	'suivre_tickets_rss' => 'Follow these bug reports by RSS',
	'suivre_tickets_rss_unique' => 'Follow this bug report by RSS',
	'suivre_tickets_tous' => 'All bug reports',
	'syndiquer_ticket' => 'Syndicate the bug report:',
	'syndiquer_tickets' => 'Syndicate the bug reports of the website',

	// T
	'ticket' => 'Bug report',
	'ticket_enregistre' => 'The bug report has been saved.',
	'tickets' => 'Bug reports',
	'tickets_autorisations' => 'Authorizations',
	'tickets_derniers_commentaires' => 'Last comments',
	'tickets_general' => 'General',
	'tickets_sticked' => 'Sticked bug reports',
	'tickets_sur_inscription' => "Bug report or comment only available for registered users.",
	'tickets_sur_inscription_droits' => 'The rights you have are inadequate.',
	'tickets_traites' => 'All processed bug reports',
	'tickets_tries' => 'Bug reports matching your criteria',
	'titre' => 'Bug reports, bug tracking',
	'titre_identification' => 'Authentication',
	'titre_liste' => 'List of the bug reports',
	'tous_tickets_ouverts' => 'All opened bug reports',
	'type_amelioration' => 'Improvements',
	'type_amelioration_long' => 'The improvements bug reports',
	'type_probleme' => 'Problem',
	'type_probleme_long' => 'Problems to resolve',
	'type_tache' => 'Task',
	'type_tache_long' => 'Current tasks',

	// V
	'vos_tickets_assignes' => 'Bug reports assigned to you',
	'vos_tickets_assignes_auteur' => 'Bug reports from @nom@ assigned to you',
	'vos_tickets_en_cours' => 'Your bug reports being editing'
);


?>
