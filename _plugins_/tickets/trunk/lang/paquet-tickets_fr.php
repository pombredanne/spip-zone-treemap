<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-tickets
// Langue: fr
// Date: 12-01-2012 14:00:29
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// T
	'tickets_description' => '{Tickets} est un outil de suivi de bugs directement intégré à l\'espace privé de SPIP. Il est conçu pour faciliter la phase de développement du site (notamment les squelettes du site public).',
	'tickets_nom' => 'Tickets',
	'tickets_slogan' => 'Système de suivi de bugs',
);
?>