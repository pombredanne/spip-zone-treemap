<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-tickets
// Langue: de
// Date: 12-01-2012 14:00:29
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// T
	'tickets_description' => '{Tickets} ist ein Werkzeug zur Fehler-Nachverfolgung (bugtracker) das im SPIP Redaktionssystem zugänglich ist. Es wurde insbesondere entwickelt, um die Zusammenarbeit bei der Gestaltung des üffentlichen Sitelayouts zu unterstützen.',
	'tickets_nom' => 'Tickets',
	'tickets_slogan' => 'System zur Fehler-Nachverfolgung',
);
?>