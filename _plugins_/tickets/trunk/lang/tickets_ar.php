<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tickets?lang_cible=ar
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'afficher_tickets' => 'عرض التذاكر',
	'assignation_attribuee_a' => 'تم تكليف @nom@ بالتذكرة.',
	'assignation_mail_titre' => 'تغيير التكليف بالتذكرة',
	'assignation_modifiee' => 'Assignation mise à jour', # NEW
	'assignation_non_modifiee' => 'Assignation non mise à jour', # NEW
	'assignation_supprimee' => 'L\'assignation de ce ticket a été supprimée.', # NEW
	'assigne_a' => 'المكلَف:',
	'assignes_a' => 'المكلَف',
	'autres_tickets_ouverts' => 'التذاكر المفتوحة الأخرى',

	// C
	'cfg_bouton_radio_desactiver_forum_public' => 'إبطال استخدام المنتديات العامة.',
	'cfg_bouton_tickets' => 'التذاكر',
	'cfg_descr_autorisations' => 'Cette page de configuration permet de paramétrer les autorisations pour l\'écriture, la modification, l\'assignation et le commentaire des tickets.', # NEW
	'cfg_descr_general' => 'Cette page de configuration permet de choisir les valeurs des champs optionnels, la modération des commentaires ainsi que divers autres paramètres.', # NEW
	'cfg_explication_cacher_moteurs' => 'Cache les tickets des moteurs de recherche en leur demandant de ne pas indexer leur contenus lorsqu\'ils sont affichés dans l\'espace public.', # NEW
	'cfg_explication_composants' => 'Séparez les composants par le caractère ":".', # NEW
	'cfg_explication_formats_documents_ticket' => 'Séparez les formats par une virgule', # NEW
	'cfg_explication_jalons' => 'Séparez les jalons par le caractère ":".', # NEW
	'cfg_explication_projets' => 'فصل المشاريع برمز ":".',
	'cfg_explication_readonly' => 'Cette partie de la configuration est déjà définie autre part.', # NEW
	'cfg_explication_versions' => 'Séparez les versions par le caractère ":".', # NEW
	'cfg_form_tickets_autorisations' => 'الأذون',
	'cfg_form_tickets_general' => 'Paramètres généraux', # NEW
	'cfg_inf_type_autorisation' => 'Si vous choisissez par statut ou par auteur, il vous sera demandé ci-dessous votre sélection de statuts ou d\'auteurs.', # NEW
	'cfg_lbl_autorisation_auteurs' => 'السماح حسب لائحة المؤلفين',
	'cfg_lbl_autorisation_statuts' => 'الماح حسب وضعية المؤلفين',
	'cfg_lbl_autorisation_webmestre' => 'السماح للمشرفين على الموقع فقط',
	'cfg_lbl_cacher_moteurs' => 'إخفاء محركات بحث',
	'cfg_lbl_case_joindre_fichiers' => 'Autoriser à joindre un ou plusieurs fichiers aux tickets', # NEW
	'cfg_lbl_case_notification_publique' => 'Être notifié dans l\'espace public plutôt que dans l\'espace privé', # NEW
	'cfg_lbl_case_selecteur_navigateur' => 'Disposer d\'un sélecteur de navigateur dans le formulaire d\'édition de ticket', # NEW
	'cfg_lbl_composants' => 'المكونات',
	'cfg_lbl_desactiver_public' => 'Désactiver l\'accès public', # NEW
	'cfg_lbl_formats_documents_ticket' => 'Formats de documents acceptés', # NEW
	'cfg_lbl_forums_publics' => 'التعليقات على التذاكر',
	'cfg_lbl_jalons' => 'العلامات',
	'cfg_lbl_joindre_fichiers' => 'ربط ملف او أكثر',
	'cfg_lbl_liste_auteurs' => 'مؤلفو الموقع',
	'cfg_lbl_notification_publique' => 'إخطار  عام',
	'cfg_lbl_projets' => 'المشاريع',
	'cfg_lbl_selecteur_navigateur' => 'محدد برامج التصفح',
	'cfg_lbl_statuts_auteurs' => 'الوضعيات الممكنة',
	'cfg_lbl_type_autorisation' => 'أسلوب الأذون',
	'cfg_lbl_versions' => 'الإصدارات',
	'cfg_lgd_autorisation_assigner' => 'التكليف بالتذاكر',
	'cfg_lgd_autorisation_commenter' => 'التعليق على التذاكر',
	'cfg_lgd_autorisation_ecrire' => 'كتابة التذاكر',
	'cfg_lgd_autorisation_modifier' => 'تعديل التذاكر',
	'cfg_lgd_champs_optionnels' => 'حقول اختيارية',
	'cfg_lgd_champs_options_autres' => 'الخيارات الأخرى',
	'cfg_lgd_notifs_forums' => 'المنتديات والإخطارات',
	'cfg_titre_tickets' => 'التذاكر - إعداد الملحق',
	'champ_assigner' => 'تكليف:',
	'champ_composant' => 'المكوِن:',
	'champ_createur' => 'فتحها:',
	'champ_date' => 'التاريخ:',
	'champ_date_debut' => 'من:',
	'champ_date_fin' => 'إلى:',
	'champ_date_modif' => 'تم التعديل في',
	'champ_description' => 'وصف التذكرة',
	'champ_exemple' => 'مثال:',
	'champ_fichier' => 'ربط ملف',
	'champ_id' => 'الرقم',
	'champ_id_assigne' => 'المكلف:',
	'champ_id_auteur' => 'المؤلف:',
	'champ_importance' => 'الأهمية',
	'champ_jalon' => 'علامة', # MODIF
	'champ_maj' => 'تحديث:',
	'champ_maj_long' => 'تاريخ التحديث',
	'champ_navigateur' => 'المتصفح:',
	'champ_nouveau_commentaire' => 'تعليق جديد',
	'champ_projet' => 'المشروع:',
	'champ_recherche' => 'بحث:',
	'champ_severite' => 'الخطورة:',
	'champ_statut' => 'الوضعية:',
	'champ_sticked' => 'Épinglé :', # NEW
	'champ_texte' => 'النص',
	'champ_titre' => 'ملخص',
	'champ_titre_ticket' => 'موضوع التذكرة',
	'champ_type' => 'النوع:',
	'champ_url_exemple' => 'عنوان URL للمثال',
	'champ_version' => 'إصدار:',
	'changement_statut_mail' => 'Le statut de ce ticket a été modifié de "@ancien@" à "@nouveau@".', # NEW
	'classement_assigne' => 'التذاكر حسب التكليف',
	'classement_asuivre' => 'التذاكر قيد المتابعة',
	'classement_composant' => 'Tickets par composant', # NEW
	'classement_jalon' => 'التذاكر حسب العلامة',
	'classement_projet' => 'Tickets par projet', # NEW
	'classement_termine' => 'التذاكر المنتهية',
	'classement_type' => 'التذاكر حسب النوع',
	'classement_version' => 'Tickets par version', # NEW
	'commentaire' => 'تعليق',
	'commentaire_aucun' => 'بدون تعليق',
	'commentaires' => 'التعليقات',
	'commenter_ticket' => 'التعليق على هذه التذكرة',
	'creer_ticket' => 'فتح تذكرة',

	// D
	'date_creation_auteur' => 'Ticket créé le <strong>@date@</strong> par <strong>@nom@</strong>', # NEW

	// E
	'erreur_date_saisie' => 'هذا التاريخ غير صالح',
	'erreur_date_saisie_superieure' => 'La date maximale doit être supérieure à la date minimale', # NEW
	'erreur_texte_longueur_mini' => 'La longueur minimale du texte est de @nb@ caractères.', # NEW
	'erreur_verifier_formulaire' => 'الرجاء التدقيق في الاستمارة',
	'explication_champ_sticked' => 'Les tickets épinglés sont toujours affichés en premier, quelque soit leur statut.', # NEW
	'explication_description_ticket' => 'Décrivez aussi précisément que possible le besoin ou le problème rencontré.
	Indiquez en particulier s’il se produit systématiquement ou occasionnellement.
	S’il s’agit d’un problème d\'affichage, précisez avec quel navigateur vous le rencontrez.', # NEW
	'explication_description_ticket_ss_nav' => 'Décrivez aussi précisément que possible le besoin ou le problème rencontré.
	Indiquez en particulier s’il se produit systématiquement ou occasionnellement.', # NEW
	'explication_fichier' => 'إضافة ملف الى تذكرتك',
	'explication_redaction' => 'Quand vous avez terminé la rédaction de votre ticket, sélectionnez le statut «ouvert et discuté».', # NEW
	'explication_url_exemple' => 'Indiquez ici l’URL d’une page concernée par ce ticket.', # NEW

	// I
	'icone_modifier_ticket' => 'تعديل هذه التذكرة',
	'icone_retour_ticket' => 'العودة الى التذكرة',
	'info_commentaire' => 'Commentaire #@id@ :', # NEW
	'info_document_ajoute' => 'أضيف:',
	'info_liste_tickets' => 'التذاكر',
	'info_numero_ticket' => 'تذكرة رقم:',
	'info_ticket_1' => 'تذكرة واحدة',
	'info_ticket_aucun' => 'لا توجد تذاكر',
	'info_ticket_nb' => '@nb@ تذكرة',
	'info_tickets' => 'التذاكر',
	'info_tickets_ouvert' => 'مفتوحة ومناقشة',
	'info_tickets_redac' => 'قيد التحرير',

	// L
	'label_paginer_par' => 'تصفح حسب:',
	'lien_filtrer' => 'ترشيح التذاكر',
	'lien_supprimer_filtres' => 'حذف كل المرشحات',

	// M
	'mail_texte_message_auto' => 'هذه رسالة آلية: الرجاء عدم الإجابة عليها.',
	'message_aucun_ticket_recherche' => 'لا يوجد اي تذكرة تناسب البحث',
	'message_automatique' => 'Ceci est un message automatique : n\'y repondez pas.', # NEW
	'message_page_publique_indisponible' => 'Cette page est indisponible. vérifiez que ZPIP est activé et que votre configuration du plugin Tickets autorise l\'accès public.', # NEW
	'message_zpip_inactif' => 'Cette option est désactivée car elle nécessite le plugin ZPIP.', # NEW

	// N
	'no_assignation' => 'لا أحد',
	'non_assignes' => 'غير مكلف بها أحد',
	'nouveau_commentaire_mail' => 'تعليق جديد على التذكرة',
	'nouveau_ticket' => 'تذكرة جديدة',

	// O
	'option_navigateur_autre' => 'Autre', # NEW
	'option_navigateur_tous' => 'كل برامج التصفح',

	// P
	'page_titre' => 'Tickets, système de suivi de bugs', # NEW

	// R
	'revenir_gestion' => 'Revenir à la gestion des tickets', # NEW

	// S
	'severite_bloquant' => 'معطِل',
	'severite_important' => 'مهم',
	'severite_normal' => 'عادي',
	'severite_peu_important' => 'قليل الأهمية',
	'sinscrire' => 'تسجيل',
	'statut_ferme' => 'مقفلة',
	'statut_ferme_long' => 'كل التذاكر المقفلة',
	'statut_inchange' => 'Le statut n\'a pas été modifié.', # NEW
	'statut_mis_a_jour' => 'Statut mis à jour', # NEW
	'statut_ouvert' => 'مفتوحة ومناقشة',
	'statut_poubelle' => 'الى المهملات',
	'statut_redac' => 'En cours de rédaction', # NEW
	'statut_resolu' => 'تم الحل',
	'statut_resolu_long' => 'كل التذاكر المحلولة',
	'suivre_tickets_assignes_a' => 'Tickets assignés à @nom@', # NEW
	'suivre_tickets_comments' => 'Suivi des commentaires de tickets', # NEW
	'suivre_tickets_comments_rss' => 'Suivre ces commentaires par RSS', # NEW
	'suivre_tickets_de' => 'Les tickets de @nom@', # NEW
	'suivre_tickets_id' => 'Suivi du ticket #@id@ : @titre@', # NEW
	'suivre_tickets_rss' => 'متابعة هذه التذاكر من خلال RSS',
	'suivre_tickets_rss_unique' => 'متابعة هذه التذكرة من خلال RSS',
	'suivre_tickets_statut' => 'Les tickets ayant le ou les statuts :', # NEW
	'suivre_tickets_tous' => 'كل التذاكر',
	'syndiquer_ticket' => 'ترخيص التذكرة:',
	'syndiquer_tickets' => 'ترخيص تذاكر الموقع',

	// T
	'texte_ticket_statut' => 'وضعية التذكرة:',
	'ticket' => 'التذكرة',
	'ticket_enregistre' => 'حفظت التذكرة ',
	'tickets' => 'التذاكر',
	'tickets_autorisations' => 'الأذون',
	'tickets_derniers_commentaires' => 'أحدث التعليقات',
	'tickets_en_cours_auteur' => 'Les tickets de @nom@ en cours de traitement', # NEW
	'tickets_general' => 'Les paramètres généraux', # NEW
	'tickets_sticked' => 'التذاكر اللاصقة',
	'tickets_sur_inscription' => 'L\'écriture des tickets ou commentaires n\'est possible qu\'aux personnes identifiées.', # NEW
	'tickets_sur_inscription_droits' => 'Les droits dont vous disposez sont insuffisants.', # NEW
	'tickets_traites' => 'كل التذاكر المعالجة',
	'tickets_tries' => 'Tickets correspondant à vos critères', # NEW
	'titre' => 'Tickets, suivi de bugs', # NEW
	'titre_identification' => 'العريف',
	'titre_liste' => 'لائحة التذاكر',
	'tous_tickets_en_redaction' => 'Tous les tickets en rédaction', # NEW
	'tous_tickets_ouverts' => 'كل التذاكر المفتوحة',
	'tous_vos_tickets' => 'كل تذاكرك',
	'type_amelioration' => 'تحسين',
	'type_amelioration_long' => 'Les tickets demandant une amélioration', # NEW
	'type_probleme' => 'المشكلة',
	'type_probleme_long' => 'المشاكل المتطلبة حلول',
	'type_tache' => 'المهمة',
	'type_tache_long' => 'المهمات التي تنتظر الإنجاز',

	// V
	'vos_tickets_assignes' => 'Les tickets qui vous sont assignés', # NEW
	'vos_tickets_assignes_auteur' => 'Les tickets de @nom@ qui vous sont assignés', # NEW
	'vos_tickets_en_cours' => 'تذاكرك قيد التحرير'
);

?>
