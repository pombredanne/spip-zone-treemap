<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-tickets
// Langue: en
// Date: 12-01-2012 14:00:29
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// T
	'tickets_description' => '{Bug reports} is a bug tracking tool integrated into SPIP. It is designed to facilitate the development phase of the site.',
	'tickets_nom' => 'Bug reports',
	'tickets_slogan' => 'Bug tracking system',
);
?>