<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(
//A
	'abonnement' => 'Abonnement',	
	'abonnements' => 'Abonnements',
	'abonnement_archives'=>'Abonnement aux archives, commence aujourd\'hui moins le nombre de jours',
	'abonnement_debut'=>'Début',
	'abonnements_echus' => 'Abonnements échus',
	'abonnement_fin'=>'échéance',
	'abonnement_relances' => 'Relance des abonnements',

	'abonner_article'=>'Abonner à un article',
	'abonner_rubrique'=>'Abonner à une rubrique',
	'abonne' => 'Abonné',
	'abonnes' => 'Abonnés',
	'abonnes_article'=>'Les abonnés à cet article',
	'abonnes_rubrique'=>'Les abonnés à cette rubrique',
	'abonner_ou_acheter_article' => 'Vous pouvez vous abonner, ou bien acheter cet article à l\'unité.',
	'abonner_ou_acheter_rubrique' => 'Vous pouvez vous abonner, ou bien acheter cette rubrique à l\'unité.',
	'abo_valide_jusqu_au'=>'Votre abonnement est valable jusqu\'au @date@',

	'acces_complet' => 'Votre abonnement permet un accès complet à cette page.',
	'acces_label'=>'Accès',
	'acces_internet_explication'=>'Faut-il accorder un accès internet personnel le temps que dure l\'abonnement?',
	'acces_internet_nozone' =>'Accorder un accès',
	'acces_internet_explication_non'=>'Non, aucun accès internet (c\'est un abonnement papier).',
	'acces_internet_explication_oui'=>'Oui, accès aux rubriques des dates de l\'abonnement.',
	'acces_internet_nb_rub'=>'Nombre de rubriques accessibles',
	'acces_internet_nb_rub_explication'=>'Si vous n\'êtes pas certain des dates de publication, précisez le nombre de rubriques accessibles pour cet abonnement.',
	'acces_internet_ids_zone'=>'Accès à une zone restreinte',
	'acces_internet_ids_zone_explication'=>'Accès à une zone, quelque soit la date des rubriques. <br />Séparer les identifiants des zones à ouvrir par une virgule.',

	'acces_refuse' => 'Votre abonnement ne permet pas un accès complet à cette page.',
	'action_creer_offre_abo' => 'Créer une offre d\'abonnement',
	'action_editer' => 'éditer',
	'action_supprimer' => 'Supprimer',
	'aucune_commande'=>'Non',

	'aucun_acces'=>'Aucune rubrique ne correspond!',
	'acces_ouvert'=>'Vous pouvez accéder à :',

	'article_abonnement'=>'Article',
	'articles' => 'Articles',
	'aucune_liaison_commande'=>'pas de commande liée',
	'aucune_zone'=>'Aucune',
	
	
//B
	'bouton_ajouter' => 'Ajouter',
	'bouton_ajouter_contactabonnement' => 'Ajouter un abonnement',
	'bouton_ajouter_contactabonnement_rubrique' => 'Ajouter une rubrique',
	'bouton_ajouter_contactabonnement_article' => 'Ajouter un article',
	'bouton_chercher' => 'Chercher',
	'bouton_commander' => 'Commander',//not use
	
//C
	'confirmer_supprimer_element'=>'Attention, vous avez demandé à supprimer un élément. Souhaitez-vous continuer?',
	'connexion_deja_abonne' => 'Si vous êtes déjà abonné, vous pouvez vous connecter à l\'aide du formulaire ci-dessous',
	'configuration_abonnements' => 'Paramétrage',
	'configuration_description_abonnement' => 'Cette page permet de définir les options d\'abonnement de votre site.',
	'configuration_description_abonnement_relances' => 'Cette page permet de définir les options de relances d\'abonnement de votre site.',
	'choisir_son_abonnement' => 'Choisir son abonnement',//not use
	'creer_abonnement' => 'Créer un abonnement',

//D
	'date_utile'=>'Date de publication',
	'date_abonnement'=>'Date',
	'donnees_correspondances' => 'Mes données de correspondance',
	'delier'=>'Délier',
	'depuis_le'=>'Depuis le @date@',
	'diffusion_payee' => 'Nb.',//not use

	
//E
	'echeance_le' => 'échéance le @date@',
	'echeance' => 'Échéance',
	'echeance_abonnement_proche' => 'Votre abonnement <i>@titre@</i> arrive à échéance le @date@.',
	'echeance_abonnement_passee' => 'Votre abonnement <i>@titre@</i> est arrivé à échéance le @date@.',
	'editer_abonnement' => 'Éditer un abonnement',
	'editer_offre_abonnement'=> 'Éditer une offre d\'abonnement',
	'editer_contacts_abonnement'=> 'éditer un abonnement',
	'enregistrement_effectue' => 'Enregistrement effectué',
	'erreurs_formulaire' => 'Des erreurs sont présentes dans votre saisie.',
	'erreur_identification' => 'Vous devez être identifié pour pouvoir vous abonner',
	'erreur_presente' => 'Une erreur est présente dans votre saisie',
	'erreur_selection_abonnement' => 'Vous devez choisir un abonnement !',
	'explication_environnement' => 'Vous pouvez utiliser des fichiers de tests
		pour vérifier que les abonnements fonctionnent correctement.
		Une fois en production,	ces fichiers de tests seront inaccessibles
		pour des raisons évidente de sécurité.',
	'explication_tarif' => 'Indiquer le prix en euros.',
	'explication_proposer_paiement' => 'Proposer un formulaire de paiement
		dès la validation du formulaire d\'inscription ?',
//F
	// Formulaires CFG
	'fieldset_securite' => 'Securités',
	'fieldset_inscription' => 'Relations avec Inscription 2',
	'fieldset_expediteur' => 'Expéditeur des mails de confirmation',
	'fieldset_confirmation' => 'Mail de confirmation d\'abonnement',
	'fieldset_echec' => 'Mail d\'échec de l\'abonnement',
	'fieldset_message_confirmation' => 'Message affiché sur la page de retour de la banque',
	'fieldset_frais_de_port' => 'Participation aux frais de port',
	'fieldset_articles_payants' => 'Articles payants',
	'fieldset_relance' => 'Email de relance a échéance',
	'fieldset_relance_avant_1' => 'Email de relance avant échéance 1',
	'fieldset_relance_avant_2' => 'Email de relance avant échéance 2',
	'fieldset_relance_apres' => 'Email de relance après échéance',
//I
	'info_acces_a_rubrique'=>'Vous avez accès aux rubriques suivantes :',
	'info_acces_a_article'=>'Vous avez accès aux articles suivants :',
	'info_acces_a_abonnement'=>'Vos abonnements :',
	'info_gauche_numero_contacts_abonnement'=>'N° de l\'abonnement',
	'info_gauche_numero_abonnement'=>'N° de l\'offre d\'abonnement',
	'info_contacts_abonne'=>'Auteur abonné',
	'info_contacts_objet'=>'Objet de l\'abonnement',
	'info_contacts_commande_liee'=>'Commande liée',
	'info_tous_abonnements'=>'Tous les abonnements',
	'info_vos_abonnements'=>'Vos abonnements',
	'info_profiter_de' => 'Profitez pleinement de @titre@ en vous abonnant.',


//J
	'je_renouvelle'=>'Je renouvelle',
	'je_me_reabonne'=>'Je me réabonne',
	'je_m_abonne'=>'Je m\'abonne',
	'jusqu_au'=>'jusqu\'au @date@',
//L
	'label_archives'=>'Entrer un chiffre pour le nombre de jours à soustraire',
	'label_environnement' => 'Environnement de travail',
	'label_environnement_test' => 'Environnement de test',
	'label_environnement_prod' => 'Environnement de production',
	'label_proposer_paiement' => 'Proposer le paiement à l\'inscription',
	'label_action' => 'Action',
	'label_titre' => 'Titre',
	'label_duree' => 'Durée',
	'label_periode' => 'Période',
	'label_nom' => 'Nom',
	'label_prenom' => 'Prénom',
	'label_ville' => 'Ville',
	'label_adresse' => 'Adresse',
	'label_adresses' => 'Adresse(s)',
	'label_cp' => 'Code postal',
	'label_date_creation' => 'Date de création',
	'label_date_expiration' => 'Date d\'expiration',
	'label_pays' => 'Pays',
	'label_email' => 'Email',
	'label_sujet' => 'Sujet',
	'label_texte' => 'Texte',
	'label_jour' => 'jours',
	'label_mois' => 'mois',
	'label_descriptif' => 'Descriptif',
	'label_tarif' => 'Tarif',
	'label_prix' => 'Prix',
	'label_frais_port_europe' => 'Prix des frais de port pour l\'europe',
	'label_frais_port_monde' => 'Prix des frais de port pour le reste du monde',
	'label_zones_ouvrir'=>'Zones',
	'liaison_commande'=>'Attention commande liée: ',
	'liste_des_abonnements' => 'Liste des offres d\'abonnements',
	'liste_vos_abonnements'=>'Liste de vos abonnements',
	'liste_des_offres'=>'Liste des offres d\'abonnements',
	'les_abonnements'=>'Les offres d\'abonnements',
	'logo'=>'Logo',


//M
	'mabonner' => 'M\'abonner !',
	'mon_abonnement' => 'abonnement',
	'mon_article' => 'Mon article',
	
//N
	'nouvel_abonnement'=>'Nouvel abonnement',
	'nous_sommes_le'=>'Aujourd\'hui, nous sommes le',
	'nombre_abonnements' => 'Nb.',
	'numeros_payants' => 'Articles payants',
	'nombre_abonnes_a_jour' => 'abonnés à jour :',
	'nombre_abonnes_dechus' => 'abonnements échus :',
	'nombre_abonnes_relances' => 'abonnés relancés :',
	'nombre_abonnes_inscrits' => 'abonnés inscrits :',
	'nombre_acheteurs_differents' => 'acheteurs différents : ',
	'num'=>'Num',
	
	
//O
	'offres_abonnement'=>'Offres d\'abonnements',

//P
	'paiement' => 'Paiement',
	'pas_de_identifiant' => 'Nom inconnu',


//R
	'renouveler_abonnement_proche' => 'Vous pouvez le renouveller dès aujourd\'hui ou bien choisir une autre formule ci-dessous.',
	'renouveler_abonnement_passe' => 'Vous pouvez le renouveller ou bien choisir une autre formule ci-dessous.',
	'rubriques'=>'Rubriques',
	'rubrique_abonnement'=>'Rubrique',
//S
	'statistiques' => 'Statistiques des abonnements',
	'statut_abonnement'=>'Statut',
	'statut_attente'=>'En attente',
	'statut_offert'=>'Offert',
	'statut_paye_abo'=>'Payé',
	'statut_envoye_abo'=>'Envoyé',
	'statut_encours'=>'En cours',
	'statut_echu'=>'échu',
	'supprimer_offre'=>'Supprimer cette offre',
	'simulation_paiement' => 'Tester une simulation de paiement avec ces boutons',
	'suivi_abonnement'=>'Suivi des abonnements',
//T
	'titre_les_abonnes'=>'LES ABONNÉS',
	'titre'=>'Titre',
	'total' => 'Total',
//U 	
	'uniquement_webmestres'=>'Par sécurité, seuls les webmestres peuvent configurer les offres.',

//V
	'valide_echu_depuis'=>'Votre abonnement n\'est plus valable depuis le @date@',
	'voir'=>'Voir',
	'voir_zones'=>'Voir les zones',
	'vos_articles' => 'Vos articles',
	'vos_acces_articles'=>'Vous avez accès aux articles suivants :',
	'vos_acces_articles_echus'=>'Vous n\'avez plus accès aux articles suivants :',
	'vos_acces_rubriques'=>'Vous avez accès aux rubriques suivantes :',
	'vos_acces_rubriques_echues'=>'Vous n\'avez plus accès aux rubriques suivantes :',
	'votre_abonnement' => 'Votre abonnement',

	);
?>
