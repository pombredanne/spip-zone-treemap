<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'title' => 'Disqus',
	'description' => 'Disqus comment platform for SPIP',
	'configuration' => 'Configuration variables', 
	'disqus_shortname' => 'Disqus shortname',
	'disqus_shortname_explication' => 'To install Disqus, you will need to know your forum shortname as registered on <a href="http://disqus.com">Disqus</a>',

	'num_items' => 'Number of items to display',
	'show_avatars' => 'Show avatars', 
	'avatar_size' => 'Avatar size',
	'size_small' => 'Small',
	'size_medium' => 'Medium',
	'size_large' => 'Large',
	'size_x_large' => 'X-Large',
	'size_ginormous' => 'Ginormous',
	'excerpt_length' => 'Comment Excerpt Length',
	
	'widgets_configuration' => 'Widgets configuration (optional)'
	
);

?>
