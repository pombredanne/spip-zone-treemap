<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'title' => 'Disqus',
	'description' => 'Plataforma de comentarios Disqus para SPIP',
	'configuration' => 'Variables de configuraci&oacute;n', 
	'disqus_shortname' => 'Identificaci&oacute;n del sitio (Disqus shortname)',
	'disqus_shortname_explication' => 'Para instalar Disqus, necesitar&aacute;s saber el identificador (<em>shortname</em>) de tu foro, 
									   como fue registrado en <a href="http://disqus.com">Disqus</a>',

	'num_items' => 'N&uacute;mero de elementos a mostrar',
	'show_avatars' => 'Mostrar avatares', 
	'avatar_size' => 'Tama&ntilde;o del avatar',
	'size_small' => 'Peque&ntilde;o',
	'size_medium' => 'Mediano',
	'size_large' => 'Grande',
	'size_x_large' => 'Muy grande',
	'size_ginormous' => 'Gigante',
	'excerpt_length' => 'Largo del extracto del comentario',
	
	'widgets_configuration' => 'Configuraci&oacute;n  de Widgets (opcional)'

);

?>
