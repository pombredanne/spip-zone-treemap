<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'rendre_prive' => 'Rendre cet article priv&eacute;',
	'rendre_public' => 'Rendre cet article public',
	'add_user_zone' => 'Ajouter un utilisateur &agrave; la zone :',
	'add_as_auteur' => 'L\'ajouter comme auteur',
	'delete' => 'Supprimer',
	'zone_auteur' => 'L\'auteur :',
	'zone_auteurs' => 'Les auteurs :',
	'zone_users' => 'Utilisateurs autoris&eacute;s :',
	'supprimer_zone_sure' => 'Vous &ecirc;tes sur le point de vous enlever de la liste des auteurs de cet article, cette action est irr&eacute;versible. Vous resterez cependant utilisateur. Voulez vous r&eacute;ellement effectuer cette action?',
	'supprimer_zone_auteur' => 'Vous &ecirc;tes sur le point de supprimer un des auteurs de cet article, cette action est irr&eacute;versible. Il restera cependant utilisateur normal. Voulez vous r&eacute;ellement effectuer cette action?',
	'supprimer_zone_autorise' => 'Vous &ecirc;tes sur le point de supprimer une des personnes autoris&eacute;es de cet article. Voulez vous r&eacute;ellement effectuer cette action?',
	'bouton_valider' => 'Valider',
);
