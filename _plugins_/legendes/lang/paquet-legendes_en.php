<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-legendes
// Langue: en
// Date: 16-06-2012 15:04:23
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// L
	'legendes_description' => 'A plugin to add captions on pictures as Flickr. inspired from [Fotonotes->http://www.fotonotes.net/] and based on the script [jQuery Image Annotation->http://www.flipbit.co.uk/jquery-image-annotation.html].',
	'legendes_nom' => 'Captions',
	'legendes_slogan' => 'Add captions on photos',
);
?>