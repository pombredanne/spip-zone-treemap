<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-legendes
// Langue: fr
// Date: 16-06-2012 15:04:23
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// L
	'legendes_description' => 'Un plugin pour ajouter des légendes à ses images comme sur Flickr. Inspiré de [Fotonotes->http://www.fotonotes.net/] et basé sur le script [jQuery Image Annotation->http://www.flipbit.co.uk/jquery-image-annotation.html].',
	'legendes_nom' => 'Légendes',
	'legendes_slogan' => 'Légender ses photos',
);
?>