<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'ajouter_legende' => 'Add a caption',
	'autoriser_creerdans' => 'Create captions',
	'autoriser_modifier' => 'Modify captions',
	'autoriser_supprimer' => 'Delete captions',

	'bouton_supprimer' => 'Delete',
	
	'cfg_descr_legendes' => 'The plugin "Captions" allows you to add annotations to images. This form allows you to configure its use.',
	'cfg_legend_statuts' => 'Status',
	'cfg_titre_legendes' => 'Captions',
	
	'editer_legende' => 'Edit a caption',
	'explications_statuts_general' => 'Each selector below set the minimum status of a user to perform a particular action.',
	
	'legende_enregistrer_ok' => 'Caption saved',
	'legende_supprimer_ok' => 'Caption deleted',
	
	'msg_yaunenote' => 'A caption is linked to this image. Hover it to view the caption.',
	'msg_yadesnotes' => '@nb@ captions are linked to this image. Hover it to view the captions.',
	
	'texte_legende' => 'Text of the caption'
	
);

?>
