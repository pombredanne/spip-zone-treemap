<?php
/***************************************************************************\
 * Plugin Duplicator pour Spip 2.0
 * Licence GPL (c) 2010 - Apsulis
 * Duplication de rubriques et d'articles
 *
\***************************************************************************/

// Ceci est un fichier langue de SPIP  --  This is a SPIP language file

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
	'aide_choix_champs' => 'Champs sql séparés par une "," .Laisser vide pour sélectionner les champs principaux.',

// C
	'choix_type' => 'Sélectionner le ou les mode(s) de duplication',
	'choix_champs' => 'Choisir les champs à dupliquer',

	'configuration_duplicator' => 'Configuration du Duplicator',

// I
	'icone_dupliquer' => 'Dupliquer la rubrique',

// L
	'label_art_champs' => 'Liste des champs à dupliquer pour chaque article :',
	'label_rub_champs' => 'Liste des champs à dupliquer pour chaque rubrique :',


// M	
	'message_annuler' => 'Annuler',
	'message_confirmer' => 'Confirmer',	
	
	'operation_executee' => "L'op&eacute;ration a bien &eacute;t&eacute; ex&eacute;cut&eacute;e.",
	'operation_annulee' => "L'op&eacute;ration a &eacute;t&eacute; annul&eacute;e.",
	'operation_retour_ok' => "Se rendre dans la rubrique copi&eacute;e.",
	'operation_retour_ko' => "Retour aux rubriques.",

	'icone_dupliquer_article' => "Dupliquer l'article",

	'operation_retour_ok_article' => "Se rendre dans l'article dupliqu&eacute;.",
	'operation_retour_ko_article' => "Retour aux articles.",

	'texte_duplicator' => "Appliquer la duplication aux articles et / ou aux rubriques"
);
