<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-gviewer
// Langue: fr
// Date: 28-05-2012 16:13:34
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// G
	'gviewer_description' => 'Modèle pour utiliser le [lecteur de documents en ligne de Google->http://docs.google.com/viewer]. Ce plugin étend le modève <code><mediaXX|embed></code> pour les documents de type doc, docx, xls, xlsx, ppt, pptx, pdf, ai, psd, eps, ps, et ttf. En l\'absence des modèles media, il est possible d\'utiliser <code><embXX|google></code>.',
	'gviewer_slogan' => 'Modèles dinsertion de documents utilisant le lecteur de documents de Google',
);
?>