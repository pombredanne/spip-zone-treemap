<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'elements_description' => 'Permet de définir sur les articles et les rubriques des éléments qui peuvent être affichés quelque part dans les squelettes.',
	'elements_nom' => 'Eléments',
	'elements_slogan' => 'Gérer des éléments sur des pages',
);

?>
