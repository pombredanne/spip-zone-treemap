#!/usr/bin/perl
## script pour extraire des infos du fichier serial.php
# License CC by-sa 2.0


while(<>) {
    if($_ =~ /^\$spip_([a-z]+) = array\($/) {
	print "Table: $1\n";
    } elsif($_ =~ /['"]([a-z_]+)['"]\s*=>/) {
	print "\t{$1} / #".uc($1)."\n";
    }

}
