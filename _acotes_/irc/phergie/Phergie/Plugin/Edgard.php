<?php
/**
 * Phergie
 *
 * PHP version 5
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://phergie.org/license
 *
 * @category  Phergie
 * @package   Phergie_Plugin_Daddy
 * @author    Phergie Development Team <team@phergie.org>
 * @copyright 2008-2011 Phergie Development Team (http://phergie.org)
 * @license   http://phergie.org/license New BSD License
 * @link      http://pear.phergie.org/package/Phergie_Plugin_Daddy
 */

/**
 * Simply responds to messages addressed to the bot that contain the phrase
 * "Who's your daddy?" and related variations.
 *
 * @category Phergie
 * @package  Phergie_Plugin_Daddy
 * @author   Phergie Development Team <team@phergie.org>
 * @license  http://phergie.org/license New BSD License
 * @link     http://pear.phergie.org/package/Phergie_Plugin_Daddy
 */
class Phergie_Plugin_Edgard extends Phergie_Plugin_Abstract
{

    public $datas = array();
    public $reloaded = false;

    /**
     * Charger les donnees du fichier texte
     *
     * @return void
     */
    public function onLoad()
    {
		$fp1 = file_get_contents("http://edgard.spip.net/spip.php?page=edgard&var_mode=recalcul");
		$fp2 = file_get_contents("http://edgard.spip.net/spip.php?page=mafaq&var_mode=recalcul");
		if ($fp1 AND $fp2)
			$this->reloaded = true;
		
        if ($fichier = fopen("/var/www/edgard.spip.net/public_html/tmp/edgard.txt", "r")) {
            while (!feof($fichier)) {
                $ligne = fgets($fichier);
                if (strlen($ligne)>2) {
                	list($key, $value) = explode(';', $ligne);
                	$this->datas[$key] = $value;
				}
            }
            fclose($fichier);
        }
    }

    /**
     * Checks messages for the question to which it should respond and sends a
     * response when appropriate
     *
     * @return void
     */
    public function onPrivmsg()
    {
        $config = $this->getConfig();
        $prefix = $config['command.prefix'];
        $event = $this->getEvent();
        $text = $event->getArgument(1);
        $target = $event->getNick();
        $source = $event->getSource();
        $trouve = false;


        //commençons par (^#\d{2,4}\b);http://core.spip.org/issues/
        $pattern = '/' . preg_quote($prefix) .
            '\#([0-9]+)/iAD';
        if (!$trouve AND preg_match($pattern, $text, $m)) {
            $msg = 'Ok ' . $target . " c'est par ici : http://core.spip.org/issues/" . $m[1];
            $this->doPrivmsg($source, $msg);
            $trouve = true;
        }


        // http://core.spip.org/projects/spip/repository/revisions/18395
        $pattern = '/' . preg_quote($prefix) .
            '^c([0-9]+)/iAD';
        if (!$trouve AND preg_match($pattern, $text, $m)) {
            $msg = 'Ok ' . $target . " le commit c'est par ici : http://core.spip.org/projects/spip/repository/revisions/" . $m[1];
            $this->doPrivmsg($source, $msg);
            $trouve = true;
        }

        // http://core.spip.org/projects/spip/repository/revisions/18395
        $pattern = '/' . preg_quote($prefix) .
            '^z([0-9]+)/iAD';
        if (!$trouve AND preg_match($pattern, $text, $m)) {
            $msg = 'Ok ' . $target . " le commit c'est par ici : http://zone.spip.org/trac/spip-zone/changeset/" . $m[1];
            $this->doPrivmsg($source, $msg);
            $trouve = true;
        }

        // @? 
        $pattern = '/' . preg_quote($prefix) .
            '^@\?([^?]+)/iAD';
        if (!$trouve AND preg_match($pattern, $text, $m)) {
            $m[1] = urlencode(trim($m[1]));
            $msg = "Hop http://edgard.spip.net/questions/" . $m[1];
            $this->doPrivmsg($source, $msg);
            $trouve = true;
        }

        // @unzip toto  
        $pattern = '/' . preg_quote($prefix) .
            '^@unzip ([^ ]+)/iAD';
        if (!$trouve AND preg_match($pattern, $text, $m)) {
            $msg = "Un zip ? http://zone.spip.org/trac/spip-zone/changeset/latest/$m[1]?old_path=/&format=zip ";
            $this->doPrivmsg($source, $msg);
            $trouve = true;
        }

        // @function() 
        $pattern = '/' . preg_quote($prefix) .
            '^@([a-z0-9_]{3,})\(\)/iAD';
        if (!$trouve AND preg_match($pattern, $text, $m)) {
            $m[1] = urlencode(trim($m[1]));
            $msg = "Hop http://doc.spip.org/@" . $m[1] . " - Hop (sinon) http://doc.spip.org/spip.php?page=recherche&recherche=" . $m[1];
            $this->doPrivmsg($source, $msg);
            $trouve = true;
        }
 
         // chercher une entrée dans les datas du wiki
        if (!$trouve) {
            foreach ($this->datas as $key => $value) {
                $pattern = '~' . preg_quote($prefix) . $key . '~iAD';
                if (preg_match($pattern, $text, $m)) {
                    $this->doPrivmsg($source, $value);
                    $trouve = true;
                    break;
                }
            }
        }
        
        // demande de reload ?
        $pattern = '/' . preg_quote($prefix) .
            '^Edgard:/iAD';
        if (!$trouve AND preg_match($pattern, $text, $m)) {
            $this->onLoad();
            if ($this->reloaded)
            	$this->doPrivmsg($source, "Et voila je suis en phase avec moi");
            else
            	$this->doPrivmsg($source, "Ah probleme au rechargement");
        }
    }
}
