<?php

// REZO module for IQ (http://f0rked.com/IQ)
// version: 2.1
// author: fil@rezo.net
// This script will take recent articles from a SPIP database and
// return the top results to #rezo every 90 seconds
// also send new stuff to #spip

$this->bind("pubm","","","",'rezo');


$this->functions["rezo"]=create_function('$args','
	global $bot;
	static $welcome;
	static $welcome_spip;
	static $dingdong;
	static $seen;
	static $seen_spip;

	if (!function_exists("spip_query")) {
		chdir("/var/shim/rezo/Web");
		include("ecrire/inc_version.php3");
		include_ecrire("inc_connect.php3");
		include_ecrire("inc_texte.php3");
		include_ecrire("inc_sites.php3");
		include_ecrire("inc_meta.php3");
	}
	if (!$bot->replyto("rss_idle",90)) {
		# initialisation
		if (!$welcome) {
			$welcome = true;
#			$bot->msg("#rezo","Salut, c\'est moi rez000 ! je reb00te...", 1, $buffering);
		}

		$s = mysql_query("SELECT * FROM spip_articles
		ORDER BY date DESC
		LIMIT 0,10");
#$bot->msg("_fil_", "coucou $s");
		while ($t = mysql_fetch_array($s)) {
			$watch =trim($t["url_site"]);
			$newseen[$watch] = true;
			if (
				!$seen[$watch]
				AND $l++<4
			) {
				$titre = filtrer_entites(supprimer_tags(texte_backend(typo($t["titre"]))));

				# aller chercher le titre de la rubrique
				list($rub) = mysql_fetch_array(mysql_query("SELECT titre FROM spip_rubriques WHERE id_rubrique=".$t["id_rubrique"]));
				$rub = filtrer_entites(supprimer_tags(texte_backend(typo($rub.", ".heures($t["date"]).":".minutes($t["date"])))));

				$bot->msg("#rezo","$titre ($rub) $watch", 1, $buffering);
				sleep(1);
			}
		}

		$seen = $newseen; # ne garder que 10 resultats dans $seen (sinon explosion)
	} # reply-to


	### troll rss->#spip
	if (!$bot->replyto("rss_idle_spip",90)) {
		# initialisation
		if (!$welcome_spip) {
			$welcome_spip = true;
#			$bot->msg("#spip","Salut, c\'est moi ! je reb00te...", 1, $buffering);
		}

		if ($rss = recuperer_page("http://sedna.spip.org/sedna/?rss=1"))
		foreach (analyser_backend($rss) as $t) {

			$watch =trim($t["url"]);
			$newseen_spip[$watch] = true;
			if (
				!$seen_spip[$watch]
				AND $l++<4
			) {
				$titre = typo($t["titre"]);
				$rub = $t["source"];
				$auteur = $t["lesauteurs"] ? ", par ".$t["lesauteurs"] : "";
				$msg = "$titre$auteur ($rub) $watch";
				$msg = unicode_to_utf_8(texte_backend(textebrut($msg)));
				$bot->msg("#spip", $msg, 1, $buffering);
				sleep(1);
			}
#			if ($dingdong <> ($t = "H+".ceil((date("U")-date("U",mktime(1,0,0,7,1,2006)))/3600)))
#				$bot->msg("#spip", $dingdong = $t, 1, $buffering);
		}

		$seen_spip = $newseen_spip; # ne garder que 10 resultats dans $seen (sinon explosion)
	} # reply-to

	chdir("/home/fil/IQ-0.9.3");  ## where is IQ ?
');

$this->infoLog("rezo module loaded");
?>
