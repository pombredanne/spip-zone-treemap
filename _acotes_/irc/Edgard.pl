#!/usr/bin/perl

use strict;
use warnings;

my $base="http://edgard.spip.org";

my $URL="$base/spip.php?page=edgard&var_mode=recalcul";
my $URL2="$base/spip.php?page=mafaq&var_mode=recalcul";
my $nick = 'Edgard';

my %hash;

# On utilise la librairie Net::IRC pour se connecter à IRC
use Net::IRC;
use HTTP::Request ;
use LWP::UserAgent;

# Configuration des options de connexion (serveur, login) :
my $server = 'irc.freenode.net';


# Informations concernant le Bot :
my $ircname = 'Bot IRC Perl';
my $username = 'perlBot';
my $version = '1.0';


# Channel sur lequel on veut que le Bot aille :
my $channel = '#spip';


my $FICHIER_WIKI="/var/www/edgard.spip.org/public_html/tmp/edgard.txt";
 

# On crée l'objet qui nous permet de nous connecter à IRC :
my $irc = new Net::IRC;


# On crée l'objet de connexion à IRC :
my $conn = $irc->newconn(
    'Server'      => $server,
    'Port'        => 6667, 
    'Nick'        => $nick,
    'Ircname'     => $ircname,
    'Username'    => $username
);


# On installe les fonctions de Hook :
$conn->add_handler('376', \&on_connect);         # Fin du MOTD => on est connecté
$conn->add_handler('public', \&on_public);       # Sur le chan


# On lance la connexion et la boucle de gestion des événements :

$irc->start();


sub on_connect
{
	my ($conn, $event) = @_;
	
	$conn->join($channel);
	#$conn->privmsg($channel, 'Salut ... ah  une minute sans IRC, que du bonheur');
	#print "<$nick>\t1 minute sans IRC, que du bonheur !\n";
	reload_edgard($conn,$channel);
	
	$conn->{'connected'} = 1;
} # Fin on_connect


sub reload_edgard
{
	my ($conn, $channel) = @_;
	my %hash2;

	#on recharge la page edgard.html qui va ecrire le fichier texte 
	my $agent2 = LWP::UserAgent->new(env_proxy => 1,keep_alive => 1, timeout => 30);
	my $header2 = HTTP::Request->new(GET => $URL2);
	my $request2 = HTTP::Request->new('GET', $URL2, $header2);
	my $response2 = $agent2->request($request2);


	#on recharge la page edgard.html qui va ecrire le fichier texte 
	my $agent = LWP::UserAgent->new(env_proxy => 1,keep_alive => 1, timeout => 30);
	my $header = HTTP::Request->new(GET => $URL);
	my $request = HTTP::Request->new('GET', $URL, $header);
	my $response = $agent->request($request);

	# Check the outcome of the response
	if ($response->is_success){
		$conn->privmsg($channel, "Et voila je suis en phase avec moi"); 
	}elsif ($response->is_error){
		$conn->privmsg($channel, "Ah probleme au rechargement "); 
 	} 



	open (FILE,"<$FICHIER_WIKI");
	while (<FILE>)
	{    
		# chaque ligne est successivement affectée à $_
		my $ligne="$_";
		chomp $ligne;
		(my $cle1, my $cle2)=split(/;/,$ligne);
		if ($cle1)
		{
			$hash2{"$cle1"}=$cle2;
			#print __LINE__."bingo $cle1    $cle2 \n";
		}
	}
	close (FILE);
	%hash=%hash2;
}




sub on_public
{
	my ($conn, $event) = @_;
	my $text = $event->{'args'}[0];
	if ( $text =~ /Edgard:/)
	{
		reload_edgard($conn,$channel);
	}
	$conn->print("<" . $event->{'nick'} . ">\t| $text");

		foreach my $key (keys(%hash))
		{
			my $lckey=lc($key);
			$text=lc($text);
			if ($text =~ /$lckey/)
			{
				# svn seul : /^[@#\[](\d+\b|([a-z0-9_]{3,})(?=\(\)))/
				if ($text =~ /^[@#\[r]([a-f0-9]+\b|([a-z0-9_]{3,})(?=\(\)))/) # [commit, #ticket ou @doc
				{
					$conn->privmsg($channel, "hop $hash{$key}".$1); # var url
				}
				elsif ($text =~ /^@\?([^\?]+)\?*/) # @?recherche
				{
					$text = "$1";
					$text =~ s/\s/\+/g;
					$conn->privmsg($channel, "$hash{$key}".$text); # var url
				}
				else
				{
					$conn->privmsg($channel, "$hash{$key}"); # Salutation sur le channel
				}
			}
		}
} # Fin on_public
