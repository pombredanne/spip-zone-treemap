# rss2irc:  Get news from rss sites and post it into a certain irc
#           channel.
#!/usr/bin/perl -w
use strict;
use Net::IRC;
use XML::RSS;
use LWP::Simple;
use Encode;

my $rss = new XML::RSS(encoding=>"utf-8");
my $irc = new Net::IRC;

my $conn = $irc->newconn(
	Server 		=> 'irc.freenode.org',
	Nick		=> 'paolaspip',
	Ircname		=> 'paolaspip',
	Username	=> 'neoskills muse'
);

$conn->{channel1} = '#spip';
$conn->{status} = "check";
$conn->{oldtime} = time;
$conn->{newtime} = time;
my $cachedir1 = "cache1/";
my @sites1;

sleep 1;
read_sites1();
sleep 1;

sub read_sites1 { my $ch1; open($ch1, "sites1") or die "Cant open configfile: $!"; while(<$ch1>) { push(@sites1, [ split /;/ ]); } }

sub on_connect {
	my $conn = shift;
	$conn->join($conn->{channel1});
        print "joined channel $conn->{channel1}\n\n";
sleep 1;
}

sub handle_waiting {
	my $conn = shift;
        sleep ( 20 );
	$conn->{oldtime} = $conn->{newtime};
	$conn->{newtime} = time;
	my $secs = $conn->{newtime} - $conn->{oldtime};
	$conn->{waittime} += $secs;
	if ($conn->{waittime} > 60) {
sleep 1;
		$conn->{status} = 'check';
		$conn->{waittime} = 0;
	} else {
sleep 1;
        $conn->{status} = "wait";
    }
}

sub report_news 
{
    my $conn = shift;
    my ($content,$site,$title,$tmp);
    
    print "Feed1: \n";
sleep 1;
    foreach my $item (@sites1) 
    {
sleep 1;
        if( $content = get($item->[2]) ) 
        {
            my $ref = eval 
            {
                $rss->parse($content);
            };
            if($@) 
            {
                print "Error parsing feed : $item \n";
            } else 
            {
                my $tmpsite  = $item->[1]." - ".$item->[0];
                print "Feed OK: $tmpsite \n";
                if(!check_cache1($item->[0], $rss->{'items'}->[0]->{'link'})) 
                {
                    sleep(1);
                    my $title = Encode::encode("utf-8", $rss->{items}->[0]->{'title'}) . ', '.Encode::encode("utf-8", $rss->{items}->[0]->{'author'}) . ' ('.Encode::encode("utf-8", $rss->{items}->[0]->{'source'}).') - ';
                    $site  = ''.$item->[1].$item->[0].': ';
                    my $link = Encode::encode("utf-8", $rss->{'items'}->[0]->{'link'});
                    #my $phrase_irc="From : ".$site." - ". $title." -> ".$link;
                    my $phrase_irc=$title." -> ".$link;
                    $conn->privmsg($conn->{channel1},$phrase_irc );    
                    update_cache1($item->[0], $rss->{'items'}->[0]->{'link'});
                    print "Cache update : $site : $title : $link OK \n";
                    sleep(1);
                }
            }
        } else {
            $conn->privmsg($conn->{channel1}, "Error fetching page for: ".$item->[0]);
sleep 1;
        }
      }

    }

sub check_cache1 {
    my ($name) = shift;
    my ($link) = shift;
    my ($fh, $cachelink);
    my $cachefile = $cachedir1."/".$name;
    return 0 if( ! -e $cachefile ); 
    open($fh, $cachefile) or die ("Cant open cachefile: $!");
    $cachelink = <$fh>;
    close($fh); 
sleep 1;
    if( $cachelink eq $link ) {
        return 1;
    } else {
        return 0;
    }
}

sub update_cache1 {
    my ($name) = shift;
    my ($link) = shift;
    my ($fh, $cachelink);
    my $cachefile = $cachedir1."/".$name;
    open($fh, ">", $cachefile) or die ("Cant open cachefile: $!");
sleep 1;
    print $fh $link;
    close($fh);
}

sub check_status {
	my $conn = shift;
    if($conn->{status} =~ /^check$/) {
		report_news($conn);
        print "New Check\n";
        $conn->{waittime} = 0;
        $conn->{status} = "wait";
    } else {
sleep 1;
        handle_waiting($conn);
    }
}

sub on_disconnect {
    my ($self, $event) = @_;
    print "Disconnected from ", $event->from(), " (",
          ($event->args())[0], "). Attempting to reconnect...\n";
    $self->connect();
}

$conn->add_handler('376', \&on_connect);
$conn->add_global_handler('disconnect', \&on_disconnect);

while (1) {
sleep 1;
	check_status($conn);
	$irc->do_one_loop();
}	
