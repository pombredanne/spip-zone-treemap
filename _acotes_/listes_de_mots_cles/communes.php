<?php

include 'ecrire/inc_version.php';

# Commencer par creer le groupe de mots, et reporter ici son numero et son titre
# numero du groupe 'communes france'
define('ID_GROUPE', 12);
# titre du groupe 'communes france'
define('TITRE_GROUPE', 'Communes de France');

$codes = file('insee.csv');
if (count($codes)<100)
	die ('fichier mal lu');

array_shift($codes); # supprimer la premiere ligne

foreach ($codes as $ligne) {
	list($commune,$cp) = explode(';', $ligne);
	$commune = ucwords(strtolower($commune));
	echo "$cp $commune<br />\n";
	spip_query("INSERT IGNORE spip_mots (titre, type, id_groupe) VALUES ('$cp $commune', "._q(TITRE_GROUPE).", "._q(ID_GROUPE).")");
}

?>
