<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// $Id$

$GLOBALS[$GLOBALS['idx_lang']] = array(
//A
	'abomailmans' 					=> 'Mailmans',
	'abonne'						=> 'Je m\'abonne',
	'active'                        => 'Active',
	'aucune_langue'					=> 'Aucune sp&eacute;cifique',

//B
	'bouton_listes_diffusion'		=> 'Les listes de diffusion',
	'btn_abonnement'                => 'S\'abonner',
	'btn_desabonnement'             => 'Se d&eacute;sabonner',

//C
	'choisir_liste'					=> 'Vous devez choisir une liste.',
	'contenu_date'					=> 'Contenu a partir de cette date',
	'creation_droits_insuffisants'	=> 'Vos droits sont insuffisants...',

//D
	'desactive'                     => 'D&eacute;sactiv&eacute;e',

//E
	'emailliste_abomailman'			=> 'L\'adresse e-mail de la liste',
	'emailliste_abosympa'           => 'L\'adresse e-mail de l\'administrateur Sympa',
	'email'							=> 'E-mail',
	'email_abonnement'				=> 'Votre adresse e-mail',
	'email_envoye'					=> 'L\'e-mail a &eacute;t&eacute; envoy&eacute; &agrave; la liste de diffusion : @liste@.',
	'email_oublie'					=> 'Vous avez oubli&eacute; votre adresse e-mail',
	'envoi_apercu'					=> 'Aper&ccedil;u',
	'envoi_confirmer'				=> 'Confirmer et envoyer',
	'envoyer_courier'				=> 'Envoyer un courrier',
	'envoyer_courier_liste'			=> 'Envoyer ce courrier &agrave; cette liste de diffusion :',
	'envoyer_mailmans'				=> 'S&eacute;l&eacute;ctionner le mod&egrave;le et son contenu',
	'erreur_email_liste_oublie'		=> 'L\'adresse email de la liste est obligatoire',
	'explication_email_sympa'		=> 'Si ce champ est renseign&eacute;, la liste est consid&eacute;r&eacute;e comme une liste de serveur "Sympa", dans le cas contraire comme une liste "Mailman".',

//I
	'icone_ajouter_liste'			=> 'Ajouter une nouvelle liste',
	'icone_envoyer_mail_liste'		=> 'Envoyer un e-mail aux lettres &agrave; partir du contenu de ce site',
	'info_sisympa'                  => '[Obligatoire si liste Sympa]',
	'insciption_listes_legende'		=> 'Abonnement au listes de diffusion',
	'inscription_lettres_legende'	=> 'Abonnement aux listes de <br />diffusion et de discussions',

//J
	'je_m_abonne'					=> 'Cochez pour valider l\'abonnement ou le d&eacute;sabonnement.',

//L
	'label_etat_liste'				=> '&Eacute;tat de la liste',
	'langue_liste'					=> 'Langue de la liste',
	'les_listes_mailmans'			=> 'Les listes mailmans ou sympas renseign&eacute;es',
	'lire_article' 					=> 'Lire l\'article',
	'liste_creee'					=> 'La liste num&eacute;ro @id@ (@titre@) a &eacute;t&eacute; cr&eacute;&eacute;e.',
	'liste_non_existante'			=> 'La liste demand&eacute;e n\'existe pas ou a &eacute;t&eacute; supprim&eacute;e',
	'liste_oublie'                  => 'Vous avez oubli&eacute; de cocher une liste !',
	'liste_supprimee'				=> 'La liste num&eacute;ro @id@ (@titre@) a &eacute;t&eacute; supprim&eacute;e.',
	'liste_updatee'					=> 'La liste num&eacute;ro @id@ (@titre@) a &eacute;t&eacute; mise &agrave; jour.',

//M
	'message'						=> 'Introduction &agrave; votre courrier, avant le contenu issu du site',
	'message_confirmation_a'      	=> 'Une demande d\'abonnement aux listes suivantes vient d\'&ecirc;tre envoy&eacute;e :',
	'message_confirmation_unique_a' => 'Une demande d\'abonnement &agrave; la liste suivante vient d\'&ecirc;tre envoy&eacute;e :',
	'message_confirmation_d'        => 'Une demande de d&eacute;sabonnement aux listes suivantes vient d\'&ecirc;tre envoy&eacute;e. ',
	'message_confirmation_unique_d'	=> 'Une demande de d&eacute;sabonnement &agrave; la liste suivante vient d\'&ecirc;tre envoy&eacute;e. ',
	'message_confirm_suite'         => 'Pour valider votre demande, r&eacute;pondez &agrave; la demande de confirmation que vous allez recevoir par mail.',
	'mot'							=> 'Et lister les articles du mot cl&eacute;',

//N
	'nom'							=> 'Nom et pr&eacute;nom (facultatif)',

//P
	'prenom'						=> 'Pr&eacute;nom',

//R
	'rubrique'						=> 'Et lister les articles de la rubrique',

//S
	'souhaite_rester'				=> 'Je souhaite rester inform&eacute;-e',
	'sujet'							=> 'Sujet du courrier',
	'sujet_obligatoire'				=> 'Le sujet est obligatoire.',
	'supprimer'						=> 'Supprimer',
	'sympa_message_confirmation'	=> 'Un email de validation a &eacute;t&eacute; envoy&eacute; &agrave l\'adresse : ',

//T
	'template'						=> 'Choisissez le mod&egrave;le et son contenu',
	'titre_abomailman'				=> 'Titre de la liste',
	'titre_liste_obligatoire'		=> 'Le titre de la liste est obligatoire',
	'toute_liste'					=> 'Toutes les listes de diffusions',

//V
	'verifier_formulaire'			=>	'V&eacute;rifiez le remplissage du formulaire.',
	'votre_email'					=> 'Votre email',
);
?>