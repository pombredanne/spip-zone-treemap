<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// $Id: abomailmans_fr.php 31752 2009-09-23 00:09:48Z kent1@arscenic.info $

$GLOBALS[$GLOBALS['idx_lang']] = array(
//A
	'abomailmans' 					=> 'Mailmans',
	'abonne'						=> 'Ich abonniere',
	'active'                        => 'Aktiv',
	'aucune_langue'					=> 'Keine ANgabn',

//B
	'bouton_listes_diffusion'		=> 'Mailinglisten',
	'btn_abonnement'                => 'Abonnieren',
	'btn_desabonnement'             => 'Abbestellen',

//C
	'choisir_liste'					=> 'Sie m&uuml;ssen eine Liste w&auml;hlen.',
	'contenu_date'					=> 'Inhalt ab diesem Datum',
	'creation_droits_insuffisants'	=> 'Sie haben kein Recht dazu ...',

//D
	'desactive'                     => 'Abgeschaltet',

//E
	'emailliste_abomailman'			=> 'E-Mail Adresse der Liste',
	'emailliste_abosympa'           => 'E-Mail Adresse des Sympa-Administrators',
	'email'							=> 'E-Mail',
	'email_abonnement'				=> 'Ihre E-Mail Adresse',
	'email_envoye'					=> 'Die E-Mail wurde an die Mailingliste verschickt: @liste@.',
	'email_oublie'					=> 'Sie haben ihre E-Mail Adresse vergessen',
	'envoi_apercu'					=> 'Vorschau',
	'envoi_confirmer'				=> 'Best&auml;tigen und absenden',
	'envoyer_courier'				=> 'Mail senden',
	'envoyer_courier_liste'			=> 'Mail an diese Mailingliste senden:',
	'envoyer_mailmans'				=> 'Modell und Inhalt ausw&auml;hlen',
	'erreur_email_liste_oublie'		=> 'Die E-Mail Adresse ist obligatorisch.',
	'explication_email_sympa'		=> 'Wenn dieses Feld ausgef&uuml;llt ist, wird von einer Sympa-Liste ausgegangen,
							anderenfalls von einer Mailman-Liste.',

//I
	'icone_ajouter_liste'			=> 'Neue Liste hinzuf&uuml;gen',
	'icone_envoyer_mail_liste'		=> 'Mail aus dem Inhalt dieser Website generieren und an die Buchstaben senden',
	'info_sisympa'                  => '[Obligatorisch bei Sympa-Listen]',
	'insciption_listes_legende'		=> 'Mailing-Listen Abonnements',
	'inscription_lettres_legende'	=> 'Abonnement bei Newslettern<br />und Diskussionslisten',

//J
	'je_m_abonne'					=> 'Markieren um Abonnement zu bestellen oder zu k&uuml;ndigen.',

//L
	'label_etat_liste'				=> 'Status der Liste',
	'langue_liste'					=> 'Sprache der Liste',
	'les_listes_mailmans'			=> 'Bekannte Mailman-Listen',
	'lire_article' 					=> 'Artikel lesen',
	'liste_creee'					=> 'Die Liste Nummer @id@ (@titre@) wurde angelegt.',
	'liste_non_existante'			=> 'Die Liste existiert nich oder wurde entfernt.',
	'liste_oublie'                  => 'Sie habe vergessen, eine Liste auszuw&auml;hlen!',
	'liste_supprimee'				=> 'Die Liste Nummer @id@ (@titre@) wurde gel&ouml;scht.',
	'liste_updatee'					=> 'Die Liste Nummer  @id@ (@titre@) wurde aktualisiert.',

//M
	'message'						=> 'Einleitungstext vor den Inhalten ihrer Website',
	'message_confirmation_a'      	=> 'Aboanfragen an folgende Listen wurden gesendet:',
	'message_confirmation_unique_a' => 'Eine Aboanfragen an folgende Liste wurden gesendet:',
	'message_confirmation_d'        => 'Stornierungsauftr&auml;ge an folgende Listen wurden gesendet. ',
	'message_confirmation_unique_d'	=> 'Stornierungsauftrag an folgende Liste wurden gesendet. ',
	'message_confirm_suite'         => 'Um ihren Auftrag zu best&auml;tigen beantworten sie bitte die Best&auml;tigungsmail, die sie erhalten werden..',
	'mot'							=> 'Artikel zu diesem Schlagwort auflisten',

//N
	'nom'							=> 'Name und Vorname (freiwillige Angabe)',

//P
	'prenom'						=> 'Vorname',

//R
	'rubrique'						=> 'Artikel der Rubrik auflisten',

//S
	'souhaite_rester'				=> 'Ich m&ouml;chte auf dem Laufenden bleiben',
	'sujet'							=> 'Thema der Mail',
	'sujet_obligatoire'				=> 'Es muss ein Thema angegeben werden.',
	'supprimer'						=> 'L&ouml;schen',
	'sympa_message_confirmation'	=> 'Eine Best&auml;tigungsmail wurde an folgende Adresse gesendet: ',

//T
	'template'						=> 'Modell und Inhalte ausw&auml;hlen',
	'titre_abomailman'				=> 'Bezeichnung der Liste',
	'titre_liste_obligatoire'		=> 'Die Liste muss eine Bezeichnung erhalten',
	'toute_liste'					=> 'ALle Mailinglisten',

//V
	'verifier_formulaire'			=>	'&Uuml;berpr&uuml;fen sie den Inhalt des Formulars.',
	'votre_email'					=> 'Ihre E-Mail',
);
?>