<?php
/* 
 * phpMyVisites : website statistics and audience measurements
 * Copyright (C) 2002 - 2006
 * http://www.phpmyvisites.net/ 
 * phpMyVisites is free software (license GNU/GPL)
 * Authors : phpMyVisites team
*/

// $Id: pagesGap.php,v 1.1 2005/11/06 22:14:26 matthieu_ Exp $


$GLOBALS['pagesGap'] = array(
	array("1", "1"),
	array("2", "2"),
	array("3", "3"),
	array("4", "4"),
	array("5", "5"),
	array("6", "7"),
	array("7", "10"),
	array("10", "14"),
	array("14", "20"),
	array("20")
	);
?>