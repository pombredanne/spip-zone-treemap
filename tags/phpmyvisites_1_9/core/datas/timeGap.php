<?php
/* 
 * phpMyVisites : website statistics and audience measurements
 * Copyright (C) 2002 - 2006
 * http://www.phpmyvisites.net/ 
 * phpMyVisites is free software (license GNU/GPL)
 * Authors : phpMyVisites team
*/

// $Id: timeGap.php,v 1.3 2005/10/07 00:38:21 matthieu_ Exp $


$GLOBALS['timeGap'] = array(
			array("0", "0.5"),
			array("0.5", "1"),
			array("1", "2"),
			array("2", "4"),
			array("4", "6"),
			array("6", "8"),
			array("8", "11"),
			array("11", "15"),
			array("15", "30")
			);
?>