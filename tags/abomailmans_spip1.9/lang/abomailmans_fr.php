<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// $Id$

$GLOBALS[$GLOBALS['idx_lang']] = array(
		'abomailmans' 					=> 'Mailmans',
		'icone_ajouter_liste'			=> 'Ajouter une nouvelle liste',
		'icone_envoyer_mail_liste'		=> 'Envoyer un e-mail aux listes &agrave; partir du contenu de ce site',
		'les_listes_mailmans'			=> 'Les listes mailmans renseign&eacute;es',
		
		
		'titre_abomailman'				=> 'Titre de la liste',
		'emailliste_abomailman'			=> 'L\'adresse e-mail de la liste',
		'supprimer'						=> 'Supprimer',

		'nom'							=> 'Nom et pr&eacute;nom (facultatif)',
		'prenom'						=> 'Pr&eacute;nom',
		'email_abonnement'				=> 'Votre adresse e-mail',
		'je_m_abonne'					=> 'Cochez pour valider l\'abonnement ou le d&eacute;sabonnement.',

		'message_confirmation_a'      	=> 'Une demande d\'abonnement aux listes suivantes vient d\'&ecirc;tre envoy&eacute;e :',
        'message_confirmation_d'        => 'Une demande de d&eacute;sabonnement aux listes suivantes vient d\'&ecirc;tre envoy&eacute;e. Il faudra r&eacute;pondre aux mails que vous allez recevoir pour valider ces demandes : ',
        'message_confirm_suite'         => 'Pour valider votre demande, r&eacute;pondez &agrave; la demande de confirmation que vous allez recevoir par mail.',
		
		'email_oublie'					=> 'Vous avez oubli&eacute; votre adresse e-mail',

		'template'						=> 'Choisissez le mod&egrave;le et son contnu',
		'sujet'							=> 'Sujet du courrier',
		'rubrique'						=> 'Et lister les articles de la rubrique',
		'mot'							=> 'Et lister les articles du mot cl&eacute;',
		'message'						=> 'Introduction &agrave; votre courrier, avant le contenu issu du site',
		'envoi_apercu'					=> 'Aper&ccedil;u',
		'envoi_confirmer'				=> 'Confirmer et envoyer',
		'envoyer_mailmans'				=> 'S&eacute;l&eacute;ctionner le mod&egrave;le et son contenu',
		
		'emailliste_abosympa'           => 'L\'adresse e-mail de l\'administrateur Sympa',
		'info_sisympa'                  => '[Obligatoire si liste Sympa]',
		'btn_abonnement'                => 'S\'abonner',
		'btn_desabonnement'             => 'Se d&eacute;sabon.',
		'liste_oublie'                  => 'Vous avez oubli&eacute; de cocher une liste !',
		'active'                        => 'Activ&eacute;',
		'desactive'                     => 'D&eacute;sactiv&eacute;'
);


?>