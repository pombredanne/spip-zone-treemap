<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'activer_message' => 'Activer la maintenance',
'en_travaux' => 'En travaux',
'parametrage_page_travaux' => 'Param&eacute;trage de la page temporaire',
'info_message' => 'Cette page permet de mettre un message temporaire sur toute les pages du site pendant une phase de maintenance.',
'message_temporaire' => 'Votre message temporaire :',

);
?>