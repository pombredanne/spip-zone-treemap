<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_forum_longueur' => 'Length of messages',
	'cfg_forum_longueur_maxi' => 'Maximum length',
	'cfg_forum_longueur_mini' => 'Minimum length',
	'comment' => 'comment',
	'comments' => 'comments',
	'comments_h' => 'Your comments',

	// D
	'date_heure_a' => 'at',
	'date_jour_le' => 'On',

	// F
	'forum_qui_etes_vous' => 'Who are you?',

	// L
	'label_email' => 'Email (not&nbsp;published)',
	'label_nom' => 'Name',
	'label_url' => 'Your web site',

	// M
	'moderation_info' => 'Warning, your message will only be displayed after it has been checked and approved.',

	// P
	'permalink_to' => 'Permanent link to the comment',

	// R
	'reponse_comment_modere' => 'Your comment has been saved and is awaiting proofreading before it is published.',
	'reponse_comment_ok' => 'Thank you for your comment!',

	// S
	'saisie_texte_info' => 'This form accepts SPIP&nbsp;shortcuts <code>[-&gt;urls] {{bold}} {italics} &lt;quotes&gt; &lt;code&gt;</code> and HTML&nbsp;code <code>&lt;q&gt; &lt;del&gt; &lt;ins&gt;</code>. To create paragraphs, just leave empty lines.',
	'saisie_texte_legend' => 'Enter your comment here',
	'submit1' => 'Preview',
	'submit2' => 'Submit',

	// T
	'titre_comments' => 'Comments'
);

?>
