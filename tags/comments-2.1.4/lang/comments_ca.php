<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_forum_longueur' => 'Llargada dels missatges',
	'cfg_forum_longueur_maxi' => 'Llargada m&agrave;xima',
	'cfg_forum_longueur_mini' => 'Llargada m&iacute;nima',
	'comment' => 'comentari',
	'comments' => 'comentaris',
	'comments_h' => 'Els teus comentaris',

	// D
	'date_heure_a' => 'a',
	'date_jour_le' => 'El',

	// F
	'forum_qui_etes_vous' => 'Qui ets?',

	// L
	'label_email' => 'Correu electr&ograve;nic (no&nbsp;publicat)',
	'label_nom' => 'Nom',
	'label_url' => 'El teu lloc Web',

	// M
	'moderation_info' => 'Atenci&oacute;, el teu missatge no apareixer&agrave; fins que no hagi estat rellegit i aprovat. ',

	// P
	'permalink_to' => 'Enlla&ccedil; permanent cap el comentari',

	// R
	'reponse_comment_modere' => 'El teu comentari s\'ha enregistrat correctament i est&agrave; esperant la relectura abans de ser publicat.',
	'reponse_comment_ok' => 'Gr&agrave;cies pel comentari!',

	// S
	'saisie_texte_info' => 'Aquest formulari accepta les dreceres SPIP <code>[-&gt;url] {{italique}} {italique} &lt;quote&gt; &lt;code&gt;</code> i el codi HTML <code>&lt;q&gt; &lt;del&gt; &lt;ins&gt;</code>. Per crear par&agrave;grafs, deixeu simplement l&iacute;nies buides.',
	'saisie_texte_legend' => 'Afegeix el comentari aqu&iacute;',
	'submit1' => 'Visualitzaci&oacute; pr&egrave;via',
	'submit2' => 'Confirmar l\'enviament',

	// T
	'titre_comments' => 'Comentaris'
);

?>
