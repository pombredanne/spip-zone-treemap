<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/comments/comments-200/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_forum_longueur' => 'Longueur des messages',
	'cfg_forum_longueur_maxi' => 'Longueur maximale',
	'cfg_forum_longueur_mini' => 'Longueur minimale',
	'comment' => 'commentaire',
	'comments' => 'commentaires',
	'comments_h' => 'Vos commentaires',

	// D
	'date_heure_a' => '&agrave;',
	'date_jour_le' => 'Le',

	// F
	'forum_qui_etes_vous' => 'Qui &ecirc;tes-vous&nbsp;?',

	// L
	'label_email' => 'Courriel (non&nbsp;publi&eacute;)',
	'label_nom' => 'Nom',
	'label_url' => 'Votre site web',

	// M
	'moderation_info' => 'Attention, votre message n\'appara&icirc;tra qu\'apr&egrave;s avoir &eacute;t&eacute; relu et approuv&eacute;.',

	// P
	'permalink_to' => 'Lien permanent vers le commentaire',

	// R
	'reponse_comment_modere' => 'Votre commentaire a bien &eacute;t&eacute; enregistr&eacute; et est en attente de relecture avant publication.',
	'reponse_comment_ok' => 'Merci pour votre commentaire&nbsp;!',

	// S
	'saisie_texte_info' => 'Ce formulaire accepte les raccourcis&nbsp;SPIP <code>[-&gt;url] {{gras}} {italique} &lt;quote&gt; &lt;code&gt;</code> et le code&nbsp;HTML <code>&lt;q&gt; &lt;del&gt; &lt;ins&gt;</code>. Pour cr&eacute;er des paragraphes, laissez simplement des lignes vides.',
	'saisie_texte_legend' => 'Ajoutez votre commentaire ici',
	'submit1' => 'Pr&eacute;visualiser',
	'submit2' => 'Confirmer l\'envoi',

	// T
	'titre_comments' => 'Comments'
);

?>
