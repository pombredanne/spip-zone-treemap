<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_forum_longueur' => 'Tamanho das mensagens',
	'cfg_forum_longueur_maxi' => 'Comprimento m&aacute;ximo',
	'cfg_forum_longueur_mini' => 'Comprimento m&iacute;nimo',
	'comment' => 'coment&aacute;rio',
	'comments' => 'coment&aacute;rios',
	'comments_h' => 'Seus coment&aacute;rios',

	// D
	'date_heure_a' => 'em',
	'date_jour_le' => 'No',

	// F
	'forum_qui_etes_vous' => 'Quem &eacute; voc&ecirc;?',

	// L
	'label_email' => 'Email (n&atilde;o publicado)',
	'label_nom' => 'Nome',
	'label_url' => 'Seu website',

	// M
	'moderation_info' => 'Aten&ccedil;&atilde;o, a mensagem s&oacute; ser&aacute; exibida depois de ter sido verificado e aprovado.',

	// P
	'permalink_to' => 'Link permanente para o coment&aacute;rio',

	// R
	'reponse_comment_modere' => 'Seu coment&aacute;rio foi salvado e est&aacute; aguardando revis&atilde;o antes de ser publicado.',
	'reponse_comment_ok' => 'Obrigado pelo seu coment&aacute;rio!',

	// S
	'saisie_texte_info' => 'Este formul&aacute;rio aceita SPIP atajos, [-&gt;url] {{negrita}} {cursiva} &lt;quote&gt; &lt;code&gt; e o c&oacute;digo HTML. Para criar par&aacute;grafos, basta deixar linhas em branco.',
	'saisie_texte_legend' => 'Deixe seu coment&aacute;rio aqu&iacute;',
	'submit1' => 'Visualiza&ccedil;&atilde;o',
	'submit2' => 'Submeter',

	// T
	'titre_comments' => 'Comments'
);

?>
