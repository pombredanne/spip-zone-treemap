<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_forum_longueur' => 'Longitud de los mensajes',
	'cfg_forum_longueur_maxi' => 'Longitud m&aacute;xima',
	'cfg_forum_longueur_mini' => 'Longitud m&iacute;nima',
	'comment' => 'comentario',
	'comments' => 'comentarios',
	'comments_h' => 'Tus comentarios',

	// D
	'date_heure_a' => 'a',
	'date_jour_le' => 'El',

	// F
	'forum_qui_etes_vous' => '&iquest;Qui&eacute;n eres?',

	// L
	'label_email' => 'Email (no publicado)',
	'label_nom' => 'Nombre',
	'label_url' => 'Su sitio web',

	// M
	'moderation_info' => 'Aviso, su mensaje s&oacute;lo ser&aacute; mostrado despu&eacute;s de haber sido rele&iacute;do y aprobado.',

	// P
	'permalink_to' => 'Enlace permanente al comentario',

	// R
	'reponse_comment_modere' => 'Su comentario ha sido guardado y est&aacute; a la espera de ser comprobado antes de que sea publicado.',
	'reponse_comment_ok' => '&iexcl;Gracias por su comentario!',

	// S
	'saisie_texte_info' => 'Este formulario acepta los atajos de SPIP, [-&gt;url] {{negrita}} {cursiva} &lt;quote&gt; &lt;code&gt;  y el c&oacute;digo HTML. Para crear p&aacute;rrafos, dejen simplemente unas l&iacute;neas vac&iacute;as.',
	'saisie_texte_legend' => 'A&ntilde;ade tu comentario aqu&iacute;',
	'submit1' => 'Previsualizaci&oacute;n',
	'submit2' => 'Confirmar el envio',

	// T
	'titre_comments' => 'Comments'
);

?>
