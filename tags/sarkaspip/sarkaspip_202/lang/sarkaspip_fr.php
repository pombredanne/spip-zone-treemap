<?php

// Ceci est un fichier langue de SPIP pour le squelette Sarka-SPIP (http://sarka-spip.com/)
// Les cha�nes de caract�res sont regroup�es suivant le fichier qui les utilise le plus

	
$GLOBALS[$GLOBALS['idx_lang']] = array(

	
//#
	  
		'0_article' => 'Aucun article',
		'0_auteur' => 'Aucun auteur',
		'0_breve' => 'Aucune br&egrave;ve',
		'0_site_web' => 'Aucun site',
		'1_article' => 'Article',
		'1_auteur' => 'Auteur',
		'1_breve' => 'Br&egrave;ve',
		'1_site_web' => 'Site Web',
	  
	
//A

		'acceder_espace_membre' => 'Se connecter comme membre du site',
		'acceder_espace_prive' => 'Acc&eacute;der &agrave l\'espace priv&eacute;',
		'accueil_site' => 'Accueil du site', // redefinition item SPIP
		'agenda' => 'Agenda',
		'agenda_annuel' => 'Agenda annuel',
		'agenda_mois_vide' => 'Aucun &eacute;v&egrave;nement pr&eacute;vu ce mois-ci',
		'agenda_fin_mois_vide' => 'Aucun &eacute;v&egrave;nement &agrave; venir d\'ici la fin du mois',
		'agenda_fin_mois_depasse' => 'Mois ant&eacute;rieur &agrave; la date courante. Aucun &eacute;v&egrave;nement ne peut &ecirc; affich&eacute;',
		'agenda_saisonnier' => 'Agenda saisonnier',
		'album_nav_debut' => 'Premi&egrave;re page',
		'album_nav_precedent' => 'Page pr&eacute;c&eacute;dente',
		'album_nav_suivant' => 'Page suivante',
		'album_nav_fin' => 'Derni&egrave;re page',
		'album_repondre'=> 'Commenter l\'album',
		'album_une'=> 'Top Album',
		'albums_galerie' => 'Galerie',
		'annonces' => 'Annonces',
		'aout' => 'Ao&ucirc;t',
		'articles' => 'Articles', // redefinition item SPIP
		'articles_aleatoires' => 'Articles au hasard',
		'articles_auteur' => 'Articles de cet auteur', // redefinition item SPIP
		'articles_mot' => 'Articles associ&eacute;s',
		'articles_recents' => 'Articles les plus r&eacute;cents', // redefinition item SPIP
		'article_rubrique' => 'Articles publi&eacute;s dans cette rubrique',
		'articles_rubrique' => 'Articles de cette rubrique', // redefinition item SPIP
		'aucun_message' => 'Aucun message n\'a encore &eacute;t&eacute; publi&eacute; en r&eacute;action aux articles de ce site.',
		'aucun_article' => 'Il n\'y a pas d\'article &agrave; cette adresse', // redefinition item SPIP
		'aujourdhui' => 'aujourd\'hui',
		'auteur' => 'Auteur',
		'auteurs' => 'Auteurs',
		'avril' => 'Avril',
	  
	
//B
	  
		'bouton_referencement' => 'Proposition de site',
		'breves' => 'Br&egrave;ves', // redefinition item SPIP
		'breves_mot' => 'Br&egrave;ves associ&eacute;es',
	  
	
//C
	  
		'commentaire' => 'Commentaire',
		'contact' => 'Contact',
		'contacter_site' => 'Contacter le webmestre',
		'coordonnees' => 'Coordonn&eacute;es',
		'copyright_site' => '&copy;',
	  
	
//D
	  
		'date' => 'Date', // redefinition item SPIP
		'date_aujourdhui' => 'aujourd\'hui', // redefinition item SPIP
		'decembre' => 'Decembre',
		'depuis_debut' => 'depuis le d&eacute;but',
		'dimanche_abrege' => 'Di',
		'docu_article' => 'Documents joints &agrave; cet article',
		'docu_rubrique' => 'Documents joints &agrave; cette rubrique',
	  
	
//E
	  
		'erreur' => 'erreur',
		'espace_prive' => 'Espace priv&eacute;', // redefinition item SPIP
		'evenements_du_jour' => 'Ev&eacute;nements de la journ&eacute;e',
	  
	
//F
	  
		'faitavec' => 'Ce site fonctionne gr&acirc;ce &agrave;', // Plus utilise 
		'faitpar' => 'SarkAFeeK (Toufik), SarkALeoN (Sylvain), SarkARchimeD (Xavier), SarkASmeL (Eric)',
		'fevrier' => 'F&eacute;vrier',
		'fil_discussion' => 'Fil de discussion',
		'filtrage_categorie' => 'Cat&eacute;gories',
		'filtre_tout' => 'Aucun filtre',
		'filtre_vide' => 'Sans cat&eacute;gorie',
		'forum' => 'Forum', // redefinition item SPIP
	  
	
//G
	  
		'galerie' => 'Galerie',
	  
	
//H
	  
		'hier' => 'hier',
	  
	
//I
	  
		'icone_sites_references' => 'Sites r&eacute;f&eacute;renc&eacute;s', // redefinition item SPIP
		'img_article' => 'Images jointes &agrave; cet article',
		'img_descriptif' => 'Descriptif',
		'img_format' => 'Format',
		'img_poids' => 'Poids',
		'img_rubrique' =>'Images jointes &agrave; cette rubrique',
		'img_taille' => 'Taille',
		'img_titre' => 'Titre',
		'imprimer_article' => 'imprimer l\'article',
		'info_afficher_album' => 'Consulter cet album',
		'info_categorie' => 'Cat&eacute;gorie',
 		'info_sans_categorie' => 'Ev&eacute;nements sans cat&eacute;gorie',
		'info_sans_filtre' => 'Tout &eacute;v&eacute;nement',
                'info_album_categorie' => 'Cat&eacute;gorie',
                'info_album_sans_categorie' => 'Albums sans cat&eacute;gorie',
                'info_album_sans_filtre' => 'Tout album',
		'info_langues' => 'Langues du site', // redefinition item SPIP
		'inscription' => 'Inscription',
		'inscrire_a_newsletter' => 'S\'inscrire &agrave; la lettre d\'information',
		'inscrire_au_site' => 'S\'inscrire sur le site',
		'intitule_referencement' => 'Proposer de r&eacute;f&eacute;rencer un site dans la rubrique',
		'item_choisir_langue' => 'Langue du site (espace priv&eacute;)',
	
//j
	  
		'janvier' => 'Janvier',
		'jeudi_abrege' => 'Je',
		'juin' => 'Juin',
		'juillet' => 'Juillet',

	
//K
	  
		'kezakospip' => 'Site r&eacute;alis&eacute; avec SPIP, un CMS gratuit sous licence GPL',
	  
	
//L
	  
		'licence' => 'Le squelette SARKA-SPIP est distribu&eacute; sous licence GPL', //  plus utilise 
		'licence_site' => 'Licence &agrave; d&eacute;finir',
		'licence_squelette' => '<a href="http://www.gnu.org/copyleft/gpl.html" title="Le squelette SARKA-SPIP est distribu&eacute; sous licence GPL" target="_blank">GPL</a>',
		'lire_suite_article' => 'lire la suite de l\'article',
                'lire_evenement_virtuel' => 'Consulter l\'&eacute;v&eacute;nement d\'origine',
		'lundi_abrege' => 'Lu',
		'login_membre' => 'Connexion',
		'login_sinscrire' => 's\'inscrire', // redefinition item SPIP
		'login_motpasseoublie' => 'mot&nbsp;de&nbsp;passe&nbsp;oubli&eacute;&nbsp;?', // redefinition item SPIP
		'login_retoursitepublic' => 'retour&nbsp;au&nbsp;site&nbsp;public', // redefinition item SPIP
		'logout_membre' => 'Deconnexion',
		'lxh' => 'par',
	  
	
//M
	  
		'mai' => 'Mai',
		'maj' => 'Derni&egrave;re mise &agrave; jour',
		'mardi_abrege' => 'Ma',
		'mars' => 'Mars',
		'mercredi_abrege' => 'Me',
		'message' => 'Message', // redefinition item SPIP
		'messages_mot' => 'Messages associ&eacute;s',
		'message_bonjour' => 'Bonjour, cher(&egrave;re)',
		'messages_forum' => 'Messages de forum', // redefinition item SPIP
		'messages_recents' => 'Messages de forums les plus r&eacute;cents', // redefinition item SPIP
		'mois_precedent' => 'Mois pr&eacute;c&eacute;dent',
		'mois_suivant' => 'Mois suivant',
		'mots_clefs' => 'Mots-cl&eacute;s', // redefinition item SPIP
		'msg_0_article_publie' => 'Aucun article publi&eacute; pour l\'instant',
		'msg_0_evt_agenda' => 'Aucun &eacute;v&eacute;nement d&eacute;fini dans l\'agenda',
		'msg_0_evt_annee' => 'Aucun &eacute;v&eacute;nement de cette cat&eacute;gorie pour l\'ann&eacute;e',
		'msg_0_evt_saison' => 'Aucun &eacute;v&eacute;nement de cette cat&eacute;gorie pour la saison',
		'msg_0_album_galerie' => 'Aucun album disponible pour l\'instant dans la galerie',
		'msg_0_album_filtre_annee' => 'Aucun album de cette cat&eacute;gorie pour l\'ann&eacute;e',
		'msg_0_album_filtre_saison' => 'Aucun album de cette cat&eacute;gorie pour la saison',
		'msg_0_album_filtre_planche' => 'Aucun album de cette cat&eacute;gorie disponible',
		'msg_evenement_virtuel' => 'Le texte de cet &eacute;v&eacute;nement n\'est pas accessible directement sur ce site. Vous pouvez tout de m&ecirc;me consulter la description d\'origine en cliquant sur le lien ci-dessous',
	
//N
	  
		'newsletter' => 'Lettre d\'information',
		'nom' => 'Nom', // redefinition item SPIP
		'novembre' => 'Novembre',
	  
	
//O
	  
		'objets_mot' => 'Objets associ&eacute;s au mot-cl&eacute;',
		'octobre' => 'Octobre',
	  
	
//P
	  
		'page_non_trouvee' => 'Page non trouv&eacute;e',
		'par_auteur' => 'par ', // redefinition item SPIP
		'plan_site' => 'Plan du site', // redefinition item SPIP
		'plusieurs_evenements_jour' => '&eacute;v&eacute;nements dans cette journ&eacute;e',
		'popularite_article' => 'popularit&eacute;',
		'poster_message' => 'Poster un message', // redefinition item SPIP
		'ps' => 'Post Scriptum',
		'publie_sur_site' => 'Publi&eacute; sur le site',
	  
	
//Q
	  
	  
	
//R
	  
		'realisation_squelette' => 'Collectif Sarka-SPIP',
		'recents_syndic' => 'Derni&egrave;res nouvelles du web',
		'recherche_articles_resultat' => 'Articles trouv&eacute;s',
		'recherche_messages_resultat' => 'Messages trouv&eacute;s dans les forums',
		'recherche_mots_resultat' => 'Mots-cl&eacute;s trouv&eacute;s et articles associ&eacute;s',
		'recherche_rubriques_resultat' => 'Rubriques trouv&eacute;es',
		'recherche_breves_resultat' => 'Br&egrave;ves trouv&eacute;es',
		'recherche_sites_resultat' => 'Sites web trouv&eacute;s',
		'rechercher' => 'Rechercher',
		'repondre_article' => 'R&eacute;pondre &agrave; cet article', // redefinition item SPIP
		'repondre_message' => 'R&eacute;pondre &agrave; ce message', // redefinition item SPIP
		'reponse_suite_article' => 'Votre r&eacute;ponse fait suite &agrave; l\'article',
		'reponse_suite_message' => 'En r&eacute;ponse au message',
		'requete' => 'Saisir le(s) mot(s)',
		'resultats_recherche' => 'R&eacute;sultats de la recherche', // redefinition item SPIP
		'retour_article' => 'Retourner &agrave; l\'article',
		'rubriques_mot' => 'Rubriques associ&eacute;es',

	
//S
	  
		'samedi_abrege' => 'Sa',
		'septembre' => 'Septembre',
		'signatures_petition' => 'Signatures', // redefinition item SPIP
		'signer_petition' => 'Signer la p&eacute;tition',
		'site_realise_avec_spip' => 'Site r&eacute;alis&eacute; avec SPIP', // redefinition item SPIP
		'sites_mot' => 'Sites web associ&eacute;s',
		'site_web' => 'Site web',
		'sites_web' => 'Sites Web', // redefinition item SPIP
		'squelette' => 'Ce site utilise le squelette SARKA-SPIP',
		'statistiques_edition' => 'Statistiques &eacute;ditoriales',
		'statistiques_visites' => 'Statistiques des visites',
		'statut_0minirezo' => 'administrateur(trice)',
		'statut_1comite' => 'r&eacute;dacteur(trice)',
		'statut_6forum' => 'visiteur(euse)',
		'sur_web' => 'Sur le Web', // redefinition item SPIP
		'syndiquer_rubrique' => 'Syndiquer cette rubrique', // redefinition item SPIP
		'syndiquer_site' => 'Syndiquer tout le site', // redefinition item SPIP

	
//T
	  
		'top_articles' => 'Articles populaires',
		'toutes_les_breves' => 'Toutes les br&egrave;ves du site',
		'tri_par_auteur' => 'Par auteur',
		'tri_par_date' => 'Par date',
		'tri_par_rubrique' => 'Par rubrique',
	  
	
//U
	  
		'url_fausse' => 'L\'adresse URL que vous demandez n\'existe pas. Le plan du site peut vous aider &agrave; trouver l\'information recherch&eacute;e.',
	  
	
//V
	  
		'vendredi_abrege' => 'Ve',
		'votre_reponse' => 'Votre r&eacute;ponse',
	  
	
//W
	  
		'web_france' => 'Web francophone',
		'web_monde' => 'Web Mondial',
	  
	
//X
	  
	  
	
//Y
	  
	  
	
//Z
	
        

	
);

?>