<?php

// This is a SPIP language file for the squelette named Sarka-SPIP (http://sarka-spip.com/)
// The character strings are gathered according to the file which uses them more

	
$GLOBALS[$GLOBALS['idx_lang']] = array(


//#
	  
		'0_article' => 'No article',
		'0_auteur' => 'No auteur',
		'0_breve' => 'No news item',
		'0_site_web' => 'No site',
		'1_article' => 'Article',
		'1_auteur' => 'Author',
		'1_breve' => 'News item',
		'1_site_web' => 'Web site',
	  
	
//A
	  
		'acceder_espace_membre' => 'Login as site member',
		'acceder_espace_prive' => 'Login to the private area',
		'accueil_site' => 'Home page', // redefinition item SPIP
		'agenda' => 'Diary',
		'agenda_annuel' => 'Yearly diary',
		'agenda_mois_vide' => 'No event scheduled for this month',
		'agenda_fin_mois_vide' => 'No more event scheduled till the end of the month',
		'agenda_fin_mois_depasse' => 'Current date subsequent to this month. No event can be listed',
		'agenda_saisonnier' => 'Seasonal diary',
		'album_nav_debut' => 'First page',
		'album_nav_precedent' => 'Previous page',
		'album_nav_suivant' => 'Next page',
		'album_nav_fin' => 'Last page',
		'album_repondre'=> 'Comment on this album',
		'album_une'=> 'Top Album',
		'albums_galerie' => 'Gallery',
		'annonces' => 'Ads',
		'aout' => 'August',
		'article_rubrique' => 'Documents published in this section',
		'articles' => 'Articles', // redefinition item SPIP
		'articles_aleatoires' => 'Random articles',
		'articles_auteur' => 'This author\'s articles', // redefinition item SPIP
		'articles_mot' => 'Articles associated',
		'articles_recents' => 'The most recent articles', // redefinition item SPIP
		'articles_rubrique' => 'This section\'s articles', // redefinition item SPIP
		'aucun_article' => 'There is no article at this address', // redefinition item SPIP
		'aucun_message' => 'No message has yet been published in reaction to the articles on this site.',
		'aujourdhui' => 'today',
		'auteur' => 'Author',
		'auteurs' => 'Authors',
		'avril' => 'April',
	  
	
//B
	  
		'breves' => 'News items', // redefinition item SPIP
		'bouton_referencement' => 'Website\'s suggestion',
		'breves_mot' => 'News associated',
	  
	
//C
	  
		'commentaire' => 'Comment',
		'contact' => 'Contact',
		'contacter_site' => 'Contact webmaster',
		'coordonnees' => 'Details',
		'copyright_site' => '&copy;',
	  
	
//D
	  
		'date' => 'Date', // redefinition item SPIP
		'date_aujourdhui' => 'today', // redefinition item SPIP
		'decembre' => 'December',
		'depuis_debut' => 'from the beginning',
		'dimanche_abrege' => 'Su',
		'docu_article' => 'Documents associated with this article',
		'docu_rubrique' => 'Documents associated with this section',
	  
	
//E
	  
		'erreur' => 'error',
		'espace_prive' => 'Private area', // redefinition item SPIP
		'evenements_du_jour' => 'Today\'s events',
	  
	
//F
	  
		'faitavec' => 'Powered by', // Unused
		'faitpar' => 'SarkAFeeK (Toufik), SarkALeoN (Sylvain), SarkARchimeD (Xavier), SarkASmeL (Eric)',
		'fevrier' => 'February',
		'fil_discussion' => 'Topic thread',
		'filtrage_categorie' => 'Categories',
		'filtre_tout' => 'Not filtered',
		'filtre_vide' => 'Not categorised',
		'forum' => 'Forum', // redefinition item SPIP
	  
	
//G
	  
		'galerie' => 'Gallery',
	  
	
//H
	  
		'hier' => 'yesterday',
	  
	
//I
	  
		'icone_sites_references' => 'Referenced sites', // redefinition item SPIP
		'img_article' => 'Images associated with this article',
		'img_descriptif' => 'Description',
		'img_format' => 'Filetype',
		'img_poids' => 'Weight',
		'img_rubrique' =>'Images associated with this section',
		'img_taille' => 'Size',
		'img_titre' => 'Title',
		'imprimer_article' => 'Print the article',
		'info_afficher_album' => 'Visualise this album',
                'info_album_categorie' => 'Category',
                'info_album_sans_categorie' => 'Albums not categorised',
                'info_album_sans_filtre' => 'All albums',
		'info_categorie' => 'Category',
                'info_langues' => 'Site\'s languages', // redefinition item SPIP
		'info_sans_categorie' => 'Events not categorised',
		'info_sans_filtre' => 'All events',
		'inscription' => 'Registration',
		'inscrire_a_newsletter' => 'Subscribe to the newsletter',
		'inscrire_au_site' => 'Register to the website',
		'intitule_referencement' => 'Suggest a new website to be referenced in section',
		'item_choisir_langue' => 'Site language (for private area)',
	  
	
//j
	  
		'janvier' => 'January',
		'jeudi_abrege' => 'Th',
		'juillet' => 'July',
		'juin' => 'June',
	  
	
//K
	  
		'kezakospip' => 'Site powered by SPIP, a free CMS under GPL',
	  
	
//L
	  
		'licence' => 'SARKA-SPIP template is distributed under GPL', // utilis�
		'licence_site' => 'Licence to be defined',
		'licence_squelette' => '<a href="http://www.gnu.org/copyleft/gpl.html" title="SARKA-SPIP template is distributed under GPL" target="_blank">GPL</a>',
                'lire_evenement_virtuel' => 'Read the original event',
		'lire_suite_article' => 'Read the whole article',
		'login_membre' => 'Member\'s login',
		'login_motpasseoublie' => 'password forgotten?', // redefinition item SPIP
		'login_retoursitepublic' => 'back to the public site', // redefinition item SPIP
		'login_sinscrire' => 'register', // redefinition item SPIP
		'logout_membre' => 'Member\'s logout',
		'lundi_abrege' => 'Mo',
		'lxh' => 'by',
	  
	
//M
	  
		'mai' => 'May',
		'maj' => 'Latest update',
		'mardi_abrege' => 'Tu',
		'mars' => 'March',
		'mercredi_abrege' => 'We',
		'message' => 'Message', // redefinition item SPIP
		'message_bonjour' => 'Hello, dear',
		'messages_forum' => 'Forum messages', // redefinition item SPIP
		'messages_mot' => 'Messages associated',
		'messages_recents' => 'Most recent forum messages', // redefinition item SPIP
		'mois_precedent' => 'Previous month',
		'mois_suivant' => 'Next month',
		'mots_clefs' => 'Keywords', // redefinition item SPIP
		'msg_0_article_publie' => 'No article published, yet',
		'msg_0_evt_agenda' => 'No event defined in the diary, yet',
		'msg_0_evt_annee' => 'No event belonging to this category for the year',
		'msg_0_evt_saison' => 'No event belonging to this category for the season',
		'msg_0_album_galerie' => 'No album defined in the gallery, yet',
		'msg_0_album_filtre_annee' => 'No album belonging to this category for the year',
		'msg_0_album_filtre_saison' => 'No album belonging to this category for the season',
		'msg_0_album_filtre_planche' => 'No album belonging to this category',
		'msg_evenement_virtuel' => 'Text of this event is not directly accessible on this site. Nevertheless, you can read the original description by clicking on the link below.',
	  
	
//N
	  
		'newsletter' => 'Newsletter',
		'nom' => 'Name', // redefinition item SPIP
		'novembre' => 'November',
	  
	
//O
	  
		'objets_mot' => 'Objects associated with the keyword',
		'octobre' => 'October',
	  
	
//P
	  
		'page_non_trouvee' => 'Page not found',
		'par_auteur' => 'by ', // redefinition item SPIP
		'plan_site' => 'Site Map', // redefinition item SPIP
		'plusieurs_evenements_jour' => 'events during this day',
		'popularite_article' => 'popularity',
		'poster_message' => 'To post a message', // redefinition item SPIP
		'ps' => 'Post Scriptum',
		'publie_sur_site' => 'Published on this site',
	  
	
//Q
	  
	  
	
//R
	  
		'realisation_squelette' => 'Collectif Sarka-SPIP',
		'recents_syndic' => 'Latest news from the web',
		'recherche_articles_resultat' => 'Found articles',
		'recherche_messages_resultat' => 'Found messages in forums',
		'recherche_mots_resultat' => 'Found keywords and associated keywords',
		'recherche_rubriques_resultat' => 'Found sections',
		'recherche_breves_resultat' => 'Found news items',
		'recherche_sites_resultat' => 'Found sites',
		'rechercher' => 'Search',
		'repondre_article' => 'Reply to this article', // redefinition item SPIP
		'repondre_message' => 'Reply to this message', // redefinition item SPIP
		'reponse_suite_article' => 'Your response to the article',
		'reponse_suite_message' => 'In response to the message',
		'requete' => 'Enter the keyword(s)',
		'resultats_recherche' => 'Search results', // redefinition item SPIP
		'retour_article' => 'Back to the article',
		'rubriques_mot' => 'Sections associated',

	
//S
	  
		'samedi_abrege' => 'Sa',
		'septembre' => 'September',
		'signatures_petition' => 'Signatures', // redefinition item SPIP
		'signer_petition' => 'Sign up the petition',
		'site_realise_avec_spip' => 'Site created with SPIP', // redefinition item SPIP
		'site_web' => 'Website',
		'sites_mot' => 'Sites associated',
		'sites_web' => 'Web sites', // redefinition item SPIP
		'squelette' => 'Website using SARKA-SPIP template',
		'statistiques_edition' => 'Publication statistics',
		'statistiques_visites' => 'Visits statistics',
		'statut_0minirezo' => 'administrator',
		'statut_1comite' => 'editor',
		'statut_6forum' => 'visitor',
		'sur_web' => 'On the Web', // redefinition item SPIP
		'syndiquer_rubrique' => 'Syndicate this section', // redefinition item SPIP
		'syndiquer_site' => 'Syndicate the whole site', // redefinition item SPIP

	
//T
	  
		'top_articles' => 'Popular articles',
		'toutes_les_breves' => 'All the news',
		'tri_par_auteur' => 'By author',
		'tri_par_date' => 'By date',
		'tri_par_rubrique' => 'By section',
	  
	
//U
	  
		'url_fausse' => 'The requested URL was not found. The site map can help you to find the requested information.',
	  
	
//V
	  
		'vendredi_abrege' => 'Fr',
		'votre_reponse' => 'Your response',
	  
	
//W
	  
		'web_france' => 'French Wide Web',
		'web_monde' => 'World Wide Web',
	  
	
//X
	  
	  
	
//Y
	  
	  
	
//Z
	
        
		

	
);

?>