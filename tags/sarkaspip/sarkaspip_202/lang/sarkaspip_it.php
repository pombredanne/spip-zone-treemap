<?php

// Ceci est un fichier langue de SPIP pour le squelette Sarka-SPIP (http://sarka-spip.com/)
// Les chaînes de caractères sont regroupées suivant le fichier qui les utilise le plus

	
$GLOBALS[$GLOBALS['idx_lang']] = array(

	
//#
	  
		'0_article' => 'Nessun articolo',
		'0_auteur' => 'Nessun autore',
		'0_breve' => 'Nessuna breve',
		'0_site_web' => 'Nessun sito',
		'1_article' => 'Articolo',
		'1_auteur' => 'Autore',
		'1_breve' => 'Breve',
		'1_site_web' => 'Sito Web',
	  
	
//A

		'acceder_espace_membre' => 'Connettersi come membro del sito',
		'acceder_espace_prive' => 'Accedere allo spazio privato',
		'accueil_site' => 'Home', // redefinition item SPIP
		'agenda' => 'Agenda',
		'agenda_annuel' => 'Agenda annuale',
		'agenda_mois_vide' => 'Nessun evento previsto in agenda per questo mese',
		'agenda_fin_mois_vide' => 'Nessun evento previsto fino alla fine di questo mese',
		'agenda_fin_mois_depasse' => 'Mese precedente alla data attuale. Nessun evento pu$ograve; essere mostrato',
		'agenda_saisonnier' => 'Agenda stagionale',
		'album_nav_debut' => 'Prima pagina',
		'album_nav_precedent' => 'Pagina precedente',
		'album_nav_suivant' => 'Pagine successiva',
		'album_nav_fin' => 'Ultima pagina',
		'album_repondre'=> 'Commenta l\'album',
		'album_une'=> 'Top Album',
		'albums_galerie' => 'Galleria',
		'annonces' => 'Annunci',
		'aout' => 'Agosto',
		'articles' => 'Articoli', // redefinition item SPIP
		'articles_aleatoires' => 'Articoli a caso',
		'articles_auteur' => 'Articoli di questo autore', // redefinition item SPIP
		'articles_mot' => 'Articoli associati',
		'articles_recents' => 'Articoli recenti', // redefinition item SPIP
		'article_rubrique' => 'Articoli pubblicati in questa rubrica',
		'articles_rubrique' => 'Articoli di questa rubrica', // redefinition item SPIP
		'aucun_message' => 'Nessun messaggio pubblicato in risposta agli articoli di questo sito.',
		'aucun_article' => 'Nessuna articolo a questo indirizzo', // redefinition item SPIP
		'aujourdhui' => 'oggi',
		'auteur' => 'Autore',
		'auteurs' => 'Autori',
		'avril' => 'Aprile',
	  
	
//B
	  
		'bouton_referencement' => 'Proponi un sito',
		'breves' => 'Brevi', // redefinition item SPIP
		'breves_mot' => 'Brevi associate',
	  
	
//C
	  
		'commentaire' => 'Commento',
		'contact' => 'Contatti',
		'contacter_site' => 'Contatta il webmaster',
		'coordonnees' => 'Dettagli',
		'copyright_site' => '&copy;',
	  
	
//D
	  
		'date' => 'Data', // redefinition item SPIP
		'date_aujourdhui' => 'oggi', // redefinition item SPIP
		'decembre' => 'Dicembre',
		'depuis_debut' => 'dall\'inizio',
		'dimanche_abrege' => 'Do',
		'docu_article' => 'Documenti allegati a questo articolo',
		'docu_rubrique' => 'Documenti allegati a questa rubrica',
	  
	
//E
	  
		'erreur' => 'errore',
		'espace_prive' => 'Redazione', // redefinition item SPIP
		'evenements_du_jour' => 'Eventi del giorno',
	  
	
//F
	  
		'faitavec' => 'Questo sito funziona grazie a', // Plus utilise 
		'faitpar' => 'SarkAFeeK (Toufik), SarkALeoN (Sylvain), SarkARchimeD (Xavier), SarkASmeL (Eric)',
		'fevrier' => 'Febbraio',
		'fil_discussion' => 'Thread di discussione',
		'filtrage_categorie' => 'Categorie',
		'filtre_tout' => 'Nessun filtro',
		'filtre_vide' => 'Senza categorie',
		'forum' => 'Forum', // redefinition item SPIP
	  
	
//G
	  
		'galerie' => 'Galleria',
	  
	
//H
	  
		'hier' => 'ieri',
	  
	
//I
	  
		'icone_sites_references' => 'Siti referenziati', // redefinition item SPIP
		'img_article' => 'Immagini allegate a questo articolo',
		'img_descriptif' => 'Descrizione',
		'img_format' => 'Formato',
		'img_poids' => 'Peso',
		'img_rubrique' =>'Immagini allegate a questa rubrica',
		'img_taille' => 'Dimensioni',
		'img_titre' => 'Titolo',
		'imprimer_article' => 'stampa l\'articolo',
		'info_afficher_album' => 'Vedi quest\'album',
		'info_categorie' => 'Categoria',
		'info_sans_categorie' => 'Eventi senza categoria',
		'info_sans_filtre' => 'Tutti gli eventi',
                'info_album_categorie' => 'Categoria',
                'info_album_sans_categorie' => 'Albums senza categoria',
                'info_album_sans_filtre' => 'Tutti gli album',
		'info_langues' => 'Lingue del sito', // redefinition item SPIP
		'inscription' => 'Iscrizione',
		'inscrire_a_newsletter' => 'Iscriversi alla newsletter',
		'inscrire_au_site' => 'Iscriversi al sito',
		'intitule_referencement' => 'Proponi un sito da referenziare in questa rubrica',
		'item_choisir_langue' => 'Lingua del sito (spazio privato)',
	
//j
	  
		'janvier' => 'Gennaio',
		'jeudi_abrege' => 'Gi',
		'juin' => 'Giugno',
		'juillet' => 'Luglio',

	
//K
	  
		'kezakospip' => 'Sito realizzato con SPIP, un CMS gratuito sotto licenza GPL',
	  
	
//L
	  
		'licence' => 'Lo squelette SARKA-SPIP &egrave; distribuito sotto licenza GPL', //  plus utilise 
		'licence_site' => 'Licenza da definire',
		'licence_squelette' => '<a href="http://www.gnu.org/copyleft/gpl.html" title="Lo squelette SARKA-SPIP &egrave; distribuito sotto licenza GPL" target="_blank">GPL</a>',
		'lire_suite_article' => 'leggi il seguito dell\'articolo',
                'lire_evenement_virtuel' => 'Consulta l\'evento originale',
		'lundi_abrege' => 'Lu',
		'login_membre' => 'Connessione',
		'login_sinscrire' => 'iscriviti', // redefinition item SPIP
		'login_motpasseoublie' => 'password&nbsp;dimenticata;&nbsp;?', // redefinition item SPIP
		'login_retoursitepublic' => 'torna&nbsp;al&nbsp;sito&nbsp;pubblico', // redefinition item SPIP
		'logout_membre' => 'Disconnessione',
		'lxh' => 'di',
	  
	
//M
	  
		'mai' => 'Maggio',
		'maj' => 'Ultimo aggiornamento',
		'mardi_abrege' => 'Ma',
		'mars' => 'Marzo',
		'mercredi_abrege' => 'Me',
		'message' => 'Messaggio', // redefinition item SPIP
		'messages_mot' => 'Messaggi associati',
		'message_bonjour' => 'Buongiorno, caro(cara)',
		'messages_forum' => 'Messaggi del forum', // redefinition item SPIP
		'messages_recents' => 'Messaggi dei forum pi&ugrave; recenti', // redefinition item SPIP
		'mois_precedent' => 'Mese precedente',
		'mois_suivant' => 'Mois successivo',
		'mots_clefs' => 'Parole chiave', // redefinition item SPIP
		'msg_0_article_publie' => 'Nessuna articolo pubblicato fino a ora',
		'msg_0_evt_agenda' => 'Nessun evento definito in agenda',
		'msg_0_evt_annee' => 'Nessun evento per l\'anno in questa categoria',
		'msg_0_evt_saison' => 'Nessun evento per la stagione in questa categoria',
		'msg_0_album_galerie' => 'Nessun album disponibile al momento in galleria',
		'msg_0_album_filtre_annee' => 'Nessun album per l\'anno in questa categoria',
		'msg_0_album_filtre_saison' => 'Nessun album per la stagione in questa categoria',
		'msg_0_album_filtre_planche' => 'Nessun album disponibile in questa categoria',
		'msg_evenement_virtuel' => 'Il testo di questo evento non &egrave; direttamente accessibile su questo sito. Puoi comunque consultare la descrizione originale cliccando sul seguente collegamento',
	
//N
	  
		'newsletter' => 'Newsletter',
		'nom' => 'Nome', // redefinition item SPIP
		'novembre' => 'Novembre',
	  
	
//O
	  
		'objets_mot' => 'Oggetti associati alla parola chiave',
		'octobre' => 'Ottobre',
	  
	
//P
	  
		'page_non_trouvee' => 'Pagina non trovata',
		'par_auteur' => 'di ', // redefinition item SPIP
		'plan_site' => 'Mappa del sito', // redefinition item SPIP
		'plusieurs_evenements_jour' => 'eventi in questo giorno',
		'popularite_article' => 'popolarit&agrave;',
		'poster_message' => 'Inviare un messaggio', // redefinition item SPIP
		'ps' => 'Post Scriptum',
		'publie_sur_site' => 'Pubblicato sul sito',
	  
	
//Q
	  
	  
	
//R
	  
		'realisation_squelette' => 'Collettivo Sarka-SPIP',
		'recents_syndic' => 'Ultime notizie del web',
		'recherche_articles_resultats' => 'Articoli trovati',
		'recherche_messages_resultats' => 'Messaggi trovati nei forum',
		'recherche_mots_resultat' => 'Parole chiave trovate ed articoli associati',
		'recherche_rubrique_resultat' => 'Rubriche trovate',
		'recherche_rubriques_resultats' => 'Brevi trovate',
		'recherche_sites_resultats' => 'Siti web trovati',
		'rechercher' => 'Ricerca',
		'repondre_article' => 'Rispondi a questo articolo', // redefinition item SPIP
		'repondre_message' => 'Rispondi a questo messaggio', // redefinition item SPIP
		'reponse_suite_article' => 'La tua risposta all\'articolo',
		'reponse_suite_message' => 'In risposta al messaggio',
		'requete' => 'Inserisci la/e parola/e',
		'resultats_recherche' => 'Risultati della ricerca', // redefinition item SPIP
		'retour_article' => 'Ritorna all\'articolo',
		'rubriques_mot' => 'Rubriche associate',

	
//S
	  
		'samedi_abrege' => 'Sa',
		'septembre' => 'Settembre',
		'signatures_petition' => 'Firme', // redefinition item SPIP
		'signer_petition' => 'Firma la petizione',
		'site_realise_avec_spip' => 'Sito realizzato con SPIP', // redefinition item SPIP
		'sites_mot' => 'Siti web associati',
		'site_web' => 'Sito web',
		'sites_web' => 'Siti Web', // redefinition item SPIP
		'squelette' => 'Questo sito utilizza lo squelette SARKA-SPIP',
		'statistiques_edition' => 'Statistiche editoriali',
		'statistiques_visites' => 'Statistiche delle visite',
		'statut_0minirezo' => 'amministratore(trice)',
		'statut_1comite' => 'redattore(trice)',
		'statut_6forum' => 'visitatore(trice)',
		'sur_web' => 'Sul Web', // redefinition item SPIP
		'syndiquer_rubrique' => 'Metti in in syndication questa rubrica', // redefinition item SPIP
		'syndiquer_site' => 'Metti in syndication tutto il sito', // redefinition item SPIP

	
//T
	  
		'top_articles' => 'Articoli popolari',
		'toutes_les_breves' => 'Tutte le brevi del sito',
		'tri_par_auteur' => 'Per autore',
		'tri_par_date' => 'Per data',
		'tri_par_rubrique' => 'Per rubrica',
	  
	
//U
	  
		'url_fausse' => 'L\'indirizzo URL richiesto non esiste. La mappa del sito pu&ograve; aiutare a trovare l\'informazione cercata.',
	  
	
//V
	  
		'vendredi_abrege' => 'Ve',
		'votre_reponse' => 'La tua risposta',
	  
	
//W
	  
		'web_france' => 'Web francofono',
		'web_monde' => 'Web Mondiale',
	  
	
//X
	  
	  
	
//Y
	  
	  
	
//Z
	
        
		

	
);

?>
