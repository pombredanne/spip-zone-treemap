<?php

// Este es un fichero idioma de SPIP para el esqueleto Sarka-SPIP (http://sarka-spip.com/)
// Las cadenas de caracteres estan agrupadas segun el fichero que las utiliza mas

	
$GLOBALS[$GLOBALS['idx_lang']] = array(

	
//#
	  
		'0_article' => 'Ning&uacute;n art&iacute;culo',
		'0_auteur' => 'Ning&uacute;n autor',
		'0_breve' => 'Ninguna breve',
		'0_site_web' => 'Ning&uacute;n sitio web',
		'1_article' => 'Art&iacute;culo',
		'1_auteur' => 'Autor',
		'1_breve' => 'Breve',
		'1_site_web' => 'Sitio Web',
	  
	
//A
	  
		'accueil_site' => 'Portada del sitio', // redefinition item SPIP
		'acceder_espace_membre' => 'Conectarse como miembro del sitio',
		'acceder_espace_prive' => 'Accedar al espacio privado',
		'agenda' => 'Agenda',
		'agenda_annuel' => 'Agenda anual',
		'agenda_mois_vide' => 'Ning&uacute;n evento previsto para el mes',
		'agenda_fin_mois_vide' => 'Ning&uacute;n evento previsto hasta el fin del mes',
		'agenda_fin_mois_depasse' => 'Mes anterior al dia de hoy. Ning&uacute;n evento puede ser visualizado',
		'agenda_saisonnier' => 'Agenda de temporada',
		'album_nav_debut' => 'Primera p&aacute;gina',
		'album_nav_precedent' => 'P&aacute;gina anterior',
		'album_nav_suivant' => 'P&aacute;gina siguiente',
		'album_nav_fin' => '&Ugrave;ltima p&aacute;gina',
		'album_repondre'=> 'Commentar el album',
		'album_une'=> 'Top Album ',
		'albums_galerie' => 'Galer&iacute;a',
		'annonces' => 'Anuncios',
		'aout' => 'Agosto',
		'article_rubrique' => 'Art&iacute;culos publicados en esta secci&oacute;n',
		'articles' => 'Art&iacute;culos', // redefinition item SPIP
		'articles_aleatoires' => 'Art&iacute;culos al azar',
		'articles_auteur' => 'Art&iacute;culos de esta autora o autor', // redefinition item SPIP
		'articles_mot' => 'Art&iacute;culos relacionados',
		'articles_recents' => 'Los art&iacute;culos m&aacute;s recientes', // redefinition item SPIP
		'articles_rubrique' => 'Art&iacute;culos de esta secci&oacute;n', // redefinition item SPIP
		'aucun_article' => 'No hay ning&uacute;n art&iacute;culo en esta direcci&oacute;n ', // redefinition item SPIP
		'aucun_message' => 'A&uacute;n no se han publicado mensajes en respuesta a los art&iacute;culos de este sitio.',
		'aujourdhui' => 'hoy',
		'auteur' => 'Autor',
		'auteurs' => 'Autors',
		'avril' => 'Abril',
	  
	
//B
	  
		'bouton_referencement' => 'Propuesta de sitio',
		'breves' => 'Breves', // redefinition item SPIP
		'breves_mot' => 'Breves relacionadas',
	  
	
//C
	  
		'commentaire' => 'Comentario',
		'contact' => 'Contacto',
		'contacter_site' => 'Contactar el webmestre',
		'coordonnees' => 'Se&ntilde;as',
		'copyright_site' => '&copy;',
	  
	
//D
	  
		'date' => 'Fecha', // redefinition item SPIP
		'date_aujourdhui' => 'hoy', // redefinition item SPIP
		'decembre' => 'Deciembre',
		'depuis_debut' => 'desde el principio',
		'dimanche_abrege' => 'Do',
		'docu_article' => 'Documentos adjuntos a este art&iacute;culo',
		'docu_rubrique' => 'Documentos adjuntos a esta secci&oacute;n',
	  
	
//E
	  
		'erreur' => 'error',
		'espace_prive' => 'Espacio privado', // redefinition item SPIP
		'evenements_du_jour' => 'Acontecimientos del d&iacute;a',
	  
	
//F
	  
		'faitavec' => 'Este sitio funciona gracias a', // No mas utilizado
		'faitpar' => 'SarkAFeeK (Toufik), SarkALeoN (Sylvain), SarkARchimeD (Xavier), SarkASmeL (Eric)',
		'fevrier' => 'Febrero',
		'fil_discussion' => 'Hilo de t&oacute;pico',
		'filtrage_categorie' => 'Categor&iacute;as',
		'filtre_tout' => 'Ninguno filtro',
		'filtre_vide' => 'Sin categor&iacute;a',
		'forum' => 'Foro', // redefinition item SPIP
	  
	
//G
	  
		'galerie' => 'Galer&iacute;a',
	  
	
//H
	  
		'hier' => 'ayer',
	  
	
//I
	  
		'icone_sites_references' => 'Sitios referenciados', // redefinition item SPIP
		'img_article' => 'Im&aacute;genes adjuntas a este art&iacute;culo',
		'img_descriptif' => 'Descripci&oacute;n',
		'img_format' => 'Formato',
		'img_poids' => 'Peso',
		'img_rubrique' =>'Im&aacute;genes adjuntas a esta secci&oacute;n',
		'img_taille' => 'Tama&ntilde;o',
		'img_titre' => 'T&iacute;tulo',
		'imprimer_article' => 'imprimir el art&iacute;culo',
		'info_afficher_album' => 'Ver el &aacute;lbum',
                'info_album_categorie' => 'Categor&iacute;a',
                'info_album_sans_categorie' => 'Albums  sin categor&iacute;a',
                'info_album_sans_filtre' => 'Todos los &aacute;lbums',
		'info_categorie' => 'Acontecimientos de la categor&iacute;a',
                'info_langues' => 'Idiomas del sitio', // redefinition item SPIP
		'info_sans_categorie' => 'Acontecimientos sin categor&iacute;a',
		'info_sans_filtre' => 'Todos los acontecimientos',
		'inscription' => 'Inscripci&oacute;n',
		'inscrire_a_newsletter' => 'Inscribirse a las novedades',
		'inscrire_au_site' => 'Inscribirse en este sitio',
		'intitule_referencement' => 'Proponer la referenciaci&oacute;n de un sitio en el secci&oacute;n',
		'item_choisir_langue' => 'Lenguaje del sitio (espacio privado)',
	  
	
//j
	  
		'janvier' => 'Enero',
		'jeudi_abrege' => 'Ju',
		'juillet' => 'Julio',
		'juin' => 'Junio',
	  
	
//K
	  
		'kezakospip' => 'Sitio realizado con SPIP, un CMS gratuito bajo licencia GPL',
	  
	
//L
	  
		'licence' => 'El esqueleto SARKA-SPIP esta distribuido bajo la licencia GPL',
		'licence_site' => 'Licencia a definir',
		'licence_squelette' => '<a href="http://www.gnu.org/copyleft/gpl.html" title="El esqueleto SARKA-SPIP esta distribuido bajo la licencia GPL" target="_blank">GPL</a>',
                'lire_evenement_virtuel' => 'Leer el acontecimiento original',
		'lire_suite_article'=> 'leer el resto del art&iacute;culo',
		'login_membre' => 'Conexi&oacute;n',
                'login_motpasseoublie' => '&iquest;contrase&ntilde;a&nbsp;olvidada?', // redefinition item SPIP
                'login_retoursitepublic' => 'volver&nbsp;al&nbsp;sitio&nbsp;p&uacute;blico', // redefinition item SPIP
                'login_sinscrire' => 'inscribirse', // redefinition item SPIP
		'logout_membre' => 'Desconexi&oacute;n',
		'lundi_abrege' => 'Lu',
		'lxh' => 'por',
	  
	
//M
	  
		'mai' => 'Mayo',
		'maj' => '&Uacute;ltima actualizaci&oacute;n',
		'mardi_abrege' => 'Ma',
		'mars' => 'Marzo',
		'mercredi_abrege' => 'Mi',
		'message' => 'Mensaje', // redefinition item SPIP
		'message_bonjour' => 'Hola, estimado(da)',
		'messages_forum' => 'Mensajes del foro', // redefinition item SPIP
		'messages_mot' => 'Mensajes relacionados',
		'messages_recents' => 'Los mensajes m&aacute;s recientes del foro', // redefinition item SPIP
		'mois_precedent' => 'Mes anterior',
		'mois_suivant' => 'Mes siguiente',
		'mots_clefs' => 'Palabras clave', // redefinition item SPIP
		'msg_0_article_publie' => 'Ninguno art&iacute;culo publicado hasta ahora',
		'msg_0_evt_agenda' => 'Ninguno acontencimiento disponible actualmente en el agenda',
		'msg_0_evt_annee' => 'Ninguno acontencimiento de esta categor&iacute;a en el a&ntilde;o',
		'msg_0_evt_saison' => 'Ninguno acontencimiento de esta categor&iacute;a en la temporada',
		'msg_0_album_galerie' => 'Ning&uacute;n &aacute;lbum disponible actualmente en la galer&iacute;a',
		'msg_0_album_filtre_annee' => 'Ninguno &aacute;lbum de esta categor&iacute;a en el a&ntilde;o',
		'msg_0_album_filtre_saison' => 'Ninguno &aacute;lbum de esta categor&iacute;a en la temporada',
		'msg_0_album_filtre_planche' => 'Ninguno &aacute;lbum de esta categor&iacute;a',
		'msg_evenement_virtuel' => 'El texto de este acontecimiento no esta accesible directamente en el sitio. Pueden, a pesar de todo, consultar la descripci�n de este acontecimiento cliqueando en el enlace siguiente',
	  
	
//N
	  
		'newsletter' => 'Novedades',
		'nom' => 'Nombre', // redefinition item SPIP
		'novembre' => 'Noviembre',
	  
	
//O
	  
		'objets_mot' => 'Objetos relacionados a la palabra clave',
		'octobre' => 'Octubre',
	  
	
//P
	  
		'par_auteur' => 'por ', // redefinition item SPIP
		'page_non_trouvee' => 'P&aacute;gina no existante',
		'plan_site' => 'Mapa del sitio', // redefinition item SPIP
		'plusieurs_evenements_jour' => 'acontecimientos en este d&iacute;a',
		'popularite_article' => 'popularidad',
		'poster_message' => 'Publicar un mensaje', // redefinition item SPIP
		'ps' => 'Post Scriptum',
		'publie_sur_site' => 'Publicado en el sitio',
	  
	
//Q
	  
	  
	
//R
	  
		'realisation_squelette' => 'Collectif Sarka-SPIP',
		'recents_syndic' => '&Uacute;ltimas noticias del Web',
		'recherche_articles_resultat' => 'Art&iacute;culos encontrados',
		'recherche_messages_resultat' => 'Mensajes encontrados en los foros',
		'recherche_mots_resultat' => 'Palabras claves encontradas y art&iacute;culos relacionados',
		'recherche_rubriques_resultat' => 'Secciones encontradas',
		'recherche_breves_resultat' => 'Breves encontradas',
		'recherche_sites_resultat' => 'Sitios Web encontrados',
		'rechercher' => 'Buscar',
		'repondre_article' => 'Comentar este art&iacute;culo', // redefinition item SPIP
		'repondre_message' => 'Responder este mensaje', // redefinition item SPIP
		'reponse_suite_article' => 'Respuesta al art&iacute;culo',
		'reponse_suite_message' => 'Respuesta al mensaje',
		'requete' => 'Escoger la(s) palabra(s)',
		'resultats_recherche' => 'Resultados de la b&uacute;squeda', // redefinition item SPIP
		'retour_article' => 'Volver al art&iacute;culo',
		'rubriques_mot' => 'Secciones relacionadas',

	
//S
	  
		'samedi_abrege' => 'Sa',
		'septembre' => 'Septiembre',
		'signatures_petition' => 'Firmas', // redefinition item SPIP
		'signer_petition' => 'Firmar la petici&oacute;n',
		'site_realise_avec_spip' => 'Sitio desarrollado con SPIP', // redefinition item SPIP
		'site_web' => 'Sitio web',
		'sites_mot' => 'Sitios Web relacionados',
		'sites_web' => 'Sitios Web', // redefinition item SPIP
		'squelette' => 'Este sitio utiliza el esqueleto SARKA-SPIP',
		'statistiques_edition' => 'Estad&iacute;sticas de publicati&oacute;n',
		'statistiques_visites' => 'Estad&iacute;sticas de las visitas',
		'statut_0minirezo' => 'administrador(a)',
		'statut_1comite' => 'redactor(a)',
		'statut_6forum' => 'visitante',
		'sur_web' => 'Del web', // redefinition item SPIP
		'syndiquer_rubrique' => 'Sindicar esta secci&oacute;n', // redefinition item SPIP
		'syndiquer_site' => 'Sindicar el sitio completo', // redefinition item SPIP

	
//T
	  
		'top_articles' => 'Art&iacute;culos populares',
		'toutes_les_breves' => 'Todas las breves del sitio',
		'tri_par_auteur' => 'Por autor',
		'tri_par_date' => 'Por fecha',
		'tri_par_rubrique' => 'Por secci&oacute;n',
	  
	
//U
	  
		'url_fausse' => 'La direccion que estas pidiendo no existe. El mapa del sitio puede ayudarte a encontrar la informacion que estas buscando.',
	  
	
//V
	  
		'vendredi_abrege' => 'Vi',
		'votre_reponse' => 'Su respuesta',
	  
	
//W
	  
		'web_france' => 'Web Francia',
		'web_monde' => 'Web Mundial',
	  
	
//X
	  
	  
	
//Y
	  
	  
	
//Z
	
        
		

	
);

?>