<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// B
'bouton_sauver' => 'Sauvegarder',
'bouton_telecharger' => 'T&eacute;l&eacute;charger',
'bouton_tout_cocher' => 'Tout cocher',
'bouton_tout_decocher' => 'Tout d&eacute;cocher',
'bouton_voir' => 'Voir',
'bulle_bouton_voir' => 'Voir le contenu de l\'archive',

// C
'colonne_date' => 'Date',
'colonne_nom' => 'Nom',
'colonne_taille_octets' => 'Taille',
'colonne_actions' => '&nbsp;',

// I
'info_sauver' => 'Cette option fabrique un fichier d\'archives contenant les donn&eacute;es de personnalisation du site comme le dernier dump de sauvegarde de la base, le(s) dossier(s) des squelettes nomm&eacute;s, le dossier images... 
	Le fichier d\'archives est cr&eacute;&eacute; dans <i>tmp/mes_fichiers/</i> et se nomme <i>mf2_aaaammjj_hhmmss.zip</i>.
	La liste exhaustive des fichiers et dossiers pouvant &ecirc;tre sauvegard&eacute;s est affich&eacute;e ci-dessous:',
'info_telecharger' => 'Cette option permet de t&eacute;l&eacute;charger un des fichiers d\'archives disponibles parmi la liste affich&eacute;e ci-dessous:',

// M
'message_rien_a_sauver' => 'Aucun fichier ni dossier &agrave; sauvegarder.',
'message_rien_a_telecharger' => 'Aucune sauvegarde disponible au t&eacute;l&eacute;chargement.',
'message_rien_a_restaurer' => 'Aucune sauvegarde disponible &agrave; la restauration.',
'message_sauvegarde_ok' => 'Le fichier d\'archives a bien &eacute;t&eacute; cr&eacute;&eacute;.',
'message_sauvegarde_nok' => 'Erreur lors de la sauvegarde. Le fichier d\'archives n\'a pas &eacute;t&eacute; cr&eacute;&eacute;.',
'message_telechargement_ok' => 'Le fichier d\'archives a bien &eacute;t&eacute; t&eacute;l&eacute;charg&eacute;.',
'message_telechargement_nok' => 'Erreur lors du t&eacute;l&eacute;chargement.',
'message_zip_propriete_nok' => 'Aucune propri&eacute;t&eacute; n\'est disponible sur cette archive.',
'message_zip_sans_contenu' => 'Aucune information n\'est disponible sur le contenu de cette archive.',
'message_zip_auteur_indetermine' => 'Non d&eacute;termin&eacute;',
'message_zip_auteur_cron' => 'Tache automatique',

// R
'resume_table_telecharger' => 'Liste des archives disponibles au t&eacute;l&eacute;chargement',
'resume_zip_auteur' => 'Cr&eacute;&eacute; par',
'resume_zip_statut' => 'Statut',
'resume_zip_compteur' => 'Fichiers / dossiers archiv&eacute;s',
'resume_zip_contenu' => 'Contenu r&eacute;sum&eacute;',


// T
'titre_page_navigateur' => 'Mes fichiers',
'titre_onglet' => 'Mes fichiers',
'titre_boite_sauver' => 'Sauvegarder mes fichiers',
'titre_boite_telecharger' => 'T&eacute;l&eacute;charger une sauvegarde',

);


?>
