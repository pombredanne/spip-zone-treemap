<?php
   /*******
    * Fichier de langue pour le plugin OpenID
    ******/

$GLOBALS[$GLOBALS['idx_lang']] = array(
  'login_login2' => 'Login (identifiant de connexion au site) ou bien OpenID&nbsp;:',
  'utilisateur_inconnu' => 'Utilisateur inconnu sur ce site',
  'verif_refusee' => 'V&eacute;rification refus&eacute;e',
  'erreur_openid' => 'Erreur d\'authentification OpenID: avez-vous bien entr&eacute; un OpenID valide?'
);
