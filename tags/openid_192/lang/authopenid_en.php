<?php
   /*******
    * Fichier de langue pour le plugin OpenID
    ******/

$GLOBALS[$GLOBALS['idx_lang']] = array(
  'login_login2' => 'Login (identifier for connection to the site) or OpenID&nbsp;:',
  'utilisateur_inconnu' => 'User unknown on this site',
  'verif_refusee' => 'Authentication request canceled',
  'erreur_openid' => 'OpenID authentication error: have you entered a valid OpenID?'
);
