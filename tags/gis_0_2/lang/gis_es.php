<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP nomm� admin_lang genere le NOW()
// langue / language = gl

$GLOBALS[$GLOBALS['idx_lang']] = array(

	//elementos del formulario CFG
	
	"api_carte" => "API para los mapas",
	"api_carte_geomap" => "Google Maps API",
	"api_carte_openlayer" => "Open Layers",
	"desactiver_swfobject" => "D&eacute;sactivar swf_object",
	"explication_pages_prive" => "Escoge las secciones donde quieres utilizar el plugin.",
	"explication_swfobject" => "D&eacute;sactiver l'insertion du script swf_object dans les pages publiques (si vous n'utilisez pas le lecteur de son dans les infobulles).",
	"formats_documents" => "Formatos admitidos",
	"pages_public" => "Espacio p&uacute;blico",
	"pages_prive" => "Espacio privado",
	"parametres_formulaire" => "Par&aacute;metros del formulario p&uacute;blico",
	"rubrique_cible" => "Rubrique cible",
	"statut_articles" => "Estado de los art&iacute;culos",
	
	//elementos del formulario para editar las coordenadas
	
	"address" => "Plaza Juda; Levi, Cordoba, Spain",
	"boton_actualizar" => "actualizar",
	"bouton_supprimer" => "borrar",
	"cambiar" => "cambiar coordenadas",
	'clic_mapa' => 'Haz clic en el mapa para cambiar las coordenadas',
	"coord_enregistre" => "Coordenadas guardadas",
	"coord_maj" => "Coordonn&eacute;es mises &agrave; jour",
	"label_address" => "Buscar!",
	"lat" => "latitud",
	"long" => "longitud",
	"zoom" => "zoom",
	
	//elementos del formulario publico
	
	"bouton_documents" => "Valider",
	"document_joint" => "Documents joints",
	"document_supprime" => "Document(s) supprim&eacute;(s)",
	"erreur_ajout_article" => "Erreur lors de l'ajout de l'article",
	"erreur_copie_impossible" => "Impossible de copier le document",
	"erreur_formats_acceptes" => "Formats accept&#233;s : @formats@.",
	"erreur_titre" => "Titre obligatoire",
	"erreur_texte" => "Veuillez entrer un texte",
	"erreur_upload" => "Erreur lors du chargement du fichier",
	"ok_formulaire_soumis" => "Votre article a &eacute;t&eacute; envoy&eacute; sur le site.",
	
	"configurar_gis" => "Configurar Plugin Gis",
);

?>

