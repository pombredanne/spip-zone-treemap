<?php

/**
 * spip.icio.us
 * Gestion de tags lies aux auteurs
 *
 * Auteurs :
 * Quentin Drouet
 * Erational
 *
 * 2007-2009 - Distribue sous licence GNU/GPL
 *
 */

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'add_tags' => 'Ajouter des tags',
	'aucun_tags' => 'Aucun tag associ&eacute;.',

	// B
	'by' => 'Par',

	// I
	'identifier' => 'Pour ajouter des tags sur cet article, vous devez vous identifier',

	// M
	'message_installation_activation' => 'Installation des tables et configuration par d&eacute;faut de spip.icio.us',
	'message_upgrade_database' => 'spipicious update @ @version@<br/>',

	// N
	'no_tags_associes' => 'Vous n\'avez pas de mot cl&eacute; associ&eacute;',

	// S
	'spipicious' => "Spip.icio.us",

	// T
	'tag_ajoute' => 'Vous avez ajout&eacute; un tag:<br />@name@',
	'tag_deja_present' => 'Vous aviez d&eacute;j&agrave; ajout&eacute; ce tag.',
	'tag_supprime' => 'Vous avez supprim&eacute; le tag :<br />@name@',
	'tags' => 'Tags :',
	'tags_ajoutes' => 'Vous avez ajout&eacute; plusieurs tags:<br />@name@',
	'tags_supprimes' => 'Vous avez supprim&eacute; plusieurs tags:<br />@name@',
	'to' => "avec",

	// V
	'vos_tags' => 'Vos tags (modification)',

	// W
	'waiting' => 'recherche de vos tags',

	'administrateurs' => 'Les administrateurs',
	'redacteurs' => 'Les r&eacute;dacteurs',
	'visiteurs' => 'Les visiteurs',
	'types_utilisateurs' => 'Qui a acc&egrave;s au formulaire ?',
	'nom_groupe_tag' => 'Groupe de mots cl&eacute; associ&eacute;',
	'waiting' => 'recherche de vos tags',
	'remove' => 'Supprimer',
);
?>