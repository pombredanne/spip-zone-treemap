<?php
// ---------------------------------------------------------
//  Ma lettre
// ---------------------------------------------------------


include(dirname(__FILE__).'/../inc_malettre.php');

include_spip('inc/presentation');
include_spip('inc/distant');
include_spip('inc/affichage');
include_spip('inc/meta');
include_spip('inc/filtres');
include_spip('inc/lang');

// -------------------------------
// Main: Ma Lettre
// -------------------------------

function exec_malettre(){ 
  global $connect_statut;
	global $connect_toutes_rubriques;
 
  include_spip("inc/charsets"); 
  include_ecrire("inc_presentation");
  
  // chemin
  $path = _DIR_RACINE;  
  $path_archive = "IMG/lettre";
  $path_archive_full = $path.$path_archive;
  $path_url = lire_meta("adresse_site");


  $p=explode(basename(_DIR_PLUGINS)."/",str_replace('\\','/',realpath(dirname(__file__))));
  define('_DIR_PLUGIN_MALETTRE',(_DIR_PLUGINS.end($p)));
  $path_plugin = dirname(__file__)."/../";
  
  // parametre par defaut (editable a la main)
  $id_article_edito  = 1;  // edito lettre
  $adresses = array(
        "Pierre Durand" => "pdurand@domaine.tld",
        "John Doe" => "jdoe@domaine.tld",
  ); 
   
  $expediteurs = array(
         "Liste A" => "liste-a@domaine.tld",
         "Liste B" => "liste-a@domaine.tld",
  );   

  // si cfg dispo, on charge les valeurs
  if (function_exists(lire_config))  {
      $id_article_edito = lire_config('malettre/id_article_edito');
      $expediteurs = array();
      for ($i=1;$i<4;$i++) {
        if (trim(lire_config("malettre/expediteur_email$i"))!="")
              $expediteurs[lire_config("malettre/expediteur_nom$i")] = lire_config("malettre/expediteur_email$i");
      }
      $adresses = array();
      for ($i=1;$i<6;$i++) {
        if (trim(lire_config("malettre/adresse_email$i"))!="")
              $adresses[lire_config("malettre/adresse_nom$i")] = lire_config("malettre/adresse_email$i");
      }
  } 
    
  

  // main ------------------------------------------------------
  
	echo debut_page(_T('malettre:ma_lettre'));	  
 
	if ($connect_statut == "0minirezo" && $connect_toutes_rubriques) {	  // admin restreint (connect_toutes_rubriques si admin)
		$page = "malettre";	

    debut_gauche();
    echo debut_boite_info();
    echo "<p>"._T('malettre:info')."</p>";
    echo "<p><a href='?exec=malettre'>"._T('malettre:ecrire_nouvelle')."</a></p>";
    echo "<p><a href='?exec=malettre&amp;action=letter_compose&amp;option=load'>"._T('malettre:charger_derniere')."</a></p>";
    echo "<p><a href='?exec=malettre_archive'>"._T('malettre:archives_gerer')."</a></p>";
    if (function_exists(lire_config)) echo "<p><a href='?exec=cfg&cfg=malettre'>"._T('malettre:config')."</a></p>";  
    echo fin_boite_info();
    
    echo debut_droite();
    
    // list last articles
		$titre = "titre";
		$table = "spip_articles"; 
		$id = "id_article";
		$statut = "publie"; 
		$temps = "id_article"; 
		$page_voir = "?exec=articles&id_article=";
    $page_edit = "?exec=articles_edit&id_article="; 
		//--		

		$action = _request('action');
		if ($action == "letter_compose") {      // compose la lettre
		        $errorFlag = false;		
      
						$option = _request('option');
						if ($option=="load") {         // on charge la derniere lettre
						
						    // recup contenu HTML
                $texte = $path_archive_full."/.malettre.html";
                $fr=fopen($texte,"r");
                while(!feof($fr)){
                      $sourceHTML  = '';
                      while(!feof($fr))  
                              $sourceHTML  .= fgets($fr,1024);
                }
                fclose($fr);
                
                // recup contenu TXT
                $texte = $path_archive_full."/.malettre_txt.html";
                $fr=fopen($texte,"r");
                while(!feof($fr)){
                      $sourceTXT  = '';
                      while(!feof($fr))
                            $sourceTXT .= fgets($fr,1024);
                }
                fclose($fr);
            
            } else {                       // on cree la lettre avec la requete web
            
            		include_spip('public/assembler');
                include_spip('inc/charsets');
            
                $add = _request('add');
    						$lettre_title = trim(strip_tags(_request('lettre_title'))); 
                $lettre_title = str_replace("\"","'", $lettre_title);
                
                $lang = _request('lang_select');
                if ($lang=="")  
                          $lang = $GLOBALS['meta']['langue_site'];
                
                            
      					// VERSION HTML & TXT
      					$sourceHTML = "";
      					$sourceTXT  = "";
      					
								$sourceHTML .= malettre_get_contents("malettre_header",0,$lang);                 // header
								$sourceTXT  .= malettre_get_contents("malettre_txt_header",0,$lang);             
								
                $sourceHTML .= malettre_get_contents("malettre_edito", $id_article_edito,$lang); // edito
                $sourceTXT  .= malettre_get_contents("malettre_txt_edito", $id_article_edito,$lang);
                
    						    
                // radio button
                if (strlen($add)>0) {   
      							foreach ($add as $value) {
      								    $sourceHTML .= malettre_get_contents("malettre_item",$value,$lang);
      								    $sourceTXT  .= malettre_get_contents("malettre_txt_item",$value,$lang);
      							}
      					}  							
      							
                // csv							
      					$art_csv = _request('art_csv'); 
      					$csv = explode(",", $art_csv);
      					if (strlen($csv)>0) {							  
      							foreach ($csv as $value2) {								
        						$value = trim($value2);								
          						if ($value!="") 	{
              								$sourceHTML .= malettre_get_contents("malettre_item",$value,$lang);
              								$sourceTXT  .= malettre_get_contents("malettre_txt_item",$value,$lang);
              				}
        						}
      					}					
    						$sourceHTML .= malettre_get_contents("malettre_footer",0,$lang);                 // foot
    						$sourceTXT  .= malettre_get_contents("malettre_txt_footer",0,$lang); 
      							
      					// ecriture fichier       											
    						if ($handle = fopen($path_archive_full."/.malettre.html", w)) { 						    
      							fwrite($handle, $sourceHTML);					
      							fclose($handle); 
                    
                    if ($handle = fopen($path_archive_full."/.malettre_txt.html", w)) { 			
        							fwrite($handle, $sourceTXT);					
        							fclose($handle);
      						  } else {
                      $errorFlag = true;
                      echo _T('malettre:erreur_ecriture')."($path.$path_archive)";
                    }	                     							
                } else {
                    $errorFlag = true;
                    echo _T('malettre:erreur_ecriture')."($path.$path_archive)";                    
    						}							
        				

    						
    				}  // fin examen requete		
    				
							
					  // affichage ?
					  if (!$errorFlag) {
						  echo "<form method='post' action='?exec=malettre'><fieldset>\n"; 
              echo "<input type='hidden' name='lang_select' value='$lang' />"; 
						  echo "<input type='hidden' name='action' value='letter_send' />\n";
              echo "<h4>"._T('malettre:expediteur')."</h4>\n";
              echo "<select name='expediteur'>\n";
							foreach ($expediteurs as $expediteur=>$val){
                  echo "<option value=\"$expediteur\" />".htmlentities($expediteur)." &lt;".htmlentities($val)."&gt;</option>";
              } 
              echo "</select>\n";   
              echo "<br />"._T('malettre:autre')." <input type='text' name='expediteur_more' />("._T('malettre:email_seulement').")\n" ;  
                   
             	echo "<h4>"._T('malettre:destinataires')."</h4>\n";
							foreach ($adresses as $adresse=>$val){
                  echo "<input type='checkbox' value=\"$val\"' name='desti[]' />$adresse &lt;$val&gt;<br />";
              }
              echo "autre: <input type='text' name='desti_more' /> ("._T('malettre:email_seulement').")<br /><br />\n";

              echo "<input type='submit' name='sub' value='Envoyer la lettre' /> \n";
              echo "<input type='button' name='sub' value='Ecrire une nouvelle lettre' onclick=\"javascript:document.location.href='?exec=malettre'\"  /> ";
              
							echo "<h4>"._T('malettre:apercu')."</h4>\n";
							echo "Sujet: <input type='text' size='55' name='lettre_title' value=\"".$lettre_title."\" /><br />\n";
              echo "<iframe width=\"750\" height=\"500\" src=\"$path_archive_full/.malettre.html?nocache=".time()."\"></iframe>\n";
							echo "<h4>"._T('malettre:version_html')."</h4>\n";
							echo "<textarea cols='70' rows='20'>$sourceHTML</textarea>";
							echo "<h4>"._T('malettre:version_txt')."</h4>\n";
							echo "<textarea cols='70' rows='20'>$sourceTXT</textarea>";
							echo "</fieldset></form>\n";
						} 					
						
   } else if ($action=='letter_send') {
		        //
	          // envoi de la lettre
	          //
	          include(dirname(__FILE__)."/../class.phpmailer.php");
		        
		        // titre 
		        $lettre_title = trim(strip_tags(_request('lettre_title'))); 
            $lettre_title = str_replace("\"","'", $lettre_title);  
            if ($lettre_title == "") {  // à supprimer ou integrer ou multilingue ?
              $months=array(1=>'Janvier', 2=>'Fevrier', 3=>'Mars', 4=>'Avril', 5=>'Mai', 6=>'Juin', 7=>'Juillet', 8=>'Aout', 9=>'Septembre', 10=>'Octobre', 11=>'Novembre', 12=>'Decembre');
              $today = getdate(mktime()-(24*3600));
              $sujet = "Les nouveautes de ".$months[$today[mon]]." ".date("Y");
            } else {
              $sujet = $lettre_title;
            }
            
            // hash            
            $lettre_hash = substr(md5(time()),0,5);
            $url_lettre_archive = "$path_url/$path_archive/lettre_".date("Ymd")."_".$lettre_hash."_"._request('lang_select').".html";
            
            // recup contenu HTML
            $texte = $path_archive_full."/.malettre.html";
            $fr=fopen($texte,"r");
            while(!feof($fr)){
                  $recup = '';
                  while(!feof($fr))  
                          $recup .= fgets($fr,1024);
            }
            fclose($fr);
            $recup = str_replace("{URL_MALETTRE}",$url_lettre_archive,$recup);
            
            // recup contenu TXT
            $texte = $path_archive_full."/.malettre_txt.html";
            $fr=fopen($texte,"r");
            while(!feof($fr)){
                  $recup_txt = '';
                  while(!feof($fr))
                        $recup_txt .= fgets($fr,1024);
            }
            fclose($fr);
            $recup_txt = str_replace("{URL_MALETTRE}",$url_lettre_archive,$recup_txt);
            
            // envoi lettre
            $exp_email = _request('expediteur_more');
            if ($exp=="") {
                $expediteur = _request('expediteur');                
                if (isset($expediteurs[$expediteur])) {
                   $exp_name = $expediteur;
                   $exp_email = $expediteurs[$expediteur];
                }  else  die("expediteur inconnu");
            } else {
              $exp_name = $exp_mail;
            }            
           
            $desti = _request('desti');
            $desti_more = _request('desti_more'); 
            if ($desti_more!="") $desti[] = $desti_more;
            
            echo "<h3>"._T('malettre:envoi')." <i style='color:#999;'>$sujet</i></h3>\n";
            echo "<div style='border:1px solid;background:#eee;margin:10px 0;padding:10px;font-family:arial,sans-serif;font-size:0.9em;'>";
                       
            if (is_array($desti)) {
              foreach ($desti as $k=>$adresse) { // envoi a tous les destinataires
                $mail = new PHPMailer();
                         
                $mail->From     = "$exp_email";
                $mail->FromName = "$exp_name";
                $mail->AddReplyTo("$exp_email");
                $mail->AddAddress($adresse,$adresse);
                                
                $mail->WordWrap = 50;           // set word wrap
                $mail->IsHTML(true);            // send as HTML
                $mail->CharSet = "utf-8"; 
                
                $mail->Subject  =  "$lettre_title";
                $mail->Body     =  $recup;
                $mail->AltBody  =  $recup_txt;
                             
                if(!$mail->Send()) {
                    $msg = "<div style='color:red'><strong>$adresse</strong> - "._T('malettre:erreur_envoi')."</div>";  
                    $msg .="Mailer Error: " . $mail->ErrorInfo; 
                    $success_flag = false;
                } else {  
                    $msg = "<div style='color:green'><strong>$adresse</strong> - <span style='color:green'>"._T('malettre:succes_envoi')."</span></div>";         
                }
                 
                echo $msg;
              }
            } else {
              echo "<div style='color:red'>"._T('malettre:erreur_no_dest')."</div>";
            }
            echo "</div>";
            
            // archivage de la lettre en dur
            $lettre_archive = "$path_archive_full/lettre_".date("Ymd")."_".$lettre_hash."_"._request('lang_select').".html";
            $f_archive=fopen($lettre_archive,"w");
            fwrite($f_archive,$recup); 
            fclose($f_archive);
            echo "<div style=\"margin:15px 0;\">"._T('malettre:archives_placer')."(<a href='$url_lettre_archive' target='_blank'>"._T('malettre:consulter')."</a>)</div>";
                      
           
            
            echo "<p><a href='?exec=malettre'>"._T('malettre:ecrire_nouvelle2')."</a></p>\n";
            
    } else {	//
	            // pas d'action: affichage des articles pour composition de la lettre
	            //
	            
	            // verif si repertoire stockage dispo
	            if (!is_dir($path_archive_full)) {                                     
                   if (!mkdir ($path_archive_full, 0777)) // on essaie de le creer  
                        echo "<div style='color:red;padding:5px;border:1px solid red;>"._T('malettre:erreur_ecrire_stockage')."($path_archive_full)</div>"; 
              }
                            
              $lang_select = _request('lang_select');
              if ($lang_select!="") $cond_lang_sql = "AND lang='$lang_select'";
                              else  $cond_lang_sql = "";
              $requete = "SELECT $id, $titre FROM $table WHERE statut like '$statut'  $cond_lang_sql ORDER BY $temps DESC LIMIT 0,50";
              $result=spip_query($requete);
                                      
              if (spip_num_rows($result) == 0) {
							  echo "aucun article disponible";
              } else {	                							
                echo "<form method='post' action='?exec=malettre'>"; 
                echo "<input type='hidden' name='action' value='letter_compose' />";
                echo "<input type='hidden' name='lang_select' value='$lang_select' />";
                echo "<fieldset>\n";
                
              
              if($GLOBALS['meta']['multi_rubriques']=="oui" || $GLOBALS['meta']['multi_articles']=="oui")
      				      $active_langs = explode(",",$GLOBALS['meta']['langues_multilingue']);
      			  else	$active_langs = "";
      				
      				 if (is_array($active_langs)) {
      				       echo _T('malettre:choix_lang');
                     foreach($active_langs as $k=>$active_lang) {
                        if ($lang_select==$active_lang) echo "<strong>[$active_lang]</strong> ";  
                                         else echo "<a href='?exec=malettre&amp;lang_select=$active_lang'>[$active_lang]</a> ";
                        
                     }
                     if ($lang_select=="") echo "<strong>["._T('malettre:lang_toute')."]</strong> ";             
                                     else  echo "<a href='?exec=malettre'>["._T('malettre:lang_toute')."]</a>";
               }
      				
                
								
								echo "<br /><br />"._T('malettre:compose_sujet')." :<i>("._T('malettre:compose_non_spip').")</i><br />\n";
								echo "<input type='text' size='55' name='lettre_title' /><br />\n";
								echo "<br />"._T('malettre:compose_contenu')." - <a href='$page_edit$id_article_edito'>"._T('malettre:compose_edito')."</a><br />\n";
								echo "<iframe width='600' height='500' src='$path_url/spip.php?page=malettre_edito&amp;id_article=$id_article_edito'></iframe>\n";
								echo $stro;								
							
								echo "<br />"._T('malettre:compose_cochant');
                echo "<table class='spip' style='width:100%;border:0;'>";
                        
                                //affichage des 50 documents 
                                while($row=spip_fetch_array($result)) {
                                        $id_document=$row['id_article'];                                        
                                        $titre=charset2unicode($row['titre']);  // BUG pb de charset  filtrer_entites ?
                                        
                                        if ($compteur%2) $couleur="#FFFFFF";
                                        else $couleur="#EEEEEE";
                                        $compteur++;
                                        
                                        echo "<tr width=\"100%\">";
                                        echo "<td bgcolor='$couleur'>";
                                        if (! empty($page_voir)) echo "<a href='$page_voir$id_document'$page_voir_fin>";
                                        echo typo("n&deg;".$id_document." - ".$titre);
                                        if (! empty($page_voir)) echo "</a>";                
                                        echo "</td>";										
                                        echo "<td align='center' bgcolor='$couleur'><input type=checkbox name=add[] value=\"$id_document\"></TD>";
                                        echo "</tr>\n";
                                }
								
								echo "<tr><td>"._T('malettre:compose_liste')."<br />";
								echo "<textarea rows='15' cols='50' id='art_csv' name='art_csv'></textarea></td></tr>";
								
                                
                echo "</table><br /><input type='submit' value='"._T('malettre:compose_submit')."' />\n";
								echo "</fieldset>\n";
								echo "</form>\n\n";
              }
		}
		//--	

		
	}	else { 
		echo "<strong>Vous n'avez pas acc&egrave;s &agrave; cette page.</strong>"; 
	}
	
	echo fin_page();
}

?>