<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

  // A
  'archives' => 'Archives',
  'archives_gerer' => 'G&eacute;rer les archives',
  'archives_placer' => 'Lettre plac&eacute;e en archive',
  'autre' => 'Autre :',
  'apercu' => 'Apercu',  

  // C
  'charger_derniere' => 'Charger la derni&egrave;re lettre',
  'config' => 'Configurer',
  'consulter' => 'Consulter',
  'compose_sujet' => 'Sujet du mail',
  'compose_non_spip' => 'raccourcis SPIP non support&eacute;s',
  'compose_contenu' => 'Texte d\'introduction',
  'compose_edito' => '&eacute;diter ce texte',
  'compose_cochant' => 'Choisissez les articles que vous vous publier dans la lettre en les cochant ...',
  'compose_liste' => 'ET / OU <br />indiquer les num&eacute;ros des articles &agrave; publier s&eacute;par&eacute;s par une virgule',
  'compose_submit' => 'Ajouter &agrave; la lettre',
  'choix_lang' => 'Composer la lettre en ',
  
  // D
  'destinataires' => "Destinataires",

  // E
  'ecrire_nouvelle' => 'Ecrire une lettre',
  'ecrire_nouvelle2' => 'Ecrire une nouvelle lettre',
  'erreur_ecriture' => '<strong>erreur:</strong> impossible de cr&eacute;er la lettre au format HTML, v&eacute;rifier le param&egrave;tre chemin d\'acc&egrave;s et les droits en &eacute;criture (chmod 777)',
  'erreur_ecriture_stockage' => 'R&eacutepertoire de stockage de la lettre impossible &agrave; cr&eacute;er',
  'erreur_envoi' => 'Erreur lors de l\'envoi du mail',
  'erreur_no_dest' => 'Erreur: aucun destinataire',
  'erreur_lecture' => 'Erreur: impossible de lire le dossier',
  'expediteur' => 'Exp&eacute;diteur',
  'email_seulement' => "email seulement",
  'envoi' => 'Envoi',
  'effacer' => 'Effacer',
  'effacer_confirm' => 'Etes vous sur de vouloir effacer cette lettre ?',
  
  // I 
  'info' => 'Cette page permet de cr&eacute;er une lettre sur mesure en choisissant vos articles.',
  
  // L  
  'lang_toute' => 'toutes les langues',
  'lettres_dispo' => 'lettre(s) disponible(s)',
  'lettre_du' => 'lettre du',
  'lettre_txt_auto' => 'Ceci est une message automatique - ne pas repondre',
  'lettre_txt_titre' => 'LETTRE D\'INFORMATION DE',
  'lettre_txt_html_dispo' => 'La version HTML de cette lettre est disponible en ligne: ',
  'lettre_txt_unsub' => 'Modifier son abonnement: ',
  'lettre_html_unsub' => 'se d&eacute;sinscrire de la lettre',
  'lien' => 'Lien',

  // M
  'ma_lettre'=>'Ma lettre',
  'ma_lettre_warning' => 'Si vous n\'arrivez pas &agrave; lire correctement cette lettre, allez directement sur',
  
  // S
  'succes_envoi' => 'Lettre bien envoy&eacute;e !',
  
  // V
  'version_html' => 'Version HTML',
  'version_txt' => 'Version Texte',
  'voir' => 'Voir'

);


?>
