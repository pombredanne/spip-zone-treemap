<?php
	/**
	 * GuestBook
	 *
	 * Copyright (c) 2008 - 2009
	 * Yohann Prigent (potter64)
	 * Ce programme est un logiciel libre distribue sous licence GNU/GPL.
	 * Pour plus de details voir le fichier COPYING.txt.
	 *  
	 **/
$GLOBALS[$GLOBALS['idx_lang']] = array(
//A
	'aucun_message_livre' => 'Aucun message dans le livre',
// C
	'champs_obligatoires' => 'Vous n\'avez pas rempli des champs obligatoires et/ou votre saisie contient des erreurs',
	'continuer_moderation' => 'Vous pouvez continuer la mod&eacute;ration des messages',
	'courriel' => 'Courriel',
// D
	'dont' => 'dont',
// E
	'erreur_champ_remplir' => 'Vous devez remplir le champ &ccedil;i-dessus',
//I
	'infos_perso' => 'Informations personnelles',
// M
	'message' => 'Message',
	'message_poste' => 'Le message a bien &eacute;t&eacute; post&eacute;, merci !',
	'moyenne_publie' => 'Moyenne des messages publi&eacute;s',
	'moyenne_tous' => 'Moyenne de tous les messages',
// N
	'nombre_messages_livre' => 'messages dans le livre',
	'nom_chiffres' => 'Ce nom contient des chiffres',
	'note' => 'Note',
	'note_message' => 'Note &amp; Message',
	'numero' => 'N&deg;',
//P
	'poster' => 'Poster',
	'prenom' => 'Pr&eacute;nom',
	'prenom_chiffres' => 'Ce pr&eacute;nom contient des chiffres',
	'pseudo' => 'Pseudo',
// R
	"repondre" => "R&eacute;pondre",
	'repondu' => 'a r&eacute;pondu',
	'rentrez_infos_perso' => 'Rentrez vos informations personnelles',
	'repasser_validation' => 'Repasser ce message en validation',
	'reponse_ok' => 'R&eacute;ponse post&eacute; !',
//S
	'statut' => 'Statut',
// T
	'titre' => 'Le livre d\'or',
//U
	'un_message_livre' => 'Un seul message',
// V
	'ville' => 'Ville'
);
?>