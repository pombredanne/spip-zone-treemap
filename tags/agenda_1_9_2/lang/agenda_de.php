<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
'bouton_annuler' => 'Storno',
'activite_editoriale' =>   'editoriale Aktivit&auml;t',
'ajouter_un_evenement' => 'diesem Artikel ein event hinzuf&uuml;gen',
'texte_agenda' => 'AGENDA',
'titre_cadre_ajouter_evenement' => 'Event hinzuf&uuml;gen',
'titre_cadre_modifier_evenement' => 'Event um&auml;ndern',
'item_mots_cles_association_evenements' => 'zu den events',
'info_un_evenement' => 'ein event',
'info_nombre_evenements' => '@nb_evenements@ event',
'info_evenements' => 'Event',
'evenements' => 'Event',
'evenement_titre' => 'Titel',
'evenement_descriptif' => 'Beschreibung',
'evenement_lieu' => 'Ort',
'evenement_date' => 'Datum',
'evenement_date_de' => 'Von ',
'evenement_date_du' => 'Vom ',
'evenement_date_a' => 'im ',
'evenement_date_au' => 'bis ',
'evenement_date_a_immediat' => 'zu ',
'evenement_date_debut' => 'Anfangsdatum',
'evenement_date_fin' => 'Enddatum',
'lien_retirer_evenement' => 'L&ouml;schen',
'evenement_horaire' => 'Zeitplan',
'titre_sur_l_agenda' => 'Im Agenda',
'icone_creer_evenement' => 'Neue Veranstaltung anlegen',
'evenement_repetitions' => 'Wiederholungen',
'nb_repetitions' => '@nb@&nbsp;Wiederholungen',
'une_repetition' => '1&nbsp;Wiederholung',
'repetition_de' => 'Wiederholung von',
'nb_mots_clefs' => '@nb@&nbsp;Schlagworte',
'un_mot_clef' => '1&nbsp;Schlagwort',
'sans_titre' => 'ohne Titel',
'evenement_autres_occurences' => 'Autres occurences :',
'bouton_supprimer' => 'L&ouml;schen',
);


?>
