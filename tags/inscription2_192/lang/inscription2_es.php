<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(

//page de affichage de la configuration du plugin
'conf_plugin' => 'Configuraci&oacute;n Inscripci&oacute;on2',
'conf_plugin_page' => 'Configuraci&oacute;n del plugin Inscripci&oacute;n2',
'description_page' => 'En esta p&aacute;gina usted puede verificar la configuraci&oacute;n del plugin tal como est&aacute; guardada en la base de datos', 
'configs' => 'Configuraciones',
'formulaire_inscription' => 'Formulario de Inscripci&oacute;n',
'info_defaut' => 'Informaciones por defecto',
'info_defaut_desc' => 'Informaciones obligatorias por defecto',
'info_perso' => 'Informaciones Personales',
'info_pros' => '<NEW>Informations professionnelles',
'info_connection' => '<NEW>Informations de connexion',
'general_infos' => '<NEW>Informations G&eacute;n&eacute;rales',
'info_perso_desc' => 'Informaciones personales que seran solicitadas a los nuevos adherentes del sitio',
'info_gener' => 'Informaciones Generales',
'info_gener_desc' => 'Opciones que seran solicitadas a los nuevos adherentes del sitio',
'info_internes' => 'Informaciones Internas',
'info_internes_desc' => 'Opciones que seran almacenadas en la base de datos, pero que no seran solicitadas a los adherentes del sitio en el formulario de inscription',
'choisir_listes' => "Elija las listas de difusion (Spip-listes) que seran propuestas a los nuevos adherentes del sitio: ",
'select_listes' => '(puede seleccionar varias presionando la tecla shift)',
'deselect_listes' => '> deseleccionar todo',

//Geolocalisation
'geoloc' => 'G&eacute;olocalisation',

'inserez_infos' => 'Ingrese los datos solicitados',

'admin_modifier_auteur_supp' => '<NEW>Informations suppl&eacute;mentaires',

'form' => 'Formulario',
'fiche' => 'Ficha',
'fiche_mod' => 'Modificable',
'table' => 'Tabla',
'par_defaut' => '<NEW> par d&eacute;faut',
'form_expl' => ': El campo ser&aacute; mostrado en el formulario INSCRIPTION2',
'form_oblig_expl' => '<NEW> : Rendre la saisie obligatoire dans le formulaire',
'fiche_expl'=> ': El campo ser&aacute; visible en la ficha de adherente',
'fiche_mod_expl' => ': El campo ser&aacute; visible y modificable en la ficha de adherente, con la condicion de tener instalado el plugin CRAYONS',
'table_expl' => ': El campo estar&aacute; presente en la tabla de adherentes (espacio privado)',

'icone_menu_config' => 'Inscripci&oacute;n2',
'nom' => '<MODIF>Pseudo',
'nom_famille' => 'Apellido',
'prenom' => 'Nombre',
'naissance' => 'Fecha de nacimiento',
'adresse' => 'Direcci&oacute;n',
'code_postal' => 'C&oacute;digo Postal',
'sexe' => 'Sexo',
'ville' => 'Ciudad',
'email' => 'E-Mail',
'telephone' => 'Tel&eacute;fono',
'fax' => 'Fax',
'mobile' => 'Celular',
'profession' => 'Profesion',
'societe' => 'Sociedad',
'url_societe' => '<NEW>Site soci&eacute;t&eacute;',
'secteur' => 'Sector',
'adresse_pro' => '<NEW>Adresse professionnelle',
'code_postal_pro' => 'C&oacute;digo Postal',
'ville_pro' => 'Ciudad',
'telephone_pro' => '<NEW>T&eacute;l&eacute;phone professionnel',
'fax_pro' => '<NEW>Fax professionnel',
'mobile_pro' => '<NEW>Mobile professionnel',
'newsletter' => 'Listas de difusi&oacute;n',
'exp_newsletter' => 'Elija las listas de difusi&oacute;n a las que se quiere abonar',
'fonction' => 'Funci&oacute;n',
'masculin' => 'masculino',
'feminin' => 'femenino',
'username' => 'Usuario (login)',
'divers' => 'Varios',
'latitude' => '<NEW>Latitude',
'longitude' => '<NEW>Longitude',
'geomap_obligatoire' => '<NEW>Pour utiliser les donn&eacute;es de g&eacute;olocalisation des personnes, il est n&eacute;cessaire d\'installer le plugin GoogleMapApi...',
'exp_divers' => 'Campo que sera propuesto con el nombre de comentario',
'categorie' => 'Categoria de cotisaci&oacute;n',
'exp_categorie' => 'Lista de categor&iacute;as de la tabla \'spip_asso_categories\' que se propondr&aacute;n a los nuevos adherentes.',
'choisir_categories' => 'Elijalas en la siguiente lista:',
'publication' => 'Publicaci&oacute;n',
'exp_publication' => 'Autorisaci&oacute;n de publicaci&oacute;n de los datos personales en un eventual anuario',
'admin' => '<NEW>Admin',
'auteur' => '<NEW>Auteur',
'adherent' => '<NEW>Adh&eacute;rent',
'a_confirmer' => '<NEW>&Agrave; confirmer',
'autre' => '<NEW>Autre',
'confirmation' => '<NEW><p>&Ecirc;tes vous s&ucirc;r(e) de vouloir continuer?</p><p>Toutes les modifications seront irreversibles!!!</p>',

'creation' => 'Fecha de craci&oacute;n de la fiche',
'statut_rel' => 'Statut interno',
'exp_statut_rel' => 'Campo diferente del \'statut\' de SPIP_auteurs, este sirve para el control interno de una instituci&oacute;n',

'activation_compte' => 'Active su cuenta',
'probleme_email' => 'Problema de e-mail&nbsp;: el correo de activaci&oacute;n no pudo enviarse.',
'email_bonjour' => 'Buenos D&iacute;as @nom@,',
'message_auto' => '(Mensaje Autom&aacute;tico)',
'texte_email_inscription' => 'Usted acaba de solicitar vuestra inscripci&oacute;n en nuestro sitio @nom_site@. 
Actualmente esta a punto de terminar el ciclo de inscripc&oacute;n. Lo &uacute;nico que le queda por hacer es hacer click en el link siguiente para activar su cuenta e ingresar su contrase&ntilde;a. Una vez finalizado usted podr&aacute; conectarse a nuestra pagina con su nombre de usuario y contrase&ntilde;a.
@link_activation@

Su nombre de usuario es: *@login@*

Si usted no ha solicitado ser parte de nuestro sitio haga click en el link siguiente para eliminar la solicitud.
@link_suppresion@

Muchas gracias.

El equipo de @nom_site@',
'lisez_mail' => 'Un email acaba de ser enviado a la direcci&oacute;n email ingresada. Para poder utilizar su cuenta solo le queda un paso, siga las instrucciones que estan en el email y active su cuenta.',
'mail_renvoye' => 'Esta direccion email ya existe en nuestra base de datos. Para activar su cuenta sigas las instrucciones que estan en el email que acaba de ser enviado.',
'redemande_password' => 'Vuelva a ingresar su contrase&ntilde;a:',
'texte_email_confirmation' => 'Su cuenta acaba de ser activada. Usted puede conectarse desde ya con sus identificadores personales.

Su nombre de usuario es: @login@
y sa contrase&ntilde;a es la que acaba de ingresar.

Muchas gracias.

El equipo de @nom_site@',
'compte_active' => 'Cuenta activada',
'suppression_faite' => 'Sus datos han sido suprimidos con exito',

'janvier' => 'Enero',
'fevrier' => 'Febrero',
'mars' => 'Marzo',
'avril' => 'Abril',
'mai' => 'Mayo',
'juin' => 'Junio',
'juillet' => 'Julio',
'aout' => 'Agosto',
'septembre' => 'Septiembre',
'octobre' => 'Octubre',
'novembre' => 'Noviembre',
'decembre' => 'Diciembre',

'accesrestreint' => 'Acceso Restringido',
'choisir_zones' => 'Elija las zonas a las que ser&aacute;n afectados los nuevos adherentes del sitio',

'domaines' => 'Restringir las inscripciones a ciertos dominios',
'exp_domaines' => 'Defina sus dominios en el archivo inc/dominios.php siguiendo el modelo',
'domaine' => 'Dominio',
'choix_domaine' => 'Elija su dominio',
'mail_non_domaine' => 'La direcci&oacute;n email que ha ingresado no pertenece al dominio elegido, intentelo nuevamente',
'format' => 'Formato',
'expl_format' => 'Elija el formato en el que prefiere recibir los emails de las listas de difusion',
'texte' => 'Texto', 
'html' => 'HTML',

//Profil
'votre_profil' => '<NEW>Votre profil',
'contacts_personnels' => '<NEW>Contacts personnels',
'contacts_pros' => '<NEW>Contacts professionnels',
'votre_adresse' => '<NEW>Votre adresse personnelle',
'votre_adresse_pro' => '<NEW>Votre adresse professionnelle',
'votre_nom_complet' => '<NEW>Votre nom complet',
'vos_contacts_personnels' => '<NEW>Vos contacts personnels',
'website' => '<NEW>Site Internet',

//validation des champs
'champ_obligatoire' => '<NEW>Ce champ est obligatoire',
'chaine_valide' => '<NEW>Veuillez ins&eacute;rer une chaine de caract&egrave;re',
'cp_valide' => '<NEW>Veuillez ins&eacute;rer un code postal valide',
'numero_valide' => '<NEW>Veuillez ins&eacute;rer un num&eacute;ro valide',
'caracteres' => '<NEW>caract&egrave;res',
'chainenombre' => '<NEW>(compos&eacute;e de lettres et/ou de chiffres)',
'chainelettre' => '<NEW>(compos&eacute;e uniquement de lettres)',
'email_valide' => '<NEW>Veuillez ins&eacute;rer un email valide',

'titre_confirmation' => 'Confirmation',
'page_confirmation' => '<NEW>Page de confirmation de votre inscription'

);


?>