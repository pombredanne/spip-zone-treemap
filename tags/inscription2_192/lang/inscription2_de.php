<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(

//page d\'affichage de la configuration du plugin
'conf_plugin' => 'Inscription2 Konfiguration',
'conf_plugin_page' => 'Registrierte Konfiguration des plugin Inscription2',
'description_page' => 'Hier k&ouml;nnen Sie Ihre Konfigurationswahl pr&uuml;fen, wie sie gelagert werden', 
'configs' => 'Konfigurationen',
'formulaire_inscription' => 'Einschreibungsformular',
'info_defaut' => 'Informations par d&eacute;faut',
'info_defaut_desc' => 'Informations obligatoires par d&eacute;faut',
'info_perso' => 'Pers&ouml;nliche Informationen',
'info_pros' => 'Berufsaufkl&auml;rungen',
'info_connection' => '<NEW>Informations de connexion',
'general_infos' => 'Allgemeine Informationen',
'info_perso_desc' => 'Pers&ouml;nliche Informationen, die von den neuen Mitgliedern des web site verlangt werden',
'info_gener' => 'Allgemeine Informationen',
'info_gener_desc' => 'Optionen, die von den neuen Mitgliedern des web site verlangt werden',
'info_internes' => 'Interne Informationen',
'info_internes_desc' => 'Options qui seront stock&eacute;es dans la base de donn&eacute;es mais ne seront pas affich&eacute;es dans le formulaire des nouveaux adh&eacute;rents',
'choisir_listes' => 'Unten w&auml;hlen Sie die den neuen Mitgliedern vorzuschlagenden Verbreitungslisten:',
'select_listes' => '(Sie k&ouml;nnen mehrere Bereiche mit dem Anschlag ausw�hlen shift)',
'deselect_listes' => '> alles unauszuw&auml;hlen',

//Geolocalisation
'geoloc' => 'G&eacute;olocalisation',

'inserez_infos' => 'Erfassen Sie bitte die verlangten Informationen',

'admin_modifier_auteur_supp' => '<NEW>Informations suppl&eacute;mentaires',

'form' => 'Formular',
'fiche' => 'Karte',
'fiche_mod' => 'Ab�nderungsf�hig',
'table' => 'Tisch',
'par_defaut' => 'par d&eacute;faut',
'form_expl' => ' : Le champ sera affich&eacute; sur le formulaire INSCRIPTION2',
'form_oblig_expl' => ' : Rendre la saisie obligatoire dans le formulaire',
'fiche_expl'=> ' : Le champ sera visible sur la fiche d\'adh&eacute;rent',
'fiche_mod_expl' => ' : Le champ sera modifiable depuis l\'interface publique par l\'adh&eacute;rent &agrave; condition d\'utiliser le plugin CRAYONS',
'table_expl' => ' : Le champ sera affich&eacute; sur la liste des adh&eacute;rents (espace priv&eacute;)',

'icone_menu_config' => 'Inscription2',
'nom' => '<MODIF>Pseudo',
'nom_famille' => 'Name',
'prenom' => 'Vorname',
'naissance' => 'Geburtstag',
'adresse' => 'Adresse',
'code_postal' => 'Postcode',
'sexe' => 'Ziviliststand',
'ville' => 'Stadt',
'email' => 'EMail',
'telephone' => 'Telephon',
'fax' => 'Fax',
'mobile' => 'Handy',
'profession' => 'Beruf',
'societe' => 'Soci&eacute;t&eacute; / Association ...',
'url_societe' => 'Site soci&eacute;t&eacute;',
'secteur' => 'Secteur',
'adresse_pro' => 'Adresse professionnelle',
'code_postal_pro' => 'Postcode',
'ville_pro' => 'Stadt',
'pays_pro' => 'Land',
'telephone_pro' => 'T&eacute;l&eacute;phone professionnel',
'fax_pro' => 'Fax professionnel',
'mobile_pro' => 'Mobile professionnel',
'newsletter' => 'Listes de diffusion',
'exp_newsletter' => 'Choisissez ci-dessous les listes de diffusion auxquelles vous voulez vous abonner',
'fonction' => 'Funktion',
'masculin' => 'Herr',
'feminin' => 'Frau',
'username' => 'Benutzername (login)',
'divers' => 'Divers',
'latitude' => '<NEW>Latitude',
'longitude' => '<NEW>Longitude',
'geomap_obligatoire' => '<NEW>Pour utiliser les donn&eacute;es de g&eacute;olocalisation des personnes, il est n&eacute;cessaire d\'installer le plugin GoogleMapApi...',
'exp_divers' => 'Champ qui sera propos&eacute; aux visiteurs avec le nom de commentaire',
'categorie' => 'Cat&eacute;gorie de cotisation',
'exp_categorie' => 'Liste d&eacute;roulante des cat&eacute;gories de la table \'spip_asso_categories\' &agrave; proposer aux nouveaux adherents.',
'choisir_categories' => 'Choisissez-les ci-dessous&nbsp;:',
'publication' => 'Ver&ouml;ffentlichung',
'exp_publication' => 'Genehmigung der Ver&ouml;ffentlichung der pers&ouml;nlichen Daten',
'admin' => 'Admin',
'auteur' => 'Autor',
'adherent' => 'Mitglied',
'a_confirmer' => 'Zu best&auml;tigen',
'autre' => 'Anderes',
'confirmation' => '<p>&Ecirc;tes vous s&ucirc;r(e) de vouloir continuer?</p><p>Toutes les modifications seront irreversibles!!!</p>',

'creation' => 'Datum Schaffung der Karte',
'statut_rel' => 'Internes Statut',
'exp_statut_rel' => 'Champ diff&eacute;rent du statut de SPIP, celui-ci sert pour le controle interne d\'une institution',

'activation_compte' => 'Aktivieren Sie Ihr Konto',
'probleme_email' => 'Probl&egrave;me de mail&nbsp;: l\'email d\'activation ne peut pas &ecirc;tre envoy&eacute;.',
'email_bonjour' => 'Bonjour @nom@,',
'message_auto' => '(Dies ist eine automatische Mitteilung)',
'texte_email_inscription' =>   'Vous avez demand&eacute; &agrave; participer &agrave; notre site @nom_site@. 
Vous &ecirc;tes actuellement sur le point de finir votre inscription. Il vous suffit de cliquer le lien ci-dessous pour activer votre compte et choisir votre mot de passe.
@link_activation@

Votre login est&nbsp;: @login@

Si vous n\'avez pas demand&eacute; cette inscription ou ne voulez plus faire partie de notre site, cliquez le lien ci-dessous.\n
@link_suppresion@

Merci de votre confiance.

L\'6eacute;quipe de @nom_site@',
'lisez_mail' => 'Un email vient d\'&ecirc;tre envoy&eacute; &agrave; l\'adresse fournie. Pour activer votre compte veuillez suivre les instructions.',
'mail_renvoye' => 'Cette adresse email a d&eacute;j&agrave; &eacute;t&eacute; enregistr&eacute;e, veuillez activer votre compte en suivant les instructions contenues dans le mail.',
'redemande_password' => 'R&eacute;inserez votre mot de passe&nbsp;:',
'texte_email_confirmation' => 'Votre compte vient d\'&ecirc;tre activ&eacute;, vous pouvez d&egrave;s maintenant vous connecter &agrave; notre site en utilisant vos identifiants personnels.\n

Votre login est&nbsp;: @login@
et votre mot de passe vous venez de le saisir.

Merci de Votre Confiance

L\'&eacute;quipe de @nom_site@',
'compte_active' => 'Votre compte a &eacute;t&eacute; activ&eacute;',
'suppression_faite' => 'la suppression de vos donn&eacute;es a &eacute;t&eacute; effectu&eacute;',

'janvier' => 'Januar',
'fevrier' => 'Februar',
'mars' => 'M&auml;rz',
'avril' => 'April',
'mai' => 'Mai',
'juin' => 'Juni',
'juillet' => 'Juli',
'aout' => 'August',
'septembre' => 'September',
'octobre' => 'Oktober',
'novembre' => 'November',
'decembre' => 'Dezember',

'accesrestreint' => 'Eingeschr�nkter Zugang',
'choisir_zones' => 'Veuillez choisir les zones auxquelles vous voulez que les nouveaux adh&eacute;rents soient affect&eacute;s',
'domaines' => 'Domaine',
'exp_domaines' => 'Restreindre l\'acc&egrave;s &agrave; une liste predefinie des domaines',
'exp_domaines2' => 'Veuillez definir vos domaines dans le fichier inc/domaines.php en suivant le mod&egrave;le.',
'domaine' => 'Domaine',
'choix_domaine' => 'Veuillez choisir votre domaine',
'mail_non_domaine' => 'L\'adresse email que vous avez donn�e n\'appartient pas au domaine que vous avez indiqu&eacute; vueillez essayer &agrave; nouveau',
'format' => 'Format',
'expl_format' => 'Veuillez choisir le format que vous pr&eacute;f&eacute;rez pour recevoir les emails des listes de diffusion',
'texte' => 'Text', 
'html' => 'HTML',

'gestion_adherent' => 'Genstion d\'adh&eacute;rents',
'configuration' => 'Konfiguration',
'ajouter_adherent' => 'Ajouter adh&eacute;rent',
'liste_adherents' => 'Liste d\'adh&eacute;rents',
'nettoyer_tables' => 'Tische zu reinigen',
'raccourcis' => 'Raccourcis',
'supprimer_adherent_red' => 'Sup',
'supprimer_adherent' => 'Supprimer adh&eacute;rent',
'editer_adherent' => 'Editer adh&eacute;rent',
'fiche_adherent' => 'Fiche adh&eacute;rent',
'action_adherent' => 'Action adh&eacute;rent',
'auteur' => 'Autor',
'aconfirmer' => 'A confirmer',
'statut' => 'Statut',
'infos_adherent' => 'Informations suppl&eacute;mentaires',
'visiteur' => 'Besucher',
'exp_statut' => 'Choisissez le statut que vous voulez attribuer aux nouveaux adh&eacute;rents',
'pays' => 'Land',
'commentaire' => 'Kommentar',
'validite' => 'Date de validit&eacute;',
'exp_validite' => 'Champ utile avec le plugin <strong>ABONNEMENT</strong>, &agrave; cocher s\'il est install&eacute;',

//profil
'votre_profil' => 'Ihr Profil',
'contacts_personnels' => 'Pers�nliche Kontakte',
'contacts_pros' => 'Professionelle Kontakte',
'votre_adresse' => 'Ihre pers�nliche Adresse',
'votre_adresse_pro' => 'Ihre professionelle Adresse',
'votre_nom_complet' => '<NEW>Votre nom complet',
'vos_contacts_personnels' => '<NEW>Vos contacts personnels',
'website' => '<NEW>Site Internet',

//validation des champs
'champ_obligatoire' => '<NEW>Ce champ est obligatoire',
'chaine_valide' => '<NEW>Veuillez ins&eacute;rer une chaine de caract&egrave;re',
'cp_valide' => '<NEW>Veuillez ins&eacute;rer un code postal valide',
'numero_valide' => '<NEW>Veuillez ins&eacute;rer un num&eacute;ro valide',
'caracteres' => '<NEW>caract&egrave;res',
'chainenombre' => '<NEW>(compos&eacute;e de lettres et/ou de chiffres)',
'chainelettre' => '<NEW>(compos&eacute;e uniquement de lettres)',
'email_valide' => '<NEW>Veuillez ins&eacute;rer un email valide',

'titre_confirmation' => 'Confirmation',
'page_confirmation' => 'Seite der Best�tigung Ihrer Einschreibung'

);

?>
