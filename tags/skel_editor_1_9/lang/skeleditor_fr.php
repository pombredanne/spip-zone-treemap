<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

//$GLOBALS['i18n_skeleditor_fr'] = array( ??
$GLOBALS[$GLOBALS['idx_lang']] = array(
  // C
  'creer'=>'Cr&eacute;er', 
  
  // E  
  'editer_skel'=>'Editer le squelette',
  'editer_skel_debug' =>'Editer le squelette (mode debug)',
  'effacer'=>"effacer",
  'effacer_confirme'=>'Voulez vous vraiment effacer ce fichier ?',
  'erreur_ouverture_fichier'=>"erreur: impossible d\'ouvrir le fichier",
  'erreur_ecriture_fichier'=>"erreur: impossible d\'&eacute;crire dans le fichier",
  'erreur_edition_ecriture'=>"erreur: fichier non &eacute;ditable en &ecute;criture",
  'erreur_ouvert_ecrit'=>"Erreur: impossible d'ouvrir ou d'éditer ce fichier.",
  'erreur_droits'=>"erreur: impossible de cr&eacute;er le fichier. V&eacute;rifier les droits en &eacute;critutre de votre r&eacute;pertoire squelette",
  'erreur_overwrite'=>"erreur: ce fichier existe d&eacute;j&agrave;",
  'erreur_sansgene'=>'Erreur: action non autoris&eacute;e',
  'erreur_parsing'=>'Erreur fatale lors de l\'analyse des boucles',
  
  // F
  'fichier'=>"fichier",
  'fichier_sauvegarde_date'=>"fichier sauvegard&eacute; @ ",    
  'fichier_efface_ok'=>'Fichier effac&eacute; avec succ&egrave;s!',
  'fichier_choix'=>'Choississez le fichier que vous voulez &eacute;diter ou visualiser.',
  'fichier_nouveau'=>'Nouveau fichier',
  'fichier_upload'=>'Uploader un fichier',
  'fichier_upload_ok'=>'Fichier upload&eacute; avec succ&egrave;s!',
  
  // P
  'parseur_titre' => 'Analyseur des boucles',
  'parseur_contenu' => 'Contenu :',
  'parseur_avant' => 'Avant',
  'parseur_milieu' => 'Milieu',
  'parseur_apres' => 'Apres',
  'parseur_altern' => 'Alternative',
  'parseur_param' => 'Param&egrave;tres:',
  'parseur_horsboucle' => 'HORS BOUCLE',
 
  
  // R 
  'repertoire'=>'r&eacute;pertoire',
  
  // S
  'sauver'=>'Sauver', 
  'skeleditor_description'=>"Permet d'&eacute;diter les fichiers du squelette en cours",
  'skeleditor_dossier'=>"dossier squelette: ",
  
  // T
  'target'=>'position',
  'telecharger'=>"t&eacute;l&eacute;charger",
  
  // U
  'upload'=>'Uploader',  
);


?>
