<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'configuration_description' => 'Permet de configurer ce que l\'on d&eacute;sire enregistrer ou pas.',
	'configuration_enregistrer_visite' => 'Enregistrer toutes les visites des personnes ayant un compte',
	'configuration_enregistrer_visite_article' => 'Enregistrer le temps passé sur un article des personnes ayant un compte',
	'configuration_titre' => 'Configuration de Big Brother',
	'date_debut' => 'Entr&eacute;e',
	'date_fin' => 'Sortie',
	'detail' => 'Voir le d&eacute;tail',
	'erreur_statistiques' => '"id_article" ou "id_auteur" manquant',
	'temps_median' => 'Temps m&eacute;dian',
	'temps_moyen' => 'Temps moyen',
	'temps_passe' => 'Temps pass&eacute;',
	'temps_total' => 'Temps total',
	'visites_article_auteur' => 'Visites de @nom@ sur l\'article <em>@titre@</em>',
	'voir_statistiques_article' => 'Statistiques de visite de l\'article',
	'voir_statistiques_auteur' => 'Statistiques de visite de cette personne'

);

?>
