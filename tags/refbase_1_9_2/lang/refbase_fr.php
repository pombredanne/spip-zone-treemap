<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'cfg_css' => 'Permet de rajouter des styles CSS au div englobant la liste des r&eacute;sultats (css). Par exemple : <i>font-size:1.25em;</i> pour agrandir le texte.',
'cfg_doublons' => 'Afficher les doublons (doublons)&nbsp;:',
'cfg_liens' => 'Afficher les liens externes (liens)&nbsp;:',
'cfg_liens_exports' => 'Afficher les abstracts et les options d\'export (liens_exports)&nbsp;:',
'cfg_max' => 'Nombre maximum de r&eacute;sultats affich&eacute;s (max)&nbsp;:',
'cfg_style' => 'Style pour la mise en forme des citations (style)&nbsp;:',
'cfg_tri' => 'Trier les r&eacute;sultats par (tri)&nbsp;:',
'cfg_tri_annee' => 'Ann&eacute;e (annee)',
'cfg_tri_auteur' => 'Auteur (auteur)',
'cfg_tri_type' => 'Type de publication (type)',
'cfg_tri_typeannee' => 'Type de publication puis par ann&eacute;e (type-annee)',
'cfg_tri_datecreation' => 'Date de cr&eacute;ation (date-creation)',
'cfg_url_refbase' => 'URL de la base refbase avec le / final (url_refbase)&nbsp;:',
'cfg_vue' => 'Vue utilis&eacute;e (vue)&nbsp;:',
'cfg_vue_citations' => 'Citations (citations)',
'cfg_vue_liste' => 'Liste en colonnes (liste)',
'cfg_vue_details' => 'Montrer tous les d&eacute;tails (details)',
'comportement_citations' => 'Param&egrave;tres par d&eacute;faut pour la vue Citations',
'comportement_defaut' => 'Param&egrave;tres par d&eacute;faut',
'config_refbase' => 'Configuration du plugin refbase',
'non' => 'non',
'option_style' => 'Avanc&eacute; : option de style (facultatif)',
'oui' => 'oui'

);
?>
