﻿<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'afficher_calendrier' => 'إظهار الأجندة',
'annee_precedente' => 'السنة الفائتة',
'annee_suivante' => 'السنة القادمة',
'annuler_recherche' => 'إلغاء البحث',

'bouton_fermer' => "إغلاق",

'mois_precedent' => 'الشهر الفائت',
'mois_suivant' => 'الشهر القادم',
'id_rapide' => 'إضافة سريعة',
'pages' => 'الصفحات',


);


?>