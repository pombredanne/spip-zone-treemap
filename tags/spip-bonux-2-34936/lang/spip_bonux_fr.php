<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'afficher_calendrier' => 'Afficher le calendrier',
'annee_precedente' => 'An&eacute;e pr&eacute;c&eacute;dente',
'annee_suivante' => 'Ann&eacute;e suivante',
'annuler_recherche' => 'Annuler la recherche',

'bouton_fermer' => "Fermer",

'mois_precedent' => 'Mois pr&eacute;c&eacute;dent',
'mois_suivant' => 'Mois suivant',
'id_rapide' => 'Ajout rapide',
'pages' => 'Pages',


);


?>