<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP nomm� admin_lang genere le NOW()
// langue / language = fr

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'about' => '&Agrave;&nbsp;propos',
'accueil_site' => 'Accueil',
'archives' => 'Archives',
'article' => 'Vous lisez...',
'auteur' => 'archives de l&#39;auteur',
'auteur_count' => 'a &eacute;crit',

// B
'browse' => 'Explorer',
'breves' => 'Br&egrave;ves',
'billet' => 'billet',
'billets' => 'billets',

//C
'category_count' => 'Cette cat&eacute;gorie contient',
'comment' => 'commentaire',
'comments' => 'commentaires',
'continue_reading' => 'Lire la suite',

//D
'dernier_article' => 'Dernier billet',
'derniers_articles' => 'Billets r&eacute;cents',
'discussion' => 'Commentaires',

//E
'email_auteur' => '&Eacute;crire &agrave; l&#39;auteur',

//F
'featured_post' => 'S&eacute;lection',
'for' => 'pour',

//R
'retour_accueil' => 'Perdu ? Retour &agrave; la page d&#39;accueil',
'rubriques' => 'Cat&eacute;gories',
'recherche' => 'Recherche',
'rss_section' => 'Fil RSS pour cette rubrique',

//S
'subscribe' => 'Abonnement',

//T
'tags' => '&Eacute;tiquettes',

//W
'welcome' => 'Bienvenue sur'

);

?>