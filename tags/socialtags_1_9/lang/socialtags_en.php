<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	//C
	'cfg_1' => 'Social tags plugin configuration',
	'cfg_2' => 'Ici vous pouvez choisir les sites de partages de vos articles.',
	'cfg_3' => 'Documentation',
	'cfg_tout_cocher' => 'Check all',
	'cfg_tout_decocher' => 'Uncheck all',
	'cfg_insertion' => 'Insertion into web pages:',
	'cfg_inserer' => 'Insert the social-tags block below the following tag:',
	'cfg_selector' => 'Selector:',
	'cfg_exemples' => 'Examples (using the dist templates):',
	'cfg_en_bas' => 'At the bottom of each page',
	'cfg_sous_bloc' => 'Beneath the title block on the article pages',
	'cfg_sous_div' => 'Beneath the div named id=socialtags',
	'cfg_badge_fb' => 'Facebook Badge/Widget',
	'cfg_badge_fb_existe' => 'If you have a Facebook Badge (',
	'cfg_badge_fb_aide' => 'see help on the Facebook site',
	'cfg_badge_fb_style' => '), specify its contents below, either as JS or as HTML.',
	'cfg_badge_html' => 'HTML Badge',
	'cfg_badge_html_ajouter_1' => '(add to your own templates using',
	'cfg_badge_html_ajouter_2' => ', or in an article with ',
	'cfg_badge_js' => 'JS Badge',
	'cfg_badge_js_1' => ' (automatically included in ',
	'cfg_referer' => 'Only display the badge and buttons for native visitors of the sites concerned.',
	'cfg_titre_socialtags' => 'Social tags'
	
);
?>
