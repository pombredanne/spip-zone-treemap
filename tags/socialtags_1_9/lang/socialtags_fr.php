<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	//C
	'cfg_1' => 'Configuration du plugin Social tags',
	'cfg_2' => 'Ici vous pouvez choisir les sites de partages de vos articles.',
	'cfg_3' => 'Documentation',
	'cfg_tout_cocher' => 'Tout cocher',
	'cfg_tout_decocher' => 'Tout d&#233;cocher',
	'cfg_insertion' => 'Insertion dans les pages :',
	'cfg_inserer' => 'Ins&#233;rer le bloc social-tags en-dessous du bloc suivant :',
	'cfg_selector' => 'S&#233;lecteur :',
	'cfg_exemples' => 'Exemples (sur les squelettes dist) :',
	'cfg_en_bas' => 'En bas de chaque page',
	'cfg_sous_bloc' => 'Sous le bloc de titre des pages article',
	'cfg_sous_div' => 'Sous la div nomm&#233;e id=socialtags',
	'cfg_badge_fb' => 'Badge/Widget Facebook',
	'cfg_badge_fb_existe' => 'Si vous avez un Badge Facebook (',
	'cfg_badge_fb_aide' => 'aide sur le site FB',
	'cfg_badge_fb_style' => '), indiquez ci-dessous son contenu, soit en JS, soit en HTML.',
	'cfg_badge_html' => 'Badge HTML',
	'cfg_badge_html_ajouter_1' => '(ajouter dans votre squelette',
	'cfg_badge_html_ajouter_2' => ', ou dans un article',
	'cfg_badge_js' => 'Badge JS',
	'cfg_badge_js_1' => ' (automatiquement inclus dans ',
	'cfg_referer' => 'N\'afficher le badge et les boutons que pour les visiteurs originaires des sites concern&#233;s.',
	'cfg_titre_socialtags' => 'Social tags'
	
);
?>
