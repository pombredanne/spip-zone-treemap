<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
    '404'                   =>'Page introuvable',
    'sites'                  =>'Sites &agrave; consulter ...',
    'aucun_resultats'       =>'Aucun r&eacute;sultat ...',
    '404_explication'       =>'La page demand&eacute;e n\'existe pas ... d&eacute;sol&eacute;',
    "accueil"               => 'Accueil',
    'categorie'            => 'Classement',
    'collection_aleatoire_sing' => 'Un objet tir&eacute; de mani&egrave;re al&eacute;atoire',
    'collection_aleatoire_plu'  => '@total@ objets tir&eacute;s de mani&egrave;re al&eacute;atoire',
    'choisir_un_critere'    => 'Merci de choisir au moins un crit&egrave;re.', 
    'criteres_recherche'    =>'Vous avez fait une recherche avec les crit&egrave;res suivants :',
    'cfg'                   => 'Configuration du squelette "Collection"',
    'classement'            => 'Classement par :',
    "contact"               => 'Contact',
    'date_ajout'            => 'Date d\'ajout',
    'date_objet'            => 'Date de l\'objet',
    'explication_pluri_criteres'    => 'Vous pouvez faire une recherche pluri-crit&egrave;res',
    'indifferent'           =>'Indiff&eacute;rent',
    'objets'                =>'Objets',
    'pluri_criteres'        =>'Recherche pluri-crit&egrave;res',
    'pluri_criteres_nul'    =>'Aucun r&eacute;sultat pour la recherche pluri-crit&egrave;re',
    'plus_infos'            =>"Plus d'infos",
    'presentation_stat'     =>"Tout ce que vous avez toujour voulu savoir sur la collection, sans jamais avoir os&eacute; le demander !",
    'resultats'             =>'R&eacute;sultats : ',
    'plan'                  =>'Plan du site',
    'statistiques'          =>'Statistiques',
    'stat_generales'        =>'Statistiques g&eacute;n&eacute;rales',
    'stat_par_critere'      =>'Statistiques par critères',
    'stat_pour_critere'     =>'Statistiques pour le crit&egrave;re «@crit@»',
    'total'                 =>'Total',
    'squelette'            =>'Squelette “<a href="http://www.spip-contrib.net/Squelette-collection,3148" class="spip_out">Collection</a>”',
    'recherche_explication' =>'La recherche se fait avec le moteur de [SPIP->http://www.spip.net] uniquement sur la description des objets, pour une recherche plus approfondie, utiliser un [?moteur de recherche].',
    'hebergeur'             =>'H&eacute;berg&eacute; par <a class="spip_out" href="@url@">@nom@</a>',
    'voir_en_ligne'         =>'Voir en ligne',
    'abonnement'            =>'Lettre d\'informations',
    'sondage'                   =>'Sondage',
    
    
    //cfg
    'cfg_proposer_recherche'    =>'Proposer de rechercher avec le moteurs de SPIP',
    'cfg_pages'                 =>'Pages disponibles',
    'cfg_afficher_date'         =>'Afficher la date d\'ajout &agrave; la collection (date de publication)',
    'cfg_afficher_date_redac'         =>'Afficher la date de l\'objet (date de r&eacute;daction ant&eacute;rieure)',
    'cfg_tout_objet'            =>'Tout les objets',
    'cfg_colorer_fond_texte'     =>'Colorer les fonds de texte (descriptif d\'un objet, introduction &agrave; une cat&eacute;gorie)',
    'cfg_liens_nav'                 =>'Couleurs des liens de navigations',
    'cfg_liens_spip_in'         =>'Couleurs des liens SPIP internes',
    'cfg_liens_spip_out'        =>'Couleurs des liens SPIP externes',
    'cfg_liens_spip_glossaire'  =>'Couleurs des liens SPIP vers le glossaire (Wikipédia)',
    'cfg_couleur_liens'         =>'Couleur des liens',
    'cfg_pagination'            =>'Nombre maximum d\'objets par page (x3 ; 0 si illimit&eacute;)',
    'cfg_couleur_titres'         =>'Couleur des titres',
    'cfg_fond_sepia'            =>'Couleur du d&eacute;grad&eacute; de fond de page (gauche)',
    'cfg_afficher_hr'           =>'Afficher les filets de s&eacute;paration horizontaux',
    'cfg_couleur_filets'        =>'Couleurs des filets de s&eacute;paration',
    'cfg_presentation'          =>'Configuration de la pr&eacute;sentation',
    'cfg_contenu'               =>'Configuration du contenu du site', 
    'cfg_menu'                  =>'Que mettre dans le menu de navigation ?',
    'cfg_sommaire'              =>'Que mettre sur la page sommaire ?',
    'cfg_plan'                  =>'Le plan du site',
    'cfg_alea9'                 =>'Neuf objets tir&eacute;s de mani&egrave;re al&eacute;atoire',
    'cfg_url_hebergeur'         =>'Adresse de l\'h&eacute;bergeur',
    'cfg_nom_hebergeur'         =>'Nom de l\'h&eacute;bergeur',
    'cfg_nyceros_pagination'    =>'Proposer de prendre les images non affich&eacute;es dans Nyceros',
    'cfg_infos_site'            =>'Informations sur le site'

);  

?>
