<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => '&Ouml;sszefoglal&aacute;sok megjelen&iacute;t&eacute;se',
	'afficher_sources' => 'Forr&aacute;s megjelen&iacute;t&eacute;se',
	'annee' => '&Eacute;v',
	'articles_recents_court' => '&Uacute;jabb cikkek',

	// C
	'connexion' => 'Csatlakoz&aacute;s',

	// D
	'deconnexion' => 'Kijelentkez&eacute;s',
	'derniere_syndication' => 'E honlap leg&uacute;jabb szindik&aacute;l&aacute;sa megt&ouml;rt&eacute;nt',
	'deuxjours' => 'K&eacute;t nap',

	// L
	'liens' => 'cikk',
	'liens_pluriel' => 'cikk',

	// M
	'masquer_resume' => '&Ouml;sszefoglal&aacute;sok rejt&eacute;se',
	'masquer_sources' => 'Forr&aacute;s rejt&eacute;se',
	'mois' => 'H&oacute;nap',

	// P
	'pas_articles' => 'Nincs cikk abban az id&#337;szakban&nbsp;!',
	'pas_synchro' => 'Nincs szinkroniz&aacute;l&aacute;s',
	'preferences' => 'Be&aacute;ll&iacute;t&aacute;sok',
	'probleme_de_syndication' => 'szindik&aacute;l&aacute;si hiba',

	// S
	'semaine' => 'H&eacute;t',
	'sources' => 'Forr&aacute;sok',
	'synchro' => 'Szinkroniz&aacute;l&aacute;s',
	'synchro_titre' => 'Menteni a honlapon az olvasott cikkek list&aacute;j&aacute;t',
	'syndication_ajour' => 'Friss&iacute;teni most',
	'syndication_fait' => 'Szindik&aacute;l&aacute;s megt&ouml;rt&eacute;nt',

	// T
	'toutes' => 'Mindegyik'
);

?>
