<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Afichar lu resumits',
	'afficher_sources' => 'Afichar aquela f&ograve;nt',
	'annee' => 'An',
	'articles_recents_court' => 'Articles recents',

	// C
	'connexion' => 'Connexion',

	// D
	'deconnexion' => 'Desconnexion',
	'derniere_syndication' => 'La darriera sindicacion facha d\'aqueu sit',
	'deuxjours' => 'Doi jorns',

	// L
	'liens' => 'article',
	'liens_pluriel' => 'articles',

	// M
	'masquer_resume' => 'Mascar lu resumits',
	'masquer_sources' => 'Mascar aquela f&ograve;nt',
	'mois' => 'Mes',

	// P
	'pas_articles' => 'I a minga d\'article dins aqueu peri&ograve;de!',
	'pas_synchro' => 'Non sincronizar ',
	'preferences' => 'Prefer&eacute;ncias',
	'probleme_de_syndication' => 'Probl&egrave;ma de sindicacion',

	// S
	'semaine' => 'Setmana',
	'sources' => 'F&ograve;nts',
	'synchro' => 'Sincronizar',
	'synchro_titre' => 'Registrar dins aqueu sit la lista dei articles legits',
	'syndication_ajour' => 'Actualizar a&uuml;ra',
	'syndication_fait' => 'La sincronizacion s\'es facha',

	// T
	'toutes' => 'Toti'
);

?>
