<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Amosar los resumes',
	'afficher_sources' => 'Amosar esta fonte',
	'annee' => 'A&ntilde;u',
	'articles_recents_court' => 'Art&iacute;culos recientes',

	// C
	'connexion' => 'Conex&oacute;n',

	// D
	'deconnexion' => 'Desconex&oacute;n',
	'derniere_syndication' => 'La cabera sindicaci&oacute;n d\'esti sitiu f&iacute;zose',
	'deuxjours' => 'Dos d&iacute;es',

	// L
	'liens' => 'art&iacute;culu',
	'liens_pluriel' => 'art&iacute;culos',

	// M
	'masquer_resume' => 'Esconder los resumes',
	'masquer_sources' => 'Esconder esti orixe',
	'mois' => 'Mes',

	// P
	'pas_articles' => 'Ensin art&iacute;culos nesti periodu',
	'pas_synchro' => 'Non sincronizar',
	'preferences' => 'Preferencies',
	'probleme_de_syndication' => 'problema cola sindicaci&oacute;n',

	// S
	'semaine' => 'Selmana',
	'sources' => 'Fontes',
	'synchro' => 'Sincronizar',
	'synchro_titre' => 'Grabar nel sitiu la llista d\'art&iacute;culos lle&iacute;os',
	'syndication_ajour' => 'Actualizar agora',
	'syndication_fait' => 'Fecha la sindicaci&oacute;n',

	// T
	'toutes' => 'Toes'
);

?>
