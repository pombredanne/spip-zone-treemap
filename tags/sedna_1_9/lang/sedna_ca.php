<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Mostrar els resums',
	'afficher_sources' => 'Mostrar aquesta font',
	'annee' => 'Any',
	'articles_recents_court' => 'Articles recents',

	// C
	'connexion' => 'Connexi&oacute;',

	// D
	'deconnexion' => 'Desconnexi&oacute;',
	'derniere_syndication' => 'La darrera sindicaci&oacute; realitzada d\'aquest lloc',
	'deuxjours' => 'Dos dies',

	// L
	'liens' => 'article',
	'liens_pluriel' => 'articles',

	// M
	'masquer_resume' => 'Amagar els resums',
	'masquer_sources' => 'Amagar aquesta font',
	'mois' => 'Mes',

	// P
	'pas_articles' => 'Cap article durant aquest periode&nbsp;!',
	'pas_synchro' => 'No sincronitzar',
	'preferences' => 'Prefer&egrave;ncies',
	'probleme_de_syndication' => 'problema de sindicaci&oacute;',

	// S
	'semaine' => 'Setmana',
	'sources' => 'Fonts',
	'synchro' => 'Sincronitzar',
	'synchro_titre' => 'Deseu al lloc Web el llistat dels articles llegits',
	'syndication_ajour' => 'Actualitzar ara',
	'syndication_fait' => 'Sindicaci&oacute; feta',

	// T
	'toutes' => 'Totes'
);

?>
