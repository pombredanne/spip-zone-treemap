<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Visa sammanfattningarna',
	'afficher_sources' => 'Visa denna syndikering',
	'annee' => 'Senaste &aring;ret',
	'articles_recents_court' => 'De senaste artiklarna',

	// C
	'connexion' => 'Logga in',

	// D
	'deconnexion' => 'Logga ut',
	'derniere_syndication' => 'Den senaste syndikeringen genomf&ouml;rdes',
	'deuxjours' => 'Senaste tv&aring; dagarna',

	// L
	'liens' => 'inl&auml;gg',
	'liens_pluriel' => 'inl&auml;gg',

	// M
	'masquer_resume' => 'G&ouml;m sammanfattningarna',
	'masquer_sources' => 'G&ouml;m den h&auml;r syndikeringen',
	'mois' => 'Senaste m&aring;naden',

	// P
	'pas_articles' => 'Det finns inga artiklar under denna period.',
	'pas_synchro' => 'Synkronisera inte',
	'preferences' => 'Inst&auml;llningar',
	'probleme_de_syndication' => 'Syndikeringsproblem',

	// S
	'semaine' => 'Senaste veckan',
	'sources' => 'Syndikeringsk&auml;llor',
	'synchro' => 'Synkronisera',
	'synchro_titre' => 'Spara listan &ouml;ver l&auml;sta meddelanden p&aring; webbplatsen',
	'syndication_ajour' => 'Uppdatera nu',
	'syndication_fait' => 'Syndikeringen klar',

	// T
	'toutes' => 'Alla'
);

?>
