<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Afiche tout rezime',
	'afficher_sources' => 'Afiche sous la',
	'annee' => 'Ane',
	'articles_recents_court' => 'Tout atik pli fr&egrave;',

	// C
	'connexion' => 'Koneksyon',

	// D
	'deconnexion' => 'Rete Koneksyon',
	'derniere_syndication' => 'Deny&egrave; sendikasyon sit-la te fin fini ',
	'deuxjours' => 'D&egrave; jour',

	// L
	'liens' => 'atik',
	'liens_pluriel' => 'atik yo',

	// M
	'masquer_resume' => 'Sere tout rezime',
	'masquer_sources' => 'Sere sous-la',
	'mois' => 'Mwa',

	// P
	'pas_articles' => 'Genyen pa oken atik nan p&eacute;yod sila&nbsp;!',
	'pas_synchro' => 'Di pa senkwonize',
	'preferences' => 'Kisa w prefere',
	'probleme_de_syndication' => 'pwobl&egrave;m ak sendikasyon',

	// S
	'semaine' => 'Sem&egrave;n',
	'sources' => 'Tout sous',
	'synchro' => 'Senkwonize',
	'synchro_titre' => 'Enskri sou sit sa-a lali atik yo ki pli li',
	'syndication_ajour' => 'Rafr&egrave;chi imedyatman',
	'syndication_fait' => 'Sendikasyon ki fini',

	// T
	'toutes' => 'Tout'
);

?>
