<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Mostrar os resumos',
	'afficher_sources' => 'Mostrar esta fonte',
	'annee' => 'Ano',
	'articles_recents_court' => 'Artigos recentes',

	// C
	'connexion' => 'Liga&ccedil;&atilde;o',

	// D
	'deconnexion' => 'Desconex&atilde;o',
	'derniere_syndication' => '&Uacute;ltima sindica&ccedil;&atilde;o deste s&iacute;tio efectuada',
	'deuxjours' => 'Dois dias',

	// L
	'liens' => 'artigo',
	'liens_pluriel' => 'artigos',

	// M
	'masquer_resume' => 'Ocultar os resumos',
	'masquer_sources' => 'Ocultar esta fonte',
	'mois' => 'M&ecirc;s',

	// P
	'pas_articles' => 'N&atilde;o h&aacute; artigos neste per&iacute;odo&nbsp;!',
	'pas_synchro' => 'N&atilde;o sincronizar',
	'preferences' => 'Prefer&ecirc;ncias',
	'probleme_de_syndication' => 'problema de sindica&ccedil;&atilde;o',

	// S
	'semaine' => 'Semana',
	'sources' => 'Fontes',
	'synchro' => 'Sincronizar',
	'synchro_titre' => 'Gravar no s&iacute;tio a lista dos artigos lidos',
	'syndication_ajour' => 'Actualizar agora',
	'syndication_fait' => 'Sindica&ccedil;&atilde;o efectuada',

	// T
	'toutes' => 'Todas'
);

?>
