<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Afichar lu resumits',
	'afficher_sources' => 'Afichar aquela foant',
	'annee' => 'An',
	'articles_recents_court' => 'Articles recents',

	// C
	'connexion' => 'Connexion',

	// D
	'deconnexion' => 'Desconnexion',
	'derniere_syndication' => 'La darriera sindicacion facha d\'aqueu sit',
	'deuxjours' => 'Doi jorns',

	// L
	'liens' => 'article',
	'liens_pluriel' => 'articles',

	// M
	'masquer_resume' => 'Mascar lu resumits',
	'masquer_sources' => 'Mascar aquela foant',
	'mois' => 'Mes',

	// P
	'pas_articles' => 'Li a minga d\'article dins aquest peri&ograve;de&nbsp;!',
	'pas_synchro' => 'Non sincronisar ',
	'preferences' => 'Prefer&eacute;n&ccedil;as',
	'probleme_de_syndication' => 'Probl&egrave;ma de sindicacion',

	// S
	'semaine' => 'Setmana',
	'sources' => 'F&ograve;nts',
	'synchro' => 'Sincronisar',
	'synchro_titre' => 'Registrar dins aqueu sit la ti&egrave;ra dei articles legits',
	'syndication_ajour' => 'Actualisar a&uuml;ra',
	'syndication_fait' => 'La sincronisacion s\'es facha',

	// T
	'toutes' => 'Toti'
);

?>
