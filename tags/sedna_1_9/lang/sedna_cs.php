<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Zobrazovat souhrny',
	'afficher_sources' => 'Zobrazit tento zdroj',
	'annee' => 'Uplynul&yacute; rok',
	'articles_recents_court' => 'P&#345;&iacute;sp&#283;vky z posledn&iacute; doby',

	// C
	'connexion' => 'U&#382;ivatelsk&eacute; jm&eacute;no',

	// D
	'deconnexion' => 'Odhl&aacute;&#353;en&iacute;',
	'derniere_syndication' => 'Datum posledn&iacute;ho zpracov&aacute;n&iacute; dat na t&#283;chto internetov&yacute;ch str&aacute;nk&aacute;ch',
	'deuxjours' => 'Dva dny',

	// L
	'liens' => '&#269;l&aacute;nek',
	'liens_pluriel' => '&#269;l&aacute;nky',

	// M
	'masquer_resume' => 'Skr&yacute;t souhrny',
	'masquer_sources' => 'Skr&yacute;t tento zdroj',
	'mois' => 'M&#283;s&iacute;c',

	// P
	'pas_articles' => 'V zadan&eacute;m obdob&iacute; nejsou &#382;&aacute;dn&eacute; &#269;l&aacute;nky!',
	'pas_synchro' => 'Nesynchronizovat',
	'preferences' => 'Nastaven&iacute;',
	'probleme_de_syndication' => 'Probl&eacute;m se zpracov&aacute;n&iacute;m dat',

	// S
	'semaine' => 'T&yacute;den',
	'sources' => 'Zdroje',
	'synchro' => 'Synchronizovat',
	'synchro_titre' => 'Ulo&#382;it seznam p&#345;e&#269;ten&yacute;ch p&#345;&iacute;sp&#283;vk&#367; na webu',
	'syndication_ajour' => 'Aktualizovat',
	'syndication_fait' => 'Zpracov&aacute;n&iacute; dat provedeno',

	// T
	'toutes' => 'V&#353;echny'
);

?>
