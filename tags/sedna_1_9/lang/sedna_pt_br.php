<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Exibir os resumos',
	'afficher_sources' => 'Exibir esta fonte',
	'annee' => 'Ano',
	'articles_recents_court' => 'Mat&eacute;rias recentes',

	// C
	'connexion' => 'Conex&atilde;o',

	// D
	'deconnexion' => 'Desconex&atilde;o',
	'derniere_syndication' => '&Uacute;ltima sindica&ccedil;&atilde;o deste site efetuada',
	'deuxjours' => 'Dois dias',

	// L
	'liens' => 'mat&eacute;ria',
	'liens_pluriel' => 'mat&eacute;rias',

	// M
	'masquer_resume' => 'Esconder os resumos',
	'masquer_sources' => 'Esconder esta fonte',
	'mois' => 'M&ecirc;s',

	// P
	'pas_articles' => 'Nenhuma mat&eacute;ria neste per&iacute;odo!',
	'pas_synchro' => 'N&atilde;o sincronizar',
	'preferences' => 'Prefer&ecirc;ncias',
	'probleme_de_syndication' => 'problema de sindica&ccedil;&atilde;o',

	// S
	'semaine' => 'Semana',
	'sources' => 'Fontes',
	'synchro' => 'Sincronizar',
	'synchro_titre' => 'Registrar no site a lista de mat&eacute;rias lidas',
	'syndication_ajour' => 'Atualizar agora',
	'syndication_fait' => 'Sindica&ccedil;&atilde;o efetuada',

	// T
	'toutes' => 'Todas'
);

?>
