<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => '&#6036;&#6020;&#6098;&#6048;&#6070;&#6025; &#6050;&#6031;&#6098;&#6032;&#6036;&#6033;&#6047;&#6020;&#6098;&#6017;&#6081;&#6036;',
	'afficher_sources' => '&#6036;&#6020;&#6098;&#6048;&#6070;&#6025; &#6036;&#6098;&#6042;&#6039;&#6038;&#6035;&#6081;&#6087;',
	'annee' => '&#6022;&#6098;&#6035;&#6070;&#6086;',
	'articles_recents_court' => '&#6050;&#6031;&#6098;&#6032;&#6036;&#6033;&#6032;&#6098;&#6040;&#6072;&#6103;',

	// C
	'connexion' => '&#6036;&#6025;&#6098;&#6023;&#6070;&#6036;&#6091;',

	// D
	'deconnexion' => '&#6038;&#6071;&#6035;&#6071;&#6031;&#6098;&#6041;&#6021;&#6081;&#6025;',
	'derniere_syndication' => 'Derni&egrave;re syndication de ce site effectu&eacute;e', # NEW
	'deuxjours' => '&#6038;&#6072;&#6042;&#6032;&#6098;&#6020;&#6083;',

	// L
	'liens' => '&#6050;&#6031;&#6098;&#6032;&#6036;&#6033;',
	'liens_pluriel' => '&#6050;&#6031;&#6098;&#6032;&#6036;&#6033; &#6035;&#6070;&#6035;&#6070;',

	// M
	'masquer_resume' => '&#6036;&#6071;&#6033;&#6036;&#6070;&#6086;&#6020; &#6050;&#6031;&#6098;&#6032;&#6036;&#6033;&#6047;&#6020;&#6098;&#6017;&#6081;&#6036;',
	'masquer_sources' => '&#6036;&#6071;&#6033;&#6036;&#6070;&#6086;&#6020; &#6036;&#6098;&#6042;&#6039;&#6038;&#6035;&#6081;&#6087;',
	'mois' => '&#6017;&#6082;',

	// P
	'pas_articles' => 'Pas d\'article dans cette p&eacute;riode&nbsp;!', # NEW
	'pas_synchro' => 'Ne pas synchroniser', # NEW
	'preferences' => '&#6021;&#6086;&#6030;&#6076;&#6043;&#6021;&#6071;&#6031;&#6098;&#6031;',
	'probleme_de_syndication' => 'probl&egrave;me de syndication', # NEW

	// S
	'semaine' => '&#6047;&#6036;&#6098;&#6031;&#6070;&#6048;&#6093;',
	'sources' => '&#6036;&#6098;&#6042;&#6039;&#6038;',
	'synchro' => 'Synchroniser', # NEW
	'synchro_titre' => 'Enregistrer sur le site la liste des articles lus', # NEW
	'syndication_ajour' => 'Mettre &agrave; jour maintenant', # NEW
	'syndication_fait' => 'Syndication effectu&eacute;e', # NEW

	// T
	'toutes' => '&#6033;&#6070;&#6086;&#6020;&#6050;&#6047;&#6091;'
);

?>
