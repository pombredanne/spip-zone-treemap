<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/sedna/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Afficher les r&eacute;sum&eacute;s',
	'afficher_sources' => 'Afficher cette source',
	'annee' => 'Ann&eacute;e',
	'articles_recents_court' => 'Articles r&eacute;cents',

	// C
	'connexion' => 'Connexion',

	// D
	'deconnexion' => 'D&eacute;connexion',
	'derniere_syndication' => 'Derni&egrave;re syndication de ce site effectu&eacute;e',
	'deuxjours' => 'Deux jours',

	// L
	'liens' => 'article',
	'liens_pluriel' => 'articles',

	// M
	'masquer_resume' => 'Masquer les r&eacute;sum&eacute;s',
	'masquer_sources' => 'Masquer cette source',
	'mois' => 'Mois',

	// P
	'pas_articles' => 'Pas d\'article dans cette p&eacute;riode&nbsp;!',
	'pas_synchro' => 'Ne pas synchroniser',
	'preferences' => 'Pr&eacute;f&eacute;rences',
	'probleme_de_syndication' => 'probl&egrave;me de syndication',

	// S
	'semaine' => 'Semaine',
	'sources' => 'Sources',
	'synchro' => 'Synchroniser',
	'synchro_titre' => 'Enregistrer sur le site la liste des articles lus',
	'syndication_ajour' => 'Mettre &agrave; jour maintenant',
	'syndication_fait' => 'Syndication effectu&eacute;e',

	// T
	'toutes' => 'Toutes'
);

?>
