<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Show summaries',
	'afficher_sources' => 'Show this feed',
	'annee' => 'Last year',
	'articles_recents_court' => 'Recent posts',

	// C
	'connexion' => 'Login',

	// D
	'deconnexion' => 'Logout',
	'derniere_syndication' => 'Last syndication of this feed performed',
	'deuxjours' => 'Last two days',

	// L
	'liens' => 'post',
	'liens_pluriel' => 'posts',

	// M
	'masquer_resume' => 'Hide summaries',
	'masquer_sources' => 'Hide this feed',
	'mois' => 'Last month',

	// P
	'pas_articles' => 'No post within this period of time!',
	'pas_synchro' => 'Do not synchronize',
	'preferences' => 'Preferences',
	'probleme_de_syndication' => 'Syndication problem',

	// S
	'semaine' => 'Last week',
	'sources' => 'Feeds',
	'synchro' => 'Synchronize',
	'synchro_titre' => 'Save the list of read posts on website',
	'syndication_ajour' => 'Update now',
	'syndication_fait' => 'Syndication done',

	// T
	'toutes' => 'All'
);

?>
