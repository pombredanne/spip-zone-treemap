<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Bildumak erakutsi',
	'afficher_sources' => 'Iturri hau erakutsi',
	'annee' => 'Urte',
	'articles_recents_court' => 'Azken artikuluak',

	// C
	'connexion' => 'Konexioa',

	// D
	'deconnexion' => 'Deskonexioa',
	'derniere_syndication' => 'Derni&egrave;re syndication de ce site effectu&eacute;e', # NEW
	'deuxjours' => 'Bi egun',

	// L
	'liens' => 'artikulua',
	'liens_pluriel' => 'artikuluak',

	// M
	'masquer_resume' => 'Bildumak maskatu',
	'masquer_sources' => 'Iturri hau maskatu',
	'mois' => 'Hilabete',

	// P
	'pas_articles' => 'Garai hunetan ez dago artikulurik&nbsp;!',
	'pas_synchro' => 'Ne pas synchroniser', # NEW
	'preferences' => 'Hobespenak',
	'probleme_de_syndication' => 'probl&egrave;me de syndication', # NEW

	// S
	'semaine' => 'Astea',
	'sources' => 'Iturriak',
	'synchro' => 'Synchroniser', # NEW
	'synchro_titre' => 'Enregistrer sur le site la liste des articles lus', # NEW
	'syndication_ajour' => 'Mettre &agrave; jour maintenant', # NEW
	'syndication_fait' => 'Syndication effectu&eacute;e', # NEW

	// T
	'toutes' => 'Denak'
);

?>
