<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Mostrar los res&uacute;menes',
	'afficher_sources' => 'Mostrar la fuente',
	'annee' => 'A&ntilde;o',
	'articles_recents_court' => 'Art&iacute;culos recientes',

	// C
	'connexion' => 'Conexi&oacute;n',

	// D
	'deconnexion' => 'Desconexi&oacute;n',
	'derniere_syndication' => 'La &uacute;ltima sindicaci&oacute;n de este sitio se efectu&oacute;',
	'deuxjours' => 'Dos d&iacute;as',

	// L
	'liens' => 'art&iacute;culo',
	'liens_pluriel' => 'art&iacute;culos',

	// M
	'masquer_resume' => 'Enmascarar los res&uacute;menes',
	'masquer_sources' => 'Enmascarar esta fuente',
	'mois' => 'Mes',

	// P
	'pas_articles' => 'Sin art&iacute;culos durante este periodo',
	'pas_synchro' => 'No sincronizar',
	'preferences' => 'Preferencias',
	'probleme_de_syndication' => 'hay un problema con la sindicaci&oacute;n',

	// S
	'semaine' => 'Semana',
	'sources' => 'Fuentes',
	'synchro' => 'Sincronizar',
	'synchro_titre' => 'Grabar en el sitio la lista de los art&iacute;culos le&iacute;dos',
	'syndication_ajour' => 'Actualizar ahora',
	'syndication_fait' => 'Efectuada la sindicaci&oacute;n',

	// T
	'toutes' => 'Todas'
);

?>
