<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Afis bann r&eacute;zim&eacute; ',
	'afficher_sources' => 'Afis sours-la',
	'annee' => 'Ann&eacute;',
	'articles_recents_court' => 'Bann zatik pli fr&egrave;',

	// C
	'connexion' => 'koneksyon',

	// D
	'deconnexion' => 'Dekoneksyon',
	'derniere_syndication' => 'Derny&egrave; sendikasyon la fin fini&nbsp;:&nbsp;',
	'deuxjours' => 'D&eacute; zour',

	// L
	'liens' => 'atik',
	'liens_pluriel' => 'bann zatik',

	// M
	'masquer_resume' => 'Kas&eacute; tout r&eacute;zim&eacute;',
	'masquer_sources' => 'Kas&eacute; sours-la',
	'mois' => 'Mwa',

	// P
	'pas_articles' => 'Ni trouv pa okin atik pou p&eacute;riod-la&nbsp;!',
	'pas_synchro' => 'senkoniz pa li la',
	'preferences' => 'Kosa ou &egrave;m my&eacute;',
	'probleme_de_syndication' => 'larlik &egrave;k lasendikasyon',

	// S
	'semaine' => 'Sem&egrave;n',
	'sources' => 'Tout bann sours',
	'synchro' => 'Senkoniz',
	'synchro_titre' => 'Enrozistr andan sit-la lo lis tout bann zatik domoun la plis lu',
	'syndication_ajour' => 'Rafresi ast&egrave;r',
	'syndication_fait' => 'Sendikasyon la fin fini',

	// T
	'toutes' => 'Tout'
);

?>
