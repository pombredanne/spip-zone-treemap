<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Amosar os resumos',
	'afficher_sources' => 'Amosar esta fonte',
	'annee' => 'Ano',
	'articles_recents_court' => 'Artigos recentes',

	// C
	'connexion' => 'Conexi&oacute;n',

	// D
	'deconnexion' => 'Desconexi&oacute;n',
	'derniere_syndication' => 'A &uacute;ltima sindicaci&oacute;n deste web foi efectuada',
	'deuxjours' => 'Dous d&iacute;as',

	// L
	'liens' => 'artigo',
	'liens_pluriel' => 'artigos',

	// M
	'masquer_resume' => 'Ocultar os resumos',
	'masquer_sources' => 'Ocultar esta fonte',
	'mois' => 'Mes',

	// P
	'pas_articles' => 'Non hai ning&uacute;n artigo deste per&iacute;odo&nbsp;!',
	'pas_synchro' => 'Non sincronizar',
	'preferences' => 'Preferencias',
	'probleme_de_syndication' => 'Problema de sindicaci&oacute;n',

	// S
	'semaine' => 'Semana',
	'sources' => 'Fontes',
	'synchro' => 'Sincronizar',
	'synchro_titre' => 'Rexistrar no web a lista dos artigos lidos',
	'syndication_ajour' => 'Actualizar agora',
	'syndication_fait' => 'A sindicaci&oacute;n foi efectuada',

	// T
	'toutes' => 'Todos'
);

?>
