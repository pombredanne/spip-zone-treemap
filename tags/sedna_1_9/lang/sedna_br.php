<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Diskouez ar berradenno&ugrave;',
	'afficher_sources' => 'Diskouez an orin-ma&ntilde;',
	'annee' => 'Bloaz',
	'articles_recents_court' => 'Pennado&ugrave; nevez',

	// C
	'connexion' => 'Kennaska&ntilde;',

	// D
	'deconnexion' => 'Digennaska&ntilde;',
	'derniere_syndication' => 'Dastumet titouro&ugrave; diwar al lec\'hienn-ma&ntilde; ar wech diwezha&ntilde; d\'an',
	'deuxjours' => 'Daou zevezh',

	// L
	'liens' => 'pennad',
	'liens_pluriel' => 'pennad',

	// M
	'masquer_resume' => 'Kuzhat ar berradenno&ugrave;',
	'masquer_sources' => 'Kuzhat an orin-ma&ntilde;',
	'mois' => 'Miz',

	// P
	'pas_articles' => 'Pennad ebet er mare-se&nbsp;!',
	'pas_synchro' => 'Chom hep kenamzeria&ntilde;',
	'preferences' => 'Dibabo&ugrave;',
	'probleme_de_syndication' => 'Kudenn evit dastum titouro&ugrave;',

	// S
	'semaine' => 'Sizhun',
	'sources' => 'Orino&ugrave;',
	'synchro' => 'Kenamzeria&ntilde;',
	'synchro_titre' => 'Enrolla&ntilde; war al lec\'hienn listenn ar pennado&ugrave; bet lennet',
	'syndication_ajour' => 'Hizivaat brema&ntilde;',
	'syndication_fait' => 'Dastumet eo bet an titouo&ugrave;',

	// T
	'toutes' => 'Holl'
);

?>
