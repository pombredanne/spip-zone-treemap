<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Afi&#349;i resumojn',
	'afficher_sources' => 'Afi&#349;i tiun fonton',
	'annee' => 'Jaro',
	'articles_recents_court' => 'Fre&#349;aj artikoloj',

	// C
	'connexion' => 'Ensaluti',

	// D
	'deconnexion' => 'Elsaluti',
	'derniere_syndication' => 'Lasta abonrilato-&#285;isdatigo farita',
	'deuxjours' => 'Du tagoj',

	// L
	'liens' => 'artikolo',
	'liens_pluriel' => 'artikoloj',

	// M
	'masquer_resume' => 'Ka&#349;i la resumojn',
	'masquer_sources' => 'Ka&#349;i tiun fonton',
	'mois' => 'Monato',

	// P
	'pas_articles' => 'Neniu artikolo dum &#265;i tiu periodo&nbsp;!',
	'pas_synchro' => 'Ne sinkronigi',
	'preferences' => 'Preferoj',
	'probleme_de_syndication' => 'Abonrilato-problemo',

	// S
	'semaine' => 'Semajno',
	'sources' => 'Fontoj',
	'synchro' => 'Sinkronigi',
	'synchro_titre' => 'Registri &#265;e la retejo la liston de la legitaj artikoloj',
	'syndication_ajour' => '&#284;isdatigi nun',
	'syndication_fait' => 'Abonligita',

	// T
	'toutes' => '&#264;iuj'
);

?>
