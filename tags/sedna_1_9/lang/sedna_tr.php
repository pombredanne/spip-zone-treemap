<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => '&Ouml;zetleri g&ouml;ster',
	'afficher_sources' => 'Bu kayna&#287;&#305; g&ouml;ster',
	'annee' => 'Y&#305;l',
	'articles_recents_court' => 'Yaz&#305;lan son makaleler',

	// C
	'connexion' => 'Ba&#287;lant&#305;',

	// D
	'deconnexion' => 'Ba&#287;lant&#305;y&#305; kes',
	'derniere_syndication' => 'Bu sitenin son payla&#351;&#305;m&#305;',
	'deuxjours' => '2 g&uuml;n',

	// L
	'liens' => 'makale',
	'liens_pluriel' => 'makale',

	// M
	'masquer_resume' => '&Ouml;zetleri gizle',
	'masquer_sources' => 'Bu kayna&#287;&#305; gizle',
	'mois' => 'Ay',

	// P
	'pas_articles' => 'Bu d&ouml;nemde makale yok&nbsp;!',
	'pas_synchro' => 'E&#351;zamanla&#351;t&#305;r<b>ma</b>',
	'preferences' => 'Tercihler',
	'probleme_de_syndication' => 'payla&#351;&#305;m sorunu',

	// S
	'semaine' => 'Hafta',
	'sources' => 'Kaynaklar',
	'synchro' => 'E&#351;zamanla&#351;t&#305;r',
	'synchro_titre' => 'Okunan makalelerin listesini siteye kaydet',
	'syndication_ajour' => '&#350;imdi g&uuml;ncelle',
	'syndication_fait' => 'Ger&ccedil;ekle&#351;tirilen payla&#351;&#305;m',

	// T
	'toutes' => 'Hepsi'
);

?>
