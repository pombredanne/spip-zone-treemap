<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Zusammenfassungen anzeigen',
	'afficher_sources' => 'Diese Quelle anzeigen',
	'annee' => 'Jahr',
	'articles_recents_court' => 'Neue Artikel',

	// C
	'connexion' => 'Verbindung',

	// D
	'deconnexion' => 'Verbindung trennen',
	'derniere_syndication' => 'Letzte Daten&uuml;bernehme dieser Website',
	'deuxjours' => 'Zwei Tage',

	// L
	'liens' => 'Artikel',
	'liens_pluriel' => 'Artikel',

	// M
	'masquer_resume' => 'Zusammenfassungen ausblenden',
	'masquer_sources' => 'Diese Quelle ausblenden',
	'mois' => 'Monat',

	// P
	'pas_articles' => 'Kein Artikel aus diesem Zeitraum vorhanden!',
	'pas_synchro' => 'Nicht synchronisieren',
	'preferences' => 'Einstellungen',
	'probleme_de_syndication' => 'Syndikationssproblem',

	// S
	'semaine' => 'Woche',
	'sources' => 'Quellen',
	'synchro' => 'Synchronisieren',
	'synchro_titre' => 'Diese Website in die Liste gelesener Artikel aufnehmen',
	'syndication_ajour' => 'Jetzt aktualisieren',
	'syndication_fait' => 'Syndication ausgef&uuml;hrt',

	// T
	'toutes' => 'Alle'
);

?>
