<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'R&eacute;sum&eacute; weisen',
	'afficher_sources' => 'D&euml;s Source weisen',
	'annee' => 'Jo&euml;r',
	'articles_recents_court' => 'Nei Artikelen',

	// C
	'connexion' => 'Verbindung',

	// D
	'deconnexion' => 'Ofbroch vun der Verbindung',
	'derniere_syndication' => 'L&auml;scht Syndicatioun vun d&euml;sem Site',
	'deuxjours' => 'Zwee Deeg',

	// L
	'liens' => 'Artikel',
	'liens_pluriel' => 'Artikelen',

	// M
	'masquer_resume' => 'R&eacute;sum&eacute; verstoppen',
	'masquer_sources' => 'Source verstoppen',
	'mois' => 'Mount',

	// P
	'pas_articles' => 'Keng Artikelen an d&euml;ser Period!',
	'pas_synchro' => 'N&euml;t synchronis&eacute;ieren',
	'preferences' => 'Optiounen',
	'probleme_de_syndication' => 'Syndicatiouns-Problem',

	// S
	'semaine' => 'Woch',
	'sources' => 'Sourcen',
	'synchro' => 'Synchronis&eacute;ieren',
	'synchro_titre' => 'D\'L&euml;scht vun de geliesenen Artikelen um Site sp&auml;icheren',
	'syndication_ajour' => 'Elo updaten',
	'syndication_fait' => 'Syndicatioun f&auml;rdeg',

	// T
	'toutes' => 'All'
);

?>
