<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Afi&#351;a&#355;i rezumatele',
	'afficher_sources' => 'Afi&#351;a&#355;i aceast&#259; surs&#259;',
	'annee' => 'Anul',
	'articles_recents_court' => 'Articole recente',

	// C
	'connexion' => 'Conexiune',

	// D
	'deconnexion' => 'Deconexiune',
	'derniere_syndication' => 'Ultima sindicalizare a acestui site efectuat&#259;',
	'deuxjours' => 'Dou&#259; zile',

	// L
	'liens' => 'articol',
	'liens_pluriel' => 'articole',

	// M
	'masquer_resume' => 'Masca&#355;i rezumatele',
	'masquer_sources' => 'Masca&#355;i aceast&#259; surs&#259;',
	'mois' => 'Luni',

	// P
	'pas_articles' => 'Nici un articol &icirc;n aceast&#259; perioad&#259;&nbsp;!',
	'pas_synchro' => 'Nu sincroniza&#355;i',
	'preferences' => 'Preferin&#355;e',
	'probleme_de_syndication' => 'problem&#259; de sindicalizare',

	// S
	'semaine' => 'S&#259;pt&#259;m&acirc;n&#259;',
	'sources' => 'Surse',
	'synchro' => 'Sincroniza&#355;i',
	'synchro_titre' => '&Icirc;nregistra&#355;i pe site lista articolelor citite',
	'syndication_ajour' => 'Aduce&#355;i la zi acum',
	'syndication_fait' => 'Sindicalizare efectuat&#259;',

	// T
	'toutes' => 'Toate'
);

?>
