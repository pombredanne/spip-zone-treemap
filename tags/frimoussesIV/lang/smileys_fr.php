<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
':-)*'=> 'sourire',
':-)'=> 'sourire',
'o:)'=> 'ange',
'O:)'=> 'ange',
'0:)'=> 'ange',
':)'=> 'sourire',
'%-)'=> 'smiley &#037;&#045;&#041;',
';-)'=> 'clin d\'oeil',
';)'=> 'clin d\'oeil',
':-('=> 'triste',
':('=> 'triste',
':-O'=> 'bouche ronde',
':O)'=> 'clown',
':O'=> 'bouche ronde',
':-D'=> 'grand sourire',
':D'=> 'grand sourire',
':o)'=> 'clown',
':0)'=> 'clown',
':0'=> 'smiley &#058;&#048;',
':-|'=> 'sans voix',
':|'=> 'sans voix',
':-/'=> 'smiley &#058;&#045;&#047;',
':/'=> 'smiley &#058;&#047;',
':-p'=> 'langue tir&eacute;e',
':p'=> 'langue tir&eacute;e',
':-...'=> 'sans voix',
':...'=> 'sans voix',
':-..'=> 'sans voix',
':..'=> 'sans voix',
':-.'=> 'sans voix',
':.'=> 'sans voix',
':-x'=> 'bisous',
':x'=> 'bisous',
'B-)'=> 'lunettes de soleil',
'B)'=> 'lunettes de soleil',
':-@'=> 'love',
':@'=> 'love',
':$'=> 'smiley &#058;&#036;',
':-*'=> 'smiley &#058;&#045;&#042;',
':*'=> 'smiley &#058;&#042;',
':-!'=> 'smiley &#058;&#045;&#033;',
':!'=> 'smiley &#058;&#033;',
'8-)'=> 'lunettes de soleil',
'8)'=> 'lunettes de soleil',
'MONK' => 'singe',	 
'|-)'=> 'smiley &#124;&#045;&#041;',
'|)'=> 'smiley &#124;&#041;',
'O-)'=> 'smiley &#079;&#045;&#041;',
'O)'=> 'smiley &#079;&#041;',
'ATTN'=> 'ATTENTION',
'SVNT'=> 'SUIVANT',
":\\'-("=> 'triste',
":\\'("=> 'triste'
);
?>
