<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/_stable_/acces_restreint/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A

	// B

	// C
	'conception_graphique_par' => 'Habillage visuel (c)',
	'commentaire' => 'commentaire',
	'commentaires' => 'commentaires',

	// D

	// I

	// L
	'lire_la_suite' => 'Lire la suite',
	'lire_la_suite_de' => ' de ',

	// M

	// P
	'personaliser_nav' => 'Personnaliser ce menu',

	// R

	// S
	'sous_licence' => 'sous Licence',

	// T

	// V

	// Z
);

?>
