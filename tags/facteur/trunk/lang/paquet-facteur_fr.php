<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// 
///  Fichier produit par PlugOnet
// Module: paquet-facteur
// Langue: fr
// Date: 09-11-2011 12:03:33
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// F
	'facteur_description' => 'Facteur s\'occupe de la distribution des courriels au format HTML, texte ou mixte ; via SMTP ou non',
	'facteur_nom' => 'Facteur',
	'facteur_slogan' => 'Il distribue vos courriels',
);
?>