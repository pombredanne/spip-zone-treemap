<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// 
///  Fichier produit par PlugOnet
// Module: paquet-facteur
// Langue: en
// Date: 09-11-2011 12:03:33
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// F
	'facteur_description' => 'Postman delivers emails in HTML, text or mixed format; via SMTP or not',
	'facteur_nom' => 'Postman',
	'facteur_slogan' => 'He delivers emails',
);
?>