<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
									   'titre_page' => 'Trier les @objets@',
									   'titre_tri_mots' => 'Ordonner les @objets@ du mot @titre_mot@ du groupe @type_mot@',
									   'tri_mots_help' => 'Ici vous pouvez glisser-deposer les @objets@ dans l\'ordre que vous voulez.<br>Cet ordre est seulement relatif au mot clef @titre_mot@ du groupe @type_mot@',
									   'voir' => 'voir',
									   'ordonner' => 'Ordonner les @objets@ selon:',
									   'rang' => 'rang:',
									   'envoyer' => 'Envoyer le nouveau tri'
);
?>
