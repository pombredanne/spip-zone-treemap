<?php
/*
 * liens_contenus
 * Gestion des liens inter-contenus
 *
 * Auteur :
 * Nicolas Hoizey
 * © 2007 - Distribue sous licence GNU/GPL
 *
 */

	$GLOBALS[$GLOBALS['idx_lang']] = array(
	    'liens_inter_contenus' => 'Links between contents',
		'objets_avec_liens_vers_courant' => 'These contents contain links to this one:',
		'objets_avec_liens_depuis_courant' => 'This content contains links to these ones:',
		'aucun_objets_avec_lien_vers_courant' => 'No other content contain link to this one.',
		'aucun_objets_avec_lien_depuis_courant' => 'This content doesn\'t contain any link to another content.',
        'legende_liens_faux_objets' => 'Red and striked links are linked content that don\'t exist.',
        'statut_prepa' => 'Editing in progress',
        'statut_prop' => 'Submitted for evaluation',
        'statut_publie' => 'Published online',
        'statut_refuse' => 'Rejected',
        'statut_poubelle' => 'In the dustbin',
        'confirmation_depublication' => 'Be careful, a published content contains at least one link to this one and will be affected if you change its status!\n\nDo you still want to change the status?',
        'confirmation_suppression' => 'Be careful, a published content contains at least one link to this one and will be affected if you delete it!\n\nDo you still want to delete it?'
	);
?>