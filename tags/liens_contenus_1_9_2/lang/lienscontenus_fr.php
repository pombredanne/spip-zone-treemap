<?php
/*
 * liens_contenus
 * Gestion des liens inter-contenus
 *
 * Auteur :
 * Nicolas Hoizey
 * © 2007 - Distribue sous licence GNU/GPL
 *
 */

	$GLOBALS[$GLOBALS['idx_lang']] = array(
	    'liens_inter_contenus' => 'Liens inter-contenus',
		'objets_avec_liens_vers_courant' => 'Ces contenus contiennent des liens vers celui-ci :',
		'objets_avec_liens_depuis_courant' => 'Ce contenu contient des liens vers ceux-ci :',
		'aucun_objets_avec_lien_vers_courant' => 'Aucun autre contenu ne contient de lien vers celui-ci.',
		'aucun_objets_avec_lien_depuis_courant' => 'Ce contenu ne contient aucun lien vers un autre contenu.',
        'legende_liens_faux_objets' => 'Les liens en rouge et barr&eacute;s indiquent des contenus li&eacute;s qui n\'existent pas.',
        'statut_prepa' => 'En pr&eacute;paration',
        'statut_prop' => 'Propos&eacute;',
        'statut_publie' => 'Publi&eacute;',
        'statut_refuse' => 'Refus&eacute;',
        'statut_poubelle' => 'A la poubelle',
        'confirmation_depublication' => 'Attention, un contenu publié pointe vers celui-ci, et sera impacté si vous le dépubliez !\n\nVoulez-vous vraiment changer le statut ?',
        'confirmation_suppression' => 'Attention, un contenu publié pointe vers celui-ci, et sera impacté si vous le supprimez !\n\nVoulez-vous vraiment le supprimer ?',
        'information_element_contenu' => 'Attention, un autre contenu pointe vers celui-ci !'
	);
?>