<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajouter_panier' => 'Ajouter au panier',
'adresse_facturation' => 'Mon adresse de facturation',
'ajouter_adresse_livraison' => 'Ajouter une adresse de livraison',
'a_bientot' => 'Bonne navigation et &agrave; bient�t',
'adresse1' => 'Adresse',
'adresse2' => 'Adresse (suite)',
'adresse3' => 'Compl&eacute;ment adresse',
'article_panier' => 'article(s).',

// B
'boutique' => 'Boutique',

// C
'catalogue_thelia' => 'Catalogue T&eacute;lia',
'choix_mode_livraison' => 'Choix du mode de livraison',
'continuer' => 'Continuer',
'commentaire_erreur_paiement' => 'Nous vous informons que la proc&eacute;dure de paiement n\'a pas pu aboutir, veuillez vous renseigner aupr&egrave;s de votre &eacute;tablissement bancaire.',
'commentaire_erreur_paiement_suite' => 'Bonne navigation et &agrave; bient�t',
'civilite' => 'Civilit&eacute;',
'choisissez' => 'Choisissez ...',
'code_postal' => 'Code postal',
'champs_obligatoires' => 'Champs obligatoires',
'creer_mon_compte' => 'Cr&eacute;er mon compte',
'contient' => 'contient',
'code_reduction' => 'Saisir votre code de r&eacute;duction',


// D


// E
'erreur_paiement' => 'Erreur de paiement',
'erreur' => 'Vous avez sans doute fait une erreur.',
'email' => 'E-mail',
'email_motdepasse' => 'Inscrivez votre e-mail pour recevoir votre mot de passe',
'email2' => 'Confirmation e-mail',

// F
'frais_livraison' => 'Frais de livraison',

// G


// H


// I
'identifier' => 'Identifiez-vous',
'identifier_erreur' => 'Identifiez-vous : des erreurs dans votre saisie !',

// J


// K


// L


// M
'mon_panier' => 'Contenu de mon panier',
'mon_panier_som' => 'Mon panier',
'mes_coordonnees' => 'Mes coordonn&eacute;es',
'modif_coordonnees' => 'Modifier mes coordonn&eacute;es',
'modifier_adresse' => 'Modifier cette adresse',
'adresse_livraison' => 'Mon adresse de livraison',
'modifier_adresse' => 'Modifier cette adresse',
'adresse_facturation_defaut' => 'Mon adresse de facturation par d&eacute;faut',
'merci' => 'Merci',
'merci_achat' => 'Notre boutique vous remercie de votre achat',
'modif_compte' => 'Mon compte : modifier mes informations',
'madame' => 'Madame',
'monsieur' => 'Monsieur',
'mademoiselle' => 'Mademoiselle',
'modif_motdepasse' => 'Modifier mon mot de passe',
'motdepasse1' => 'Mot de passe',
'motdepasse2' => 'Confirmation mot de passe',
'mon_compte' => 'Mon compte',
'mode_paiement' => 'Je choisis un mode de paiement',
'mon_email' => 'Mon e-mail',
'mon_passe' => 'Mon mot de passe',
'modifier_info_perso' => 'Modifier mes informations personnelles (coordonn&eacute;es, mot de passe, ... )',

// N
'nouveau_client' => 'Je suis un nouveau client',
'nom' => 'Nom',
'nouveau_motdepasse' => 'Nouveau mot de passe',

// O
'oubli_motdepasse' => 'Mon compte : mot de passe perdu ou oubli&eacute;',
'ouvrir_compte' => 'Ouvrir un compte',
'oubli_passe' => 'Mot de passe oubli&eacute;',

// P
'photo_produit' => 'Photo',
'produit' => 'Produits',
'promo' => 'Promotion',
'prix_unitaire' => 'Prix unitaire',
'probleme_adresse_livraison' => 'Vous devez choisir une autre adresse de livraison. <br/>Nous ne r&eacute;alisons pas de livraison vers ce pays.',
'page_precedente' => 'Page pr&eacute;c&egrave;dente',
'page_suivante' => 'Page suivante',
'prenom' => 'Pr&eacute;nom',
'pays' => 'Pays',

// Q
'quantite' => 'Quantit&eacute;',

// R
'references_produits' => 'R&eacute;f&eacute;rence',
'reglement_cheque' => 'Je choisis un mode de r&egrave;glement : par ch&egrave;que',
'recapitulatif_commande' => 'R&eacute;capitulatif de ma commande',
'remise' => 'Remise',

// S
'supprimer' => 'Supprimer',
'selectionner_autre_adresse' => 'S&eacute;lectionnez une autre adresse de livraison',
'deconnecter' => 'se d&eacute;connecter',

// T
'total_ttc' => 'Total TTC',
'total' => 'Total',
'total_panier' => 'Total du panier',
'type_livraison_tarif' => 'Type de livraison &amp; tarif',
'texte_paiement_cheque' => 'Nous vous remercions de la confiance que vous nous accordez.<br />Un email recapitulatif de votre commande vous a &eacute;t&eacute; envoy&eacute; &agrave; l\'adresse: THELIA-CLIENT_EMAIL.<br />Votre commande sera valid&eacute;e par nos services &agrave; r&eacute;ception de votre paiement.<br /><br />Ecrire ici les infos sur le r&eacute;glement par ch&egrave;que ...',
'telfixe' => 'T&eacute;l&eacute;phone fixe',
'telport' => 'T&eacute;l&eacute;phone portable',

// U

// V
'valider_panier' => 'Valider mon panier',
'valider' => 'Valider',
'ville' => 'Ville',
'vide_panier_som' => 'Mon panier est vide !',
'visualiser_commandes' => 'Visualiser mes commandes',
'vider' => 'Vider',
'visualiser_coordonnees_commandes' => 'Visualiser mes coordonn&eacute;es  et mes commandes'

// W


// X


// Y


// Z


);


?>
