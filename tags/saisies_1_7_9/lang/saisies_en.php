<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_annee' => 'Year',
	'label_jour' => 'Day',
	'label_mois' => 'Month',

	// O
	'option_attention_explication' => 'A message more important than the explanation.',
	'option_attention_label' => 'Warning',
	'option_cacher_option_intro_label' => 'Hide the first empty choice',
	'option_choix_destinataires_explication' => 'One or several authors among which the user can make his choice. If nothing selected, it will be the author who installed the site to be chosen.',
	'option_choix_destinataires_label' => 'Possible recipients',
	'option_class_label' => 'Additional CSS Classes',
	'option_cols_explication' => 'Field width in characters. This option is not always applied/used because the CSS styles of your site can cancel it.',
	'option_cols_label' => 'Width',
	'option_datas_explication' => 'You need to specify a choice for each row in the form of "key|label of the choice"',
	'option_datas_label' => 'List of the available choices',
	'option_defaut_label' => 'Default value',
	'option_disable_avec_post_explication' => 'Same as previous option position but still post value in a hidden field.',
	'option_disable_avec_post_label' => 'Disabled but posted.',
	'option_disable_explication' => 'The field can not get the focus.',
	'option_disable_label' => 'Disable the field',
	'option_explication_explication' => 'If necessary, a short sentence describing the subject field.',
	'option_explication_label' => 'Explanation',
	'option_groupe_affichage' => 'Display',
	'option_groupe_description' => 'Description',
	'option_groupe_utilisation' => 'Usage',
	'option_groupe_validation' => 'Validation',
	'option_info_obligatoire_explication' => 'You can modify the default indication of obligation: <i>[Obligatoire]</i>.',
	'option_info_obligatoire_label' => 'Indication of obligation',
	'option_label_case_label' => 'Label located beside the check box',
	'option_label_explication' => 'The title that will be displayed.',
	'option_label_label' => 'Label',
	'option_maxlength_explication' => 'The user can not type more characters than this number.',
	'option_maxlength_label' => 'Maximum number of characters',
	'option_nom_explication' => 'A digital name that identifies the field. It should contain only lowercase alphanumeric characters or the character "_".',
	'option_nom_label' => 'Field name',
	'option_obligatoire_label' => 'Required field',
	'option_option_intro_label' => 'Label for the first empty choice',
	'option_pliable_label' => 'Expandable',
	'option_pliable_label_case' => 'The group of fields can be expanded or shrunk.',
	'option_plie_label' => 'Already shrunk',
	'option_plie_label_case' => 'If the group of fields can be expanded and shrunk, then this option will make it already shrink with the form displays.',
	'option_readonly_explication' => 'The field can be viewed, selected, but not modified.',
	'option_readonly_label' => 'Read only',
	'option_rows_explication' => 'Field height in lines. This option is not always applied/used because the CSS styles of your site can cancel it.',
	'option_rows_label' => 'Lines number',
	'option_size_explication' => 'Field width in characters. This option is not always applied/used because the CSS styles of your site can cancel it.',
	'option_size_label' => 'Field size',
	'option_type_choix_plusieurs' => 'Allow the user to choose <strong>many</ strong> recipients.',
	'option_type_choix_tous' => 'Make <strong>all</ strong> these authors as recipients. The user will not have choice.',
	'option_type_choix_un' => 'Allow the user to choose <strong>one</ strong> recipient.',
	'option_type_explication' => 'In "disguised" mode, the field contents as typed will be replaced with asterisks.',
	'option_type_label' => 'Field type',
	'option_type_password' => 'Disguised',
	'option_type_text' => 'Normal',

	// S
	'saisie_case_explication' => 'Used to activate or deactivate a particular option.',
	'saisie_case_titre' => 'Single check box',
	'saisie_checkbox_explication' => 'Used to select several options using check boxes.',
	'saisie_checkbox_titre' => 'Check boxes',
	'saisie_destinataires_explication' => 'Used to select one or more recipients from among the pre-selected authors.',
	'saisie_destinataires_titre' => 'Recipients',
	'saisie_explication_explication' => 'A general explanatory description.',
	'saisie_explication_titre' => 'Explanation',
	'saisie_fieldset_explication' => 'A frame which may include several fields.',
	'saisie_fieldset_titre' => 'Fieldset',
	'saisie_file_explication' => 'Send a file',
	'saisie_file_titre' => 'File',
	'saisie_hidden_explication' => 'A pre-filled field that the user will never see.',
	'saisie_hidden_titre' => 'Hidden field',
	'saisie_input_explication' => 'A simple line of text that can be visible or hidden (password).',
	'saisie_input_titre' => 'Textfield',
	'saisie_oui_non_explication' => 'Either a Yes or No response',
	'saisie_oui_non_titre' => 'Yes or No',
	'saisie_radio_defaut_choix1' => 'One',
	'saisie_radio_defaut_choix2' => 'Two',
	'saisie_radio_defaut_choix3' => 'Three',
	'saisie_radio_explication' => 'Used to select one single option from several possibilities.',
	'saisie_radio_titre' => 'Radio buttons',
	'saisie_selection_explication' => 'Select an option from a dropdown list box.',
	'saisie_selection_multiple_explication' => 'Used for selecting several options from a list.',
	'saisie_selection_multiple_titre' => 'Multiple selection',
	'saisie_selection_titre' => 'Dropdown list box',
	'saisie_textarea_explication' => 'A multilines text field.',
	'saisie_textarea_titre' => 'Textarea',

	// T
	'tous_visiteurs' => 'All visitors (even if not registered)',

	// V
	'vue_sans_reponse' => '<i>Without response</i>',

	// Z
	'z' => 'zzz'
);

?>
