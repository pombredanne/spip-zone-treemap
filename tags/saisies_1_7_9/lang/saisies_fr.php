<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/saisies/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_annee' => 'Ann&eacute;e',
	'label_jour' => 'Jour',
	'label_mois' => 'Mois',

	// O
	'option_attention_explication' => 'Un message plus important que l\'explication.',
	'option_attention_label' => 'Avertissement',
	'option_cacher_option_intro_label' => 'Cacher le premier choix vide',
	'option_choix_destinataires_explication' => 'Un ou plusieurs auteurs parmis lesquels l\'utilisateur pourra faire son choix. Si rien n\'est s&eacute;lectionn&eacute;, c\'est l\'auteur qui a install&eacute; le site qui sera choisi.',
	'option_choix_destinataires_label' => 'Destinataires possibles',
	'option_class_label' => 'Classes CSS suppl&eacute;mentaires',
	'option_cols_explication' => 'Largeur du bloc en nombre de caractères. Cette option n\'est pas toujours appliqu&eacute;e car les styles CSS de votre site peuvent l\'annuler.',
	'option_cols_label' => 'Largeur',
	'option_datas_explication' => 'Vous devez indiquez un choix par ligne sous la forme "cle|Label du choix"',
	'option_datas_label' => 'Liste des choix possibles',
	'option_defaut_label' => 'Valeur par d&eacute;faut',
	'option_disable_avec_post_explication' => 'Identique &agrave; l\'option pr&eacute;c&eacute;dente mais poste quand m&ecirc;me la valeur dans un champ cach&eacute;.',
	'option_disable_avec_post_label' => 'D&eacute;sactiver mais poster',
	'option_disable_explication' => 'Le champ ne peut plus obtenir le focus.',
	'option_disable_label' => 'D&eacute;sactiver le champ',
	'option_explication_explication' => 'Si besoin, une courte phrase d&eacute;crivant l\'objet du champ.',
	'option_explication_label' => 'Explication',
	'option_groupe_affichage' => 'Affichage',
	'option_groupe_description' => 'Description',
	'option_groupe_utilisation' => 'Utilisation',
	'option_groupe_validation' => 'Validation',
	'option_info_obligatoire_explication' => 'Vous pouvez modifier l\'indication d\'obligation par d&eacute;faut : <i>[Obligatoire]</i>.',
	'option_info_obligatoire_label' => 'Indication d\'obligation',
	'option_label_case_label' => 'Label plac&eacute; &agrave; c&ocirc;t&eacute; de la case',
	'option_label_explication' => 'Le titre qui sera affich&eacute;.',
	'option_label_label' => 'Label',
	'option_maxlength_explication' => 'L\'utilisateur ne pourra pas taper plus de caract&egrave;res que ce nombre.',
	'option_maxlength_label' => 'Nombre de caract&egrave;res maximum',
	'option_nom_explication' => 'Un nom informatique qui identifiera le champ. Il ne doit contenir que des caractères alpha-numériques minuscules ou le caractère "_".',
	'option_nom_label' => 'Nom du champ',
	'option_obligatoire_label' => 'Champ obligatoire',
	'option_option_intro_label' => 'Label du premier choix vide',
	'option_pliable_label' => 'Pliable',
	'option_pliable_label_case' => 'Le groupe de champs pourra &ecirc;tre repli&eacute;.',
	'option_plie_label' => 'D&eacute;j&agrave; pli&eacute;',
	'option_plie_label_case' => 'Si le groupe de champs est pliable, il sera d&eacute;j&agrave; pli&eacute; &agrave; l\'affichage du formulaire.',
	'option_readonly_explication' => 'Le champ peut &ecirc;tre lu, s&eacute;lectionn&eacute;, mais pas modifi&eacute;.',
	'option_readonly_label' => 'Lecture seule',
	'option_rows_explication' => 'Hauteur du bloc en nombre de ligne. Cette option n\'est pas toujours appliqu&eacute;e car les styles CSS de votre site peuvent l\'annuler.',
	'option_rows_label' => 'Nombre de lignes',
	'option_size_explication' => 'Largeur du champ en nombre de caract&egrave;res. Cette option n\'est pas toujours appliqu&eacute;e car les styles CSS de votre site peuvent l\'annuler.',
	'option_size_label' => 'Taille du champ',
	'option_type_choix_plusieurs' => 'Permettre à l\'utilisateur de choisir <strong>plusieurs</strong> destinataires.',
	'option_type_choix_tous' => 'Mettre <strong>tous</strong> ces auteurs en destinataires. L\'utilisateur n\'aura aucun choix.',
	'option_type_choix_un' => 'Permettre à l\'utilisateur de choisir <strong>un seul</strong> destinataire.',
	'option_type_explication' => 'En mode "masqu&eacute;", le contenu du champ ne sera pas visible.',
	'option_type_label' => 'Type du champ',
	'option_type_password' => 'Masqu&eacute;',
	'option_type_text' => 'Normal',

	// S
	'saisie_case_explication' => 'Permet d\'activer ou de d&eacute;sactiver quelque chose.',
	'saisie_case_titre' => 'Case unique',
	'saisie_checkbox_explication' => 'Permet de choisir plusieurs options avec des cases.',
	'saisie_checkbox_titre' => 'Cases à cocher',
	'saisie_destinataires_explication' => 'Permet de choisir un ou plusieurs destinataires parmis des auteurs pr&eacute;-s&eacute;lectionn&eacute;.',
	'saisie_destinataires_titre' => 'Destinataires',
	'saisie_explication_explication' => 'Un texte explicatif g&eacute;n&eacute;ral.',
	'saisie_explication_titre' => 'Explication',
	'saisie_fieldset_explication' => 'Un cadre qui pourra englober plusieurs champs.',
	'saisie_fieldset_titre' => 'Groupe de champs',
	'saisie_file_explication' => 'Envoi d\'un fichier',
	'saisie_file_titre' => 'Fichier',
	'saisie_hidden_explication' => 'Un champ pr&eacute;-rempli que l\'utilisateur ne pourra pas voir.',
	'saisie_hidden_titre' => 'Champ cach&eacute;',
	'saisie_input_explication' => 'Une simple ligne de texte, pouvant &ecirc;tre visible ou masqu&eacute;e (mot de passe).',
	'saisie_input_titre' => 'Ligne de texte',
	'saisie_oui_non_explication' => 'Oui ou non, c\'est clair ? :)',
	'saisie_oui_non_titre' => 'Oui ou non',
	'saisie_radio_defaut_choix1' => 'Un',
	'saisie_radio_defaut_choix2' => 'Deux',
	'saisie_radio_defaut_choix3' => 'Trois',
	'saisie_radio_explication' => 'Permet de choisir une option parmis plusieurs disponibles.',
	'saisie_radio_titre' => 'Boutons radios',
	'saisie_selection_explication' => 'Choisir une option dans une liste déroulante.',
	'saisie_selection_multiple_explication' => 'Permet de choisir plusieurs options avec une liste.',
	'saisie_selection_multiple_titre' => 'S&eacute;lection multiple',
	'saisie_selection_titre' => 'Liste d&eacute;roulante',
	'saisie_textarea_explication' => 'Un champ de texte sur plusieurs lignes.',
	'saisie_textarea_titre' => 'Bloc de texte',

	// T
	'tous_visiteurs' => 'Tous les visiteurs (m&ecirc;me non enregistr&eacute;s)',

	// V
	'vue_sans_reponse' => '<i>Sans réponse</i>',

	// Z
	'z' => 'zzz'
);

?>
