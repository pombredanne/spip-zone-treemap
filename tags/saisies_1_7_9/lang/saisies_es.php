<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_annee' => 'Ann&eacute;e', # NEW
	'label_jour' => 'Jour', # NEW
	'label_mois' => 'Mois', # NEW

	// O
	'option_attention_explication' => 'Un mensaje m&aacute;s importante que la explicaci&oacute;n.',
	'option_attention_label' => 'Aviso',
	'option_cacher_option_intro_label' => 'Esconder la primera opci&oacute;n vac&iacute;a',
	'option_choix_destinataires_explication' => 'Una lista de autoras o autores de SPIP. Se podr&aacute;n elegir los destinatorios dentro de esta lista. Si no se selecciona nadie, la elecci&oacute;n ser&aacute; la persona que instal&oacute; el sitio.',
	'option_choix_destinataires_label' => 'Destinaciones posibles',
	'option_class_label' => 'Clases CSS adicionales',
	'option_cols_explication' => 'Ancho del bloque (en n&uacute;mero de car&aacute;cteres). Esta opci&oacute;n no se aplica siempre, porque puede ser cancelada por los estilos CSS de tu sitio.',
	'option_cols_label' => 'Ancho',
	'option_datas_explication' => 'Tiene que dar una opci&oacute;n por l&iacute;nea bajo la forma "clave|Etiqueta de la opci&oacute;n"',
	'option_datas_label' => 'Lista de opciones posibles',
	'option_defaut_label' => 'Valor por defecto',
	'option_disable_avec_post_explication' => 'Como la opci&oacute;n anterior, pero igual env&iacute;a el valor en un campo escondido.',
	'option_disable_avec_post_label' => 'Deactivar pero enviar',
	'option_disable_explication' => 'El campo ya no puede obtener el foco.',
	'option_disable_label' => 'Deactivar el campo',
	'option_explication_explication' => 'Si hace falta, una frase corta que describe el campo',
	'option_explication_label' => 'Explicaci&oacute;n',
	'option_groupe_affichage' => 'Aparencia',
	'option_groupe_description' => 'Descripci&oacute;n',
	'option_groupe_utilisation' => 'Uso',
	'option_groupe_validation' => 'Validaci&oacute;n',
	'option_info_obligatoire_explication' => 'Puede modificar la indicaci&oacute;n de campo obligatoria: <i>[Obligatorio</i>.',
	'option_info_obligatoire_label' => 'Indicaci&oacute;n de campo obligatorio',
	'option_label_case_label' => 'Etiqueta posicionada al lado de la checkbox',
	'option_label_explication' => 'El t&iacute;tulo que se ense&ntilde;ar&aacute;',
	'option_label_label' => 'Etiqueta',
	'option_maxlength_explication' => 'El campo no podr&aacute; contener m&aacute;s car&aacute;cteres que este n&uacute;mero.',
	'option_maxlength_label' => 'N&uacute;mero m&aacute;ximo de car&aacute;cteres',
	'option_nom_explication' => 'Un nombre inform&aacute;tico que identificar&aacute; el campo. S&oacute;lo puede contener car&aacute;cteres alfanum&eacute;ricos min&uacute;sculos o el car&aacute;cter "_".',
	'option_nom_label' => 'Nombre del campo',
	'option_obligatoire_label' => 'Campo obligatorio',
	'option_option_intro_label' => 'Label de la primera opci&oacute;n vac&iacute;a',
	'option_pliable_label' => 'Desplegable',
	'option_pliable_label_case' => 'El grupo de campos se podr&aacute; contraer y desplegar.',
	'option_plie_label' => 'Ya est&aacute; contraido',
	'option_plie_label_case' => 'Si el grupo de campos se puede contraer, ya estar&aacute; contraido cuando se ense&ntilde;e el formulario.',
	'option_readonly_explication' => 'El campo se puede leer, seleccionar, pero no se puede modificar.',
	'option_readonly_label' => 'S&oacute;lo lectura',
	'option_rows_explication' => 'Altura del bloque en n&uacute;mero de l&iacute;neas. Esta opci&oacute;n no se aplica siempre, porque puede ser cancelada por los estilos CSS de su sitio.',
	'option_rows_label' => 'N&uacute;mero de l&iacute;neas',
	'option_size_explication' => 'Ancho del campo (n&uacute;mero de car&aacute;cteres). Esta opci&oacute;n no se aplica siempre, porque puede ser cancelada por los estilos CSS de su sitio.',
	'option_size_label' => 'Tama&ntilde;o del campo',
	'option_type_choix_plusieurs' => 'Permitirle al usuario elegir <strong>varias</strong> personas destinatorias.',
	'option_type_choix_tous' => 'Poner a <strong>todos/as</strong> estas autoras como destinatorias. El usuario no tendr&aacute; ninguna opci&oacute;n.',
	'option_type_choix_un' => 'Permitirle al usuario elegir <strong>s&oacute;lo una</strong> persona destinatoria.',
	'option_type_explication' => 'En modo "escondido", el contenido del campo no ser&aacute; visible.',
	'option_type_label' => 'Tipo del campo',
	'option_type_password' => 'Escondido',
	'option_type_text' => 'Normal',

	// S
	'saisie_case_explication' => 'Permite activar o deactivar algo.',
	'saisie_case_titre' => 'Checkbox &uacute;nico',
	'saisie_checkbox_explication' => 'Permite elegir varias opciones con checkboxes',
	'saisie_checkbox_titre' => 'Checkboxes',
	'saisie_destinataires_explication' => 'Permite elegir una o varias personas destinatorias dentro de las autoras seleccionadas.',
	'saisie_destinataires_titre' => 'Personas destinatorias',
	'saisie_explication_explication' => 'Un texte explicatif g&eacute;n&eacute;ral.', # NEW
	'saisie_explication_titre' => 'Explication', # NEW
	'saisie_fieldset_explication' => 'Un marco que podr&aacute; englobar varios campos.',
	'saisie_fieldset_titre' => 'Grupo de campos',
	'saisie_file_explication' => 'Envoi d\'un fichier', # NEW
	'saisie_file_titre' => 'Fichier', # NEW
	'saisie_hidden_explication' => 'Un champ pr&eacute;-rempli que l\'utilisateur ne pourra pas voir.', # NEW
	'saisie_hidden_titre' => 'Champ cach&eacute;', # NEW
	'saisie_input_explication' => 'Una sola l&iacute;nea de texto, que puede ser visible u ocultada (contrase&ntilde;a).',
	'saisie_input_titre' => 'L&iacute;nea de texto',
	'saisie_oui_non_explication' => 'S&iacute; o no. &iquest;Est&aacute; claro? :)',
	'saisie_oui_non_titre' => 'S&iacute; o no',
	'saisie_radio_defaut_choix1' => 'Uno',
	'saisie_radio_defaut_choix2' => 'Dos',
	'saisie_radio_defaut_choix3' => 'Tres',
	'saisie_radio_explication' => 'Permite elegir una opci&oacute;n dentro de varias opciones disponibles.',
	'saisie_radio_titre' => 'Botones de opci&oacute;n',
	'saisie_selection_explication' => 'Elegir una opci&oacute;n dentro de una lista desplegable.',
	'saisie_selection_multiple_explication' => 'Permite elegir varias opciones con una lista.',
	'saisie_selection_multiple_titre' => 'Selecci&oacute;n multiple',
	'saisie_selection_titre' => 'Lista desplegable',
	'saisie_textarea_explication' => 'Un campo de texto sobre varias l&iacute;neas.',
	'saisie_textarea_titre' => 'Bloque de texto',

	// T
	'tous_visiteurs' => 'Todos y todas las visitantes (incluso no registradas)',

	// V
	'vue_sans_reponse' => '<i>Sin respuesta</i>',

	// Z
	'z' => 'zzz'
);

?>
