<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_annee' => 'Ann&eacute;e', # NEW
	'label_jour' => 'Jour', # NEW
	'label_mois' => 'Mois', # NEW

	// O
	'option_attention_explication' => '<NEW>Spr&aacute;va d&ocirc;le&#382;itej&#353;ia ako vysvetl&iacute;vka.',
	'option_attention_label' => '<NEW>Upozornenie',
	'option_cacher_option_intro_label' => 'Cacher le premier choix vide', # NEW
	'option_choix_destinataires_explication' => '<NEW>Autori, z ktor&yacute;ch si pou&#382;&iacute;vate&#318; m&ocirc;&#382;e vybra&#357;. Ak si nevyberie, bude to autor, ktor&yacute; nain&#353;taloval str&aacute;nku, ktor&aacute; m&aacute; by&#357; vybran&aacute;.',
	'option_choix_destinataires_label' => '<NEW>Mo&#382;n&iacute; pr&iacute;jemcovia',
	'option_class_label' => '<NEW>&#270;al&#353;ie CSS triedy',
	'option_cols_explication' => '<NEW>&#352;&iacute;rka po&#318;a v znakoch. T&aacute;to mo&#382;nos&#357; sa v&#382;dy nepou&#382;&iacute;va, lebo CSS &#353;t&yacute;ly va&#353;ej str&aacute;nky to m&ocirc;&#382;u zru&#353;i&#357;.',
	'option_cols_label' => '<NEW>&#352;&iacute;rka',
	'option_datas_explication' => 'Vous devez indiquez un choix par ligne sous la forme "cle|Label du choix"', # NEW
	'option_datas_label' => 'Liste des choix possibles', # NEW
	'option_defaut_label' => '<NEW>Predvolen&aacute; hodnota',
	'option_disable_avec_post_explication' => 'Same as previous option position but still post value in a hidden field.', # MODIF
	'option_disable_avec_post_label' => 'Disabled but posted.', # MODIF
	'option_disable_explication' => 'The field can not get the focus.', # MODIF
	'option_disable_label' => 'Disable the field', # MODIF
	'option_explication_explication' => 'If necessary, a short sentence describing the subject field.', # MODIF
	'option_explication_label' => 'Explanation', # MODIF
	'option_groupe_affichage' => 'Display', # MODIF
	'option_groupe_description' => 'Description', # MODIF
	'option_groupe_utilisation' => 'Usage', # MODIF
	'option_groupe_validation' => 'Validation', # MODIF
	'option_info_obligatoire_explication' => 'You can modify the default indication of obligation: <i>[Obligatoire]</i>.', # MODIF
	'option_info_obligatoire_label' => 'Indication of obligation', # MODIF
	'option_label_case_label' => 'Label plac&eacute; &agrave; c&ocirc;t&eacute; de la case', # NEW
	'option_label_explication' => 'The title that will be displayed.', # MODIF
	'option_label_label' => 'Label', # MODIF
	'option_maxlength_explication' => 'The user can not type more characters than this number.', # MODIF
	'option_maxlength_label' => 'Maximum number of characters', # MODIF
	'option_nom_explication' => 'A digital name that identifies the field. It should contain only lowercase alphanumeric characters or the character "_".', # MODIF
	'option_nom_label' => 'Field name', # MODIF
	'option_obligatoire_label' => 'Required field', # MODIF
	'option_option_intro_label' => 'Label du premier choix vide', # NEW
	'option_pliable_label' => 'Pliable', # NEW
	'option_pliable_label_case' => 'Le groupe de champs pourra &ecirc;tre repli&eacute;.', # NEW
	'option_plie_label' => 'D&eacute;j&agrave; pli&eacute;', # NEW
	'option_plie_label_case' => 'Si le groupe de champs est pliable, il sera d&eacute;j&agrave; pli&eacute; &agrave; l\'affichage du formulaire.', # NEW
	'option_readonly_explication' => 'The field can be viewed, selected, but not modified.', # MODIF
	'option_readonly_label' => 'Read only', # MODIF
	'option_rows_explication' => 'Field height in lines. This option is not always applied/used because the CSS styles of your site can cancel it.', # MODIF
	'option_rows_label' => 'Lines number', # MODIF
	'option_size_explication' => 'Field width in characters. This option is not always applied/used because the CSS styles of your site can cancel it.', # MODIF
	'option_size_label' => 'Field size', # MODIF
	'option_type_choix_plusieurs' => 'Allow the user to choose <strong>many</ strong> recipients.', # MODIF
	'option_type_choix_tous' => 'Make <strong>all</ strong> these authors as recipients. The user will not have choice.', # MODIF
	'option_type_choix_un' => 'Allow the user to choose <strong>one</ strong> recipient.', # MODIF
	'option_type_explication' => '<MODIF>Mode "password", the contents of the field will be hidden.', # MODIF
	'option_type_label' => '<NEW>Typ po&#318;a',
	'option_type_password' => '<NEW><MODIF>Heslo',
	'option_type_text' => '<NEW>Norm&aacute;lny',

	// S
	'saisie_case_explication' => 'Permet d\'activer ou de d&eacute;sactiver quelque chose.', # NEW
	'saisie_case_titre' => 'Case unique', # NEW
	'saisie_checkbox_explication' => 'Permet de choisir plusieurs options avec des cases.', # NEW
	'saisie_checkbox_titre' => 'Cases à cocher', # NEW
	'saisie_destinataires_explication' => '<NEW><MODIF>Umo&#382;&#328;uje vybra&#357; jedn&eacute;ho alebo viacer&yacute;ch pr&iacute;jemcov z predvolen&yacute;ch autorov .',
	'saisie_destinataires_titre' => '<NEW>Pr&iacute;jemcovia',
	'saisie_explication_explication' => 'Un texte explicatif g&eacute;n&eacute;ral.', # NEW
	'saisie_explication_titre' => 'Explication', # NEW
	'saisie_fieldset_explication' => '<NEW>R&aacute;m, v ktorom m&ocirc;&#382;e by&#357; nieko&#318;ko pol&iacute;.',
	'saisie_fieldset_titre' => 'Fieldset', # MODIF
	'saisie_file_explication' => 'Envoi d\'un fichier', # NEW
	'saisie_file_titre' => 'Fichier', # NEW
	'saisie_hidden_explication' => 'Un champ pr&eacute;-rempli que l\'utilisateur ne pourra pas voir.', # NEW
	'saisie_hidden_titre' => 'Champ cach&eacute;', # NEW
	'saisie_input_explication' => '<NEW>Jednoduch&yacute; riadok textu, ktor&yacute; mo&#382;no zobrazi&#357; alebo skry&#357; (heslo).',
	'saisie_input_titre' => '<NEW>Textov&eacute; pole',
	'saisie_oui_non_explication' => 'Oui ou non, c\'est clair ? :)', # NEW
	'saisie_oui_non_titre' => 'Oui ou non', # NEW
	'saisie_radio_defaut_choix1' => 'Un', # NEW
	'saisie_radio_defaut_choix2' => 'Deux', # NEW
	'saisie_radio_defaut_choix3' => 'Trois', # NEW
	'saisie_radio_explication' => 'Permet de choisir une option parmis plusieurs disponibles.', # NEW
	'saisie_radio_titre' => 'Boutons radios', # NEW
	'saisie_selection_explication' => 'Choisir une option dans une liste déroulante.', # NEW
	'saisie_selection_multiple_explication' => 'Permet de choisir plusieurs options avec une liste.', # NEW
	'saisie_selection_multiple_titre' => 'S&eacute;lection multiple', # NEW
	'saisie_selection_titre' => 'Liste d&eacute;roulante', # NEW
	'saisie_textarea_explication' => '<NEW>Textov&eacute; pole s viacer&yacute;mi riadkami.',
	'saisie_textarea_titre' => 'Textarea', # MODIF

	// T
	'tous_visiteurs' => '<NEW>V&#353;etci n&aacute;v&#353;tevn&iacute;ci (aj nezaregistrovan&iacute;)',

	// V
	'vue_sans_reponse' => '<i>Sans réponse</i>', # NEW

	// Z
	'z' => 'zzz', # MODI
);

?>
