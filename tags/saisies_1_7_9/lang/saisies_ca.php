<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_annee' => 'Any',
	'label_jour' => 'Dia',
	'label_mois' => 'Mes',

	// O
	'option_attention_explication' => 'Un missatge m&eacute;s important que l\'explicaci&oacute;.',
	'option_attention_label' => 'Advert&egrave;ncia',
	'option_cacher_option_intro_label' => 'Amagar la primera elecci&oacute; buida',
	'option_choix_destinataires_explication' => 'Un o diversos autors, d\'entre els quals hi ha l\'usuari, podr&agrave; fer la seva tria. Si no hi ha res seleccionat, &eacute;s l\'autor que ha instal&middot;lat el lloc el que ser&agrave; escollit.',
	'option_choix_destinataires_label' => 'Possibles destinataris',
	'option_class_label' => 'Classes CSS suplement&agrave;ries',
	'option_cols_explication' => 'Amplada del bloc en n&uacute;mero de car&agrave;cters. Aquesta opci&oacute; no s\'aplica sempre ja que els estils CSS del vostre lloc el poden anul&middot;lar.',
	'option_cols_label' => 'Amplada',
	'option_datas_explication' => 'Heu d\'especificar una opci&oacute; per l&iacute;nia en la forma "cle|Label du choix"',
	'option_datas_label' => 'Llista d\'eleccions possibles',
	'option_defaut_label' => 'Valor per defecte',
	'option_disable_avec_post_explication' => 'Id&egrave;ntic que l\'opci&oacute; anterior per&ograve; tanmateix envia el valor dins d\'un camps amagat.',
	'option_disable_avec_post_label' => 'Desactivar per&ograve; enviar',
	'option_disable_explication' => 'El camp no pot obtenir el focus.',
	'option_disable_label' => 'Desactivar el camp',
	'option_explication_explication' => 'Si &eacute;s necessari, una frase curta descriu l\'objecte del camp.',
	'option_explication_label' => 'Explicaci&oacute;',
	'option_groupe_affichage' => 'Visualitzaci&oacute;',
	'option_groupe_description' => 'Descripci&oacute;',
	'option_groupe_utilisation' => 'Utilitzaci&oacute;',
	'option_groupe_validation' => 'Validaci&oacute;',
	'option_info_obligatoire_explication' => 'Podeu, per defecte, modificar la indicaci&oacute; d\'obligaci&oacute;: <i>[Obligatoire]</i>.',
	'option_info_obligatoire_label' => 'Indicaci&oacute; d\'obligaci&oacute;',
	'option_label_case_label' => 'Etiqueta situada al costat de la casella',
	'option_label_explication' => 'El t&iacute;tol que es mostrar&agrave;.',
	'option_label_label' => 'Etiqueta',
	'option_maxlength_explication' => 'L\'usuari no podr&agrave; escriure m&eacute;s car&agrave;cters que aquest nombre.',
	'option_maxlength_label' => 'N&uacute;mero m&agrave;xim de car&agrave;cters',
	'option_nom_explication' => 'Un nom inform&agrave;tic que identificar&agrave; el camp. Ha de contenir nom&eacute;s car&agrave;cters alfanum&egrave;rics min&uacute;sculs o el car&agrave;cter "_".',
	'option_nom_label' => 'Nom del camp',
	'option_obligatoire_label' => 'Camp obligatori',
	'option_option_intro_label' => 'Etiqueta de la primera elecci&oacute; buida',
	'option_pliable_label' => 'Plegable',
	'option_pliable_label_case' => 'El grup de camps es podr&agrave; replegar.',
	'option_plie_label' => 'Ja plegat',
	'option_plie_label_case' => 'Si el grup de camps &eacute;s plegable, ja estar&agrave; plegat a la visualitzaci&oacute; del formulari.',
	'option_readonly_explication' => 'El camp es pot llegir, seleccionar, per&ograve; no modificar.',
	'option_readonly_label' => 'Nom&eacute;s lectura',
	'option_rows_explication' => 'Al&ccedil;ada del bloc en n&uacute;mero de l&iacute;nies. Aquesta opci&oacute; no es pot aplicar sempre ja que els estils CSS del vostre lloc el poden anul&middot;lar.',
	'option_rows_label' => 'N&uacute;mero de l&iacute;nies',
	'option_size_explication' => 'Amplada del camp en n&uacute;mero de car&agrave;cters. Aquesta opci&oacute; no es pot aplicar sempre ja que els estils CSS del vostre lloc el poden anul&middot;lar. ',
	'option_size_label' => 'Mida del camp',
	'option_type_choix_plusieurs' => 'Permetr&agrave; a l\'usuari escollir <strong>diversos</strong> destinataris.',
	'option_type_choix_tous' => 'Posar <strong>tots</strong> aquests autors com a destinataris. L\'usuari no tindr&agrave; cap tria.',
	'option_type_choix_un' => 'Permetre a l\'usuari escollir <strong>un &uacute;nic</strong> destinatari.',
	'option_type_explication' => 'En mode "amagat", el contingut del camp no ser&agrave; visible.',
	'option_type_label' => 'Tipus del camp',
	'option_type_password' => 'Amagat',
	'option_type_text' => 'Normal',

	// S
	'saisie_case_explication' => 'Permet activar o desactivar alguna cosa.',
	'saisie_case_titre' => 'Casella &uacute;nica',
	'saisie_checkbox_explication' => 'Permet escollir diverses opcions amb caselles.',
	'saisie_checkbox_titre' => 'Caselles a marcar',
	'saisie_destinataires_explication' => 'Permet escollir un o diversos destinataris entre els autors seleccionats pr&egrave;viament.',
	'saisie_destinataires_titre' => 'Destinataris',
	'saisie_explication_explication' => 'Un text explicatiu general.',
	'saisie_explication_titre' => 'Explicaci&oacute;',
	'saisie_fieldset_explication' => 'Un quadre que podr&agrave; englobar diversos camps.',
	'saisie_fieldset_titre' => 'Grup de camps',
	'saisie_file_explication' => 'Enviament d\'un arxiu ',
	'saisie_file_titre' => 'Arxiu',
	'saisie_hidden_explication' => 'Un camp omplert pr&egrave;viament que l\'usuari no podr&agrave; veure.',
	'saisie_hidden_titre' => 'Camp amagat',
	'saisie_input_explication' => 'Una simple l&iacute;nia de text, que podr&agrave; ser visible o estar amagada (contrasenya).',
	'saisie_input_titre' => 'L&iacute;nia de text',
	'saisie_oui_non_explication' => 'Si o no, est&agrave; clar? :)',
	'saisie_oui_non_titre' => 'Si o no ',
	'saisie_radio_defaut_choix1' => 'Un',
	'saisie_radio_defaut_choix2' => 'Dos',
	'saisie_radio_defaut_choix3' => 'Tres',
	'saisie_radio_explication' => 'Permet escollir una opci&oacute; entre les diverses disponibles.',
	'saisie_radio_titre' => 'Botons r&agrave;dios',
	'saisie_selection_explication' => 'Escollir una opci&oacute; en una llista desplegable.',
	'saisie_selection_multiple_explication' => 'Permet escollir diverses opcions amb una llista.',
	'saisie_selection_multiple_titre' => 'Selecci&oacute; m&uacute;ltiple',
	'saisie_selection_titre' => 'Llista desplegable',
	'saisie_textarea_explication' => 'Un camp de text en diverses l&iacute;nies.',
	'saisie_textarea_titre' => 'Bloc de text',

	// T
	'tous_visiteurs' => 'Tots els visitants (fins i tot els no registrats)',

	// V
	'vue_sans_reponse' => '<i>Sense resposta</i>',

	// Z
	'z' => 'zzz'
);

?>
