<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_annee' => 'Jahr',
	'label_jour' => 'Tag',
	'label_mois' => 'Monat',

	// O
	'option_attention_explication' => 'Nachricht wichtiger als Meldung',
	'option_attention_label' => 'Achtung',
	'option_cacher_option_intro_label' => 'Erste leere Auswahl ausblenden',
	'option_choix_destinataires_explication' => 'Einer oder mehrere Empf&auml;nger, welche der Besucher ausw&auml;hlen kann. Wenn er keine Auswahl trifft, geht die Nachricht an den ersten Administrator (Autor 1).',
	'option_choix_destinataires_label' => 'M&ouml;gliche Empf&auml;nger',
	'option_class_label' => 'Zus&auml;tzliche CSS-Klassen',
	'option_cols_explication' => 'Breite des Blocks in Zeichen. Diese Option kann durch ihre CSS-Stile unwirksam grmacht werden.',
	'option_cols_label' => 'Breite',
	'option_datas_explication' => 'Sie m&uuml;ssen in jeder Zeile eine Option im Format "Schl&uuml;ssel|Bezeichnung" angeben.',
	'option_datas_label' => 'M&ouml;gliche Angaben',
	'option_defaut_label' => 'Standardwert',
	'option_disable_avec_post_explication' => 'Identisch mit voriger Option, jedoch wird der Wert in ein verstecktes Feld eingef&uuml;gt.',
	'option_disable_avec_post_label' => 'Senden trotz Deaktivierung',
	'option_disable_explication' => 'Das Feld erh&auml;lt keinen Fokus.',
	'option_disable_label' => 'Feld deaktivieren',
	'option_explication_explication' => 'Falls erforderlich kurze Beschreibung des Feldobjekts',
	'option_explication_label' => 'Erl&auml;uterung',
	'option_groupe_affichage' => 'Anzeige',
	'option_groupe_description' => 'Beschreibung',
	'option_groupe_utilisation' => 'Verwendung',
	'option_groupe_validation' => 'Best&auml;tigung',
	'option_info_obligatoire_explication' => 'Sie k&ouml;nnen die Standardbezeichnung f&uuml;r Pflichtfelder &auml;ndern:<i>[Pflichtfeld]</i>.',
	'option_info_obligatoire_label' => 'Pflichtfeld-Anzeige',
	'option_label_case_label' => 'Bezeichnung neben dem Feld',
	'option_label_explication' => 'Anzeigetitel',
	'option_label_label' => 'Bezeichnung',
	'option_maxlength_explication' => 'Der Besucher kann maximal diese Anzahl Zeichen eingeben',
	'option_maxlength_label' => 'Zeichen maximal',
	'option_nom_explication' => 'Reserviertes Wort f&uuml;r das Feld. Darf nur alphanumerische klein geschriebene und das Zeichen "_" (Unterstrich) enthalten.',
	'option_nom_label' => 'Feldname',
	'option_obligatoire_label' => 'Pflichtfeld',
	'option_option_intro_label' => 'Bezeichnung der ersten leeren Auswahl',
	'option_pliable_label' => 'Klappbar',
	'option_pliable_label_case' => 'Die Feldgruppe kann zugeklappt werden',
	'option_plie_label' => 'Bereits zugeklappt',
	'option_plie_label_case' => 'Wen die Feldgruppe klappbar ist, wird sie beim Anzeigen des Formulars zun&auml;chst zugeklappt angezeigt.',
	'option_readonly_explication' => 'Dieses Feld kann angezeigt aber nicht bearbeitet werden.',
	'option_readonly_label' => 'Nur Lesen',
	'option_rows_explication' => 'H&ouml;he des Blocks in Zeilen. Diese Option ist nicht immer wirksam, da sie von individuellen CSS-Stilen abgeschaltet werden kann.',
	'option_rows_label' => 'Anzahl Zeilen',
	'option_size_explication' => 'Breite des Felds in Zeichen. Diese Oprion ist nicht immer wirksam, da sie durch individuelle CSS.Stile abgeschaltet werden kann.',
	'option_size_label' => 'Feldgr&ouml;&szlig;e',
	'option_type_choix_plusieurs' => 'Auswahl <strong>mehrerer</strong> Empf&auml;nger erlauben',
	'option_type_choix_tous' => '<strong>Alle</strong> diese Autoren als Empf&auml;nger hinzuf&uuml;gen. Der Besucher kann keine Auswahl treffen.',
	'option_type_choix_un' => 'Der Besucher kann nur <strong>einen einzigen</strong> Empf&auml;nger ausw&auml;hlen.',
	'option_type_explication' => 'Im "versteckten" Modus wird der Inhalt dieses Felds nicht angezeigt.',
	'option_type_label' => 'Feldtyp',
	'option_type_password' => 'Versteckt',
	'option_type_text' => 'Normal',

	// S
	'saisie_case_explication' => 'Erm&ouml;glicht Dinge zu aktivieren und deaktivieren',
	'saisie_case_titre' => 'Nur eine Option',
	'saisie_checkbox_explication' => 'Erlaubt mehrer Optionen auszuw&auml;hlen',
	'saisie_checkbox_titre' => 'K&auml;stchen zum Abhaken',
	'saisie_destinataires_explication' => 'Erm&ouml;glicht mehrere Empf&auml;nger aus den vorgeschlagenen Autoren auszuw&auml;hlen',
	'saisie_destinataires_titre' => 'Empf&auml;nger',
	'saisie_explication_explication' => 'Allgemeine Beschreibung',
	'saisie_explication_titre' => 'Beschreibung',
	'saisie_fieldset_explication' => 'Ein Rahmen, der mehrere Felder enthalten kann',
	'saisie_fieldset_titre' => 'Feldgruppe',
	'saisie_file_explication' => 'Datei senden',
	'saisie_file_titre' => 'Datei',
	'saisie_hidden_explication' => 'Ein f&uuml;r den Nutzer unsichtbares, vorab ausgef&uuml;lltes Feld',
	'saisie_hidden_titre' => 'Verborgenes Feld',
	'saisie_input_explication' => 'Eine einfache Textzeile, kann angezeigt oder ausgeblendet werden (Passwort)',
	'saisie_input_titre' => 'Textzeile',
	'saisie_oui_non_explication' => 'Ja oder nein, alle klar ? :)',
	'saisie_oui_non_titre' => 'Ja oder nein',
	'saisie_radio_defaut_choix1' => 'Eins',
	'saisie_radio_defaut_choix2' => 'Zwei',
	'saisie_radio_defaut_choix3' => 'Drei',
	'saisie_radio_explication' => 'Erm&ouml;glicht eine Option aus mehreren verf&uuml;gbaren auszuw&auml;hlen',
	'saisie_radio_titre' => 'Radiokn&ouml;pfe',
	'saisie_selection_explication' => 'Eine Option aus einer Drop-Down-Liste ausw&auml;hlen.',
	'saisie_selection_multiple_explication' => 'Mehrere Optionen aus einer Liste ausw&auml;hlen',
	'saisie_selection_multiple_titre' => 'Mehrfachauswahl',
	'saisie_selection_titre' => 'Drop-Down-Liste',
	'saisie_textarea_explication' => 'Mehrzeiliges Textfeld',
	'saisie_textarea_titre' => 'Textblock',

	// T
	'tous_visiteurs' => 'Alle Besucher (auch nicht eingeschriebene)',

	// V
	'vue_sans_reponse' => '<i>Ohne Antwort</i>',

	// Z
	'z' => 'Zzz'
);

?>
