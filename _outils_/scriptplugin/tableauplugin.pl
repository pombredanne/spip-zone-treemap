#!/usr/bin/perl -w

use strict;
use warnings;
use File::Spec ;

open( OUT, ">tableaurecapilulatif.html") ;
my @dev_plugin = () ;
my @dev_localisation = () ;
my @test_plugin = () ;
my @test_localisation = () ;
my @experimental_plugin = () ;
my @experimental_localisation = () ;
my @stable_plugin = () ;
my @stable_localisation = () ;
my @autreetat_plugin = () ;
my @autreetat_localisation = () ;
my @autreetat_etat = () ;

sub trouver_etat{
  my $test_etat  = 0 ;
  my $test_fin = 0 ;
  my $etat = '' ;
  open ( HANDLE, "plugin.xml" ) ;
  while (<HANDLE>) {
    chomp( $_ );
    if ( $test_etat == 1 ) {
      $test_fin = 1 ;
      if ( $_ =~ /\s*(\w*)\s*/  ) {
	$etat = $1 ;
      }
      $test_etat = 0 ;
    }
    if ( $_ =~ /\s*<etat>\s*(\w+)\s*/ ) {
      $etat = $1 ;
      $test_fin = 1 ;
    } elsif ( $_ =~ /^\s*<etat>\s*$/ ) {
      $test_etat = 1 ;
    }
  }
  close(HANDLE) ;
  return $etat ;
}

sub classer_etat{
  my  @donnees   = @_ ;
  my $etat = shift(@donnees) ;
  my $plugin = shift(@donnees) ;
  my $localisation = shift(@donnees) ;

  if ( $etat eq "dev") {
    push( @dev_plugin, $plugin ) ;
    push( @dev_localisation, $localisation ) ;
  } elsif ( $etat eq "test") {
    push( @test_plugin, $plugin ) ;
    push( @test_localisation, $localisation ) ;
  } elsif ( $etat eq "experimental") {
    push( @experimental_plugin, $plugin ) ;
    push( @experimental_localisation, $localisation ) ;
  } elsif ( $etat eq "stable") {
    push( @stable_plugin, $plugin ) ;
    push( @stable_localisation, $localisation ) ;
  } else {
    push( @autreetat_plugin, $plugin ) ;
    push( @autreetat_localisation, $localisation ) ;
    push( @autreetat_etat, $etat ) ;
  }
}

my $chemin_absolu_local =  File::Spec->rel2abs( "." ) ;
foreach my $name (  <*> ) {
  chdir "$chemin_absolu_local" ;
  if ( -d $name ) {
    chdir "$chemin_absolu_local/$name" ;
    if ( -e "plugin.xml") {
      my $letat = trouver_etat() ;
      classer_etat( "$letat", "$name", "$name" ) ;
    } else {
      foreach my $name2 (  <*> ) {
	chdir "$chemin_absolu_local/$name" ;
	if ( -d $name2 ) {
	  chdir "$chemin_absolu_local/$name/$name2" ;
	  if ( -e "plugin.xml") {
            my $letat = trouver_etat() ;
	    classer_etat( "$letat", "$name2", "$name/$name2" ) ;
	  }
	}
      }
    }
  }
}

print OUT<<EOF;
<html>
<head>
<title> tableau recapilulatif des plugins </title>
<head>
<body>
<table>
<tr><td>Nom plugin</td><td> etat </td><td> localisation</td></tr>
EOF
my $nombre = @stable_plugin+ @test_plugin+@experimental_plugin+@dev_plugin+@autreetat_plugin ;
print OUT "<tr><td colspan\"3\">Il y a $nombre plugins </td></tr>\n" ;
print OUT "<tr><td colspan\"3\"> Plugin  a l etat stable </td></tr>\n" ;
foreach my $name (@stable_plugin) {
  print OUT  "<tr><td> $name</td><td>stable</td><td>".shift(@stable_localisation)."</td></tr> \n" ;
}
 $nombre = @stable_plugin ;
print OUT "<tr><td colspan\"3\">Il y a $nombre  plugins  a l etat stable </td></tr>\n" ;

print OUT  "<tr><td colspan\"3\"> Plugin  a l etat test </td></tr>\n" ;
foreach my $name (@test_plugin) {
  print OUT "<tr><td> $name</td><td>test</td><td>".shift(@test_localisation)."</td></tr> \n" ;
}
$nombre = @test_plugin ;
print OUT "<tr><td colspan\"3\">Il y a $nombre  plugins  a l etat test </td></tr>\n" ;

print OUT "<tr><td colspan\"3\"> Plugin  a l etat experimental </td></tr>\n" ;
foreach my $name (@experimental_plugin) {
  print OUT  "<tr><td> $name</td><td>experimental</td><td>".shift(@experimental_localisation)."</td></tr> \n" ;
}
$nombre = @experimental_plugin ;
print OUT "<tr><td colspan\"3\"> Il y a $nombre  plugins a l etat experimental </td></tr>\n" ;

print OUT "<tr><td colspan\"3\"> Plugin  a l etat dev </td></tr>\n" ;
foreach my $name (@dev_plugin) {
  print OUT "<tr><td> $name</td><td>dev</td><td>".shift(@dev_localisation)."</td></tr> \n" ;
}
$nombre = @dev_plugin ;
print OUT "<tr><td colspan\"3\"> Il y a $nombre  plugins a l etat dev </td></tr>\n" ;


print OUT  "<tr><td colspan\"3\"> Plugin dans un autre etat </td></tr>\n" ;
foreach my $name (@autreetat_plugin) {
  print OUT "<tr><td> $name</td><td>".shift(@autreetat_etat)."</td><td>".shift(@autreetat_localisation)."</td></tr> \n" ;
}
$nombre = @autreetat_plugin ;
print OUT  "<tr><td colspan\"3\">Il y a $nombre  plugins  dans un autre etat </td></tr>\n" ;

print OUT<<EOF;
</table>
</body>
</html>
EOF

close(OUT) ;
print " le fichier tableaurecapilulatif.html vient d'etre cr�e\n" ;

