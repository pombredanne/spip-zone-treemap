#! /usr/bin/bash
PROGNAME=`basename $0`
PARAMETRE="$*"

##############################################
## fonction Usage
## => affiche toutes les options utilisables 
##############################################
Usage() {
   echodate "*************************************************************************************************"
   echodate " Usage $1                                                                                        "
   echodate "   -H             Aide                                                                   "
   echodate "   -RACINE        Dossier du maitre spip RACINE (le repertoire ou se trouve tout le bazar) "
   echodate "   -DOMAINE       Dans le cas 3 (nom du domaine sous la forme piif.fr "
   echodate "   -SITE          Le site a creer  "
   echodate "   -MAITRE        Le spip de reference " 
   echodate "   -ENFANTS       Les sites spip qui en d�pendent "
   echodate "   -CAS           Bon faut vraiment trouver mieux !!! "
   echodate "   -H             Aide                                                                   "
   echodate "*************************************************************************************************"
   echodate " Exemple : -DOMAINE piif.fr -RACINE /var/www/html -MAITRE spip -ENFANTS spip/SPIPS -CAS 3 -SITE toto"
   exit 1;
}


################################################
## fonction echodate 
## => echo avec date systeme pour les logs 
################################################
echodate() {
   echo "[`date '+%d/%m/%Y - %T'`] : $1" 
}


################################################
## fonction isArgEmpty 
## => sort si l'argument pass� est vide 
################################################
isArgEmpty() {
   if [ "X$2" == "X" ]; then
      echodate "Argument obligatoire pour l'option $1 !"
      exit 2
   fi
}


################################################
## fonction isFlagHere 
## => sort si l'option n'est pas pass�e 
################################################
isFlagHere() {
   if [ "X$2" == "X" ]; then
      echodate "Flag $1 obligatoire !"
      exit 2
   fi
}


################################################
## fonction parseParam
## => recupere les parametres du scripts
################################################
parseParam() {

## recuperation des parametres
OPTIONFLAG="-"

while [ $# -ne 0 ]
do

   ARG=""
   OPTION=$1

   if ( [ $# -ne 1 ] ); then
      if ( [ `echo $2|cut -c1` != "${OPTIONFLAG}" ] ); then
         shift
         ARG=$1
      fi
   fi
   shift

   case ${OPTION} in
      -h|-H)
            Usage ${PROGNAME}
            exit 0
            ;;
      -cas|-CAS)
            OPT_CAS=${OPTION}
            ARG_CAS=${ARG}
            isArgEmpty ${OPTION} ${ARG}
            CAS="${ARG_CAS}"
            ;;
      -racine|-RACINE)
            OPT_RACINE=${OPTION}
            ARG_RACINE=${ARG}
            isArgEmpty ${OPTION} ${ARG}
            RACINE="${ARG_RACINE}"
            ;;
      -domaine|-DOMAINE)
            OPT_DOMAINE=${OPTION}
            ARG_DOMAINE=${ARG}
            DOMAINE="${ARG_DOMAINE}"
            ;;
      -site|-SITE)
            OPT_SITE=${OPTION}
            ARG_SITE=${ARG}
            isArgEmpty ${OPTION} ${ARG}
            SITE="${ARG_SITE}"
            ;;
      -enfants|-ENFANTS)
            OPT_ENFANTS=${OPTION}
            ARG_ENFANTS=${ARG}
            isArgEmpty ${OPTION} ${ARG}
            ENFANTS="${ARG_ENFANTS}"
            ;;
      -maitre|-MAITRE)
            OPT_MAITRE=${OPTION}
            ARG_MAITRE=${ARG}
            isArgEmpty ${OPTION} ${ARG}
            MAITRE="${ARG_MAITRE}"
            ;;
      -cas|-CAS)
            OPT_CAS=${OPTION}
            ARG_CAS=${ARG}
            isArgEmpty ${OPTION} ${ARG}
            CAS="${ARG_CAS}"
            ;;
      -*)
            echo "Option invalide : ${OPTION}"
            Usage ${PROGNAME}
            exit 1
            ;;
      *)
            echo "Mauvais parametre : ${OPTION} ${ARG}"
            Usage ${PROGNAME}
            exit 1
            ;;
   esac
done

}

#######################################################################################################
echodate "Debut de ${PROGNAME}"
echodate "Parametres de lancement : ${PARAMETRE}"


parseParam ${PARAMETRE}

isFlagHere SITE "${OPT_SITE}" ${PROGNAME}
isFlagHere RACINE "${OPT_RACINE}" ${PROGNAME}
isFlagHere CAS "${OPT_CAS}" ${PROGNAME}
isFlagHere ENFANTS "${OPT_ENFANTS}" ${PROGNAME}
isFlagHere MAITRE "${OPT_MAITRE}" ${PROGNAME}


# Cas 1 :
#  le spip de base (noyau) ici : http://mon_serveur.org/
#  et les sites l� : http://mon_serveur.org/mon_site/
#  C'est en fait le cas 2 avec mes_sites = /
# Cas 2 :
#  le spip de base (noyau) ici : http://mon_serveur.org/mes_spips/
#  et les sites l� : http://mon_serveur.org/mes_sites/mon_site/
# Cas 3 :
#  le spip de base (noyau) ici : http://mon_serveur.org/
#  et les sites dans http://mon_site.mon_serveur.org/ (mais redirigeant
#  vers l'emplacement physique identique � celui de http://mon_serveur.org/)






#�creer le bout d'arbo de repertoires, avec les droits qu'il faut

# Mais avant verifions qu'il n'existe pas deja 
if [ -d  ${RACINE}/${ENFANTS}/${SITE} ]; then
	echodate "ERROR repertoire ${RACINE}/${ENFANTS}/${SITE}  existe deja "
	exit -5 
else
	mkdir -p ${RACINE}/${ENFANTS}/${SITE}
	retour=$?
if [ ${retour} -eq 0 ] ; then 
		echodate "repertoire ${RACINE}/${ENFANTS}/${SITE} cree "
	else 
		echodate "ERROR impossible de creer ${RACINE}/${ENFANTS}/${SITE} "
	fi
	chmod 755 ${RACINE}/${ENFANTS}/${SITE}
fi

#Creation des repertoires necessaires pour le site "esclave"
for dir in config tmp var IMG ; do
	mkdir -p ${RACINE}/${ENFANTS}/${SITE}/$dir
	chmod 777 ${RACINE}/${ENFANTS}/${SITE}/$dir
done

for dir in squelettes/formulaires upload ; do
	mkdir -p ${RACINE}/${ENFANTS}/${SITE}/$dir
	chmod 755 ${RACINE}/${ENFANTS}/${SITE}/$dir
done

#�si necessaire, creer le .htaccess par defaut
if [ ! -f ${RACINE}/${MAITRE}/.htaccess ] ; then
	cp ${RACINE}/${MAITRE}/htaccess.txt ${RACINE}/${MAITRE}/.htaccess

	case ${CAS} in
		1|2)
		rempl=${MAITRE}
		;;
		3)
		rempl="/\\n\\nRewriteCond %{SERVER_NAME} ^(www\.)?(.+).${DOMAINE}\\$\\nRewriteRule (.*) %2/\\\$1 [QSA,L]"
	esac

	perl -pi -e "s!^\#?RewriteBase .*\$!RewriteBase $rempl!" ${RACINE}/${MAITRE}/.htaccess

fi

if [ ! ${CAS} -eq 3 ] ; then
	case ${CAS} in
		1)
		ajout="RewriteRule ^${SITE}/(.*) /\\\$1 [QSA,L]"
		;;
		2)
		ajout="RewriteRule ^${SITE}/(.*) ${MAITRE}/\\\$1 [QSA,L]"
		;;
	esac
	perl -pi -e "s!^.*REGLAGES PERSONNALISES.*\$!$&\\n\# site 'multi spip' ${SITE}\\nRewriteRule ^${SITE}\\$ ${SITE}/ [R,L]\\n$ajout\\n!" ${RACINE}/${MAITRE}/.htaccess
fi
