<?php

	/*
	 * Ce script, appele depuis l'exterieur, va provoquer une mise a jour de la
	 * base de donnees. Il faut evidemment le securiser etc. si on compte
	 * l'employer "en prod" ; repond a la question posee par le ticket
	 *  <http://trac.rezo.net/trac/spip/ticket/186>
	 */

	//
	// connexion spip
	//
	chdir("$home");
	include "ecrire/inc_version.php";

	// mise a jour
	echo "maj shim $shim<br />\n";

#	include_ecrire('inc_mini_pres.php');
#	include_ecrire('inc_presentation.php3');
	spip_connect();
	if (!$db_ok) {
		echo "Erreur de connexion MySQL.";
		exit;
	}

	include_ecrire('inc_meta');
	if ($spip_version == lire_meta("version_installee"))
		echo "OK<br />version $spip_version deja ok";
	else if (spip_version <= lire_meta("version_installee"))
		echo "euh, tu descends d'une version ($spip_version) ?????";
	else {
		include_spip('base/create');
		echo "Mise a jour depuis ".lire_meta("version_installee")." vers $spip_version";
		creer_base();
		maj_base();
	}

	// appliquer la config par defaut (nouveaux metas par exemple)
	include_spip('inc/config');
	init_config();

?>
