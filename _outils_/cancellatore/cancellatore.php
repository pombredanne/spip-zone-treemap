<?php
/*
Ce script fut connu comme togggdeletor.php

VOUS NE DEVEZ EN AUCUN CAS LE LAISSER SUR VOTRE SERVEUR :

Il sert a detruire ce que httpd possede
et aide a nettoyer ce que ftp ou ssh ne possede pas.
Vous devez le renommer pour qu'il fonctionne.

Le champs de saisie : ce que vous voulez nettoyer, click on "list"
Si OK, un deuxieme bouton "clean" apparaitra.
Ce truc est rudimentaire et ne peut detruire que ce sur quoi httpd a des droits.
Eventuellement, il faut jongler avec FTP en meme temps.

Bon courage.
God save the files.
*/
// le script n'a pas le droit de s'appeler cancelatore
if (preg_match('#xcancellatore\.php#', __FILE__)) {
	die('rename the script !');
}

/***
*	Détruit tant que possible et récursivement l'argument
***/
function delfil($fil)
{
	// un fichier simple , on le kill et on retourne
    if (!is_dir($fil)) {
        return unlink($fil);
    }
    
    // un répertoire, on l'ouvre et on traite récursivement ses fichiers
    $dh=opendir($fil);
    while (false !== ($subfil=readdir($dh))) {
    	// pas le répertoire sauf lui-même et son pêre
        if ($subfil!="." && $subfil!="..") {
            $fullpath=$fil."/".$subfil;
            if (!delfil($fullpath)) {
                die("error by {$fullpath}");
            }
        }
    }
    closedir($dh);
    // finalement, on pète le répertoire lui-même
    if(rmdir($fil)) {
        return true;
    } else {
        die("cannot remove folder {$fil}");
        return false;
    }
}

/***
*	Liste récursivement l'argument
***/
function lisfil($fil)
{
	$writable = is_writable($fil) ? ' writable' : '';
    if (!is_dir($fil)) {
        return ' <span class="file' . $writable . '">' .
        	htmlentities(basename($fil)) . '</span>';
    }
    $ret = '<div class="folder' . $writable . '">
    	<span class="foldernam">' . htmlentities($fil) . '<br /></span>';
/*	if (!$writable) {
		return $ret . '</div>';
	}
*/
    $dh=opendir($fil);
    $sub = array();
    while (false !== ($subfil=readdir($dh))) {
        if ($subfil!="." && $subfil!="..") {
            $sub[$subfil] = $fil."/".$subfil;
        }
    }
    ksort($sub);
    foreach ($sub as $subfil => $fullpath) {
        $ret .= lisfil($fullpath);
    }
    closedir($dh);
	return $ret . '</div>';
}

/***
*	Main
***/

$form = $fil = $out = '';
if (isset($_POST['fil'])) {
    $fil = $_POST['fil'];
	$out = lisfil($fil);
	$md5 = md5($out);
	$form .= '<input type="hidden" name="md5" value="'.$md5.'">';
	if (isset($_POST['md5']) && $_POST['md5'] == $md5) {
	    if (isset($_POST['clean']) && $_POST['clean']) {
	        delfil($fil);
	        $out = lisfil($fil);
	    } else {
	        $form .= '<input type="submit" name="xclean" value="delete">';
	    }
    }
}

echo <<<EOF
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Cancellatore pka togggdeletor</title>
	<style>
		body {
			background-color: #C9E0DD;
		}
		.folder, .file {
			background-color: #FFF;
			margin: 7px; 
		}
		.folder {
			color: red;
			display: block;
			border: green 1px solid;
		}
		.file {
			color: black;
			display: inline;
			border: red 1px solid;
		}
		.writable {
			background-color: #FEE;
		}
	</style>
</head>
<body>
	<h2>Cancellatore pka togggdeletor</h2>
	<form method="post" name="deletor">
		<input type="text" name="fil" value="{$fil}" >
		<input type="submit" name="list" value='list'>
		{$form}
	</form>
	<span class="folder" style="display: inline; float: right;">folder</span>
	<span class="file" style="display: inline; float: right;">file</span>
	<span class="writable folder" style="display: inline; float: right;">writable folder</span>
	<span class="writable file" style="display: inline; float: right;">writable file</span>
	{$out}
</body>
</html>

EOF;

?>
