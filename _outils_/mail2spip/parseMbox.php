<?php

// script quick and dirty pour traiter un ou plusieurs mail
// php splitMbox.php -spip /var/www/html/spip -mbox /home/pif/.evolution/mail/local/Inbox.sbd/spip

// A REVOIR : pr�voir le cas improbable d'une boucle
// sinon, on peut planter le spip en envoyant un mail aux headers bricol�s

setlocale(LC_TIME, 'fr_FR', 'fr');

// le fichier d'entree
$mbox= null;

// le repertoire ou se trouve l'arbo du site spip
// dans lequel il faut inserer les messages
$spipDir= null;

$rubRef= 1;
$autRef= 1;

// analyse de la ligne de commande
array_shift($argv);

while(count($argv)!=0) {
  $opt= array_shift($argv);
  switch($opt) {
  case '-rubrique':
	$rubRef= array_shift($argv);
	break;
  case '-spip':
	$spipDir= array_shift($argv);
	break;
  case '-mbox':
	$mbox= array_shift($argv);
	break;
  default:
	die("option $opt ???\n");
	break;
  }
}

if($mbox==null) {
	die("fichier mbox obligatoire");
}

function Dspip_query($q) {
	//echo "QUERY $q.\n";
	return spip_query($q);
}

function Dspip_abstract_insert($t, $c, $v) {
	//echo "INSERT $t($c)($v).\n";
	return spip_abstract_insert($t, $c, $v);
}

// verifier la connexion a la bdd
define('_DIR_RACINE', "$spipDir/");
define('_DIR_RESTREINT_ABS', "$spipDir/ecrire/");
define('_DIR_RESTREINT', _DIR_RESTREINT_ABS);
include("$spipDir/ecrire/inc_version.php");
spip_connect();
include_spip("base/abstract_sql");

$r= spip_abstract_select(array("id_article, titre"),
						 array("spip_articles"),
						 array(array('=', 'id_rubrique', $rubRef)));
if (!$r) {
  die("echec d'acces a la bdd (".mysql_error($r).")\n");
}

$articles= array();
while($a= spip_fetch_array($r)) {
	$articles[$a['titre']]= $a['id_article'];
}

// ouvrir le fichier mbox
$cnx= imap_open($mbox, null, null, OP_READONLY);
if (!$cnx) {
	var_export(imap_errors());
	die("echec open de $mbox\n");
}

for($i=1; $i<=imap_num_msg($cnx); $i++) {
	$headers= imap_headerinfo($cnx, $i);
	$bodystr= imap_fetchstructure($cnx, $i);

	// recup�rer l'id et l'�ventuel id parent
	if(preg_match('/<(.*?)>/', $headers->in_reply_to, $re)) {
		$parent= $re[1];
	} elseif(preg_match('/.*<(.*?)>/', $headers->references, $re)) {
		$parent= $re[1];
	} else {
		$parent= null;
	}
	//echo var_export($headers, 1)."->$parent\n";

	if(!($id= $headers->message_id)) {
		echo("message sans id ! -> on passe\n");
		continue;
	}
	if(preg_match('/^<(.*)>$/', $id, $re)) {
		$id= $re[1];
	}

	//echo "$id => ".var_export($bodystr, 1)."\n";

	// r�cup�rer le sujet, la date, l'auteur et le texte
	$subjectArray= imap_mime_header_decode($headers->subject);
	$subject='';
	foreach($subjectArray as $item) {
		if($item->charset=='default'
		   || preg_match('/iso-?8859-?1/i', $item->charset)) {
			$subject.=$item->text;
		} else {
			$subject.='??????';
		}
	}
	if(!$subject) $subject= 'sans titre';

	$time= strtotime($headers->date);
	$mois= strftime('%Y-%m', $time);
	$date= strftime('%Y-%m-%d %H:%M:%S', $time);

	$auteur= $headers->fromaddress;

	$auteurArray= imap_mime_header_decode($headers->fromaddress);
	$auteur='';
	foreach($auteurArray as $item) {
		if($item->charset=='default' || $item->charset=='iso-8859-1') {
			$auteur.=$item->text;
		} else {
			$auteur.='??????';
		}
	}
	if(!$auteur) $auteur= 'anonyme';

	$auteur= preg_replace_callback('/<(.*)>/', 'obfuscate', $auteur);

	$texte= trouverTexte($bodystr, $cnx, $i);

	// ICI, on a un message d'id $id (pas vide) et de parent direct
	// $parent (eventuellement vide)

	// par contre on ne sais pas encore ou on va l'accrocher
	$article= null;

	//echo "$id <- $parent\n";

	// il y a alors plusieurs possibilites :
	// - le message est en base ou pas
	//   - en base = vide, et sert d'ancetre a d'autres
	// - il a un parent ou pas
    //   - pas de parent = message racine
	// - s'il a un parent, il est en base ou pas
    //   - parent en base = cas "normal", ou message vide ayant servi
	//     a en accrocher d'autres

	// r�cup�rer l'�ventuel message existant
	$f_id= spip_abstract_fetsel(
			array('id_forum', 'id_parent', 'id_thread', 'url_site', 'texte'),
			array('spip_forum'),
			array(array('=', 'url_site', "'$id'")));

	if($f_id) {
		$quoi='update';
		if($f_id['texte']) {
			echo("message $id deja en base !\n");
			continue;
		}
		if($f_id['id_forum']!=$f_id['id_thread']) {
			echo("message $id deja attache !\n");
			continue;
		}
		$id_forum= $f_id['id_forum'];
	} else {
		// sinon, faudra en cr�er un nouveau
		$quoi='insert';
	}

	// si un parent est specifie
	if($parent) {
		$f_parent= spip_abstract_fetsel(
			array('id_forum', 'id_parent','id_thread', 'url_site', 'id_article'),
			array('spip_forum'),
			array(array('=', 'url_site', "'$parent'")));

		// mais qu'on ne l'a pas en bdd
		if(!$f_parent) {
			$article= trouverArticle($mois, $time);

			// on en cr�e un vide
			$id_parent= Dspip_abstract_insert(
				'spip_forum',
				"(id_article, statut, url_site, titre)",
				"($article, 'publie', '$parent', '???')");
			if(!$id_parent) {
				die(mysql_error());
			}
			// qui est isole
			$r= Dspip_query("UPDATE spip_forum"
					  ."   SET id_parent=0, id_thread=$id_parent"
					  ." WHERE id_forum=$id_parent");
			if(!$r) {
				die(mysql_error());
			}

			// et on y accroche le message courant
			$id_parent= $id_thread= $id_parent;
		} else {
			// sinon (parent deja en bdd) on y accroche le message courant
			$id_parent =$f_parent['id_forum'];
			$id_thread =$f_parent['id_thread'];
			$article= $f_parent['id_article'];
		}

		// dans les 2 cas, si le message etait deja en bdd, c'etait pour
		// �tre racine de gamins qui changent donc d'ancetre
		if($quoi=='update') {
			$r= Dspip_query("UPDATE spip_forum"
				  ."   SET id_thread=".$id_thread
				  ." WHERE id_thread=".$id_forum);
			if(!$r) {
				die(mysql_error());
			}
		}
	} else {
		$id_parent= 0;
		if($quoi=='update') {
			$id_thread= $id_forum;
		} // else on ne le saura qu'apres l'insert
	}

	if(!$article) {
		$article= trouverArticle($mois, $time);
	}

	if($quoi=='update') {
		$r= Dspip_query("UPDATE spip_forum"
				."   SET date_heure='$date', id_article=$article,"
				."      id_parent= $id_parent, id_thread=$id_thread,"
				."      titre='".addslashes(utf8_encode($titre))."',"
				."      auteur='".addslashes(utf8_encode($auteur))."',"
				."      texte='".addslashes(utf8_encode($texte))."'"
				." WHERE id_forum=$id_forum");
		if(!$r) {
			die(mysql_error());
		}
	} else {
		if($parent) {
			$id_forum= Dspip_abstract_insert(
				'spip_forum',
				"(id_article, statut, url_site, date_heure, titre,"
				."auteur, texte, id_parent, id_thread)",
				"($article, 'publie', '$id', '$date', '"
				.addslashes(utf8_encode($subject))."', '"
				.addslashes(utf8_encode($auteur))."','"
				.addslashes(utf8_encode($texte))."', $id_parent, $id_thread)");
			if(!$id_forum) {
				die(mysql_error());
			}
		} else {
			$id_forum= Dspip_abstract_insert(
				'spip_forum',
				"(id_article, statut, url_site, date_heure, titre,"
				." auteur, texte)",
				"($article, 'publie', '$id','$date', '"
				.addslashes(utf8_encode($subject))."', '"
				.addslashes(utf8_encode($auteur))."', '"
				.addslashes(utf8_encode($texte))."')");
			if(!$id_forum) {
				die(mysql_error());
			}
			$r= Dspip_query("UPDATE spip_forum"
				  ."   SET id_parent=0, id_thread=$id_forum"
				  ." WHERE id_forum=$id_forum");
			if(!$r) {
				die(mysql_error());
			}
		}
	}
}

function trouverTexte($struct, $cnx, $n, $position=null) {
	//echo "$position $struct->subtype \n";
	if($struct->subtype=='PLAIN' || $struct->subtype=='HTML') {
		if(!$position) {
			$texte= imap_body($cnx, $n);
		} else {
			$texte= imap_fetchbody($cnx, $n, $position);
		}
		$texte= decoder($texte, $struct->encoding);
		//echo "decoder => $texte\n";
		if($struct->subtype=='PLAIN') {
			$texte= str_replace('&', '&amp;', $texte);
			$texte= str_replace('<', '&lt;', $texte);
			$texte= str_replace("\n", "\n_ ", $texte);
			return $texte;
		} else {
			return "<html>$texte</html>";
		}
	}

	if($struct->parts) {
		return trouverTexte($struct->parts[0], $cnx, $n, 
							$position?"$position.1":"1");
	}

	return "?????";
}

function decoder($message, $coding) {
	//echo "decoder($coding, $message)\n\n\n\n";
	if ($coding == 0) {
		return $message;//imap_7bit($message);
	} elseif ($coding == 1) {
		return $message;//imap_8bit($message);
	} elseif ($coding == 2) {
		return $message;//imap_binary($message);
	} elseif ($coding == 3) {
		return imap_base64($message);
	} elseif ($coding == 4) {
		return imap_qprint($message);
	} elseif ($coding == 5) {
		return $message;
	} else {
		// ???
		return $message;
	}
}

function trouverArticle($mois, $time) {
	global $articles, $autRef, $rubRef;

	if($articles[$mois]) {
		return $articles[$mois];
	}
	// sinon, cr�er l'article pour ce mois
	$id= Dspip_abstract_insert(
		'spip_articles',
		"(titre, surtitre, id_rubrique, statut, date)",
		"('$mois', '".utf8_encode(strftime("%B %Y", $time))
		."', $rubRef, 'publie', '".strftime("%Y-%m-01", $time)
		."')");
	if(!$id) {
		echo("erreur sur insert article $mois : ".mysql_error()."\n");
		return;
	}
	Dspip_abstract_insert('spip_auteurs_articles',
							   "(id_auteur, id_article)",
							   "($autRef, $id)");
	$articles[$mois]= $id;
	return $id;
}

function obfuscate($adr) {
	$a= str_replace('.', ' <i>POINT</i> ', $adr[1]);
	$a= str_replace('@', ' <i>�</i> ', $a);
	return "<font size='-2'>&lt;$a&gt;</font>";
}

?>
