<?php

// script quick and dirty pour traiter un ou plusieurs mail
// php splitMbox.php -spip /var/www/html/spip -mbox ~/.evolution/mail/local/Inbox.sbd/spip

// A REVOIR : pr�voir le cas improbable d'une boucle
// sinon, on peut planter le spip en envoyant un mail aux headers bricol�s

// le fichier d'entree
$mbox= '-';

// le repertoire ou se trouve l'arbo du site spip
// dans lequel il faut inserer les messages
$spipDir= null;

$articleRef= 1;

// analyse de la ligne de commande
array_shift($argv);

while(count($argv)!=0) {
  $opt= array_shift($argv);
  switch($opt) {
  case '-article':
	$articleRef= array_shift($argv);
	break;
  case '-spip':
	$spipDir= array_shift($argv);
	break;
  case '-mbox':
	$mbox= array_shift($argv);
	break;
  default:
	die("option $opt ???\n");
	break;
  }
}

function Dspip_query($q) {
	echo "QUERY $q.\n";
	return spip_query($q);
}

function Dspip_abstract_insert($t, $c, $v) {
	echo "INSERT $t($c)($v).\n";
	return spip_abstract_insert($t, $c, $v);
}

// verifier la connexion a la bdd
define('_DIR_RACINE', "$spipDir/");
define('_DIR_RESTREINT_ABS', "$spipDir/ecrire/");
define('_DIR_RESTREINT', _DIR_RESTREINT_ABS);
include("$spipDir/ecrire/inc_version.php");
spip_connect();
include_spip("base/abstract_sql");

$r= spip_abstract_select(array("count(*)"), array("spip_articles"));
if (!$r || !spip_abstract_fetch($r)) {
  die("echec d'acces a la bdd (".mysql_error($r).")\n");
}


// ouvrir le fichier d'entree
if($mbox=='-') {
	$f= fopen('php://stdin', 'r');
} else {
	$f= fopen($mbox, 'r');
}


// On cherche des blocs s�par�s par "^From "
$mail=null;
while($l=fgets($f)) {
	if(substr($l, 0, 5)=='From ') {
		if($mail!=null) {
			send($mail);
		}
		$mail=$l;
	} else {
		$mail.=$l;
	}
}

if($mail!=null) {
	send($mail);
}

function send($mail) {
	global $articleRef;

	$p= strpos($mail, "\n\n");
	$head= substr($mail, 0, $p);
	$body= substr($mail, $p+1);

	$headers= imap_rfc822_parse_headers($head);

	if(preg_match('/<(.*?)>/', $headers->in_reply_to, $re)) {
		$parent= $re[1];
	} elseif(preg_match('/.*<(.*?)>/', $headers->references, $re)) {
		$parent= $re[1];
	}
	//echo var_export($headers, 1)."->$parent\n";

	if(!($id= $headers->message_id)) {
		echo("message sans id ! -> on passe");
		return;
	}

	if(preg_match('/^<(.*)>$/', $id, $re)) {
		$id= $re[1];
	}

	// ICI, on a un message d'id $id (pas vide) et de parent direct
	// $parent (eventuellement vide)

	echo "$id <- $parent\n";

	$subjectArray= imap_mime_header_decode($headers->subject);
	$subject='';
	foreach($subjectArray as $item) {
		if($item->charset=='default' || $item->charset=='iso-8859-1') {
			$subject.=$item->text;
		} else {
			$subject.='??????';
		}
	}

	$date= strftime('%Y-%m-%d %H:%M:%S', strtotime($headers->date));

	$texte= substr($body, 0, 30);

	// il y a alors plusieurs possibilites :
	// - le message est en base ou pas
	//   - en base = vide, et sert d'ancetre a d'autres
	// - il a un parent ou pas
    //   - pas de parent = message racine
	// - s'il a un parent, il est en base ou pas
    //   - parent en base = cas "normal", ou message vide ayant servi
	//     a en accrocher d'autres

	// r�cup�rer l'�ventuel message existant
	$f_id= spip_abstract_fetsel(
			array('id_forum', 'id_parent', 'id_thread', 'url_site', 'texte'),
			array('spip_forum'),
			array(array('=', 'url_site', "'$id'")));

	if($f_id) {
		$quoi='update';
		if($f_id['texte']) {
			echo("message $id deja en base !");
			return;
		}
		if($f_id['id_forum']!=$f_id['id_thread']) {
			echo("message $id deja attache !");
			return;
		}
	} else {
		// sinon, en cr�er un nouveau
		$quoi='insert';
		$f_id=array(
				'url_site' => $id,
				'titre' => $header['subject']?$header['subject']:'sans titre',
				'texte' => $texte);
	}

	// si un parent est specifie
	if($parent) {
		$f_parent= spip_abstract_fetsel(
			array('id_forum', 'id_parent', 'id_thread', 'url_site'),
			array('spip_forum'),
			array(array('=', 'url_site', "'$parent'")));

		// mais qu'on ne l'a pas en bdd
		if(!$f_parent) {
			// on en cr�e un vide
			$id_parent= Dspip_abstract_insert(
				'spip_forum', "(id_article, statut, url_site)",
				"($articleRef, 'publie', '$parent')");
			if(!$id_parent) {
				die(mysql_error());
			}
			// qui est isole
			$r= Dspip_query("UPDATE spip_forum"
					  ."   SET id_parent=0, id_thread=$id_parent"
					  ." WHERE id_forum=$id_parent");
			if(!$r) {
				die(mysql_error());
			}

			// et on y accroche le message courant
			$f_id['id_parent']= $f_id['id_thread']= $id_parent;
		} else {
			// sinon (parent deja en bdd) on y accroche le message courant
			$f_id['id_parent']=$f_parent['id_forum'];
			$f_id['id_thread']=$f_parent['id_thread'];
		}

		// dans les 2 cas, si le message etait deja en bdd, c'etait pour
		// �tre racine de gamins qui changent donc d'ancetre
		if($quoi=='update') {
			$r= Dspip_query("UPDATE spip_forum"
				  ."   SET id_thread=".$f_id['id_thread']
				  ." WHERE id_thread=".$f_id['id_forum']);
			if(!$r) {
				die(mysql_error());
			}
		}
	}

	if($quoi=='update') {
		$r= Dspip_query("UPDATE spip_forum"
			  ."   SET texte='".addslashes($texte)."'"
			  ." WHERE id_forum=".$f_id['id_forum']);
		if(!$r) {
			die(mysql_error());
		}
	} else {
		if($parent) {
			$id_id= Dspip_abstract_insert(
				'spip_forum',
				"(id_article, statut, url_site, titre, texte,"
				." id_parent, id_thread)",
				"($articleRef, 'publie', '".$f_id['url_site']."', '"
				.addslashes($f_id['titre'])."', '"
				.addslashes($texte)."',"
				.$f_id['id_parent'].",".$f_id['id_thread'].")");
			if(!$id_id) {
				die(mysql_error());
			}
		} else {
			$id_id= Dspip_abstract_insert(
				'spip_forum',
				"(id_article, statut, url_site, titre, texte)",
				"($articleRef, 'publie', '".$f_id['url_site']."','"
				.addslashes($f_id['titre'])."', '"
				.addslashes($texte)."')");
			if(!$id_id) {
				die(mysql_error());
			}
			$r= Dspip_query("UPDATE spip_forum"
				  ."   SET id_parent=0, id_thread=$id_id"
				  ." WHERE id_forum=$id_id");
			if(!$r) {
				die(mysql_error());
			}
		}
	}

}

?>
