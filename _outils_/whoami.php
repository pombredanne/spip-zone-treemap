<?php

  /** script minimal definissant une fonction qui permet de determiner
   * l'id_auteur courant (null si anonyme)
   * utilisable pour identifier une session a partir d'un script externe a spip
   * attention : l'appelant doit etre sous l'arbo de spip, sans quoi le cookie
   * de session n'est pas visible
   */

  // le point d'init
define('_DIR_RACINE', ".");

// le minimum vital pour être en "environnement spip"
define('_DIR_RESTREINT_ABS', _DIR_RACINE."/ecrire/");
define('_DIR_RESTREINT', _DIR_RESTREINT_ABS);
include(_DIR_RACINE."/ecrire/inc_version.php");

// retourne null pour un internaute anonyme, sinon, son id_auteur
function whoami() {
	if ($_COOKIE['spip_session']) {
		$var_f = charger_fonction('session', 'inc');
		if ($connect_id_auteur = $var_f()) {
			return $connect_id_auteur;
		}
	}
	return null;
}

// exemple d'appel :
// echo whoami();

?>
