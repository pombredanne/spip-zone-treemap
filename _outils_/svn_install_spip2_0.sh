#! /bin/sh
# Un petit script pour automatiser l'installation de SPIP 2 (branche stable)
# et de quelques plugins d'utilite
# le tout en une seule ligne de commande :
# ./svn_install_spip2_0.sh
# 
# Script original de Cedric MORIN | cedric {CHEZ} yterium {point} net
# Revu et corrige pour SPIP 2 (branche stable 2.0.x)
# le 2 Juillet 2009
# par Loiseau2nuit | l.oiseau2nuit {CHEZ} gmail {point} com
#
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!! livre tel quel sans aucune garantie du gouvernement                          !!!
# !!! l'auteur decline toute responsabilite en cas de degat sur un serveur de prod !!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


# INSTALLATION DE SPIP 
echo "Installation de SPIP branche 2.0.x en cours. Merci de patienter..."
echo "Currently installing SPIP 2.0 stable release. Please wait ..."
svn checkout svn://trac.rezo.net/spip/branches/spip-2.0 spip
cd spip

echo "Creation des dossiers additionnels et attribution des droits en ecriture"
echo "Creating and Chmoding additionnal directories"
# ON CONSTRUIT LES REPERTOIRES ADDITIONNELS USUELS et on leur donne les droits en ecriture
mkdir plugins
mkdir lib
mkdir themes
mkdir squelettes

# ON ATTRIBUE LES DROITS EN ECRITURE SUR LES DOSSIERS
chmod 755 IMG
chmod 755 local
chmod 755 config
chmod 755 tmp
chmod 755 ecrire
chmod 755 plugins
chmod 755 themes
chmod 755 lib
echo "OK !"


# INSTALLATION DES PLUGINS
# On construit au prealable un dossier par type de fonctionalites pour les ranger
# mettre les lignes "mkdir..." et "cd ..." en commentaires (rajouter # devant) pour avoir tous vos plugins
# � la racine du dossier /plugins
echo "Installation des plugins et squelettes en cours, merci de patienter ..."
echo "Installing plugins and templates, please wait ..."

# Installation des squelettes
svn checkout svn://zone.spip.org/spip-zone/_themes_ themes
sleep 4;
# partie � affiner en fonction de vos besoins
# ou m�nage � faire une fois votre zoning choisis
cd plugins
mkdir squel
cd squel
svn checkout svn://zone.spip.org/spip-zone/_squelettes_/zpip
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/zen-garden
sleep 4;
cd ..

# LISTE ADMINISTRATION
mkdir admin
cd admin
svn checkout svn://zone.spip.org/spip-zone/_plugins_/cfg
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/spip-bonux-2
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/autorite
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/champs_extras2
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/compositions
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/crayons
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/gestion_documents 
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/menus 
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/boutons_admin_supp
sleep 4;
# svn checkout svn://zone.spip.org/spip-zone/_plugins_/bandeau bandeau
# sleep 4;
# D�sactiv� car K.O. depuis 2.0.9 aux derni�res nouvelles...
svn checkout svn://zone.spip.org/spip-zone/_plugins_/couteau_suisse
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/en_travaux
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/exclure_secteur
sleep 4;
# svn checkout svn://zone.spip.org/spip-zone/_plugins_/mots_partout
# sleep 4;
# A tester avant de g�n�raliser...
# svn checkout svn://zone.spip.org/spip-zone/_plugins_/mots_techniques
# sleep 4;
# Selon moi SPIP a atteint un niveau de maturit� permettant de se passer des mot-techniques. Maintenant c'est vous qui voyez...
svn checkout svn://zone.spip.org/spip-zone/_core_/plugins/porte_plume porte_plume_core
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/porte_plume porte_plume_extensions
sleep 4;
cd ..

# MODULE SUPPLEMENTAIRES POUR INTEGRATION
mkdir integr
cd integr
svn checkout svn://zone.spip.org/spip-zone/_plugins_/fonctions_images fonctions_images
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/noie
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/skiplink
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/thinckbox2
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/socialtags
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/tablesorter
sleep 4;
cd ..

# QUELQUES FONCTIONALITEES ADDITIONNELS
mkdir mods
cd mods
svn checkout svn://zone.spip.org/spip-zone/_fondation_/forms_1_9_3_foireux_mais_parfois_utile formulaires
sleep 4;
# Parce que son nom l'indique plut�t bien... alors pas troller et pas taper, merci !!!
svn checkout svn://zone.spip.org/spip-zone/_plugins_/fulltext
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/metas
sleep 4;
svn checkout svn://zone.spip.org/spip-zone/_core_/plugins/urls_etendues
sleep 4;
echo "OK !"
cd ..

echo "Votre Installation de SPIP branche 2.0.x (version stable) est termin�e. Veuillez v�rifier que tout s'est d�roul� correctement puis rendez-vous sur http://votre_site_spip/ecrire afin de configurer votre site SPIP"
echo "Pour effectuer vos mises � jour : 'svn checkout svn://zone.spip.org/spip-zone/_outils_/svn_updater_spip2_0.sh' puis 'chmod +x svn_updater_spip2_0.sh' puis enfin './svn_updater_spip2_0.sh' "

echo "Your SPIP 2.0.X stable release is now ready. Please check that all went correctly then, go to http://votre_site_spip/ecrire to set your SPIP site up"
echo "If you want to upgrade your installation : 'svn checkout svn://zone.spip.org/spip-zone/_outils_/svn_updater_spip2_0.sh' then 'chmod +x svn_updater_spip2_0.sh' then './svn_updater_spip2_0.sh' "
