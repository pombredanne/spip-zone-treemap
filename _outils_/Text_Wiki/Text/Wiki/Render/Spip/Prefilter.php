<?php
class Text_Wiki_Render_Spip_Prefilter extends Text_Wiki_Render {
    
    /**
    * Render's prefilter for Spip.
    * @access public
    * @return string The text to be rendered by following rules
    */
    function token()
    {
        return '';
    }
}
?>
