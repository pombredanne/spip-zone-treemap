<?php

class Text_Wiki_Render_Spip_Table extends Text_Wiki_Render {
    
    /**
    * Renders table token for Spip.
    * @access public
    * @param array $options The "options" portion of the token (second element)
    * @return string The text rendered from the token options.
    */

/* Non traités:
 * $options['type'] == 'caption_start' ou 'caption_end' (mediawiki)
 * disponible: $options['order'] , numero de cellule
 * $options non traitees, dépend du parser, seul mediawiki les a tous :
     * 'level' => the table nesting level (starting zero) ('table_start')
     * 'rows' => the number of rows in the table ('table_start')
     * 'cols' => the number of columns in the table or rows
     *           ('table_start' and 'row_start')
     * 'span' => column span ('cell_start')
     * 'row_span' => row span ('cell_start')
     * 'attr' => header optional attribute flag ('row_start' or 'cell_start')
     * 'format' => table, row or cell optional styling ('xxx_start')
*/
    
    function token($options)
    {
        static $last = '';

        // make nice variable names (type, attr, span)
        $pad = '    ';
        $output = '';
        
        switch ($options['type']) {
        
        case 'table_start':
        case 'row_end':
        case 'cell_start':
        default:
            break;

        case 'row_start':
            $output = "\n|";
            break;

        case 'table_end':
            $output = "\n";
            break;
        
        case 'cell_end':
            $output = '|';
            break;
        }
        $last = $options['type'];
        return $output;
    }
}
?>
