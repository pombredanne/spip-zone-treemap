<?php

class Text_Wiki_Render_Spip_Heading extends Text_Wiki_Render {
    
    /**
    * Renders heading token for Spip.
    * @access public
    * @param array $options The "options" portion of the token (second element)
    * @return string The text rendered from the token options.
    */

    function token($options)
    {
        return $options['type'] == 'start' ? "\n{{{" :
            ($options['type'] == 'end' ?  "}}}\n" : 
            ''); // $options['level'] available for spip extended sub headings
    }
}
?>
