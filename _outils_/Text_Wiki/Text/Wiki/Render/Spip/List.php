<?php

class Text_Wiki_Render_Spip_List extends Text_Wiki_Render {
    
    /**
    * Renders list token for Spip.
    * @access public
    * @param array $options The "options" portion of the token (second element)
    * @return string The text rendered from the token options.
    */

/*
  Options éventuellement disponibles dépendant du wiki, peu represemtable dans spip:

     * 'type' =>
     *     'bullet_list_start' : the start of a bullet list
     *     'bullet_list_end'   : the end of a bullet list
     *     'number_list_start' : the start of a number list
     *     'number_list_end'   : the end of a number list
     *     'item_start'   : the start of item text (bullet or number)
     *     'item_end'     : the end of item text (bullet or number)
     *     'unknown'      : unknown type of list or item
     *
     * 'level' => the indent level (0 for the first level, 1 for the
     * second, etc)
     *
     * 'count' => the list item number at this level. not needed for
     * xhtml, but very useful for PDF and RTF.
     *
     * 'format' => the optional enumerating type : A, a, I, i, or 1 (default)
     *             as HTML <ol> tag's type attribute (only for number_... type)
     *
     * 'key' => the optional starting number/letter (not for items)
*/    
    function token($options)
    {
        static $current = "\n";
        $return = '';
        
        switch ($options['type']) {
        
        case 'bullet_item_start':
        case 'number_item_start':
            $return = $current;
            break;

        case 'bullet_list_start':
        	if ($options['level'] < 2) {
        		$current .= '*';
        	} else {
        		$current .= $current[1];
        	}
        	break;

        case 'number_list_start':
        	if ($options['level'] < 2) {
        		$current .= '#';
        	} else {
        		$current .= $current[1];
        	}
        	break;

        case 'bullet_list_end':
        case 'number_list_end':
    		$current = substr($current, 0, -1);
        	break;

        case 'bullet_item_end':
        case 'number_item_end':
        default:
            break;
        }
        return $return;
    }
}
?>
