<?php

class Text_Wiki_Render_Spip_Bold extends Text_Wiki_Render {
    
    /**
    * Renders bold token for Spip.
    * @access public
    * @param array $options The "options" portion of the token (second element)
    * @return string The text rendered from the token options.
    */

    function token($options)
    {
        return $options['type'] == 'start' ? '{{' : '}}';
    }
}
