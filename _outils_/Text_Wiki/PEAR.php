<?php
// PEAR compat classes just for the errors

/**
 * PEAR minimal compat Class
 */
class PEAR {
	function isError($data)
	{
        if (is_a($data, 'c_Error')) {
            return true;
        }
        return false;
	}
	function &raiseError($message = null)
	{
		$a = &new c_Error($message);
		return $a;
	}

// une copie directe de PEAR

    // {{{ throwError()

    /**
     * Simpler form of raiseError with fewer options.  In most cases
     * message, code and userinfo are enough.
     *
     * @param string $message
     *
     */
    function &throwError($message = null,
                         $code = null,
                         $userinfo = null)
    {
        if (isset($this) && is_a($this, 'PEAR')) {
            $a = &$this->raiseError($message, $code, null, null, $userinfo);
            return $a;
        } else {
            $a = &PEAR::raiseError($message, $code, null, null, $userinfo);
            return $a;
        }
    }

    // }}}
}

class c_Error {
	var $message;
	function c_Error($message) {
		$this->message = $message;
	}
}
