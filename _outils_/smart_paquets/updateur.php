<?php

// l'url du repo en premier argument
if (!isset($argv[1]) OR !($user = $argv[1])){
	echo "Vous devez indiquer en premier argument repertoire du repository\n";
	die();
}

define('_TRACE',true);
define('_SLEEP_BETWEEN',200000);

define('_DIR_REPOSITORY',$argv[1]);

chdir(_DIR_REPOSITORY);
$k = 2;
$dirs = array();
$op = "";
while (isset($argv[$k]) AND $subdir=$argv[$k]){
	if (!in_array($subdir,array('A','U','D'))) {
		$subdir = rtrim($subdir,'/');
		while ($subdir 
		  AND $subdir!==dirname($subdir)
		  AND !in_array($subdir,$dirs)
		  ) {
			$dirs[] = $subdir;
			// si c'est deja un dir, on fait un up non recursif
			if (is_dir($subdir) AND $op!=='D')
				$out = exec_trace("svn up -N $subdir");
			// sinon, si le fichier/dossier n'existe pas mais son parent oui
			// on up
			elseif(($op=='D' OR !file_exists($subdir)) AND is_dir(dirname($subdir))) {
				$op = "";
				$out = exec_trace("svn up $subdir");
			}
			// sinon il sera up en meme temps que son parent

			if (strpos($out,'cleanup')!==FALSE) {
				$headers = "From: updateur@rezo.net\r\n";
				mail("cedric@yterium.com","updateur",$out,$headers);
			}

			$subdir = dirname($subdir);
		}

		if ($subdir  AND !in_array($subdir,$dirs)) {
			$out = exec_trace("svn up -N $subdir");
			$dirs[] = $subdir;
		}
	}
	else $op = $subdir;

	$k++;
}
$dirs = implode("\n",$dirs)."\n";
file_put_contents("dirs-changed.txt",$dirs,FILE_APPEND);

/**
 * Echo avec un horodatage
 */
function echo_trace($out){
	$outh = date("[Y-m-d H:i:s] ",time()).$out."\n";
	if (_TRACE) echo $outh;

	if (strncmp($out,"Erreur :",8)==0)
		$GLOBALS['erreurs'][]=$outh;

	return $outh;
}

/**
 * exec avec une trace de la commande et de son resultat
 */
function exec_trace($commande){
	$output = "";
	$return_var="";
	echo_trace($commande);
	$commande = "$commande 2>&1";
	$out = exec($commande,$output,$return_var);
	if ($output!==null) {
		$out = end($output);
	}
	if (strlen(trim($out)))
		array_map('echo_trace',$output);
	return $out;
}


?>
