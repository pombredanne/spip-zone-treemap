<?php

// ----------------------------------------- PARAMETRAGE -------------------------------------------

// Test des arguments obligatoires
if (!isset($argv[1]) OR !$argv[1]){
	echo "Argument 1 - OBLIGATOIRE : url du repository svn\n";
	echo "Argument 2 : repertoire relatif a ./ dans lequel sera fait le checkout\n";
	echo "Argument 3 : repertoire de depot des paquets crees (defaut: paquets)\n";
	echo "Argument 4 : nom du fichier listant les archives a creer (defaut: archivelist.txt)\n";
	echo "Argument 5 : nom sans extension du fichier xml recapitulant toutes les archives (defaut: archives)\n";
	echo "Argument 6 : nom du fichier xml contenant les infos des plugins (defaut: plugin.xml)\n";
	echo "Argument 7 : gestionnaire de versions motorisant le repository concerne (defaut: svn)\n";
	echo "Argument 8 : email du webmestre destinataire\n";
	echo "Argument 9 : email de l'emetteur\n";
	die();
}

// --------------------------------------- CONFIGURATION -------------------------------------------

date_default_timezone_set('Europe/Paris');

define('_TRACE',true);
define('_SLEEP_BETWEEN',200000);
// on force une mise a jour de tous les paquets une fois pas jour,
// entre minuit et 1H
#define('_FORCE_UPDATE',date('H')<1);
define('_FORCE_UPDATE',false);
// Pour inclure les fichiers de langue
define('_ECRIRE_INC_VERSION', 1);

include('inc_empaqueteur.php');

// ----------------------------------- SESSION D'EMPAQUETAGE ---------------------------------------

// Appel de la fonction principale d'empaquetage avec les parametres idoines
// -> les valeurs en dur devront devenir des parametres des scripts a terme
empaqueteur($argv[1],									// $url
	   isset($argv[2]) ? $argv[2] : '', 				// $dir_repo
	   isset($argv[3]) ? $argv[3] : 'paquets',			// $dir_paq
	   isset($argv[4]) ? $argv[4] : 'archivelist.txt',	// $src
	   isset($argv[5]) ? $argv[5] : 'archives',			// $dest
	   isset($argv[6]) ? $argv[6] : 'paquet',			// $dtd_prio
	   isset($argv[7]) ? $argv[7] : 'svn',				// $nom_vcs
	   isset($argv[8]) ? $argv[8] : '',					// $mail_to
	   isset($argv[9]) ? $argv[9] : '');				// $mail_from
?>
