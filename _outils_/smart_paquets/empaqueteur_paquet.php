<?php

// Construit le contenu multi des balises nom, slogan et description a partir des items de langue
// contenus dans les fichiers paquet-prefixe_langue.php
function empaqueteur_paquet_multi($dir_source) {
	$multis = '';

	if ($fichier_fr = glob($dir_source . '/lang/paquet-*_fr.php')) {
		// Determination du nom du module, du prefixe et des items de langue
		$nom_fr = basename($fichier_fr[0], '.php');
		$prefixe = substr($nom_fr, 7, strlen($nom_fr)-3-7);
		$module = "paquet-$prefixe";
		$item_nom = $prefixe . "_nom";
		$item_slogan = $prefixe . "_slogan";
		$item_description = $prefixe . "_description";

		// On cherche tous les fichiers de langue destines a la traduction du paquet.xml
		if ($fichiers_langue = glob($dir_source . "/lang/{$module}_*.php")) {
			$nom = $slogan = $description = '';
			foreach ($fichiers_langue as $_fichier_langue) {
				$nom_fichier = basename($_fichier_langue, '.php');
				$langue = substr($nom_fichier, strlen($module) + 1 - strlen($nom_fichier));
				// Si la langue est reconnue, on traite la liste des items de langue
				if (isset($GLOBALS['codes_langues'][$langue])) {
					$GLOBALS['idx_lang'] = $langue;
 					include($_fichier_langue);
					foreach ($GLOBALS[$langue] as $_item => $_traduction) {
						if ($_item == $item_nom)
							$nom .= "\n[$langue]$_traduction"; 
						if ($_item == $item_slogan)
							$slogan .= "\n[$langue]$_traduction"; 
						if ($_item == $item_description)
							$description .= "\n[$langue]$_traduction"; 
					}
				}
			}

			// Finaliser la construction des balises multi
			if ($nom) $multis .= "<nom>\n<multi>$nom\n</multi>\n</nom>\n";
			if ($slogan) $multis .= "<slogan>\n<multi>$slogan\n</multi>\n</slogan>\n";
			if ($description) $multis .= "<description>\n<multi>$description\n</multi>\n</description>\n";
		}
	}

	return ($multis ? "<multis>\n$multis</multis>" : '');
}


// Renvoie le path complet du logo a partir de la balise icon de plugin.xml et de la racine des sources
function empaqueteur_paquet_logo($xml, $dir_source) {
  return !preg_match('#logo\s*=\s*[\'"](.+)[\'"]#i', $xml, $matches) ? '' : ($dir_source . '/' . trim($matches[1]));
}

?>
