<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

// Ce script suppose que le repertoire outils/validerPlugins qui le contient
// est installe a la racine d'une distribution de SPIP
// ainsi que le repertoire plugins.
// Il applique le validateur XML de SPIP sur tous ses fichiers plugin.xml,
// sur la base de la DTD plugin.xml ici fournie
// mais dont les lacunes sont inevitables faute de specifications precises.
// Mais ca permet un premier diagnostic

$dir = $argv[1] ? $argv[1] : 'plugins/';
$nom = $argv[2] ? $argv[2] : 'plugin.xml';
$dtd = $argv[3] ? $argv[3] : 'plugin.dtd';
$write = $argv[4];
$pdtd = 'prive/' . $dtd;

chdir('../../');
// Installer les dtd dans spip, mais les retirer ensuite
if (!file_exists($pdtd)) {
  copy('plugins/plugonet/' . $dtd, $pdtd);
  $dtd = false;
}
if (!file_exists($pdtd2='prive/paquet.dtd')) {
  copy('plugins/plugonet/' . 'paquet.dtd', $pdtd2);
  $dtd2 = false;
} else $dtd2 = true;

require 'ecrire/inc_version.php';
@unlink(_DIR_TMP."plugin_xml_cache.gz");
// pour _RACCOURCI_LIEN _EXTRAIRE_MULTI et extraire_trads
require_once 'ecrire/inc/filtres.php'; 
require_once 'ecrire/plugins/get_infos.php';
require_once 'plugins/langonet/inc/langonet_generer_fichier.php'; 
require_once 'plugins/plugonet/inc/plugonet_generer.php'; 

$files = preg_files($dir,  '/' . $nom . '$');
if (!$files) {
  echo "Aucun fichier ne correspond\n ";
} else {
  echo date('Y-m-d H:i:s'), " ", ($n = count($files)), " fichiers\n";
  list($msg, $erreurs, $res) = inc_plugonet_generer($files, $write);
  $res = join("\n", $res) . "\n";
  if ($n == 1) echo $res; else ecrire_fichier('archives.xml', $res);
  echo date('Y-m-d H:i:s'), $msg, join("\n", $erreurs);
}
if (!$dtd) unlink($pdtd);
if (!$dtd2) unlink('prive/paquet.dtd');
?>
