<?php
	## un script a executer en ligne de commande, pour reindexer totalement
	## un site ; ca peut tout faire exploser, vous etes prevenus !
	
	## mettre le script a la racine, et lancer la commande :
	## php cli_reindexer.php

	## TODO : securiser pour forcer l'usage en ligne de commande
	if($_SERVER['DOCUMENT_ROOT'])
		die("script a utiliser en ligne de commande exclusivement");

	include('ecrire/inc_version.php3');
	include_ecrire('inc_connect');
	include_ecrire('inc_index');
	while (effectuer_une_indexation(10))
		echo ".";


?>
