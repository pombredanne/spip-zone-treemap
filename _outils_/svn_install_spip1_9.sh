#! /bin/sh
# Un petit script pour automatiser l'installation de SPIP 2 (branche stable)
# et de quelques plugins d'utilite
# le tout en une seule ligne de commande :
# ./svn_install_spip1_9.sh
# 
# Script original de Cedric MORIN | cedric {CHEZ} yterium {POINT} net
# Revu et corrige pour integrer les recentes modifs d'arborescence de spip-zone
# le 2 Juillet 2009
# par Loiseau2nuit | l.oiseau2nuit {CHEZ} gmail {POINT} com
#
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!! livre tel quel sans aucune garantie du gouvernement                          !!!
# !!! l'auteur decline toute responsabilite en cas de degat sur un serveur de prod !!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

mkdir spip-1.9
# INSTALLATION DE SPIP 
echo "Installation de SPIP branche 1.9.x en cours. Merci de patienter..."
svn checkout svn://trac.rezo.net/spip/branches/spip-1.9 spip-1.9
cd spip-1.9

# ON ATTRIBUE LES DROITS EN ECRITURE SUR LES DOSSIERS
echo "Creation des dossiers additionnels et attribution des droits en ecriture"
chmod 755 CACHE
chmod 755 IMG
chmod 755 ecrire
chmod 755 ecrire/data
echo "Operation effectuee avec succes"


# ON CONSTRUIT LES REPERTOIRES ADDITIONNELS USUELS 
mkdir plugins
chmod 755 plugins

# INSTALLATION DES PLUGINS
echo "Installation des plugins en cours, merci de patienter..."
cd plugins

# On construit au prealable un dossier par type de fonctionalites pour les ranger
# mettre les lignes "mkdir..." et "cd ..." en commentaires (rajouter # devant) pour avoir tous vos plugins
# à la racine du dossier /plugins

# LISTE ADMINISTRATION
mkdir admin
cd admin
svn checkout svn://zone.spip.org/spip-zone/_plugins_/desactiver_cache desactiver_cache
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/console console
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/desactiver_flash desactiver_flash
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/-jQuery ./-jQuery
sleep 2;
cd ..

# LISTE FONCTIONALITES
mkdir fonctionnalites
cd fonctionnalites
svn checkout svn://zone.spip.org/spip-zone/_plugins_/acces_restreint acces_restreint
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/acronymes acronymes
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/agenda agenda
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/widget_calendar widget_calendar
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/barre_typo_ecologique barre_typo_ecologique
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/comarquage comarquage
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/csv_import csv_import
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/dewplayer dewplayer
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/Forms forms
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/ecard ecard
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/gestion_documents gestion_documents
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/phpmyvisites phpmyvisites
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/recherche_etendue recherche_etendue
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/revision_nbsp revision_nbsp
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/sitemap sitemap
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/orientation orientation
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/spip_listes spip-listes
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/_squelettes_/pagination_ahah pagination_ahah
sleep 2;
cd ..

# LISTE OPTIONS
mkdir options
cd options
svn checkout svn://zone.spip.org/spip-zone/_plugins_/messagerie messagerie
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/pim_agenda pim_agenda
sleep 2;
svn checkout svn://zone.spip.org/spip-zone/_plugins_/timeline timeline
sleep 2;
cd ..

echo "Les plugins ont ete installes correctement. Merci de patienter..."

cd ..

cp inc-public.php3 inc-public.php
cd ..

# La ligne suivante servirait a "bloquer" la version de SPIP dans l'etat de son deploiement
# c.a.d : retire tous les /.svn places en racine des sous-dossiers de SPIP
# et bloque ainsi les possibilite de svn-update
# Decommenter si vous voulez "figer" le SPIP ainsi installe
#
# svn export --force http://xxx/xxx/spip/installation/install_base $1

echo "Votre Installation de SPIP branche 1.9.x est desormais fonctionnelle. Vous pouvez maintenant vous rendre sur http://votre_site_spip/ecrire afin de configurer votre site SPIP"
