#!/bin/bash
WORKDIR=/home/Desktop/spip

echo "commencons par svn update"
cd $WORKDIR/spip
svn update >$WORKDIR/svn_update.log
cat $WORKDIR/svn_update.log
REVISION=`cat $WORKDIR/svn_update.log | grep vision | cut -d" " -f4 | sed -e "s/\.//"`
echo "nous en sommes a la revision : $REVISION "

if [ -d ${WORKDIR}/svn/spip_${REVISION} ]
then
  echo "le repertoire ${WORKDIR}/svn/spip_${REVISION} existe deja "
else
  echo "copie spip vers spip_$REVISION "
  cp -r $WORKDIR/spip $WORKDIR/svn/spip_$REVISION
  echo "on vire tous les svn"
  find ${WORKDIR}/svn/spip_${REVISION} -name "*svn" | xargs rm -rf
  find ${WORKDIR}/svn/spip_${REVISION} -name ".*svn" | xargs rm -rf
  echo "un petit tar ... puis gz"
  tar -cvf ${WORKDIR}/svn/spip_${REVISION}.tar ${WORKDIR}/svn/spip_${REVISION}>${WORKDIR}/tar.log
  gzip ${WORKDIR}/svn/spip_$REVISION.tar
  echo " ${WORKDIR}/svn/spip_$REVISION.tar.gz dispo "

  echo "une version fr_en pour la place"
  echo "copie spip vers spipi_fr_en_ar_$REVISION "
  cp -r $WORKDIR/spip $WORKDIR/svn/spip_fr_en_ar_$REVISION
  echo "on vire tous les svn"
  find ${WORKDIR}/svn/spip_fr_en_ar_${REVISION} -name "*svn" | xargs rm -rf
  find ${WORKDIR}/svn/spip_fr_en_ar_${REVISION} -name ".*svn" | xargs rm -rf
  for i in `ls ${WORKDIR}/svn/spip_fr_en_ar_${REVISION}/ecrire/lang  | grep -v "_fr.php3" | grep -v "_en.php3" | grep -v "_ar.php3" `
  do
  rm ${WORKDIR}/svn/spip_fr_en_ar_${REVISION}/ecrire/lang/$i
  done
  echo "un petit tar ... puis gz"
  tar -cvf ${WORKDIR}/svn/spip_fr_en_ar_${REVISION}.tar ${WORKDIR}/svn/spip_fr_en_ar_${REVISION}>${WORKDIR}/tar.log
  gzip ${WORKDIR}/svn/spip_fr_en_ar_$REVISION.tar
  echo " ${WORKDIR}/svn/spip_fr_en_ar_$REVISION.tar.gz dispo "
fi
