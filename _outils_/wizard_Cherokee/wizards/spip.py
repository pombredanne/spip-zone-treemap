# -*- coding: utf-8 -*-
#
# Cherokee-admin's SPIP wizard
#
# Authors:
#      Alvaro Lopez Ortega <alvaro@alobbs.com>
#      Taher Shihadeh <taher@octality.com>
#
# Copyright (C) 2010 Alvaro Lopez Ortega
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 2 of the GNU General Public
# License as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#

#
# Tested:
# 2010/04/12: SPIP 2.9.2 Cherokee 0.99.41
#

import os
import re
import CTK
import Wizard
import validations
from util import *

NOTE_WELCOME_H1 = N_("Welcome to the SPIP wizard")
NOTE_WELCOME_P1 = N_('<a target="_blank" href="http://www.spip.net/en">SPIP</a> is an award-winning content management system (CMS), which enables you to build Web sites and powerful online applications.')
NOTE_WELCOME_P2 = N_('More simply, SPIP is what you use when you want to work with your blogging software, not fight it.')
NOTE_LOCAL_H1   = N_("Application Source Code")
NOTE_LOCAL_DIR  = N_("Local directory where the SPIP source code is located. Example: /var/www/spip.")
NOTE_HOST_H1    = N_("New Virtual Server Details")
NOTE_HOST       = N_("Host name of the virtual server that is about to be created.")
NOTE_WEBDIR     = N_("Web directory where you want SPIP to be accessible. (Example: /blog)")
NOTE_WEBDIR_H1  = N_("Public Web Directory")
ERROR_NO_SRC    = N_("Does not look like a SPIP source directory.")

PREFIX    = 'tmp!wizard!spip'
URL_APPLY = r'/wizard/vserver/spip/apply'

CONFIG_DIR = """

# Forbidden access to /tmp and /config

%(pre_rule_plus2)s!match = or
%(pre_rule_plus2)s!match!left = directory
%(pre_rule_plus2)s!match!left!directory = %(web_dir)s/tmp
%(pre_rule_plus2)s!match!right = directory
%(pre_rule_plus2)s!match!right!directory = %(web_dir)s/config
%(pre_rule_plus2)s!handler = custom_error
%(pre_rule_plus2)s!handler!error = 403

# Forbidden access to .svn

%(pre_rule_plus1)s!match = request
%(pre_rule_plus1)s!match!request = %(web_dir)s/(.*/)?\.svn
%(pre_rule_plus1)s!handler = custom_error
%(pre_rule_plus1)s!handler!error = 403

# The PHP rule comes here

%(pre_rule_minus1)s!match = and
%(pre_rule_minus1)s!match!final = 1
%(pre_rule_minus1)s!match!left = directory
%(pre_rule_minus1)s!match!left!directory = %(web_dir)s
%(pre_rule_minus1)s!match!right = exists
%(pre_rule_minus1)s!match!right!iocache = 1
%(pre_rule_minus1)s!match!right!match_any = 1
%(pre_rule_minus1)s!match!right!match_index_files = 0
%(pre_rule_minus1)s!match!right!match_only_files = 1
%(pre_rule_minus1)s!handler = file
%(pre_rule_minus1)s!handler!iocache = 1

# URLs "propres", "propres2", "libres", "arbo"

%(pre_rule_minus10)s!match = request
%(pre_rule_minus10)s!match!request = %(web_dir)s/[^\.]+(\.html)?$
%(pre_rule_minus10)s!handler = redir
%(pre_rule_minus10)s!handler!rewrite!1!regex = %(web_dir)s/[^\.]+(\.html)?$
%(pre_rule_minus10)s!handler!rewrite!1!show = 0
%(pre_rule_minus10)s!handler!rewrite!1!substring = %(web_dir)s/spip.php?url_propre=$0

# Compatibility with "html" URLs for a nice transition

%(pre_rule_minus7)s!match = request
%(pre_rule_minus7)s!match!request = %(web_dir)s/(rubrique|article|breve|auteur|mot|site)([0-9]+)(\.html)?$
%(pre_rule_minus7)s!handler = redir
%(pre_rule_minus7)s!handler!rewrite!1!show = 0
%(pre_rule_minus7)s!handler!rewrite!1!regex = %(web_dir)s/(rubrique|article|breve|auteur|mot|site)([0-9]+)(\.html)?$
%(pre_rule_minus7)s!handler!rewrite!1!substring = %(web_dir)s/spip.php?page=$1&id_$1=$2

# ping http://site/1234 => article1234

%(pre_rule_minus9)s!match = request
%(pre_rule_minus9)s!match!request = %(web_dir)s/([1-9][0-9]*)$
%(pre_rule_minus9)s!handler = redir
%(pre_rule_minus9)s!handler!rewrite!1!show = 0
%(pre_rule_minus9)s!handler!rewrite!1!regex = %(web_dir)s/([1-9][0-9]*)$
%(pre_rule_minus9)s!handler!rewrite!1!substring = %(web_dir)s/spip.php?action=redirect&type=article&id=$1

# Standards files (if not presents)

%(pre_rule_minus11)s!match = request
%(pre_rule_minus11)s!match!request = %(web_dir)s/(robots\.txt|favicon\.ico|sitemap\.xml)?$
%(pre_rule_minus11)s!handler = redir
%(pre_rule_minus11)s!handler!rewrite!1!show = 0
%(pre_rule_minus11)s!handler!rewrite!1!regex = %(web_dir)s/(robots\.txt|favicon\.ico|sitemap\.xml)?$
%(pre_rule_minus11)s!handler!rewrite!1!substring = %(web_dir)s/spip.php?page=$1

"""

CONFIG_VSERVER = """
%(pre_vsrv)s!document_root = %(local_dir)s
%(pre_vsrv)s!nick = %(host)s
%(pre_vsrv)s!directory_index = index.html,index.php

# Forbidden access to /tmp and /config

%(pre_rule_plus2)s!match = or
%(pre_rule_plus2)s!match!left = directory
%(pre_rule_plus2)s!match!left!directory = /tmp
%(pre_rule_plus2)s!match!right = directory
%(pre_rule_plus2)s!match!right!directory = /config
%(pre_rule_plus2)s!handler = custom_error
%(pre_rule_plus2)s!handler!error = 403

# Forbidden access to .svn

%(pre_rule_plus1)s!match = request
%(pre_rule_plus1)s!match!request = /(.*/)?\.svn
%(pre_rule_plus1)s!handler = custom_error
%(pre_rule_plus1)s!handler!error = 403

# The PHP rule comes here

# If a static file exists, return it

%(pre_rule_minus1)s!match = exists
%(pre_rule_minus1)s!match!iocache = 1
%(pre_rule_minus1)s!match!match_any = 1
%(pre_rule_minus1)s!match!match_index_files = 0
%(pre_rule_minus1)s!match!match_only_files = 1
%(pre_rule_minus1)s!handler = file

# List of the content of the template directory

%(pre_rule_minus2)s!match = directory
%(pre_rule_minus2)s!match!directory = /squelettes-dist
%(pre_rule_minus2)s!handler = common

%(pre_rule_minus3)s!match = directory
%(pre_rule_minus3)s!match!directory = /squelettes
%(pre_rule_minus3)s!handler = common

# Static files used in the Cherokee themes

%(pre_rule_minus4)s!match = directory
%(pre_rule_minus4)s!match!directory = /cherokee_themes
%(pre_rule_minus4)s!document_root = /usr/share/cherokee/themes
%(pre_rule_minus4)s!handler = file

%(pre_rule_minus4)s!match = directory
%(pre_rule_minus4)s!match!directory = /icons
%(pre_rule_minus4)s!document_root = /usr/share/cherokee/icons
%(pre_rule_minus4)s!handler = file

# Apache-like redirection, for the document root

%(pre_rule_minus5)s!match = fullpath
%(pre_rule_minus5)s!match!fullpath!1 = /
%(pre_rule_minus5)s!handler = redir
%(pre_rule_minus5)s!handler!rewrite!1!regex = ^/$
%(pre_rule_minus5)s!handler!rewrite!1!substring = spip.php
%(pre_rule_minus5)s!handler!rewrite!1!show = 0
%(pre_rule_minus5)s!handler!rewrite!2!regex = ^/\?(.*)$
%(pre_rule_minus5)s!handler!rewrite!2!substring = spip.php?$1
%(pre_rule_minus5)s!handler!rewrite!2!show = 0
%(pre_rule_minus5)s!encoder!deflate = allow
%(pre_rule_minus5)s!encoder!gzip = allow

# Apache-like redirection, for the admin directory

%(pre_rule_minus6)s!match = request
%(pre_rule_minus6)s!match!request = ^/ecrire
%(pre_rule_minus6)s!handler = redir
%(pre_rule_minus6)s!handler!rewrite!3!regex = ^/ecrire/\?(.*)$
%(pre_rule_minus6)s!handler!rewrite!3!substring = /ecrire/index.php?$1
%(pre_rule_minus6)s!handler!rewrite!3!show = 0
%(pre_rule_minus6)s!handler!rewrite!4!regex = ^/ecrire/$
%(pre_rule_minus6)s!handler!rewrite!4!substring = /ecrire/index.php
%(pre_rule_minus6)s!handler!rewrite!4!show = 0
%(pre_rule_minus6)s!handler!rewrite!5!regex = ^/ecrire$
%(pre_rule_minus6)s!handler!rewrite!5!substring = /ecrire/index.php
%(pre_rule_minus6)s!handler!rewrite!5!show = 0

# Compatibility with "html" URLs for a nice transition  

%(pre_rule_minus7)s!match = request
%(pre_rule_minus7)s!match!request = /(rubrique|article|breve|auteur|mot|site)(\d+)(\.html)?$
%(pre_rule_minus7)s!handler = redir
%(pre_rule_minus7)s!handler!rewrite!1!regex = /(rubrique|article|breve|auteur|mot|site)(\d+)(\.html)?\?(.*)$
%(pre_rule_minus7)s!handler!rewrite!1!substring = /spip.php?page=$1&id_$1=$2&$4
%(pre_rule_minus7)s!handler!rewrite!1!show = 0
%(pre_rule_minus7)s!handler!rewrite!2!regex = /(rubrique|article|breve|auteur|mot|site)(\d+)(\.html)?$
%(pre_rule_minus7)s!handler!rewrite!2!substring = /spip.php?page=$1&id_$1=$2
%(pre_rule_minus7)s!handler!rewrite!2!show = 0
%(pre_rule_minus7)s!encoder!deflate = allow
%(pre_rule_minus7)s!encoder!gzip = allow

# ping http://site/1234 => article1234

%(pre_rule_minus9)s!match = request
%(pre_rule_minus9)s!match!request = /(\d+)$
%(pre_rule_minus9)s!handler = redir
%(pre_rule_minus9)s!handler!rewrite!1!show = 0
%(pre_rule_minus9)s!handler!rewrite!1!regex = /(\d+)$
%(pre_rule_minus9)s!handler!rewrite!1!substring = /spip.php?action=redirect&id=$1&type=article
%(pre_rule_minus9)s!encoder!deflate = allow
%(pre_rule_minus9)s!encoder!gzip = allow

# URLs "propres", "propres2", "libres", "arbo"

%(pre_rule_minus10)s!match = request
%(pre_rule_minus10)s!match!request = /[^\.]+(\.html)?$
%(pre_rule_minus10)s!handler = redir
%(pre_rule_minus10)s!handler!rewrite!1!regex = /([^\.]+)(\.html)?\?(.*)$
%(pre_rule_minus10)s!handler!rewrite!1!substring = /spip.php?url_propre=$1&$2
%(pre_rule_minus10)s!handler!rewrite!1!show = 0
%(pre_rule_minus10)s!handler!rewrite!2!regex = /([^\.]+)(\.html)?$
%(pre_rule_minus10)s!handler!rewrite!2!substring = /spip.php?url_propre=$1
%(pre_rule_minus10)s!handler!rewrite!2!show = 0
%(pre_rule_minus10)s!encoder!deflate = allow
%(pre_rule_minus10)s!encoder!gzip = allow

# Standards files (if not presents)

%(pre_rule_minus11)s!match = request
%(pre_rule_minus11)s!match!request = /(robots\.txt|favicon\.ico|sitemap\.xml)?$
%(pre_rule_minus11)s!handler = redir
%(pre_rule_minus11)s!handler!rewrite!1!show = 0
%(pre_rule_minus11)s!handler!rewrite!1!regex = /(robots\.txt|favicon\.ico|sitemap\.xml)?$
%(pre_rule_minus11)s!handler!rewrite!1!substring = /spip.php?page=$1

"""

SRC_PATHS = [
    "/usr/share/spip",          # Debian, Fedora
    "/var/www/*/htdocs/spip",   # Gentoo
    "/srv/www/htdocs/spip",     # SuSE
    "/usr/local/www/data/spip", # BSD
    "/opt/local/www/spip"       # MacPorts
]


class Commit:
    def Commit_VServer (self):
        # Create the new Virtual Server
        next = CTK.cfg.get_next_entry_prefix('vserver')
        CTK.cfg['%s!nick'%(next)] = CTK.cfg.get_val('%s!host'%(PREFIX))
        Wizard.CloneLogsCfg_Apply ('%s!logs_as_vsrv'%(PREFIX), next)

        # PHP
        php = CTK.load_module ('php', 'wizards')

        error = php.wizard_php_add (next)
        if error:
            return {'ret': 'error', 'errors': {'msg': error}}

        php_info = php.get_info (next)

        # SPIP
        props = cfg_get_surrounding_repls ('pre_rule', php_info['rule'])
        props['pre_vsrv']  = next
        props['host']      = CTK.cfg.get_val('%s!host'      %(PREFIX))
        props['local_dir'] = CTK.cfg.get_val('%s!local_dir' %(PREFIX))

        config = CONFIG_VSERVER %(props)
        CTK.cfg.apply_chunk (config)
        Wizard.AddUsualStaticFiles(props['pre_rule_minus1'])

        # Clean up
        CTK.cfg.normalize ('%s!rule'%(next))
        CTK.cfg.normalize ('vserver')

        del (CTK.cfg[PREFIX])
        return CTK.cfg_reply_ajax_ok()


    def Commit_Rule (self):
        vsrv_num = CTK.cfg.get_val ('%s!vsrv_num'%(PREFIX))
        next = 'vserver!%s' %(vsrv_num)

        # PHP
        php = CTK.load_module ('php', 'wizards')

        error = php.wizard_php_add (next)
        if error:
            return {'ret': 'error', 'errors': {'msg': error}}

        php_info = php.get_info (next)

        # SPIP
        props = cfg_get_surrounding_repls ('pre_rule', php_info['rule'])
        props['pre_vsrv']  = next
        props['web_dir']   = CTK.cfg.get_val('%s!web_dir'   %(PREFIX))
        props['local_dir'] = CTK.cfg.get_val('%s!local_dir' %(PREFIX))

        config = CONFIG_DIR %(props)
        CTK.cfg.apply_chunk (config)

        # Clean up
        CTK.cfg.normalize ('%s!rule'%(next))

        del (CTK.cfg[PREFIX])
        return CTK.cfg_reply_ajax_ok()


    def __call__ (self):
        if CTK.post.pop('final'):
            # Apply POST
            CTK.cfg_apply_post()

            # VServer or Rule?
            if CTK.cfg.get_val ('%s!vsrv_num'%(PREFIX)):
                return self.Commit_Rule()
            return self.Commit_VServer()

        return CTK.cfg_apply_post()


class WebDirectory:
    def __call__ (self):
        table = CTK.PropsTable()
        table.Add (_('Web Directory'), CTK.TextCfg ('%s!web_dir'%(PREFIX), False, {'value': '/blog', 'class': 'noauto'}), _(NOTE_WEBDIR))

        submit = CTK.Submitter (URL_APPLY)
        submit += CTK.Hidden('final', '1')
        submit += table

        cont = CTK.Container()
        cont += CTK.RawHTML ('<h2>%s</h2>' %(_(NOTE_WEBDIR_H1)))
        cont += submit
        cont += CTK.DruidButtonsPanel_PrevCreate_Auto()
        return cont.Render().toStr()


class Host:
    def __call__ (self):
        table = CTK.PropsTable()
        table.Add (_('New Host Name'),    CTK.TextCfg ('%s!host'%(PREFIX), False, {'value': 'www.example.com', 'class': 'noauto'}), _(NOTE_HOST))
        table.Add (_('Use Same Logs as'), Wizard.CloneLogsCfg('%s!logs_as_vsrv'%(PREFIX)), _(Wizard.CloneLogsCfg.NOTE))

        submit = CTK.Submitter (URL_APPLY)
        submit += CTK.Hidden('final', '1')
        submit += table

        cont = CTK.Container()
        cont += CTK.RawHTML ('<h2>%s</h2>' %(_(NOTE_HOST_H1)))
        cont += submit
        cont += CTK.DruidButtonsPanel_PrevCreate_Auto()
        return cont.Render().toStr()


class LocalSource:
    def __call__ (self):
        guessed_src = path_find_w_default (SRC_PATHS)

        table = CTK.PropsTable()
        table.Add (_('SPIP Local Directory'), CTK.TextCfg ('%s!local_dir'%(PREFIX), False, {'value': guessed_src}), _(NOTE_LOCAL_DIR))

        submit = CTK.Submitter (URL_APPLY)
        submit += table

        cont = CTK.Container()
        cont += CTK.RawHTML ('<h2>%s</h2>' %(_(NOTE_LOCAL_H1)))
        cont += submit
        cont += CTK.DruidButtonsPanel_PrevNext_Auto()
        return cont.Render().toStr()


class PHP:
    def __call__ (self):
        php = CTK.load_module ('php', 'wizards')
        return php.External_FindPHP()


class Welcome:
    def __call__ (self):
        cont = CTK.Container()
        cont += CTK.RawHTML ('<h2>%s</h2>' %(_(NOTE_WELCOME_H1)))
        cont += Wizard.Icon ('spip', {'class': 'wizard-descr'})
        box = CTK.Box ({'class': 'wizard-welcome'})
        box += CTK.RawHTML ('<p>%s</p>' %(_(NOTE_WELCOME_P1)))
        box += CTK.RawHTML ('<p>%s</p>' %(_(NOTE_WELCOME_P2)))
        box += Wizard.CookBookBox ('cookbook_spip')
        cont += box

        # Send the VServer num if it's a Rule
        tmp = re.findall (r'^/wizard/vserver/(\d+)/', CTK.request.url)
        if tmp:
            submit = CTK.Submitter (URL_APPLY)
            submit += CTK.Hidden('%s!vsrv_num'%(PREFIX), tmp[0])
            cont += submit

        cont += CTK.DruidButtonsPanel_Next_Auto()
        return cont.Render().toStr()


def is_spip_dir (path):
    path = validations.is_local_dir_exists (path)
    module_inc = os.path.join (path, 'ecrire/inc_version.php')
    try:
        validations.is_local_file_exists (module_inc)
    except:
        raise ValueError, _(ERROR_NO_SRC)
    return path


VALS = [
    ('%s!local_dir'%(PREFIX), validations.is_not_empty),
    ('%s!host'     %(PREFIX), validations.is_not_empty),
    ('%s!web_dir'  %(PREFIX), validations.is_not_empty),

    ('%s!local_dir'%(PREFIX), is_spip_dir),
    ('%s!host'     %(PREFIX), validations.is_new_vserver_nick),
    ('%s!web_dir'  %(PREFIX), validations.is_dir_formatted)
]

# VServer
CTK.publish ('^/wizard/vserver/spip$',   Welcome)
CTK.publish ('^/wizard/vserver/spip/2$', PHP)
CTK.publish ('^/wizard/vserver/spip/3$', LocalSource)
CTK.publish ('^/wizard/vserver/spip/4$', Host)

# Rule
CTK.publish ('^/wizard/vserver/(\d+)/spip$',   Welcome)
CTK.publish ('^/wizard/vserver/(\d+)/spip/2$', PHP)
CTK.publish ('^/wizard/vserver/(\d+)/spip/3$', LocalSource)
CTK.publish ('^/wizard/vserver/(\d+)/spip/4$', WebDirectory)

# Common
CTK.publish (r'^%s$'%(URL_APPLY), Commit, method="POST", validation=VALS)
