#!/bin/bash
#
# Updates svn (log: name-from-to.log) and builds : (from=previous version, to=current version
# - a bash name-from-to.delete to remove deleted files and directories
# - a name-from-to.tgz of the added/modified files (+ the root .svn/entries)
# only needed parameter is the base dir , name is the last compound of it
# optional second parameter is a revision number (as for rollback)
#
# Met à jour un svn (log: name-from-to.log) et construit :
# - un exécutable name-from-to.delete pour supprimer les fichiers et répertoires disparus
# - une archive name-from-to.tgz qui contient les fichiers modifiés ou ajoutés
#   et le .svn/entries de la racine pour le suivi de version
# l'unique paramètre obligatoire  est le répertoire de base, "name" en est la dernière composante 
# un deuxième paramètre optionnel est un numéro de révision (mettons pour un rollback)
# les fichiers produits permettent de mettre à jour une installation sans svn ni rsync
#
# Example, spip étant localement en 5977 et le svn version 6258
# ./svnup2tgz spip
# - la copie locale est mise à jour en 6258, voir le log dans spip-5977-6258.log,
# - un fichier spip-5977-6258.delete est produit qui supprime les fichiers disparus, à exécuter
#   sur le site de production par ./spip-5977-6258.delete (depuis le répertoire parent de spip)
# - une archive spip-5977-6258.tgz est fabriquée avec tous fichiers modifiés ou nouveaux
#   à extraire sur le site de production (du répertoire parent de spip)
#
if [ $# = 0 ]
    then
        echo
        echo "usage: $0 svn_dir_to_update [revision_number]"
        echo
        exit 1
fi
NAM=${1%/}
NAM=${NAM##*/}

VER=$(($2))
if [ $VER -gt 0 ]
then
	VER="-r $VER"
else
	VER=''
fi

OLD=`svnversion -n $1`
if [ -z "${OLD}" ] ; then
    echo
    echo "no $1 svn local repository available, svn checkout $1 ?"
    echo
    exit 2
fi
echo "Current version: ${OLD}"
svn update $VER $1 > ${NAM}-${OLD}-up.log
NEW=`svnversion -n $1`
if [ $NEW -eq $OLD ]; then
    echo "NO CHANGE since ${OLD}"
    rm ${NAM}-${OLD}-up.log
    exit 0
fi

echo "New version: ${NEW}"
BAS="${NAM}-${OLD}-${NEW}"
mv ${NAM}-${OLD}-up.log ${BAS}.log

# deleted files or dirs?
RM=$(sed -n '/^[D]\s\s*\(.*\)$/s//\1/p' ${BAS}.log)
if [ -n "${RM}" ] ; then
    echo '#!/bin/bash' > ${BAS}.delete
    echo "# deleted from ${OLD} to ${NEW}" >> ${BAS}.delete
    for fil in ${RM}; do
        echo "rm -Rv ${fil}" >> ${BAS}.delete
        echo "${fil} removed"
    done
    chmod a+x ${BAS}.delete
fi

# added/changed files ?
for fil in  $(sed -n '/^[AU]\s\s*\(.*\)$/s//\1/p' ${BAS}.log); do
    if [ ! -d "$fil" ] ; then
        TGZ="${TGZ} ${fil}" ;
    fi
done	# fil
if [ -n "${TGZ}" ] ; then
    tar czvf ${BAS}.tgz $1/.svn/entries ${TGZ}
    echo "You will have to tar xzvf ${BAS}.tgz from root dir on the site"
fi
if [ -n "${RM}" ] ; then
    echo "After you have run ./${BAS}.delete from the same root"
fi

# conflict or merged files ?
CG=$(sed -n '/^[CG]\s\s*\(.*\)$/s//\1/p' ${BAS}.log)
if [ -n "${CG}" ] ; then
    echo "CAUTION: conflict or merged files !"
    echo "${CG}"
fi
exit 0
