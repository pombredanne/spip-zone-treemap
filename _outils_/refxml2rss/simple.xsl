<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:strip-space elements="*" />

<!-- les fichiers plugin.xml -->
<xsl:template match="plugin">
		<plugin>
  		<nom><xsl:value-of select="nom" /></nom>
			<version><xsl:value-of select="version" /></version>
			<etat><xsl:value-of select="etat" /></etat>
			<prefix><xsl:value-of select="prefix" /></prefix>
		</plugin>		
</xsl:template>

<!-- les fichiers theme.xml -->
<xsl:template match="theme">
		<theme>
  		<nom><xsl:value-of select="nom" /></nom>
			<version><xsl:value-of select="version" /></version>
			<etat><xsl:value-of select="etat" /></etat>
		</theme>		
</xsl:template>


</xsl:stylesheet>
