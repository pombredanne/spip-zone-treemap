#!/bin/bash

###
# creer le rss de ref.xml en transformant avec xsltproc de libxslt + ref.xsl
# utile notamment lorsque php n'a pas l'extension xslt
# Arguments:
# 1) le ref.xml.gz a transformer (ca marche aussi pour paquets.xml.gz)
#    defaut: ./ref.xml.gz, celui du repertoire courant
# 2) le .rss.xml.gz destination
#    defaut meme dossier/nom que 1) avec .rss insere devant .xml.gz
# 3) le log de la transformation, renseigne sur l'etat du xml traite :)
#    defaut meme dossier/nom que 1) avec .xml.gz remplace par .rss.log
#
#
# ref.xsl doit etre present dans le repertoire de ce script
# (prendre tout le dossier refxml2rss)

# repertoire du script pour y trouver le ref.xsl
cd `dirname $0`
exedir=$PWD
cd $OLDPWD

# le ref.xml.gz a traiter
refxmlgz=${1:-./ref.xml.gz}

# fichier rss.xml.gz destination,
# si param 2 absent, on insere juste .rss avant .xml.gz => ref.rss.xml.gz
rssxmlgz=${2:-${refxmlgz/%xml.gz/rss.xml.gz}}

# fichier log,
# si param 3 absent, on remplace .xml.gz par .rss.log => ref.rss.log
rsslog=${3:-${refxmlgz/%xml.gz/rss.log.txt}}

# echo "$refxmlgz $refdir $rssxmlgz"; exit

gunzip -c "$refxmlgz" | xsltproc "$exedir/ref.xsl" 2>$rsslog - | gzip > "$rssxmlgz"
echo "$(basename $0) $(date): Fini pour la production de $rssxmlgz depuis $refxmlgz" >> $rsslog

nblignes=`wc -l $rsslog | cut -d " " -f1`
echo "nblignes fichier de log  : $nblignes" 
if [ "$nblignes" -gt "1" ]
then
#mutt -s "[PAQUET-ZONE]probleme " ben.spip@gmail.com,pierre.fiches@free.fr,marcimat@free.fr <$rsslog 
mail -s "[PAQUET-ZONE]probleme " ventrea@gmail.com,spip-zone@rezo.net  <$rsslog
fi
