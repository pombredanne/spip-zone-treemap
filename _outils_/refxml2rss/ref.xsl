<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:dc="http://purl.org/dc/elements/1.1/">
	
<xsl:template match="zone">
    <rss version="2.0" xmlns:dc="http://purl.org/dc/elements/1.1/">
    
    <channel>
    	<title>SPIP-zone - liste des plugins</title>
    	<link>http://trac.rezo.net/trac/spip-zone/browser/_plugins_</link>
    	<description type="html">Liste des &lt;a href=&quot;http://trac.rezo.net/trac/spip-zone/browser/_plugins_&quot;&gt; plugins de spip-zone&lt;a /&gt; avec &lt;a href=&quot;http://files.spip.org/spip-zone/&quot;&gt;leurs paquets&lt;a /&gt;. 
			Extraction des infos de ref.xml.gz par une xslt pour g#233;nerer un rss, http://trac.rezo.net/trac/spip-zone/browser/_outils_/refxml2rss/</description>
    	<language>fr</language>
    	<generator>http://trac.rezo.net/trac/spip-zone/browser/_outils_/refxml2rss/ref.xsl</generator>
      <xsl:apply-templates select="zone_elt" />
    </channel>
    
    </rss>
</xsl:template>

<xsl:template match="zone_elt"><xsl:if test="plugin">
	<item>
		<title><xsl:value-of select="plugin/nom" />
		<xsl:text> - Version : </xsl:text><xsl:value-of select="plugin/version" /></title>  
		<link><xsl:value-of select="svn_revision/uri" /></link>
		<guid isPermaLink="true"><xsl:value-of select="svn_revision/uri" /></guid>
		<dc:date><xsl:value-of select="svn_revision/commit" /></dc:date>
		<category domain="http://www.spip.net/fr_article3448.html#etat"><xsl:value-of select="plugin/etat" /></category>
		<xsl:if test="svn_revision/archive"><enclosure url="{svn_revision/archive}" type="application/zip" /></xsl:if>
		<description><xsl:value-of select="svn_revision/origine" /> 
		<xsl:text>&lt;br /&gt;Auteur : </xsl:text><xsl:value-of select="plugin/auteur" />
		<xsl:text>&lt;br /&gt;&lt;br /&gt;</xsl:text>
			<xsl:value-of select="plugin/description" />
		</description>
	</item>
</xsl:if></xsl:template>

</xsl:stylesheet>