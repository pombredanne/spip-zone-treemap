<?
// Cr�e le processeur XSLT
	 $xh = xslt_create();
	 xslt_set_base ($xh, 'file://' . getcwd () . '/');

// ici on utilise des variables en dur pour xml en entr�e et xslt utilis�e
  $xml_entre = 'plugin.xml';  // chemin du fichier xml � lire
  if (substr('/', $xml_entre)) {  // si $xml_entre est un chemin r�cup�rer le nom du fichier
  	 $e = split('/', $xml_entre);
  	 $nom_fichier = array_pop($e); 
  }
  else {  // si $xml_entre est un nom de fichier
  		 $nom_fichier = $xml_entre;
  }
  $xslt_utiliser = 'simple.xsl';  // chemin de la xslt � utiliser
  
// fichier simplifi� � cr�er en sortie
  $rep_sortie = 'sortie/';  // chemin du r�pertoire de sortie
  $chemin_fichier = $rep_sortie.'simple.'.$nom_fichier;
 
// traitement par la XSLT
    $result = xslt_process($xh, $xml_entre, $xslt_utiliser);
    if (!$result){
       echo ("Erreur XSLT ...");
    }
  	else {
  // �crire le fichier simplifi� obtenu
      	$pointeur = fopen($chemin_fichier, 'w+');
      	fwrite($pointeur, $result);
      	fclose($pointeur);
  	}
// D�truit le processeur XSLT
xslt_free($xh);


?>
