# Un petit Updater toujours d'apres un vieux script de Cedric MORIN
# Revu et mis au gout du jour par loiseau2nuit
# d'apr�s les fichiers SVN_install_spip_xyz.sh disponibles sur la zone 

# MAJ SPIP CORE
echo "Veuillez patienter pendant que SPIP se met � jour ..."
echo "Please wait while we upgrade your SPIP installation ..."
svn update *
sleep 4;
echo "OK !"

# MAJ PLUGINS
echo "Veuillez patienter pendant que vos Plugins se mettent � jour ..."
echo "Please wait while we upgrade your Plugins directory ..."
cd plugins
svn update admin/*
sleep 4;
svn update integr/*
sleep 4;
svn update mods/*
sleep 4;
svn update squel/*
sleep 4;
echo "OK !"
cd ..

echo "Operation terminee. Veuillez verifier que tout s'est d�roul� correctement avant de continuer !"
echo "Process ended. Please check that all went right before continuing !"