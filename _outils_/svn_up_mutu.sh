#!/bin/bash
#
# script de mise a jour par svn update d'un SPIP mutualise
#
# Copyright 2009, cy_altern <cy_altern@yahoo.fr>
# distributed under GNU/GPL licence
#
# 3 elements mis a jour independament:
#	. le SPIP situe dans le dossier /rep/spip/primaire  
#	. le pseudo-plugin mutualisation situe dans le dossier /rep/spip/primaire/mutualisation 
#	. tous les plugins situes dans les dossiers /rep/spip/primaire/plugins/le_pluginX si max_prof=1 
#						ou  /rep/spip/primaire/plugins/sous_dossier/le_pluginY  si max_prof=2 
#						ect...


# $rep_mutu est le chemin du repertoire racine du SPIP primaire de la mutu
rep_mutu="/var/www/mon-site-spip.tld/html"

# $max_prof est le nombre de niveau de sous-dossiers � scanner dans le repertoire /plugins
max_prof=1

echo "*** mise a jour du SPIP de $rep_mutu ***"
svn up --accept theirs-full $rep_mutu

echo "*** mise a jour de $rep_mutu/mutualisation ***"
svn up --accept theirs-full $rep_mutu/mutualisation

echo "*** mise a jour de tous les dossiers de $rep_mutu/plugins ***"
for rep in $( find $rep_mutu/plugins -maxdepth $max_prof -type d  ) 
do 
	echo "*** mise a jour de $rep ***"
	svn up --accept theirs-full $rep
done

echo "*** fin de la mise a jour ***"