<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2008                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;	#securite

include_spip('inc/headers');
include_spip('base/abstract_sql');

// http://doc.spip.org/@install_bases
function install_bases($adresse_db, $login_db, $pass_db,  $server_db, $choix_db, $sel_db, $chmod_db){
	global $spip_version_base;

	// Prefix des tables :
	// contrairement a ce qui est dit dans le message (trop strict mais c'est
	// pour notre bien), on va tolerer les chiffres en plus des minuscules
	// S'il n'est pas defini par mes_options/inc/mutualiser, on va le creer
	// a partir de ce qui est envoye a l'installation
	if (!defined('_INSTALL_TABLE_PREFIX')) {
		$table_prefix = ($GLOBALS['table_prefix'] != 'spip')
		? $GLOBALS['table_prefix']
		: trim(preg_replace(',[^a-z0-9],','',strtolower(_request('tprefix'))));
		// S'il est vide on remet spip
		if (!$table_prefix)
			$table_prefix = 'spip';
	} else {
		$table_prefix = _INSTALL_TABLE_PREFIX;
	}

	$GLOBALS['connexions'][$server_db]
	= spip_connect_db($adresse_db, 0, $login_db, $pass_db, '', $server_db);

	$GLOBALS['connexions'][$server_db][$GLOBALS['spip_sql_version']]
	= $GLOBALS['spip_' . $server_db .'_functions_' . $GLOBALS['spip_sql_version']];

	$fquery = sql_serveur('query', $server_db);
	if ($choix_db == "new_spip") {
		if (preg_match(',^[a-z_][a-z_0-9]*$,i', $sel_db))
			$fquery("CREATE DATABASE $sel_db", $server_db);
	}

	sql_selectdb($sel_db, $server_db);
	// Completer le tableau decrivant la connexion

	$GLOBALS['connexions'][$server_db]['prefixe'] = $table_prefix;
	$GLOBALS['connexions'][$server_db]['db'] = $sel_db;

	$old = sql_showbase($table_prefix  . "_meta", $server_db);
	if ($old) $old = sql_fetch($old, $server_db);
	if (!$old) {

		// Si possible, demander au serveur d'envoyer les textes
		// dans le codage std de SPIP,
		$charset = sql_get_charset(_DEFAULT_CHARSET, $server_db);

		if ($charset) {
			sql_set_charset($charset['charset'], $server_db);
			$GLOBALS['meta']['charset_sql_base'] = 
				$charset['charset'];
			$GLOBALS['meta']['charset_collation_sql_base'] = 
				$charset['collation'];
			$GLOBALS['meta']['charset_sql_connexion'] = 
				$charset['charset'];
			$charsetbase = $charset['charset'];
		} else {
			spip_log(_DEFAULT_CHARSET . " inconnu du serveur SQL");
			$charsetbase = 'standard';
		}
		spip_log("Creation des tables. Codage $charsetbase");
		creer_base($server_db); // AT LAST
		creer_base_types_doc($server_db);
		// memoriser avec quel charset on l'a creee

		if ($charset) {
			$t = array('nom' => 'charset_sql_base',
				   'valeur' => $charset['charset'],
				   'impt' => 'non');
			@sql_insertq('spip_meta', $t, '', $server_db);
			$t['nom'] = 'charset_collation_sql_base';
			$t['valeur'] = $charset['collation'];
			@sql_insertq('spip_meta', $t, '', $server_db);
			$t['nom'] = 'charset_sql_connexion';
			$t['valeur'] = $charset['charset'];
			@sql_insertq('spip_meta', $t, '', $server_db);
		}
		$t = array('nom' => 'version_installee',
			   'valeur' => $spip_version_base,
			   'impt' => 'non');
		@sql_insertq('spip_meta', $t, '', $server_db);
		$t['nom'] = 'nouvelle_install';
		$t['valeur'] = 1;
		@sql_insertq('spip_meta', $t, '', $server_db);
	} else {

	  // pour recreer les tables disparues au besoin
	  spip_log("Table des Meta deja la. Verification des autres.");
	  creer_base($server_db); 
	  $fupdateq = sql_serveur('updateq', $server_db);

	  $r = $fquery("SELECT valeur FROM spip_meta WHERE nom='version_installee'", $server_db);

	  if ($r) $r = sql_fetch($r, $server_db);
	  $version_installee = !$r ? 0 : (double) $r['valeur'];
	  if (!$version_installee OR ($spip_version_base < $version_installee)) {
	    $fupdateq('spip_meta', array('valeur'=>$spip_version_base, 'impt'=>'non'), "nom='version_installee'", $server_db);
	    spip_log("nouvelle version installee: $spip_version_base");
	  }
	  // eliminer la derniere operation d'admin mal terminee
	  // notamment la mise a jour 
	  @$fquery("DELETE FROM spip_meta WHERE nom='import_all' OR  nom='admin'", $server_db);
	}

	$ligne_rappel = ($server_db != 'mysql') ? ''
	: (test_rappel_nom_base_mysql($server_db)
	  .test_sql_mode_mysql($server_db)	);

	$result_ok = @$fquery("SELECT COUNT(*) FROM spip_meta", $server_db);
	if (!$result_ok) return "<!--\nvielle = $old rappel= $ligne_rappel\n-->";

	if ($chmod_db) {
		install_fichier_connexion(_FILE_CHMOD_TMP, "@define('_SPIP_CHMOD', ". sprintf('0%3o',$chmod_db).");\n");
	}

	if (preg_match(',(.*):(.*),', $adresse_db, $r))
		list(,$adresse_db, $port) = $r;
	else $port = '';

	install_fichier_connexion(_FILE_CONNECT_TMP, 
				  $ligne_rappel
				  . install_connexion($adresse_db,
						      $port,
						      $login_db,
						      $pass_db,
						      $sel_db,
						      $server_db,
						      $table_prefix));
	return '';
}

// http://doc.spip.org/@install_propose_ldap
function install_propose_ldap()
{
	return generer_form_ecrire('install', (
			fieldset(_T('info_authentification_externe'),
				array(
				'etape' => array(
					'label' => _T('texte_annuaire_ldap_1'),
					'valeur' => 'ldap1',
					'hidden' => true
					)),
				 bouton_suivant(_T('bouton_acces_ldap'))
				 )));
}


// http://doc.spip.org/@install_premier_auteur
function install_premier_auteur($email, $login, $nom, $pass, $hidden)
{
	return info_progression_etape(3,'etape_','install/') .
		info_etape(_T('info_informations_personnelles'),

		     "<strong>"._T('texte_informations_personnelles_1')."</strong>" .
			     aide ("install5") .
			     "<p>" ._T('texte_informations_personnelles_2')." " ._T('info_laisser_champs_vides') ."</p>"
			     )
	. generer_form_ecrire('install', (
			  "\n<input type='hidden' name='etape' value='4' />"
			  . $hidden
			  . fieldset(_T('info_identification_publique'),
				    array(
					  'nom' => array(
							 'label' => "<strong>"._T('entree_signature')."</strong><br />\n"._T('entree_nom_pseudo_1')."\n",
							 'valeur' => $nom
							 ),
					  'email' => array(
							   'label' => "<strong>"._T('entree_adresse_email')."</strong>\n",
							   'valeur' => $email
							   )
					  )
				    )

			  . fieldset(_T('entree_identifiants_connexion'),
				   array(
					 'login' => array(
							  'label' => "<strong>"._T('entree_login')."</strong><br />\n"._T('info_plus_trois_car')."\n",
							  'valeur' => $login
							  ),
					 'pass' => array(
							 'label' => "<strong>"._T('entree_mot_passe')."</strong><br />\n"._T('info_plus_cinq_car_2')."\n",
							 'valeur' => $pass
							 ),
					 'pass_verif' => array(
							       'label' => "<strong>"._T('info_confirmer_passe')."</strong><br />\n",
							       'valeur' => $pass
							       )
					 )
				     )
			  . bouton_suivant()));
}

// http://doc.spip.org/@install_etape_3_dist
function install_etape_3_dist()
{
	$ldap_present = _request('ldap_present');

	if (!$ldap_present) {
		$adresse_db = defined('_INSTALL_HOST_DB')
		? _INSTALL_HOST_DB
		: _request('adresse_db');

		$login_db = defined('_INSTALL_USER_DB')
		? _INSTALL_USER_DB
		: _request('login_db');

		$pass_db = defined('_INSTALL_PASS_DB')
		? _INSTALL_PASS_DB
		: _request('pass_db');

		$server_db = defined('_INSTALL_SERVER_DB')
		? _INSTALL_SERVER_DB
		: _request('server_db');

		$chmod_db = defined('_SPIP_CHMOD')
		? _SPIP_CHMOD
		: _request('chmod');

		$choix_db = defined('_INSTALL_NAME_DB')
		? _INSTALL_NAME_DB
		: _request('choix_db');

		$sel_db = ($choix_db == "new_spip")
		? _request('table_new') : $choix_db;

		$res = install_bases($adresse_db, $login_db, $pass_db,  $server_db, $choix_db, $sel_db, $chmod_db);

		if ($res) {
			$res .= info_progression_etape(2,'etape_','install/', true);

			$res .= "<p class='resultat echec'><strong>"._T('avis_operation_echec')."</strong></p>"._T('texte_operation_echec');
		}
	
	} else { 
		$res = '';
		list($adresse_db, $login_db, $pass_db, $sel_db, $server_db)
		= analyse_fichier_connection(_FILE_CONNECT_TMP);
	}

	if (!$res) {
		if (file_exists(_FILE_CONNECT_TMP))
			include(_FILE_CONNECT_TMP);
		else
			redirige_url_ecrire('install');
	
		if (file_exists(_FILE_CHMOD_TMP))
			include(_FILE_CHMOD_TMP);
		else
			redirige_url_ecrire('install');

		$hidden = predef_ou_cache($adresse_db, $login_db, $pass_db, $server_db)
		  . (defined('_INSTALL_NAME_DB') ? ''
		     : "\n<input type='hidden' name='sel_db' value='$sel_db' />");
		$res =  "<p class='resultat ok'><strong>"
		. _T('info_base_installee')
		. "</strong></p>"
		. install_premier_auteur(_request('email'),
					_request('login'),
					_request('nom'),
					_request('pass'),
					 $hidden)
		  . (($ldap_present  OR !function_exists('ldap_connect'))
		     ?  '' : install_propose_ldap());
	}

	echo install_debut_html();
	echo $res;
	echo install_fin_html();
}

// Tester si mysql ne veut pas du nom de la base dans les requetes

// http://doc.spip.org/@test_rappel_nom_base_mysql
function test_rappel_nom_base_mysql($server_db)
{
	$GLOBALS['mysql_rappel_nom_base'] = true;
	sql_delete('spip_meta', "nom='mysql_rappel_nom_base'", $server_db);
	$ok = spip_query("INSERT INTO spip_meta (nom,valeur) VALUES ('mysql_rappel_nom_base', 'test')", $server_db);

	if ($ok) {
		sql_delete('spip_meta', "nom='mysql_rappel_nom_base'", $server_db);
		return '';
	} else {
		$GLOBALS['mysql_rappel_nom_base'] = false;
		return "\$GLOBALS['mysql_rappel_nom_base'] = false; ".
		"/* echec de test_rappel_nom_base_mysql a l'installation. */\n";
	}
}
// http://doc.spip.org/@test_sql_mode_mysql
function test_sql_mode_mysql($server_db){
	$res = sql_select("version() as v",'','','','','','',$server_db);
	$row = sql_fetch($res,$server_db);
	if (version_compare($row['v'],'5.0','>='))
		return "define('_MYSQL_SET_SQL_MODE',true);\n";
	return '';
}
?>
