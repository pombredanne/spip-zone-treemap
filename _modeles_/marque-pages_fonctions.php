<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

if (count($_POST)
AND include_spip('inc/autoriser')
AND true  //autoriser('marquepage') // ???
) {
	if ($_POST['creer_marque_pages']) {
		include_spip('base/abstract_sql');
		include_spip('inc/rubriques');

		// Si id_rubrique vaut 0 ou n'est pas definie, creer l'article
		// dans la premiere rubrique racine
		if ($id_rubrique = intval($_POST['id_rubrique'])) {

			$row = spip_fetch_array(spip_query("SELECT id_secteur FROM spip_rubriques WHERE id_rubrique=$id_rubrique"));
			$id_secteur = $row['id_secteur'];

			$id_syndic = spip_abstract_insert("spip_syndic",
				"(id_rubrique, id_secteur, statut, date, nom_site, syndication)",
				"($id_rubrique, $id_secteur, 'marque', NOW(), 'marque-pages', 'non')");

			include_spip('inc/invalideur');
			suivre_invalideur(0,true);
		}
	}
	else if ($_POST['creer_bookmark']) {
		include_spip('base/abstract_sql');

		// Si id_rubrique vaut 0 ou n'est pas definie, creer l'article
		// dans la premiere rubrique racine
		if ($id_syndic = intval($_POST['id_syndic'])) {
			$id_syndic_article = spip_abstract_insert("spip_syndic_articles",
				"(id_syndic, statut, date)",
				"($id_syndic, 'publie', NOW())");

			include_spip('inc/invalideur');
			suivre_invalideur(0,true);
		}
	}
}

?>