/***************************************************************************\
 *  Supports de formation - Création de sites Web sous SPIP                *
 *                                                                         *
 *  Copyright (c) 2007 - Patrick VINCENT                                   *
 *                                                                         *
 *  Ces supports sont distribués sous licence GNU/FDL.					   *
\***************************************************************************/



***************************************************************************
 * HISTORIQUE
***************************************************************************
-Novembre 2007 - Patrick VINCENT - pvincent@erasme.org / pvincent4@gmail.com



***************************************************************************
 * LICENSE
***************************************************************************
Les supports et les exercices sont proposés sous license GNU - FDL :
http://fr.wikipedia.org/wiki/Licence_de_documentation_libre_GNU
http://www.gnu.org/copyleft/fdl.html

Des licences particulières peuvent s’appliquer à quelques éléments inclus 
dans le support de cours.



***************************************************************************
 * DESCRIPTIF
***************************************************************************

- Programme de formation sur 3 à 4 journées pour utilisateurs maîtrisant 
les bases de la création de pages Web.
- Exercices et corrections liés au document principal, compter la moitié 
du temps pour les TPs.


***************************************************************************
 * PLAN
***************************************************************************

PARTIE 1 - SPIP - UTILISATION
A-LES SITES WEB
1- Rappels techniques et généralités
2- Les transferts par FTP
3- Les CMS
4- SPIP
 
B-UTILISATION DE SPIP
0- Installation
1- Connexion à l’espace privé et chaîne de publication
2- L’interface privée
3- Configuration du site
4- Gérer les rubriques
5- Publication : articles, brèves, sites syndiqués
6- Mots-clefs
7- Retrouver du contenu
8- Travail Collaboratif sous SPIP
9- Forum, calendrier, messagerie
10- Les statistiques
 
C-LES OUTILS DU WEBMASTER
1- Installer un squelette
2- Installer un plugin
3- Installer une copie locale du site

PARTIE 2 - LES STANDARDS DU WEB
A-DU HTML AU XHTML
1- Principes du langage HTML
2- Le langage XHTML
3- L’entête (head)
4- Structure du corps (body)
5- Les balises du corps
 
B-CSS2
1- Les feuilles de style
2- Syntaxe et sélecteurs
3- Mise en forme
4- Positionnement
5- Création de templates
6- Compatibilité et accessibilité
 
C-INTRODUCTION A JAVASCRIPT
1- Généralités
2- Variables, fonctions et opérateurs
3- Evènements
4- Les objets du navigateur

PARTIE 3 - SPIP - MODIFICATION DE LA MISE EN FORME
A-FONCTIONNEMENT DU NOYAU
1- Structure des données
2- Calcul des pages et modèles d’affichage
3- Le système de fichiers du noyau
 
B-LE LANGAGE DE BOUCLES DE SPIP
1- Boucles : Syntaxe, critères, filtres de sélection et de tri
2- Balises : Syntaxe, filtres de balise, balises communes à tout le site, formulaires
3- Boucles avancées
4- Variables et portée
5- Inclusion de squelettes
 
C-DEVELOPPEMENT D’UN JEU DE SQUELETTES
1- En-tête
2- Menu
3- Sommaire
4- Article
5- Rubrique
6- Forum
 
D-METHODOLOGIE
1- Chaîne de développement du site
2- Mettre à jour une ancienne version de SPIP -> 1.9.2
3- Reprise d’un site existant -> SPIP

