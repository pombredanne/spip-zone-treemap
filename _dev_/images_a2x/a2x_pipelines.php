<?php


function a2x_post_image_filtrer($img){
	// recuperer l'image finale
	$fichier = extraire_attribut($img, 'src');
	if (($p=strpos($fichier,'?'))!==FALSE)
		$fichier=substr($fichier,0,$p);
	if (strlen($fichier) < 1)
		$fichier = $img;

	// TODO : regarder si $fichier-x2 existe deja
	// et est plus recent que $fichier : le renvoyer dans ce cas
	// car la prochaine fois on aura pas la chaine de .src pour reconstruire l'image x2

	// recuperer la chaine de filtre qui a produit l'image
	$reconstruire = array();
	$f = $fichier;
	while (lire_fichier($src = "$f.src",$source)
		AND $valeurs=unserialize($source)
    AND ($f = $valeurs['fichier']) # l'origine est connue (on ne verifie pas son existence, qu'importe ...)
    ) {
			$reconstruire[] = $valeurs['reconstruction'];
  }

	// reconstruire une image x2 en rejouant la chaine de filtres
	// (les filtres sans modif ne feront rien)
	$imgx2 = null;
	while (count($reconstruire)){
		$r = array_pop($reconstruire);
		$fonction = $r[0];
		$args = $r[1];
		if (is_null($imgx2))
			$imgx2 = $args[0];

		// repartir de l'etape precedente
		$args[0] = $imgx2;

		// agrandir l'image x2
		switch ($fonction){
			case "image_reduire" :
				// multiplier la taille par 2
				if(isset($args[1]))
					$args[1] = 2*$args[1];
				if(isset($args[2]))
					$args[2] = 2*$args[2];
				break;
			default:
				// TODO : gerer aussi les image_recadre, image_passe_partout...
				break;
		}

		// appliquer le filtre
		$imgx2 = call_user_func_array($fonction, $args);
	}
	$fichierx2 = extraire_attribut($imgx2, 'src');
	if (($p=strpos($fichierx2,'?'))!==FALSE)
		$fichierx2=substr($fichierx2,0,$p);
	ramasse_miettes($fichierx2);

	// TODO : sauvegarder l'image dans $fichier-x2
	// car la prochaine fois on aura pas la chaine de .src pour reconstruire l'image x2

	// supprimer les images intermediaires qui ont servie a produire $img
	// car ce ne sera pas forcement fait celon ce qu'on renvoie ici
	# si jamais le fichier final n'a pas ete calcule car suppose temporaire
	if (!@file_exists($fichier))
		reconstruire_image_intermediaire($fichier);
	ramasse_miettes($fichier);

	// a ce stade :
	// $imgx2 est l'image x2
	// $img est l'image x1
	// on peut renvoyer ce qu'on veut (mix des 2, imgx2 uniquement...)

	return $imgx2;
}

?>