﻿
je veux afficher correctement les images sur les écrans haute
résolution (par exemple l'écran Retina de l'iPad3, mais aussi le mode
"agrandissement" des images dans les ebooks). Dans le jargon c'est
"@2x".


Pour ça l'idée est d'agir au niveau de |image_reduire, en procédant en
deux passes :

1. on multiplie par deux les valeurs de taille demandée (par exemple
1000px au lieu de 500px), et on réduit

2. ensuite on réduit de nouveau l'image, mais uniquement en mode
"style='width:…'", c-à-d sans créer de vignette, à la taille voulue
(500px)


ce qui nous donne par exemple :

* en partant d'une image de 2890px :

    <img src='local/cache-vignettes/L1000xH563/xxxxx.jpg' width='500'
height='282' alt='JPEG - 1.3 Mo' style='width:500px;height:282px;'
class='a2x' />


* en partant d'une image de 756px (donc entre 500 et 2x500) :

   <img src='local/cache-vignettes/L756xH756/xxxx.jpg' width='500'
height='500' alt='JPEG - 253.9 ko' style='width:500px;height:500px;'
class='a2x' />


* (bien entendu, une image plus petite que 500px passe sans modif)



A noter : vu comment le code est construit, j'ai trouvé beaucoup plus
simple d'intégrer cela directement dans le core (désactivé par défaut)
que de le faire en plugin.

=> Une question serait de savoir si on l'active par un define(), par
un réglage global du site, ou par une fonction dont le résultat peut
dépendre de l'air du temps (page demandée, nom du client, etc.). Je
n'ai pas d'avis ferme là-dessus.

=> Par ailleurs au passage je me rends compte qu'on n'a pas dans le
core de fonction |ajouter_classe{} ou |ajouter_style{} qui
permettraient de manipuler les attributs class="xxx yyy" et
style="css:truc; css2: machin;". Je pense qu'on peut les ajouter
aussi, ce sera utile pour cette fonction.


Pour en discuter je pose une proposition de patch sur la zone dans
_dev_/images_a2x/
(http://zone.spip.org/trac/spip-zone/changeset/65886) ; mais c'est
bien une modif du core que je propose. Si vous voulez tester, il
suffit de récupérer ce inc/filtres_images_lib_mini.php et de le mettre
à la place de celui du core. J'ai aussi ajouté une bordure verte sur
les images @2x, pour débugguer.