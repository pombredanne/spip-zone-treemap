#!/bin/bash

WORKDIR=$1
PID=$$ 
LOGDIR="${WORKDIR}/log_tests"
DATESQL=`date +%Y%m%d`
HEURE=`date +%H:%M:%S`
LOGFILE=$LOGDIR/tests_${DATESQL}
MAILFILE=$LOGDIR/MAIL 

mkdir $LOGDIR

echo "" >$LOGFILE

#on recupere la page avec tous les liens a tester 
lynx -dump http://localhost/test/tests/ | grep "/tests/"  | sed -e "s/^[\ ]*[0-9]*\./ /g"| sort>$LOGDIR/listeTests

#on teste chacun des liens 
for i in `cat $LOGDIR/listeTests `
do 
	echo "$i" >>$LOGFILE
	echo "- - - - - - - - - - - - - - - - - - " >>$LOGFILE
	lynx -dump $i >>$LOGFILE
done

#Le nombre total de tests 
NBTEST=`cat $LOGDIR/listeTests |wc -l `
echo "Les tests http://demo.spip.org/svn/tests"  >$MAILFILE
echo "Nb de tests a effectuer : $NBTEST" >>$MAILFILE
NBTESTOK=`grep OK $LOGFILE   | wc -l `>>$MAILFILE
NBTESTKO=`grep KO $LOGFILE  | wc -l `>>$MAILFILE
echo "Nb de tests OK          : $NBTESTOK">>$MAILFILE
echo "Nb de tests 'sans OK'   : $NBTESTKO">>$MAILFILE
echo "" >>$MAILFILE
echo "" >>$MAILFILE
cat $LOGFILE >>$MAILFILE 

#on vire les Xsecondes pour pouvoir faire le diff 
cat $LOGFILE | perl -pe "s/([0-9]*\.[0-9]*s)//g" >${MAILFILE}_diff

diff ${MAILFILE}_diff ${MAILFILE}_diff.old >${MAILFILE}_ladiff.txt 
cp ${MAILFILE}_diff ${MAILFILE}_diff.old 

echo " " >>$MAILFILE  
echo "- - - - - - - - - - - - - - - - - - " >>$MAILFILE 
echo "- - - - - - - - - - - - - - - - - - " >>$MAILFILE 
echo "diff par raport � la version pr�c�dente : " >>$MAILFILE 
echo " " >>$MAILFILE  

cat ${MAILFILE}_ladiff.txt >>$MAILFILE 

cat ${MAILFILE} | perl -pe "s/http:\/\/localhost\/test/http:\/\/demo.spip.org\/svn/g" >${MAILFILE}_final


 
if test `cat  ${MAILFILE}_ladiff.txt | wc -l` -ne 0
then
  mutt -s "[tests] resultat des testsi ... c est le diff a la fin qui est interessant" spip-commit@rezo.net < ${MAILFILE}_final
  #mutt -s "[tests] resultat des tests" ben.spip@gmail.com < ${MAILFILE}_final
fi

