#!/bin/bash
# Redirection des mails
# Placer dans le virtual host correspondant quelque chose comme:
#	php_admin_value sendmail_path "/home/demo/demo/demo_mail.sh /home/demo/public_html/stable/IMG/mail.txt"
# l'argument est le fichier ou le mail va etre ecrit, ici dans IMG car il est public et nettoye

fichier=${1:=/tmp/mail.txt}
shift

echo "******************* $(date) *********************
arguments: $@
" >> $fichier
cat - >> $fichier

exit 0
