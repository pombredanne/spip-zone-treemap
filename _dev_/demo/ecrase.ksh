#!/bin/bash
#Init 
WORK=/home/demo/public_html
cd $WORK


echo ""
echo "005. export mysql modele "
  echo "----------------------------------"
mysqldump -udemo -p$1 demo_modele_stable >$WORK/../tmp/demo_modele.sql 
echo ""




for i in /home/demo/public_html:stable /home/demo/public_html/dev:svn
do

	repertoire=`echo $i | cut -d ':' -f1`
	version=`echo $i | cut -d ':' -f2`
  echo "-------------------------------"
  echo "----- REPERTOIRE $repertoire   | $version -----"
  echo "-------------------------------"
  
  
  echo ""
  echo "001. svn update .. les tests aussi"
  echo "----------------------------------"
  svn update $repertoire
  svn update $repertoire/tests

  echo ""
  echo "002. import de la base  "
  echo "----------------------------------"
  mysql -udemo -p$1 demo_${version} <$WORK/../tmp/demo_modele.sql
  
  echo ""
  echo "003. des updates dans la base" 
  echo "----------------------------------"
  mysql -udemo -p$1 demo_${version} <$WORK/../demo/nettoyage.sql

  echo ""
  echo "004. upgrade db + differents trucs php (php_post_copie)"
  echo "----------------------------------"
  cd $repertoire/ecrire/
  cp $WORK/../demo/php_post_copie.php .
  php php_post_copie.php $version
  #rm -f php_post_copie.php


  echo ""
  echo "005. IMG copie modele vers $i "
  echo "----------------------------------"
  rm -rf $repertoire/IMG 
  cp -p -r $WORK/modele/IMG $repertoire
  # initialisation du log des mails
  echo "Début log mails: $(date)
----------------------------------
" >> $repertoire/IMG/mail.txt
  chmod a+w $repertoire/IMG/mail.txt
  echo ""



  echo ""
  echo ""
  echo "006. Nettoyage cache "
  echo "----------------------------------"
  rm -rf $repertoire/tmp/*
  rm -rf $repertoire/squelettes
  echo ""
  echo ""
  echo ""

  echo ""
  echo ""
  echo "007. On personnalise un peu le squelette "
  echo "----------------------------------"
  mkdir $repertoire/squelettes
  echo "- le pied de page avec la date"
  cp $repertoire/dist/inc-pied.html $repertoire/squelettes
  madate=`date` 
  sed "s/<small>/<small>Site ecrase le <i>$madate<\/i> <br>/" $repertoire/squelettes/inc-pied.html >/tmp/$$
  mv /tmp/$$ $repertoire/squelettes/inc-pied.html
  echo "- le menu_lang a droite "
  cp $repertoire/dist/inc-rubriques.html $repertoire/squelettes
  sed "s/<B_rubriques>/\[(#MENU_LANG)\]<br\/><B_rubriques>/" $repertoire/squelettes/inc-rubriques.html >/tmp/$$
  mv /tmp/$$ $repertoire/squelettes/inc-rubriques.html
  cp $repertoire/squelettes/inc-rubriques.html $repertoire/squelettes-test/dist
  echo ""
  echo ""

  echo ""
  echo ""
  echo "008. Mes options "
  echo "----------------------------------"
  echo "<?php" >$WORK/modele/config/mes_options.php
  echo '$forcer_lang=true;'>>$WORK/modele/config/mes_options.php
  echo "define('SWITCHER_AFFICHER', true);" >>$WORK/modele/config/mes_options.php
  echo '?>'>>$WORK/modele/config/mes_options.php
  cp $WORK/modele/config/mes_options.php $repertoire/config
  
done 

