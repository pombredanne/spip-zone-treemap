#!/bin/bash
#Init 
WORK=/home/demo/public_html
cd $WORK

#Checkout des versions 
svn checkout svn://trac.rezo.net/spip/branches/spip-1.9.2 $WORK/modele/ 
svn checkout svn://trac.rezo.net/spip/branches/spip-1.9.2 $WORK/
svn checkout svn://trac.rezo.net/spip/spip $WORK/dev/ 


#Les moteurs de recherches n ont rien a faire ici 
echo "User-agent: *">$WORK/robots.txt 
echo "Disallow:/">>$WORK/robots.txt 

#Droits sur les repertoires 
for i in "$WORK/modele" $WORK "$WORK/dev"   
do
  chmod 777 $i/IMG
  chmod 777 $i/tmp
  chmod 777 $i/local
  chmod 777 $i/config
  


  mkdir $i/plugins
  mkdir $i/squelettes-test 
   
  # sarka  
#  mkdir $i/squelettes-test/sarka
#  svn checkout http://www.smellup.net/svn/SARKA-SPIP/TAGS/RELEASE_1.9.2/squelettes  $i/squelettes-test/sarka  
   
   
  # alternatives 
  mkdir $i/squelettes-test/alternatives
  svn checkout svn://zone.spip.org/spip-zone/_squelettes_/alter/alternatives $i/squelettes-test/alternatives 
   
  # dist  
  cp -p -r $i/dist $i/squelettes-test/ 

  #plugin switcher
  mkdir $i/plugins/switcher  
  svn checkout svn://zone.spip.org/spip-zone/_plugins_/_stable_/switcher  $i/plugins/switcher 
  

  #les tests 
  mkdir $i/tests  
  svn checkout svn://zone.spip.org/spip-zone/_dev_/tests $i/tests 
  
done 
