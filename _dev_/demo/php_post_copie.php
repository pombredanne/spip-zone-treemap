<?php
	print "-Upgrade de la base\n";
 	// forcer l'upgrade, ca ne fera rien si il n'y en a pas
	// ce script doit etre place dans ecrire/
	include dirname(__FILE__) . '/inc_version.php';
	if (!spip_connect()) {
		die("demo_upgrade.php: Impossible de connecter\n");
	}
	include_spip('inc/lang');

	include_spip('base/create');
	creer_base();
	include_spip('base/upgrade');
	maj_base();

	include_spip('inc/acces');
	include_spip('inc/config');
	ecrire_acces();
	init_config();

	//version svn ou stable 
	$version=$_SERVER[argv][1]; 
	print "-Changement du nom du site $version \n";
	if ( $version == "stable" ) 
	{ 
		ecrire_meta('adresse_site',"http://demo.spip.org");
	ecrire_meta('nom_site',"<multi>[fr]Site de demo SPIP [en]SPIP demo website [it]Sito dimostrativo di SPIP</multi>") ;
	}
	else
	{
		ecrire_meta('adresse_site',"http://demo.spip.org/$version");
	ecrire_meta('nom_site',"<multi>[fr]Site de demo SPIP (version $version)[en]SPIP demo website ($version version)[it]Sito dimostrativo di SPIP ($version version)</multi>") ;
	}	
	ecrire_meta('accepter_inscriptions',"non") ;
	ecrire_meta('descriptif_site',"<multi>[fr]Vous etes sur le site de demonstration de SPIP. Pour vous en servir , voir ici [->art31] [it]Benvenuti sul sito dimostrativo di SPIP. Per poterlo usare leggete
[->35] [en]This is the demonstration website for SPIP. To use it see below : [->art32]</multi> ") ;
	ecrire_metas();
?>
