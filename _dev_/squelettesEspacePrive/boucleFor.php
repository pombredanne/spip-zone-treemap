<?php

/** BOUCLE FOR
 * Christian Lefebvre, Oct. 2005
 * Distribu� sous licence GPL
 */

$GLOBALS['tables_principales']['spip_for'] =
	array('field' => array(
			 "debut" => "int",
			 "fin" => "int",
			 "pas" => "int"), 'key' => array());
$GLOBALS['table_des_tables']['for'] = 'for';

function boucle_FOR($id_boucle, &$boucles) {
	$boucle = &$boucles[$id_boucle];
	//var_export($boucle);
	if (count($boucle->separateur))
	  $code_sep= ("'". ereg_replace("'","\'",join('',$boucle->separateur)) ."'");
	else
	  $code_sep="''";

	$debut=1;
	$fin=null;
	$pas=1;

	foreach($boucle->criteres as $critere) {
	  if($critere->op!='=') continue;
	  $val= calculer_liste($critere->param[1],
						   array(), $boucles, $boucle->id_parent);

	  switch($critere->param[0][0]->texte) {
	  case 'debut': $debut= $val; break;
	  case 'fin'  : $fin  = $val; break;
	  case 'pas'  : $pas  = $val; break;
	  }
	}

	if($fin===null) {
	  erreur_squelette("pas de fin d�finie",
					   $boucle->id_boucle);
	}
	//echo "\nboucle_FOR($debut, $fin, $pas)\n";

	$code=<<<CODE
	\$SP++;
	\$code=array();
	for(\$i=$debut; \$i<=$fin; \$i+=$pas) {
		\$Numrows['$id_boucle']['compteur_boucle']=\$i;
		\$code[]=$boucle->return;
	}
	\$t0= join($code_sep, \$code);
	return \$t0;
CODE;
 
	return $code;
}

?>
