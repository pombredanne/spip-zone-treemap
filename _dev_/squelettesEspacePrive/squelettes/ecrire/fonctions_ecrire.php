<?php

function chercher_squelette($fond, $id_rubrique, $lang) {
	global $rubrique, $sousrubrique;
	if ($fond=='-') {
	  if(isset($_REQUEST["rubrique"])) {
	    $rubrique= $_REQUEST["rubrique"];
	    if(isset($_REQUEST["sousrubrique"])) {
		  $sousrubrique= $_REQUEST["sousrubrique"];
		  $fond="$rubrique/$sousrubrique";
	    } else {
		  $sousrubrique='';
		  if(!find_in_path("ecrire/$rubrique.html")) {
			$fond="$rubrique/index";
		  } else {
			$fond="$rubrique";
		  }
	    }
	    if($fond=="asuivre/asuivre") $fond='index';
	  } else {
	    $rubrique='asuivre'; $sousrubrique='asuivre';
	    $fond='index';
	  }
	}

	if(getPrefs('display')!='4') {
	  $fond= "ecrire/$fond";
	} else {
	  $fond= "ecrire-oo/$fond";
	}

	// l�, on reprend le code du chercher_squelette standard
	$ext = $GLOBALS['extension_squelette'];

	// Accrocher un squelette de base dans le chemin
	if (!$base = find_in_path("$fond.$ext")) {
		// erreur webmaster : $fond ne correspond a rien
		erreur_squelette(_T('info_erreur_squelette2',
			 array('fichier'=>$fond)),
			 $dossier);
		return '';
	}

	// supprimer le ".html" pour pouvoir affiner par id_rubrique ou par langue
	$squelette = substr($base, 0, - strlen(".$ext"));

	return $squelette;
}

// pour http_no_cache
include_ecrire('inc_headers.php');

include_ecrire('inc_layer.php3');

/**
 * ajout� � une boucle AUTEURS, limite celle ci � l'auteur actuellement connect�
 */
function critere_session($idb, &$boucles, $crit) {
	$boucle = &$boucles[$idb];
	if($boucle->type_requete!='auteurs') {
		erreur_squelette(_T('zbug_info_erreur_squelette'),
						 "critere session uniquement sur boucle auteur");
		return;
	}
	$boucle->where[] = $boucle->id_table.'.id_auteur=\'".$GLOBALS[\'auteur_session\'][\'id_auteur\']."\'';
}


/**
 * r�cup�re le tableau des pr�f�rences de l'auteur courant, ou un de ses champs
 */
function balise_PREFS($p) {
  if ($p->param && !$p->param[0][0]) {
	$champ=  calculer_liste($p->param[0][1],
							$p->descr,
							$p->boucles,
							$p->id_boucle);
	array_shift($p->param);
  }

  if(!$champ) {
	$champ= 'null';
  }

  $p->code = 'getPrefs('.$champ.')';
  $p->statut = 'php';

  return $p;
}

/**
 * affiche son contenu si le display est dans la liste des arguments
 */
function balise_DISPLAY($p) {
  $cond=array();
  $disp='($x=getPrefs(\'display\'))';
  
  foreach($p->param as $param) {
	if(!$param[0]) {
	  $val= calculer_liste($param[1], $p->descr,
						   $p->boucles, $p->id_boucle);
	  if($val=="'avancees'" || $val=="'basiques'") {
		$cond[]="getPrefs('options')==$val";
	  } elseif($val=="'large'" || $val=="'etroit'") {
		$cond[]="getPrefs('spip_ecran')==$val";
	  } elseif($val{1}=='!') {
		$cond[]="$disp!='".substr($val, 2);
	  } else {
		$cond[]="$disp==$val";
	  }
	  $disp='$x';
	}
  }

  $p->code = '('.join(' && ', $cond).')?\' \':\'\'';
  $p->statut = 'php';

  return $p;
}

function balise_RIEN($p) {
  $p->code = "''";
//   $param0= &$p->param[0][1];
//   $p->code = calculer_liste(array_shift($param0), $p->descr,
// 							$p->boucles, $p->id_boucle);
  $p->statut = 'php';

  return $p;
}

function balise_AIDE($p) {
  $aide= $p->param[0][1][0]->texte;
// calculer_liste($p->param[0][1], $p->descr,
// 						$p->boucles, $p->id_boucle);
  $p->code = "'".str_replace("'", "\\'", aide($aide))."'";
  $p->statut = 'php';

  return $p;
}

function balise_TIMESTAMP($p) {
  $delta= $p->param[0][1][0]->texte;
  $p->code = "UNIX_TIMESTAMP($delta)";
  $p->statut = 'php';
}

 /**
  * initialise un tableau static avec les pr�f�rences de l'utilisateur courant
  * et retourne la valeur d'une de ses entr�es, ou tout le tableau si
  * $champ est null
  */
function getPrefs($champ= null) {
  global $prefs;

  if($champ===null) return $prefs;
  return $prefs[$champ];
}

/**
 * retourne la valeur d'un meta
 */
function balise_META($p) {
	if ($p->param && !$p->param[0][0]) {
		$meta =  calculer_liste($p->param[0][1],
					$p->descr,
					$p->boucles,
					$p->id_boucle);
		array_shift($p->param);
	}
	$p->code = 'lire_meta('.$meta.')';
	$p->statut = 'php';
	return $p;
}

/**
 * retourne la valeur d'une constante PHP
 */
function balise_DEFINE($p) {
	if ($p->param && !$p->param[0][0]) {
		$define = $p->param[0][1][0]->texte;
		array_shift($p->param);
	}
	$p->code = $define;
	$p->statut = 'php';
	return $p;
}



/**
 * permet de r�cup�rer un attribut accesskey de 1 � 9 puis 0
 * les appels au dela du 10�me ne retournent rien
 */
function balise_ACCESSKEY($p) {
	static $menu_accesskey=0;

	$p->code= "((++\$GLOBALS['menu_accesskey'] < 10)?
		' accesskey=\''.\$GLOBALS['menu_accesskey'].'\''
		:((\$GLOBALS['menu_accesskey'] == 10)?' accesskey=\'0\'':''))";
	$p->statut = 'php';
	return $p;
}



/**
 * retourne un tableau d�crivant les entr�es de la barre d'icones
 * filtr� selon les droits de l'utilisateur
 */
function BarreDIcones($prefs) {
	global $spip_lang_rtl, $spip_lang;
	$statut= getPrefs('statut');
	$barre=array(
		array(
			'rubrique' => 'asuivre', 'sousrubrique' => 'asuivre',
			'icone' => 'asuivre-48.png',
			'libelle' => 'icone_a_suivre',
		),
		array(
			'rubrique' => 'documents', 'sousrubrique' => 'rubriques',
			'icone' => "documents-48$spip_lang_rtl.png",
			'libelle' => 'icone_edition_site',
		),
		array(
			'rubrique' => 'redacteurs', 'sousrubrique' => 'forum-interne',
			'icone' => 'messagerie-48.png',
			'libelle' => 'titre_forum',
		),
		array(
			'rubrique' => 'auteurs', 'sousrubrique' => 'redacteurs',
			'icone' => 'redacteurs-48.png',
			'libelle' => 'icone_auteurs',
			'menu' => null,
	));
	if($statut=='0minirezo'
	   && $GLOBALS['connect_toutes_rubriques']
	   && lire_meta('activer_statistiques')) {
		$barre[]=array(
			'rubrique' => 'suivi', 'sousrubrique' => 'statistiques',
			'icone' => 'statistiques-48.png',
			'libelle' => 'icone_statistiques_visites',
		);
	}
	if($statut=='0minirezo'
	   && $GLOBALS['connect_toutes_rubriques']) {
		$barre[]=array(
			'rubrique' => 'administration', 'sousrubrique' => 'configuration',
			'icone' => 'administration-48.png',
			'libelle' => 'icone_configuration_site',
		);
	}
	$barre[]='espacement';
	$barre[]=array(
			'rubrique' => 'vide', 'sousrubrique' => 'aide-en-ligne',
			'icone' => 'aide-48'.lang_dir($spip_lang,$spip_lang_rtl).'.png',
			'libelle' => 'icone_aide_ligne',
			'url' => "javascript:window.open('aide_index.php3?var_lang=$spip_lang', 'aide_spip', 'scrollbars=yes,resizable=yes,width=740,height=580');",
			'url2' => "aide_index.php3?var_lang=$spip_lang",
			'target' => 'aide_spip'
	);
	$barre[]=array(
			'rubrique' => 'visiter', 'sousrubrique' => 'visiter',
			'icone' => "visiter-48$spip_lang_rtl.png",
			'libelle' => 'icone_visiter_site',
			'url' => '/'
	);
	return $barre;
}


function largeurIcone($texte) {
	$display= getPrefs('display');

	$texte= _T($texte);
	if ($display == 1) {
		$largeur = 80;
	} elseif ($display == 3){
		$largeur = 40;
	} elseif (count(explode(" ", $texte)) > 1) {
		$largeur = 80;
	} else {
	    $largeur = 70;
	}
	if ($display != 3 AND strlen($texte)>16) $largeur += 20;

	return $largeur;
}

function BarreDIconesSecondaires($prefs) {
	$id_auteur= getPrefs('id_auteur');
	$statut= getPrefs('statut');
	$options= getPrefs('options');
	$display= getPrefs('display');
	$connect_toutes_rubriques= getPrefs('connect_toutes_rubriques');

	if($statut!='0minirezo' || !$connect_toutes_rubriques) {
		return null;
	}

	if ($display == 3) {
		$delta = 60;
	} else {
		$delta = 80;
	}
	// l'original ajoute 4px s'il y a un libelle de plusieurs mots, mais bof ...
	if (getPrefs('spip_ecran') == "large") $delta+= 30;
	$decalage=largeurIconeBandeauPrincipal(_T('icone_a_suivre'));

	$barre=array();

	// barre d'icones "�dition"
	$sousbarre=array();

	// faire un count � la place !
	$nombre_articles = spip_num_rows(spip_query("SELECT art.id_article FROM spip_articles AS art, spip_auteurs_articles AS lien WHERE lien.id_auteur = '$id_auteur' AND art.id_article = lien.id_article LIMIT 1"));

	if ($nombre_articles > 0) {
	  $sousbarre[]=array('libelle' => "icone_tous_articles",
						 'icone' => "article-24.gif",
						 'rubrique' => 'documents',
						 'sousrubrique' => "articles");
	}
	if (lire_meta("activer_breves") != "non"){
	  $sousbarre[]=array('libelle' => "icone_breves",
						 'icone' => "breve-24.gif",
						 'rubrique' => 'documents',
						 'sousrubrique' => "breves");
	}
	if ($options == "avancees"){
	  if (lire_meta('articles_mots') != "non") {
		$sousbarre[]=array('libelle' => "icone_mots_cles",
						   'icone' => "mot-cle-24.gif",
						   'rubrique' => 'documents',
						   'sousrubrique' => "mots");
	  }

	  if (lire_meta('activer_sites') != "non") {
		$sousbarre[]=array('libelle' => "icone_sites_references",
						   'icone' => "site-24.gif",
						   'rubrique' => 'documents',
						   'sousrubrique' => "sites");
	  }

	  $nombre_docrub= spip_num_rows(spip_query("SELECT * FROM spip_documents_rubriques LIMIT 1"));
	  if ($nombre_docrub > 0) {
		$sousbarre[]=array('libelle' => "icone_doc_rubrique",
						   'icone' => "doc-24.gif",
						   'rubrique' => 'documents',
						   'sousrubrique' => "documents");
	  }
	}
	
	$barre[]= array('rubrique' => 'documents',
					'decalage' => $decalage,
					'barre' => $sousbarre);
	$decalage+=largeurIconeBandeauPrincipal(_T('icone_edition_site'));

	// barre d'icones "forum"
	$sousbarre=array();

	if (lire_meta('forum_prive_admin') == 'oui') {
		$sousbarre[]=array('libelle' => "icone_forum_administrateur",
						   'icone' => "forum-admin-24.gif",
						   'rubrique' => 'redacteurs',
					   'sousrubrique' => "privadm");
	}

	$sousbarre[]=array('libelle' => "icone_suivi_forums",
					   'icone' => "suivi-forum-24.gif",
					   'rubrique' => 'redacteurs',
					   'sousrubrique' => "forum-controle");

	$sousbarre[]=array('libelle' => "icone_suivi_pettions",
					   'icone' => "suivi-petition-24.gif",
					   'rubrique' => 'redacteurs',
					   'sousrubrique' => "suivi-petition");
	
	$barre[]= array('rubrique' => 'redacteurs',
					'decalage' => $decalage,
					'barre' => $sousbarre);
	$decalage+=largeurIconeBandeauPrincipal(_T('titre_forum'));

	// barre d'icones "auteurs"
	$sousbarre=array();

	$sousbarre[]=array('libelle' => "icone_informations_personnelles",
					   'icone' => "fiche-perso-24.gif",
					   'rubrique' => 'auteurs',
					   'sousrubrique' => "perso");

	$sousbarre[]=array('libelle' => "icone_creer_nouvel_auteur",
					   'icone' => "auteur-24.gif",
					   'rubrique' => 'auteurs',
					   'sousrubrique' => "creer");

	$barre[]= array('rubrique' => 'auteurs',
					'decalage' => $decalage,
					'barre' => $sousbarre);
	$decalage+=largeurIconeBandeauPrincipal(_T('icone_auteurs'));

	if(lire_meta("activer_statistiques") != 'non') {
		// barre d'icones "statistiques"
		$sousbarre=array();

		$sousbarre[]=array('libelle' => "icone_repartition_visites",
						   'icone' => "rubrique-24.gif",
						   'rubrique' => 'suivi',
						   'sousrubrique' => "repartition");

		if (lire_meta('multi_articles') == 'oui'
			OR lire_meta('multi_rubriques') == 'oui') {
		  $sousbarre[]=array('libelle' => "onglet_repartition_lang",
							 'icone' => "langues-24.gif",
							 'rubrique' => 'suivi',
							 'sousrubrique' => "repartition-langues");
		}

		$sousbarre[]=array('libelle' => "titre_liens_entrants",
						   'icone' => "referers-24.gif",
						   'rubrique' => 'suivi',
						   'sousrubrique' => "referers");

		$barre[]= array('rubrique' => 'suivi',
						'decalage' => $decalage,
						'barre' => $sousbarre);
		$decalage+=largeurIconeBandeauPrincipal(_T('icone_statistiques_visites'));
	}

	// barre d'icones "config"
	$sousbarre=array();

	$sousbarre[]=array('libelle' => "icone_gestion_langues",
					   'icone' => "langues-24.gif",
					   'rubrique' => 'administration',
					   'sousrubrique' => "langues");

	if ($options == "avancees") {
	  $sousbarre[]=array('libelle' => "icone_maintenance_site",
						 'icone' => "base-24.gif",
						 'rubrique' => 'administration',
						 'sousrubrique' => "base");
	  $sousbarre[]=array('libelle' => "onglet_vider_cache",
						 'icone' => "cache-24.gif",
						 'rubrique' => 'administration',
						 'sousrubrique' => "cache");
	} else {
	  $sousbarre[]=array('libelle' => "icone_sauver_site",
						 'icone' => "base-24.gif",
						 'rubrique' => 'administration',
						 'sousrubrique' => "base");
	}

	$barre[]= array('rubrique' => 'administration',
					'decalage' => $decalage,
					'barre' => $sousbarre);
	$decalage+=largeurIconeBandeauPrincipal(_T('icone_configuration_site'));

	return $barre;
}

function largeurIconeBandeauPrincipal($texte) {
	$spip_display= getPrefs('display');
	$spip_ecran= getPrefs('spip_ecran');
	$connect_statut= getPrefs('statut');
	$connect_toutes_rubriques= getPrefs('connect_toutes_rubriques');

	if ($spip_display == 1){
	  $largeur = 80;
	} elseif ($spip_display == 3) {
	  $largeur = 60;
	} elseif (count(explode(" ", $texte)) > 1) {
	  $largeur = 84;
	} else {
	  $largeur = 80;
	}

	if ($spip_ecran == "large") $largeur = $largeur + 30;

	if (!($connect_statut == "0minirezo" AND $connect_toutes_rubriques)) {
		$largeur = $largeur + 30;
	}

	return $largeur;
}

function lienBarreDIcones($icone) {
  if($icone['url']) {
	if (eregi("^javascript:",$icone['url'])) {
		return 'onClick="'.$icone['url'].'; return false;" href="'
		  .$icone['url2'].'" target="'.$icone['target'].'"';
	}
	else {
		return 'href="'.$icone['url'].'"';
	}
  } else {
	return 'href="ecrire.php?rubrique='.$icone['rubrique']
	  .'&sousrubrique='.$icone['sousrubrique'].'"';
  }
}

//include_ecrire('inc_calendrier.php');
function html_agenda() {
	$today = getdate(time());
	$jour_today = $today["mday"];
	$mois_today = $today["mon"];
	$annee_today = $today["year"];
	$date = date("Y-m-d", mktime(0,0,0,$mois_today, 1, $annee_today));
	$mois = mois($date);
	$annee = annee($date);
	$jour = jour($date);

	// Taches (ne calculer que la valeur booleenne...)
	if (spip_num_rows(spip_query("SELECT type FROM spip_messages AS messages WHERE id_auteur=$connect_id_auteur AND statut='publie' AND type='pb' AND rv!='oui' LIMIT 1")) OR
	    spip_num_rows(spip_query("SELECT type FROM spip_messages AS messages, spip_auteurs_messages AS lien WHERE ((lien.id_auteur='$connect_id_auteur' AND lien.id_message=messages.id_message) OR messages.type='affich') AND messages.rv='oui' AND messages.date_heure > DATE_SUB(NOW(), INTERVAL 1 DAY) AND messages.date_heure < DATE_ADD(NOW(), INTERVAL 1 MONTH) AND messages.statut='publie' GROUP BY messages.id_message ORDER BY messages.date_heure LIMIT 1"))) {
		$largeur = "410px";
		$afficher_cal = true;
	}
	else {
		$largeur = "200px";
		$afficher_cal = false;
	}



	// Calendrier
	$gadget .= "<div id='bandeauagenda' class='bandeau_couleur_sous' style='width: $largeur; $spip_lang_left: 100px;'>";
	$gadget .= "<a href='calendrier.php3?type=semaine' class='lien_sous'>";
	$gadget .= _T('icone_agenda');
	$gadget .= "</a>";
	
	$gadget .= "<table><tr>";
	$gadget .= "<td valign='top' width='200'>";
	$gadget .= "<div>";
	$gadget .= http_calendrier_agenda($annee_today, $mois_today, $jour_today, $mois_today, $annee_today, false, 'calendrier.php3');
	$gadget .= "</div>";
	$gadget .= "</td>";
	if ($afficher_cal) {
		$gadget .= "<td valign='top' width='10'> &nbsp; </td>";
		$gadget .= "<td valign='top' width='200'>";
		$gadget .= "<div>&nbsp;</div>";
		$gadget .= "<div style='color: black;'>";
		$gadget .=  http_calendrier_rv(sql_calendrier_taches_annonces(),"annonces");
		$gadget .=  http_calendrier_rv(sql_calendrier_taches_pb(),"pb");
		$gadget .=  http_calendrier_rv(sql_calendrier_taches_rv(), "rv");
		$gadget .= "</div>";
		$gadget .= "</td>";
	}
	
	$gadget .= "</tr></table>";
	$gadget .= "</div>";

	return $gadget;
}
?>
