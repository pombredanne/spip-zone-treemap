<?php
/**
 * �quivalent de page.php3 sp�cifiques aux squelettes /ecrire
 * ont attend 2 arguments rubrique et sousrubrique pour d�finir un
 * squelette.
 */

if(!defined('_SQUELETTES_ECRIRE')) {
	define('_SQUELETTES_ECRIRE', 1);

	// histoire d'avoir un point de d�part
	if (!defined('_ECRIRE_INC_VERSION')) {
	  include("ecrire/inc_version.php3");
	}

	// on commence par regarder si on a une raison d'�tre l�
	include('ecrire/inc.php3');

	if(!$GLOBALS['auteur_session']) {
	  die("y'a personne");
	}

	// on ajoute quelques bricoles au profil
	global $prefs;
	$prefs= array_merge($GLOBALS['auteur_session'], $prefs);
	$prefs['connect_toutes_rubriques']= $GLOBALS['connect_toutes_rubriques'];
	$prefs['connect_id_rubrique']= $GLOBALS['connect_id_rubrique'];
	$prefs['admin']= ($prefs['statut']=='0minirezo')?($prefs['connect_toutes_rubriques']?'tout':'restreint'):'non';

	$prefs['spip_ecran']=
		($GLOBALS['_COOKIE']['spip_ecran']=='large')?'large':'etroit';
	$prefs['spip_accepte_ajax']= $GLOBALS['_COOKIE']['spip_accepte_ajax'];

	$couleurs= $GLOBALS['couleurs_spip'][$prefs['couleur']];
	$prefs['couleur_foncee']= $couleurs['couleur_foncee'];
	$prefs['couleur_claire']= $couleurs['couleur_claire'];
	$prefs['couleur_lien']= $couleurs['couleur_lien'];
	$prefs['couleur_lien_off']= $couleurs['couleur_lien_off'];
}

// puis on cherche le squelette qui va bien selon les arguments
if(isset($contexte_inclus['fond'])) {
  $fond = $contexte_inclus['fond'];
} else {
  $fond= '-';
}

$delais=0;
$flag_preserver=true;
include ("inc-public.php3");

?>
