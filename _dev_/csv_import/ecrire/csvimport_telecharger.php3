<?php

include ("inc_csvimport.php3");

function formater_reponse($ligne, $schema, $valeurs) {
	static $groupes, $mots;

	// Prendre les differents champs dans l'ordre
	foreach ($schema as $index => $t) {
		if (!$v = $valeurs[$t['code']]) {
			$ligne[] = "";
			continue;
		}
		$ligne[] = strval(join(', ', $v));
	}
	return csv_ligne($ligne);
}
if (!isset($retour))
	$retour = 'csvimport_tous.php3';

if (!isset($table))
	$table = '';

$operations = array();

$titre = "";
$is_importable = table_importable($table,$titre,$operations);
if (in_array('export',$operations))
  $csvimport_export_actif = true;


if ((!isset($delim))&&($csvimport_export_actif)){
	$icone = "csvimport-24.png";
	$export_link = new Link("csvimport_telecharger.php3");
	if ($table) $export_link->addVar('table', $table);
	if ($retour) $export_link->addVar('retour', $retour);

	debut_page($titre, "documents", "csvimport");
	debut_gauche();

	echo "<br /><br />\n";
	debut_droite();

	debut_cadre_relief($icone);
	gros_titre($titre);
	echo "<br />\n";
	echo _L("Format du fichier téléchargé :");
	echo "<br />\n";
	// Extrait de la table en commençant par les dernieres maj
	echo $export_link->getForm('POST', '');
	echo "<select name='delim'>\n";
	echo "<option value=','>"._L("Format CSV")."</option>\n";
	echo "<option value=';'>"._L("Format CSV pour Excel (séparateur ';')")."</option>\n";
	echo "</select>";
	echo "<br /><br />\n";
	echo "<input type='submit' name='ok' value='Telecharger' />\n";

	fin_cadre_relief();


	//
	// Icones retour
	//
	if ($retour) {
		echo "<br />\n";
		echo "<div align='$spip_lang_right'>";
		icone(_T('icone_retour'), $retour, $icone, "rien.gif");
		echo "</div>\n";
	}
	fin_page();
	exit;

}

if ($csvimport_export_actif){
	if (isset($csvimport_tables_auth[$table]['field']))
		$tablefield=$csvimport_tables_auth[$table]['field'];
	else
		$tablefield=array_keys($tables_principales[$table]['field']);

	//
	// Telechargement du contenu de la table au format CSV
	//

	$output = csv_ligne($tablefield,$delim);
	//$tablefield = array_flip($tablefield);

	$query="SELECT * FROM $table";
	$result = spip_query($query);
	while ($row=spip_fetch_array($result)){
		$ligne=array();
		foreach($tablefield as $key)
		  if (isset($row[$key]))
		    $ligne[]=$row[$key];
			else
			  $ligne[]="";
		$output .= csv_ligne($ligne,$delim);
	}

	$filename = preg_replace(',[^-_\w]+,', '_', translitteration(textebrut(typo($titre))));
	$charset = lire_meta('charset');
	Header("Content-Type: text/comma-separated-values; charset=$charset");
	Header("Content-Disposition: attachment; filename=$filename.csv");
	//Header("Content-Type: text/plain; charset=$charset");
	Header("Content-Length: ".strlen($output));
	echo $output;
	exit;
}
else {
	acces_interdit();
}
?>
