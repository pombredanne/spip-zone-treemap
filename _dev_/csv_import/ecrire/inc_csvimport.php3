<?php

include ("inc.php3");
include_ecrire ("inc_db_mysql.php3");
include_ecrire ("inc_abstract_sql.php3");
include_ecrire("inc_charsets.php3");

function acces_interdit() {
	debut_page(_T('avis_acces_interdit'), "documents", "cvsimport");
	debut_gauche();
	debut_droite();
	echo "<strong>"._T('avis_acces_interdit')."</strong>";
	fin_page();
	exit;
}

function table_importable($nom_table,&$titre,&$operations){
	global $csvimport_tables_auth;
	global $connect_statut;
	$declared = false;
	foreach($csvimport_tables_auth as $table=>$infos){
	  if (strcmp($table,$nom_table)==0){
	    $declared = true;
	    if (isset($infos['statut'])){
				if (!in_array($connect_statut,$infos['statut']))
	    		$declared = false;
			}
			if ($declared){
				if (isset($infos['titre']))
					$titre .= $infos['titre'];
				if (isset($infos['operations']))
				  foreach($infos['operations'] as $op)
				    $operations[]=$op;
			}
		}
	}
	return $declared;
}

function afficher_tables($titre_table, $icone = '') {
	global $csvimport_tables_auth;
	global $connect_statut;
	global $couleur_claire, $couleur_foncee;
	global $connect_id_auteur;

	if (!$icone) $icone = "csvimport-24.png";

	if (count($csvimport_tables_auth)) {
		if ($titre_table) echo "<div style='height: 12px;'></div>";
		echo "<div class='liste'>";
		bandeau_titre_boite2($titre_table, $icone, $couleur_claire, "black");
		echo "<table width='100%' cellpadding='3' cellspacing='0' border='0'>";

		$num_rows = count($csvimport_tables_auth);

		$ifond = 0;
		$premier = true;

		$compteur_liste = 0;
		foreach($csvimport_tables_auth as $latable=>$info) {
	    $declared = true;
	    if (isset($info['statut'])){
				if (!in_array($connect_statut,$info['statut']))
	    		$declared = false;
			}
			if ($declared) {

				$maj_exist = true;
				$query="SELECT maj FROM $latable ORDER BY maj DESC";
		 		$result = spip_query($query);
		 		if (!$result) {
					$query="SELECT * FROM $latable";
			 		$result = spip_query($query);
			 		$maj_exist = false;
		 		}

		 		$nb_data=spip_num_rows($result);
		 		$last_mod='';
		 		if ($maj_exist){
					$row = spip_fetch_array($result);
					$last_mod = $row['maj'];
			 	}

				$vals = '';
				$titre = $latable;
				if (isset($info['titre']))
					$titre = $info['titre'];

				$importable = false;
				$exportable = false;
				if (isset($info['operations'])){
				  if ((in_array('add',$info['operations']))||(in_array('replaceall',$info['operations'])))
						$importable = true;
				  if (in_array('export',$info['operations']))
						$exportable = true;
				}

				$link = new Link("csvimport_import.php3");
				$link->addVar("table", $latable);
				$link->addVar("retour", $GLOBALS['clean_link']->getUrl());
				if ($nb_data) {
					$puce = 'puce-verte-breve.gif';
				}
				else {
					$puce = 'puce-orange-breve.gif';
				}

				$s = "";
				if ($importable)
					$s .= "<a href=\"".$link->getUrl()."\">";
				$s .= "<img src='img_pack/$puce' width='7' height='7' border='0'>&nbsp;&nbsp;";
				$s .= typo($titre);
				if ($importable)
					$s .= "</a>";
				$s .= " &nbsp;&nbsp;";
				$vals[] = $s;

				$s = "";
				if ($nb_data) {
					$s .= $nb_data . " " . _L("enregistrements");
					if ($last_mod)
					  $s .= " (" . $last_mod . ")";
				}
				$vals[] = $s;

				$s = "";
				if ($exportable){
					$link = new Link("csvimport_telecharger.php3");
					$link->addVar("table", $latable);
					$link->addVar("retour", $GLOBALS['clean_link']->getUrl());
					$s .= "<a href=\"".$link->getUrl()."\">";
					$s .= _L("T�l�charger");
					$s .= "</a>";
				}
				$vals[] = $s;

				$table[] = $vals;
			}
		}

		$largeurs = array('','','');
		$styles = array('arial11', 'arial1', 'arial1');
		afficher_liste($largeurs, $table, $styles);
		echo "</table>";
		echo "</div>\n";
	}
	else {
		echo _L("Pas de tables d�clar�es pour l'import CSV");
 	}
}

function csv_champ($champ) {
	$champ = preg_replace(',[\s]+,', ' ', $champ);
	$champ = str_replace(',",', '""', $champ);
	return '"'.$champ.'"';
}

function csv_ligne($ligne, $delim = ',') {
	return join($delim, array_map('csv_champ', $ligne))."\r\n";
}

/**
 * Based on an example by ramdac at ramdac dot org
 * Returns a multi-dimensional array from a CSV file optionally using the
 * first row as a header to create the underlying data as associative arrays.
 * @param string $file Filepath including filename
 * @param bool $head Use first row as header.
 * @param string $delim Specify a delimiter other than a comma.
 * @param int $len Line length to be passed to fgetcsv
 * @return array or false on failure to retrieve any rows.
 */
function importcsv($file, $head = false, $delim = ",", $enclos = '"', $len = 1000) {
   $return = false;
   $handle = fopen($file, "r");
   if ($handle){
	   if ($head) {
	       $header = fgetcsv($handle, $len, $delim);
	   }
	   while (($data = fgetcsv($handle, $len, $delim)) !== FALSE) {
	       if ($head AND isset($header)) {
	           foreach ($header as $key=>$heading) {
	               $row[$heading]=(isset($data[$key])) ? $data[$key] : '';
	           }
	           $return[]=$row;
	       } else {
	           $return[]=$data;
	       }
	   }
	   fclose($handle);
   }
   return $return;
}

function show_erreurs($erreur){
	$output = "";
	if (count($erreur)>0){
		$output .= "<div class='messages'>";
		foreach($erreur as $steper=>$desc)
			foreach($desc as $val)
				$output .=  "<strong>$steper::$val</strong><br />";
		$output .=  "</div>\n";
	}
	return $output;
}

function table_visu_extrait($nom_table,$nombre_lignes = 0){
	$maj_exist = true;
	$limit = "";
	if ($nombre_lignes > 0)
	  $limit = " LIMIT " . ($nombre_lignes+1);
	$query="SELECT * FROM $nom_table ORDER BY maj DESC" . $limit;
	$result = spip_query($query);
	if (!$result) {
		$query="SELECT * FROM $nom_table $limit";
 		$result = spip_query($query);
 		$maj_exist = false;
	}

	$nb_data=spip_num_rows($result);
	if ($nombre_lignes==0)
		$nombre_lignes = $nb_data;
	$data_count = 0;
	$head_set = false;
	$nb_col = 0;
	echo "<table>";
	while (($row = spip_fetch_array($result,SPIP_ASSOC))&&($data_count++<$nombre_lignes)){
		if (!$head_set){
			echo "<tr>";
			foreach($row as $key=>$value){
			  echo "<th>" . htmlentities($key) . "</th>";
			  $nb_col++;
			}
			echo "</tr>\n";
			$head_set = true;
		}
		echo "<tr>";
		foreach($row as $key=>$value)
		  echo "<td>" . htmlentities($value) . "</td>";
		echo "</tr>\n";
	}
	if ($nb_data>$nombre_lignes){
		$query="SELECT COUNT(*) FROM $nom_table";
		list($num_rows) = spip_fetch_array(spip_query($query));
		echo "<tr><td colspan='$nb_col' style='border-top:1px dotted;'>$num_rows "._L("lignes")." ...</td></tr>\n";
	}
	echo "</table>\n";
	if ($data_count==0)
	  echo _L("Table vide");
}

function array_visu_extrait($data, $head, $nombre_lignes = 0){
	$output = "";
	$data_count = 0;
	$head_set = false;
	$nb_col = 0;
	if ($data!=false){
		$output .= "<table>";
		foreach($data as $key=>$ligne) {
			if (($head==true)&&($head_set==false)){
				$output .= "<tr>";
				foreach($ligne as $key=>$value){
				  $output .= "<th>" . htmlentities($key) . "</th>";
				  $nb_col++;
				}
				$output .= "</tr>\n";
				$head_set = true;
			}
			else{
				$output .= "<tr>";
				foreach($ligne as $value){
				  $output .= "<td>" . htmlentities($value) . "</td>";
				}
				$output .= "</tr>\n";
			}
			if (($nombre_lignes>0)&&($data_count++>=$nombre_lignes))
			  break;
		}
		$output .= "</table>\n";
	}
	if ($data_count==0)
	  $output .= _L("Pas de donn�e");
	else
	  $output .= count($data) . _L(" lignes au total");
	return $output;
}

function array_visu_assoc($data, $table, $assoc_field, $nombre_lignes = 0){
	global $tables_principales;
	global $csvimport_tables_auth;
	if (isset($csvimport_tables_auth[$table]['field']))
		$tablefield=$csvimport_tables_auth[$table]['field'];
	else
		$tablefield=array_keys($tables_principales[$table]['field']);
	$assoc=array_flip($assoc_field);

	$output = "";
	$data_count = 0;
	$output .= "<table>";
	$output .= "<tr>";
	foreach($tablefield as $value){
	  $output .= "<th>" . htmlentities($value) . "</th>";
	}
	$output .= "</tr>\n";

	if ($data!=false){
		foreach($data as $key=>$ligne) {
			$output .= "<tr>";
			foreach($tablefield as $value){
			  $output .= "<td>";
			  if ((isset($assoc[$value]))&&(isset($ligne[$assoc[$value]])))
			    $output .= $ligne[$assoc[$value]];
				else
					$output .= "&nbsp;";
				$output .= "</td>";
			}
			$output .= "</tr>\n";
			if (($nombre_lignes>0)&&(++$data_count>=$nombre_lignes))
			  break;
		}
	}
	$output .= "</table>";

	if ($data_count>0)
	  $output .= count($data) . _L(" lignes au total");
	return $output;
}

function field_associate($data, $table, $assoc_field){
	global $tables_principales;
	global $csvimport_tables_auth;
	$assoc=$assoc_field;
	$csvfield=array_keys($data{1});
	$csvfield=array_flip($csvfield);

	if (isset($csvimport_tables_auth[$table]['field']))
		$tablefield=$csvimport_tables_auth[$table]['field'];
	else
		$tablefield=array_keys($tables_principales[$table]['field']);
	$tablefield=array_flip($tablefield);

	// on enleve toutes les associations dont
	// la cle n'est pas un csvfield
	// la valeur n'est pas un tablefield
	// l'un des deux est deja affecte
	foreach ($assoc as $key=>$value){
		$good_key = false;
		$good_value = false;
		if (array_key_exists($key,$csvfield)){
		  $good_key = true;
		}
		if ((array_key_exists($value,$tablefield))||($value==-1)){
		  $good_value = true;
		}
		if (($good_key==false)||($good_value==false))
		  unset($assoc[$key]);
		else{
			unset($csvfield[$key]);
			if ($value!=-1) unset($tablefield[$value]);
		}
	}

	//assoc auto des cles qui portent le meme nom
	$accents=array('�','�','�','�','�',"�","�");
	$accents_rep=array('e','e','e','a','u',"o","c");
	foreach(array_keys($csvfield) as $csvkey){
		foreach(array_keys($tablefield) as $tablekey)
		  if (strcasecmp(str_replace($accents,$accents_rep,$csvkey),$tablekey)==0){
				$assoc[$csvkey]=$tablekey;
				unset($csvfield[$csvkey]);
				unset($tablefield[$tablekey]);
			}
 	}
	//assoc des autres dans l'ordre qui vient
	$tablefield=array_keys($tablefield);
	foreach(array_keys($csvfield) as $csvkey){
		$assoc[$csvkey]=array_shift($tablefield);
		if ($assoc[$csvkey]==NULL) $assoc[$csvkey]="-1";
		unset($csvfield[$csvkey]);
	}
	return $assoc;
}

function field_configure($data, $table, $assoc){
	$output = "";
	global $tables_principales;
	global $csvimport_tables_auth;

	$csvfield=array_keys($data{1});
	if (isset($csvimport_tables_auth[$table]['field']))
		$tablefield=$csvimport_tables_auth[$table]['field'];
	else
		$tablefield=array_keys($tables_principales[$table]['field']);

	$output .= "<table><tr><td>"._L("Champ CSV")."</td><td>"._L("Champ Table")."</td></tr>";
	foreach($csvfield as $csvkey){
		$output .=  "<tr>";
		$output .=  "<td>$csvkey</td>";
		$output .= "<td><select name='assoc_field[$csvkey]'>\n";
		$output .= "<option value='-1'>"._L("Ne pas importer")."</option>\n";
		foreach($tablefield as $tablekey){
			$output .= "<option value='$tablekey'";
			if ($assoc[$csvkey]==$tablekey)
			  $output .= " selected='selected'";
			$output .= ">$tablekey</option>\n";
		}
		$output .= "</select></td></tr>";
	}
	$output .= "</table>";
	return $output;
}

// vidange de la table
function vidange_table($table){
	$query = "DELETE FROM $table";
	$res = spip_query($query); // et voila ...
}

function ajoute_table_csv($data, $table, $assoc_field, &$erreur){
	global $tables_principales;
	global $csvimport_tables_auth;
	$assoc = array_flip($assoc_field);

	$tablefield=array_keys($tables_principales[$table]['field']);
	$output = "";
	// y a-t-il une cle primaire ?
	if (isset($tables_principales[$table]['key']["PRIMARY KEY"])){
		$primaire = $tables_principales[$table]['key']["PRIMARY KEY"];
		// la cle primaire est-elle importee ?
		if (in_array($primaire,$assoc_field))
		  unset($primaire);
 	}
	// y a-t-il un champ TIMESTAMP ?
	$test=array_flip($tables_principales[$table]['field']);
	if (isset($test['TIMESTAMP']))
	  $stamp = $test['TIMESTAMP'];

	if ($data!=false){
		$count_lignes = 0;
		foreach($data as $key=>$ligne) {
      $count_lignes ++;
			// creation de la cle primaire puis modif de l'enregistrement
			//if (isset($primaire)){
				$what = "(";
				$with = "(";
				$check = array_flip($tablefield);
				foreach($check as $key=>$value){
				  if ((isset($assoc[$key]))&&(isset($ligne[$assoc[$key]]))){
						$what .= "$key,";
						$with .= "'" . addslashes($ligne[$assoc[$key]]) . "',";
						unset($check[$key]);
					}
		 		}
				if ((isset($stamp))&&isset($check[$stamp])){
					$what .= "$stamp,";
					$with .= "NOW(),";
				}
				if ((strlen($what)>1)&&(strlen($with)>1)) {
					$what = substr($what,0,strlen($what)-1) . ")";
					$with = substr($with,0,strlen($with)-1) . ")";
					$id_primary = spip_abstract_insert($table, $what, $with);
					if ($id_primary==0)
					  $erreur[$count_lignes][] = "ajout impossible ::$what::$with::<br />";
				}
				else
				  $erreur[$count_lignes][] = "rien � ajouter<br />";
		 	//}
			// creation de l'enregistrement direct
		 	/*else {

			}*/
		}
	}
}

?>
