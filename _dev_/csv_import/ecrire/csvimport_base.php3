<?php
// -----------------------------------------------------------------------------
// Declaration des tables agenda partage
// creation 04/10/2005 pour SPIP 1.8.2d

$csvimport_tables_auth=
array(
	'csvimport'=>array(
	  // Titre de la table tel qu'affiche dans lIHM
		'titre'=>'Table des Logements',
		// statuts autorises pour l'import/export
		'statut'=>array('0minirezo'),
		// operations autorisees
		'operations'=>array('add','replaceall','export'),
		// champs autorises pour l'import/export
		'field'=>array("code","libelle","commune","type","surfmax","surfmin","prixmin","prixmax","prixmoy","nombre","zone")
		)
);

global $tables_principales;

//-- Table CSVIMPORT ------------------------------------------
$csvimport_matable_nom='csvimport';
$csvimport_matable = array(
		"id_import"	=> "bigint(21) NOT NULL",
		"code" => "varchar(4)",
		"libelle" => "varchar(100)",
		"commune" => "varchar(100)",
		"type" => "varchar(28)",
		"surfmax" => "varchar(28)",
		"surfmin" => "varchar(28)",
		"prixmin" => "varchar(28)",
		"prixmax" => "varchar(28)",
		"prixmoy" => "varchar(28)",
		"nombre" => "varchar(28)",
		"zone" => "varchar(50)",
		"maj"	=> "TIMESTAMP"
);

$csvimport_matable_key = array(
		"PRIMARY KEY"	=> "id_import",
		//"KEY date_debut"	=> "date_debut",
		//"KEY date_fin"	=> "date_fin"
		"KEY type"	=> "type"
		);

$tables_principales[$csvimport_matable_nom] =
	array('field' => &$csvimport_matable, 'key' => &$csvimport_matable_key);

eval('
function boucle_'.strtoupper($csvimport_matable_nom).'($id_boucle, &$boucles) {
	$boucle = &$boucles[$id_boucle];
	$id_table = $boucle -> id_table;
	$boucle -> from[] = "'.$csvimport_matable_nom.' AS $id_table";
	return calculer_boucle($id_boucle, $boucles);
}
');

global $table_primary;
$table_primary[$csvimport_matable_nom]="id_import";

?>
