<?php

include ("inc_csvimport.php3");

if (!isset($step))
	$step = 1;
if (!isset($retour))
	$retour = 'csvimport_tous.php3';

if (!isset($table))
	$table = '';

$titre = _L("Import CSV : ");
$icone = "csvimport-24.png";
$operations = array();

$import_link = new Link("csvimport_import.php3");
if ($table) $import_link->addVar('table', $table);
if ($retour) $import_link->addVar('retour', $retour);
$clean_link = new link();
$clean_link = clone($import_link); //PHP5--> il faut cloner explicitement

$is_importable = table_importable($table,$titre,$operations);

if (in_array('replaceall',$operations))
  $csvimport_replace_actif = true;
if (in_array('add',$operations))
  $csvimport_add_actif = true;

//
// Affichage de la page
//

debut_page($titre, "documents", "csvimport");
debut_gauche();

echo "<br /><br />\n";



debut_droite();

$erreur=array();

if ($is_importable) {

	// --- STEP 3
	if ($step==3){
		if ((isset($remplacer))&&(isset($annule_remplace)))
		  $step--;
		else if ((isset($ajouter))&&(isset($annule_ajoute)))
		  $step--;
		else if (isset($apercu))
		  	$step--;
		else if ((isset($remplacer))&&(!isset($csvimport_replace_actif)))
		  	$step--;
		else if ((isset($ajouter))&&(!isset($csvimport_add_actif)))
		  	$step--;
  }

	if ($step==3){
		global $step;
		if ( (!isset($file_name))||(!isset($tmp_name))||(!isset($size))||(!isset($type)) )
			 $erreur[$step][] = _L("Fichier absent");

		if (!isset($delim))
			 $erreur[$step][] = _L("Delimiteur non d�fini");
		/*if (!isset($head))
			 $erreur[$step][] = _L("Header non d�fini");*/
		if (!isset($assoc_field))
			 $erreur[$step][] = _L("Correspondances CSV-Table non d�finies");
		if (isset($erreur[$step])) $step--;
	}
	
	if ($step==3){
		if (!isset($head)) $head = false;
		$data = importcsv($tmp_name, $head, $delim);
		if ($data==false) {
		  $erreur[$step][] = _L("Fichier vide");
		}
		$new_assoc=field_associate($data, $table, $assoc_field);
		$test=array_diff($new_assoc,$assoc_field);
		if (count($test)>0){
			$erreur[$step][] = _L("Correspondances CSV-Table incompl�tes");
		}
		if (isset($erreur[$step])) $step--;
	}
	if ($step==3){
		$link = new link();
		$link = clone($import_link); //PHP5--> il faut cloner explicitement
		$link->addVar('file_name', $file_name);
		$link->addVar('tmp_name', $tmp_name);
		$link->addVar('size', $size);
		$link->addVar('type', $type);
		$link->addVar('step', 3);
		foreach($assoc_field as $key=>$value)
			$link->addVar("assoc_field[$key]", $value);
		$link->addVar("delim",$delim);
		$link->addVar("head",$head);

		/*echo "<br />\n";
		if (count($erreur)>0){
			echo "<div class='messages'>";
			foreach($erreur as $steper=>$desc)
				foreach($desc as $val)
					echo "<strong>$steper::$val</strong><br />";
			echo "</div>\n";
	 	}*/

		if ((isset($remplacer))&&(!isset($confirme_remplace))){
			$link->addVar('remplacer', 'oui');
			debut_cadre_relief($icone);
			gros_titre($titre);
			// Extrait de la table en commen�ant par les dernieres maj
			table_visu_extrait($table,5);
			fin_cadre_relief();

			debut_cadre_enfonce();
			echo array_visu_assoc($data, $table, $assoc_field, 5);
			fin_cadre_enfonce();
			echo "<div style='padding: 2px; color: black;'>&nbsp;";
			echo _L("Cette op�ration va entra�ner la suppression de toutes les donn�es pr�sentes dans la table.");
			echo $link->getForm('POST', '');
			echo "<input type='submit' name='annule_remplace' value='"._L('Annuler')."' class='fondo'>";
			echo "</div>\n";
			echo "<div class='iconedanger' style='margin-top:15px;'>";
			echo "<input type='submit' name='confirme_remplace' value='"._L('Remplacer toute la table')."' class='fondo'>";
			echo "</div>\n";
			echo "</form>";
		}
		else if ((isset($ajouter))&&(!isset($confirme_ajoute))){
			$link->addVar('ajouter', 'oui');
			debut_cadre_relief($icone);
			gros_titre($titre);
			// Extrait de la table en commen�ant par les dernieres maj
			table_visu_extrait($table,5);
			fin_cadre_relief();

			debut_cadre_enfonce();
			echo array_visu_assoc($data, $table, $assoc_field, 5);
			fin_cadre_enfonce();
			echo "<div style='padding: 2px; color: black;'>&nbsp;";
			echo _L("Les donn�es du fichier CSV vont �tre ajout�es � la table comme illustr� ci-dessus.");
			echo $link->getForm('POST', '');
			echo "<input type='submit' name='annule_ajoute' value='"._L('Annuler')."' class='fondo'> ";
			echo "<input type='submit' name='confirme_ajoute' value='"._L('Ajouter les donn�es')."' class='fondo'>";
			echo "</form>";
 		}
		else {
			// vidange de la table
			if ((isset($remplacer))&&(isset($confirme_remplace))){
				vidange_table($table);
			}
			// le reste est identique que ce soit un ajout ou un remplace
			if ((isset($remplacer))||(isset($ajouter))){
				$err = array();
				$out = ajoute_table_csv($data, $table, $assoc_field,$err);

				debut_cadre_relief($icone);
				gros_titre($titre);
				// Extrait de la table en commen�ant par les dernieres maj
				table_visu_extrait($table,10);
				fin_cadre_relief();

				if (count($err)){
					echo bouton_block_invisible("erreurs");
					echo count($err) . _L(" erreurs lors de l'ajout dans la base");
					echo debut_block_invisible("erreurs");
					echo show_erreurs($err);
					echo fin_block();
				}
				else
					echo show_erreurs($err);

				//debut_cadre_enfonce();
				// Extrait de la table en commen�ant par les dernieres maj
				//table_visu_extrait($table,10);
				//fin_cadre_enfonce();
  		}
		}
	}	else {
		debut_cadre_relief($icone);
		gros_titre($titre);
		// Extrait de la table en commen�ant par les dernieres maj
		table_visu_extrait($table,5);
		fin_cadre_relief();
 	}


	//
	// Icones retour
	//
	if ($retour) {
		echo "<br />\n";
		echo "<div align='$spip_lang_right'>";
		icone(_T('icone_retour'), $retour, $icone, "rien.gif");
		echo "</div>\n";
	}




	// --- STEP 2
	if ($step==2){
		global $step;
		if (!isset($_FILES))
			$erreur[$step][] = _L("Probl�me inextricable...");
		if (
				(!isset($_FILES['csvfile']))
			&&( (!isset($file_name))||(!isset($tmp_name))||(!isset($size))||(!isset($type)) )
			 )
			 $erreur[$step][] = _L("Probl�me lors du chargement du fichier");

		if ((isset($_FILES['csvfile']))&&($_FILES['csvfile']['error']!=0))
			$erreur[$step][]=_L("Probl�me lors du chargement du fichier (erreur ".$_FILES['csvfile']['error'].")");
		if (isset($erreur[$step])) $step--;
	}
	if ($step==2){
		global $step;
		if (!isset($head)) $head = false;

		if (isset($_FILES['csvfile'])){
			$file_name = $_FILES['csvfile']['name'];
			$tmp_name = $_FILES['csvfile']['tmp_name'];
			$size = $_FILES['csvfile']['size'];
			$type = $_FILES['csvfile']['type'];

			$dest = _DIR_SESSIONS.basename($tmp_name);
			move_uploaded_file ( $tmp_name, $dest );
			$tmp_name = $dest;
	 	}


		if (!isset($delim)){
			if ($type=="application/vnd.ms-excel")
				$delim = ";"; // specificite Excel de faire des fichiers csv avec des ; au lieu de ,
			else{
				$handle = fopen($tmp_name, "rt");
  			$contenu = fread($handle, 8192);
				fclose($handle);
				if ($contenu!=FALSE){
					if (substr_count($contenu,",")>=substr_count($contenu,";"))
						$delim = ",";
					else
						$delim = ";";
				}
				else
					$delim = ",";
			}
	 	}
		$data = importcsv($tmp_name, $head, $delim);
		if ($data==false) {
		  $erreur[$step][] = _L("Fichier vide");
		  $step--;
		}

		if (!isset($assoc_field))
		  $assoc_field = array();

		$assoc_field=field_associate($data, $table, $assoc_field);
	}
	if ($step==2){

		$link = new link();
		$link = clone($import_link); //PHP5--> il faut cloner explicitement
		$link->addVar('file_name', $file_name);
		$link->addVar('tmp_name', $tmp_name);
		$link->addVar('size', $size);
		$link->addVar('type', $type);

		echo "<br />\n";
		echo show_erreurs($erreur);

		debut_cadre_enfonce();
		echo array_visu_extrait($data, /*$head*/true, 5);
		fin_cadre_enfonce();


		debut_cadre_relief();
		$link->addVar('step', 3);
		echo $link->getForm('POST', '');
		echo "<div style='margin: 2px; background-color: $couleur_claire; color: black;'>&nbsp;";
		echo "Pr�visualisation ";
		echo "<input type='submit' name='apercu' value='"._L('Appliquer')."' class='fondl'>";
		echo "</div>";

		echo "<strong><label for='separateur'>"._L("Caract�re de s�paration")."</label></strong> ";
		echo "<input type='text' name='delim' id='separateur' class='fondl' style='width:2em;' maxlength='1' value='$delim'><br />";
		echo "<strong><label for='entete'>"._L("1<sup>�re</sup> ligne d'en-t�te")."</label></strong> ";
		echo "<input type='checkbox' name='head' id='entete' class='fondl' style='width:2em;' value='true'";
		if ($head==true)
		  echo " checked='checked'";
		echo "><br />";


		echo field_configure($data, $table, $assoc_field);

		echo "</div><hr />\n";

		echo "<div align='$spip_lang_left'>";

		echo array_visu_assoc($data, $table, $assoc_field, 5);
		echo "</div><hr />\n";

		/*$link->addVar('step', 3);
		foreach($assoc_field as $key=>$value)
			$link->addVar("assoc_field[$key]", $value);
		$link->addVar("delim",$delim);
		$link->addVar("head",$head);*/

		if ($csvimport_add_actif) {
			echo "<div style='padding: 2px; color: black;'>&nbsp;";
			echo "<input type='submit' name='ajouter' value='"._L('Ajouter � la table')."' class='fondo'>";
			echo "</div>\n";
		}

		if ($csvimport_replace_actif) {
			echo "<div class='iconedanger' style='margin-top:15px;'>";
			echo "<input type='submit' name='remplacer' value='"._L('Remplacer toute la table')."' class='fondo'>";
			echo "</div>\n";
		}

		echo "</form>";

		fin_cadre_relief();
	}

	// --- STEP 1

	if ($step==1){
		echo "<br />\n";
		echo "<div align='$spip_lang_left'>";
		echo show_erreurs($erreur);

		$link = new link();
		$link = clone($import_link); //PHP5--> il faut cloner explicitement
		$link->addVar('step', 2);
		$link->addVar('head', 'true');
		echo $link->getForm('POST', '', 'multipart/form-data');
		echo "<strong><label for='file_name'>"._L("Fichier CSV � importer")."</label></strong> ";
		echo "<br />";
		echo "<input type='file' name='csvfile' id='file_name' class='formo'>";
		echo "<input type='submit' name='Valider' value='"._T('bouton_valider')."' class='fondo'>";
		echo "</form></div>\n";
	}
}
else {
	//
	// Icones retour
	//
	if ($retour) {
		echo "<br />\n";
		echo "<div align='$spip_lang_right'>";
		icone(_T('icone_retour'), $retour, $icone, "rien.gif");
		echo "</div>\n";
	}
}

fin_page();

?>
