Ce patch ajoute un cache sur find_in_path()

cf. http://thread.gmane.org/gmane.comp.web.spip.devel/48135

Je viens de tester sur spip-contrib mon proto cache de find_in_path :
il économise entre 10 et 30ms (soit environ 1ms par include_spip comme
le signalait Cédric) ; j'ai aussi profilé les include_once qui se
trouvent à l'intérieur de find_in_path() : ceux-là consomment environ
200ms (pour une page en cache).

Au total, pour servir une page en cache (environ 500ms au total), le
serveur passe autant de temps à charger et compiler des fichiers php
qu'à les exécuter.

Conclusions :
* ce cache fonctionne, mais apporte peanuts (20ms sur 500ms), tout en
introduisant potentiellement des bugs
  => je ne le commite pas dans le core
* une mise en cache de type eaccelerator, gagnerait 10 fois plus, tout
en travaillant moins
  cf. http://www.spip-blog.net/Les-jantes-en-acier-chrome.html
* ça peut valoir le coup de retirer du code  :-)

-- Fil