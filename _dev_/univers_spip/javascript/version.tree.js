
jQuery(function(){
	jQuery("<div class='treemap'>").hide().insertAfter("table");
	jQuery("table").each(function(){
		var dataFunction;
		//if(jQuery(this).is("#versions"))
		//	dataFunction = getDataVersions;
		jQuery(this).treemap(640,480,{target:jQuery(this).next(),getData:dataFunction});
		enhanceTreemap(jQuery("div.treemap"));
	})

	jQuery("#clic").click(function(){
		jQuery("table, div.treemap").toggle();
		jQuery(this).text( jQuery(this).is(":contains(treemap)")?"tableaux <<":">> treemap" );
	});
	jQuery("#clic").get(0).click();
})

function getDataVersions(t) {
	var data = [];
	var maj_versions = {};
	jQuery("tr",t).each(function(){
		var cells = jQuery(">td",this);
		var version = cells.eq(0).html();
		var maj = parseInt(version.replace(/[.-]/g,''));
		if(maj == "NaN") {
			maj_versions[maj] = [version,cells.eq(1).html()];
		} else {
			if(maj<100) maj *=10;
			var maj2 = parseInt(maj/10);
			var maj3 = parseInt(maj%10);
			if(!maj_versions[maj2]) maj_versions[maj2] = {};
			if(!maj_versions[maj2][maj3]) maj_versions[maj2][maj3] = [];
			maj_versions[maj2][maj3].push([version,cells.eq(1).html()]);
		}
	});

	jQuery.each(maj_versions,function(i,n){
		var row = [i.charAt(0)+'.'+i.charAt(1),n];
		if(n.constructor==Object) {
			var row2 = [];
			jQuery.each(n,function(i,n2){
				row2.push([row[0]+'.'+i,n2]);
			});
			row[1] = row2;
		}

		data.push(row);
	});
	return data;
}

function enhanceTreemap(t,data) {
	jQuery("div.treemapCell",t).hover(function(){jQuery(this).addClass("selected")},function(){jQuery(this).removeClass("selected")});
}