<?php
/*
 * Plugin Univers SPIP
 * (c) 2010 Cedric
 * Distribue sous licence GPL
 *
 */

function genie_univers_check_1_dist($t) {
	include_spip('inc/univers_analyser');

	// les sites en attente de validation
	$res = sql_select("*", "spip_websites", "statut='prop' AND (retry=0 OR date<".sql_quote(date('Y-m-d H:i:s',time()-3600)).")", "", "date,retry","10,10");
	while ($row = sql_fetch($res)) {
		univers_analyser_un($row);
	}
	return 0;
}


?>