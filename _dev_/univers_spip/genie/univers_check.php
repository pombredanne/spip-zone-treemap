<?php
/*
 * Plugin Univers SPIP
 * (c) 2010 Cedric
 * Distribue sous licence GPL
 *
 */

function genie_univers_check_dist($t) {
	include_spip('inc/univers_analyser');

	$il_y_a_une_heure = date('Y-m-d H:i:s',time()-3600);
	// 2 sites en attente de validation
	$res = sql_select("*", "spip_websites", "statut='prop' AND (retry=0 OR date<".sql_quote($il_y_a_une_heure).")", "", "date,retry","0,2");
	while ($row = sql_fetch($res)) {
		univers_analyser_un($row);
	}

	$il_y_a_quatre_heure = date('Y-m-d H:i:s',time()-4*3600);
	// revisiter 5 sites deja vu, en commencant par les plus anciens
	$res = sql_select("*", "spip_websites", "statut='publie' AND (retry=0 OR date<".sql_quote($il_y_a_quatre_heure).")", "", "date,retry","0,5");
	while ($row = sql_fetch($res)) {
		univers_analyser_un($row);
	}

	// revisiter un site publie, en retry de plus de 4 heures
	$res = sql_select("*", "spip_websites", "statut='publie' AND (retry>0 AND date<".sql_quote($il_y_a_quatre_heure).")", "", "date,retry","0,1");
	while ($row = sql_fetch($res)) {
		univers_analyser_un($row);
	}

	// passer a la poubelle les sites proposes sans DNS et essayes au moins 5 fois
	sql_updateq("spip_websites",array('statut'=>'poub'),"statut='prop' AND status='no-dns' AND retry>=5");

	// passer a la poubelle les sites morts et essayes au moins 10 fois
	// soit un propose pas vu vivant dans les 10 dernieres heures
	// soit un publie (donc vu vivant un jour) pas vu vivant dans les 40 dernieres heures
	sql_updateq("spip_websites",array('statut'=>'poub'),"statut IN ('prop','publie') AND status='dead' AND retry>=10");

	return 0;
}


?>