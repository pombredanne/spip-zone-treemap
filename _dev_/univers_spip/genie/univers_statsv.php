<?php
/*
 * Plugin Univers SPIP
 * (c) 2010 Cedric
 * Distribue sous licence GPL
 *
 */

function genie_univers_statsv_dist($t) {
	include_spip('inc/univers');

	$versions = array(
		"3.1",
		"3.0",
		"2.2",
		"2.1",
		"2.0",
		"1.9.3",
		"1.9.2",
		"1.9.1",
	);

	$stats = array("date" => date('Y-m-d H:i:s'));

	foreach ($versions as $version){
		$n = sql_countsel("spip_websites","statut='publie' AND spip LIKE ".sql_quote("$version%"));
		$stats[$version] = $n;
	}

	// jsonencodons
	$stats = json_encode($stats);

	$filename = _DIR_VAR.(defined('_UNIVERS_STATSV_FILE')?_UNIVERS_STATSV_FILE:"histostats.json.txt");
	ecrire_fichier($filename,"$stats\n",false,false);

	return 0;
}


?>