<?php
/*
 * Plugin Univers SPIP
 * (c) 2010 Cedric
 * Distribue sous licence GPL
 *
 */


if (!defined("_ECRIRE_INC_VERSION")) return;

function univers_declarer_tables_interfaces($interface){
	// 'spip_' dans l'index de $tables_principales
	$interface['table_des_tables']['websites']='websites';

	return $interface;
}

function univers_declarer_tables_principales($tables_principales){

	$spip_websites = array(
		"id_website" 	=> "bigint(21) NOT NULL",
		"url" => "varchar(255) default '' NOT NULL",
		"titre" => "text DEFAULT '' NOT NULL",
		"descriptif"	=> "text DEFAULT '' NOT NULL",
		"ip" => "varchar(255) default '' NOT NULL",
		"spip" => "varchar(255) default '' NOT NULL",
		"server" => "varchar(255) default '' NOT NULL",
		"php" => "varchar(255) default '' NOT NULL",
		"gzip" => "varchar(3) default '' NOT NULL",
		"length" => "bigint(21) NOT NULL",
		"size" => "bigint(21) NOT NULL",
		"plugins" => "bigint(21) default NULL",
		"pays" => "char(3) default '' NOT NULL",

		"date" => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL",
		"statut" => "varchar(10) default 'prop' NOT NULL",

		"retry" 	=> "int(5) default 0 NOT NULL",
		"status" 	=> "varchar(10) default '' NOT NULL",
		);

	$spip_websites_key = array(
		"PRIMARY KEY" 	=> "id_website",
	);

	$tables_principales['spip_websites'] = array(
		'field' => &$spip_websites,
		'key' => &$spip_websites_key);

	return $tables_principales;
}


function univers_declarer_tables_auxiliaires($tables_auxiliaires){
	$spip_websites_plugins = array(
		"id_website" 	=> "bigint(21) NOT NULL",
		"plugin" => "varchar(64) default '' NOT NULL",
		"version" => "varchar(255) default '' NOT NULL",
		);

	$spip_websites_plugins_key = array(
		"PRIMARY KEY" 	=> "id_website, plugin",
	);

	$tables_auxiliaires['spip_websites_plugins'] = array(
		'field' => &$spip_websites_plugins,
		'key' => &$spip_websites_plugins_key);

	return $tables_auxiliaires;
}

function univers_upgrade($nom_meta_base_version,$version_cible){
	$current_version = 0.0;
	if (   (!isset($GLOBALS['meta'][$nom_meta_base_version]) )
			|| (($current_version = $GLOBALS['meta'][$nom_meta_base_version])!=$version_cible)){
		include_spip('base/abstract_sql');
		if (version_compare($current_version,"0.1.2",'<')){
			echo "Creation des tables";
			include_spip('base/serial');
			include_spip('base/auxiliaires');
			include_spip('base/create');
			creer_base();
			ecrire_meta($nom_meta_base_version,$current_version=$version_cible,'non');
		}
		if (version_compare($current_version,"0.1.4",'<')){
			include_spip('inc/univers');
			$res = sql_select('referer','spip_referers',"referer LIKE '%spip.php%' AND referer NOT LIKE 'http://www.spip-contrib.net%' AND referer NOT LIKE '%localhost%'");
			echo "Import depuis les referer %spip.php% : ".sql_count($res)." <br />";
			while ($row = sql_fetch($res))
				univers_proposer_site($row['referer']);
			ecrire_meta($nom_meta_base_version,$current_version="0.1.4",'non');
		}
		if (version_compare($current_version,"0.1.5",'<')){
			$res = sql_select('referer','spip_referers',"referer LIKE '%/ecrire/%' AND referer NOT LIKE 'http://www.spip-contrib.net%' AND referer NOT LIKE '%localhost%'");
			echo "Import depuis les referer %/ecrire/% : ".sql_count($res)." <br />";
			while ($row = sql_fetch($res))
				univers_proposer_site(preg_replace(',/ecrire/.*$,Uims','/spip.php',$row['referer']));
			ecrire_meta($nom_meta_base_version,$current_version="0.1.5",'non');
		}
		if (version_compare($current_version,"0.1.8",'<')){
			include_spip('base/serial');
			include_spip('base/auxiliaires');
			include_spip('base/create');
			maj_tables(array('spip_websites','spip_websites_plugins'));
			sql_updateq("spip_websites", array('plugins'=>0),"statut='publie'");
			$res = sql_select("id_website, count(plugin) AS nb", "spip_websites_plugins", "", "id_website");
			while($row = sql_fetch($res))
				sql_updateq("spip_websites", array('plugins'=>$row['nb']),'id_website='.intval($row['id_website']));
			ecrire_meta($nom_meta_base_version,$current_version="0.1.8",'non');
		}
		if (version_compare($current_version,"0.1.9",'<')){
			include_spip('base/serial');
			include_spip('base/auxiliaires');
			include_spip('base/create');
			// ajout du champ pays
			maj_tables(array('spip_websites','spip_websites_plugins'));
			include_spip('inc/univers_analyser');
			$c = sql_countsel("spip_websites", "pays='' AND IP<>''");
			echo "MAJ 0.1.9 : $c pays a renseigner<br />";
			$res = sql_select("id_website, ip","spip_websites", "pays='' AND IP<>''");
			while($row = sql_fetch($res)) {
				sql_updateq("spip_websites",
								array('pays'=>univers_geoip($row['ip'])),'id_website='.intval($row['id_website']));
			}
			ecrire_meta($nom_meta_base_version,$current_version="0.1.9",'non');
		}
	}
}

function univers_vider_tables($nom_meta_base_version) {
	effacer_meta($nom_meta_base_version);
	sql_drop_table("spip_websites_key");
}


?>