<?php
/*
 * Plugin Univers SPIP
 * (c) 2010 Cedric
 * Distribue sous licence GPL
 *
 */

function univers_taches_generales_cron($taches_generales){
	$taches_generales['univers_feed'] = 240;
	#$taches_generales['univers_google'] = 77;
	$taches_generales['univers_referers'] = 12*3600;
	$taches_generales['univers_statsv'] = 3*24*3600;
	$taches_generales['univers_boss'] = 173;
	$taches_generales['univers_check'] = 97;
	#$taches_generales['univers_check_1'] = 41;
	#$taches_generales['univers_check_2'] = 37;
	#$taches_generales['univers_check_3'] = 31;
	return $taches_generales;
}

?>
