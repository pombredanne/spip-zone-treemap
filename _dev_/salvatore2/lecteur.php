<?php

/*
    This file is part of Salvatore, the translation robot of Trad-lang (SPIP)

    Salvatore is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Trad-Lang is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Trad-Lang; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Copyright 2003-2012
        Florent Jugla <florent.jugla@eledo.com>,
        Philippe Riviere <fil@rezo.net>,
        Chryjs <chryjs!@!free!.!fr>,
 		kent1 <kent1@arscenic.info>
*/


require_once(dirname(__FILE__).'/inc_tradlang.php');
$tmp=_SALVATORE_TMP;

/* modules de SPIP requis - il y a surement plus propre... */
include_spip('base/abstract_sql');
include_spip('inc/invalideur');
include_spip('inc/tradlang_verifier_langue_base');
include_spip('inc/charsets');
include_spip('inc/filtres');
include_spip('inc/xml');
include_spip('inc/lang_liste');

$url_site = sql_getfetsel('valeur','spip_meta','nom="adresse_site"');

/* MAIN ***********************************************************************/

trad_log("\n=======================================\nLECTEUR\nPrend les fichiers de reference dans sa copie locale et met a jour la base de donnees\n=======================================\n");

$liste_sources=charger_fichier_traductions(); // chargement du fichier traductions.txt

foreach ($liste_sources as $source) {
	trad_log("==== Module ".$source[1]." =======================================\n");
	$liste_fic_lang= glob($tmp.$source[1].'/'.$source[1]."_*.php");
	$import = true;
	/**
	 * On test ici si le fichier est géré par un autre salvatore
	 * Si oui on empeche son export en le signifiant
	 */
	if(file_exists($xml = $tmp.$source[1].'/'.$source[1].'.xml')){
		$xml_content = spip_xml_load($xml);
		if(is_array($xml_content)){
			spip_xml_match_nodes('/^traduction/',$xml_content,$matches);
			$test = '<'.key($matches).'>';
			$url = extraire_attribut($test,'url');
			if($url && ($url != $url_site)){
				$import = false;
				$sujet = 'Lecteur : Erreur sur '.$source[1];
				$corps = "\nErreur : import impossible, le fichier est traduit autre part : $url\n\n";
				trad_sendmail($sujet,$corps);
				trad_log("\nErreur : import impossible, le fichier est traduit autre part : $url\n\n");
			}
		}
	}
	if($import){
		/**
		 * on doit absolument charger la langue principale en premier (a cause des MD5)
		 */ 
		$fic_lang_principal=$tmp.$source[1].'/'.$source[1].'_'.$source[2].".php";
		$priorite = '';
		$modifs = 0;
		if(defined('_TRAD_PRIORITE_DEFAUT'))
			$priorite = _TRAD_PRIORITE_DEFAUT;
		if (in_array($fic_lang_principal,$liste_fic_lang)) {
			$module = sql_fetsel('*','spip_tradlang_modules','module='.sql_quote($source[1]));
			$id_module = $module['id_tradlang_module'];
			/**
			 * Si le module n'existe pas... on le crée
			 */
			if(!intval($id_module)){
				$id_module = sql_insertq('spip_tradlang_modules',
					array('module'=>$source[1],'nom_mod'=>$source[1],'lang_prefix'=>$source[1],'lang_mere'=>$source[2],'priorite' => $priorite)
				);
			}
			/**
			 * Si la langue mere a changée, on la modifie
			 */
			else if($module['lang_mere'] != $source[2]){
				$id_module = $module['id_tradlang_module'];
				sql_updateq('spip_tradlang_modules',array('lang_mere'=>$source[2]),'id_tradlang_module='.intval($id_module));
			}
			/**
			 * Si $id_module n'est pas un entier => on tue le script
			 */
			if(!intval($id_module)){
				$sujet = 'Lecteur : Erreur sur '.$source[1];
				$corps = "Le module n'est pas un entier";
				trad_sendmail($sujet,$corps);
				die("Le module n'est pas un entier");
			}
			$liste_id_orig = array();
			$modifs = import_module_spip($source,$fic_lang_principal,$liste_id_orig,1,$id_module);
			$langues_a_jour = array();
			foreach ($liste_fic_lang as $f){
				if ($f != $fic_lang_principal){
					import_module_spip($source,$f,$liste_id_orig,0,$id_module);
					$fich = basename($f,".php");
					$fich = str_replace($source[1],'',$fich);
					list(,$lang)=explode("_",$fich,2);
					if(($modifs > 0) && function_exists('inc_tradlang_verifier_langue_base_dist')){
						inc_tradlang_verifier_langue_base_dist($source[1],$lang);
						trad_log("|-- Synchro de la langue ".$lang." pour le module ".$source[1]."\n");
					}else if (!function_exists('inc_tradlang_verifier_langue_base_dist')){
						trad_log("|-- Fonction de synchro inexistante\n");
					}
					$langues_a_jour[] = $lang;
				}
			}
			/**
			 * On s'occupe des langues en base sans fichier
			 * s'il y a eu au moins une modif et que l'on peut faire la synchro
			 */
			if(($modifs > 0) && function_exists('inc_tradlang_verifier_langue_base_dist')){
				$langues_pas_a_jour = sql_select('lang','spip_tradlangs','id_tradlang_module='.intval($id_module).' AND '.sql_in('lang',$langues_a_jour,'NOT'),'lang');
				while($langue_a_jour = sql_fetch($langues_pas_a_jour)){
					inc_tradlang_verifier_langue_base_dist($source[1],$langue_a_jour['lang']);
					trad_log("|-- Synchro de la langue non exportée en fichier ".$langue_a_jour['lang']." pour le module ".$source[1]."\n");
				}
			}
			suivre_invalideur('1');
			trad_log("|\n");
			unset($langues_a_jour);
			unset($langues_pas_a_jour);
		}
		else {
			$sujet = 'Lecteur : Erreur sur '.$source[1];
			$corps = "|-- Pas de fichier lang ".$source[2]." pour le module ".$source[1]." : import impossible pour ce module\n";
			trad_sendmail($sujet,$corps);
			die("|-- Pas de fichier lang ".$source[2]." pour le module ".$source[1]." : import impossible pour ce module\n");
		}
	}
}

return 0;

/**
 * Import d'un fichier de langue dans la base
 * 
 * @param array $source
 * @param string $module
 * @param array $liste_id_orig
 * @param int $orig 1 signifie que c'est la langue originale
 * @param int $id_module
 * @return string 
 */
function import_module_spip($source=array(),$module='',&$liste_id_orig,$orig=null,$id_module) {
	trad_log( "!\n+ Import de $module\n");
	$memtrad = $GLOBALS['idx_lang'] = 'i18n_'.crc32($module).'_tmp';
	$GLOBALS[$GLOBALS['idx_lang']] = null;
	$comm_fic_lang = charger_comm_fichier_langue($module);
	include $module;

	$str_lang = $GLOBALS[$memtrad];  // on a vu certains fichiers faire des betises et modifier idx_lang

	if (is_null($str_lang)) {
		trad_log("Erreur, fichier $module mal forme\n");
		$sujet = 'Lecteur : Erreur sur '.$module;
		$corps = "Erreur, fichier $module mal forme\n";
		trad_sendmail($sujet,$corps);
		return false;
	}

	/**
	 * Nettoyer le contenu de ses <MODIF>,<NEW> et <PLUS_UTILISE>
	 * Ces chaines sont utilisées comme statut
	 */ 
	$status = array();

	foreach($str_lang as $id=>$v) {
		if(1==$orig)
			$status[$id] = 'OK';
		else if (preg_match(',^<(MODIF|NEW|PLUS_UTILISE)>,US', $v, $r)) {
			$str_lang[$id] = preg_replace(',^(<(MODIF|NEW|PLUS_UTILISE)>)+,US', '', $v);
			$status[$id] = $r[1];
		}
		else	
			$status[$id] = 'OK';
	}

	$fich = basename($module,".php");
	$fich = str_replace($source[1],'',$fich);
	$mod = $source[1];
	list(,$lang)=explode("_",$fich,2);
	
	if(!array_key_exists($lang,$GLOBALS['codes_langues'])){
		trad_log("!-- Attention : La langue $lang n'existe pas dans les langues possibles - $mod \n");	
	}
	else{
		if (1==$orig)
			$res = spip_query("SELECT id, str, md5 FROM spip_tradlangs WHERE module='".$source[1]."' and lang='".$lang."' ");
		else
			$res = spip_query("SELECT id, str, md5 FROM spip_tradlangs WHERE module='".$source[1]."' and lang='".$lang."' and statut!='MODIF' ");
		$nb = sql_count($res);
		if ($nb > 0)
			trad_log("!-- Fichier de langue $lang du module $mod deja inclus dans la base\n");
	
		$ajoutees = $inchangees = $supprimees = $modifiees = $ignorees = 0;
		
		/**
		 * Si la langue est deja dans la base, on ne l'ecrase que s'il s'agit
		 * de la langue source
		 */
		if ($nb == 0 OR $orig == 1) {
			// La liste de ce qui existe deja
			$existant = array();
			while ($t = spip_fetch_array($res))
				$existant[$t['id']] = $t['md5'];
	
			$bigwhere = "module='".$source[1]."' and lang='".$lang."'";
	
			// Dans ce qui arrive, il y a 4 cas :
			foreach (array_unique(array_merge(
				array_keys($existant), array_keys($str_lang)
			)) as $id) {
				$comm=(isset($comm_fic_lang[$id])) ? $comm_fic_lang[$id] : "";
				// * chaine neuve
				if (isset($str_lang[$id])
				AND !isset($existant[$id]))
				{
					if ($orig) {
						$md5 = md5($str_lang[$id]);
					} else if (!isset($liste_id_orig[$id])) {
						trad_log("!-- Chaine $id inconnue dans la langue principale\n");
						$ignorees++;
					} else {
						$md5 = $liste_id_orig[$id];
					}
	
					if (isset($md5)){
						/*
						 * Forcer le passage en UTF-8
						 */
						$str_lang[$id] = str_replace("\r\n", "\n", unicode_to_utf_8(
							html_entity_decode(
								preg_replace('/&([lg]t;)/S', '&amp;\1', $str_lang[$id]),
								ENT_NOQUOTES, 'utf-8')
						));
						if(in_array($comm,array('NEW','OK','MODIF','MODI')) && $orig != 1){
							if($comm == 'MODI')
								$comm = 'MODIF';
							$status[$id] = $comm;
							$comm = '';
						}
						else if ((strlen($comm) > 1) && preg_match("/(.*?)(NEW|OK|MODIF)(.*?)/",$comm,$matches)) {
							if($orig != 1)
								$status[$id] = $matches[2];
							$comm = preg_replace("/(NEW|OK|MODIF)/",'',$comm);
						}
						$titre = $id.' : '.$source[1].' - '.$lang;
						spip_query("INSERT spip_tradlangs (id_tradlang_module,titre,module,lang,id,str,comm,md5,statut)
						VALUES ("
							._q($id_module).', '
							._q($titre).', '
							._q($source[1]).', '
							._q($lang).', '
							._q($id).', '
							._q($str_lang[$id]).', '
							._q($comm).', '
							._q($md5).', '
							._q($status[$id])
							.")"
						);
						$ajoutees++;
					}
				}
				else
				// * chaine existante
				if (isset($str_lang[$id]) AND isset($existant[$id]))
				{
					// * identique ? => NOOP
					$md5 = md5($str_lang[$id]);
					if ($md5 == $existant[$id]) {
						$inchangees++;
					}
					// * modifiee ? => UPDATE
					else {
						// modifier la chaine
						spip_query("UPDATE spip_tradlangs SET
							str="._q($str_lang[$id])
							. ($orig
								? ", md5="._q($md5).", statut='OK'"
								: ", md5="._q($existant[$id]).", statut=''"
							)
							. ($comm
								? ", comm="._q($comm)." "
								: ""
							)
							." WHERE $bigwhere AND id="._q($id)
						);
	
						// signaler le status MODIF de ses traductions
						if ($orig && ($orig != 0))
							spip_query("UPDATE spip_tradlangs SET statut='MODIF'
								WHERE module='".$source[1]
								."' AND id="._q($id)
								." AND md5!="._q($md5)
								." AND lang!="._q($lang)
								." AND statut!='NEW'"
							);
						$modifiees++;
					}
				}
				else
				// * chaine supprimee
				if (!isset($str_lang[$id]) AND isset($existant[$id])){
					// mettre au grenier
					spip_query("UPDATE spip_tradlangs SET id="._q($source[1].'_'.$id)
					.", module='attic' WHERE id="._q($id)." AND module="._q($source[1]));
					$supprimees++;
				}
				
				if ($orig AND isset($str_lang[$id]))
					$liste_id_orig[$id] = md5($str_lang[$id]);
			}
	
			trad_log("!-- module ".$source[1].", $lang : $modifiees modifiees, $ajoutees ajoutees, $supprimees supprimees, $ignorees ignorees, $inchangees inchangees\n");
		}
	}
	unset($liste_id_orig);
	unset($str_lang);
	unset($GLOBALS[$GLOBALS['idx_lang']]);
	return $ajoutees + $supprimees + $modifiees;
}

/**
 * Chargement des commentaires de fichier de langue
 * Le fichier est chargé en mode texte pour récupérer les commentaires dans lesquels sont situés les statuts
 * 
 * @param string $f Le chemin du fichier de langue
 * @return array $liste_trad Un tableau id/chaine
 */
function charger_comm_fichier_langue($f) {

	$contenu=file_get_contents($f);

	$tab=preg_split("/\r\n|\n\r|;\n|\n\/\/|\(\n|\n\);\n|\'\,\n|\n[\s\t]*(\')|\/\/[\s\t][0-9A-Z]\n[\s\t](\')/", $contenu,'-1',PREG_SPLIT_NO_EMPTY);

	$liste_trad=array();
	reset($tab);
	
	while (list(,$ligne) = each($tab))
	{
		$ligne = str_replace("\'",'',$ligne);
		if (strlen($ligne)>0) {
			if (preg_match("/(.*?)\'[\s\t]*=>[\s\t]*\'(.*?)\'[\s\t]*,{0,1}[\s\t]*(#.*)?/ms",$ligne,$matches)) {
				if (isset($matches[1]) and isset($matches[3]) and strlen(trim($matches[3]))>0 ) {
					list(,$comm)=explode("#",$matches[3]);
					$liste_trad[$matches[1]]=trim($comm);
				}
			}
		}
	}
	reset($liste_trad);
	return $liste_trad;
}

?>