<?php

/*
    This file is part of Salvatore, the translation robot of Trad-lang (SPIP)

    Salvatore is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Trad-Lang is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Trad-Lang; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Copyright 2003-2012
        Florent Jugla <florent.jugla@eledo.com>,
        Philippe Riviere <fil@rezo.net>,
        Chryjs <chryjs!@!free!.!fr>,
 		kent1 <kent1@arscenic.info>
*/


/**
 * Ce script va chercher les fichiers définis dans 
 * 
 */
require_once(dirname(__FILE__).'/inc_tradlang.php');
$tmp=_SALVATORE_TMP;

// pas de nettoyage, on va essayer de gerer les conflits intelligemment
// trad_log(exec("rm -rf ".$tmp."*"));


trad_log("\n=======================================\nTIREUR\nVa chercher les fichiers dans SVN et les depose dans sa copie locale\n=======================================\n");

$liste_sources=charger_fichier_traductions(); // chargement du fichier traductions.txt

$ret=0;
foreach($liste_sources as $source) {
	trad_log("\n===== Module ".$source[1]." =====\n");
	if (is_dir($tmp.$source[1].'/.svn')){
		$depot = "env LANG=en svn info --non-interactive ".$tmp.$source[1]." | awk '/^URL:/ { print $2 }'";
		$depot = exec($depot,$depot);
		$depot = $depot.'/';
		if($depot != $source[0]){
			$cmd = "svn switch ".$source[0]." ".$tmp.$source[1]."/";
		}else{
			$cmd = "svn update  --non-recursive ".$tmp.$source[1]."/";
		}
	}else
		$cmd = "svn checkout --non-recursive ". $source[0]."/ ".$tmp.$source[1].'/';
	trad_log("$cmd\n");
	exec("$cmd 2> /dev/null",$out,$int);
	if($int == 0){
		trad_log(end($out)."\n");
	}else{
		$sujet = 'Tireur : Erreur';
		$corps = "$cmd\n\n";
		$corps .= "L'adresse distante de ce module n'est certainement plus valide\n\n";
		trad_sendmail($sujet,$corps);
		die("L'adresse distante de ce module n'est certainement plus valide\n\n");
	}
	// controle des erreurs : requiert au moins 1 fichier par module !
	if (!file_exists($tmp.$source[1].'/'.$source[1].'_'.$source[2].".php")) {
		$ret=1;
		$sujet = 'Tireur : Erreur';
		$corps = "! Erreur pas de fichier de langue conforme dans le module : $tmp".$source[1]."\n";
		trad_sendmail($sujet,$corps);
		die("! Erreur pas de fichier de langue conforme dans le module : $tmp".$source[1]."\n");
	}
}

return $ret;

?>