<?php

/*
    This file is part of Salvatore, the translation robot of Trad-lang (SPIP)

    Salvatore is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Trad-Lang is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Trad-Lang; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Copyright 2003-2012
        Florent Jugla <florent.jugla@eledo.com>,
        Philippe Riviere <fil@rezo.net>,
        Chryjs <chryjs!@!free!.!fr>,
 		kent1 <kent1@arscenic.info>
*/

require_once(dirname(__FILE__).'/inc_tradlang.php');
$tmp= _SALVATORE_TMP;

trad_log("\n=======================================\nECRIVEUR\nExporte les fichiers de traduction dans sa copie locale a partir de la base de donnees\n=======================================\n");

$liste_sources=charger_fichier_traductions(); // chargement du fichier traductions.txt

if (!is_dir($tmp)) die ("Manque le repertoire ".$tmp);

include_spip('base/abstract_sql');
include_spip('inc/filtres');
include_spip('inc/xml');

/**
 * On récupère l'URL du site de traduction
 * Elle servira à :
 * -* empêcher l'export de fichiers traduits sur une autre plateforme
 * -* générer l'url de l'interface de traduction d'un module
 */
$url_site = sql_getfetsel('valeur','spip_meta','nom="adresse_site"');

foreach ($liste_sources as $source) {
	trad_log("==== Module ".$source[1]." =======================================\n");
	$export = true;
	/**
	 * On test ici si le fichier est géré par un autre salvatore
	 * Si oui on empeche son export en le signifiant
	 */
	if(file_exists($xml = $tmp.$source[1].'/'.$source[1].'.xml')){
		$xml_content = spip_xml_load($xml);
		if(is_array($xml_content)){
			spip_xml_match_nodes('/^traduction/',$xml_content,$matches);
			$test = '<'.key($matches).'>';
			$url = extraire_attribut($test,'url');
			if($url && ($url != $url_site)){
				$export = false;
				$sujet = 'Ecriveur : Erreur sur '.$source[1];
				$corps = "\nErreur : export impossible, le fichier est traduit autre part : $url != $url_site\n\n";
				trad_sendmail($sujet,$corps);
				trad_log("\nErreur : export impossible, le fichier est traduit autre part : $url != $url_site\n\n");		
			}
		}
	}
	/**
	 * Si on l'exporte
	 */
	if($export){
		$id_tradlang_module = sql_getfetsel('id_tradlang_module','spip_tradlang_modules','module='.sql_quote($source[1]));
		$url_trad = url_absolue(generer_url_entite($id_tradlang_module,'tradlang_module'),$url_site);
		export_trad_module($source,$url_site,$url_trad);
	}
}

return 0;

//
// Genere les fichiers de traduction d'un module
//
function export_trad_module($source,$url_site,$url_trad) {
	global $tmp;

	// sanity check
	if (!is_dir($tmp.$source[1]."/"))
		return false;

	// charger la langue originale, pour la copier si necessaire
	$res=spip_query("SELECT id,id_tradlang_module,str,comm,statut FROM spip_tradlangs WHERE module='".$source[1]."' and lang='".$source[2]."' GROUP BY id");
	$count_original = 0;
	while ($row=spip_mysql_fetch($res)) {
		$row['statut'] = 'NEW';
		$lorigine[$row['id']] = $row;
		$id_tradlang_module = $row['id_tradlang_module'];
		$count_original++;
	}
	
	$res=spip_query("SELECT lang,COUNT(*) as N FROM spip_tradlangs WHERE module='".$source[1]."' AND statut IN ('OK','MODIF') GROUP BY lang ORDER BY lang");
	$liste_lang=array();
	$minimal = $count_original/2;
	while ($row=spip_fetch_array($res)) {
		if($row['N'] >= $minimal){
			$liste_lang[]=$row['lang'];
		}else{
			$liste_lang_non_exportees[]=$row['lang'];
		}
	}

	// traiter chaque langue
	$traducteurs = array();
	foreach($liste_lang as $lang) {
		trad_log(" genere la langue $lang ");

		$tab = "\t";

		$res=spip_query("SELECT id,str,comm,statut FROM spip_tradlangs WHERE module='".$source[1]."' and lang='".$lang."' GROUP BY id");
		$x=array();
		$prev="";
		$traduits = 0;
		$tous = $lorigine; // on part de l'origine comme ca on a tout meme si c'est pas dans la base de donnees (import de salvatore/lecteur.php)
		while ($row=spip_mysql_fetch($res)) {
			$tous[$row['id']] = $row;
		}
		ksort($tous);
		foreach ($tous as $row) {
			if(strlen($row['comm']) > 1){
				// On remplace les sauts de lignes des commentaires sinon ça crée des erreurs php
				$row['comm'] = str_replace(array("\r\n", "\n", "\r"),' ', $row['comm']);
				// Conversion des commentaires en utf-8 
				$row['comm'] = unicode_to_utf_8(
					html_entity_decode(
						preg_replace('/&([lg]t;)/S', '&amp;\1', $row['comm']),
						ENT_NOQUOTES, 'utf-8')
				);
			}
			
			trad_log($row['comm']);
			if ($prev!=strtoupper($row['id'][0])) $x[] = "\n$tab// ".strtoupper($row['id'][0]);
			$prev=strtoupper($row['id'][0]);
			if ($row['statut'] != 'NEW')
				$traduits ++;
			if (strlen($row['statut']) && ($row['statut'] != 'OK'))
				$row['comm'] .= ' '.$row['statut'];
			if (trim($row['comm'])) $row['comm']=" # ".trim($row['comm']); // on rajoute les commentaires ?

			$str = $row['str'];

			$oldmd5 = md5($str);
			//$str = unicode_to_utf_8(html_entity_decode($str, ENT_NOQUOTES, 'utf-8'));
			$str = unicode_to_utf_8(
				html_entity_decode(
					preg_replace('/&([lg]t;)/S', '&amp;\1', $str),
					ENT_NOQUOTES, 'utf-8')
			);
			
			$newmd5 = md5($str);
			if ($oldmd5 !== $newmd5) spip_query("UPDATE spip_tradlangs SET md5='$newmd5' WHERE md5='$oldmd5' AND module='$source[1]'");

			$x[]="$tab".var_export($row['id'],1).' => ' .var_export($str,1).','.$row['comm'];
		}
		$orig = ($lang == $source[2]) ? $source[0] : false;

		trad_log(" - traduction ($traduits/$count_original), export\n");
		// historiquement les fichiers de lang de spip_loader ne peuvent pas etre securises
		$secure = ($source[1] == 'tradloader')
			? ''
			: "if (!defined('_ECRIRE_INC_VERSION')) return;\n\n";

		$fd = fopen($tmp.$source[1]."/".$source[1].'_'.$lang.'.php', 'w');

		# supprimer la virgule du dernier item
		$x[count($x)-1] = preg_replace('/,([^,]*)$/', '\1', $x[count($x)-1]);

		$contenu = join("\n",$x);

		// L'URL du site de traduction
		$url_trad = parametre_url($url_trad,'lang_cible',$lang);
		# ecrire le fichier
		fwrite($fd,
		'<'.'?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
'

. ($orig
	? '// Fichier source, a modifier dans '.$orig
: '// extrait automatiquement de '.$url_trad.'
// ** ne pas modifier le fichier **
'
)
."\n".$secure.'$GLOBALS[$GLOBALS[\'idx_lang\']] = array(
'
. str_replace("\r\n", "\n", $contenu)
.'
);

?'.'>
'
		);
		fclose($fd);
		
		// noter la langue et les traducteurs pour lang/module.xml
		$traducteurs[$lang] = array();
		$people_unique = array();
		$s = spip_query('SELECT DISTINCT(traducteur) FROM spip_tradlangs WHERE module='._q($source[1])." and lang="._q($lang));
		while ($t = sql_fetch($s)){
			$traducteurs_lang = explode(',',$t['traducteur']);
			foreach($traducteurs_lang as $traducteur){
				if(!in_array($traducteur,$people_unique)){
					if(is_numeric($traducteur) AND $id_auteur=intval($traducteur)){
						$traducteur_supp['nom'] = extraire_multi(sql_getfetsel('nom','spip_auteurs','id_auteur='.$id_auteur));
						$traducteur_supp['lien'] = url_absolue(generer_url_entite($id_auteur,'auteur'),$url_site);
					}else if(trim(strlen($traducteur)) > 0){
						$traducteur_supp['nom'] = trim($traducteur);
						$traducteur_supp['lien'] = '';
					}
					if(isset($traducteur_supp['nom']))
						$traducteurs[$lang][] = $traducteur_supp;
					unset($traducteur_supp);
					$people_unique[] = $traducteur;
				}
			}
		}
		unset($people_unique);
	}

	// ecrire lang/module.xml
	$xml = "<traduction module=\"$source[1]\" gestionnaire=\"salvatore\" url=\"$url_site\" source=\"$source[0]\" reference=\"$source[2]\">\n";
	foreach($traducteurs as $lang => $peoples) {
		if ($peoples) {
			$xml .= "	<langue code=\"$lang\" url=\"".parametre_url($url_trad,'lang_cible',$lang)."\">\n";
			foreach ($peoples as $people) {
				$xml .= "		<traducteur nom=\"".entites_html($people['nom'])."\" lien=\"".entites_html($people['lien'])."\" />\n";
			}
			$xml .= "	</langue>\n";
		} else
			$xml .= "	<langue code=\"$lang\" />\n";
	}
	unset($traducteurs[$source[2]]);
	$xml .= "</traduction>\n";

	ecrire_fichier($tmp.$source[1]."/".$source[1].'.xml', $xml);
	
	if(isset($liste_lang_non_exportees) && (count($liste_lang_non_exportees) > 0)){
		$liste_lang_non_exportees_string = implode(', ',$liste_lang_non_exportees);
		trad_log("\nLes langues suivantes ne sont pas exportées car trop peu traduites:\n");
		trad_log("$liste_lang_non_exportees_string\n");
	}
	if(!in_array($source[1],array('spip','ecrire','public'))){
		foreach($liste_lang as $lang){
			passthru("svn add "._SALVATORE_TMP.$source[1]."/".$source[1]."_$lang.php* 2> /dev/null") ? trad_log("$log\n") : '';
		}
	}
	trad_log("\n".passthru("svn status "._SALVATORE_TMP.$source[1]."/")."\n");
}

?>
