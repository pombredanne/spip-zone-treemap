<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2009                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;

define('_DEFAULT_DB', '(DESCRIPTION =  (ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1521))  (CONNECT_DATA =   (SERVER = DEDICATED)  (SERVICE_NAME = orcl)  ) )');
#define('_DEFAULT_DB', 'orcl');
define('_DEDAULT_OCI_SESSION_MODE', OCI_DEFAULT);

// Se connecte et retourne le nom de la fonction a connexion persistante
// avec les valeurs par defaut ci-dessus

function req_oci_dist($addr, $port, $login, $pass, $db='', $prefixe='') {
	static $last_connect = array();
	
	/*	// si provient de selectdb
	if (empty($addr) && empty($port) && empty($login) && empty($pass)){
		foreach (array('addr','port','login','pass','prefixe') as $a){
			$$a = $last_connect[$a];
		}
		} */
	@list($host, $p) = split(':', $addr);
	if ($p >0) $port = " port=$p" ; else $port = '';
	$tnsnames = _DEFAULT_DB;
# $tnsnames = preg_replace('/[(]\s*SERVICE_NAME\s*=\s*[^)]*[)]/', "(SERVICE_NAME $db)",  _DEFAULT_DB);      
	$link = oci_connect($login, $pass, $tnsnames, NULL, _DEDAULT_OCI_SESSION_MODE);
	if ($link) {
	  // A revoir
#		oci_execute(oci_parse($link, "variable LAST_INSERT NUMBER;"));
#		oci_execute(oci_parse($link, "BEGIN LAST_INSERT := 0; END;"));
		$last_connect = array (
			'addr' => $addr,
			'port' => $port,
			'login' => $login,
			'pass' => $pass,
			'db' => '', // a affiner
			'prefixe' => $prefixe,
		);
	}
	spip_log("Connexion par oci_connect vers $login $pass $host, base $db, prefixe $prefixe "
		 . ($link ? "operationnelle ($link)" : 'impossible'));

	return !$link ? false : array(
		'db' => '',
		'prefixe' => $prefixe ? $prefixe : 'spip',
		'link' => $link,
		);
}

$GLOBALS['spip_oci_functions_1'] = array(
		'alter' => 'spip_oci_alter',
		'count' => 'spip_oci_count',
		'countsel' => 'spip_oci_countsel',
		'create' => 'spip_oci_create',
		'create_base' => 'spip_oci_create_base',
		'create_view' => 'spip_oci_create_view',
		'date_proche' => 'spip_oci_date_proche',
		'delete' => 'spip_oci_delete',
		'drop_table' => 'spip_oci_drop_table',
		'drop_view' => 'spip_oci_drop_view',
		'errno' => 'spip_oci_errno',
		'error' => 'spip_oci_error',
		'explain' => 'spip_oci_explain',
		'fetch' => 'spip_oci_fetch',
		'free' => 'spip_oci_free',
		'hex' => 'spip_oci_hex',
		'in' => 'spip_oci_in',
		'insert' => 'spip_oci_insert',
		'insertq' => 'spip_oci_insertq',
		'insertq_multi' => 'spip_oci_insertq_multi',
		'listdbs' => 'spip_oci_listdbs',
		'multi' => 'spip_oci_multi',
		'query' => 'spip_oci_query',
		'quote' => 'spip_oci_quote',
		'replace' => 'spip_oci_replace',
		'replace_multi' => 'spip_oci_replace_multi',
		'select' => 'spip_oci_select',
		'selectdb' => 'spip_oci_selectdb',
		'set_connect_charset' => 'spip_oci_set_connect_charset',
		'showbase' => 'spip_oci_showbase',
		'showtable' => 'spip_oci_showtable',
		'update' => 'spip_oci_update',
		'updateq' => 'spip_oci_updateq',
		);

// Par ou ca passe une fois les traductions faites
function spip_oci_trace_query($query, $serveur='')
{
	$connexion = &$GLOBALS['connexions'][$serveur ? $serveur : 0];
	$prefixe = $connexion['prefixe'];
	$link = $connexion['link'];

	if (isset($_GET['var_profile'])) {
		include_spip('public/tracer');
		$t = trace_query_start();
	} else $t = 0 ;

	$connexion['last'] = $query;
	// pas utilise pour l'instant
	$connexion['resource'] = $r = oci_parse($link, $query);
	spip_log("serveur $serveur link: $link exec: $r query: $query");
	if ($r) { $r2 = oci_execute($r); if (!$r2) $r = false;}
	spip_log("execute $r ". ($r2 ? '' : spip_oci_error($serveur)));
	return $t ? trace_query_end($query, $t, $r, $serveur) : $r;
}

// Fonction de requete generale quand on est sur que c'est SQL standard.
// Elle change juste le noms des tables ($table_prefix) dans le FROM etc

function spip_oci_query($query, $serveur='',$requeter=true)
{
	$connexion = $GLOBALS['connexions'][$serveur ? $serveur : 0];
	$prefixe = $connexion['prefixe'];

	if (preg_match('/\s(SET|VALUES|WHERE|DATABASE)\s/i', $query, $regs)) {
		$suite = strstr($query, $regs[0]);
		$query = substr($query, 0, -strlen($suite));
	} else $suite ='';
	$query = preg_replace('/([,\s])spip_/', '\1'.$prefixe.'_', $query) . $suite;
	
	// renvoyer la requete inerte si demandee
	if (!$requeter) return $query;

	return spip_oci_trace_query($query, $serveur);
}

function spip_oci_reserved($v)
{
	if (strpos($v, 'date')===false AND strpos($v, 'mode')===false) return $v;
	$p = preg_match('/^\s*[(](.*)[)]\s*$/s', $v, $r);
	if ($p) $v = $r[1];
	$v = preg_split('/\s*,\s*/', $v);
	foreach($v as $i=>$k) 
	  if ($k === 'date' OR $k === 'mode') $v[$i] = ('"' . $k . '"');
	$v = implode(',',$v);
	if ($p) $v = "($v)";
	return $v;
}

// Alter en PG ne traite pas les index
function spip_oci_alter($query, $serveur='',$requeter=true) {

	if (!preg_match('/^\s*(IGNORE\s*)?TABLE\s+(\w+)\s+(ADD|DROP|CHANGE)\s*([^,]*)(.*)$/is', $query, $r)) {
	  spip_log("$query incompris", 'oci');
	} else {
	  if ($r[1]) spip_log("j'ignore IGNORE dans $query", 'oci');
	  $f = 'spip_oci_alter_' . strtolower($r[3]);
	  if (function_exists($f))
	    $f($r[2], $r[4], $serveur);
	  else spip_log("$query non prevu", 'oci');
	}
	// Alter a plusieurs args. Faudrait optimiser.
	if ($r[5])
	  spip_oci_alter("TABLE " . $r[2] . substr($r[5],1));
}
	      
function spip_oci_alter_change($table, $arg, $serveur='',$requeter=true)
{
	if (!preg_match('/^`?(\w+)`?\s+`?(\w+)`?\s+(.*?)\s*(DEFAULT .*?)?(NOT\s+NULL)?\s*(DEFAULT .*?)?$/i',$arg, $r)) {
	  spip_log("alter change: $arg  incompris", 'oci');
	} else {
	  list(,$old, $new, $type, $default, $null, $def2) = $r;
	  $actions = array("ALTER $old TYPE " . mysql2oci_type($type));
	  if ($null)
	    $actions[]= "ALTER $old SET NOT NULL";
	  else
	    $actions[]= "ALTER $old DROP NOT NULL";

	  if ($d = ($default ? $default : $def2))
	    $actions[]= "ALTER $old SET $d";
	  else
	    $actions[]= "ALTER $old DROP DEFAULT";

	  spip_oci_query("ALTER TABLE $table " . join(', ', $actions));

	  if ($old != $new)
	    spip_oci_query("ALTER TABLE $table RENAME $old TO $new", $serveur);
	}
}

function spip_oci_alter_add($table, $arg, $serveur='',$requeter=true) {
	if (!preg_match('/^(INDEX|KEY|PRIMARY\s+KEY|)\s*(.*)$/', $arg, $r)) {
		spip_log("alter add $arg  incompris", 'oci');
		return NULL;
	}
	if (!$r[1]) {
		preg_match('/`?(\w+)`?(.*)/',$r[2], $m);
		return spip_oci_query("ALTER TABLE $table ADD " . $m[1] . ' ' . mysql2oci_type($m[2]),  $serveur);
	} elseif ($r[1][0] == 'P') {
		preg_match('/\(`?(\w+)`?\)/',$r[2], $m);
		return spip_oci_query("ALTER TABLE $table ADD CONSTRAINT $table" .'_pkey PRIMARY KEY (' . $m[1] . ')', $serveur);
	} else {
		preg_match('/`?(\w+)`?\s*(\([^)]*\))/',$r[2], $m);
		return spip_oci_query("CREATE INDEX " . $table . '_' . $m[1] . " ON $table" . str_replace("`","",$m[2]),  $serveur);
	}
}

function spip_oci_alter_drop($table, $arg, $serveur='',$requeter=true) {
	if (!preg_match('/^(INDEX|KEY|PRIMARY\s+KEY|)\s*`?(\w*)`?/', $arg, $r))
	  spip_log("alter drop: $arg  incompris", 'oci');
	else {
	    if (!$r[1])
	      return spip_oci_query("ALTER TABLE $table DROP " . $r[2],  $serveur);
	    elseif ($r[1][0] == 'P')
	      return spip_oci_query("ALTER TABLE $table DROP CONSTRAINT $table" . '_pkey', $serveur);
	    else {
		return spip_oci_query("DROP INDEX " . $table . '_' . $r[2],  $serveur);
	    }
	}
}

function spip_oci_explain($query, $serveur='',$requeter=true){
	if (strpos(ltrim($query), 'SELECT') !== 0) return array();
	$connexion = $GLOBALS['connexions'][$serveur ? $serveur : 0];
	$prefixe = $connexion['prefixe'];
	$link = $connexion['link'];
	if (preg_match('/\s(SET|VALUES|WHERE)\s/i', $query, $regs)) {
		$suite = strstr($query, $regs[0]);
		$query = substr($query, 0, -strlen($suite));
	} else $suite ='';
	$query = 'EXPLAIN PLAN FOR ' . preg_replace('/([,\s])spip_/', '\1'.$prefixe.'_', $query) . $suite;

	$r = oci_execute($r2 = oci_parse($link,$query));
	return $r ? spip_oci_fetch($r2, NULL, $serveur) : false;
}

// FIXME: Orace n'aurait pas de notion de BD ?

function spip_oci_selectdb($db, $serveur='',$requeter=true) {
	// se connecter a la base indiquee
	// avec les identifiants connus
	$index = $serveur ? $serveur : 0;
	$db = '';
	if ($link = spip_connect_db('', '', '', '', $db, 'oci', '', '')){
		if (($db==$link['db']) && $GLOBALS['connexions'][$index] = $link)
			return $db;					
	} else
		return false;
}

// A revoir

function spip_oci_listdbs($serveur='') {

  $r = sql_allfetsel('TABLESPACE_NAME', 'user_tablespaces', '','','','','', $serveur);
  if (!$r) return false;
  return array_map('array_shift', $r);
}

function spip_oci_select($select, $from_as, $where='',
			$groupby=array(), $orderby='', $limit='',
                           $having='', $serveur='',$requeter=true){

	$connexion = $GLOBALS['connexions'][$serveur ? $serveur : 0];
	$prefixe = $connexion['prefixe'];
	$link = $connexion['link'];

	$limit = preg_match("/^\s*(([0-9]+),)?\s*([0-9]+)\s*$/", $limit,$limatch);
	if ($limit) {
		$offset = intval($limatch[2]);
		$count = $limatch[3] + $offset;
	}

	$from = preg_replace('/\s+AS\s+(\w+)/i', ' \1', $from_as);
	$from =  spip_oci_from($from, $prefixe);
	$select = ltrim(spip_oci_frommysql($select));
	$orderby = spip_oci_orderby($orderby, $select, $from_as);
	if ($orderby) $orderby = ("\nORDER BY $orderby");
	if ($select[0] === '*') $select = $from . "." . $select;
	$groupby = spip_oci_groupby($groupby, $from_as, $select, $prefixe);

	if ($having) {
	  if (is_array($having)) {
	    $having = join("\n\tAND ", array_map('calculer_oci_where', $having));
	  }
	  $having = "\nHAVING $having";
	} else $having ='';
	if ($where)
	  $where = "\nWHERE (" . (!is_array($where) ? calculer_oci_where($where) : (join("\n\tAND ", array_map('calculer_oci_where', $where)))) .')';
	else $where = '';
	if ($groupby AND !preg_match('/^[\w ]+$/', $from)) {
	  $where = "\nWHERE ($groupby IN (SELECT $groupby FROM $from$where$having\nGROUP BY $groupby))";
	  $groupby = $having = '';
	}  
	$query =  (!$from ? '' : "\nFROM $from")
	. $where 
	. ($groupby ? "\nGROUP BY $groupby" : '')
	. $orderby
	. $having;

	if ($limit) {
	  $limit = $offset ? "BETWEEN $offset AND $count" : "<= $count";
	  $query = "SELECT * FROM (SELECT ROWNUM AS R, $select $query) WHERE R $limit";
	} else $query = "SELECT $select $query";
	// renvoyer la requete inerte si demandee
	if (!$requeter) return $query;
	
	$r = spip_oci_trace_query($query, $serveur);
	return $r ? $r : $query;
}

// traitement des prefixes de table 

function spip_oci_from($clause, $prefixe)
{
	if (is_array($clause)) $clause = spip_oci_select_as($clause);
	return !$prefixe ? $clause : preg_replace('/(\b)spip_/','\1'.$prefixe.'_', $clause);
}

function spip_oci_orderby($order, $select, $from)
{
	$res = array();
	$arg = (is_array($order) ?  $order : preg_split('/\s*,\s*/',$order));
	if (!$arg) return '';
	preg_match_all('/(\w+)\s+AS\s+(\w+)/s', $select, $r, PREG_SET_ORDER);
	$as = array();
	foreach($r as $m) $as[$m[2]] = $m[1];
	foreach($arg as $v) {
		if (preg_match('/(case\s+.*?else\s+0\s+end)\s*AS\s+' . $v .'/', $select, $m)) {

		  $v = $m[1];
		}
		$v = preg_replace('/[^,]*\s+AS\s+"?(\w+)"?\s*/i',' \1', $v);
		foreach($as as $k => $a) 
		  $v = preg_replace('/' . $k . '[.]/', $a . '.', $v);
		$res[]=$v;
	}
	return spip_oci_frommysql(join(',',$res));
}

// Le groupby de Oracle ignore les alias du select, qu'il faut donc recopier
// On espere qu'il saura optimiser cette duplication.

function spip_oci_groupby($groupby, $from, $select, $prefixe)
{
	if (!is_array($groupby)) $groupby = explode(',',$groupby);
	preg_match_all('/(\w+)\s+AS\s+(\w+)/s', $select, $r, PREG_SET_ORDER);
	foreach($r as $m) {
	  if (($k=array_search($m[2], $groupby)) !== false)
	    $groupby[$k]= $m[1];
	}

	$groupby = join(',',$groupby);
	if (!$groupby) return '';

	$groupby = spip_oci_frommysql($groupby);
	$groupby = preg_replace('/\b(.*?)\s+AS\s+"?(\w+)"?\s*/s',' \1', $groupby);
	return $groupby;
}

// Conversion des operateurs MySQL en PG
// IMPORTANT: "0+X" est vu comme conversion numerique du debut de X 
// Les expressions de date ne sont pas gerees au-dela de 3 ()
// A ameliorer.

function spip_oci_frommysql($arg)
{
	if (!is_array($arg)) 
		return spip_oci_1frommysql($arg);
	else return join(", ", array_map('spip_oci_1frommysql', $arg));
}

function spip_oci_1frommysql($arg)
{
	$res = spip_oci_fromfield($arg);

	$res = preg_replace('/([(]\w+[<>].*?[)]\s*)AS\s+/',
			    '(case when \1 then 1 else 0 end) AS',
			    $res);

	$res = preg_replace('/\brand[(][)]/i','random()', $res);

	$res = preg_replace('/NOW\s*[(]\s*[)]/i', 'sysdate', $res);

	$res = preg_replace('/\b0\.0[+]([a-zA-Z0-9_.]+)\s*/',
			    'to_number(regexp_substr(\1, \'^ *[0-9.]+\'))',
			    $res);

	$res = preg_replace('/\b0[+]([a-zA-Z0-9_.]+)\s*/',
			    'to_number(regexp_substr(\1, \'^ *[0-9]+\'))',
			    $res);

	$res = preg_replace('/\bconv[(]([^,]*)[^)]*[)]/i',
			    'to_number(regexp_substr(\1, \'^ *[0-9]+\'))',
			    $res);

	$res = preg_replace('/UNIX_TIMESTAMP\s*[(]\s*[)]/',
			    ' EXTRACT(epoch FROM NOW())', $res);

	$res = preg_replace('/UNIX_TIMESTAMP\s*[(]([^)]*)[)]/',
			    ' EXTRACT(epoch FROM \1)', $res);


	$res = preg_replace('/\bDAYOFMONTH\s*[(]([^()]*([(][^()]*[)][^()]*)*[^)]*)[)]/',
			    ' EXTRACT(day FROM \1)',
			    $res);

	$res = preg_replace('/\bMONTH\s*[(]([^()]*([(][^)]*[)][^()]*)*[^)]*)[)]/',
			    ' EXTRACT(month FROM \1)',
			    $res);

	$res = preg_replace('/\bYEAR\s*[(]([^()]*([(][^)]*[)][^()]*)*[^)]*)[)]/',
			    ' EXTRACT(year FROM \1)',
			    $res);

	$res = preg_replace('/TO_DAYS\s*[(]([^()]*([(][^)]*[)][()]*)*)[)]/',
			    ' EXTRACT(day FROM \1 - \'0001-01-01\')',
			    $res);

	$res = preg_replace("/(EXTRACT[(][^ ]* FROM *)\"([^\"]*)\"/", '\1\'\2\'', $res);
	$res = preg_replace('/DATE_FORMAT\s*[(]([^,]*),\s*\'%Y%m%d\'[)]/', 'to_number(to_char(\1, \'YYYYMMDD\'), \'8\')', $res);
	$res = preg_replace('/DATE_FORMAT\s*[(]([^,]*),\s*\'%Y%m\'[)]/', 'to_number(to_char(\1, \'YYYYMM\'),\'6\')', $res);
	$res = preg_replace('/DATE_SUB\s*[(]([^,]*),/', '(\1 -', $res);
	$res = preg_replace('/DATE_ADD\s*[(]([^,]*),/', '(\1 +', $res);
	$res = preg_replace('/INTERVAL\s+(\d+)/', 'INTERVAL \'\1\'', $res);
#	$res = preg_replace('/INTERVAL\s+(\d+)\s+DAY/', ' \1', $res);
#	$res = preg_replace('/INTERVAL\s+(\d+)\s+HOUR/', ' \1/24', $res);
#	$res = preg_replace('/INTERVAL\s+(\d+)\s+MINUTE/', ' \1/(24*60)', $res);
	// Manque MONTH
#	$res = preg_replace('/([+<>-]=?)\s*(\'\d+-\d+-\d+\s+\d+:\d+(:\d+)\')/', '\1 timestamp \2', $res);
#	$res = preg_replace('/(\'\d+-\d+-\d+\s+\d+:\d+:\d+\')\s*([+<>-]=?)/', 'timestamp \1 \2', $res);

#	$res = preg_replace('/([+<>-]=?)\s*(\'\d+-\d+-\d+\')/', '\1 timestamp \2', $res);
#	$res = preg_replace('/(\'\d+-\d+-\d+\')\s*([+<>-]=?)/', 'timestamp \1 \2', $res);

#	$res = preg_replace('/(timestamp .\d+)-00-/','\1-01-', $res);
#	$res = preg_replace('/(timestamp .\d+-\d+)-00/','\1-01',$res);
# correct en theorie mais produit des debordements arithmetiques
#	$res = preg_replace("/(EXTRACT[(][^ ]* FROM *)(timestamp *'[^']*' *[+-] *timestamp *'[^']*') *[)]/", '\2', $res);
#	$res = preg_replace("/(EXTRACT[(][^ ]* FROM *)('[^']*')/", '\1 timestamp \2', $res);

	$res = preg_replace('/\bdate\b/', '"date"', $res);
	$res = preg_replace('/\bmode\b/', '"mode"', $res);
	return str_replace('REGEXP', '~', $res);
}

function spip_oci_fromfield($arg)
{
	while(preg_match('/^(.*?)FIELD\s*\(([^,]*)((,[^,)]*)*)\)/', $arg, $m)) {

		preg_match_all('/,([^,]*)/', $m[3], $r, PREG_PATTERN_ORDER);
		$res = '';
		$n=0;
		$index = $m[2];
		foreach($r[1] as $v) {
			$n++;
			$res .= "\nwhen $index=$v then $n";
		}
		$arg = $m[1] . "case $res else 0 end "
		  . substr($arg,strlen($m[0]));
	}
	return $arg;
}

function calculer_oci_where($v)
{
	if (!is_array($v))
		return spip_oci_frommysql($v);

	$op = str_replace('REGEXP', '~', array_shift($v));
	if (!($n=count($v)))
		return $op;
	else {
		$arg = calculer_oci_where(array_shift($v));
		if ($n==1) {
			  return "$op($arg)";
		} else {
			$arg2 = calculer_oci_where(array_shift($v));
			if ($n==2) {
				return "($arg $op $arg2)";
			} else return "($arg $op ($arg2) : $v[0])";
		}
	}
}


function calculer_oci_expression($expression, $v, $join = 'AND'){
	if (empty($v))
		return '';
	
	$exp = "\n$expression ";
	
	if (!is_array($v)) 
		$v = array($v);
	
	if (strtoupper($expression) === 'WHERE')
		$v = array_map('spip_oci_frommysql', $v);
	
	if (!empty($v)) {
		if (strtoupper($join) === 'AND')
			return $exp . join("\n\t$join ", array_map('calculer_oci_where', $v));
		else
			return $exp . join($join, $v);
	}
}

function spip_oci_select_as($args)
{
	$argsas = "";
	foreach($args as $k => $v) {
		if (substr($k,-1)=='@') {
			// c'est une jointure qui se refere au from precedent
			// pas de virgule
		  $argsas .= '  ' . $v ;
		}
		else {
			$as = '';
			//  spip_log("$k : $v");
			if (!is_numeric($k)) {
				if (preg_match('/\.(.*)$/', $k, $r))
					$v = $k;
				elseif ($v != $k) {
					$p = strpos($v, " ");
					if ($p)
					  $v = substr($v,0,$p) . " $k" . substr($v,$p);
					else  $as = " $k"; 
				}
			}
			// spip_log("subs $k : $v avec $as");
			// if (strpos($v, 'JOIN') === false)  $argsas .= ', ';
			$argsas .= ', '. $v . $as; 
		}
	}
	return substr($argsas,2) . $join;
}

// Les LOB ne sont pas automatiquement convertis en chaine
// et les cles sont toujours en majuscules

function spip_oci_fetch($res, $t='', $serveur='', $requeter=true) {

	$connexion = $GLOBALS['connexions'][$serveur ? $serveur : 0];
	$r = !is_resource($res) ? false : oci_fetch_assoc($res);
	if (!is_array($r))
	  spip_log("oci fetc $res retour vide " . 	$connexion['resource'] . ' ' .	$connexion['last']);
	// pas utilise pour l'instant


	else {
		$r = array_change_key_case($r, CASE_LOWER);
		foreach($r as $k => $v) {
			if (is_object($v)) {
				$r[$k] = $v->load();
			}
		}
	}
	return $r;
}

function spip_oci_countsel($from = array(), $where = array(), $groupby=array(), $having = array(), $serveur='',$requeter=true) 
{
	$c = !$groupby ? '*' : ('DISTINCT ' . (is_string($groupby) ? $groupby : join(',', $groupby)));
	$r = spip_oci_select("COUNT($c) AS \"N\"", $from, $where,'', '', '',$having, $serveur, $requeter);
	if (!$requeter) return $r;
	if (!is_resource($r)) return 0;
	$r = oci_fetch_assoc($r);
	return $r['N'];
}

function spip_oci_count($res, $serveur='',$requeter=true) {
	return !$res ? 0 : oci_num_rows($res);
}
  
function spip_oci_free($res, $serveur='',$requeter=true) {
}

function spip_oci_delete($table, $where='', $serveur='',$requeter=true) {

	$connexion = $GLOBALS['connexions'][$serveur ? $serveur : 0];
	$prefixe = $connexion['prefixe'];
	$link = $connexion['link'];
	if ($prefixe) $table = preg_replace('/^spip/', $prefixe, $table);
	
	$query = calculer_oci_expression('DELETE FROM', $table, ',')
			. calculer_oci_expression('WHERE', $where, 'AND');
			
	// renvoyer la requete inerte si demandee
	if (!$requeter) return $query;
	
	return spip_oci_trace_query($query, $serveur);
}

function spip_oci_insert($table, $champs, $valeurs, $desc=array(), $serveur='',$requeter=true) {
	global $tables_principales;
	$connexion = $GLOBALS['connexions'][$serveur ? $serveur : 0];
	$prefixe = $connexion['prefixe'];
	$link = $connexion['link'];

	if (!$desc) $desc = description_table($table);
	$seq = spip_oci_sequence($table);
	if ($prefixe) {
		$table = preg_replace('/^spip/', $prefixe, $table);
		$seq = preg_replace('/^spip/', $prefixe, $seq);
	}
	$champs = spip_oci_reserved($champs);
	$args = (strlen($champs)>=3);
	if ($seq) {
		$prim = $desc['key']['PRIMARY KEY'];
		$champs = "($prim" . ($args ? (',' .substr($champs,1)) : ')');
		$valeurs = "($seq" . '.nextval' . 
			      ($args ? (',' . substr($valeurs,1)) : ')');
	}

	// Il faudrait reussir a faire marcher ca
#	$ret = !$seq ? '' : (" RETURNING currval('$seq') INTO :last_insert");
	$ret = !$seq ? '' : ' ';
	$ins = "$champs VALUES $valeurs";

	$r = spip_oci_trace_query("INSERT INTO $table $ins $ret", $serveur);
	if (!$r) return false;
	if (!$ret) return 0;
	$r = spip_oci_trace_query('SELECT ' . $seq .".currval FROM dual", $serveur);
	if (!is_resource($r))
		spip_sql_erreur($serveur);
	else {
	  $r = oci_fetch_array($r, OCI_NUM);
	  spip_log("insertion dans $table: " . $r[0]);
	}
	return $r[0];
}

function spip_oci_insertq($table, $couples=array(), $desc=array(), $serveur='',$requeter=true) {

	if (!$desc) $desc = description_table($table);
	if (!$desc) die("$table insertion sans description");
	$fields =  $desc['field'];
	foreach ($couples as $k => $v) {
		unset($couples[$k]);
		$couples[spip_oci_reserved($k)]=spip_oci_cite($v, $fields[$k]);
	}
	return spip_oci_insert($table, "(".join(',',array_keys($couples)).")", "(".join(',', $couples).")", $desc, $serveur, $requeter);
}


function spip_oci_insertq_multi($table, $tab_couples=array(), $desc=array(), $serveur='',$requeter=true) {

	if (!$desc) $desc = description_table($table);
	if (!$desc) die("$table insertion sans description");
	if (!$table_couples) return true;
	$fields =  isset($desc['field'])?$desc['field']:array();
	
	$valeurs = array();
	foreach ($tab_couples as $couples) {
		foreach ($couples as $k => $v) {
			unset($couples[$k]);
			$couples[spip_oci_reserved($k)]= spip_oci_cite($v, $fields[$k]);
		}
		$valeurs[] = '(' .join(',', $couples) . ')';
	}
	$valeurs = implode(', ',$valeurs);
	$cles = "(" . join(',',array_keys($couples)). ')';	
	return	spip_oci_insert($table, $cles, $valeurs, $desc, $serveur, $requeter);
}


function spip_oci_update($table, $champs, $where='', $desc='', $serveur='',$requeter=true) {

	if (!$champs) return;
	$connexion = $GLOBALS['connexions'][$serveur ? $serveur : 0];
	$prefixe = $connexion['prefixe'];
	$link = $connexion['link'];

	if ($prefixe) $table = preg_replace('/^spip/', $prefixe, $table);
	$set = array();
	foreach ($champs as $k => $v) $set[]= spip_oci_reserved($k) . '=' . $v; 

	$query = calculer_oci_expression('UPDATE', $table, ',')
		. calculer_oci_expression('SET', $set, ',')
		. calculer_oci_expression('WHERE', $where, 'AND');
		
	// renvoyer la requete inerte si demandee
	if (!$requeter) return $query;
	
	return spip_oci_trace_query($query, $serveur);
}

// idem, mais les valeurs sont des constantes a mettre entre apostrophes
// sauf les expressions de date lorsqu'il s'agit de fonctions SQL (NOW etc)
function spip_oci_updateq($table, $champs, $where='', $desc=array(), $serveur='',$requeter=true) {
	if (!$champs) return;
	if (!$desc) $desc = description_table($table);
	$fields = $desc['field'];
	foreach ($champs as $k => $val)
		$champs[$k] = spip_oci_cite($val, $fields[$k]);
	return spip_oci_update($table, $champs, $where, $desc, $serveur, $requeter);
}


function spip_oci_replace($table, $values, $desc, $serveur='',$requeter=true) {

	if (!$values) {spip_log("replace vide $table"); return 0;}
	$connexion = $GLOBALS['connexions'][$serveur ? $serveur : 0];
	$prefixe = $connexion['prefixe'];
	$link = $connexion['link'];

	if (!$desc) $desc = description_table($table);
	if (!$desc) die("$table insertion sans description");
	$prim = $desc['key']['PRIMARY KEY'];
	$ids = preg_split('/,\s*/', $prim);
	$noprims = $prims = array();
	foreach($values as $k=>$v) {
		$values[$k] = $v = spip_oci_cite($v, $desc['field'][$k]);
		spip_log("replace pour $ids avec $k $v");
		$i = spip_oci_reserved($k) . '=' . $v;
		if (!in_array($k, $ids))
			$noprims[$k]= $i;
		else $prims[$k]= $i;
	}

	$where = join(' AND ', $prims);
	if (!$where) {
		return spip_oci_insert($table, "(".join(',',array_keys($values)).")", "(".join(',', $values).")", $desc, $serveur);
	}
	$couples = join(',', $noprims);

	$seq = spip_oci_sequence($table);
	if ($prefixe) {
		$table = preg_replace('/^spip/', $prefixe, $table);
		$seq = preg_replace('/^spip/', $prefixe, $seq);
	}

	if ($couples) {
	  $couples = spip_oci_trace_query("UPDATE $table SET $couples WHERE $where", $serveur);
	  if (!$couples) return false; // erreur a approfondir
	  if (oci_num_rows($couples)) return true; // l'unique a ete modifie
	}
	$ret = !$seq ? '' : '';#  (" RETURNING nextval('$seq') < $prim");
		
	$couples = oci_execute($r2 = oci_parse($link, $q = "INSERT INTO $table (" . join(',',array_keys($values)) . ') VALUES (' .join(',', $values) . ")$ret"));
	if ($couples AND $ret) {
		  $r = oci_fetch_array($r2, OCI_NUM);
		  if ($r[0]) {
		    $q = "SELECT setval('$seq', $prim) from $table";
		// Le code de SPIP met parfois la sequence a 0 (dans l'import)
		// MySQL n'en dit rien, on fait pareil ici
		    $couples = spip_oci_trace_query($q, $serveur);
		  }
	}

	return $couples;
}


function spip_oci_replace_multi($table, $tab_couples, $desc=array(), $serveur='',$requeter=true) {
	// boucler pour traiter chaque requete independemment
	foreach ($tab_couples as $couples){
		$retour = spip_oci_replace($table, $couples, $desc, $serveur,$requeter);
	}
	// renvoie le dernier id 
	return $retour; 
}

// Donne la sequence eventuelle associee a une table 
// Pas extensible pour le moment,

function spip_oci_sequence($table)
{
	global $tables_principales;

	include_spip('base/serial');
	if (!isset($tables_principales[$table])) return false;
	$desc = $tables_principales[$table];
	$prim = @$desc['key']['PRIMARY KEY'];
	if (!preg_match('/^\w+$/', $prim)
	OR strpos($desc['field'][$prim], 'int') === false)
		return '';
	else  {	return $table . '_seq';}
}

// Explicite les conversions de Mysql d'une valeur $v de type $t
// Dans le cas d'un champ date, pas d'apostrophe, c'est une syntaxe ad hoc

function spip_oci_cite($v, $t)
{
	if (sql_test_date($t)) {
		if (strpos("0123456789", $v[0]) === false)
			return spip_oci_frommysql($v);
		else {
			if (strpos($v, "-00-00") === 4) {
				$v = substr($v,0,4)."-01-01".substr($v,10);
				if (substr($v,0,4) === '0000')
				  $v = "1970".substr($v,4);
			}
			return "TO_TIMESTAMP('$v', 'YYYY-MM-DD HH24:MI:SS')";
		}
	}
	elseif (!sql_test_int($t))
		return  spip_oci_quote($v);
	elseif (is_numeric($v) OR (strpos($v, 'CAST(') === 0))
		return $v;
	elseif ($v[0]== '0' AND $v[1]!=='x' AND  ctype_xdigit(substr($v,1)))
		return  substr($v,1);
	else {
		spip_log("Warning: '$v'  n'est pas de type $t", 'oci');
		return intval($v);
	}
}

function spip_oci_hex($v)
{
	return "CAST(x'" . $v . "' as number)";
}

function spip_oci_quote($a)
{
	if (is_array($a))
		return join(",", array_map('spip_oci_quote', $a));
	elseif (is_numeric($a)) 
	  return strval($a);
	if (strlen($a) > 4000) $a = "TROP LONG" . substr($a,0,4000);
	return ("'" . str_replace("'", "''", $a) . "'");
}

// BRICOLAGE POUR eviter l'erreur, a finir
function spip_oci_date_proche($champ, $interval, $unite)
{
	return "("
	. $champ
        . (($interval <= 0) ? '>' : '<')
	. "TO_TIMESTAMP("
	  . sql_quote(date('Y-m-d H:i:s'))
	. ",'YYYY-MM-DD HH24:MI:SS')"
#        . (($interval <= 0) ? '-' : '+')
#	. ', INTERVAL '
#	. (($interval > 0) ? $interval : (0-$interval))
#	. ' '
#	. $unite
	. ")";
}

function spip_oci_in($val, $valeurs, $not='', $serveur) {
//
// IN (...) souvent limite a 255  elements, d'ou cette fonction assistante
//
	if (strpos($valeurs, "CAST(x'") !== false)
		return "($val=" . join("OR $val=", explode(',',$valeurs)).')';
	$n = $i = 0;
	$in_sql ="";
	while ($n = strpos($valeurs, ',', $n+1)) {
	  if ((++$i) >= 255) {
			$in_sql .= "($val $not IN (" .
			  substr($valeurs, 0, $n) .
			  "))\n" .
			  ($not ? "AND\t" : "OR\t");
			$valeurs = substr($valeurs, $n+1);
			$i = $n = 0;
		}
	}
	$in_sql .= "($val $not IN ($valeurs))";

	return "($in_sql)";
}

// Il semble qu'oci_error n'est appelable qu'une seule fois sur une meme res.

function spip_oci_error($serveur='', $index='message') {
	static $err = array();
	$connexion = $GLOBALS['connexions'][$serveur ? $serveur : 0];
	if (!$r = $connexion['resource']) $r = $connexion['link'];
	if (!isset($err[$r]))
		$err[$r] = $r ? oci_error($r) : oci_error();
	return $err[$r][$index];
}

function spip_oci_errno($serveur='') {
	return spip_oci_error($serveur, 'code');
}

function spip_oci_drop_table($table, $exist='', $serveur='',$requeter=true)
{
	if ($exist) $exist =" IF EXISTS";
	return spip_oci_query("DROP TABLE$exist $table", $serveur, $requeter);
}

// supprime une vue 
function spip_oci_drop_view($view, $exist='', $serveur='',$requeter=true) {
	if ($exist) $exist =" IF EXISTS";
	return spip_oci_query("DROP VIEW$exist $view", $serveur, $requeter);
}

// A revoir

function spip_oci_showbase($match, $serveur='',$requeter=true)
{
	$connexion = $GLOBALS['connexions'][$serveur ? $serveur : 0];
	$link = $connexion['link'];
	  
	return false; # query($link, "SELECT tablename FROM tables WHERE tablename ILIKE '$match'");
}

// A revoir

function spip_oci_showtable($nom_table, $serveur='',$requeter=true)
{
	return false;
	$connexion = $GLOBALS['connexions'][$serveur ? $serveur : 0];
	$link = $connexion['link'];

	$res = oci_execute($r2 = oci_parse($link, "SELECT column_name, column_default, data_type FROM information_schema.columns WHERE table_name ILIKE " . _q($nom_table)));
	if (!$res) return false;
	
	// etrangement, $res peut ne rien contenir, mais arriver ici...
	// il faut en tenir compte dans le return
	$fields = array();
	while($field = oci_fetch_array($r2, OCI_NUM)) {
		$fields[$field[0]] = $field[2] . (!$field[1] ? '' : (" DEFAULT " . $field[1]));
	}

	$res = oci_execute($r2 = oci_parse($link, "SELECT indexdef FROM  indexes WHERE tablename ILIKE " . _q($nom_table)));
	$keys = array();
	while($index = oci_fetch_array($r2, OCI_NUM)) {
		if (preg_match('/CREATE\s+(UNIQUE\s+)?INDEX.*\((.*)\)$/',
			       $index[0],$r)) {
			$index = split(',', $r[2]);
			$keys[($r[1] ? "PRIMARY KEY" : ("KEY " . $index[0]))] = 
			  $r[2];
		}
	}

	return count($fields) ? array('field' => $fields, 'key' => $keys) : false;
}

// Fonction de creation d'une table SQL nommee $nom
// a partir de 2 tableaux PHP :
// champs: champ => type
// cles: type-de-cle => champ(s)
// si $autoinc, c'est une auto-increment (i.e. serial) sur la Primary Key
// Le nom des index est prefixe par celui de la table pour eviter les conflits
function spip_oci_create($table, $champs, $cles, $autoinc=false, $temporary=false, $serveur='',$requeter=true) {

  // les contraintes doivent etre nommees toutes differemment
  // on les numerote, mais ca ne marchera pas pour des creations ulterieures
	static $cpt = 0;
	$connexion = $GLOBALS['connexions'][$serveur ? $serveur : 0];
	$prefixe = $connexion['prefixe'];
	$link = $connexion['link'];

	$nom = !$prefixe ? $table : preg_replace('/^spip/', $prefixe, $table);
	$query = $prim = $prim_name = $v = $s = $p='';
	$keys = array();

	// certains plugins declarent les tables  (permet leur inclusion dans le dump)
	// sans les renseigner (laisse le compilo recuperer la description)
	if (!is_array($champs) || !is_array($cles)) 
		return;

	foreach($cles as $k => $v) {
		if (strpos($k, "KEY ") === 0) {
		  $v = spip_oci_reserved($v);
		  $i = $nom . '_k' . (++$cpt);
		  $keys[] = "CREATE INDEX $i ON $nom($v)";
		} else {
		  if ($k == "PRIMARY KEY") {
			$prim_name = $v; 
		  }
		  $i = $nom . 'p_' . (++$cpt);
		  $r = spip_oci_trace_query("ALTER TABLE $nom DROP CONSTRAINT $i", $serveur);
		  $prim .= "$s\n\t\tCONSTRAINT " . $i . ' ' . str_replace('`','"',$k) ." ($v)";
		}
		$s = ",";
	}
	$s = '';
	
	$character_set = "";
	/* // charset en Oracle a revoir
	if (@$GLOBALS['meta']['charset_sql_base'])
		$character_set .= " CHARACTER SET ".$GLOBALS['meta']['charset_sql_base'];
	if (@$GLOBALS['meta']['charset_collation_sql_base'])
		$character_set .= " COLLATE ".$GLOBALS['meta']['charset_collation_sql_base'];
	*/
	foreach($champs as $k => $v) {
		$v = preg_replace('/DEFAULT\s+\'(\d+)\'/', 'DEFAULT \1', $v);
		$v = preg_replace('/DEFAULT\s+\'\'\s+NOT\s+NULL/i', 'DEFAULT \'\'', $v);
		$k = spip_oci_reserved(str_replace('`','',$k));
		if (preg_match(',([a-z]*\s*(\(\s*[0-9]*\s*\))?(\s*binary)?),i',$v,$defs)){
			if (preg_match(',(char|text),i',$defs[1]) AND !preg_match(',binary,i',$defs[1]) ){
				$v = $defs[1] . $character_set . ' ' . substr($v,strlen($defs[1]));
			}
		}

		$query .= "$s\n\t\t$k " . mysql2oci_type($v)  ;
		$s = ",";
	}
	if ($prim_name AND $seq = spip_oci_sequence($table)) {

		if ($prefixe) {
			$seq = preg_replace('/^spip/', $prefixe, $seq);
		}

		$r = spip_oci_trace_query("DROP sequence $seq", $serveur);
		$r = "create sequence $seq start with 1 increment by 1 nomaxvalue"; 
		$r = spip_oci_trace_query($r, $serveur);
		if (!$r)
			spip_log("creation de la sequence $nom impossible (deja la ?)");
	}

	$temporary = $temporary ? 'TEMPORARY':'';

	// En l'absence de "if not exists" , on neutralise les erreurs

	$query = "CREATE $temporary TABLE $nom ($query" . ($prim ? ",$prim" : '') . ")".
	($character_set?" DEFAULT $character_set":"")
	."\n";

	$r = spip_oci_trace_query("DROP table $nom", $serveur);
	$r = spip_oci_trace_query($query, $serveur);

	if (!$r)
		spip_log("creation de la table $nom impossible (deja la ?)");
	else {
	  foreach($keys as $index) {spip_oci_trace_query($index, $serveur);}
	} 
	return $r;
}


function spip_oci_create_base($nom, $serveur='',$requeter=true) {
	return spip_oci_query("CREATE DATABASE $nom", $serveur, $requeter);
}


// Fonction de creation d'une vue SQL nommee $nom
function spip_oci_create_view($nom, $query_select, $serveur='',$requeter=true) {
	if (!$query_select) return false;
	// vue deja presente
	if (sql_showtable($nom, false, $serveur)) {
		if ($requeter) spip_log("Echec creation d'une vue sql ($nom) car celle-ci existe deja (serveur:$serveur)");
		return false;
	}
	
	$query = "CREATE VIEW $nom AS ". $query_select;
	return spip_oci_query($query, $serveur, $requeter);
}


function spip_oci_set_connect_charset($charset, $serveur='',$requeter=true){
	spip_log("changement de charset sql a ecrire en ORACLE");
}

// Selectionner la sous-chaine dans $objet
// correspondant a $lang. Cf balise Multi de Spip

function spip_oci_multi ($objet, $lang) {
	$r = "regexp_replace("
	  . $objet
	  . ",'<multi>.*[[]"
	  . $lang
	  . "[]]([^[]*).*</multi>', '\\\\1') AS multi";
	return $r;
}

// Palanquee d'idiosyncrasies MySQL dans les creations de table
// A completer par les autres, mais essayer de reduire en amont.

function mysql2oci_type($v)
{
  return     preg_replace('/int(eger)?((\s*[(]\s*\d+\s*[)])|\s)/i', 'number(19,0)', 
		preg_replace('/bigint(\s*[(]\s*\d+\s*[)])?/i', 'number(19,0)', 
		preg_replace("/text/i", 'varchar2(4000)',
		preg_replace("/longtext/i", 'clob',
		str_replace("mediumtext", 'clob',
		preg_replace("/tinytext/i", 'varchar2(255)',
	  	str_replace("longblob", 'clob',
		str_replace("0000-00-00",'0001-01-01',
		preg_replace("/(datetime).*$/i","date DEFAULT CURRENT_TIMESTAMP",
		preg_replace("/unsigned/i", '',
		preg_replace("/double/i", 'float(126)', 	
		preg_replace("/tinyint/i", 'number(5)', 	
		preg_replace("/VARCHAR\((\d+)\)\s+BINARY/i", 'varchar2(\1)', 
		preg_replace("/ENUM *[(][^)]*[)]/i", "varchar2(255)",
					      $v 
			     ))))))))))))));
}

// Renvoie false si on n'a pas les fonctions oci (pour l'install)
function spip_versions_oci(){
	return function_exists('oci_connect');
}

?>
