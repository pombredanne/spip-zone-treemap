#!/bin/bash

###
# Une extension de getopt pour analyser les paramètres d'un shell
#
# © bertrand Gugger bertrand@toggg.com 2007 _ Licence LGPL
#
# Le premier paramètre décrit la liste des options courtes et/ou longues
# cf man getopt , man getopts
# Si pas d'option courte spécifique (x=...), initiale de l'option longue
# Le retour est a evaluer comme:
# eval $(options.sh 'x=option:: y=sans z=requis: long: moins:: nopar' "$@")
# les options seront des variables nommees comme le long nom (ici $sans, $moins...)
# Les arguments non-options se retrouvent dans $args[]
###

shopt -s extglob
for opt in $1 ; do
	arg=${opt##+([^:])}
	opt=${opt%%+([:])}
	s=${opt:0:1}
	opt=${opt#?=}
	short="$short$s$arg"
	long="$long -l$opt$arg"
	if [ -z $arg ] ; then
		sed="${sed}s/ -\($s\|-$opt\) / $opt=1; /g;"
	else
		sed="${sed}s/ -\($s\|-$opt\) \([^ ]*\) / $opt=\2\; /g;"
	fi
done
shift	
echo ' '$(getopt -o$short $long -- "$@")' );' | sed -e "${sed}s/ -- / args=(\$(cd \$(dirname \$0);pwd;cd \$OLDPWD;)\/\$(basename \$0) /"
exit
