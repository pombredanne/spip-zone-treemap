<?php
/*
 * zonedist: paquetage de zone svn
 *
 * © bertrand Gugger bertrand@toggg.com 2007 - Distribue sous licence LGPL
 *
 */

// quelle heure est-il ?
echo '# ' . $argv[0] . ':      ' . date('r') ."\n";

/*
	La premiere ligne devrait etre le repertoire cible
*/
if (!(($cible = trim(fgets(STDIN))) && is_dir($cible)  && is_readable($cible)
			 && is_writable($cible))) {
	echo "# STOP, impossible d'ecrire dans le repertoire $cible (dir:" .
		is_dir($cible) . ", lisible:" . is_readable($cible)  .
		", ecrivable:" . is_writable($cible) . ")\n";
	exit(1);
}

/*
	La seconde ligne devrait etre la derniere revision
	 ( LANG=; time svn info . | grep -i 'Last Changed Rev:')
*/
if (!($line = fgets(STDIN)) ||
		!preg_match('#Last\s+Changed\s+Rev\s*:\s*(\d+)#i', $line, $match) ||
		!($was = 0 + $match[1])) {
	echo "# STOP, pas de svn, recu:$line\n";
	exit(1);
}

/*
	Ensuite le log de svn up
	Fabriquer un tableau des fichiers et repertoires contenant changes
*/
$changed = array();
while (preg_match('#^[ADUCG]\s+(.+)$#', $line = fgets(STDIN), $matches)) {
	$changed[$rep = $matches[1]] = true;
	while (($pos = strrpos($rep, '/')) && ($rep = substr($rep, 0, $pos)) &&
			!isset($changed[$rep])) {
		$changed[$rep] = true;
	}
}

/*
	Jusqu'a "at revision" xxx.
*/
if (!preg_match('#.+\s+(\d+)\.$#', $line, $matches)
		|| !($is = 0 + ($matches[1]))
		|| $was > $is) {
	var_dump($changed);
	echo "# parti de $was, on arrive a rien ($is), recu:$line\n";
	exit(1);
}
// des changements ?
if (!$changed) {
	echo '# ' . $argv[0] . ' ' . date('r') .": pas de chamgement toujours en $was\n";
	exit(0);
}
var_dump($changed);

function pos_xml($nomfic, &$xml, &$pos)
{
	global $cible;
	$pos = array();
	$xml = '';
	$xml = join('', @gzfile($cible . '/' . $nomfic . '.xml.gz'));
	if (!preg_match('#<(\w+ id=")(.*?)"#', $xml, $mat1)) {
		return 0;
	}
	$ret =  preg_match_all("#{$mat1[1]}(.*?)\"#", $xml, $matches, PREG_OFFSET_CAPTURE+PREG_SET_ORDER);
	$id = '';
	foreach ($matches as $match) {
		if ($id) {
			$pos[$id] = array($laspos, $match[0][1] - $laspos);
		}
		$id = $match[1][0];
		$laspos = $match[0][1];
	}
	$pos[$id] = array($laspos, strlen($xml) - $laspos);
	return $ret;
}

function code_change(&$folders, $name, $key, &$val, &$changed)
{
	global $was;
	return  $key == 'z' && ($pos = strpos($name, ':')) ? (
		0 + substr($name, $pos) <= $was ? true : 'change avant version'
		) : (isset($changed[$name]) ? 'svn' : true);
}

function archive_change(&$foldold, $name, $key, &$val, &$folders)
{
	return !isset($folders[$name][$key]) ? 'archive supprimee' : (
		$folders[$name][$key] != $val ? 'archive changee' : false);
}

/*
	Fabriquer un tableau decrivant les repertoires a traiter:
	array("repertoire" => array(
		'z' => ligne archivelist.txt explosee si presente
		'p' => TRUE si plugin.xml
		't' => TRUE si theme.xml
*/
function addfol(&$folders, $name, $key, $val, $fun, &$arg)
{
	if (!$ret = $fun($folders, $name, $key, $val, $arg)) {
		return false;
	}
	if (!isset($folders[$name])) {
		$folders[$name] = array();
	}
	$folders[$name][$key] = $val;
	if ($ret !== true) {
		$folders[$name]['todo'] = $ret;
	}
	return true;
}
function folders(&$folders, $in, $fun, &$arg)
{
	while (($line = fgets($in)) !== false) {
		if (strpos("X# \t", $line[0])) {
			continue;
		}
		if (count($zip = explode(';', $line)) > 1) {
			if ($zip[0][strlen($zip[0]) - 1] == '/') {
				$zip[0] = substr($zip[0], 0, -1);
			}
			addfol($folders, array_shift($zip), 'z', $zip, $fun, $arg);
			continue;
		}
		if (!preg_match('#^(.+)/(?:(p)lugin|theme)\.xml$#', $line, $matches)) {
			continue;
		}
		addfol($folders, $matches[1], empty($matches[2]) ? 't' : 'p', true, $fun, $arg);
	}
}

// les plugin.xml, theme.xml et references archivelist.txt triees
$folders = array();
$status = folders($folders, STDIN, 'code_change', $changed);

$delfol = array();
if (isset($changed['archivelist.txt'])) {
	$old = fopen('archivelist.txt.old', 'r');
	$statold = folders($delfol, $old, 'archive_change', $folders);
}
echo "# a detruire *********************************************\n";
var_dump($delfol);
echo "# todo *********************************************\n";
foreach ($folders as $name => $folder) {
	if (isset($folder['todo']) && $folder['todo']) {
		var_dump($folder);
	}
}
// Chercher le heros qui est plugin.xml, theme.xml et dans archivelist.txt
echo "# heros *********************************************\n";
foreach ($folders as $folder => $things) {
	if (!empty($things['z']) && !empty($things['p']) && !empty($things['t'])) {
		echo $folder . "\n";
	}
}

echo "# paquets.xml.gz *********************************************\n";
$paqxml = '';
$paqpos = array();
$paqsta = pos_xml('paquets', $paqxml, $paqpos);
var_dump($paqpos);

echo "# ref.xml.gz *********************************************\n";
$refxml = '';
$refpos = array();
$refsta = pos_xml('ref', $refxml, $refpos);
var_dump($refpos);

echo "# fin *********************************************\n";
echo "# On a fait de $was a $is\n";
echo '# ' . $argv[0] . ':      ' . date('r') ."\n";
?>
