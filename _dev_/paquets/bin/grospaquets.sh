#!/bin/bash

#maj du source local et creation des zip de spip et spip zone
bin/paquets.sh spip spip-zone >/dev/null 2>&1

#creation des flux rss
checkout/spip-zone/_outils_/refxml2rss/refxml2rss.sh paquets/spip-zone/ref.xml.gz
checkout/spip-zone/_outils_/refxml2rss/refxml2rss.sh paquets/spip-zone/paquets.xml.gz

#creation des zip complets des 3 rep de plugins de la zone
bin/paquet-unique.sh checkout/spip-zone/_plugins_/_test_  paquets/plugins/test plugins-test tmp
bin/paquet-unique.sh checkout/spip-zone/_plugins_/_dev_  paquets/plugins/dev plugins-dev tmp
bin/paquet-unique.sh checkout/spip-zone/_plugins_/_stable_  paquets/plugins/stable plugins-stable tmp
