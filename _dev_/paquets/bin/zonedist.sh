#!/bin/bash

###
# zonedist: paquetage de zone svn
#
# © bertrand Gugger bertrand@toggg.com 2007 - Distribue sous licence LGPL
#
#/

# developpement pour creer les paquets (.zip ...) , ne fait rien encore
# tout en updatant la copie svn locale donnée en 1er argument
# selon <todo>archivelist.maison.txt sinon </todo> archivelist.txt situee a sa racine
# argument 1 : repertoire svn d'ou tirer archivelist.txt et les paquets
# argument 2 : repertoire des archives
#

lastexit=1
if [ $(($#)) -lt 2 ]
    then
        echo
        echo "usage: $0 svn_dir_to_archive destination_dir"
        echo
        exit -1
fi
cd `dirname $0`; dirbin=$PWD; cd $OLDPWD
cd $2; dirtar=$PWD; cd $OLDPWD
cd $1
touch archivelist.txt
cp archivelist.txt archivelist.txt.old
(echo $dirtar; (LANG=; svn info . | grep 'Last Changed Rev:') ; svn up
 (find * \( -iname plugin.xml -o -iname theme.xml \) -print
  grep -P '^[^\s#]' archivelist.txt ) | sort) |
   php ${0%sh}php
cd $OLDPWD
