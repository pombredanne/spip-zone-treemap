#!/bin/bash

###
# creer les paquets (.zip ...) pour les racines données en argument (ou PQ_SVNREP)
# des répertoires référencés par archivelist.txt
# dans les svn donnés en arguments, défaut : spip et spip-zone
# en y adjoignant à la base svn.version (version svn et date courante)
# environement:
# PQ_SOURCE défaut /home/svn/checkout où sont mises les copies locales des repos
# PQ_DEST défaut /var/www/shim/fil/Web/files/ où on va poser les .tgz et les .zip
# PQ_SVN défaut svn://zone.spip.org/ la repos où on va tout chercher
# PQ_SVNREP une liste de répertoires à y visiter défaut "spip spip-zone" (écrasé par arguments)
#
# ces paramètres peuvent être introduits en ligne mais sont fixés dans paquets.cfg
#
# exemple pour créer les paquets de spip et spip-zone
# repos dans le répertoire checkout/ et paquets posés dans paquets/ :
#
# PQ_SOURCE=checkout  PQ_DEST=paquets  ./paquets-alternatif.sh spip spip-zone
#
cd `dirname $0`
dirbin=$PWD
cd $OLDPWD
paquetszonesh=$dirbin/paquets-zone.sh

# ça écrase la ligne de commande sauf pour PQ_SVNREP où les arguments priment
paquetscfg=paquets.cfg
if [ ! -s $paquetscfg ]
then
	paquetscfg=$dirbin/paquets.cfg
fi
if [ -s $paquetscfg ]
then
	. $paquetscfg
fi

cd "${PQ_SOURCE:=checkout}"
PQ_SOURCE=$PWD
cd "$OLDPWD"
cd "${PQ_DEST:=paquets}"
PQ_DEST=$PWD
cd "$OLDPWD"
PQ_SVN=${PQ_SVN:=svn://zone.spip.org/}
PQ_SVN=${PQ_SVN%/}
PQ_SVNREP=${@:-${PQ_SVNREP:-spip spip-zone}}

echo "Dans $PQ_SOURCE/ , création paquets pour ${PQ_SVNREP// /, } depuis $PQ_SVN/ à destination de $PQ_DEST/"

export PQ_TRAC PQ_DEST PQ_SOURCE

# mkdir -p $PQ_SOURCE
# rm -rf $PQ_SOURCE/*

for truc in $PQ_SVNREP; do
    echo "au tour de $truc"
    mkdir -p "$PQ_DEST/$truc/"
    echo `date` "au tour de $truc" > "$PQ_DEST/$truc/log.txt"
    trucdir=$PQ_SOURCE/$truc
    mkdir -p "$trucdir"

	if [ -s "$trucdir/archivelist.maison.txt" ]
	then
		if [ ! -s "$trucdir/archivelist.maison.txt.old" ]
		then
			touch "$trucdir/archivelist.maison.txt.old"
		fi
	else
		if [ -s "$trucdir/archivelist.txt" ]
		then
			cp "$trucdir/archivelist.txt" "$trucdir/archivelist.txt.old"
		else
			touch "$trucdir/archivelist.txt.old"
		fi
	fi

    if svn -q -N checkout "$PQ_SVN/$truc" "$trucdir"
    then
        svn -q up "$trucdir"
	if $paquetszonesh "$trucdir" "$PQ_DEST/$truc" >> "$PQ_DEST/$truc/log.txt"
	    then
	        echo "fini pour $truc"
		    echo "fini pour $truc a" `date` >> "$PQ_DEST/$truc/log.txt"
	    else
	        echo "$0: no $PQ_SVN/$truc/archivelist.txt"
	    fi
    else
        echo "$0: bad svn"
    fi
	if [ -s "$trucdir/archivelist.maison.txt" ]
	then
		cp "$trucdir/archivelist.maison.txt" "$trucdir/archivelist.maison.txt.old"
	fi
done

exit 0
