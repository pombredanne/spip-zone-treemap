#!/bin/bash

###
# Afficher svn.version pour un répertoire de svn local (à jour)
# et retourner la date de modification en secondes depuis epoch
#
# argument 1 : sous-repertoire de svn dont on veut la version
# argument 2 : optionel, fichier dont on veut comparer la date de modification
#              a la date de derniere mise a jour du svn
# Retour: 1 si 2eme parametre present et est un fichier plus vieux, 0 sinon
#
# exemple pour le plugin coloration_code:
#
# ./paquet-version.sh checkout/spip-zone/_plugins_/_typo_/coloration_code/

if [ $# = 0 ]
then
    echo
    echo "usage: $0 svn_dir [file to compare]"
    echo
    exit -1
fi
cd $(dirname $0)
dirbin=$PWD
cd $OLDPWD

# recuperer l'option -r1234 ou --revision 1234 dans $revision
eval $($dirbin/options.sh 'revision:' "$@")
if [ -n "$revision" ] ; then
	revision=" --revision $revision"
fi
svn_dir=${args[1]%/}
rev='?'
date='?'
url='?'
path='?'
eval $(LANG=; svn info$revision "$svn_dir" | sed -ne '
	s/Path:\s*\([^ ]*\)/path="\1\";/p
	s/URL:\s*\([^ ]*\)/url="\1\";/p
	s/Last Changed Rev:\s*\(.*\)/rev="\1\";/p
	s/Last Changed Date:\s*\(.*\)/date="\1\";/p')
echo '<svn_revision><text_version>'
echo "Origine $url le `date`"
echo "Revision: $rev"
echo "Dernier commit $date"
if [ -n "$PQ_TRAC" ]
then
	eval  $PQ_TRAC
fi
if [ -n "$text" ]
then
	echo -e "$text"
fi
echo '</text_version>'
if [ -n "$xml" ]
then
	echo -e "$xml"
fi
echo "<origine>$url</origine>"
echo "<date_production>`date`</date_production>"
echo "<revision>$rev</revision>"
echo "<commit>$date</commit>"
echo '</svn_revision>'
if [ -z "${args[2]}" ]
then
	exit 0
fi
if [ -s "${args[2]}" ] && [ `stat -c %Y "${args[2]}"` -gt `date +%s -d "$date"` ]
then
	exit 0
fi
exit 1
#  mutt -s 'sujet' destinataire@toto.com <fichier (juste pour pas oublier, on en aura besoin pour la plume et le goudron)
