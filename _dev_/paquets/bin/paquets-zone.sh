#!/bin/bash

###
# creer les paquets (.zip ...)
# pour la copie svn locale donnée en 1er argument
# selon archivelist.maison.txt sinon archivelist.txt situee a sa racine
# argument 1 : repertoire svn d'ou tirer archivelist.txt et les paquets
# argument 2 : repertoire des archives
# environement:
# PQ_TRAC pour calculer les liens voir: ... dans les svn.revision produits
#

export PQ_TRAC
shopt -s extglob
lastexit=1
if [ $(($#)) -lt 2 ]
    then
        echo
        echo "usage: $0 svn_dir_to_archive destination_dir"
        echo
        exit -1
fi
temp="$(pwd)/tmp"

cd `dirname $0`
dirbin=$PWD
cd $OLDPWD
paquetunique=$dirbin/paquet-unique.sh
paquetversionsh=$dirbin/paquet-version.sh

iddest=${2#$PQ_DEST}
cd "$2"
if [ $? -ne 0 ]
then
    echo "$0: bad destination rep $2"
    exit 1
fi
destruc=$PWD
refxml=$PWD/ref.xml
echo '<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE zone SYSTEM "http://files.spip.org/dtd/ref.dtd">
<zone>' > $refxml
paqxml=$PWD/paquets.xml
echo '<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE zone SYSTEM "http://files.spip.org/dtd/ref.dtd">
<zone>' > $paqxml
cd "$OLDPWD"

idsource=${1#$PQ_SOURCE}
cd "$1"
if [ $? -ne 0 ]
then
    echo "$0: bad origin svn local rep $1"
    exit 1
fi
oritruc=$PWD
rm -f "$temp/xml.tar.bz2"
tar cjf "$temp/xml.tar.bz2" `find * \( -iname plugin.xml -o -iname theme.xml \) -print`
cd "$OLDPWD"

if [ -s "$temp/xml.tar.bz2" ]
then
	cd "$temp"
	tar xjf xml.tar.bz2
	for f in `find * \( -iname 'plugin.xml' -o -iname 'theme.xml' \) -print`
	do
		echo "<zone_elt id=\"x_$idsource/$f\">" >> $refxml
		$paquetversionsh `dirname "$oritruc/$f"` >> "$f"
		grep -v '^<?xml' "$f" >> $refxml
		echo '</zone_elt>' >> $refxml
	done
	cd "$OLDPWD"
fi

if [ -s "$oritruc/archivelist.maison.txt" ]
then
	archivelisttxt="$oritruc/archivelist.maison.txt"
else
	archivelisttxt="$oritruc/archivelist.txt"
fi

if [ ! -s "$archivelisttxt.old" ]
then
	touch "$archivelisttxt.old"
fi

if [ -s "$archivelisttxt" ]
then
	IFS=';'
	diff --strip-trailing-cr --changed-group-format='%<' --unchanged-group-format='' \
	"$archivelisttxt.old" "$archivelisttxt" | \
	while read rep arc nam rul; do
    	if [ $rep ] && [ ${rep:0:1} != '#' ]
    	then
    		arc=${arc:=${rep##*/}}
    		rm -fv "$destruc/$arc".*
    	fi
    done
    (cat "$archivelisttxt" ; echo) | dos2unix | while read rep arc nam rul; do
    	if [ $rep ] && [ ${rep:0:1} != '#' ]
    	then
    		rev=${rep##+([^:])}
			if [ -n "$rev" ] ; then
				rep=${rep%[:]*}
				rev=" --revision ${rev:1}"
			fi
    		
    		arc=${arc:=${rep##*/}}
            nam=${nam:=$arc}
			svnversion=$(eval "$paquetversionsh$rev $oritruc/$rep $destruc/$arc.zip")"
<archivelist>$rep$rev;$arc;$nam</archivelist>"
			if [ $? -eq 0 ]
			then
				echo ";$rep;$arc; =="
			else
				export svnversion
				echo ";$rep;$arc; !="
	            echo $(eval "$paquetunique$rev $oritruc/$rep $destruc/$arc $nam $temp $rul")
            fi
            inplug=
			if [ -s "$oritruc/$rep/plugin.xml" ]
			then
				inplug=$(grep -v '^<?xml' "$oritruc/$rep/plugin.xml")
			fi

			echo "<zone_elt id=\"a_$iddest/$arc\">$inplug$svnversion</zone_elt>" >> $paqxml
        fi
    done
    lastexit=0
else
    echo "$0: no $archivelisttxt"
fi

echo '</zone>' >> $refxml
gzip -f "$refxml"
echo '</zone>' >> $paqxml
gzip -f "$paqxml"
rm -rf "$temp"/*
exit $lastexit
