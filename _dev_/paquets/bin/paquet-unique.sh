#!/bin/bash

###
# Creer le paquet paq.zip pour un répertoire de svn local (à jour)
# en y adjoignant à la base svn.version (version svn et date courante)
#
# La date de dernier commit est comparée à la date du .zip ,
# pas de génération si pas de changement , virer le .zip pour forcer une regénération
# 
#
# argument 1 : sous-répertoire de svn à empaqueter
# argument 2 : fichier archive sans extension
#             (déf. dernière portion du chemin svn dans le répertoire courant)
# argument 3 : répertoire racine du paquet dans l'archive (déf. dernière portion du chemin svn)
# argument 4 : répertoire de travail où le svn sera extrait (déf. répertoire courant)
#
# exemple pour empaqueter le plugin coloration_code: 1 seul paramètre le rep. svn, reste défaut
# ( coloration_code.zip et coloration_code.tgz dans le répertoire courant , usage de /tmp )
#
# ./paquet-unique.sh checkout/spip-zone/_plugins_/_typo_/coloration_code/

# exemple pour empaqueter spip-1.8 dans paquets/ tout en appelant sa racine spip/ et tmp local
#
# ./paquet-unique.sh checkout/spip/branches/spip-1.8/ paquets/spip/SPIP-v1-8-3 spip tmp
#

if [ $# = 0 ]
    then
        echo
        echo "usage: $0 svn_dir_to_pack [dest_fil [paq_root [work_dir [rule]]]]"
        echo
        exit 1
fi
cd `dirname $0`
dirbin=$PWD
cd $OLDPWD
paquetversion=$dirbin/paquet-version.sh

# recuperer l'option -r1234 ou --revision 1234 dans $revision
eval $($dirbin/options.sh 'revision: j=bzip2 z=gzip' "$@")
if [ -n "$revision" ] ; then
	revision=" --revision $revision"
fi

svn_dir=${args[1]%/}
dest_fil=${args[2]:-$PWD/${svn_dir##*/}}
paq_root=${args[3]:-${svn_dir##*/}}
work_dir=${args[4]:-/tmp}
work_dir=${work_dir%/}

cd "${dest_fil%/*}"
dest_fil=$PWD/${dest_fil##*/}
cd "$OLDPWD"

if [ -z "$svnversion" ]
  then
	svnversion="`$paquetversion$revision $svn_dir $dest_fil.zip`"
	if [ $? -eq 0 ]
	then
		echo "$paq_root =="
		exit 0
	fi
fi

echo "on fait $paq_root"

mkdir -p -- "$work_dir"
rm -rf -- "$work_dir/$paq_root"
mkdir -p -- "$work_dir/$paq_root"
if (svn --force export$revision "$svn_dir" "$work_dir/$paq_root")
then
	cd "$work_dir"
    echo "$svnversion" > "${paq_root}/svn.revision"
	rm -f -- "${paq_root}/_REGLES_DE_COMMIT.txt"
	if [ $gzip ]
	  then
	    tar czf "$dest_fil.tgz" "$paq_root"
    fi
	if [ $bzip2 ]
	  then
		tar cjf "$dest_fil.tbz" "$paq_root"
    fi
	rm -f -- "$dest_fil.zip"
    echo -n "$paq_root" | zip -q -9 -r -@ - > "$dest_fil.zip"
    echo "OK pour $paq_root"
    ret=0
	cd "$OLDPWD"
else
    echo "$0: bad svn or no $svn_dir"
    ret=1
fi
rm -rf -- "$work_dir/$paq_root"

exit $ret
