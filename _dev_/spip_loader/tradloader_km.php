<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=km
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'ផ្តើម ការតំលើង >>',

	// C
	'ce_repertoire' => 'ថតឯកសារនេះ',

	// D
	'donnees_incorrectes' => '<h4>Incorrect data. Please try
again or use the manual installation procedure instead.</h4>
  <p>Error : @erreur@</p>', # NEW
	'du_repertoire' => 'ថតឯកសារ',

	// E
	'echec_chargement' => '<h4>The download has failed. Please
try again or use the manual installation procedure instead.</h4>', # NEW

	// T
	'texte_intro' => '<p><b>Welcome to the automatic
installation process of @paquet@.</b> <p>First, the programme will check the
permissions on the current directory and then it will begin to download the
@paquet@ files to @dest@. <p>Please click on the button to continue.', # NEW
	'texte_preliminaire' => '<br /><h2>Preliminary step: <b>Set the access permissions</b></h2>
 <p><b>It is not possible to write to the
current directory.</b></p>
 <p>To change the permissions of the directory in
which you are installing @paquet@ use your FTP client.<br /> The procedure is explained in detail in the Installation Guide. Choose between:</p>
 <ul>
 <li><b>If you have an FTP client with a graphical interface</b>, set the permissions
of the directory to make it open for everyone to write to it.</li>
 <li><b>If you have an FTP client with a text interface</b>, change the permissions of the directory to the value @chmod@.</li>
 <li><b>If you are using a Telnet access</b>,
execute the command <i>chmod @chmod@  current_directory</i>.</li>
 </ul>
<p>Once this has been done, please <b><a href=\'spip_loader.php?charger=oui\'>reload this page</a></b>
 to start to download and install SPIP.</p>
 <p>If you continue to receive this error notification, you will need to use the manualinstallation method
 (downloading the SPIP files by FTP) instead.</p>',
	'titre' => 'ទំនាញយក នៃ @paquet@'
);

?>
