<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=hr
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Instalirati >>',

	// C
	'ce_repertoire' => 'ovaj direktorij',

	// D
	'donnees_incorrectes' => '<h4>Pogrešni podaci. Molim pokušajte
ponovo ili koristite ručnu proceduru instalacije.</h4>
  <p>Greška : @erreur@</p>',
	'du_repertoire' => 'direktorij',

	// E
	'echec_chargement' => '<h4>Preuzimanje nije uspjelo. Molim
Pokušajte ponovo ili koristite ručnu proceduru instalacije.</h4>',

	// T
	'texte_intro' => '<p><b>Dobrodošli u automatski postupak
instalacije @paquet@.</b> <p>Program će prvo provjeriti dozvole
aktuelnog direktorija i zatim početi preuzimanje 
@paquet@ datoteka u @dest@. <p>Molim kliknite dugme za nastavak.',
	'texte_preliminaire' => '<br /><h2>Početni korak: <b>Postavite dozvole pristupa</b></h2>
 <p><b>Trenutno nije moguće pisati u aktuelni
direktorij.</b></p>
 <p>Za promijenu dozvole pristupa direktoriju u
koji instalirate @paquet@ koristite Vaš FTP program.<br /> Taj postupak je detaljno objašnjen u uputi za instalaciju. Izaberite između:</p>
 <ul>
 <li><b>Ako imate FTP program sa grafičkim sučeljem</b>, promijenite dozvole pristupa tako 
da svako u njega može pisati.</li>
 <li><b>Ako imate FTP program sa tekstualnim sučeljem</b>, promijenite dozvole za direktorij u vrijednost @chmod@.</li>
 <li><b>Ako koristite Telnet pristup</b>,
izvršite komandu <i>chmod @chmod@  aktuelni_direktorij</i>.</li>
 </ul>
<p>Kada ste to učinili, <b><a href=\'spip_loader.php?charger=oui\'>učitajte ovu stranicu ponovo</a></b>
 da biste pokrenuli preuzimanje i instalirali @paquet@.</p>
 <p>Ako i pored toga budete dobijali ovu istu poruku, moraćete koristiti ručni metod instalacije
 (preuzimanje @paquet@ datoteka pomoću FTP).</p>',
	'titre' => 'Preuzimanje @paquet@'
);

?>
