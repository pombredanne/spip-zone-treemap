<?php
/*
 * kit_loader: etape finale de l'installation d'un spip pre-configure
 *
 * Auteur : bertrand@toggg.com
 * � 2007 - Distribue sous licence LGPL
 *
 */
// ce script est inclus en fin d'etape 6

	// nettoyer kit_loader de la racine en recuperant l'url du serveur kit_loader
	// (autodestruction de ce script lui-meme)
	$url_serveur = kit_nettoyer_racine();
	ecrire_meta('kit_loader_serveur', $url_serveur);
    ecrire_metas();

	spip_log('kit_loader: etape finale (' . $url_serveur . ')');

	// inclusion du chargeur du plugin chargeur provenant du paquet kitspip.zip
	include _DIR_RACINE . 'plugins/chargeur/inc/chargeur.php';


	// chargement decompression de mes_fichiers.zip
	$status = chargeur_charger_zip($url_serveur . 'CONFIG/mes_fichiers.zip');
	if ($status <= 0) {
		spip_log('kit_loader erreur ' . $status . ' pour paquet mes_fichiers.zip');
	}

	// restitution du dump si present
	if (!$dump = preg_files(_DIR_DUMP . '.*\.xml\.gz$')) {
		spip_log('Installation terminee, pas de dump de la BD disponible');
		return true;
	}

	// declenchement de import_all comme si on y etait
	ob_start();
	include_spip('exec/import_all');
	$dump = substr(strrchr($dump[0], '/'), 1);
	$GLOBALS['connect_toutes_rubriques'] = true;
	$GLOBALS['connect_id_auteur'] = 1;
	$_REQUEST['exec'] = 'import_all';
	$_REQUEST['archive'] = $dump;
	import_all_debut($_REQUEST);
	$import_all = charger_fonction('import_all');
	$import_all();
	ob_end_clean();

	// installation des plugins issus du dump trouves dans spip_meta
	lire_metas();
	include_spip('inc/plugin');
	$plugins = liste_plugin_actifs();
	// le plugin chargeur est deja la
	unset($plugins['CHARGEUR']);
	unset($plugins['KITSPIP']);
	foreach ($plugins as $key=>$plug){
		// ca ne marche que si le paquet s'appelle pareil que le repertoire du plugin
		$status = chargeur_charger_zip(array(
					'depot' => $url_serveur . 'DISTRIB/',
					'nom' => $plug['dir'],
					'remove' => 'spip/plugins',
					'dest' => dirname(__FILE__) . '/plugins'));
		if ($status <= 0) {
			spip_log('kit_loader erreur ' . $status . ' pour plugin ' . $plug['dir']);
		} else {
			$_POST['s' . substr(md5('statusplug_' . $plug['dir']),0,16)] = 'O';
		}
	}
	// activer les plugins
	clearstatcache();
	$_POST['s' . substr(md5("statusplug_chargeur"),0,16)] = 'O';
	include_spip('action/activer_plugins');
	enregistre_modif_plugin();
	verif_plugin();
	installe_plugins();
	
	spip_log('Installation terminee');
	redirige_par_entete(_DIR_RACINE . 'spip.php');

// Nettoyer tous les fichiers kit_loader... de la racine
// Mais auparavant, recuperer l'url du serveur originel de kit_loader
function kit_nettoyer_racine() {
	$url_serveur = false;
	($lines = @file(_DIR_RACINE . 'kit_loader.php')) || ($lines = array());
	foreach ($lines as $line ) {
		if (($pos = strpos($line, "define('_SERVEUR_URL', '")) !== false) {
			$url_serveur = substr($line, $pos + 24, -4);
			break;
		}
	}

	$d = opendir(_DIR_RACINE);
	while (false !== ($f = readdir($d))) {
		if (preg_match('/^kit_loader.*\.(php|js|css)$/', $f)) {
			unlink(_DIR_RACINE . $f);
		}
	}
	closedir($d);
	return $url_serveur;
}
?>
