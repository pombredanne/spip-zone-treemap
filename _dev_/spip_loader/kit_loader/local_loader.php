<?php
header('Content-Type: text/plain');

echo '<' . "?php
	/********************************* Définitions propres à local_loader.php */

	define('_LOCAL_URL', 'http" .
	(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 's' : '') .
	'://' . $_SERVER['HTTP_HOST'] .
	 substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], 'INSTALL/')) . "');\n";
?>
	// Adresse des librairies necessaires a spip_loader (pclzip et fichiers de langue)
	define('_URL_LOADER_DL', _LOCAL_URL . 'DISTRIB/');
	define('_URL_PAQUET_ZIP', _URL_LOADER_DL . 'spip.zip');
	define('_SPIP_LOADER_SCRIPT', basename($_SERVER['PHP_SELF']));
	error_reporting(E_ALL ^ E_NOTICE);

	/********************************* Fin local_loader.php */

<?php

$lines = file('../DISTRIB/spip_loader.php.txt');
unset($lines[0]);
echo implode('', $lines);

?>
