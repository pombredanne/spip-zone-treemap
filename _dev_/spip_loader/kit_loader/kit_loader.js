/*
 * Kit_loader: hoster jQuery pour enchainer les phases d'install avec historique
 *
 * Auteur : bertrand@toggg.com
 * © 2007 - Distribue sous licence LGPL
 *
 * Au premier appel de kit_loader.php, installation du hoster et controle des chargements suivant
 *
 */

var spiploader =  new spipLoader();
 
jQuery( function() {
	spiploader.init();
});

// callbacks des chargements faits en ajax
var spiploader_ajax =
{
	"beforeSend":function(xml) {
					jQuery(":input", spiploader.hoster).each(function() {
					    this.disabled = "disabled";
					});					
				},
	"success":function(d){
				    spiploader.addHistoire(
				    	"succès étape" + spiploader.compteur++ + spiploader.hoster.html(),
				    	"retour" + spiploader.compteur );
					var hote = d.match(/<body[^>]*>((.*|\n)*)<\/body>/i);
/*
// Dommage, on ne chope que le premier ...
// comment re-armer l'ajax apres le flush de inc/import.php ?
// en attendant, le self() de inc/import.php remplace par beton "spip.php"
					var ehote = hote[1].match(
						/(window\.setTimeout\('location\.href="[^"]*";',)(\d+)\);/);
					if (ehote) {
						alert(ehote[1]+"////"+ehote[2]);
					}
*/
					hote = hote ? hote[1].replace(
						/(window\.setTimeout\('location\.href=")[^"]*(";',)(\d+)\);/,
						"$1spip.php$2 $3);") : "Reply has no body ...";
					spiploader.hoster.html(hote);
					spiploader.setSubmit();
				}
}

// le prototype loader
function spipLoader(options)
{
	this.compteur = 1;
    for (opt in options) {
        this[opt] = options[opt];
    }
}
spipLoader.prototype.init = function()
{
	// creer l'historique en dessous
	this.histoire = jQuery('<div id="sl_histoire"><h3>Historique</h3></div>');
	this.topHistoire = this.histoire.find("h3");
    this.addHistoire("Lancement de l'installation");

	// la colonne de gauche avec timer, phases et etapes et la pub
	this.side = jQuery('<div id="sl_side"><h3>SPIP kit_loader</h3>\
		<p id="sl_now">0/d&eacute;marrage</p><div id="sl_timer"></div></div>');
	this.topSide = this.side.find("#sl_timer");
	this.now = jQuery("#sl_now", this.side);

	// recuperer, nettoyer et ranger la pub que kit_loader.php a pris sur le zine
	this.pub = jQuery("#sl_pub");
	// arranger l'url des images, completer le lien et le destiner a une autre fenetre
	this.pub.find("li a img").each(function() {
		this.src = "http://zine.spip.org/" + this.src.match(/IMG.*$/)[0];
		jQuery(this).after("&nbsp;" +
			jQuery(this).parent().attr({"target":"blank"}).attr("title").replace(/www\./, ''));
	});
	jQuery("h2 a", this.pub).attr({"target":"blank"});
	// deplacer la pub en colonne gauche
	this.side.append(this.pub);

	// hop! on replace tout
	var hote = jQuery("body").html();
	jQuery("body").html(
		'<div id="sl_hoster">' + hote + '</div>');
	this.hoster = jQuery("#sl_hoster");
	jQuery("body").prepend(this.side).append(this.histoire)
		.append("<div style='display:none'><a id='final' href='spip.php'> </a></div>");

	// creer le timer (juste l'installer)
	this.timer = new Delai({'id':'sl_timer'});

	// finalement, on demarre, accrocher les hooks et submits auto
	this.setSubmit();
}
spipLoader.prototype.addHistoire = function(txt, id)
{
	this.topHistoire.after(
		'<div class="sl_histoire_elt"' +
		(id ? ' id="' + id + '"' : '') +
		'><h5>' + new Date().toLocaleString() +
		'</h5><p>' + txt + '</p></div>');
}
spipLoader.prototype.setSubmit = function()
{
	var taux = jQuery("#taux").text();
	if (taux) {
		this.taux = parseFloat(taux);
	}
	
	this.forme = jQuery("form[input[@type=submit]]", this.hoster).eq(0);
	this.phase = 0;
	this.etape = "";
	if (this.forme.length) {
		// attention y a des input nommés action dans spip ...
		var ici = this.forme.attr("action").match(
			/\w+_loader\.php\?action=(\w+)|action=(test_dirs)|(exec=install)/);
		if (ici) {
			while (!ici[++this.phase]);
			this.etape = ici[3] ? this.forme[0].etape.value : ici[1] ? ici[1] : ici[2];
			this.topSide.after('<p class="downside">' + this.now.html() + '</p>');
			this.now.html(this.phase + '/' + this.etape);
			this.timer.cancel();
			if (ici[1] && this.taux) {
				this.timer.start(parseInt(1000*cfg.etapes[this.etape][4]/this.taux));
			}
			if (this.etape == "2" || this.etape == "4") {
				this.forme.ajaxForm(spiploader_ajax);
			} else {
				if (this.etape == "6") {
					this.forme[0].nom.value = "admin";
					this.forme[0].login.value = "admin";
					this.forme[0].pass.value = "admin";
					this.forme[0].pass_verif.value = "admin";
				}
				this.forme.ajaxSubmit(spiploader_ajax);
				return;
			}
		} else { // ici, on devrait etre a la dernier etape
			alert("dernier ?");
			this.forme.submit();
			return;
		}
	} else { // fini, on redirige sur spip
		var urlspip = window.location.href.toString().replace(/kit_loader\.php.*/g, "spip.php");
		window.location.href = urlspip;
//		jQuery("#final").click();
		return;
	}

	// menus de changement de langue, juste recharger la page
	jQuery("select[@name=lang], select[@name=var_lang_ecrire]", this.hoster)
	  .each( function() {
		this.onchange=function() {jQuery(this.form).submit();};
		jQuery(this.form)
			.ajaxForm(
			{"success":function(d){
				var hote = d.match(/<body[^>]*>((.*|\n)*)<\/body>/i);
				spiploader.hoster.html(hote[1]);
				spiploader.setSubmit();
			}})
		});
}

// le temps qui passe
tabDelais = {};
function Delai(options)
{
	// timer rafraichi toutes les demi secondes
	this.interval = 500;
	// affichage: diviseurs successifs avec la chaine ajoutee a gauche si pas fini
	this.show = [10, '', 100, '.', 60, ':', 60, ':', 24, '&nbsp;'];
    for (opt in options) {
        this[opt] = options[opt];
    }
    // l'element ou on va poser le temps
    this.elt = jQuery("#" + this.id);
    
    // enregistre dans le tableau general des delais pour le setTimeout()
    tabDelais[this.id] = this;
}

// Demarrer le timer
Delai.prototype.start = function(delai)
{
    this.cancel();
	this.delai = delai;
    this.created = new Date();
    this.fin = this.created.valueOf() + this.delai;
    this.reste();
}

// Arreter le timer (ca ne l'efface pas)
Delai.prototype.cancel = function()
{
    if (this.timeout) {
		clearTimeout(this.timeout);
	}
	this.timeout = false;
}

// la cheville ouvriere qui fait l'affichage rafraichi
Delai.prototype.reste = function()
{
	// afficher le temps qui passe
    this.elt.html(delai2string(parseInt(this.fin - new Date().valueOf()), this.show));
    
    // (re)armer le timeout
    this.timeout = setTimeout('tabDelais["'+this.id + '"].reste();', this.interval);
}

// rend une chaine d'apres des millisecondes
// et un tableau de diviseurs + affichages intercalaires
// le premier diviseur donne la precision, 1 pour rester en millisecondes
function delai2string(delai, show)
{
	delai = parseInt(delai / show[0]);
    var sign = '&nbsp;';
    if (delai < 0 ) {
        delai = -delai;
        sign = '-';
    }
    var out = '';
    for (var i = 2; i < show.length && delai > 0; i += 2) {
    	var bout = '' + (delai % show[i]);
    	var large = '' + (show[i] - 1);
    	while (bout.length < large.length) {
    		bout = '0' + bout;
    	}
	    delai = parseInt(delai / show[i]);
    	out = bout + show[i - 1] + out;
    }
    return sign + out;
}
