<?php
/*
 * Kit_loader: empaquetage de spip_loader pour enchainer plusieurs installations
 *
 * Auteur : bertrand@toggg.com
 * © 2007 - Distribue sous licence LGPL
 *
 * Charge une suite de .zip et quelques scripts en extra (hoster kit_loader.js)
 *
 */

// les différents zip à charger
// etape => array( sous-dossier, nom paquet, path initial a virer, prochaine etape)
// kitspip en premier pour pas faire trop attendre
// extra doit etre apres spip car il écrase les fichiers correspondant de spip
$etapes = array(
	'init' => array('INSTALL/', 'kitspip', 'spip', 'spip'),
	'spip'  => array('DISTRIB/', 'spip', 'spip', 'extra'),
	'extra' => array('CONFIG/', 'spip_extra', 'spip', '')
);

// definir l'url du serveur de zip et scripts
define('_SERVEUR_URL', 'http' .
	(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 's' : '') .
	'://' . $_SERVER['HTTP_HOST'] .
	 substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], 'INSTALL/')));

// incorporer les tailles pour les count down
$basdir = dirname(dirname(__FILE__));
foreach ($etapes as $zip => $desc) {
	$etapes[$zip][4] = filesize($basdir . '/' . $desc[0] . $desc[1] . '.zip');
}

$cfg = var2js(array(
	'_SERVEUR_URL' => _SERVEUR_URL,
	'basdir' => $basdir,
	'etapes' => $etapes
));

$etapes = var_export($etapes, true);

// on renvoit du texte, un script PHP
header('Content-Type: text/plain');

echo '<' . "?php
	/********************************* Définitions propres à kit_loader.php */

	define('_SERVEUR_URL', '" . _SERVEUR_URL . "');
	// les différents zip à charger
	// etape => array( sous-dossier, nom paquet, path initial a virer, prochaine etape)
	 \$etapes = $etapes;
";
?>
	error_reporting(E_ALL ^ E_NOTICE);

	// Adresse des librairies necessaires a spip_loader (pclzip et fichiers de langue)
	define('_URL_LOADER_DL', _SERVEUR_URL . 'DISTRIB/');

	if (isset($_REQUEST['action'])) {
		$action = $_REQUEST['action'];
	} else {
		$pub = recuperer_page("http://zine.spip.org/?page=kit-boussole");
		$action = 'init';
	}
	define('_SPIP_LOADER_SCRIPT', basename($_SERVER['PHP_SELF']) . '?action=' . $action);

	// en fonction de action constantes de l'etape courante
	define('_NOM_PAQUET_ZIP', $etapes[$action][1]);
	define('_URL_PAQUET_ZIP', _SERVEUR_URL . $etapes[$action][0] . _NOM_PAQUET_ZIP . '.zip');
	define('_REMOVE_PATH_ZIP', $etapes[$action][2]);
	if ($etapes[$action][3]) {
		define(	'_SPIP_LOADER_URL_RETOUR',
			basename($_SERVER['PHP_SELF']) . '?action=' . $etapes[$action][3]);
	}

	// pour court-circuiter le "En travaux"
	define('_FILE_CONNECT', 'kit_loader');
	// pour le wrapper "historique"
	define('_SPIP_LOADER_EXTRA',
		'../INSTALL/kit_loader.css,kit_loader_jquery.js,kit_loader_form.js,../INSTALL/kit_loader.js'); // ../INSTALL/kit_loader_back.php,

	/********************************* Fin kit_loader.php */
<?php
// incorporer spip_loader
$lines = file('../DISTRIB/spip_loader.php.txt');
// enlever le tag php
unset($lines[0]);
for ($i = count($lines); --$i; ) {
	// ajouter les scripts necessaires au phases d'install
	if (strpos($lines[$i], '</head>') !== false) {
		$lines[$i] =
		 "<script type='text/javascript' src='dist/javascript/spip_barre.js'></script>\n" .
		 "<script type='text/javascript' src='dist/javascript/md5.js'></script>\n" .
		 "<script type='text/javascript'>
		 var cfg = " . $cfg . ";
		 cfg.orig_HTTP_USER_AGENT = '" . $_SERVER['HTTP_USER_AGENT'] . "';
		 </script>\n" .
		 $lines[$i];
		break;
	}
	// ne nettoyer que le zip tant qu'on n'est pas au dernier
	if (strpos($lines[$i], '@unlink($dir_base.$fichier);') !== false) {
		$lines[$i] .= "
		if (strpos(_SPIP_LOADER_URL_RETOUR, 'kit_loader') !== false) {
			return true;
		}\n";
		continue;
	}
	// un petit peu de pub ne nuit pas
	if (strpos($lines[$i], '</body>') !== false) {
		$lines[$i] = "<div id=\"sl_pub\">' . \$GLOBALS['pub'] . '</div>\n" . $lines[$i];
		continue;
	}
}
echo implode('', $lines);

/**
    * Transform a variable into its javascript equivalent (recursive)
    * @access private
    * @param mixed the variable
    * @return string js script | boolean false if error
    */
function var2js($var) {
    $asso = false;
    switch (true) {
        case is_null($var) :
            return 'null';
        case is_string($var) :
            return '"' . addcslashes($var, "\"\\\n\r") . '"';
        case is_bool($var) :
            return $var ? 'true' : 'false';
        case is_scalar($var) :
            return $var;
        case is_object( $var) :
            $var = get_object_vars($var);
            $asso = true;
        case is_array($var) :
            $keys = array_keys($var);
            $ikey = count($keys);
            while (!$asso && $ikey--) {
                $asso = $ikey !== $keys[$ikey];
            }
            $sep = '';
            if ($asso) {
                $ret = '{';
                foreach ($var as $key => $elt) {
                    $ret .= $sep . '"' . $key . '":' . var2js($elt);
                    $sep = ',';
                }
                return $ret ."}\n";
            } else {
                $ret = '[';
                foreach ($var as $elt) {
                    $ret .= $sep . var2js($elt);
                    $sep = ',';
                }
                return $ret ."]\n";
            }
    }
    return false;
}
