<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=sv
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Installera >>',

	// C
	'ce_repertoire' => 'den här katalogen',

	// D
	'donnees_incorrectes' => '<h4>Felaktig uppgift. Var vänlig
försök igen eller använd den manuella installationen istället.</h4>
  <p>Fel : @erreur@</p>',
	'du_repertoire' => 'katalogen',

	// E
	'echec_chargement' => '<h4>Nedladdningen har misslyckats. Var vänlig
försök igen eller använd den manuella installationen istället.</h4>',

	// T
	'texte_intro' => '<p><b>Välkommen till den automatiska
installationen av @paquet@.</b> <p>Först kommer programmet att kontrollera
rättigheterna på den aktuella katalogen, sedan börjar den med nedladdningen av 
@paquet@ filerna till @dest@. <p>Var vänlig, klicka på knappen för att fortsätta.',
	'texte_preliminaire' => '<br /><h2>Förberedande steg: <b>Ställ in rättighetern för katalogen</b></h2>
 <p><b>Det är inte möjligt att skriva till den 
aktuella katalogen.</b></p>
 <p>För att ändra rättigheterna för katalogen
du försöker installera @paquet@ i, använd din FTP-klient.<br /> Hur du gör är beskrivet i detalj i installationshandledningen. Välj mellan:</p>
 <ul>
 <li><b>Om du har en grafisk FTP-klient</b>, sätt rättigheterna
för katalogen så att alla kan skriva till den.</li>
 <li><b>Om du har en FTP-klient med textgränssnitt</b>, ändra rättigheterna för katalogen till värdet @chmod@.</li>
 <li><b>Om du använder en telnet- eller ssh-klient</b>,
kör kommandot <i>chmod @chmod@  aktuell_katalog</i>.</li>
 </ul>
<p>När det är gjort, var vänlig <b><a href=\'spip_loader.php?charger=oui\'>uppdatera denna sida</a></b>
 för att starta nedladdningen och installera SPIP.</p>
 <p>Om du fortfarande får det här flemeddelandet, måste du använda den manuella installationen
 (Ladda ned SPIP med FTP) istället.</p>',
	'titre' => 'Ladda ned @paquet@'
);

?>
