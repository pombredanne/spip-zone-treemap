<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=ast
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Entamar l\'asitiamientu >>',

	// C
	'ce_repertoire' => 'd\'esti direutoriu',

	// D
	'donnees_incorrectes' => '<h4>Datos incorreutos. Vuelva a intentalo, o use l\'asitiamientu manual.</h4>
  <p>Fallu producíu: @erreur@</p>',
	'du_repertoire' => 'del direutoriu',

	// E
	'echec_chargement' => '<h4>Falló la carga. Vuelva a intentalo, o use l\'asitiamientu manual.</h4>',

	// T
	'texte_intro' => '<p><b>Afáyese nel procesu d\'asitiamientu automáticu de @paquet@.</b></p>
  <p>El sistema ya comprobó los permisos d\'acesu al direutoriu actual.
  Agora va entamar la descarga de los datos de @paquet@ dientro de @dest@.</p>
  <p>Calque nel botón siguiente pa continuar.</p>',
	'texte_preliminaire' => '<br /><h2>Previu: <b>Regular los derechos d\'accesu</b></h2>
  <p><b>El direutoriu actual nun tien accesu pa escritura.</b></p>
  <p>Pa igualo, usa\'l programa cliente FTP pa regular los derechos d\'accesu
  a esti direutoriu (direutoriu d\'instalación de @paquet@).<br />
  El procedimientu detalláu s\'esplica na guía d\'instalación. A escoyer:</p>
  <ul>
  <li><b>Si tienes un cliente FTP gráficu</b>, regula les propiedaes del direutoriu actual
  pa que tenga accesu d\'escritura pa toos.</li>
  <li><b>Si\'l cliente FTP ye en mou testu</b>, camuda el mou del direutoriu al valor @chmod@.</li>
  <li><b>Si tienes accesu Telnet</b>, fai un <i>chmod @chmod@ direutoriu_actual</i>.</li>
  </ul>
  <p>De magar tea fecha esta operación, podrás <b><a href=\'@href@\'>recargar esta páxina</a></b>
  pa entamar la descarga y darréu la instalación.</p>
  <p>Si siguiere l\'error, tendrás que pasar al procesu d\'instalación clásicu
  (carga de tolos ficheros per FTP).</p>',
	'titre' => 'Descarga de @paquet@'
);

?>
