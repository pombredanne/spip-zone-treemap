<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=oc_ni
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Començar l\'installacion >>',

	// C
	'ce_repertoire' => 'd\'aqueu repertòri',

	// D
	'donnees_incorrectes' => '<h4>Marridas donadas. Vorgatz tornar provar, ò utilisar l\'installacion manuala.</h4>
  <p>Error: @erreur@</p>',
	'du_repertoire' => 'dau repertòri',

	// E
	'echec_chargement' => '<h4>Lo cargament a capitat. Vorgatz tornar provar, ò utilisar l\'installacion manuala.</h4>',

	// T
	'texte_intro' => '<p><b>Benvenguda dins l\'installacion automatica de @paquet@.</b></p>
  <p>Lo systèma verifica en promier lu dreches d\'accès au repertòri corrent.
  Es de lançar aüra lo telecargament dei donadas @paquet@ dins aquest repertòri.</p>
  <p>Vorgatz clicar lo boton seguent per continuar.</p>',
	'texte_preliminaire' => '<br /><h2>Preliminari : <b>Reglar lu dreches d\'accès</b></h2>
  <p><b>Lo repertòri corrent es pas accessible en escritura.</b></p>
  <p>Per l\'arrengar, emplegatz lo voastre client FTP e cambiatz lu dreches d\'accès
  d\'aqueu repertòri (repertòri d\'installacion de @paquet@).<br />
  La guida d\'installacion explica en detalhs lo biais de far. De chausir :</p>
  <ul>
  <li><b>S\'avètz un client FTP grafic</b>, reglatz li proprietats dau repertòri corrent
  per que sigue accessible en escritura à toi.</li>
  <li><b>Se lo voastre client FTP es en mòde tèxto</b>, cambiatz lo mòde dau repertòri à la valor @chmod@.</li>
  <li><b>S\'avètz un accès Telnet</b>, faguètz un <i>chmod @chmod@ repertori_corrent</i>.</li>
  </ul>
  <p>Un còup aquò fach podretz <b><a href=\'spip_loader.php?charger=oui\'>tornar cargar aquesta pàgina</a></b>
  per lançar lo telecargament pi l\'installacion.</p>
  <p>Se l\'error demoara, vos caudrà emplegar lo biais d\'installacion classic e
  (encargament de toi lu fichièrs per FTP).</p>',
	'titre' => 'Descargament de @paquet@'
);

?>
