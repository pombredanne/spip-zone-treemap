<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=oc_lnc
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Començar l\'installacion >>',

	// C
	'ce_repertoire' => 'd\'aquel repertòri',

	// D
	'donnees_incorrectes' => '<h4>Donadas incorrèctas. Volgatz tornar assajar, o emplegar l\'installacion manuala.</h4>
  <p>Error: @erreur@</p>',
	'du_repertoire' => 'del repertòri',

	// E
	'echec_chargement' => '<h4>Lo cargament a abocat. Volgatz tornar assajar, o emplegar l\'installacion manuala.</h4>',

	// T
	'texte_intro' => '<p><b>Benvenguda dins l\'installacion automatica de @paquet@.</b></p>
  <p>D\'en primièr, lo système a verificat los dreches d\'accès al repertòri corrent,
  Ara va lançar lo telecargament de las donadas @paquet@ dintre @dest@.</p>
  <p>Volgatz clicar lo boton seguent per continuar.</p>',
	'texte_preliminaire' => '<br /><h2>Preliminari : <b>Reglar los dreches d\'accès</b></h2>
  <p><b>Lo repertòri corrent es pas accessible en escritura.</b></p>
  <p>Per o arrengar, emplegatz vòstre client FTP e cambiatz los dreches d\'accès
  d\'aquel repertòri (repertòri d\'installacion de @paquet@).<br />
  La guida d\'installacion explica en detalhs lo biais de far. De causir :</p>
  <ul>
  <li><b>S\'avètz un client FTP grafic</b>, reglatz las proprietats del repertòri corrent
  per que siá accessible en escritura a totes.</li>
  <li><b>Se vòstre client FTP es en mòde tèxt</b>, cambiatz lo mòde del repertòri a la valor @chmod@.</li>
  <li><b>S\'avètz avez un accès Telnet</b>, fasètz un <i>chmod @chmod@ repertori_corrent</i>.</li>
  </ul>
  <p>Un còp aquò fach pouiretz <b><a href=\'spip_loader.php?charger=oui\'>tornar cargar aquesta pagina</a></b>
  per lançar lo telecargament puèi l\'installacion.</p>
  <p>Se l\'error demòra, vos caldrà emplegar lo biais d\'installacion classic e
  (encargament de totes los fichiers per FTP).</p>',
	'titre' => 'Telecargament de @paquet@'
);

?>
