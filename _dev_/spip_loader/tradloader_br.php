<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=br
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Kregiñ da staliañ >>',

	// C
	'ce_repertoire' => 'ar c\'havlec\'h-mañ',

	// D
	'donnees_incorrectes' => '<h4>Direizh eo ar roadennoù. Klaskit en-dro! pe grit gant ar staliadur dre-zorn.</h4>
  <p>Ar fazi a zo bet : @erreur@</p>',
	'du_repertoire' => 'ar c\'havlec\'h',

	// E
	'echec_chargement' => '<h4>C\'hwitet eo ar c\'hargañ. Klaskit en-dro, pe grit gant ar staliañ dre-zorn</h4>',

	// T
	'texte_intro' => '<p><b>Degemer mat war benveg staliañ emgefreek @paquet@.</b></p>
  <p>Gwiriet eo bet an aotreoù da dizhout ar c\'havlec\'h red,
  ha bremañ e vo pellgarget roadennoù @paquet@ e @dest@.</p>
  <p>Klikit war an nozelenn da heul evit kenderc\'hel ganti.</p>',
	'texte_preliminaire' => '<br /><h2>A-raok pep tra : 
  <b>Renkañ ar gwirioù dont tre</b></h2>
  <p><b>N\'eus ket tu skrivañ war ar c\'havlec\'h red.</b></p>
  <p>Implijit ho meziant FTP evit renkañ se dre gemmañ ar gwirioù dont tre
war ar c\'havlec\'h-mañ (an hini a staliit @paquet@ warnañ).<br />
  Dre ar munud eo displeget penaos ober er sturlevr staliañ. Da zibab :</p>
  <ul>
  <li><b>Ma rit gant ur meziant FTP grafek</b>, kemmit perzhioù ar c\'havlec\'h red
a-benn ma c\'hellfe forzh piv skrivañ warnañ.</li>
  <li><b>Ma rit gant ur meziant FTP e mod testenn</b>, roit an talvoud @chmod@ d\'an teul.</li>
  <li><b>Ma rit gant TelNet</b>, grit <i>chmod @chmod@ repertoire_courant</i>.</li>
  </ul>
  <p>Pa vo bet graet an dra-se e vo tu deoc\'h <b><a href=\'spip_loader.php?charger=oui\'>adkargañ ar bajennad-mañ</a></b>
  a-benn kregiñ gant ar pellgargañ, hag ar staliañ.</p>
  <p>Ma chom ar fazi e vo dav deoc\'h ober gant an doare klasel da staliañ
  (pellgargañ pep tra dre FTP).</p>',
	'titre' => 'Pellgargañ @paquet@'
);

?>
