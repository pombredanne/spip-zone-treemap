<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=zh_tw
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => '開始安裝 >>',

	// C
	'ce_repertoire' => '該目錄',

	// D
	'donnees_incorrectes' => '<h4>資料錯誤。請重新開始，或是自行手動安裝。</h4>
  <p>產生的錯誤 : @erreur@</p>',
	'du_repertoire' => '目錄',

	// E
	'echec_chargement' => '<h4>Le chargement a échoué. Veuillez réessayer, ou utiliser l\'installation manuelle.</h4>', # NEW

	// T
	'texte_intro' => '<p><b>Bienvenue dans la procédure d\'installation automatique de @paquet@.</b></p>
  <p>Le système a vérifié les droits d\'accès au répertoire courant.
  Il va lancer maintenant le téléchargement des données @paquet@ à l\'intérieur @dest@.</p>
  <p>Veuillez appuyer sur le bouton suivant pour continuer.</p>', # NEW
	'texte_preliminaire' => '<br /><h2>Préliminaire : <b>Régler les droits d\'accès</b></h2>
  <p><b>Le répertoire courant n\'est pas accessible en écriture.</b></p>
  <p>Pour y remédier, utilisez votre client FTP afin de régler les droits d\'accès
  à ce répertoire (répertoire d\'installation de @paquet@).<br />
  La procédure est expliquée en détail dans le guide d\'installation. Au choix :</p>
  <ul>
  <li><b>Si vous avez un client FTP graphique</b>, réglez les propriétés du répertoire courant
  afin qu\'il soit accessible en écriture pour tous.</li>
  <li><b>Si votre client FTP est en mode texte</b>, changez le mode du répertoire à la valeur @chmod@.</li>
  <li><b>Si vous avez un accès Telnet</b>, faites un <i>chmod @chmod@ repertoire_courant</i>.</li>
  </ul>
  <p>Une fois cette manipulation effectuée, vous pourrez <b><a href=\'spip_loader.php?charger=oui\'>recharger cette page</a></b>
  afin de commencer le téléchargement puis l\'installation.</p>
  <p>Si l\'erreur persiste, vous devrez passer par la procédure d\'installation classique
  (téléchargement de tous les fichiers par FTP).</p>',
	'titre' => '下載套件 @paquet@'
);

?>
