<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=lb
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Installatioun ufänken >>',

	// C
	'ce_repertoire' => 'vun dësem Dossier',

	// D
	'donnees_incorrectes' => '<h4>Falsch Daten. Probéiert nach eng Kéier, oder benotzt déi manuell Installatioun.</h4>
<p>Fehler: @erreur@</p>',
	'du_repertoire' => 'vum Dossier',

	// E
	'echec_chargement' => '<h4>D\'Lueden huet nët fonktionnéiert. Probéiert nach eng Kéier, oder benotzt déi manuell Installatioun.</h4>',

	// T
	'texte_intro' => '<p><b>Wëllkom bei der automatëscher Installatiouns-Prozedur vun @paquet@.</b>
<p>De System huet d\'Zougangs-Rechter um aktuellen Dossier kontrolléiert.
Hien lued elo d\'@paquet@-Daten an den Dossier @dest@.</p>
<p>Drëckt op den Knäppchen "Weider".</p>',
	'texte_preliminaire' => '<br /><h2>Fir d\'éischt: <b>Zougangs-Rechter astellen</b><h2>
<p><b>Den aktuellen Dossier kann nët beschriwen ginn.</b></p>
<p>Fir dat ze verbesseren, benotzt ären FTP-Client fir d\'Zougansrechter vun dësem Dossier anzestellen (wou @paquet@ installéiert gëtt).
D\'Prozedur ass am Detail am Installatiouns-Guide beschriwen. Zum Beispill:</p>
<ul>
<li><b>Wann dir e graphëschen FTP-Client hutt</b>, regléiert d\'Rechter vum aktuellen Dossier sou dat en fir jiddfereen beschreiwbar ass.</li>
<li><b>Wann dir een FTP-Client mat Text-Modus hutt</b>, ännert d\'Rechter vum Dossier op d\'Valeur @chmod@.</li>
<li><b>Wann dir Telnet fuert</b>, maacht een <i>chmod @chmod@ aktuellen_dossier</i>.</li>
</ul>
<p>Wann dës Ännerung gemaach ass, da kënnt dir <b><a href=\'spip_loader.php?charger=oui\'>dës Säit nei lueden</a></b>
an d\'Installatioun ufänken.</p>
<p>Wann de Fehler bestoë bleiwt da musst dir déi klassësch Installatiouns-Prozedur benotzen (all d\'Dateien per FTP op de Server lueden).</p>',
	'titre' => '@paquet@ lueden'
);

?>
