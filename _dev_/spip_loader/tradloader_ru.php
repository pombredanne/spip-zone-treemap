<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=ru
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Установить >>',

	// C
	'ce_repertoire' => 'эта папка',

	// D
	'donnees_incorrectes' => '<h4>Неправильная информация. Попробуйте еще раз или попробуйте установить SPIP вручную.</h4>
<p>Ошибка : @erreur@</p>',
	'du_repertoire' => 'папка',

	// E
	'echec_chargement' => '<h4>Не удалось скачать установочный файл. Попробуйте позже или установите SPIP вручную.</h4>',

	// T
	'texte_intro' => '<p><b>Добро пожаловать в систему автоматической установки @paquet@.</b> <p>Сначала программа устанвоки проверит права доступа на текущую папку и попробует автоматически скачать установочный файл @paquet@ в  @dest@. <p>Нажмите на кнопку для начала установки.',
	'texte_preliminaire' => '<br /><h2>Préliminaire : <b>Régler les droits d\'accès</b></h2>
  <p><b>Le répertoire courant n\'est pas accessible en écriture.</b></p>
  <p>Pour y remédier, utilisez votre client FTP afin de régler les droits d\'accès
  à ce répertoire (répertoire d\'installation de @paquet@).<br />
  La procédure est expliquée en détail dans le guide d\'installation. Au choix :</p>
  <ul>
  <li><b>Si vous avez un client FTP graphique</b>, réglez les propriétés du répertoire courant
  afin qu\'il soit accessible en écriture pour tous.</li>
  <li><b>Si votre client FTP est en mode texte</b>, changez le mode du répertoire à la valeur @chmod@.</li>
  <li><b>Si vous avez un accès Telnet</b>, faites un <i>chmod @chmod@ repertoire_courant</i>.</li>
  </ul>
  <p>Une fois cette manipulation effectuée, vous pourrez <b><a href=\'@href@\'>recharger cette page</a></b>
  afin de commencer le téléchargement puis l\'installation.</p>
  <p>Si l\'erreur persiste, vous devrez passer par la procédure d\'installation classique
  (téléchargement de tous les fichiers par FTP).</p>', # NEW
	'titre' => 'Скачать @paquet@'
);

?>
