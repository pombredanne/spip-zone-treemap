<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=sk
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Spustiť inštaláciu >>',

	// C
	'ce_repertoire' => 'z tohto priečinka',

	// D
	'donnees_incorrectes' => '<h4>Nesprávne údaje. Prosím,
skúste to znova alebo namiesto toho využite manuálnu inštaláciu.</h4>
  <p>Chyba: @erreur@</p>',
	'du_repertoire' => 'priečinka',

	// E
	'echec_chargement' => '<h4>Sťahovanie sa nepodarilo. Prosím,
skúste to znova alebo namiesto toho využite manuálnu inštaláciu.</h4>',

	// T
	'texte_intro' => '<p><b>Vitajte pri procese
automatickej inštalácie modulu @paquet@.</b> <p>Program najprv skontroluje povolenia
aktuálneho priečinka a potom začne sťahovať
súbory modulu @paquet@ do priečinka @dest@. <p>Prosím, kliknite na tlačidlo, aby ste mohli pokračovať.',
	'texte_preliminaire' => '<br /><h2>Predprípravný krok: <b>Nastavenie prístupových povolení</b></h2>
 <p><b>Do aktuálneho priečinka sa
nedá zapisovať.</b></p>
 <p>Na zmenu povolení priečinka,
v ktorom inštalujete @paquet@, použite svojho FTP klienta.<br /> Postup je podrobne popísaný v inštalačnej príručke. Vyberte si medzi:</p>
 <ul>
 <li><b>Ak máte FTP klienta s grafickým rozhraním,</b> nastavte povolenia
priečinka, aby sa otvoril pre každého, kto chce doňho zapisovať.</li>
 <li><b>Ak máte FTP klienta s textovým rozhraním,</b> zmeňte povolenia priečinka na hodnotu @chmod@.</li>
 <li><b>Ak využívate prístup cez Telnet,</b>
vykonajte príkaz <i>chmod @chmod@  current_directory.</i></li>
 </ul>
<p>Keď to urobíte, prosím <b><a href=\'spip_loader.php?charger=oui\'>znova obnovte túto stránku,</a></b>
 aby ste mohli začať so sťahovaním a inštaláciou SPIPu.</p>
 <p>Ak stále dostávate toto hlásenie o chybe, inštaláciu budete musieť vykonať manuálnou metódou
 (stiahnuť súbory SPIPu cez FTP).</p>',
	'titre' => 'Stiahnuť @paquet@'
);

?>
