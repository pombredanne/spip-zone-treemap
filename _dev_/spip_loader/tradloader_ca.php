<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=ca
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Començar la instal·lació >>',

	// C
	'ce_repertoire' => 'd\'aquest directori',

	// D
	'donnees_incorrectes' => '<h4>Dades incorrectes. Si us plau, intenteu-ho de nou o feu servir la instal·lació manual.</h4>',
	'du_repertoire' => 'del directori',

	// E
	'echec_chargement' => '<h4>La descàrrega ha fallat. Si us plau, intenteu-ho de nou o feu servir la instal·lació manual.</h4>',

	// T
	'texte_intro' => '<p><b>Benvingut al procés d\'instal·lació automàtic de @paquet@.</b>
  <p>El sistema ha verificat els drets d\'accés al directori actual,
  Ara començarà la baixada de dades @paquet@ a l\'interior @dest@.</p>
  <p>Premeu el següent botó per tal de continuar.</p>',
	'texte_preliminaire' => '<br /><h2>Preliminar: <b>Ajustar els drets d\'accés</b></h2>
  <p><b>El directori actual no té drets d\'escriptura.</b></p>
  <p>Per posar-hi remei, utilitzeu el vostre client FTP i ajusteu els drets d\'accés
  a aquest directori (carpeta d\'instal·lació de @paquet@).<br />
  El procés està explicat detalladament a la guia d\'instal·lació. A escollir:</p>
  <ul>
  <li><b>Si teniu un client FTP gràfic</b>, ajusteu les propietats del directori actual
  per tal que tots hi puguin escriure.</li>
  <li><b>Si el vostre client FTP és en mode text</b>, canvieu el mode del directori al valor @chmod@.</li>
  <li><b>Si teniu un accés Telnet</b>, feu un <i>chmod @chmod@ directori actual</i>.</li>
  </ul>
  <p>Un cop feta aquesta manipulació, podreu <b><a href=\'@href@\'>recarregar aquesta pàgina</a></b>
  per tal de començar la descàrrega i després la instal·lació.</p>
  <p>Si l\'error persisteix, haureu de passar al procés d\'instal·lació clàssic
  (descàrrega de tots els fitxers per FTP).</p>',
	'titre' => 'Descàrrega de @paquet@'
);

?>
