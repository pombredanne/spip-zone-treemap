<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=gl
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Comezar a instalación >>',

	// C
	'ce_repertoire' => 'deste cartafol',

	// D
	'donnees_incorrectes' => '<h4>Datos incorrectos. Faga un novo intento, ou utilice a instalación manual.</h4>
<p>Erro producido: @erreur@</p>',
	'du_repertoire' => 'do cartafol',

	// E
	'echec_chargement' => '<h4>A carga non vai. Inténteo de novo, ou utilice a instalación manual.</h4>  ',

	// T
	'texte_intro' => '<p><b>Benvido/a ao proceso de instalación automática de @paquet@.</b>
  <p>O sistema comprobou os permisos de acceso ao cartafol presente,
  agora, lanzará o proceso de carga dos datos de @paquet@  dentro deste cartafol.
  <p>Prema sobre o botón seguinte para continuar.',
	'texte_preliminaire' => '<br /><h2>Preliminar : 
  <b>Regrar os permisos de acceso</b></h2>
  <p><b>O directorio actual non é accesíbel para escritura.</b>
  <p>Para corrixilo, empregue o seu programa cliente de FTP co fin de establecer os permisos de acceso 
  para este cartafol (directorio de instalación de @paquet@).
  O procedemento está explicado detalladamente na guía de instalación. Escolla:<br />
<ul>
  <li><b>De ter un programa cliente de FTP gráfico</b>, estableza as propiedades do directorio presente
  para que sexa accesíbel á escritura para todos.
  <p>
  <li><b>De ter un cliente FTP en modo texto</b>, troque o modo do cartafol e estableza o valor 777.
    <p>
  <li><b>De ter un acceso Telnet</b>, faga un <i>chmod @chmod@ directorio_presente</i>.<p>
  </ul>
  <p>Logo de realizar este axuste, poderá <b><a href=\'spip_loader.php?charger=oui@hash@@id_auteur@\'>refrescar esta páxina</A></b>
  para comezar a carga e logo a instalación.</p>
  <p>Se o erro persiste, deberá pasar ao procedemento de instalación clásico
  (carga de todos os ficheiros por FTP).</p>',
	'titre' => 'Carga de @paquet@'
);

?>
