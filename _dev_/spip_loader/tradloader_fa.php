<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=fa
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'نصب را شروع كنيد',

	// C
	'ce_repertoire' => 'از این رپرتوار',

	// D
	'donnees_incorrectes' => '<h4>داده های نادرست.خواهشمند است دوباره امتحان کنید و یا اینکه از نصب دستی استفاده کنید.</h4>
<p>اشتباه: @erreur@</p>',
	'du_repertoire' => 'از رپرتوار',

	// E
	'echec_chargement' => '<h4>بارگذاری انجام نشد. خواهشمند است دوباره امتحان کنید و یا اینکه از نصب دستی استفاده کنید. </h4> ',

	// T
	'texte_intro' => '<p><b>به روند خودکار نصب @paquet@ خوش آمدید.</b></p>
 سیستم حقوق دسترسی به رپرتوار کنونی را وارسی کرد و بزودی داده های @paquet@ را در درون @dest@ بارگذاری میکند.<p> خواهشمند است دکمه ی زیر را برای ادامه ی روند فشار دهید.</p>',
	'texte_preliminaire' => '<br /><h2>گام نخست: <b> مجوزهاي دسترسي را تنظيم كنيد</b></h2>
<p><b>نوشتن در ديركتوري فعلي ناممكن است.</b></p>
<p> براي تغيير مجوز‌هاي ديركتوري‌اي كه مي‌خواهيد @paquet@ را در آن نصب كنيد از اف.تي.پي خود استفاده كنيد.<br />
روش كار با جزئيات در راهنماي تصوب گفته مي‌شود. انتخاب كنيد: </p>
<ul>
<li><b>اگر اف.تي.پي با ظاهر گرافيكي داريد</b>, مجوز‌هاي ديركتوري را طوري تنظيم كنيدكه براي نوشتن همه در آن باز باشد.</li>
<li><b>اگر اف.تي.پي شما ظاهر متني دارد</b>, مجوز‌هاي ديركتوري را به متغيير @chmod@ تعيير دهيد.</li>
<li><b> اگر يك ارتباط Telnet داريد</b>, اين فرمان را اجرا كنيد <i>chmod @chmod@ repertoire_courant</i>.</li>
  </ul>
<p>هنگامي كه اجرا شد، لطفاً <b><a href=\'@href@\'>اين صفحه را بازبارگذاي كنيد</a></b>
براي انيكه بارگذاري و نصب را آغاز كنيد.</p>
<p>اگر اخطار خطا را دريافت كرديد، لازم است از روش نصب دستي استفاده كنيد
(بارگذاري تمام فايل‌هاي اسپپ با اف.تي.پي)..</p>',
	'titre' => 'بارگذاری @paquet@'
);

?>
