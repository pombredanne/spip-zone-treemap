<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=nl
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Met de installatie beginnen >>',

	// C
	'ce_repertoire' => 'van deze lijst',

	// D
	'donnees_incorrectes' => '<h4>Incorrecte gegevens. Gelieve opnieuw proberen of de handinstallatie te gebruiken.</h4>
  <p>Geproduceerde fout:  @erreur@</p>',
	'du_repertoire' => 'van de lijst',

	// E
	'echec_chargement' => '<h4>De lading is niet geslaagd. Gelieve opnieuw proberen, of de handinstallatie te gebruiken.</h4>',

	// T
	'texte_intro' => '<p><b>Welkom in de procedure van automatische installatie van @paquet@.</b></p>
  <p>Het systeem heeft de rechten van toegang tot de lopende lijst gecontroleerd.
  Hij zal de download van de gegevens @paquet@ binnen nu lanceren @dest@.</p>
  <p>Gelieve op de volgende knoop te steunen om door te gaan.</p>',
	'texte_preliminaire' => '<br /><h2>Preliminairen : <b>De rechten van toegang regelen</b></h2>
  <p><b>De lopende lijst is niet toegankelijk in schrift.</b></p>
  <p>Om er te verhelpen, gebruikt uw klant FTP teneinde de rechten van toegang
 te regelen aan deze lijst (lijst van installatie van @paquet@).<br />
  De procedure wordt omstandig in de gids van installatie uitgelegd. Aan de keus :</p>
  <ul>
  <li><b>Als u een grafische klant FTP hebt</b>, de eigendommen van de lopende lijst regelt
  opdat hij toegankelijk in schrift voor iedereen is.</li>
  <li><b>Als uw klant FTP in manier tekst is</b>, verandert de manier van de lijst aan de waarde @chmod@.</li>
  <li><b>Als u een toegang Telnet hebt</b>, gedaan een <i>chmod @chmod@ repertoire_courant</i>.</li>
  </ul>
  <p>Eens deze uitgevoerde hantering, zult u <b><a href=\'spip_loader.php?charger=oui\'>deze bladzijde kunnen </a></b> opladen
   teneinde met de download te beginnen vervolgens de installatie.</p>
  <p>Als de fout voortduurt, zult u via de procedure van klassieke installatie moeten gaan
  (download van alle bestanden door FTP).</p>',
	'titre' => 'Download van @paquet@'
);

?>
