<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=cs
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Instalovat >>',

	// C
	'ce_repertoire' => 'tento adresář',

	// D
	'donnees_incorrectes' => '<h4>Chybné údaje. Prosím zkuste
znovu nebo použijte manuální instalaci.</h4>
  <p>Error : @erreur@</p>',
	'du_repertoire' => 'adresář',

	// E
	'echec_chargement' => '<h4>Stažení se nepovedlo. Prosím
zkuste znovu nebo použijte manuální instalaci.</h4>',

	// T
	'texte_intro' => '<p><b>Vítejte v automatické
instalační proceduře @paquet@u.</b> </p><p>Program nejprve zkontroluje
oprávnění pro aktuální adresář a potom do něj začne stahovat
instalační soubory @paquet@u.</p> <p>Pro pokračování stiskněte tlačítko.</p>',
	'texte_preliminaire' => '<br /><h2>Přípravný rok: <b>Nastavte
přístupová oprávnění</b></h2> <p><b>Není možné zapisovat do 
aktuálního adresáře.</b></p> <p>Pro změnu přístupových práv do adresáře
kam instalujete @paquet@, použijte svého FTP klienta. KOnkrétní postup je popsán
v Uživatelské příručce. Zvolte jeden postup:</p> <ul> <li><b>Pokud
máte grafického FTP klienta</b>, nastavte oprávnění tak,
aby do adresáře mohli všichni zapisovat.</li> <li><b>Pokud máte
textového FTP klienta</b>, nastavte přístupová oprávnění 
adresáře na hodnotu @chmod@.</li> <li><b>Pokud používáte telnet/ssh přístup</b>,
spusťte příkaz <i>chmod @chmod@  current_directory</i>.</li> </ul>
<p>Až budete hotovi,<b><a
href=\'spip_loader.php?charger=oui\'>obnovte tuto
stránku</a></b>, aby mohlo začít stahování a instalace @paquet@u. </p><p>Bude-li se
tato chyba opakovat,budete muset použít
manuální instalační postup (stáhnout instalační soubory pomocí FTP).</p>',
	'titre' => 'Probíhá stahování @paquet@'
);

?>
