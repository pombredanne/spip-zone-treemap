<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=ar
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'بدء التثبيت <<',

	// C
	'ce_repertoire' => 'هذا المجلد',

	// D
	'donnees_incorrectes' => '<h4>بيانات غير صالحة. الرجاء إعادة المحاولة أو استخدام التثبيت اليدوي.</h4>',
	'du_repertoire' => 'المجلد',

	// E
	'echec_chargement' => '<h4>فشل التحميل. الرجاء إعادة المحاولة او استخدام التثبيت اليدوي.</h4>',

	// T
	'texte_intro' => '<p><b>أهلاً بكم الى عملية تثبيت @paquet@ الآلية.</b></p>
  <p>تحقق النظام من امتيازات الوصول الى الدليل الجاري،
 سيباشر تحميل بيانات @paquet@ في داخل @dest@.</p>
  <p>الرجاء النقر على الزر التالي للمتابعة.</p>',
	'texte_preliminaire' => '<br /><h2>تمهيد:   <b>إعداد إمتيازات الدخول</b></h2>
  <p><b>الدليل الحالي لا يقبل امتياز الكتابة.</b></p>
  <p>لحل المشكلة، يجب استخدام برنامج FTP لتغيير امتيازات الدخول
الى هذا الدليل (دليل تثبيت @paquet@).<br />
  العملية مفسرة بالتفصيل في دليل التثبيت. يمكن اختيار:</p>
  <ul>
<li><b>اذا كان لديك برنامج FTP رسومي</b>، يجب إعداد خصائص المجلد الجاري
 لكي يسمح بالكتابة للجميع.</li>
<li><b>اذا كان برنامج FTP بواجهة نصية</b>، يجب تحديد امتيازات المجلد الجاري لتكون @chmod@.</li>
<li><b>اذا كان لديك وصول Telnet</b>، يجب ادخال <i>chmod @chmod@ المجلد الجاري</i>.</li>
  </ul>
  <p>بعد إتمام هذه العملية، يمكنك <b><a href=\'@href@\'>إعادة تحميل هذه الصفحة</a></b>
  للبدء بتحميل النظام وتثبيته.</p>
  <p>اذا استمر الخطأ، يجب العودة الى عملية التثبيت التقليدية
  (تحميل كل الملفات بواسطة FTP).</p>',
	'titre' => 'تحميل @paquet@'
);

?>
