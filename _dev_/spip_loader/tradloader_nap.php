<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=nap
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Accummenciate â nstallazzione >>',

	// C
	'ce_repertoire' => '\'e chistu repertorio',

	// D
	'donnees_incorrectes' => '<h4>\'E date nun songo currette. Pruvate n\'ata vota, o ausate \'a nstallazzione manuale.</h4>
  <p>Arrore produtto: @erreur@</p>',
	'du_repertoire' => 'd\'\'o repertorio',

	// E
	'echec_chargement' => '<h4>\'O carrecamento facette fetècchia. Pruvate n\'ata vota, o ausate â nstallazzione manuale.</h4>',

	// T
	'texte_intro' => '<p><b>Bemmenute â procedura \'e nstallazzione automateca \'e @paquet@.</b></p>
  <p>\'O sistema a valedato \'e deritte d\'acciesso ô repertorio currentt.
  Mommò accummenciarrà \'o scarrecamento d\'\'e date @paquet@ à l\'intérieur @dest@.</p>
  <p>sprimmite ô buttone ca vene pe ccuntinuà.</p>',
	'texte_preliminaire' => '<br /><h2>Préliminaire : <b>Régler les droits d\'accès</b></h2>
  <p><b>Le répertoire courant n\'est pas accessible en écriture.</b></p>
  <p>Pour y remédier, utilisez votre client FTP afin de régler les droits d\'accès
  à ce répertoire (répertoire d\'installation de @paquet@).<br />
  La procédure est expliquée en détail dans le guide d\'installation. Au choix :</p>
  <ul>
  <li><b>Si vous avez un client FTP graphique</b>, réglez les propriétés du répertoire courant
  afin qu\'il soit accessible en écriture pour tous.</li>
  <li><b>Si votre client FTP est en mode texte</b>, changez le mode du répertoire à la valeur @chmod@.</li>
  <li><b>Si vous avez un accès Telnet</b>, faites un <i>chmod @chmod@ repertoire_courant</i>.</li>
  </ul>
  <p>Une fois cette manipulation effectuée, vous pourrez <b><a href=\'spip_loader.php?charger=oui\'>recharger cette page</a></b>
  afin de commencer le téléchargement puis l\'installation.</p>
  <p>Si l\'erreur persiste, vous devrez passer par la procédure d\'installation classique
  (téléchargement de tous les fichiers par FTP).</p>',
	'titre' => 'Scarrecamento \'e @paquet@'
);

?>
