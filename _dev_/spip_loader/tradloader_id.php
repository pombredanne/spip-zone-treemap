<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=id
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Instal >>',

	// C
	'ce_repertoire' => 'direktori ini',

	// D
	'donnees_incorrectes' => '<h4>Data tidak benar. Silakan coba
sekali lagi atau gunakan prosedur instalasi manual bila perlu.</h4>
  <p>Kesalahan : @erreur@</p>',
	'du_repertoire' => 'direktori',

	// E
	'echec_chargement' => '<h4>Proses unduh gagal. Silakan
mencoba lagi atau gunakan prosedur instalasi manual.</h4>',

	// T
	'texte_intro' => '<p><b>Selamat datang di proses instalasi
otomatis @paquet@.</b> <p>Pertama-tama, program akan memeriksa
hak akses pada direktori sekarang dan kemudian akan memulai proses unduh
berkas @paquet@ ke @dest@. <p>Silakan klik tombol untuk melanjutkan proses.',
	'texte_preliminaire' => '<br /><h2>Langkah awal: <b>Set hak akses</b></h2>
 <p><b>Tidak dapat menulis di
direktori sekarang.</b></p>
 <p>Untuk mengubah hak akses direktori di mana
anda akan menginstal @paquet@ gunakan klien FTP anda.<br />Prosedur dijelaskan secara rinci dalam manual instalasi. Pilih antara:</p>
 <ul>
 <li><b>Jika anda memiliki sebiah klien FTP dengan tatap muka grafis</b>, set hak akses
direktori untuk membuatnya terbuka bagi setiap orang untuk menulisnya.</li>
 <li><b>Jika anda memiliki sebuah klien FTP dengan tatap muka teks</b>, ubah hak akses direktori ke nilai @chmod@.</li>
 <li><b>Jika anda menggunakan akses Telnet</b>,
jalankan perintah <i>chmod @chmod@  direktori_sekarang</i>.</li>
 </ul>
<p>Setelah ini dilakukan, silakan <b><a href=\'spip_loader.php?charger=oui\'>perbaharui halaman ini</a></b>
 untuk memulai proses unduh dan menginstal SPIP.</p>
 <p>Jika anda menerima sebuah notifikasi kesalahan, anda harus melakukan proses instalasi secara manual
 (mengunduh berkas SPIP melalui FTP).</p>',
	'titre' => 'Unduh @paquet@'
);

?>
