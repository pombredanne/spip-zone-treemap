<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=es
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Empezar la instalación >>',

	// C
	'ce_repertoire' => 'de esta carpeta',

	// D
	'donnees_incorrectes' => '<h4>Datos incorrectos. Vuelve a probar, o utiliza la instalación manual.</h4>
  <p>Erreur produite: @erreur@</p>',
	'du_repertoire' => 'de la carpeta',

	// E
	'echec_chargement' => '<h4>La descarga falló. Vuelve a probar, o utiliza la instalación manual.</h4>',

	// T
	'texte_intro' => '<p><b>Bienvenido o bienvenida al proceso de instalación automática de @paquet@.</b></p>
  <p>El sistema ha verificado primero los permisos de acceso a la carpeta actual.
  Ahora va a iniciar la descarga de los datos @paquet@ dentro de la carpeta @dest@.</p>
  <p>Pulse el botón siguiente para continuar.</p>',
	'texte_preliminaire' => '<br /><h2>Preliminar: 
  <b>Ajustar los derechos de acceso</b></h2>
  <p><b>La carpeta actual no está accesible en modo escritura.</b></p>
  <p>Para resolverlo, utiliza tu cliente FTP y ajusta los permisos de acceso
 a esta carpeta (carpeta de instalación de @paquet@).<br />
  El proceso está explicado con detalle en la guía de instalación. Según el caso:</p>
  <ul>
  <li><b>Si tienes un cliente FTP gráfico</b>, ajusta las propiedades de la carpeta actual
  para que esté accesible en escritura para todos.</li>
  <li><b>Si tu cliente FTP funciona en modo texto</b>, cambia el modo de la carpeta al valor @chmod@.</li>
  <li><b>Si tienes un acceso ssh o Telnet</b>, ejecuta un <i>chmod @chmod@ carpeta_actual</i>./li>
  </ul>
  <p>Una vez efectuado este cambio, podrás <b><a href=\'spip_loader.php?charger=oui\'>volver a cargar esta página</a></b>
  para empezar la descarga y luego la instalación.</p>
  <p>Si el error persiste, deberás pasar por el procedimiento de instalación clásico. 
  (subir todos los archivos por FTP).</p>', # MODIF
	'titre' => 'Descarga de @paquet@'
);

?>
