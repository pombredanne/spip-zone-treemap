<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=eo
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Komenci instalprocezon >>',

	// C
	'ce_repertoire' => 'de tiu dosierujo',

	// D
	'donnees_incorrectes' => '<h4>Datenoj ne korektaj. Bonvolu reprovi, aŭ faru instalprocezon mane. </h4>
  <p>Eraro okazinta: @erreur@</p>',
	'du_repertoire' => 'de la dosierujo',

	// E
	'echec_chargement' => '<h4>Ŝargo malsukcesis. Bonvolu reprovi, aŭ faru instalprocezon mane.</h4>',

	// T
	'texte_intro' => '<b>Bonvenon al la aŭtomata instalproceduro de SPIP.</b>
  <p>La sistemo ĵus kontrolis la alirrajtojn al la kuranta dosierujo,
  kaj tuj lanĉos la elŝuton de la SPIP-datenoj @paquet@ enen de tiu dosierujo.
  <p>Bonvolu musklaki la postan butonon por daŭrigi.',
	'texte_preliminaire' => '<br /><h2>Antaŭfaroj : <b>Agordu la alirratojn</b></h2>
  <p><b>La kuranta dosierujo ne estas skribe modifebla.</b></p>
  <p>Por tion ŝanĝi, uzu vian FTP-klienton por agordi alirrajtojn
  al tiu dosierujo (SPIP-dosierujo por instali @paquet@).<br />
  La proceduro estas detale priskribita en la instalgviddokumento. Laŭ elekto :</p>
  <ul>
  <li><b>Se vi havas grafikan FTP-klienton</b>, agordu la trajtojn de la kuranta dosierujo
  por ke ĝi estu skribe modifebla far ĉiuj.</li>
  <li><b>Se via FTP-kliento funkcias laŭ teksta modo</b>, ŝanĝu la alirstatuson de la dosierujo al la valoro @chmod@.</li>
  <li><b>Se vi havas Telnetan aliron</b>, tajpu <i>chmod @chmod@ kuranta_dosierujo</i>.<li>
  </ul>
  <p>Tiu faro plenumita, vi povos <b><a href=\'spip_loader.php?charger=oui\'>reŝargi tiun paĝon</a></b>
  por komenci la elŝuton kaj la instalprocezon.</p>
  <p>Se la eraro daŭras, vi ekprovu per klasika instalproceduro
  (elŝutado de ĉiuj dosieroj per FTP).</p>',
	'titre' => 'Elŝuto de @paquet@'
);

?>
