<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/tradloader?lang_cible=tr
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Kuruluma başla >>',

	// C
	'ce_repertoire' => 'bu dizinin',

	// D
	'donnees_incorrectes' => '<h4>Veriler hatalı. Lütfen tekrar deneyiniz veya manüel olarak (el ile) kurunuz.</h4>
  <p>Oluşan hata: @erreur@</p>',
	'du_repertoire' => 'dizininin',

	// E
	'echec_chargement' => '<h4>Yüklemede başarısız. hata oluştu.  Le chargement a échoué. Lütfen tekrar deneyiniz veya manüel olarak (el ile) kurunuz.</h4>',

	// T
	'texte_intro' => '<p><b>@paquet@ paketinin otomatik kurulumuna hoşgeldiniz.</b></p>
  <p>Sistem aktif dizine erişim haklarını kontrol etti.
  Şimdi @dest@ dizinine @paquet@ paketini indirme işlemini başlatacak.</p>
  <p>Lütfen devam etmek için "Sonraki" düğmesine basınız.</p>',
	'texte_preliminaire' => '<br /><h2>Ön bilgi : <b>Erişim haklarını düzenleyiniz</b></h2>
  <p><b>Aktif dizine yazma izni yok.</b></p>
  <p>Bu sorunu çözmek için FTP programınızı kullanarak 
  bu dizine erişim haklarını düzenleyiniz(@paquet@ paketinin kurulum dizini).<br />
  Yordam kurulum kitapçığında detaylı olarak anlatılmıştır. Tercihinize göre :</p>
  <ul>
  <li><b>eğer grafik tabanlı bir FTP programınız varsa</b>, aktif dizinin niteliklerini herkese yazma hakkı verecek biçimde ayarlayınız.</li>
  <li><b>Eğer metin tabanlı bir FTP programınız varsa dizinin modunu @chmod@ değerine getiriniz.</li>
  <li><b>Eğer Telnet erişiminiz varsa</b>, <i>chmod @chmod@ aktif_dizin</i> komutunu çalıştırınız.</li>
  </ul>
  <p>Bu işlemi yaptıktan sonra <b><a href=\'spip_loader.php?charger=oui\'>bu sayfayı yeniden yükle</a></b>
 komutuyla indirmeyi başlatıp sonra kurulumu yapabilirsiniz.</p>
  <p>Eğer hata tekrarlanırsa klasik kurulum yordamına geçmelisiniz
  (FTP ile tüm kurulum dosyalarının indirilmesi).</p>',
	'titre' => '@paquet@ paketinin indirilmesi'
);

?>
