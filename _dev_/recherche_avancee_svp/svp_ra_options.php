<?php

// Affiche notamment les donnees de la requete de recherche
//define('SVP_RA_DEBUG', true);

function svp_ra_upgrade($nom_meta_base_version, $version_cible){
	$current_version = "0.0";

	if (isset($GLOBALS['meta'][$nom_meta_base_version]))
		$current_version = $GLOBALS['meta'][$nom_meta_base_version];
		
	if ($current_version=="0.0") {
		ecrire_meta($nom_meta_base_version,$current_version=$version_cible);
		ecrire_meta('svp_etat_plugin', 
					serialize(array('stable', 'test', 'dev', 'experimental')));
	}
}

function svp_ra_vider_tables($nom_meta_base_version) {
	effacer_meta($nom_meta_base_version);
	effacer_meta('svp_etat_plugin');
}


?>
