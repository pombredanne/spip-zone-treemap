<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 */
function formulaires_recherche_avancee_charger_dist(){
	if ($GLOBALS['spip_lang'] != $GLOBALS['meta']['langue_site'])
		$lang = $GLOBALS['spip_lang'];
	else
		$lang='';

	$table_tout = array(0=>'tout');
	$cur_spip_version_str = spip_version();
	$table_spip_version = array(0=>substr($cur_spip_version_str, 0, 3));
	$valeurs = array(
		'lang' => $lang,
		'recherche' => _request('recherche'),
		'categories' => _request('categories') ? svp_ra_clean_tout(_request('categories')) : $table_tout,
//		'branches_spip' => _request('branches_spip') ? svp_ra_clean_tout(_request('branches_spip')) : $table_spip_version,
		'branches_spip' => _request('branches_spip') ? svp_ra_clean_tout(_request('branches_spip')) : $table_tout,
		'etats_plugin' => _request('etats_plugin') ? svp_ra_clean_tout(_request('etats_plugin')) : $table_tout,
		'depots' => _request('depots') ? svp_ra_clean_tout(_request('depots')) : $table_tout,
		'version_plugin' => _request('version_plugin'),
		'date_plugin' => _request('date_plugin'),
		'date_scope' => _request('date_scope') ? _request('date_scope') : 'avant',
		'langue_trad' => _request('langue_trad') ? _request('langue_trad') : (strlen($lang) && $lang!='fr' ? $lang : 'tout'),
	);

	return $valeurs;
}

/**
 */
function formulaires_recherche_avancee_verifier_dist(){
	$erreurs = array();
	return $erreurs;
}

/**
 */
function formulaires_recherche_avancee_traiter_dist(){
	include_spip('inc/svp_ra_rechercher');
	$retour = array('message_ok'=>array());

	$phrase = _request('recherche');
	$categories = _request('categories');
	$branches_spip = _request('branches_spip');
	$etats_plugin = _request('etats_plugin');
	$depots = _request('depots');

	$version_plugin = _request('version_plugin');
	$date_plugin = _request('date_plugin');
	$date_scope = _request('date_scope');
	$langue_trad = _request('langue_trad');

	$doublon = (_request('doublon') == 'oui') ? true : false;
	$tri = ($phrase) ? 'score' : 'nom';

	// On recupere la liste des paquets en recherche avancee
	$plugins = svp_rechercher_avancee_plugins_spip(
		$phrase, $categories, $etats_plugin, $depots, $branches_spip, false, false, $doublon, $tri,
		$langue_trad, array('date'=>$date_plugin, 'aperator'=>$date_scope), $version_plugin
	);

	// DEBUG
//	echo "<pre>".var_export($plugins,1)."</pre>";

	// Determination des messages de retour
	$ids_plugins = $ids_paquets = array();
	if ($plugins) {
		foreach($plugins as $plus_name=>$plug_infos) {
			$ids_plugins[] = $plug_infos['id_plugin'];
			$ids_paquets[] = $plug_infos['id_paquet'];
		}
	}
	$retour['message_ok']['num'] = count($plugins);
	$retour['message_ok']['plugins'] = $ids_plugins;
	$retour['message_ok']['paquets'] = $ids_paquets;
	$retour['message_ok']['dorecherche'] = 'oui';
	$retour['editable'] = true;

	$retour['message_ok']['debug_infos'] = "<pre>
phrase: ".var_export($phrase,1)."
categories: ".var_export($categories,1)."
branches_spip: ".var_export($branches_spip,1)."
etats_plugin: ".var_export($etats_plugin,1)."
depots: ".var_export($depots,1)."
version_plugin: ".var_export($version_plugin,1)."
date_plugin: ".var_export($date_plugin,1)." ($date_scope)
langue_trad: ".var_export($langue_trad,1)."
</pre>";

	return $retour;
}
?>
