<?php

if (!defined("_ECRIRE_INC_VERSION")) return;
include_spip('inc/plugin');
include_spip('inc/svp_rechercher');

// ----------------------- Recherche avancee de plugins ---------------------------------

function svp_rechercher_avancee_plugins_spip(
	$phrase, $categories=array(), $etats=array(), $depots=array(), $versions_spip=array(),
	$exclusions=array(), $afficher_exclusions=false, $doublon=false, $tri='nom',
	$langue_trad='', $date_infos=array(), $version_plugin=''
) {
	include_spip('inc/rechercher');
	
	$plugins = array();
	$scores = array();
	$ids_paquets = array();
	
	// Verif des tableaux
	if (!is_array($categories)) $categories = array(0=>$categories);
	if (!is_array($etats)) $etats = array(0=>$etats);
	if (!is_array($depots)) $depots = array(0=>$depots);
	if (!is_array($versions_spip)) $versions_spip = array(0=>$versions_spip);
	if (!is_array($date_infos)) $date_infos = array('date'=>$date_infos);

	// On prepare l'utilisation de la recherche en base SPIP en la limitant aux tables spip_depots, spip_plugins
	// et spip_paquets  si elle n'est pas vide
	if ($phrase) {
		$liste = liste_des_champs();
		$tables = array(
			'depot' => $liste['depot'], 'plugin' => $liste['plugin'], 'paquet' => $liste['paquet']
//			'plugin' => $liste['plugin'], 'paquet' => $liste['paquet']
		);
		$options = array('jointures' => true, 'score' => true);
	
		// On cherche dans tous les enregistrements de ces tables des correspondances les plugins qui
		// correspondent a la phrase recherchee
		// -- On obtient une liste d'id de plugins et d'id de paquets
		$resultats = array('plugin' => array(), 'paquet' => array());
		$resultats = recherche_en_base($phrase, $tables, $options);

		// -- On prepare le tableau des scores avec les paquets trouves par la recherche
		if ($resultats) {
			// -- On convertit les id de plugins en id de paquets
			$ids = array();
			if ($resultats['plugin']) {
				$ids_plugin = array_keys($resultats['plugin']);
				$where[] = sql_in('id_plugin', $ids_plugin);
				$ids = sql_allfetsel('id_paquet, id_plugin', 'spip_paquets', $where);
			}
			// -- On prepare les listes des id de paquet et des scores de ces memes paquets
			if ($resultats['paquet']) {
				$ids_paquets = array_keys($resultats['paquet']);
				foreach ($resultats['paquet'] as $_id => $_score) {
					$scores[$_id] = intval($resultats['paquet'][$_id]['score']);
				}
			}
			// -- On merge les deux tableaux de paquets sans doublon en mettant a jour un tableau des scores
			foreach ($ids as $_ids) {
				$id_paquet = intval($_ids['id_paquet']);
				$id_plugin = intval($_ids['id_plugin']);
				if (array_search($id_paquet, $ids_paquets) === false) {
					$ids_paquets[] = $id_paquet;
					$scores[$id_paquet] = intval($resultats['plugin'][$id_plugin]['score']);
				}
				else {
					$scores[$id_paquet] = intval($resultats['paquet'][$id_paquet]['score']) 
										+ intval($resultats['plugin'][$id_plugin]['score']);
				}
			}
		}
	}

	// Maintenant, on continue la recherche en appliquant, sur la liste des id de paquets,
	// les filtres complementaires : categorie, etat, exclusions et compatibilite spip
	// si on a bien trouve des resultats precedemment ou si aucune phrase n'a ete saisie
	// -- Preparation de la requete
	if (!$phrase OR $resultats) {
		$from = array('spip_plugins AS t1', 'spip_paquets AS t2', 'spip_depots AS t3');
		$select = array(
			't1.nom AS nom', 't1.slogan AS slogan', 't1.prefixe AS prefixe', 't1.id_plugin AS id_plugin', 
			't2.id_paquet AS id_paquet', 't2.description AS description', 
			't2.compatibilite_spip AS compatibilite_spip', 't2.traductions AS traductions',
			't2.auteur AS auteur', 't2.licence AS licence', 't2.etat AS etat',
			't2.logo AS logo', 't2.version AS version', 't2.nom_archive AS nom_archive',
			't2.date_modif AS date_modif', 't3.url_archives AS url_archives', 
		);
		$where = array('t1.id_plugin=t2.id_plugin', 't2.id_depot=t3.id_depot');

		if ($ids_paquets)
			$where[] = sql_in('t2.id_paquet', $ids_paquets);

		if (($categories) AND count($categories) AND ($categories[0] != 'tout'))
			$where[] = sql_in('t1.categorie', $categories);

		if (($etats) AND count($etats) AND ($etats[0] != 'tout'))
			$where[] = sql_in('t2.etat', $etats);

		if (($depots) AND count($depots) AND ($depots[0] != 'tout'))
			$where[] = sql_in('t2.id_depot', $depots);

		if (($langue_trad) AND strlen($langue_trad) AND $langue_trad!='tout')
			$where[] = 't2.traductions LIKE ' . sql_quote('%"'.$langue_trad.'"%');
		
		if (($version_plugin) AND strlen($version_plugin))
			$where[] = 't2.version REGEXP ' . sql_quote(
				($version_plugin[0]=='^' ? '' : '^')
				.addslashes(str_replace('*', '(.*)', $version_plugin))
				.($version_plugin[strlen($version_plugin)-1]=='$' ? '' : '(.*)$')
			);
		
		if ($exclusions AND !$afficher_exclusions)
			$where[] = sql_in('t2.id_plugin', $exclusions, 'NOT');

		if (($date_infos) AND count($date_infos) AND isset($date_infos['date'])) {
			list($_d, $_m, $_y) = explode('/', $date_infos['date']);
			$_op = '<=';
			if (isset($date_infos['operator']) AND $date_infos['operator']=='apres')
				$_op = '>=';
			$where[] = 'DATE(t2.date_modif) ' . $_op . sql_quote("$_y-$_m-$_d");
		}

		if (defined('SVP_RA_DEBUG') AND SVP_RA_DEBUG===true) {
			echo "<pre>
select: ", var_export($select,1), "
from: ", var_export($from,1), "
where: ", var_export($where,1), "
</pre>";
		}

		if ($resultats = sql_select($select, $from, $where)) {
			while ($paquets = sql_fetch($resultats)) {
				$prefixe = $paquets['prefixe'];
				$version = $paquets['version'];
				$nom = extraire_multi($paquets['nom']);
				$slogan = extraire_multi($paquets['slogan']);
				$description = extraire_multi($paquets['description']);
				$paquet_compatibilite_spip = $paquets['compatibilite_spip'];
				// Verif de versions SPIP
				if (strlen($paquet_compatibilite_spip) AND count($versions_spip) AND $versions_spip[0]!='tout') {
					$versionspip_ok = false;
					foreach($versions_spip as $vers)
						if (svp_verifier_compatibilite_spip($paquet_compatibilite_spip, $vers))
							$versionspip_ok = true;
				} else {
					$versionspip_ok = true;
				}
				// Si version compatible ... ok
				if ($versionspip_ok) {
					// Le paquet remplit tous les criteres, on peut le selectionner
					// -- on utilise uniquement la langue du site
					$paquets['nom'] = $nom;
					$paquets['slogan'] = $slogan;
					$paquets['description'] = $description;
					// -- on ajoute le score si on a bien saisi une phrase
					if ($phrase)
						$paquets['score'] = $scores[intval($paquets['id_paquet'])];
					else
						$paquets['score'] = 0;
					// -- on construit l'url de l'archive
					$paquets['url_archive'] = $paquets['url_archives'] . '/' . $paquets['nom_archive'];
					// -- on gere les exclusions si elle doivent etre affichees
					if ($afficher_exclusions AND in_array($paquets['id_plugin'], $exclusions))
						$paquets['installe'] = true;
					else
						$paquets['installe'] = false;
					// -- On traite les doublons (meme plugin, versions differentes)
					if ($doublon)
						// ajout systematique du paquet
						$plugins[] = $paquets;
					else {
						// ajout 
						// - si pas encore trouve 
						// - ou si sa version est inferieure (on garde que la derniere version)
						if (!$plugins[$prefixe]
						OR ($plugins[$prefixe] AND spip_version_compare($plugins[$prefixe]['version'], $version, '<'))) {
							$plugins[$prefixe] = $paquets;
						}
					}
				}
			}
		}
		
		// On trie le tableau par score décroissant ou nom croissant
		$fonction = 'svp_trier_par_' . $tri;
		if ($doublon)
			usort($plugins, $fonction);
		else
			uasort($plugins, $fonction);
	}
	
	return $plugins;
}

?>
