<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Produit automatiquement par le plugin LangOnet a partir de la langue source fr
// Module: svp
// Langue: fr

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C //
	'commentaire_select_multiple' => 'Champs multiples : utilisez la touche "Ctrl" pour s&#233;lectionner plusieurs lignes.',
	'champ_accepte_regex' => 'Ce champ accepte les expressions r&eacute;guli&egrave;res.',
	'champ_recherche_explication' => 'Ce champ sera recherch&eacute; dans les titres, descriptifs, auteurs et tout texte en g&eacute;n&eacute;ral des plugins. Les op&eacute;rateurs logiques sont accept&eacute;s.',
	'chaine_recherchee' => 'Recherche de',
	'chaine_version_recherchee' => 'Cha&icirc;ne de version',

// D //
	'date_scope_avant' => 'Jusqu\'&agrave; cette date',
	'date_scope_avant_ttl' => 'Rechercher les paquets jusqu\'&agrave; cette date',
	'date_scope_apres' => 'Depuis cette date',
	'date_scope_apres_ttl' => 'Rechercher les paquets depuis cette date',
	'date_de_recherche' => 'Date',

// E //
	'etat_plugin_stable' => 'stable',
	'etat_plugin_dev' => 'en d&eacute;veloppement',
	'etat_plugin_test' => 'en test',
	'etat_plugin_experimental' => 'experimental (!)',

// I //
	'info_lancer_rechercher' => 'Lancer la recherche &gt;',
	'info_recommencer' => 'Recommencer',
	'info_vider_champs' => 'Vide tous les champs de recherche',
	'info_rechercher' => 'Rechercher',
	'info_rechercher_categorie_version' => 'Informations concernant les fonctionnalit&eacute;s',
	'info_rechercher_depot' => 'Informations concernant le(s) d&eacute;p&ocirc;t(s)',
	'info_rechercher_categorie_infos_plugin' => 'Informations concernant le(s) paquet(s)',
	'info_rechercher_traduction' => 'Filtrer par traduction',

	'info_rechercher_chaine' => 'Terme(s) ou expression(s)',
	'info_rechercher_categories' => 'Filtrer par cat&eacute;gorie(s)',
	'info_rechercher_branches_spip' => 'Filtrer par branche(s) de SPIP',
	'info_rechercher_depots' => 'Filtrer par d&eacute;p&ocirc;t(s)',
	'info_rechercher_etats_plugin' => 'Filtrer par &eacute;tat(s) de d&eacute;veloppement',
	'info_rechercher_version_plugin' => 'Rechercher une version d\'un plugin',
	'info_rechercher_date_plugin' => 'Rechercher par date',
	'info_rechercher_date_scope' => 'Comment utiliser cette date ...',

// L //
	'le' => ' le ',

// O //
	'option_langues_toutes' => 'Toutes les langues',

// R //
	'recherche_avancee' => 'Recherche avanc&eacute;e',
	'rechercher_avancee' => 'Recherche avanc&eacute;e',
	'resultat_trouve' => '1 r&eacute;sultat trouv&eacute;',
	'resultats_trouves' => '@nb@ r&eacute;sultats trouv&eacute;s',
	'recherche_sans_resultat' => 'Recherche sans r&eacutesultat ...',
	'rappel_criteres' => 'Rappel de vos crit&egrave;res',

// T //
	'toutes_categories' => 'Toutes les cat&eacute;gories',
	'tous_etats' => 'Tous les &eacute;tats',
	'traduction_recherchee' => 'Plugin traduit en',

);
?>