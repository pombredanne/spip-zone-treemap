<?php

function trouver_chaine_etat($code=''){
	return _T('svp_ra:etat_plugin_'.$code);
}

function svp_ra_clean_tout ( $table=array(), $all=false ) {
	if (!is_array($table)) 
		$table = array($table);

	if (in_array('tout', $table)) {
		if ($all || (!$all && count($table)>1)) {
			$key = array_search('tout', $table);
			unset($table[$key]);
		}
	}	

	return $table;
}

?>
