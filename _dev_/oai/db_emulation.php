<?php

	class db_res {
		var $r;
		
		function db_res() {
		}
		
		function numRows() {
			return spip_num_rows($this->r);
		}

		function fetchRow($format='ignore', $limit=0) {
			if ($limit>1) {
				while ($limit-- > 0)
					spip_fetch_array($this->r);
			}

			return spip_fetch_array($this->r);
		}

		function getMessage() {
			return false;
		}
	}

	class DB {
		function DB() {
		}

		function query($query) {
			$res = new db_res;
			$res->r = spip_query($query);
			return $res;
		}
		
		function isError() {
			return false;
		}
	}

	$db = new DB;

?>