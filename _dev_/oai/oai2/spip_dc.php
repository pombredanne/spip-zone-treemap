<?
/*
* +----------------------------------------------------------------------+
* | PHP Version 4                                                        |
* +----------------------------------------------------------------------+
* | Copyright (c) 2002 Heinrich Stamerjohanns                            |
* |                                                                      |
* | dc_record.php -- Utilities for the OAI Data Provider                 |
* |                                                                      |
* | This is free software; you can redistribute it and/or modify it under|
* | the terms of the GNU General Public License as published by the      |
* | Free Software Foundation; either version 2 of the License, or (at    |
* | your option) any later version.                                      |
* | This software is distributed in the hope that it will be useful, but |
* | WITHOUT  ANY WARRANTY; without even the implied warranty of          |
* | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         |
* | GNU General Public License for more details.                         |
* | You should have received a copy of the GNU General Public License    |
* | along with  software; if not, write to the Free Software Foundation, |
* | Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA         |
* |                                                                      |
* +----------------------------------------------------------------------+
* | Derived from work by U. M�ller, HUB Berlin                           |
* |                                                                      |
* | Written by Heinrich Stamerjohanns, May 2002                          |
* /            stamer@uni-oldenburg.de                                   |
* +----------------------------------------------------------------------+
*/
//
// $Id: spip_dc.php 280 2005-02-04 13:20:18Z fil $
//

// this handles unqualified DC records, but can be also used as a sample
// for other formats.
// just specify the next variable according to your own metadata prefix
// change output your metadata records further below.

// please change to the according metadata prefix you use 
$prefix = 'oai_dc';

// you do need to change anything in the namespace and schema stuff
// the correct headers should be created automatically

$output .= 
'   <metadata>'."\n";

$output .= metadataHeader($prefix);

// please change according to your metadata format
$indent = 6;


## selectionner la langue (pour propre() et typo())
lang_select($record['lang']);

## elements spip directs
$output .= xmlrecord(typo(supprimer_numero($record['titre'])),
	'dc:title', '', $indent);

$output .= xmlrecord($record['lang'],
	'dc:language', '', $indent);

# les dates a reformater
$output .= xmlrecord(
	date("Y-m-d\\TH-M-i-s", strtotime($record['date'])),
	'dc:date', '', $indent);

## elements a aller chercher plus en profondeur dans la base

# auteurs
$auteurs=array();
$id_article = $record['id_article'];
$s = spip_query("SELECT aut.nom AS nom
	FROM spip_auteurs AS aut, spip_auteurs_articles AS lien
	WHERE lien.id_article=$id_article AND aut.id_auteur=lien.id_auteur
	ORDER BY nom");
while ($t=spip_fetch_array($s)) {
	$nom = typo($t['nom']);
	$output .= xmlrecord($nom,
		'dc:creator', '', $indent);

	## si on prefere, on peut mettre les spip_auteurs dans le dc:contributor
	#	$output .= xmlrecord($nom,
	#	'dc:contributor', '', $indent);
}

# description = #INTRODUCTION
$output .= xmlrecord(
	calcul_introduction('articles',
		$record['texte'], $record['chapo'], $record['descriptif']),
	'dc:description', '', $indent);

# mots-cles = on stocke tous les mots dans un petit tableau, tries par groupe
$mots=array();
$id_article = $record['id_article'];
$s = spip_query("SELECT mot.titre AS titre, mot.type AS groupe
	FROM spip_mots AS mot, spip_mots_articles AS lien
	WHERE lien.id_article=$id_article AND mot.id_mot=lien.id_mot
	ORDER BY titre");
while ($t=spip_fetch_array($s))
	$mots[$t['groupe']][] = typo($t['titre']);


# Chaque groupe de mots-cles commencant par 'dc_****' sera envoye dans
# le dc:**** correspondant
foreach ($mots as $titre_groupe => $mots_groupe) {
	if (eregi('^dc_(.*)$', $titre_groupe, $regs))
		foreach ($mots_groupe as $mot)
			$output .= xmlrecord($mot,
				'dc:'.$regs[1], '', $indent);

		# et on les efface pour ne pas en souffrir plus tard si on veut faire
		# un autre traitement pour les mots-cles non dc_... (voir ci-dessous)
		unset ($mots[$titre_groupe]);
}


/*
 ### alternative mots-cles
 ### Ce bloc de code prend tous les mots-cles restants et les passe
 ### dans dc:subject

foreach ($mots as $mots_groupe) 
	foreach ($mots_groupe as $mot)
		$output .= xmlrecord($mot,
			'dc:subject', '', $indent);
*/


## dc_identifier : ici on ne passe pas l'identifier canonique
## dans l'archive ($oaiprefix.$id_article) mais aussi bien l'URL (Minh)
## cf. http://listes.rezo.net/archives/spip-lab/2004-12/msg00113.html
$output .= xmlrecord(
	lire_meta('adresse_site').'/'
	.ereg_replace('^/', '', generer_url_article($record['id_article'])),
	'dc:identifier', '', $indent);

## dc_relation = les traductions par exemple (Minh)
if ($id_trad = $record['id_trad']) {
	$id_article = $record['id_article'];
	$s = spip_query("SELECT id_article FROM spip_articles
		WHERE id_trad=$id_trad AND statut='publie'
		AND id_article<>$id_article");
	while ($t = spip_fetch_array($s)) {
		$identifier = $oaiprefix.$t['id_article'];
		$output .= xmlrecord($identifier, 'dc:relation', '', $indent);
	}
}

## elements ignores pour l'instant
#$output .= xmlrecord($record['dc_type'], 'dc:type', '', $indent);
#$output .= xmlrecord($record['dc_format'], 'dc:format', '', $indent);
#$output .= xmlrecord($record['dc_coverage'], 'dc:coverage', '', $indent);



// Here, no changes need to be done
$output .=           
'     </'.$prefix;
if (isset($METADATAFORMATS[$prefix]['record_prefix'])) {
	$output .= ':'.$METADATAFORMATS[$prefix]['record_prefix'];
}
$output .= ">\n";
$output .= 
'   </metadata>'."\n";
?>
