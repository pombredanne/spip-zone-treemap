#!/bin/bash

###
# SMS: creer les inc/*.php à partir de l'original Horde/PEAR de Net_SMS
# password pour checkout anonyme = horde
#
#cvs -d :pserver:cvsread@anoncvs.horde.org:/repository login
# remplacer "update" par "checkout" la premier fois ...
#cvs -d :pserver:cvsread@anoncvs.horde.org:/repository update framework/Net_SMS
mkdir -p inc/sms
find framework/Net_SMS -name '*.php' -print0 | xargs -0 php patch_SMS.php inc
find inc/ -name '*.php' -exec php -l {} \;
