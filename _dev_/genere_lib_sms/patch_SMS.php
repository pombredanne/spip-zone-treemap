<?php
// pour generer inc/sms à partir du Net_SMS de Horde/PEAR
// normalement lance par makeSMSinc.sh
// necessite un checkout de Net_SMS , cf makeSMSinc.sh
// si le 1er argument ("inc" défaut) est un repertoire ecrivable,
// genere les fichiers, echo sinon

$sea = array(
	"#require_once 'PEAR\.php'#",
	"#(require|include)_once 'Net/SMS/' \. \\$(\w+) \. '.php'#",
	"#(require|include)_once 'Mail\.php'#",
	"#(require|include)_once 'HTTP/Request\.php'#",
	"#(require|include)_once 'Net/SMPP/Client\.php'#",
	"#PEAR::#",
	"#HTTP_Request\(#",
	"#Mail::#",
	"#PEAR_Error#",
	"#'/SMS/'#",
    "#\b_\(#",
    '#^<\?php#'
    );
$rep = array(
    "include_spip('inc/c_pear')",
    "include_spip('inc/sms/' . \$$2)",
    "include_spip('inc/c_mail')",
    "include_spip('inc/c_http_request')",
    "include_spip('inc/c_net_smpp_client')",
    "c_PEAR::",
    "c_HTTP_Request(",
    "c_Mail::",
    "c_Error",
    "'/sms/'",
    "_L(",
    "<?php\n// ATTENTION ! Genere avec makeSMSinc.sh et {$_SERVER['argv'][0]}, NE PAS EDITER !"
    );

$pasgene = false;
if (!isset($_SERVER['argv'][1]) ||
    !is_dir($tar = $_SERVER['argv'][1]) ||
    !is_writable($tar = $_SERVER['argv'][1])) {
	$pasgene = "*** NOTE ***\nEnter a directory to put processed files as first argument\n Followed by the files to process\n";
}
$max = count($_SERVER['argv']);

for ($i = 2; $i < $max; $i++) {
    $script = $_SERVER['argv'][$i];
	// on vire framework/Net_SMS/ et en minuscules
	$target = "$tar/" . strtolower(substr($script, 18));
	echo "\n******** $script dans $target ********\n";
	$source = preg_replace($sea, $rep, file_get_contents($script));
	if ($pasgene) {
		echo $source;
	} else {
		$fw = fopen($target, 'w');
		fwrite($fw, $source);
		fclose($fw);
	}
}
echo $pasgene;
?>
