1. Maquette SQLite pour Spip
____________________________

Le but de cette maquette est d'identifier les adaptations necessaires dans Spip.
La prochaine etape sera de faire evoluer le noyau Spip pour accueillir proprement
une Base de donnee SQLite (et tant qu'a faire d'autres bases):

   http://trac.rezo.net/trac/spip/ticket/209

  
La maquette fonctionne pour les squelettes dist et Epona
a partir d'une base importee (30 auteurs, 400 articles, 1000 forums)

  Ce qui ne marche pas en partie publique (dist):

    - recherche

  Ce qui ne marche pas en partie privee:

    - messages
    - cron (requete sql GET_LOCK) desactive
    - ajax. desactive
    - export de base

  Ce qui n'a pas ete teste:

    - traductions, revisions, petitions, upgrade
    - squelettes elabores
    - bases distribuee
    - ...

2. Mise en oeuvre 
_________________

  - Disposer d'un serveur php5/SQLite (par ex. MAMP pous MacOS)
  - Spip 1.9 beta 1 svn 5994 - je peux fournir un spip.zip complet

  Ce dossier comporte :

    - les fichiers modifies (*.sqlite) et les originaux (*.ref)
    - le phpinfo.html de MAMP
    - /ecrire/load_sqlite : un script pour aider l'install


  copier les fichiers dans votre arborescence Spip (sans le suffixe .sqlite)
  
  Pour tout rejouer, il faut detruire:
    - inc_connect.php 
    - la base de donnee /ecrire/spip.sqlite
    - data et CACHE

  
3. Modifications noyau
______________________
  Les changements ont ete faits dans une optique minimale et pour avancer vite.
  c'est sale, mais on voit vite les principaux changements (indentation decalee)

  
4. Incompatibilites et contournements
_____________________________________

  - "IF NOT EXISTS" refuse. supprime

  - auto_increment refuse. supprime

  - KEY non supporte. ignore

  - ENUM non supporte . remplace par des varchar adhoc.

  - BINARY non supporte. supprime

  - les cotes inversees (`) sont refusees. supprime

  - Problemes sur la population des types_documents (INSERT INTO) a cause du IGNORE refuse et de l'initialisation du id_type
   (j'ai mis a NULL au lieu de 0).

  - REPLACE a remplacer par REPLACE INTO.

  - `nom_base`.nom_table a remplacer par 'nom_base'.nom_table

  - FLOOR() - RAND() refuse. remplace par rand php

  - pas d'INSERT partiel. suppression de NOT NULL

  - le SHOW() n'existe pas; remplace par un SELECT MASTER_TABLE et traitement ad-hoc du resultat

  -  SELECT GET_LOCK() inconnu. bloque le cron. desactive.

  -  DATE_SUB(), DATE_ADD() inconnus. remplace par strtotime() (bidouille de regexp pas propre).

  - The SQL standard specifies that single-quotes in strings are escaped  by putting two single quotes in a row.
    SQL works like the Pascal programming  language in the regard. SQLite follows this standard.

       pas de de "de-quotage" sur un SELECT. Ajout stripslashes() sur spip_auteurs prefs.
       on remplace addslashes() par un str_replace ("'","''") au niveau de l'import ==> a generaliser....
  
  - spip_fetch_array() retournant array avec key='table.champ' au lieu de 'champ' dans les requetes
      utilisant 'table.champ' (article.php, presentation.php, brouteur...)
    ==> modification requete  + traitement resultat (ajout du prefixe 'auteurs' ... ) partie privee
    ==> filtrage du prefixe au niveau du fetch, pour les requetes de la partie publique (abstract)
      Cette distinction evite de tout penaliser et est necessaire en raison d'un bug SQLite :

  - Bug SQLite avec 'AS ' : 
            SELECT articles.id_article,  petitions.id_article AS petition FROM spip_articles AS articles LEFT JOIN spip_petitions
            AS petitions USING (id_article) WHERE statut='prepa' AND id_rubrique='2';
      ==> SQL error: no such column: spip_articles.id_article 
      ==> crash (Bus error) avec ma run-time php/sqlite MAMP

    Contournement: 
      ==> on prefixe  et on supprime le AS :
           SELECT spip_articles.id_article, spip_petitions.id_article FROM spip_articles LEFT JOIN spip_petitions
           USING (id_article) WHERE statut='prepa' AND id_rubrique='2';
      ==> traiter le resultat spip_fetch_array() avec  key='table.champ' au lieu de 'champ'

  - NOW(), UNIX_TIMESTAMP(), TO_DAYS(), DAYOFMONTH(), MONTH(), YEAR() . remplace par UDF 

  - LEAST() remplace par UDF

  - HEX(hash), VERSION() refuse. Supprime (probleme MySQL v3 / v4)

  - FIND_IN_SET refuse ==> remplacement par UDF

  - expr REGEXP pattern : remplace par UDF

  - INSERT INTO (...) VALUES (nuplet1), (nuplet2) non supporte => N INSERT a 1 seul nuplet 

  - HAVING inopine sur article. bug Spip dans sql_petitions()  Remplace 1 par rien (parametre du abstract_fetsel)


5. Restrictions sur les fonctions qui marchent
______________________________________________

  - tests non exhaustifs; faits sous MacOS

? ? les accents (MacOS) sont refuses dans les champs texte.  non reproduit...

  - pas de de "de-quotage" texte sur un SELECT. Un " est lu \"  ...

  - quote ' refuse en saisie de texte

  - blocs multi (mots, auteurs) : creer_objet_multi() inhibe

  - import type 1.2 et sur base vierge (minimale) uniquement

  
6. Performances
______________

Sachant que la maquette est penalisee par les mediations MySQL --> SQLite

Import d'un dump.xml de 1,5 Moctets:

  MySQL  : 20 sec.
  SQLite : 17 sec. (optimisee par BEGIN TRANSACTION / COMMIT)

Visite de 82 pages (sans cache) du site public (articles, rubriques, mots...)

  MySQL  : 34 sec.
  sqlite : 25 sec.


7. Changements
______________
 
2006 Mar 21 M.Lebas : 1er depot - on arrive a l'ecran de login
2006 Mar 22 M.Lebas : gere le PATH spip.sqlite selon contexte & chg. mineurs; Pb sur SHOW
2006 Mar 24 M.Lebas : on accede a la partie privee et on cree les auteurs
2006 Mar 26 M.Lebas : gestion preferences, rubriques et breves (restrictions accents et quotes)
                      pense bete, annonces, agenda, bandeaux
                      page d'accueil amelioree (affichage avec erreur squelette dist residuelle)
2006 Mar 28 M.Lebas : articles, mot-cles, sites
2006 Apr 02 M.Lebas : import de dump.xml. ==> travail avec vraie base (1,5 Mo)
                      gestion forums
                      accueil public
2006 Apr 04 M.Lebas : Partie publique : rubriques, breves, forums (affichage). Reste une erreur squelette pour article
2006 Apr 21 M.Lebas : Partie publique OK avec squelettes dist et Epona
                      Partie privee : recherche, docs joints, correction import (liens entre tables)
                      amelioration perfs
