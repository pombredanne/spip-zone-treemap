#!/bin/sh
cd /home/smart_paquets_changelog
export LC_ALL=fr_FR@euro

# Ici avec generation d'un ChangeLog pour chaque plugin
#+ Plus lecture d'un fichier 'authors.xml' si present
/usr/bin/php empaqueteur.php \
	svn://zone.spip.org/spip-zone \
	spip-zone \
	paquets \
	archivelist.txt \
	archives \
	plugin.xml \
	svn \
	spip-zone@rezo.net \
	paquets-zone@rezo.net\
	svn.changelog;
