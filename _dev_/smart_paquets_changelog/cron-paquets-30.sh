#!/bin/sh
cd /home/smart_paquets_changelog
export LC_ALL=fr_FR@euro

/usr/bin/php empaqueteur.php \
	svn://trac.rezo.net/spip \
	spip \
	paquets \
	archivelist.txt \
	archives \
	plugin.xml \
	svn \
	spip-dev@rezo.net \
	paquets-spip@rezo.net;
