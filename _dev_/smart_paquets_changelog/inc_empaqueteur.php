<?php

// -------------------------------------- APPEL PRINCIPAL ------------------------------------------

// Fonction principale appelee par le script empaqueteur.php
// Elle lance successivement:
// 1. la creation des zips et de la base des logos
// 2. le nettoyage des paquets obsoletes
// 3. la creation du fichier xml des paquets
// 4. la creation du fichier des traductions de Salvatore
// 5. la creation d'un index des logos
//
// $url			: url du repository des sources (ex: svn://zone.spip.org/spip-zone)
// $dir_repo	: repertoire racine des sources extraits du repository (ex: spip-zone)
// $dir_paq		: repertoire de depot des paquets crees (ex: paquets)
// $src			: nom du fichier listant les archives a creer (ex: archivelist.txt)
// $dest		: nom sans extension du fichier xml recapitulant toutes les archives (ex: archives)
// $xml			: nom du fichier xml contenant les infos des plugins (ex: plugin.xml)
// $nom_vcs		: gestionnaire de versions motorisant le repository concerne (ex: svn)
// $mail_to		: destinataire des mails d'information
// $mail_from	: emetteur des mails d'information
// $changelog 	: doit-on generer un fichier 'ChangeLog' (defaut: non | ex: ChangeLog)
// $dest_rss	: nom sans extension du fichier rss recapitulant toutes les archives (ex: plugins)
//
// return		: aucun
function empaqueteur($url, $dir_repo, $dir_paq, $src, $dest, $xml, $nom_vcs, $mail_to, $mail_from, $changelog, $dest_rss)
{
	global $erreurs;
	$dir_tmp = $dir_paq.'tmp/';
	require('inc_outils.php');

	// Determination du producteur a utiliser 
	// - plugin pour l'actuelle DTD
	// - paquet pour la future DTD
	$producteur =  'empaqueteur_' . basename($xml, '.xml');
	require($producteur . '.php');

	$erreurs = array();
	$url = trim($url);
	$dir_repo = rtrim($dir_repo ? $dir_repo : basename($url),'/') .'/';
	$dir_paq = rtrim($dir_paq ? $dir_paq : '.','/') .'/';
	
	// 1. creation des zips et de la base des logos
	list($depot, $zips, $logos) = empaqueteur_archives($url, $dir_repo, $dir_paq, $src, $nom_vcs, $xml, $changelog);
	
	// 2. nettoyage des paquets obsoletes
	if (!$erreurs) {
		$old = array_keys($zips);
		$old = array_merge($old, $logos); 
		// ne pas nettoyer le fichier archives.xml !
		foreach(is_array($dest) ? $dest : array($dest) as $nom_dest)
			$old[] = $nom_dest . '.xml';
		// ne pas nettoyer le fichier chargeur.xml !
		foreach(is_array($dest_rss) ? $dest_rss : array($dest_rss) as $nom_dest_rss)
			$old[] = $nom_dest_rss . '.xml';
		// ne pas nettoyer le fichier traductions.txt ! 
		$old[] = 'traductions.txt'; 
		// ne pas nettoyer l'index des logos logos.php ! 
		$old[] = 'logos.php'; 
		nettoyer_vieux_fichiers($old, $dir_paq . $dir_repo, $dir_tmp); 
	}
	elseif ($mail_to) 
		envoyer_mail($erreurs, $mail_to, $mail_from);

	// Obtention des contenus du fichier xml des paquets et de celui des traductions Salvatore
	list($archives, $chargeur, $salvatore) = $producteur($depot, $zips, $dir_repo);
	
	// 3. la creation du fichier xml des paquets
	// + et du xml de chargement SPIP
	if ($archives) {
		empaqueteur_xml_archives($dest . '.xml', $archives, $dir_paq, $dir_repo);
		empaqueteur_rss_archives($dest_rss . '.xml', $chargeur, $dir_paq, $dir_repo);
	} else
		echo_trace("Aucun Zip produit");

	// 4. la creation du fichier des traductions de Salvatore
	if ($salvatore)
		empaqueteur_salvatore('traductions.txt', $salvatore, $dir_paq, $dir_repo);
	else
		echo_trace("Aucune traduction avec Salvatore");

	// 5. la creation d'un index des logos
	empaqueteur_index_logos('logos.php', $dir_paq, $dir_repo);

	// 6. le logo du depot le cas echeant
	if (isset($depot['logo']) && @file_exists($dir_repo.$depot['logo']))
		empaqueteur_depot_logo($depot['logo'], $dir_paq, $dir_repo);
}


// ---------------------------------- CREATION DES ARCHIVES ----------------------------------------

// Fonction de creation des zips et de la base des logos
// Elle lance successivement:
// 1. Creation des repertoires de travail et import initial (uniquement la premiere fois)
// 2. Check-out ou update des sources a archiver
// 3. Lecture de la liste des archives a creer
// 4. Creation de la liste des archives requises
//
// $url			: url du repository des sources (ex: svn://zone.spip.org/spip-zone)
// $dir_repo	: repertoire racine des sources extraits du repository (ex: spip-zone)
// $dir_paq		: repertoire de depot des paquets crees (ex: paquets)
// $src			: nom du fichier listant les archives a creer (ex: archivelist.txt)
// $nom_vcs		: gestionnaire de versions motorisant le repository concerne (ex: svn)
// $xml			: nom du fichier xml contenant les infos des plugins (ex: plugin.xml)
// $changelog 	: doit-on generer un fichier 'ChangeLog' (defaut: non | ex: ChangeLog)
//
// return		: tableau des infos du depot et des zips crees
//					0 : tableau associatif (info -> valeur) de chaque info collectee sur le depot
//					1 : tableau associatif (nom de l'archive -> liste des infos) des zips crees
function empaqueteur_archives($url, $dir_repo, $dir_paq, $src, $nom_vcs, $xml, $changelog)
{
	// Definition des deux sous-repertoires temporaires et definitifs des paquets
	$dir_tmp = $dir_paq.'tmp/';
	$dir_paq .= $dir_repo;

	// Test pour savoir si le gestionnaire de version est bien installe sur le serveur
	if (!function_exists($vcs = 'vcs_exec_' . $nom_vcs)) {
		echo_trace("VCS non disponible: '$nom_vcs'");
		$vcs = 'explode'; // i.e. ne fait rien de ses arguments.
	}

	// 1. Creation des repertoires de travail et import initial
	if (!file_exists($dir_repo)){
		preparer_chemin(dirname(rtrim($dir_repo,'/')));
		if ($url) $vcs("checkout", "$url $dir_repo");
	}
	if (!file_exists($dir_paq))
		preparer_chemin($dir_paq);
	if (!file_exists($dir_tmp))
		preparer_chemin($dir_tmp);

	// 2. Check-out ou update des sources a archiver
	// Si le repo est en file:// on fait un svnsync dessus,
	// le up est fait par un hook post-commit sur ce qui a change uniquement
	// sauf une fois par jour
	if (preg_match(',^file://,',$url)) {
		$vcs("sync", $url);
		if (_FORCE_UPDATE)
			$vcs("up",rtrim($dir_repo,'/'));
	}
	elseif ($url) {
		$vcs("up", rtrim($dir_repo,'/'));
	}

	// 3. Lecture de la liste des archives a creer
	list($depot, $paquets) = lister_paquets($dir_repo . $src);

	// 4. Creation de la liste des archives requises
	$zips = array();
	$logos = array();
	foreach($paquets as $paquet){
		if ($paquet['revision']=='HEAD' AND
		    list($infos, $logo) = creer_paquet($paquet, $dir_repo, $dir_paq, $dir_tmp, $vcs, $xml, $changelog)
		) { 
			$zips[$paquet['nom'] .".zip"] = $infos;
			if ($logo) $logos[] = $logo; 
			if (intval(_SLEEP_BETWEEN)) usleep(_SLEEP_BETWEEN);
		}
	}
	echo_trace(count($zips) . " trouves");

//	return array($depot, $zips);
	return array($depot, $zips, $logos);
}


// Lister les paquets demandes dans le fichier des archives
// et compiler les informations sur le depot
//
// $src			: nom du fichier listant les archives a creer
//
// return		: tableau des infos du depot et des archives a creer
//					0 : tableau associatif (info -> valeur) de chaque info collectee sur le depot
//					1 : tableau associatif des archives a creer (source, nom, nom_dossier et revision)
function lister_paquets($src){

	echo_trace("chargement de $src");
	if (!$archivefile=file($src)){
		echo_trace("Erreur : Impossible de lire $src");
		return array();
	}

	$depot = array();
	$paquets = array();
	foreach($archivefile as $ligne=>$lignepaquet){
		$lignepaquet=rtrim($lignepaquet);//on vire le retour ligne de la fin
		if (strlen($lignepaquet)) {
			if (substr($lignepaquet,0,1)!="#") {
				// C'est une ligne de definition d'un paquet :
				// - on separe les parametres
				// - et on fixe ceux manquants
				$a = explode(";",$lignepaquet);
				$b = preg_split("/:/",$a[0]);
				$source = $b[0];
				$svn_version = empty($b[1]) ?'HEAD' : $b[1];		
				$nom_paquet = empty($a[1]) ? basename($source) : $a[1];
				$nom_dossier = empty($a[2]) ? $nom_paquet : $a[2];
				// Ajout au tableau des paquets a construire
				$paquets[] = array('source'=>rtrim($source,'/'),
						   'nom'=>$nom_paquet,
						   'nom_dossier'=>$nom_dossier,
						   'revision'=>$svn_version);
			}
			else if (preg_match('#@([^=\s]+)\s*=(.+)$#', substr($lignepaquet, 1), $matches)){
				// C'est une ligne d'information sur le depot
				// - on stocke le parametre trouve
				$depot[trim($matches[1])] = trim($matches[2]);
			}
		}
	}

	echo_trace(count($depot)." informations de depot definies");
	echo_trace(count($paquets)." paquets definis");
	return array($depot, $paquets);
}
	

// Fonction de creation d'un paquet soit :
// - de l'archive des sources (zip actuellement)
// - des informations completes sur le paquet (yc la liste des traductions)
// - et de son logo eventuel
// - du ChangeLog si demande
//
// $paquet		: tableau des infos sur l'archive a creer
// $dir_repo	: repertoire racine des sources extraits du repository
// $dir_paq		: repertoire de depot des paquets crees
// $dir_tmp		: repertoire temporaire de depot des archives crees
// $vcs			: fonction d'interface du vcs utilise (ex: vcs_exec_svn)
// $xml			: nom du fichier xml contenant les infos des plugins (ex: plugin.xml)
// $changelog 	: doit-on generer un fichier 'ChangeLog' (defaut: non | ex: ChangeLog)
//
// return		: tableau des infos sur le paquet cree
//					0 : taille du paquet
//					1 : date du paquet
//					2 : arborescence des sources du paquet (relatif a l'url des sources)
//					3 : date du dernier commit
//					4 : liste des traductions sous la forme de la balise XML <traductions>
//					5 : contenu du fichier xml du plugin
function creer_paquet($paquet, $dir_repo, $dir_paq, $dir_tmp, $vcs, $xml, $changelog)
{
	// Verifier le repertoire source du paquet a creer
	$dsource = $dir_repo. $paquet['source'];
	if (!file_exists($dsource)){
		echo_trace("Erreur : $dsource inexistant");
		return false;
	}
	
	// Ajouter le fichier svn.revision dans les fichiers de l'archive pour les besoins de SPIP
	$rev = $dsource . "/svn.revision";
	$vcs("info", "$dsource > $rev");
	$info = renseigner_revision_paquet($rev);
	$zip = $paquet['nom'] .".zip";
	$zippath = $dir_paq.$zip;
	if (!archiver($dsource, $zip, $zippath, $paquet, $info, $dir_tmp,
		$vcs=='vcs_exec_svn' ? $changelog : false, $dir_repo)) 
			return false;
	// -- copier le svn.revision de stable/spip.zip qui permet de connaitre la derniere version stable
	if ($zip=="stable/spip.zip") {
		// necessaire de remettre le fichier a la date actuelle
		// car il a la date du dernier commit sur ce paquet
		# touch($dsource."/svn.revision");
		copie_update($rev, $dir_paq.dirname($zip)."/svn.revision");
	}
	// -- supprimer le fichier info revision cree ci-dessus
	@unlink($rev);

	$f = $dsource . '/' . $xml ;
	$traductions='';
	$logo='';
	if (file_exists($f)) {
		// Recuperer le xml qui decrit le plugin
	    $re = ",<"."\?xml[^>]*\?".">,Uims";
		$desc = trim(preg_replace($re,'',file_get_contents($f)));

		// Construire la liste des traductions du plugin
		$traductions = compiler_traductions($dsource);

		// Creer le logo du paquet
		$f = 'empaqueteur_' . basename($xml, '.xml') . '_logo';
		$f = !function_exists($f) ? "" : $f($desc, $dsource);
		if ($f AND file_exists($f) AND preg_match('/[.][^.]*$/', $f, $r)) {
//			$d = $dir_paq . $paquet['nom'] . $r[0];
			$logo = $paquet['nom'] . $r[0]; 
			$d = $dir_paq . $logo; 
			copy($f, $d);
		}
	}
	else {
		// Paquet sans xml correspondant a une contribution quelconque
		echo_trace("(info) Paquet $zip sans $xml");
		$desc = '';
	} 
	
	return array(
		array(
			filesize($zippath),
			filemtime($zippath),
			$paquet['source'],
			$info[1],
			$traductions,
			$desc
		), 
		$logo
	);
}


// Fonction de creation d'une archive des sources contenu dans une arborescence donnee.
// Aujourd'hui l'archive est toujours un zip
//
// $source		: emplacement des sources a archiver
// $zip			: nom de l'archive a creer
// $zippath		: chemin complet de l'archive a creer
// $paquet		: tableau des infos sur l'archive a creer
// 		=> $paquet['nom_dossier'] : arborescence dans le zip qui sera cree lors du dezippage
// $rev			: tableau des informations sur le dernier commit (revision, date)
// $dir_tmp		: repertoire temporaire de depot des archives creees
// $changelog 	: doit-on generer un fichier 'ChangeLog' (defaut: non | ex: ChangeLog)
// $dir_repo	: repertoire racine des sources extraits du repository
//
// return		: true/false
function archiver($source, $zip, $zippath, $paquet, $rev, $dir_tmp, $changelog, $dir_repo){
	$nom_dossier = $paquet['nom_dossier'];
	$zipfile = basename($zip);
	list($revision, $date_commit) = $rev;
	$date_paquet = file_exists($zippath) ? filemtime($zippath) : 0;
	// tester si le paquet est a jour
	if (strtotime($date_commit) < $date_paquet AND !_FORCE_UPDATE) {
		echo_trace("$zip OK : du ".date('Y-m-d H:i:s',$date_paquet)." / dernier commit du $date_commit");
		return true;
	}
	else {
		$tmp = $dir_tmp.$nom_dossier;
		preparer_chemin($tmp);
		// Ajouter le ChangeLog si demande et vcs SVN
		if ($changelog && strlen($changelog)) {
//			echo_trace("=> infos du paquet : ".var_export($paquet,1));
			empaqueteur_changelog_svn($source, $changelog, $revision, $dir_repo, $source);
		}
		if (!rename($source,$tmp)) {
			echo_trace("Erreur : $source --> $tmp");
			return false;
		} else {
			$d = getcwd();
			chdir($dir_tmp);
			$base_dir = reset(explode('/',$nom_dossier));
			// zipper en prenant la date du fichier le plus recent
			// comme date du paquet
			exec_trace("zip -roq $zipfile $base_dir -x \*/.svn\*");
			chdir($d);

			$date_paquet = filemtime($dir_tmp.$zipfile);
			// cas ou le dernier commit consiste en la suppression de fichiers
			// du coup le zip est plus ancien que le dernier commit
			// on corrige manuellement
			if ($date_paquet<strtotime($date_commit)) {
				touch($dir_tmp.$zipfile,strtotime($date_commit)+1);
			}
			rename($tmp,$source);
		}
		supprimer_chemin($dir_tmp,$nom_dossier);
		copie_update($dir_tmp.$zipfile,$zippath);
		return true;
	}
}


// Fonction de compilation de la liste des traductions d'un plugin sous forme d'une suite
// de balises <traduction>
// On considere que les fichiers de langue ou les rapports de salvatore sont toujours dans
// le sous-repertoire lang/
//
// $source		: emplacement des sources du plugin
//
// return		: chaine composee des balises <traduction>
function compiler_traductions($source){

	// On charge une fois la liste des codes de langues
	if (empty($GLOBALS['codes_langues']));
		include('inc_langues.php');

	$traductions = '';

	// Determination des modules sous salvatore : on cherche les rapports xml
	$modules_salvatore = array();
	if ($rapports = glob($source . '/lang/*.xml')) {
		foreach ($rapports as $_rapport) {
			$modules_salvatore[] = basename($_rapport, '.xml');
			$contenu = file_get_contents($_rapport);
			$traductions .= $contenu . "\n";
		}
	}

	// Determination des modules non traduits par salvatore
	// Cette recherche n'est pas totalement deterministe car on est oblige de considerer
	// qu'il existe toujours un fichier module_fr.php pour identifier le nom du module
	if ($fichiers_fr = glob($source . '/lang/*_fr.php')) {
		foreach ($fichiers_fr as $_fichier_fr) {
			$nom_fichier = basename($_fichier_fr, '.php');
			$module = substr($nom_fichier, 0, strlen($nom_fichier)-3);
			// Si ce module n'a pas ete traite dans un rapport Salvatore on cherche toutes
			// ses traductions via les fichiers de langue.
			if (!in_array($module, $modules_salvatore) 
			AND ($fichiers_langue = glob($source . "/lang/$module_*.php"))) {
				$liste_langues = '';
				foreach ($fichiers_langue as $_fichier_langue) {
					$nom_fichier = basename($_fichier_langue, '.php');
					$langue = substr($nom_fichier, strlen($module) + 1 - strlen($nom_fichier));
					// Si la langue est reconnue, on l'ajoute a la liste des traductions
					// Comme on ne connait pas les traducteurs, la balise est donc vide
					if (isset($GLOBALS['codes_langues'][$langue]) AND $langue != 'fr')
						$liste_langues .= "\t" . '<langue code="' . $langue . '">' . '</langue>' . "\n";
				}
				// Le gestionnaire n'est pas precise et la langue de reference est toujours le fr
				$traductions .= '<traduction module="' . $module . '" reference="fr">' . "\n" . 
								$liste_langues .
								'</traduction>' . "\n";			
			}
		}
	}
	
	// On inclus les balise <traduction> dans une balise <traductions> sans attribut qui
	// facilite le travail du parser
	if ($traductions)
		$traductions = '<traductions>' . "\n\t" . $traductions . '</traductions>' . "\n";	
	
	return $traductions;
}


// --------------------------- CREATION DES FICHIERS RESULTAT --------------------------------------

// Fonction creant le fichier xml recapitulant toutes les archives creees
//
// Ne pas le reecrire si rien de neuf, c'est + efficace et ca permet
// au detecteur de nouvelle version d'utiliser If-Modified-Since.
//
// Attention, file_put_contents fait ce qu'il veut de certaines lignes vides
// http://fr2.php.net/manual/fr/function.file-put-contents.php
//
// $nom_fichier	: nom du fichier xml recapitulant toutes les archives
// $archives	: contenu du nouveau fichier xml resultant des archives creees
// $dir_paq		: repertoire de depot des paquets crees
// $dir_repo	: repertoire racine des sources extraits du repository
//
// return		: aucun
function empaqueteur_xml_archives($nom_fichier, $archives, $dir_paq, $dir_repo)
{
	$taille = strlen($archives);
	$f = $dir_paq . $dir_repo . $nom_fichier;
	$old = (file_exists($f)) ? trim(file_get_contents($f)) : '';
	if ($old != $archives) {
		echo_trace("Nouveau $f de taille $taille");
		file_put_contents($f, $archives);
		return;
	}
	echo_trace("$f intact (taille: $taille)");
}


// Fonction creant le fichier RSS recapitulant toutes les archives creees
//
// Ne pas le reecrire si rien de neuf, c'est + efficace et ca permet
// au detecteur de nouvelle version d'utiliser If-Modified-Since.
//
// Attention, file_put_contents fait ce qu'il veut de certaines lignes vides
// http://fr2.php.net/manual/fr/function.file-put-contents.php
//
// $nom_fichier	: nom du fichier xml recapitulant toutes les archives
// $archives	: contenu du nouveau fichier xml resultant des archives creees
// $dir_paq		: repertoire de depot des paquets crees
// $dir_repo	: repertoire racine des sources extraits du repository
//
// return		: aucun
function empaqueteur_rss_archives($nom_fichier, $archives, $dir_paq, $dir_repo)
{
	$taille = strlen($archives);
	$f = $dir_paq . $dir_repo . $nom_fichier;
	$old = (file_exists($f)) ? trim(file_get_contents($f)) : '';
	if ($old != $archives) {
		echo_trace("Nouveau $f de taille $taille");
		$archives = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<rss version=\"2.0\"
	xmlns:dc=\"http://purl.org/dc/elements/1.1/\"
	xmlns:content=\"http://purl.org/rss/1.0/modules/content/\"
>
$archives
</rss>";

		file_put_contents($f, $archives);
		return;
	}
	echo_trace("$f intact (taille: $taille)");
}


// Fonction creant le fichier des traductions de Salvatore
//
// Ne pas le reecrire si rien de neuf, c'est + efficace et ca permet
// au detecteur de nouvelle version d'utiliser If-Modified-Since.
//
// Attention, file_put_contents fait ce qu'il veut de certaines lignes vides
// http://fr2.php.net/manual/fr/function.file-put-contents.php
//
// $nom_fichier	: nom du fichier des traductions
// $salvatore	: contenu du nouveau fichier des traductions Salvatore
// $dir_paq		: repertoire de depot des paquets crees
// $dir_repo	: repertoire racine des sources extraits du repository
//
// return		: aucun
function empaqueteur_salvatore($nom_fichier, $salvatore, $dir_paq, $dir_repo)
{
	$taille = strlen($salvatore);
	$fichier = $dir_paq . $dir_repo . $nom_fichier;
	$old = (file_exists($fichier)) ? trim(file_get_contents($fichier)) : '';
	if ($old != $salvatore) {
		echo_trace("Nouveau $fichier de taille $taille");
		file_put_contents($fichier, $salvatore);
		return;
	}
	echo_trace("$fichier intact (taille: $taille)");
}


// Fonction creant le fichier index des logos permettant d'afficher une page 
// de controle des logos
//
// Le fichier est recree systematiquement
//
// $nom_fichier	: nom du fichier index des logos
// $dir_paq		: repertoire de depot des paquets crees
// $dir_repo	: repertoire racine des sources extraits du repository
//
// return		: aucun
function empaqueteur_index_logos($nom_fichier, $dir_paq, $dir_repo)
{
	$contenu = '';
	$logos = array_merge(glob($dir_paq . $dir_repo . '*.png'),
						glob($dir_paq . $dir_repo . '*.gif'),
						glob($dir_paq . $dir_repo . '*.jpg'));
	foreach($logos as $_logo)
		$contenu .= '<img src="' . basename($_logo) . '" />' . "\n";

	$fichier = $dir_paq . $dir_repo . $nom_fichier;
	if(strlen($contenu)){
		file_put_contents($fichier, $contenu);
		echo_trace("Nouvel index cree : " . count($logos) . " logos");
	}else{
		echo_trace("Aucun logo trouvés, pas d'index créé");
	}
}

// Fonction copiant le logo des depots
//
// L'image n'est copiee que si elle est presente (ah bon?)
// et si elle est absente de dir_paq ou a mettre a jour ...
//
// $nom_fichier	: nom du logo
// $dir_paq		: repertoire de depot des paquets crees
// $dir_repo	: repertoire racine des sources extraits du repository
//
// return		: aucun
function empaqueteur_depot_logo($nom_fichier, $dir_paq, $dir_repo)
{
	$original = $dir_repo.$nom_fichier;
	$copie = $dir_paq.$dir_repo.$nom_fichier;
	if (
		!@file_exists($copie) ||
		(@file_exists($copie) && filemtime($original) > filemtime($copie))
	) {
		echo_trace("Creation/MAJ du logo du depot : $original > $copie");
		return copy($original, $copie);
	}
	return;
}

// Fonction de generation de fichier "ChangeLog" a partir des commits SVN
// La commande et son resultat sont traces
//
// $paquet		: tableau des infos sur l'archive concernee
// $changelog	: nom du fichier
// $rev			: numero de revision concernee
// $dir_repo	: repertoire racine des sources extraits du repository (ex: spip-zone)
// $dir_tmp		: repertoire temporaire de depot des archives crees
//
// return		: le resultat de la commande
// ---------
// Ce script PHP se base sur le script SHELL d'Arthur de Jong : "svn2cl.sh"
// <http://arthurdejong.org/svn2cl/>
// svn2cl.sh - front end shell script for svn2cl.xsl
// Copyright (C) 2005, 2006, 2007, 2008, 2009, 2010 Arthur de Jong.
// ---------
function empaqueteur_changelog_svn($source, $changelog, $rev, $dir_repo, $tmp) 
{
	if (!is_string($changelog) || !strlen($changelog)) return false;
	global $svn2changelog_params, $svn2cl_pathinrepo;

	// On complète les infos du paquet
	$otrunk = trouver_svntrunk($source);
	$trunk = @file_exists($otrunk) ? $otrunk : $source;
	// Le fichier 'authors.xml' si present
	if(@file_exists("$dir_repo/authors.xml"))
		$svn2changelog_params['xsltproc']['authorsfile'] = "`pwd`/$dir_repo/authors.xml";
	// Le chemin retire des paths de fichiers
	if (isset($svn2cl_pathinrepo) && strlen($svn2cl_pathinrepo)) {
		$svn2changelog_params['xsltproc']['strip-prefix'] = str_replace(
			str_replace($dir_repo, '', $source), 
			str_replace($dir_repo, '', $trunk), 
			$svn2cl_pathinrepo
		);
	}
	// La commande 'svnlog'
	$SVN_PARAMS = '';
	foreach($svn2changelog_params['svnlog'] as $param=>$val) {
		if ($val===true)
			$SVN_PARAMS .= " --$param";
		elseif (is_string($val) || is_numeric($val))
			$SVN_PARAMS .= " --$param \"$val\"";
	}
	// La commande 'xsltproc'
	$XSLT_PARAMS = '';
	foreach($svn2changelog_params['xsltproc'] as $param=>$val) {
		if (is_string($val) || is_numeric($val))
			$XSLT_PARAMS .= " --stringparam $param \"$val\"";
	}
	// La commande globale
	$cmd_changelog = "unset LC_ALL; \
  svn $SVN_PARAMS --xml log \"`pwd`/$trunk@$rev\" | \
  xsltproc $XSLT_PARAMS --nowrite --nomkdir --nonet \"`pwd`/svn2changelog_spip.xsl\" - \
  > \"`pwd`/$tmp/$changelog\";";

	echo_trace("svn log $trunk@$rev > $tmp/$changelog");
	// Pour debug
//	exec_trace("$cmd_changelog");	
	exec("$cmd_changelog");	
	return;
}

?>
