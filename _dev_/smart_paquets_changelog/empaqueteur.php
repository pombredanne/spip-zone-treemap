<?php

// ----------------------------------------- PARAMETRAGE -------------------------------------------

// Test des arguments obligatoires
if (!isset($argv[1]) OR !$argv[1]){
	echo "Argument 1 - OBLIGATOIRE : url du repository svn\n";
	echo "Argument 2 : repertoire relatif a ./ dans lequel sera fait le checkout\n";
	echo "Argument 3 : repertoire de depot des paquets crees (defaut: paquets)\n";
	echo "Argument 4 : nom du fichier listant les archives a creer (defaut: archivelist.txt)\n";
	echo "Argument 5 : nom sans extension du fichier xml recapitulant toutes les archives (defaut: archives)\n";
	echo "Argument 6 : nom du fichier xml contenant les infos des plugins (defaut: plugin.xml)\n";
	echo "Argument 7 : gestionnaire de versions motorisant le repository concerne (defaut: svn)\n";
	echo "Argument 8 : email du webmestre destinataire\n";
	echo "Argument 9 : email de l'emetteur\n";
	echo "Argument 10 : nom du fichier 'ChangeLog' si generation (defaut: non)\n";
	echo "Argument 11 : nom sans extension du fichier rss recapitulant toutes les archives (defaut: chargeur)\n";
	die();
}

// --------------------------------------- CONFIGURATION -------------------------------------------

// Timezone sur europe (marre des messages d'erreur PHP)
date_default_timezone_set('Europe/Berlin');

define('_TRACE',true);
define('_SLEEP_BETWEEN',200000);
// on force une mise a jour de tous les paquets une fois pas jour,
// entre minuit et 1H
define('_FORCE_UPDATE',date('H')<1);
#error_reporting(E_ALL);

include('inc_empaqueteur.php');

// --------
// Parametres passes au modele XSL pour le ChangeLog
global $svn2changelog_params; 
$svn2changelog_params = array(
	'svnlog' => array(
		'verbose' => true, // Voir la liste des fichiers concernes
		'limit' => false, // Limiter le nombre de revisions
		'stop-on-copy' => false, // Arreter lors d'une copie (defaut: non)
	),
	'xsltproc' => array(
	    'strip-prefix' => '/', // Path remplace dans les listes de fichiers
    	'linelen' => 100, // Longueur max des lignes
	    'groupbyday' => 'yes', // Grouper par date
    	'separate-daylogs' => 'yes', // Separer les revs 
	    'include-rev' => 'yes', // Inclure le numero de rev
    	'include-actions' => 'yes', // Inclure l'action sur chaque fichier
	    'breakbeforemsg' => 'yes', // Passer a la ligne avant le log de commit
    	'ignore-message-starting' => 'ignore', // Mot-cle des messages a ignorer
	    'version-message-starting' => 'version', // Mot-cle des messages de version
	)
);
global $svn2cl_pathinrepo; 
// $svn2cl_pathinrepo retient le chemin du paquet courant dans le repo pour retrait dans changelog
// --------

// ----------------------------------- SESSION D'EMPAQUETAGE ---------------------------------------

// Appel de la fonction principale d'empaquetage avec les parametres idoines
// -> les valeurs en dur devront devenir des parametres des scripts a terme
empaqueteur($argv[1],									// $url
	   isset($argv[2]) ? $argv[2] : '', 				// $dir_repo
	   isset($argv[3]) ? $argv[3] : 'paquets',			// $dir_paq
	   isset($argv[4]) ? $argv[4] : 'archivelist.txt',	// $src
	   isset($argv[5]) ? $argv[5] : 'archives',			// $dest
	   isset($argv[6]) ? $argv[6] : 'plugin.xml',		// $xml
	   isset($argv[7]) ? $argv[7] : 'svn',				// $nom_vcs
	   isset($argv[8]) ? $argv[8] : '',					// $mail_to
	   isset($argv[9]) ? $argv[9] : '',					// $mail_from
	   isset($argv[10]) ? $argv[10] : '',				// $changelog
	   isset($argv[11]) ? $argv[11] : 'chargeur'		// $dest_rss
);
?>
