#!/bin/sh
cd /home/smart_paquets_changelog
export LC_ALL=fr_FR@euro

/usr/bin/php empaqueteur.php \
	file:///home/svn/repository/spip-zone/ \
	spip-zone \
	paquets \
	archivelist.txt \
	archives \
	plugin.xml \
	svn \
	spip-zone@rezo.net \
	paquets-zone@rezo.net;
