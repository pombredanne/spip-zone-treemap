#!/bin/sh
cd /home/smart_paquets_changelog
export LC_ALL=fr_FR@euro

/usr/bin/php empaqueteur.php \
	svn://zone.spip.org/spip-zone \
	spip-zone \
	paquets \
	archivelist.txt \
	archives \
	plugin.xml \
	svn \
	spip-zone@rezo.net \
	paquets-zone@rezo.net;
	
/usr/bin/php empaqueteur.php \
	svn://trac.rezo.net/spip \
	spip \
	paquets \
	archivelist.txt \
	archives \
	plugin.xml \
	svn \
	spip-dev@rezo.net \
	paquets-spip@rezo.net;
