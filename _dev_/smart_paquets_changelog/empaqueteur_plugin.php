<?php

// Construit la concatenation des informations essentielles
// contenu dans les fichiers plugin.xml et des informations relatives a chaque paquet
// Construit egalement les informations de traduction par salvatore a destination du fichier traductions.txt
function empaqueteur_plugin($depot, $zips, $dir_repo=false) {
	$xml = '';
	$xml_rss = '';
	$salvatore = '';
	foreach($zips as $zip => $infos) {
		list($size, $time, $source, $commit, $traductions, $plugin_xml) = $infos;
		// Bloc d'info du paquet
		$xml .= "<archive id=\"$zip\">
<zip>
	<file>$zip</file>
	<size>$size</size>
	<date>$time</date>
	<source>$source</source>
	<last_commit>$commit</last_commit>
</zip>
$traductions
$plugin_xml
</archive>\n\n";

		$plugin_logo = '';
		foreach(array('png', 'gif', 'jpg') as $img_ext) {
			$img_plg = str_replace('.zip', '.'.$img_ext, $zip);
			if(file_exists(rtrim($dir_repo, '/').'/'.$img_plg))
				$plugin_logo = rtrim($depot['url_archives'], '/').'/'.$img_plg;
		}
		$xml_rss .= "<item>
		<title>".empaqueteur_plugin_info($plugin_xml, 'nom')."</title>
		<link>".rtrim($depot['url_serveur'], '/')."/</link>
		<guid isPermaLink=\"true\">".rtrim($depot['url_serveur'], '/').'/'.rtrim($source, '/')."</guid>
		<dc:date>".date(DATE_ATOM, $time)."</dc:date>
		<dc:format>text/html</dc:format>
		<dc:language>fr</dc:language>
		<dc:creator>".empaqueteur_plugin_info($plugin_xml, 'auteur')."</dc:creator>
		<description>
		&lt;img src='$plugin_logo' /&gt;
		Etat: ".empaqueteur_plugin_info($plugin_xml, 'etat')."
		".empaqueteur_plugin_info($plugin_xml, 'description')."
		</description>
		<enclosure url=\"".rtrim($depot['url_archives'], '/').'/'.$zip."\" length=\"$size\" type=\"application/zip\" />
	</item>\n\n";

		// Bloc d'info pour Salvatore si il existe
		if (preg_match_all("#<salvatore\s+module=['\"](\w*)['\"]\s+reference=['\"](\w*)['\"]\s*/>#i", $plugin_xml, $matches)) {
			foreach ($matches[1] as $_i => $_module) {
				$salvatore .= rtrim($depot['url_serveur'], '/') . '/' .	rtrim($source, '/') . '/lang/;' . 
							  $_module . ';' .
							  $matches[2][$_i] . "\n";
			}
		}
	}

	// On complete les archives avec les informations du depot
	// Pour l'instant ce bloc n'implemente pas la nouvelle DTD
	if (!$xml) return '';
	$xml_depot = '';
	foreach ($depot as $_balise => $_valeur) {
		$xml_depot .= "<$_balise>$_valeur</$_balise>\n";
	}
	if ($xml_depot) 
		$xml_depot_full = "<depot>\n$xml_depot</depot>\n";
	else
		$xml_depot_full = "\n";
	$xml = "$xml_depot_full<archives>
$xml
</archives>";

	// On complete le chargeur avec les informations du depot
	// Pour l'instant ce bloc n'implemente pas la nouvelle DTD
	if (!$xml_rss) return '';
	$xml_depot_rss = "
	<title>".$depot['titre']."</title>
	<link>".$depot['url_serveur']."</link>
	<description>".$depot['descriptif']."</description>
	<language>fr</language>
			\n";
	if (isset($depot['logo']) && strlen($depot['logo'])) {
		$xml_depot_rss .= "
	<image>
		<title>".$depot['titre']."</title>
		<url>".rtrim($depot['url_archives'], '/').'/'.$depot['logo']."</url>
		<link>".$depot['url_serveur']."</link>
	</image>
			\n";
	}
	$xml_rss = "<channel>
$xml_depot_rss
$xml_rss
</channel>";

	if($salvatore){
	// On ajoute une en-tete au fichier genere
	$salvatore = 
"# LISTE DES PLUGINS / SQUELETTES UTILISANT SALVATORE
# --------------------------------------------------
# Depot : " . $depot['titre'] . "
# Generation par Smart-Paquets le " . date('d-m-Y H:i') . "
#\n" . $salvatore;
	}
	return array($xml, $xml_rss, $salvatore);
}


// Renvoie le path complet du logo a partir de la balise icon de plugin.xml et de la racine des sources
function empaqueteur_plugin_logo($plugin_xml, $dir_source) {
  return !preg_match('#<icon[^>]*>\s*(.+)\s*</icon>#i', $plugin_xml, $matches) ? '' : ($dir_source . '/' . trim($matches[1]));
}

// Renvoie une info d'un XML
function empaqueteur_plugin_info($plugin_xml, $info='nom') {
  return !preg_match('#<'.$info.'[^>]*>\n*\s*(.+)\s*\n*</'.$info.'>#i', $plugin_xml, $matches) ? '' : trim($matches[1]);
}

?>
