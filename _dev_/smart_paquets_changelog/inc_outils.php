<?php

// ---------------------------------------- INTERFACE VCS ------------------------------------------

// Interface de commande du gestionnaire de version Subversion (SVN)
//
// $cmd			: commande svn
// $args		: liste des arguments (ligne de commande)
//
// return		: le resultat de la commande
function vcs_exec_svn($cmd, $args)
{
	$bin = ($cmd == 'sync') ? 'svnsync' : 'svn';
	return exec_trace("$bin $cmd $args");
}


// Fonction generique de lancement d'une commande
// La commande et son resultat sont traces
//
// $commande	: commande a executer
//
// return		: le resultat de la commande
function exec_trace($commande){
	$output = array();
	echo_trace($commande);
	$out = exec("$commande 2>&1", $output);
	if ($output!==null) {
		$out = end($output);
	}
	if (strlen(trim($out)))
		array_map('echo_trace',$output);
	return $out;
}


// -------------------------------------- TRACE ET MAIL --------------------------------------------

// Fonction de trace avec horodatage
//
// $out			: chaine a tracer
//
// return		: la trace horodatee
function echo_trace($out){
	$outh = @date("[Y-m-d H:i:s] ",time()).$out."\n";
	if (_TRACE) echo $outh;

	if (strncmp($out,"Erreur :",8)==0)
		$GLOBALS['erreurs'][]=$outh;

	return $outh;
}


// Fonction d'envoi d'un mail sur les erreurs collectees durant la session d'empaquetage
//
// $erreurs		: tableau des erreurs collectees
// $mail_to		: destinataire des mails d'information
// $mail_from	: emetteur des mails d'information
//
// return		: aucun
function envoyer_mail($erreurs, $mail_to, $mail_from)
{
	$headers =
		"From: ".($mail_from ?$mail_from : $mail_to)."\r\n"
		."Reply-To: ".($mail_from ?$mail_from : $mail_to)."\r\n";
	mail($mail_to,"empaqueteur",implode("",$erreurs),$headers);
}


// -------------------------------- TRAITEMENT DES ARBORESCENCES -----------------------------------

// Fonction de creation d'une arborescence/vide/vers/un/nom/de/dossier
//
// $dir			: arborescence a creer
//
// return		: aucun
function preparer_chemin($dir){
	if (strlen($dir)){
		if ($parent = dirname($dir) AND !is_dir($parent))
			preparer_chemin($parent);
		if (!is_dir($dir))
			mkdir($dir);
	}
}


// Fonction de suppression supprimer une arborescence/vide/vers/un/nom/de/dossier
// a partir d'un repertoire $base que l'on conserve
//
// $base		: repertoire de base a partir duquel commence la suppression
// $dir			: arborescence a detruire
//
// return		: aucun
function supprimer_chemin($base, $dir){
	if (strlen($dir) AND $dir!='.'){
		if (file_exists($base .$dir))
			rmdir($base .$dir);
		supprimer_chemin($base, dirname($dir));
	}
}


// ----------------------------------- TRAITEMENT DES PAQUETS --------------------------------------

// Fonction de mise a jour d'un paquet
// Si le paquet a change on le recopie dans le repertoire de destination
//
// $source		: paquet venant d'etre cree dans le repertoire temporaire
// $dest		: paquet dans le repertoire final de depot (existe deja ou pas)
//
// return		: aucun
function copie_update($source, $dest){
	if (file_exists($source)){
		$ts = filemtime($source);
		if (file_exists($dest))
			unlink($dest);
		else {
			// si l'archive doit etre mise dans un sous repertoire
			// creer l'arbo la premiere fois
			preparer_chemin(dirname($dest));
		}
		rename($source,$dest);
		touch($dest,$ts);
	}
	else
		echo_trace("Erreur : fichier $source non trouve");
}


// Fonction de lecture des informations du fichier revision.svn
// Ce fichier est utilise par SPIP pour afficher son numero de version dans l'interface privee 
// a l'aide de la fonction version_svn_courante() 
// http://doc.spip.org/@version_svn_courante 
//
// $file		: fichier revision.svn du paquet en cours de creation
//
// return		: tableau des infos lues dans revision.svn
//					0 : numero de revision
//					1 : date du dernier commit
function renseigner_revision_paquet($file){

	$revision = $date_commit = 0;

	$infos = @file($file);
	if (!$infos) return array('', 0);
	$xml_props = $txt_props = array();
	$depo_root = '';
	foreach($infos as $line) {
		if (preg_match('/^(Last Changed Rev|R.vision de la derni.re modification)\s*: (?<revision>\d*)$/',$line,$matches))
			$xml_props['revision'] = $txt_props['Revision'] = $matches['revision'];
		if (preg_match('/^URL\s*: (?<url>.*)$/',$line,$matches)) 
			$xml_props['origine'] = $txt_props['Origine'] = $matches['url'];
		if (preg_match('/^(Last Changed Date|Date de la derni.re modification\s*): (?<date_commit>[^(]*)($|\()/',$line,$matches)) 
			$xml_props['commit'] = $txt_props['Dernier commit'] = $matches['date_commit'];
		if (preg_match('/^(Repository Root|Racine du d.p.t\s*): (?<url_root>.*)$/',$line,$matches)) 
			$depo_root = $matches['url_root'];
	}
	
	global $svn2cl_pathinrepo; 
	$svn2cl_pathinrepo = str_replace($depo_root, '', $xml_props['origine']);

	$svn_revision = "<svn_revision>\n<text_version>";
	foreach($txt_props as $prop => $val) $svn_revision .= "\n$prop: $val";
	$svn_revision .= "\n</text_version>";
	foreach($xml_props as $prop => $val) $svn_revision .= "\n<$prop>$val</$prop>";
	$svn_revision .= "\n</svn_revision>";
	
	if ($fp = @fopen($file,"w")) {
		fwrite($fp, $svn_revision);
		@fclose($fp);
	} 
	else echo_trace("Erreur: impossible d'ecrire dans $file");
	// mettre la date du fichier a celle du dernier commit
	// pour ne pas fausser la date du paquet (qui est celle du plus recent fichier)
	touch($file,strtotime($xml_props['commit']));

	$date_commit = date('Y-m-d H:i:s',strtotime($xml_props['commit']));

	return array($xml_props['revision'], $date_commit);
}


// Fonction de suppression des paquets et logos obsoletes du repertoire de depot final
//
// $paquets_a_jour	: tableau des paquets venant d'etre mis a jour
// $dir_paq			: repertoire de depot des paquets crees
// $dir_tmp			: repertoire temporaire de depot des archives crees
//
// return		: aucun
function nettoyer_vieux_fichiers($paquets_a_jour, $dir_paq, $dir_tmp, $prepend='') {
	$maxfiles = 10000; // securite
	#echo_trace("Nettoyer vieux paquets:".$dir_paq);
	$nbfiles = 0;

	if (@is_dir($dir_paq) AND is_readable($dir_paq) AND $d = @opendir($dir_paq)) {
		while (($f = readdir($d)) !== false && ($nbfiles<$maxfiles)) {
			#echo_trace("fichier $f lu");
			if ($f[0] != '.' # ignorer . .. .svn etc
			AND $f != 'CVS'
			AND $f != 'remove.txt'
			AND is_readable($g = $dir_paq.$f)) {
				#echo_trace("fichier $g a supprimer ?");
				if (is_dir($g)){
					nettoyer_vieux_fichiers($paquets_a_jour, "$g/", $dir_tmp, "$prepend$f/");
				}
				elseif (is_file($g)) {
					#echo_trace("fichier $g est un zip");
					if (!in_array($prepend.$f,$paquets_a_jour)){
						echo_trace("Suppression du vieux paquet $f");
						unlink($g);
						@unlink($dir_tmp.$f); // securite, on vire aussi le vieux paquet tmp eventuel
					}
				}
			}
			$nbfiles++;
		}
		closedir($d);
	}
}

// Fonction de recherche du tronc d'un depot
//
// $dir			: chemin concerne sur le depot
// $search		: repertoire recherche (defaut: tag ou tags)
// $find		: repertoire a trouver (defaut: trunk)
//
// return		: le chemin trouve
function trouver_svntrunk($dir=null, $search='tag', $find='trunk') 
{
	if (is_null($dir) || !is_string($dir) || !strlen($dir)) return $dir;
	$trunk = '';
	foreach(explode('/', $dir) as $part) {
		if (substr_count($part, $search)!=0) {
			$trunk .= $find; break;
		}
		$trunk .= $part.DIRECTORY_SEPARATOR;
	}
	return trim($trunk, DIRECTORY_SEPARATOR);
}

?>
