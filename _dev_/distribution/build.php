<?php


make_distrib(file_get_contents('spip-nu+crayons.xml'));

define('_CHMOD', 0777);

function make_distrib($f) {
	@mkdir ($rep = 'out_'.time());

	$xml = simplexml_load_string($f);

	echo "** nom: ".$xml->nom."\n";
	echo "* desc: ".$xml->description."\n";
	foreach($xml->children() as $key => $tab) {

		switch ($key) {
			case 'necessite':
			case 'utilise':
				charger_paquet($tab, $rep);
				break;
		}
	}

	// recopier le fichier dans ./config/distribution.xml
	$dist = $rep.'/config/distribution.xml';
	@mkdir(dirname($dist), _CHMOD, true);
	($fp = fopen($dist, 'w')) && fwrite($fp, $f) && fclose($fp);
}

/*
 * $tab = (id, methode, url)
 */
function charger_paquet($tab, $rep) {

	$attrs = $tab->attributes();

	$purl = escapeshellarg((string) $attrs->url);
	$dest = $rep.'/'.$attrs['path'].'/';
	@mkdir($dest, _CHMOD, true);
	$pdest = escapeshellarg($dest);

	$options = (string) $attrs->options;

	switch((string) $attrs->methode) {
		case 'svn':
			$cmd = "svn checkout $options $purl $pdest";
			echo "# $cmd\n";
			`$cmd`;
			$cmd = "svn info $purl > $pdest/svn.revision";
			echo "# $cmd\n";
			`$cmd`;
			break;

		case '':
			die ("methode vide\n");

		default:
			die ("methode ".$attrs['methode']." non supportée\n");
	}
}

?>
