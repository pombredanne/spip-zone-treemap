<?php
if (!defined("_ECRIRE_INC_VERSION")) return;



// Un morceau d'ajax qui affiche le descriptif d'un plugin a telecharger

function exec_plugins_info_dist() {
	exec_plugins_info_args( _request('plug'));
}

// http://doc.spip.org/@exec_charger_plugin_descr_args
function exec_plugins_info_args($plug_file) {
	if (!autoriser('configurer', 'plugins') OR !$plug_file) {
		include_spip('inc/minipres');
		echo minipres();
	} else {
		include_spip('inc/plugin');
		echo affiche_bloc_plugin($plug_file, plugin_get_infos($plug_file));
	}
}

?>
