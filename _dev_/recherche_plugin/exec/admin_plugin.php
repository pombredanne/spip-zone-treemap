<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2008                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/config');
include_spip('inc/plugin');
include_spip('inc/presentation');
include_spip('inc/layer');
include_spip('inc/actions');
include_spip('inc/securiser_action');

// http://doc.spip.org/@exec_admin_plugin_dist
function exec_admin_plugin_dist($retour='') {

	if (!autoriser('configurer', 'plugins')) {
		include_spip('inc/minipres');
		echo minipres();
	} else {

	verif_plugin();

	$commencer_page = charger_fonction('commencer_page', 'inc');
	echo $commencer_page(_T('icone_admin_plugin'), "configuration", "plugin");
	

	echo "<br />\n";
	echo "<br />\n";


	echo gros_titre(_T('icone_admin_plugin'),'',false);

	
	echo debut_gauche('plugin',true);
	echo debut_boite_info(true);
	$s = "";
	$s .= _T('info_gauche_admin_tech');
	$s .= "<p><img src='". chemin_image('puce-verte.gif') . "' width='9' height='9' alt='' /> "._T('plugin_etat_stable')."</p>";
	$s .= "<p><img src='". chemin_image('puce-orange.gif') . "' width='9' height='9' alt='' /> "._T('plugin_etat_test')."</p>";
	$s .= "<p><img src='". chemin_image('puce-poubelle.gif') . "' width='9' height='9' alt='' /> "._T('plugin_etat_developpement')."</p>";
	$s .= "<p><img src='". chemin_image('puce-rouge.gif') . "' width='9' height='9' alt='' /> "._T('plugin_etat_experimental')."</p>";
	echo $s;
	echo fin_boite_info(true);

	// Si on a CFG, ajoute un lien (oui c'est mal)
	if (defined('_DIR_PLUGIN_CFG')) {
		echo debut_cadre_enfonce('',true);
		echo icone_horizontale('CFG &ndash; '._T('configuration'), generer_url_ecrire('cfg'), _DIR_PLUGIN_CFG.'cfg-22.png', '', false);
		echo fin_cadre_enfonce(true);
	}

	// Lister les librairies disponibles
	if ($libs = liste_librairies()) {
		debut_cadre_enfonce('', '', '', _T('plugin_librairies_installees'));
		ksort($libs);
		echo '<dl>';
		foreach ($libs as $lib => $rep)
			echo "<dt>$lib</dt><dd>".joli_repertoire($rep)."</dd>";
		echo '</dl>';
		echo fin_cadre_enfonce(true);
	}

	echo debut_droite('plugin', true);

	include_spip('public/assembler');
	include_spip('inc/filtres');
	$contexte = array(
		'ajax'=>'',
		'recherche_plugin'=>_request('recherche_plugin'),
		'fond'=>'prive/configurer/plugins'
		);
	echo "<div class='ajaxbloc env-"
		. encoder_contexte_ajax($contexte)
		. "'>\n"
		. recuperer_fond('',$contexte)
		. "</div><!-- ajaxbloc -->\n";

	if (include_spip('inc/charger_plugin')) {
		echo formulaire_charger_plugin($retour);
	}

	echo fin_gauche(), fin_page();
	}
}

?>
