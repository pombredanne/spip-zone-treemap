<?php
include_spip('inc/config');
include_spip('inc/plugin');
include_spip('inc/presentation');
include_spip('inc/layer');
include_spip('inc/actions');
include_spip('inc/securiser_action');

function formulaires_plugins_charger_dist(){
	$recherche = _request('recherche_plugin','');

	$format = '';
	if (_request('format')!==NULL)
		$format = _request('format');

	list($resultats, $nombre) = plugins_formulaire_resultats($recherche,$format);
	
	include_spip('inc/securiser_action');
	return array(
		'recherche_plugin' => $recherche,
		'nombre_resultats' => ($recherche ? _L('@nb@ réponses',array('nb'=>$nombre)) : ''),
		'_resultats' => $resultats,
		// en fait, il faut mettre liste/arbre en option de la recherche directement...
		// plop. yaka !
		//'_forcer_request' => 'oui' // pour recuperer les GET des liens (liste/arbre)
		
	);
}

function formulaires_plugins_verifier_dist(){
	$erreurs = array();
	
	// si on vient du formulaire rechercher, on ne fait aucun traitement.
	if (!('oui' == _request('frm_activer_plugins')))
		$erreurs['rien_a_faire'] = " ";
	// sinon, verifier l'autorisation
	elseif (!autoriser('configurer', 'plugins'))
		$erreurs['message_erreur'] = _L('acces_interdit');
		
	return $erreurs;
}

function formulaires_plugins_traiter_dist(){
	$res = array('editable'=>true);
	
	// forcer la maj des metas pour les cas de modif de numero de version base via phpmyadmin
	lire_metas();
	// enregistrer les modifs
	include_spip('action/activer_plugins');
	enregistre_modif_plugin();
	
	if (isset($GLOBALS['meta']['plugin_erreur_activation'])
	AND autoriser('configurer', 'plugins', null, $id_auteur)) {
		if ($message = $GLOBALS['meta']['plugin_erreur_activation'])
			$res['message_erreur'] = $message;
		effacer_meta('plugin_erreur_activation');
	}
	
	return $res;
}

// installe les plugins, mais recupere le buffur si possible,
// sinon ajax se retrouve avec des header deja envoyes
function rp_installe_plugins(){
	if ($GLOBALS['flag_ob']) {
		ob_start();
		installe_plugins();
		$compte_rendu = ob_get_contents();
		ob_end_clean();
	} 	else {
		installe_plugins();
	}
	return $compte_rendu;
}

// cette fonction retourne
// - soit une liste de plugins correspondant a la recherche
// - soit en absence de recherche les plugins actifs, interessants,
// ainsi que tous les plugins a la racine /plugins/ et dans /plugins/auto/
function plugins_formulaire_resultats($recherche="", $format=''){

	$lpf = liste_plugin_files();
	$lcpa = liste_chemin_plugin_actifs();
	$plugins_interessants = @array_keys(unserialize($GLOBALS['meta']['plugins_interessants']));
	if (!is_array($plugins_interessants))
		$plugins_interessants = array();
	
	if ($lpf) {
				  
		// pas de recherche = comme avant
		if (!$recherche) {

			// S'il y a plus de 10 plugins pas installes, les signaler a part ;
			// mais on affiche tous les plugins mis a la racine ou dans auto/
			if (count($lpf) - count($lcpa) > 9
			AND _request('afficher_tous_plugins') != 'oui') {

				$dir_auto = substr(_DIR_PLUGINS_AUTO, strlen(_DIR_PLUGINS));
				$lcpaffiche = array();
				foreach ($lpf as $f)
					if (!strpos($f, '/')
					OR ($dir_auto AND substr($f, 0, strlen($dir_auto)) == $dir_auto)
					OR in_array($f, $lcpa)
					OR in_array($f, $plugins_interessants))
						$lcpaffiche[] = $f;
				if (count($lcpaffiche)<10 && !$format) $format = 'liste';
				$corps = plugins_generer_liens_liste($lpf, $lcpa, $format,true)
					. affiche_les_plugins($lcpaffiche, $lcpa, $format);

			} else {
				$corps = plugins_generer_liens_liste($lpf, $lcpa, $format)
					//. (count($lpf)>20 ? $sub : '')
					. affiche_les_plugins($lpf, $lcpa, $format);
			}
		}
		// on a des choses à rechercher !
		// il faut penser a mettre en hinput hidden coche les plugins actifs
		// non presents dans le resultat de recherche
		// sinon ils seraient consideres desactives lors de la validation du
		// formulaire d'activation
		else {
			$results = $scores = array();
			foreach($lpf as $adresse) {
				$info = plugin_get_infos($adresse);

				// on compte les points
				$find = '';

				if (false !== stripos($info['nom'], $recherche))
					$find='titre';
				elseif (false !== stripos($info['prefix'], $recherche)) 
					$find='titre';
				elseif (false !== stripos($info['auteur'], $recherche)) 
					$find='auteur';
				//if (false !== stripos($adresse, $recherche)) 
				//	$find[]='adresse';			
				elseif (false !== stripos($info['description'], $recherche)) 
					$find='description';	

				if ($find) {
					$results[] = $adresse;
					$scores[$adresse] = $find;
				}
			}

			$nb = count($results);
			$format= ($f = _request('format')) ? $f : 'recherche';
			$corps = plugins_generer_liens_liste($lpf, $lcpa, $format,true, $recherche)
					. affiche_les_plugins($results, $lcpa, $format, $scores);
			// ajouter les hidden
			$hiddens = array_diff($lcpa,$results);
			foreach ($hiddens as $h){
				$name = 's' . substr(md5("statusplug_$h"),0,16);
				$corps .= "\n<input type='hidden' name='$name' value='O' />";
			}
			
			
			// infos sur les paquets distants
			include_spip('inc/charger_plugin');
			$plugs_distants = liste_plugins_distants(true);
			//spip_log($plugs_distants,'plugins');
		}

		return array($corps,$nb);
	}
}

function plugins_generer_liens_liste(&$lpf, &$lcpa, $format, $tous=false, $recherche=""){
	$url_tous = parametre_url(self(),'recherche_plugin','');
	$url = parametre_url(self(), 'recherche_plugin', $recherche);
	foreach(array('recherche_titre','recherche_description','recherche_repertoire','recherche_auteur') as $r)
		$url = parametre_url($url, $r, _request($r));
	
	$lien_format = ($format!='liste')
		?("<a href='".parametre_url($url,'format','liste')."' class='ajax'>"._T('plugins_vue_liste')."</a>")
	 	:("<a href='".parametre_url($url,'format','arbre')."' class='ajax'>"._T('plugins_vue_hierarchie')."</a>");

	return "<p>$lien_format | "._T('plugins_actifs',array('count'=>count($lcpa)))."\n"
	  . " | <a href='". parametre_url($url_tous,'afficher_tous_plugins', $tous?'oui':'') ."' class='ajax'>"
	  ._T('plugins_disponibles',array('count'=>count($lpf)))."</a></p>\n";
}

// http://doc.spip.org/@affiche_les_plugins
function affiche_les_plugins($liste_plugins, $liste_plugins_actifs, $format='arbre', $scores=array()){
	if ($format=='liste'){
		$liste_plugins = array_flip($liste_plugins);
		foreach(array_keys($liste_plugins) as $chemin) {
			$info = plugin_get_infos($chemin);
			$liste_plugins[$chemin] = strtoupper(trim(typo(translitteration(unicode2charset(html2unicode($info['nom']))))));
		}
		asort($liste_plugins);
		$res = affiche_liste_plugins($liste_plugins,$liste_plugins_actifs);
	}
	elseif ($format == 'recherche') {
		$liste_plugins = array_flip($liste_plugins);
		foreach(array_keys($liste_plugins) as $chemin) {
			$liste_plugins[$chemin] = $scores[$chemin];
		}
		arsort($liste_plugins);		
		$res = affiche_recherche_plugins($liste_plugins,$liste_plugins_actifs);
	}
	else
		$res = affiche_arbre_plugins($liste_plugins,$liste_plugins_actifs);
	return http_script("
	jQuery(function(){
		jQuery('input.check').click(function(){
			jQuery(this).parent().toggleClass('nomplugin_on');
		});
		jQuery('div.nomplugin a[@rel=info]').click(function(){
			var prefix = jQuery(this).parent().prev().attr('name');
			if (!jQuery(this).siblings('div.info').html()) {
				jQuery(this).siblings('div.info').prepend(ajax_image_searching).load(
					jQuery(this).attr('href').replace(/admin_plugin|plugins/, 'info_plugin'), {},
					function() {
						document.location = '#' + prefix;
					}
				);
			} else {
				if (jQuery(this).siblings('div.info').toggle().attr('display') != 'none') {
					document.location = '#' + prefix;
				}
			}
			return false;
		});
	});
	") . $res;
}

// http://doc.spip.org/@affiche_block_initiale
function affiche_block_initiale($initiale,$block,$block_actif){
	if (strlen($block)){
		return "<li>"
		  . bouton_block_depliable($initiale,$block_actif?true:false)
		  . debut_block_depliable($block_actif)
		  . "<ul>$block</ul>"
		  . fin_block()
		  . "</li>";
	}
	return "";
}

// http://doc.spip.org/@affiche_liste_plugins
function affiche_liste_plugins($liste_plugins, $liste_plugins_actifs){
	$block_par_lettre = count($liste_plugins)>10;
	$fast_liste_plugins_actifs = array_flip($liste_plugins_actifs);
	$maxiter=1000;
	$res = '';
	$block = '';
	$initiale = '';
	$block_actif = false;
	foreach($liste_plugins as $plug => $nom){
		if (($i=substr($nom,0,1))!==$initiale){
			$res .= $block_par_lettre ? affiche_block_initiale($initiale,$block,$block_actif): $block;
			$initiale = $i;
			$block = '';
			$block_actif = false;
		}
		// le rep suivant
		$actif = @isset($fast_liste_plugins_actifs[$plug]);
		$block_actif = $block_actif | $actif;
		$id = substr(md5($plug),0,16);
		$block .= "<li>"
			. ligne_plug($plug, $actif, $id)
			. "</li>\n";
	}
	$res .= $block_par_lettre ? affiche_block_initiale($initiale,$block,$block_actif): $block;
	return "<ul>"
	. $res
	. "</ul>";
}

// http://doc.spip.org/@affiche_liste_plugins
function affiche_recherche_plugins($liste_plugins, $liste_plugins_actifs){
	//$block_par_lettre = count($liste_plugins)>10;
	$fast_liste_plugins_actifs = array_flip($liste_plugins_actifs);
	$maxiter=1000;
	$res = '';
	$block = '';
	$initiale = '';
	$block_actif = false;
	foreach($liste_plugins as $plug => $nom){
		if ($nom!==$initiale){
			//$res .= $block_par_lettre ? affiche_block_initiale(_T($initiale),$block,$block_actif): $block;
			$res .= affiche_block_initiale(_T($initiale),$block,$block_actif);
			$initiale = $nom;
			$block = '';
			$block_actif = false;
		}
		// le rep suivant
		$actif = @isset($fast_liste_plugins_actifs[$plug]);
		$block_actif = $block_actif | $actif;
		$id = substr(md5($plug),0,16);
		$block .= "<li>"
			. ligne_plug($plug, $actif, $id)
			. "</li>\n";
	}
	//$res .= $block_par_lettre ? affiche_block_initiale(_T($initiale),$block,$block_actif): $block;
	$res .= affiche_block_initiale(_T($initiale),$block,$block_actif);
	return "<ul>"
	. $res
	. "</ul>";
}

// http://doc.spip.org/@tree_open_close_dir
function tree_open_close_dir(&$current,$target,$deplie=array()){
	if ($current == $target) return "";
	$tcur = explode("/",$current);
	$ttarg = explode("/",$target);
	$tcom = array();
	$output = "";
	// la partie commune
	while (reset($tcur)==reset($ttarg)){
		$tcom[] = array_shift($tcur);
		array_shift($ttarg);
	}
	// fermer les repertoires courant jusqu'au point de fork
	while($close = array_pop($tcur)){
		$output .= "</ul>\n";
		$output .= fin_block();
		$output .= "</li>\n";
	}
	$chemin = "";
	if (count($tcom))
		$chemin .= implode("/",$tcom)."/";
	// ouvrir les repertoires jusqu'a la cible
	while($open = array_shift($ttarg)){
		$visible = @isset($deplie[$chemin.$open]);
		$chemin .= $open . "/";
		$output .= "<li>";
		$output .= bouton_block_depliable($chemin,$visible);
		$output .= debut_block_depliable($visible);

		$output .= "<ul>\n";
	}
	$current = $target;
	return $output;
}

// http://doc.spip.org/@affiche_arbre_plugins
function affiche_arbre_plugins($liste_plugins, $liste_plugins_actifs){
	$racine = basename(_DIR_PLUGINS);
	$init_dir = $current_dir = "";
	// liste des repertoires deplies : construit en remontant l'arbo de chaque plugin actif
	// des qu'un path est deja note deplie on s'arrete
	$deplie = array($racine=>true);
	$fast_liste_plugins_actifs=array();
	foreach($liste_plugins_actifs as $key=>$plug){
		$fast_liste_plugins_actifs["$racine/$plug"]=true;
		$dir = dirname("$racine/$plug");$maxiter=100;
		while(strlen($dir) && !isset($deplie[$dir]) && $dir!=$racine && $maxiter-->0){
			$deplie[$dir] = true;
			$dir = dirname($dir);
		}
	}
	
	// index repertoires --> plugin
	$dir_index=array();
	foreach($liste_plugins as $key=>$plug){
		$liste_plugins[$key] = "$racine/$plug";
		$dir_index[dirname("$racine/$plug")][] = $key;
	}
	
	$visible = @isset($deplie[$current_dir]);
	$maxiter=1000;

	$res = '';
	while (count($liste_plugins) && $maxiter--){
		// le rep suivant
		$dir = dirname(reset($liste_plugins));
		if ($dir != $current_dir)
			$res .= tree_open_close_dir($current_dir,$dir,$deplie);
			
		// d'abord tous les plugins du rep courant
		if (isset($dir_index[$current_dir]))
			foreach($dir_index[$current_dir] as $key){
				$plug = $liste_plugins[$key];
				$actif = @isset($fast_liste_plugins_actifs[$plug]);
				$id = substr(md5($plug),0,16);
				$res .= "<li>"
				. ligne_plug(substr($plug,strlen($racine)+1), $actif, $id)
				. "</li>\n";
				unset($liste_plugins[$key]);
			}
	}
	$res .= tree_open_close_dir($current_dir,$init_dir, true);

	return "<ul>"
	. $res
	. "</ul>";
}

// http://doc.spip.org/@ligne_plug
function ligne_plug($plug_file, $actif, $id){
	global $spip_lang_right;
	static $id_input=0;
	static $versions = array();

	$erreur = false;
	$vals = array();

	$info = plugin_get_infos($plug_file);

	// plug pour CFG
	if ($actif
	AND defined('_DIR_PLUGIN_CFG')) {
		if (include_spip('inc/cfg') // test CFG version >= 1.0.5
		AND $i = icone_lien_cfg(_DIR_PLUGINS.$plug_file))
			$s .= '<div style="float:right;">'.$i.'</div>';
	}


	$versions[$info['prefix']] = isset($versions[$info['prefix']]) ?
			$versions[$info['prefix']] + 1 : '';
	$s .= "<div id='" . $info['prefix'] . $versions[$info['prefix']] . "' class='nomplugin ".($actif?'nomplugin_on':'')."'>";
	if (isset($info['erreur'])){
		$s .=  "<div class='plugin_erreur'>";
		$erreur = true;
		foreach($info['erreur'] as $err)
			$s .= "/!\ $err <br/>";
		$s .=  "</div>";
	}

	$etat = 'dev';
	if (isset($info['etat']))
		$etat = $info['etat'];
	$nom = typo($info['nom']);

	$id = substr(md5("aide_$plug_file"),0,8);
	$puce_etat = array(
	"dev"=>"<img src='". chemin_image('puce-poubelle.gif') . "' width='9' height='9' alt='dev' />",
	"test"=>"<img src='". chemin_image('puce-orange.gif') . "' width='9' height='9' alt='dev' />",
	"stable"=>"<img src='". chemin_image('puce-verte.gif') . "' width='9' height='9' alt='dev' />",
	"experimental"=>"<img src='". chemin_image('puce-rouge.gif') . "' width='9' height='9' alt='dev' />",
	);
	
	if (isset($puce_etat[$etat]))
	$s .= $puce_etat[$etat];

	if (!$erreur){
		$name = 's' . substr(md5("statusplug_$plug_file"),0,16);
		$s .= "\n<input type='checkbox' name='$name' id='label_$id_input' value='O'";
		$s .= $actif?" checked='checked'":"";
		$s .= " class='check' />";
		$s .= "\n<label for='label_$id_input'>"._T('activer_plugin')."</label>";
	}
	$id_input++;

	$url_stat = generer_url_ecrire(_request('exec'),"plug=".urlencode($plug_file));
	$s .= "<a href='$url_stat' rel='info' class='titre_plugin' title='$plug_file'>$nom</a>";

	// afficher les details d'un plug en secours ; la div sert pour l'ajax
	$s .= "<div class='info'>";
	if (urldecode(_request('plug'))==$plug_file)
		$s .= affiche_bloc_plugin($plug_file, $info);
	$s .= "</div>";

	$s .= "</div>";
	return $s;
}


function plugins_script_info_plug(){
	$script = "
	jQuery(document).ready(function() {
	
	});";
	return http_script($script);
}
	
?>
