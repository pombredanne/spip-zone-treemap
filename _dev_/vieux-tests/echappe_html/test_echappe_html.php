<?php
// Test pour echappe_html (toggg) horus: actuel: 127, recursif: 218, nouveau: 126
require_once '../usage_ressources.php';
/*
function code_echappement($echap, $source) {
	return $echap;
}
*/
define('_BALISES_BLOCS',
	'div|pre|ul|ol|li|blockquote|h[1-6r]|'
	.'t(able|[rdh]|body|foot|extarea)|'
	.'form|object|center|marquee|address|'
	.'d[ltd]|script|noscript|map|button|fieldset');

function code_echappement($rempl, $source='') {
	if (!strlen($rempl)) return '';

	// Convertir en base64
	$base64 = base64_encode($rempl);

	// Tester si on echappe en span ou en div
	$mode = preg_match(',</?('._BALISES_BLOCS.')[>[:space:]],iS', $rempl) ?
		'div' : 'span';
	$nn = ($mode == 'div') ? "\n\n" : '';

	return
		"<$mode class=\"base64$source\" title=\"$base64\"></$mode>$nn";
}
function echappe($regs) {
	return
	 "\n@ {$regs[1]} ({$regs[2]}) ===========================================\n{$regs[3]}\n========================================================\n";
}
function traiter_echap_html($regs) {
	return echappe($regs);
}
function traiter_echap_code($regs) {
	return echappe($regs);
}
function traiter_echap_cadre($regs) {
	return echappe($regs);
}
function traiter_echap_frame($regs) {
	return echappe($regs);
}
function traiter_echap_script($regs) {
	return echappe($regs);
}

function echappe_html_tries($letexte, $source='', $no_transform=false,
$preg='') {
	if (!strlen($letexte)) return '';

	if (!$preg) $preg = ',<(html|code|cadre|frame|script)'
			.'(\s[^>]*)?'
			.'>(.*)</\1>,Uims';
	while (preg_match_all(
	$preg,
	$letexte, $matches, PREG_SET_ORDER))
	foreach ($matches as $regs) {
		// balise d'ouverture == imbrication non finie
		if (strpos($regs[$endr = count($regs) - 1], '<' . $regs[1]) !== false
//		 && preg_match('#<' . $regs[1] . '\s?[^>]*>#Uims', $regs[$endr])
		 && (!isset($lastreg) || $lastreg != $regs[$endr])) {
			if (isset($pmid) || (($posmid = strpos($preg, '(.*)'))
						&& ($pmid = '.') && ($psav = $preg))) {
				$pmid = '(?:<\1[^>]*>(' . $pmid . '*)</\1>|.)';
				$preg = substr_replace($psav, $pmid, $posmid + 1, 1);
				$lastreg = $regs[$endr];
//				echo $endr . htmlentities($preg) . "<br />" . htmlentities($regs[$endr]) . "<br />";
				continue 2;
			}
		}		// echappements tels quels ?
		if ($no_transform) {
			$echap = $regs[0];
		}

		// sinon les traiter selon le cas
		else if (function_exists($f = 'traiter_echap_'.strtolower($regs[1])))
			$echap = $f($regs);
		else if (function_exists($f = $f.'_dist'))
			$echap = $f($regs);

		$letexte = str_replace($regs[0],
			code_echappement($echap, $source),
			$letexte);
	}

	// Gestion du TeX
	if (strpos($letexte, "<math>") !== false) {
		include_spip('inc/math');
		$letexte = traiter_math($letexte, $source);
	}

	// Echapper le php pour faire joli (ici, c'est pas pour la securite)
	if (preg_match_all(
	',<[?].*($|[?]>),UisS',
	$letexte, $matches, PREG_SET_ORDER))
	foreach ($matches as $regs) {
		$letexte = str_replace($regs[0],
			code_echappement(highlight_string($regs[0],true), $source),
			$letexte);
	}

	return $letexte;
}

function echappe_html_new($letexte, $source='', $no_transform=false,
$preg='') {
	if (!strlen($letexte)) return '';

	if (!$preg) $preg = '(html|code|cadre|frame|script)';
	$preg = '#(.*)<(?:' . $preg .'(\s[^>]*)?|/' . $preg . ')>#UimsS';
	if (preg_match_all(
	$preg,
	$letexte, $matches, PREG_SET_ORDER)) {
		$i = 0;
		$imax = count($matches);
		while ($i < $imax) {
			// prochain ouvrant
			while (empty($matches[$i][2])) {
				if (++$i >= $imax) {
					break 2;
				}
			}
			if (!isset($matches[$i][3])) {
				$matches[$i][3] = '';
			}
			$regs = array('<' . $matches[$i][2] . $matches[$i][3] . '>',
					 $matches[$i][2], $matches[$i][3], '');

			// fermant correspondant
			$open = 1;
			while (++$i < $imax) {
				$open += ($matches[$i][2] == $regs[1])
					 - (!empty($matches[$i][4]) && $matches[$i][4] == $regs[1]);
				if (!$open) {
					break;
				}
				$regs[3] .= $matches[$i][0];
			}
			if ($open) {
				break;
			}
			$regs[0] .= $regs[3] . $matches[$i][0];
			$regs[3] .= $matches[$i][1];

			// echappements tels quels ?
			if ($no_transform) {
				$echap = $regs[0];
			}

			// sinon les traiter selon le cas
			else if (function_exists($f = 'traiter_echap_'.strtolower($regs[1])))
				$echap = $f($regs);
			else if (function_exists($f = $f.'_dist'))
				$echap = $f($regs);

			$letexte = str_replace($regs[0],
				code_echappement($echap, $source),
				$letexte);
		}
	}

	// Gestion du TeX
	if (strpos($letexte, "<math>") !== false) {
		include_spip('inc/math');
		$letexte = traiter_math($letexte, $source);
	}

	// Echapper le php pour faire joli (ici, c'est pas pour la securite)
	if (preg_match_all(
	',<[?].*($|[?]>),UisS',
	$letexte, $matches, PREG_SET_ORDER))
	foreach ($matches as $regs) {
		$letexte = str_replace($regs[0],
			code_echappement(highlight_string($regs[0],true), $source),
			$letexte);
	}

	return $letexte;
}

function echappe_html($letexte, $source='', $no_transform=false,
$preg='') {
	if (!strlen($letexte)) return '';

	if (!$preg) $preg = ',<(html|code|cadre|frame|script)'
			.'(\s[^>]*)?'
			.'>(.*)</\1>,UimsS';
	if (preg_match_all(
	$preg,
	$letexte, $matches, PREG_SET_ORDER))
	foreach ($matches as $regs) {
		// echappements tels quels ?
		if ($no_transform) {
			$echap = $regs[0];
		}

		// sinon les traiter selon le cas
		else if (function_exists($f = 'traiter_echap_'.strtolower($regs[1])))
			$echap = $f($regs);
		else if (function_exists($f = $f.'_dist'))
			$echap = $f($regs);

		$letexte = str_replace($regs[0],
			code_echappement($echap, $source),
			$letexte);
	}

	// Gestion du TeX
	if (strpos($letexte, "<math>") !== false) {
		include_spip('inc/math');
		$letexte = traiter_math($letexte, $source);
	}

	// Echapper le php pour faire joli (ici, c'est pas pour la securite)
	if (preg_match_all(
	',<[?].*($|[?]>),UisS',
	$letexte, $matches, PREG_SET_ORDER))
	foreach ($matches as $regs) {
		$letexte = str_replace($regs[0],
			code_echappement(highlight_string($regs[0],true), $source),
			$letexte);
	}

	return $letexte;
}
function echappe_html_recursif($letexte, $source='', $no_transform=false,
$preg='') {
	if (!strlen($letexte)) return '';

	if (!$preg) $preg = 		',<(html|code|cadre|frame|script)'
			.'(\s[^>]*)?'
			.'>((?:(?R)|.)*)</\1>,UimsS';
	if (preg_match_all(
	$preg,
	$letexte, $matches, PREG_SET_ORDER))
	foreach ($matches as $regs) {
		// echappements tels quels ?
		if ($no_transform) {
			$echap = $regs[0];
		}

		// sinon les traiter selon le cas
		else if (function_exists($f = 'traiter_echap_'.strtolower($regs[1])))
			$echap = $f($regs);
		else if (function_exists($f = $f.'_dist'))
			$echap = $f($regs);

		$letexte = str_replace($regs[0],
			code_echappement($echap, $source),
			$letexte);
	}

	// Gestion du TeX
	if (strpos($letexte, "<math>") !== false) {
		include_spip('inc/math');
		$letexte = traiter_math($letexte, $source);
	}

	// Echapper le php pour faire joli (ici, c'est pas pour la securite)
	if (preg_match_all(
	',<[?].*($|[?]>),UisS',
	$letexte, $matches, PREG_SET_ORDER))
	foreach ($matches as $regs) {
		$letexte = str_replace($regs[0],
			code_echappement(highlight_string($regs[0],true), $source),
			$letexte);
	}

	return $letexte;
}

function comparaison($txt) {
	$ANCIEN = echappe_html($txt[0]);
	$RECURSIF = echappe_html_recursif($txt[0]);
	$NOUVEAU = echappe_html_tries($txt[0]);
	$res = array();
	if ($NOUVEAU == $RECURSIF) {
		$return = true;
		if ($NOUVEAU == $ANCIEN) {
			$res['ANCIEN == NOUVEAU == RECURSIF'] = $NOUVEAU;
		} else {
			$res['ANCIEN'] = $ANCIEN;
			$res['NOUVEAU == RECURSIF'] = $NOUVEAU;
		}
	} else {
		$return = false;
		if ($NOUVEAU == $ANCIEN) {
			$res['ANCIEN == NOUVEAU'] = $NOUVEAU;
			$res['RECURSIF'] = $RECURSIF;
		} else {
			$res['ANCIEN'] = $ANCIEN;
			$res['RECURSIF'] = $RECURSIF;
			$res['NOUVEAU'] = $NOUVEAU;
		}
	}
	if ($GLOBALS['argc'] < 2) {
		foreach ($res as $comp => $val) {
			echo $comp . ":\n" . $val . "\n\n";
		}
	}
	return $return;
}

$test['simple imbriqué'] = 
'avant 1<code class="php"> avant 2<code>le code</code>apres 1</code>apres 2';
$test['complexe imbriqué'] = <<<EOT
{{{code class="php"}}}
avant blah
<code class="blah">
blah in
balh 2
</code>
apres blah et avant php
<code class="php telecharge">
<?php
function uncomment(\$source) {
	return preg_replace(
		'#(?:/\*(?:(?R)|.)*\*/|//.*$)|(([\'"])(?:.*)(?<!\\\\)\2|([^\'"/]+))#msU',
		"$1", \$source);
}
?>
</code>
{{{code tout court}}}
<code>
<?php
function uncomment(\$source) {
	return preg_replace(
		'#(?:/\*(?:(?R)|.)*\*/|//.*$)|(([\'"])(?:.*)(?<!\\\\)\2|([^\'"/]+))#msU',
		"$1", \$source);
}
?>
</code>
{{{Tu vois ?}}}
Voilà , <code><code class="xxx">insere tout avec des <br /> , pas de <div class="spip_code"></code></code>

On peut croire que c'est embétant , faut mettre une div autour pour encadrer , mais cela permet d'orienter geshi en cours de route comme dans [Compte à rebours (revisited)->article6]
<?
EOT;
$test['unicode sans rien'] = 
"azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-";
$test['sans rien'] = 
"astuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolos";
$test['code sans imbrication'] = 
'avant 1<code class="php"> avant 2 code le code code apres 1</code>apres 2';
$test['pourriture'] = <<<EOT
Le code mis en <code><code></code> ou en <code><cadre></code> peut lui même contenir  <code><code></code> ou <code><cadre></code> ...
<code class="xxx">
ça 'xiste pô
</code>
Je voudrais présenter l'usage du plugin coloration_code qui fournit une extension à geshi : la classe "spip".
Je mets donc :
<cadre>
Pour insérer du code coloré, il suffit de rajouter {class="xxx"} au tag code de spip:
<code class="php">
// mon morceau de php
\$variable = "blah";
// ...
</code>
et le tour est joué
</cadre>
Ici présenté avec le <code><cadre></code> d'origine de spip

Mais si je mets le même texte dans <code><cadre class="spip"></code> , voilà le résultat:
<cadre class="spip">
Pour insérer du code coloré, il suffit de rajouter {class="xxx"} au tag code de spip:
<code class="php">
// mon morceau de php
\$variable = "blah";
// ...
</code>
et le tour est joué
</cadre>

Voilà c'est corrigé, [->http://trac.rezo.net/trac/spip-zone/changeset/3823] , mais on s'est aperçu que le problème est le même pour le &lt;code> d'origine de spip. (voir plus bas)

En fait, l'expression régulière (regexp) utilisée par le plugin pour récupérer les morceaux de code à colorer devrait être récursive !

<code>
',<(cadre|code)[[:space:]]+class="(.*)"[[:space:]]*>(.*)</(cadre|code)>,Uims'
</code> est insuffisant.

Il faut un truc comme:
<div style="color: red;">
<code>
',<(cadre|code)[[:space:]]+class=("|\')(.*)\2([^>])*>((?:((?R))|.)*)</\1>,Uims'
</code>
</div>
Les différences:
- la balise de fermeture est recherchée avec \1 , c'est à dire cadre ou code comme trouvé en début d'espression
- pour l'intérieur (le code en lui-même) on ne cherche pas seulement .* (n'importe quoi) mais (?:((?R))|.)* : le motif complet recherché , soit un &lt;code>...&lt/code> ou cadre imbriqué , ou n'importe quoi (.) . C'est une regexp récursive.
- une amélioration supplémentaire : le paramètre class peut être donné entre simple ou double apostrophes.
- le texte dans le tag après class="xxx" est capté ce qui pourra permettre des extensions futures, comme insérer des attributs supplémentaires au code html fabriqué.

Essai de code dans code (au lieu de cadre comme tout en haut)

<code>
Pour insérer du code coloré, il suffit de rajouter {class="xxx"} au tag code de spip:
<code class="php">
// mon morceau de php
\$variable = "blah";
// ...
</code>
et le tour est joué
</code>

Donc comme l'ancien coloration_code, le  &lt;/code> est mangé et "et le tour est joué" apparait hors-code.
EOT;
/*
Je sais pas pourquoi , mon php5.1.4 cli s'est mis à boucler avec ça,
d'où echappe_html_recursif() ... le (?R) a-t-il encore frappé ?

$funtab = array(
	'actuel'=>'echappe_html',
	'recursif'=>create_function('$t', "return echappe_html(\$t, '', false,
		',<(html|code|cadre|frame|script)(\s[^>]*)?>((?:(?R)|.)*)</\1>,UimsS');"),
	'nouveau'=>'echappe_html_new');
*/
//$res = ru_get($funtab = array('essais'=>'echappe_html_tries'), $test, 1, 'comparaison');
//die();
$funtab = array(
	'actuel'=>'echappe_html',
	'recursif'=>'echappe_html_recursif',
	'nouveau'=>'echappe_html_new',
	'essais'=>'echappe_html_tries');

$res = ru_get($funtab, $test, 1000, 'comparaison');

/*
foreach ($res as $genre=>$out) {
	echo "************* $genre **************\n";
	print_r($out);
}
*/
echo ru_out_spip($res);
echo "\n\n";
echo ru_out_php($res);
