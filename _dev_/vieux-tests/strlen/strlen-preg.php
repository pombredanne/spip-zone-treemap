﻿<?php

//
// Gerer les outils mb_string
//
// http://doc.spip.org/@spip_substr

require_once '../usage_ressources.php';

function spip_substr_split($c, $start=0, $length = NULL) {
	$r = preg_split(',(.[\x80-\xBF]*),S', $c, -1,
		PREG_SPLIT_DELIM_CAPTURE);
	array_pop($r);
	if (isset($length))
		return join('', array_slice($r, 2*$start, 2*$length));
	else
		return join('', array_slice($r, 2*$start));
}


// http://doc.spip.org/@spip_strlen
function spip_strlen($c) {
	return strlen(preg_replace(',[\x80-\xBF],', '', $c));
}

function spip_substr_fil($c, $start=0, $length = NULL) {

	// Cas pathologique
	if ($length === 0)
		return '';

	// S'il y a un demarrage, on se positionne
	if ($start > 0)
		$c = substr($c, strlen(spip_substr_fil($c, 0, $start)));
	elseif ($start < 0)
		return spip_substr_fil($c, spip_strlen($c)+$start, $length);

	if (!$length)
		return $c;

	if ($length > 0) {
		// on prend n fois la longueur desiree, pour etre surs d'avoir tout
		// (un caractere utf-8 prenant au maximum n bytes)
		$n = 0; while (preg_match(',[\x80-\xBF]{'.(++$n).'},', $c));
		$c = substr($c, 0, $n*$length);
		// puis, tant qu'on est trop long, on coupe...
		while (($l = spip_strlen($c)) > $length)
			$c = substr($c, 0, $length - $l);
		return $c;
	}

	// $length < 0
	return spip_substr_fil($c, 0, spip_strlen($c)+$length);
}


function spip_substr_preg_u($c, $start=0, $length=NULL) {
	$l=0;
	// version manuelle
	if ($start==0) {
		$re_start='';
	} else {
		if($start<0) {
			$start = ($l=spip_strlen($c))+$start;
		}
		$re_start= ".\{$start}";
	}
	
	if (!$length) {
		$re_end="(.*)";
	} else {
		if($length<0) {
			$length = ($l?$l:spip_strlen($c))+$length-$start;
		}
		$re_end="(.\{0,$length})";
	}

	if(preg_match("/^${re_start}${re_end}/usS", $c, $m)) {
		return $m[1];
	}
	return FALSE;
}

$test = array();
$test['unicode,0,20'] = array("azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-", 0,20);

$test['unicode,10,20'] = array("azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-", 10,20);

$test['unicode,-20,-10'] = array("azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-azerty小さくてもグローバルなケベックの村-", -20,-10);

$test['ascii,-3,-1'] = array("astuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolos", -3,-1);

$test['ascii,475'] = array("astuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolosastuce & travaux de mode rigolos", 475);

$funtab = array(
	'split' => 'spip_substr_split',
	'fil' => 'spip_substr_fil',
	'preg_u' => 'spip_substr_preg_u');

$res = ru_get($funtab, $test, 10000);

foreach ($res as $genre=>$out) {
	echo "************* $genre **************\n";
	print_r($out);
}

