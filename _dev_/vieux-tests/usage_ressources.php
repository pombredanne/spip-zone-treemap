<?php
/*
 *  usage_ressources.php (c) toggg 2006 -- licence GPL
 *
 *  pour comparer les ressources utilisée par des fonctions sur un jeu de test
 */
set_time_limit(0);
error_reporting(E_ALL);
ini_set('display_errors', true);

function ru_description($item = '')
{
	static $desc = array(
		'time' => 'Temps reel (humain)',
		'utime' => 'Temps cpu user',
		'stime' => 'Temps cpu systeme',
		'pagereclaim' => 'Pages reclamees',
		'pagefaults' => 'Fautes de pages',
		'swap' => 'Nombre de swaps',
		'vcsw' => 'Changements de contexte volontaires',
		'ivcsw' => 'Changements de contexte involontaires'
	);
	if ($item) {
		return $desc[$item];
	}
	return $desc;
}

function ru_out_php(&$res, $opt = array())
{
	foreach (array('resnam' => '')
			 as $optnam => $def) {
		$$optnam = empty($opt[$optnam]) ? $def : $opt[$optnam];
	}
	return "<?php \$ru_res['" .
				( $resnam ? $resnam : "ru_out_" . phpversion()) .
				"'] = unserialize('" .	serialize($res) . "');\n?>\n";
}

function ru_out_spip(&$res, $opt = array())
{
	foreach (array('titre' => ' Spip: Usage Ressources')
			 as $optnam => $def) {
		$$optnam = empty($opt[$optnam]) ? $def : $opt[$optnam];
	}
	$desc = ru_description();
	$out = '{{{ ' . phpversion() . ' ' . $titre . " }}}";
	foreach (array_keys($res) as $genre) {
		$out .= "\n\n{{{ " . $genre . ' (' . ($res[$genre]['ok'] ? 'ok' : 'bad') .
		 ") }}}\n| {Ressources} |";
		unset($res[$genre]['ok']);
		$funk = array_keys($res[$genre]);
		foreach ($funk as $fun) {
			$out .= ' {{' . $fun . '}} |';
		}
		foreach (array_keys($res[$genre][$funk[0]]) as $ru) {
			$out .= "\n| {{" . $ru . '}} |';
			foreach ($funk as $fun) {
				$out .= ' ' . $res[$genre][$fun][$ru] . ' |';
			}
		}
	}
	return $out . "\n";
}

function microtime_float()
{
	list($usec, $sec) = explode(" ", microtime());
	return ((float)$usec + (float)$sec);
}

function ru_snapshot($was = array())
{
	$ru = getrusage();
	$is=array(
		'time' => microtime_float(),
		'utime' => (float)$ru["ru_utime.tv_sec"] +
				   (float)$ru["ru_utime.tv_usec"]*.000001,
		'stime' => (float)$ru["ru_stime.tv_sec"] +
				   (float)$ru["ru_stime.tv_usec"]*.000001,
		'pagereclaim' => $ru["ru_minflt"],
		'pagefaults' => $ru["ru_majflt"],
		'swap' => $ru["ru_nswap"],
		'vcsw' => $ru["ru_nvcsw"],
		'ivcsw' => $ru["ru_nivcsw"]
	);
	if (!$was) {
		return $is;
	}
	foreach ($was as $key=>$val) {
		$is[$key] -= $val;
	}
	return $is;
}

/*
 Attention, si vous testez avec un unique parametre array $arr, vous devez passer array($arr)
 evidemment on pourrait appeler call_user_func , on prefere une sitution "réelle"
*/
function ru_get(&$funtab, &$testab, $nb = 100000, $comparaison='')
{
	$res=array();
	foreach (array_keys($testab) as $genre) {
		$res[$genre] = array();
		if (!is_array($testab[$genre])) {
			$tst = array(&$testab[$genre]);
		} else {
			$tst = &$testab[$genre];
		}
		if ($comparaison) {
			$res[$genre]['ok'] = comparaison($tst);
		}
		foreach ($funtab as $sol=>$fun) {
			echo '*********** ' . $genre . ' / ' . $sol . "\n";
			$dep = ru_snapshot();
			switch (count($tst)) {
			case 1:
				for ($n=0 ; $n < $nb; $n++) {
					$fun($tst[0]);
				}
				break;
			case 2:
				for ($n=0 ; $n < $nb; $n++) {
					$fun($tst[0], $tst[1]);
				}
				break;
			case 3:
				for ($n=0 ; $n < $nb; $n++) {
					$fun($tst[0], $tst[1], $tst[2]);
				}
				break;
			case 4:
				for ($n=0 ; $n < $nb; $n++) {
					$fun($tst[0], $tst[1], $tst[2], $tst[3]);
				}
				break;
			default:
				for ($n=0 ; $n < $nb; $n++) {
					call_user_func_array($fun, $tst);
				}
				break;
			}
			$res[$genre][$sol] = ru_snapshot($dep);
		}
	}
	return $res;
}
