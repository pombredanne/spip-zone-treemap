<?php

	// detecter les robots, et les calmer
	function bot_fais_attention() {
		define('_DIR_TMP', _DIR_RACINE.'tmp/');
		$ua = strtolower($_SERVER["HTTP_USER_AGENT"]);

		if (isset($_GET['ua']))
			$ua = $_GET['ua'];

		#spip_log($ua, 'robots');

		$gentil = (
			strpos($ua, 'googlebot')
			OR strpos($ua, 'yahoo! slurp')
			OR strpos($ua, 'moreoverbot')
		);

		if ($gentil) {
			#spip_log('gentil', 'robots');
			$url = $_SERVER['REQUEST_URI'];
			foreach ($_GET as $cle => $var) {
				if (substr($cle, 0,6) == 'debut_') {
					include_spip('inc/filtres');
					$url = parametre_url($url, $cle, '');
				}
				if ($url !== $_SERVER['REQUEST_URI']) {
					spip_log('redirige '. $_SERVER['REQUEST_URI'] .' vers '.$url, 'robots');
					include_spip('inc/headers');
					redirige_par_entete($url);
				}
			}
		
		} else {
		
			$mechant = (
				strpos($ua, 'twiceler')
			);

			if ($mechant) {
				#spip_log('mechant', 'robots');
				include_spip('inc/headers');
				http_status(403); // forbidden
				die('bad bot');
			}
		}

	}

	bot_fais_attention();

?>
