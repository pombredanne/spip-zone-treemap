<?php

/*
    This file is part of SPIP

    Trad-Lang is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Trad-Lang is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Trad-Lang; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Copyright 2003-2008
        Florent Jugla <florent.jugla@eledo.com>,
        Philippe Riviere <fil@rezo.net>
        Chryjs <chryjs!@!free!.!fr>,
*/


require_once(dirname(__FILE__).'/inc_tradlang.php');
$tmp=_SALVATORE_TMP;

/* modules de SPIP requis - il y a surement plus propre... */
include_spip('base/abstract_sql');
include_spip('inc/vieilles_defs');

/* MAIN ***********************************************************************/

trad_log("\nlecteur\n Prend les fichiers de reference dans sa copie locale et met a jour la base de donnees\n\n\n");

$liste_sources=charger_fichier_traductions(); // chargement du fichier traductions.txt

foreach ($liste_sources as $source) {
	trad_log("==== Module ".$source[1]." =======================================\n");
	$liste_fic_lang= glob($tmp.$source[1].'/'.$source[1]."_*.php");
	// on doit absolument charger la langue principale en premier (a cause des MD5)
	$fic_lang_principal=$tmp.$source[1].'/'.$source[1].'_'.$source[2].".php";
	if (in_array($fic_lang_principal,$liste_fic_lang)) {
		$liste_id_orig = array();
		import_module($source,$fic_lang_principal,$liste_id_orig,1);
		foreach ($liste_fic_lang as $f)
			if ($f != $fic_lang_principal)
				import_module($source,$f,$liste_id_orig,0);
	}
	else {
		trad_log("|-- Pas de fichier lang ".$source[2]." pour le module ".$source[1]." : import impossible pour ce module\n");
	}
}

return 0;

/* MAIN ***********************************************************************/

//
// $orig = 0 : langue secondaire
// $orig = 1 : langue principale du module
//
function import_module($source=array(),$module='',&$liste_id_orig,$orig=0) {
	trad_log( "!\n+ Import de $module\n");
	$memtrad = $GLOBALS['idx_lang'] = 'i18n_'.crc32($module).'_tmp';
	$GLOBALS[$GLOBALS['idx_lang']] = null;

	$comm_fic_lang = charger_comm_fichier_langue($module);
	include $module;

	$str_lang = $GLOBALS[$memtrad];  // on a vu certains fichiers faire des betises et modifier idx_lang

	if (is_null($str_lang)) {
		trad_log("Erreur, fichier $module mal forme\n");
		return false;
	}

	// nettoyer le contenu de ses <MODIF>
	$status = array();
	foreach($str_lang as $id=>$v) {
		if (preg_match(',^<(MODIF|NEW|PLUS_UTILISE)>,US', $v, $r)) {
			$str_lang[$id] = preg_replace(',^(<(MODIF|NEW|PLUS_UTILISE)>)+,US', '', $v);
			$status[$id] = $r[1];
		}
		else	
			$status[$id] = '';
	}

	$fich = basename($module,".php");
	list($mod,$lang)=explode("_",$fich,2);

	if (1==$orig)
		$res = spip_query("SELECT id, str, md5 FROM trad_lang WHERE module='".$source[1]."' and lang='".$lang."' ");
	else
		$res = spip_query("SELECT id, str, md5 FROM trad_lang WHERE module='".$source[1]."' and lang='".$lang."' and status!='MODIF' ");
	$nb = spip_num_rows($res);
	if ($nb > 0)
		trad_log("!-- Fichier de langue $lang du module $mod deja inclus dans la base\n");

	// Si la langue est deja dans la base, on ne l'ecrase que s'il s'agit
	// de la langue source
	if ($nb == 0 OR $orig == 1) {
		// La liste de ce qui existe deja
		$existant = array();
		while ($t = spip_fetch_array($res))
			$existant[$t['id']] = $t['md5'];

		$bigwhere = "module='".$source[1]."' and lang='".$lang."'";


		$ajoutees = $inchangees = $supprimees = $modifiees = $ignorees = 0;

		// Dans ce qui arrive, il y a 4 cas :
		foreach (array_unique(array_merge(
			array_keys($existant), array_keys($str_lang)
		)) as $id) {
			$comm=(isset($comm_fic_lang[$id])) ? $comm_fic_lang[$id] : "";
			// * chaine neuve
			if (isset($str_lang[$id])
			AND !isset($existant[$id]))
			{
				unset($md5);
				if ($orig) {
					$md5 = md5($str_lang[$id]);
				} else if (!isset($liste_id_orig[$id])) {
					trad_log("!-- Chaine $id inconnue dans la langue principale\n");
					$ignorees++;
				} else {
					$md5 = $liste_id_orig[$id];
				}

				if (isset($md5))
				{
					spip_query("INSERT trad_lang (module,lang,id,str,comm,md5,status)
					VALUES ("
						._q($source[1]).', '
						._q($lang).', '
						._q($id).', '
						._q($str_lang[$id]).', '
						._q($comm).', '
						._q($md5).', '
						._q($status[$id])
						.")"
					);
					$ajoutees++;
				}
			}
			else
			// * chaine existante
			if (isset($str_lang[$id])
			AND isset($existant[$id]))
			{
				// * identique ? => NOOP
				$md5 = md5($str_lang[$id]);
				if ($md5 == $existant[$id]) {
					$inchangees++;
				}
				// * modifiee ? => UPDATE
				else {
					// modifier la chaine
					spip_query("UPDATE trad_lang SET
						str="._q($str_lang[$id])
						. ($orig
							? ", md5="._q($md5).", status=''"
							: ", md5="._q($existant[$id]).", status=''"
						)
						. ($comm
							? ", comm="._q($comm)." "
							: ""
						)
						." WHERE $bigwhere AND id="._q($id)
					);

					// signaler le status MODIF de ses traductions
					if ($orig)
						spip_query("UPDATE trad_lang SET status='MODIF'
							WHERE module='".$source[1]
							."' AND id="._q($id)
							." AND md5!="._q($md5)
						);
					$modifiees++;
				}
			}
			else
			// * chaine supprimee
			if (!isset($str_lang[$id])
			AND isset($existant[$id]))
			{
				// mettre au grenier
				spip_query("UPDATE trad_lang SET id="._q($source[1].'_'.$id)
				.", module='attic' WHERE id="._q($id)." AND module="._q($source[1]));
				$supprimees++;
			}

			if ($orig AND isset($str_lang[$id]))
				$liste_id_orig[$id]=md5($str_lang[$id]);

		}

		trad_log("!-- module ".$source[1].", $lang : $modifiees modifiees, $ajoutees ajoutees, $supprimees supprimees, $ignorees ignorees, $inchangees inchangees\n");
	}
} // import_module

//
// on le charge aussi en mode texte pour recuperer les commentaires
//
function charger_comm_fichier_langue($f) {

	$contenu=file_get_contents($f);

	$tab=preg_split("/\r\n|\n\r|\n|\r/", $contenu);

	$liste_trad=array();

	reset($tab);
	while (list(,$ligne) = each($tab))
	{
		$ligne=trim($ligne);
		if (strlen($ligne)>0) {
			if (preg_match("/[\s\t]*\'(.*?)\'[\s\t]*=>[\s\t]*\'(.*?)\'[\s\t]*,{0,1}[\s\t]*(#.*)?/",$ligne,$matches)) {
				if (isset($matches[1]) and isset($matches[3]) and strlen(trim($matches[3]))>0 ) {
					list(,$comm)=explode("#",$matches[3]);
					$liste_trad[$matches[1]]=trim($comm);
				}
			}
		}
	}
	reset($liste_trad);
	return $liste_trad;
} // charger_comm_fichier_langue


?>
