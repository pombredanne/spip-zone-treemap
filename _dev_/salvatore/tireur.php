<?php
/*
    This file is part of SPIP

    Trad-Lang is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Trad-Lang is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Trad-Lang; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Copyright 2003-2008
        Florent Jugla <florent.jugla@eledo.com>,
        Philippe Riviere <fil@rezo.net>
        Chryjs <chryjs!@!free!.!fr>,
*/


//il va chercher les fichiers dans le svn
require_once(dirname(__FILE__).'/inc_tradlang.php');
$tmp=_SALVATORE_TMP;

// pas de nettoyage, on va essayer de gerer les conflits intelligemment
// trad_log(exec("rm -rf ".$tmp."*"));

/* MAIN ***********************************************************************/

trad_log("\ntireur\n Va chercher les fichiers dans SVN et les depose dans sa copie locale\n\n");

$liste_sources=charger_fichier_traductions(); // chargement du fichier traductions.txt

$ret=0;
foreach($liste_sources as $source) {
	trad_log("\n===== Module ".$source[1]." ======================================\n");
	if (is_dir($tmp.$source[1].'/.svn'))
		$cmd = "svn update  --non-recursive ".$tmp.$source[1]."/";
	else
		$cmd = "svn checkout --non-recursive ". $source[0]."/ ".$tmp.$source[1].'/';
	trad_log("$cmd\n");
	trad_log(passthru($cmd)."\n");
	// controle des erreurs : requiert au moins 1 fichier par module !
	if (!file_exists($tmp.$source[1].'/'.$source[1].'_'.$source[2].".php")) {
		$ret=1;
		trad_log("! Erreur pas de fichier de langue conforme dans le module : $tmp".$source[1]."\n");
	}
}

return $ret;
/* MAIN ***********************************************************************/

?>