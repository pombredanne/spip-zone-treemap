<?php
/*
    This file is part of SPIP

    Trad-Lang is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Trad-Lang is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Trad-Lang; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Copyright 2003-2008
        Florent Jugla <florent.jugla@eledo.com>,
        Philippe Riviere <fil@rezo.net>
        Chryjs <chryjs!@!free!.!fr>,
*/

// il envoie les fichiers dans le svn


require_once(dirname(__FILE__).'/inc_tradlang.php');
require_once(_DIR_ETC.'salvatore_passwd.inc');

if (!isset($SVNUSER)
OR !isset($SVNPASSWD))
	die ('Veuillez indiquer $SVNUSER et $SVNPASSWD dans le fichier '._DIR_ETC.'salvatore_passwd.inc');

$tmp=_SALVATORE_TMP;

/* MAIN ***********************************************************************/

trad_log("\npousseur\n Prend les fichiers langue dans sa copie locale et les commite SVN\n\n");

$liste_sources=charger_fichier_traductions(); // chargement du fichier traductions.txt

foreach ($liste_sources as $source) {
	$module = $source[1];
	trad_log("\n===== Module $module ======================================\n");
	
	$f = _SALVATORE_TMP.$module.'/';
#	trad_log(exec("svn add --quiet $f*php 2>/dev/null")."\n");

	$ignore = array(
#		'spip','ecrire','public'
	);

	if (in_array($module , $ignore))
		trad_log("$module ignore'\n");
	else
		trad_log(exec("svn commit $f --username $SVNUSER --password $SVNPASSWD --no-auth-cache --non-interactive -m'langues ($module)'")."\n");
}

return 0;
/* MAIN ***********************************************************************/

?>