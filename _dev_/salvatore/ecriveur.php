<?php

/*
    This file is part of SPIP

    Trad-Lang is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Trad-Lang is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Trad-Lang; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Copyright 2003-2011
        Florent Jugla <florent.jugla@eledo.com>,
        Philippe Riviere <fil@rezo.net>
        Chryjs <chryjs!@!free!.!fr>,
*/

require_once('inc_tradlang.php');
$tmp= _SALVATORE_TMP;
#trad_log(exec("rm -rf ".$tmp."*"));

include_spip('base/abstract_sql');
include_spip('inc/filtres');


/* MAIN ***********************************************************************/

trad_log("\necriveur\n Exporte les fichiers de traduction dans sa copie locale a partir de la base de donnees\n\n\n");

$liste_sources=charger_fichier_traductions(); // chargement du fichier traductions.txt

if (!is_dir($tmp)) die ("Manque le repertoire ".$tmp);

foreach ($liste_sources as $source) {
	trad_log("==== Module ".$source[1]." =======================================\n");
	export_trad_module($source);
	trad_log(exec("svn status "._SALVATORE_TMP.$source[1]."/")."\n");
}

return 0;
/* MAIN ***********************************************************************/

//
// Genere les fichiers de traduction d'un module
//
function export_trad_module($source) {
	global $tmp;
	$res=spip_query("SELECT DISTINCT(lang) FROM trad_lang WHERE module='".$source[1]."'");
	$liste_lang=array();
	while ($row=spip_fetch_array($res)) {
		$liste_lang[]=$row['lang'];
	}

	// sanity check
	if (!is_dir($tmp.$source[1]."/"))
		return false;

	// charger la langue originale, pour la copier si necessaire
	$res=spip_query("SELECT id,str,comm,status FROM trad_lang WHERE module='".$source[1]."' and lang='".$source[2]."' GROUP BY id");
	while ($row=spip_mysql_fetch($res)) {
		$row['status'] = 'NEW';
		$lorigine[$row['id']] = $row;
	}

	// traiter chaque langue
	$traducteurs = array();
	foreach($liste_lang as $lang) {
		trad_log(" genere la langue $lang\n");

		$tab = in_array($source[1], array('spip', 'public', 'ecrire'))
			? '' : "\t";

		$res=spip_query("SELECT id,str,comm,status FROM trad_lang WHERE module='".$source[1]."' and lang='".$lang."' GROUP BY id");
		$x=array();
		$prev="";
		$traduits = 0;
		$total = sql_count($res);
		$tous = $lorigine; // on part de l'origine comme ca on a tout meme si c'est pas dans la base de donnees (import de salvatore/lecteur.php)
		while ($row=spip_mysql_fetch($res)) {
			$tous[$row['id']] = $row;
		}
		ksort($tous);
		foreach ($tous as $row) {
			if ($prev!=strtoupper($row['id'][0])) $x[] = "\n$tab// ".strtoupper($row['id'][0]);
			$prev=strtoupper($row['id'][0]);
			if ($row['status'] != 'NEW')
				$traduits ++;
			if (strlen($row['status']))
				$row['comm'] .= ' '.$row['status'];
			if (trim($row['comm'])) $row['comm']=" # ".trim($row['comm']); // on rajoute les commentaires ?

			$str = $row['str'];

			#// conversion utf8
			#if (in_array($source[1], array(
			#'crayons', 'cfg'
			#))) {
			#	$oldmd5 = md5($str);
			#	$str = unicode_to_utf_8(html_entity_decode($str, ENT_NOQUOTES, 'utf-8'));
			#	$newmd5 = md5($str);
			#	if ($oldmd5 !== $newmd5) spip_query("UPDATE trad_lang SET md5='$newmd5' WHERE md5='$oldmd5' AND module='$source[1]'");
			#}

			$x[]="$tab".var_export($row['id'],1).' => ' .var_export($str,1).','.$row['comm'] ;
		}
		$orig = ($lang == $source[2]) ? $source[0] : false;

		// ne pas ecrire le fichier si le taux de traduction est trop faible
		if ($traduits < $total/2 AND $total > 5) {
			trad_log("trop peu traduit ($traduits/$total), ignore\n");
		} else {
			// historiquement les fichiers de lang de spip_loader ne peuvent pas etre securises
			$secure = ($source[1] == 'tradloader')
				? ''
				: "if (!defined('_ECRIRE_INC_VERSION')) return;\n\n";

			$fd = fopen($tmp.$source[1]."/".$source[1].'_'.$lang.'.php', 'w');

			# supprimer la virgule du dernier item
			$x[count($x)-1] = preg_replace('/,([^,]*)$/', '\1', $x[count($x)-1]);

			$contenu = join("\n",$x);

			# ecrire le fichier
			fwrite($fd,
			'<'.'?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
'

. ($orig
	? '// Fichier source, a modifier dans '.$orig
	: '// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **
'
)
."\n".$secure.'$GLOBALS[$GLOBALS[\'idx_lang\']] = array(
'
	. $contenu
	.'
);

?'.'>
'
			);
			fclose($fd);

			// noter la langue et les traducteurs pour lang/module.xml
			$traducteurs[$lang] = array();
			$s = spip_query('SELECT DISTINCT(traducteur) FROM trad_lang WHERE module='._q($source[1])." and lang="._q($lang));
			while ($t = sql_fetch($s))
				$traducteurs[$lang][] = $t['traducteur'];
		}
	}


	// ecrire lang/module.xml
	$xml = "<traduction module=\"$source[1]\" gestionnaire=\"salvatore\" reference=\"$source[2]\">\n";
	unset($traducteurs[$source[2]]);
	foreach($traducteurs as $lang => $people) {
		$people = array_filter($people);
		if ($people) {
			$xml .= "	<langue code=\"$lang\">\n";
			foreach ($people as $nom) {
				$xml .= "		<traducteur nom=\"".entites_html($nom)."\" />\n";
			}
			$xml .= "	</langue>\n";
		} else
			$xml .= "	<langue code=\"$lang\" />\n";
	}
	$xml .= "</traduction>\n";

	ecrire_fichier($tmp.$source[1]."/".$source[1].'.xml', $xml);

} //export_trad_module

?>
