<?php


function selecteur_objets_input($type, $_id, $from, $options) {
	$uniqid = uniqid();

	$ret = "<input id='$uniqid' name='selecteur_$_id' type='text' />\n";
	$ret .= "<input name='type' type='hidden' value='$type' />\n";
	$ret .= <<<EOF
<script type='text/javascript'><!--
	var search_timeout = 0;
	jQuery('#$uniqid')
	.keyup(function(){
		if(search_timeout) clearTimeout(search_timeout);
		var me = this;
		search_timeout = setTimeout(function() {
			search_timeout = 0;
			jQuery(me)
			.next()
			.load('selecteur_generique.php', {
				'id_selecteur': jQuery(me).attr('name'),
				'type': jQuery(me).next().next().attr('value'),
				'val': me.value
			}); // TODO il faudrait ici annuler un eventuel xhr lance plus tot et qui n'aurait pas fini
		}, 300);
	})
	.after('<div></div>');
// --></script>
EOF;

	return $ret;
}

function selecteur_objets_menu($type, $_id, $from, $options) {
	include_spip('inc/texte');

	$ret = "<select name='$_id'>\n";

	$s = spip_query("SELECT * $from");
	while ($t = spip_fetch_array($s)) {
		$titre = isset($t['titre'])
			? typo($t['titre'])
			: typo($t['nom']);

		$ret .= "<option value='"._q($t[$_id])."'>".$titre."</option>\n";
	}

	$ret .= "</select>\n";

	return $ret;
}


function selecteur_objets ($type, $options = null) {
	$from = "FROM spip_".table_objet($type);
	$_id = id_table_objet($type);

	// Etablir le nombre des objets parmi lesquels il est possible de choisir
	$s = spip_query("SELECT COUNT(*) AS n $from");
	$t = spip_fetch_array($s);
	$total_objets = $t['n'];

	if ($total_objets == 0)
		return '';

	if ($total_objets < 50)
		return selecteur_objets_menu($type, $_id, $from, $options);
	else
		return selecteur_objets_input($type, $_id, $from, $options);
}

function nomlike($val) {
	return "nom LIKE "._q("$val%");
}
function titrelike($val) {
	return "titre LIKE "._q("$val%");
}

function selecteur_objets_livesearch ($type, $val) {
#	spip_query("SET NAMES 'utf8'");

	if (!in_array($type, array('article', 'mot', 'auteur')))
		die('erreur selecteur_objets_livesearch'); # securite

	$from = "FROM spip_".table_objet($type);
	$_id = id_table_objet($type);

/*	include_spip('inc/mots');
	$r = affiche_mots_ressemblant($val, 'article', 1, array(),null,null,null,null,null,null);

	return $r;
*/

	$val = array_filter(array_map('trim', explode(',', $val)));


	// chaque table a son champ de titre
	if ($type == 'auteur')
		$champ = 'nom';
	else
		$champ = 'titre';


	if ($val) {
		$where = "WHERE ".join(' OR ', array_map($champ.'like', $val));
		// Etablir les objets
		$s = spip_query("SELECT * $from $where LIMIT 20");
		while ($t = spip_fetch_array($s))
			$ret .= $t[$champ]."<br />\n";
	}

	return $ret;
}


if (!defined('_ECRIRE_INC_VERSION')) {
	chdir('..');
	include 'ecrire/inc_version.php';
}

if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
	echo selecteur_objets_livesearch(_request('type'), _request('val'));
}

?>