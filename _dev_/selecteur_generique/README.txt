
L'idée de ce dev est de se faire une base de code pour un sélecteur
"générique" (au moins de mots-clés, auteurs, rubriques...) :

* suffisamment riche pour égaler en fonctionnalité les sélecteurs
qui existent déjà dans SPIP ;

* suffisamment simple pour pouvoir évoluer (ceux de spip sont une voie
de garage tellement ils sont devenus complexes).

* l'ergonomie est prioritaire :
- "intuitif"
- "rapide"
- "clair"



INSTALLATION & test:
--------------------

récupérer le répertoire selecteur_generique, et le mettre à la racine de SPIP.
Puis pointer le navigateur vers http://site.example.tld/selecteur_generique/


Règles de commit :
------------------

go go !

