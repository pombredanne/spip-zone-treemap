<html>
<head>
<title>Statistiques versions SPIP et plugins </title>
<script src="jquery.js" type="text/javascript"></script>
<script src="treemap.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery(function(){
	jQuery("<div class='treemap'>").hide().insertAfter("table");
	jQuery("table").each(function(){
		var dataFunction;
		if(jQuery(this).is("#versions"))
			dataFunction = getDataVersions; 
		jQuery(this).treemap(640,480,{target:jQuery(this).next(),getData:dataFunction});
		enhanceTreemap(jQuery("div.treemap"));
	})
	
	jQuery("#clic").click(function(){
		jQuery("table, div.treemap").toggle();
		jQuery(this).text( jQuery(this).is(":contains(treemap)")?"tableaux <<":">> treemap" );
	});	
})

function getDataVersions(t) {
	var data = [];
	var maj_versions = {};
	jQuery("tr",t).each(function(){
		var cells = jQuery(">td",this);
		var version = cells.eq(0).html();		
		var maj = parseInt(version.replace(/[.-]/g,''));
		if(maj == "NaN") {
			maj_versions[maj] = [version,cells.eq(1).html()];		
		} else {
			if(maj<100) maj *=10;
			var maj2 = parseInt(maj/10);
			var maj3 = parseInt(maj%10);
			if(!maj_versions[maj2]) maj_versions[maj2] = {};
			if(!maj_versions[maj2][maj3]) maj_versions[maj2][maj3] = [];
			maj_versions[maj2][maj3].push([version,cells.eq(1).html()]);
		} 
	});
	
	jQuery.each(maj_versions,function(i,n){
		var row = [i.charAt(0)+'.'+i.charAt(1),n];
		if(n.constructor==Object) {
			var row2 = [];
			jQuery.each(n,function(i,n2){
				row2.push([row[0]+'.'+i,n2]);
			});
			row[1] = row2;
		}
		
		data.push(row);
	});
	return data;
}

function enhanceTreemap(t,data) {
	jQuery("div.treemapCell",t).hover(function(){jQuery(this).addClass("selected")},function(){jQuery(this).removeClass("selected")});
}
</script>
<style type="text/css">
  	.treemapCell {background-color:#FF6600}
  	.treemapHead {background-color:#B34700}
		.treemapCell.selected, .treemapCell.selected .treemapCell.selected {background-color:#FFCC80 !important}
  	.treemapCell.selected .treemapCell {background-color:#FF9900}
  	.treemapCell.selected .treemapHead {background-color:#B36B00}
  	.transfer {border:1px solid black}
</style>
</head>
<body>

<button id='clic' class='treemap' style='float:right;'>&gt;&gt; treemap</button>

<?php

	error_reporting(E_ALL^E_NOTICE);

	if (!$f = file_get_contents('resultats.txt'))
		die('je cherche les resultats du script dans resultats.txt');


	$sites = preg_split(',-- ,m', $f);

	echo "<h1>".count($sites)." sites (".date('Y-m-d').")</h1>\n";

	// Versions de SPIP
	$versions = array();
	$plugins = array();
	$pluginsv = array();
	$erreurs = array();
	$nbplugs = array();

	foreach ($sites as $site) {
		if (preg_match(',Composed-By: (.*)( @ www.spip.net)( ?\+ ?(.*))?$,m', $site, $r)) {
			$versions[str_replace('SPIP ','',$r[1])]++;

			if ($p = array_filter(explode(',', $r[4]))) {
				foreach ($p as $plugin) {
					$plugins[preg_replace(',[(].*,','', $plugin)]++;
					$pluginsv[$plugin]++; // versionne
				}
				$nbplugs[count($p).' plugins'] ++;
			}
		}

		if (preg_match(',Location: ,i', $site))
			$erreurs['location']++;
		if (preg_match(',404 not found,i', $site))
			$erreurs['erreur 404']++;
		if (preg_match(',injoignable,i', $site))
			$erreurs['injoignable']++;
		if (preg_match(',\n0\n0,', $site))
			$erreurs['aucune mention de SPIP']++;
		if (preg_match(',\n\d+\n0,', $site))
			$erreurs['aucune mention de spip.net']++;
	}



	function affiche_table($array,$id="") {
		echo "<table".($id?" id='$id'":"").">\n";
		foreach ($array as $k => $n) {
			echo "<tr><td>$k</td><td>$n</td></tr>\n";
		}
		echo "</table>\n";
	}


	echo "<h2>Versions:</h2>\n";
	arsort($versions);
	affiche_table($versions,"versions");

	$total = count($sites) - $erreurs['injoignable'] - $erreurs['erreur 404'];
	$inconnue = ($total - array_sum($versions));
	$pc = intval($inconnue / $total * 10000)/100;
	echo "<p>Version inconnue : $inconnue ($pc%).</p>\n";


	echo "<h2>Plugins:</h2>\n";
	arsort($plugins);
	affiche_table($plugins);

	echo "<h2>Plugins, versionn&#233;s:</h2>\n";
	arsort($pluginsv);
	affiche_table($pluginsv);


	echo "<h2>Nombre de plugins install&#233;s:</h2>\n";
	arsort($nbplugs);
	affiche_table($nbplugs);

	echo "<p>Nombre total de sites ayant active un ou plusieurs plugins: ".array_sum($nbplugs)."</p>\n";


	echo "<h2>Erreurs:</h2>\n";
	arsort($erreurs);
	affiche_table($erreurs);


?>
</body>
</html>
