<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2010                                                *
 *  SPIP Team								   *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;

// Surcharge pour afficher le resultat de la tache "versions" du genie
// dans un onglet supplementaire

include_spip('inc/presentation');

function exec_statistiques_referers()
{
	$jour = _request('jour');
	$limit  = _request('limit');
// nombre de referers a afficher
	$limit = intval($limit);	//secu
	if (!autoriser('voirstats','article')) {
		include_spip('inc/minipres');
		echo minipres();
	} else {

		if ($jour == 'debut') {
		  $res = versions_compte();
		} else {
			if ($limit == 0) $limit = 100;
			if ($jour<>'veille') $jour='jour';

			$referenceurs = charger_fonction('referenceurs', 'inc');
			$res = $referenceurs (0, "SUM(visites_$jour)", 'spip_referers', "visites_$jour>0", "referer", $limit);
			$n =  count($res);
			$args = "jour=$jour@limit=" . strval($limit+200);
			$plus = generer_url_ecrire('statistiques_visites', $args);

			if ($plus) {
				$plus = ($limit == $n)
				? "<div style='text-align:right;'><b><a href='$plus'>+++</a></b></div>"
				  : '';
			}

			$titre = _T('titre_liens_entrants')
			. " ($n " 
			. ($n <= 1 ?  _T('info_site') :  _T('info_sites'))
			. ")";

			if ($res) {
				$res = "<br /><div style='font-size:small;' class='verdana1'>"
				. "<ul class='referers'><li>"
				. join("</li><li>\n", $res)
				. "</li></ul>"
				. $plus;
			} else $res ='';

		}

		$commencer_page = charger_fonction('commencer_page', 'inc');

		echo $commencer_page(_T('titre_page_statistiques_referers'), "statistiques_visites", "referers");
		echo "<br /><br />";

		echo gros_titre(_T('titre_liens_entrants'),'', false);
		echo debut_gauche('', true);
		if ($jour !== 'debut') {
			echo debut_boite_info(true);

			echo "<p style='font-size:small; text-align:left;' class='verdana1'>"._T('info_gauche_statistiques_referers')."</p>";

			echo fin_boite_info(true);
		}

		echo debut_droite('', true);
		echo barre_onglets("stat_referers", $jour);
		echo $res;
		echo fin_gauche(), fin_page();
	}
}

function barre_onglets_stat_referers() {

	$onglets = array();
	$onglets['jour']=
		  new Bouton(null, 'date_aujourdhui',
			generer_url_ecrire("statistiques_referers",""));
	$onglets['veille']=
		  new Bouton(null, 'date_hier',
			generer_url_ecrire("statistiques_referers","jour=veille"));
	$onglets['debut']=
		  new Bouton(null, 'onglet_repartition_debut',
			generer_url_ecrire("statistiques_referers","jour=debut"));
	return $onglets;
}

function versions_compte()
{
	include_spip('genie/versions');
	error_reporting(E_ALL^E_NOTICE);

	// le fichier des resultats futur est inclus
	// juste pour savoir ou il en est ($debut)
	if (file_exists(_VERSIONS_FILE_TMP)) {
		include(_VERSIONS_FILE_TMP);
		$date_next = date("Y-m-d H:i:s",filemtime(_VERSIONS_FILE_TMP));
	} else {$date_next; $debut = 0;}

	$date = file_exists(_VERSIONS_FILE) ? filemtime(_VERSIONS_FILE): 'NOW()';

	$total =  sql_countsel('spip_signatures AS S, spip_articles AS A', 'A.id_trad=' . _VERSIONS_ID_ARTICLE . " AND A.id_article=S.id_article AND UNIX_TIMESTAMP(S.date_time) <= $date");

	$s = 'background-color: blue; width:' .ceil((450*$debut)/$total) . 'px';
	$res = "<div id='versions' style='background-color: white; width: 450px; height: 20px; margin:10px;' class='centered'>"
	  . "<div style='$s' title='$debut / $total le $date_next'/>&nbsp;</div></div>\n";

	if (!file_exists(_VERSIONS_FILE)) 

		return $res . _L('R&eacute;sultats absents');

	include(_VERSIONS_FILE);

	$date2 = file_exists(_VERSIONS_FILE_PRED) ? filemtime(_VERSIONS_FILE_PRED) : 0;
	$res .= '<h1>'
	  . _T('info_nombre_sites', array('nb_sites' => $total))
	  . ' '
	  . date('Y-m-d', $date2)
	  . ' - '
	  . date('Y-m-d', $date)
	  . '</h1>'  ;

	$arbre = versions_sous_versions($versions);
	ksort($arbre, SORT_STRING);
	$res .= affiche_arbre($arbre);

	$total -=   ($erreurs[0] + $erreurs[404]);
	$inconnue = ($total - array_sum($versions));
	$pc = intval($inconnue / $total * 10000)/100;
	$res .= "<p>Versions inconnues : $inconnue ($pc%).</p>\n";

	$res .= "<h2>Erreurs:</h2>\n";
	arsort($erreurs);
	$res .= affiche_table($erreurs);

	$res .= "<p>Nombre total de sites ayant activ&eacute; un ou plusieurs plugins: ".array_sum($nbplugs)."</p>\n";

	$res .= "<h2>Plugins:</h2>\n";
	arsort($plugins);
	$res .= affiche_table($plugins);

	$res .= "<h2>Plugins, versionn&#233;s:</h2>\n";
	arsort($pluginsv);
	$res .= affiche_table($pluginsv);

	$res .= "<h2>Nombre de plugins install&#233;s:</h2>\n";
	arsort($nbplugs);
	$res .= affiche_table($nbplugs);

	return $res;
}

function versions_sous_versions($versions) {
	$top = array();
	foreach ($versions as $k => $v) {
	  $nom = preg_replace('/\W+/', '.', $k);
	  $nums = preg_split('/\./', $nom);
	  $index = '';
	  foreach ($nums as $i) {
	    if ($i !=='') {
	      $index .= $i . '.';
	      @$top[$index] += $v;
	    }
	  }
	}
	return $top;
}

function affiche_table($array,$id="") {
	$res = '';
	foreach ($array as $k => $n) {
		$res .= "<tr><td>$k</td><td>$n</td></tr>\n";
	}
	return "<table".($id?" id='$id'":"").">\n" . $res . "</table>\n";
}

function affiche_arbre($array) {
	$res = '';
	$c = true;
	foreach ($array as $k => $val) {
	  $c = !$c;
	  $res .= '<tr' . ($c ? ' style="background-color:white"' :'') . '>';
	  $n= count(preg_split('/[. ]/', $k));
	  for($i=2; $i<$n; $i++)
	    $res .= "<td>&nbsp;&nbsp;&nbsp;</td>";
	  $nom = $k;
	  $version = preg_replace('/\D+/', '', $k);
	  if (function_exists('generer_url_spip')) {
	    $version = generer_url_spip($version,'','');
	    if ($version) {
	      $date = ' ' . affdate(sql_getfetsel('date', 'spip_articles', 'id_article=' . intval($version[1])));
	      $url = generer_url_entite($version[1], $version[0]);
	      $nom = "<a href='$url'>$k</a> $date";
	    }
	  }
	  $res .= "<td  style='text-align:left;' colspan='" . (8-$i) . "'>$nom</td>";
#	  for(; $i<7; $i++) $res .= "<td>&nbsp;</td>";
	  $n = (20-$n-$n) . 'pt' ;
	  $res .= "<td style='text-align:right; font-size:$n;'>$val</td></tr>\n";
	}
	return "<table border='1'><tr><th align='center' colspan='6'>"
	  . 'Version'
	  . "</th><th>Nombre</th></tr>\n"
	  . $res
	  . "</table>\n";
}

?>
