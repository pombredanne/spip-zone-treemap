<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2010                                                *
 *  SPIP Team								   *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;

// Repertoire des resultats 
define('_VERSIONS_DIR', _DIR_TMP);
// Fichier des resultats 
define('_VERSIONS_FILE', _VERSIONS_DIR. 'versions_nb.php');
// Fichier des resultats precedents
define('_VERSIONS_FILE_PRED', _VERSIONS_DIR. 'versions_nb_pred.php');
// Fichier des resultats en cours
define('_VERSIONS_FILE_TMP', _VERSIONS_DIR. 'versions_nb_tmp.php');
// Nombre de site traites a chaque declenchement du genie
define('_VERSIONS_LIMIT', 5);

// Numero de l'article de reference des traductions avec signatures associees
define('_VERSIONS_ID_ARTICLE', 884);

// Script pour compter les differentes versions de SPIP utilisees par les 
// sites ayant signale leur existence

function genie_versions()
{
#	error_reporting(E_ALL^E_NOTICE);

	if (file_exists(_VERSIONS_FILE_TMP)) {
		include(_VERSIONS_FILE_TMP);
	} else {
		$versions = array();
		$plugins = array();
		$pluginsv = array();
		$erreurs = array();
		$nbplugs = array();
		$debut = 0;
	}
	$request = array_map('array_shift', sql_allfetsel('S.url_site AS url', 'spip_signatures AS S LEFT JOIN spip_articles AS A ON A.id_article=S.id_article', "S.statut<>'poubelle' AND A.id_trad=" . _VERSIONS_ID_ARTICLE, '', "S.date_time", intval($debut) . ',' . _VERSIONS_LIMIT));

	$debut +=  _VERSIONS_LIMIT;

	if (!$request) {
		@spip_unlink(_VERSIONS_FILE_PRED);
		@rename(_VERSIONS_FILE, _VERSIONS_FILE_PRED);
		$file = _VERSIONS_FILE;
	} else {
	  $file = _VERSIONS_FILE_TMP;
	  include_spip('inc/distant');
	  foreach ($request as $u) {
	  
		spip_log("genie version : $u ");
		$site = recuperer_lapage($u);
		if (!$site) {
			$erreurs['injoignable']++;
			$header = $page = '';
		} else {
		  list($header, $page) = $site;
		  if (preg_match(',Composed-By: (.*)( @ www.spip.net)( ?\+ ?(.*))?$,m', $header, $r)) {
			$versions[str_replace('SPIP ','',$r[1])]++;

			if ($p = array_filter(explode(',', $r[4]))) {
				foreach ($p as $plugin) {
					$plugins[preg_replace(',[(].*,','', $plugin)]++;
					$pluginsv[$plugin]++; // versionne
				}
				$nbplugs[count($p).' plugins'] ++;
			}
		  }
		if (preg_match(',404 ,', $header))
			$erreurs[404]++;

		elseif (!preg_match(',spip,i', $page))
			$erreurs['aucune mention de SPIP']++;
		elseif (!preg_match(',spip.net,i', $page))
			$erreurs['aucune mention de spip.net']++;
		}
		spip_log("genie version : $u header: " . strlen($header) . ', content: ' . strlen($page));
	  }
	}
	@spip_unlink(_VERSIONS_FILE_TMP);
	ecrire_fichier($file, '<' . '?php' . 
		"\n" . '$erreurs = ' . var_export($erreurs, true) .
		";\n" . '$versions = ' . var_export($versions, true) .
		";\n" . '$plugins = ' . var_export($plugins, true) .
		";\n" . '$pluginsv = ' . var_export($pluginsv, true) .
		";\n" . '$nbplugs = ' . var_export($nbplugs, true) .
		";\n" . '$debut = ' . var_export($debut, true) .
		"\n?" . '>');
	return 1;
}

?>