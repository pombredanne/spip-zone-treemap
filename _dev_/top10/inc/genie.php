<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2010                                                *
 *  SPIP Team								   *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;

// Ajout d'une tache pour le genie

include(_DIR_RESTREINT . 'inc/genie.php');

function inc_genie($taches = array()) {

	if (!$taches) $taches['versions'] = 360;
	return inc_genie_dist(taches_generales($taches));
}