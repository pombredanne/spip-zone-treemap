/*
*	treemap plugin for jQuery (version 1.0.1 27/2/2007)
*	Copyright (c) 2007 Renato Formato <renatoformato@virgilio.it>
*	Dual licensed under the MIT (MIT-LICENSE.txt)
* and GPL (GPL-LICENSE.txt) licenses.
*/
(function() {
jQuery.fn.treemap = function(w,h,options) {
	options = jQuery.extend({labelCell:0,dataCell:1,headHeight:20,borderWidth:1,sort:true,nested:false},options);
	var or_target = options.target;
	return this.pushStack(jQuery.map(this,function(el){
		var data;
		if(!options.getData) {
			if(!jQuery.nodeName(el,"table")) return; 
			data = treemap.getDataFromTable(el,options);
		} else {
			data = options.getData(el);
		}
		//copy data because during the processing elements are deleted
		data = data.concat();
		
		if(jQuery.fn.treemap.caller!=treemap.layoutRow)
			treemap.normalizeValues(data);
		
		if (options.sort)
			data.sort(function(a,b){
				var val1 = b[1], val2 = a[1];
				val1 = val1.constructor==Array?treemap.getValue(val1):val1;
				val2 = val2.constructor==Array?treemap.getValue(val2):val2;
				return val1-val2;
			});
		
		options.target = or_target || el;
		options.numSquare = 0;
		   
		treemap.render(data,h,w,options);
		
		if(options.target==el && jQuery.nodeName(el,"table")) {
			var newObj = jQuery(el).find(">").insertBefore(el);
			jQuery(el).remove();
			el = newObj.get();
		}
		return el; 
	}));
}

var treemap = {
	 normalizeValues : function(data) {
		if(treemap.normalizeValues.caller==treemap.layoutRow) return;
		for(var i=0,dl=data.length;i<dl;i++)
			if(data[i][1].constructor==Array) 
				treemap.normalizeValues(data[i][1]);
			else 
				data[i][1] = parseFloat(data[i][1]); 	
	},
	
	getDataFromTable : function(table,options) {
		var data = [];
		jQuery("tbody tr",table).each(function(){
			var cells = jQuery("td",this);
			if(options.labelCell==undefined) options.labelCell = options.dataCell;
			var row = [cells.eq(options.labelCell).html(),
								 cells.eq(options.dataCell).html()];
			data.push(row);
		});
		return data;
	}, 
	
	emptyView: jQuery("<div>").addClass("treemapView"),
	
	render : function(data,h,w,options) {
		options.height = h;
		options.width = w;
		var s = treemap.calculateArea(data);
		options.viewAreaCoeff = w*h/s;
		options.view = treemap.emptyView.clone().css({'width':w,'height':h});
		treemap.squarify(data,[],h,true,options);
		jQuery(options.target).empty().append(options.view);
	},
	
	squarify : function(data,row,w,orientation,options) {
		if(w<=0) return; //exit if there's no space left on the treemap
		var widerRow = row,s,s2,current;
		do {
			row = widerRow; 
			s = treemap.calculateArea(row);
			if(data.length==0) return treemap.layoutRow(row,w,orientation,s,options,true);
			current = data.shift();
			widerRow = row.concat();
			widerRow.push(current);
			s2 = s+(current[1].constructor==Array?treemap.getValue(current[1]):current[1]);
		} while (treemap.worst(row,w,s,options.viewAreaCoeff)>=treemap.worst(widerRow,w,s2,options.viewAreaCoeff))		

		var rowDim = treemap.layoutRow(row,w,orientation,s,options);
		data.unshift(current);

		if(!rowDim) rowDim = treemap.layoutRow([['',s]],w,orientation,s,options,true);
		var width;
		if(orientation) {
			options.width -= rowDim;
			width = options.width;
		} else {
			options.height -= rowDim;
			width = options.height;
		}
		treemap.squarify(data,[],width,!orientation,options);
	},
	
	worst : function(row,w,s,coeff) {
		var rl = row.length;
		if(!rl) return Number.POSITIVE_INFINITY;
		var w2 = w*w, s2 = s*s*coeff;
		var r1 = (w2*(row[0][1].constructor==Array?treemap.getValue(row[0][1]):row[0][1]))/s2;
		var r2 = s2/(w2*(row[rl-1][1].constructor==Array?treemap.getValue(row[rl-1][1]):row[rl-1][1]));
		return Math.max( r1, r2 );
	},
	
	emptyCell: jQuery("<div>").addClass("treemapCell").css({'float':'left','overflow':'hidden'}),
	emptySquare: jQuery("<div>").addClass("treemapSquare").css('float','left'),
	
	layoutRow : function(row,w,orientation,s,options,last) {
		var square = treemap.emptySquare.clone();
		var rowDim, h = s/w;
		if(orientation) {
			rowDim = last?options.width:Math.min(Math.round(h*options.viewAreaCoeff),options.width);
			square.css({'width':rowDim,'height':w}).addClass("treemapV");
		} else {
			rowDim = last?options.height:Math.min(Math.round(h*options.viewAreaCoeff),options.height);
			square.css({'height':rowDim,'width':w}).addClass("treemapH");
		}
		var rl = row.length-1,sum = 0, bw = options.borderWidth, bw2 = bw*2, cells = []; 
		for(var i=0;i<=rl;i++) {
			var n = row[i],hier = n[1].constructor == Array, head = [], val = hier?treemap.getValue(n[1]):n[1];
			var cell = treemap.emptyCell.clone();
			if(!hier) cell.html(n[0]).attr('title',n[0]+' ('+val+')'); 
			var lastCell = i==rl;
			var fixedDim = rowDim, varDim = lastCell ? w-sum : Math.round(val/h);
			if(varDim<=0) return 0;
			sum += varDim;
			var cellStyles = {};
			if(bw && rowDim>=bw2 && varDim>=bw2) {
				if(jQuery.boxModel) {
					fixedDim -= bw*(2-(options.numSquare>=2 || !options.numSquare && options.nested?1:0)-(last && options.nested?1:0));
					varDim -= bw*(2-(!lastCell||options.nested?1:0)-(options.numSquare>=1 && !i?1:0));
				}
				cellStyles.border = bw+'px solid';
				if(!lastCell || options.nested) 
					cellStyles['border'+(orientation?'Bottom':'Right')] = 'none';
				if(options.numSquare>=2 || !options.numSquare && options.nested) 
					cellStyles['border'+(orientation?'Left':'Top')] = 'none';
				if(options.numSquare>=1 && !i) 
					cellStyles['border'+(orientation?'Top':'Left')] = 'none';
				if(last && options.nested)
					cellStyles['border'+(orientation?'Right':'Bottom')] = 'none';
			} 
			var height = orientation?varDim:fixedDim, width = orientation?fixedDim:varDim;
			
			cellStyles.height = height;
			cellStyles.width = width;
			if(hier) {
				if(options.headHeight) {
					head = jQuery("<div class='treemapHead'>").css({"width":width,"height":options.headHeight,"overflow":"hidden"}).html(n[0]).attr('title',n[0]+' ('+val+')');
					if(orientation) 
						height = varDim -= options.headHeight;
					else
						height = fixedDim -= options.headHeight;
					 
				}
				if(height>0) {
					var new_opt = {};
					for(var prop in options) new_opt[prop] = options[prop]; 
					new_opt = jQuery.extend(new_opt,{getData:function(){return n[1].concat()},target:undefined,nested:true});
					cell.treemap(width,height,new_opt);
				}
				cell.prepend(head);
			}
			
			cell.css(cellStyles);
			cells.push(cell[0]);
		}
		options.view.append(square.append(cells));
		options.numSquare++;
		return rowDim;
	},
	
	calculateArea : function(row) {
		if(row.total) return row.total;
		var s = 0,rl = row.length;
		for(var i=0;i<rl;i++) {  
			var val = row[i][1];
			s += val.constructor==Array?treemap.getValue(val):val;
		}
		
		return row.total = s;
	},
	
	getValue : function(val) {
			if(!val.total) val.total=treemap.calculateArea(val);
			return val.total;
	}
	
}
})()
