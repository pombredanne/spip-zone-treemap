<?php

class Conf {
  private static $instance;

  private $www_root = "/var/www/";

  private $tmp_root = "/var/tmp/paquets";

  private $copielocale_root = "/var/tmp/working_copy/";

  final public function getWww_root()
  {
    return $this->www_root;
  }

  final public function getTmp_root()
  {
    return $this->tmp_root;
  }

  final public function getCopielocale_root()
  {
    return $this->copielocale_root;
  }
  final public function setWww_root($val)
  {
    $this->www_root=$val;
  }

  final public function setTmp_root($val)
  {
    $this->tmp_root=$val;
  }

  final public function setCopielocale_root($val)
  {
    $this->copielocale_root=$val;
  }
  public static function getInstance()
  {
  	if (!isset(self::$instance)) {
		$c = __CLASS__;
		self::$instance = new $c;
	}

	return self::$instance;
  }

  private function __construct()
  {
  	//rien
  }

}
?>
