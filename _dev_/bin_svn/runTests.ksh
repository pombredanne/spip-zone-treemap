#!/bin/bash

WORKDIR="/home/test/public_html/svn"
PID=$$ 
LOGDIR="${WORKDIR}/log_tests"
DATESQL=`date +%Y%m%d`
HEURE=`date +%H:%M:%S`


mkdir $LOGDIR

echo ""
echo "svn update"
#on commence par se mettre dans la derniere version
#de spip
echo "mise a jour de spip"
echo "--------------------"
cd $WORKDIR 
svn update > $LOGDIR/svn_update_spip.log
echo `grep revision  $LOGDIR/svn_update_spip.log` 

#des tests
echo "mise a jour des test"
echo "--------------------"
cd $WORKDIR/tests
svn update > $LOGDIR/svn_update_test.log
echo `grep revision  $LOGDIR/svn_update_test.log` 
 

echo ""
rm $LOGDIR/RESULTATTEST


lynx -dump http://localhost/test/svn/tests/ | grep "/tests/"  | sed -e "s/^[\ ]*[0-9]*\./ /g"| sort>$LOGDIR/listeTests
for i in `cat $LOGDIR/listeTests `
do 
	#le nom du fichier de log ... on underscore tout ce qu'on peut
	LOGFILE=`echo $i|sed -e "s/[\-|\:|\.|\=|\/|\?|\&]/_/g"`
 
	resultat=`lynx -dump $i| egrep "KO|OK"`
	echo "$DATESQL;$HEURE;$resultat" >>$LOGDIR/$LOGFILE
	echo "$DATESQL;$HEURE;$resultat;$LOGFILE" >>$LOGDIR/RESULTATTEST
done
NBTEST=`cat $LOGDIR/listeTests |wc -l `
echo "Nb de tests a effectuer : $NBTEST"
NBTESTOK=`grep OK $LOGDIR/RESULTATTEST   | wc -l `
NBTESTKO=`grep KO $LOGDIR/RESULTATTEST  | wc -l `
echo "Nb de tests OK          : $NBTESTOK"
echo "Nb de tests KO          : $NBTESTKO"

