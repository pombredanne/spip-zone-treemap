#!/bin/bash

# Script permettant de calculer les archives des fichiers svn de spip-zone
# pour les elements qui sont references dans le fichier "archivelist.txt"

# Ancienne version
memo_version='versiontestee.txt'
OLD_VERSION=`head -1 $memo_version`

# PQ_DEST défaut /var/www/shim/fil/Web/files/ où on va poser les .tgz et les .zip
PQ_DEST='archives-spip-zone'

# On récupère la dernière version de la liste des archives
svn co svn://trac.rezo.net/spip-zone --non-recursive
archivelisttxt='spip-zone/archivelist.txt'

# On sauvegarde le numero de version en cours pour plus tard
svn info -r HEAD svn://trac.rezo.net/spip-zone | grep vision | sed 's/[^0-9]//g' > /tmp/version_courante

# On recupere les fichiers modifies depuis le dernier zip
svn diff -r "$OLD_VERSION":HEAD 'svn://trac.rezo.net/spip-zone' > /tmp/diff.txt
fgrep '+++' /tmp/diff.txt | grep 'vision' > /tmp/diff2.txt
cat /tmp/diff2.txt | awk '{print $2}' > /tmp/diff.txt

sed 's/;/ /g' $archivelisttxt > /tmp/archivelisttxt

cat '/tmp/archivelisttxt' | while read rep arc nam rul; do
       if [ $rep ] && [ ${rep:0:1} != '#' ]
               then
               if [ `fgrep $rep '/tmp/diff.txt' | wc -l` != 0 ]
                       then
                       echo "Archivage de $rep"
                       # Calcul du zip
			rev=${rep##+([^:])}
                        if [ -n "$rev" ] ; then
                                rep=${rep%[:]*}
                                rev=" --revision ${rev:1}"
                        fi
                	arc=${arc:=${rep##*/}}
			nam=${nam:=$arc}
			mkdir -p $nam
			svn export -q --force svn://trac.rezo.net/spip-zone/$rep $nam
			zip -mr -q $PQ_DEST/$arc.zip $nam
               fi
       fi
done

# La version en cours devient l'ancienne version
mv /tmp/version_courante $memo_version
