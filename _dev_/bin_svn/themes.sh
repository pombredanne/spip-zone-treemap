#!/bin/bash
#
# Script de construction d'un repertoire regroupant tous les dossiers
# de la zone qui contiennent un fichier theme.xml.
BASEDIR=/home/miroirspip/miroirspip/spip-zone
BACKUP=/home/miroirspip/www/builds/themes
ZIPNAME=theme.zip

cd $BASEDIR
for i in `find -name 'theme.xml'`; do
mkdir -p $BACKUP/
zip -r -q /home/miroirspip/www/builds/themes/tmp `dirname $i`
done 
cd $BACKUP
zip -q -d tmp */.svn */.svn/*
mv -f tmp.zip $ZIPNAME
exit
