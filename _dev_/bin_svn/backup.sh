#! /bin/bash

## bidouilles installation
tracadmin9=/home/svn/tracEnLocal/usr/bin/trac-admin
export PYTHONPATH=/home/svn/tracEnLocal/site-packages


# nettoyer les repertoires destination
rm -rf backup/svn/* backup/trac/*
mkdir -p backup/svn
mkdir -p backup/trac

# copier les fichiers svn + trac
for i in `ls -1 projects/`; do \
	svnadmin hotcopy projects/$i backup/svn/$i
	$tracadmin9 trac/$i hotcopy backup/trac/$i
done


# envoyer sur infora.cursys.net
rsync -a --delete backup/ infora.cursys.net::trac-rezo-net/

# nettoyer de nouveau 
rm -rf backup/svn/* backup/trac/*

# faire les paquets tgz dans http://zone.spip.org/files/spip-zone/ etc
# c'est toggg maintenant qui fait ca
#bin/paquets.sh spip spip-zone

