<?php
$GLOBALS['sqla_version'] = '0.3.5';

$GLOBALS['sqla_data'] = array();

/* fournit une balise pour recuperer la version de SQLAdmin en cours */
function balise_sqladmin_version($p){
	global $sqla_version;
	$p->code="'".$sqla_version."'";
	return $p;	
}


/* fournit la liste des fonctions sql possibles */
function sqla_liste_SQL_fonctions(){
	return array(
		'sql_query' ,
		'sql_select'/*,
		'sql_insert',
		'sql_insertq',
		'sql_replace',
		'sql_update',
		'sql_updateq'	*/
	);
}

/* recupere la liste des noms de connexions possibles */
function sqla_liste_connexions(){
	include_spip('inc/flock');
	$files = preg_files(_DIR_CONNECT, '.*\.php');
	$liste = array();
	foreach ($files as $f){
		$f = str_replace(_DIR_CONNECT,'',$f);
		$f = str_replace('.php','',$f);
		$liste[] = $f;
	}
	return $liste;
}


/* ajoute lles nouveaux parametres postes dans l'historique existant */
function sqla_ajouter_parametres_dans_historique(){
	$historique = unserialize(_request('sqla_historiques'));
	if (!$historique) $historique=array();

	$histo = array();

	$histo['connect'] 	= _request('connect');
	$histo['fonction']	= _request('sqla_sql_fonction');
	$histo['params']	= sqla_get_champs();
	
	if ($histo['fonction']) array_unshift($historique, $histo);
	if (count($historique)>5) array_pop($historique);
	
	global $sqla_data;
	$sqla_data['historique'] = $historique;
}

/* ajoute la requete generee dans l'historique */
function sqla_ajouter_requete_dans_historique(){
	global $sqla_data;
	if (_request('sqla_sql_fonction')){	
		$sqla_data['historique'][0]['requete'] = sqla_fin_ligne($sqla_data['requete']['query']); 
	}
}


/* recupere la liste des dernieres requetes executees */
function sqla_liste_historique(){
	global $sqla_data;
	$historique = $sqla_data['historique'];

	return $historique ? $historique : array();
}

/* presente un texte comprehensible pour la liste de l'historique */
function sqla_presenter_historique($h){
	$histo = array();
	foreach ($h as $c=>$v){
		$texte = '('.$v['fonction'].') ' . substr($v['requete'],0,60);
		$histo[$c] = $texte;
	}

	return $histo;
}

/* transforme un tableau en une suite de <option> */
function array2option($tableau, $valeur_selectionnee='', $cle=false){
	$s = "";
	foreach ($tableau as $c=>$t){
		$sel = ($t == $valeur_selectionnee) ? " selected='selected'":'';
		$v = $cle ? $c : $t;
		$s .= "<option value='$v'$sel>$t</option>\n";
	}
	return $s;
}

/* execute la requete demandee */
function sqla_executer_requete(){
	global $sqla_data;
	
	$res='';
	$serveur 	  	= _request('connect');
	$sql_fonction 	= _request('sqla_sql_fonction');
	$params 		= sqla_get_champs();
	
	$params['select'] 	= sqla_exploser_texte($params['select']);
	$params['from'] 	= sqla_exploser_texte($params['from']);
	$params['where'] 	= sqla_exploser_texte($params['where']);
	$params['groupby'] 	= sqla_exploser_texte($params['groupby']);
	$params['orderby'] 	= sqla_exploser_texte($params['orderby']);
	//$params['limit'] 	= sqla_exploser_texte($params['limit']);
	$params['having'] 	= sqla_exploser_texte($params['having']);
	
	if (!$sql_fonction) return;
	
	$sqla_data['requete'] = array();
	$sqla_data['requete']['connect'] = $serveur;
	
	switch ($sql_fonction){
		case 'sql_query':
			$sqla_data['requete']['query'] = $sql_fonction($params['query'], $serveur, false);
			if ($sqla_data['requete']['ressource'] = $sql_fonction($params['query'], $serveur)){
				$sqla_data['requete']['total'] = sql_count($sqla_data['requete']['ressource'], $serveur);
				$sqla_data['requete']['resultats'] = array();
				while ($r = sql_fetch($sqla_data['requete']['ressource'], $serveur)){
					$sqla_data['requete']['resultats'][]=$r;
				}			
			} else {
				$sqla_data['requete']['erreurs'] = sql_error("SqlAdmin: ".$sqla_data['requete']['query'],$serveur);
			}
			break;
			
		case 'sql_select':
			$sqla_data['requete']['query'] = $sql_fonction(
				$params['select'], $params['from'], $params['where'], 
				$params['groupby'], $params['orderby'], $params['limit'],
				$params['having'], $serveur, false);
			
			if ($sqla_data['requete']['ressource'] = $sql_fonction(
					$params['select'], $params['from'], $params['where'], 
					$params['groupby'], $params['orderby'], $params['limit'],
					$params['having'], $serveur)){
				$sqla_data['requete']['total'] = sql_count($sqla_data['requete']['ressource'], $serveur);
				$sqla_data['requete']['resultats'] = array();
				while ($r = sql_fetch($sqla_data['requete']['ressource'], $serveur)){
					$sqla_data['requete']['resultats'][]=$r;
				}			
			} else {
				$sqla_data['requete']['erreurs'] = sql_error("SqlAdmin: ".$sqla_data['requete']['query'],$serveur);
			}
			break;
	}

	return;
}

/* recupere les champs du formulaire dans un tableau */
function sqla_get_champs(){
	$champs = array();
	$champs['query'] 	= _request('sqla_champ_query');
	$champs['select'] 	= _request('sqla_champ_select');
	$champs['from'] 	= _request('sqla_champ_from');
	$champs['where'] 	= _request('sqla_champ_where');
	$champs['groupby'] 	= _request('sqla_champ_groupby');
	$champs['orderby'] 	= _request('sqla_champ_orderby');
	$champs['limit']	= _request('sqla_champ_limit');
	$champs['having'] 	= _request('sqla_champ_having');
	
	return $champs;
}

/* explose le contenu du texte pour en faire un array de valeurs */
function sqla_exploser_texte($texte){
	return $texte ? explode("\r\n",$texte) : $texte;	
}

/* affiche le contenu demande du resultat de requete */
function sqla_afficher($quoi){
	global $sqla_data;
	
	switch ($quoi){
		case 'erreurs':
			return $sqla_data['requete']['erreurs'];
			break;
			
		case 'infos':
			return "Version du serveur SQL en cours : " . sql_version($sqla_data['requete']['connect']);
			break;
			
		case 'resultats':
			$retour = "<table>";
			$entete = 0;
			if ($sqla_data['requete']['resultats']){
				foreach ($sqla_data['requete']['resultats'] as $r){
					if (!$entete){
						$retour .= "<tr>";
						foreach ($r as $cle=>$val)	{
							$retour .=  "<th>$cle</th>\n";
						}
						$retour .=  "</tr>\n";
						$entete = 1;		
					}
					$retour .=  "<tr>";
					foreach ($r as $val)	{
						$retour .=  "<td>$val</td>\n";
					}
					$retour .=  "</tr>\n";
				}
			}
			$retour .=  "</table>\n";
			return $retour;
			break;
		
		case 'requete':
			return $sqla_data['requete']['query'];
			break;

		case 'total':
			return "sql_count : ". $sqla_data['requete']['total']
					. '<br />count($rows) : ' . count($sqla_data['requete']['resultats']);
			break;	
			
	}
}


/* harmonise les fins de lignes */
function sqla_fin_ligne($txt){
	$hop = array(
		"\r\n" => "\n",
		"\r" => "\n",
		"\n" => "\r\n"	
	);
	return str_replace(array_keys($hop),$hop, $txt);	
}
?>
