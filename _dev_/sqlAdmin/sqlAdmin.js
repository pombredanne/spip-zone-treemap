jQuery(document).ready(function() {
		
	/*
	* Onglets resultats
	*/
	$('#sqla_retours div:not(:first)').hide();
	$('#sqla_resultats .sqla_onglets a:first').addClass('onglet_on');
	$("#sqla_resultats .sqla_onglets a").click(function(){
		$('#sqla_resultats .sqla_onglets a').removeClass('onglet_on');
		$(this).addClass('onglet_on');
		
		id_tab = '#'+$(this).attr('title');
		$('#sqla_retours div').hide();
		$(id_tab).show();
	});
	
	/* 
	 * Onglets requete
	 */
	/* voir aussi masquer/cacher les champs */
	$('#sqla_champs .sqla_onglets a').click(function(){
		$('#sqla_champs .sqla_onglets a').removeClass('onglet_on');
		$(this).addClass('onglet_on');
		
		$('#sqla_champs textarea').hide();
		id_tab = '#'+$(this).attr('title');
		$(id_tab).show();
	});

	
	/*
	 * Masquer/cacher les champs en fonction de la fonction
	 * sql_ choisie
	 */
	function afficher_params(fonction){
		$('#sqla_champs textarea').hide();
		$('#sqla_champs .sqla_onglets a').removeClass('onglet_on').hide();		

		switch (fonction){
			case 'sql_query':
				$('#sqla_champ_query').show();
				$('#sqla_label_query').show().addClass('onglet_on');
				break;
			case 'sql_select':
				$('#sqla_champ_select').show();
				$('#sqla_label_select').show().addClass('onglet_on');
				$('#sqla_label_from').show();
				$('#sqla_label_where').show();
				$('#sqla_label_orderby').show();
				$('#sqla_label_groupby').show();
				$('#sqla_label_limit').show();
				$('#sqla_label_having').show();
				break;	
		}
	}

	afficher_params($('#sqla_sql_fonction option:selected').val());
	
	$('#sqla_sql_fonction').change(function(){
		$(this).find("option:selected").each(function(){
			afficher_params($(this).val());
		});
	});
	
	
	
	/*
	 * Historique
	 */
	$('#sqla_historique').change(function(){
		$(this).find("option:selected").each(function(){
			n = $(this).val();
			if (n!=''){
				/* recuperer l'historique */
				histo = unserialize(
					$('#sqla_historiques').html()
					.replace(/\r\n/g,"\n")
					.replace(/\r/g,"\n")
					.replace(/\n/g,"\r\n")
				);
				params = histo[n]['params'];
				$('#sqla_champ_query').val(params['query']);
				$('#sqla_champ_select').val(params['select']);
				$('#sqla_champ_from').val(params['from']);
				$('#sqla_champ_where').val(params['where']);
				$('#sqla_champ_orderby').val(params['orderby']);
				$('#sqla_champ_groupby').val(params['groupby']);
				$('#sqla_champ_limit').val(params['limit']);
				$('#sqla_champ_having').val(params['having']);
				
					 
				/* selectionner la bonne fonction */
				$('#sqla_sql_fonction option:selected').removeAttr('selected')
					.parent()
					.find('[@value='+histo[n]['fonction']+']')
					.attr('selected','selected');
				afficher_params(histo[n]['fonction']);
			}
		});
	});



	/* 
	 * masquer sur demande spip_debug 
	 */
	$('#sqla_bouton_erreur_squelettes').toggle(
		function(){$('#spip-debug').hide(); $(this).addClass('sqla_bouton_actif');},
		function(){$('#spip-debug').show(); $(this).removeClass('sqla_bouton_actif');}
	);

});



/*
--- fonction serialize et unserialize ---
auteur : XoraX
email : xxorax@gmail.com
info : http://www.xorax.info/blog/programmation/40-javascript-serialize-php.html
version : 1.2 - 2007/04/23

ChangeLog:
----------
1.2 : ajout du support pour la sérialization d'Object php (case "O") + maj de la page de test
1.1 : fix bug dans unserialize sur boolean 

Description:
------------
permet de décoder la chaine revoyé par la fonction serialize php.
ne prend pas (encore?) en compte les objects.
*/

function serialize (txt) {
	switch(typeof(txt)){
	case 'string':
		return 's:'+txt.length+':"'+txt+'";';
	case 'number':
		if(txt>=0 && String(txt).indexOf('.') == -1 && txt < 65536) return 'i:'+txt+';';
		return 'd:'+txt+';';
	case 'boolean':
		return 'b:'+( (txt)?'1':'0' )+';';
	case 'object':
		var i=0,k,ret='';
		for(k in txt){
			//alert(isNaN(k));
			if(!isNaN(k)) k = Number(k);
			ret += serialize(k)+serialize(txt[k]);
			i++;
		}
		return 'a:'+i+':{'+ret+'}';
	default:
		return 'N;';
		alert('var undefined: '+typeof(txt));return undefined;
	}
}

function unserialize(txt){
	var level=0,arrlen=new Array(),del=0,final=new Array(),key=new Array(),save=txt;
	while(1){
		switch(txt.substr(0,1)){
		case 'N':
			del = 2;
			ret = null;
		break;
		case 'b':
			del = txt.indexOf(';')+1;
			ret = (txt.substring(2,del-1) == '1')?true:false;
		break;
		case 'i':
			del = txt.indexOf(';')+1;
			ret = Number(txt.substring(2,del-1));
		break;
		case 'd':
			del = txt.indexOf(';')+1;
			ret = Number(txt.substring(2,del-1));
		break;
		case 's':
			del = txt.substr(2,txt.substr(2).indexOf(':'));
			ret = txt.substr( 1+txt.indexOf('"'),del);
			del = txt.indexOf('"')+ 1 + ret.length + 2;
		break;
		case 'a':
			del = txt.indexOf(':{')+2;
			ret = new Array();
			arrlen[level+1] = Number(txt.substring(txt.indexOf(':')+1, del-2))*2;
		break;
		case 'O':
			txt = txt.substr(2);
			var tmp = txt.indexOf(':"')+2;
			var nlen = Number(txt.substring(0, txt.indexOf(':')));
			name = txt.substring(tmp, tmp+nlen );
			//alert(name);
			txt = txt.substring(tmp+nlen+2);
			del = txt.indexOf(':{')+2;
			ret = new Object();
			arrlen[level+1] = Number(txt.substring(0, del-2))*2;
		break;
		case '}':
			txt = txt.substr(1);
			if(arrlen[level] != 0){alert('var missed : '+save); return undefined;};
			//alert(arrlen[level]);
			level--;
		continue;
		default:
			if(level==0) return final;
			alert('syntax invalid(1) : '+save+"\nat\n"+txt+"level is at "+level);
			return undefined;
		}
		if(arrlen[level]%2 == 0){
			if(typeof(ret) == 'object'){alert('array index object no accepted : '+save);return undefined;}
			if(ret == undefined){alert('syntax invalid(2) : '+save);return undefined;}
			key[level] = ret;
		} else {
			var ev = '';
			for(var i=1;i<=level;i++){
				if(typeof(key[i]) == 'number'){
					ev += '['+key[i]+']';
				}else{
					ev += '["'+key[i]+'"]';
				}
			}
			//alert(ev);
			eval('final'+ev+'= ret;');
		}
		arrlen[level]--;//alert(arrlen[level]-1);
		if(typeof(ret) == 'object') level++;
		txt = txt.substr(del);
		continue;
	}
}
