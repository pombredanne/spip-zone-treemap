<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(
	"titre_syntaxeur" => "Syntaxeur",
	"titre_formulaire_generer_syntaxe" => "Générateur de syntaxe",
	"description_generateur_syntaxe" => "
			Entrer l'emplacement d'un fichier source de SPIP et choisir une syntaxe d'arriv&eacute;e.
	",
	
	"saisie_syntaxe" => "Syntaxe à obtenir",
	"saisie_squelette" => "Squelette source",
	"saisie_retour" => "Syntaxe calcul&eacute;e",
	
	"erreur_renseigner_squelette" => "Vous devez donner une adresse de squelette SPIP",
	"erreur_squelette_introuvable" => "Il n'y a pas de squelette SPIP r&eacute;pondant &agrave; ce chemin.",
	"erreur_survenue" => "Une erreur est survenue !",
	
	"squelette_transpose" => "Voici le squelette transpos&eacute; dans la syntaxe &laquo;@gram@&raquo;",
);

?>
