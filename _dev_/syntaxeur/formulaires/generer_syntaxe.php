<?php

function lister_formats_syntaxe() {
	$l = find_all_in_path('public/', 'format_\w+\.php');
	$liste=array();
	foreach($l as $nom=>$addr) {
		$nom = substr($nom, 7, -4);
		$liste[$nom] = $nom;
	}
	return $liste;
}

function formulaires_generer_syntaxe_charger_dist() {
	$contexte = array(
		"syntaxe" => "html",
		"squelette" => "exemples/tout.html",
		"syntaxe_retour" => "",
	);

	// pas de traitement : suppression de la syntaxe de retour poste
	if (!_request('traitement_ok')) {
		set_request('syntaxe_retour', null);
	}
	
	return $contexte;
}

function formulaires_generer_syntaxe_verifier_dist() {
	if (!$squelette = _request('squelette')) {
		$erreurs['squelette'] = _T('syntaxeur:erreur_renseigner_squelette');
	}
	if (!find_in_path($squelette)) {
		$erreurs['squelette'] = _T('syntaxeur:erreur_squelette_introuvable');
	}
	if (count($erreurs)) {
		$erreurs['message_erreur'] = _T('syntaxeur:erreur_survenue');
	}
	return $erreurs;
}


function formulaires_generer_syntaxe_traiter_dist() {
	$gram = _request('syntaxe');
	$squelette = _request('squelette');
	
	// il ne faut pas l'extension du squelette ici
	$i = pathinfo($squelette);
	$squelette = substr($squelette, 0, -(strlen($i['extension'])+1));
	

	$transformer = charger_fonction('transformer','public');
	$resultat = $transformer($squelette, $gram);
	
	set_request('resultat', $resultat);
	set_request('traitement_ok', true);
	set_request('syntaxe_retour', $resultat);

	return array(
		"editable" => true,
		"message_ok" => _T("syntaxeur:squelette_transpose", array("gram"=>$gram))
	);
}

?>
