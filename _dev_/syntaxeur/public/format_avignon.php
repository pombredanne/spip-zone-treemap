<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2009                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;

function format_boucle_avignon ($avant, $nom, $type, $crit, $corps, $apres, $altern, $prof)
{
	$nl = "\n" . str_repeat("\t", $prof);
	$pi = "<" . "?spip" . $nl;
	$avant = $avant ? ($pi . "AVANT ". $nom . " ?" . ">$avant") : "";
	$apres = $apres ? ($apres . $pi . "APRES ". $nom . " ?" . ">") : "";
	$altern = $altern ? ($altern . $pi . "SANS ". $nom . " ?" . ">") : "";
	if ($crit) $crit = "$nl\t$crit";
	return ($avant
	  . $pi
	  . "BOUCLE $nom ($type) {"
	  . $crit
	  . "?" .">"
	  . $corps
	  . $pi . "} $nom ?" . ">"
	  . $apres
	  . $altern);
}

function format_inclure_avignon ($file, $args, $prof)
{
	$prof = "\n" . ($prof <= 0 ? '' : str_repeat("\t", $prof));
	$file = '{' . $file . '}';
	$args = !$args ?  "" : ("(" . join(", ",$args) . ")");
	return ("<" . "?spip$prof" . "INCLURE " . $file . $args  . " ?" . ">");
}

function format_polyglotte_avignon ($args, $prof)
{
	$contenu = array(); 
	foreach($args as $l=>$t)
		$contenu[]= ($l ? "[$l]" : '') . $t;

	return ("#:{" . join("", $contenu) . "}");
}

function format_idiome_avignon ($nom, $module, $args, $filtres, $prof)
{
	foreach ($args as $k => $v) $args[$k] = "$k=$v";
	$args = (!$args ? '' : ('{' . join(',', $args) . '}'));
	$i = "#:"  . ($module ? "$module:" : "") . $nom . $args . $filtres;
	return $filtres ? "[($i)]" : $i;
}

function format_champ_avignon ($nom, $boucle, $etoile, $avant, $apres, $args, $filtres, $prof)
{
	$nom = "#"
	. (($boucle!=='') ? ($boucle . ":") : "")
	. $nom
	. $etoile
	. $args
	. $filtres;

	$s = ($avant OR $apres OR $filtres OR (strpos($args, '(#') !==false));

	return ($s ? ("[" . "$avant($nom)$apres" . "]") : $nom);
}

function format_liste_avignon ($fonc, $args, $prof)
{
	return  (($fonc!=='' ? "|$fonc" : '')
	. (!$args ? "" : ("{" . join(",", $args) . "}")));
}


function format_texte_avignon ($texte, $prof)
{
	return $texte;
}

function format_critere_avignon ($critere)
{
	foreach ($critere as $k => $crit) {
		$crit_s = '';
		foreach ($crit as $operande) {
			list($type, $valeur) = $operande;
			if ($type == 'champ' AND $valeur[0]=='[') {
			  $v = substr($valeur,1,-1);
			  if (preg_match(',^[(](#[^|]*)[)]$,sS', $v))
			    $valeur = substr($v,1,-1);
			}
			$crit_s .= $valeur;
		}
		$critere[$k] = $crit_s;
	}

	return ((!$critere ? "" : ("{" . join(",", $critere) . "}")));
}

// Concatenation sans separateur: verifier qu'on ne cree pas de faux lexemes
function format_suite_avignon ($args)
{
	for($i=0; $i < count($args)-1; $i++) {
		list($texte, $type) = $args[$i];
		list($texte2, $type2) = $args[$i+1];
		if (!$texte OR !$texte2) continue; 
		$c1 = substr($texte,-1);
		if ($type2 !== 'texte') {
		  // si un texte se termine par ( et est suivi d'un champ
		  // ou assimiles, forcer la notation pleine
			if ($c1 == '(' AND substr($texte2,0,1) == '#')
				$args[$i+1][0] = '[(' . $texte2 . ')]';
		} else {
			if ($type == 'texte') continue;
			// si un champ ou assimiles est suivi d'un texte
			// et si celui-ci commence par un caractere de champ
			// ou idiome (:) forcer la notation pleine
			if (($c1 == '}' AND substr(ltrim($texte2),0,1) == '|')
			OR (preg_match('/[\w\d_*]/', $c1) AND preg_match('/^[\w\d_*{|:]/', $texte2)))
				$args[$i][0] = '[(' . $texte . ')]';
		}
	}
	return join("", array_map('array_shift', $args));
}

?>
