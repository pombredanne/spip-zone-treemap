<?php


/* 
 * Transforme un squelette d'une syntaxe concrete a une autre.
 * Pour cela : 
 * - on donne une adresse de squelette, et la grammaire future (nom de la syntaxe concrete)
 *   et la fonction :
 *   - calcule le nom du squelette, son mime
 *   - l'encode dans un charset pour spip
 *   - l'analyse (phraser) pour passer de la syntaxe concrete a la syntaxe abstraite
 *   - puis on decompile l'arbre dans la syntaxe concrete choisie
 * 
 * @param string $source adresse du squelette a phraser/decompiler
 * @param string $gram grammaire de sortie a produire
 * @param string $connect nom de la connexion sql
 * 
 */
function public_transformer_dist($source, $gram_sortie="html", $connect="") {
	

	// trouver la bonne adresse du squelette dans le path
	// (a revoir pour ne pas balayer le path ?)
	$styliser = charger_fonction('styliser', 'public');
	list($squelette, $mime_type, $gram, $sourcefile) =
		$styliser($source, $contexte = array(), $GLOBALS['spip_lang'], $connect, _EXTENSION_SQUELETTES);

	// mettre dans le bon charset le contenu
	include_spip('inc/charsets');
	$squelette = transcoder_page($squelette);
	
	// calcul d'un hash du nom
	include_spip('public/composer');
	$nom = calculer_nom_fonction_squel($squelette, $mime_type, $connect);

    $descr = array('nom' => $nom,
            'gram' => $gram,
            'sourcefile' => $sourcefile,
            'squelette' => $squelette);	
            
	// recuperer le contenu de la page demandee
	if (!lire_fichier($sourcefile, $squelette));

	// phraser le squelette en question
	// on obtient alors un arbre de syntaxe abstraite
	$boucles = array();
	$phraser = charger_fonction('phraser_' . $gram, 'public');
	$arbre = $phraser($squelette, '', $boucles, $descr);

	
	// decompiler l'arbre pour obtenir une nouvelle syntaxe concrete
	// dans la grammaire choisie
	include_spip('public/decompiler');		
	return public_decompiler($arbre, $gram_sortie);
}

?>
