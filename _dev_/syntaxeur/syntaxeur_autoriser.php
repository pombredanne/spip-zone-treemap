<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2009                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

// pour le pipeline d'autorisation
function syntaxeur_autoriser(){}


// bouton du bandeau
function autoriser_syntaxeur_bouton_dist($faire, $type='', $id=0, $qui = NULL, $opt = NULL){
	return 	($qui['statut'] == '0minirezo' and !$qui['restreint']);
}

?>
