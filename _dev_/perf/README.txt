# 2006 Jun 8  - Marc Lebas
# 2006 Jun 16 - Marc Lebas : ab remplace wget

Shell-scripts de comparaisons de temps entre 2 versions SPIP

   abp : script de comparaison partie publique avec ab
   abv : script de comparaison partie privee  avec ab
   cmph : script de comparaisons (abp + abv) pour N versions SVN

Voir les instructions dans les scripts.

Avant de lancer un script, penser a :

 - fermer toutes les applications, 
 - desactiver l'economiseur d'ecran
 - desactiver cron et tous les demons superflus
 - supprimer eventuellement des Plugins ou squelettes particuliers
 - s'assurer que les bases SQL sont équivalentes

Les scripts ont ete testes sous MacOS.

Note : sous MacOS on peut installer svn par fink

