<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// Tests pour la version V2, par intervalle
	'cheval' => 'جواد واحد',
	'cheval_2' => 'جوادان',
	'cheval_3' => 'جياد',
	'cheval_pluriel' => 'جواد',
	'cheval_pluriel_0' => 'جوادان',
	'cheval_pluriel_1' => '@nb@ جياد',
	'cheval_pluriel_2' => '@nb@ جواداً',
	'cheval_pluriel_3' => '@nb@ جواد',
	
	'1_cheval_robe' => 'جواد واحد @robe@ أزرق',
	'1_cheval_robe_pluriel' => '@nb@ جواد @robe@ أزرق',
	'1_cheval_robe_pluriel_0' => 'جوادان @robe@ أزرق',
	'1_cheval_robe_pluriel_1' => '@nb@ جياد @robe@ أزرق',
	'1_cheval_robe_pluriel_2' => '@nb@ جواداً @robe@ أزرق',
	'1_cheval_robe_pluriel_3' => '@nb@ جواد @robe@ أزرق',
	
	'couleur_alezan' => 'ثوبه',
	'couleur_alezan_pluriel' => 'ثوبها',
	'couleur_alezan_pluriel_0' => 'ثوبهما',
	'couleur_alezan_pluriel_1' => 'ثوبها',
	'couleur_alezan_pluriel_2' => 'ثوبها',
	'couleur_alezan_pluriel_3' => 'ثوبها',
		
	'temps_minute' => 'منذ دقيقة واحدة',
	'temps_minute_pluriel' => 'منذ @nb@ دقيقة',
	'temps_minute_pluriel_0' => 'منذ دقيقتين',
	'temps_minute_pluriel_1' => 'منذ @nb@ دقائق',
	'temps_minute_pluriel_2' => 'منذ @nb@ دقيقةً',
	'temps_minute_pluriel_3' => 'منذ @nb@ دقيقة',

);
?>
