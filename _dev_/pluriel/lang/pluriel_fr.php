<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// Tests pour la version V2, par intervalle
	'cheval' => 'cheval',
	'cheval_pluriel' => 'chevaux',
	'cheval_pluriel_0' => 'chevaux',
	'1_cheval_robe' => '@nb@ cheval avec une robe @robe@',
	'1_cheval_robe_pluriel' => '@nb@ chevaux avec une robe @robe@',
	'1_cheval_robe_pluriel_0' => '@nb@ chevaux avec une robe @robe@',
	'couleur_alezan' => 'alezan',
	'couleur_alezan_pluriel_0' => 'alezan',
	
	'temps_minute' => 'il y a @nb@ minute',
	'temps_minute_pluriel' => 'il y a @nb@ minutes',
	'temps_minute_pluriel_0' => 'il y a @nb@ minutes',

);
?>
