<?php
include_spip('inc/lang_pluriel');
/**
 * Gestion du singulier et du pluriel pour une chaine de langue et une langue donnees
 *         Le filtre permet de gerer des pluriels multiples dont la forme est choisie par une formule.
 *         Cette formule est definie dans un tableau 'code' => 'formule' qui regroupe uniquement les
 *         langues ayant des formes non standard (cad differente de n>1)
 *         Ce tableau est une globale dans le fichier inc/lang_pluriel.php
 *         L'item pluriel resultant d'une forme $forme se nomme item_un_pluriel_${formel} 
 *         Concernant le traitement, pour ne pas ralentir les langues a pluriel "standard n>1" l'evaluation 
 *         n'est faite que pour les langues du tableau. Pour les autres on retient la forme pluriel_0
 *         Cette logique est donc celle de gettext a l'exception du traitement du zero et 
 *         le suffixe _pluriel_ permettra de facilement passer des .php aux .po et vice et versa
 *         si besoin.
 *
 * @param int				$n
 * @param string			$chaine_un
 * @param array / string	$options
 * @return string
 */

// $n					=> le compteur !
// $chaine_un			=> item de langue pour le singulier (format php)
// $extras				=> parametres additionnels valables pour le singulier et le pluriel
//					   la cle 'nb' n'est pas consideree comme un parametre mais comme un 
//					   renommage de parametre valeur par defaut fixe a @nb@
//					   Si on veut juste renommer 'nb' on peut passer la chaine directe
function un_ou_plusieurs($n, $chaine_un, $options=array()) {
	global $spip_lang, $spip_lang_pluriel;

	if (!$n=intval($n)) 
		return '';

	// Par defaut, le parametre eventuel designant la valeur dans la chaine est reperer par @nb@
	// On teste si il est precise une option differente : dans ce cas on supprime aussi cette cle
	// des parametres optionnels
	$params = array('nb' => $n);
	if (is_array($options)) {
		if (array_key_exists('nb', $options)) {
			$params = array($options['nb'] => $n);
			unset($options['nb']);
		}
		$params = array_merge($params, $options);
	}
	else if ($options)
		$params = array($options => $n);

	// Traitement des langues qui supportent des formes de pluriel
	// -- On traite le seuil si il existe ou on retourne la forme standard de pluriel
	$texte = '';
	if (array_key_exists($spip_lang, $spip_lang_pluriel) AND $n > 1) {
		// On evalue la formule de calcul de la forme de pluriel correspondant a la valeur fournie
		// -- Cette valeur est coherente avec la methode utilisee par gettext sur les .po auto_commit
		//    traitement du zero pret
		$formule = $spip_lang_pluriel[$spip_lang];
		$forme = eval("return $formule;");
		if ($forme !== null)
			$texte = _T($chaine_un . '_pluriel_' . strval($forme), $params);
	}
	
	// Traitement des autres cas incluant aussi l'absence de chaine pluriel specifique pour une langue le supportant
	if (!$texte)
		if ($n == 1)
			$texte = _T($chaine_un, $params);
		else
			$texte = _T($chaine_un . '_pluriel_0', $params);
	
	return $texte;
}

// Ancienne version gardee pour historique
function un_ou_plusieurs_v1($n, $chaine_un, $options=array()) {
	global $spip_lang;
	// D�finition du tableau des langues ayant des formes de pluriel multiples
	// Cette d�claration est sujette � modification !!!
	static $spip_lang_pluriel = array(
		'ar' => '2,3,11');

	if (!$n=intval($n)) 
		return '';

	// Par defaut, le parametre eventuel designant la valeur dans la chaine est reperer par @nb@
	// On teste si il est precise une option differente : dans ce cas on supprime aussi cette cle
	// des parametres optionnels
	$params = array('nb' => $n);
	if (is_array($options)) {
		if (array_key_exists('nb', $options)) {
			$params = array($options['nb'] => $n);
			unset($options['nb']);
		}
		$params = array_merge($params, $options);
	}
	else if ($options)
		$params = array($options => $n);

	// Traitement des langues qui supportent des formes de pluriel
	// -- On traite le seuil si il existe ou on retourne la forme standard de pluriel
	$texte = '';
	if (array_key_exists($spip_lang, $spip_lang_pluriel) AND $n > 1) {
		// On recupere les seuils min de la langue
		$seuils = array_map('intval', explode(',', $spip_lang_pluriel[$spip_lang]));
		if ($seuils) {
			asort($seuils);
			foreach ($seuils as $_i => $_borne_min) {
				// Si on est sur le dernier seuil on consid�re que la borne max est infinie et on lui affecte 0
				$borne_max = ($_i == count($seuils) - 1) ? 0 : $seuils[$_i+1];
				if ($borne_max == 0)
					// On est sur le dernier seuil, c'est donc le seuil pluriel standard
					// -- on sort de la boucle pour passer dans le cas standard
					break;
				else 
					if (($n >= $_borne_min) AND ($n < $borne_max)) {
						$item = $chaine_un . '_' . strval($_borne_min);
						$texte = _T($item, $params);
						// On verifie que l'item pluriel existe bien et que le texte retourne est donc le bon
						// Sinon on essayera d'utiliser l'item pluriel comme pour les autres langues
						// -- Le test n'est pas terrible mais c'est la seule solution aujourd'hui !!!
						$item_non_trouve = str_replace('_', ' ', (($n = strpos($item,':')) === false ? $item : substr($item, $n+1)));
						if (!$texte OR ($texte == $item_non_trouve))
							$texte = '';
						break;
					}
					
					
			}
		}
		
	}
	
	// Traitement des autres cas incluant aussi l'absence de chaine pluriel specifique pour une langue le supportant
	if (!$texte)
		if ($n == 1)
			$texte = _T($chaine_un, $params);
		else
			$texte = _T($chaine_un . '_pluriel', $params);
	
	return $texte;
}

?>