<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/presentation');
include_spip('pluriel_fonctions');

function exec_pluriel_dist(){
	global $spip_lang_right;
	// si pas autorise : message d'erreur
	if (!autoriser('webmestre')) {
		include_spip('inc/minipres');
		echo minipres();
	} else {

	// pipeline d'initialisation
	pipeline('exec_init', array('args'=>array('exec'=>'plugonet'),'data'=>''));

	// entetes
	$commencer_page = charger_fonction('commencer_page', 'inc');
	echo $commencer_page(_T('Pluriel'), "naviguer", "pluriel");
	echo "<br />\n";
	echo "<br />\n";
	
	// titre
	echo gros_titre('Tests nouveau filtre de gestion singulier/pluriel<br />|un_ou_plusieurs','', false);
	
	// barre d'onglets
//	echo barre_onglets("pluriel", "pluriel");
	
	// colonne gauche
	echo debut_gauche('', true);
	echo pipeline('affiche_gauche', array('args'=>array('exec'=>'pluriel'),'data'=>''));
	
	// colonne droite
	echo creer_colonne_droite('', true);
	echo pipeline('affiche_droite', array('args'=>array('exec'=>'pluriel'),'data'=>''));
	
	// centre
	echo debut_droite('', true);

	// contenu
	// Test filtre
	echo "<br />\n";
	echo "<strong>temps_minute</strong><br />"; 
	for ($i = 1; $i <= 15; $i++) {
		echo "nb=$i : <br />" . 
				un_ou_plusieurs($i, 'pluriel:temps_minute') . 
				"<br />";
	}
	echo "<br />\n";
	echo "<strong>cheval</strong><br />"; 
	for ($i = 1; $i <= 15; $i++) {
		echo "nb=$i : <br />" . 
				un_ou_plusieurs($i, 'pluriel:cheval') . 
				"<br />";
	}
	for ($i = 100; $i <= 115; $i++) {
		echo "nb=$i : <br />" . 
				un_ou_plusieurs($i, 'pluriel:cheval') . 
				"<br />";
	}
	echo "<br />\n";
	echo "<strong>1_cheval_robe</strong><br />"; 
	for ($i = 1; $i <= 15; $i++) {
		echo "nb=$i : <br />" . 
				un_ou_plusieurs($i, 'pluriel:1_cheval_robe', 
									array('robe' => un_ou_plusieurs($i, 'pluriel:couleur_alezan'))) . 
				"<br />";
	}
	
	// fin contenu
	echo pipeline('affiche_milieu', array('args'=>array('exec'=>'pluriel'),'data'=>''));

	echo fin_gauche(), fin_page();
	}
}

?>
