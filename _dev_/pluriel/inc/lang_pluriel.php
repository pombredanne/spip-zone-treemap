<?php

/**
 * Gestion du singulier et du pluriel pour certaines langues
 *
 */
$GLOBALS['spip_lang_pluriel'] = array(
	"ar" => "(\$n <= 2 ? 0 : (\$n <= 10 ? 1 : (\$n <= 99 ? 2 : 3)));"
);

?>