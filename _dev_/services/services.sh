#!/bin/sh

# Test des différents services de spip

# on s'amenage un p'tit coin pour bosser pepere
ici=$(cd $(dirname $0) ; /bin/pwd)
rm -rf $ici/tmp/*
mkdir -p $ici/tmp
tmp=$ici/tmp

# commande pour faire un get
# mettre "GET" pour aller chercher a distance, ou "cat" pour aller en local
##GET=GET
GET=cat

# l'endroit ou se trouvent les fichiers de reference
##BASEURL=http://www.spip.net/services
BASEURL=$ici/zz

# le repertoire ou il faut deposer les fichiers du jour
DEST=$tmp

# commande pour faire un simple head pour tester les sites
HEAD=HEAD

# recup de la liste des services
$GET "$BASEURL/services.txt" > $tmp/services

# pour chaque service :
IFS=';'
cat $tmp/services | while read service test ; do
    echo "SERVICES $service"

    # recup de la liste des serveurs
    $GET "$BASEURL/$service.txt" > $tmp/$service.txt.ref

    # si on n'arrive pas a recuperer la liste, on passe
    if [ ! -s $tmp/$service.txt.ref ] ; then
	echo "liste des services $service INDISPO" 1>&2
	continue
    fi

    # pour chaque serveur
    cat $tmp/$service.txt.ref | while read url contact ; do
	# si il marche, on l'ajoute a la liste des serveurs ok
	if $HEAD "$url$test" > /dev/null ; then
	    echo "URL $url ok"
	    echo "$url" >> $DEST/service.$service.ok
    	else
	    # sinon, on passe
	    echo "erreur sur service $service $url" 1>&2
    	fi
    done

done
