<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// Formulaire de forum
'forum_info_modere' => 'Ce site mod&egrave;re les commentaires. Votre message n\'appara&icirc;tra qu\'apr&egrave;s avoir &eacute;t&eacute; relu et approuv&eacute;.',



'forum_page_url' => ' ',

'forum_votre_nom' => '* Votre nom',
'forum_votre_email' => '* Votre courriel (non publi&eacute;)',
'forum_titre' => ' ',
'forum_url' => 'Votre site Web (optionnel)',

'forum_voir_avant' => 'Pr&eacute;visualiser',
'forum_message_definitif' => 'Confirmer l\'envoi',

// Formulaire de contact
'form_pet_votre_email' => 'Votre courriel :',
'form_prop_sujet' => 'Sujet :',
'info_texte_message' => 'Texte de votre message :',

'form_prop_envoyer' => 'Pr&eacute;visualiser',
'form_prop_confirmer_envoi' => 'Confirmer l\'envoi',

// Divers
'sites_web' => 'Liens utiles',
'suite' => 'Lire la suite',
'contact' => 'Contact',

"aucun_article" => 'Aucun article',
"1_article" => '1 article',
"nb_articles" => '@nb@ articles',

);

?>