<?php

function formulaires_troglo_charger_dist() {
	$contexte = array(
		'tee' => '',
		'pseudo' => '',
		'email' => '',
		'nbenfants' => '',
		'nbadultes' => '',
		'jour' => '',
	);
	return $contexte;
}


function formulaires_troglo_verifier_dist() 
{
	include_spip('base/abstract_sql');
	$retour = array();
	if (  !_request('email')) {
		$retour['message_erreur'] = "Merci de renseigner l'Email " ;
	}
	else 
	{
		if (!email_valide(_request('email'))) {
			$retour['message_erreur'] = "Votre email ne semble pas valide ";
		}
		else 
		{
			if (  !_request('pseudo')) {
				$row= sql_fetsel('*', 'spip_troglo', 'email="'._request ('email').'"');
				set_request('tee', $row['tee']) ; 
				set_request('pseudo', $row['pseudo']) ; 
				set_request('nbenfants', $row['nbenfants']); 
				set_request('nbadultes', $row['nbadultes']); 
				$jour=array();
				if ( $row['vendredi'] == 1 ) {array_push($jour,'vendredi');} 
				if ( $row['samedi'] == 1 ) {array_push($jour,'samedi');} 
				if ( $row['dimanche'] == 1 ) {array_push($jour,'dimanche');} 
				set_request('jour', $jour);
				$retour['message_erreur'] = "&nbsp;";
			}
		}
	}
	 
	return $retour ;
}




function formulaires_troglo_traiter_dist (){
	$retour = array();
include_spip('base/abstract_sql');

#appeller cette fonction pour la creation de la table 
# _maFonctionJustePourleFunQuiNeSeraPasAppellee45 () ; 


	$jour =_request('jour');
	$id_troglo = sql_getfetsel('id_troglo', 'spip_troglo', 'email="'._request ('email').'"');
	spip_log(__LINE__);
	if ( $id_troglo >0 ) {
	spip_log("update $id_troglo");
	   $res_lien = sql_updateq(
			'spip_troglo',
			array(
				'pseudo' => _request('pseudo') ,
				'tee' => _request ('tee'), 
				'email' => _request ('email'), 
				'nbadultes' => _request ('nbadultes'),
				'nbenfants' => _request ('nbenfants'), 
				'vendredi' => in_array('vendredi',$jour),
				'samedi' => in_array('samedi',$jour),
				'dimanche' => in_array('dimanche',$jour),
				'date' => date('Y-m-d H:i:s'), 
			),
			"id_troglo=$id_troglo") ;  
			$retour['message_ok'] = $retour['message_ok']."Merci vos infos sont à jour";
		$sujet_mail="[TROGLO] "._request('email')." Mise a jour"; 
            
		}
	else {	
	spip_log("Insert ");
	   $res_lien = sql_insertq('spip_troglo',array(
                'pseudo' => _request('pseudo') ,
                'email' => _request ('email'), 
                'tee' => _request ('tee'), 
                'nbadultes' => _request ('nbadultes'),
                'nbenfants' => _request ('nbenfants'), 
		'date' => date('Y-m-d H:i:s'), 
                'vendredi' => in_array('vendredi',$jour),
                'samedi' => in_array('samedi',$jour),
                'dimanche' => in_array('dimanche',$jour),
        ));
			$retour['message_ok'] = $retour['message_ok']." Merci c'est noté, si vous voulez modifier vos infos : il suffit de reremplir le formulaire avec l'email que vous avez utilisé";
		$sujet_mail="[TROGLO] "._request('email')." Inscription"; 
	}

	$texte_email ="\n";
	$envoyer_mail = charger_fonction('envoyer_mail','inc');
	$envoyer_mail("ben@rezo.net", "".$sujet_mail,htmlentities($texte_email),"ben@rezo.net");

	$retour['editable']=true; 	
	include_spip('inc/invalideur');
	suivre_invalideur(1);
	return $retour ; 
	/*

	// message
	//	include_spip('inc/headers');
//	redirige_par_entete('http://36.36pulp.com/spip.php?page=merci');

	
//	return array(
//	"redirect" =>'http://36pulp.com/spip.php?page=merci',
//	);
*/
}


function _maFonctionJustePourleFunQuiNeSeraPasAppellee45 ()
{

include_spip('base/create');
include_spip('base/abstract_sql');
include_spip('base/serial');
	
$spip_troglo = array(
		"id_troglo"	=> "bigint(21) NOT NULL",
		"tee"	   => "text DEFAULT '' NOT NULL",
		"pseudo"	   => "text DEFAULT '' NOT NULL",
		"email"	   => "text DEFAULT '' NOT NULL",
		"vendredi"	=> "integer DEFAULT '0' NOT NULL",
		"samedi"	   => "integer DEFAULT '0' NOT NULL",
		"dimanche"	=> "integer DEFAULT '0' NOT NULL",
		"nbenfants"	=> "integer DEFAULT '0' NOT NULL",
		"nbadultes"	=> "integer DEFAULT '0' NOT NULL",
		"date"	=> "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL",
		"ip"	=> "varchar(40) DEFAULT '' NOT NULL"

);

$spip_troglo_key = array(
		"PRIMARY KEY"		=> "id_troglo"
);
$table_troglo= array('field' => &$spip_troglo, 'key' => &$spip_troglo_key);

creer_ou_upgrader_table("spip_troglo",$table_troglo,true);


}


?>
