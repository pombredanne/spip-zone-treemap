==== Objectif de _galaxie_ ====

Ce dépot à pour vocation de regrouper l'ensemble des developpements concernant les sites de la communauté.

==== Pourquoi un dépot dédié ====

Il se trouve qu'avec le temps les sites de la communauté se retrouvent avec des spécificités telles qu'elles ne peuvent pas êtré généralisées. Pour autant, il est utile de pouvoir les developper en commun et que chacun puisse les modifier et que les modifs soient répercutées en ligne.

Pour aider les visiteurs de la zoneA et de ne pas perdre les utilisateurs dans leur choix :
- de plugins, 
- d'outils 
- ou de squelettes, 
il est préférables de mettre à part ces developpements.

Ainsi les dépots _plugins_ et _squelettes_ retrouvent leur objectif de base : proposer des outils mis à disposition par la communauté et exploitable par tous.


==== Régles ====

1/ Le nom des sous dépots
Les dépots doivent prendre le nom du site comme _galaxie_/www.spip.net/
Comme les projets contenus sont dédiés à un site unique, cette solution offre une cohérence d'organisation.

2/ Organisation

2.1/ Squelettes
_galaxie_/www.site.net/squelettes/  pour les squelettes du site

2.2/ plugins
_galaxie_/www.sites.net/plugins/    pour les plugins
un sous repertoire par plugin

2.3/ outils
_galaxie_/www.site.net/_outils_/    pour les outils dédiés
nous pouvons retrouver par exemple des script shell ou autre pour automatiser certains taches (cron , ...)


==== Divers ===

**Déplacer un projets :

Au besoin, création de la rubrique principale :
svn mkdir _galaxie_/www.site.net/

Exemple pour déplacer un squelette : 
svn rename  svn://zone.spip.org/spip-zone/.../squelettes  svn://zone.spip.org/spip-zone/_galaxie_/www.site.net/squelettes

**Synchroniser les serveurs

Pour remettre à plat les dépots locaux (serveur, poste de dev)
svn switch svn://zone.spip.org/spip-zone/_galaxie_/www.site.net/squelettes /chemin_local
