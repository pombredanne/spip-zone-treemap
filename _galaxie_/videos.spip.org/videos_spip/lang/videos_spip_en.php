<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

//A
'about_authors' => 'About the authors',
'about_video' => 'About the video',

//C
'contact_auteur' => 'Contact',
'comite' => 'Comittee',
'comment' => 'Commentaire',
'comments' => 'Commentaires',
'commentaires_fermes' => 'Comments closed',
'country' => 'Country',

//D
'dernieres_videos_auteur' => 'Lasted videos from',
'description' => 'Description',

//E
'embed_video' => 'embed the video',

//L
'lang' => 'Spoken language',
'licence' => 'Conditions of use',
'linktopage' => 'link to this page',
'linktovideo' => 'link to video',
'login' => 'Login',

//I
'inscription' => 'Inscription',

//M
'ma_page' => 'My page',

//P
'published' => 'Published the',

//R
'related_articles' => 'Related articles',
'related_thematic' => 'Other videos related to the thematic',

//S
'subtitles' => 'Subtitles',

//T
'thematic' => 'Thematic',

//V
'videos_auteur' => 'All videos from'
'views' => 'Views'

);

?>
