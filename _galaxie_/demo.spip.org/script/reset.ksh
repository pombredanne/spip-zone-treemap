#!/bin/bash
REP=/var/www/demo.spip.net/public_html

cd $REP
#on fait un svn update sur la branche stable 
svn update 

#on vide le cache 
rm -rf $REP/tmp/cache 
rm -rf $REP/local/*
rm -rf $REP/IMG/*

#on recopie les images et la base 
echo " cp -p -r $REP/../svn/IMG/* $REP/IMG/" 
cp -p -r $REP/../svn/IMG/* $REP/IMG/

echo "cp  $REP/../svn/config/bases/spip.sqlite $REP/config/bases/spip.sqlite" 
cp  $REP/../svn/config/bases/spip.sqlite $REP/config/bases/spip.sqlite 
chmod 755 $REP/config/bases/spip.sqlite 

chown -R www-data:www-data $REP
 
