<?php

// This is a SPIP language file -- Ceci est un fichier langue de SPIP
// pour les squelettes de SPIP Mag'

$GLOBALS[$GLOBALS['idx_lang']] = array (

// A
'a_propos' => 'About this site',
'a_retenir' => 'To remember',
'abo_liste' => 'To be involved or just stay informed, subscribe to the SPIP Mag&#8217; mailing list',
'agenda' => 'Agenda',
'aller_contenu' => 'Go to the content',
'aller_forum' => 'Go to the forum',
'aller_menu' => 'Go to the menu',
'aller_recherche' => 'Go the search form',
'aucun_article' => 'No results for this search.',

// C
'calendrier' => 'Calendar',
'contact' => 'Contact us',

// D
'dans_rubrique' => 'In this section',
'date_modif' => 'Date of latest modification',
'date_public_detail' => 'Date de publication de cette page',
'date_update_detail' => 'Date de la derni&egrave;re modification de cette page',
'documents_joints' => 'Documents joints',

// E
'epingle' => 'In the spolight',
'et' => 'and',
'exemple' => 'Example for the contribution',

// F
'recherche_mot' => 'Search the articles related to this keyword',
'fil_atom' => 'Atom',
'fil_rss' => 'RSS',
'form_recherche' => 'Search form',

// I
'imprimer' => 'Print this page',
'index_auteurs' => 'Index of authors',

// K
'kwadneuf' => 'What\'s new?',

// L
'lastmod' => 'Last modification of the site',
'liens' => 'Links',
'liens_privilegies' => 'Favourite links',

// M
'mag' => 'Mag&#8217;',
'mailing_list' => 'Mailing list',
'mentions_legales' => 'Legal notice',
'mis_a_jour_le' => 'Last update',
'mise_a_jour' => 'Update',

// N
'nombre_visiteurs' => 'Number of visitors',

// O
'ordre_antichronologique' => 'Anti-chronological order',
'ordre_chronologique' => 'Chronological order',
'ordre_inverse' => 'reversed order',
'ordre_normal' => 'straight ordre',

// P
'par_date' => 'By date',
'par_date_modif' => 'By modification date',
'par_nombre_visiteurs' => 'By the number of visitors',
'par_ordre_antichronologique' => 'By anti-chronological order',
'par_ordre_chronologique' => 'By chronological order',
'par_pertinence' => 'By relevance',
'par_popularite' => 'By popularity',
'participer' => 'Participate to the site',
'popularite_detail' => 'Daily Numbre of visitors',
'public' => 'Publication date',
'publie_le' => 'Published on',

// Q

// R
'recherche' => 'Search',
'retour_accueil' => 'Back to the homepage',

// S
'site_heberge_par' => 'Hosted by',
'sondage' => 'Poll',
'sous_rubriques' => 'Sub-sections',
'squelettes' => 'The templates of this site are freely available',
'sur_spipmag' => 'On SPIP Mag&#8217;',

// T
'titre_forum' => 'Les r&eacute;actions re&ccedil;ues &agrave; cet article',
'total_visiteurs' => 'visitors since publication',
'traductions' => 'Translations of this article',
'trier' => 'Sort',
'trier_date' => 'Sort by date',
'trier_par' => 'Sort by',
'trier_pertinence' => 'Sort by relevance',
'tris_divers' => 'Various sortings',

// U
'update' => 'Last update',

// V
'visiteurs' => 'visitors',
'visiteurs_detail' => 'Total number of visits on this page',
'visiteurs_par_jour' => 'visitors by day',
'votre_requete' => 'Your request',

// X
'xhtml_valide' => 'Valid XHTML'

);

?>
