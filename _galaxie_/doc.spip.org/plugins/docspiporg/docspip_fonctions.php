<?php

  /**
   * transforme le liste de fichiers presente dans le ps d'un article
   * de l'autodoc en liste de liens vers le trac
   * utilise aussi pour lister les appelants dans l'extra de meme nom
   */
  function urlVersTrac($texte, $titre=null) {

    static $tracRoot='http://core.spip.org/trac/spip/browser';
  
    if(trim($texte)=='') return '';
  
    $fichiers=preg_split('/\s+/', $texte);
    
    if($titre) {
  	$ancre= "#$titre";
    } else {
  	$ancre= '';
    }
    
    $res='';
    foreach($fichiers as $fichier) {
        $res .= "<li><a href='$tracRoot/$fichier$ancre'>$fichier</a></li>\n";
    }
    if($res) return "<ul>\n$res</ul>\n";
    else return '';
  }
  
  function affiche_auteur_diff($auteur) {	
    // Si c'est un nombre, c'est un auteur de la table spip_auteurs
      if ($auteur == intval($auteur)
        AND $s = sql_query("SELECT * FROM spip_auteurs WHERE id_auteur="._q($auteur))
        AND $t = sql_fetch($s)) return typo($t['nom']);
      else return $auteur;
  }

// les fonctions pour le mod�le memo_api.html  
  // retourne une s�rie de balises <$balise> avec les segments de $ch s�par�s par $delimiteur
  function liste($ch, $delimiteur=',', $balise='li') {
      $ret = '';
      $Targs = explode($delimiteur, $ch);
      foreach ($Targs as $arg) $ret .= '<'.$balise.'>'.$arg.'</'.$balise.'>';
      return $ret;
  }
  // retourne le fichier en fin d'un chemin
  function fichier($chem) {
      $Tchem = explode('/', $chem);
      return array_pop($Tchem);
  }
  
  function cheminTrac($texte, $titre=null) {
      static $tracRoot='http://trac.rezo.net/trac/spip/browser/spip';
      if(trim($texte) == '') return '';
      if ($Ttexte = strtok($texte, " \n\t")) $texte = $Ttexte;
      if($titre) $ancre = "#$titre";
      else $ancre = '';

      return $tracRoot.'/'.$texte.$ancre;
  }

?>
