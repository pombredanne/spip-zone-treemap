<?php

  /**
   * ce script permet d'analyser le code d'une serie de fichiers
   * et de generer des rubriques et articles spip a partir de ce qu'il y trouve
   */

// le repertoire ou se trouvent les sources de spip a analyser
$dirSrc= null;

// celui ou se trouve l'arbo du site spip dans lequel il faut inserer la doc
$spipDir= null;

// le format des urls vers lesquelles il faut pointer
$urlDocSite= "http://doc.spip.org/";
$prefixeDocSite= "@";

// les fichiers a prendre en compte
$extensions=array('php', 'php3');
$sousRepertoires=array(
	'.',
	//	'plugins',
	'ecrire',
	'ecrire/action',
	'ecrire/balise',
	'ecrire/base',
	'ecrire/charsets',
	'ecrire/exec',
	'ecrire/genie',
	'ecrire/inc',
    'ecrire/install',
	'ecrire/public',
	'ecrire/req',
	'ecrire/urls',
	'ecrire/xml'	
);


// la liste de fichiers a traiter
$fichiers= array();

// analyse de la ligne de commande
array_shift($argv);

while(count($argv)!=0) {
  $opt= array_shift($argv);
  switch($opt) {
  case '-url':
	$urlDocSite= array_shift($argv);
	break;
  case '-prefixe':
	$prefixeDocSite= array_shift($argv);
	break;
  case '-spip':
    // la racine du site doc.spip.org
	$spipDir= array_shift($argv);
	break;
  case '-src':
    //le depot svn local
	$dirSrc= array_shift($argv);
	break;
  case '-fichier':
	array_unshift($fichiers, array_shift($argv));
	break;
  default:
	die("option $opt ???\n");
	break;
  }
}

// apres ca, quelques variables doivent etre peuplees
if($spipDir == null) {
  die("option -spip necessaire\n");
}

if($dirSrc == null) {
  die("option -src necessaire\n");
}

chdir($spipDir);

// le minimum pour faire comme si on etait dans spip
define('_DIR_RACINE', "$spipDir/");
define('_DIR_RESTREINT_ABS', "$spipDir/ecrire/");
define('_DIR_RESTREINT', _DIR_RESTREINT_ABS);
include("$spipDir/ecrire/inc_version.php");

// verifier la connexion a la bdd
spip_connect();
include_spip("base/abstract_sql");

$r= sql_select(array("count(*)"), array("spip_articles"));
if (!$r || !sql_fetch($r)) {
  die("echec d'acces a la bdd (".sql_error($r).")\n");
}


//on determine l'identifiant de batch
$id_auteur = sql_getfetsel('id_auteur', 'spip_auteurs',"login LIKE 'batch'");

/***************
// VERSION 1 : recup via un squelettes

// inclure la liste des rubriques/articles deja en place
include_spip("public/assembler");
$code=recuperer_fond('geneDoc');

// il s'agit d'un squelette generant un extrait de code php
// => eval dessus pour l'interpreter
//echo "<<<\n$code\n>>>\n";
eval($code);
//echo "VERSION 1\n";
//var_export($rubriques);
***************/

// VERSION 2 : recup sans squelettes

// trouver la rubrique "code"
$r= sql_select(
	array("id_rubrique"),
	array("spip_rubriques"),
	array(array("=", "titre", "'code'")));
if(!($l=sql_fetch($r))) {
	die("trouve pas la rubrique 'code'");
}
$secteur= $l['id_rubrique'];
$rubriques= array('.' => $secteur);

// sortir tout ce qui est en dessous
$r= sql_select(
	array("id_rubrique", "id_parent", "titre"),
	array("spip_rubriques"),
	array(array("=", "id_secteur", $secteur)));
while($l=sql_fetch($r)) {
	$rr[]= $l['id_rubrique'];
	$enfants[$l['id_parent']][$l['id_rubrique']]=$l['titre'];
}

// sortir tous les articles qui y sont accroches
$r= sql_select(
	array("id_rubrique", "id_article", "titre"),
	array("spip_articles"),
	array(
		array("in", "id_rubrique", '('.join($rr, ',').')'),
		array('=', 'statut', "'publie'")
	));
while($l=sql_fetch($r)) {
	$articles[$l['id_rubrique']][$l['id_article']]= $l['titre'];
}

// reconstituer l'arbo
$rubriques= reconstituer($secteur);
//echo "\nVERSION 2\n";
//var_export($rubriques);

// se positionner a la racine du depot svn
chdir($dirSrc);

// puis, si la liste des fichiers n'est pas passee en argument, lister
// tous les fichiers
if(count($fichiers)==0) {
  foreach($sousRepertoires as $sousRepertoire) {
	foreach($extensions as $ext) {
	  //parcours toutes les repertoires et liste tous les fichiers repondant au masque (.php et .php3)
	  $fichiers= array_merge($fichiers, glob("$sousRepertoire/*.$ext"));
	}
  }
  
  // depublier tous les articles pour laisser la suite du script remettre
  // a 'publie' ceux qui sont valides
  sql_updateq('spip_articles',array("statut"=>'prepa'),"id_secteur=".$secteur);
  // mais on ne fait ca que si on passe tous les fichiers, sinon, on va
  // refuser des articles sur lesquels on ne va pas repasser
}
//var_export($fichiers);

// les choses qu'on va chercher a recuperer, et leur nom francais
$types=array('function' => 'fonction', 'class' => 'classe');

// en passant, on construit un tableau avec tous les appels qu'on croise
$appels=array();
// et un de toutes les fonctions, pour savoir quels appels sont pertinants
$fonctions=array();

foreach($fichiers as $n => $fichier) {
  //echo "fichier\t$fichier\n";
  $base= basename($fichier);

  // creer la rubrique associee a ce fichier
  $rubs= explode('/', $fichier);
  $rub= &creerArbo($rubs, $rubriques);
  //echo "creerArbo => ".var_export($rub, 1);

  // lire le fichier dans un tableau de lignes
  $code= file($fichier);
  $recode=array();

  $modifie= false;	 

  // y chercher les declarations
  foreach($code as $num => $ligne) {
	if(preg_match("/^(?:\s*)(class|function)\s+&?(\w+)\s*(\(.*)/", $ligne, $reg)) {
	  list($tout, $type, $nom, $args)= $reg;
	  //echo "$type\t$fichier\t$nom\n";

	  if($type=='function') {
		  $l= $args;
		  // chercher a recuperer tous les arguments dans cette ligne
		  // ou en ajoutant les lignes suivantes
		  for($i=1; $i<10; $i++) {
			  if(preg_match("/^\((.*)\)\s*{/s", $l, $reg)) {
				  $args= $reg[1];
				  break;
			  }
			  $l.=$code[$num+$i];
		  }
	  }

	  $link= "// $urlDocSite$prefixeDocSite$nom";

	  // le lien est deja la => on passe
	  if($num == 0 || !preg_match(
		  "|^$sep// $urlDocSite$prefixeDocSite$nom$|",
		  $code[$num-1])) {
	  	// il y a un lien foireux => on remplace
	  	if($num > 0 && preg_match("|^[\s/*]*$urlDocSite.*|",
								  $code[$num-1])) {
	  		$recode[$num-1]= "$sep$link\n";
			$i= $num-2;
	  	} else {
	  		// sinon, on en insere un nouveau
	  		$recode[$num]= "$sep$link\n".$code[$num];
			$i= $num-1;
	  	}
		$modifie= true;
	  } else {
		$i= $num-2;
	  }

	  // remonter dans le code pour choper les commentaires
	  $comments="";
	  $dansRem=false;
	  while($i>=0) {
		if(!$dansRem && preg_match("|^\s*/\*\**(.*)\*\**/\s$|", $code[$i], $r)) {
		  // commentaire bloc sur une ligne
		  $comments="$r[1]\n$comments";
		} elseif(!$dansRem && preg_match(":^\s*(\#+|//+)\s*(.*)$:",
										 $code[$i], $r)) {
		  // commentaire mono ligne (diese ou double slash)
		  $comments="$r[2]\n$comments";
		} elseif(preg_match("|^\s*$|", $code[$i])) {
		  // ligne vide, on passe ...
		} elseif($dansRem && preg_match("|^\s*/\*\**(.*)$|", $code[$i], $r)) {
		  // debut d'un commentaire bloc
		  $comments="$r[1]\n$comments";
		  $dansRem= false;
		} elseif($dansRem) {
		  $comments=$code[$i]."\n$comments";
		} elseif(!$dansRem && preg_match("|^\s*(.*)\**\*/\s$|", $code[$i], $r)) {
		  // fin d'un commentaire bloc
		  $comments="$r[1]\n$comments";
		  $dansRem= true;
		} else {
		  break;
		}
		$i--;
	  }

	  $article= array(
			'titre' => $nom,
			'rubrique' => $rub['.'],
			'surtitre' => $types[$type],
			'descriptif' => $comments,
			'fichier' => $fichier,
			'url_propre' => "$nom"
	  );
	  if($args) {
		  $article['soustitre']= $args;
	  }
	  //echo (string)($num-1)." : ".var_export($code[$num-1], 1)."\n";
	  //echo "$num : ".var_export($code[$num], 1)."\n";
	  //var_export($article);
	  $fonctions[$nom]= $rub["A $nom"]= creerArticle($article);
	} else {
	  // pas une declaration de fonction => peut etre un appel de fonction
	  if(preg_match_all('/(\w+)\(/', $ligne, $reg)) {
	    //ne pas oublier le cas de figure appel de fonctions imbriquées et autre
	    foreach($reg[1] as $val) {
	      $appels[$val][$fichier]=null;
	    }
	  }
	}
  }

  //echo "\nAPRES : ".var_export($rub, 1);

  if($modifie) {
    foreach($recode as $num => $line) {
	  $code[$num]= $recode[$num];
	}
	rename($fichier, $fichier.'.orig');
	$f= fopen($fichier, 'w');
	fwrite($f, join('', $code));
	fclose($f);
  }
}
//var_export($rubriques);

// on va maintenant parcourir tous les appels de fonctions, pour les
// lister dans leurs articles respectifs
echo "appels : ".count($appels)."\n";
//var_dump($fonctions);
//var_dump($appels);
foreach($appels as $fonction => $appelants) {
	if($id=$fonctions[$fonction]) {
		$extra= array('appelants' => join(' ', array_keys($appelants)));
		sql_updateq('spip_articles',array("extra"=>serialize($extra)),"id_article=".$id);
	}
}

/**
 * Creation d'un article avec les details specifie
 * SI l'article n'existe pas deja, sinon, il faut juste mettre a jour
 * les bouts necessaires, en essayant de rien casser
 */
//** http://doc.spip.org/@creerArticle **
function creerArticle($detail) {
  global $secteur, $id_auteur;
  // recuperer l'eventuel article existant 
  echo "\n\n******************\nfonction ". $detail['titre']."\n";
  $l = sql_fetsel(
               array("id_article", "soustitre", "ps", "descriptif", "statut"),
			   array("spip_articles"),
   			   array("titre='".$detail['titre']."'")  
  );
    
  if($l) {
	$id= $l['id_article'];
	$soustitre= $l['soustitre'];
	$ps= $l['ps'];
	$statut= $l['statut'];

	$todo=array();

	// s'il existe, on le passe a 'publie'
	if($statut!='publie') {
	  //echo "**** Validation article ".$detail['titre']." sous "
	  //	.$detail['rubrique']."\n";
	  $todo["statut"]= 'publie';
	}

	// puis, on verifie que les champs  contiennent ce qu'on veut
    $soustitre= $detail['soustitre'];
	if($l['soustitre']!=$soustitre) {
	  $todo["soustitre"]= $soustitre;
	}

	if(!strstr($ps, $detail['fichier'])) {
	  $ps= $detail['fichier']."\n$ps";
	  $todo["ps"]= $ps;
	}

	$desc= $detail['descriptif'];
	if($l['descriptif']!=$desc) {
	  $todo["descriptif"]= $desc;
	}

	// si necessaire, faire l'update
	if(count($todo)!=0) {
	    echo "update à faire";
	    var_export($todo);
	    sql_updateq("spip_articles",$todo,"id_article=".$id);
	}
  //pas de select donc insertion
  } else {
		// sinon, creer le fichier
		$ps= $detail['fichier'];
		echo "**** Creation article ".$detail['titre']." sous "
		  .$detail['rubrique']."\n";
	
		// TODO : ajouter un html_escape ou un truc du genre ?????
		$id = sql_insertq("spip_articles",
		    array("id_rubrique" => $detail['rubrique'],
		            "titre" => $detail['titre'],
		            "surtitre" => $detail['surtitre'],
		            "soustitre" => $detail['soustitre'],
		            "descriptif" => $detail['titre'],
		            "ps" => $ps,
		            "date" => "now()",
		            "statut" => "publie",
		            "id_secteur" => $secteur,
		            "langue_choisie" => "non",
		            "url_propre" => $detail['url_propre']
		    )
		);
	
		// et lui coller un auteur
		sql_insertq("spip_auteurs_articles",
	        	    array(
	        	        "id_auteur" => $id_auteur,
	        	        "id_article" => $id
	        	    )
		);
  }

  return $id;
}

/**
 * Reconstitution d'une branche de l'arbo
 */
function &reconstituer($idBranche) {
	global $rubriques, $enfants, $articles;

	$res= array('.' => $idBranche);
	// on liste les rubriques enfants de la branche id
	if(isset($enfants[$idBranche])) {
		foreach($enfants[$idBranche] as $id => $nom) {
			$res["R $nom"]= reconstituer($id);
		}
	}
	if(isset($articles[$idBranche])) {
		foreach($articles[$idBranche] as $id => $titre) {
			$res["A $titre"]= $id;
		}
	}
	return $res;
}

/**
 * Creation d'une rubrique
 */
//** http://doc.spip.org/@creerRubrique **
function creerRubrique($nom, $fonction, $parent, $avant) {
  global $secteur;
  echo "**** Creation rubrique $type/$nom sous $parent";
  
  $id = sql_insertq(
                    "spip_rubriques",
                    array(
                        "id_parent" => $parent,
                        "titre" => $nom,
                        "descriptif" => $fonction,
                        "id_secteur" => $secteur,
                        "langue_choisie" => 'non',
                        "statut" => 'publie',
                        "url_propre" => $nom
                    )
  );
  
  return $id;
}

/**
 * retourne d'id de la rubrique code/x/y/z, apr�s avoir cree les
 * rubriques manquantes si necessaire
 */
//** http://doc.spip.org/@creerArbo **
function &creerArbo($demande, &$existe, $avant= "code") {
  $premier= array_shift($demande);

  // s'il existe, on cherche le descendant
  if(array_key_exists("R $premier", $existe)) {
	if(count($demande)!=0) {
	  return creerArbo($demande, $existe["R $premier"], "$avant/$premier");
	} else {
	  return $existe["R $premier"];
	}
  } else {
	// sinon, on met de cote de dernier element
	array_unshift($demande, $premier);
	$fichier= array_pop($demande);
	// on cree la liste de rubriques associee aux repertoires
	$d= &$existe;
	foreach($demande as $rep) {
	  $idr= creerRubrique($rep, 'repertoire', $d['.'], $avant);
	  $avant.="/$rep";
	  $d["R $rep"]= array('.' => $idr);
	  $d= &$d["R $rep"];
	}
	// puis celle du fichier final
	$idr= creerRubrique($fichier, 'fichier', $d['.'], $avant);
	$d["R $fichier"]= array('.' => $idr);
	return $d["R $fichier"];
  }
}

?>
