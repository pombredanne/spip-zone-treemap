#!/bin/bash

ici=$(cd $(dirname $0) ; /bin/pwd)

force=0

# recup des arguments site et svn
while getopts fs:r: OPT ; do
	case $OPT in
		f) force=1
		;;
		s) site=$OPTARG
		;;
		r) svn=$OPTARG
		;;
		*) echo "utilisation : $0 -s chemin_install_spip -r chemin_image_svn"
		exit 1
		;;
	esac
done
shift $(($OPTIND-1))

# verif (comment dit on a getopt qu'un arg est obligatoire ???)
if [ -z "$site" -o -z "$svn" ] ; then
	echo "utilisation : $0 [-f] -s chemin_install_spip -r chemin_image_svn"
	echo "argument -s et -r obligatoires"
	echo "-f pour faire la generation de doc, sans update svn"
	exit 1
fi

if [ $force -eq 0 ] ; then
	echo "=== lancement de svn update ==="
	cd $svn
	svn update > $ici/upd

	if [ $(cat $ici/upd | wc -l) -eq 1 ] ; then
		echo "pas de changements => on arrete la"
		exit 0
	fi
	version=$(tail -1 $ici/upd|perl -pi -e 's/.*?(\d+).*/$1/')
else
	version=$(cd $svn;svn info .|grep Revision|cut -d ' ' -f 2)
fi

echo "Version svn $version"

echo "=== execution du batch d'autodoc ==="
cd $site
php -c $ici $ici/genererLiensDoc.php \
	-spip $site -src $svn "$@" > $ici/log 2> $ici/log2

# la sortie d'erreur ne doit rien contenir de special, a part 
if ! diff -q $ici/log2 $ici/log2.reference ; then
	echo "log2 pas normal => on arrete la"
	echo "=== STDOUT ==="
	cat $ici/log
	echo "=== STDERR ==="
	cat $ici/log2
	exit 1
fi

cat $ici/log

echo "=== test des changements dans l'image svn ==="
cd $svn
svn status  | grep -v '^\?' > $ici/sta

if [ ! -s $ici/sta ] ; then
	echo "pas de changements dans le svn => on arrete la"
	exit 0
fi

# arrive' la, il faudrait faire un commit, mais on va rester prudent
# => on regarde de pres l'allure des modifs
svn diff | grep -v "^ " > $ici/diff
if [ -n "$(egrep -v "^(====|--- |\+\+\+ |@@ |[-+] *// \http://doc.spip.org|Index)" $ici/diff)" ] ; then
	echo "contenu suspect dans le svn diff => on arrete la"
	cat $ici/diff
	exit 1
fi

echo "=== commit ==="
svn commit -m "autodoc" > $ici/commit
cat $ici/commit

echo "=== fin ==="
exit 0
