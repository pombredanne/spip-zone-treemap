<?php

function formulaires_valider_paquet_charger() {
	$valeurs = array();
	$champs = array('xml');
	foreach($champs as $_champ){
		$valeurs[$_champ] = _request($_champ);
	}

	return $valeurs;
}

function formulaires_valider_paquet_verifier(){
	$erreurs = array();
	$obligatoires = array('xml');
	foreach($obligatoires as $_obligatoire){
		if(!_request($_obligatoire)){
			$erreurs[$_obligatoire] = _T('info_obligatoire');
		}
	}

	return $erreurs;
}

function formulaires_valider_paquet_traiter(){
	// Recuperation des champs du formulaire
	$contenu_xml = _request('xml');

	// Creation du fichier a partir du contenu copie
	$dir = sous_repertoire(_DIR_TMP, "pluginspip");
	$fichier_xml = $dir . '/paquet_' . md5($contenu_xml) . '.xml';
	ecrire_fichier($fichier_xml, $contenu_xml);

	// Validation du fichier
	$traitement = 'validation_paquetxml';
 	$valider = charger_fonction('plugonet_traiter','inc');
 	list($erreurs, $duree) = $valider($traitement, array($fichier_xml));

	// Formatage et affichage des resultats
	// -- Message global sur la generation des fichiers : toujours ok aujourd'hui
	// -- Texte des resultats par fichier traite
 	$formater = charger_fonction('plugonet_formater','inc');
	$retour = array();
 	list($resume, $analyse) = $formater($traitement, $erreurs, $duree, false);
 	$retour['message_ok']['resume'] = '';
 	$retour['message_ok']['analyse'] = $analyse;
	$retour['editable'] = true;
	
	return $retour;
}

?>