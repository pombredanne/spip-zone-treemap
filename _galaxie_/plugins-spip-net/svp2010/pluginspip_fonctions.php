<?php

function categorie2couleur($categorie) {
	
	static $couleurs = array('auteur' => '1310b2',
							'communication' => 'acbd70',
							'date' => '471bb2',
							'divers' => '50699b', 
							'edition' => 'b22ba4', 
							'maintenance' => '09b2a3', 
							'multimedia' => 'de175f', 
							'navigation' => 'b26714', 
							'outil' => 'dfb811', 
							'performance' => '11b23c', 
							'statistique' => 'bd87c0', 
							'squelette' => '40dd5d', 
							'theme' => 'b6a71b', 
							'aucune' => '0a63b2');
	if (!$categorie
	OR ($categorie == 'defaut') 
	OR (!$couleur = $couleurs[$categorie]))
		$couleur = 'b9274d';

	return $couleur;
}


function image_titre_article ($titre) {
	include_spip("inc/filtres_images");

	$police = "HelveNeuMedCon.ttf";
	
	$titre = mb_strtoupper($titre);
	$titre = str_replace("&NBSP;", "&nbsp;", $titre);
	$titre = str_replace("&AMP;", "&", $titre);

	$titre = image_typo($titre, "couleur=888888", "police=$police", "taille=54px", "largeur=1000", "padding=14");
	$titre = image_reduire_par($titre, 2);
	
	$titre2 = $titre;
	$titre2 = image_gamma($titre2, -125);
	$titre2 = image_flou($titre2, 4);
	$titre2 = image_alpha($titre2, 60);
	$titre2 = image_aplatir($titre2, "png", "666666");

//	$masque = image_sepia("squelettes/masques/masque-titre.png", $couleur);
	
	$titre = image_masque($titre, "masques/masque-titre.png");
	
	$titre = image_masque($titre2, extraire_attribut($titre,"src"), "mode=normal", "top=1", "left=1");
	
	$titre = image_aplatir($titre, "gif", "ffffff");
	
	return $titre;

}

function image_petit_titre_article ($titre, $couleur='666666') {
	include_spip("inc/filtres_images");

	$titre = mb_strtoupper($titre);
	$titre = str_replace("&NBSP;", "&nbsp;", $titre);
	$titre = str_replace("&AMP;", "&", $titre);
	
	$titre = image_typo($titre, "couleur=888888", "police=HelveNeuHeaConObl.ttf", "taille=54px", "largeur=2000", "padding=14");
	$titre = image_reduire_par($titre, 5);
	
	$titre2 = $titre;
	$titre2 = image_gamma($titre2, -125);
	$titre2 = image_flou($titre2, 4);
	$titre2 = image_alpha($titre2, 60);
	$titre2 = image_aplatir($titre2, "png", $couleur);
	
//	$masque = image_sepia("squelettes/masques/masque-titre.png", $couleur);
	
	$titre = image_masque($titre, "masques/masque-titre.png");
	
	$titre = image_masque($titre2, extraire_attribut($titre,"src"), "mode=normal", "top=1", "left=1");
	
	$titre = image_aplatir($titre, "gif", $couleur);
	
	return $titre;

}

?>