<?php

// On force le mode d'utilisation de SVP a non runtime car on veut presenter tous les
// plugins contenus dans les depots quelque soit leur compatibilite spip
define('_SVP_MODE_RUNTIME', false);

// Liste des pages publiques d'objet supportees par le squelette (depot, plugin).
// Permet d'afficher le bouton voir en ligne dans la page d'edition de l'objet
define('_SVP_PAGES_OBJET_PUBLIQUES', 'depot:plugin');

// Taille des listes et pas de pagination de la page sommaire
define('_PLUGINSPIP_TAILLE_SELECTION_PLUGINS', 10);
define('_PLUGINSPIP_TAILLE_TOP_PLUGINS', 30);
define('_PLUGINSPIP_TAILLE_MAJ_PLUGINS', 30);
define('_PLUGINSPIP_PAS_TOP_PLUGINS', 10);
define('_PLUGINSPIP_PAS_MAJ_PLUGINS', 10);

// Branche SPIP stable
define('_PLUGINSPIP_BRANCHE_STABLE', '3.0');

// urls propres en minuscules
define ('_url_minuscules', 1);

?>
