<?php
/**
 * Infos plugin
 * Licence GPL3
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

function action_api_infos_plugin_dist(){

	$where = array();

	if ($l = _request('lien_doc'))
		$where = "pa.lien_doc LIKE" . sql_quote("$l%");

	if ($p = _request('prefixe'))
		$where = "pa.prefixe=" . sql_quote($p);

	include_spip("inc/plugin");
	$vtarget = array('2.0', '2.1', '3.0', '3.1');
	$res = array();
	if (count($where)){
		$paquets = sql_allfetsel('pa.prefixe,pa.version,pa.lien_doc,pa.logo,pa.nom_archive,pa.maj_archive,pa.compatibilite_spip,pl.categorie', 
		'spip_paquets as pa JOIN spip_plugins as pl ON pa.id_plugin=pl.id_plugin', $where, '', 'pa.version DESC');
		foreach ($paquets as $paquet){
			if ($paquet['nom_archive']){
			$prefixe = $paquet['prefixe'];
			foreach ($vtarget as $v){
				$vindex = "SPIP-$v";
				if (!isset($res['plugins'][$prefixe][$vindex])
					OR !$res['plugins'][$prefixe][$vindex]
				){
					if (
					     plugin_version_compatible($paquet['compatibilite_spip'],"$v.0")
					  OR plugin_version_compatible($paquet['compatibilite_spip'],"$v.99")
					){
						$pv = explode('.',$paquet['version']);
						$pv = array_map('intval',$pv);
						$pv = implode('.',$pv);
						$h = dirname($paquet['logo']);
						$h = ($h?$h:"http://files.spip.org/spip-zone");
						$res['plugins'][$prefixe][$vindex] = array(
							'version' => $pv,
							'lien_doc' => $paquet['lien_doc'],
							'logo' => $paquet['logo'],
							'archive' => $h.'/'.$paquet['nom_archive'],
							'date' => $paquet['maj_archive'],
							'categorie' => $paquet['categorie'],
						);
					}
					else
						$res['plugins'][$prefixe][$vindex] = false;
				}
			}
			}
		}

	}
	if (!$res){
		include_spip('inc/headers');
		http_status(404);
		echo "404 Not Found";
	}
	else {
		$res = json_encode($res);
		$content_type = "application/json";
		header("Content-type: $content_type; charest=utf-8");
		echo $res;
	}
}
