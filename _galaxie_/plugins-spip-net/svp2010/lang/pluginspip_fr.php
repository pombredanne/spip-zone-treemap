<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_galaxie_/plugins-spip-net/svp2010/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_xml' => 'Fichier XML',
	'bulle_aller_aide' => 'Consulter la charte et l\'aide de ce site',
	'bulle_aller_depot' => 'Afficher la liste des plugins du dépôt',
	'bulle_aller_depots' => 'Se rendre sur la page d\'accueil des dépôts',
	'bulle_aller_developpement' => 'Se rendre sur la zone de développement',
	'bulle_aller_plugins' => 'Se rendre sur la page d\'accueil des plugins',
	'bulle_aller_signalements' => 'Consulter ou rédiger un signalement',
	'bulle_aller_statistiques' => 'Se rendre sur la page des statistiques',
	'bulle_aller_telechargements' => 'Se rendre sur la page des téléchargements',
	'bulle_filtrer_par_categorie' => 'Filtrer sur la catégorie',
	'bulle_rechercher_plugin' => 'Lancer la recherche',
	'bulle_resumer_plugin' => 'Afficher un résumé du plugin',

	// C
	'categorie_toute' => 'Toutes les catégories',
	'compat_spip' => 'pour SPIP',

	// D
	'derniere_maj' => 'Mis à jour le',
	'derniere_version' => 'Dernière version',

	// E
	'explication_signalement_contact' => 'Si vous détectez une erreur dans l’affichage d\'un plugin ou dans le site lui-même vous avez la possibilité de laisser un message aux administrateurs. Pensez bien, suivant la nature du problème, à préciser la page et le plugin fautif.',

	// I
	'info_actualisation_depot_cron' => 'Les plugins des dépôts sont actualisés automatiquement toutes les @periode@ heure(s).',
	'info_aucun_depot_disponible' => 'Aucun dépôt disponible.',
	'info_aucun_paquet_disponible' => 'Aucun paquet disponible.',
	'info_aucun_plugin_disponible' => 'Aucun plugin disponible.',
	'info_aucun_plugin_disponible_version' => 'Aucun plugin disponible pour SPIP @version@, vous pouvez <a href="@url@">étendre la recherche à toutes les versions de SPIP</a>.',
	'info_aucun_prefixe_disponible' => 'Aucun préfixe disponible.',
	'info_aucune_compatibilite_spip' => 'non communiquée',
	'info_contenu_paquet' => 'Copiez-collez le contenu exact de votre paquet.xml dans la zone de saisie ci-dessous et lancez la validation.',
	'info_non_dispo' => 'Information non disponible',
	'info_page_non_autorisee' => 'Vous n\'êtes pas autorisé à consulter cette page',
	'info_rechercher_plugin' => 'Rechercher un plugin',
	'info_valider_paquet' => 'Cette page vous permet de valider formellement un fichier <code>paquet.xml</code> de description d\'un plugin. Si aucune erreur n\'est détectée alors votre <code>paquet.xml</code> est valide et peut être utilisé sans problème dans votre plugin. Dans le cas contraire, suivez les indications pour corriger les erreurs.',
	'intertitre_contenu_paquet' => 'Contenu de votre paquet.xml',
	'intertitre_paquets_contribution' => 'Les autres contributions',
	'intertitre_paquets_plugin' => 'Les plugins',
	'intertitre_resultat_paquet' => 'Résultat de la validation :',

	// L
	'label_archive' => 'Archive',
	'label_auteur' => 'Auteur',
	'label_copyright' => 'Copyright',
	'label_credit' => 'Crédits',
	'label_etat' => 'État',
	'label_gestionnaire' => 'Gestionnaire',
	'label_hebergement' => 'Hébergée par',
	'label_langue_reference' => 'Langue référence',
	'label_licence' => 'Licence',
	'label_maj' => 'Générée le',
	'label_module' => 'Module de langue',
	'label_nbr_sites' => 'Utilisé par',
	'label_necessite_librairies' => 'Nécessite les librairies',
	'label_necessite_plugins' => 'Nécessite les plugins',
	'label_taille' => 'Taille',
	'label_traductions' => 'Traductions',
	'label_tri' => 'Trier : ',
	'label_tri_maj' => 'par date de mise à jour',
	'label_tri_nbr' => 'par nombre d\'installations',
	'label_tri_nom' => 'par nom',
	'label_tri_points' => 'par pertinence',
	'label_tri_pop' => 'par popularité',
	'label_utilise_plugins' => 'Compatible avec',
	'lien_demo' => 'Démonstration',
	'lien_dev' => 'Developpement',
	'lien_documentation' => 'Documentation',
	'lien_sources' => 'Code source',

	// P
	'plugin_commits' => 'Dernières modifications',
	'plugin_forums' => 'Messages de forum',

	// T
	'titre_au_hasard' => 'Au hasard',
	'titre_bloc_pied_actualite' => 'Actualité des plugins',
	'titre_bloc_pied_utilisation' => 'Ce site sous SPIP @version@ utilise les plugins',
	'titre_editer_selection' => 'Editer la sélection',
	'titre_maj_plugins' => 'Mises à jour récentes',
	'titre_page_aide' => 'Aide',
	'titre_page_depots' => 'Dépôts',
	'titre_page_plugins' => 'Plugins',
	'titre_page_prefixes' => 'Préfixes',
	'titre_page_signalements' => 'Signaler une erreur',
	'titre_page_statistiques' => 'Statistiques',
	'titre_page_telechargements' => 'Téléchargements',
	'titre_page_valider_paquet' => 'Valider un paquet.xml',
	'titre_rss_plugins' => 'Flux des plugins',
	'titre_selection' => 'En vedette',
	'titre_top_plugins' => 'Les @nb@ plus utilisés',
	'toutes_versions_spip' => 'Toutes les versions'
);

?>
