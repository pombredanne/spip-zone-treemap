<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/pluginspip?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_xml' => 'XML file',
	'bulle_aller_aide' => 'Consult the chart and the help of this site',
	'bulle_aller_depot' => 'Display the list of plugins of the repository',
	'bulle_aller_depots' => 'Go to the homepage of the repositories',
	'bulle_aller_developpement' => 'Go to the development zone',
	'bulle_aller_plugins' => 'Go to the homepage of the plugins',
	'bulle_aller_signalements' => 'Consult or write a report',
	'bulle_aller_statistiques' => 'Go to the statistics page',
	'bulle_aller_telechargements' => 'Go to the downloads page',
	'bulle_filtrer_par_categorie' => 'Sort on category',
	'bulle_rechercher_plugin' => 'Launch the search',
	'bulle_resumer_plugin' => 'View a summary of the plugin',

	// C
	'categorie_toute' => 'All categories',
	'compat_spip' => 'for SPIP',

	// D
	'derniere_maj' => 'Updated on',
	'derniere_version' => 'Last version',

	// E
	'explication_signalement_contact' => 'If you detect an error in the display of a plugin or in the website itself, you can leave a message to the administrators. Depending on the nature of the problem, specify the page and the defect plugin.',

	// I
	'info_actualisation_depot_cron' => 'The plugins of the repositories are automatically updated every  @periode@ hour(s).',
	'info_aucun_depot_disponible' => 'No repository available.',
	'info_aucun_paquet_disponible' => 'No package available.',
	'info_aucun_plugin_disponible' => 'No plugin available.',
	'info_aucun_plugin_disponible_version' => 'No plugin available for SPIP @version@, you can <a href="@url@">extend the search to all versions of SPIP</a>.',
	'info_aucun_prefixe_disponible' => 'No prefix available.',
	'info_aucune_compatibilite_spip' => 'not provided',
	'info_contenu_paquet' => 'Copy & Paste the exact contents of your paquet.xml in the text box below and start the validation.',
	'info_non_dispo' => 'Information not available',
	'info_page_non_autorisee' => 'You are not allowed to consult this page',
	'info_rechercher_plugin' => 'Search for a plugin',
	'info_valider_paquet' => 'This page allows you to validate a <code> paquet.xml </ code> describing a plugin. If no errors are detected then your <code> paquet.xml </ code> is valid and can be used without problem in your plugin. Otherwise, follow the instructions to correct the errors.',
	'intertitre_contenu_paquet' => 'Content of your paquet.xml',
	'intertitre_paquets_contribution' => 'The other contributions',
	'intertitre_paquets_plugin' => 'The plugins',
	'intertitre_resultat_paquet' => 'Résult of the validation:',

	// L
	'label_archive' => 'Archive',
	'label_auteur' => 'Author',
	'label_copyright' => 'Copyright',
	'label_credit' => 'Credits',
	'label_etat' => 'State',
	'label_gestionnaire' => 'Managed by',
	'label_hebergement' => 'Hosted by',
	'label_langue_reference' => 'Reference language',
	'label_licence' => 'License',
	'label_maj' => 'Generated on',
	'label_module' => 'Language module',
	'label_nbr_sites' => 'Used by',
	'label_necessite_librairies' => 'Requires libraries',
	'label_necessite_plugins' => 'Requires plugins',
	'label_taille' => 'Size',
	'label_traductions' => 'Translations',
	'label_tri' => 'Sort: ',
	'label_tri_maj' => 'by date of update',
	'label_tri_nbr' => 'by number of installations',
	'label_tri_nom' => 'by name',
	'label_tri_points' => 'by relevance',
	'label_tri_pop' => 'by popularity',
	'label_utilise_plugins' => 'Compatible with',
	'lien_demo' => 'Demonstration',
	'lien_dev' => 'Development',
	'lien_documentation' => 'Documentation',
	'lien_sources' => 'Source code',

	// P
	'plugin_commits' => 'Recent changes',
	'plugin_forums' => 'Forum messages',

	// T
	'titre_au_hasard' => 'Randomly',
	'titre_bloc_pied_actualite' => 'News of the plugins',
	'titre_bloc_pied_utilisation' => 'This site under SPIP @version@ uses the plugins',
	'titre_editer_selection' => 'Edit the selection',
	'titre_maj_plugins' => 'Recent updates',
	'titre_page_aide' => 'Help',
	'titre_page_depots' => 'Repositories',
	'titre_page_plugins' => 'Plugins',
	'titre_page_prefixes' => 'Prefixes',
	'titre_page_signalements' => 'Report an error',
	'titre_page_statistiques' => 'Statistics',
	'titre_page_telechargements' => 'Downloads',
	'titre_page_valider_paquet' => 'Validate a paquet.xml',
	'titre_rss_plugins' => 'Feed of the plugins',
	'titre_selection' => 'Featured',
	'titre_top_plugins' => 'The @nb@ more used',
	'toutes_versions_spip' => 'All versions'
);

?>
