<?php

include_spip('inc/distant');


function formulaires_grmleu_charger_dist()
{
    $contexte = array(
        'nom' => '',
        'motdepasse' => ''

    );
    return $contexte;
}


function formulaires_grmleu_verifier_dist()
{
    $retour = array();

    if (!_request('nom')) {
        $retour['message_erreur'] = "Merci de renseigner un nom de sous-domaine par exmple toto pour toto.grml.eu";
    }
    if (!_request('motdepasse')) {
        $retour['message_erreur'] .= "<br/>Merci de renseigner le mot de passe magique";
    }

    if (!($retour['message_erreur'])) {
        if (_request('motdepasse') != "ecureuil") {
            $retour['message_erreur'] = "Desolé le mot de passe ne semble pas bon :99% de chance que tu sois un robot, 1% de chance que tu sois étourdi(e)";
        }
        else
        {

if (glob( _DIR_RACINE . $GLOBALS['mutualisation_dir'] .'/'. _request('nom') . '.grml.eu'))
{
                $retour['message_erreur'] = "Ah non pas possible, le site "._request('nom').".grml.eu  existe deja ";
            }
        }
    }
    return $retour;
}


function formulaires_grmleu_traiter_dist()
{
    $retour = array();
    $site = _request('nom');

    return array(
        "redirect" => "http://" . $site . ".grml.eu",
    );

}

?>
