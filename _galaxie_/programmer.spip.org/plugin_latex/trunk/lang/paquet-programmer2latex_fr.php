<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-programmer2latex
// Langue: fr
// Date: 08-01-2012 19:15:09
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// P
	'programmer2latex_description' => 'Sert à traduire programmer.spip.org en LaTeX. On fait une compilation LaTeX local.
		Pour la conversion des niveaux de titres :
			- Une section de lang = 1 livre (on choisit la langue du livre qu\'on fait créer)
			- Une partie (introduction, Ecriture des squelettes etc) =  \part
			- Un chapitre (boucles, balises etc)					 = \chapter
			- 1 article 											 = \section 
			- 1 intertitre (le cas échéant)							 = \subsection
			
		On verra pour les détails plus tard
		
		Pour compiler : xelatex -shell-escape
		
		Nécéssite pygmentize en locale afin d\'avoir la coloration code
		
		Reste aussi à voir : gestion de l\'indexation. À discuter avec Marcimat',
	'programmer2latex_slogan' => 'Permettre de convertir le squelette documentation du HTML vers le LaTeX',
);
?>