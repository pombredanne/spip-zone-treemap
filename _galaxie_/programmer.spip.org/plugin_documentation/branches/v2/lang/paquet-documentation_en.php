<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'documentation_description' => 'Documentation template',
	'documentation_nom' => 'Documentation template',
	'documentation_slogan' => 'Documentation template'
);

?>
