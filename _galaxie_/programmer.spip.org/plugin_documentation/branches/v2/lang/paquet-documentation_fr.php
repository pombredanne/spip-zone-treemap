<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_galaxie_/programmer.spip.org/plugin_documentation/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'documentation_description' => 'Squelette {{Documentation}}',
	'documentation_nom' => 'Squelette Documentation',
	'documentation_slogan' => 'Squelette Documentation'
);

?>
