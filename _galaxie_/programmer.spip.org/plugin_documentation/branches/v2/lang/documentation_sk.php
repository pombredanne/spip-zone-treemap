<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.org
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aller_index' => 'Register',
	'auteur' => 'Autor',

	// C
	'cfg_descriptif_documentation' => 'Možnosti šablóny dokumentácie',
	'cfg_titre_documentation' => 'Šablóna "Dokumentácia"',
	'champ_auteur' => 'Autor(i)',
	'champ_date' => 'Dátum',
	'champ_id' => 'ID',
	'champ_langue' => 'Jazyk',
	'champ_resume' => 'Zhrnutie',
	'champ_texte' => 'Text',
	'champ_titre' => 'Nadpis',
	'chapitre' => 'Kapitola: ',
	'choisir' => 'Vybrať...',
	'commentaire' => 'komentár',
	'commentaire_aucun' => 'Žiaden komentár',
	'commentaires' => 'komentáre',
	'commentez' => 'Okomentujte dokumentáciu',
	'conception_graphique' => 'Grafický dizajn',
	'conception_graphique_adaptee_par' => 'od',
	'conception_graphique_par' => 'Farebný motív upravený z',
	'creer_nouvelle_suggestion' => 'Poslať nový návrh',

	// D
	'description' => 'Popis',
	'documentation_papier' => 'Documentation papier !', # NEW
	'documentation_papier_complement' => 'Na čítanie vo voľnom čase...',

	// E
	'editer_suggestion' => 'Upraviť tento návrh',
	'en_savoir_plus' => 'Zistite viac!',
	'erreur_de_chargement_ajax' => 'Chyba pri nahrávaní AJAXU!',
	'erreur_inscription_desactivee' => 'Registrácie na túto stránku sú deaktivované.',
	'erreur_inscription_session' => 'Už ste sa prihlásili.',
	'exemple' => 'Príklad',
	'exercice' => 'Exercice', # NEW
	'explication_activer_formulaire_ecrire_auteur' => 'Zobraziť formulár na napísanie autorovi pre neznámym návštevníkom?',
	'explication_barre_menu_absente' => 'Zásuvný modul Menu umožňuje
		zobraziť panel a definovať jeho vstupy.
		Aktivujte možnosť, aby sa nezobrazil!',
	'explication_css_geshi' => 'Utiliser une CSS pour Geshi
		(Coloration de code) unique
		(et non une définition juste au dessus des codes) ?', # NEW
	'explication_description_suggestion' => '
		Signalez une coquille,
		proposez une reformulation,
		soufflez à l\'oreille un bug,
		bref, exprimez vous sur le site et son contenu !
	', # NEW
	'explication_interdire_recherche_tickets' => 'Ak zaškrtnete túto možnosť, lístky nebudú zobrazené vo vyhľadávači stránky.',
	'explication_navigation_ajax' => 'Afficher tous les articles et
		rubrique dans la colonne de navigation peut être coûteux
		en performance et volumineux en octets à envoyer si
		la documentation contient de nombreuses pages. Cette
		option permet de limiter l\'arborescence envoyée au
		secteur en cours de lecture, le reste pouvant alors
		être obtenu en AJAX au survol des autres secteurs.', # NEW
	'explication_taille_redimensionnement_image' => 'En fonction du thème choisi, la taille
		de redimensionnement des images peut être trop petite
		ou trop grande (par défaut 440px de large) par rapport à la largeur
		de la colonne du contenu. Choisissez une valeur la plus adaptée à votre thème graphique.', # NEW
	'explication_utiliser_champs_extras' => 'Označte polia pridané pomocou šablóny, ktoré nebudete chcieť používať.',

	// I
	'icones_par' => 'Upravené ikony farebného motívu',
	'index' => 'Register',
	'integrale' => 'Hotovo!',

	// L
	'label_activer_formulaire_ecrire_auteur' => 'Napísať autorovi',
	'label_avancement' => 'Percento dokončenia',
	'label_barre_menu_absente' => 'Odstrániť panel menu',
	'label_charger_url' => 'Rýchly prístup:',
	'label_css_geshi' => 'CSS z Geshi',
	'label_exemple' => 'Príklad',
	'label_exercice' => 'Exercice', # NEW
	'label_groupe_mot_index' => 'Skupina kľúčových slov na zaindexovanie',
	'label_interdire_recherche_tickets' => 'Nezobrazovať lístky vo vyhľadávaní',
	'label_navigation_ajax' => 'Navigácia cez AJAX',
	'label_reponse' => 'Reakcia',
	'label_secteur_langue' => 'Použiť sektor podľa jazyka?',
	'label_sepia_logo' => 'Farba sépie   ge!',
	'label_sepia_logo_nb' => 'Farba sépie n&b !',
	'label_sous_titre_sommaire' => 'Pod názvom stránky so zhrnutím',
	'label_taille_redimensionnement_image' => 'Maximálna šírka obrázkov',
	'label_titre_sommaire' => 'Názov stránky so zhrnutím',
	'label_utiliser_champs_extras' => 'Nepoužívať doplnkové polia',
	'label_version' => 'Verzia dokumentácie',
	'label_vue_chapitre' => 'Používať zobrazenie "kapitola"',
	'licence' => 'Licencia',
	'lien_sedna' => 'Stránky, ktoré sledujeme',

	// M
	'maj' => 'Úprava',
	'mentions_legales' => 'Informácie právneho charakteru',
	'mis_a_jour' => 'Aktualizovať',
	'mots_cles' => 'Kľúčové slová',

	// N
	'navigation_clavier' => 'Stránky môžete meniť
			pomocou
ľavej a pravej šípky na klávesnici!',
	'nom' => 'Názov/Meno',
	'nouvelle_suggestion' => 'Nový návrh',

	// P
	'partez_a_laventure' => 'Zúčastnite sa dobrodrúžstva!',
	'precedent' => 'Predchádzajúca',
	'proposer_suggestion' => 'Navrhnite vylepšenie!',
	'publie_le' => 'Publikovaný',

	// R
	'reponse' => 'Reakcia',

	// S
	'signaler_coquille' => 'Nahláste chybu.',
	'sinscrire' => 'Zaregistrovať sa',
	'sommaire' => 'Obsah',
	'sommaire_livre' => 'Zhrnutie',
	'sous_licence' => 'pod licenciou',
	'suggestion' => 'Návrh',
	'suggestions' => 'Návrhy',
	'suivant' => 'Ďalšia',
	'suivi' => 'Sledovať',
	'suivi_dernieres_modifications_articles' => 'Posledné zmeny článkov',
	'suivi_derniers_articles' => 'Najnovšie články',
	'suivi_derniers_articles_proposes' => 'Najnovšie odoslané články',
	'suivi_derniers_commentaires' => 'Najnovšie komentáre',
	'suivi_description' => 'Sledovať stránku...',
	'symboles' => 'Symboly',

	// T
	'table_des_matieres' => 'Obsah',
	'tickets_sur_inscription' => 'Lístky na písanie alebo komentáre sa poskytujú iba tým, ktorí sa prihlásili.',
	'titre_identification' => 'Identifikácia',
	'titre_inscription' => 'Registrácia',
	'tout_voir' => 'Zobraziť všetko',
	'traductions' => 'Preklady',

	// V
	'vue_en_chapitre' => 'kapitola',
	'vue_en_chapitre_explication' => 'Zobraziť po kapitolách',
	'vue_en_page' => 'stránka',
	'vue_en_page_explication' => 'Zobraziť po stránkach'
);

?>
