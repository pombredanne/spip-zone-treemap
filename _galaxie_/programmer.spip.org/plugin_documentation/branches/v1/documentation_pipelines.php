<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

function documentation_pre_boucle($boucle){
	// ARTICLES, RUBRIQUES : {par num titre, titre}
	if (in_array($boucle->type_requete, array('rubriques','articles'))
	AND !$boucle->order) {
		$boucle->select[] = "0+" . $boucle->id_table . ".titre AS autonum";
		$boucle->order[]  = "'autonum'";
		$boucle->order[]  = "'" . $boucle->id_table . ".titre'";
	}
	return $boucle;
}


function documentation_jquery_plugins($plugins){
	$plugins[] = "javascript/menu_documentation.js";
	$plugins[] = "javascript/documentation.js";
	return $plugins;
}

function documentation_insert_head($flux){
	// pour les petites roues ajax de .animeajax()
	$flux .= "
	<script type='text/javascript'><!--
	var ajax_image_searching = \"<img src='" . url_absolue(chemin_image("searching.gif")) . "' alt='' />\";
	--></script>
	";
	return $flux;
}

// valeurs par defaut sur les nouveaux tickets
function documentation_formulaire_charger($flux){
	if ($flux['args']['form'] == 'editer_ticket'
		and (!$flux['args']['args'][0] OR $flux['args']['args'][0] == 'oui') // nouveau ticket
		and !$flux['data']['type']) {
			$flux['data']['type'] = 2;
			$flux['data']['severite'] = 4;
	}
	return $flux;
}


// ajouter automatiquement des title "art30" sur les raccourcis [->art30]
// donc, transformer [->art30] en [|art30->art30]
// ce qui permet a l'integrale de gerer des numeros de pages sur les liens
function documentation_pre_liens($texte){	
	// uniquement dans le public
	if (test_espace_prive()) return $texte;

	$regs = $match = array();
	// pour chaque lien
	if (preg_match_all(_RACCOURCI_LIEN, $texte, $regs, PREG_SET_ORDER)) {	
		foreach ($regs as $reg) {
			// si le lien est de type raccourcis "art40"
			if (preg_match(_RACCOURCI_URL, $reg[4], $match)) {
				$title = '|' . $match[1] . $match[2];
				// s'il n'y a pas deja ce title
				if (false === strpos($reg[0], $title)) {
					$lien = substr_replace($reg[0], $title, strpos($reg[0], '->'), 0);
					$texte = str_replace($reg[0], $lien, $texte);
				}
			}
		}	
	}
	return $texte;
}

// transformer les <a title="art30" href="xxx">zzz</a>
// en <a href="#art30">zzz</a> (enfin <a title="art30" href="#art30">zzz</a> en attendant mieux)
// seulement dans le squelette integrale
// pour que le pdf puisse calculer les numero de page des liens
function documentation_affichage_final($page){
	// uniquement dans le public
	if (test_espace_prive()) return $page;
	
	if (_request('page') == 'integrale') {
		include_spip('inc/filtres');
		include_spip('inc/lien');
		$as = extraire_balises($page, 'a');
		foreach($as as $a) {
			if ($title = extraire_attribut($a, 'title')) {
				if (preg_match(_RACCOURCI_URL, $title, $match)) {
					$old_a = $a;
					// on laisse le title parce que a[href|="#"] ne semble pas pris en compte avec Prince...
					// $a = vider_attribut($a, 'title');
					$a = inserer_attribut($a, 'href', '#' . $title);
					$page = str_replace($old_a, $a, $page);
				}
			}
		}
		unset($as, $a, $old_a, $title);

		// INDEX
		// et dans la foulee : generer des liens d'index dans l'ordre de la page :
		// on cherche les <!-- index_moi:mot#ID_MOT -->
		if (preg_match_all('#<!-- index_moi:(mot\d+) -->#', $page, $regs, PREG_SET_ORDER)) {
			foreach ($regs as $reg) {
				$liens = array();
				$trouve = 'id="('.$reg[1].'([[:alpha:]][^"]+))"'; // au moins un caractere car si "mot10" on pourrait trouver des "mot103rub4"
				if (preg_match_all("#$trouve#", $page, $ids, PREG_SET_ORDER)) {
					foreach ($ids as $id) {
						$interessant = ($id[2][0] == 'r'); // vers une rubrique... loin d'etre ideal
						$lien = "<a href='#$id[1]'></a>";
						if ($interessant) $lien = '<strong>'.$lien.'</strong>';
						$liens[] = $lien;
					}
				}
				$liens = implode(', ', $liens);
				$page = str_replace("<!-- index_moi:$reg[1] -->", $liens, $page);
			}
		}
	}
	return $page;
}

?>
