(function($){
	$(document).ready(function(){

		/*
		 * Menu depliant de navigation
		 */
		var menu_a_ete_plie = false;
		function menu_documentation() {
			// fermer les ul qui n'ont pas a etre ouverts par defaut (meme en utilisant ajax_parallel_load)
			// mais ne pas toucher a ceux qui viennent d'etre integres en ajax par le menu
			setTimeout(function(){
			nouveaux = $('#navigation .menu li a, #navigation .menu li .titre').not('.do');
			if (nouveaux.size()) {
				if (!menu_a_ete_plie) {
					menu_a_ete_plie = true;
					$('#navigation .menu li:not(.on) ul').hide();
					$('#navigation .menu li:not(.on) ul').each(function(){
						$(this).parent('li').find('a').each(function(){
							if($(this).next().is('ul'))
								$(this).addClass('fermee');
						});
						
					});
					$('#navigation .menu li.on ul').each(function(){
						$(this).parent('li').find('a').each(function(){
							if($(this).next().is('ul'))
								$(this).addClass('ouverte');
						});
					});
				}
				nouveaux.addClass('do').hover(function(){

					var me=$(this);
					var time=400;
					// un temps plus long pour refermer !
					if (me.parent().find('>ul').is(':visible')) {
						me.addClass('ouverte');
						time=1000;
					}

					me.addClass('hop');
					setTimeout(function(){
						// verifier que la souris n'est pas deja partie !
						if (me.hasClass("hop")) {
							var parent = me.parent(); // parent = li
							// verifier que ce n'est pas une liste exposee
							if (!parent.hasClass('on')) {
								// fermer les ul
								var ul = parent.find('>ul');
								if (ul.is(':visible')) {
									ul.find('li:not(.on) ul').hide();
									ul.slideUp('fast');
									me.removeClass('ouverte');
									me.addClass('fermee');
								// ou ouvrir le premier
								} else {
									// selon la config de documentations
									// il faut peut etre charger en ajax le contenu
									// de la navigation, uniquement pour les ul
									// de premier niveau.
									if (ul.size()) {
										ul.slideDown('fast');
										me.addClass('ouverte');
										me.removeClass('fermee');
									} else {
										if (parent.is('.racines:not(.done)')) {
											parent.addClass('done');
											me.append('<span class="image_loading"></span>').animeajax();
											$.ajax({
												url: "spip.php?page=navigation_ajax",
												data: ({id_rubrique : me.attr("title").substr(3)}),
												success: function(html){
													parent.append(html);
													parent.find('li:not(.on) ul').hide();
													parent.find('.image_loading').remove();
													// appliquer le menu sur les nouveaux venus
													menu_documentation();
												},
											});
										}
									}
								}
							}
						}
					}, time);
				},function(){
					$(this).removeClass('hop');
				});
			}
			},500);
		}
		menu_documentation();
		onAjaxLoad(menu_documentation);

		/*
		 * Navigation precedent suivant
		 * avec les fleches du clavier
		 */
		$(document).keyup(function(e){
			// seulement si pas de crayon actif ni de focus sur un champs de saisie (commentaire)
			if ($('.crayon-active').length || $('input:focus,textarea:focus,select:focus').length) return;

			// fleche droite :
			if ((e.keyCode == 39) && (e.ctrlKey != true) && (e.metaKey != true)) {
				if (url = $('#contenu .prec_suiv .suivant a').attr('href')) {
					this.location.href = url;
				}
			}
			// fleche gauche :
			if ((e.keyCode == 37) && (e.ctrlKey != true) && (e.metaKey != true)) {
				if (url = $('#contenu .prec_suiv .precedent a').attr('href')) {
					this.location.href = url;
				}
			}
		});
	});
})(jQuery);
