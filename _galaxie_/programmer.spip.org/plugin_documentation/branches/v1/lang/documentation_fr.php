<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	//A
	'auteur' => 'Auteur',
	'aller_index' => 'Index',

	//C
	'champ_id' => 'Id',
	'champ_titre' => 'Titre',
	'champ_texte' => 'Texte',
	'champ_langue' => 'Lang',
	'champ_auteur' => 'Auteur(s)',
	'champ_date' => 'Date',
	'champ_resume' => 'Resum&eacute;',
	'choisir'=>'Choisir...',
	'conception_graphique_par'=>'Th&egrave;me graphique adapt&eacute; de ',
	'conception_graphique_adaptee_par'=>'par',
	'cfg_titre_documentation' => 'Squelette "Documentation"',
	'cfg_descriptif_documentation' => 'Options du squelette de documentation',
	'chapitre' => 'Chapitre : ',
	'conception_graphique' => 'Conception graphique',
	'commentaire' => 'commentaire',
	'commentaire_aucun' => 'Aucun commentaire',
	'commentaires' => 'commentaires',
	'commentez' => 'Commentez la documentation',
	'creer_nouvelle_suggestion' => 'Proposer une nouvelle suggestion',

	//D
	'description' => 'Description',
	'documentation_papier' => 'Documentation papier !',
	'documentation_papier_complement' => 'Pour lire à tête reposée...',

	//E
	'en_savoir_plus' => 'En savoir plus !',
	'editer_suggestion' => 'Editer cette suggestion',
	'erreur_de_chargement_ajax' => 'Erreur de chargement AJAX !',
	'erreur_inscription_desactivee' => 'Les inscriptions sont d&eacute;sactiv&eacute;es sur ce site.',
	'erreur_inscription_session' => 'Vous &ecirc;tes d&eacute;j&agrave; identifi&eacute;.',
	'explication_description_suggestion' => "
		Signalez une coquille,
		proposez une reformulation,
		soufflez &agrave; l'oreille un bug,
		bref, exprimez vous sur le site et son contenu !
	",
	'exemple' => 'Exemple',
	'exercice' => 'Exercice',
	'explication_activer_formulaire_ecrire_auteur' => 'Afficher le formulaire pour &eacute;crire &agrave; un auteur aux visiteurs non identifi&eacute;s ?',
	'explication_barre_menu_absente' => "Le plugin Menu permet
		d'afficher une barre d'onglet et de d&eacute;finir ses entr&eacute;es.
		Activer l'option pour ne pas l'afficher !",
	'explication_css_geshi' => "Utiliser une CSS pour Geshi
		(Coloration de code) unique
		(et non une définition juste au dessus des codes) ?",
	'explication_interdire_recherche_tickets' => 'Si cette option est cochée, les tickets ne seront pas affichés dans le moteur de recherche du site.',
	'explication_navigation_ajax' => "Afficher tous les articles et
		rubrique dans la colonne de navigation peut &ecirc;tre co&ucirc;teux
		en performance et volumineux en octets &agrave; envoyer si
		la documentation contient de nombreuses pages. Cette
		option permet de limiter l'arborescence envoy&eacute;e au
		secteur en cours de lecture, le reste pouvant alors
		&ecirc;tre obtenu en AJAX au survol des autres secteurs.",
	'explication_taille_redimensionnement_image' => 'En fonction du thème choisi, la taille
		de redimensionnement des images peut être trop petite
		ou trop grande (par défaut 440px de large) par rapport à la largeur
		de la colonne du contenu. Choisissez une valeur la plus adaptée à votre thème graphique.',
	'explication_utiliser_champs_extras' => 'Cochez les champs ajoutés par le squelette que vous ne souhaitez pas utiliser.',

	//I
	'integrale' => 'L\'int&eacute;grale !',
	'icones_par' => 'Icones adapt&eacute;es du th&egrave;me ',
	'index' => 'Index',

	//L
	'label_activer_formulaire_ecrire_auteur' => '&Eacute;crire &agrave; un auteur',
	'label_avancement' => 'Pourcentage r&eacute;alis&eacute;',
	'label_barre_menu_absente' => 'Enlever la barre de menu',
	'label_charger_url'=>'Acc&egrave;s rapide :',
	'label_css_geshi'=>'CSS de Geshi',
	'label_exemple' => 'Exemple',
	'label_exercice' => 'Exercice',
	'label_groupe_mot_index'=>"Groupe de mots pour l'index",
	'label_interdire_recherche_tickets' => 'Ne pas afficher les tickets dans la recherche',
	'label_navigation_ajax' => 'Navigation AJAX',
	'label_reponse' => 'Réponse',
	'label_secteur_langue' => 'Utiliser un secteur par langue ?',
	'label_sepia_logo' => 'Couleur de sepia-ge !',
	'label_sepia_logo_nb' => 'Couleur de sepia n&b !',
	'label_sous_titre_sommaire' => 'Sous titre de la page sommaire',
	'label_taille_redimensionnement_image' => 'Largeur maximale les images',
	'label_titre_sommaire' => 'Titre de la page sommaire',
	'label_utiliser_champs_extras' => 'Ne pas utiliser les champs supplémentaires',
	'label_version' => 'Version de la documentation',
	'label_vue_chapitre' => 'Utiliser la vue "chapitre"',
	'licence'=>"Licence",
	'lien_sedna' => 'Les sites que nous suivons',

	//M
	'maj' => 'R&eacute;vision du ',
	'mentions_legales' => 'Mentions l&eacute;gales',
	'mis_a_jour' => 'Mis &agrave; jour',
	'mots_cles' => 'Mots Cl&eacute;s',

	//N
	'navigation_clavier' => 'Vous pouvez tourner les pages
			avec les fl&egrave;ches gauche et droite de votre clavier !',
	'nouvelle_suggestion' => 'Nouvelle suggestion',
	'nom' => 'Nom',

	//P
	'partez_a_laventure' => 'Partez &agrave; l\'aventure !',
	'proposer_suggestion' => 'Proposez une am&eacute;lioration !',
	'publie_le' => 'Publi&eacute; le',
	'precedent' => 'Pr&eacute;c&eacute;dent',

	//R
	'reponse' => 'Réponse',
	
	//S
	'signaler_coquille' => 'Signalez une coquille…',
	'sinscrire' => "S'inscrire",
	'sommaire' => 'Contenu',
	'sommaire_livre' => 'Sommaire',
	'suivant' => 'Suivant',
	'suivi' => 'Suivi',
	'suivi_description' => 'Suivi du site...',
	'suivi_derniers_articles' => 'Derniers articles',
	'suivi_derniers_articles_proposes' => 'Derniers articles propos&eacute;s',
	'suivi_dernieres_modifications_articles' => 'Dernieres modifications des articles',
	'suivi_derniers_commentaires' => 'Derniers commentaires',
	'sous_licence' => 'sous licence',
	'suggestion' => 'Suggestion',
	'suggestions' => 'Suggestions',
	'symboles' => 'Symboles',

	//T
	'traductions' => 'Traductions',
	'table_des_matieres' => 'Table des matières',
	'tickets_sur_inscription' => "
		L'&eacute;criture des tickets ou commentaires n'est
		possible qu'aux personnes identifi&eacute;es.
	",
	'titre_identification' => 'Identification',
	'titre_inscription' => 'Inscription',
	'tout_voir' => 'Tout voir',

	// V
	'vue_en_page' => "page",
	'vue_en_page_explication' => "Visualiser par page",
	'vue_en_chapitre' => "chapitre",
	'vue_en_chapitre_explication' => "Visualiser par chapitre",
);
?>
