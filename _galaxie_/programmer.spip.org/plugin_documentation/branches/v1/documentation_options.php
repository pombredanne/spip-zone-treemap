<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS['table_des_traitements']['TITRE'][]= 'typo(supprimer_numero(%s))';

// definir un bloc 'zoom' supplementaire
$GLOBALS['z_blocs'] = array('contenu','navigation','extra','head','zoom');
// exemple de configuration de mes_options :
define('_Z_AJAX_PARALLEL_LOAD','navigation,extra,zoom');


//$GLOBALS['forcer_lang'] = true;
define('_URLS_PROPRES_MIN', 2); // pour fr/ et en/
define('PLUGIN_COLORATION_CODE_TAB_WIDTH', 4);

if (lire_config('documentation/css_geshi')) {
	define('PLUGIN_COLORATION_CODE_SANS_STYLES', true); // styles dans un fichier css specifique
} else {
	define('PLUGIN_COLORATION_CODE_STYLES_INLINE', false); // styles au dessus des blocs de codes.
}

// ne pas notifier tous les auteurs a la creation d'un ticket 
define('_TICKETS_AUTORISATION_NOTIFIER', '0minirezo');
define('LONGUEUR_MINI_COMMENTAIRES_TICKETS', 5); // un petit merci !

// saisies de SAISIES dans le plugin CE2
define('_CHAMPS_EXTRAS_SAISIES_EXTERNES', true);

// stocker la langue d'arrivee pour que le sommaire affiche la langue souhaitee
// et on ajoute la langue dans le contexte systematiquement.
if (!$langue = _request('lang')) {
	include_spip('inc/lang');
	$langues = explode(',', $GLOBALS['meta']['langues_multilingue']);
	$langue = utiliser_langue_visiteur();
	if (!in_array($langue, $langues)) {
		$langue = $GLOBALS['meta']['langue_site'];
	}
	set_request('lang', $langue);
}

// stocker la langue...
if ($langue != $_COOKIE['spip_lang']) {
	include_spip('inc/cookie');
	spip_setcookie('spip_lang', $langue);
}

// stocker la vue (voir en page, ou voir en chapitre) ...
if ((!$vue = _request('vue')) && (lire_config('documentation/vue_chapitre','on') == 'on')) {
	if (!$vue = $_COOKIE['documentation_vue']) { $vue = 'page';	}
	set_request('vue', $vue);
} 
if ((lire_config('documentation/vue_chapitre','on') == 'on') && in_array($vue, array('page','chapitre')) and $vue != $_COOKIE['documentation_vue']) {
	include_spip('inc/cookie');
	spip_setcookie('documentation_vue', $vue);
}


?>
