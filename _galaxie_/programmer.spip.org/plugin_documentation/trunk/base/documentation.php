<?php
if (!defined("_ECRIRE_INC_VERSION")) return;

function documentation_declarer_champs_extras($champs = array()) {
	include_spip('inc/config');
	$config_extras = lire_config('documentation/utiliser_champs_extras',array());
	if(!in_array('exemple',$config_extras)){
		$champs['spip_articles']['exemple'] = array(
			'saisie' => 'textarea', // type de saisie
			'options'=> array(
				'nom'	=> 'exemple',
				'label' => _T('documentation:label_exemple'), // chaine de langue 'prefix:cle'
				'sql' 	=> "text NOT NULL DEFAULT ''", // declaration sql
				'traitements' => _TRAITEMENT_RACCOURCIS, // _TRAITEMENT_TYPO
				'rechercher' => 2,
				'class' => "inserer_barre_edition inserer_previsualisation", // classes CSS
				'li_class' => "haut", // classes CSS
				)
		);
	}
	if(!in_array('exemple',$config_extras)){
		$champs['spip_articles']['exercice'] = array(
			'saisie' => 'textarea', // type de saisie
			'options'=> array(
				'nom'	=> 'exercice',
				'label' => _T('documentation:label_exercice'), // chaine de langue 'prefix:cle'
				'sql' 	=> "text NOT NULL DEFAULT ''", // declaration sql
				'traitements' => _TRAITEMENT_RACCOURCIS, // _TRAITEMENT_TYPO
				'rechercher' => 2,
				'class' => "inserer_barre_edition inserer_previsualisation", // classes CSS
				'li_class' => "haut", // classes CSS
				)
		);
	}

	if(!in_array('exemple',$config_extras)){
		$champs['spip_articles']['reponse'] = array(
			'saisie' => 'textarea', // type de saisie
			'options'=> array(
				'nom'	=> 'reponse',
				'label' => _T('documentation:label_reponse'), // chaine de langue 'prefix:cle'
				'sql' 	=> "text NOT NULL DEFAULT ''", // declaration sql
				'traitements' => _TRAITEMENT_RACCOURCIS, // _TRAITEMENT_TYPO
				'rechercher' => 2,
				'class' => "inserer_barre_edition inserer_previsualisation", // classes CSS
				'li_class' => "haut", // classes CSS
				)
		);
	}
	return $champs;
}
?>
