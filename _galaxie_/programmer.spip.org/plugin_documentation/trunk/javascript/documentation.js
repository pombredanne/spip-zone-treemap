(function($){
$(document).ready(function(){
	/* Affichage du formulaire */	
	if($('.page_article  #forum .ajoute_commentaire').length > 0){
		$('.page_article #formulaire_forum').hide();
	}
	$('.page_article  #forum .ajoute_commentaire').unbind('click').click(function(e){
		e.preventDefault();
		if(jQuery("#forum").children('#formulaire_forum').length == 0){
			var me = jQuery('#forum');
				jQuery('#formulaire_forum').siblings('p.repondre').show();
				jQuery('#formulaire_forum')
					.hide()
					.detach()
					.appendTo(jQuery(me))
					.slideDown('fast')
					.find('form')
					.removeClass('noajax')
					.find('input[name=id_forum]').val(0);
				jQuery('#formulaire_forum').find('form.preview,.reponse_formulaire').remove();
				jQuery(me).children('#formulaire_forum').last().positionner(true);
				var connect = jQuery('#formulaire_forum .saisie_session_nom a,#formulaire_forum .session_qui .details a').eq(0);
				if(connect.length!=0){
					var url = connect.attr('href').match(/url=([^&"']*)/);
					url = escape(unescape(url[1]).replace(/#.*/, "")+"#reply0");
					connect.attr('href',connect.attr('href').replace(/url=([^&"']*)/,"url="+url));
				}
		}
		else if ($('.page_article #formulaire_forum:hidden')){
			$('.page_article #formulaire_forum').fadeIn();
		}	
	});
	/* cacher les reponses sur l'espace public */
	$('.page_article .reponse')
		.toggleClass('close').find('>h3')
		.click(function(){
			$(this).parent().toggleClass('close').toggleClass('open');
		});
	/* reduire la taille des elements optionnels dans le formulaire d'article */
	$('.cadre-formulaire-editer .formulaire_editer_article')
		.find('.editer_ps, .editer_exemple, .editer_exercice, .editer_reponse').each(function(){
			if (!$(this).find('textarea').val()) {
				$(this).addClass('plier').find('label').click(function(){
					$(this).parent().toggleClass('deplier').toggleClass('plier');
				});
			}
		});
});
})(jQuery);
