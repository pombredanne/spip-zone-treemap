<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_galaxie_/programmer.spip.org/plugin_documentation/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auteur' => 'Auteur',

	// C
	'cfg_descriptif_documentation' => 'Options du squelette de documentation',
	'cfg_titre_documentation' => 'Squelette "Documentation"',
	'champ_auteur' => 'Auteur(s)',
	'champ_date' => 'Date',
	'champ_id' => 'Id',
	'champ_langue' => 'Lang',
	'champ_resume' => 'Resumé',
	'champ_texte' => 'Texte',
	'champ_titre' => 'Titre',
	'chapitre' => 'Chapitre : ',
	'choisir' => 'Choisir...',
	'commentaire' => 'commentaire',
	'commentaire_aucun' => 'Aucun commentaire',
	'commentaires' => 'commentaires',
	'commentez' => 'Commentez la documentation',
	'conception_graphique' => 'Conception graphique',
	'conception_graphique_adaptee_par' => 'par',
	'conception_graphique_de' => 'Thème graphique de ',
	'conception_graphique_par' => 'Thème graphique adapté de ',
	'creer_nouvelle_suggestion' => 'Proposer une nouvelle suggestion',

	// D
	'description' => 'Description',
	'documentation_papier' => 'Documentation papier !',
	'documentation_papier_complement' => 'Pour lire à tête reposée...',

	// E
	'editer_suggestion' => 'Editer cette suggestion',
	'en_savoir_plus' => 'En savoir plus !',
	'erreur_de_chargement_ajax' => 'Erreur de chargement AJAX !',
	'erreur_inscription_desactivee' => 'Les inscriptions sont désactivées sur ce site.',
	'erreur_inscription_session' => 'Vous êtes déjà identifié.',
	'exemple' => 'Exemple',
	'exercice' => 'Exercice',
	'explication_activer_formulaire_ecrire_auteur' => 'Afficher le formulaire pour écrire à un auteur aux visiteurs non identifiés ?',
	'explication_barre_menu_absente' => 'Le plugin Menu permet
		d\'afficher une barre d\'onglet et de définir ses entrées.
		Activer l\'option pour ne pas l\'afficher !',
	'explication_css_geshi' => 'Utiliser une CSS pour Geshi
		(Coloration de code) unique
		(et non une définition juste au dessus des codes) ?',
	'explication_description_suggestion' => '
		Signalez une coquille,
		proposez une reformulation,
		soufflez à l\'oreille un bug,
		bref, exprimez vous sur le site et son contenu !
	',
	'explication_interdire_recherche_tickets' => 'Si cette option est cochée, les tickets ne seront pas affichés dans le moteur de recherche du site.',
	'explication_navigation_ajax' => 'Afficher tous les articles et
		rubrique dans la colonne de navigation peut être coûteux
		en performance et volumineux en octets à envoyer si
		la documentation contient de nombreuses pages. Cette
		option permet de limiter l\'arborescence envoyée au
		secteur en cours de lecture, le reste pouvant alors
		être obtenu en AJAX au survol des autres secteurs.',
	'explication_taille_redimensionnement_image' => 'En fonction du thème choisi, la taille
		de redimensionnement des images peut être trop petite
		ou trop grande (par défaut 440px de large) par rapport à la largeur
		de la colonne du contenu. Choisissez une valeur la plus adaptée à votre thème graphique.',
	'explication_utiliser_champs_extras' => 'Cochez les champs ajoutés par le squelette que vous ne souhaitez pas utiliser.',

	// I
	'icones_par' => 'Icones adaptées du thème ',
	'integrale' => 'L\'intégrale !',

	// L
	'label_activer_formulaire_ecrire_auteur' => 'Écrire à un auteur',
	'label_avancement' => 'Pourcentage réalisé',
	'label_barre_menu_absente' => 'Enlever la barre de menu',
	'label_charger_url' => 'Accès rapide :',
	'label_css_geshi' => 'CSS de Geshi',
	'label_exemple' => 'Exemple',
	'label_exercice' => 'Exercice',
	'label_interdire_recherche_tickets' => 'Ne pas afficher les tickets dans la recherche',
	'label_navigation_ajax' => 'Navigation AJAX',
	'label_reponse' => 'Réponse',
	'label_secteur_langue' => 'Utiliser un secteur par langue ?',
	'label_sepia_logo' => 'Couleur de sepia-ge !',
	'label_sepia_logo_nb' => 'Couleur de sepia n&b !',
	'label_sous_titre_sommaire' => 'Sous titre de la page sommaire',
	'label_taille_redimensionnement_image' => 'Largeur maximale les images',
	'label_titre_sommaire' => 'Titre de la page sommaire',
	'label_utiliser_champs_extras' => 'Ne pas utiliser les champs supplémentaires',
	'label_version' => 'Version de la documentation',
	'licence' => 'Licence',
	'lien_sedna' => 'Les sites que nous suivons',

	// M
	'maj' => 'Révision du ',
	'mentions_legales' => 'Mentions légales',
	'mis_a_jour' => 'Mis à jour',
	'mots_cles' => 'Mots Clés',

	// N
	'navigation_clavier' => 'Vous pouvez tourner les pages
			avec les flèches gauche et droite de votre clavier !',
	'nom' => 'Nom',
	'nouvelle_suggestion' => 'Nouvelle suggestion',

	// P
	'partez_a_laventure' => 'Partez à l\'aventure !',
	'precedent' => 'Précédent',
	'proposer_suggestion' => 'Proposez une amélioration !',
	'publie_le' => 'Publié le',

	// R
	'reponse' => 'Réponse',

	// S
	'signaler_coquille' => 'Signalez une coquille…',
	'sinscrire' => 'S\'inscrire',
	'sommaire' => 'Contenu',
	'sommaire_livre' => 'Sommaire',
	'sous_licence' => 'sous licence',
	'suggestion' => 'Suggestion',
	'suggestions' => 'Suggestions',
	'suivant' => 'Suivant',
	'suivi' => 'Suivi',
	'suivi_dernieres_modifications_articles' => 'Dernieres modifications des articles',
	'suivi_derniers_articles' => 'Derniers articles',
	'suivi_derniers_articles_proposes' => 'Derniers articles proposés',
	'suivi_derniers_commentaires' => 'Derniers commentaires',
	'suivi_description' => 'Suivi du site...',
	'symboles' => 'Symboles',

	// T
	'table_des_matieres' => 'Table des matières',
	'tickets_sur_inscription' => '
		L\'écriture des tickets ou commentaires n\'est
		possible qu\'aux personnes identifiées.
	',
	'titre_identification' => 'Identification',
	'titre_inscription' => 'Inscription',
	'tout_voir' => 'Tout voir',
	'traductions' => 'Traductions'
);

?>
