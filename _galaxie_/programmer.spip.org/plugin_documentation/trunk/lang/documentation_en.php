<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/documentation?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auteur' => 'Author',

	// C
	'cfg_descriptif_documentation' => 'Options of the documentation template',
	'cfg_titre_documentation' => '"Documentation" template',
	'champ_auteur' => 'Author',
	'champ_date' => 'Date',
	'champ_id' => 'Id',
	'champ_langue' => 'Lang',
	'champ_resume' => 'Summary',
	'champ_texte' => 'Text',
	'champ_titre' => 'Title',
	'chapitre' => 'Chapter: ',
	'choisir' => 'Choose...',
	'commentaire' => 'comment',
	'commentaire_aucun' => 'No comments',
	'commentaires' => 'comments',
	'commentez' => 'Comment on the documentation',
	'conception_graphique' => 'Graphic design',
	'conception_graphique_adaptee_par' => 'by',
	'conception_graphique_de' => 'Graphical theme from ',
	'conception_graphique_par' => 'Theme based on ',
	'creer_nouvelle_suggestion' => 'Make a new suggestion',

	// D
	'description' => 'Description',
	'documentation_papier' => 'Printed documentation !',
	'documentation_papier_complement' => 'To read at leisure...',

	// E
	'editer_suggestion' => 'Edit this suggestion',
	'en_savoir_plus' => 'More',
	'erreur_de_chargement_ajax' => 'AJAX loading error!',
	'erreur_inscription_desactivee' => 'Registration are disabled on this site.',
	'erreur_inscription_session' => 'You are already identified.',
	'exemple' => 'Example',
	'exercice' => 'Exercise',
	'explication_activer_formulaire_ecrire_auteur' => 'Display the write-to-an-author form to anonymous visitors?',
	'explication_barre_menu_absente' => 'The Menu plugin allows
		to display a tab bar and define its inputs.
		Enable the option to not display it.',
	'explication_css_geshi' => 'Use a CSS file for Geshi
		unique (code colouring)
		(and not with definitions just above the codes)?',
	'explication_description_suggestion' => '
		Report an error, 
		propose a rewording, 
		tell us about a bug,
		in short, share your thoughts about the site or its content!
	',
	'explication_interdire_recherche_tickets' => 'If this checkbox is checked, the bug reports won\'t be displayed in the website search engine.',
	'explication_navigation_ajax' => 'Show all articles and section in the navigation column can be costly
		in performance and large in bytes to be sent if the documentation contains many pages. this
		option lets you limit the tree sent to the sector currently viewed, the rest can then
		be obtained by AJAX hovering other sectors.',
	'explication_taille_redimensionnement_image' => 'Depending on the chosen theme, the resized size
		of images may be too small or too high (default 440px wide) compared to the width
		content of the column. Choose the most adapted value for your grafical theme.',
	'explication_utiliser_champs_extras' => 'Check the fields added by the template that you do not want to use.',

	// I
	'icones_par' => 'Icons adapted from the theme ',
	'integrale' => 'Complete!',

	// L
	'label_activer_formulaire_ecrire_auteur' => 'Write to an author',
	'label_avancement' => 'Percentage complete',
	'label_barre_menu_absente' => 'Remove menu bar',
	'label_charger_url' => 'Quick access:',
	'label_css_geshi' => 'CSS by Geshi',
	'label_exemple' => 'Example',
	'label_exercice' => 'Exercise',
	'label_interdire_recherche_tickets' => 'Do not display the bug reports in the search engine',
	'label_navigation_ajax' => 'AJAX Navigation',
	'label_reponse' => 'Answer',
	'label_secteur_langue' => 'Use one sector per language?',
	'label_sepia_logo' => 'Sepia colour !',
	'label_sepia_logo_nb' => 'Sepia colour b&w !',
	'label_sous_titre_sommaire' => 'Subtitle of the contents page',
	'label_taille_redimensionnement_image' => 'Maximum width of images',
	'label_titre_sommaire' => 'Title of the contents page',
	'label_utiliser_champs_extras' => 'Do not use the additional fields',
	'label_version' => 'Documentation version',
	'licence' => 'License',
	'lien_sedna' => 'Websites we follow',

	// M
	'maj' => 'Updated on ',
	'mentions_legales' => 'Legal notices',
	'mis_a_jour' => 'Updated',
	'mots_cles' => 'Keywords',

	// N
	'navigation_clavier' => 'You can turn the pages using the keyboard\'s left and right arrow keys!',
	'nom' => 'Name',
	'nouvelle_suggestion' => 'New suggestion',

	// P
	'partez_a_laventure' => 'Begin the adventure!',
	'precedent' => 'Previous',
	'proposer_suggestion' => 'Suggest an improvement!',
	'publie_le' => 'Published',

	// R
	'reponse' => 'Answer',

	// S
	'signaler_coquille' => 'Report an error…',
	'sinscrire' => 'Register',
	'sommaire' => 'Contents',
	'sommaire_livre' => 'Contents',
	'sous_licence' => 'under the',
	'suggestion' => 'Suggestion',
	'suggestions' => 'Suggestions',
	'suivant' => 'Next',
	'suivi' => 'Monitor',
	'suivi_dernieres_modifications_articles' => 'Recent modifications to the articles',
	'suivi_derniers_articles' => 'Recent articles',
	'suivi_derniers_articles_proposes' => 'Last articles proposed',
	'suivi_derniers_commentaires' => 'Recent comments',
	'suivi_description' => 'Monitor the site...',
	'symboles' => 'Symbols',

	// T
	'table_des_matieres' => 'Table of contents',
	'tickets_sur_inscription' => '
		Writing tickets and comments is only available
		for registered users.
	',
	'titre_identification' => 'Identification',
	'titre_inscription' => 'Registration',
	'tout_voir' => 'View all',
	'traductions' => 'Translations'
);

?>
