<?php

define('_NOM_FICHIER_PDF', 'documentation');
define('_NOM_DOCUMENTATION_PDF', 'documentation_pdf');
define('_DIR_DOCUMENTATION_PDF', _DIR_IMG . _NOM_DOCUMENTATION_PDF . '/');

define('_BIN_PRINCE', '/usr/local/bin/prince'); // ubuntu 9.10
#define('_BIN_PRINCE', '/usr/bin/prince'); // ubuntu 9.4

define('_SKEL_DOCUMENTATION', 'integrale');
define('_DOCUMENTATION_PDF_ECHO', false);


function docpdf_log($texte) {
	if (!_DOCUMENTATION_PDF_ECHO) {
		spip_log($texte, 'pdf');
	} else {
		print_r($texte);
		echo "\n<br />\n";
	}
}

/**
 * Retourne un tableau des differents formats
 * de PDF a generer. Ces formats correspondent
 * a des noms de CSS existantes dans le répertoire css/
 * sans l'extension, tel que "a4".
 *
 * @return array la liste des formats
**/
function formats_pdf() {
	$pdf_todo = array(
		"a4", "a4nb",
		"a5",  "a5nb",
	#	"17x22", "17x22nb",
	#	"b5", "b5nb",
	);
	return $pdf_todo;
}


function genie_generer_documentation_pdf_dist($t) {
	docpdf_log('Genie documentation pdf en marche !');

	sous_repertoire(_DIR_DOCUMENTATION_PDF);
	
	foreach (formats_pdf() as $nom) {
		generer_les_pdf(_DIR_DOCUMENTATION_PDF . $nom . '/' , $nom, false);
	}
	
	// chapitres
	# generer_les_pdf(_DIR_DOCUMENTATION_PDF . 'chapitres/');
	
	return true;
}


function generer_les_pdf($dir, $format='', $generer_chapitres = true) {
	// creer le repertoire d'accueil
	sous_repertoire($dir);
	
	// supprimer les anciens fichiers
	docpdf_log('Suppression des pdf !');
	foreach (preg_files($dir, '.*\.pdf') as $fichier) {
		supprimer_fichier($fichier);
	}
	
	// appeler le Grand Prince a la rescousse
	include_spip('lib/prince/prince');

	// lister les langues, si utile (1 secteur par langue) :
	if (lire_config('documentation/secteur_langue') == 'on') {
		docpdf_log('Creation des pdf par secteur de langue.');
		// selection des secteurs de langue
		$secteurs = sql_allfetsel(array('lang', 'id_rubrique', 'date'), 'spip_rubriques', array(
			'id_parent = ' . sql_quote(0),
			'statut = ' . sql_quote('publie')));
		foreach ($secteurs as $secteur) {
			$date_max = sql_getfetsel('date_modif', 'spip_articles', array('id_secteur='.sql_quote($secteur['id_rubrique']), 'statut=' . sql_quote('publie')), '', 'date_modif DESC', '1');
			$date_max = max($secteur['date'], $date_max); 
			generer_chapitres_et_livre_pdf($dir, $format, $generer_chapitres, $secteur['id_rubrique'], $secteur['lang'], $date_max);
		}
	} else {
		// juste les secteurs
		docpdf_log('Creation des pdf (racine).');
		generer_chapitres_et_livre_pdf($dir, $format, $generer_chapitres);
	}
}



function generer_un_livre_pdf($id_secteur, $format) {
	$secteur = sql_fetsel(array('lang', 'id_rubrique', 'date'), 'spip_rubriques', array(
			'id_rubrique = ' . sql_quote($id_secteur),
			'statut = ' . sql_quote('publie')));
	if (!$secteur) {
		return false;
	}
	$date_max = sql_getfetsel('date_modif', 'spip_articles', array('id_secteur='.sql_quote($id_secteur), 'statut=' . sql_quote('publie')), '', 'date_modif DESC', '1');
	$date_max = max($secteur['date'], $date_max);
	$dir = _DIR_DOCUMENTATION_PDF . $format . '/';
	sous_repertoire(_DIR_DOCUMENTATION_PDF);
	sous_repertoire($dir);

	// appeler le Grand Prince a la rescousse
	include_spip('lib/prince/prince');
	
	generer_chapitres_et_livre_pdf($dir, $format, false, $id_secteur, $secteur['lang'], $date_max);
}



function generer_chapitres_et_livre_pdf($dir, $format='', $generer_chapitres=true, $id_rubrique=0, $lang='', $date = '', $multiples_quatre=false) {

	include_spip('inc/filtres_mini');
	$url_doc = url_absolue(generer_url_public(_SKEL_DOCUMENTATION));
	if ($format) {
		$url_doc = parametre_url($url_doc, 'format', $format, '&');
	}
	
	
	// on boucle sur les chapitres
	if ($generer_chapitres) {
		include_spip('inc/rubriques'); // calcul_branche_in
		$chapitres = sql_allfetsel(array('id_rubrique','date','0+titre AS autonum'), 'spip_rubriques', 'id_parent='.sql_quote($id_rubrique), '', 'autonum, titre');
		foreach ($chapitres as $num=>$chapitre) {
			// calcul de la date de derniere maj
			
			$date_max = sql_getfetsel('date_modif', 'spip_articles', array(sql_in('id_rubrique', calcul_branche_in($chapitre['id_rubrique'])), 'statut=' . sql_quote('publie')), '', 'date_modif DESC', '1');
			$date_max = max($chapitre['date'],$date_max);
			
			$fichier = $dir . _NOM_FICHIER_PDF;
			$url = $url_doc;
			if ($lang) {
				$url = parametre_url($url, 'lang', $lang, '&');
				$fichier .= '_' . $lang;
			}
			$url = parametre_url($url, 'id_rubrique', $chapitre['id_rubrique'], '&');
			$fichier .= '_chapitre_' . ($num + 1) . '.pdf';
			
			// on genere de pdf des chapitres
			docpdf_log('Chapitre ' . ($num + 1) . " (rubrique $chapitre[id_rubrique])");
			generer_documentation_pdf($url, $fichier, $date_max, $multiples_quatre);
		}
	}
	
	// on genere de pdf integral du secteur
	$fichier = $dir . _NOM_FICHIER_PDF;
	$url = $url_doc;
	if ($lang) {
		$url = parametre_url($url, 'lang', $lang, '&');
		$fichier .= '_' . $lang;
	}
	$fichier .= '.pdf';
	docpdf_log('Livre ' . ($lang ? " ($lang)" : ''));
	generer_documentation_pdf($url, $fichier, $date, $multiples_quatre);
}

function generer_documentation_pdf($url, $fichier, $date_modif, $multiples_quatre = false) {
		if (!$date) $date = time();
		docpdf_log("- Utilisation de : " . htmlentities($url));
		docpdf_log("- Creation de : $fichier, date : $date_modif");
		$err = array();
		$Prince = new Prince(_BIN_PRINCE);
		$Prince->convert_file_to_file($url, $fichier, $err);
		@touch($fichier, strtotime($date_modif));
		# if ($err) docpdf_log($err);

		// on a le fichier...
		// on cree un nouveau fichier pour les multiples de 4
		// ne pas boucler !
		if ($multiples_quatre) {
			// on compte le nombre de pages du pdf
			include_spip(_DIR_LIB_FPDF . 'fpdf');
			include_spip(_DIR_LIB_FPDI . 'fpdi');

			$pdf =& new FPDI();
			$pagecount = $pdf->setSourceFile($fichier);
			$reste = $pagecount % 4;
			docpdf_log("- Nombre de page : " . $pagecount . " (modulo 4 : $reste donc " . (4 - $reste) . " &agrave; ajouter)");
			if ($reste) {
				$url = parametre_url($url, 'ajouter_pages', 4-$reste, '&');
				$fichier = str_replace('.pdf', '+.pdf', $fichier);
				generer_documentation_pdf($url, $fichier, $date_modif, false);
			}
		}
			
}

?>
