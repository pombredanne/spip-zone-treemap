<?php
#$GLOBALS['theme_prive_defaut'] = 'simple';
#define('_INTERDIRE_COMPACTE_HEAD_ECRIRE', true);

// cache plus gros et plus long
$GLOBALS['quota_cache'] = 150;
define('_DUREE_CACHE_DEFAUT', 24*3600*30);

// lien vers le redmine...
// pour extensions code du plugin porte plume (pp_codes)
// redmine SVN (branche 2.1)
@define('_URL_BROWSER_TRAC',
	'http://core.spip.org/projects/spip/repository/entry/branches/spip-2.1/@file@');

// tris dans l'espace prive
define('_TRI_GROUPES_MOTS', 'multi');  # 0+titre,titre // multi
define('_TRI_ARTICLES_RUBRIQUE', '0+titre,titre');  # date DESC

error_reporting(E_ALL^E_NOTICE);
ini_set ("display_errors", "On");
define('SPIP_ERREUR_REPORT',E_ALL^E_NOTICE);
define('SPIP_ERREUR_REPORT_INCLUDE_PLUGINS',E_ALL^E_NOTICE);

?>
