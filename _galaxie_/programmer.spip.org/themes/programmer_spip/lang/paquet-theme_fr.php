<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-theme
// Langue: fr
// Date: 17-02-2012 19:25:19
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// T
	'theme_description' => 'Un thème pour le site Programmer SPIP.',
	'theme_slogan' => 'Un thème pour le site Programmer SPIP',
);
?>