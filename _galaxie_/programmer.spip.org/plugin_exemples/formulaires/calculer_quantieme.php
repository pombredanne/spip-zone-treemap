<?php

function formulaires_calculer_quantieme_charger_dist (){
	$valeurs = array(
		'date_jour' => '',
		'quantieme' => '', // le passer une fois calcule (pour exemple)
	);
	return $valeurs;
}

function formulaires_calculer_quantieme_verifier_dist (){
	$erreurs = array();
	foreach(array ('date_jour') as $obligatoire) {
		
		if (!_request($obligatoire)) $erreurs[$obligatoire] = _T('exemple_doc:champ_obligatoire');
	}
	
	if (count($erreurs))
		$erreurs['message_erreur'] = _T('exemple_doc:erreur_saisie');
		
	return $erreurs;
}

function formulaires_calculer_quantieme_traiter_dist (){

	$date_jour	 = _request('date_jour');
	$quantieme = calcule_quantieme($date_jour);

	if($quantieme !== false) { 
		set_request('quantieme', $quantieme); // on transmet le quantieme au prochain chargement... pour exemple. 
		return array(
			'editable' => true,
			'message_ok'=> _T('exempledoc:retour_calcul',array('date_jour'=>$date_jour, 'quantieme'=>$quantieme)),
		);
	} 
	else {
		return array(
			'editable' => true,
			'message_erreur'=> _T('exempledoc:retour_erreur'),
		);
	}
}

function calcule_quantieme($date_jour) {
	// ajouter 1
	list($jour, $mois, $annee) = explode('/', $date_jour);
	if ($time = mktime( 0, 0, 0, $mois, $jour, $annee)) {
		return date('z', $time)+1;
	}
	return false;
}


?>
