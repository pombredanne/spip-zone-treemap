<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'champ_obligatoire' => 'This field is mandatory',
	// E
	'erreur_calcul' => 'Error during operation !',
	'erreur_saisie' => 'The data you have entered holds errors.',
	// I
	'info_quantieme_calcule' => 'The day number : @quantieme@',
	// L
	'label_bouton_calculer' => 'Find',
	'label_date' => 'Date (dd/mm/yyyy) :',
	'langue_du_visiteur' => 'Your language:',
	// R
	'retour_calcul' => 'The day number for @date_jour@ is @quantieme@',
);
?>
