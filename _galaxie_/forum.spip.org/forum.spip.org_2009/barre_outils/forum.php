<?php
/*
 * Plugin Porte Plume pour SPIP 2
 * Licence GPL
 * Auteur Matthieu Marcillaud
 */
if (!defined("_ECRIRE_INC_VERSION")) return;



/**
 * Definition de la barre 'forum' pour markitup
 */
function barre_outils_forum(){
	// on modifie simplement la barre d'edition
	$edition = charger_fonction('edition','barre_outils');
	$barre = $edition();
	$barre->nameSpace = 'forum';
//	$barre->cacherTout();
	$barre->cacher(array(
		'header1',
		'liste_ul', 'liste_ol', 'indenter', 'desindenter',
		'notes',
		'uppercase', 'lowercase',
	));
	return $barre;
}


?>
