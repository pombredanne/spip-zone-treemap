<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_message_mot' => 'Ce mot-clé n\'est attaché à aucun message dans cette langue.', # NEW
	'aucune_reponse' => 'Pas de réponse', # NEW
	'avertissement_code_forum' => 'Pour insérer du code ou mettre en valeur vos solutions, vous pouvez utiliser les raccourcis typographiques suivants :<ul><li>&lt;code&gt;... une ou plusieurs lignes de code ...&lt;/code&gt;</li><li>&lt;cadre&gt;... code ayant des lignes très longues ...&lt;/cadre&gt;</li></ul>', # NEW
	'avertissementforum' => '<b>Rogad byin</b> Bann forom sitweb-la lé byin gayar. Ni di mersi zot tout po sak i fé roul ron tout bann zespas lantrèd-la.<p>Somansa, kank tout bann forom-la i grandi, lé pli difisil po swiv é lir tout bann kontribisyon. Akoz sa, é pou rann tout zot forompli gadyamb, ni domann azot prangard zot i swi byin indé rokomandasyon ni vé fé azot :<br><img src=\'puce.gif\' border=\'0\'> avan zot i mèt ansanm inn nouvo sijé pou kozé si bann forom, mersi zot gard si bann dalon la pa déza kozé isi-minm si bann soz-la ou vé abordé ;<br><img src=\'puce.gif\' border=\'0\'> véy byin zot i poz byin son késtyon dann la ribrik présiz pou lo sijé ou vé abord. ',
	'avertissementtitre' => 'Véy byin ou la donn inn titr prési é klèr pou zot késtyon.Akoz i va rann pli fasil la navigasyon tout bann zot vizitèr dann bann forom.',

	// B
	'barre_cadre_html' => 'Encadrer et colorer <cadre class=\'html4strict\'>du code html</cadre>', # NEW
	'barre_cadre_php' => 'Encadrer et colorer <cadre class=\'php\'>du code php</cadre>', # NEW
	'barre_cadre_spip' => 'Encadrer et colorer <cadre class=\'spip\'>du code spip</cadre>', # NEW
	'barre_code' => 'Insérer &lt;code&gt;du code&lt;/code&gt;', # NEW
	'barre_inserer_code' => 'Insérer, encadrer, colorer du code', # NEW
	'barre_quote' => 'Citer <quote>un message</quote>', # NEW

	// C
	'classer' => 'Classer', # NEW
	'clos' => 'Ce fil de discussion est clos', # NEW

	// D
	'deplacer_dans' => 'Déplacer dans', # NEW
	'derniers' => 'Derniers messages', # NEW
	'download' => 'Apiy tèrla po télésarz nout dèrnyèr vèrsyon',

	// F
	'forum_attention_explicite' => 'Ce titre n\'est pas assez explicite, veuillez le préciser :', # NEW
	'forum_invalide_titre' => 'Ce fil de messages a été invalidé', # NEW
	'forum_votre_email' => 'Votre adresse email (si vous souhaitez recevoir les réponses) :', # NEW

	// G
	'galaxie' => 'Dans la galaxie SPIP', # NEW

	// I
	'info_ajouter_document' => 'Vous pouvez joindre une capture d\'écran à votre message', # NEW
	'info_connexion' => 'Permet d\'éditer son message pendant une heure', # NEW
	'info_tag_forum' => 'Vous pouvez étiqueter cette page de forum avec les mots-clés qui vous semblent les plus appropriés ; ils permettront aux prochains visiteurs du site de mieux se repérer :', # NEW
	'interetquestion' => 'Di anou koman ou pans késtyon la lé itil pou zot',
	'interetreponse' => 'Di anou koman ou pans répons-la lé itil pou zot',
	'inutile' => 'inutile', # NEW

	// L
	'liens_utiles' => 'Liens utiles', # NEW
	'login_login2' => 'Login', # NEW

	// M
	'meme_sujet' => 'Sur le même sujet', # NEW
	'merci' => 'merci', # NEW
	'messages' => 'messages', # NEW

	// N
	'navigationrapide' => 'Navigation rapide :', # NEW
	'nouvellequestion' => 'Poz inn nouvèl késtyon',
	'nouvellereponse' => 'Réponn késtyon-la',

	// P
	'page_utile' => 'Cette page vous a-t-elle été:', # NEW
	'par_date' => 'par date', # NEW
	'par_interet' => 'par intérêt', # NEW
	'par_pertinence' => 'par pertinence', # NEW

	// Q
	'questions' => 'Bann késtyon',
	'quoideneuf' => 'Kosa la sanz bann dérnyé tan',

	// R
	'rechercher' => 'Pou rod',
	'rechercher_forums' => 'Rod andan bann forom',
	'rechercher_tout_site' => 'lo sit antyé',
	'reponses' => 'Bann répons',
	'resolu' => 'Résolu', # NEW
	'resolu_afficher' => 'Afficher en premier les messages liés au mot-clé « résolu »', # NEW
	'resolu_masquer' => 'Masquer les messages liés au mot-clé « résolu »', # NEW

	// S
	'suggestion' => 'Avant de continuer, avez-vous consulté les pages suivantes ? Elles contiennent peut-être la réponse que vous cherchez.', # NEW
	'suivi_thread' => 'Syndiquer ce fil de forum', # NEW

	// T
	'thememessage' => 'Lo tèm forom-la i abord :',
	'toutes_langues' => 'Dans toutes les langues', # NEW
	'traductions' => 'Bann tradiksyon pou lo téks-la :',

	// U
	'utile' => 'utile' # NEW
);

?>
