<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/forumspip?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_message_mot' => 'Aquesta paraula clau no està lligada a cap missatge en aquesta llengua.',
	'aucune_reponse' => 'Cap resposta',
	'avertissement_code_forum' => 'Per insertar codi o destacar les vostres solucions, podeu utilitzar les següents dreceres tipogràfiques:<ul><li>&lt;code&gt;... una o diverses línies de codi ...&lt;/code&gt;</li><li>&lt;cadre&gt;... codi que tingui línies molt llargues ...&lt;/cadre&gt;</li></ul>', # MODIF
	'avertissementforum' => '<b>N.B.</b> Els fòrums d\'aquest lloc son molt actius. Que tots aquells que animen i enriqueixen aquests espais d\'ajuda mútua rebin aquí el nostre agraïment.<p>Això no obstant, com més actius són els fòrums, més difícils esdevenen de seguir i consultar. Per a fer que aquests fòrums siguin més agradables, us agrairíem de seguir aquestes recomanacions:<br><img src=\'puce.gif\' border=\'0\'> abans d\'exposar un nou tema de discussió, verifiqueu si el tema no ha estat ja abordat aquí;<br><img src=\'puce.gif\' border=\'0\'>tingueu cura de fer la vostra pregunta dins la secció que li correspongui.',
	'avertissementtitre' => '<b>Tinga cura de donar <font color=\'red\'>un títol explícit per a la questió</font> per a facilitar la navegació d\'altres visitants dins dels forums. </b><p><font color=\'red\'>Els missatges amb títols no explícits son esborrats.</font>',

	// B
	'barre_cadre_html' => 'Emmarcar i acolorir <cadre class=\'html4strict\'>du code html</cadre>',
	'barre_cadre_php' => 'Emmarcar i acolorir <cadre class=\'php\'>du code php</cadre>',
	'barre_cadre_spip' => 'Emmarcar i acolorir <cadre class=\'spip\'>du code spip</cadre>',
	'barre_code' => 'Inserir <code>du code</code>',
	'barre_inserer_code' => 'Inserir, emmarcar, acolorir el codi',
	'barre_quote' => 'Citar <quote>un missatge</quote>',

	// C
	'classer' => 'Classificar',
	'clos' => 'Aquest fil de discussió està tancat',

	// D
	'deplacer_dans' => 'Déplacer dans', # NEW
	'derniere_connexion' => 'Dernière connexion :', # NEW
	'derniers' => 'Últims missatges',
	'download' => 'Descarregar la darrera versió',

	// F
	'facultatif' => 'facultatif', # NEW
	'faq' => 'FAQ', # NEW
	'faq_descriptif' => 'Sujets résolus les mieux notés par les visiteurs', # NEW
	'forum_attention_explicite' => 'Aquest títol no és prou explícit, preciseu-lo si-us-plau: ', # MODIF
	'forum_invalide_titre' => 'Aquest fil de missatges s\'ha invalidat',
	'forum_votre_email' => 'El vostre correu electrònic (si voleu rebre les respostes):', # MODIF

	// G
	'galaxie' => 'A dins de la galàxia SPIP',

	// I
	'info_ajouter_document' => 'Podeu afegir una captura de pantalla al vostre missatge',
	'info_connexion' => 'Us permet editar el vostre missatge durant una hora',
	'info_ecrire_auteur' => 'Vous devez être connecté-e pour envoyer un message privé :', # NEW
	'info_envoyer_message_prive' => 'permet d\'envoyer des messages privés aux contributeurs enregistrés', # NEW
	'info_tag_forum' => 'Podeu etiquetar aquesta pàgina del fòrum amb les paraules clau que us semblin més apropiades; permetran als futurs visitants del lloc orientar-se millor:', # MODIF
	'infos_stats_personnelles' => 'permet de consulter ses informations de connexion personnelles', # NEW
	'interetquestion' => 'Indiqueu l\'interés que us porta a aquesta qüestió',
	'interetreponse' => 'Indiqueu l\'interés que us porta a aquesta resposta',
	'inutile' => 'inútil',

	// L
	'liens_utiles' => 'Enllaços útils',
	'login_login2' => 'Login',

	// M
	'meme_sujet' => 'Amb el mateix subjecte',
	'merci' => 'gràcies',
	'messages' => 'missatges',
	'messages_auteur' => 'Messages de cet auteur :', # NEW
	'messages_connexion' => 'Messages depuis la dernière connexion :', # NEW

	// N
	'navigationrapide' => 'Navegació ràpida:', # MODIF
	'nb_sujets_forum' => 'Sujets', # NEW
	'nb_sujets_resolus' => 'Sujets résolus', # NEW
	'nouvellequestion' => 'Proposar una nova qüestió',
	'nouvellereponse' => 'Respondre a la qüestió',

	// P
	'page_utile' => 'Aquesta pàgina us ha semblat:',
	'par_date' => 'per data',
	'par_interet' => 'per interès',
	'par_pertinence' => 'per pertinença',

	// Q
	'questions' => 'Preguntes',
	'quoideneuf' => 'Modificacions recents',

	// R
	'rechercher' => 'Cercar',
	'rechercher_forums' => 'Buscar dins dels fòrums',
	'rechercher_tout_site' => 'tot el web',
	'reponses' => 'Respostes',
	'resolu' => 'Resolt',
	'resolu_afficher' => 'Mostrar primer els missatges lligat a la paraula clau «resolt»', # MODIF
	'resolu_masquer' => 'Amagar els missatges lligats a la paraula clau «resolt»', # MODIF

	// S
	'statut' => 'Statut :', # NEW
	'suggestion' => 'Abans de continuar, heu consultat les pàgines següents? Potser contenen la resposta que busqueu.',
	'suivi_thread' => 'Sindicar aquest fil del fòrum',
	'sujets_auteur' => 'Sujets de cet auteur :', # NEW

	// T
	'thememessage' => ' Tema d\'aquest fòrum :',
	'toutes_langues' => 'En totes les llengües',
	'traductions' => ' Traduccions d\'aquest text :',

	// U
	'utile' => 'útil'
);

?>
