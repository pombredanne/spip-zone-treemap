<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_message_mot' => 'Ce mot-cl&eacute; n\'est attach&eacute; &agrave; aucun message dans cette langue.', # NEW
	'aucune_reponse' => 'Pas de r&eacute;ponse', # NEW
	'avertissement_code_forum' => 'Para insertar c&oacute;digo o valorizar tus soluciones, puedes utilizar los atajos tipogr&aacute;ficos  siguientes:<ul><li>&lt;code&gt;... una o varias l&iacute;neas de c&oacute;digo...&lt;/code&gt;</li><li>&lt;cadre&gt;... c&oacute;digo que tiene l&iacute;neas muy largas ...&lt;/cadre&gt;</li></ul>',
	'avertissementforum' => '<b>OjO</b> Los foros de este sitio son muy activos. Ante todo, se agradece a quienes animan y enriquecen estos espacios de ayuda mutua. <p>No obstante, cuanto m&aacute;s activos son los foros, m&aacute;s son dificiles de seguir y de consultar. Para que se vuelvan m&aacute;s agradables te agradecemos sigas las siguientes recomendaciones:   <br><img src=\'puce.gif\' border=\'0\'> antes de lanzar un nuevo tema de discusi&oacute;n, verifica que no fu&eacute; abordado anteriormente;<br><img src=\'puce.gif\' border=\'0\'>vigila que haces tu pregunta en la secci&oacute;n que corresponde.',
	'avertissementtitre' => 'Cuida en darle un t&iacute;tulo expl&iacute;cito a tu pregunta, para luego facilitar la navegaci&oacute;n de los visitantes de los foros. ',

	// B
	'barre_cadre_html' => 'Encadrer et colorer &lt;cadre class=\'html4strict\'&gt;du code html&lt;/cadre&gt;', # NEW
	'barre_cadre_php' => 'Encadrer et colorer &lt;cadre class=\'php\'&gt;du code php&lt;/cadre&gt;', # NEW
	'barre_cadre_spip' => 'Encadrer et colorer &lt;cadre class=\'spip\'&gt;du code spip&lt;/cadre&gt;', # NEW
	'barre_code' => 'Insertar c&oacute;digo', # MODIF
	'barre_inserer_code' => 'Ins&eacute;rer, encadrer, colorer du code', # NEW
	'barre_quote' => 'Citer &lt;quote&gt;un message&lt;/quote&gt;', # NEW

	// C
	'classer' => 'Classer', # NEW
	'clos' => 'Ce fil de discussion est clos', # NEW

	// D
	'derniers' => 'Derniers messages', # NEW
	'download' => 'Descargar la &uacute;ltima versi&oacute;n',

	// F
	'forum_attention_explicite' => 'Ce titre n\'est pas assez explicite, veuillez le pr&eacute;ciser&nbsp;:', # NEW
	'forum_invalide_titre' => 'Ce fil de messages a &eacute;t&eacute; invalid&eacute;', # NEW
	'forum_votre_email' => 'Votre adresse email (si vous souhaitez recevoir les r&eacute;ponses)&nbsp;:', # NEW

	// G
	'galaxie' => 'Dans la galaxie SPIP', # NEW

	// I
	'info_ajouter_document' => 'Vous pouvez joindre une capture d\'&eacute;cran &agrave; votre message', # NEW
	'info_tag_forum' => 'Puedes etiquetar esta p&aacute;gina de foro con palabras claves que te parecen importantes; le permitir&aacute;n a las pr&oacute;ximos visitantes ubicarse mejor.', # MODIF
	'interetquestion' => 'Indica el inter&eacute;s que tiene para ti esta pregunta ',
	'interetreponse' => 'Indica el inter&eacute;s que tiene para ti esta respuesta ',
	'inutile' => 'in&uacute;til',

	// L
	'liens_utiles' => 'Liens utiles', # NEW
	'login_login2' => 'Login', # NEW

	// M
	'meme_sujet' => 'Sur le m&ecirc;me sujet', # NEW
	'merci' => 'gracias',
	'messages' => 'messages', # NEW

	// N
	'navigationrapide' => 'Navigation rapide&nbsp;:', # NEW
	'nouvellequestion' => 'Hacer una nueva pregunta',
	'nouvellereponse' => 'Responder a la pregunta',

	// P
	'page_utile' => 'Esta p&aacute;gina fue para t&iacute;:',
	'par_date' => 'par date', # NEW
	'par_interet' => 'par int&eacute;r&ecirc;t', # NEW
	'par_pertinence' => 'par pertinence', # NEW

	// Q
	'questions' => 'Preguntas',
	'quoideneuf' => 'Modificaciones recientes',

	// R
	'rechercher' => 'Buscar',
	'rechercher_forums' => 'Buscar en los foros',
	'rechercher_tout_site' => 'el sitio entero',
	'reponses' => 'Respuestas',
	'resolu' => 'R&eacute;solu', # NEW
	'resolu_afficher' => 'Afficher en premier les messages li&eacute;s au mot-cl&eacute; &laquo;&nbsp;r&eacute;solu&nbsp;&raquo;', # NEW
	'resolu_masquer' => 'Masquer les messages li&eacute;s au mot-cl&eacute; &laquo;&nbsp;r&eacute;solu&nbsp;&raquo;', # NEW

	// S
	'suggestion' => 'Avant de continuer, avez-vous consult&eacute; les pages suivantes&nbsp;? Elles contiennent peut-&ecirc;tre la r&eacute;ponse que vous cherchez.', # NEW
	'suivi_thread' => 'Syndiquer ce fil de forum', # NEW

	// T
	'thememessage' => 'Tema de este foro:',
	'toutes_langues' => 'Dans toutes les langues', # NEW
	'traductions' => 'Traducci&oacute;n de este texto:',

	// U
	'utile' => '&uacute;til'
);

?>
