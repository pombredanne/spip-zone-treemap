<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'avertissement_code_forum' => 'Um Code einzuf&uuml;gen oder Ihre L&ouml;sungen hervorzuheben, k&ouml;nnen Sie folgende K&uuml;rzel verwenden:<ul><li>&lt;code&gt;... eine oder mehrere Zeilen Code ...&lt;/code&gt;</li><li>&lt;cadre&gt;... Code mit sehr langen Zeilen ...&lt;/cadre&gt;</li></ul>',
'avertissementforum' => '<b>N.B.</b> Die Foren dieser Website sind sehr aktiv. Vielen Dank an alle, die sich hier engagieren und Hilfestellungen geben.<p>Bitte denken Sie daran, dass Forendurch eine grosse Zahl an Beitr&auml;gen leicht un&uuml;bersichtlich werden. Vielen Dank daf&uuml;r, dass Sie die folgenden Hinweis beachten:<br><img src=\'puce.gif\' border=\'0\'> Bevor Sie ein neues Thema beginnen, pr&uuml;fen Sie bitte, ob es nicht bereits behandelt worden ist.<br><img src=\'puce.gif\' border=\'0\'> Bitte achten Sie darauf, dass Sie Ihre Frage in der daf&uuml;r vorgesehenen Rubrik stellen.',
'avertissementtitre' => 'Bitte denken Sie daran, <font color=\'red\'>Ihrer Frage einen aussagekr&auml;ftigen Titel zu geben</font>, damit die anderen Leser sich leichter in den Foren orientieren k&ouml;nnen.<p><font color=\'red\'>Eintr&auml;ge ohne Titel werden gel&ouml;scht.</font>',


// B
'barre_cadre' => 'Text einrahmen',
'barre_code' => 'Code einf&uuml;gen',


// D
'download' => 'Download der neuesten Version ',


// I
'info_tag_forum' => 'Sie k&ouml;nnen diese Seite mit Schlagworten versehen. Damit unterst&uuml;tzen Sie die Orientierung Ihrer Leser.',
'interetquestion' => 'Bitte beschreiben Sie den Grund Ihrer Frage',
'interetreponse' => 'Bitte teilen Sie uns mit, ob die Antwort f&uuml;r Sie von Interesse war.',
'inutile' => 'Nicht hilfreich',


// M
'merci' => 'Danke',


// N
'nouvellequestion' => 'Neue Frage stellen',
'nouvellereponse' => 'Auf diese Frage antworten',


// P
'page_utile' => 'Sie fanden diese Seite:',


// Q
'questions' => 'Fragen',
'quoideneuf' => 'Neue Eintr&auml;ge',


// R
'rechercher' => 'Suchen',
'rechercher_forums' => 'In den Foren suchen',
'rechercher_tout_site' => 'Ganze Website durchsuchen',
'reponses' => 'Antworten',


// T
'thememessage' => 'Thema dieses Forums:',
'traductions' => '&Uuml;bersetzungen dieses Textes:',


// U
'utile' => 'Hilfreich'

);


?>
