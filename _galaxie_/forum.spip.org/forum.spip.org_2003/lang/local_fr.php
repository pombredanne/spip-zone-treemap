<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'avertissement_code_forum' => 'Pour ins&eacute;rer du code ou mettre en valeur vos solutions, vous pouvez utiliser les raccourcis typographiques suivants&nbsp;:<ul><li>&lt;code&gt;... une ou plusieurs lignes de code ...&lt;/code&gt;</li><li>&lt;cadre&gt;... code ayant des lignes tr&egrave;s longues ...&lt;/cadre&gt;</li></ul>',
'avertissementforum' => '<b>N.B.</b> Les forums de ce site sont tr&egrave;s actifs. Que tous ceux qui animent et enrichissent ces espaces d\'entraide soient ici remerci&eacute;s.<p>Cependant, plus les forums sont actifs, et plus ils deviennent difficiles &agrave; suivre et &agrave; consulter. Pour rendre ces forums plus agr&eacute;ables, nous vous remercions de suivre ces recommandations&nbsp;:<br><img src=\'puce.gif\' border=\'0\'> avant de lancer un nouveau sujet de discussion, merci de v&eacute;rifier que ce sujet n\'a pas d&eacute;j&agrave; &eacute;t&eacute; abord&eacute; ici&nbsp;;<br><img src=\'puce.gif\' border=\'0\'> prenez soin de poser votre question dans la rubrique qui lui est consacr&eacute;e.',
'avertissementtitre' => '<b>Prenez soin de donner <font color=\'red\'>un titre explicite &agrave; votre question</font> pour faciliter ensuite la navigation des autres visiteurs dans les forums.</b><p><font color=\'red\'>Les messages dont le titre n\'est pas explicite sont supprim&eacute;s.</font>',


// B
'barre_cadre' => 'Encadrer du texte',
'barre_code' => 'Ins&eacute;rer du code',


// D
'download' => 'T&eacute;l&eacute;charger la derni&egrave;re version',

// F
'forum_votre_email' => 'Votre adresse email (si vous souhaitez recevoir les r&eacute;ponses)&nbsp;:',

// I
'info_tag_forum' => 'Vous pouvez &eacute;tiqueter cette page de forum avec des mots-cl&eacute;s qui vous semblent importants ; ils permettront aux prochains visiteurs du site de mieux se rep&eacute;rer.',
'interetquestion' => 'Indiquez l\'int&eacute;r&ecirc;t que vous portez &agrave; cette question',
'interetreponse' => 'Indiquez l\'int&eacute;r&ecirc;t que vous portez &agrave; cette r&eacute;ponse',
'inutile' => 'inutile',


// M
'merci' => 'merci',


// N
'nouvellequestion' => 'Poser une nouvelle question',
'nouvellereponse' => 'R&eacute;pondre &agrave; la question',


// P
'page_utile' => 'Cette page vous a-t-elle &eacute;t&eacute;:',


// Q
'questions' => 'Questions',
'quoideneuf' => 'Modifications r&eacute;centes',


// R
'rechercher' => 'Rechercher',
'rechercher_forums' => 'Rechercher dans les forums',
'rechercher_tout_site' => 'tout le site',
'reponses' => 'R&eacute;ponses',


// T
'thememessage' => 'Th&egrave;me de ce forum :',
'traductions' => 'Traductions de ce texte :',


// U
'utile' => 'utile'

);


?>
