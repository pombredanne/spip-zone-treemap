<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'avertissement_code_forum' => 'To insert some code or ... ou mettre en valeur vos solutions, vous pouvez utiliser les raccourcis typographiques suivants&nbsp;:<ul><li>&lt;code&gt;... une ou plusieurs lignes de code ...&lt;/code&gt;</li><li>&lt;cadre&gt;... code ayant des lignes tr&egrave;s longues ...&lt;/cadre&gt;</li></ul>',
'avertissementforum' => '<b>P.S.</b> The forums of this site are vey active. Many thanks to those who animate and enrich these areas of mutual assistance.<p>However, the more the forums are active, the more dificult to follow and to consult they become. To turn these forums into a really exhilarating experience, we would be grateful if you follow these recommendations:<br><img src=\'puce.gif\' border=\'0\'> before starting a new discussion topic, please make sure that the subject has not been discussed here earlier;<br><img src=\'puce.gif\' border=\'0\'> make sure you ask your question in the section dedicated to it.',
'avertissementtitre' => '<b>Make sure you give <font color=\'red\'>a clear title to your question</font> in order to help other visitors navigate the forums.</b><p><font color=\'red\'>Messages without clear titles are deleted.</font>',


// B
'barre_cadre' => 'To frame text',
'barre_code' => 'Insert some code',


// D
'download' => 'Download the latest version',


// I
'info_tag_forum' => 'You can tag this forum page with the keywords that you think are important. This will help future visitors find answers quickly.',
'interetquestion' => 'Please specify your interest in this question',
'interetreponse' => 'Please specify your interest in this answer',
'inutile' => 'useless',


// M
'merci' => 'thank you',


// N
'notermessages' => '<NEW><PLUS_UTILISE>Puedes participar a la vida de este foro, indicando para cada pregunta y respuesta, el inter&eacute;s suscitado por el tema. <p>Puedes evaluar por un lado el inter&eacute;s general de la discusi&oacute;n (parte superior de la p&aacute;gina), y por otro lado cada respuesta, lo que permitir&aacute; resaltar las respuestas que mejor responden al problema planteado.',
'nouvellequestion' => 'Ask a new question',
'nouvellereponse' => 'Answering the question',


// P
'page_utile' => 'Did you find this page:',


// Q
'questions' => 'Questions',
'quoideneuf' => 'Recent changes',


// R
'rechercher' => 'Search',
'rechercher_forums' => 'Search in forums',
'rechercher_tout_site' => 'the whole site',
'reponses' => 'Answers',


// T
'thememessage' => 'This forum\'s theme:',
'traductions' => 'Translations of this text:',


// U
'utile' => 'useful'

);


?>
