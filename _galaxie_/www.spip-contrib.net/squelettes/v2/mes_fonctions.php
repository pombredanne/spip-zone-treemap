<?php

define('SECTEUR_WIKI', 607);


/**
 * Critere {tout_voir} permet de deverouiller l'acces restreint sur une boucle
 *
 * @param unknown_type $idb
 * @param unknown_type $boucles
 * @param unknown_type $crit
 */
if (!function_exists('critere_tout_voir_dist')){
	function critere_tout_voir_dist($idb, &$boucles, $crit) {
		$boucle = &$boucles[$idb];
		$boucle->modificateur['tout_voir'] = true;
	}
}

// true quand on vient du wiki
// true dans l'espace prive
// true dans les crayons
// false dans les autres pages (publiques hors wiki)
function boucle_exclure_secteur() {
	return
	(
		_DIR_RESTREINT==''
		OR _request('action')
		OR (defined('RUBRIQUE_WIKI_OK') AND RUBRIQUE_WIKI_OK)
	)
		? '0'
		: SECTEUR_WIKI
	;
}

//
// <BOUCLE(ARTICLES)> sans le wiki (secteur 607)
//
function boucle_ARTICLES($id_boucle, &$boucles) {
	$boucle = &$boucles[$id_boucle];
	$id_table = $boucle->id_table;

	if (!$boucle->modificateur['tout_voir'])
		$boucle->where[] = array("'!='", "'$id_table.id_secteur'", 'sql_quote(boucle_exclure_secteur())');

	return boucle_ARTICLES_dist($id_boucle, $boucles);
}

//
// <BOUCLE(RUBRIQUES)> sans le wiki (secteur 607)
//
function boucle_RUBRIQUES($id_boucle, &$boucles) {
	$boucle = &$boucles[$id_boucle];
	$id_table = $boucle->id_table;

	if (!$boucle->modificateur['tout_voir'])
		$boucle->where[] = array("'!='", "'$id_table.id_secteur'", 'sql_quote(boucle_exclure_secteur())');

	return boucle_RUBRIQUES_dist($id_boucle, $boucles);
}


function date_rfc822($date_heure) {
  list($annee, $mois, $jour) = recup_date($date_heure);
  list($heures, $minutes, $secondes) = recup_heure($date_heure);
  $time = mktime($heures, $minutes, $secondes, $mois, $jour, $annee);
  return gmdate("D, d M Y H:i:s +0100", $time);
}



function dec2hex($v) {
return substr('00'.dechex($v), -2);
}


function age_style($date) {
 	
// $decal en secondes
$decal = date("U") - date("U", strtotime($date));
			 
// 3 jours = vieux
$decal = min(1.0, sqrt($decal/(3*24*3600)));
			 
// Quand $decal = 0, c'est tout neuf : couleur vive
// Quand $decal = 1, c'est vieux : bleu pale
$red = ceil(128+127*(1-$decal));
$blue = ceil(130+60*$decal);
$green = ceil(200+55*(1-$decal));
										 
$couleur = dec2hex($red).dec2hex($green).dec2hex($blue);
											 
return 'background-color: #'.$couleur.';';
}

if (!function_exists('generer_url_site')){
function generer_url_site($id){
	return generer_url_entite($id,'site');
}
}

function compter_visiteurs(){
	return count(preg_files(_DIR_TMP.'visites/','.'));
}


function ohloh_widgetize($bio,$id_auteur,$email){
	if (!$email OR !$id_auteur=intval($id_auteur)) return $bio;
	if (strpos($bio,"www.ohloh.net")!==FALSE) return $bio;
	$hash = md5($email);
	include_spip('inc/distant');
	$oh = recuperer_page("http://www.ohloh.net/accounts/$hash/widgets");
	if (!$oh
	 OR count($widgets = extraire_balises($oh,'textarea'))<5)
		$widget = "Pas de compte sur www.ohloh.net";
	else {
		$widget = preg_replace(',^<textarea[^>]*>,i','',$widgets[4]);
		$widget = preg_replace(',</textarea>$,i','',$widget);
		$widget = html_entity_decode($widget);
	}
	
	$bio .= $widget;
	// mettons a jour sauvagement !
	include_spip('abstract_sql');
	$bio_sql = sql_getfetsel('bio','spip_auteurs','id_auteur='.intval($id_auteur));
	sql_updateq('spip_auteurs',array('bio'=>$bio_sql."\n".$widget),'id_auteur='.intval($id_auteur));
	return $bio;
}
/*
function critere_compteur($idb, &$boucles, $crit){
	$boucle = &$boucles[$idb];
	
	$params = $crit->param;
	$table = array_shift($params);
	$table = $table[0]->texte;
	if(preg_match(',^(\w+)([<>=])([0-9]+)$,',$table,$r)){
		$table=$r[1];
		$op=$r[2];
		$op_val=$r[3];
	}
	$type = objet_type($table);
	$type_id = id_table_objet($type);
	$table_sql = table_objet_sql($type);
	
	$trouver_table = charger_fonction('trouver_table','base');
	$arrivee = array($table, $trouver_table($table, $boucle->sql_serveur));
	$depart = array($boucle->id_table,$trouver_table($boucle->id_table, $boucle->sql_serveur));

	if ($compt = calculer_jointure($boucle,$depart,$arrivee)){

		$boucle->select[]= "COUNT($compt.$type_id) AS compteur_$table";	
		if ($op)
			$boucle->having[]= array("'".$op."'", "'compteur_".$table."'",$op_val);
	}
}

function balise_COMPTEUR_dist($p) {
	$p->code = '';
	if (isset($p->param[0][1][0])
	AND $champ = ($p->param[0][1][0]->texte))
		return rindex_pile($p, "compteur_$champ", 'compteur');
  return $p;
}
*/

function sc_versions_compatibles($id,$type){
	$compatibles = array();
	
	static $versions = array(
	190=>'SPIP 1.9.0',
	191=>'SPIP 1.9.1',
	192=>'SPIP 1.9.2',
	200=>'SPIP 2.0.0',
	);
	static $versions_min_inc=array(
	12=>array(190,191,192,200), // toutes versions de spip...
	100=>array(190,191,192), // 1.9 toutes versions sans plus de precision...
	258=>array(190), // 1.9.0
	119=>array(191), // 1.9.1
	120=>array(192), // 1.9.2
	261=>array(200), // 2.0.0
	);
	static $versions_max_excl = array(
	260 => array(190,191,192,200),
	259 => array(200),
	);

	include_spip('base/abstract_sql');
	$id_objet = id_table_objet($type);
	$mot_min = array_map('reset',sql_allfetsel('id_mot',"spip_mots_".$type.'s',"$id_objet=".intval($id) . " AND " . sql_in('id_mot',array_keys($versions_min_inc))));
	foreach($mot_min as $mot)
		if (isset($versions_min_inc[$mot]))
			$compatibles = array_merge($compatibles,$versions_min_inc[$mot]);
	if (!count($compatibles))
		return "";
	$compatibles = array_unique($compatibles);

	$mot_max = array_map('reset',sql_allfetsel('id_mot',"spip_mots_".$type.'s',"$id_objet=".intval($id) . " AND " . sql_in('id_mot',array_keys($versions_max_excl))));
	foreach($mot_max as $mot)
		if (isset($versions_max_excl[$mot]))
			$compatibles = array_diff($compatibles,$versions_max_excl[$mot]);
	if (!count($compatibles))
		return "";

	$res = "";
	foreach ($compatibles as $v)
		$res .= "<li>".$versions[$v]."</li>";
	return $res;
}


// Transforme la couleur de fond de l'image en transparence
// alpha = 0: aucune transparence
// alpha = 127: completement transparent
function image_fond_transparent($im,$background_color, $tolerance=12,$coeff_lissage=7,$alpha = 127)
{
	include_spip('inc/filtres_images');
	$fonction = array('image_fond_transparent', func_get_args());
	$image = image_valeurs_trans($im, "fond_transparent-$background_color-$tolerance-$coeff_lissage-$alpha", "png", $fonction);
	if (!$image) return("");
	
	$x_i = $image["largeur"];
	$y_i = $image["hauteur"];
	
	$im = $image["fichier"];
	$dest = $image["fichier_dest"];
	
	$creer = $image["creer"];
	
	if (true OR $creer) {
		$bg = couleur_hex_to_dec($background_color);
		$bg_r = $bg['red'];
		$bg_g = $bg['green'];
		$bg_b = $bg['blue'];
	
		// Creation de l'image en deux temps
		// de facon a conserver les GIF transparents
		$im = $image["fonction_imagecreatefrom"]($im);
		imagepalettetotruecolor($im);
		$im2 = imagecreatetruecolor($x_i, $y_i);
		@imagealphablending($im2, false);
		@imagesavealpha($im2,true);
		$color_t = ImageColorAllocateAlpha( $im2, 255, 255, 255 , 127 );
		imagefill ($im2, 0, 0, $color_t);
		imagecopy($im2, $im, 0, 0, 0, 0, $x_i, $y_i);

		$im_ = imagecreatetruecolor($x_i, $y_i);
		imagealphablending ($im_, FALSE );
		imagesavealpha ( $im_, TRUE );
		$color_f = ImageColorAllocateAlpha( $im_, 255, 255, 255 , $alpha );

		for ($x = 0; $x < $x_i; $x++) {
			for ($y = 0; $y < $y_i; $y++) {
				$rgb = ImageColorAt($im2, $x, $y);
				$r = ($rgb >> 16) & 0xFF;
				$g = ($rgb >> 8) & 0xFF;
				$b = $rgb & 0xFF;
				if ((($d=abs($r-$bg_r)+abs($g-$bg_g)+abs($b-$bg_b))<=$tolerance)){
					imagesetpixel ( $im_, $x, $y, $color_f );
				}
				elseif ($tolerance AND $d<=($coeff_lissage+1)*$tolerance){
					$transp = round($alpha*(1-($d-$tolerance)/($coeff_lissage*$tolerance)));
					$color_p = ImageColorAllocateAlpha( $im_, $r, $g, $b , $transp);					
					imagesetpixel ( $im_, $x, $y, $color_p );
				}
				else
					imagesetpixel ( $im_, $x, $y, $rgb );
			}
		}
		image_gd_output($im_,$image);
		imagedestroy($im_);
		imagedestroy($im);
		imagedestroy($im2);
	}
	
	return image_ecrire_tag($image,array('src'=>$dest));
}

?>
