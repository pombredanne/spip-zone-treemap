<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

//
// Special pour spip.net
//

// Barre de navigation de spip.net :
'documentation' => 'Documentation',
'glossaire' => 'Glossaire',
'contribuer' => 'Contribuer',
'aide' => 'Aide',

// autre...
'dernieres_modifs' => 'Derni&egrave;res modifications',
'maj' => 'maj', // abbreviation de 'mise a jour'
'sites_realises_avec_spip' => 'Sites r&eacute;alis&eacute;s avec SPIP',
'derniers_sites_realises_avec_spip' => 'Derniers sites r&eacute;alis&eacute;s avec SPIP',
'sites_references' => 'Sites r&eacute;f&eacute;renc&eacute;s',

//
// Des trucs qui manquent, quelque soit le site...
//

// pour la navigation :
'accueil' => 'Accueil',
'lire_suite' => 'Lire la suite',
'liens_utiles' => 'Liens utiles',

// autres...
'quoideneuf' => 'Quoi de neuf ?',
'FAQ' => 'FAQ',

// specifique SPIP :
'squelette' => 'Squelette',
'squelettes' => 'Squelettes',
'squelette_voir' => 'Voir le squelette de cette page',
'squelettes_dossier' => 'Dossier squelettes'

// Voir aussi : http://fraichdist.online.fr/spip.php?article292

);

?>