<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

//
// Special pour spip.net
//

// Barre de navigation de spip.net :
'documentation' => 'Documentation',
'glossaire' => 'Glossary',
'contribuer' => 'Contribute',
'aide' => 'Help',

// autre...
'dernieres_modifs' => 'Latest modifications',
'maj' => 'upgrade', // abbreviation de 'mise a jour'
'sites_realises_avec_spip' => 'SPIP inside',
'derniers_sites_realises_avec_spip' => 'Latest sites designed by SPIP',
'sites_references' => 'Referenced sites',

//
// Des trucs qui manquent, quelque soit le site...
//

// pour la navigation :
'accueil' => 'Home',
'lire_suite' => 'Read more',
'liens_utiles' => 'Useful links',

// autres...
'quoideneuf' => 'What\'s new?',
'FAQ' => 'FAQ',

// specifique SPIP :
'squelette' => 'Template',
'squelettes' => 'Templates',
'squelette_voir' => 'Show the template of this page',
'squelettes_dossier' => 'Templates folder'

// Voir aussi : http://fraichdist.online.fr/spip.php?article292

);

?>