<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// Bare de nav
'visiteurs_en_ce_moment' => '<strong>@nb@</strong> visitantes en este momento',
'contribs' => 'contribuciones',
'documentation' => 'Documentaci&oacute;n',
'glossaire' => 'Glosario',
'contribuer' => 'Contribuir',
'aide' => 'Ayuda',
'telechargement' => 'Descargas',
'wiki' => 'Cuaderno wiki',

'info_rechercher_02' => 'Buscar en este sitio',
'info_rechercher' => '&iexcl;Encontrar!',
'info_auteurs' => 'Contribuidores',

// Information sur l'article
'versions'=>'Compatibilidad',
'version_inconnue'=>'<em>&iexcl;Compatibilidad desconocida!</em>',

// Menus lateraux
'dans_autres_langues' => 'En otros idiomas',
'c_est_chaud' => '&iexcl;Lo m&aacute;s calientito!',
'derniers_articles' => 'Las &uacute;ltimas',
'articles_top_notes' => 'Las preferidas',
'articles_top_popularite' => 'Las m&aacute;s leidas',

// Infos auteur
'login_login2' => 'Login&nbsp;:',
'ma_page' => 'Mi p&aacute;gina',
'participation_auteur'=>'contribuy&oacute; en:',

// pied
'ca_discute_par_ici' => 'Hablando de eso...',
'ca_spip_par_la' => 'SPIPeando del otro...',


//
// Special pour spip.net
//

// Barre de navigation de spip.net :

// autre...
'dernieres_modifs' => '&Uacute;ltimas modificaciones',
'maj' => 'actualizaci&oacute;n',
'sites_realises_avec_spip' => 'Sitios realizados con SPIP',
'derniers_sites_realises_avec_spip' => '&Uacute;ltimos sitios realizados con SPIP',
'sites_references' => 'Sitios referenciados',

//
// Des trucs qui manquent, quel que soit le site...
//

// pour la navigation :
'accueil' => 'Portada',
'lire_suite' => 'Seguir leyendo',
'liens_utiles' => 'Enlaces &uacute;tiles',
'lien_direct_forum' => 'Visitar el foro',
'retour_top' => 'Volver arriba',

// autres...
'quoideneuf' => '&iquest;Qu&eacute; tal?',
'FAQ' => 'Preguntas frecuentes',

// specifique SPIP :
'squelette' => 'Esqueleto',
'squelettes' => 'Esqueletos',
'squelette_voir' => 'Ver el esqueleto de esta p&aacute;gina',
'squelettes_dossier' => 'Carpeta de esqueletos',
's_inscrire' => '&iexcl;Quiero participar!',
'pass_vousinscrire' => 'Me inscribo en Spip-Contrib',
'sur_le_carnet' => 'En el Cuaderno Wiki',

// messagerie :
'messages_recus' => 'Mensajes recibidos',
'messages_envoyes' => 'Mensajes enviados',
'ecrire_message' => 'Enviar un mensaje',

// les tris :
'par_pertinence' => 'Los m&aacute; pertinentes',
'par_date' => 'Los m&aacute; reci&eacute;n publicados',
'par_popularite' => 'Los m&aacute;s populares',
'par_note' => 'Con mejores calificaciones',
'par_nom' => 'Por Nombre',
'par_contributions' => 'Principales contribuidores y contribuidoras',
'par_titre' => 'Por T&iacute;tulo',

);

?>
