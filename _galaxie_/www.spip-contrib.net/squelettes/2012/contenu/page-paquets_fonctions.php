<?php
/*
 * Page paquets de SPIP-Contrib
 * (c) 2009-2010 xxx
 * Distribue sous licence GPL
 *
 */


define('_DIR_ROOT','paquets/');

function paquet_infos($base_dir,$file){
	static $matches = null;
	$infos = array();
	if (is_null($matches)){
		include_spip("inc/xml");
		$xml = spip_xml_load(_DIR_ROOT.$base_dir."archives.xml", false, true); // non strict, sans conversion de charset (tout en entites)
		if (!$xml)
			$matches = array();
		else
			spip_xml_match_nodes(",^archive\b,",$xml,$matches);
	}
	$file = substr($file,strlen($base_dir));
	if (isset($matches['archive id="'.$file.'"'])){
		if (spip_xml_match_nodes(",^lien$,",reset($matches['archive id="'.$file.'"']),$liens)){
			$liens = reset($liens);
			$infos['lien'] = propre(trim(spip_xml_aplatit($liens)));
		}
	}
	return $infos;
}


function paquets_contenu_dir($dir,$type='file'){
	$dir = _DIR_ROOT . $dir;
	static $fichiers = null;
	if (is_null($fichiers)){
		$fichiers = array('dir'=>array(),'file'=>array());
		$maxfiles = 10000;
		if (@is_dir($dir) AND is_readable($dir) AND $d = @opendir($dir)) {
			while (($f = readdir($d)) !== false && ($nbfiles<$maxfiles)) {
				if ($f[0] != '.' # ignorer . .. .svn etc
				AND $f != 'CVS'
				AND $f != 'remove.txt'
				AND is_readable($f = "$dir"."$f")) {
					if (is_file($f)) {
						$fichiers['file'][substr($f,strlen(_DIR_ROOT))] = array('file'=>basename($f),'size'=>filesize($f),'date'=>filemtime($f));
						$nbfiles++;
					}
					elseif (is_dir($f)){
						$fichiers['dir'][substr($f,strlen(_DIR_ROOT)).'/'] = basename($f);
						$nbfiles++;
					}
				}
			}
			closedir($d);
		}
	}
	return $fichiers[$type];
}

function paquets_liste($dir="spip-zone/", $type='file'){
	$files = array();

	$dir = normalise_dir($dir);
	$base_dir = reset(explode('/',$dir));
	if (strlen($base_dir))
		$base_dir .= '/';

	$files = paquets_contenu_dir($dir,$type);
	if ($type=='file'){
		foreach($files as $f=>$infos){
			$files[$f] = $infos + paquet_infos($base_dir,$f);
		}
	}
	elseif(strlen(rtrim(dirname($dir),'/')) 
		AND strlen(rtrim(dirname($dir),'/'))>=strlen(rtrim($base_dir,'/'))){
		$files = array(dirname($dir).'/'=>'..')+$files;
	}
	return $files;
}

function paquets_initiales($dir="spip-zone/"){
	$dir = normalise_dir($dir);
	$files = paquets_contenu_dir($dir,'file');
	$initiales = array();
	foreach($files as $f=>$infos) {
		$nom = $infos['file'];
		$i = strtoupper($nom[0]);
		if (!isset($initiales[$i]))
		  $initiales[$i] = $nom;
	}
	return $initiales;
}

function normalise_dir($dir)
{
	$dir = trim($dir);
	$dir = str_replace('\\','/',$dir);
	$dir = str_replace('../','',$dir);
	$dir = str_replace('./','',$dir);
	$dir = str_replace(chr(0),'',$dir);
	$dir = ltrim($dir,'/');
	$dir = rtrim($dir,'/');

	if (strlen($dir))
		$dir.='/';

	return $dir;
}

function xdate($time,$format){return date($format,$time);}
?>
