RewriteEngine On

################ CONFIGURATION ######################

### Configuration sous-repertoire
# Chez la plupart des hebergeurs il faut indiquer "RewriteBase /"
# sinon modifiez cette ligne

RewriteBase /


# site spipnet
RewriteRule ^([a-z]{2,3}(_[a-z]{2,3}){0,2})_?$ spip.php?page=secteur&sitelang=$1 [QSA,L]
RewriteRule ^(plan|site)$      spip.php?page=$1       [QSA,L]
RewriteRule ^(oc)_(plan|site)$ spip.php?page=$2&lang=oc_lnc [QSA,L]
RewriteRule ^([a-z]{2,3})_(plan|site)$ spip.php?page=$2&lang=$1 [QSA,L]
RewriteRule ^([a-z]{2,3})/$    $1 [R,L]
RewriteRule ^([a-z_]+_)?rubrique([0-9]+)\.html$ spip.php?page=rubrique&id_rubrique=$2 [QSA,L]
RewriteRule ^([a-z_]+_)?article([0-9]+)\.html$ spip.php?page=article&id_article=$2 [QSA,L]
RewriteRule ^([a-z_]+_)?breve([0-9]+)\.html$ spip.php?page=breve&id_breve=$2 [QSA,L]
RewriteRule ^([a-z_]+_)?download$      spip.php?page=article [QSA,L]
RewriteRule ^(([a-z_]+_)?download)/    $1 [R]
RewriteRule ^([a-z_]+)_suivi$  spip.php?page=article [QSA,L]

# utilisons coral pour voir http://www.coralcdn.org/
#RewriteCond %{HTTP_USER_AGENT} !^CoralWebPrx
#RewriteCond %{QUERY_STRING} !^coral-no-serve
#RewriteCond /var/shim/spipnet/Web/%{REQUEST_FILENAME} -f
#RewriteRule ^/(IMG/|spip-dev/DISTRIB/).* http://www.spip.net.nyud.net:8080$0 [R,L]

# mots cles / index general / glossaire
RewriteRule ^@oc$              spip.php?page=mot&lang=oc_lnc [QSA,L]
RewriteRule ^@([a-z_0-9,-]*)$  spip.php?page=mot&lang=$1 [QSA,L]

# aide en ligne
RewriteRule ^aide/([a-z_0-9]+)-aide\.html$     aide.php3?lang_aide=$1 [QSA,L]
# cas des elements pris dans img_pack
RewriteRule ^aide(/ecrire)?(/puce(_rtl)?\..*|/img_pack/.+) dist/images$2 [L]
#images facon (aide/IMG/...)
RewriteRule ^aide/IMG/cache/TeX-(.*)           IMG/cache-TeX/$1 [L]
RewriteRule ^aide(/IMG/.*)                     $1 [L]
# images facon (aide/fr/xxxx.gif)
RewriteRule ^aide/AIDE(/.*/.+)                 AIDE$1 [L]
# css
RewriteRule ^aide(/spip_admin\.css)            $1 [L]
# appel direct de l'aide
RewriteRule ^aide/(aide_index.php3|layer.js)$          ecrire/$1 [QSA,L]
RewriteRule ^aide/                             aide_online.php3 [QSA,L]

