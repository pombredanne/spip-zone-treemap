<?php

$GLOBALS['puce'] = '- ';

# pour ceux qui ont le vieux modele et utilisent [-><version|stableurl>]
function filtres_telecharger($texte) {
	return str_replace('&lt;version|stableurl&gt;', 'http://files.spip.org/spip/', $texte);
}

// pour les forums
function raccourcir_nom($nom) {
	if (strpos($nom, "@")) {
		$nom = substr($nom, 0, strpos($nom, "@"));	
	}
	return $nom;
}

// pour afficher proprement le nom des langues
function afficher_nom_langue ($lang) {
	if (ereg("^oc(_|$)", $lang))
		return "occitan";
	else
		return traduire_nom_langue($lang);
}

// hack pour ne jamais afficher les secteurs d'aide en ligne
// sauf evidemment dans le cas de l'aide en ligne, ou dans l'espace prive
define('secteurs_aide', '324');
if (!defined('aide_en_ligne')
AND !_DIR_RACINE) {
	function boucle_ARTICLES($id_boucle, &$boucles) {
		$boucles[$id_boucle]->where[] = array("'NOT IN'", "'articles.id_secteur'", '"('.secteurs_aide.')"');
		return boucle_DEFAUT_dist($id_boucle, $boucles);
	}
	function boucle_RUBRIQUES($id_boucle, &$boucles) {
		$boucles[$id_boucle]->where[] = array("'NOT IN'", "'rubriques.id_secteur'", '"('.secteurs_aide.')"');
		return boucle_DEFAUT_dist($id_boucle, $boucles);
	}
	function boucle_HIERARCHIE($id_boucle, &$boucles) {
		$boucles[$id_boucle]->where[] = array("'NOT IN'", "'rubriques.id_secteur'", '"('.secteurs_aide.')"');
		return boucle_HIERARCHIE_dist($id_boucle, $boucles);
	}
}

// Prend une URL et lui ajoute/retire une ancre après l'avoir nettoyee
// pour l'ancre on vire les non alphanum du debut, et on remplace ceux dans le mot par -
// http://doc.spip.org/@ancre_url replace{}
function ancre_url_propre($url, $ancre) {
	// lever l'#ancre
	if (preg_match(',^([^#]*)(#.*)$,', $url, $r)) {
		$url = $r[1];
	}
	$ancre = preg_replace(array('/^[^-_a-zA-Z0-9]+/', '/[^-_a-zA-Z0-9]/'), array('', '-'), $ancre);
	return $url .'#'. $ancre;
}

#include _DIR_RACINE.'squelettes/inc-urls-trad.php';

?>
