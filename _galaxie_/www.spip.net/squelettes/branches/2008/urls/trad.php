<?php

// executer une seule fois
if (defined("_INC_URLS2")) return;
define("_INC_URLS2", "1"); // est-ce encore utile ?

// Les cles de ce tableau sont reperees par le htaccess
$GLOBALS['articles_aide_en_ligne'] = array(
	'download' =>  2670,
	'suivi' => 2275,
	'article(\d+)\.html' => 0
	);

@define('_TRAD_REWRITE_BASE', '/');
define('_TRAD_ARTICLE', '#^' . _TRAD_REWRITE_BASE . "(([a-z_]+)_)?(" . 
       join('|', array_keys($GLOBALS['articles_aide_en_ligne'])) .
       ")$#");
define('_TRAD_AIDE',     '#^' . _TRAD_REWRITE_BASE . "aide/([^-]*)-aide\.html$#");
define('_TRAD_MOT',      '#^' . _TRAD_REWRITE_BASE . "(@[a-z_0-9,-]+)$#");
define('_TRAD_RUBRIQUE', '#^' . _TRAD_REWRITE_BASE . "([a-z_]+)_$#");


define('URLS_TRAD_EXEMPLE', 'fr_rubrique_91.html');

function langue_choix ($id, $type='article') {
	switch ($type) {
	case 'article':
		if ($a = sql_fetsel('lang, id_secteur', "spip_articles", "id_article='$id'")) {

			if ($a['id_secteur'] == 324) # aide en ligne
				return "aide/".$a['lang'].'-aide.html';
			if ($a['lang'] == 'cpf_hat')
				return 'cpf_hat';
			return ereg_replace("_.*","",$a['lang']);
		}
	case 'rubrique':
		if ($a = sql_fetsel('lang, id_secteur', "spip_rubriques", "id_rubrique='$id'")) {
			if ($a['id_secteur'] == 324) # aide en ligne
				return "aide/".$a['lang'].'-aide.html';
			if ($a['lang'] == 'cpf_hat')
				return 'cpf_hat';
			return ereg_replace("_.*","",$a['lang']);
		}
	case 'forum':
		include_spip('inc/forum');
		if ($racine = racine_forum($id))
			return langue_choix($racine[1], $racine[0]);
	}
	return '';
}

function _generer_url_trad($type, $id, $args='', $ancre='')
{
	$args = ($args ? "?$args" : '') . ($ancre ? "#$ancre" : '');
	$lang = langue_choix ($id, $type);
	if (function_exists($f = "generer_url_trad_$type"))
		return _DIR_RACINE.$f($lang, $id, $args);
	else return '';
}

function generer_url_trad_rubrique($lang, $id, $args) {
	return ($lang ? "{$lang}_" : '') . 'rubrique' . $id . '.html' . $args;
}

function generer_url_trad_auteur($lang, $id, $args) {
	return  "./?page=auteur&amp;id_auteur=$id&amp;$args";
}

function generer_url_trad_breve($lang, $id, $args) {
	return ($lang ? "{$lang}_" : '') . 'breve' . $id . '.html' . $args;
}

function generer_url_trad_article($lang, $id_article, $args) {
	if (ereg('aide/', $lang))
		return $lang;
	else if ($lang)
		return $lang."_article$id_article.html$args";
	else
		return "article$id_article.html$args";
}

function generer_url_trad_forum($lang, $id_forum, $args) {
	$t = sql_fetsel("id_thread, id_forum", 'spip_forum', "id_forum=$id_forum");
	if (!$t) return '';
	$url = $lang."_".$t['id_thread'].'.html';
	if ($t['id_forum'] <> $t['id_thread']) $url .= '#forum'.$t['id_forum'];
	return $url;
}

function generer_url_trad_mot($lang, $id_mot, $args) {
	include_spip('inc/charsets');
	$q = sql_fetsel('titre', 'spip_mots',  "id_mot=$id_mot");
	if (!$q) return "mot$id_mot.html$args";
	$url = '@'.preg_replace(';[^a-z0-9_,-];', '',
			strtolower(translitteration($q['titre'])));
	$extra = serialize(array('url'=>$url));
	sql_updateq('spip_mots', array('extra' => $extra), "id_mot=$id_mot");
	return $url . $args;
}

function generer_url_trad_document($lang, $id_document, $args) {

	include_spip('inc/documents');
	return get_spip_doc(sql_getfetsel('fichier', 'spip_documents', 'id_document=' . sql_quote($id_document)));
}

function urls_trad_dist($i, &$entite, $args='', $ancre=''){
	if (is_numeric($i))
		return _generer_url_trad($entite, $i, $args, $ancre);

	$url = $i;
	$id_objet = $type = 0;
	$url_redirect = null;
	// recuperer les &debut_xx;
	if (is_array($args))
		$contexte = $args;
	else
		parse_str($args,$contexte);

	$lang = (isset($contexte['sitelang'])?$contexte['sitelang']:null); //RewriteRule ?page=secteur&sitelang=URI
	// recuperer les rubriques meres demandees par "www.spip.net/LANG"
	if (preg_match(_TRAD_RUBRIQUE, $url, $regs) || $lang) {
		$entite = 'rubrique';
		if ($regs[1]) $lang = $regs[1];
		if ($lang == 'fr'){
		  // cas particulier du francais qui a plusieurs rubriques
			$contexte['id_rubrique'] = 91;
		}
		else {
			$where =  "id_parent=0 AND lang LIKE "
				. sql_quote($lang .'%')
				. " AND statut='publie'"; 

			$t = sql_getfetsel("id_secteur", 'spip_rubriques', $where . " AND NOT (titre LIKE '%-aide.html%')");
			if (!$t)
				$t = sql_getfetsel("id_secteur", 'spip_rubriques', $where);
			if ($t){
				$contexte['id_rubrique'] = $t;
			}
			else {
				$sites_redirection = Array (
					'da' => 'http://listes.rezo.net/mailman/listinfo/spip-da',
				//	'de' => 'http://www.spip.de/',
				//	'eo' => 'http://listes.rezo.net/mailman/listinfo/spip-eo',
					'gl' => 'http://listes.rezo.net/mailman/listinfo/spip-gl',
					'it' => 'http://listes.rezo.net/mailman/listinfo/spip-it',
				//	'nl' => 'http://listes.rezo.net/mailman/listinfo/spip-nl',
					'pt' => 'http://listes.rezo.net/mailman/listinfo/spip-pt'
				);

				if ($url = $sites_redirection[$lang]) {
					$url_redirect = $url;
				} else {
					$url_redirect = "http://www.spip.net/";
				}
			}
		}
	}

	// recuperer l'aide en ligne
	else if (preg_match(_TRAD_AIDE, $url, $regs)) {
		$entite = 'aide';
		$lang = addslashes($regs[1]);

		## redirections d'aide
		if (preg_match(',^oc(_.*)?$,', $lang)) $lang = 'oc_lnc';
		if (preg_match(',^pt(_.*)?$,', $lang)) $lang = 'pt';

		$id_rubrique = sql_getfetsel('id_rubrique', 'spip_rubriques',
				   "id_parent=324 AND lang="
				   . sql_quote($lang)
				   . " AND statut='publie' AND (titre LIKE '%-aide.html%')");
		if ($id_rubrique)
			$contexte['id_rubrique'] = $id_rubrique;
	}

	// recuperer l'article correspondant a "www.spip.net/xx_suivi"
	// si possible dans la langue, sinon en francais
	else if (preg_match(_TRAD_ARTICLE, $url, $regs)) {
		$entite = 'article';
		$lang = $regs[2];
		$id_original = $GLOBALS['articles_aide_en_ligne'][$regs[3]];
		if (!$id_original) $id_original = intval($regs[4]);
		$id_article = sql_getfetsel('id_article', 'spip_articles', "id_trad=".intval($id_original)." AND statut='publie'", '',  "lang<>'$lang',lang<>'fr'");
		if ($id_article)
			$contexte['id_article'] = $id_article;
	}

	// recuperer les mots-cles (balises de spip)
	else if (preg_match(_TRAD_MOT, $url, $regs)) {
		$entite = 'mot';
		$extra = serialize(array('url'=>$regs[1]));
		$id_mot = sql_getfetsel('id_mot', 'spip_mots', "extra=" . sql_quote($extra));
		if ($id_mot)
			$contexte['id_mot'] = $id_mot;
	}

	return array($contexte, $entite, $url_redirect, null);
}
?>
