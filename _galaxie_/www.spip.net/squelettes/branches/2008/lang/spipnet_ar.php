<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_galaxie_/www.spip.net/squelettes/2008/lang
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'chercher_parmi_les_signataires' => 'Chercher parmi les signataires',

	// D
	'derniers_sites_realises_avec_spip' => 'Derniers sites réalisés avec SPIP',

	// G
	'glossaire' => 'مسرد',

	// L
	'liens_utiles' => 'وصلات مفيدة',

	// M
	'maj' => 'mis à jour le ',

	// T
	'trad_bilan' => 'Bilan des traductions',
	'trad_espace' => 'Espace de traduction',

	// W
	'web_independant' => 'Pour un web indépendant',
	'web_independant_manifeste' => 'Manifeste pour un web indépendant'
);

?>