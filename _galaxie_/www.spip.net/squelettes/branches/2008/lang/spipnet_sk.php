<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/spipnet?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'chercher_parmi_les_signataires' => 'Vyhľadať medzi podpísanými ľuďmi',

	// D
	'derniers_sites_realises_avec_spip' => 'Najnovšie stránky vytvorené v SPIPe',

	// G
	'glossaire' => 'Slovník termínov',

	// L
	'liens_utiles' => 'Užitočné odkazy',

	// M
	'maj' => 'Aktualizované',

	// T
	'trad_bilan' => 'Bilancia prekladov',
	'trad_espace' => 'Prekladateľská zóna',

	// W
	'web_independant' => 'Pre nezávislý web',
	'web_independant_manifeste' => 'Manifest pre nezávislý web'
);

?>
