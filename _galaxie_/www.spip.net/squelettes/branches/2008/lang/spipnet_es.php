<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/spipnet?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'chercher_parmi_les_signataires' => 'Chercher parmi les signataires',

	// D
	'derniers_sites_realises_avec_spip' => 'Derniers sites réalisés avec SPIP',

	// G
	'glossaire' => 'Glosario',

	// L
	'liens_utiles' => 'Enlaces de interés',

	// M
	'maj' => 'mis à jour le ',

	// T
	'trad_bilan' => 'Bilan des traductions',
	'trad_espace' => 'Espace de traduction',

	// W
	'web_independant' => 'Pour un web indépendant',
	'web_independant_manifeste' => 'Manifeste pour un web indépendant'
);

?>
