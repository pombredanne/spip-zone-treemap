<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/spipnet?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'chercher_parmi_les_signataires' => 'Search among the signatories',

	// D
	'derniers_sites_realises_avec_spip' => 'Last websites made with SPIP',

	// G
	'glossaire' => 'Glossary',

	// L
	'liens_utiles' => 'Useful links',

	// M
	'maj' => 'updated on',

	// T
	'trad_bilan' => 'Translation balance',
	'trad_espace' => 'Translation area',

	// W
	'web_independant' => 'For an independant web',
	'web_independant_manifeste' => 'Manifesto for an independant web'
);

?>
