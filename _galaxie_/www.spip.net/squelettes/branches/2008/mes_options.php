<?php

# spip.us fait proxy mais ne nous a jamais contactes :
if ($_SERVER['REMOTE_ADDR'] == '88.176.90.50') {
	include_spip('inc/headers');
	redirige_par_entete('http://spip.net/');
}

	# taille max du cache
	$quota_cache = 60;

	# moderation des petitions
	define('_SPIP_MODERATEURS_PETITION', 'scalepa@gmail.com,ben.spip@gmail.com');

	$type_urls = 'trad';

	$table_des_traitements['TITRE'][]= 'typo(supprimer_numero(%s))';

	# raccourcis [->spip19] etc

// pas d'autobr dans l'aide, ca casse des images (autobr a debug donc)
if (_request('lang_aide')) define('_AUTOBR','');

function generer_url_ecrire_spip($rac,$args,$ancre){
	return generer_url_spip($rac, $args, $ancre);
}

function generer_url_spip($rac, $args, $ancre)
{
	static $liens_spip = array(
		1 => 1309,
		10 => 1309,
		103 => 1309,
		104 => 1309,
		105 => 1309,
		12 => 1310,
		121 => 1310,
		13 => 1253,
		14 => 1832,
		15 => 1911,
		16 => 1965,
		17 => 2102,
		171 => 2102,
		172 => 2102,
		18 => 2991,
		181 => 2991,
		182 => 3173,
		183 => 3333,
		19 => 3368,
		191 => 3462,
		192 => 3567,
		20 => 3784,
		21 => 4728,
		30 => 5427,
	);

	if (isset($liens_spip[$rac])) {
		$id = $liens_spip[$rac];
		$id_trad = sql_getfetsel('id_article', 'spip_articles', 
				     "id_trad=" 
				     . $id
				     . " AND lang="
					 . sql_quote($GLOBALS['spip_lang']));
		if ($id_trad) $id = $id_trad;
		return array('article', $id);
	}
	spip_log("raccourci spip$id inconnu");
	return '';
 }


	# mise a jour des squelettes par http://www.spip.net/ecrire/?exec=svn_update
	define('_SVN_UPDATE_AUTEURS', '1:180:9:3021');

	# des urls pourries gatent le systeme
	if (_DIR_RESTREINT AND count(explode('/', $_SERVER['REQUEST_URI'])) -count(explode('/', $_SERVER['QUERY_STRING'])) > 2)
		die(header('Location: /'));
	

// antispam de signature de forum (a integrer dans akismet ?)
function inc_controler_signature($id_article, $nom_email, $adresse_email, $message, $nom_site, $url_site, $url_page) {
	if ($a = @unserialize($GLOBALS['meta']['spampetitions'])  
	AND strlen($a = $a['regexp'])
	AND (
		preg_match($a, $nom_email)
		OR preg_match($a, $adresse_email)
		OR preg_match($a, $message)
	)) {
		spip_log("spam detecte sur la petition $id_article", 'spam');
		return false; // spam detecte
	}


	return inc_controler_signature_dist($id_article, $nom_email, $adresse_email, $message, $nom_site, $url_site, $url_page);
}

?>
