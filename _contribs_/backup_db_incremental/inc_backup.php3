<?php

#
# Debut de contrib "backup"
#
# L'idee est d'avoir un backup permanent (par spip-cron)
# incremental (quelques elements a la fois)
# compatible SQL (pour reinjecter des articles directement dans la base)
# ventile par article/rubrique (pour faire des restaurations selectives)
# voir http://spip.blog.ouvaton.org/blog.php?id_blog=1&id_rubrique=7
#

## pour utiliser ce fichier il faut ajouter une tache "backup"
## dans ecrire/inc_cron

//
// Ce fichier ne sera execute qu'une fois
if (defined("_INC_BACKUP")) return;
define("_INC_BACKUP", "1");

define('_DIR_BACKUP', _DIR_SESSIONS.'backup/');
if (!is_dir(_DIR_BACKUP)) {
	mkdir (_DIR_BACKUP);
}

function spip_backup() {
	spip_log('backup');

	$s = spip_query("SHOW TABLES");
	$h = rand(0, spip_num_rows($s)-1);
	while ($t = spip_fetch_array($s)) {
		if ($h-- == 0)
			spip_log($table = $t[0]);
	}

	$s = spip_query("SHOW FIELDS FROM $table");
	while ($t = spip_fetch_array($s))
		spip_log($t[0]);


	$tables = array (
		'spip_articles' => 'id_article',
		'spip_rubriques' => 'id_rubrique'
	);

	foreach ($tables as $table => $id) {
		$s = spip_query("SELECT *, UNIX_TIMESTAMP(maj) AS d
		FROM $table ORDER BY maj DESC");
		while ($t = spip_fetch_array($s)
		AND $n < 15) {

			$dest = _DIR_BACKUP.$table.$t[$id].'.sql';

			# backup recent : inutile !
			if (@file_exists($dest)
			AND @filemtime($dest) > $t['d'])
				continue;

			# sinon enregistrer le fichier
			unset($keys);
			unset($vals);
			foreach ($t as $key => $val) {
				if (!is_int($key)) {
					$keys[] = "`$key`";

					if (lire_meta('charset') <> 'utf-8')
						$val = unicode_to_utf8(charset2unicode($val,
							lire_meta('charset'), true));

					$vals[] = "/* $key */ '".addslashes($val)."'";
				}
			}
			
			$r = "# $table.$id=".$t[$id]."\n";
			$r .= "REPLACE $table (\n"
				. join (",\n", $keys)
				.")\nVALUES (\n"
				. join (",\n", $vals)."\n);";
			$dest = $table.$t[$id].'.sql';
			ecrire_fichier(_DIR_BACKUP.$dest, $r);
			spip_log(_DIR_BACKUP.$dest);

			# au bout de 15 fichiers on arrete
			$n ++;
		}
	}
}


?>
