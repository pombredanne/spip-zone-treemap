This project is a patch proposed for the modification of the authentification mechanism.  

It is related to the discussion on security which I started on spip-dev:  
http://permalink.gmane.org/gmane.comp.web.spip.devel/30145 

The list of the answers can be found here : 
http://comments.gmane.org/gmane.comp.web.spip.devel/30145 

CAUTION :  
Many files of the core of SPIP are modified. Use them in consequence, at your own risk.

ORGANIZATION :
You will find here : 
- a file patch.[version-number-of-the-svn-used].txt 
- all the patched files 
- this README.txt

COMMIT RULES :  
- You must absolutely understand the current mechanism of authentification: you can read some technical details at  http://www.spip-contrib.net/spikini/AuthentificationDansSpip [French]
- add your file patch[svn_used_version].txt 
- Please complete README.txt by explaining your modifications.  

In a general way, you must comply with the commit rules given by Mortimer:  http://zone.spip.org/trac/spip-zone/file/_contribs_/_balises_/exif/tags/1.8.1/_REGLES_DE_COMMIT.txt [French]

TODO:  
- Trying to pass this mechanism into a pluggin (possible ?)
- Reinforcing the mechanism which is clearly not sufficient enough (see the objection of Fil:  http://permalink.gmane.org/gmane.comp.web.spip.devel/30157 and that of Christian :  http://permalink.gmane.org/gmane.comp.web.spip.devel/30158) 
- Reinforcing the management of the sessions and/or the cookies 

You can contact me in case of problem or suggestion.  But I will not make any service after sale;)

.Gilles VINCENT
gilles.vincent@gmail.com
