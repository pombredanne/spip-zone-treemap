<?php
	/****************************\
	 * serveur d'authenfication *
	\****************************/

	function retour_erreur($code) {
		$texte = _L($code);/*_T("local/spip:".$code)*/
		return "\t<erreur>1</erreur>\n" .
		"\t<message>".$texte."</message>\n";
	}

	include("ecrire/inc_version.php3");
	include_ecrire("inc_connect.php3"); // pour $db_ok
	include_ecrire("inc_auth_spip.php3");
	$charset = lire_meta('charset');
	@header('Content-type: text/xml; charset='.$charset);
	echo "<"."?xml version=\"1.0\" encoding=\"".$charset."\" ?".">\n";
	$contenu = "<auth>\n";

	if(empty($infos) AND empty($login)) {
		$contenu .= retour_erreur("pas de demande");
	}
	else if (!$GLOBALS['db_ok']) {
		$contenu .= retour_erreur("pas de base MySQL disponible");
	}
	else if ($serveur_off) {
		$contenu .= retour_erreur("serveur de compte desactive");
	}
	else if ($infos) {
		$contenu .= "\t<erreur>0</erreur>\n";
		$contenu .= "\t<infos>\n";
		$contenu .= "\t\t<moteur>SPIP</moteur>\n";
		$contenu .= "\t\t<version>".$spip_version_affichee."</version>\n";
		$contenu .= "\t\t<encodage>".$charset."</encodage>\n";
		$contenu .= "\t\t<methode>4</methode>\n"; //Spip fait du challenge MD5 avec graine aleatoire
		$contenu .= "\t</infos>\n";
	}
	else {
		//Demande de graine ou authentification ?
		if(empty($session_password) AND empty($session_password_md5)) {
			$query = "SELECT alea_actuel, alea_futur FROM spip_auteurs WHERE login='".addslashes($login)."' AND statut<>'5poubelle'";
			$result = spip_query($query);
			if($row = spip_fetch_array($result)) {
				$contenu .= "\t<erreur>0</erreur>\n";
				$contenu .= "\t<infos>\n";
				$contenu .= "\t\t<login>".$login."</login>\n";
				$contenu .= "\t\t<alea_actuel>".$row['alea_actuel']."</alea_actuel>\n";
				$contenu .= "\t\t<alea_futur>".$row['alea_futur']."</alea_futur>\n";
				$contenu .= "\t</infos>\n";
			}
			else {
				$contenu .= retour_erreur("login inconnu");
			}
		}
		else {
			$auth = new Auth_spip;
			// Essayer les mots de passe md5
			$ok = $auth->verifier_challenge_md5($login, $session_password_md5, $next_session_password_md5);
			// Sinon essayer avec le mot de passe en clair
			if (!$ok && $session_password) $ok = $auth->verifier($login, $session_password);

			if($ok) {
				$contenu .= "\t<erreur>0</erreur>\n";
				$contenu .= "\t<infos>\n";
				$contenu .= "\t\t<test>connexion reussi. Envoi des infos utilisateur</test>\n";
				$contenu .= "\t</infos>\n";			
			}
			else {
				$contenu .= retour_erreur("login incorrect ou password errone");
			}

			// Si la connexion a reussi
			if ($ok) {
				// Nouveau redacteur ou visiteur inscrit par mail :
				// 'nouveau' -> '1comite' ou  '6forum'
				// Si LDAP : importer l'utilisateur vers la base SPIP
				$auth->activer();
			}
		}
	}
	
	$contenu .= "</auth>";
	echo $contenu;
?>
