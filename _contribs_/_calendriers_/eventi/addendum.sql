ALTER TABLE spip_articles
	ADD date_debut_evt DATE DEFAULT '0000-00-00',
	ADD heure_debut_evt TIME DEFAULT '25:00:00',
	ADD date_fin_evt DATE DEFAULT '0000-00-00',
	ADD heure_fin_evt TIME DEFAULT '25:00:00';
