<?php
global $tables_principales, $tables_relations, $table_primary, $table_date;

// pour la gestion de rendez-vous (�v�nenements)

$tables_principales['rdvs']   =  array(
	'field' => array(
		'id_rdv'		=>	"BIGINT(21) NOT NULL",
		'date_debut'	=>	"DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00'",
		'date_fin'		=>	"DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00'",
		'periodique'	=>	"ENUM('oui','non') NOT NULL DEFAULT 'non'",
		'per_qtite'		=>	"SMALLINT NOT NULL DEFAULT 0",
		'per_cycle'		=>	"DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00'",
		'per_debut'		=>	"DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00'",
		'per_fin'		=>	"DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00'"),
	'key' => array(
		'PRIMARY KEY'	=>	"id_rdv",
		'KEY date_debut'	=> "date_debut",
		'KEY date_fin'	=> "date_fin")
	);

// pour l'affichage de date (jour, affichage court, etc)
$table_date['rdvs'] = "date_fin";
$table_date['rdvs'] = "date_debut";

// la relation entre les articles et les rendez-vous

$spip_articles_rdvs = array(
	"id_rdv"		=>	"BIGINT(21) DEFAULT '0' NOT NULL",
	"id_article"	=>	"BIGINT(21) DEFAULT '0' NOT NULL");

$spip_articles_rdvs_key = array(
	"KEY id_rdv"		=>	"id_rdv",
	"KEY id_article"	=>	"id_article");

$GLOBALS['tables_principales']['spip_articles_rdvs']   =  array(
	'field'	=> &$spip_articles_rdvs,
	'key'	=> &$spip_articles_rdvs_key);

$tables_relations['rdvs']['id_article'] = 'articles_rdvs';
$tables_relations['articles']['id_rdv'] = 'articles_rdvs';

// la relation entre les rubriques et les rendez-vous

$spip_rubriques_rdvs = array(
	"id_rdv"		=>	"BIGINT(21) DEFAULT '0' NOT NULL",
	"id_rubrique"	=>	"BIGINT(21) DEFAULT '0' NOT NULL");

$spip_rubriques_rdvs_key = array(
	"KEY id_rdv"		=>	"id_rdv",
	"KEY id_rubrique"	=>	"id_rubrique");

$GLOBALS['tables_principales']['spip_rubriques_rdvs']   =  array(
	'field'	=> &$spip_rubriques_rdvs,
	'key'	=> &$spip_rubriques_rdvs_key);

$tables_relations['rdvs']['id_rubrique'] = 'rubriques_rdvs';
$tables_relations['rubriques']['id_rdv'] = 'rubriques_rdvs';

//
// <BOUCLE(RDVS)>
//

function boucle_RDVS( $id_boucle, &$boucles) {
	$boucle = &$boucles[$id_boucle];
	$id_table = $boucle->id_table;
	$boucle->from[] = "spip_rdvs AS $id_table";
 	return calculer_boucle( $id_boucle, $boucles);
}
?>
