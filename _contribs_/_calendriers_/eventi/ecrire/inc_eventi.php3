<?php

if (defined("_INC_EVENT_FUN")) return;
define("_INC_EVENT_FUN", "1");

// include ("inc.php3");
// include_ecrire ("inc_abstract_sql.php3");	 // gestions des tables SQL
include_ecrire ("inc_filtres.php3");	// filtres sur les dates et heures

// une fonction de r�cup�ration de variables (cf inc-public-global)
// Renvoie le _GET ou le _POST emis par l'utilisateur, NULL sinon
function _request($var) {
	global $_GET, $_POST;
	if (isset($_GET[$var])) return $_GET[$var];
	if (isset($_POST[$var])) return $_POST[$var];
	return NULL;
}

// Fonctions sur les formats de dates MySQL et usuels //////////////////////////

function date_from_mysql( $ze_date ) {
	if (ereg('([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})', $ze_date, $reg)) {
		return $reg[3].'/'.$reg[2].'/'.$reg[1];
	}
	return $ze_date;
}

function date_to_mysql( $ze_date ) {
	if (ereg('([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})', $ze_date, $reg)) {
		$annee = $reg[3];
		// gestion des ann�es � 2 chiffres
		if (strlen($annee)<3) {
			if ($annee<90) {
				$annee += 2000;
			}
			else {
				$annee += 1900;
			}
		}
		// un jour � 2 chiffres
		$reg[3] = str_pad( $reg[3], 2, '0', STR_PAD_LEFT ); // compl�te avec des z�ros

		return $annee.'-'.$reg[2].'-'.$reg[1];
	}
	return $ze_date;
}

function heure_from_mysql( $ze_date ) {
	if (ereg('([0-9]{2}):([0-9]{2}):([0-9]{2})', $ze_date, $reg)) {
		return $reg[1].':'.$reg[2].':'.$reg[3];
	}
	return $ze_date;

}

function heure_to_mysql( $ze_heure ) {
	if (ereg('([0-9]{1,2})[:h ]([0-9]{2})[:m ]?([0-9]{2})?', $ze_heure, $reg)) {
		$reg[1] = str_pad( $reg[1], 2, '0', STR_PAD_LEFT );
		return $reg[1].':'.$reg[2].':'.(($reg[3]=='') ? '00' : $reg[3]);
	}
	return $ze_heure;
}

function test_date( $ze_date ) {
	if (ereg('([0-9]{1,2})/([0-9]{1,2})/[0-9]{2,4}', $ze_date, $reg)) {
		if ( (($reg[1]>=0) || ($reg[1]<32)) && (($reg[2]>=0) || ($reg[2]<13)) ) {
			return true;
		}
	}
	return false;
}

function test_heure( $ze_heure ) {
	if (ereg('([0-9]{1,2})[:h]([0-9]{2})(:[0-9]{2})?', $ze_heure)) {
		if ( (($reg[1]>=0) || ($reg[1]<25)) && (($reg[2]>=0) || ($reg[2]<=60)) ) {
			return true;
		}
	}
	return false;
}

function date_est_nul( $ze_date ) {
	return (date_to_mysql($ze_date)=='0000-00-00');
}

function heure_est_nul( $ze_heure ) {
	return (heure_to_mysql( $ze_heure )=='00:00:00');
}

// fonction de r�cup�ration de l'id_rdv ////////////////////////////////////////

function recup_id_rdv() {
	// on r�cup�re l'id du rendez-vous attach� � l'�l�ment choisi (si yen a un)
	global $id_article,$id_rubrique;
	if ($id_article) {
		$req = "SELECT id_rdv
				FROM spip_articles_rdvs
				WHERE id_article=$id_article";
	}
	elseif ($id_rubrique) {
		$req = "SELECT id_rdv
				FROM spip_rubriques_rdvs
				WHERE id_rubrique=$id_rubrique";
	}
	$result = spip_query( $req );
	// on r�cup�re le rendez-vous, s'il existe...
	// dans le futur, il se peut qu'il y en ait plusieurs...
	if ($row = spip_abstract_fetch( $result )) {
		return $row['id_rdv'];
	}
	else {
		// sinon, nya pas de rendez-vous
		return null;
	}
}

//


?>
