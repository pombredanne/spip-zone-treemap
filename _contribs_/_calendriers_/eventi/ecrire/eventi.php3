<?php

/*
 * Fait pour �tre int�grer � la r�daction d'article ou de rubrique
 * afin d'y attacher un �v�nement
 */

include_ecrire( "event_func.php3" );

// R�sup�ration et/ou initialisation des valeurs ///////////////////////////////

$rdv_error= array();

$datetime_nul='0000-00-00 00:00:00';

$action = _request('action');

$id_article= _request('id_article');
$id_rubrique= _request('id_rubrique');

if (!$action) {
	if ($id_article || $id_rubrique) {
		$action='select';
	}
	else {
		$action = 'rien';
	}
}

//--------------
// r�cup�ration
//--------------

switch ($action) {
// annuler
	case 'annul':

		$id_rdv= recup_id_rdv();
		// dates et heures nulles
		$datetime_debut = $datetime_nul;
		$datetime_fin = $datetime_nul;
		break; // fin case annul

// selectionner

	case 'select' :
		$id_rdv= recup_id_rdv();

		if ($id_rdv) {
			$req = "SELECT date_debut, date_fin
					FROM spip_rdvs
					WHERE id_rdv=$id_rdv";
			$result = spip_query( $req );
			if ($row = spip_abstract_fetch( $result )) {
				$datetime_debut = $row['date_debut'];
				$datetime_fin = $row['date_fin'];
			}
		}
		else {
			// sinon, nya pas de rendez-vous donc, dates et heures nulles
			$datetime_debut = $datetime_nul;
			$datetime_fin = $datetime_nul;
		}
		break; // fin case select

// ne rien faire

	case 'rien':
		$id_rdv = null;
		$datetime_debut = $datetime_nul;
		$datetime_fin = $datetime_nul;
		break;

// attribuer

	case 'attrib':
		// ya qqch s�lectionn� ?
		if ((!$id_article) && (!$id_rubrique)) {
			echo "pas d'element s�lectionn�";
			$action = 'rien';
			break;
		}

		$id_rdv= recup_id_rdv();
		// on r�cup�re les dates modifi�s
		$date_debut = _request('date_debut');
		if (!$date_debut) {
			$date_debut='00/00/0000';
		}

		$heure_debut = heure_to_mysql(_request('heure_debut'));
		if (!$heure_debut) {
			$heure_debut='00:00:00';
		}
		else {
			// pour montrer qu'une heure est valide, on met les secondes � '01'
			$heure_debut=ereg_replace( "([0-9]{1,2}[:h][0-9]{2}[:h])00","\\101", $heure_debut );
		}

		$date_fin = _request('date_fin');
		if (!$date_fin) $date_fin='00/00/0000';

		$heure_fin = heure_to_mysql(_request('heure_fin'));
		if (!$heure_fin) {
			$heure_fin='00:00:00';
		}
		else {
			// pour montrer qu'une heure est valide, on met les secondes � '01'
			$heure_fin =ereg_replace( "([0-9]{1,2}[:h][0-9]{2}[:h])00","\\101", $heure_fin );
		}

		break;
} // fin switch action

// les dates

//	1. les tester et les modifier si besoin
//	2. r�cup�rer les jours, mois, ann�es, heures et minutes correspondantes

if ($action=='attrib') {
	//date debut
	if (!test_date($date_debut)) {
		$rdv_error['date_debut']='invalide';
		$date_debut = $datetime_nul;
		$action = 'rien';
	}

	//heure debut
	if (!test_heure($heure_debut)) {
		$rdv_error['heure_debut']='invalide';
		$heure_debut = '00:00:00';
		$action = 'rien';
	}

	//date fin
	if (!test_date($date_fin)) {
		$rdv_error['date_fin']='invalide';
		$date_fin = $datetime_nul;
		$action = 'rien';
	}

	//heure fin
	if (!test_heure($heure_fin)) {
		$rdv_error['heure_fin']='invalide';
		$heure_fin = '00:00:00';
		$action = 'rien';
	}
}
else {
	//date debut
	$date_debut = date_from_mysql( $datetime_debut );
	//heure debut
	$heure_debut = join( ":", recup_heure( $datetime_debut ));
	// date fin
	$date_fin = date_from_mysql( $datetime_fin );
	// heure fin
	$heure_fin = join( ":", recup_heure( $datetime_fin ));
}

// les modifications

switch ($action) {

	case 'attrib' :
		// si on attribue et...
		if ( $id_rdv ) {
			// ...si ya un rendez-vous attach�, on met � jour
			spip_query("UPDATE spip_rdvs
						SET date_debut='".date_to_mysql( $date_debut )." ".heure_to_mysql( $heure_debut)."',
								date_fin='".date_to_mysql( $date_fin )." ".heure_to_mysql( $heure_fin)."'
						WHERE id_rdv=$id_rdv");
		}
		else {
			// sinon, on en cr�e un nouveau
			$id_rdv = spip_abstract_insert(
							"spip_rdvs",
							"( date_debut, date_fin)",
							"(	'".date_to_mysql( $date_debut )." ".heure_to_mysql( $heure_debut)."',
								'".date_to_mysql( $date_fin )." ".heure_to_mysql( $heure_fin)."' )"
						);
			// on fait la correspondance avec l'�lement
			if ($id_article) {
				$req = "INSERT INTO spip_articles_rdvs
						(id_rdv, id_article)
						VALUES ($id_rdv, $id_article)";
			}
			elseif ($id_rubrique) {
				$req = "INSERT INTO spip_rubriques_rdvs
						(id_rdv, id_rubrique)
						VALUES ($id_rdv, $id_rubrique)";
			}
			$result= spip_query( $req );
		}
		break;

	case 'annul' :
		if ( $id_rdv ) {
			// on efface le rendez-vous
			spip_query("DELETE FROM spip_rdvs WHERE id_rdv=$id_rdv");
			// on efface la correspondance
			if ( $id_article ) {
				spip_query("DELETE FROM spip_articles_rdvs
								WHERE id_rdv=$id_rdv_choice
								AND id_article=$id_choice");
			}
			elseif ( $id_rubrique ) {
				spip_query("DELETE FROM spip_rubriques_rdvs
								WHERE id_rdv=$id_rdv_choice
								AND id_rubrique=$id_choice");
			}
		}
		break;
} // fin switch $action

// L'affichage /////////////////////////////////////////////////////////////////

		echo "<form name='dateheures_rdv' action='rdvs.php3' method='GET'>";
		echo "<input type='hidden' name='action' value='attrib'>";
		if ($id_article) {
			echo "<input type='hidden' name='id_article' value='$id_article'>";
		}
		elseif ($id_rubrique) {
			echo "<input type='hidden' name='id_rubriquee' value='$id_rubrique'>";
		}

		bandeau_titre_boite2("D�but&nbsp;:");

		echo '<table border="0" align="center"><tr>';
		echo "<td style='text-align:right;'>Date&nbsp;:</td>";
		echo '<td><input name="date_debut" type="text" size="10" maxlength="10"'.
				(date_est_nul($date_debut) ? '></td>' : " value='$date_debut'></td>");
		echo "</tr><tr>";

		echo "<td style='text-align:right;'>Heure&nbsp;:</td>";
		echo '<td><input name="heure_debut" type="text" size="5" maxlength="5"'.
				(heure_est_nul($heure_debut) ? '></td>' : " value='".heures_minutes($heure_debut)."'></td>");
		echo "</tr></table>&nbsp;";

		bandeau_titre_boite2("Fin (facultatif)&nbsp;:");

		echo '<table border="0" align="center"><tr>';
		echo "<td style='text-align:right;'>Date&nbsp;:</td>";
		echo '<td><input name="date_fin" type="text" size="10" maxlength="10"'.
				(date_est_nul($date_fin) ? '></td>' : " value='$date_fin'></td>");
		echo "</tr><tr>";
		echo "<td style='text-align:right;'>Heure&nbsp;:</td>";
		echo '<td><input name="heure_fin" type="text" size="5" maxlength="5"'.
				(heure_est_nul($heure_fin) ? '></td>' : " value='".heures_minutes($heure_fin)."'></td>");
		echo "</tr></table>";

		echo '<p align="center"><input type="submit" value="Attacher">&nbsp
			<input type="button" name="detach" value="D�tacher"
			onClick="document.dateheures_rdv.action.value=\'annul\';submit();"></p>';
		echo "</form>";

?>
