<?php

define("_ROBOT_", "1");
$g_parse = array();

include('trad-liens/scripts/config_db.php');

function connect_db()
{
  global $host, $port, $db, $user, $pass;

  $hd = mysql_connect($host.":".$port, $user, $pass);
  if ($hd == false)
    {
      echo "impossible de se connecter a la base...\n";
      return false;
    }

  $query = "use ".$db;
  $result = mysql_query($query);
  if (!$result)
    {
      echo "base ".$db." non utilisable...\n";
      return false;
    }

  return true;
}

function close_db()
{
  mysql_close();
}

function rbok($rep)
{
  return (is_array($rep));
}


// utilisee pour permettre de passer des
// valeurs null quand une chaine n'est pas
// initialisee
function sb($str)
{
  if ($str=='')
    return "NULL";
  else
    return "'".$str."'";
}

function inserer_document($art_url,$art_titre, $art_auteur, $art_auteur_trans,
  $art_traducteur, $art_date, $art_id_orig, $art_type_acces, $art_type_article, $edition)
{
  $art_url = mysql_escape_string($art_url);
  $edition = mysql_escape_string($edition);
  $art_titre = mysql_escape_string($art_titre);
  $art_auteur = mysql_escape_string($art_auteur);
  $art_auteur_trans = mysql_escape_string($art_auteur_trans);
  $art_traducteur = mysql_escape_string($art_traducteur);
  $art_date = mysql_escape_string($art_date);
  $art_id_orig = mysql_escape_string($art_id_orig);
  $art_type_acces = mysql_escape_string($art_type_acces);
  $art_type_article = mysql_escape_string($art_type_article);

  $query = "INSERT INTO trad_item(id_lien, url, titre, auteur, auteur_trans, traducteur, ".
    "date, date_creation, type_acces, type_article, edition, id_orig) VALUES ".
    "(NULL, '".$art_url."', '".$art_titre."', ".sb($art_auteur).", ".sb($art_auteur_trans).", ".
    sb($art_traducteur).", ".sb($art_date).", now(), ".sb($art_type_acces).", ".
    sb($art_type_article).", ".sb($edition).", ".sb($art_id_orig).")";

  $result = mysql_query($query);
  if (!$result)
    return 'creation';

  return array(mysql_insert_id());
}


function analyse_rep($ret)
{
  $rep = array();

  $anstr = substr($ret, 0, 1024);
  if (!ereg("(.*)^HTTP/([0-1]\.[0-1]) ([0-9][0-9][0-9]) (.*)", $anstr, $match))
    return $rep;

  $rep['version'] = $match[2];
  $rep['statut'] = $match[3];
  $rep['corps'] = $match[4];

  return $rep;
}


function extrait_serveur($srv)
{
  $rep = array();

  if (!ereg("http://(.*)", $srv, $match))
    return false;

  $rep['srv'] = $match[1];

  // recup port
  if (!ereg("(.*):([0-9]*)(.*)", $rep['srv'], $match))
    $rep['port'] = 80;
  else
    {
      $rep['port'] = $match[2];
      $rep['srv'] = $match[1].$match[3];
    }

  // recup url
  if (ereg("([^/]*)/(.*)", $rep['srv'], $match))
    {
      $rep['srv'] = $match[1];
      $rep['url'] = "/".$match[2];
    }
  else
    $rep['url'] = "/";

  return $rep;
}


function dans_site($srv, $url)
{
  if (!ereg("http://(.*)", $url, $m))
    {
      $pc = $url[0];
      if ($pc=='/' || $pc=='#')
	return 1; 
      else 
	return 2;  // url relative
    }

  if (ereg("^".$srv."(.*)", $m[1], $n))
    return 1;  // url absolue dans site

  return 0;  // hors site
}



function http_get($server, $port, $url) 
{  
  $user_agent = "Mozilla/4.0 (compatible; MSIE 5.5; Windows 98)";
  
  /*echo "serveur=".$server."\n";
  echo "port=".$port."\n";
  echo "url=".$url."\n";*/

  $headers = "GET ".$url." HTTP/1.1\n";
  $headers .= "Host: $server\n";
  $headers .= "User-Agent: $user_agent\n";
  $headers .= "Accept: */*\n";
  $headers .= "Accept-Language: fr-en\n";
  $headers .= "Accept-Charset: ISO-8859-1\n";
  $headers .= "Keep-Alive: 60\n";
  $headers .= "Proxy-Connection: keep-alive\n";
  $headers .= "Content-Type: application/x-www-form-urlencoded\n";
  $headers .= "Cache-Control: no-cache\n\n";

  //echo $headers."\n";
  $fp = fsockopen($server, $port, $errno, $errstr);

  if (!$fp) {
    echo "impossible ouvrir socket\n";
    return false;
  }
  fputs($fp, $headers);
  
  $ret = "";

  while (!feof($fp))
    {
      $next = fgets($fp, 1024); 
      $ret.= $next;
      if (eregi("(.*)</html>(.*)", $next))
	break; 
      if (eregi("(.*)</body>(.*)", $next))
	break; 
    }
  
  fclose($fp);
  return $ret;  
}


function extrait_lien($page)
{
  global $module_lg;

  $ret = array();
  $patt_list = $module_lg->get_liens();
  
  while(list(,$patt) = each($patt_list))
    {
      //echo $patt."\n";
      $liens = array();
      if (preg_match_all($patt, $page, $liens, PREG_PATTERN_ORDER))
	{
	  while(list(,$lien)=each($liens[2]))
	    $ret[] = $lien;
	}
    }

  return $ret;
}


function non_parsee($srv, $port, $url)
{
  global $g_parse;

  //if (array_key_exists($srv.$url, $g_parse))
  if (isset($g_parse[$srv.$url]))  // php3
    return false;
  return true;
}


function parse($srv, $port, $url, $ref, $niv)
{
  global $g_parse, $module_lg;

  if ($niv == 0)
    return;

  if (dans_site($srv, $url) && non_parsee($srv, $port, $url))
    {
      echo "test...<".$url.">";
      $g_parse[$srv.$url] = '1';

      $page = http_get($srv, $port, $url);
      $rep = analyse_rep($page);

      echo "...".$rep['statut']."\n";
      if ($rep['statut'][0] != '2')
	return; // page erreur

      if ($module_lg->examine_page($page)==false)
	{
	  echo "page non article\n";
	}
      else
	{
	  echo "---------------------------------\n";
	  echo "url=<".$url.">\n";
	  echo "date=<".$_POST["art_date"].">\n";
	  echo "auteurs=<".$_POST["art_auteur"].">\n";
	  echo "titre=<".$_POST["art_titre"].">\n";
	  echo "edition=<".$module_lg->nom_edition().">\n";   
	  echo "---------------------------------\n";
	  
	  $res = inserer_document($url, $_POST["art_titre"], $_POST["art_auteur"], "", 
		  $_POST["art_traducteur"],  $_POST["art_date"], "", 
		  "public", "article", $module_lg->nom_edition());

	  if (rbok($res))
	    echo "insertion ok...\n";
	  else
	    echo "probleme lors de l'insertion\n";	  
	}

      $liens = extrait_lien($page);     
      while(list(,$lien)=each($liens))
	{
	  //echo $lien."\n";
	  $ds = dans_site($srv, $lien);
	  if ($ds==2) // url relative
	    $lien = $ref.$lien;
	  if ($ds!=0)
	    parse($srv, $port, $lien, $url, $niv-1);
	}

    }
}

// main
{
  @set_time_limit(0);

  if (!connect_db())
    exit();

  $d = dir(".");
  while ($entry=$d->read())
    {
      if (ereg("^module_(..).php$", $entry, $reg))
	{
	  $lg = $reg[1];
	  include "module_".$lg.".php";
	  echo "\nTRAVAIL sur ".$lg."\n";
	  if ($module_lg->option_robot()==true)
	    {
	      $conn = extrait_serveur($module_lg->url_site());
	      parse($conn['srv'], $conn['port'], $conn['url'], $conn['url'], -1);      
	    }
	  else
	    echo "...non robotisable\n";
	}
    }

  close_db();
  exit();
}


?>
