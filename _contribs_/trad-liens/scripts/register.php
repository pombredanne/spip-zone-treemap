<?php


	// creer_rubrique('/truc/machin/chose', $lang)
	function creer_rubrique($titre, $lang = '') {

		// eclater l'arborescence demandee
		$arbo = explode('/', preg_replace(',^/,', '', $titre));

		$id_parent = 0;
		foreach ($arbo as $titre) {
			$s = spip_query("SELECT id_rubrique, id_secteur FROM spip_rubriques
			WHERE titre = '".addslashes($titre)."'
			AND id_parent=".intval($id_parent));
			if (!$t = spip_fetch_array($s)) {
				spip_query("INSERT INTO spip_rubriques
				(titre, id_parent, statut, lang) VALUES
				('".addslashes($titre)."', $id_parent, 'prive', '$lang')");
				$id_rubrique = spip_insert_id();
				if ($id_parent > 0) {
					list($id_secteur) = spip_fetch_array(spip_query(
					"SELECT id_secteur FROM spip_rubriques
					WHERE id_rubrique=$id_rubrique"));
				} else
					$id_secteur = $id_rubrique;

				spip_query("UPDATE spip_rubriques SET id_secteur=$id_secteur
				WHERE id_rubrique=$id_rubrique");
			} else
				$id_rubrique = $t['id_rubrique'];

			// pour la recursion
			$id_parent = $id_rubrique;
		}

		return $id_rubrique;
	}


// verifier que l'auteur est enregistr� et lui proposer de changer de langue
// s'il en a plusieurs


// Appel de SPIP avec un squelette vide, permet d'avoir acc�s � la librairie
// de SPIP + connexion + authentication + compression etc.
$fond='vide';
$delais=0;
$flag_preserver = true;

$option_site = false;

define ("AUTORISE_VISU", "1");
include 'ecrire/inc_version.php';
include_spip('inc/cookie');

// demander un login au passant non identifi�
if (!$auteur_session) 
	redirige_par_entete(generer_url_public('login', 'url=index.php', true));

// Connecter � la base
spip_connect();
if (!$db_ok) {
	include_spip('inc/minipres');              
	install_debut_html ('Erreur base de donn&eacute;es');
	install_fin_html();
	exit;
}

/*
// chercher l'edition de cet auteur ; s'il y en a plusieurs, voir le cookie
$q = "SELECT * FROM spip_articles AS article, spip_auteurs_articles AS lien
	WHERE lien.id_auteur = ". $auteur_session['id_auteur']
	." AND article.id_secteur=1 AND article.statut='publie' AND lien.id_article=article.id_article";
*/

$q = "SELECT * FROM spip_rubriques WHERE id_parent=0";
$s = spip_query($q);

$num_res = spip_num_rows($s);

if ($num_res == 0) {
	include_spip('inc/minipres');
	install_debut_html ('Erreur base de donn&eacute;es');
	echo "Vous n'&ecirc;tes pas &eacute;diteur sur cette base";
	install_fin_html();
	exit;
}

// Compose le menu d'�dition si besoin, 
$menu_edition = '';
$jaja_code = "<script language=\"javascript\"><!-- 
function recharge() {
var ed = window.document.chg.var_edition;
parent.parent.location.href = './index.php?var_edition='+ed.options[ed.selectedIndex].value; } 
--></script>\n";
if ($num_res == 1) // cas le plus simple
{
	$row = spip_fetch_array($s);
	$edition = $row['titre'];
	$texte_edition = $row['texte'];
} else {
	while ($row = spip_fetch_array($s)) {
		// prendre la premi�re �dition (si cookie ou quoi on voit apr�s)
   	        if (!$edition) { $edition = $row['titre']; $texte_edition = $row['texte']; }

		if ($var_edition == $row['titre']) {			// changement demand� ?
			$selected = ' selected';
			$edition = $row['titre'];
			$texte_edition = $row['texte'];
			spip_setcookie('spip_edition', $edition);	// poser cookie
			$spip_edition = $var_edition;
		} else if (($spip_edition == $row['titre']) AND !$var_edition) {	// v�rifier le cookie
			$selected = ' selected';
			$edition = $row['titre'];
			$texte_edition = $row['texte'];
		} else {
			$selected = '';
		}

		$menu_edition .= '<option value="'.$row['titre'].'"'.$selected.'>'.$row['titre'].'</option>';
	}

	$menu_edition = '<select name="var_edition" OnChange="recharge()">'.$menu_edition.'</select>';
	$menu_edition = $jaja_code.'<form method="POST" action="./index.php" name="chg"><tr><td>'.$menu_edition.'</td></tr></form>';
}

// pas de cache !
@Header("Expires: 0");
@Header("Cache-Control: no-cache,must-revalidate");
@Header("Pragma: no-cache");

?>
