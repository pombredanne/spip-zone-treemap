<?php

function change_module($val)
{
  global $module;
  $module = $val;
  $_POST['module'] = $val;
  $_GET['module'] = $val;
}

function dispatche()
{
  global $module, $sous_module, $article_id, $frame;
  global $idx_selected_orig, $idx_selected_cible, $titre;
  global $extraire, $menu_edition, $message, $texte_edition, $nombre_lignes;
  global $consulter, $recuperer_x, $enregistrer, $revenir, $reload, $module_lg;

  global $edition, $mots_titre, $auteurs, $url, $url_site;
  global $num_ref, $date_pub, $duree_pub, $edition_cible;
  global $duree_modif, $non_lie, $edition_non_lie, $type_article;
  global $article_id_orig, $article_id_cible, $bout_lier, $bout_delier, $limit_marquage;

  global $art_date, $art_url, $art_edition;
  global $art_titre, $art_auteur, $art_traducteur, $art_auteur_trans;
  global $art_type_acces, $art_id_orig, $art_type_article;

  $page = '';
  $erreur = '';

  if (($module=='orig') || ($module=='cible'))
    {
      if (!empty($consulter))
	change_module('consulter');
    }

  if ($module=='enregistrer')
    {
      if (empty($reload))
	{
	  if (!empty($enregistrer))
	    {
	      $erreur = modifier_document();
	      change_module('modifier');
	    }
	  else if (!empty($bout_delier))
	    change_module('delier');
	  else if (!empty($revenir))
	    change_module('orig');
	}
      else
	change_module('modifier');
    }

  if ($module=='creer')
    {  
      if (!empty($recuperer_x))
	{
	  // recuperation des donnees et incrustation dans la page
	  $hdl = extrait_serveur($url_site.$art_url);
	  $page = http_get($hdl['srv'], $hdl['port'], $hdl['url']);
	  $stat = analyse_rep_http($page);

	  if ($stat['statut'] == '401')  // page protegee
	    $_POST['art_type_acces'] = 'protege';
	  else if ($stat['statut'][0] == 2)
	    $module_lg->examine_page($page);  // fonction dependante du module
	  
	  change_module('creer');  
	}
      else if (!empty($enregistrer)) // insertion nouv. document
	{
	  $erreur = inserer_document($art_url,$art_titre, $art_auteur, 
	     $art_auteur_trans, $art_traducteur, $art_date, 
	     $art_id_orig, $art_type_acces, $art_type_article, $edition);

	  if (!rbok($erreur))
	    change_module('creer');
	  else
	    change_module('orig');
	}
      else if (!empty($revenir))
	change_module('orig');
      else
        change_module('creer');
    }

  if (($article_id==0) && (!empty($frame)) && 
      (($module=='consulter') || ($module=='modifier'))) // cas clic apres pas de reponse
    change_module($frame);

  $liste_type_article = get_liste_type_article();
  $liste_type_acces = get_liste_type_acces();
  $liste_mois = get_liste_mois();
  $liste_annees = get_liste_annees();

  switch($module)
    {

    case 'orig':
      $page = "accueil_orig";

      if (!rbok($liste_dates = get_liste_dates($edition)))
	{ $erreur = $liste_dates; break; }
      if (!rbok($liste_pub_depuis = get_liste_pub_depuis()))
        { $erreur = $liste_pub_depuis; break; }
      if (!rbok($liste_modif_depuis = get_liste_modif_depuis()))
	{ $erreur = $liste_modif_depuis; break; }

      if (!rbok($liste_edition = get_liste_edition_orig($edition)))
	{ $erreur = $liste_edition; break; }

      if (empty($date_pub))
	init_orig();

#      // recup. de tous ceux qui ont ete recemment modifie
#      if (!rbok($articles_rec = get_documents($edition, '', '', '', '', '', '', 
#	      $limit_marquage, '', '', '', $edition)))
#        { $erreur = $articles_rec; break; }

      if (!rbok($articles = get_documents($edition, $mots_titre, $auteurs, $url,
	  $num_ref, $date_pub, $duree_pub, $duree_modif, $non_lie, 
	  $edition_non_lie, $type_article, $edition)))
        { $erreur = $articles; break; }

      $articles = formate_liste($articles, $articles_rec);
      
      if (empty($article_id))
	$article_id = $articles[0]['id'];


      nettoie_ext_variables();

      $articles = nettoie_variables($articles);
      $articles_rec = nettoie_variables($articles_rec);
      break;

    case 'cible':
      $page = "accueil_cible";
      if (!rbok($liste_dates = get_liste_dates($edition_cible)))
	{ $erreur = $liste_dates; break; }
      if (!rbok($liste_pub_depuis = get_liste_pub_depuis()))
	{ $erreur = $liste_pub_depuus; break; }
      if (!rbok($liste_modif_depuis = get_liste_modif_depuis()))
	{ $erreur = $liste_modif_depuis; break; }

      if (!rbok($liste_edition = get_liste_edition_cible($edition)))
	{ $erreur = $liste_edition; break; }

      if (empty($date_pub))
	init_cible();

      $articles_rec = array();
      /*
        if (!rbok($articles_rec = get_documents('--', '', '', '', '', '', '', 
	      $limit_marquage, '', '', '', $edition)))
	{ $erreur = $articles_rec; break; }
      */

      if (!rbok($articles = get_documents($edition_cible, $mots_titre, 
	  $auteurs, $url, $num_ref, $date_pub, $duree_pub, $duree_modif, 
	  $non_lie, $edition, $type_article, $edition)))
	{ $erreur = $articles; break; }

      $articles = formate_liste($articles, $articles_rec);
      if (empty($article_id))
	$article_id = $articles[0]['id'];

      nettoie_ext_variables();
      $articles = nettoie_variables($articles);
      break;

    case 'bas':
      $page = "accueil_bas";
      break;

    case 'consulter':
      $page = "consulter";

      if (!rbok($article = get_article($article_id)))
	{ $erreur = $article; break; }
      if (!rbok($liens = get_liens($article_id)))
	{ $erreur = $liens; break; }
      if (!rbok($lies = get_articles_lies($article_id)))
	{ $erreur = $lies; break; }
      if (!rbok($ed_liees = get_editions($lies)))
	{ $erreur = $ed_liees; break; }
      if (!rbok($ids_lies = get_ids($lies)))
	{ $erreur = $ids_lies; break; }

      nettoie_ext_variables();
      $article = nettoie_variables($article);
      $liens = nettoie_variables($liens);
      break;

    case 'modifier':
      $page = "modifier";
      $rafraichir = false;

      if (!rbok($article = get_article($article_id)))
	{ $erreur = $article; break; }
      if (!rbok($liens = get_liens($article_id)))
	{ $erreur = $liens; break; }
      if (!rbok($lies = get_articles_lies($article_id)))
	{ $erreur = $lies; break; }
      if (!rbok($ed_liees = get_editions($lies)))
	{ $erreur = $ed_liees; break; }
      if (!rbok($ids_lies = get_ids($lies)))
	{ $erreur = $ids_lies; break; }

      $article = nettoie_variables($article);
      $liens = nettoie_variables($liens);
      nettoie_ext_variables();
      $liste_type_article = get_liste_type_article(false);
      break;
     
    case 'creer':
      $page = "nouveau";

      if (empty($art_date))
	$_POST['art_date'] = date('Y-m-d');

      if (!rbok($site = get_site($edition)))
	{ $erreur = $site; break; }
      
      $liste_type_article = get_liste_type_article(false);
      nettoie_ext_variables();
      break;
      
    case 'lier':
      $page = "accueil_bas";
      $rafraichir = true;

      $liste_type_article = get_liste_type_article(false);
      if (!rbok($ret = lier($article_id_orig, $article_id_cible)))
	$erreur = $ret;
      break;
      
    case 'delier':
      $rafraichir = true;
      $page = "modifier";

      // on delie
      if (!rbok($ret = delier($article_id)))
	{ $erreur = $ret; break; }

      // puis on fait comme modifier
      if (!rbok($article = get_article($article_id)))
	{ $erreur = $article; break; }
      if (!rbok($liens = get_liens($article_id)))
	{ $erreur = $liens; break; }
      if (!rbok($lies = get_articles_lies($article_id)))
	{ $erreur = $lies; break; }
      if (!rbok($ed_liees = get_editions($lies)))
	{ $erreur = $ed_liees; break; }
      if (!rbok($ids_lies = get_ids($lies)))
	{ $erreur = $ids_lies; break; }

      $liste_type_article = get_liste_type_article(false);
      $article = nettoie_variables($article);
      $liens = nettoie_variables($liens);
      nettoie_ext_variables();
      break;

    case 'scanner':
      $page = "scanner";
      @set_time_limit(0);
      $statut = $module_lg->scanne();
      break;

    case 'stats':
      $page = "stats";
      @set_time_limit(0);
      $lann = recup_annees($edition); 
      sort($lann); reset($lann);  
      $res = array();
      while(list(,$ann)=each($lann))
	{
	  $liste = recup_stat($ann[0], $edition);
	  while(list(,$row)=each($liste))
	    {	
	      $dates = sprintf("%04d-%02d", $row["annee"], $row["mois"]);
	      $compte = intval($row["compte"]);

	      if (!is_array($res[$dates]))
		$res[$dates] = array();

	      if ($row["type_article"]=="hors_sommaire")
		$res[$dates][0] += 1;
	      else
	      if ($row["type_article"]=="masque")
		$res[$dates][-1] += 1;
	      else
		{
		  $res[$dates][$compte] += 1;	
		}
	    }
	}
      ksort($res);
      break;

    case 'message':
      $page = "message";
      break;
    }


  include("./trad-liens/html/header.php");

  if ($page!='')
    include("./trad-liens/html/".$page.".php");
  include("./trad-liens/html/footer.php");

}


function formate_liste($articles, $articles_rec)
{
  if ((count($articles)+count($articles_rec))==0)
    {
      $articles[0]['titre'] = "pas de r&eacute;ponse...";
      $articles[0]['id'] = 0;
    }
  return $articles;
}


function champs_caches_recherche($module)
{
  if ($module=='orig')
    {
      global $mots_titre, $auteurs, $url, $article_id;
      global $num_ref, $date_pub, $duree_pub, $idx_selected_orig, $idx_selected_cible;
      global $duree_modif, $non_lie, $edition_non_lie, $type_article;
      
      $str="";
      $str .= "<input type='hidden' name='mots_titre' value='".$mots_titre."'>\n";
      $str .= "<input type='hidden' name='auteurs' value='".$auteurs."'>\n";
      $str .= "<input type='hidden' name='url' value='".$url."'>\n";
      $str .= "<input type='hidden' name='num_ref' value='".$num_ref."'>\n";
      $str .= "<input type='hidden' name='type_article' value='".$type_article."'>\n";
      $str .= "<input type='hidden' name='date_pub' value='".$date_pub."'>\n";
      $str .= "<input type='hidden' name='duree_pub' value='".$duree_pub."'>\n";
      $str .= "<input type='hidden' name='duree_modif' value='".$duree_modif."'>\n";
      $str .= "<input type='hidden' name='non_lie' value='".$non_lie."'>\n"; 
      $str .= "<input type='hidden' name='edition_non_lie' value='".$edition_non_lie."'>\n";
      $str .= "<input type='hidden' name='idx_selected_orig' value='".$idx_selected_orig."'>\n";
      $str .= "<input type='hidden' name='idx_selected_cible' value='".$idx_selected_cible."'>\n";
      return $str;
    }
  else 
    {
      global $mots_titre, $auteurs, $url, $article_id;
      global $num_ref, $date_pub, $duree_pub, $edition_cible, $type_article;
      global $duree_modif, $non_lie, $idx_selected_cible, $idx_selected_orig;
      
      $str="";
      $str .= "<input type='hidden' name='mots_titre' value='".$mots_titre."'>\n";
      $str .= "<input type='hidden' name='auteurs' value='".$auteurs."'>\n";
      $str .= "<input type='hidden' name='url' value='".$url."'>\n";
      $str .= "<input type='hidden' name='num_ref' value='".$num_ref."'>\n";
      $str .= "<input type='hidden' name='date_pub' value='".$date_pub."'>\n";
      $str .= "<input type='hidden' name='duree_pub' value='".$duree_pub."'>\n";
      $str .= "<input type='hidden' name='edition_cible' value='".$edition_cible."'>\n";
      $str .= "<input type='hidden' name='type_article' value='".$type_article."'>\n";
      $str .= "<input type='hidden' name='non_lie' value='".$non_lie."'>\n"; 
      $str .= "<input type='hidden' name='duree_modif' value='".$duree_modif."'>\n";
      $str .= "<input type='hidden' name='idx_selected_cible' value='".$idx_selected_cible."'>\n";
      $str .= "<input type='hidden' name='idx_selected_orig' value='".$idx_selected_orig."'>\n";
      return $str;
    }
}


function init_orig()
{
  global $mots_titre, $auteurs, $url, $edition;
  global $num_ref, $date_pub, $duree_pub, $idx_selected_orig;
  global $duree_modif, $non_lie, $edition_non_lie, $type_article;
 
  $mots_titre = "";
  $auteurs = "";
  $url = "";
  $num_ref = "";

  if (empty($date_pub)) // introduit pour traiter retour de stats
    $date_pub = "--";

  $duree_pub = "--";
  $duree_modif = "--"; //24*3600;  // 1 jours
  $non_lie = "on";
  $type_article = 'article';

  if ($edition=='fr')
    $edition_non_lie = "en"; 
  else
    $edition_non_lie = "fr";

  $idx_selected_orig = 0;
}


function init_cible()
{
  global $mots_titre, $auteurs, $url, $idx_selected_cible;
  global $num_ref, $date_pub, $duree_pub, $edition_cible, $type_article;
  global $duree_modif, $non_lie, $edition_non_lie, $edition;
 
  $mots_titre = "";
  $auteurs = "";
  $url = "";
  $num_ref = "";
  $date_pub = "--";
  $duree_pub = "--";
  $duree_modif = "--"; //24*3600;  // 1 jours
  $non_lie = "on";
  $type_article = 'article';

  // comme edition_non_lie de
  // origine
  if ($edition=='fr')
    $edition_cible = "en"; 
  else
    $edition_cible = "fr";

  $idx_selected_cible = 0;
}


function get_liste_type_acces()
{
  $ret = array();

  $ret['public'] = 'Public';
  $ret['protege'] = 'Prot&eacute;g&eacute;';

  return $ret;
}

function get_liste_type_article($compl=true)
{
  $ret = array();

  if ($compl)
    $ret['--'] = "--";

  $ret['article'] = 'Article';
  $ret['hors_sommaire'] = 'Hors Sommaire';
  $ret['masque'] = 'Masqu&eacute;';

  return $ret;
}

function get_liste_pub_depuis()
{
  $ret = array();

  $ret['--'] = "--";
  $ret['1 an'] = 365;
  $ret['6 mois'] = 183;
  $ret['3 mois'] = 92;
  $ret['2 mois'] = 61;
  $ret['1 mois'] = 31;
  $ret['15 jours'] = 15;
  $ret['1 semaine'] = 7;
  $ret['1 jour'] = 1;

  return $ret;
}

function get_liste_modif_depuis()
{
  $ret = array();

  $ret['--'] = "--";
  $ret['1 mois'] = 365*2*3600;
  $ret['15 jours'] = 24*15*3600;
  $ret['1 semaine'] = 7*24*3600;
  $ret['1 jour'] = 24*3600;
  $ret['4 heures'] = 4*3600;
  $ret['1 heure'] = 3600;

  return $ret;
}

function get_liste_mois()
{
  $mois = array();
  for ($i=1; $i<=12; $i++)
    $mois[] = sprintf("%02d", $i);
  return $mois;
}


function get_liste_annees()
{
  $annee = array();
  for ($i=date('Y'); $i>=1980; $i--)
    $annee[] = sprintf("%04d", $i);
  return $annee;
}

function formate_message($erreur)
{
  switch($erreur)
    {
    case 'non_liable':
      return "Attention, ces articles ne peuvent pas &ecirc;tre li&eacute;s ensemble : l'un d'eux au moins ".
	"est d&eacute;j&agrave; li&eacute; avec un article de la m&ecirc;me &eacute;dition que l'autre.";
      break;
    case 'non_deliable':
      return "Attention, cet article ne peut &ecirc;tre d&eacute;li&eacute; car il n'est pas li&eacute;.";
      break;
    case 'creation':
      return "Erreur de cr&eacute;ation; v&eacute;rifier qu'un article ayant cette URL ou ".
	"cet identifiant n'existe pas d&eacute;j&agrave dans la base. <br>".mysql_error();
      break;

    // ajout Fil pour migration alan.rezo.net
    case 'technique':
	return "Erreur technique, probablement dans la connexion MySQL.";
	break;

    default:
      return "Erreur : impossible de cr&eacute;er cet enregistrement dans la base.".
        "Un enregistrement ayant d&eacute;j&agrave; cet URL ou ce num&eacute;ro de r&eacute;f. existe. ".mysql_error();
      break;
    }
}



?>
