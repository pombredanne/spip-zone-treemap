<?php


function rbok($rep)
{
  return (is_array($rep));
}


// utilisee pour permettre de passer des
// valeurs null quand une chaine n'est pas
// initialisee
function sb($str)
{
#  if ($str=='')
#    return "NULL";
#  else
    return "'".$str."'";  
}



function inserer_document($art_url,$art_titre, $art_auteur, $art_auteur_trans,
  $art_traducteur, $art_date, $art_id_orig, $art_type_acces, $art_type_article, $edition)
{
  $art_url = mysql_escape_string($art_url);
  $id_edition = secteur_edition($edition);
  $art_titre = mysql_escape_string($art_titre);
  $art_auteur = mysql_escape_string($art_auteur);
 # $art_auteur_trans = mysql_escape_string($art_auteur_trans);
  $art_traducteur = mysql_escape_string($art_traducteur);
  $art_date = mysql_escape_string($art_date);
#  $art_id_orig = mysql_escape_string($art_id_orig);
#  $art_type_acces = mysql_escape_string($art_type_acces);
#  $art_type_article = mysql_escape_string($art_type_article);

  $id_rubrique = creer_rubrique(
  	'/'.$edition.
  	'/'.substr($art_date, 0, strlen('2006')).
  	'/'.substr($art_date, 0, strlen('2006-01')));

  $query = "INSERT INTO spip_articles(id_trad, url_site, titre, surtitre, ps, ".
    "date, id_secteur, id_rubrique, statut') VALUES ".
    "(0, '".$art_url."', '".$art_titre."', ".sb($art_auteur).", ".
    sb($art_traducteur).", ".sb($art_date).
    ", ".sb($id_edition).", ".sb($id_rubrique).", 'publie')";

  $result = spip_query($query);
  if (!$result)
    return 'creation';
 
  return array(mysql_insert_id());
}

// Une edition == un secteur
// un administrateur de ce secteur est administrateur de l'edition
function get_site($edition)
{
  $edition = mysql_escape_string($edition);

  $query = "SELECT id_rubrique as id, titre as edition, descriptif as url_site
  FROM spip_rubriques WHERE titre='".$edition."' AND id_parent=0";

  $result = spip_query($query);
  if (!$result)
    return 'technique';

  $ret = array();
  while($row = spip_fetch_array($result))
    $ret[] = $row;

  return $ret;
}


function modifier_document()
{
  global $art_url,$art_titre, $art_auteur, $art_traducteur, $article_id, $art_type_article;
  global $art_date, $art_id_orig, $art_type_acces, $art_auteur_trans;

  $art_url = mysql_escape_string($art_url);
  $art_titre = mysql_escape_string($art_titre);
  $art_auteur = mysql_escape_string($art_auteur);
#  $art_auteur_trans = mysql_escape_string($art_auteur_trans);
  $art_traducteur = mysql_escape_string($art_traducteur);
  $art_date = mysql_escape_string($art_date);
#  $art_id_orig = mysql_escape_string($art_id_orig);
#  $art_type_acces = mysql_escape_string($art_type_acces);
#  $art_type_article = mysql_escape_string($art_type_article);
  $article_id = mysql_escape_string($article_id);

  $query = "UPDATE spip_articles SET ";

  $query .= "url_site='".$art_url."', ".
    "maj=null,".
    "titre='".$art_titre."', ".
    "surtitre=".sb($art_auteur).", ".
    "date=".sb($art_date).", ".
  #  "type_acces='".$art_type_acces."',".
  #  "type_article='".$art_type_article."',".
    "ps=".sb($art_traducteur)." ";

#  if (!empty($art_id_orig))
#    $query .= ", id_orig=".sb($art_id_orig)." ";

  $query .= " WHERE id_article=".sb($article_id);

  $result = spip_query($query);
  if (!$result)
    return 'technique';
 
  return array();
}


function get_documents(
  $edition_cible,
  $mots_titre,
  $auteurs,
  $url,
  $num_ref,
  $date_pub,
  $duree_pub,
  $duree_modif,
  $non_lie,
  $edition_non_lie,
  $type_article,
  $edition_courante='')
{
  global $limit;

  $edition_courante = secteur_edition($edition_courante);
  $edition_cible = secteur_edition($edition_cible);
  $mots_titre = mysql_escape_string($mots_titre);
  $auteurs = mysql_escape_string($auteurs);
  $url = mysql_escape_string($url);
#  $num_ref = mysql_escape_string($num_ref);
  $date_pub = mysql_escape_string($date_pub);
 # $duree_pub = mysql_escape_string($duree_pub);
#  $duree_modif = mysql_escape_string($duree_modif);
  $non_lie = mysql_escape_string($non_lie);
  $edition_non_lie = mysql_escape_string($edition_non_lie);
 # $type_article = mysql_escape_string($type_article);
  
  $tous = false;

  if ($non_lie=='on') 
    {
      // dans ce cas, on commence par faire une table temporaire
      // de ceux qu'on ne veut pas
      $query = "create temporary table if not exists tmp (id_trad bigint(21) ".
	"not null, key id_trad (id_trad))";
      if (!spip_query($query))
	return 'technique';

      $query = "delete from tmp";
      if (!spip_query($query))
	return 'technique';

      $query = "insert into tmp select id_trad from spip_articles where id_secteur=".$edition_courante." ".
	"and id_trad>0";
      if (!spip_query($query))
	return 'technique';

      $query = "select straight_join t1.* from spip_articles t1 left join tmp t2 on ".
	"t1.id_trad=t2.id_trad where t2.id_trad IS NULL ";
    }
  else
    $query = "select straight_join t1.* from spip_articles t1 ".
      " where '1==1' ";


  if ($edition_cible)
    $query .= " AND t1.id_secteur=".$edition_cible;
  else if ($edition_courante)
    $query .= " AND t1.id_secteur!=".$edition_courante;

  if (!empty($mots_titre))
    {
      if ($mots_titre=="--")
	$query .= " AND t1.titre = ''";
      else
	{
	  $lt = explode(" ", $mots_titre);
	  reset($lt);
	  while(list(,$it)=each($lt))
	    $query .= " AND t1.titre like '%".$it."%'";
	}
    }
  
  if (!empty($auteurs))
    {
      if ($auteurs=="--")
	$query .= " AND t1.surtitre = ''";
      else
	{
	  $lt = explode(" ", $auteurs);
	  reset($lt);
	  while(list(,$it)=each($lt))
	    $query .= " AND (t1.surtitre like '%".$it."%' OR t1.ps like '%".$it."%')";
	}
    }
  
  if (!empty($url))
    {
      $lt = explode(" ", $url);
      reset($lt);
      while(list(,$it)=each($lt))
	$query .= " AND t1.url_site like '%".$it."%'";
    }

/*  if (!empty($type_article))
    {
      if ($type_article=='--')
	$query .= " AND t1.type_article!='masque'";
      else if ($type_article=='article')
	$query .= " AND t1.type_article='article'";
      else if ($type_article=='hors_sommaire')
	$query .= " AND t1.type_article='hors_sommaire'";
      else if ($type_article=='masque')
	$query .= " AND t1.type_article='masque'";
    }
*/
/*  if (!empty($num_ref))
    {
      if ($num_ref=="--")
	$query .= " AND (isnull(t1.id_orig) OR (t1.id_orig='".$num_ref."'))";
      else
	$query .= " AND t1.id_orig='".$num_ref."'";
    }
  */

  if (!empty($date_pub) && ($date_pub!='--'))
    $query .= " AND t1.date like '".$date_pub."%'";

/*  if (!empty($duree_pub) && ($duree_pub!='--'))
    $query .= " AND TO_DAYS(now())-TO_DAYS(t1.date)<='".$duree_pub."'";
*/
 /* if ($duree_modif!='--')
    $query .= " AND unix_timestamp(current_timestamp())-unix_timestamp(t1.maj)<='".$duree_modif."'";
*/

  $query .= " order by titre";

  $query .= " limit ".$limit;

#var_dump($query);

  $result = spip_query($query);
  if (!$result)
    return 'technique';

  $ret = array();
  while($row = spip_fetch_array($result))
    $ret[$row['id_article']] = $row;

  if ($non_lie=='on') 
    {
      $query = "drop table tmp";
      spip_query($query);
    }

  return $ret;
}



// recupere toutes les dates de
// la base. si l'edition est initialisee
// ne recupere que celles ayant des
// articles dans cette edition
function get_liste_dates($edition='')
{
  $edition = mysql_escape_string($edition);

  $supp = "";
  if ($edition!="" && $edition!="--")
    {
      $edition = secteur_edition($edition);
      $supp = "where id_secteur=".$edition." ";
    }

  $query = "select distinct date from spip_articles ".$supp."order by date desc";

  $result = spip_query($query);
  if (!$result)
    return 'technique';
  
  $ret = array();
  $ret["--"] = "--";
  $ann = "";
  while($row = spip_fetch_array($result))
    {
      if (!ereg("([0-9][0-9][0-9][0-9])-([0-9][0-9])-([0-9][0-9])", $row[0], $match))
	continue;

      if ($ann!=$match[1])
	{
	  $ret[$match[1]] = $match[1];
	  $ann = $match[1];
	}

      $ret[$match[1]."-".$match[2]] = " ".$match[1]."-".$match[2];
    }

  return $ret;
}


function get_liste_edition_cible($edition_excl)
{
  $tab['--'] = '--';

  $ret = get_liste_edition_orig($edition_excl);
  if (!rbok($ret)) 
    return $ret;

  return (array_merge($tab, $ret));
}


// va chercher la liste de editions
// dans les tables SPIP
function get_liste_edition_orig($edition_excl)
{
  $edition_excl = mysql_escape_string($edition_excl);

  $query = "SELECT id_rubrique as id, titre as edition, descriptif as url_site ".
    "FROM spip_rubriques WHERE titre!='".$edition_excl."' AND id_parent=0";

  $result = spip_query($query);
  if (!$result)
    return 'technique';

  $ret = array();
  while($row = spip_fetch_array($result))
    $ret[] = $row['edition'];

  return $ret;
}


function get_article($id)
{
  $id = intval($id);

  $query = "SELECT t.* FROM spip_articles t ".
    "WHERE  t.id_article=".$id;
  $ret = array();
  $result = spip_query($query);
  if (!$result)
    return 'technique';

  $ret = spip_fetch_array($result);  
  $ret['edition'] = edition_secteur($ret['id_secteur']);
  return $ret;
}


function get_article_orig($id_orig, $edition)
{

die("erreur dans get_article_orig : id_orig");

  $id_orig = mysql_escape_string($id_orig);
  $edition = secteur_edition($edition);

  $query = "SELECT t.* FROM spip_articles t ".
    "WHERE t.id_secteur='".$edition."' AND t.id_orig='".$id_orig."'";

  $ret = array();
  $result = spip_query($query);
  if (!$result)
    return 'technique';

  $ret = spip_fetch_array($result);  
  return $ret;
}


function get_liens($id)
{
  $id = intval($id);

  $query = "SELECT t2.* FROM spip_articles t1, spip_articles t2 ".
    "WHERE t1.id_trad>0 AND t1.id_article=".$id." AND t1.id_trad=t2.id_trad ".
    "AND t2.id_article!=".$id;

  $result = spip_query($query);
  if (!$result)
    return 'technique';

  $ret = array();
  while($row = spip_fetch_array($result)) {
    $row['edition'] = edition_secteur($row['id_secteur']);
    $ret[] = $row;
  }

  return $ret;
}


function get_articles_lies($article_id)
{
  $article_id = intval($article_id);

  list($id_trad) = spip_fetch_array(spip_query("SELECT id_trad
  FROM spip_articles
  WHERE id_article = $article_id"));

  if ($id_trad > 0)
	  $query = "select * from spip_articles
	  WHERE id_trad = $id_trad
	  AND id_article <> ".$article_id;
  else
  	$query = "SELECT * FROM spip_articles WHERE 1=0";

  $result = spip_query($query);
  if (!$result)
    return 'technique';

  $ret = array();
  while($row = spip_fetch_array($result)) {
    $row['edition'] = edition_secteur($row['id_secteur']);
    $ret[] = $row;
  }

  return $ret;
}


// la fonction suivante est a utiliser  sur 
// le resultat de get_articles_lies (pour 
// recuperer les editions d'une liste d'article)
function get_editions($liste_article)
{
  $ret = array();
  reset($liste_article);
  while(list($key,$article)=each($liste_article))
    $ret[] = $article['edition'];

  return $ret;
}

function secteur_edition($nom) {
	$s = spip_query($q = "SELECT id_rubrique FROM spip_rubriques
	WHERE titre='".addslashes($nom)."' AND id_parent=0");
	$s = spip_fetch_array($s);
	return intval($s['id_rubrique']);
}
function edition_secteur($id_secteur) {
	$s = spip_query($q = "SELECT titre FROM spip_rubriques
	WHERE id_rubrique=".intval($id_secteur)." AND id_parent=0");
	$s = spip_fetch_array($s);
	return $s['titre'];
}

// la fonction suivante est a utiliser  sur 
// le resultat de get_articles_lies (pour 
// recuperer les ids d'une liste d'article)
function get_ids($liste_article)
{
  $ret = array();
  reset($liste_article);
  while(list($key,$article)=each($liste_article))
    $ret[] = $article['id_article'];
  return $ret;
}


// verifie si 2 articles sont liables. Si oui,
// renvoie array(), sinon renvoie 'non_liable'
function sont_liables($id1, $id2)
{
  if (!rbok($lies1 = get_articles_lies($id1)))
    return $lies1;
  if (!rbok($lies2 = get_articles_lies($id2)))
    return $lies2;

  if (!rbok($eds1 = get_editions($lies1)))
    return $eds1;
  if (!rbok($eds2 = get_editions($lies2)))
    return $eds2;

  if (count(array_intersect($eds1, $eds2)) != 0)
    return 'non_liable';
  return array();
}


function lier($id1, $id2)
{
  $id1 = intval($id1);
  $id2 = intval($id2);

  if (!rbok($art1 = get_article($id1)))
    return $art1;
  if (!rbok($art2 = get_article($id2)))
    return $art2;

  // verifie si les 2 articles sont lies
  $stat_liables = sont_liables($id1, $id2);
  if (!rbok($stat_liables))
    return $stat_liables;

  $lien1 = intval($art1['id_trad']); $lien2 = intval($art2['id_trad']);

  // s'ils etaient delies, c'est comme s'ils etaient vierges
  if ($lien1<0) $lien1=0;
  if ($lien2<0) $lien2=0;

  if (!$lien1 AND !$lien2)
    {
      $lien = min($id1, $id2);
      $query = "update spip_articles set id_trad=".$lien." where id_article=".$id2." or id_article=".$id1;
    }

  // dans les 2 cas suivants, ca ne sert a rien de faire l'update sur les 2 ids
  // mais on le fait quand meme pour remette a jour le champs "modif" des 2.

  else if ($lien1 AND !$lien2)
    $query = "update spip_articles set id_trad=".$lien1." where id_article=".$id2." or id_article=".$id1;

  else if (!$lien1 AND $lien2)
    $query = "update spip_articles set id_trad=".$lien2." where id_article=".$id1." or id_article=".$id2;

  else  // tous 2 non nuls
    {
      $lien = min($lien1, $lien2);
      $query = "update spip_articles set id_trad=".$lien." where id_trad=".$lien1." or id_trad=".$lien2;
    }

  $result = spip_query($query);
  if (!$result)
    return 'technique';

  return array();
}


function delier($id)
{
  $id = intval($id);

  $art = get_article($id);
  if (!rbok($art))
    return $art;

  $lien = $art['id_trad']; 
  if ($lien=='')
    return 'non_deliable';

  $query = "select id_article from spip_articles where id_trad=".$lien;

  $result = spip_query($query);
  if (!$result)
    return 'technique';

  $num = spip_num_rows($result);

  if ($num <= 2)
    {
      $query = "update spip_articles set id_trad=0 where id_trad=".$lien;
      $result = spip_query($query);
      if (!$result)
	return 'technique';
      return array();
    }
  else
    {
      $id2 = $id;
      while (($row = spip_fetch_array($result)) && ($id==$id2))
	$id2 = $row['id_article'];

      if ($id2==$id)
	return 'technique';
    }

  $nouv_lien = $lien;

  // si le lien detruit avait pour id celui de id1, on change
  if (($lien==$id) || ($lien==-$id))
    $nouv_lien = $id2;
  
  // passe en negatif (pour signaler deliage a l'admin)
  if ($nouv_lien>0)  
    $nouv_lien = -$nouv_lien;

  // remet a jour les liens des autres lies
  $query = "update spip_articles set id_trad=".$nouv_lien." where id_trad=".$lien;
  $result = spip_query($query);
  if (!$result)
    return 'technique';

  // delie defintiv. l'id1
  $query = "update spip_articles set id_trad=0 where id_article=".$id;  
  $result = spip_query($query);
  if (!$result)
    return 'technique';

  return array();
}


// fonction pour tester la presence
// du document. renvoie 1 si le document
// est deja present par son url, 2 s'il
// est present par son id et 0 si non present.
function test_document($edition, $url, $id_orig)
{
  $edition = secteur_edition($edition);
  $url = mysql_escape_string($url);
  $id_orig = mysql_escape_string($id_orig);

  $query = "select * from spip_articles where id_secteur='".$edition."' and url_site='".$url."'";
  $result = spip_query($query);
  if (spip_num_rows($result) != 0)
    return 1;

  $query = "select * from spip_articles where id_secteur='".$edition."' and id_orig='".$id_orig."'";
  $result = spip_query($query);
  if (spip_num_rows($result) != 0)
    return 2;

  return 0;
}


function optimise_table()
{
  $query = "OPTIMIZE TABLE spip_articles";
  $result = spip_query($query);
}


// renvoie une liste contenant
// les annees des articles de la base
// pour une edition donnee
function recup_annees($langue)
{
  $langue = secteur_edition($langue);
  $query = "select distinct left(date,4) from ".
	"spip_articles where id_secteur=".$langue;

  $result = spip_query($query);
  if (!$result)
    return 'technique';

  $ret = array();
  while($row = spip_fetch_array($result))
    $ret[] = $row;

  return $ret;
}


function recup_stat($annee, $langue)
{
  $annee = mysql_escape_string($annee);
  $langue = secteur_edition($langue);

  // 1 - recupere les articles lies 

  $query = "select t1.titre as titre, YEAR(t1.date) as annee,MONTH(t1.date) as mois,count(t2.id_trad) as compte from spip_articles t1,spip_articles t2 where YEAR(t1.date)=".$annee." and t1.id_trad>0 and t1.id_trad=t2.id_trad and t1.id_secteur=".$langue." group by t2.id_trad order by annee,mois,titre";

  $result = spip_query($query);
  if (!$result)
    return 'technique';

  $ret = array();
  while($row = spip_fetch_array($result))
    $ret[] = $row;

  // 2 - recupere les articles non lies

  $query = "select t1.titre as titre, YEAR(t1.date) as annee,MONTH(t1.date) as mois,1 as compte from spip_articles t1 where YEAR(t1.date)=".$annee." and t1.id_trad>0 and t1.id_secteur=".$langue." order by annee,mois,titre";

  $result = spip_query($query);
  if (!$result)
    return 'technique';

  while($row = spip_fetch_array($result))
    $ret[] = $row;

  return $ret;
}

?>
