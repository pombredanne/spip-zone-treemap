<?php

if (defined("_MODULE_PHP_"))
  return;
define("_MODULE_PHP_", "1");

class module {
  
  var $nom_module;
  var $url_info;
  var $liens;

  function module($nom, $url) {
    $this->nom_module = $nom;
    $this->url_info = $url;
    $this->liens = array( "/<a (.*)href=[ ]*[\"'](.*)[\"'](.*)>/Ui",
        "/<form (.*)action=[ ]*[\"'](.*)['](.*)>/Ui");
  }

  function nom_edition() {
    return $this->nom_module;
  }

  function url_site() {
    return $this->url_info;
  }

  // renvoie le charset de la page
  // passee en parametre
  function get_charset($page){
    preg_match("/(.*)<(.*)content-type(.*)charset=([a-zA-Z0-9\-_]*)(.*)>(.*)/i", $page, $chr);
    return $chr[4];
  }

  // renvoie vrai si le charset de la
  // page est de l'utf-8
  function is_utf8($page) {
    $chr = trim($this->get_charset($page));
    if (eregi("^utf-8$", $chr))
      return true;
    return false;
  }

  function option_scanner() {
    // renvoie vrai si le site est scannable (c.a.d que
    // int_listeXX existe et que la fonction traite_ligne
    // est surchargee.
    return false;
  }

  // methode utilisee lors du scan. 
  function scanne()
    {
      $voleur = $this->url_info;
      
      $fd = file($voleur);

      $ch = "";
      foreach ($fd as $ligne)
	{
	  //$ligne = fgets($fd, 4096);
	  $ch .= $this->traite_ligne($ligne);
	}
      //fclose($fd);
      optimise_table();
      return $ch;
    }

  function traite_ligne($ligne) {
    // fonction utilisee lors du scan - interprčte une
    // ligne du script int_listeXX et insere le document
    // recupere dans la base
  }
  
  function option_robot() {
    // renvoie vrai si le site doit etre parcouru par le 
    // robot. Dans ce cas, la methode examine_page doit
    // etre surchargee.(voir  module anglais)
    return false;
  }

  // methode utilisee par le robot pour extraire les liens de 
  // la page.
  function get_liens() {    
    return $this->liens;
  }

  function option_extrait() {
    // renvoie vrai si l'option "recuperer informations a
    // partir de l'URL" doit etre positionne dans la page
    // de creation d'un nouveau document. Si vrai, la
    // methode examine_page doit etre surchargee (voir 
    // module anglais)
    return false;
  }

  function examine_page($page) {
    // cette methode initialise les variables $_POST['art_auteur'],
    // $_POST['art_auteur'], $_POST['art_traducteur'], $_POST['art_titre'],
    // $_POST['art_date'] a partir du texte d'une page
  }
 
}


?>
