<?php

/*function _L($str)
{
  return $str;
}*/


function get_aamm($date)
{
  if (!ereg("([0-9][0-9][0-9][0-9])-([0-9][0-9])-([0-9][0-9])", $date, $match))
    return "0000";
  return substr($match[1],2,2).$match[2];
}

function include_aff($nom)
{
  nettoie_ext_variables();
  include($nom);
}


function format_bouton($message)
{
  return(str_pad($message, 10, " ", STR_PAD_BOTH));
}

// bouton pour retourner a la
// page de garde
function get_bouton_annuler($texte=" Page Accueil ")
{
  $ret = "<FORM ACTION='./index.php' METHOD='POST'>".
    "<INPUT TYPE='submit' VALUE='".format_bouton($texte)."'></FORM>";
  return $ret;
}


function get_bouton_annuler_lier($id_liable, $texte=" Annuler ")
{
  $ret = "<FORM ACTION='./index.php' METHOD='POST'>".
    "<INPUT TYPE='hidden' VALUE='".$id_liable."' NAME='id'>".
    "<INPUT TYPE='hidden' VALUE='modifier_article' NAME='module'>".
    "<INPUT TYPE='hidden' VALUE='2' NAME='etape'>".
    "<INPUT TYPE='submit' VALUE='".format_bouton($texte)."'></FORM>";
  return $ret;
}


function get_bouton_annuler_delier($id, $texte=" Annuler ")
{
  $ret = "<FORM ACTION='./index.php' METHOD='POST'>".
    "<INPUT TYPE='hidden' VALUE='".$id."' NAME='id'>".
    "<INPUT TYPE='hidden' VALUE='modifier_article' NAME='module'>".
    "<INPUT TYPE='hidden' VALUE='2' NAME='etape'>".
    "<INPUT TYPE='submit' VALUE='".format_bouton($texte)."'></FORM>";
  return $ret;
}


function get_bouton_retour($texte="Revenir")
{
  $ret = "<FORM ACTION='./index.php' METHOD='POST'>\n";
  reset($_GET);
  while(list($cle,$val)=each($_GET))
    {
      $val = htmlspecialchars(stripslashes($val), ENT_QUOTES);
      if (($cle=='etape') || ($cle=='etape_r'))
	$val = $val - 1;
      $ret .= "<INPUT TYPE='HIDDEN' VALUE='".$val."' NAME='".$cle."'>\n";
    }
  reset($_POST);
  while(list($cle,$val)=each($_POST))
    {
      $val = htmlspecialchars(stripslashes($val), ENT_QUOTES);
      if (($cle=='etape') || ($cle=='etape_r'))
	$val = $val - 1;
      $ret .= "<INPUT TYPE='HIDDEN' VALUE='".$val."' NAME='".$cle."'>\n";
    }
  $ret .= "<INPUT TYPE='submit' VALUE='".format_bouton($texte)."'>\n";
  $ret .= "</FORM>\n";
  return $ret;
}


function get_GET()
{
  $ret = "";

  reset($_GET);
  while(list($cle,$val)=each($_GET))
    {
      $val = urlencode(stripslashes($val));
      $ret .= $cle."=".$val."&";
    }

  reset($_POST);
  while(list($cle,$val)=each($_POST))
    {
      $val = urlencode(stripslashes($val));
      $ret .= $cle."=".$val."&";
    }
  $ret .= "dummy=0";
  return $ret;
}


// nettoyage des variables avant
// un affichage
function nettoie_ext_variables()
{
  $tabs = array($_GET, $_POST);
  while (list(,$ens)=each($tabs))
    {
      reset($ens);
      while(list($cle,$val)=each($ens))
	{
	  global $$cle;
	  $val = stripslashes($val);
	  $val = str_replace("'", "&#39;", $val);
	  $$cle = $val;
	}
    }
}


// nettoyage recursif de l'array
function nettoie_variables($ens)
{
  if (!is_array($ens))
    {
      $ens = stripslashes($ens);
      $ens = str_replace("'", "&#39;", $ens);
    }
  else
    {
      reset($ens);
      while(list($cle,$val)=each($ens))
	$ens[$cle] = nettoie_variables($val);
    }
  return $ens;
}


function check_date($date)
{
  return true;
}


function check_auteur($auteur)
{
  return true;
}


function check_titre($titre)
{
  if (!isset($titre) || ($titre==''))
      return false;
  return true;
}


function extrait_variables($page, $edition, $statut)
{
  global $titrea, $auteur, $date_pub, $type_doc, $type;

  // initialise les variables HTTP avec valeurs extraites
  // de la page

  if (preg_match("/<meta name=\"Date\" content=\"([0-9]*-[0-9]*-[0-9]*)\">/U",
		 $page, $date))
    $_POST['date_pub'] = $date[1];
  
  if (preg_match("/<meta name=\"Authors\" content=\"(.*)\">/U",
		 $page, $authors))
    $_POST['auteur'] = $authors[1];

  if (preg_match("/<title>(.*)<\/title>/U",
		 $page, $titre))

    $_POST['titrea'] = $titre[1];      

  if ($edition=='fr')
    $_POST['type_doc'] = '1';
  else
    $_POST['type_doc'] = '0';

  if ($statut == '401')
    $_POST['type'] = 'protege';
  else
    $_POST['type'] = 'public';
}


// verifie une reponse HTTP
// et renvoie le statut
function analyse_rep_http($ret, $init=false)
{
  $rep = array();

  $anstr = substr($ret, 0, 1024);
  if (!ereg("(.*)^HTTP/([0-1]\.[0-1]) ([0-9][0-9][0-9]) (.*)", $anstr, $match))
    return $rep;

  $rep['version'] = $match[2];
  $rep['statut'] = $match[3];
  $rep['corps'] = $match[4]; 

  return $rep;
}


function extrait_serveur($srv)
{
  $rep = array();

  if (!ereg("^http://(.*)", $srv, $match))
    return false;

  $rep['srv'] = $match[1];

  // recup port
  if (!ereg("(.*):([0-9]*)(.*)", $rep['srv'], $match))
    $rep['port'] = 80;
  else
    {
      $rep['port'] = $match[2];
      $rep['srv'] = $match[1].$match[3];
    }

  // recup url
  if (ereg("([^/]*)/(.*)", $rep['srv'], $match))
    {
      $rep['srv'] = $match[1];
      $rep['url'] = "/".$match[2];
    }
  else
    $rep['url'] = "";

  return $rep;
}


function decompose_url($srv, $url)
{
  $rep = array();

  if (!ereg("^http://(.*)", $url, $m))
    {
      $pc = $url[0];
      $rep['url'] = $url;
      if ($pc=='/' || $pc=='#')
	$rep['statut'] = 1;
      else
	$rep['statut'] = 2;
      return $rep;
    }

  if (ereg("^".$srv."(.*)", $url, $n))
    {
      $rep['url'] = $n[1];
      $rep['statut'] = 3;
      return $rep;
    }

  return false;  // hors site
}


function http_get($server, $port, $url)
{
  if ($url=="")
    $url = "/";

  $user_agent = "Mozilla/4.0 (compatible; MSIE 5.5; Windows 98)";

  //echo "serveur=".$server."\n";
  //echo "port=".$port."\n";
  //echo "url=".$url."\n";

  $headers = "GET ".$url." HTTP/1.1\n";
  $headers .= "Host: $server\n";
  $headers .= "User-Agent: $user_agent\n";
  $headers .= "Accept: */*\n";
  $headers .= "Accept-Language: fr-en\n";
  $headers .= "Accept-Charset: ISO-8859-1\n";
  $headers .= "Keep-Alive: 60\n";
  $headers .= "Proxy-Connection: keep-alive\n";
  $headers .= "Content-Type: application/x-www-form-urlencoded\n";
  $headers .= "Cache-Control: no-cache\n\n";

  //echo $headers."\n";
  $fp = fsockopen($server, $port, $errno, $errstr, 5.0);

  if (!$fp) {
    //echo "impossible ouvrir socket : ".$errno.", ".$errstr."\n";
    return false;
  }
  fputs($fp, $headers);

  $ret = "";

  while (!feof($fp))
    {
      $next = fgets($fp, 1024);
      $ret.= $next;
      //if (eregi("(.*)</html>(.*)", $next))
      //  break;
      if (eregi("(.*)</body>(.*)", $next))
        break;
    }

  fclose($fp);
  return $ret;
}


function check_url($url, $edition, $init=false)
{ 
  global $titrea, $auteur, $date_pub, $type, $type_doc;

  if (empty($url))
    return 'url_vide';

  // on recupere le serveur pour
  // l'edition. Ici le serveur peut
  // etre une URL
  $srv = get_serveur($edition);
  if ($srv==false)
    return 'url_entrer_article_non_valide';

  // on decompose l'url, au retour, 
  // urld['url'] contient l'url sans la 
  // partie serveur (s'il y'en a une)
  $urld = decompose_url($srv, $url);
  if ($urld==false)
    return 'url_entrer_article_non_valide';

  // on extrait le serveur reel
  $srvr = extrait_serveur($srv);
  if ($srvr==false)
    return 'url_entrer_article_non_valide';

  // et on fait le GET
  $page = http_get($srvr['srv'], $srvr['port'], $srvr['url'].$urld['url']);

  // puis on analyse la reponse
  $anl = analyse_rep_http($page);

  if ($anl['statut'][0] != "2")
    {
      if ($anl['statut'] == "401")
	{
	  extrait_variables($page, $edition, $anl['statut']);
	  if ($init==true)  // cas controle de la page
	    return "url_protege";
	  else
	    return ''; // dans le cas modif ou creation, on permet quand meme
	}
      return 'url_entrer_article_non_valide';
    }

    extrait_variables($page, $edition, $anl['statut']);

  return '';
}


function check_non_vide($str)
{
  if ($str == '')
    return false;
  return true;
}

// permet de verifier qu'une
// edition est autorisee
function check_edition($edition)
{
  $sites = get_sites();
  
  while(list(,$site)=each($sites))
    {
      if ($site['edition']==$edition)
	return true;
    }

  return false;
}


// permet de verifier que
// le serveur d'une nouvelle edition
// repond bien present
function check_srv($srv)
{
  $srv = extrait_serveur($srv);
  if ($srv==false)
    return false;

  // et on fait le GET
  $page = http_get($srv['srv'], $srv['port'], $srv['url']);

  // puis on analyse la reponse
  $anl = analyse_rep_http($page);
  if ($anl['statut'][0] != "2")
    return false;
  
  return true;
}



// classe controleur. Cette classe
// permet de gerer les arguments passes
// aux trad-liens/scripts
class controleur 
{
  var $sac;
  
  function controleur()
    {
      $this->sac = array();
    }

  function add($nom, $valeur)
    {
      $this->sac[$nom] = $valeur;
    }

  function get($nom)
    {
      return $this->sac[$nom];
    }

}



?>
