<?php

$texte_edition="";

//define("TRAD_SITE", "http://localhost/spip/index.php");
define("TRAD_SITE", "http://".$_SERVER['HTTP_HOST']."/~fil/spip/trad-liens/index.php");

// nombre de lignes affichees dans la recherche
$nombre_lignes=20;

// nombre de reponses renvoyees par la base
$limit = 50;

// temps en seconde au dela duquel les 
// articles ne sont plus systematiquement 
// renvoye par la recherche
$limit_marquage = 900;

chdir ('..');
// authentification & selection langue
if (file_exists('trad-liens/scripts/register.php')) {
	require('trad-liens/scripts/register.php');
} else {
	@header("Location: ".TRAD_SITE);
	exit;
}

include_once 'ecrire/inc_version.php';
include_spip('inc/presentation');

// main
{
/*  
    $fd = fopen("/tmp/debug_td.log", "a"); 
    fwrite($fd, "GET-------\n");
    reset($_GET);
    while(list($cle,$data)=each($_GET))
    fwrite($fd, $cle."=[".$data."]\n");
    fwrite($fd, "POST-------\n");
    reset($_POST);
    while(list($cle,$data)=each($_POST))
    fwrite($fd, $cle."=[".$data."]\n");
    fclose($fd);
*/  
  
  ob_start();
  ini_set("memory_limit", "32M");

  $titre = "Trad*Liens v0.2";

  include("trad-liens/scripts/utilitaires.php");
  include("trad-liens/scripts/fonctions.php");
  include("trad-liens/scripts/querys.php");

  include_ecrire("inc_charsets.php3");
  if (file_exists("trad-liens/scripts/module_".$edition.".php"))
    {
      include("trad-liens/scripts/module_".$edition.".php");
      $option_site = true;
    }

  if (empty($module))
    {
      include("trad-liens/html/accueil.php");
    }
  else
    dispatche();
  
#  ob_end_flush();
#  exit;
}

?>
