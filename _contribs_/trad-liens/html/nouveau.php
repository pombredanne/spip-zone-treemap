<?php
if (!defined("AUTORISE_VISU"))
{
  header("Status: 404 Not Found");
  exit;
}
?>

<script language="javascript">
<!--
function changedate()
{
  var sel_an = window.document.cree.annee;
  var sel_mois = window.document.cree.mois;
  var idx_an = sel_an.selectedIndex;
  var idx_mois = sel_mois.selectedIndex;
  var hid_date = window.document.cree.art_date;
  
  hid_date.value= sel_an.options[idx_an].text + '-' + sel_mois.options[idx_mois].text + '-01';
  return true;
}

function rafraichit_url(url)
{
  window.document.cree.art_url.value=url;
}

-->
</script>



<FORM ACTION='./index.php' METHOD='POST' NAME="cree">
<INPUT TYPE='hidden' VALUE='<?php echo $article_id; ?>' NAME='article_id'>

        <input type="hidden" name="frame" value="<?php echo $frame; ?>">
        <input type="hidden" name="module" value="creer">
         <?php echo champs_caches_recherche($frame); ?>

    <table border=0 cellspacing=2 cellpadding=0 bgcolor=#cccccc class=window align=center width=350>

      <tr>
      <td colspan=2 bgcolor=#0A246A background="./trad-liens/images/fond.gif" class=windowtitle>    
	
         <table width=100% border=0 cellspacing=0 cellpadding=2 class=windowtitle>
	    <tr>
		<td>  <font size=+1> Cr&eacute;ation d'un nouveau document</font>
		</td>
	      </tr>
	  </table>
	
       </td>
       </tr>

	<tr>
	  <td colspan=2>
	    <table width=100% border=0 cellspacing=0 cellpadding=0 class=window1 height=32>
	      <tr>
		<td colspan=2>		

		  <table border=0 cellspacing=0 cellpadding=2 width=100% height=32>

			<tr>


<?php if ($module_lg->option_extrait()==true) { ?>

	  <td width=1% class=window onMouseOver="this.style.backgroundColor='#eeee88'"  onMouseOut="this.style.backgroundColor=''" >  
	      <input title="Essayer de r&eacute;cup&eacute;rer les informations gr&acirc;ce &agrave; l'url." type="image" ALT="r&eacute;cup&eacute;rer" HSPACE=2 name="recuperer" height=24 width=24 border=0 src="./trad-liens/images/mini_eyes.jpg"> 
          </td>

<?php } ?>
		          <td class=window width=100% align=center nowrap>&nbsp; </td>

			</tr>
		    </table> 

		  </td>
		</tr>
       </table>
     </td>
    </tr>
  

  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>Edition</TD>
    <TD align=left class=line  nowrap>
     <?php echo "<INPUT TYPE='text' SIZE='6' NAME='art_edition' VALUE='".$edition."' DISABLED>"; ?>
    </TD>
  </TR>

  <TR bgcolor=#eeeeee>
    <TD  align=left class=line width=40%>URL</TD>
    <TD align=left class=line  nowrap>
     <?php echo "<INPUT TYPE='text' SIZE='32' MAXLENGTH='255' NAME='art_url' VALUE='".$art_url."' >";
	    echo "&nbsp;<A HREF='#' onClick=\"window.open(window.document.cree.art_url.value,'_blank','toolbar=1,location=1,directories=0,status=0,scrollbars=1,resizable=1,copyhistory=0,menuBar=0,width=700,height=600');return(false);\"><b>[test]</b>&nbsp;</A>\n";

     ?>
    </TD>
  </TR>

  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>Titre</TD>
    <TD align=left class=line  nowrap>
     <?php echo "<INPUT TYPE='text' SIZE='32' NAME='art_titre' VALUE='".$art_titre."' >"; ?>
    </TD>
  </TR>

  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>Auteur(s)</TD>
    <TD align=left class=line  nowrap>
     <?php echo "<INPUT TYPE='text' SIZE='32' MAXLENGTH='64' NAME='art_auteur' VALUE='".$art_auteur."' >"; ?>
    </TD>
  </TR>

  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>Aut. Trans.</TD>
    <TD align=left class=line  nowrap>
     <?php echo "<INPUT TYPE='text' SIZE='32' MAXLENGTH='64' NAME='art_auteur_trans' VALUE='".$art_auteur_trans."' >"; ?>
    </TD>
  </TR>

  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>Traducteur(s)</TD>
    <TD align=left class=line  nowrap>
     <?php echo "<INPUT TYPE='text' SIZE='32' MAXLENGTH='64' NAME='art_traducteur' VALUE='".$art_traducteur."' >"; ?>
    </TD>
  </TR>

  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>Date</TD>
    <TD align=left class=line  nowrap>

     <?php echo "<INPUT TYPE='hidden' NAME='art_date' VALUE='".$art_date."' >"; ?>
    
    <?php 
      ereg("([0-9][0-9][0-9][0-9])-([0-9][0-9])(.*)", $art_date, $dt);

      echo "<select name='mois' onchange='changedate();'>\n";
       reset($liste_mois);
      while(list(,$mois)=each($liste_mois))
        echo "<option value='".$mois."' ".($dt[2]==$mois?"SELECTED":"").">".$mois."\n";
      echo "</select>";
      echo "&nbsp";

      echo "<select name='annee' onchange='changedate();'>\n";
      reset($liste_annees);
      while(list(,$annee)=each($liste_annees))
        echo "<option value='".$annee."' ".($dt[1]==$annee?"SELECTED":"").">".$annee."\n";
      echo "</select>";
    ?>

    </TD>
  </TR>


  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>Acc&egrave;s</TD>
    <TD align=left class=line  nowrap>
      <SELECT NAME="art_type_acces">
    <?php reset($liste_type_acces);
          while(list($key,$it)=each($liste_type_acces))
            { 
              $sel="";
              if ($art_type_acces==$key) $sel=" SELECTED";
              echo "<OPTION VALUE='".$key."'".$sel.">".$it;
            }
    ?>
      </SELECT>
    </TD>
  </TR>


  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>Type</TD>
    <TD align=left class=line  nowrap>
      <SELECT NAME="art_type_article">
    <?php reset($liste_type_article);
          while(list($key,$it)=each($liste_type_article))
            { 
              $sel="";
              if ($art_type_article==$key) $sel=" SELECTED";
              echo "<OPTION VALUE='".$key."'".$sel.">".$it;
            }
    ?>
      </SELECT>
    </TD>
  </TR>


  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>Num. r&eacute;f&eacute;rence</TD>
    <TD align=left class=line  nowrap>
      <?php echo"<INPUT TYPE='text' NAME='art_id_orig' VALUE='".$art_id_orig."' SIZE='16' MAXLENGTH='16' >"; ?>
    </TD>

  </TR>
	

  <TR>

     <td valign="top" align="right" colspan="2">

       <INPUT TYPE='submit' ALT='enregistrer' STYLE='width: 30%' NAME='enregistrer' VALUE='<?php echo format_bouton("Enreg."); ?>'>
       <INPUT TYPE='submit' ALT='revenir' STYLE='width: 30%' NAME='revenir' VALUE='<?php echo format_bouton("Annuler"); ?>'>

   </TD>
  </TR>
	
</TABLE>
      </FORM>      


<script language="javascript">
<!--
if (top.bas.ch_page_<?php echo $frame; ?>)
{ 
  top.bas.ch_page_<?php echo $frame; ?>('nouveau'); 
}
-->
</script>
