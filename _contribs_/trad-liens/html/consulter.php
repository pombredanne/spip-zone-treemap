<?php
if (!defined("AUTORISE_VISU"))
{
  header("Status: 404 Not Found");
  exit;
}

?>

    <table border=0 cellspacing=2 cellpadding=0 bgcolor=#cccccc class=window align=center width=350>

      <tr>
      <td colspan=2 bgcolor="#d0fff0" background="./trad-liens/images/fond.gif" class=windowtitle>    
	
         <table width=100% border=0 cellspacing=0 cellpadding=2 class=windowtitle>
	    <tr>
		<td>  <font size=+1>  <?php echo $article['titre']; ?>&nbsp;</font>
		</td>
	      </tr>
	  </table>
	
       </td>
       </tr>



	<tr>
	  <td colspan=2>
	    <table width=100% border=0 cellspacing=0 cellpadding=0 class=window1>
	      <tr>
		<td colspan=2>		

		  <table border=0 cellspacing=0 cellpadding=2 width=100%>

			<tr>

         <?php if ($frame=='orig') { ?>
       <form action="./index.php" method="POST" name="modifier">       
		  <td width=1% class=window onMouseOver="this.style.backgroundColor='#eeee88'"  onMouseOut="this.style.backgroundColor=''">   
       <INPUT TYPE='hidden' NAME='modifier' VALUE='<?php echo format_bouton("MODIFIER"); ?>'>
        <input type="hidden" name="frame" value="<?php echo $frame; ?>">
        <input type="image" title="Appeler la page de modification de cet article." ALT="modifier" HSPACE=2 border=0 src="./trad-liens/images/edit.gif" OnClick="submit();" name="modifier">
        <input type="hidden" name="module" value="modifier">
        <input type="hidden" name="article_id" value="<?php echo $article['id_article']; ?>">
         <?php echo champs_caches_recherche($frame); ?>
       </td></form>
         <?php }   ?> 

            <td class=window width=100% align=center height=32 nowrap>&nbsp;</td>

			</tr>
		    </table> 

		  </td>
		</tr>
       </table>
     </td>
    </tr>

   <TR  bgcolor=#eeeeee>
    <TD align=left class=line width=40%>Edition</TD>
    <TD align=left class=line nowrap><?php echo '<INPUT TYPE="text" SIZE="6" NAME="edition" VALUE="'.$article['edition'].'" >'; ?></TD>
   </TR>

  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>URL</TD>
    <TD align=left class=line nowrap>
     <?php echo "<INPUT TYPE='text' SIZE='32' MAXLENGTH='255' NAME='url' VALUE='".$article['url_site']."' >";
	    echo "&nbsp;<A HREF='#' title='Visualiser cet article.' onClick=\"window.open('".$article['url_site']."','_blank','toolbar=0,location=0,directories=0,status=0,scrollbars=1,resizable=1,copyhistory=0,menuBar=0,width=700,height=600');return(false)\"><b>[visu]</b>&nbsp;</A>\n";

     ?>
    </TD>
  </TR>

  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>Titre</TD>
    <TD align=left class=line nowrap>
     <?php echo "<INPUT TYPE='text' SIZE='32' MAXLENGTH='255' NAME='titrea' VALUE=\"".entites_html($article['titre'])."\" >"; ?>
    </TD>
  </TR>

  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>Auteur(s)</TD>
    <TD align=left class=line nowrap>
     <?php echo "<INPUT TYPE='text' SIZE='32' MAXLENGTH='64' NAME='auteur' VALUE='".$article['surtitre']."' >"; ?>
    </TD>
  </TR>

<!--  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>Aut. Trans.</TD>
    <TD align=left class=line  nowrap>
     <?php echo "<INPUT TYPE='text' SIZE='32' MAXLENGTH='64' NAME='auteur_trans' VALUE='".$article['auteur_trans']."' >"; ?>
    </TD>
  </TR>
-->
  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>Traducteur(s)</TD>
    <TD align=left class=line  nowrap>
     <?php echo "<INPUT TYPE='text' SIZE='32' MAXLENGTH='64' NAME='traducteur' VALUE='".$article['ps']."' >"; ?>
    </TD>
  </TR>

  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>Date</TD>
    <TD align=left class=line  nowrap>
      <?php echo "<INPUT TYPE='hidden' NAME='date_pub' VALUE='".$article['date']."' >"; ?>
   <?php 
      ereg("([0-9][0-9][0-9][0-9])-([0-9][0-9])(.*)", $article['date'], $dt);
      echo "<input type='text' size='2' value='".$dt[2]."' >";
      echo "&nbsp;<input type='text' size='4' value='".$dt[1]."' >";
    ?>
    </TD>
  </TR>

<!--  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>Acc&egrave;s</TD>
    <TD align=left class=line  nowrap>
     <?php echo "<INPUT TYPE='text' SIZE='16' MAXLENGTH='16' NAME='type_acces' VALUE='".$liste_type_acces[$article['type_acces']]."' >"; ?>
    </TD>
  </TR>
-->
<!--  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>Type</TD>
    <TD align=left class=line  nowrap>
     <?php echo "<INPUT TYPE='text' SIZE='16' MAXLENGTH='16' NAME='type_acces' VALUE='".$liste_type_article[$article['type_article']]."' >"; ?>
    </TD>
  </TR>
-->

<!--
  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>R&eacute;f.</TD>
    <TD align=left class=line  nowrap>
      <?php echo"<INPUT TYPE='text' NAME='numero' VALUE='".$article['id_orig']."' SIZE='16' MAXLENGTH='16' >"; ?>
    </TD>
  </TR>
-->
  <TR bgcolor=#eeeeee>
    <TD align=left class=line width=40%>LIENS</TD>
    <TD align=left class=line  nowrap>&nbsp;
<?php
  reset($liens);
  while(list(,$lien)=each($liens))
    {
       echo "&nbsp;<A HREF='#' onClick=\"window.open('".$lien['url_site']."','_blank','toolbar=0,location=0,directories=0,status=0,scrollbars=1,resizable=1,copyhistory=0,menuBar=0,width=700,height=600');return(false)\"><b>[".$lien['edition']."]</b>&nbsp;</A>\n";
    }
?>
    </TD>
  </TR>

<?php include ('suggerer.php'); ?>

  <TR>
      <form action="./index.php" method="POST" name="revenir">	    
     <td valign="top" align="right" colspan="2">
       <INPUT TYPE='submit' STYLE='width: 30%' ALT='ok' NAME='revenir' VALUE='<?php echo format_bouton("Ok"); ?>'>
       <input type="hidden" name="module" value="<?php echo $frame; ?>">
       <input type="hidden" name="article_id" value="<?php echo $article['id_article']; ?>">
       <?php echo champs_caches_recherche($frame); ?>
   </TD>
      </FORM>      
  </TR>

</TABLE>

<script language="javascript">
<!--
if (top.bas.ch_page_<?php echo $frame; ?>)
{ 
  top.bas.ch_page_<?php echo $frame; ?>('consulter'); 
}
-->
</script>


