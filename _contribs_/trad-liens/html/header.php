<?php
if (!defined("AUTORISE_VISU"))
{
  header("Status: 404 Not Found");
  exit;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

 <HTML>
  <HEAD>
    <TITLE><?php echo $titre; ?></TITLE>
    <META HTTP-EQUIV="Expires" CONTENT="0">
    <META HTTP-EQUIV="cache-control" CONTENT="no-cache,no-store">
    <META HTTP-EQUIV="pragma" CONTENT="no-cache">
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">

<style>
A {
text-decoration : none;
}
.t {
font-size: 9pt;
text-align : center;
font-family: Verdana;
}
.t2 {
font-size: 8pt;
text-align : center;
font-family: Verdana;
}
.n {
  font-family: Fixedsys
}
.s {
font-size: 10pt;
text-align : right;
font-family: Verdana;
}
.sy {
font-family: Fixedsys;
}
.s2 {
font-family: Fixedsys;
color: red;
}
.tab {
font-size: 10pt;
text-align : center;
font-family: Verdana;
background: #cccccc;
}
.tr {
background: #ffffff;
}
</style><style>
.title {
color: 'black';
background: #D4D0C8;
text-align: 'center';
BORDER-RIGHT:   #888888 1px outset;
BORDER-TOP:     #ffffff 2px outset;
BORDER-LEFT:    #ffffff 1px outset;
BORDER-BOTTOM:  #888888 1px outset;
}
.window {
BORDER-RIGHT:  buttonhighlight 2px outset;
BORDER-TOP:    buttonhighlight 2px outset;
BORDER-LEFT:   buttonhighlight 2px outset;
BORDER-BOTTOM: buttonhighlight 2px outset;
FONT: 8pt Tahoma, Verdana, Geneva, Arial, Helvetica, sans-serif;
BACKGROUND-COLOR: #D4D0C8;
CURSOR: default;
}
.window1 {
BORDER-RIGHT:  #eeeeee 1px solid;
BORDER-TOP:    #808080 1px solid;
BORDER-LEFT:   #808080 1px solid;
BORDER-BOTTOM: #eeeeee 1px solid;
FONT: 8pt Tahoma, Verdana, Geneva, Arial, Helvetica, sans-serif;
}
.line {
BORDER-RIGHT:   #cccccc 1px solid;
BORDER-TOP:     #ffffff 1px solid;
BORDER-LEFT:    #ffffff 1px solid;
BORDER-BOTTOM:  #cccccc 1px solid;
/*background: #eeeeee;*/
font: 9pt Tahoma;
}
.line2 {
background: #ffffcc;
}
.black {color: black}
a:link.black {color: black}
a:active.black {color: black}
a:visited.black {color: black}
a:hover.black {color: #0000ff}

.white {color: white}
a:link.white{color: white}
a:active.white{color: white}
a:visited.white{color: white}
a:hover.white{color: #ffff77}

a:link     {color: #000000;}
a:active   {color: #000000;}
a:visited  {color: #000000;}
a:hover    {color: #000000;}
a {
CURSOR: default;
}

.windowtitle {
font: 9pt; Tahoma, Verdana, Geneva, Arial, Helvetica, sans-serif;
font-weight: bold;
color: white;
}
.bleu {
# background: #fbfbff;
}
.vert {
# background: #fbfffb;
}
.or {
# background: #ffedd8;
}
</style>

<script language="javascript">
<!--

<?php if (!is_array($erreur) && ($erreur!='')) { ?>
window.open('./index.php?module=message&message=<?php echo $erreur; ?>','_blank','toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,menuBar=0,width=320,height=140');
<?php } ?>

function get_GET()
{
  return '<?php echo get_GET(); ?>';
}

function no_back()
{
  if (navigator.userAgent.indexOf("Safari")==-1)
    history.go(1);
}

function tr(a0,a1,a2,a3,a4,a5,x) {
document.write("<tr bgcolor=#eeeeee><td align=left class=line width=40%>"+a0+"</td><td align=right class=line  nowrap> "+a1+" </td></tr>");
}
-->
</script>

</HEAD>

<body bgcolor=#f0f0f8 onload="javascript: no_back();">

  <CENTER>
