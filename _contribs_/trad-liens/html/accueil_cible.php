<?php
if (!defined("AUTORISE_VISU"))
{
  header("Status: 404 Not Found");
  exit;
}
?>

    <form action="./index.php" method="POST" name="recherche">
    <input type="hidden" name="module" value="cible">
    <input type="hidden" name="frame" value="cible">

    <table border=0 cellspacing=2 cellpadding=0 bgcolor=#cccccc class=window align=center width=350>

      <tr>
      <td colspan=2 bgcolor=#0A246A background="./trad-liens/images/fond.gif" class=windowtitle>    
	
         <table width=100% border=0 cellspacing=0 cellpadding=2 class=windowtitle>
	    <tr>
		<td>  <font size=+1>Autres &eacute;ditions</font>
		</td>
	      </tr>
	  </table>
	
       </td>
       </tr>

	<tr>
	  <td colspan=2>
	    <table width=100% height=32 border=0 cellspacing=0 cellpadding=0 class=window1>
	      <tr>
		<td colspan=2>		

		  <table border=0 cellspacing=0 cellpadding=2 width=100% height=32>
			<tr>
		          <td class=window width=100% align=center nowrap>&nbsp; </td>
			</tr>
		    </table> 

		  </td>
		</tr>
       </table>
     </td>
    </tr>


                <tr bgcolor=#eeeeee>
                  <td align=left class=line width=40%>Mots du titre : </td><td align=left class=line  nowrap><INPUT CLASS="bleu" STYLE="width: 100%" TYPE="text" VALUE="<?php echo $mots_titre; ?>" NAME="mots_titre">
                  </td>
                </tr>

                <tr bgcolor=#eeeeee>
                  <td align=left class=line width=40%>Aut./Trad. : </td><td align=left class=line  nowrap><INPUT TYPE="text" CLASS="bleu" STYLE="width: 100%" VALUE="<?php echo $auteurs; ?>" NAME="auteurs">
                  </td>
                </tr>

                <tr bgcolor=#eeeeee><td align=left class=line width=40%>URL : </td><td align=left class=line  nowrap>
                    <SELECT NAME="edition_cible" CLASS="bleu" STYLE="width: 20%">
                    <?php 
                      reset($liste_edition);
                      while (list($key,$it)=each($liste_edition))
                        echo "<OPTION VALUE='".$it."'".($edition_cible==$it?" SELECTED":"").">".$it;
                    ?>
                   </SELECT>
                  <INPUT TYPE="text" VALUE="<?php echo $url; ?>" CLASS="bleu" STYLE="width: 78%" NAME="url">
                  </td>
                </tr>

                <tr bgcolor=#eeeeee>
                  <td  align=left class=line width=40%>Date Pub. : </td><td align=right class=line  nowrap><SELECT CLASS="bleu" STYLE="width: 50%" NAME="date_pub">
                    <?php 
                      reset($liste_dates);
                      while (list($key,$it)=each($liste_dates))
                        echo "<OPTION VALUE='".$key."'".($date_pub==$key?" SELECTED":"").">".$it."\n";
                    ?>
                   </SELECT>
                  </td>
                </tr>


                <tr bgcolor=#eeeeee>
                  <td  align=left class=line width=40%>Articles non li&eacute;s : </td><td align=right class=line  nowrap>
                   <INPUT TYPE="checkbox" NAME="non_lie" <?php echo ($non_lie=="on"?" CHECKED":""); ?>> 
		   avec <b><?php echo $edition; ?>&nbsp;</b>
                  </td>
                </tr>

            </td>
          </tr>

          <tr>
            <td colspan=2 valign="top" align="right">
               <INPUT TYPE="submit" STYLE="width: 100%" ALT="chercher" VALUE="Chercher" NAME="chercher">
            </td>
          </tr>

          <tr bgcolor=#ffffff>
            <td  colspan=2><SELECT NAME="article_id" SIZE="<?php echo $nombre_lignes; ?>" CLASS="bleu" STYLE="width: 100%" onChange="change_idx();">

                    <?php
/*	                reset($articles_rec);
                        while (list($key,$it)=each($articles_rec))
                           {
			     $aamm=get_aamm($it['date']);
			     echo "<OPTION STYLE='width: 345px' VALUE='".$it['id_article']."'>[".$aamm."]&nbsp;+++&nbsp;".couper($it['titre'], 50)."\n";
                           }
*/
                    ?>
                    <?php
		        reset($articles);
                        while (list($key,$it)=each($articles))
                          {
			    $aamm = get_aamm($it['date']);
#                            if (!array_key_exists($key, $articles_rec))
                                echo "<OPTION STYLE='width: 345px' VALUE='".$it['id_article']."'>[".$aamm."]&nbsp;".couper($it['titre'], 50)."\n";
                           }
                    ?>

                </SELECT>
            </td>
          </tr>

          <tr>
            <td colspan=2 valign="top" align="right">
               <INPUT TYPE="submit" STYLE="width: 100%" ALT="consulter" VALUE="Consulter" NAME="consulter">
            </td>
          </tr>

      </table>
 </form>


<script language="javascript">
<!--

function change_idx()
{
  val = window.document.recherche.article_id.selectedIndex;
  //window.document.recherche.idx_selected_cible.value=val;
  top.bas.change_idx_cible(val);
}

if (top.bas.ch_page_orig)
{ 
  top.bas.ch_page_cible('cible'); 
}

sel = window.document.recherche.article_id;
if (top.bas.get_idx_cible)
  idx = top.bas.get_idx_cible();
else
  idx = 0;
if (idx<sel.length)
  sel.selectedIndex = idx;
else
  sel.selectedIndex = sel.length-1;

-->
</script>
