----------------------------------------------------------------------
    Copyright (C) 2002 Fabrice Gangler 
	
    www.drop-zone-city.com	/// Assistants SPIP pour Dreamweaver//

----------------------------------------------------------------------

	Ce programme est un logiciel libre ; vous pouvez le redistribuer 
	et/ou le modifier conform�ment aux dispositions de la 
	Licence Publique G�n�rale GNU, telle que publi�e par la 
	Free Software Foundation ; version 2 de la licence, ou encore 	
	(� votre choix) toute version ult�rieure.

	Ce programme est distribu� dans l'espoir qu'il sera utile,
	 mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de 
	COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. 
	Pour plus de d�tails, voir la Licence Publique G�n�rale GNU.

	Un exemplaire de la Licence Publique G�n�rale GNU doit �tre fourni 
	avec ce programme ; si ce n'est pas le cas, �crivez � la 
	Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis. 
	
	--------------------------------------------------------------

	- GNU General Public License : 
	  http://www.gnu.org/copyleft/gpl.html

	- Traduction (Non officielle) en fran�ais de la Licence GPL
	  http://www.april.org/gnu/gpl_french.html

----------------------------------------------------------------------


L'objectif (de l'auteur) est de passer la main sur ce projet 
tout en favorisant une certaine continuit�. Si vous �tes int�ress�,
n'h�sitez pas � le contacter.



======================================================================
		C H A N G E   L O G
======================================================================
		
	--------------------------------------------------------------
	Version 0.50	12/01/2002	Fabrice

	- Correction des bugs introduits par la version 0.49 !!
	- Ajout de la balise #RECHERCHE (SPIP 1.5.1)
	- Ajout d'un assistant g�n�rant des boucles exemples 
	- Possibilit� de g�n�rer les balises sans les boucles


	--------------------------------------------------------------
	Version 0.49	18/12/2002	Fabrice

	- BUG crit�re 'par hasard' corrig�
	

	--------------------------------------------------------------
	Version 0.46	06/12/2002	Fabrice

	5�me version 
	- BUG javascript 'eval()' pour version MX 	corrig�
	- BUG Format/Non des balises 			corrig�
	- Extension Dreamweaver pour une installation propre (DW 3/4/MX)
	- G�n�ration du code autour de la s�lection  (Uniquement DW MX)
	- Maj. Boucles & Balises (SPIP version 1.4)
	- Maj. Boucles & Balises (SPIP version 1.5)	A finir
	- Ajout d'un rep�re visuel pour conna�tre la version de SPIP
	  n�cessaire pour utiliser une balise ou un crit�re.


	--------------------------------------------------------------
	Version 0.45	29/08/2002	Fabrice

	4�me version 
	- Proc�dure d'install pour MX

	--------------------------------------------------------------
	Version 0.44	28/08/2002	Fabrice

	3�me version 
	- Correction BUG (Affichage du  '#')
	- Changement de texte (Pour boucle <Bn></B><//B>)


	--------------------------------------------------------------
	Version 0.40	23/08/2002	Fabrice

	1er version pour SPIP 1.4




======================================================================
		R O A D M A P
======================================================================
	
	- Validation du CODE g�n�r� par la liste SPIP-DEV
	- Validation de l'ordre des crit�res par la liste SPIP-DEV
	- Rajout d'exemple(s)
	- R�gler le PB des '<separator />' pour MX
	- Validation du nom des boucles 
	  avec le nom des boucles d�j� pr�sentes

	- ...




================================================================== END



----------------------------------------------------------------------
    Copyright (C) 2002 Fabrice Gangler 
	
    www.drop-zone-city.com	/// Assistants SPIP pour Dreamweaver//
----------------------------------------------------------------------