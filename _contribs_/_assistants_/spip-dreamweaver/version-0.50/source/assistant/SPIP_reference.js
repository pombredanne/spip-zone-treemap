// ----------------------------------------------------------------------
//   Copyright (C) 2002 Fabrice Gangler (contact@drop-zone-city.com)
//
//    www.drop-zone-city.com				 /// SPIP_reference.js //////
// ----------------------------------------------------------------------


//---------------    SPIP R�ference - BOUCLES / BALISES    ---------------

var tab_selection = new Array ;
var tab_affichage = new Array ;
var tab_balise = new Array ;

// TAB_SELECTION 
tab_selection ['RUBRIQUES']= new Array( 'tout' , 'id_rubrique' , 'id_secteur' , 'id_parent' , 'id_enfant' , 'meme_parent' , 'recherche' , 'id_mot' , 'id_groupe' , 'titre_mot___' , 'type_mot___' ) ;
tab_selection ['BREVES']= new Array ('tout' , 'id_rubrique' , 'id_breve' , 'id_mot' , 'id_groupe' , 'titre_mot___' , 'type_mot___' , 'recherche' ) ;
tab_selection  ['ARTICLES']= new Array ('tout' , 'id_rubrique' , 'id_secteur' , 'id_article' , 'id_auteur' , 'id_mot' , 'id_groupe' , 'titre_mot___' , 'type_mot___' , 'recherche') ;
tab_selection  ['DOCUMENTS']= new Array ( 'tout' , 'id_rubrique' , 'id_article' , 'id_breve' );
tab_selection  ['MOTS']= new Array ( 'tout' , 'id_mot' , 'id_groupe' , 'id_rubrique' , 'id_article' , 'id_breve', 'id_syndic' , 'id_forum' , 'titre___' , 'type___' ) ;
tab_selection  ['AUTEURS']= new Array ('tout', 'id_auteur' , 'id_article' ) ;
tab_selection  ['FORUMS']= new Array ( 'id_forum' , 'id_rubrique' , 'id_article' , 'id_breve' , 'id_parent' , 'id_enfant' , 'meme_parent' , 'id_mot' , 'id_groupe' , 'titre_mot___' , 'type_mot___' , 'plat' , 'id_secteur' ) ;
tab_selection  ['SIGNATURES']= new Array ('tout', 'id_article' , 'id_signature') ;
tab_selection  ['HIERARCHIE']= tab_selection ['RUBRIQUES'] ;
tab_selection  ['SITES']= new Array ( 'id_syndic' , 'tout' , 'id_rubrique' , 'id_secteur' , 'id_breve' , 'id_mot' , 'id_groupe' , 'titre_mot___' , 'type_mot___' ) ;
tab_selection  ['SYNDIC_ARTICLES']=  new Array ('tout', 'id_syndic_article', 'id_syndic', 'id_rubrique', 'id_secteur');

// TAB_AFFICHAGE 
tab_affichage ['COMMUNS']= new Array ( 'inverse' , 'par__hasard') ;
tab_affichage ['RUBRIQUES']= new Array ('inverse' , 'par__hasard' , 'exclus' , 'doublons' ) ;
tab_affichage['BREVES']= new Array ('inverse' , 'par__hasard') ;
tab_affichage ['ARTICLES']= new Array ('inverse' , 'par__hasard' , 'exclus' , 'doublons') ;
tab_affichage ['DOCUMENTS']= new Array ('doublons' , 'mode___vignette' , 'mode___document' , 'extension___' ) ;
tab_affichage ['MOTS']= new Array ('inverse' , 'par__hasard') ;
tab_affichage ['AUTEURS']= new Array ('inverse' , 'par__hasard') ;
tab_affichage ['FORUMS']=  new Array ('inverse' , 'par__hasard') ;
tab_affichage ['SIGNATURES']= new Array ('inverse' , 'par__hasard') ;
tab_affichage ['HIERARCHIE']= tab_affichage ['RUBRIQUES'] ;
tab_affichage ['SITES']=  new Array ('inverse' , 'par__hasard' , 'exclus' , 'doublons' , 'moderation_N_oui' , 'moderation___oui' , 'syndication___oui' , 'syndication___non' ) ;
tab_affichage ['SYNDIC_ARTICLES']= new Array ('inverse' , 'par__hasard') ;


// TAB_CLASSEMENT - A mettre � jour (TAB pas encore utilis�)
// TAB_NON-CLASSEMENT - A mettre � jour (TAB pas encore utilis�)


// TAB_BALISE - Logiquement � deduire de CLASSEMENT et NON-CLASSEMENT
tab_balise ['RUBRIQUES']= new Array ('ID_RUBRIQUE' , 'TITRE' , 'DESCRIPTIF' , 'TEXTE' , 'ID_SECTEUR' , 'NOTES' , 'INTRODUCTION' , 'URL_RUBRIQUE' , 'DATE' , 'FORMULAIRE_FORUM' , 'PARAMETRES_FORUM' , 'FORMULAIRE_SITE' , 'LOGO_RUBRIQUE' , 'LOGO_RUBRIQUE_NORMAL' , 'LOGO_RUBRIQUE_SURVOL' ) ;
tab_balise ['BREVES']= new Array ('ID_BREVE' , 'TITRE' , 'DATE' , 'TEXTE' , 'NOM_SITE' , 'URL_SITE' , 'ID_RUBRIQUE' , 'NOTES' , 'INTRODUCTION' , 'URL_BREVE' , 'FORMULAIRE_FORUM' , 'PARAMETRES_FORUM' , 'LOGO_BREVE' ,  'LOGO_BREVE_RUBRIQUE' ) ;
tab_balise ['ARTICLES']= new Array ('ID_RUBRIQUE' , 'ID_ARTICLE' , 'SURTITRE' , 'TITRE' , 'SOUSTITRE' , 'DESCRIPTIF' , 'CHAPO' , 'TEXTE' , 'PS' , 'DATE' , 'DATE_REDAC' , 'DATE_MODIF' , 'ID_SECTEUR' , 'VISITES' , 'POPULARITE' , 'NOTES' , 'INTRODUCTION' , 'LESAUTEURS' , 'URL_ARTICLE' , 'FORMULAIRE_FORUM' , 'FORMULAIRE_SIGNATURE' , 'PARAMETRES_FORUM' , 'LOGO_ARTICLE' , 'LOGO_ARTICLE_RUBRIQUE' , 'LOGO_RUBRIQUE' , 'LOGO_ARTICLE_NORMAL' , 'LOGO_ARTICLE_SURVOL' ) ;

tab_balise ['DOCUMENTS']= new Array ( 'LOGO_DOCUMENT' , 'URL_DOCUMENT' , 'TITRE' , 'DESCRIPTIF' , 'TYPE_DOCUMENT' , 'TAILLE' , 'LARGEUR' , 'HAUTEUR' , 'EMBED_DOCUMENT' ) ;
tab_balise ['MOTS']= new Array ( 'ID_MOT' , 'TITRE' , 'DESCRIPTIF' , 'TEXTE' , 'TYPE' , 'LOGO_MOT' ) ;
tab_balise ['AUTEURS']= new Array ("ID_AUTEUR" , "NOM" , "BIO" ,  "EMAIL" , "NOM_SITE" , "URL_SITE" , "PGP" , "FORMULAIRE_ECRIRE_AUTEUR" , "NOTES" , "LOGO_AUTEUR" ) ;
tab_balise ['FORUMS']= new Array ('ID_BREVE' , 'ID_RUBRIQUE' , 'ID_ARTICLE' , 'TITRE' , 'DATE' , 'TEXTE' , 'NOM_SITE' , 'URL_SITE' , 'NOM' , 'EMAIL' , 'IP' , 'FORMULAIRE_FORUM' , 'PARAMETRES_FORUM' ) ;
tab_balise ['SIGNATURES']= new Array ( 'ID_SIGNATURE' , 'ID_ARTICLE' , 'DATE' , 'MESSAGE' , 'NOM' , 'EMAIL' , 'NOM_SITE' , 'URL_SITE' ) ;
tab_balise ['HIERARCHIE']= tab_balise ['RUBRIQUES'] ;
tab_balise ['SITES']= new Array ("ID_SYNDIC" , "NOM_SITE" , "URL_SITE" , "DESCRIPTIF" , "ID_RUBRIQUE" , "ID_SECTEUR" , "LOGO_SITE" ) ;
tab_balise ['SYNDIC_ARTICLES']= new Array ('ID_SYNDIC_ARTICLE' , 'ID_SYNDIC' , 'TITRE' , 'URL_ARTICLE' , 'DATE' , 'LESAUTEURS' , 'NOM_SITE' , 'URL_SITE' ) ;
tab_balise ['FORMULAIRE']= new Array ( 'FORMULAIRE_ADMIN' , 'FORMULAIRE_INSCRIPTION' ,'FORMULAIRE_RECHERCHE' , 'FORMULAIRE_FORUM' , 'FORMULAIRE_SIGNATURE' , 'FORMULAIRE_SITE' , 'FORMULAIRE_ECRIRE_AUTEUR' , 'LOGIN_PRIVE' , 'LOGIN_PUBLIC' , 'URL_LOGOUT' , 'CHARSET' , 'NOM_SITE_SPIP' , 'URL_SITE_SPIP' , 'EMAIL_WEBMASTER' , 'PUCE' , 'RECHERCHE' ) ;


// ----------------------------------------------------------------------
//   Copyright (C) 2002 Fabrice Gangler (contact@drop-zone-city.com)
//
//    www.drop-zone-city.com				 /// SPIP_reference.js //////
// ----------------------------------------------------------------------