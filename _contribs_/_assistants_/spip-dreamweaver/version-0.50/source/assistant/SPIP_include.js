// ----------------------------------------------------------------------
//   Copyright (C) 2002 Fabrice Gangler (contact@drop-zone-city.com)
//
//    www.drop-zone-city.com				   /// SPIP_include.js //////
// ----------------------------------------------------------------------


//---------------     API FUNCTIONS    ---------------

function isDOMRequired() {
	// Return false, indicating that this object is available in code view.
	return false;
}

function objectTag() {

	var file_include = document.forms[0].file_include.value ;
	var str_output = '' ;
	var str_param = '' ;
	
	var f = document.forms[0] ;

	var param = new Array() ;
	param [0] =  new Array() ; 
	param [0][0] = f.param_0.options[f.param_0.selectedIndex].value;
  	param [0][1] = f.param_0_egal_valeur.value ;
	param [0][2] = f.param_0_egal.checked  ;
	
	param [1] =  new Array() ; 
	param [1][0] = f.param_1.options[f.param_1.selectedIndex].value;
  	param [1][1] = f.param_1_egal_valeur.value ;
	param [1][2] = f.param_1_egal.checked  ;
	
	param [2] =  new Array() ; 
	param [2][0] = f.param_2.options[f.param_2.selectedIndex].value;
  	param [2][1] = f.param_2_egal_valeur.value ;
	param [2][2] = f.param_2_egal.checked  ;	
	
	if( file_include == ''){

		alert( "Le NOM du FICHIER doit toujours �tre renseign� !	\n\n Par d�faut, le nom \'Votre_Fichier\' vas �tre utilis�.");
		
  		file_include = "Votre_Fichier" ;
  	}	
	
	for ( var i=0 ; i < param.length ; i++ ){
   
		if( param [i][0] != 'vide' ){
			str_param += '{' + param [i][0] ;	
			if ( param [i][2] ){
				str_param +=  '=' + param [i][1] ;
			}		
			str_param += '}';
		}
	}  
	
   	// -- G�n�ration...
	str_output = '\n\n <INCLURE(' + file_include + ')' + str_param + '> \n\n';	
	 return str_output ;
}

// ----------------------------------------------------------------------
//   Copyright (C) 2002 Fabrice Gangler (contact@drop-zone-city.com)
//
//    www.drop-zone-city.com				   /// SPIP_include.js //////
// ----------------------------------------------------------------------