
// ----------------------------------------------------------------------
//   Copyright (C) 2002 Fabrice Gangler (contact@drop-zone-city.com)
//
//    www.drop-zone-city.com					/// SPIP_balise.js //////
// ----------------------------------------------------------------------

//---------------     Var MSG    ---------------

var msg_abs_nom = "Le NOM de la BOUCLE doit toujours �tre renseign� ! " ;
	msg_abs_nom += "\n\n Par d�faut, le nom \'Votre_Boucle\' vas �tre utilis�." ;
	
var msg_use_boucle = "Il n'est pas possible d'utiliser \'<B></B>\' ou " ;
	msg_use_boucle +=  " \'</B>\' \n sans utiliser \'<BOUCLE></BOUCLE>\'." ;
	

//---------------     API FUNCTIONS    ---------------

function isDOMRequired() {
	// Return false, indicating that this object is available in code view.
	return false;
}


function objectTag() {

  	var str_B_Start = '';
	var str_B_End = '';
	var str_B_Alternative = '';
	var str_Boucle_Start = '';
	var str_Boucle_End = '';
	var str_content = '';
	var str_Param = '';
	var str_br = '' ;
	
	var f = document.forms[0] ;
	
	var affichage_balise = f.affichage_balise.options[f.affichage_balise.selectedIndex].value;
	var balise_name = f.name.value ;
  	var balise_type = f.balisetype.options[f.balisetype.selectedIndex].value;
  	var balise_B = f.balise_B.value ;
  	var balise_BB = f.balise_BB.value ;
	var balise_BOUCLE = f.balise_BOUCLE.value ;
	
	// -- Utilisation des balises <BR> dans le code
	if( f.br_add.checked ){
			str_br = '<BR>' ;
	}

		
	// -- Msg d'alerte - Abscence de nom pour la balise
	if ( f.balise_BOUCLE.checked && balise_name == ''){
	
		alert( msg_abs_nom );		
  		balise_name = "Votre_Boucle" ;
  	}	
	
	
	// -- Ajout de '_' si 'balise_name' est de type texte
 	var reg_balise_name = /([^0-9])/; 

	if ( reg_balise_name.exec(balise_name)){
		// Suppresion des espaces
		balise_name = balise_name.replace( /([\s\W])/gi , '_') ;
		balise_name = "_" + balise_name ;		
  	}		


	// -- Parametre
	function tab_critere(tab, critere_base ) {
		var critere = '' ;
		var str = '' ;
		for ( var k=0 ; k < tab[balise_type].length ; k++ ){	
			critere = critere_base + tab[balise_type][k] ;						
			if ( eval("f."+critere+".checked") ){
				str  += '{' + tab[balise_type][k] + '}';
			}	
		} 
		return str ;
	}
				
	str_Param  += tab_critere(tab_selection, 'select__' ) ;
	str_Param  += tab_critere(tab_affichage, 'affichage__' ) ;	
	str_Param = str_Param.replace( /___/gi , ' = ') ;
	str_Param = str_Param.replace( /_N_/gi , ' != ') ;
	
	// gestion des crit�re type 'par hasard'
	str_Param = str_Param.replace( /__/gi , ' ') ;	
	
	// -- Balise � afficher
	str_content += str_br + '\n';
	var balise = '' ;
	var balise_content = '';
	var balise_add_0 = ' ' ;
	var balise_add_1 = ' ' + str_br + '\n' ;
	
	if( affichage_balise != "rien" ){

		if( f.balise_add.checked ){
			balise_add_0 = ' [(#' ;
			balise_add_1 = ')] ' + str_br + '\n' ;
		}
		else{
			balise_add_0 = ' #' ;
			balise_add_1 = ' ' + str_br + '\n' ;
		}
		
		for ( var i=0 ; i < tab_balise[balise_type].length ; i++ ){	
			balise = tab_balise [balise_type][i] ;
			balise_content = balise_add_0 +  balise + balise_add_1 ;

			if ( affichage_balise != "tout" ){				
				if ( eval("f."+balise+".checked") ){
					str_content += balise_content ;
				}
			}
			else{
					str_content += balise_content ;
			}
		}  
	}


	// -- Balise <BOUCLE> & alternative <B></B>  <//B>
	
	if ( f.balise_BOUCLE.checked ){

		// -- Balise <BOUCLE>
		str_Boucle_Start += '<BOUCLE'  + balise_name +'('+ balise_type +')';
		str_Boucle_Start += str_Param ;
		str_Boucle_Start += '>\n'  ;
		
		str_Boucle_End += str_br + '\n' + '</BOUCLE' + balise_name +'>\n';
		
		// -- Balise alternative <B></B>
		if ( f.balise_B.checked ){
			str_B_Start = '<B'  + balise_name +'>\n';
			str_B_End = '</B'  + balise_name +'>\n';
		}
	
		// -- Balise alternative <//B>
		if ( f.balise_BB.checked  ){
			str_B_Alternative = '<//B'  + balise_name +'>\n';
		}
	}
	
	// -- Msg d'Erreur :
	// si BOUCLE est d�sactiv�, il n'est pas possible d'utiliser <B><//B>
	if ( !f.balise_BOUCLE.checked && ( f.balise_B.checked || f.balise_BB.checked )) { 
		alert( msg_use_boucle );
	}

		
	// -- G�n�ration...	
	var beginWrap = str_B_Start + str_Boucle_Start ;
		beginWrap += str_br + str_content + str_br ;
			
	var endWrap = str_Boucle_End + str_B_End + str_B_Alternative ;
	
	// -- G�n�ration pour MX	
	// Manually wrap tags around selection.
	// var dom = dw.getDocumentDOM();
	// dom.source.wrapSelection(beginWrap,endWrap);
	// Just return -- don't do anything else.
	// return;
	
	// -- G�n�ration pour DW 3/4
	 str_output =  beginWrap + endWrap ;		
	 return str_output ;
}

// ----------------------------------------------------------------------
//   Copyright (C) 2002 Fabrice Gangler (contact@drop-zone-city.com)
//
//    www.drop-zone-city.com					/// SPIP_balise.js //////
// ----------------------------------------------------------------------