/*
 * Created on 28-apr-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL		
 * 
 */
package com.spip.modelli;


/**
 * @date 28-apr-2005
 * @author Renato Formato
 *
 * A JMenuAA that is Locale aware 
 * 
 */
public class JMenuAALocalized extends JMenuAA implements LocaleListener {

	private String msg_ID;

	/**
	 * @param s String identifier of the localized string
	 */
	public JMenuAALocalized(String id) {
		super(Messages.getString(id));
		msg_ID = id;
	}

	/**
	 * @param id String identifier of the localized string
	 * @param b
	 */
	public JMenuAALocalized(String id, boolean b) {
		super(Messages.getString(id), b);
		msg_ID = id;
	}
	
	public void changedLocale() {
		setText(Messages.getString(msg_ID));
	}

}
