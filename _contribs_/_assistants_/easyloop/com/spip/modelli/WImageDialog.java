/*
 * Created on 7-mag-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL
 * 
 */
package com.spip.modelli;

import java.awt.Container;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @date 7-mag-2005
 * @author Renato Formato
 *
 */
public class WImageDialog extends JDialog implements ActionListener {

	public static String[] showImageDialog(Frame owner) {
		WImageDialog id = new WImageDialog(owner);
		id.show();
		return null;
	}

	private JTextField address,altText,width,height;
	
	/**
	 * @param owner
	 * @throws java.awt.HeadlessException
	 */
	protected WImageDialog(Frame owner) throws HeadlessException {
		super(owner, Messages.getString("WImageDialog.Title"), true);
		addGUI();
	}
	
	private void addGUI() {
		JPanel panel,panel2;
		JLabel label;
		JButtonAA button;
		Container cp = getContentPane();
		
		GridBagConstraints c = new GridBagConstraints(); 
		c.gridx = GridBagConstraints.RELATIVE;
		c.anchor = GridBagConstraints.LINE_END;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(2,2,2,2);
		panel = new JPanel(new GridBagLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(5,10,5,10));
		
		label = new JLabel(Messages.getString("WImageDialog.FileAddress"));
		panel.add(label,c);
		address = new JTextField(10);
		panel.add(address,c);
		
		panel2 = new JPanel(new GridBagLayout());
		
		c.gridy = 0;
		button = new JButtonAALocalized("WImageDialog.File");
		panel2.add(button,c);
		button = new JButtonAA("<-2");
		panel2.add(button,c);
		button = new JButtonAA("<-1");
		panel2.add(button,c);
		button = new JButtonAA("<-0");
		panel2.add(button,c);
		button = new JButtonAA("<");
		panel2.add(button,c);
		
		c.gridy = 1;
		c.gridwidth = 2;
		panel.add(panel2,c);
		
		c.gridy = 2;
		c.gridwidth = 1;
		label = new JLabel(Messages.getString("WImageDialog.AltText"));
		panel.add(label,c);
		altText = new JTextField(10);
		panel.add(altText,c);
		c.gridy = 3;
		label = new JLabel(Messages.getString("WImageDialog.Size"));
		panel.add(label,c);
		
		panel2 = new JPanel();
		panel2.setLayout(new BoxLayout(panel2,BoxLayout.LINE_AXIS));
		width = new JTextField();
		panel2.add(width);
		panel2.add(Box.createHorizontalStrut(4));
		height = new JTextField();
		panel2.add(height);
		panel.add(panel2,c);
		
		c.gridy = 4;
		c.gridwidth = 2;
		panel2 = new JPanel();
		panel2.setLayout(new BoxLayout(panel2,BoxLayout.LINE_AXIS));
		button = new JButtonAALocalized("Dialogs.Cancel");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				hide();
			}
		});
		panel2.add(button);
		panel2.add(Box.createHorizontalGlue());
		button = new JButtonAALocalized("Dialogs.Enter");
		button.addActionListener(this);
		panel2.add(button);
		
		panel.add(panel2,c);
		
		cp.add(panel);
		pack();
		setLocationRelativeTo(getParent());
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		hide();
	}
}
