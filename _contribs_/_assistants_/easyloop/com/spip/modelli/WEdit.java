/*
 * Created on 6-apr-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL
 * 				  
 */
package com.spip.modelli;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.undo.UndoManager;

import edu.stanford.ejalbert.BrowserLauncher;

/**
 * @author Renato Formato
 * 
 * 
 */
public class WEdit extends JFrame implements ActionListener, UndoableEditListener, CaretListener {

	public static boolean AA=false;//Force Antialias (default=off)
		
	//Drawer declarations
	JDrawer jd;
	JTreeAA jtree;
	JButtonAA btnCustom;
	//Toolbar declarations
	JToolBar jt;
	JButtonAALocalized btnCredits;
	JButtonAALocalized btnOpen;
	JButtonAALocalized btnSave;
	JButtonAALocalized btnView;
	JButtonAALocalized btnCss;
	JButtonAALocalized btnLoops;
	JButtonAALocalized btnHelp;
	JButtonAALocalized btnLibrary;
	//Dichiarazioni Editor
	JTextAreaAA editor;
	//Dichiarazioni bottoni editor
	JPanel bp;
	JButtonAALocalized btnHeader;
	JPopupMenu popupHeader;
	JButtonAALocalized btnEntity;
	JPopupMenu popupEntity,popupLoops;
	JButtonAALocalized btnColor;
	JButtonAALocalized btnURL;
	JButtonAALocalized btnImage;
	JButtonAALocalized btnLink;
	JButtonAALocalized btnList;
	JButtonAALocalized btnTable;
	JCheckBoxAALocalized btnAccent;
	//Style buttons declarations
	JComboBoxAA fontsize;
	JComboBoxAA font;
	JCheckBoxAALocalized bold;
	JButtonAA tableft;
	JButtonAA tabright;
	//Menu declarations
	JMenuBar menu;
	//Drawer data declaration
	Vector sections;
	//mouse adapter declaration
	MyMouseAdapter ma;
	//Editor managment declarations
	int start,end;
	int sel_start,sel_end;
	HashMap actions;
	UndoManager undo;
	String lastSearch;
	//Current file
	String current_file;
	
	
	private class MyMouseAdapter extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount()!=2) return;
			TreePath tp = jtree.getSelectionPath();
			if (tp==null) return;
			if (tp.getPathCount()==0) return;
			String sez,string,dato;
			sez = (String)((DefaultMutableTreeNode)tp.getPathComponent(1)).getUserObject();
			string = (String)((DefaultMutableTreeNode)tp.getLastPathComponent()).getUserObject(); 
			if (sez.equals(string)) return;
			dato = getdato(sez,string);
			try {
				String[] tabs = getInitialWhitespace();
				Matcher m = Pattern.compile("(?m)^(\t| {1,3})*").matcher(dato);
				String group,firstline,therest;
				if (m.find()) {
					group = m.group();
					int endlineoffeset = dato.indexOf('\n');
					firstline = dato.substring(0,endlineoffeset+1);
					therest = dato.substring(endlineoffeset+1); 
					therest = therest.replaceAll("(?m)^(\t| {1,3})*",group + tabs[0]);
					if (therest.matches("\n$")) therest.replaceAll("\n$",tabs[1]);
					dato = firstline + therest;
				}
				if (editor.getSelectedText()!=null) dato = dato.replaceAll("<Recup />",editor.getSelectedText()); //$NON-NLS-1$
				editor.replaceSelection(dato);
			} catch (Exception ex) {};
		}
	}
	
	public WEdit() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setTitle(Messages.getString("MainWindow.Title")); //$NON-NLS-1$
		ma = new MyMouseAdapter();
		loadData();
		addGUI();
		URL url = WEdit.class.getResource("/com/spip/modelli/easyloop_icon.png");
		setIconImage(new ImageIcon(url).getImage());
		initPref();
	}
	
	/* (non-Javadoc)
	 * @see java.awt.Container#paint(java.awt.Graphics)
	 */
	public void paint(Graphics g) {
		if (AA) {
		    Graphics2D G = (Graphics2D) g;
		    G.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		}
		super.paint(g);
	}
	
	private void initPref() {
		font.setSelectedIndex(1);
		fontsize.setSelectedIndex(2);
	}
	
	private void addGUI() {
		Container cp = this.getContentPane();
		cp.add(addToolbar(),BorderLayout.NORTH);
		cp.add(addCenterPane(),BorderLayout.CENTER);
		addMenu();
		jd = new JDrawer(this,JDrawer.EAST);
		jtree = new JTreeAA();
		populateTree(jtree);
		JScrollPane sp = new JScrollPane(jtree);
		Container cpd = jd.getContentPane();
		cpd.setLayout(new BoxLayout(cpd,BoxLayout.Y_AXIS));
		sp.setAlignmentX(1);
		cpd.add(sp);
		btnCustom = new JButtonAA("+"); //$NON-NLS-1$
		btnCustom.setAlignmentX(1);
		btnCustom.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createEmptyBorder(4,4,4,4),btnCustom.getBorder()));
		cpd.add(btnCustom);
		jd.setSize(300,420);
	}
	
	private void addMenu() {
		menu = new JMenuBarAA();
		JMenuAA mymenu = new JMenuAALocalized("Menu.File"); //$NON-NLS-1$
		mymenu.setMnemonic(KeyEvent.VK_F);
		JMenuItemAA myitem;
		mymenu.add(createMenu("Menu.New",KeyEvent.VK_N,"ctrl N","fileNew")); //$NON-NLS-1$ //$NON-NLS-2$
		mymenu.add(createMenu("Menu.OpenRec",0,null,null,false)); //$NON-NLS-1$
		mymenu.add(createMenu("Menu.Open",KeyEvent.VK_A,"ctrl O","fileOpen")); //$NON-NLS-1$ //$NON-NLS-2$
		mymenu.addSeparator();
		mymenu.add(createMenu("Menu.Save",KeyEvent.VK_S,"ctrl S","fileSave",false)); //$NON-NLS-1$ //$NON-NLS-2$
		mymenu.add(createMenu("Menu.SaveName",KeyEvent.VK_C,"ctrl shift S","fileSaveName",false)); //$NON-NLS-1$ //$NON-NLS-2$
		mymenu.add(createMenu("Menu.SaveIn",0,"ctrl alt S",null,false)); //$NON-NLS-1$ //$NON-NLS-2$
		mymenu.add(createMenu("Menu.SaveVers",0,null,null,false));
		mymenu.addSeparator();
		mymenu.add(createMenu("Menu.FTP",0,null,null,false));
		mymenu.addSeparator();
		mymenu.add(createMenu("Menu.Biblio",KeyEvent.VK_B,(String)null,null,false)); //$NON-NLS-1$
		mymenu.add(createMenu("Menu.Update",KeyEvent.VK_O,(String)null,null,false)); //$NON-NLS-1$
		mymenu.addSeparator();
		mymenu.add(createMenu("Menu.SetPage",KeyEvent.VK_I,(String)null,null,false)); //$NON-NLS-1$
		mymenu.add(createMenu("Menu.Print",KeyEvent.VK_P,(String)null,null,false)); //$NON-NLS-1$
		mymenu.addSeparator();
		mymenu.add(createMenu("Menu.Quit",KeyEvent.VK_E,"ctrl Q",null)); //$NON-NLS-1$ //$NON-NLS-2$
		menu.add(mymenu);
		mymenu = new JMenuAALocalized("Menu.Edit"); //$NON-NLS-1$
		mymenu.setMnemonic(KeyEvent.VK_M);
		mymenu.add(createMenu("Menu.Undo",KeyEvent.VK_A,"ctrl Z","annulla")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		mymenu.addSeparator();
		mymenu.add(createMenu("Menu.Cut",KeyEvent.VK_T,"ctrl X","taglia")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		mymenu.add(createMenu("Menu.Copy",KeyEvent.VK_C,"ctrl C","copia")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		mymenu.add(createMenu("Menu.Paste",KeyEvent.VK_I,"ctrl V","incolla")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		mymenu.add(createMenu("Menu.Delete",KeyEvent.VK_N,"DELETE","cancella")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		mymenu.addSeparator();
		mymenu.add(createMenu("Menu.SelAll",KeyEvent.VK_Z,"ctrl A","Sel_tutto")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		mymenu.addSeparator();
		mymenu.add(createMenu("Menu.NoTab",0,(String)null,"0indenta")); //$NON-NLS-1$ //$NON-NLS-2$
		mymenu.add(createMenu("Menu.+Tab",0,(String)null,"+indenta")); //$NON-NLS-1$ //$NON-NLS-2$
		mymenu.add(createMenu("Menu.-Tab",0,(String)null,"-indenta")); //$NON-NLS-1$ //$NON-NLS-2$
		mymenu.addSeparator();
		mymenu.add(createMenu("Menu.FindRepl",KeyEvent.VK_S,"ctrl F","trova_sost")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		mymenu.add(createMenu("Menu.FindNext",KeyEvent.VK_S,"F3","ripeti_trova",false)); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		mymenu.add(createMenu("Menu.NumLin",0,(String)null,"numera")); //$NON-NLS-1$ //$NON-NLS-2$
		mymenu.addSeparator();
		mymenu.add(createMenu("Menu.Maiusc",0,null,"maiusc",false)); //$NON-NLS-1$ //$NON-NLS-2$		
		mymenu.add(createMenu("Menu.Minusc",0,null,"1minusc",false)); //$NON-NLS-1$ //$NON-NLS-2$
		mymenu.add(createMenu("Menu.minusc",0,null,"minusc",false)); //$NON-NLS-1$ //$NON-NLS-2$	
		mymenu.add(createMenu("Menu.NoTag",0,null,"notag",false)); //$NON-NLS-1$ //$NON-NLS-2$
		mymenu.add(createMenu("Menu.AutoPTag",0,null,"instagp",false)); //$NON-NLS-1$ //$NON-NLS-2$
		mymenu.addSeparator();
		myitem = createMenu("Menu.Conv",0,(String)null,"convert"); //$NON-NLS-1$ //$NON-NLS-2$
		mymenu.add(myitem);
		mymenu.addSeparator();
		mymenu.add(createMenu("Menu.Pref",0,
				KeyStroke.getKeyStroke(KeyEvent.VK_COMMA,InputEvent.CTRL_MASK),"menu_modifica"));
		menu.add(mymenu);
		menu.add(creaMenuLang());
		mymenu = new JMenuAALocalized("Menu.Window"); //$NON-NLS-1$
		mymenu.setMnemonic(KeyEvent.VK_N);
		mymenu.add(createMenu("Menu.Minimize",0,null,null,false)); //$NON-NLS-1$
		mymenu.add(createMenu("Menu.Maximize",0,null,null,false)); //$NON-NLS-1$
		mymenu.addSeparator();
		mymenu.add(createMenu("Menu.RipAll",0,null,null,false)); //$NON-NLS-1$
		menu.add(mymenu);
		mymenu = new JMenuAALocalized("Menu.Help"); //$NON-NLS-1$
		mymenu.setMnemonic(KeyEvent.VK_QUOTE);
		mymenu.setDisplayedMnemonicIndex(0);
		mymenu.add(createMenu("Menu.Guide",KeyEvent.VK_G,"ctrl typed ?",null,false));  //$NON-NLS-1$ //$NON-NLS-2$
		mymenu.addSeparator();
		mymenu.add(createMenu("Menu.About",KeyEvent.VK_I,(String)null,null)); //$NON-NLS-1$

		menu.add(mymenu);
		setJMenuBar(menu);
	}
	
	private void loadData() {
		File rulesFile = new File(Messages.getString("WEdit.LibraryName")); //$NON-NLS-1$
		if (rulesFile.exists() && rulesFile.isFile()) {
			try {
				BufferedReader br = new BufferedReader(new FileReader(rulesFile));
				String line = br.readLine();
				String[] fields;
				Object[] sectionData;
				sections = new Vector();
				Vector subSections;
				int size=0;
				while (line!=null) {
					fields = line.split("\\+\\+\\+"); //$NON-NLS-1$
					if (fields.length==1 && line.trim().length()!=0) {
						sectionData = new Object[2];//
						sectionData[0] = line; //name of the section
						subSections = new Vector();
						sectionData[1] = subSections; //vector of subsections
						sections.add(sectionData);
						size++;
					} else if (fields.length==2) { 
						//adds a subsection to the last section
						((Vector)((Object[])sections.get(size-1))[1]).add(fields);
					}
					line = br.readLine();
				}
				br.close();
			} catch (IOException ex) {ex.printStackTrace();}
		} else {System.out.println(Messages.getString("WEdit.RulesNotFound"));} //$NON-NLS-1$
	}
	
	private void populateTree(JTree t) {
		//don't know why but if i don't put this line of code the tree will show empty
		//as soon as the user changes locale.
		t.setRootVisible(true); 
		DefaultMutableTreeNode root = new DefaultMutableTreeNode("root"),nodo,nodo2; //$NON-NLS-1$
		((DefaultTreeModel)t.getModel()).setRoot(root);
		DefaultTreeCellRenderer r = (DefaultTreeCellRenderer)t.getCellRenderer();
		r.setLeafIcon(null);
		r.setClosedIcon(null);
		r.setOpenIcon(null);
		Object[] sezione,voce;
		Vector voci;
		int size1 = sections.size();
		for (int i = 0; i < size1; i++) {
			sezione = (Object[])sections.get(i); 
			nodo = new DefaultMutableTreeNode(sezione[0]);
			voci = (Vector)sezione[1];
			int size2 = voci.size();
			for (int j = 0; j < size2; j++) {
				voce = (Object[])voci.get(j);
				if (((String)voce[0]).toLowerCase().startsWith("<html>")) { //$NON-NLS-1$
					voce[0] = " " + voce[0];} //$NON-NLS-1$
				nodo2 = new DefaultMutableTreeNode(voce[0]);
				nodo.add(nodo2);
			}
			root.add(nodo);
		}
		t.expandRow(0);
		t.setRootVisible(false);
		t.setShowsRootHandles(true);
		t.addMouseListener(ma);
	}
	
	private JButtonAALocalized createLocalizedButton(String text,String actCommand) {
		JButtonAALocalized jb = new JButtonAALocalized(text);
		jb.addActionListener(this);
		jb.setActionCommand(actCommand);
		return jb;
	}
	
	private JToolBar addToolbar() {
		jt = new JToolBar(JToolBar.HORIZONTAL);
		jt.setRollover(true);
		btnCredits = createLocalizedButton("WEdit.Templates",null); //$NON-NLS-1$ 
		jt.add(btnCredits);
		jt.addSeparator();
		btnOpen = createLocalizedButton("WEdit.Open","fileOpen"); //$NON-NLS-1$
		jt.add(btnOpen);
		btnSave = createLocalizedButton("WEdit.Save","fileSave"); //$NON-NLS-1$
		btnSave.setEnabled(false);
		jt.add(btnSave);
		jt.addSeparator();
		btnCss = createLocalizedButton("WEdit.CSS",null); //$NON-NLS-1$
		jt.add(btnCss);
		creaPopupCicli();
		btnLoops = createLocalizedButton("WEdit.BOUCLE","popupLoops"); //$NON-NLS-1$
		jt.add(btnLoops);
		jt.addSeparator();
		btnView = createLocalizedButton("WEdit.Show","fileShow"); //$NON-NLS-1$
		jt.add(btnView);
		jt.add(Box.createHorizontalGlue());
		btnHelp = new JButtonAALocalized("WEdit.Help"); //$NON-NLS-1$
		jt.add(btnHelp);
		jt.addSeparator();
		btnLibrary = new JButtonAALocalized("WEdit.Library"); //$NON-NLS-1$
		btnLibrary.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jd.toggle();
			}
		});
		jt.add(btnLibrary);
		return jt;
	}
	
	private void creaPopupCicli() {
		JMenuItemAA menuitem; 
		popupLoops = new JPopupMenu();
		popupLoops.add(createMenu("WEdit.SECTIONS",0,(String)null,"openRubriques"));
		popupLoops.add(createMenu("WEdit.ARTICLES",0,(String)null,"openArticles"));
		popupLoops.add(createMenu("WEdit.NEWS",0,(String)null,"openBreves"));
		popupLoops.add(createMenu("WEdit.SITES",0,(String)null,"openSites"));
		popupLoops.add(createMenu("WEdit.SYNDIC_ARTICLES",0,(String)null,"openSyndic"));
		popupLoops.add(createMenu("WEdit.KEYWORDS",0,(String)null,"openMots"));
		popupLoops.add(createMenu("WEdit.AUTHORS",0,(String)null,"openAuthors"));
		popupLoops.add(createMenu("WEdit.FORUM",0,(String)null,"openForum"));
		popupLoops.add(createMenu("WEdit.DOCUMENTS",0,(String)null,"openDocuments"));
		popupLoops.add(createMenu("WEdit.HIERARCHY",0,(String)null,"openHierarchy"));
		popupLoops.add(new JSeparator());
		popupLoops.add(createMenu("WEdit.THISSITE",0,(String)null,"openThisSite"));
	}
	
	private JPanel addCenterPane() {
		JPanel centerp = new JPanel(new BorderLayout());
		centerp.add(addButtonPane(),BorderLayout.NORTH);
		editor = new JTextAreaAA();
		undo = new UndoManager();
		editor.getDocument().addUndoableEditListener(this);
		editor.addCaretListener(this);
		editor.setLineWrap(true);
		editor.setWrapStyleWord(true);
		editor.setTabSize(3);
		createActionMap(editor);
		JScrollPane sp = new JScrollPane(editor);
		centerp.add(sp,BorderLayout.CENTER);
		centerp.add(addCharacterPane(),BorderLayout.SOUTH);
		return centerp;
	}
	
	private JPanel addButtonPane() {
		bp = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		creaPopupHeader();
		c.gridy=1;
		c.anchor=GridBagConstraints.LINE_END;
		c.insets=new Insets(1,1,1,1);
		btnHeader = createLocalizedButton("WEdit.Html","popupHeader"); //$NON-NLS-1$ $NON-NLS-2$ 
		c.weightx=1;
		bp.add(btnHeader,c);
		creaPopupEntity();
		btnEntity = createLocalizedButton("WEdit.Char","popupEntity"); //$NON-NLS-1$ $NON-NLS-2$
		c.weightx=0;
		bp.add(btnEntity,c);
		c.fill=GridBagConstraints.BOTH;
		btnLink = createLocalizedButton("WEdit.Link","openLink"); //$NON-NLS-1$ $NON-NLS-2$
		bp.add(btnLink,c);
		btnList = createLocalizedButton("WEdit.List","openList"); //$NON-NLS-1$ $NON-NLS-2$
		bp.add(btnList,c);
		btnTable = createLocalizedButton("WEdit.Table","openTable"); //$NON-NLS-1$ $NON-NLS-2$
		bp.add(btnTable,c);
		c.gridx=2;
		c.gridy=0;
		btnColor = createLocalizedButton("WEdit.Colors","openColor"); //$NON-NLS-1$ $NON-NLS-2$
		bp.add(btnColor,c);
		c.gridx=GridBagConstraints.RELATIVE;
		btnURL = createLocalizedButton("WEdit.URL","openURL"); //$NON-NLS-1$
		bp.add(btnURL,c);
		btnImage = createLocalizedButton("WEdit.Image","openImage"); //$NON-NLS-1$
		bp.add(btnImage,c);
		c.gridx=5;
		c.gridheight=2;
		btnAccent = new JCheckBoxAALocalized("WEdit.Accents"); //$NON-NLS-1$
		bp.add(btnAccent,c);
		return bp;
	}
	
	private void creaPopupEntity() {
		JMenuItemAA menuitem; 
		popupEntity = new JPopupMenu();
		popupEntity.add(createMenu("WEdit.And",0,(String)null,"insertAnd")); //$NON-NLS-1$
		popupEntity.add(createMenu("WEdit.Ampersand",0,(String)null,"insertAmpersand")); //$NON-NLS-1$
		popupEntity.add(createMenu("WEdit.GT",0,(String)null,"insertGT")); //$NON-NLS-1$
		popupEntity.add(createMenu("WEdit.LT",0,(String)null,"insertLT")); //$NON-NLS-1$
		popupEntity.add(createMenu("WEdit.NumbSign",0,(String)null,"insertNumbSign")); //$NON-NLS-1$
		popupEntity.add(createMenu("WEdit.At",0,(String)null,"insertAt")); //$NON-NLS-1$
		popupEntity.add(createMenu("WEdit.LBr",0,(String)null,"insertLBr")); //$NON-NLS-1$
		popupEntity.add(createMenu("WEdit.RBr",0,(String)null,"insertRBr")); //$NON-NLS-1$
		popupEntity.add(createMenu("WEdit.Nbsp",0,(String)null,"insertNbsp")); //$NON-NLS-1$
	}
	
	private void creaPopupHeader() {
		JMenuItemAA menuitem; 
		popupHeader = new JPopupMenu();
		popupHeader.add(createMenu("WEdit.H1",0,(String)null,"insertH1")); //$NON-NLS-1$
		popupHeader.add(createMenu("WEdit.H2",0,(String)null,"insertH2")); //$NON-NLS-1$
		popupHeader.add(createMenu("WEdit.H3",0,(String)null,"insertH3")); //$NON-NLS-1$
		popupHeader.addSeparator();
		popupHeader.add(createMenu("WEdit.Paragraph",0,(String)null,"insertParagraph")); //$NON-NLS-1$
		popupHeader.add(createMenu("WEdit.BoldTag",0,(String)null,"insertBoldTag")); //$NON-NLS-1$
		popupHeader.add(createMenu("WEdit.Italic",0,(String)null,"insertItalic")); //$NON-NLS-1$
		popupHeader.add(createMenu("WEdit.BrLine",0,(String)null,"insertBrLine")); //$NON-NLS-1$
		popupHeader.add(createMenu("WEdit.HrLine",0,(String)null,"insertHrLine")); //$NON-NLS-1$
		popupHeader.addSeparator();
		popupHeader.add(createMenu("WEdit.Span",0,(String)null,"insertSpan")); //$NON-NLS-1$
		popupHeader.add(createMenu("WEdit.Div",0,(String)null,"insertDiv")); //$NON-NLS-1$
		popupHeader.add(createMenu("WEdit.Pre",0,(String)null,"insertPre")); //$NON-NLS-1$
		popupHeader.addSeparator();
		popupHeader.add(createMenu("WEdit.Acro",0,(String)null,"insertAcro")); //$NON-NLS-1$
		popupHeader.add(createMenu("WEdit.Comment",0,(String)null,"insertComment")); //$NON-NLS-1$
		popupHeader.addSeparator();
		popupHeader.add(createMenu("WEdit.UnList",0,(String)null,"insertUnList")); //$NON-NLS-1$
		popupHeader.add(createMenu("WEdit.ListItem",0,(String)null,"insertListItem")); //$NON-NLS-1$
		popupHeader.addSeparator();
		popupHeader.add(createMenu("WEdit.TableTag",0,(String)null,"insertTableTag")); //$NON-NLS-1$
		popupHeader.add(createMenu("WEdit.TableRow",0,(String)null,"insertTableRow")); //$NON-NLS-1$
		popupHeader.add(createMenu("WEdit.TableHeader",0,(String)null,"insertTableHeader")); //$NON-NLS-1$
		popupHeader.add(createMenu("WEdit.TableCell",0,(String)null,"inseritTableCell")); //$NON-NLS-1$
	}

	private JPanel addCharacterPane() {
		JPanel cp = new JPanel();
		Dimension d = new Dimension(5,0);
		cp.setLayout(new BoxLayout(cp,BoxLayout.X_AXIS));
		cp.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		String[] sizes = {"9","10","12","14","16"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		fontsize = new JComboBoxAA(sizes);
		fontsize.addActionListener(this);
		fontsize.setMaximumSize(new Dimension(100,100));
		fontsize.setPreferredSize(new Dimension(50,0));
		cp.add(fontsize);
		cp.add(Box.createRigidArea(d));
		String[] fonts = {"Courier New","Arial","Tahoma","Times New Roman","Georgia"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		font = new JComboBoxAA(fonts);
		font.addActionListener(this);
		font.setMaximumSize(new Dimension(200,100));
		font.setPreferredSize(new Dimension(150,0));
		cp.add(font);
		bold = new JCheckBoxAALocalized("WEdit.Bold"); //$NON-NLS-1$
		bold.addActionListener(this);
		cp.add(bold);
		tableft = new JButtonAA("<="); //$NON-NLS-1$
		tableft.setActionCommand("-indenta"); //$NON-NLS-1$
		tableft.addActionListener(this);
		cp.add(tableft);
		cp.add(Box.createRigidArea(d));
		tabright = new JButtonAA("=>"); //$NON-NLS-1$
		tabright.setActionCommand("+indenta"); //$NON-NLS-1$
		tabright.addActionListener(this);
		cp.add(tabright);
		cp.add(Box.createHorizontalGlue());
		return cp;
	}

	public void actionPerformed(ActionEvent e) {
		Object s = e.getSource();
		if (s.equals(bold)) {
			int stile = (bold.isSelected())?Font.BOLD:Font.PLAIN;
			editor.setFont(editor.getFont().deriveFont(stile));
			return;
		} 
		if (s.equals(fontsize)) {
			float size = new Float(fontsize.getSelectedItem().toString()).floatValue();
			editor.setFont(editor.getFont().deriveFont(size));
			return;
		} 
		if (s.equals(font)) {
			Font oldfont = editor.getFont();
			editor.setFont(new Font(
				(String)font.getSelectedItem(),
				oldfont.getStyle(),
				oldfont.getSize()
			));
			return;
		}
		try {
			String actionCommand = e.getActionCommand();
			if (actionCommand.equals("-indenta")) { //$NON-NLS-1$
				String sel = getSelectedLines(editor);
				int del_tabs_1=0,del_tabs = 0,old_length;
				String[] linee = sel.split("\n");  //$NON-NLS-1$
				StringBuffer sb = new StringBuffer();
				for (int i = 0; i < linee.length; i++) {
					old_length = linee[i].length();
					linee[i] = linee[i].replaceFirst("^(\t| {1,3})",""); //$NON-NLS-1$ //$NON-NLS-2$
					del_tabs+=old_length-linee[i].length();
					if (i==0) del_tabs_1 = del_tabs;
					sb.append(linee[i]+"\n"); //$NON-NLS-1$
				}
				editor.replaceRange(sb.toString(),start,end);
				editor.grabFocus();
				if (sel_start==start) {sel_start++;}
				editor.select(sel_start-del_tabs_1,sel_end-del_tabs);
				return;
			}
			if (actionCommand.equals("+indenta")) { //$NON-NLS-1$
				String sel = getSelectedLines(editor);
				String[] linee = sel.split("\n"); //$NON-NLS-1$
				StringBuffer sb = new StringBuffer();
				for (int i = 0; i < linee.length; i++) {
					linee[i] = '\t' + linee[i];
					sb.append(linee[i]+"\n"); //$NON-NLS-1$
				}
				editor.replaceRange(sb.toString(),start,end);
				editor.grabFocus();
				if (sel_start==start) {sel_start--;}
				editor.select(sel_start+1,sel_end+linee.length);
				return;
				}
			if (actionCommand.equals("0indenta")) { //$NON-NLS-1$
				String sel = getSelectedLines(editor);
				int del_tabs_1 = 0,del_tabs = 0,old_length;
				String[] linee = sel.split("\n");  //$NON-NLS-1$
				StringBuffer sb = new StringBuffer();
				for (int i = 0; i < linee.length; i++) {
					old_length = linee[i].length();
					linee[i] = linee[i].replaceFirst("^(\t| {1,3})*",""); //$NON-NLS-1$ //$NON-NLS-2$
					del_tabs+=old_length-linee[i].length();
					if (i==0) del_tabs_1 = del_tabs; 
					sb.append(linee[i]+"\n"); //$NON-NLS-1$
				}
				editor.replaceRange(sb.toString(),start,end);
				editor.grabFocus();
				if (sel_start==start) {sel_start++;}
				editor.select(sel_start-del_tabs_1,sel_end-del_tabs);
				return;
				}
			if (actionCommand.equals("Sel_tutto")) { //$NON-NLS-1$
				editor.grabFocus();
				editor.selectAll();
				return;
				}
			if (actionCommand.equals("copia")) { //$NON-NLS-1$
				editor.grabFocus();
				editor.copy();
				return;
				}
			if (actionCommand.equals("incolla")) { //$NON-NLS-1$
				editor.grabFocus();
				editor.paste();
				return;
				}
			if (actionCommand.equals("taglia")) { //$NON-NLS-1$
				editor.grabFocus();
				editor.cut();
				return;
				}
			if (actionCommand.equals("annulla")) { //$NON-NLS-1$
				editor.grabFocus();
				undo.undo();
				updateUndoState();
				return;
				}	
			if (actionCommand.equals("cancella")) { //$NON-NLS-1$
				editor.grabFocus();
				if (editor.getSelectedText()!=null) {
					editor.replaceSelection(""); //$NON-NLS-1$
				} else {
					int pos = editor.getCaretPosition();
					editor.replaceRange(null,pos,pos+1);
				}
				return;
				}
			if (actionCommand.equals("trova_sost")) { //$NON-NLS-1$
				String[] result = WFindDialog.showFindDialog(this);
				if (result.length==1) {Cerca(result[0]);}
				else if(result.length==2) {findReplace(result);}
				return;
			}	
			if (actionCommand.equals("ripeti_trova")) { //$NON-NLS-1$
				Cerca(lastSearch); return;
			}
			if (actionCommand.equals("numera")) { //$NON-NLS-1$
				WNumDialog wn = new WNumDialog(editor.getText(),this);
				wn.show(); return;
			}		
			if (actionCommand.equals("maiusc")) { //$NON-NLS-1$
				int st=editor.getSelectionStart(); int en=editor.getSelectionEnd();
				editor.replaceSelection(editor.getSelectedText().toUpperCase());
				editor.select(st,en); return;
			}		
			if (actionCommand.equals("minusc")) { //$NON-NLS-1$
				int st=editor.getSelectionStart(); int en=editor.getSelectionEnd();
				editor.replaceSelection(editor.getSelectedText().toLowerCase());
				editor.select(st,en); return;
			}		
			if (actionCommand.equals("1minusc")) { //$NON-NLS-1$
				String word;
				int st=editor.getSelectionStart(); int en=editor.getSelectionEnd();
				String sel = editor.getSelectedText();
				Pattern p = Pattern.compile("\\b[\\w]+\\b"); //$NON-NLS-1$
				Matcher m = p.matcher(sel);
				StringBuffer sb= new StringBuffer();
				while(m.find()) {
					word = sel.substring(m.start(),m.end());
					word = word.substring(0,1).toUpperCase() + word.substring(1).toLowerCase();
					m.appendReplacement(sb,word);
				}
				m.appendTail(sb);
				editor.replaceSelection(sb.toString());
				editor.select(st,en); return;
			}		
			if (actionCommand.equals("notag")) { //$NON-NLS-1$
				String sel = editor.getSelectedText();
				editor.replaceSelection(sel.replaceAll("<[^>]*>","")); return; //$NON-NLS-1$ //$NON-NLS-2$
			}		
			if (actionCommand.equals("instagp")) { //$NON-NLS-1$
				String sel = editor.getSelectedText();
				sel = sel.replaceAll("\n\n","\n</p>\n<p>\n"); //$NON-NLS-1$ //$NON-NLS-2$
				if (sel.indexOf("\n")!=-1) sel = "<p>\n" + sel + "\n</p>"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				editor.replaceSelection(sel); return;
			}
			if (actionCommand.equals("convert")) { //$NON-NLS-1$
				WConversionsDialog.showConversionsDialog(this);
				return;
			}	
			if (actionCommand.startsWith("setLocale")) { //$NON-NLS-1$
				String[] dati = actionCommand.split(":"); 
				Messages.setLocale(new Locale(dati[1]));
				changedLocale();
				return;
			}
			if (actionCommand.startsWith("open")) {
				String openCommand = actionCommand.substring(4);
				if (openCommand.equals("Articles")) { //$NON-NLS-1$
					WArticlesDialog.showLoopDialog(this);
				} else if  (openCommand.equals("Rubriques")) { //$NON-NLS-1$
					WRubriquesDialog.showLoopDialog(this);
				} else if  (openCommand.equals("Breves")) { //$NON-NLS-1$
					WBrevesDialog.showLoopDialog(this);
				} else if  (openCommand.equals("Sites")) { //$NON-NLS-1$
					WSitesDialog.showLoopDialog(this);
				} else if (openCommand.equals("Syndic")) { //$NON-NLS-1$
					WSyndicDialog.showLoopDialog(this);
				} else if (openCommand.equals("Mots")) { //$NON-NLS-1$
					WMotsDialog.showLoopDialog(this);
				} else if (openCommand.equals("Authors")) { //$NON-NLS-1$
					WAuthorsDialog.showLoopDialog(this);
				} else if (openCommand.equals("Forum")) { //$NON-NLS-1$
					WForumDialog.showLoopDialog(this);
				} else if (openCommand.equals("Documents")) { //$NON-NLS-1$
					WDocumentsDialog.showLoopDialog(this);
				} else if (openCommand.equals("Hierarchy")) { //$NON-NLS-1$
					WHierarchyDialog.showLoopDialog(this);
				} else if (openCommand.equals("ThisSite")) { //$NON-NLS-1$
					WSiteTagsDialog.showLoopDialog(this);
				} else if (openCommand.equals("Color")) { //$NON-NLS-1$
						Color result = JColorChooser.showDialog(this,Messages.getString("WColorChooser.Title"),
								new Color(0));		
						if (result!=null) editor.replaceSelection(createColorString(result));
				} else if  (openCommand.equals("Table")) { //$NON-NLS-1$
					WTableDialog.showTableDialog(this);
				} else if  (openCommand.equals("Image")) { //$NON-NLS-1$
					WImageDialog.showImageDialog(this);
				} else if  (openCommand.equals("URL")) { //$NON-NLS-1$
					WURLDialog.showURLDialog(this);
				}
			}
			if (actionCommand.startsWith("file")) {
				String fileCommand = actionCommand.substring(4);
				if (fileCommand.equals("Open")) { //$NON-NLS-1$
					JFileChooser jfc = new JFileChooser();
					int result = jfc.showOpenDialog(this);
					if (result==JFileChooser.APPROVE_OPTION) {
						File f = jfc.getSelectedFile();
						openFile(f);
					}
				} else if (fileCommand.equals("Save")) { //$NON-NLS-1$
					if (current_file==null) {saveFileName();} 
					else saveFile(current_file);
				} else if (fileCommand.equals("SaveName")) { //$NON-NLS-1$
					saveFileName();
				} else if (fileCommand.equals("New")) { //$NON-NLS-1$
					WEdit we = Application.app.createMainWindow();
					we.show();
				}  else if (fileCommand.equals("Show")) { //$NON-NLS-1$
					if (current_file!=null) BrowserLauncher.openURL("file://" + current_file);
				}
			}
			if (actionCommand.startsWith("insert")) { //$NON-NLS-1$
				String insertCommand = actionCommand.substring(6);
				String seltext = editor.getSelectedText();
				if (seltext==null) seltext="";
				if (insertCommand.equals("And")) { //$NON-NLS-1$
					editor.replaceSelection("&");
				} else if (insertCommand.equals("Ampersand")) { //$NON-NLS-1$
					editor.replaceSelection("&amp;");
				} else if (insertCommand.equals("GT")) { //$NON-NLS-1$
					editor.replaceSelection("&lt;");
				} else if (insertCommand.equals("LT")) { //$NON-NLS-1$
					editor.replaceSelection("&gt;");
				} else if (insertCommand.equals("NumbSign")) { //$NON-NLS-1$
					editor.replaceSelection("#");
				} else if (insertCommand.equals("At")) { //$NON-NLS-1$
					editor.replaceSelection("@");
				} else if (insertCommand.equals("LBr")) { //$NON-NLS-1$
					editor.replaceSelection("{");
				} else if (insertCommand.equals("RBr")) { //$NON-NLS-1$
					editor.replaceSelection("}");
				} else if (insertCommand.equals("Nbsp")) { //$NON-NLS-1$
					editor.replaceSelection("&nbsp;");
				} else if (insertCommand.equals("H1")) { //$NON-NLS-1$
					editor.replaceSelection("<h1>" + seltext + "</h1>");
				} else if (insertCommand.equals("H2")) { //$NON-NLS-1$
					editor.replaceSelection("<h2>" + seltext + "</h2>");
				} else if (insertCommand.equals("H3")) { //$NON-NLS-1$
					editor.replaceSelection("<h3>" + seltext + "</h3>");
				} else if (insertCommand.equals("Paragraph")) { //$NON-NLS-1$
					editor.replaceSelection("<p>\n" + seltext + "\n</p>");
				} else if (insertCommand.equals("BoldTag")) { //$NON-NLS-1$
					editor.replaceSelection("<strong>" + seltext + "</strong>");
				} else if (insertCommand.equals("Italic")) { //$NON-NLS-1$
					editor.replaceSelection("<em>" + seltext + "</em>");
				} else if (insertCommand.equals("BrLine")) { //$NON-NLS-1$
					editor.replaceSelection("<br />");
				} else if (insertCommand.equals("HrLine")) { //$NON-NLS-1$
					editor.replaceSelection("<hr />\n");
				} else if (insertCommand.equals("Span")) { //$NON-NLS-1$
					editor.replaceSelection("<span class='sp01'>" + seltext + "</span>");
				} else if (insertCommand.equals("Div")) { //$NON-NLS-1$
					editor.replaceSelection("<div class='dv01'\n>" + seltext + "\n</div>");
				} else if (insertCommand.equals("Pre")) { //$NON-NLS-1$
					editor.replaceSelection("<pre>\n" + seltext + "\n</pre>");
				} else if (insertCommand.equals("Acro")) { //$NON-NLS-1$
					editor.replaceSelection("<acronym title='Comment'>" + seltext + "</acronym>");
				} else if (insertCommand.equals("Comment")) { //$NON-NLS-1$
					editor.replaceSelection("<!-- Comment -->");
				} else if (insertCommand.equals("UnList")) { //$NON-NLS-1$
					String[] tabs = getInitialWhitespace();
					editor.replaceSelection("<ul>\n	<li> </li>\n" + tabs[0] + 
							"\t<li> </li>\n</ul>" + tabs[1]);
				} else if (insertCommand.equals("ListItem")) { //$NON-NLS-1$
					editor.replaceSelection("<li>" + seltext + "</li>");
				} else if (insertCommand.equals("TableTag")) { //$NON-NLS-1$
					String[] tabs = getInitialWhitespace();
					editor.replaceSelection("<table cellspacing='1'>\n" + tabs[0] + 
						"\t<tr>\n" + tabs[0] + "\t\t<th> </th>\n" + tabs[0] + 
						"\t\t<th> </th>\n" + tabs[0] + "\t</tr>\n" + tabs[0] + "\t" +
						"<tr>\n" + tabs[0] + "\t\t<td> </td>\n" + tabs[0] +
						"\t\t<td> </td>\n" + tabs[0] + "\t</tr>\n" + tabs[0] + 
						"</table>" + tabs[1] );
				} else if (insertCommand.equals("TableRow")) { //$NON-NLS-1$
					editor.replaceSelection("<tr>" + seltext + "</tr>");
				} else if (insertCommand.equals("TableHeader")) { //$NON-NLS-1$
					editor.replaceSelection("<th>" + seltext + "</th>");
				} else if (insertCommand.equals("TableCell")) { //$NON-NLS-1$
					editor.replaceSelection("<td>" + seltext + "</td>");
				} else return;
				editor.requestFocus();
			} else if (actionCommand.startsWith("popup")) { //$NON-NLS-1$
				String popupCommand = actionCommand.substring(5);
				if (popupCommand.equals("Header")) {
					popupHeader.show(btnHeader,0,
							Math.round((float)0.8*btnHeader.getHeight()));
				} else if (popupCommand.equals("Entity")) {
					popupEntity.show(btnEntity,0,
							Math.round((float)0.8*btnEntity.getHeight()));
				} else if (popupCommand.equals("Loops")) {
					popupLoops.show(btnLoops,0,
							Math.round((float)0.8*btnLoops.getHeight()));
				}
			}
		} catch (Exception ex) {System.out.println(ex.toString());}
	}
	
	/**
	 * @param f
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void openFile(File f) throws FileNotFoundException, IOException {
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		StringBuffer sb = new StringBuffer((int)f.length());
		String line=br.readLine();
		while (line!=null) {
			sb.append(line + '\n');
			line = br.readLine();
		}
		if (current_file!=null) {
			WEdit we = Application.app.createMainWindow();
			we.openFile(f);
			we.show();
		} else {		
			setTitle(Messages.getString("MainWindow.Title") + " - " +f.getName());
			editor.setText(sb.toString());
			current_file = f.getAbsolutePath();
		}
	}

	/**
	 * @throws IOException
	 */
	private void saveFileName() throws IOException {
		JFileChooser jfc;
		File cf=null;
		if (current_file!=null) {
			cf = new File(current_file);
			jfc = new JFileChooser(cf.getParentFile());
		} else {
			jfc = new JFileChooser();
		}
		jfc.setSelectedFile(cf);
		int result = jfc.showSaveDialog(this);
		if (result==JFileChooser.APPROVE_OPTION) {
			File f = jfc.getSelectedFile();
			saveFile(f.getAbsolutePath());
		}
	}

	/**
	 * @throws IOException
	 */
	private void saveFile(String path) throws IOException {
		File f = new File(path);
		FileWriter fw = new FileWriter(f);
		setTitle(Messages.getString("MainWindow.Title") + " - " +f.getName());
		fw.write(editor.getText());
		fw.close();
		current_file = path;
	}

	private String getdato(String sez,String el) {
		for (int i = 0; i < sections.size(); i++) {
			if (((Object[])sections.get(i))[0]==sez) {
				Vector v = (Vector)((Object[])sections.get(i))[1];
				for (int j = 0; j < v.size(); j++) {
					String[] c = (String[])v.get(j);
					if (c[0]==el) {
						String dato = c[1];
						Matcher m = Pattern.compile("\\.(/+)\\.").matcher(dato); //$NON-NLS-1$
						StringBuffer sb = new StringBuffer();
						boolean trovato=false;
						while (m.find()) {
							trovato=true;
							StringBuffer newsb = new StringBuffer();
							for(i=0;i<m.group(1).length();i++) newsb.append("\n");  //$NON-NLS-1$
							m.appendReplacement(sb,newsb.toString());
						}
						m.appendTail(sb);
						if (trovato && sb.charAt(sb.length()-1)!='\n') sb.append('\n'); 
						return sb.toString();
					}
				}
			} 
		}
		return ""; //$NON-NLS-1$
	}

	private String getSelectedLines(JTextArea t) {
		int start_line,end_line;
		try {
		sel_start = t.getSelectionStart();
		start_line = t.getLineOfOffset(sel_start);
		sel_end = t.getSelectionEnd();
		end_line = t.getLineOfOffset(sel_end);
		String[] linee = new String[end_line-start_line+1];
		start = t.getLineStartOffset(start_line);
		end = t.getLineEndOffset(end_line);
		return t.getText(start,end-start);
		} catch (Exception ex) {return null;}
	}
	
	private String[] getInitialWhitespace() throws BadLocationException {
		int line = editor.getLineOfOffset(editor.getSelectionStart());
		int linestart = editor.getLineStartOffset(line);
		int lineend = editor.getLineEndOffset(line);
		Matcher m = Pattern.compile("^[\t| ]*").matcher(editor.getText(
				linestart,lineend-linestart));
		String tabs = "";
		String lastChar = "";
		if (lineend==editor.getSelectionEnd()) lastChar = "\n";
		if (m.lookingAt()) tabs = m.group();
		String[] result = {tabs,lastChar};
		return result;
	}
	
	private void createActionMap(JTextComponent jt) {
		actions = new HashMap();  
		Action[] azioni = jt.getActions();
		for (int i = 0; i < azioni.length; i++) {
			Action a = azioni[i];
			actions.put(a.getValue(Action.NAME),a);
		}
	}
	
	private JMenuItemAA createMenu(String testo,int Mnemonic,String AccKey,
			String nomeAzione) {
		JMenuItemAALocalized m = new JMenuItemAALocalized(testo,Mnemonic);
		m.setAccelerator(KeyStroke.getKeyStroke(AccKey));
		m.setActionCommand(nomeAzione);
		m.addActionListener(this);
		return m;
	}

	private JMenuItemAA createMenu(String testo,int Mnemonic,KeyStroke AccKey,
			String nomeAzione) {
		JMenuItemAALocalized m = new JMenuItemAALocalized(testo,Mnemonic);
		m.setAccelerator(AccKey);
		m.setActionCommand(nomeAzione);
		m.addActionListener(this);
		return m;
	}

	private JMenuItemAA createMenu(String testo,int Mnemonic,String AccKey, 
			String nomeAzione,boolean enabled) {
		JMenuItemAALocalized m = new JMenuItemAALocalized(testo,Mnemonic);
		m.setAccelerator(KeyStroke.getKeyStroke(AccKey));
		m.setActionCommand(nomeAzione);
		m.addActionListener(this);
		m.setEnabled(enabled);
		return m;
	}
	
	private JMenuAA creaMenuLang() {
		JMenuAA lang = new JMenuAALocalized("Menu.Languages");
		//Retrieve all languages
		ButtonGroup group = new ButtonGroup();
		Locale l = Messages.getLocale();
		for (int i = 0; i < Application.Languages.length; i++) {
			boolean selected = false;
			Object[] language = (Object[])Application.Languages[i]; 
			if (language[1].equals(l)) selected=true; 
			JCheckBoxMenuItem mi = new JCheckBoxMenuItem((String)language[0],selected);
			mi.setActionCommand("setLocale:" + language[1]); //$NON-NLS-1$

			mi.addActionListener(this);
			group.add(mi);
			lang.add(mi);
		}
		return lang;
	}
	
	public void undoableEditHappened(UndoableEditEvent e) {
		undo.addEdit(e.getEdit());
		updateUndoState();
	}
	
	private void updateUndoState() {
		boolean flag=false;
		if (undo.canUndo()) {flag=true;}
		menu.getMenu(1).getMenuComponent(0).setEnabled(flag);		
	}
	
	public void Cerca(String txt_find) {
		txt_find = txt_find.toLowerCase();
		String txt = editor.getText().toLowerCase();
		int pos = txt.indexOf(txt_find,editor.getCaretPosition());
		if (pos==-1) {
			pos= txt.indexOf(txt_find);
		} 
		editor.grabFocus();
		if (pos!=-1) {
			editor.select(pos,pos+txt_find.length());
			menu.getMenu(1).getMenuComponent(14).setEnabled(true);
			lastSearch = txt_find;
		} else {
			menu.getMenu(1).getMenuComponent(14).setEnabled(false);
		}
	}
	
	public void findReplace(String[] param) {
		editor.setText(editor.getText().replaceAll(param[0],param[1]));
	}

	private void refreshMenus() {
		boolean flag;
		JMenuBar bar = getJMenuBar();
		JMenu m;
		//Update the menus on selection of text
		flag = editor.getSelectionStart()!=editor.getSelectionEnd();
		m = bar.getMenu(1);
		for(int i=17;i<22;i++) {
			m.getItem(i).setEnabled(flag);
		}
		//Update the menus on presence of text
		flag = !editor.getText().equals("");
		m = bar.getMenu(0);
		m.getItem(4).setEnabled(flag);
		m.getItem(5).setEnabled(flag);
		btnSave.setEnabled(flag);
		
	}

	public void caretUpdate(CaretEvent e) {
		refreshMenus();
	}
	
	private void changedLocale() {
		//Change Locale for menubar 
		for (int i = 0; i < menu.getMenuCount(); i++) {
			JMenuAALocalized jm = (JMenuAALocalized)menu.getMenu(i); 
			jm.changedLocale();
			for (int j = 0; j < jm.getMenuComponentCount(); j++) {
				Component c = jm.getMenuComponent(j);
				if (c instanceof JMenuItemAALocalized) ((JMenuItemAALocalized)c).changedLocale();  
			}
		}
		//Change Locale for the several popup
		for (int i = 0; i < popupLoops.getComponentCount(); i++) {
			Component c = popupLoops.getComponent(i);
			if (c instanceof JMenuItemAALocalized) ((JMenuItemAALocalized)c).changedLocale();
		}
		for (int i = 0; i < popupHeader.getComponentCount(); i++) {
			Component c = popupHeader.getComponent(i);
			if (c instanceof JMenuItemAALocalized) ((JMenuItemAALocalized)c).changedLocale();
		}
		for (int i = 0; i < popupEntity.getComponentCount(); i++) {
			Component c = popupEntity.getComponent(i);
			if (c instanceof JMenuItemAALocalized) ((JMenuItemAALocalized)c).changedLocale();
		}
		//Change the Locale for the toolbar buttons
		for (int i = 0; i < jt.getComponentCount(); i++) {
			Component c = jt.getComponent(i);
			if (c instanceof JButtonAALocalized) ((JButtonAALocalized)c).changedLocale();
		}
		//Change the Locale for the editor buttons
		for (int i = 0; i < bp.getComponentCount(); i++) {
			Component c = bp.getComponent(i);
			if (c instanceof JButtonAALocalized) ((JButtonAALocalized)c).changedLocale();
		}
		btnAccent.changedLocale();
		bold.changedLocale();
		((DefaultTreeModel)jtree.getModel()).setRoot(null);
		loadData();
		populateTree(jtree);
		Locale.setDefault(Messages.getLocale());
		JComponent.setDefaultLocale(Messages.getLocale());
		JColorChooser.setDefaultLocale(Messages.getLocale());
		//update the ui for language switch
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {}
	}
	
	String createColorString(Color c) {
		String s = Integer.toHexString(c.getRGB() & 0xffffff ).toUpperCase();
		if (s.length()<6) s = '0' + s;
		return '#' + s;
	}
}
