/*
 * Created on 1-mag-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL
 * 
 */
package com.spip.modelli;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

/**
 * @date 1-mag-2005
 * @author Renato Formato
 *
 */
public abstract class WLoopDialog extends JDialog implements ActionListener {

	private JToolBar jt;
	
	private String strGeneral = Messages.getString("WLoopDialog.General");
	private String strSpecific = Messages.getString("WLoopDialog.Specific");
	private String strSpecificCrit = Messages.getString("WLoopDialog.SpecificCrit");
	private String strCommon = Messages.getString("WLoopDialog.Common");
	private String strCommonCrit = Messages.getString("WLoopDialog.CommonCrit");
	private String strLogos = Messages.getString("WLoopDialog.Logos");
	private String strTags = Messages.getString("WLoopDialog.Tags");
	private String strResults = Messages.getString("WLoopDialog.Results");
	private String strPreview = Messages.getString("WLoopDialog.Preview");
	protected JToggleButton btnGeneral,btnSpecific,btnCommon;
	protected JToggleButton btnLogos,btnTags,btnResults;
	private JButtonAA btnHelp,btnCancel,btnOk;
	
	protected JComponent panelGeneral,panelSpecific,panelCommon;
	protected JComponent panelLogos,panelTags,panelResults;
	private JComponent panelContainer,currentPanel;
	private JLabel label;
	private Dimension mindimension;
	
	/**
	 * @param owner
	 * @throws java.awt.HeadlessException
	 */
	protected WLoopDialog(Frame owner,String title) throws HeadlessException {
		super(owner, title, true);
		setPanels();
		addGUI();
	}

	private void addGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		jt = new JToolBar();
		jt.setBorder(BorderFactory.createEmptyBorder(10,10,0,10));
		jt.setFloatable(false);
		//creates button
		btnGeneral = createToolbarButton(strGeneral,"pan_General");
		btnSpecific = createToolbarButton(strSpecific,"pan_Specific"); 
		btnCommon = createToolbarButton(strCommon,"pan_Common");
		btnLogos = createToolbarButton(strLogos,"pan_Logos");
		btnTags = createToolbarButton(strTags,"pan_Tags");
		btnResults = createToolbarButton(strResults,"pan_Results");
		//adds them to a group as to select one at a time
		ButtonGroup btngrp= new ButtonGroup();
		btngrp.add(btnGeneral);		
		btngrp.add(btnSpecific);
		btngrp.add(btnCommon);
		btngrp.add(btnLogos);
		btngrp.add(btnTags);
		btngrp.add(btnResults);
		//adds buttons to toolbar
		jt.add(btnGeneral);
		jt.add(btnSpecific);
		jt.add(btnCommon);
		jt.add(btnLogos);
		jt.add(btnTags);
		jt.add(Box.createHorizontalGlue());
		jt.add(btnResults);
		//adds toolbar to dialog
		cp.add(jt,BorderLayout.PAGE_START);
		//creates container panel
		panelContainer = new JPanel(new BorderLayout());
		panelContainer.setBorder(BorderFactory.createEmptyBorder(10 ,10,5,10));
		panelContainer.add(createCommonButtons(),BorderLayout.PAGE_END);
		panelContainer.add(createLabels(),BorderLayout.PAGE_START);
		mindimension = getMaximumPanelSize();
		currentPanel = panelGeneral;
		currentPanel.setPreferredSize(mindimension);
		panelContainer.add(currentPanel,BorderLayout.CENTER);
		//adds container panel to dialog
		cp.add(panelContainer,BorderLayout.CENTER);
		btnGeneral.setSelected(true);
		pack();
		setLocationRelativeTo(getParent());
	}

	private JToggleButton createToolbarButton(String text,String name) {
		JToggleButton tb = new JToggleButton(text);
		tb.addActionListener(this);
		tb.setActionCommand(name);
		return tb;
	}
	
	private JButtonAA createButton(String text,String name) {
		JButtonAA tb = new JButtonAA(text);
		tb.addActionListener(this);
		tb.setActionCommand(name);
		return tb;
	}
	
	private JPanel createCommonButtons() {
		JPanel jp = new JPanel();
		jp.setLayout(new BoxLayout(jp,BoxLayout.LINE_AXIS));
		btnHelp = createButton(Messages.getString("Dialogs.Help"),"Help");
		btnCancel = createButton(Messages.getString("Dialogs.Cancel"),"Cancel");
		btnOk = createButton(Messages.getString("Dialogs.Enter"),"Enter");
		jp.add(btnHelp);
		jp.add(Box.createHorizontalGlue());
		jp.add(btnCancel);
		jp.add(Box.createHorizontalStrut(5));
		jp.add(btnOk);
		//Add some space to the end to support Mac Os platform
		if (System.getProperty("os.name").startsWith("Mac OS X")) jp.setBorder(BorderFactory.createEmptyBorder(0,0,10,0));
		
		return jp;
	}
	
	private JPanel createLabels() {
		JPanel jp = new JPanel(new BorderLayout());
		label = new JLabel(strGeneral + ":"); //init to the first panel
		label.setFont(label.getFont().deriveFont(Font.BOLD));
		jp.add(label,BorderLayout.PAGE_START);
		jp.add(new JSeparator(JSeparator.HORIZONTAL),BorderLayout.PAGE_END);
		return jp;
	}
	
	private Dimension getMaximumPanelSize() {
		Dimension d = new Dimension();
		if (panelGeneral!=null) {
			panelGeneral.validate();
			d = ExpandD1toD2(d,panelGeneral.getPreferredSize());
		}
		if (panelSpecific!=null) {
			d = ExpandD1toD2(d,panelSpecific.getPreferredSize());
		}
		if (panelCommon!=null) {
			panelCommon.validate();
			d = ExpandD1toD2(d,panelCommon.getPreferredSize());
		}
		if (panelLogos!=null) {
			d = ExpandD1toD2(d,panelLogos.getPreferredSize());
		}
		if (panelTags!=null) {
			d = ExpandD1toD2(d,panelTags.getPreferredSize());
		}
		if (panelResults!=null) {
			d = ExpandD1toD2(d,panelResults.getPreferredSize());
		}
		//d.height+=20;
		//d.width+=20;
		return d;
	}
	
	private Dimension ExpandD1toD2(Dimension d1,Dimension d2) {
		Dimension result = new Dimension();
		result.height = (d1.height>d2.height)?d1.height:d2.height; 
		result.width = (d1.width>d2.width)?d1.width:d2.width;
		return result;
	}
	
	public void actionPerformed(ActionEvent e) {
		if (currentPanel!=null) panelContainer.remove(currentPanel);
		String command = e.getActionCommand();
		if (command.startsWith("pan")) {
			JComponent my_panel=null;
			if (command=="pan_General") {
				label.setText(strGeneral + ":");
				my_panel=panelGeneral;
			}
			else if (command=="pan_Specific") {
				label.setText(strSpecificCrit);
				my_panel=panelSpecific;
			}
			else if (command=="pan_Common") {
				label.setText(strCommonCrit);
				my_panel=panelCommon;
			}
			else if (command=="pan_Logos") {
				label.setText(strLogos + ":");
				my_panel=panelLogos;
			}
			else if (command=="pan_Tags") {
				label.setText(strTags + ":");
				my_panel=panelTags;
			}
			else if (command=="pan_Results") {
				label.setText(strPreview + ":");
				my_panel=panelResults;
			}
			if (my_panel!=null) {
				my_panel.setPreferredSize(mindimension);
				panelContainer.add(my_panel,BorderLayout.CENTER);
				currentPanel = my_panel;
			}
			panelSelected(command);
			validate();
			repaint();
		}
		else if (command=="Help") {
			helpSelected();
		}
		else if (command=="Cancel") {
			cancelSelected();
		}
		else if (command=="Enter") {
			enterSelected();
		}
	}
	
	protected abstract void setPanels();
	
	protected abstract void panelSelected(String name);

	protected abstract void helpSelected();
	
	protected abstract void cancelSelected();
	
	protected abstract void enterSelected();
}
