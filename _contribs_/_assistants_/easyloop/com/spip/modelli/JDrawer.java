/*
 * Copyright 2002 by Mark A. Kobold
 * 
 * The contents of this file are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the License for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is iSQL-Viewer, A Mutli-Platform Database Tool.
 * 
 * The Initial Developer of the Original Code is Markus A. Kobold.
 * 
 * Portions created by Mark A. Kobold are Copyright (C) Copyright (C) 2000 Mark A. Kobold. All Rights Reserved.
 * Contributor(s): Mark A. Kobold <mkobold@sprintpcs.com>.
 * 
 * Contributor(s): all the names of the contributors are added in the source code where applicable.
 * 
 * If you didn't download this code from the following link, you should check if you aren't using an obsolete version:
 * http://isql.sourceforge.net/
 */
package com.spip.modelli;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JWindow;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.MouseInputAdapter;

/**
 * A Simple pure Java version of the Drawer component usually only found in Mac
 * OS X.
 * <p>
 * Please note that this class does lack some functionality simply because some
 * features cannot be expressed in the Java language. However all things
 * considered this is a pretty decent working component. Please bare in mind
 * that this is more or less of a hack and doesn't compare fully to the native
 * drawer found in mac osx world this implementation is roughly 90% accurate.
 * <br>
 * Here is a run down of the methods of interest.
 * <p>
 * This class is designed to be as simple as possible. Like most Top-Level
 * components you will want to call getContentPane() to configure your component
 * that you wish to have show in this drawer.
 * <p>
 * This class has one method of intrest which is the toggle() method to toggle
 * the drawer in and out please note that the animation can be disabled outside
 * the class. when the drawer is toggled ChangeEvents will be fired.
 * <p>
 * Using swing constants NORTH, SOUTH, EAST, WEST, LEFT, RIGHT with the
 * setPreferredLocation will determine which side the drawer is attached to. if
 * need be in certain situation the drawer may toggle to the opposite side if
 * there is no room on the preferred side.
 * <p>
 * Here is a sample snippet of user code to use this component.
 * 
 * <pre>
 * 
 *  
 *   
 *    
 *                            public class DrawerFrame extends JFrame {
 *                                 JDrawer drawer = new JDrawer(this);
 *                                 public DrawerFrame();
 *                                      super(&quot;Drawer Test);
 *                                      JDrawer drawer = new JDrawer(this);
 *                                      drawer.setPreferredLocation(JDrawer.WEST);
 *                                      JButton btn = new JButton(&quot;Toggle Drawer&quot;);
 *                                      btn.addActionListener(new ActionListener(){ 
 *                                            public void actionPerformed(ActionEvent ae){
 *                                               drawer.toggle();
 *                                            }
 *                                      getContentPane().add(btn, BorderLayout.NORTH);
 *                                      pack();
 *                                      show();
 *                                }
 *                           }
 *     
 *    
 *   
 *  
 * </pre>
 * 
 * <p>
 * I would also like to take this space to let you know that this class is
 * mostly an illusion since the java language doesn't lend itself to truly
 * create this component and have it behave correctly, so please bear in mind
 * that this component is not fool proof and you can cause erratic behavior by
 * calling certain methods pragmatically the drawer should adjust itself but i
 * obivously haven't tested every concievable situation yet. Also if you like
 * this component and choose to use i would be intrested to hear about it.
 * @see #getContentPane()
 * @see #toggle()
 * @see #setPreferredLocation(int)
 * @author Markus A. Kobold &lt;mkobold at sprintpcs dot com&gt;
 */
public class JDrawer extends JWindow implements SwingConstants {

    /**
     * List of ChangeListeners to be notified when the drawer is toggled.
     */
    protected ArrayList       changeListeners = null;
    /**
     * The current window this drawer is attached to.
     */
    protected Window          parentWindow    = null;
    /**
     * This class helps ensure the illusion that the frame is attached to the
     * parentWindow.
     */
    protected FrameListener   eventHandler    = new FrameListener();
    /**
     * This is the runnable object for preforming the animation safely outside
     * the event queue.
     */
    protected ToggleAnimation animator        = new ToggleAnimation();
    /**
     * Flag to enable or disable the animation of toggling the drawer in and out
     */
    protected boolean         quickToggle     = false;
    /**
     * The true contentpane for this window, this is *NOT* the actually content
     * pane used outside this class
     */
    protected JPanel          contentPane     = new JPanel(new BorderLayout());
    /**
     * The preferred drawer location NORTH,SOUTH,EAST,WEST,LEFT,RIGHT are
     * acceptable constants.
     */
    protected int             preferredLoc    = EAST;
    /**
     * A simple JComponent to handle border the user content on the EAST side of
     * this Drawer
     */
    protected DrawerHandle    handleEast      = new DrawerHandle(6, true);
    /**
     * A simple JComponent to handle border the user content on the WEST side of
     * this Drawer
     */
    protected DrawerHandle    handleWest      = new DrawerHandle(6, true);
    /**
     * A simple JComponent to handle border the user content on the NORTH side
     * of this Drawer
     */
    protected DrawerHandle    handleNorth     = new DrawerHandle(6, false);
    /**
     * A simple JComponent to handle border the user content on the SOUTH side
     * of this Drawer
     */
    protected DrawerHandle    handleSouth     = new DrawerHandle(6, false);
    /**
     * This is the actuall container returned outside this class as the
     * contentpane for users.
     */
    protected JComponent      userContent     = new JPanel(new BorderLayout());
    /**
     * Simple border for the userContent panel for creating a lowered effect.
     */
    protected Border          contentBdr      = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
    /**
     * A MouseInputAdapter for handling the Dragging of the drawers active side
     * to expand or collapse the drawer.
     */
    protected DragHandler     dndHandler      = new DragHandler();
    /**
     * This is mainly used to make sure if the user has expanded the drawer to
     * given size it will be used instead of the default.
     */
    protected Dimension       userSize        = null;
    private ChangeEvent       toggleEvent     = new ChangeEvent(this);

    /**
     * This is basic constructor.
     * <p>
     * This is equivelant to new JDrawer(null, JDrawer.EAST, false); This drawer
     * will attached to the designated shared window instance.
     * @see #JDrawer(Window, int, boolean)
     */
    public JDrawer() {

        this(null, EAST, false);
    }

    /**
     * This is basic constructor.
     * <p>
     * This is equivelant to new JDrawer(owner, JDrawer.EAST, false);
     * @param owner Window component to attach this drawer to.
     * @see #JDrawer(Window, int, boolean)
     * @since 1.0
     */
    public JDrawer(Window owner) {

        this(owner, EAST, false);
    }

    /**
     * Another basic constructor.
     * <p>
     * This is equivelant to to new JDrawer(owner, location, false).
     * @param owner Window component to attach this drawer to.
     * @param location The Preferred location of this drawer.
     * @see #JDrawer(Window, int, boolean)
     * @throws IllegalArgumentException if the preferred location is invalid.
     * @since 1.0
     */
    public JDrawer(Window owner, int location) {

        this(owner, location, false);
    }

    /**
     * Default Constructor for this class.
     * <p>
     * If the given owner window is null the default shared window will be used
     * as provided by the super's constructor.
     * <p>
     * @param owner Window component to attach this drawer to.
     * @param location The Preferred location of this drawer.
     * @param isVisible Sets the drawer to be visible by default.
     * @throws IllegalArgumentException if the preferred location is invalid.
     * @since 1.0
     */
    public JDrawer(Window owner, int location, boolean isVisible) {

        super(owner);
        contentPane.add(handleWest, BorderLayout.WEST);
        contentPane.add(handleEast, BorderLayout.EAST);
        contentPane.add(handleSouth, BorderLayout.SOUTH);
        contentPane.add(handleNorth, BorderLayout.NORTH);
        contentPane.add(userContent, BorderLayout.CENTER);
        Border bdr = BorderFactory.createLineBorder(getForeground(), 1);

        contentPane.setBorder(bdr);
        userContent.setBorder(contentBdr);
        setVisible(isVisible);
        super.setContentPane(contentPane);
        setParentWindow(owner);
        setPreferredLocation(location);
        pack();
    }

    /**
     * This method toggles this drawer in and out.
     * <p>
     * if this drawer is not already toggling this drawer this method will
     * return immeadiately. if quick toggling is enabled this method will simply
     * setVisible(!isVisible()) other wise the animation code will be posted to
     * the swing utilities invokeLater(Runnable).
     * <p>
     * It should also be noted that if this drawer is toggled and can not fit on
     * the preferred side and *CAN* fit on the opposite side it will switch
     * sides automagically.
     * @see #setQuickToggleEnabled(boolean)
     * @see SwingUtilities#invokeLater(java.lang.Runnable)
     * @since 1.0
     */
    public synchronized void toggle() {

        if (!animator.isToggling()) {
            if (quickToggle) {
                setVisible(!isVisible());
                fireStateChanged(toggleEvent);
            } else {
                SwingUtilities.invokeLater(animator);
            }
        }
    }

    /**
     * Adds change listener object to listen for when the drawer is toggled.
     * <p>
     * @param cl instanceof ChangeLister to be notified on drawer toggles.
     */
    public void addChangeListener(ChangeListener cl) {

        if (cl == null)
            return;
        if (changeListeners == null)
            changeListeners = new ArrayList();
        if (!changeListeners.contains(cl)) {
            synchronized (changeListeners) {
                changeListeners.add(cl);
            }
        }
    }

    /**
     * Removes the given ChangeListener from the listener list.
     * <p>
     * if the given ChangeListener is null or there are no changelisteners at
     * all this method will return imediately.
     * @param cl ChangeListener to remove from listener list.
     */
    public void removeChangeListener(ChangeListener cl) {

        if (cl == null || changeListeners == null)
            return;
        if (changeListeners.contains(cl)) {
            synchronized (changeListeners) {
                changeListeners.remove(cl);
            }
        }
    }

    /**
     * This method is a synonym to the isVisible() method.
     * <p>
     * if true the drawer is closed and not visible, otherwise the drawer is
     * open and visible to the user.
     * @see #toggle()
     * @return boolean determining the toggled state of this drawer
     * @since 1.0
     */
    public boolean isToggled() {

        return isVisible();
    }

    /**
     * Retrieves the parent window that this drawer is attached to.
     * <p>
     * returns and instance of window that this JDrawer was created with or the
     * window refrence used with the last setParentWindow(Window) call
     * @see #setParentWindow(Window)
     * @return Window refrence that this Drawer is attached to.
     */
    public Window getParentWindow() {

        return parentWindow;
    }

    /**
     * Sets the preferred *side* of the parrent window the drawer is displayed
     * on.
     * <p>
     * This will set the preferred location in which to show the drawer, this
     * value should be one of the following SwingConstants
     * <ul>
     * <li>NORTH
     * <li>SOUTH
     * <li>EAST, RIGHT
     * <li>WEST, LEFT
     * </ul>
     * <p>
     * It should also be noted that if this drawer is toggled and can not fit on
     * the preferred side and *CAN* fit on the opposite side it will switch
     * sides automagically.
     * @see #getPreferredLocation()
     * @throws IllegalArgumentException if the location is an invalid location.
     * @param location to have the drawer shown
     */
    public void setPreferredLocation(int location) {

        clearHandleListeners(preferredLoc, location);
        preferredLoc = location;
        updateBounds();
    }

    /**
     * Gets the preferred *side* of the parrent window the drawer is displayed
     * on.
     * <p>
     * This will get the preferred location in which to show the drawer, this
     * value will be one of the following SwingConstants
     * <ul>
     * <li>NORTH
     * <li>SOUTH
     * <li>EAST, RIGHT
     * <li>WEST, LEFT
     * </ul>
     * @see #setPreferredLocation(int)
     * @return Integer constant representing the side this drawer is attached
     *         to.
     */
    public int getPreferredLocation() {

        return preferredLoc;
    }

    /**
     * Set the window Object this drawer is attached to.
     * <p>
     * If this drawer is already attached the component listener will be removed
     * and then added to the given window. This method is fairly fast and if the
     * drawer should be needed to jump from window to window it can be done
     * fairly reasonably.
     * @param parentWindow The new Window to attach this Drawer to.
     */
    public void setParentWindow(Window parentWindow) {

        if (parentWindow == null) {
            throw new NullPointerException();
        }
        if (this.parentWindow != null) {
            this.parentWindow.removeComponentListener(eventHandler);
            this.parentWindow.removeWindowListener(eventHandler);
        }
        this.parentWindow = parentWindow;
        this.parentWindow.addComponentListener(eventHandler);
        this.parentWindow.addWindowListener(eventHandler);
        updateBounds();
    }

    /**
     * Enables or disables the toggle animation of this drawer.
     * <p>
     * If quick toggle is enabled this will simple just set this component to
     * invisible thus a fairly instant toggle.
     * <p>
     * This will only take effect on the next and preceding toggle calls.
     * @param f to determine is the quick toggle is enabled or not.
     */
    public void setQuickToggleEnabled(boolean f) {

        quickToggle = f;
    }

    /**
     * This returns the content pane to show user components.
     * <p>
     * Other components can be added this container to be shown in this drawer.
     * @see javax.swing.RootPaneContainer#getContentPane()
     */
    public Container getContentPane() {

        return userContent;
    }

    public boolean getFocusableWindowState() {

        return false;
    }

    /**
     * This will set content pane of user components.
     * <p>
     * A container that holds the component to show in this drawer. it is
     * recommend to stick with JPanels as it tries to type cast to a Jpanel to
     * set a border to create a lowered effect.
     * <p>
     * This method is mainly overriden to help ensure the behaviour of this
     * component however it is not fool proof by any means.
     * @see javax.swing.RootPaneContainer#setContentPane(java.awt.Container)
     */
    public void setContentPane(Container contentPane) {

        contentPane.remove(userContent);
        userContent.setBorder(null);
        if (contentPane != null) {
            contentPane.add(contentPane, BorderLayout.CENTER);
            userContent = (JComponent) contentPane;
            userContent.setBorder(contentBdr);
        }
    }

    /**
     * This method ensures the right handle setup for the drawer based a changed
     * location.
     * <p>
     * based on the old location the mouse listeners will be removed and also
     * the opposite handle will be readded since the if the preferred location
     * is EAST there will be not handle on the WEST side of the drawer.
     * <p>
     * Based on the new location the mouse listener will be added to the correct
     * side to enable user dragging of the drawer side.
     * <p>
     * Make sure you are using the proper constants with this method call as no
     * check at this time is made to ensure that the locations are correct, so
     * incorrect location may cause unusual behaviour.
     * @param old preferred location
     * @param loc is the new preferred location where the drawer will be shown
     *            on.
     */
    protected void clearHandleListeners(int old, int loc) {

        switch (old) {
            case NORTH :
                handleNorth.removeMouseListener(dndHandler);
                handleNorth.removeMouseMotionListener(dndHandler);
                contentPane.add(handleSouth, BorderLayout.SOUTH);
                break;
            case SOUTH :
                handleSouth.removeMouseListener(dndHandler);
                handleSouth.removeMouseMotionListener(dndHandler);
                contentPane.add(handleNorth, BorderLayout.NORTH);
                break;
            case WEST :
            case LEFT :
                handleWest.removeMouseListener(dndHandler);
                handleWest.removeMouseMotionListener(dndHandler);
                contentPane.add(handleEast, BorderLayout.EAST);
                break;
            case EAST :
            case RIGHT :
                handleEast.removeMouseListener(dndHandler);
                handleEast.removeMouseMotionListener(dndHandler);
                contentPane.add(handleWest, BorderLayout.WEST);
                break;
        }
        switch (loc) {
            case NORTH :
                handleNorth.addMouseListener(dndHandler);
                handleNorth.addMouseMotionListener(dndHandler);
                contentPane.remove(handleSouth);
                break;
            case SOUTH :
                handleSouth.addMouseListener(dndHandler);
                handleSouth.addMouseMotionListener(dndHandler);
                contentPane.remove(handleNorth);
                break;
            case LEFT :
            case WEST :
                handleWest.addMouseListener(dndHandler);
                handleWest.addMouseMotionListener(dndHandler);
                contentPane.remove(handleEast);
                break;
            case RIGHT :
            case EAST :
                handleEast.addMouseListener(dndHandler);
                handleEast.addMouseMotionListener(dndHandler);
                contentPane.remove(handleWest);
                break;
        }
    }

    /**
     * Notifies all registered change listeners of a ChangeEvent.
     * <p>
     * if there have been no change listeners added yet this method will return
     * immediately, otherwise this method will iterate through the listener list
     * and notify them.
     * @param e ChangeEvent to forward.
     */
    protected void fireStateChanged(ChangeEvent e) {

        if (changeListeners == null)
            return;
        synchronized (changeListeners) {
            Iterator itr = changeListeners.iterator();
            while (itr.hasNext()) {
                ChangeListener l = (ChangeListener) itr.next();
                try {
                    l.stateChanged(e);
                } catch (Throwable t) {
                }
            }
        }
    }

    /**
     * A method to update the bounds of this drawer.
     * <p>
     * This method is probably called the most this helps ensure the illusion
     * that the drawer is attached to the given parent window. Basically the
     * logic of how the drawer is sized up is handled here.
     */
    protected void updateBounds() {

        if (!parentWindow.isShowing()) {
            return;
        }
        int hieght = 0;
        int width = 0;
        int gutter = 0;
        Rectangle r = parentWindow.getBounds();
        switch (preferredLoc) {
            case NORTH :
                hieght = (isVisible() ? (userSize == null ? getPreferredSize().height : userSize.height) : 0);
                width = r.width;
                gutter = (width / 20);
                this.setBounds(r.x + gutter, r.y - hieght, r.width - (2 * gutter), hieght);
                break;
            case SOUTH :
                hieght = (isVisible() ? (userSize == null ? getPreferredSize().height : userSize.height) : 0);
                width = r.width;
                gutter = (width / 20);
                this.setBounds(r.x + gutter, r.y + r.height, r.width - (2 * gutter), hieght);
                break;
            case LEFT :
            case WEST :
                hieght = r.height;
                width = (isVisible() ? (userSize == null ? getPreferredSize().width : userSize.width) : 0);
                gutter = (hieght / 20);
                this.setBounds(r.x - width, r.y + gutter, width, hieght - (2 * gutter));
                break;
            case RIGHT :
            case EAST :
                hieght = r.height;
                width = (isVisible() ? (userSize == null ? getPreferredSize().width : userSize.width) : 0);
                gutter = (hieght / 20);
                this.setBounds(r.x + r.width, r.y + gutter, width, hieght - (2 * gutter));
                break;
            default :
                break;
        }
        if (isVisible()) {
            validateTree();
        }
    }

    /**
     * Method to help determine if there is enough room to expand the drawer at
     * a given location.
     * <p>
     * This will call the respective private method of canExpandXX() where XX is
     * North,South, etc..
     * @param location a preferred location value to test expandability.
     * @return boolean if the drawer can be expanded at the given location.
     */
    protected boolean canExpand(int location) {

        switch (location) {
            case NORTH :
                return canExpandNorth();
            case SOUTH :
                return canExpandSouth();
            case LEFT :
            case WEST :
                return canExpandWest();
            case RIGHT :
            case EAST :
                return canExpandWest();
            default :
                return false;
        }
    }

    /**
     * Returns an instance of the Component that is currently listening for
     * mouse events.
     * <p>
     * based on the preferred location the respectiv instance will be returned.
     * @return Component that is currently the *active* handle.
     */
    protected Component getActiveHandle() {

        switch (preferredLoc) {
            case NORTH :
                return handleNorth;
            case SOUTH :
                return handleSouth;
            case LEFT :
            case WEST :
                return handleWest;
            case RIGHT :
            case EAST :
                return handleEast;
            default :
                return null;
        }
    }

    private boolean canExpandEast() {

        Rectangle r = parentWindow.getBounds();
        int farthestX = r.x + r.width + (userSize != null ? userSize.width : contentPane.getPreferredSize().width);
        Toolkit tk = Toolkit.getDefaultToolkit();
        int maxWidth = tk.getScreenSize().width;
        return (farthestX < maxWidth);
    }

    private boolean canExpandSouth() {

        Rectangle r = parentWindow.getBounds();
        int farthestY = r.y + r.height + (userSize != null ? userSize.height : contentPane.getPreferredSize().height);
        Toolkit tk = Toolkit.getDefaultToolkit();
        int maxHeight = tk.getScreenSize().height;
        return (farthestY < maxHeight);
    }

    private boolean canExpandNorth() {

        Rectangle r = parentWindow.getBounds();
        int farthestY = r.y - (userSize != null ? userSize.height : contentPane.getPreferredSize().height);
        return (farthestY >= 0);
    }

    private boolean canExpandWest() {

        Rectangle r = parentWindow.getBounds();
        int farthestX = r.x - (userSize != null ? userSize.width : contentPane.getPreferredSize().width);
        return (farthestX >= 0);
    }

    /**
     * This class is created to listen as the parent window moves or changes
     * size to make sure the drawer is in its proper place. In the MVC of things
     * the parentWindow can somewhat be viewed as the model for this compoent.
     * as it determines how it looks, it is a stretch i am sure but hey it works
     */
    private class FrameListener extends ComponentAdapter implements WindowListener {

        protected boolean isToggled = false;

        /*
         * (non-Javadoc)
         * @see java.awt.event.WindowListener#windowActivated(java.awt.event.WindowEvent)
         */
        public void windowActivated(WindowEvent e) {

            if (!JDrawer.this.isEnabled()) {
                setEnabled(true);
            }
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.WindowListener#windowClosed(java.awt.event.WindowEvent)
         */
        public void windowClosed(WindowEvent e) {

            Window window = e.getWindow();
            if (window != JDrawer.this) {
                dispatchEvent(new WindowEvent(JDrawer.this, e.getID()));
            }
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.WindowListener#windowClosing(java.awt.event.WindowEvent)
         */
        public void windowClosing(WindowEvent e) {

            Window window = e.getWindow();
            if (window != JDrawer.this) {
                dispatchEvent(new WindowEvent(JDrawer.this, e.getID()));
            }
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.WindowListener#windowDeactivated(java.awt.event.WindowEvent)
         */
        public void windowDeactivated(WindowEvent e) {

            Window other = e.getOppositeWindow();
            if (other instanceof JDialog && other != getParentWindow()) {
                JDialog dlg = (JDialog) other;
                if (dlg.isModal()) {
                    setEnabled(false);
                }
            }
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.WindowListener#windowDeiconified(java.awt.event.WindowEvent)
         */
        public void windowDeiconified(WindowEvent e) {

            if (isToggled) {
                toggle();
            }
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.WindowListener#windowIconified(java.awt.event.WindowEvent)
         */
        public void windowIconified(WindowEvent e) {

            isToggled = isToggled();
            if (isToggled) {
                setVisible(false);
            }
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.WindowListener#windowOpened(java.awt.event.WindowEvent)
         */
        public void windowOpened(WindowEvent e) {

            updateBounds();
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.ComponentListener#componentResized(java.awt.event.ComponentEvent)
         */
        public void componentResized(ComponentEvent e) {

            updateBounds();
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.ComponentListener#componentMoved(java.awt.event.ComponentEvent)
         */
        public void componentMoved(ComponentEvent e) {

            updateBounds();
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.ComponentListener#componentHidden(java.awt.event.ComponentEvent)
         */
        public void componentHidden(ComponentEvent e) {

            setVisible(false);
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.ComponentListener#componentShown(java.awt.event.ComponentEvent)
         */
        public void componentShown(ComponentEvent e) {

            updateBounds();
        }
    }

    /**
     * This class enables the user to interactivle expand and collapse the
     * drawer to thier liking and it will automatically toggle itself closed if
     * the user collapses the drawer to a size that repestively given the
     * location of the drawer is smaller than the preferred size of the content.
     */
    private class DragHandler extends MouseInputAdapter {

        private boolean   toggleOnRelease = false;
        private boolean   isDragging      = false;
        private Dimension originalSize    = null;

        /*
         * (non-Javadoc)
         * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
         */
        public void mouseEntered(MouseEvent e) {

            switch (preferredLoc) {
                case NORTH :
                case SOUTH :
                    setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
                    break;
                case RIGHT :
                case EAST :
                case LEFT :
                case WEST :
                    setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
                    break;
            }
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.MouseMotionListener#mouseMoved(java.awt.event.MouseEvent)
         */
        public void mouseExited(MouseEvent e) {

            if (!isDragging) {
                setCursor(Cursor.getDefaultCursor());
            } else {
                mouseEntered(e);
            }
        }

        /**
         * @see java.awt.event.MouseMotionListener#mouseDragged(java.awt.event.MouseEvent)
         */
        public void mouseDragged(MouseEvent e) {

            int ourX = e.getX();
            int ourY = e.getY();
            Rectangle old = getBounds();
            userContent.invalidate();
            validateTree();
            int minH = JDrawer.super.getContentPane().getMinimumSize().height;
            int minW = JDrawer.super.getContentPane().getMinimumSize().width;
            toggleOnRelease = false;
            isDragging = true;
            switch (preferredLoc) {
                case NORTH :
                    int yoff = Math.abs(ourY);
                    if (ourY == 0) {
                        return;
                    } else if (ourY < 0) {
                        setBounds(old.x, old.y - yoff, old.width, old.height + yoff);
                    } else {
                        setBounds(old.x, old.y + yoff, old.width, old.height - yoff);
                    }
                    if (getBounds().height > minH) {
                        if (userSize != null) {
                            userSize.setSize(old.width, getBounds().height);
                        } else {
                            userSize = new Dimension(old.width, getBounds().height);
                        }
                    } else if (getBounds().height < minH) {
                        toggleOnRelease = true;
                    }
                    break;
                case SOUTH :
                    if (ourY == 0) {
                        return;
                    } else if (ourY < 0) {
                        int height = old.height - Math.abs(ourY);
                        if (height < 0)
                            height = 0;
                        setBounds(old.x, old.y, old.width, height);
                    } else {
                        setBounds(old.x, old.y, old.width, old.height + ourY);
                    }
                    if (getBounds().height > minH) {
                        if (userSize != null) {
                            userSize.setSize(old.width, getBounds().height);
                        } else {
                            userSize = new Dimension(old.width, getBounds().height);
                        }
                    } else if (getBounds().height < minH) {
                        toggleOnRelease = true;
                    }
                    break;
                case RIGHT :
                case EAST :
                    if (ourX == 0 || toggleOnRelease) {
                        return;
                    } else if (ourX < 0) {
                        int width = old.width - Math.abs(ourX);
                        if (width < 0) {
                            width = 0;
                        }
                        setBounds(old.x, old.y, width, old.height);
                    } else {
                        setBounds(old.x, old.y, old.width + ourX, old.height);
                    }
                    if (getBounds().width > minW) {
                        if (userSize != null) {
                            userSize.setSize(getBounds().width, old.height);
                        } else {
                            userSize = new Dimension(getBounds().width, old.height);
                        }
                    } else if (getBounds().width < minW) {
                        toggleOnRelease = true;
                    }
                    break;
                case LEFT :
                case WEST :
                    int xoff = Math.abs(ourX);
                    if (ourX == 0 || toggleOnRelease) {
                        return;
                    } else if (ourX < 0) {
                        setBounds(old.x - xoff, old.y, old.width + xoff, old.height);
                    } else {
                        setBounds(old.x + xoff, old.y, old.width - xoff, old.height);
                    }
                    if (getBounds().width > minW) {
                        if (userSize != null) {
                            userSize.setSize(getBounds().width, old.height);
                        } else {
                            userSize = new Dimension(getBounds().width, old.height);
                        }
                    } else if (getBounds().width < minW) {
                        toggleOnRelease = true;
                    }
                    break;
            }
            getContentPane().invalidate();
            validateTree();
            e.consume();
        }

        /**
         * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
         */
        public void mouseReleased(MouseEvent e) {

            if (toggleOnRelease) {
                toggle();
                userSize = originalSize;
            }
            originalSize = null;
            isDragging = false;
        }

        /**
         * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
         */
        public void mousePressed(MouseEvent e) {

            originalSize = new Dimension(getBounds().width, getBounds().height);
        }
    }

    /**
     * This class is basically a ripper from the internal filler class of the
     * javax.swing.Box class. i had to do this because the component created by
     * Box.createxxxStrut(int) won't listen for mouse events. So this is safer
     * in the long run.
     */
    private static class DrawerHandle extends JComponent {

        private Dimension reqMin;
        private Dimension reqPref;
        private Dimension reqMax;

        /**
         * Constructor to create shape with the given size ranges.
         * @param min Minimum size
         * @param pref Preferred size
         * @param max Maximum size
         */
        public DrawerHandle(int width, boolean vertical) {

            reqMin = new Dimension((!vertical ? 0 : width), (!vertical ? width : 0));
            reqPref = new Dimension((!vertical ? 0 : width), (!vertical ? width : 0));
            reqMax = new Dimension((!vertical ? Integer.MAX_VALUE : width), (!vertical ? width : Integer.MAX_VALUE));
        }

        /**
         * Change the size requests for this shape. An invalidate() is
         * propagated upward as a result so that layout will eventually happen
         * with using the new sizes.
         * @param min Value to return for getMinimumSize
         * @param pref Value to return for getPreferredSize
         * @param max Value to return for getMaximumSize
         */
        public void changeShape(Dimension min, Dimension pref, Dimension max) {

            reqMin = min;
            reqPref = pref;
            reqMax = max;
            invalidate();
        }

        // ---- Component methods ------------------------------------------
        /**
         * Returns the minimum size of the component.
         * @return the size
         */
        public Dimension getMinimumSize() {

            return reqMin;
        }

        /**
         * Returns the preferred size of the component.
         * @return the size
         */
        public Dimension getPreferredSize() {

            return reqPref;
        }

        /**
         * Returns the maximum size of the component.
         * @return the size
         */
        public Dimension getMaximumSize() {

            return reqMax;
        }
    }

    /**
     * This class does the whole toggle animation bit that is pretty much it,
     * check the toggleXXX(boolean) methods if you need to disable to automagic
     * side switching.
     */
    private class ToggleAnimation implements Runnable {

        private boolean isToggling = false;

        public synchronized boolean isToggling() {

            synchronized (this) {
                return isToggling;
            }
        }

        private void toggleSouth(boolean isClosing) {

            if (!isClosing && !canExpandSouth() && canExpandNorth()) {
                setVisible(false);
                setPreferredLocation(NORTH);
                setVisible(true);
                toggleNorth(isClosing);
            }
            int height = getSize().height;
            int width = getSize().width;
            boolean flag = (isClosing ? height > 0 : height < getPreferredSize().height);
            long delta = getPreferredSize().height / 10;
            while (flag) {
                if (isClosing)
                    height -= delta;
                else
                    height += delta;
                flag = (isClosing ? height != 0 : height < getPreferredSize().height);
                if (height < 0) {
                    flag = false;
                    height = 0;
                }
                synchronized (JDrawer.this) {
                    setSize(width, height);
                }
                if (flag)
                    try {
                        Thread.sleep(2);
                    } catch (Throwable t) {
                        return;
                    }
            }
        }

        private void toggleNorth(boolean isClosing) {

            if (!isClosing && !canExpandNorth() && canExpandSouth()) {
                setVisible(false);
                setPreferredLocation(SOUTH);
                setVisible(true);
                toggleSouth(isClosing);
            }
            int height = getSize().height;
            int width = getSize().width;
            boolean flag = (isClosing ? height > 0 : height < getPreferredSize().height);
            long delta = getPreferredSize().height / 10;
            while (flag) {
                if (isClosing)
                    height -= delta;
                else
                    height += delta;
                flag = (isClosing ? height != 0 : height < getPreferredSize().height);
                if (height < 0) {
                    flag = false;
                    height = 0;
                }
                Rectangle r = getBounds();
                synchronized (JDrawer.this) {
                    setBounds(r.x, r.y + (3 * (isClosing ? 1 : -1)), width, height);
                }
                if (flag)
                    try {
                        Thread.sleep(2);
                    } catch (Throwable t) {
                        return;
                    }
            }
        }

        private void toggleWest(boolean isClosing) {

            if (!isClosing && !canExpandWest() && canExpandEast()) {
                setVisible(false);
                setPreferredLocation(EAST);
                setVisible(true);
                toggleEast(isClosing);
            }
            int height = getSize().height;
            int width = getSize().width;
            boolean flag = (isClosing ? width > 0 : width < getPreferredSize().width);
            long delta = getPreferredSize().width / 10;
            while (flag) {
                if (isClosing)
                    width -= delta;
                else
                    width += delta;
                flag = (isClosing ? width != 0 : width < getPreferredSize().width);
                if (width < 0) {
                    flag = false;
                    width = 0;
                }
                Rectangle r = getBounds();
                synchronized (JDrawer.this) {
                    setBounds(r.x + (3 * (isClosing ? 1 : -1)), r.y, width, height);
                }
                if (flag)
                    try {
                        Thread.sleep(2);
                    } catch (Throwable t) {
                        return;
                    }
            }
        }

        private void toggleEast(boolean isClosing) {

            if (!isClosing && !canExpandEast() && canExpandWest()) {
                setVisible(false);
                setPreferredLocation(WEST);
                setVisible(true);
                toggleWest(isClosing);
            }
            int height = getSize().height;
            int width = getSize().width;
            boolean flag = (isClosing ? width > 0 : width < getPreferredSize().width);
            long delta = getPreferredSize().height / 10;
            while (flag) {
                if (isClosing)
                    width -= delta;
                else
                    width += delta;
                flag = (isClosing ? width != 0 : width < getPreferredSize().width);
                if (width < 0) {
                    flag = false;
                    width = 0;
                }
                synchronized (JDrawer.this) {
                    setSize(width, height);
                }
                if (flag)
                    try {
                        Thread.sleep(2);
                    } catch (Throwable t) {
                        return;
                    }
            }
        }

        public void run() {

            isToggling = true;
            boolean isClosing = isVisible();
            setVisible(true);
            switch (preferredLoc) {
                case NORTH :
                    toggleNorth(isClosing);
                    break;
                case SOUTH :
                    toggleSouth(isClosing);
                    break;
                case LEFT :
                case WEST :
                    toggleWest(isClosing);
                    break;
                case RIGHT :
                case EAST :
                    toggleEast(isClosing);
                    break;
                default :
                    break;
            }
            if (isClosing)
                setVisible(false);
            updateBounds();
            fireStateChanged(toggleEvent);
            isToggling = false;
        }
    }
}