/*
 * Created on 7-mag-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL
 *
 */
package com.spip.modelli;

import java.awt.Container;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @date 7-mag-2005
 * @author Renato Formato
 *
 */
public class WURLDialog extends JDialog implements ActionListener {
	
	public static String[] showURLDialog(Frame owner) {
		WURLDialog ud = new WURLDialog(owner);
		ud.show();
		return null;
	}

	private JTextField address;
	
	/**
	 * @param owner
	 * @throws java.awt.HeadlessException
	 */
	protected WURLDialog(Frame owner) throws HeadlessException {
		super(owner, Messages.getString("WURLDialog.Title"), true);
		addGUI();
	}
	
	private void addGUI() {
		JPanel panel,panel2;
		JLabel label;
		JButtonAA button;
		Container cp = getContentPane();
		
		GridBagConstraints c = new GridBagConstraints(); 
		c.anchor = GridBagConstraints.LINE_END;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(2,2,2,2);
		panel = new JPanel(new GridBagLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(5,10,5,10));
		
		label = new JLabel(Messages.getString("WURLDialog.FileAddress"));
		panel.add(label,c);
		address = new JTextField(10);
		c.gridwidth = 4;
		panel.add(address,c);
		c.gridy = 1;
		c.gridx = 2;
		c.gridwidth = 2;
		button = new JButtonAALocalized("Dialogs.File");
		panel.add(button,c);
		c.gridwidth = 1;
		c.gridx = 4;
		button = new JButtonAA("<");
		panel.add(button,c);
		c.gridx = 5;
		button = new JButtonAA("http://");
		panel.add(button,c);
		c.gridy = 2;
		c.gridx = 2;
		button = new JButtonAA("<-2");
		panel.add(button,c);
		c.gridx = GridBagConstraints.RELATIVE;
		button = new JButtonAA("<-1");
		panel.add(button,c);
		button = new JButtonAA("<-0");
		panel.add(button,c);
		button = new JButtonAA("mailto:");
		panel.add(button,c);
		c.gridy = 3;
		c.gridx = 5;
		button = new JButtonAA("javascript:");
		panel.add(button,c);
		
		c.gridy = 4;
		c.gridx = 0;
		c.gridwidth = 5;
		panel2 = new JPanel();
		panel2.setLayout(new BoxLayout(panel2,BoxLayout.LINE_AXIS));
		button = new JButtonAALocalized("Dialogs.Cancel");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				hide();
			}
		});
		panel2.add(button);
		panel2.add(Box.createHorizontalGlue());
		button = new JButtonAALocalized("Dialogs.Enter");
		button.addActionListener(this);
		panel2.add(button);
		
		panel.add(panel2,c);
		
		cp.add(panel);
		pack();
		setLocationRelativeTo(getParent());
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		hide();
	}

}
