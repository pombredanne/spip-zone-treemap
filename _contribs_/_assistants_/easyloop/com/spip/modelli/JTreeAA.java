/*
 * Created on 8-apr-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL		
 * 
 */
package com.spip.modelli;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JTree;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;

/**
 * @author Renato Formato
 *
 * 
 * 
 */
class JTreeAA extends JTree {

	/**
	 * 
	 */
	public JTreeAA() {
		super();
		if (WEdit.AA) changeFont();
	}

	/**
	 * @param value
	 */
	public JTreeAA(Object[] value) {
		super(value);
		if (WEdit.AA) changeFont();
	}

	/**
	 * @param value
	 */
	public JTreeAA(Hashtable value) {
		super(value);
		if (WEdit.AA) changeFont();
	}

	/**
	 * @param value
	 */
	public JTreeAA(Vector value) {
		super(value);
		if (WEdit.AA) changeFont();
	}

	/**
	 * @param newModel
	 */
	public JTreeAA(TreeModel newModel) {
		super(newModel);
		if (WEdit.AA) changeFont();
	}

	/**
	 * @param root
	 */
	public JTreeAA(TreeNode root) {
		super(root);
		if (WEdit.AA) changeFont();
	}

	/**
	 * @param root
	 * @param asksAllowsChildren
	 */
	public JTreeAA(TreeNode root, boolean asksAllowsChildren) {
		super(root, asksAllowsChildren);
		if (WEdit.AA) changeFont();
	}

	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paint(java.awt.Graphics)
	 */
	public void paint(Graphics g) {
	    if (WEdit.AA) {
			Graphics2D G = (Graphics2D) g;
		    G.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
	    }
		super.paint(g);
	}
	
	private void changeFont() {setFont(getFont().deriveFont(Font.BOLD));}
}
