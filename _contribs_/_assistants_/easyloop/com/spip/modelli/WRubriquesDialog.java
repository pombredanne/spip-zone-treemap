package com.spip.modelli;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
/*
 * Created on 2-mag-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL
 *
 * */

/**
 * @date 2-mag-2005
 * @author Renato Formato
 *
 */
public class WRubriquesDialog extends WLoopAdapter {

	
	JCheckBoxAA id_rubrique,id_secteur,branche,id_parent,id_enfant,meme_parent;
	JCheckBoxAA recerche,id_mot,titre_mot,tout,id_groupe,type_mot;
	
	JCheckBoxAA LOGO_RUBRIQUE,LOGO_RUBRIQUE_NORMAL,LOGO_RUBRIQUE_SURVOL;
	
	JCheckBoxAA ID_RUBRIQUE,ID_SECTEUR,TITRE,DESCRIPTIF,TEXTE;
	JCheckBoxAA URL_RUBRIQUE,DATE,INTRODUCTION,NOTES;
	JCheckBoxAA FORMULAIRE_FORUM,PARAMETERS_FORUM,FORMULAIRE_SITE;
	
	/**
	 * @param owner The main windows
	 * @throws HeadlessException
	 */
	private WRubriquesDialog(Frame owner) throws HeadlessException {
		super(owner,Messages.getString("WRubriquesDialog.Title"));
		loopType = "RUBRIQUES";
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#showLoopDialog(java.awt.Frame)
	 */
	public static String[] showLoopDialog(Frame owner) {
		WRubriquesDialog rd = new WRubriquesDialog(owner);
		rd.show();
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#panelSelected()
	 */
	protected void panelSelected(String name) {
		StringBuffer loop = new StringBuffer();
		if (name.equals("pan_Results")) {
			loop.append(createStartLoop());
			loop.append(createSpecificCriterionLoop());
			loop.append(createCommonCriterionLoop());
			loop.append(createIncludedElementsLoop());
			loop.append(createLogosLoop());
			loop.append(createTagsLoop());
			loop.append(createEndLoop());
			result.setText(loop.toString());
		}
	}

	protected String createSpecificCriterionLoop() {
		StringBuffer loop = new StringBuffer();
	    if (id_rubrique.isSelected()) loop.append("{id_rubrique}");
	    if (id_secteur.isSelected()) loop.append("{id_secteur}");
	    if (branche.isSelected()) loop.append("{branche}");
	    if (id_parent.isSelected()) loop.append("{id_parent}");
	    if (id_enfant.isSelected()) loop.append("{id_enfant}");
	    if (meme_parent.isSelected()) loop.append("{meme_parent}");
	    if (recerche.isSelected()) loop.append("{recerche}");
	    if (id_mot.isSelected()) loop.append("{id_mot}");
	    if (titre_mot.isSelected()) loop.append("{titre_mot=xxx}");
	    if (tout.isSelected()) loop.append("{tout}");
	    if (id_groupe.isSelected()) loop.append("{id_groupe=zzz}");
	    if (type_mot.isSelected()) loop.append("{type_mot=yyy}");
		return loop.toString();
	}
	
	protected String createLogosLoop() {
		StringBuffer loop = new StringBuffer();
	    if (LOGO_RUBRIQUE.isSelected()) {
	    	loop.append("[(#LOGO_RUBRIQUE");
	    	createLogoFilters(loop);
	    }
	    if (LOGO_RUBRIQUE_NORMAL.isSelected()) {
	    	loop.append("[(#LOGO_RUBRIQUE_NORMAL");
	    	createLogoFilters(loop);
	    }
	    if (LOGO_RUBRIQUE_SURVOL.isSelected()) {
	    	loop.append("[(#LOGO_RUBRIQUE_SURVOL");
	    	createLogoFilters(loop);
	    }
	    return loop.toString();
	}
	
	protected String createTagsLoop() {
		StringBuffer loop = new StringBuffer();
	    if (ID_SECTEUR.isSelected()) {
	    	loop.append("[(#ID_SECTEUR");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (ID_RUBRIQUE.isSelected()) {
	    	loop.append("[(#ID_RUBRIQUE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (DATE.isSelected()) {
	    	loop.append("[(#DATE");
	    	if (TEXT_DATE.isSelected()) loop.append("|affdate)<br />]\n"); else loop.append(")]\n");
	    }
	    if (URL_RUBRIQUE.isSelected()) loop.append("<a href=\"#URL_RUBRIQUE\" target=\"_self\" title=\"[(#DESCRIPTIF|textebrut|entites_html)]\">\n");
	    if (TITRE.isSelected()) {
	    	loop.append("[(#TITRE");
	    	if (REMOVE_NUMBER.isSelected()) loop.append("|supprimer_numero");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (URL_RUBRIQUE.isSelected()) loop.append("</a>\n");
	    if (INTRODUCTION.isSelected()) {
	    	loop.append("[(#INTRODUCTION");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (DESCRIPTIF.isSelected()) {
	    	loop.append("[(#DESCRIPTIF");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }	    
	    if (TEXTE.isSelected()) {
	    	loop.append("[(#TEXTE");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (NOTES.isSelected()) {
	    	loop.append("[(#NOTES");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (FORMULAIRE_FORUM.isSelected()) loop.append("[(#FORMULAIRE_FORUM)]\n");
	    if (PARAMETERS_FORUM.isSelected()) loop.append("[(#PARAMETRES_FORUM)]\n");
	    if (FORMULAIRE_SITE.isSelected()) loop.append("[(#FORMULAIRE_SITE)]\n");
		return loop.toString();
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#helpSelected()
	 */
	protected void helpSelected() {
		JOptionPane.showMessageDialog(this,"Help");
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#cancelSelected()
	 */
	protected void cancelSelected() {
		hide();
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#enterSelected()
	 */
	protected void enterSelected() {
		StringBuffer loop = new StringBuffer();
		loop.append(createStartLoop());
		loop.append(createSpecificCriterionLoop());
		loop.append(createCommonCriterionLoop());
		loop.append(createIncludedElementsLoop());
		loop.append(createLogosLoop());
		loop.append(createTagsLoop());
		loop.append(createEndLoop());
		WEdit we = (WEdit)getParent();
		we.editor.replaceSelection(loop.toString());
		hide();
	}
	
	protected JComponent createSpecific() {
		JLabel l;
		JPanel panel;
		int compIndex=0;
		
		JPanel jp = (JPanel)super.createSpecific();

		jp.add(Box.createVerticalGlue(),compIndex++);
		l = new JLabel(Messages.getString("WLoopDialog.SelectionCrit"));
		l.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp.add(l,compIndex++);
		
		panel = new JPanel(new GridBagLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(5,0,0,0));
		id_rubrique = new JCheckBoxAALocalized("WLoopDialog.id_rubrique");
		id_secteur = new JCheckBoxAALocalized("WLoopDialog.id_secteur");
		branche = new JCheckBoxAALocalized("WLoopDialog.branche_rub");
		id_parent = new JCheckBoxAALocalized("WLoopDialog.id_parent");
		id_enfant =  new JCheckBoxAALocalized("WLoopDialog.id_enfant");
		meme_parent =  new JCheckBoxAALocalized("WLoopDialog.meme_parent");
		recerche =  new JCheckBoxAALocalized("WLoopDialog.recerche");
		id_mot =  new JCheckBoxAALocalized("WLoopDialog.id_mot");
		titre_mot =  new JCheckBoxAALocalized("WLoopDialog.titre_mot");
		tout =  new JCheckBoxAALocalized("WLoopDialog.tout");
		id_groupe =  new JCheckBoxAALocalized("WLoopDialog.id_groupe");
		type_mot =  new JCheckBoxAALocalized("WLoopDialog.type_mot");

		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 1;
		panel.add(id_rubrique,c);
		panel.add(id_secteur,c);
		panel.add(branche,c);
		panel.add(id_parent,c);
		panel.add(id_enfant,c);
		c.gridy = 1;
		panel.add(meme_parent,c);
		panel.add(recerche,c);
		panel.add(id_mot,c);
		panel.add(titre_mot,c);
		c.gridy = 2;
		panel.add(tout,c);
		panel.add(id_groupe,c);
		panel.add(type_mot,c);
		//to fix bad starting size, not sufficient to show all component
		Dimension d = panel.getMaximumSize();
		d.height = panel.getMinimumSize().height;
		panel.setMaximumSize(d);
		
		panel.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp.add(panel,compIndex++);
		
		return jp;
	}

	protected  JComponent createLogos() {
		JPanel panel;
		JLabel l;
		int compIndex=0;
		
		JPanel jp = (JPanel)super.createLogos();
		
		panel = (JPanel)jp.getComponent(0);
		panel.add(Box.createVerticalStrut(10),compIndex++);
		LOGO_RUBRIQUE = new JCheckBoxAA("LOGO_RUBRIQUE");
		panel.add(LOGO_RUBRIQUE,compIndex++);
		panel.add(Box.createVerticalStrut(LOGO_RUBRIQUE.getPreferredSize().height),
				compIndex++);
		
		compIndex=0;
		panel = (JPanel)jp.getComponent(1);
		panel.add(Box.createVerticalStrut(10),compIndex++);
		LOGO_RUBRIQUE_NORMAL = new JCheckBoxAA("LOGO_RUBRIQUE_NORMAL");
		panel.add(LOGO_RUBRIQUE_NORMAL,compIndex++);
		LOGO_RUBRIQUE_SURVOL = new JCheckBoxAA("LOGO_RUBRIQUESURVOL");
		panel.add(LOGO_RUBRIQUE_SURVOL,compIndex++);
		
		return jp;
	}

	protected JComponent createTags() {
		JPanel panel;
		JLabel l;
		int compIndex = 0;

		JPanel jp = (JPanel)super.createTags();
		
		panel = new JPanel(new GridBagLayout());
		l = new JLabel(Messages.getString("WLoopDialog.DB_Tags"));
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 1;
		c.gridwidth = 5;
		c.gridx = GridBagConstraints.RELATIVE;
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		ID_SECTEUR= new JCheckBoxAALocalized("WLoopDialog.ID_SECTEUR");
		panel.add(ID_SECTEUR,c);
		ID_RUBRIQUE = new JCheckBoxAALocalized("WLoopDialog.ID_RUBRIQUE");
		panel.add(ID_RUBRIQUE,c);
		TITRE = new JCheckBoxAALocalized("WLoopDialog.TITRE");
		panel.add(TITRE,c);
		DESCRIPTIF = new JCheckBoxAALocalized("WLoopDialog.DESCRIPTIF");
		panel.add(DESCRIPTIF,c);
		TEXTE = new JCheckBoxAALocalized("WLoopDialog.TEXTE");
		panel.add(TEXTE,c);
		jp.add(panel,compIndex++);

		panel = new JPanel(new GridBagLayout());
		c.gridy = 0;
		c.gridwidth = 4;
		l = new JLabel(Messages.getString("WLoopDialog.Calc_Tags"));
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		URL_RUBRIQUE = new JCheckBoxAALocalized("WLoopDialog.URL_RUBRIQUE");
		panel.add(URL_RUBRIQUE,c);
		DATE = new JCheckBoxAALocalized("WLoopDialog.DATE");
		panel.add(DATE,c);
		INTRODUCTION = new JCheckBoxAALocalized("WLoopDialog.INTRODUCTION");
		panel.add(INTRODUCTION,c);
		NOTES = new JCheckBoxAALocalized("WLoopDialog.NOTES");
		panel.add(NOTES,c);
		c.gridy = 2;
		FORMULAIRE_FORUM = new JCheckBoxAALocalized("WLoopDialog.FORMULAIRE_FORUM");
		panel.add(FORMULAIRE_FORUM,c);
		PARAMETERS_FORUM = new JCheckBoxAALocalized("WLoopDialog.PARAMETRES_FORUM");
		panel.add(PARAMETERS_FORUM,c);
		FORMULAIRE_SITE = new JCheckBoxAALocalized("WLoopDialog.FORMULAIRE_SITE");
		panel.add(FORMULAIRE_SITE,c);
		jp.add(panel,compIndex++);

		return jp;
	}
}
