/*
 * Created on 28-apr-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL		
 * 
 */
package com.spip.modelli;

/**
 * @date 28-apr-2005
 * @author Renato Formato
 *
 * 
 * 
 */
public interface LocaleListener {
	public void changedLocale();
}
