/*
 * Created on 4-mag-2005
 *
 */
package com.spip.modelli;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JWindow;


public class JSplash extends JWindow {
	
	private Image img;
	
	public JSplash() {
		super();
		URL url = JSplash.class.getResource("/com/spip/modelli/splash.png");
		img = Toolkit.getDefaultToolkit().createImage(url);
		//Since the createImage return imediately, waits for its loading
		MediaTracker mt = new MediaTracker(this);
		mt.addImage(img,0);
		try {
			mt.waitForAll();
		} catch (InterruptedException e) {}
		//Resize the splash windows as the image
		setSize(img.getWidth(this),img.getHeight(this));
		//Center splash window
		setLocationRelativeTo(null);
	}

	public void paint(Graphics g) {
		g.drawImage(img,0,0,this);
	}
	
	public void show() {
		//show the splash screen
	    super.show();
	    //	  Wait some seconds before hiding
	    Timer t = new Timer();
	    TimerTask tt = new TimerTask() {
			public void run() {
				hide();
			}
		};
	    t.schedule(tt,2000);		
	}
}