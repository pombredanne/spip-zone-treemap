/*
 * Created on 11-apr-2005
 * 
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL				  
 * 
 */
package com.spip.modelli;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * @author Renato Formato
 *
 * 
 * 
 */
public class WFindDialog extends JDialog implements DocumentListener, ActionListener {

	JTextAreaAA find,replace;
	JButtonAA btnfind,btnreplace,btncancel;
	static private String[] res;
	/**
	 * @param owner
	 * @param title
	 * @param modal
	 * @throws java.awt.HeadlessException
	 */
	private WFindDialog(Frame owner) throws HeadlessException {
		super(owner, Messages.getString("WFindDialog.Title"), true); //$NON-NLS-1$
		addGUI();
	}

	private void addGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		JPanel jp = new JPanel();
		jp.setLayout(new BoxLayout(jp,BoxLayout.Y_AXIS));
		JLabel jl = new JLabel(Messages.getString("WFindDialog.Find")); //$NON-NLS-1$
		jl.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		jp.add(jl);
		find = new JTextAreaAA();
		find.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		find.setRows(3);
		find.setAlignmentX(JTextAreaAA.LEFT_ALIGNMENT);
		find.getDocument().addDocumentListener(this);
		jp.add(find);
		jl = new JLabel(Messages.getString("WFindDialog.Replace")); //$NON-NLS-1$
		jl.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		jp.add(jl);
		replace = new JTextAreaAA();
		replace.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		replace.setAlignmentX(JTextAreaAA.LEFT_ALIGNMENT);
		replace.setRows(3);
		jp.add(replace);
		cp.add(jp,BorderLayout.CENTER);
		jp = new JPanel();
		btnfind = new JButtonAA(Messages.getString("WFindDialog.Find")); //$NON-NLS-1$
		btnfind.addActionListener(this);
		jp.add(btnfind);
		btnreplace = new JButtonAA(Messages.getString("WFindDialog.ReplaceALL")); //$NON-NLS-1$
		btnreplace.setEnabled(false);
		btnreplace.addActionListener(this);
		jp.add(btnreplace);
		btncancel = new JButtonAA(Messages.getString("WFindDialog.Cancel")); //$NON-NLS-1$
		btncancel.addActionListener(this);
		jp.add(btncancel);
		jp.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		cp.add(jp,BorderLayout.SOUTH);
		pack();
		setLocationRelativeTo(getParent());
	}

	/* (non-Javadoc)
	 * @see javax.swing.event.DocumentListener#changedUpdate(javax.swing.event.DocumentEvent)
	 */
	public void changedUpdate(DocumentEvent e) {
		updateButtons();
	}

	/* (non-Javadoc)
	 * @see javax.swing.event.DocumentListener#insertUpdate(javax.swing.event.DocumentEvent)
	 */
	public void insertUpdate(DocumentEvent e) {
		updateButtons();
	}

	/* (non-Javadoc)
	 * @see javax.swing.event.DocumentListener#removeUpdate(javax.swing.event.DocumentEvent)
	 */
	public void removeUpdate(DocumentEvent e) {
		updateButtons();
	}
	
	private void updateButtons() {
		boolean flag=false;
		if (find.getText()!=null && !find.getText().equals("")) {flag=true;} //$NON-NLS-1$
		btnreplace.setEnabled(flag);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		Object s = e.getSource();
		if (s==btncancel) {this.hide();return;}
		if (s==btnreplace) {
			res = new String[2];
			res[0]=find.getText();
			res[1]=replace.getText();
			hide();
			}
		if (s==btnfind) {
			res = new String[1];
			res[0]=find.getText();
			hide();
		}
	}
	
	static public String[] showFindDialog(Frame owner) {
		WFindDialog td = new WFindDialog(owner);
		td.show();
		return res;
	}
	
}
