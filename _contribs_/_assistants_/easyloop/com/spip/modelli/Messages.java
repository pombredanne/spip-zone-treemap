/*
 * Created on 19-apr-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL		
 * 
 */
package com.spip.modelli;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class responsible to retrieve all localized strings and available locales.
 * 
 * @author Renato Formato
 *	
 */
public class Messages {
	private static final String BUNDLE_NAME = "com.spip.modelli.messages";//$NON-NLS-1$

	private static Locale locale = Locale.getDefault();
	
	private static ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME,locale);

	private Messages() {
	}

	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}

	/**
	 * Sets the current locale
	 * @param l the <code>Locale</code> object 
	 */
	public static void setLocale(Locale l) {
		locale = l;
		RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME,locale);
	}
	
	public static Locale getLocale() {return RESOURCE_BUNDLE.getLocale();}
	
	/** 
	 *  Returns an array in which each row is a message file,
	 *  a specific locale. In the row, element 0 is the display 
	 *  string of the Locale and element 1 is the {@link Locale} object 
	 *  itself. 
	 * 
	 * 	@return An array with a display string and the Locale for each localized file.
	*/
	public static Object[] getLocales() {	
		JarResources r;
		String[] files;
		try {
			r = new JarResources("easyloop.jar");
			files = r.getEntryKeys();
		} catch (IOException e) {
			//File not found. Maybe we not in a Jar file
			//Try to find messages files in the file system
			ClassLoader cl = Messages.class.getClassLoader();
			URL res = cl.getResource("com/spip/modelli/");
			if(res==null) return null; //No resource found
			File f;
			try {
				f = new File(new URI(res.toString()));
				files = f.list(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						if (name.endsWith("properties")) return true;
						return false;
					}
				});
			} catch (URISyntaxException e1) {
				return null;
			}
		}
	    //Get the locale lang,country and variant if present as lang_country_variant 
	    Pattern p = Pattern.compile(".*messages(_(.*))\\.properties");
	    Vector found = new Vector();
	    Object[] locales;
	    for (int i = 0; i < files.length; i++) {
			Matcher m = p.matcher(files[i]);
			if (m.find()) {
				String[] loc = m.group(2).split("_");
				Locale l=null;
				locales = new Object[2];
				switch (loc.length) {
					case 1:
						l = new Locale(loc[0]);
						locales[0]=l.getDisplayLanguage(l);
						locales[1]=l;
						found.add(locales);
						break;
					case 2:
						l = new Locale(loc[0],loc[1]);
						locales[0]=l.getDisplayLanguage(l) + " (" + 
							l.getDisplayCountry(l) + ")";
						locales[1]=l;
						found.add(locales);
						break;
					case 3:	
						l = new Locale(loc[0],loc[1],loc[2]);
						locales[0]=l.getDisplayLanguage(l) + " (" + 
						l.getDisplayCountry(l) + " - " + 
						l.getDisplayVariant(l) + ")";
						locales[1]=l;
						found.add(locales);
				}
			}
		}	    
	    return found.toArray();
	}
}
