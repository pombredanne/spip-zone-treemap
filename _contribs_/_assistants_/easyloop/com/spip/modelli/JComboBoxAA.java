/*
 * Created on 6-apr-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL
 * 
 */
package com.spip.modelli;

import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.Vector;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;

/**
 * @author Renato Formato
 *
 * 
 * 
 */
public class JComboBoxAA extends JComboBox {

	/**
	 * 
	 */
	public JComboBoxAA() {
		super();
		if (WEdit.AA) {
			setRenderer(new DefaultListCellRendererAA());
			changeFont();
		}
	}

	/**
	 * @param items
	 */
	public JComboBoxAA(Object[] items) {
		super(items);
		if (WEdit.AA) {
			setRenderer(new DefaultListCellRendererAA());
			changeFont();
		}
	}

	/**
	 * @param items
	 */
	public JComboBoxAA(Vector items) {
		super(items);
		if (WEdit.AA) {
			setRenderer(new DefaultListCellRendererAA());
			changeFont();
		}
	}

	/**
	 * @param aModel
	 */
	public JComboBoxAA(ComboBoxModel aModel) {
		super(aModel);
		if (WEdit.AA) {
			setRenderer(new DefaultListCellRendererAA());
			changeFont();
		}
	}

	public void paint(Graphics g) {
	    if (WEdit.AA) {
		    Graphics2D G = (Graphics2D) g;
		    G.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		}
		super.paint(g);
	}
	
	private class DefaultListCellRendererAA extends DefaultListCellRenderer {
		
		/* (non-Javadoc)
		 * @see javax.swing.DefaultListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
		 */
		public Component getListCellRendererComponent(JList list, Object value,
				int index, boolean isSelected, boolean cellHasFocus) {
			Component c = super.getListCellRendererComponent(list, value, index,
					isSelected, cellHasFocus);
			if (WEdit.AA) c.setFont(c.getFont().deriveFont(Font.BOLD));
			return c;
		}
		
		public void paint(Graphics g) {
		    if (WEdit.AA) {
			    Graphics2D G = (Graphics2D) g;
			    G.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		    }
			super.paint(g);
		}
		
	}
	
	private void changeFont() {setFont(getFont().deriveFont(Font.BOLD));}
	
}
