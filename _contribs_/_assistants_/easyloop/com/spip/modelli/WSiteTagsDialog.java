/*
 * Created on 4-mag-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL
 * 
 */
package com.spip.modelli;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * @date 4-mag-2005
 * @author Renato Formato
 *
 */
public class WSiteTagsDialog extends WLoopAdapter {

	JCheckBoxAA URL_SITE_SPIP,LOGO_SITE_SPIP,NOM_SITE_SPIP;
	JCheckBoxAA EMAIL_WEBMASTER,CHARSET,PUCE,FORMULAIRE_ADMIN;
	
	/**
	 * @param owner
	 * @param title
	 * @throws HeadlessException
	 */
	public WSiteTagsDialog(Frame owner) throws HeadlessException {
		super(owner, Messages.getString("WSiteTagsDialog.Title"));
		btnGeneral.setVisible(false);
		btnSpecific.setVisible(false);
		btnCommon.setVisible(false);
		btnLogos.setVisible(false);
		btnTags.doClick();
	}
	
	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#showLoopDialog(java.awt.Frame)
	 */
	public static String[] showLoopDialog(Frame owner) {
		WSiteTagsDialog sd = new WSiteTagsDialog(owner);
		sd.show();
		return null;
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#panelSelected()
	 */
	protected void panelSelected(String name) {
		StringBuffer loop = new StringBuffer();
		if (name.equals("pan_Results")) {
			loop.append(createTagsLoop());
			result.setText(loop.toString());
		}			
	}

	protected String createTagsLoop() {
		StringBuffer loop = new StringBuffer();
	    if (URL_SITE_SPIP.isSelected()) {
	    	loop.append("[(#URL_SITE_SPIP");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (LOGO_SITE_SPIP.isSelected()) {
	    	loop.append("[(#LOGO_SITE_SPIP");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (NOM_SITE_SPIP.isSelected()) {
	    	loop.append("[(#NOM_SITE_SPIP");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (EMAIL_WEBMASTER.isSelected()) {
	    	loop.append("[(#EMAIL_WEBMASTER");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (CHARSET.isSelected()) {
	    	loop.append("[(#CHARSET");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (PUCE.isSelected()) loop.append("[(#PUCE)]\n");
	    if (FORMULAIRE_ADMIN.isSelected()) loop.append("[(#FORMULAIRE_ADMIN)]\n");
		return loop.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#helpSelected()
	 */
	protected void helpSelected() {
		JOptionPane.showMessageDialog(this,"Help");
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#cancelSelected()
	 */
	protected void cancelSelected() {
		hide();
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#enterSelected()
	 */
	protected void enterSelected() {
		StringBuffer loop = new StringBuffer();
		loop.append(createTagsLoop());
		WEdit we = (WEdit)getParent();
		we.editor.replaceSelection(loop.toString());
		hide();
	}
	
	protected JComponent createTags() {
		JPanel panel;
		JLabel l;
		int compIndex = 0;

		JPanel jp = (JPanel)super.createTags();
		
		TEXT_DATE.setVisible(false);
		JUSTIFY.setVisible(false);
		REMOVE_NUMBER.setVisible(false);
		
		((JPanel)jp.getComponent(0)).remove(1);
		
		
		panel = new JPanel(new GridBagLayout());
		l = new JLabel(Messages.getString("WLoopDialog.DB_Tags"));
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 1;
		c.gridwidth = 5;
		c.gridx = GridBagConstraints.RELATIVE;
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		URL_SITE_SPIP = new JCheckBoxAALocalized("WLoopDialog.URL_SITE_SPIP");
		panel.add(URL_SITE_SPIP,c);
		LOGO_SITE_SPIP = new JCheckBoxAALocalized("WLoopDialog.LOGO_SITE_SPIP");
		panel.add(LOGO_SITE_SPIP,c);
		NOM_SITE_SPIP = new JCheckBoxAALocalized("WLoopDialog.NOM_SITE_SPIP");
		panel.add(NOM_SITE_SPIP,c);
		c.gridy = 2;
		EMAIL_WEBMASTER = new JCheckBoxAALocalized("WLoopDialog.EMAIL_WEBMASTER");
		panel.add(EMAIL_WEBMASTER,c);
		CHARSET = new JCheckBoxAALocalized("WLoopDialog.CHARSET");
		panel.add(CHARSET,c);
		PUCE = new JCheckBoxAALocalized("WLoopDialog.PUCE");
		panel.add(PUCE,c);
		c.gridy = 3;
		FORMULAIRE_ADMIN = new JCheckBoxAALocalized("WLoopDialog.FORMULAIRE_ADMIN");
		panel.add(FORMULAIRE_ADMIN,c);
		jp.add(panel,compIndex++);

		return jp;
	}	
}
