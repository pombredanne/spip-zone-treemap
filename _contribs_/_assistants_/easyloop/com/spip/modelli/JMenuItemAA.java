/*
 * Created on 6-apr-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL		
 * 
 */
package com.spip.modelli;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JMenuItem;

/**
 * @author Renato Formato
 *
 * 
 * 
 */
public class JMenuItemAA extends JMenuItem {

	/**
	 * 
	 */
	public JMenuItemAA() {
		super();
		if (WEdit.AA) changeFont();
	}

	/**
	 * @param text
	 */
	public JMenuItemAA(String text) {
		super(text);
		if (WEdit.AA) changeFont();
	}

	/**
	 * @param text
	 * @param mnemonic
	 */
	public JMenuItemAA(String text, int mnemonic) {
		super(text, mnemonic);
		if (WEdit.AA) changeFont();
	}

	/**
	 * @param a
	 */
	public JMenuItemAA(Action a) {
		super(a);
		if (WEdit.AA) changeFont();
	}

	/**
	 * @param icon
	 */
	public JMenuItemAA(Icon icon) {
		super(icon);
		if (WEdit.AA) changeFont();
	}

	/**
	 * @param text
	 * @param icon
	 */
	public JMenuItemAA(String text, Icon icon) {
		super(text, icon);
		if (WEdit.AA) changeFont();
	}

	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paint(java.awt.Graphics)
	 */
	public void paint(Graphics g) {
	    if (WEdit.AA) {
			Graphics2D G = (Graphics2D) g;
		    G.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
	    }
	    super.paint(g);
	}
	
	private void changeFont() {setFont(getFont().deriveFont(Font.BOLD));}
}
