package com.spip.modelli;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
/*
 * Created on 2-mag-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL
 *
 * */

/**
 * @date 2-mag-2005
 * @author Renato Formato
 *
 */
public class WMotsDialog extends WLoopAdapter {

	JCheckBoxAA tout,id_mot,id_groupe,id_rubrique,id_article;
	JCheckBoxAA id_breve,id_syndic,id_forum,titre,type;
	
	JCheckBoxAA LOGO_MOT,LOGO_MOT_NORMAL,LOGO_MOT_SURVOL;
	
	JCheckBoxAA ID_MOT,TITRE,DESCRIPTIF,TEXTE,TYPE;
	JCheckBoxAA URL_MOT,ID_GROUPE,GROUPE_TITRE;

	/**
	 * @param owner The main windows
	 * @throws HeadlessException
	 */
	private WMotsDialog(Frame owner) throws HeadlessException {
		super(owner,Messages.getString("WMotsDialog.Title"));
		loopType = "MOTS";
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#showLoopDialog(java.awt.Frame)
	 */
	public static String[] showLoopDialog(Frame owner) {
		WMotsDialog md = new WMotsDialog(owner);
		md.show();
		return null;
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#panelSelected()
	 */
	protected void panelSelected(String name) {
		StringBuffer loop = new StringBuffer();
		if (name.equals("pan_Results")) {
			loop.append(createStartLoop());
			loop.append(createSpecificCriterionLoop());
			loop.append(createCommonCriterionLoop());
			loop.append(createIncludedElementsLoop());
			loop.append(createLogosLoop());
			loop.append(createTagsLoop());
			loop.append(createEndLoop());
			result.setText(loop.toString());
		}			
	}

	protected String createSpecificCriterionLoop() {
		StringBuffer loop = new StringBuffer();
		if (tout.isSelected()) loop.append("{tout}");
		if (id_mot.isSelected()) loop.append("{id_mot}");
		if (id_groupe.isSelected()) loop.append("{id_groupe=zzz}");		
		if (id_rubrique.isSelected()) loop.append("{id_rubrique}");
		if (id_article.isSelected()) loop.append("{id_article}");
		if (id_breve.isSelected()) loop.append("{id_breve}");
	    if (id_syndic.isSelected()) loop.append("{id_syndic}");
	    if (id_forum.isSelected()) loop.append("{id_forum}");
	    if (titre.isSelected()) loop.append("{titre=xxx}");
	    if (type.isSelected()) loop.append("{type=yyy}");
	    	    return loop.toString();
	}
	
	protected String createLogosLoop() {
		StringBuffer loop = new StringBuffer();
	    if (LOGO_MOT.isSelected()) {
	    	loop.append("[(#LOGO_MOT");
	    	createLogoFilters(loop);
	    }
	    if (LOGO_MOT_NORMAL.isSelected()) {
	    	loop.append("[(#LOGO_MOT_NORMAL");
	    	createLogoFilters(loop);
	    }
	    if (LOGO_MOT_SURVOL.isSelected()) {
	    	loop.append("[(#LOGO_MOT_SURVOL");
	    	createLogoFilters(loop);
	    }
	    return loop.toString();
	}
	
	protected String createTagsLoop() {
		StringBuffer loop = new StringBuffer();
	    if (ID_MOT.isSelected()) {
	    	loop.append("[(#ID_MOT");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (ID_GROUPE.isSelected()) {
	    	loop.append("[(#ID_GROUPE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (TYPE.isSelected()) {
	    	loop.append("[(#TYPE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (URL_MOT.isSelected()) loop.append("<a href=\"#URL_MOT\" target=\"_self\" title=\"[(#DESCRIPTIF|textebrut|entites_html)]\">\n");
	    if (TITRE.isSelected()) {
	    	loop.append("[(#TITRE");
	    	if (REMOVE_NUMBER.isSelected()) loop.append("|supprimer_numero");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (URL_MOT.isSelected()) loop.append("</a>\n");
	    if (DESCRIPTIF.isSelected()) {
	    	loop.append("[(#DESCRIPTIF");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }	    
	    if (TEXTE.isSelected()) {
	    	loop.append("[(#TEXTE");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    return loop.toString();
		/*//CONTENU BALISES ///////////////////////////////////////////
	    If URL_MOT.Value then
	      LeText = LeText + "                 <a href=""#URL_MOT"" target=""_self"" title=""[(#DESCRIPTIF|textebrut|entites_html)]"">"+ CR
	    End
	    
	    
	    If TITRE223.Value then
	      LeText = LeText + "                      [(#"+ TITRE223.Caption +""
	      if SuppNumeroTitre.Value then
	        LeText = LeText + "|"+SuppNumeroTitre.Caption +""
	      End
	      If brBalise.Value then
	        LeText = LeText + ")"+ brBalise.Caption +"]" + CR
	      Else
	        LeText = LeText + ")]" + CR
	      End
	    End
	    
	    
	    If URL_MOT.Value then
	      LeText = LeText + "                 </a>"+ CR
	    End*/
	}
		
	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#helpSelected()
	 */
	protected void helpSelected() {
		JOptionPane.showMessageDialog(this,"Help");
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#cancelSelected()
	 */
	protected void cancelSelected() {
		hide();
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#enterSelected()
	 */
	protected void enterSelected() {
		StringBuffer loop = new StringBuffer();
		loop.append(createStartLoop());
		loop.append(createSpecificCriterionLoop());
		loop.append(createCommonCriterionLoop());
		loop.append(createIncludedElementsLoop());
		loop.append(createLogosLoop());
		loop.append(createTagsLoop());
		loop.append(createEndLoop());
		WEdit we = (WEdit)getParent();
		we.editor.replaceSelection(loop.toString());
		hide();
	}
	
	protected JComponent createSpecific() {
		JLabel l;
		JPanel panel;
		int compIndex=0;
		
		JPanel jp = (JPanel)super.createSpecific();

		((JPanel)jp.getComponent(2)).remove(exclus);
		
		jp.add(Box.createVerticalGlue(),compIndex++);
		l = new JLabel(Messages.getString("WLoopDialog.SelectionCrit"));
		l.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp.add(l,compIndex++);
		
		panel = new JPanel(new GridBagLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(5,0,0,0));
		tout = new JCheckBoxAALocalized("WLoopDialog.tout");
		id_mot =  new JCheckBoxAALocalized("WLoopDialog.id_mot");
		id_groupe =  new JCheckBoxAALocalized("WLoopDialog.id_groupe");
		id_rubrique = new JCheckBoxAALocalized("WLoopDialog.id_rubrique");
		id_article =  new JCheckBoxAALocalized("WLoopDialog.id_article");
		id_breve =  new JCheckBoxAALocalized("WLoopDialog.id_breve");
		id_syndic =  new JCheckBoxAALocalized("WLoopDialog.id_syndic");
		id_forum =  new JCheckBoxAALocalized("WLoopDialog.id_forum");
		titre =  new JCheckBoxAALocalized("WLoopDialog.titre");
		type =  new JCheckBoxAALocalized("WLoopDialog.type");
		
		
				
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx=1;
		panel.add(tout,c);
		panel.add(id_mot,c);
		panel.add(id_groupe,c);
		panel.add(id_rubrique,c);
		panel.add(id_article,c);
		c.gridy = 1;
		panel.add(id_breve,c);
		panel.add(id_syndic,c);
		panel.add(id_forum,c);
		panel.add(titre,c);
		panel.add(type,c);
		
		//to fix bad starting size, not sufficient to show all component
		//panel.setMaximumSize(panel.getMinimumSize());
		Dimension d = panel.getMaximumSize();
		d.height = panel.getMinimumSize().height;
		panel.setMaximumSize(d);

		panel.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp.add(panel,compIndex++);
		
		return jp;
	}
	
	protected  JComponent createLogos() {
		JPanel panel;
		JLabel l;
		int compIndex=0;
		
		JPanel jp = (JPanel)super.createLogos();
		
		panel = (JPanel)jp.getComponent(0);
		panel.add(Box.createVerticalStrut(10),compIndex++);
		LOGO_MOT = new JCheckBoxAA("LOGO_MOT");
		panel.add(LOGO_MOT,compIndex++);
		panel.add(Box.createVerticalStrut(LOGO_MOT.getPreferredSize().height),
				compIndex++);
		
		compIndex=0;
		panel = (JPanel)jp.getComponent(1);
		panel.add(Box.createVerticalStrut(10),compIndex++);
		LOGO_MOT_NORMAL = new JCheckBoxAA("LOGO_MOT_NORMAL");
		panel.add(LOGO_MOT_NORMAL,compIndex++);
		LOGO_MOT_SURVOL = new JCheckBoxAA("LOGO_MOT_SURVOL");
		panel.add(LOGO_MOT_SURVOL,compIndex++);
		
		return jp;
	}

	protected JComponent createTags() {
		JPanel panel;
		JLabel l;
		int compIndex = 0;

		JPanel jp = (JPanel)super.createTags();
		
		((JPanel)jp.getComponent(0)).remove(TEXT_DATE);
		
		panel = new JPanel(new GridBagLayout());
		l = new JLabel(Messages.getString("WLoopDialog.DB_Tags"));
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 1;
		c.gridwidth = 5;
		c.gridx = GridBagConstraints.RELATIVE;
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		ID_MOT = new JCheckBoxAALocalized("WLoopDialog.ID_MOT");
		panel.add(ID_MOT,c);
		TITRE = new JCheckBoxAALocalized("WLoopDialog.TITRE");
		panel.add(TITRE,c);
		DESCRIPTIF = new JCheckBoxAALocalized("WLoopDialog.DESCRIPTIF");
		panel.add(DESCRIPTIF,c);
		TEXTE = new JCheckBoxAALocalized("WLoopDialog.TEXTE");
		panel.add(TEXTE,c);
		TYPE = new JCheckBoxAALocalized("WLoopDialog.TYPE");
		panel.add(TYPE,c);

		jp.add(panel,compIndex++);

		panel = new JPanel(new GridBagLayout());
		c.gridy = 0;
		c.gridwidth = 4;
		l = new JLabel(Messages.getString("WLoopDialog.Calc_Tags"));
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		URL_MOT = new JCheckBoxAALocalized("WLoopDialog.URL_MOT");
		panel.add(URL_MOT,c);
		jp.add(panel,compIndex++);

		panel = new JPanel(new GridBagLayout());
		c.gridy = 0;
		c.gridwidth = 4;
		l = new JLabel(Messages.getString("WLoopDialog.LoopGroup"));
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		ID_GROUPE = new JCheckBoxAALocalized("WLoopDialog.ID_GROUPE");
		panel.add(ID_GROUPE,c);
		GROUPE_TITRE = new JCheckBoxAALocalized("WLoopDialog.TITRE");
		panel.add(GROUPE_TITRE,c);
		jp.add(panel,compIndex++);
		
		return jp;
	}
}
