/*
 * Created on 6-apr-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL		
 * 
 */
package com.spip.modelli;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.Action;
import javax.swing.JMenu;

/**
 * @author Renato Formato
 *
 * 
 * 
 */
public class JMenuAA extends JMenu {

	
	/**
	 * 
	 */
	public JMenuAA() {
		super();
		if (WEdit.AA) changeFont();
	}

	/**
	 * @param s
	 */
	public JMenuAA(String s) {
		super(s);
		if (WEdit.AA) changeFont();
	}

	/**
	 * @param s
	 * @param b
	 */
	public JMenuAA(String s, boolean b) {
		super(s, b);
		if (WEdit.AA) changeFont();
	}

	/**
	 * @param a
	 */
	public JMenuAA(Action a) {
		super(a);
		if (WEdit.AA) changeFont();
	}

	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paint(java.awt.Graphics)
	 */
	public void paint(Graphics g) {
	    if (WEdit.AA) {
		    Graphics2D G = (Graphics2D) g;
		    G.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
	    }
		super.paint(g);
	}
	
	private void changeFont() {setFont(getFont().deriveFont(Font.BOLD));}
}
