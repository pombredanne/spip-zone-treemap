/*
 * Created on 1-mag-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL
 * 				  
 */
package com.spip.modelli;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * @date 1-mag-2005
 * @author Renato Formato
 *
 */
public class WHierarchyDialog extends WLoopAdapter {

	JCheckBoxAA id_rubrique,id_article;
	
	JCheckBoxAA TITRE,URL_RUBRIQUE;
	
	private WHierarchyDialog(Frame owner) throws HeadlessException {
		super(owner,Messages.getString("WHierarchyDialog.Title"));
		btnLogos.setVisible(false);
		loopType = "HIERARCHIE";
	}
	
		
	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#showLoopDialog(java.awt.Frame)
	 */
	public static String[] showLoopDialog(Frame owner) {
		WHierarchyDialog hd = new WHierarchyDialog(owner);
		hd.show();
		return null;
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#panelSelected()
	 */
	protected void panelSelected(String name) {
		StringBuffer loop = new StringBuffer();
		if (name.equals("pan_Results")) {
			loop.append(createStartLoop());
			loop.append(createSpecificCriterionLoop());
			loop.append(createCommonCriterionLoop());
			loop.append(createIncludedElementsLoop());
			loop.append(createTagsLoop());
			loop.append(createEndLoop());
			result.setText(loop.toString());
		}			
	}

	protected String createSpecificCriterionLoop() {
		StringBuffer loop = new StringBuffer();
	    if (id_rubrique.isSelected()) loop.append("{id_rubrique}");
	    if (id_article.isSelected()) loop.append("{id_article}");
	    return loop.toString();
	}
	
	protected String createTagsLoop() {
		StringBuffer loop = new StringBuffer();
		if (URL_RUBRIQUE.isSelected()) loop.append("<a href=\"#URL_RUBRIQUE\" target=\"_self\" title=\"[(#DESCRIPTIF|textebrut|entites_html)]\">\n");
		if (TITRE.isSelected()) {
	    	loop.append("[(#TITRE");
	    	if (REMOVE_NUMBER.isSelected()) loop.append("|supprimer_numero");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (URL_RUBRIQUE.isSelected()) loop.append("</a>\n");
		return loop.toString();
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#helpSelected()
	 */
	protected void helpSelected() {
		JOptionPane.showMessageDialog(this,"Help");
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#cancelSelected()
	 */
	protected void cancelSelected() {
		hide();
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#enterSelected()
	 */
	protected void enterSelected() {
		StringBuffer loop = new StringBuffer();
		loop.append(createStartLoop());
		loop.append(createSpecificCriterionLoop());
		loop.append(createCommonCriterionLoop());
		loop.append(createIncludedElementsLoop());
		loop.append(createTagsLoop());
		loop.append(createEndLoop());
		WEdit we = (WEdit)getParent();
		we.editor.replaceSelection(loop.toString());
		hide();
	}

	protected JComponent createSpecific() {
		JLabel l;
		JPanel panel;
		int compIndex=0;
		
		JPanel jp = (JPanel)super.createSpecific();

		JPanel co = (JPanel)jp.getComponent(2); 
		co.remove(exclus);
		co.remove(unique);
		
		jp.add(Box.createVerticalGlue(),compIndex++);
		l = new JLabel(Messages.getString("WLoopDialog.SelectionCrit"));
		l.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp.add(l,compIndex++);
		
		panel = new JPanel(new GridBagLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(5,0,0,0));
		id_rubrique = new JCheckBoxAALocalized("WLoopDialog.id_rubrique");
		id_article = new JCheckBoxAALocalized("WLoopDialog.id_article");

		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 1;
		panel.add(id_rubrique,c);
		panel.add(id_article,c);
		//to fix bad starting size, not sufficient to show all component
		Dimension d = panel.getMaximumSize();
		d.height = panel.getMinimumSize().height;
		panel.setMaximumSize(d);
		
		panel.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp.add(panel,compIndex++);
		
		return jp;
	}
	
	protected JComponent createCommon() {
		JPanel panel,panel2;
		JPanel jp = (JPanel)super.createCommon();
		panel = (JPanel)jp.getComponent(0);
		panel.remove(6);
		panel.remove(5);
		panel.remove(4);
		sortDate.setVisible(false);
		panel = (JPanel)jp.getComponent(1);
		panel2 = (JPanel)panel.getComponent(0);
		panel2.remove(1);
		panel2.add(Box.createVerticalStrut(inverse.getPreferredSize().height));
		panel2 = (JPanel)panel.getComponent(1);
		panel2.remove(3);
		panel2.remove(2);
		panel2.remove(1);
		panel2.remove(0);
		
		return jp;
	}
	
	protected JComponent createTags() {
		JPanel panel;
		JLabel l;
		int compIndex = 0;

		JPanel jp = (JPanel)super.createTags();
		
		TEXT_DATE.setVisible(false);
		JUSTIFY.setVisible(false);
		
		panel = new JPanel(new GridBagLayout());
		l = new JLabel(Messages.getString("WLoopDialog.DB_Tags"));
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 1;
		c.gridwidth = 5;
		c.gridx = GridBagConstraints.RELATIVE;
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		TITRE = new JCheckBoxAALocalized("WLoopDialog.TITRE");
		panel.add(TITRE,c);
		jp.add(panel,compIndex++);

		panel = new JPanel(new GridBagLayout());
		c.gridy = 0;
		c.gridwidth = 4;
		l = new JLabel(Messages.getString("WLoopDialog.Calc_Tags"));
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		URL_RUBRIQUE = new JCheckBoxAALocalized("WLoopDialog.URL_RUBRIQUE");
		panel.add(URL_RUBRIQUE,c);
		jp.add(panel,compIndex++);

		return jp;
	}	
}
