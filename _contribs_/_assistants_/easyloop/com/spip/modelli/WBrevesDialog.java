package com.spip.modelli;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
/*
 * Created on 2-mag-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL
 *
 * */

/**
 * @date 2-mag-2005
 * @author Renato Formato
 *
 */
public class WBrevesDialog extends WLoopAdapter {

	JCheckBoxAA tout,id_rubrique,id_breve,id_mot;
	JCheckBoxAA titre_mot,type_mot,id_groupe,recerche;
	
	JCheckBoxAA logo_breve,logo_breve_rub,logo_breve_norm,logo_breve_roll;
	
	JCheckBoxAA ID_BREVE,DATE,TITRE,ID_RUBRIQUE,TEXTE,NOM_SITE,URL_SITE;
	JCheckBoxAA URL_BREVE,INTRODUCTION,NOTES,FORMULAIRE_FORUM,PARAMETRES_FORUM;
	
	/**
	 * @param owner The main windows
	 * @throws HeadlessException
	 */
	private WBrevesDialog(Frame owner) throws HeadlessException {
		super(owner,Messages.getString("WBrevesDialog.Title"));
		loopType = "BREVES";
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#showLoopDialog(java.awt.Frame)
	 */
	public static String[] showLoopDialog(Frame owner) {
		WBrevesDialog bd = new WBrevesDialog(owner);
		bd.show();
		return null;
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#panelSelected()
	 */
	protected void panelSelected(String name) {
		StringBuffer loop = new StringBuffer();
		if (name.equals("pan_Results")) {
			loop.append(createStartLoop());
			loop.append(createSpecificCriterionLoop());
			loop.append(createCommonCriterionLoop());
			loop.append(createIncludedElementsLoop());
			loop.append(createLogosLoop());
			loop.append(createTagsLoop());
			loop.append(createEndLoop());
			result.setText(loop.toString());
		}
	}

	protected String createSpecificCriterionLoop() {
		StringBuffer loop = new StringBuffer();
	    if (id_rubrique.isSelected()) loop.append("{id_rubrique}");
	    if (id_breve.isSelected()) loop.append("{id_secteur}");
	    if (tout.isSelected()) loop.append("{tout}");
	    if (id_mot.isSelected()) loop.append("{id_mot}");
	    if (titre_mot.isSelected()) loop.append("{titre_mot=xxx}");
	    if (id_groupe.isSelected()) loop.append("{id_groupe=zzz}");
	    if (type_mot.isSelected()) loop.append("{type_mot=yyy}");
	    if (recerche.isSelected()) loop.append("{recerche}");
		return loop.toString();
	}
	
	protected String createLogosLoop() {
		StringBuffer loop = new StringBuffer();
	    if (logo_breve.isSelected()) {
	    	loop.append("[(#LOGO_BREVE");
	    	createLogoFilters(loop);
	    }
	    if (logo_breve_rub.isSelected()) {
	    	loop.append("[(#LOGO_BREVE_RUBRIQUE");
	    	createLogoFilters(loop);
	    }
	    if (logo_breve_norm.isSelected()) {
	    	loop.append("[(#LOGO_BREVE_NORMAL");
	    	createLogoFilters(loop);
	    }
	    if (logo_breve_roll.isSelected()) {
	    	loop.append("[(#LOGO_BREVE_SURVOL");
	    	createLogoFilters(loop);
	    }
	    return loop.toString();
	}
	
	protected String createTagsLoop() {
		StringBuffer loop = new StringBuffer();
	    if (ID_RUBRIQUE.isSelected()) {
	    	loop.append("[(#ID_RUBRIQUE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (ID_BREVE.isSelected()) {
	    	loop.append("[(#ID_Breve");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (DATE.isSelected()) {
	    	loop.append("[(#DATE");
	    	if (TEXT_DATE.isSelected()) loop.append("|affdate");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (URL_BREVE.isSelected()) loop.append("<a href=\"#URL_BREVE\" target=\"_self\" title=\"[(#DESCRIPTIF|textebrut|entites_html)]\">\n");
	    if (TITRE.isSelected()) {
	    	loop.append("[(#TITRE");
	    	if (REMOVE_NUMBER.isSelected()) loop.append("|supprimer_numero");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (URL_BREVE.isSelected()) loop.append("</a>\n");
	    if (INTRODUCTION.isSelected()) {
	    	loop.append("[(#INTRODUCTION");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (TEXTE.isSelected()) {
	    	loop.append("[(#TEXTE");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (URL_SITE.isSelected()) loop.append("<a href=\"#URL_SITE\" target=\"_self\" title=\"[(#DESCRIPTIF|textebrut|entites_html)]\">\n");
	    if (NOM_SITE.isSelected()) {
	    	loop.append("[(#NOM_SITE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (URL_SITE.isSelected()) loop.append("</a>\n");
	    if (NOTES.isSelected()) {
	    	loop.append("[(#NOTES");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (FORMULAIRE_FORUM.isSelected()) loop.append("[(#FORMULAIRE_FORUM)]\n");
	    if (PARAMETRES_FORUM.isSelected()) loop.append("[(#PARAMETRES_FORUM)]\n");
	    return loop.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#helpSelected()
	 */
	protected void helpSelected() {
		JOptionPane.showMessageDialog(this,"Help");
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#cancelSelected()
	 */
	protected void cancelSelected() {
		hide();
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#enterSelected()
	 */
	protected void enterSelected() {
		StringBuffer loop = new StringBuffer();
		loop.append(createStartLoop());
		loop.append(createSpecificCriterionLoop());
		loop.append(createCommonCriterionLoop());
		loop.append(createIncludedElementsLoop());
		loop.append(createLogosLoop());
		loop.append(createTagsLoop());
		loop.append(createEndLoop());
		WEdit we = (WEdit)getParent();
		we.editor.replaceSelection(loop.toString());
		hide();
	}
	
	protected JComponent createSpecific() {
		JLabel l;
		JPanel panel;
		int compIndex=0;
		
		JPanel jp = (JPanel)super.createSpecific();

		jp.add(Box.createVerticalGlue(),compIndex++);
		l = new JLabel(Messages.getString("WLoopDialog.SelectionCrit"));
		l.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp.add(l,compIndex++);
		
		panel = new JPanel(new GridBagLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(5,0,0,0));
		tout = new JCheckBoxAALocalized("WLoopDialog.tout");
		id_rubrique = new JCheckBoxAALocalized("WLoopDialog.id_rubrique");
		id_breve =  new JCheckBoxAALocalized("WLoopDialog.id_breve");
		id_mot =  new JCheckBoxAALocalized("WLoopDialog.id_mot");
		titre_mot =  new JCheckBoxAALocalized("WLoopDialog.titre_mot");
		type_mot =  new JCheckBoxAALocalized("WLoopDialog.type_mot");
		id_groupe =  new JCheckBoxAALocalized("WLoopDialog.id_groupe");
		recerche =  new JCheckBoxAALocalized("WLoopDialog.recerche");
				
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx=1;
		panel.add(tout,c);
		panel.add(id_rubrique,c);
		panel.add(id_breve,c);
		panel.add(id_mot,c);
		c.gridy = 1;
		panel.add(titre_mot,c);
		panel.add(type_mot,c);
		panel.add(id_groupe,c);
		panel.add(recerche,c);
		
		//to fix bad starting size, not sufficient to show all component
		//panel.setMaximumSize(panel.getMinimumSize());
		Dimension d = panel.getMaximumSize();
		d.height = panel.getMinimumSize().height;
		panel.setMaximumSize(d);

		panel.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp.add(panel,compIndex++);
		
		return jp;
	}

	protected  JComponent createLogos() {
		JPanel panel;
		JLabel l;
		int compIndex=0;
		
		JPanel jp = (JPanel)super.createLogos();
		
		panel = (JPanel)jp.getComponent(0);
		panel.add(Box.createVerticalStrut(10),compIndex++);
		logo_breve = new JCheckBoxAA("LOGO_BREVE");
		panel.add(logo_breve,compIndex++);
		logo_breve_rub = 
			new JCheckBoxAA("LOGO_BREVE_RUBRIQUE");
		panel.add(logo_breve_rub,compIndex++);
		
		compIndex=0;
		panel = (JPanel)jp.getComponent(1);
		panel.add(Box.createVerticalStrut(10),compIndex++);
		logo_breve_norm = 
			new JCheckBoxAA("LOGO_BREVE_NORMAL");
		panel.add(logo_breve_norm,compIndex++);
		logo_breve_roll = 
			new JCheckBoxAA("LOGO_BREVE_SURVOL");
		panel.add(logo_breve_roll,compIndex++);
		
		return jp;
	}
	
	protected JComponent createTags() {
		JPanel panel;
		JLabel l;
		int compIndex = 0;

		JPanel jp = (JPanel)super.createTags();
		
		panel = new JPanel(new GridBagLayout());
		l = new JLabel(Messages.getString("WLoopDialog.DB_Tags"));
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 1;
		c.gridwidth = 5;
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		ID_BREVE= new JCheckBoxAALocalized("WLoopDialog.ID_BREVE");
		panel.add(ID_BREVE,c);
		TITRE = new JCheckBoxAALocalized("WLoopDialog.TITRE");
		panel.add(TITRE,c);
		DATE = new JCheckBoxAALocalized("WLoopDialog.DATE");
		panel.add(DATE,c);
		TEXTE = new JCheckBoxAALocalized("WLoopDialog.TEXTE");
		panel.add(TEXTE,c);
		ID_RUBRIQUE = new JCheckBoxAALocalized("WLoopDialog.ID_RUBRIQUE");
		panel.add(ID_RUBRIQUE,c);
		c.gridy = 2;
		NOM_SITE = new JCheckBoxAALocalized("WLoopDialog.NOM_SITE");
		panel.add(NOM_SITE,c);
		URL_SITE = new JCheckBoxAALocalized("WLoopDialog.URL_SITE");
		panel.add(URL_SITE,c);
		jp.add(panel,compIndex++);
		
		panel = new JPanel(new GridBagLayout());
		c.gridy=0;
		c.gridwidth = 4;
		l = new JLabel(Messages.getString("WLoopDialog.Calc_Tags"));
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		URL_BREVE = new JCheckBoxAALocalized("WLoopDialog.URL_BREVE");
		panel.add(URL_BREVE,c);
		INTRODUCTION = new JCheckBoxAALocalized("WLoopDialog.INTRODUCTION");
		panel.add(INTRODUCTION,c);
		NOTES = new JCheckBoxAALocalized("WLoopDialog.NOTES");
		panel.add(NOTES,c);
		c.gridy = 2;
		FORMULAIRE_FORUM = new JCheckBoxAALocalized("WLoopDialog.FORMULAIRE_FORUM");
		panel.add(FORMULAIRE_FORUM,c);
		PARAMETRES_FORUM = new JCheckBoxAALocalized("WLoopDialog.PARAMETRES_FORUM");
		panel.add(PARAMETRES_FORUM,c);
		jp.add(panel,compIndex++);

		return jp;
	}	
}
