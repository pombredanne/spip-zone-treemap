/*
 * Created on 28-apr-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL
 * 
 */
package com.spip.modelli;

import javax.swing.Icon;

/**
 * @date 28-apr-2005
 * @author Renato Formato
 *
 * 
 * 
 */
public class JButtonAALocalized extends JButtonAA implements LocaleListener {

	private String msg_ID;
	/**
	 * @param id
	 */
	public JButtonAALocalized(String id) {
		super(Messages.getString(id));
		msg_ID = id;
	}

	/**
	 * @param id
	 * @param icon
	 */
	public JButtonAALocalized(String id, Icon icon) {
		super(Messages.getString(id), icon);
		msg_ID = id;
	}

	public void changedLocale() {
		setText(Messages.getString(msg_ID));
	}
}
