/*
 * Created on 29-apr-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL				  
 * 
 */
package com.spip.modelli;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @date 29-apr-2005
 * @author Renato Formato
 *
 * Enumerate resources in a Jar file
 * 
 */
public class JarResources {

	private String path;
	private HashMap map = new HashMap();
	/**
	 * @throws IOException
	 * 
	 */
	public JarResources(String Jar_path) throws IOException {
		path=Jar_path;
		init();
	}
	
	private void init() throws IOException {
			JarFile jf = new JarFile(path);
			Enumeration je = jf.entries();
			if (je==null) {System.out.println("No Jarresources");return;}
			JarEntry entry;
			while (je.hasMoreElements()) {
				 entry = (JarEntry)je.nextElement();
				 map.put(entry.getName(),entry);
			}
	}

	public String[] getEntryKeys() {
		String[] temp = new String[1];
		return (String[])map.keySet().toArray(temp);
	}
}
