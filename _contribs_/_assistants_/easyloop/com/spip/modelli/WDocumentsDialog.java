/*
 * Created on 1-mag-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL
 * 				  
 */
package com.spip.modelli;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * @date 1-mag-2005
 * @author Renato Formato
 *
 */
public class WDocumentsDialog extends WLoopAdapter {

	JCheckBoxAA id_article,id_rubrique,id_breve,mode,extension,autostart;
	
	JCheckBoxAA LOGO_DOCUMENT,URL_DOCUMENT,TITRE,DESCRIPTIF;
	JCheckBoxAA TYPE_DOCUMENT,TAILLE,LARGEUR,HAUTEUR;
	JCheckBoxAA ID_DOCUMENT,EMBED_DOCUMENT;
	
	private WDocumentsDialog(Frame owner) throws HeadlessException {
		super(owner,Messages.getString("WDocumentsDialog.Title"));
		btnCommon.setVisible(false);
		btnLogos.setVisible(false);
		loopType = "DOCUMENTS";
	}
	
		
	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#showLoopDialog(java.awt.Frame)
	 */
	public static String[] showLoopDialog(Frame owner) {
		WDocumentsDialog dd = new WDocumentsDialog(owner);
		dd.show();
		return null;
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#panelSelected()
	 */
	protected void panelSelected(String name) {
		StringBuffer loop = new StringBuffer();
		if (name.equals("pan_Results")) {
			loop.append(createStartLoop());
			loop.append(createSpecificCriterionLoop());
			loop.append(createIncludedElementsLoop());
			loop.append(createTagsLoop());
			loop.append(createEndLoop());
			result.setText(loop.toString());
		}					
	}

	protected String createSpecificCriterionLoop() {
		StringBuffer loop = new StringBuffer();
		if (id_article.isSelected()) loop.append("{id_article}");
		if (id_rubrique.isSelected()) loop.append("{id_rubrique}");
	    if (id_breve.isSelected()) loop.append("{id_breve}");
	    if (mode.isSelected()) loop.append("{mode=document}");
	    if (extension.isSelected()) loop.append("{extension=}");
	    return loop.toString();
	}
	
	protected String createTagsLoop() {
		StringBuffer loop = new StringBuffer();
	    if (LOGO_DOCUMENT.isSelected()) {
	    	loop.append("[(#LOGO_DOCUMENT");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (URL_DOCUMENT.isSelected()) loop.append("<a href=\"#URL_DOCUMENT\" target=\"_self\" title=\"[(#DESCRIPTIF|textebrut|entites_html)]\">\n");
	    if (TITRE.isSelected()) {
	    	loop.append("[(#TITRE");
	    	if (REMOVE_NUMBER.isSelected()) loop.append("|supprimer_numero");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (URL_DOCUMENT.isSelected()) loop.append("</a>\n");
	    if (DESCRIPTIF.isSelected()) {
	    	loop.append("[(#DESCRIPTIF");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (TYPE_DOCUMENT.isSelected()) {
	    	loop.append("[(#TYPE_DOCUMENT");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (TAILLE.isSelected()) {
	    	loop.append("[(#TAILLE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (LARGEUR.isSelected()) {
	    	loop.append("[(#LARGEUR");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (HAUTEUR.isSelected()) {
	    	loop.append("[(#HAUTEUR");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (ID_DOCUMENT.isSelected()) {
	    	loop.append("[(#ID_DOCUMENT");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (EMBED_DOCUMENT.isSelected()) {
	    	loop.append("[(#EMBED_DOCUMENT");
	    	if (autostart.isSelected()) loop.append("|autostart=true)]\n"); else loop.append(")]\n");
	    }
	   return loop.toString();
	}
		
	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#helpSelected()
	 */
	protected void helpSelected() {
		JOptionPane.showMessageDialog(this,"Help");
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#cancelSelected()
	 */
	protected void cancelSelected() {
		hide();
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#enterSelected()
	 */
	protected void enterSelected() {
		StringBuffer loop = new StringBuffer();
		loop.append(createStartLoop());
		loop.append(createSpecificCriterionLoop());
		loop.append(createIncludedElementsLoop());
		loop.append(createTagsLoop());
		loop.append(createEndLoop());
		WEdit we = (WEdit)getParent();
		we.editor.replaceSelection(loop.toString());
		hide();
	}
	
	protected JComponent createSpecific() {
		JLabel l;
		JPanel panel;
		int compIndex=0;
		
		JPanel jp = (JPanel)super.createSpecific();

		panel = (JPanel)jp.getComponent(2);
		panel.remove(exclus);
		panel.remove(unique);
		mode = new JCheckBoxAALocalized("WLoopDialog.mode");
		panel.add(mode,0);
		extension = new JCheckBoxAALocalized("WLoopDialog.extension");
		panel.add(extension,2);
		
		jp.add(Box.createVerticalGlue(),compIndex++);
		l = new JLabel(Messages.getString("WLoopDialog.SelectionCrit"));
		l.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp.add(l,compIndex++);
		
		panel = new JPanel(new GridBagLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(5,0,0,0));
		id_article = new JCheckBoxAALocalized("WLoopDialog.id_article");
		id_rubrique = new JCheckBoxAALocalized("WLoopDialog.id_rubrique");
		id_breve = new JCheckBoxAALocalized("WLoopDialog.id_breve");

		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 1;
		panel.add(id_article,c);
		panel.add(id_rubrique,c);
		panel.add(id_breve,c);
		//to fix bad starting size, not sufficient to show all component
		Dimension d = panel.getMaximumSize();
		d.height = panel.getMinimumSize().height;
		panel.setMaximumSize(d);
		
		panel.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp.add(panel,compIndex++);
		
		return jp;
	}
	
	protected JComponent createTags() {
		JPanel panel;
		JLabel l;
		int compIndex = 0;

		JPanel jp = (JPanel)super.createTags();
		
		((JPanel)jp.getComponent(0)).remove(TEXT_DATE);
		
		autostart = new JCheckBoxAALocalized("autostart");
		GridBagConstraints c = new GridBagConstraints();
		c.gridy=1;
		c.gridx=3;
		((JPanel)jp.getComponent(0)).add(autostart,c);
		
		panel = new JPanel(new GridBagLayout());
		l = new JLabel(Messages.getString("WLoopDialog.DB_Tags"));
		c.anchor = GridBagConstraints.LINE_START;
		c.gridy=0;
		c.weightx = 1;
		c.gridwidth = 5;
		c.gridx = GridBagConstraints.RELATIVE;
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		LOGO_DOCUMENT = new JCheckBoxAALocalized("WLoopDialog.LOGO_DOCUMENT");
		panel.add(LOGO_DOCUMENT,c);
		URL_DOCUMENT = new JCheckBoxAALocalized("WLoopDialog.URL_DOCUMENT");
		panel.add(URL_DOCUMENT,c);
		TITRE = new JCheckBoxAALocalized("WLoopDialog.TITRE");
		panel.add(TITRE,c);
		DESCRIPTIF = new JCheckBoxAALocalized("WLoopDialog.DESCRIPTIF");
		panel.add(DESCRIPTIF,c);
		c.gridy = 2;
		TYPE_DOCUMENT = new JCheckBoxAALocalized("WLoopDialog.TYPE_DOCUMENT");
		panel.add(TYPE_DOCUMENT,c);
		TAILLE = new JCheckBoxAALocalized("WLoopDialog.TAILLE");
		panel.add(TAILLE,c);
		LARGEUR = new JCheckBoxAALocalized("WLoopDialog.LARGEUR");
		panel.add(LARGEUR,c);
		HAUTEUR = new JCheckBoxAALocalized("WLoopDialog.HAUTEUR");
		panel.add(HAUTEUR,c);
		c.gridy = 3;
		ID_DOCUMENT = new JCheckBoxAALocalized("WLoopDialog.ID_DOCUMENT");
		panel.add(ID_DOCUMENT,c);
		EMBED_DOCUMENT = new JCheckBoxAALocalized("WLoopDialog.EMBED_DOCUMENT");
		panel.add(EMBED_DOCUMENT,c);
		
		jp.add(panel,compIndex++);

		return jp;
	}	
}
