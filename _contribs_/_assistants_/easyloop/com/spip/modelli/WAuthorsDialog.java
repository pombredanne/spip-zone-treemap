package com.spip.modelli;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
/*
 * Created on 2-mag-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL
 *
 * */

/**
 * @date 2-mag-2005
 * @author Renato Formato
 *
 */
public class WAuthorsDialog extends WLoopAdapter {

	JCheckBoxAA tout,id_auteur,id_article;
	
	JCheckBoxAA LOGO_AUTEUR,LOGO_AUTEUR_NORMAL,LOGO_AUTEUR_SURVOL;
	
	JCheckBoxAA ID_AUTEUR,NOM,BIO,EMAIL,NOM_SITE,URL_SITE,PGP,URL_AUTEUR,NOTES;
	JCheckBoxAA FORMULAIRE_ECRIRE_AUTEUR;
	
	/**
	 * @param owner The main windows
	 * @throws HeadlessException
	 */
	private WAuthorsDialog(Frame owner) throws HeadlessException {
		super(owner,Messages.getString("WAuthorsDialog.Title"));
		loopType = "AUTEURS";
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#showLoopDialog(java.awt.Frame)
	 */
	public static String[] showLoopDialog(Frame owner) {
		WAuthorsDialog ad = new WAuthorsDialog(owner);
		ad.show();
		return null;
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#panelSelected()
	 */
	protected void panelSelected(String name) {
		StringBuffer loop = new StringBuffer();
		if (name.equals("pan_Results")) {
			loop.append(createStartLoop());
			loop.append(createSpecificCriterionLoop());
			loop.append(createCommonCriterionLoop());
			loop.append(createIncludedElementsLoop());
			loop.append(createLogosLoop());
			loop.append(createTagsLoop());
			loop.append(createEndLoop());
			result.setText(loop.toString());
		}			
	}

	protected String createSpecificCriterionLoop() {
		StringBuffer loop = new StringBuffer();
		if (tout.isSelected()) loop.append("{tout}");
		if (id_auteur.isSelected()) loop.append("{id_auteur}");
		if (id_article.isSelected()) loop.append("{id_article}");
	    return loop.toString();
	}
	
	protected String createLogosLoop() {
		StringBuffer loop = new StringBuffer();
	    if (LOGO_AUTEUR.isSelected()) {
	    	loop.append("[(#LOGO_AUTEUR");
	    	createLogoFilters(loop);
	    }
	    if (LOGO_AUTEUR_NORMAL.isSelected()) {
	    	loop.append("[(#LOGO_AUTEUR_NORMAL");
	    	createLogoFilters(loop);
	    }
	    if (LOGO_AUTEUR_SURVOL.isSelected()) {
	    	loop.append("[(#LOGO_AUTEUR_SURVOL");
	    	createLogoFilters(loop);
	    }
	    return loop.toString();
	}
	
	protected String createTagsLoop() {
		StringBuffer loop = new StringBuffer();
	    if (ID_AUTEUR.isSelected()) {
	    	loop.append("[(#ID_AUTEUR");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (NOM.isSelected()) {
	    	loop.append("[(#NOM");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (BIO.isSelected()) {
	    	loop.append("[(#BIO");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (EMAIL.isSelected()) loop.append("<a href=\"mailto:#EMAIL\" target=\"_self\" title=\"[(#DESCRIPTIF|textebrut|entites_html)]\">\n");
	    if (NOM.isSelected()) {
	    	loop.append("[(#NOM");
	    	if (REMOVE_NUMBER.isSelected()) loop.append("|supprimer_numero");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (EMAIL.isSelected()) loop.append("</a>\n");
	    if (URL_SITE.isSelected()) loop.append("<a href=\"#URL_SITE\" target=\"_self\" title=\"[(#DESCRIPTIF|textebrut|entites_html)]\">\n");
	    if (NOM_SITE.isSelected()) {
	    	loop.append("[(#NOM_SITE");
	    	if (REMOVE_NUMBER.isSelected()) loop.append("|supprimer_numero");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }	    
	    if (URL_SITE.isSelected()) loop.append("</a>\n");
	    
	    if (PGP.isSelected()) {
	    	loop.append("[(#PGP");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }	    
	    if (URL_AUTEUR.isSelected()) {
	    	loop.append("[(#URL_AUTEUR");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (NOTES.isSelected()) {
	    	loop.append("[(#NOTES");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }	    
	    if (FORMULAIRE_ECRIRE_AUTEUR.isSelected()) {
	    	loop.append("[(#FORMULAIRE_ECRIRE_AUTEUR)]\n");
	    }
	    return loop.toString();
	}
		
	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#helpSelected()
	 */
	protected void helpSelected() {
		JOptionPane.showMessageDialog(this,"Help");
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#cancelSelected()
	 */
	protected void cancelSelected() {
		hide();
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#enterSelected()
	 */
	protected void enterSelected() {
		StringBuffer loop = new StringBuffer();
		loop.append(createStartLoop());
		loop.append(createSpecificCriterionLoop());
		loop.append(createCommonCriterionLoop());
		loop.append(createIncludedElementsLoop());
		loop.append(createLogosLoop());
		loop.append(createTagsLoop());
		loop.append(createEndLoop());
		WEdit we = (WEdit)getParent();
		we.editor.replaceSelection(loop.toString());
		hide();
	}

	protected JComponent createSpecific() {
		JLabel l;
		JPanel panel;
		int compIndex=0;
		
		JPanel jp = (JPanel)super.createSpecific();

		jp.add(Box.createVerticalGlue(),compIndex++);
		l = new JLabel(Messages.getString("WLoopDialog.SelectionCrit"));
		l.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp.add(l,compIndex++);
		
		panel = new JPanel(new GridBagLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(5,0,0,0));
		tout = new JCheckBoxAALocalized("WLoopDialog.tout");
		id_auteur = new JCheckBoxAALocalized("WLoopDialog.id_auteur");
		id_article = new JCheckBoxAALocalized("WLoopDialog.id_article");

		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 1;
		panel.add(tout,c);
		panel.add(id_auteur,c);
		panel.add(id_article,c);
		//to fix bad starting size, not sufficient to show all component
		Dimension d = panel.getMaximumSize();
		d.height = panel.getMinimumSize().height;
		panel.setMaximumSize(d);
		
		panel.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp.add(panel,compIndex++);
		
		return jp;
	}
	
	protected  JComponent createLogos() {
		JPanel panel;
		JLabel l;
		int compIndex=0;
		
		JPanel jp = (JPanel)super.createLogos();
		
		panel = (JPanel)jp.getComponent(0);
		panel.add(Box.createVerticalStrut(10),compIndex++);
		LOGO_AUTEUR = new JCheckBoxAA("LOGO_AUTEUR");
		panel.add(LOGO_AUTEUR,compIndex++);
		panel.add(Box.createVerticalStrut(LOGO_AUTEUR.getPreferredSize().height),
				compIndex++);
		
		compIndex=0;
		panel = (JPanel)jp.getComponent(1);
		panel.add(Box.createVerticalStrut(10),compIndex++);
		LOGO_AUTEUR_NORMAL = new JCheckBoxAA("LOGO_AUTEUR_NORMAL");
		panel.add(LOGO_AUTEUR_NORMAL,compIndex++);
		LOGO_AUTEUR_SURVOL = new JCheckBoxAA("LOGO_AUTEUR_SURVOL");
		panel.add(LOGO_AUTEUR_SURVOL,compIndex++);
		
		return jp;
	}
	
	protected JComponent createTags() {
		JPanel panel;
		JLabel l;
		int compIndex = 0;

		JPanel jp = (JPanel)super.createTags();
		
		((JPanel)jp.getComponent(0)).remove(TEXT_DATE);
		
		panel = new JPanel(new GridBagLayout());
		l = new JLabel(Messages.getString("WLoopDialog.DB_Tags"));
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 1;
		c.gridwidth = 5;
		c.gridx = GridBagConstraints.RELATIVE;
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		ID_AUTEUR= new JCheckBoxAALocalized("WLoopDialog.ID_AUTEUR");
		panel.add(ID_AUTEUR,c);
		NOM = new JCheckBoxAALocalized("WLoopDialog.NOM");
		panel.add(NOM,c);
		BIO = new JCheckBoxAALocalized("WLoopDialog.BIO");
		panel.add(BIO,c);
		EMAIL = new JCheckBoxAALocalized("WLoopDialog.EMAIL");
		panel.add(EMAIL,c);
		c.gridy = 2;
		NOM_SITE = new JCheckBoxAALocalized("WLoopDialog.NOM_SITE");
		panel.add(NOM_SITE,c);
		URL_SITE = new JCheckBoxAALocalized("WLoopDialog.URL_SITE");
		panel.add(URL_SITE,c);
		PGP = new JCheckBoxAALocalized("WLoopDialog.PGP");
		panel.add(PGP,c);

		jp.add(panel,compIndex++);

		panel = new JPanel(new GridBagLayout());
		c.gridy = 0;
		c.gridwidth = 4;
		l = new JLabel(Messages.getString("WLoopDialog.Calc_Tags"));
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		URL_AUTEUR = new JCheckBoxAALocalized("WLoopDialog.URL_AUTEUR");
		panel.add(URL_AUTEUR,c);
		NOTES = new JCheckBoxAALocalized("WLoopDialog.NOTES");
		panel.add(NOTES,c);
		FORMULAIRE_ECRIRE_AUTEUR = new JCheckBoxAALocalized("WLoopDialog.FORMULAIRE_ECRIRE_AUTEUR");
		panel.add(FORMULAIRE_ECRIRE_AUTEUR,c);
		jp.add(panel,compIndex++);

		return jp;
	}	
}
