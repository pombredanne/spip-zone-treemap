/*
 * Created on 8-apr-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL		
 * 
 */
package com.spip.modelli;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JTextArea;
import javax.swing.text.Document;

/**
 * @author Renato Formato
 *
 * 
 * 
 */
public class JTextAreaAA extends JTextArea {

	public JTextAreaAA(int rows, int columns) {
		super(rows, columns);		
	}
	
	public JTextAreaAA(String text) {
		super(text);
	}
	
	public JTextAreaAA(String text, int rows, int columns) {
		super(text, rows, columns);
	}
	
	public JTextAreaAA(Document doc) {
		super(doc);
	}
	
	public JTextAreaAA(Document doc, String text, int rows, int columns) {
		super(doc, text, rows, columns);
	}
	
	public JTextAreaAA() {
		super();
	}

	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paint(java.awt.Graphics)
	 */
	public void paint(Graphics g) {
	    if (WEdit.AA) {
			Graphics2D G = (Graphics2D) g;
		    G.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
	    }
	    super.paint(g);
	}
}
