/*
 * Created on 6-apr-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL		
 * 
 */
package com.spip.modelli;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JMenuBar;

/**
 * @author Renato Formato
 *
 * 
 * 
 */
public class JMenuBarAA extends JMenuBar {

	/**
	 * 
	 */
	public JMenuBarAA() {
		super();
		if (WEdit.AA) changeFont();
	}

	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paint(java.awt.Graphics)
	 */
	public void paint(Graphics g) {
	    if (WEdit.AA) {
		    Graphics2D G = (Graphics2D) g;
		    G.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
	    }
		super.paint(g);
	}
	
	private void changeFont() {setFont(getFont().deriveFont(Font.BOLD));}
}
