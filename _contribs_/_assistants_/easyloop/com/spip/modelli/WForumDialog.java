/*
 * Created on 1-mag-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL
 * 				  
 */
package com.spip.modelli;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * @date 1-mag-2005
 * @author Renato Formato
 *
 */
public class WForumDialog extends WLoopAdapter {

	JCheckBoxAA id_forum,id_article,id_rubrique,id_breve,id_parent;
	JCheckBoxAA id_enfant,meme_parent,plat,id_secteur,id_mot;
	JCheckBoxAA titre_mot,id_groupe,type_mot;
	
	JCheckBoxAA ID_FORUM,ID_BREVE,ID_ARTICLE,ID_RUBRIQUE,DATE;
	JCheckBoxAA TITRE,TEXTE,NOM_SITE,URL_SITE,NOM,EMAIL,IP;
	JCheckBoxAA FORMULAIRE_FORUM,PARAMETRES_FORUM;
	
	private WForumDialog(Frame owner) throws HeadlessException {
		super(owner,Messages.getString("WForumDialog.Title"));
		btnLogos.setVisible(false);
		loopType = "FORUM";
	}
	
		
	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#showLoopDialog(java.awt.Frame)
	 */
	public static String[] showLoopDialog(Frame owner) {
		WForumDialog fd = new WForumDialog(owner);
		fd.show();
		return null;
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#panelSelected()
	 */
	protected void panelSelected(String name) {
		StringBuffer loop = new StringBuffer();
		if (name.equals("pan_Results")) {
			loop.append(createStartLoop());
			loop.append(createSpecificCriterionLoop());
			loop.append(createCommonCriterionLoop());
			loop.append(createIncludedElementsLoop());
			loop.append(createTagsLoop());
			loop.append(createEndLoop());
			result.setText(loop.toString());
		}					
	}

	protected String createSpecificCriterionLoop() {
		StringBuffer loop = new StringBuffer();
		if (id_forum.isSelected()) loop.append("{id_forum}");
		if (id_article.isSelected()) loop.append("{id_article}");
		if (id_rubrique.isSelected()) loop.append("{id_rubrique}");
		if (id_breve.isSelected()) loop.append("{id_breve}");
		if (id_parent.isSelected()) loop.append("{id_parent}");
		if (id_enfant.isSelected()) loop.append("{id_enfant}");
		if (meme_parent.isSelected()) loop.append("{meme_parent}");
		if (plat.isSelected()) loop.append("{plat}");
		if (id_secteur.isSelected()) loop.append("{id_secteur}");
	    if (id_mot.isSelected()) loop.append("{id_mot}");
	    if (titre_mot.isSelected()) loop.append("{titre_mot=xxx}");
	    if (id_groupe.isSelected()) loop.append("{id_groupe=zzz}");
	    if (type_mot.isSelected()) loop.append("{type_mot=yyy}");
	    return loop.toString();
	}
	
	
	protected String createTagsLoop() {
		StringBuffer loop = new StringBuffer();
	    if (ID_FORUM.isSelected()) {
	    	loop.append("[(#ID_FORUM");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (ID_BREVE.isSelected()) {
	    	loop.append("[(#ID_BREVE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (ID_ARTICLE.isSelected()) {
	    	loop.append("[(#ID_ARTICLE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (ID_RUBRIQUE.isSelected()) {
	    	loop.append("[(#ID_RUBRIQUE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (DATE.isSelected()) {
	    	loop.append("[(#DATE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (TITRE.isSelected()) {
	    	loop.append("[(#TITRE");
	    	if (REMOVE_NUMBER.isSelected()) loop.append("|supprimer_numero");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (TEXTE.isSelected()) {
	    	loop.append("[(#TEXTE");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (NOM_SITE.isSelected()) {
	    	loop.append("[(#NOM_SITE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (URL_SITE.isSelected()) {
	    	loop.append("[(#URL_SITE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (NOM.isSelected()) {
	    	loop.append("[(#NOM");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (EMAIL.isSelected()) {
	    	loop.append("[(#EMAIL");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (IP.isSelected()) {
	    	loop.append("[(#IP");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (FORMULAIRE_FORUM.isSelected()) loop.append("[(#FORMULAIRE_FORUM)]\n");
	    if (PARAMETRES_FORUM.isSelected()) loop.append("[(#PARAMETRES_FORUM)]\n");
	    return loop.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#helpSelected()
	 */
	protected void helpSelected() {
		JOptionPane.showMessageDialog(this,"Help");
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#cancelSelected()
	 */
	protected void cancelSelected() {
		hide();
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#enterSelected()
	 */
	protected void enterSelected() {
		StringBuffer loop = new StringBuffer();
		loop.append(createStartLoop());
		loop.append(createSpecificCriterionLoop());
		loop.append(createCommonCriterionLoop());
		loop.append(createIncludedElementsLoop());
		loop.append(createTagsLoop());
		loop.append(createEndLoop());
		WEdit we = (WEdit)getParent();
		we.editor.replaceSelection(loop.toString());
		hide();
	}

	protected JComponent createSpecific() {
		JLabel l;
		JPanel panel;
		int compIndex=0;
		
		JPanel jp = (JPanel)super.createSpecific();

		jp.add(Box.createVerticalGlue(),compIndex++);
		l = new JLabel(Messages.getString("WLoopDialog.SelectionCrit"));
		l.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp.add(l,compIndex++);
		
		panel = new JPanel(new GridBagLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(5,0,0,0));
		id_forum = new JCheckBoxAALocalized("WLoopDialog.id_forum");
		id_article = new JCheckBoxAALocalized("WLoopDialog.id_article");
		id_rubrique = new JCheckBoxAALocalized("WLoopDialog.id_rubrique");
		id_breve = new JCheckBoxAALocalized("WLoopDialog.id_breve");
		id_parent = new JCheckBoxAALocalized("WLoopDialog.id_parent");
		id_enfant = new JCheckBoxAALocalized("WLoopDialog.id_enfant");
		meme_parent = new JCheckBoxAALocalized("WLoopDialog.meme_parent");
		plat = new JCheckBoxAALocalized("WLoopDialog.plat");
		id_secteur = new JCheckBoxAALocalized("WLoopDialog.id_secteur");
		id_mot = new JCheckBoxAALocalized("WLoopDialog.id_mot");
		titre_mot = new JCheckBoxAALocalized("WLoopDialog.titre_mot");
		id_groupe = new JCheckBoxAALocalized("WLoopDialog.id_groupe");
		type_mot = new JCheckBoxAALocalized("WLoopDialog.type_mot");
		
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 1;
		panel.add(id_forum,c);
		panel.add(id_article,c);
		panel.add(id_rubrique,c);
		panel.add(id_breve,c);
		panel.add(id_parent,c);
		c.gridy = 1;
		panel.add(id_enfant,c);
		panel.add(meme_parent,c);
		panel.add(plat,c);
		panel.add(id_secteur,c);
		panel.add(id_mot,c);
		c.gridy = 2;
		panel.add(titre_mot,c);
		panel.add(id_groupe,c);
		panel.add(type_mot,c);
		//to fix bad starting size, not sufficient to show all component
		Dimension d = panel.getMaximumSize();
		d.height = panel.getMinimumSize().height;
		panel.setMaximumSize(d);
		
		panel.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp.add(panel,compIndex++);
		
		return jp;
	}
	
	protected JComponent createCommon() {
		JPanel jp = (JPanel)super.createCommon();
		
		JPanel panel =  (JPanel)jp.getComponent(0);
		panel.remove(6);
		panel.remove(5);
		panel.remove(4);
		
		return jp;
	}
	
	protected  JComponent createLogos() {
		return null;
	}
	
	protected JComponent createTags() {
		JPanel panel;
		JLabel l;
		int compIndex = 0;

		JPanel jp = (JPanel)super.createTags();
		
		panel = new JPanel(new GridBagLayout());
		l = new JLabel(Messages.getString("WLoopDialog.DB_Tags"));
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 1;
		c.gridwidth = 5;
		c.gridx = GridBagConstraints.RELATIVE;
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		ID_FORUM = new JCheckBoxAALocalized("WLoopDialog.ID_FORUM");
		panel.add(ID_FORUM,c);
		ID_BREVE = new JCheckBoxAALocalized("WLoopDialog.ID_BREVE");
		panel.add(ID_BREVE,c);
		ID_ARTICLE = new JCheckBoxAALocalized("WLoopDialog.ID_ARTICLE");
		panel.add(ID_ARTICLE,c);
		ID_RUBRIQUE = new JCheckBoxAALocalized("WLoopDialog.ID_RUBRIQUE");
		panel.add(ID_RUBRIQUE,c);
		DATE = new JCheckBoxAALocalized("WLoopDialog.DATE");
		panel.add(DATE,c);
		c.gridy = 2;
		TITRE = new JCheckBoxAALocalized("WLoopDialog.TITRE");
		panel.add(TITRE,c);
		TEXTE = new JCheckBoxAALocalized("WLoopDialog.TEXTE");
		panel.add(TEXTE,c);
		NOM_SITE = new JCheckBoxAALocalized("WLoopDialog.NOM_SITE");
		panel.add(NOM_SITE,c);
		URL_SITE = new JCheckBoxAALocalized("WLoopDialog.URL_SITE");
		panel.add(URL_SITE,c);
		NOM = new JCheckBoxAALocalized("WLoopDialog.NOM");
		panel.add(NOM,c);
		c.gridy = 3;
		EMAIL = new JCheckBoxAALocalized("WLoopDialog.EMAIL");
		panel.add(EMAIL,c);
		IP = new JCheckBoxAALocalized("WLoopDialog.IP");
		panel.add(IP,c);
		jp.add(panel,compIndex++);

		panel = new JPanel(new GridBagLayout());
		c.gridy = 0;
		c.gridwidth = 4;
		l = new JLabel(Messages.getString("WLoopDialog.Calc_Tags"));
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		FORMULAIRE_FORUM = new JCheckBoxAALocalized("WLoopDialog.FORMULAIRE_FORUM");
		panel.add(FORMULAIRE_FORUM,c);
		PARAMETRES_FORUM = new JCheckBoxAALocalized("WLoopDialog.PARAMETRES_FORUM");
		panel.add(PARAMETRES_FORUM,c);
		jp.add(panel,compIndex++);

		return jp;
	}
}
