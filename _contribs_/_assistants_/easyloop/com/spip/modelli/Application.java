/*
 * Created on 4-mag-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL				  
 *
 * */
package com.spip.modelli;

import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JWindow;
import javax.swing.UIManager;

/**
 * @date 4-mag-2005
 * @author Renato Formato
 *
 */
public class Application  {

	public static Object[] Languages;
	public static Application app;
	private Vector mainWindows; 
	private WEdit Main;
	private static JWindow jw;
	
    public static void main(String[] args) {
	    //System properties for Mac OS
	    //Put menu on the top of the screen
	    System.setProperty("apple.laf.useScreenMenuBar","true"); 
	    //Change the name of the app at the top of the screen
	    //??? How do it in the code  by argument to the VM -Xdock:name of app
	    //Create the about menu
	    System.setProperty("com.apple.mrj.application.apple.menu.about.name",Messages.getString("MainWindow.Title"));
	    //Get translated languages
	    Languages = Messages.getLocales();
		//Create splash screen
	    Runnable gui = new Runnable() {
			public void run() {
			    jw = new JSplash();
			    jw.show();		    
			}
		};
		try {EventQueue.invokeAndWait(gui);} catch(Exception e) {}
		//wait until is it visible
		//create the application
	    gui = new Runnable() {
			public void run() {
				app = new Application();
				//Wait for splash window hides 
				while (jw.isVisible()) {};
				//Show main window
				app.getMainWindow().show();		
			}
		};
		EventQueue.invokeLater(gui);
    }
 
    	public Application() {
    	    try {
    	        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    	      }
    	      catch(Exception e) {
    	        e.printStackTrace();
    	      }
    	    //Preload slow loading classes
    	    JLabel l = new JLabel("<html></html>");
    	    JFileChooser jfc = new JFileChooser();
    	    
    	    //Create the vector in which to store the window
    	    mainWindows = new Vector();
    	    //Create main window
    	    Main = createMainWindow();
    		Main.setLocationRelativeTo(null);
    	}
    
    	public WEdit createMainWindow() {
    		WEdit wd = new WEdit();
    		wd.pack();
    		wd.setSize(600,400);
    		mainWindows.add(wd);
    		wd.addWindowListener(new WindowAdapter() {

    			public void windowClosed(WindowEvent e) {
    				mainWindows.remove(e.getSource());
    				if (mainWindows.size()==0) System.exit(0);
    			}
    		});
    		return wd;
    	}
    	
    	public WEdit getMainWindow() {
    		return Main;
    	}
}
