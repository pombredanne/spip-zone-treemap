/*
 * Created on 8-apr-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL		
 * 
 */
package com.spip.modelli;

import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JComponent;

import com.sun.java.swing.plaf.windows.WindowsMenuBarUI;

/**
 * @author Renato Formato
 *
 * 
 * 
 */
public class MyMenuBarUI extends WindowsMenuBarUI {

	/**
	 * 
	 */
	public MyMenuBarUI() {
		super();
	}

	/* (non-Javadoc)
	 * @see javax.swing.plaf.ComponentUI#paint(java.awt.Graphics, javax.swing.JComponent)
	 */
	public void paint(Graphics g, JComponent c) {
	    if (WEdit.AA) g.setFont(g.getFont().deriveFont(Font.BOLD));
		super.paint(g, c);
	}
	
}
