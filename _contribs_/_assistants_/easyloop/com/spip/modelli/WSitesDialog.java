package com.spip.modelli;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
/*
 * Created on 2-mag-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL
 *
 * */

/**
 * @date 2-mag-2005
 * @author Renato Formato
 *
 */
public class WSitesDialog extends WLoopAdapter {

	JCheckBoxAA tout,id_syndic,id_rubrique,id_secteur;
	JCheckBoxAA id_mot,titre_mot,type_mot,id_groupe;

	JCheckBoxAA LOGO_SITE,URL_SYNDIC,LOGO_SITE_NORMAL,LOGO_SITE_SURVOL;
	
	JCheckBoxAA ID_SECTEUR,ID_RUBRIQUE,ID_SYNDIC;
	JCheckBoxAA NOM_SITE,URL_SITE,DESCRIPTIF,TEXTE;
	JCheckBoxAA FORMULAIRE_SITE;
	
	/**
	 * @param owner The main windows
	 * @throws HeadlessException
	 */
	private WSitesDialog(Frame owner) throws HeadlessException {
		super(owner,Messages.getString("WSitesDialog.Title"));
		loopType = "SITES";
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#showLoopDialog(java.awt.Frame)
	 */
	public static String[] showLoopDialog(Frame owner) {
		WSitesDialog sd = new WSitesDialog(owner);
		sd.show();
		return null;
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#panelSelected()
	 */
	protected void panelSelected(String name) {
		StringBuffer loop = new StringBuffer();
		if (name.equals("pan_Results")) {
			loop.append(createStartLoop());
			loop.append(createSpecificCriterionLoop());
			loop.append(createCommonCriterionLoop());
			loop.append(createIncludedElementsLoop());
			loop.append(createLogosLoop());
			loop.append(createTagsLoop());
			loop.append(createEndLoop());
			result.setText(loop.toString());
		}			
	}

	protected String createSpecificCriterionLoop() {
		StringBuffer loop = new StringBuffer();
	    if (id_rubrique.isSelected()) loop.append("{id_rubrique}");
	    if (id_secteur.isSelected()) loop.append("{id_secteur}");
	    if (id_syndic.isSelected()) loop.append("{id_syndic}");
	    if (tout.isSelected()) loop.append("{tout}");
	    if (id_mot.isSelected()) loop.append("{id_mot}");
	    if (titre_mot.isSelected()) loop.append("{titre_mot=xxx}");
	    if (id_groupe.isSelected()) loop.append("{id_groupe=zzz}");
	    if (type_mot.isSelected()) loop.append("{type_mot=yyy}");
	    return loop.toString();
	}
	
	protected String createLogosLoop() {
		StringBuffer loop = new StringBuffer();
	    if (LOGO_SITE.isSelected()) {
	    	loop.append("[(#LOGO_SITE");
	    	createLogoFilters(loop);
	    }
	    if (LOGO_SITE_NORMAL.isSelected()) {
	    	loop.append("[(#LOGO_SITE_NORMAL");
	    	createLogoFilters(loop);
	    }
	    if (LOGO_SITE_SURVOL.isSelected()) {
	    	loop.append("[(#LOGO_SITE_SURVOL");
	    	createLogoFilters(loop);
	    }
	    if (URL_SYNDIC.isSelected()) {
	    	loop.append("[(#URL_SYNDIC");
	    	if (br.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    return loop.toString();
	}
	
	protected String createTagsLoop() {
		StringBuffer loop = new StringBuffer();
	    if (ID_SECTEUR.isSelected()) {
	    	loop.append("[(#ID_SECTEUR");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (ID_RUBRIQUE.isSelected()) {
	    	loop.append("[(#ID_RUBRIQUE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (ID_SYNDIC.isSelected()) {
	    	loop.append("[(#ID_SYNDIC");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (URL_SITE.isSelected()) loop.append("<a href=\"#URL_SITE\" target=\"_self\" title=\"[(#DESCRIPTIF|textebrut|entites_html)]\">\n");
	    if (NOM_SITE.isSelected()) {
	    	loop.append("[(#NOM_SITE");
	    	if (REMOVE_NUMBER.isSelected()) loop.append("|supprimer_numero");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (URL_SITE.isSelected()) loop.append("</a>\n");
	    if (DESCRIPTIF.isSelected()) {
	    	loop.append("[(#DESCRIPTIF");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }	    
	    if (TEXTE.isSelected()) {
	    	loop.append("[(#TEXTE");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (FORMULAIRE_SITE.isSelected()) loop.append("[(#FORMULAIRE_SITE)]\n");
		return loop.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#helpSelected()
	 */
	protected void helpSelected() {
		JOptionPane.showMessageDialog(this,"Help");
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#cancelSelected()
	 */
	protected void cancelSelected() {
		hide();
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#enterSelected()
	 */
	protected void enterSelected() {
		StringBuffer loop = new StringBuffer();
		loop.append(createStartLoop());
		loop.append(createSpecificCriterionLoop());
		loop.append(createCommonCriterionLoop());
		loop.append(createIncludedElementsLoop());
		loop.append(createLogosLoop());
		loop.append(createTagsLoop());
		loop.append(createEndLoop());
		WEdit we = (WEdit)getParent();
		we.editor.replaceSelection(loop.toString());
		hide();
	}
	
	protected JComponent createSpecific() {
		JLabel l;
		JPanel panel;
		int compIndex=0;
		
		JPanel jp = (JPanel)super.createSpecific();

		((JPanel)jp.getComponent(2)).remove(exclus);
		
		jp.add(Box.createVerticalGlue(),compIndex++);
		l = new JLabel(Messages.getString("WLoopDialog.SelectionCrit"));
		l.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp.add(l,compIndex++);
		
		panel = new JPanel(new GridBagLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(5,0,0,0));
		tout = new JCheckBoxAALocalized("WLoopDialog.tout");
		id_syndic =  new JCheckBoxAALocalized("WLoopDialog.id_syndic");
		id_rubrique = new JCheckBoxAALocalized("WLoopDialog.id_rubrique");
		id_secteur =  new JCheckBoxAALocalized("WLoopDialog.id_secteur");
		id_mot =  new JCheckBoxAALocalized("WLoopDialog.id_mot");
		titre_mot =  new JCheckBoxAALocalized("WLoopDialog.titre_mot");
		type_mot =  new JCheckBoxAALocalized("WLoopDialog.type_mot");
		id_groupe =  new JCheckBoxAALocalized("WLoopDialog.id_groupe");
				
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx=1;
		panel.add(tout,c);
		panel.add(id_syndic,c);
		panel.add(id_rubrique,c);
		panel.add(id_secteur,c);
		c.gridy = 1;
		panel.add(id_mot,c);
		panel.add(titre_mot,c);
		panel.add(type_mot,c);
		panel.add(id_groupe,c);
		
		//to fix bad starting size, not sufficient to show all component
		//panel.setMaximumSize(panel.getMinimumSize());
		Dimension d = panel.getMaximumSize();
		d.height = panel.getMinimumSize().height;
		panel.setMaximumSize(d);

		panel.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp.add(panel,compIndex++);
		
		return jp;
	}

	protected  JComponent createLogos() {
		JPanel panel;
		JLabel l;
		int compIndex=0;
		
		JPanel jp = (JPanel)super.createLogos();
		
		panel = (JPanel)jp.getComponent(0);
		panel.add(Box.createVerticalStrut(10),compIndex++);
		LOGO_SITE = new JCheckBoxAA("LOGO_SITE");
		panel.add(LOGO_SITE,compIndex++);
		URL_SYNDIC = new JCheckBoxAALocalized("WLoopDialog.URL_SYNDIC");
		panel.add(URL_SYNDIC,compIndex++);
		
		compIndex=0;
		panel = (JPanel)jp.getComponent(1);
		panel.add(Box.createVerticalStrut(10),compIndex++);
		LOGO_SITE_NORMAL = new JCheckBoxAA("LOGO_SITE_NORMAL");
		panel.add(LOGO_SITE_NORMAL,compIndex++);
		LOGO_SITE_SURVOL = 
			new JCheckBoxAA("LOGO_SITE_SURVOL");
		panel.add(LOGO_SITE_SURVOL,compIndex++);
		
		return jp;
	}
	
	protected JComponent createTags() {
		JPanel panel;
		JLabel l;
		int compIndex = 0;

		JPanel jp = (JPanel)super.createTags();
		
		((JPanel)jp.getComponent(0)).remove(TEXT_DATE);
		
		panel = new JPanel(new GridBagLayout());
		l = new JLabel(Messages.getString("WLoopDialog.DB_Tags"));
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 1;
		c.gridwidth = 5;
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		ID_SECTEUR = new JCheckBoxAALocalized("WLoopDialog.ID_SECTEUR");
		panel.add(ID_SECTEUR,c);
		ID_RUBRIQUE = new JCheckBoxAALocalized("WLoopDialog.ID_RUBRIQUE");
		panel.add(ID_RUBRIQUE,c);
		ID_SYNDIC = new JCheckBoxAALocalized("WLoopDialog.ID_SYNDIC");
		panel.add(ID_SYNDIC,c);
		jp.add(panel,compIndex++);
		c.gridy = 2;
		NOM_SITE = new JCheckBoxAALocalized("WLoopDialog.NOM_SITE");
		panel.add(NOM_SITE,c);
		URL_SITE = new JCheckBoxAALocalized("WLoopDialog.URL_SITE");
		panel.add(URL_SITE,c);
		DESCRIPTIF = new JCheckBoxAALocalized("WLoopDialog.DESCRIPTIF");
		panel.add(DESCRIPTIF,c);
		TEXTE = new JCheckBoxAALocalized("WLoopDialog.TEXTE");
		panel.add(TEXTE,c);
		
		
		panel = new JPanel(new GridBagLayout());
		c.gridy=0;
		c.gridwidth = 4;
		l = new JLabel(Messages.getString("WLoopDialog.Calc_Tags"));
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		FORMULAIRE_SITE = new JCheckBoxAALocalized("WLoopDialog.FORMULAIRE_SITE");
		panel.add(FORMULAIRE_SITE,c);
		jp.add(panel,compIndex++);

		return jp;
	}		
}
