/*
 * Created on 12-apr-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL		
 * 
 */
package com.spip.modelli;

import java.awt.HeadlessException;
import java.text.NumberFormat;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

/**
 * @author Renato Formato
 *
 * 
 * 
 */
public class WNumDialog extends JFrame {
	
	JTextAreaAA ed;
	/**
	 * @throws java.awt.HeadlessException
	 */
	public WNumDialog(String txt,JFrame owner) throws HeadlessException {
		super(Messages.getString("WNumDialog.Title")); //$NON-NLS-1$
		addGUI();
		setText(txt,owner);
	}

	private void addGUI() {
		ed = new JTextAreaAA();
		getContentPane().add(new JScrollPane(ed));
		setSize(600,400);
	}
	
	private void setText(String txt,JFrame owner) {
		StringBuffer sb = new StringBuffer();
		String[] linee = txt.split("\n"); //$NON-NLS-1$
		NumberFormat f = NumberFormat.getInstance();
		f.setMinimumIntegerDigits(6);
		f.setMaximumIntegerDigits(6);
		f.setGroupingUsed(false);
		for (int i = 0; i < linee.length; i++) {
			sb.append(f.format(i) + "--" + linee[i] + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		ed.setFont(((WEdit)owner).editor.getFont());
		ed.setText(sb.toString());
	}
	
}
