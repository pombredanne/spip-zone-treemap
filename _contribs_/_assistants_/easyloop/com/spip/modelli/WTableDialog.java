/*
 * Created on 7-mag-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL
 */
package com.spip.modelli;

import java.awt.Color;
import java.awt.Container;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * @date 7-mag-2005
 * @author Renato Formato
 *
 */
public class WTableDialog extends JDialog implements ActionListener {
	
	public static String[] showTableDialog(Frame owner) {
		WTableDialog td = new WTableDialog(owner);
		td.show();
		return null;
	}

	private JTextField numColumns,numRows,border,innerMargin,width,padding;
	private JComboBoxAA alignment;
	private JCheckBoxAA title,shortText;
	private JTextField columnwidth1,columnwidth2,columnwidth3,columnwidth4;
	private JButtonAALocalized cancel,insert;
	
	/**
	 * @param owner
	 * @throws java.awt.HeadlessException
	 */
	protected WTableDialog(Frame owner) throws HeadlessException {
		super(owner, Messages.getString("WTableDialog.Title"), true);
		addGUI();
	}

	private void addGUI() {
		JPanel panel,panel2,panel3;
		JLabel label;
		Container cp = getContentPane();
		cp.setLayout(new BoxLayout(cp,BoxLayout.PAGE_AXIS));
		
		panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(5,10,5,10));
		panel.setLayout(new BoxLayout(panel,BoxLayout.LINE_AXIS));
		
		panel2 = new JPanel(new GridBagLayout());
		panel2.setBorder(BorderFactory.createEmptyBorder(0,0,0,10));
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = GridBagConstraints.RELATIVE;
		c.anchor = GridBagConstraints.LINE_END;
		c.insets = new Insets(2,2,2,2);
		label = new JLabel(Messages.getString("WTableDialog.NumColumns"));
		panel2.add(label,c);
		label = new JLabel(Messages.getString("WTableDialog.NumRows"));
		panel2.add(label,c);
		label = new JLabel(Messages.getString("WTableDialog.Border"));
		label.setForeground(Color.RED);
		panel2.add(label,c);
		label = new JLabel(Messages.getString("WTableDialog.InnerMargin"));
		label.setForeground(Color.RED);
		panel2.add(label,c);
		label = new JLabel(Messages.getString("WTableDialog.Alignment"));
		label.setForeground(Color.RED);
		panel2.add(label,c);
		c.gridx = 1;
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		numColumns = new JTextField("2",5);
		panel2.add(numColumns,c);
		numRows = new JTextField("2");
		panel2.add(numRows,c);
		border = new JTextField();
		panel2.add(border,c);		
		innerMargin = new JTextField();
		panel2.add(innerMargin,c);
		String[] list = {"",Messages.getString("WTableDialog.AlignmentLeft"),
				Messages.getString("WTableDialog.AlignmentCenter"),
				Messages.getString("WTableDialog.AlignmentRight")
		} ;
		alignment = new JComboBoxAA(list);
		panel2.add(alignment,c);
		
		panel.add(panel2);
		
		panel2 = new JPanel();
		panel2.setLayout(new BoxLayout(panel2,BoxLayout.PAGE_AXIS));
		
		c.weightx = 0;
		c.gridx = GridBagConstraints.RELATIVE;
		
		panel3 = new JPanel();
		panel3.setLayout(new GridBagLayout());
		
		label = new JLabel(Messages.getString("WTableDialog.Width"));
		label.setForeground(Color.RED);
		label.setHorizontalAlignment(SwingConstants.TRAILING);
		panel3.add(label,c);
		width = new JTextField(5);
		c.weightx=1;
		panel3.add(width,c);
		c.gridy=1;
		c.gridwidth=2;
		title = new JCheckBoxAALocalized("WTableDialog.WithTitle",true);
		panel3.add(title,c);
		c.gridy++;
		c.gridwidth=1;
		label = new JLabel(Messages.getString("WTableDialog.Padding"));
		label.setHorizontalAlignment(SwingConstants.TRAILING);
		c.weightx=0;
		panel3.add(label,c);
		padding = new JTextField("1");
		c.weightx=1;
		panel3.add(padding,c);
		shortText = new JCheckBoxAALocalized("WTableDialog.ShortText");
		c.gridy++;
		c.gridwidth=2;
		panel3.add(shortText,c);
		panel2.add(panel3);
		
		panel.add(panel2);
		
		cp.add(panel);
		
		panel = new JPanel(new GridBagLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(5,10,5,10));
		label = new JLabel(Messages.getString("WTableDialog.ColumnsWidth"));
		label.setForeground(Color.RED);
		panel.add(label,c);
		columnwidth1 = new JTextField(5);
		panel.add(columnwidth1,c);
		columnwidth2 = new JTextField(5);
		panel.add(columnwidth2,c);
		columnwidth3 = new JTextField(5);
		panel.add(columnwidth3,c);
		columnwidth4 = new JTextField(5);
		panel.add(columnwidth4,c);
		
		cp.add(panel);
		
		label = new JLabel(Messages.getString("WTableDialog.TableWarning"));
		label.setForeground(Color.RED);
		label.setAlignmentX(0.5f);
		cp.add(label);
		
		panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(5,10,5,10));
		panel.setLayout(new BoxLayout(panel,BoxLayout.LINE_AXIS));
		cancel = new JButtonAALocalized("Dialogs.Cancel");
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				hide();	
			}
		});
		panel.add(cancel);
		panel.add(Box.createHorizontalGlue());
		insert = new JButtonAALocalized("WTableDialog.InsertTable");
		insert.addActionListener(this);
		panel.add(insert);
		
		cp.add(panel);
		
		pack();
		setLocationRelativeTo(getParent());
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		int imax,jmax,i,j;
		StringBuffer table = new StringBuffer();
		String typecell;
		imax = new Integer(numRows.getText()).intValue();
		jmax = new Integer(numColumns.getText()).intValue();
		if (imax>0 && jmax>0) {
			table.append("<table");
			if (!width.getText().equals("")) table.append(" width=\"" + width.getText() + "\"");
			if (!border.getText().equals("")) table.append(" border=\"" + border.getText() + "\"");
			if (!innerMargin.getText().equals("")) table.append(" cellspacing=\"" + innerMargin.getText() + "\"");
			if (!padding.getText().equals("")) table.append(" cellpadding=\"" + padding.getText() + "\"");
			if (alignment.getSelectedIndex()>0) {
				table.append(" align=\"");
				switch(alignment.getSelectedIndex()) {
					case 1: table.append("left");break;
					case 2: table.append("center");break;
					case 3: table.append("right");
				}
				table.append("\"");
			}
			table.append(" >\n\n");
			for(i=1;i<=imax;i++) {
				typecell = (i==1)?"th":"td";
				table.append("\t<tr>\n");
				for(j=1;j<=jmax;j++) {
					table.append("\t\t<" + typecell);
					if (i==1) {
						switch(j) {
							case 1: 
								if (!columnwidth1.getText().equals("")) 
									table.append(" width=\"" + columnwidth1.getText() +"\"");
								break;
							case 2:
								if (!columnwidth2.getText().equals("")) 
									table.append(" width=\"" + columnwidth2.getText() +"\"");
								break;
							case 3:
								if (!columnwidth3.getText().equals("")) 
									table.append(" width=\"" + columnwidth3.getText() +"\"");
								break;
							case 4:		
								if (!columnwidth4.getText().equals("")) 
									table.append(" width=\"" + columnwidth4.getText() +"\"");
								break;
						}
					}
					table.append(">");
					if (!shortText.isSelected()) table.append("\n\n\t\t");
					table.append("</" + typecell + ">\n");
				}
				table.append("\t</tr>\n\n");
			}
			table.append("</table>\n");
		}
		WEdit we = (WEdit)getParent();
		we.editor.replaceSelection(table.toString());
		hide();
	}
}
