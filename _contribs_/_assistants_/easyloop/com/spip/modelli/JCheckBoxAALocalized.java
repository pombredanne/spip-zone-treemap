/*
 * Created on 28-apr-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL
 * 
 */
package com.spip.modelli;

import javax.swing.Icon;

/**
 * @date 28-apr-2005
 * @author Renato Formato
 *
 * 
 * 
 */
public class JCheckBoxAALocalized extends JCheckBoxAA implements LocaleListener {

	private String msg_ID;
	
	/**
	 * @param id
	 */
	public JCheckBoxAALocalized(String id) {
		super(Messages.getString(id));
		msg_ID = id;
	}

	/**
	 * @param id
	 * @param selected
	 */
	public JCheckBoxAALocalized(String id, boolean selected) {
		super(Messages.getString(id), selected);
		msg_ID = id;
	}

	/**
	 * @param id
	 * @param icon
	 */
	public JCheckBoxAALocalized(String id, Icon icon) {
		super(Messages.getString(id), icon);
		msg_ID = id;
	}

	/**
	 * @param id
	 * @param icon
	 * @param selected
	 */
	public JCheckBoxAALocalized(String id, Icon icon, boolean selected) {
		super(Messages.getString(id), icon, selected);
		msg_ID = id;
	}
	
	public void changedLocale() {
		setText(Messages.getString(msg_ID));
	}

}
