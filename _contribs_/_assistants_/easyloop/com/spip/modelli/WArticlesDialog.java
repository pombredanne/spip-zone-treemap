/*
 * Created on 1-mag-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL
 * 				  
 */
package com.spip.modelli;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * @date 1-mag-2005
 * @author Renato Formato
 *
 */
public class WArticlesDialog extends WLoopAdapter {

	JCheckBoxAA id_article,id_rubrique,id_sector,branche,id_auteur,recerche;
	JCheckBoxAA id_mot,titre_mot,type_mot,tout,lang,date,id_groupe;
	
	JCheckBoxAA logo_article,logo_article_rub,logo_article_norm,logo_article_roll;
	
	JCheckBoxAA ID_ARTICLE,TOP_TITLE,TITLE,SUBTITLE,DESCRIPTION;
	JCheckBoxAA DECK,TEXT,PS,ID_RUBRIQUE,ID_SECTOR;
	JCheckBoxAA SITE_NAME,SITE_URL,VISITS,POPULARITY,DATE;
	
	JCheckBoxAA ARTICLE_URL,INTRODUCTION,FOOT_NOTES,AUTHORS;
	JCheckBoxAA FORUM_FORM,FORUM_PARAMETERS,SIGNATURE_FORM;
	
	//JCheckBoxAA CR2,REMOVE_NUMBER,TEXT_DATE,JUSTIFY;
	
	private WArticlesDialog(Frame owner) throws HeadlessException {
		super(owner,Messages.getString("WArticlesDialog.Title"));
		loopType = "ARTICLES";
	}
	
		
	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#showLoopDialog(java.awt.Frame)
	 */
	public static String[] showLoopDialog(Frame owner) {
		WArticlesDialog ad = new WArticlesDialog(owner);
		ad.show();
		return null;
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#panelSelected()
	 */
	protected void panelSelected(String name) {
		StringBuffer loop = new StringBuffer();
		if (name.equals("pan_Results")) {
			loop.append(createStartLoop());
			loop.append(createSpecificCriterionLoop());
			loop.append(createCommonCriterionLoop());
			loop.append(createIncludedElementsLoop());
			loop.append(createLogosLoop());
			loop.append(createTagsLoop());
			loop.append(createEndLoop());
			result.setText(loop.toString());
		}		
	}

	protected String createSpecificCriterionLoop() {
		StringBuffer loop = new StringBuffer();
		/*
		    // CRITERES SPECIFIQUES ///////////////////////////////////////////
    
    If id_article.Value then
      LeText = LeText + "{"+ id_article.Caption +"}"
    End
    
    If id_rubrique22.Value then
      LeText = LeText + "{"+ id_rubrique22.Caption +"}"
    End
    
    If id_secteur22.Value then
      LeText = LeText + "{"+ id_secteur22.Caption +"}"
    End
    
    If branche.Value then
      LeText = LeText + "{"+ branche.Caption +"}"
    End
    
    If id_auteur.Value then
      LeText = LeText + "{"+ id_auteur.Caption +"}"
    End
    
    
    If recherche.Value then
      LeText = LeText + "{"+ recherche.Caption +"}"
    End
    
    
    If id_mot22.Value then
      LeText = LeText + "{"+ id_mot22.Caption +"}"
    End
    
    
    If titre_mot22.Value then
      LeText = LeText + "{"+ titre_mot22.Caption +"}"
    End
    
    If type_mot.Value then
      LeText = LeText + "{"+ type_mot.Caption +"}"
    End
    
    
    If tout.Value then
      LeText = LeText + "{"+ tout.Caption +"}"
    End
    
    
    If lang.Value then
      LeText = LeText + "{"+ lang.Caption +"}"
    End
    
    
    If date23.Value then
      LeText = LeText + "{"+ date23.Caption +"}"
    End
    
    
    If id_groupe.Value then
      LeText = LeText + "{"+ id_groupe.Caption +"}"
    End
    
    
    
    
    
    
    
    
    
    // CRITERES COMMUNS ///////////////////////////////////////////
    
    
    
    If num_titre.Value then
      LeText = LeText + "{"+ num_titre.Caption +"}"
    End
    
    If par_titre.Value then
      LeText = LeText + "{"+ par_titre.Caption +"}"
    End
    
    If par_date22.Value then
      LeText = LeText + "{"+ par_date22.Caption +"}"
    End
    
    If id_article_X.Value then
      LeText = LeText + "{"+ id_article_X.Caption +"}"
    End
    
    If titre___aA.Value then
      LeText = LeText + "{"+ titre___aA.Caption +"}"
    End
    
    If inverse.Value then
      LeText = LeText + "{"+ inverse.Caption +"}"
    End
    
    If hasard.Value then
      LeText = LeText + "{"+ hasard.Caption +"}"
    End
    
    If doublons.Value then
      LeText = LeText + "{"+ doublons.Caption +"}"
    End
    
    If exclus.Value then
      LeText = LeText + "{"+ exclus.Caption +"}"
    End
    
    If unique.Value then
      LeText = LeText + "{"+ unique.Caption +"}"
    End
    
    If ChAffichage.Text <> "" then
      LeText = LeText + "{"+ChAffichage.Text +"}"
    End
    
    If ChMultiCol.Text <> "" then
      LeText = LeText + "{"+ ChMultiCol.Text +"}"
    End
    
    If ChSeparateur.Text <> "" then
      LeText = LeText + "{" + """" + ChSeparateur.Text + """" + "}"
    End
    
    LeText = LeText + ">" + CR
    
		 */
		if (id_article.isSelected()) loop.append("{id_article}");
	    if (id_rubrique.isSelected()) loop.append("{id_rubrique}");
	    if (id_sector.isSelected()) loop.append("{id_secteur}");
	    if (branche.isSelected()) loop.append("{branche}");
	    if (id_auteur.isSelected()) loop.append("{id_auteur}");
	    if (recerche.isSelected()) loop.append("{recerche}");
	    if (id_mot.isSelected()) loop.append("{id_mot}");
	    if (titre_mot.isSelected()) loop.append("{titre_mot=xxx}");
	    if (tout.isSelected()) loop.append("{tout}");
	    if (id_groupe.isSelected()) loop.append("{id_groupe=zzz}");
	    if (type_mot.isSelected()) loop.append("{type_mot=yyy}");
	    if (lang.isSelected()) loop.append("{lang}");
	    if (date.isSelected()) loop.append("{date}");
		return loop.toString();
	}

	protected String createLogosLoop() {
		StringBuffer loop = new StringBuffer();
		/*
    // CONTENU LOGOS ///////////////////////////////////////////
    
    If LOGO_ARTICLE22.Value then
      LeText = LeText + "                      [(#"+ LOGO_ARTICLE22.Caption +""
      if gauche.Value then
        LeText = LeText + "|"+gauche.Caption +""
      End
      if centre.Value then
        LeText = LeText + "|"+centre.Caption +""
      End
      if droite.Value then
        LeText = LeText + "|"+droite.Caption +""
      End
      If brlogo.Value then
        LeText = LeText + ")"+brlogo.Caption +"]" + CR
      Else
        LeText = LeText + ")]" + CR
      End
    End
    
    
    
    If LOGO_ARTICLE_RUBRIQUE.Value then
      LeText = LeText + "                      [(#"+ LOGO_ARTICLE_RUBRIQUE.Caption +""
      if gauche.Value then
        LeText = LeText + "|"+gauche.Caption +""
      End
      if centre.Value then
        LeText = LeText + "|"+centre.Caption +""
      End
      if droite.Value then
        LeText = LeText + "|"+droite.Caption +""
      End
      If brlogo.Value then
        LeText = LeText + ")"+brlogo.Caption +"]" + CR
      Else
        LeText = LeText + ")]" + CR
      End
    End
    
    
    
    If LOGO_ARTICLE_NORMAL.Value then
      LeText = LeText + "                      [(#"+ LOGO_ARTICLE_NORMAL.Caption +""
      if gauche.Value then
        LeText = LeText + "|"+gauche.Caption +""
      End
      if centre.Value then
        LeText = LeText + "|"+centre.Caption +""
      End
      if droite.Value then
        LeText = LeText + "|"+droite.Caption +""
      End
      If brlogo.Value then
        LeText = LeText + ")"+brlogo.Caption +"]" + CR
      Else
        LeText = LeText + ")]" + CR
      End
    End
    
    
    If LOGO_ARTICLE_SURVOL.Value then
      LeText = LeText + "                      [(#"+ LOGO_ARTICLE_SURVOL.Caption +""
      if gauche.Value then
        LeText = LeText + "|"+gauche.Caption +""
      End
      if centre.Value then
        LeText = LeText + "|"+centre.Caption +""
      End
      if droite.Value then
        LeText = LeText + "|"+droite.Caption +""
      End
      If brlogo.Value then
        LeText = LeText + ")"+brlogo.Caption +"]" + CR
      Else
        LeText = LeText + ")]" + CR
      End
    End
 
		 */
	    if (logo_article.isSelected()) {
	    	loop.append("[(#LOGO_ARTICLE");
	    	createLogoFilters(loop);
	    }
	    if (logo_article_rub.isSelected()) {
	    	loop.append("[(#LOGO_ARTICLE_RUBRIQUE");
	    	createLogoFilters(loop);
	    }
	    if (logo_article_norm.isSelected()) {
	    	loop.append("[(#LOGO_ARTICLE_NORMAL");
	    	createLogoFilters(loop);
	    }
	    if (logo_article_roll.isSelected()) {
	    	loop.append("[(#LOGO_ARTICLE_SURVOL");
	    	createLogoFilters(loop);
	    }
	    return loop.toString();
	}
	
	protected String createTagsLoop() {
		StringBuffer loop = new StringBuffer();
		if (ID_SECTOR.isSelected()) {
	    	loop.append("[(#ID_SECTEUR");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (ID_RUBRIQUE.isSelected()) {
	    	loop.append("[(#ID_RUBRIQUE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (ID_ARTICLE.isSelected()) {
	    	loop.append("[(#ID_ARTICLE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (DATE.isSelected()) {
	    	loop.append("[(#DATE");
	    	if (TEXT_DATE.isSelected()) loop.append("|affdate");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
		if (AUTHORS.isSelected()) loop.append("[(#LES_AUTEURS)<br />]\n");
		if (TOP_TITLE.isSelected()) {
	    	loop.append("[(#SURTITRE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (ARTICLE_URL.isSelected()) loop.append("<a href=\"#URL_ARTICLE\" target=\"_self\" title=\"[(#DESCRIPTIF|textebrut|entites_html)]\">\n");
	    if (TITLE.isSelected()) {
	    	loop.append("[(#TITRE");
	    	if (REMOVE_NUMBER.isSelected()) loop.append("|supprimer_numero");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (ARTICLE_URL.isSelected()) loop.append("</a>\n");
	    if (SUBTITLE.isSelected()) {
	    	loop.append("[(#SOUSTITRE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (INTRODUCTION.isSelected()) {
	    	loop.append("[(#INTRODUCTION");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (DESCRIPTION.isSelected()) {
	    	loop.append("[(#DESCRIPTIF");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (DECK.isSelected()) {
	    	loop.append("[(#CHAPO");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }	    
	    if (SITE_URL.isSelected()) loop.append("<a href=\"#URL_SITE\" target=\"_self\" title=\"[(#DESCRIPTIF|textebrut|entites_html)]\">\n");
	    if (SITE_NAME.isSelected()) {
	    	loop.append("[(#NOM_SITE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (SITE_URL.isSelected()) loop.append("</a>\n");
	    if (TEXT.isSelected()) {
	    	loop.append("[(#TEXTE");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (PS.isSelected()) {
	    	loop.append("[(#PS");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (FOOT_NOTES.isSelected()) {
	    	loop.append("[(#NOTES");
	    	if (JUSTIFY.isSelected()) loop.append("|justifier"); 
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (VISITS.isSelected()) {
	    	loop.append("[(#VISITES");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (POPULARITY.isSelected()) {
	    	loop.append("[(#POPULARITE");
	    	if (CR2.isSelected()) loop.append(")<br />]\n"); else loop.append(")]\n");
	    }
	    if (FORUM_FORM.isSelected()) loop.append("[(#FORMULAIRE_FORUM)]\n");
	    if (FORUM_PARAMETERS.isSelected()) loop.append("[(#PARAMETRES_FORUM)]\n");
	    if (SIGNATURE_FORM.isSelected()) loop.append("[(#FORMULAIRE_SIGNATURE)]\n");
		return loop.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#helpSelected()
	 */
	protected void helpSelected() {
		JOptionPane.showMessageDialog(this,"Help");
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#cancelSelected()
	 */
	protected void cancelSelected() {
		hide();
	}

	/* (non-Javadoc)
	 * @see com.spip.modelli.WLoopDialog#enterSelected()
	 */
	protected void enterSelected() {
		StringBuffer loop = new StringBuffer();
		loop.append(createStartLoop());
		loop.append(createSpecificCriterionLoop());
		loop.append(createCommonCriterionLoop());
		loop.append(createIncludedElementsLoop());
		loop.append(createLogosLoop());
		loop.append(createTagsLoop());
		loop.append(createEndLoop());
		WEdit we = (WEdit)getParent();
		we.editor.replaceSelection(loop.toString());
		hide();
	}

	protected JComponent createSpecific() {
		JLabel l;
		JPanel panel;
		int compIndex=0;
		
		JPanel jp = (JPanel)super.createSpecific();

		jp.add(Box.createVerticalGlue(),compIndex++);
		l = new JLabel(Messages.getString("WLoopDialog.SelectionCrit"));
		l.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp.add(l,compIndex++);
		
		panel = new JPanel(new GridBagLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(5,0,0,0));
		id_article = new JCheckBoxAA(Messages.getString("WLoopDialog.id_article"));
		id_rubrique = new JCheckBoxAA(Messages.getString("WLoopDialog.id_rubrique"));
		id_sector = new JCheckBoxAA(Messages.getString("WLoopDialog.id_secteur"));
		branche = new JCheckBoxAA(Messages.getString("WLoopDialog.branche_art"));
		id_auteur =  new JCheckBoxAA(Messages.getString("WLoopDialog.id_auteur"));
		recerche =  new JCheckBoxAA(Messages.getString("WLoopDialog.recerche"));
		id_mot =  new JCheckBoxAA(Messages.getString("WLoopDialog.id_mot"));
		titre_mot =  new JCheckBoxAA(Messages.getString("WLoopDialog.titre_mot"));
		type_mot =  new JCheckBoxAA(Messages.getString("WLoopDialog.type_mot"));
		tout =  new JCheckBoxAA(Messages.getString("WLoopDialog.tout"));
		lang =  new JCheckBoxAA(Messages.getString("WLoopDialog.lang"));
		date =  new JCheckBoxAA(Messages.getString("WLoopDialog.date"));
		id_groupe =  new JCheckBoxAA(Messages.getString("WLoopDialog.id_groupe"));
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		panel.add(id_article,c);
		panel.add(id_rubrique,c);
		panel.add(id_sector,c);
		panel.add(branche,c);
		panel.add(id_auteur,c);
		c.gridy = 1;
		panel.add(recerche,c);
		panel.add(id_mot,c);
		panel.add(titre_mot,c);
		panel.add(type_mot,c);
		c.gridy = 2;
		panel.add(tout,c);
		panel.add(lang,c);
		panel.add(date,c);
		panel.add(id_groupe,c);
		//to fix bad starting size, not sufficient to show all component
		//panel.setMaximumSize(panel.getMinimumSize());
		Dimension d = panel.getMaximumSize();
		d.height = panel.getMinimumSize().height;
		panel.setMaximumSize(d);
		
		panel.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp.add(panel,compIndex++);
		
		return jp;
	}
	
	protected  JComponent createLogos() {
		JPanel panel;
		JLabel l;
		int compIndex=0;
		
		JPanel jp = (JPanel)super.createLogos();
		
		panel = (JPanel)jp.getComponent(0);
		panel.add(Box.createVerticalStrut(10),compIndex++);
		logo_article = new JCheckBoxAA(Messages.getString("WArticlesDialog.logo_art"));
		panel.add(logo_article,compIndex++);
		logo_article_rub = 
			new JCheckBoxAA(Messages.getString("WArticlesDialog.logo_art_rub"));
		panel.add(logo_article_rub,compIndex++);
		
		compIndex=0;
		panel = (JPanel)jp.getComponent(1);
		panel.add(Box.createVerticalStrut(10),compIndex++);
		logo_article_norm = 
			new JCheckBoxAA(Messages.getString("WArticlesDialog.logo_art_norm"));
		panel.add(logo_article_norm,compIndex++);
		logo_article_roll = 
			new JCheckBoxAA(Messages.getString("WArticlesDialog.logo_art_roll"));
		panel.add(logo_article_roll,compIndex++);
		
		return jp;
	}
	
	protected JComponent createTags() {
		JPanel panel;
		JLabel l;
		int compIndex = 0;

		JPanel jp = (JPanel)super.createTags();
		
		panel = new JPanel(new GridBagLayout());
		l = new JLabel(Messages.getString("WLoopDialog.DB_Tags"));
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 1;
		c.gridwidth = 5;
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		ID_ARTICLE = new JCheckBoxAA(Messages.getString("WLoopDialog.ID_ARTICLE"));
		panel.add(ID_ARTICLE,c);
		TOP_TITLE = new JCheckBoxAA(Messages.getString("WLoopDialog.SURTITRE"));
		panel.add(TOP_TITLE,c);
		TITLE = new JCheckBoxAA(Messages.getString("WLoopDialog.TITRE"));
		panel.add(TITLE,c);
		SUBTITLE = new JCheckBoxAA(Messages.getString("WLoopDialog.SOUSTITRE"));
		panel.add(SUBTITLE,c);
		DESCRIPTION = new JCheckBoxAA(Messages.getString("WLoopDialog.DESCRIPTIF"));
		panel.add(DESCRIPTION,c);
		c.gridy = 2;
		DECK = new JCheckBoxAA(Messages.getString("WLoopDialog.CHAPO"));
		panel.add(DECK,c);
		TEXT = new JCheckBoxAA(Messages.getString("WLoopDialog.TEXTE"));
		panel.add(TEXT,c);
		PS = new JCheckBoxAA(Messages.getString("WLoopDialog.PS"));
		panel.add(PS,c);
		ID_RUBRIQUE = new JCheckBoxAA(Messages.getString("WLoopDialog.ID_RUBRIQUE"));
		panel.add(ID_RUBRIQUE,c);
		ID_SECTOR = new JCheckBoxAA(Messages.getString("WLoopDialog.ID_SECTEUR"));
		panel.add(ID_SECTOR,c);
		c.gridy = 3;
		SITE_NAME = new JCheckBoxAA(Messages.getString("WLoopDialog.NOM_SITE"));
		panel.add(SITE_NAME,c);
		SITE_URL = new JCheckBoxAA(Messages.getString("WLoopDialog.URL_SITE"));
		panel.add(SITE_URL,c);
		VISITS = new JCheckBoxAA(Messages.getString("WLoopDialog.VISITES"));
		panel.add(VISITS,c);
		POPULARITY = new JCheckBoxAA(Messages.getString("WLoopDialog.POPULARITE"));
		panel.add(POPULARITY,c);
		DATE = new JCheckBoxAA(Messages.getString("WLoopDialog.DATE"));
		panel.add(DATE,c);
		jp.add(panel,compIndex++);

		panel = new JPanel(new GridBagLayout());
		c.gridy=0;
		c.gridwidth = 4;
		l = new JLabel(Messages.getString("WLoopDialog.Calc_Tags"));
		panel.add(l,c);
		c.gridwidth = 1;
		c.gridy = 1;
		ARTICLE_URL = new JCheckBoxAA(Messages.getString("WLoopDialog.URL_ARTICLE"));
		panel.add(ARTICLE_URL,c);
		INTRODUCTION = new JCheckBoxAA(Messages.getString("WLoopDialog.INTRODUCTION"));
		panel.add(INTRODUCTION,c);
		FOOT_NOTES = new JCheckBoxAA(Messages.getString("WLoopDialog.NOTES"));
		panel.add(FOOT_NOTES,c);
		AUTHORS = new JCheckBoxAA(Messages.getString("WLoopDialog.LESAUTEURS"));
		panel.add(AUTHORS,c);
		c.gridy = 2;
		FORUM_FORM = new JCheckBoxAA(Messages.getString("WLoopDialog.FORMULAIRE_FORUM"));
		panel.add(FORUM_FORM,c);
		FORUM_PARAMETERS = new JCheckBoxAA(Messages.getString("WLoopDialog.PARAMETRES_FORUM"));
		panel.add(FORUM_PARAMETERS,c);
		c.gridwidth = 2;
		SIGNATURE_FORM = new JCheckBoxAA(Messages.getString("WLoopDialog.FORMULAIRE_SIGNATURE"));
		panel.add(SIGNATURE_FORM,c);
		jp.add(panel,compIndex++);

		return jp;
	}
}
