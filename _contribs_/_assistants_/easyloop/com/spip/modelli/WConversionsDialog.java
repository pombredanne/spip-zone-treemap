/*
 * Created on 12-apr-2005
 *
 * Copyright 2005 Renato Formato, Karim Belkacem
 * License GNU/GPL		
 * 
 */
package com.spip.modelli;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;

/**
 * @author Renato Formato
 *
 * 
 * 
 */
public class WConversionsDialog extends JDialog implements ActionListener {

	JButtonAA btnword,btnhtml,btnpacbase,btncancel;
	
	private WConversionsDialog(Frame owner) throws HeadlessException {
		super(owner, Messages.getString("WConversionsDialog.Title"), true); //$NON-NLS-1$
		addGUI();
	}

	private void addGUI() {
		
	    int max_width=0,button_height=0;
	    Dimension dim; 
	    JLabel j1,j2,j3;
	    
	    Container cp = getContentPane();
		j1 = new JLabel(Messages.getString("WConversionsDialog.HTMLDescription")); //$NON-NLS-1$
		//center label
		j1.setAlignmentX(0.5f);
		btnhtml = new JButtonAA(Messages.getString("WConversionsDialog.HTMLLabel")); //$NON-NLS-1$
		btnhtml.addActionListener(this);
		btnhtml.setAlignmentX(0.5f);
		//get std button height
		button_height = btnhtml.getPreferredSize().height;
		//get maximum button width
		if (max_width<btnhtml.getMaximumSize().width) max_width=btnhtml.getMaximumSize().width;  
		j2 = new JLabel(Messages.getString("WConversionsDialog.WordDescription")); //$NON-NLS-1$
		j2.setAlignmentX(0.5f);
		btnword = new JButtonAA(Messages.getString("WConversionsDialog.WordLabel")); //$NON-NLS-1$
		btnword.addActionListener(this);
		btnword.setAlignmentX(0.5f);
		if (max_width<btnword.getMaximumSize().width) max_width=btnword.getMaximumSize().width;
		j3 = new JLabel(Messages.getString("WConversionsDialog.EXTRDescrptioni")); //$NON-NLS-1$
		j3.setAlignmentX(0.5f);
		btnpacbase = new JButtonAA(Messages.getString("WConversionsDialog.EXTRLabel")); //$NON-NLS-1$
		btnpacbase.addActionListener(this);
		btnpacbase.setAlignmentX(0.5f);
		if (max_width<btnpacbase.getMaximumSize().width) max_width=btnpacbase.getMaximumSize().width;
		//set label width to maximum button width. set label height
		j1.setPreferredSize(new Dimension(max_width,75));
		j2.setPreferredSize(new Dimension(max_width,50));
		j3.setPreferredSize(new Dimension(max_width,50));
		//set button width the same for all buttons
		btnhtml.setMaximumSize(new Dimension(max_width,button_height));
		btnword.setMaximumSize(btnhtml.getMaximumSize());
		btnpacbase.setMaximumSize(btnhtml.getMaximumSize());
		JPanel jp = new JPanel();
		jp.setLayout(new BoxLayout(jp,BoxLayout.Y_AXIS));
		jp.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		jp.add(j1);
		jp.add(btnhtml);
		jp.add(Box.createRigidArea(new Dimension(0,10)));
		jp.add(new JSeparator(JSeparator.HORIZONTAL));
		jp.add(j2);
		jp.add(btnword);
		jp.add(Box.createRigidArea(new Dimension(0,10)));
		jp.add(new JSeparator(JSeparator.HORIZONTAL));
		jp.add(j3);
		jp.add(btnpacbase);
		cp.add(jp);
		pack();
		//Centra la finesrta di dialogo
		setLocationRelativeTo(getParent());
	}

	public void actionPerformed(ActionEvent e) {
		Object s = e.getSource();
		if (s==btncancel) {this.hide();return;}
		if (s==btnword) {
			/* TODO To implement method for converts from word */
			hide();
			}
		if (s==btnhtml) {
			/* TODO To implement method for converts from html */
			hide();
		}
		if (s==btnpacbase) {
			/* TODO To implement method for converts from extr */
			hide();
		}		
	}
	
	static public void showConversionsDialog(Frame owner) {
		WConversionsDialog td = new WConversionsDialog(owner);
		td.show();
	}
	
}
