----------------------------------------------------------------------
    Copyright (C) 2004 Fabrice Gangler 
	
    www.drop-zone-city.com	     // SPIP Boucle Web-GENERATOR  //
----------------------------------------------------------------------

	Ce programme est un logiciel libre ; vous pouvez le redistribuer 
	et/ou le modifier conform�ment aux dispositions de la 
	Licence Publique G�n�rale GNU, telle que publi�e par la 
	Free Software Foundation ; version 2 de la licence, ou encore 	
	(� votre choix) toute version ult�rieure.

	Ce programme est distribu� dans l'espoir qu'il sera utile,
	 mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de 
	COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. 
	Pour plus de d�tails, voir la Licence Publique G�n�rale GNU.

	Un exemplaire de la Licence Publique G�n�rale GNU doit �tre fourni 
	avec ce programme ; si ce n'est pas le cas, �crivez � la 
	Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis. 
	
	--------------------------------------------------------------

	- GNU General Public License : 
	  http://www.gnu.org/copyleft/gpl.html

	- Traduction (Non officielle) en fran�ais de la Licence GPL
	  http://www.april.org/gnu/gpl_french.html

----------------------------------------------------------------------


======================================================================
		C R E D I T S
======================================================================
	
	--------------------------------------------------------------
	- Contrib (Recherche dans la documentation) sur SPIP-Contrib
	- Extention Dreamweaver pour SPIP




======================================================================
		R E M E R C I E M E N T
======================================================================

	Pour leur aide, merci � : ...


	


================================================================== END


----------------------------------------------------------------------
    Copyright (C) 2004 Fabrice Gangler 
	
    www.drop-zone-city.com	     // SPIP Boucle Web-GENERATOR  //
----------------------------------------------------------------------