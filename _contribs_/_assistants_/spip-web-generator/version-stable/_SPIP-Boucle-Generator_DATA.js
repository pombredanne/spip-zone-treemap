	// SPIP R�ference - BOUCLES / BALISES    
	///////////////////////////////////////////////////////////////////////////
	
	var tab_BOUCLE = new Array ;
	
	tab_BOUCLE [0] = new Array();
	tab_BOUCLE [0]['NAME'] = 'RUBRIQUES' ;
	tab_BOUCLE [0]['Selection'] = new Array( 'tout' , 'id_rubrique' , 'id_secteur' , 'id_parent' , 'id_enfant' , 'meme_parent' , 'recherche' , 'id_mot' , 'id_groupe' , 'titre_mot=' , 'type_mot=' ) ;
	tab_BOUCLE [0]['Affichage'] = new Array ('inverse' , 'par hasard' , 'exclus' , 'doublons' ) ;
	tab_BOUCLE [0]['Balise'] = new Array ('ID_RUBRIQUE' , 'TITRE' , 'DESCRIPTIF' , 'TEXTE' , 'ID_SECTEUR' , 'NOTES' , 'INTRODUCTION' , 'URL_RUBRIQUE' , 'DATE' , 'FORMULAIRE_FORUM' , 'PARAMETRES_FORUM' , 'FORMULAIRE_SITE' , 'LOGO_RUBRIQUE' , 'LOGO_RUBRIQUE_NORMAL' , 'LOGO_RUBRIQUE_SURVOL' ) ;
	
	tab_BOUCLE [1] = new Array();
	tab_BOUCLE [1]['NAME'] = 'ARTICLES' ;
	tab_BOUCLE [1]['Selection'] = new Array ('tout' , 'id_rubrique' , 'id_secteur' , 'id_article' , 'id_auteur' , 'id_mot' , 'id_groupe' , 'titre_mot=' , 'type_mot=' , 'recherche') ;
	tab_BOUCLE [1]['Affichage'] = new Array ('inverse' , 'par hasard' , 'exclus' , 'doublons') ;
	tab_BOUCLE [1]['Balise'] = new Array ('ID_RUBRIQUE' , 'ID_ARTICLE' , 'SURTITRE' , 'TITRE' , 'SOUSTITRE' , 'DESCRIPTIF' , 'CHAPO' , 'TEXTE' , 'PS' , 'DATE' , 'DATE_REDAC' , 'DATE_MODIF' , 'ID_SECTEUR' , 'VISITES' , 'POPULARITE' , 'NOTES' , 'INTRODUCTION' , 'LESAUTEURS' , 'URL_ARTICLE' , 'FORMULAIRE_FORUM' , 'FORMULAIRE_SIGNATURE' , 'PARAMETRES_FORUM' , 'LOGO_ARTICLE' , 'LOGO_ARTICLE_RUBRIQUE' , 'LOGO_RUBRIQUE' , 'LOGO_ARTICLE_NORMAL' , 'LOGO_ARTICLE_SURVOL' ) ;
	
	tab_BOUCLE [2] = new Array();
	tab_BOUCLE [2]['NAME'] = 'BREVES' ;
	tab_BOUCLE [2]['Selection'] = new Array ('tout' , 'id_rubrique' , 'id_breve' , 'id_mot' , 'id_groupe' , 'titre_mot=' , 'type_mot=' , 'recherche' ) ;
	tab_BOUCLE [2]['Affichage'] = new Array ('inverse' , 'par hasard') ;
	tab_BOUCLE [2]['Balise'] = new Array ('ID_BREVE' , 'TITRE' , 'DATE' , 'TEXTE' , 'NOM_SITE' , 'URL_SITE' , 'ID_RUBRIQUE' , 'NOTES' , 'INTRODUCTION' , 'URL_BREVE' , 'FORMULAIRE_FORUM' , 'PARAMETRES_FORUM' , 'LOGO_BREVE' ,  'LOGO_BREVE_RUBRIQUE' ) ;
		
	tab_BOUCLE [3] = new Array();
	tab_BOUCLE [3]['NAME'] = 'DOCUMENTS' ;
	tab_BOUCLE [3]['Selection'] = new Array ( 'tout' , 'id_rubrique' , 'id_article' , 'id_breve' );
	tab_BOUCLE [3]['Affichage'] = new Array ('doublons' , 'mode=vignette' , 'mode=document' , 'extension=' ) ;
	tab_BOUCLE [3]['Balise'] = new Array ( 'LOGO_DOCUMENT' , 'URL_DOCUMENT' , 'TITRE' , 'DESCRIPTIF' , 'TYPE_DOCUMENT' , 'TAILLE' , 'LARGEUR' , 'HAUTEUR' , 'EMBED_DOCUMENT' ) ;

	tab_BOUCLE [4] = new Array();
	tab_BOUCLE [4]['NAME'] = 'MOTS' ;
	tab_BOUCLE [4]['Selection'] = new Array ( 'tout' , 'id_mot' , 'id_groupe' , 'id_rubrique' , 'id_article' , 'id_breve', 'id_syndic' , 'id_forum' , 'titre=' , 'type=' ) ;
	tab_BOUCLE [4]['Affichage'] = new Array ('inverse' , 'par hasard') ;
	tab_BOUCLE [4]['Balise'] = new Array ( 'ID_MOT' , 'TITRE' , 'DESCRIPTIF' , 'TEXTE' , 'TYPE' , 'LOGO_MOT' ) ;

	tab_BOUCLE [5] = new Array();
	tab_BOUCLE [5]['NAME'] = 'AUTEURS' ;
	tab_BOUCLE [5]['Selection'] = new Array ('tout', 'id_auteur' , 'id_article' ) ;
	tab_BOUCLE [5]['Affichage'] = new Array ('inverse' , 'par hasard') ;
	tab_BOUCLE [5]['Balise'] = new Array ("ID_AUTEUR" , "NOM" , "BIO" ,  "EMAIL" , "NOM_SITE" , "URL_SITE" , "PGP" , "FORMULAIRE_ECRIRE_AUTEUR" , "NOTES" , "LOGO_AUTEUR" ) ;

	tab_BOUCLE [6] = new Array();
	tab_BOUCLE [6]['NAME'] = 'SIGNATURES' ;
	tab_BOUCLE [6]['Selection'] = new Array ('tout', 'id_article' , 'id_signature') ;
	tab_BOUCLE [6]['Affichage'] =  new Array ('inverse' , 'par hasard') ;
	tab_BOUCLE [6]['Balise'] = new Array ( 'ID_SIGNATURE' , 'ID_ARTICLE' , 'DATE' , 'MESSAGE' , 'NOM' , 'EMAIL' , 'NOM_SITE' , 'URL_SITE' ) ;

	tab_BOUCLE [7] = new Array();
	tab_BOUCLE [7]['NAME'] = 'HIERARCHIE' ;
	tab_BOUCLE [7]['Selection'] = tab_BOUCLE [0]['Selection'] ;
	tab_BOUCLE [7]['Affichage'] = tab_BOUCLE [0]['Affichage']
	tab_BOUCLE [7]['Balise'] = tab_BOUCLE [0]['Balise']
	
	tab_BOUCLE [8] = new Array();
	tab_BOUCLE [8]['NAME'] = 'FORUMS' ;
	tab_BOUCLE [8]['Selection'] = new Array ( 'id_forum' , 'id_rubrique' , 'id_article' , 'id_breve' , 'id_parent' , 'id_enfant' , 'meme_parent' , 'id_mot' , 'id_groupe' , 'titre_mot=' , 'type_mot=' , 'plat' , 'id_secteur' ) ;
	tab_BOUCLE [8]['Affichage'] = new Array ('inverse' , 'par hasard') ;
	tab_BOUCLE [8]['Balise'] = new Array ('ID_BREVE' , 'ID_RUBRIQUE' , 'ID_ARTICLE' , 'TITRE' , 'DATE' , 'TEXTE' , 'NOM_SITE' , 'URL_SITE' , 'NOM' , 'EMAIL' , 'IP' , 'FORMULAIRE_FORUM' , 'PARAMETRES_FORUM' ) ;

	tab_BOUCLE [9] = new Array();
	tab_BOUCLE [9]['NAME'] = 'SITES' ;
	tab_BOUCLE [9]['Selection'] = new Array ( 'id_syndic' , 'tout' , 'id_rubrique' , 'id_secteur' , 'id_breve' , 'id_mot' , 'id_groupe' , 'titre_mot=' , 'type_mot=' ) ;
	tab_BOUCLE [9]['Affichage'] = new Array ('inverse' , 'par hasard' , 'exclus' , 'doublons' , 'moderation_N_oui' , 'moderation=oui' , 'syndication=oui' , 'syndication=non' ) ;
	tab_BOUCLE [9]['Balise'] = new Array ('ID_SYNDIC' , 'NOM_SITE' , 'URL_SITE' , 'DESCRIPTIF' , 'ID_RUBRIQUE' , 'ID_SECTEUR' , 'LOGO_SITE' ) ;

	tab_BOUCLE [10] = new Array();
	tab_BOUCLE [10]['NAME'] = 'SYNDIC_ARTICLES' ;
	tab_BOUCLE [10]['Selection'] = new Array ('tout', 'id_syndic_article', 'id_syndic', 'id_rubrique', 'id_secteur');
	tab_BOUCLE [10]['Affichage'] =  new Array ('inverse' , 'par hasard') ;
	tab_BOUCLE [10]['Balise'] = new Array ('ID_SYNDIC_ARTICLE' , 'ID_SYNDIC' , 'TITRE' , 'URL_ARTICLE' , 'DATE' , 'LESAUTEURS' , 'NOM_SITE' , 'URL_SITE' ) ;

	tab_BOUCLE ['FORMULAIRE'] = new Array();
	tab_BOUCLE ['FORMULAIRE']['Balise'] = new Array ( 'FORMULAIRE_ADMIN' , 'FORMULAIRE_INSCRIPTION' ,'FORMULAIRE_RECHERCHE' , 'FORMULAIRE_FORUM' , 'FORMULAIRE_SIGNATURE' , 'FORMULAIRE_SITE' , 'FORMULAIRE_ECRIRE_AUTEUR' ) ;
	
	tab_BOUCLE ['SITE_SPIP'] = new Array();
	tab_BOUCLE ['SITE_SPIP']['Balise'] = new Array (   'LOGIN_PRIVE' , 'LOGIN_PUBLIC' , 'URL_LOGOUT' , 'CHARSET' , 'NOM_SITE_SPIP' , 'URL_SITE_SPIP' , 'EMAIL_WEBMASTER' , 'PUCE' , 'RECHERCHE' ) ;

	var	tab_EXEMPLE = new Array();
	tab_EXEMPLE [0]= new Array();
	tab_EXEMPLE [0]['name']= '10_articles_pop';
	tab_EXEMPLE [0]['descriptif']= 'Les 10 br�ves les plus r�centes';
	tab_EXEMPLE [0]['code'] = '' ;

	tab_EXEMPLE [1]= new Array();
	tab_EXEMPLE [1]['name']= '10_breve_recent';
	tab_EXEMPLE [1]['descriptif']= 'Les 10 articles les plus consult�s';
		
	tab_EXEMPLE [2]= new Array();
	tab_EXEMPLE [2]['name']= '10_breve_recent';
	tab_EXEMPLE [2]['descriptif']= 'Navigation';


	// tab_affichage ['COMMUNS']= new Array ( 'inverse' , 'par hasard') ;
	
	// TAB_CLASSEMENT - A mettre � jour (TAB pas encore utilis�)
	// TAB_NON-CLASSEMENT - A mettre � jour (TAB pas encore utilis�)
	// TAB_BALISE - Logiquement � deduire de CLASSEMENT et NON-CLASSEMENT
		// tab_balise ['FORMULAIRE']= new Array ( 'FORMULAIRE_ADMIN' , 'FORMULAIRE_INSCRIPTION' ,'FORMULAIRE_RECHERCHE' , 'FORMULAIRE_FORUM' , 'FORMULAIRE_SIGNATURE' , 'FORMULAIRE_SITE' , 'FORMULAIRE_ECRIRE_AUTEUR' , 'LOGIN_PRIVE' , 'LOGIN_PUBLIC' , 'URL_LOGOUT' , 'CHARSET' , 'NOM_SITE_SPIP' , 'URL_SITE_SPIP' , 'EMAIL_WEBMASTER' , 'PUCE' , 'RECHERCHE' ) ;
