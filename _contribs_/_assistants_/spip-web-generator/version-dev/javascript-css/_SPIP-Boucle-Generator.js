

		
	// liste_Boucle()
	//////////////////////////////////
	function liste_Boucle(){ 

		var str_out = '' ;

		for ( var j=0 ; j < (tab_BOUCLE.length) ; j++ ){
			str_out += '<label><input type="radio" ';
			
			if (j==1){
				str_out += ' checked="checked" ';
			}
			str_out += ' onclick="ChangeValeur(\'boucleType_Valeur\', \''+ j +'\'); ChangeValeur(\'etape_max\', 1 );" ';
			str_out += ' name="BoucleTypeSELECT" value="'+ j +'"';
			str_out += ' id="BoucleTypeSELECT'+ j  +'" />&nbsp;' ;
			str_out += tab_BOUCLE[j]['NAME'] + '<label>'  ;
		}	

		return str_out ;		
	}
	
	
		
	// affichage pour CheckAll()
	//////////////////////////////////
	// function HTML_LinkListe(formName, elementName){ 
	function HTML_LinkListe(formName){ 	

		var str_out = '<br /><label>' ;		
		// var elementName = 'boucle_balise';
		
		str_out +=  '<input type="checkbox" name="checkAll" '
		// str_out +=  'onclick="CheckAll('+ formName +', ' + elementName + ')" /> ' ;
		str_out +=  'onclick="CheckAll('+ formName +', \'boucle_balise\')" /> ' ;
		str_out +=  'Tous / Aucun</label>' ;	
		return str_out ;	
	}
	
			
	// CheckAll()
	//////////////////////////////////
	function CheckAll(formName, elementName){		
		var f = document.eval(formName) ;
		for (i=0; i < f.elements.length; i++){
			var e = f.elements[i];
			if (e.name == elementName){
				e.checked = f.checkAll.checked;
			}
		}
	}
	
	
	// liste_Balise
	//////////////////////////////////
	function liste_Balise( id_boucle, nb_colonne ){ 

		var str_out = '' ;
		var temp = '' ;
		var ad0 = ''; 
		var ad1 = '';
		var test_K = 'OK' ;		
		var tab_total  = tab_BOUCLE [id_boucle]['Balise'].length ; 
		
		if (nb_colonne == 2){
			ad0 = '<div class="DemiListe">' ;		
			ad1 = '</div><div class="DemiListe">' ;		
			str_out +=  ad0 ;
		}		 
		for ( var j=0 ; j < tab_total ; j++ ){		
			if ( j > (tab_total / 2)  ){
				if (test_K=='OK'){				
					test_K='NNNN' ;
					str_out +=  ad1 ;
				}					
			}
			temp = tab_BOUCLE [id_boucle]['Balise'][j] ;
			str_out +=  '<label><input name="boucle_balise" id="'+ temp +'" type="checkbox" />' ;	
			str_out +=  '&nbsp; #'+ temp +' </label>' ; 
		}		
		
		// str_out += HTML_LinkListe('form_A3', 'boucle_balise');		
		str_out += HTML_LinkListe('form_A3');
		
		str_out += ad0 ;			
		return str_out ;	
	}		 
	
	
	// liste_Critere
	//////////////////////////////////
	function liste_Critere( id_boucle, name_tab ){ 

		var str_out = '' ;		
		var tab  = tab_BOUCLE [id_boucle][name_tab] ; 
			 
		for ( var j=0 ; j < tab.length ; j++ ){
			str_out +=  '<label><input name="'+ name_tab +'" id="'+ tab[j] +'" type="checkbox" />' ;	
			str_out +=  '&nbsp; #'+ tab[j] +' </label> ' ; 
		}		
		// str_out += HTML_LinkListe('form_A2', name_tab);
		return str_out ;	
	}		 
	
	

	
	
	// Afficher le bas de page
	////////////////////////////////
	function afficherBas() {
		str_out = '' ;
		str_out = recupDIV('bas')  ;
		document.write (str_out) ;
	}
	


	// Afficher la liste des boucles
	////////////////////////////////
	function afficher_ListeBoucle(id) {
		
		var str_out = '' ;
		
		if(str_out = liste_Boucle()) {
 			ecrireDIV(id, str_out) ;
 		}
  		else {
    		erreur_alert("Erreur --> afficher_ListeBoucle - " + id);
    		return false;
  		}	
	}

	// Afficher la liste de crit�res
	////////////////////////////////
	function afficher_ListeCritere(name) {	
		str_out = '' ;
		str_out = liste_Critere (RecupValeur('boucleType_Valeur'), name );
		ecrireDIV(name, str_out) ;
		return ;
	}
	
	
	// Afficher la liste de balises
	////////////////////////////////
	function afficher_ListeBalise(name) {
		str_out = '' ;
		str_out = liste_Balise (RecupValeur('boucleType_Valeur'),2);
		ecrireDIV(name, str_out) ;
		return ;
	}

	// Afficher le navigateur (Etape par Etape)
	////////////////////////////////
	function afficher_navigateur() {
	
		s_1 = RecupValeur('switch') ;
		s_2  = RecupValeur('etape_total')  ;
		s_3  = RecupValeur('etape') ;
		
		str_out = '<span>Etape(s) : </span>' ;
		
		if ( s_3 > 0 ){
			str_out +=  '<a href="#" onClick="etape(\'A0\')">0</a>' ;	
		}
		else  {
			str_out +=  '0' ;		
			// return true ;
		}		
		
		for ( var j=1 ;  j < s_2   ; j++ ){
			
			str_out +=  ' &nbsp; ' ;
			str_lien = '<a href="#" onClick="etape(\'' + s_1 + j + '\')">' + j + '</a>' ;
		
			if ( j < s_3 ){
				str_out +=  str_lien ;	
			}
			
			if ( j == s_3 ){
				str_out +=  j  ;
			}
						
			if ( j > s_3 ){
							
				if (RecupValeur('etape_max') > RecupValeur('etape')){
					str_out +=  str_lien ;
				}
				else{
					str_out +=  '<span class="gris">' + j + '</span>' ;
				}
			} 
		}				
		ecrireDIV('navigateur', str_out) ;
		return ;
	}
	
	

	
	
	// -- Ajout de '_' si 'balise_name' est de type texte + suppression des Espaces
	function Validation_NomBalise(balise_name){
	
		if (balise_name == ''){
			balise_name = "Votre_Boucle" ;
		}
		var reg_balise_name = /([^0-9])/; 
		
		if ( reg_balise_name.exec(balise_name)){
			// Suppresion des espaces
			balise_name = balise_name.replace( /([\s\W])/gi , '_') ;
			balise_name = "_" + balise_name ;		
		}		
		return balise_name ;	
	}
	
	
	// -- Parametre
	function tab_critere(tab, critere_base) {
		var critere = '' ;
		var str = '' ;
		for ( var k=0 ; k < tab.length ; k++ ){	
			critere = critere_base + tab[k] ;						
			if ( document.getElementById(critere).checked ){
				str  += '{' + tab[k] + '}';
			}	
		} 
		return str ;
	}
	
	function gestion_BALISE(boucle_type, balise_add_0, balise_add_1 ){
		var tab_temp = tab_BOUCLE [boucle_type]['Balise'] ;				 
		for ( var j=0 ; j < tab_temp.length ; j++ ){
			if(document.getElementById(tab_temp[j]).checked){
				str_output += balise_add_0 + tab_temp[j] + balise_add_1  ;
			}
		}
		return str_output ;
	}	
	
	
	// G�n�ration du Code
	function GenerationCode(){
	
		
		var	str_output = '\n' ;
		var	str_tab = '' ;
		var	str_br = '' ;		
		
		// Traitement des tabulations 
		str_tab = '\t' ; // str_tab = ' ' ;
		
		// Traitement de la balise <br />
		if ( document.getElementById('br_add_1').checked ){
			str_br = '<br> ' ;
		}
		if (document.getElementById('br_add_2').checked){
			str_br = '<br /> ' ;
		}
		
		// Traitement de la balise <br />
		if ( document.getElementById('balise_add').checked ){
			balise_add_0 = str_tab + str_br + ' [(#' ;
			balise_add_1 = ')] \n' ;
		}
		else{
			balise_add_0 = str_tab + str_br + ' #' ;
			balise_add_1 =  '\n' ;
		}		
		
		
		// Traitement en fonction du champ  cach� SWITCH
		switch (document.getElementById('switch').value){
				
			case 'A' :
				var balise_name =  Validation_NomBalise (document.getElementById('boucleNAME').value) ; 				

				var boucle_type =  RecupValeur('boucleType_Valeur') ;
				var str_Param = '' ;
				str_Param  += tab_critere(tab_BOUCLE [boucle_type]['Selection'], '') ;
				str_Param  += tab_critere(tab_BOUCLE [boucle_type]['Affichage'], '') ;
											
				if ( document.getElementById('balise_BOUCLE').checked ){
			
					// -- Balise <BOUCLE>
					var str_Boucle_Start = '<BOUCLE'  + balise_name +'('+ tab_BOUCLE [boucle_type]['NAME']  +')';
					str_Boucle_Start += str_Param + '>\n\n'  ;
					
					var str_Boucle_End = '\n' +'</BOUCLE' + balise_name +'>\n';
					
					// -- Balise alternative <B></B>
					var str_B_Start = '' ;
					var str_B_End = '' ;
					if ( document.getElementById('balise_B').checked ){
						str_B_Start = '<B'  + balise_name +'>\n';
						str_B_End = '</B'  + balise_name +'>\n';
					}
				
					// -- Balise alternative <//B>
					var str_B_Alternative = '' ;
					if ( document.getElementById('balise_BB').checked  ){
						var str_B_Alternative = '<//B'  + balise_name +'>\n';
					}
				}
				
				str_output +=    str_B_Start + str_Boucle_Start ;
				
				// -- BALISE � afficher
				var tab_temp = tab_BOUCLE [boucle_type]['Balise'] ;				 
				for ( var j=0 ; j < tab_temp.length ; j++ ){
					if(document.getElementById(tab_temp[j]).checked){
						str_output += balise_add_0 + tab_temp[j] + balise_add_1  ;
					}
				}
				// str_output +=  gestion_BALISE (boucle_type, balise_add_0, balise_add_1 );
				str_output +=  str_Boucle_End + str_B_End + str_B_Alternative ;
				break ;
		
			case 'B' :
				str_tab = '' ;
				tab_temp = tab_BOUCLE ['FORMULAIRE']['Balise'] ;				 
				for ( var j=0 ; j < tab_temp.length ; j++ ){
					if(document.getElementById(tab_temp[j]).checked){
						str_output += balise_add_0 + tab_temp[j] + balise_add_1  ;
					}
				}
				tab_temp = tab_BOUCLE ['SITE_SPIP']['Balise'] ;				 
				for ( var j=0 ; j < tab_temp.length ; j++ ){
					if(document.getElementById(tab_temp[j]).checked){
						str_output += balise_add_0 + tab_temp[j] + balise_add_1  ;
					}
				}				
				break ;
				
			case 'C' :
				kit_exemple = RecupValeur('kit_exemple') ;
				if ( kit_exemple == 0 ){
					str_output += '\n' + '<B_Breve_10_Plus_Recent> \n' ;
					str_output += '<b> Les 10 derni�res br�ves : </b>'+ '\n' +  str_br + '\n' ;
					str_output +=  '<BOUCLE_Breve_10_Plus_Recent(BREVES){par date}{inverse}{0,10}> \n\n' ;
					str_output +=  str_tab + str_br  + '[(#DATE|affdate)] : <a href="#URL_BREVE">[(#TITRE|supprimer_numero)]</a>'+ ' \n' ;
					str_output += '\n' + '</BOUCLE_Breve_10_Plus_Recent> \n' ;
					str_output += '</B_Breve_10_Plus_Recent> \n' ;
					str_output += '\n' + str_br +' Aucune br�ve... \n' ;
					str_output += '<//B_Breve_10_Plus_Recent> \n\n' ;
				}
				
				if ( kit_exemple == 1 ){
					str_output += '\n' + '<B_Article_10_Plus_Consulter> \n' ;
					str_output += '<b> Les 10 articles les plus consult�s : </b>'+ '\n' +  str_br + '\n' ;
					str_output +=  '<BOUCLE_Article_10_Plus_Consulter(ARTICLES){par visites}{inverse}{0,10}> \n' ;
					str_output +=   '\n' ;
					str_output +=   str_tab + str_br  +'[(#DATE|affdate)] : ' +'\n' ;
					str_output +=   str_tab +'<a href="#URL_ARTICLE">' ;
					str_output +=  '[(#TITRE|supprimer_numero)]</a>'+ '\n' ;
					str_output +=  str_tab +'[(#VISITES) visiteur(s)]  \n\n' ;
					str_output +=  '</BOUCLE_Article_10_Plus_Consulter> \n' ;
					str_output += '</B_Article_10_Plus_Consulter> \n' ;
					str_output +=  '\n' + str_br  +'Aucun article... \n' ;
					str_output += '<//B_Article_10_Plus_Consulter> \n\n\n' ;
				}
				
				if ( kit_exemple == 2 ){
					str_output += '\n' + '<B_navigation> \n' ;
					str_output += '<b> Navigation dans le site : </b>'+ str_br  +' \n' ;
					str_output += '<BOUCLE_navigation(ARTICLES){id_article}> \n\n' ;
					str_output +=  str_tab +'<B_navig> \n' ;
					str_output +=  str_tab +'<a href="#URL_SITE_SPIP">' ;
					str_output += 'Accueil</a> &gt; \n\n' ;
					str_output +=  str_tab +'<BOUCLE_navig(HIERARCHIE){id_article}{"&gt;"}> \n' ; 
					str_output +=  str_tab + str_tab +'<a href="#URL_RUBRIQUE">' ;
					str_output += '[(#TITRE|supprimer_numero)]</a> \n' ;
					str_output +=  str_tab +'</BOUCLE_navig> \n' ;
					str_output +=  str_tab +'</B_navig> \n' ;
					str_output +=  str_tab +'<//B_navig> \n\n' ;
					str_output += '</BOUCLE_navigation> \n' ;
					str_output += '</B_navigation> \n' ;
					str_output += '<//B_navigation> \n\n' ;
				}		
				break ;				
		}
				
		document.getElementById('resultat').value = str_output ; 
	}
	
	
	
	
	
	
	
////// Gestion des Etapes ////////////////////////


	// Ecrire un message dans un calque
	///////////////////////////////////	
	function first(){	
	 var str = RecupValeur('switch')+ "1";
	 etape(str);
	 }
	 
	 
	
	// Gestion des �tapes du quetionnaire
	/////////////////////////////////////
	function etape(n){
	
		// top();
		switch (n){
		
		case 'A0' :
			ChangeValeur('switch', 'A' );
			ChangeValeur('etape', '0' );
			ChangeValeur('etape_total', '1' );
			ChangeValeur('boucleType_Valeur', '1' );	
			ChangeValeur('etape_max', '0' );	
			afficherDIV('head', 'visible') ;
			break ;
		
		case 'A1' :
			// Selection du type de boucle
			ChangeValeur('etape_total', 5 );
			ChangeEtape(1);						
			str = RecupValeur('etape_max') ;
			afficher_ListeBoucle('typeBOUCLE') ;	
			if ( str  == 1 ){
			//	ChangeValeur('etape_total', 5 );
			//	afficher_ListeBoucle('typeBOUCLE') ;  
			}
			break ;

		case 'A2' :
			// Selection des crit�res d'affichages / de selection
			ChangeEtape(2);
			str = RecupValeur('etape_max') ;	
			if ( str  == 2 ){
				afficher_ListeCritere('Selection') ; 
				afficher_ListeCritere('Affichage') ; 
			}
			break ;
			
		case 'A3' :
			// Selection des balises � afficher
			ChangeEtape(3);
			str = RecupValeur('etape_max') ;
						
			if ( str == 3  ){
				afficher_ListeBalise('A3balise') ;
			}			
			break ;
		
		case 'A4' :
			// G�n�ration du Code
			ChangeEtape( RecupValeur('etape_total')-1 );
			GenerationCode();
		break ;
			
		case 'B1' :
			ChangeValeur('switch', 'B');
			ChangeValeur('etape_total', 3 );
			ChangeEtape(1);
			break ;
			
		case 'C1' :
			ChangeValeur('switch', 'C');
			ChangeValeur('etape_total', 3 );
			ChangeEtape(1);			
			break ;
		}
	
		afficherDIV('A0', 'hidden') ;
		afficherDIV('A1', 'hidden') ;
		afficherDIV('A2', 'hidden') ;
		afficherDIV('A3', 'hidden') ;
		afficherDIV('A4', 'hidden') ;
		afficherDIV('C1', 'hidden') ;
		afficherDIV('B1', 'hidden') ;
		
		afficherDIV( n , 'visible') ;
		afficher_navigateur() ;
	}
	
	
	
	// Changer l'�tape en cours
	////////////////////////////////
	function ChangeEtape(n) {				
		str = RecupValeur('etape_max') ; 		
		if (str  < n ){
			ChangeValeur('etape_max', n );
		}
		ChangeValeur('etape', n );
		return ;
	}	