----------------------------------------------------------------------
    Copyright (C) 2006 Fabrice Gangler 	
    www.drop-zone-city.com	 

 		   // Syntaxe SPIP au format XML + Moulinette XSLT  //
----------------------------------------------------------------------

	Ce programme est un logiciel libre ; vous pouvez le redistribuer 
	et/ou le modifier conform�ment aux dispositions de la 
	Licence Publique G�n�rale GNU, telle que publi�e par la 
	Free Software Foundation ; version 2 de la licence, ou encore 	
	(� votre choix) toute version ult�rieure.

	Ce programme est distribu� dans l'espoir qu'il sera utile,
	 mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de 
	COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. 
	Pour plus de d�tails, voir la Licence Publique G�n�rale GNU.

	Un exemplaire de la Licence Publique G�n�rale GNU doit �tre fourni 
	avec ce programme ; si ce n'est pas le cas, �crivez � la 
	Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis. 
	
	--------------------------------------------------------------

	- GNU General Public License : 
	  http://www.gnu.org/copyleft/gpl.html

	- Traduction (Non officielle) en fran�ais de la Licence GPL
	  http://www.april.org/gnu/gpl_french.html

----------------------------------------------------------------------


======================================================================
		I N S T A L L A T I O N
======================================================================



======================================================================
		U T I L I S A T I O N
======================================================================

- mettre � jour si besoin le fichier "syntax-SPIP.xml"
  � partir de la documentation officiel

- lancer le fichier "syntax-SPIP.xml" dans un navigateur Web moderne
- la transformation XSLT est g�n�r� � l'�cran
- copier le contenu dans le fichier "_SPIP-Boucle-Generator_DATA.js"
  de SPIP-Web-Generator


================================================================== END



----------------------------------------------------------------------
    Copyright (C) 2006 Fabrice Gangler 	
    www.drop-zone-city.com	 

 		   // Syntaxe SPIP au format XML + Moulinette XSLT  //
----------------------------------------------------------------------