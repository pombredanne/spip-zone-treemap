<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output 
	method="html"
	encoding="ISO-8859-1"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	indent="yes" />
<xsl:template match="/">

<xsl:for-each select="spip-syntaxe/les-boucles/boucle[not(@traitement='autre')]">
<!--
   Copyright (C) 2006  Fabrice Gangler
   www.drop-zone-city.com

   	 fichier SPIP_reference.js
   	 pour Assitant SPIP Dreamweaver
   	 g�n�rer � partir du fichier syntax-SPIP.xml
	     - spip_version : <xsl:value-of select="spip-syntaxe/@spip_version"/>
	     - spip_version_base : <xsl:value-of select="spip-syntaxe/@spip_version_base"/>
	     - version_syntaxe : <xsl:value-of select="spip-syntaxe/@version_syntaxe"/>
	     - maj : <xsl:value-of select="spip-syntaxe/@maj"/>


   					/// Boucle_<xsl:value-of select="@type"/>.htm //////
 -->



<html>
<head>
	<title>Boucle  <xsl:value-of select="@type"/></title>
	<script language="javascript" src="SPIP_balise.js"></script>
	<script language="javascript" src="SPIP_reference.js"></script>
</head>
<body>

<h1> <xsl:value-of select="@type"/> </h1>

<h2> Balises </h2>
<h3> Balises extraites de la base de donn�e </h3>
	<xsl:for-each select="./balise[@type='db']">
		<input type="checkbox" name="select__{text()}" /> <xsl:value-of select="."/>
		<xsl:if test="@version"> (<xsl:value-of select="@version"/>) </xsl:if>
		<xsl:if test="not(position() = last())"> <br /> </xsl:if>
	</xsl:for-each>

<h3>  Balises calcul� par SPIP </h3>
	<xsl:for-each select="./balise[@type='spip']">

<!--
		<xsl:attribute-set name="imput">
			<xsl:attribute name="type">checkbox</xsl:attribute>
		</xsl:attribute-set>

		<xsl:element	name="input">
			<xsl:value-of select="."/>
		</xsl:element>

-->

		<input type="checkbox" name="select__{text()}" />  <xsl:value-of select="."/>
		<xsl:if test="@version"> (<xsl:value-of select="@version"/>) </xsl:if>
		<xsl:if test="not(position() = last())"> <br /> </xsl:if>
	</xsl:for-each>

<h2> Crit�res </h2>
<h3> Crit�res de s�lection </h3> 	

	<xsl:for-each select="./critere[@type='selection']">
		<input type="checkbox" name="select__{text()}" /> <xsl:value-of select="."/>
		<xsl:if test="@version"> (<xsl:value-of select="@version"/>) </xsl:if>
		<xsl:if test="not(position() = last())"> <br /> </xsl:if>
	</xsl:for-each>

<h3>  Crit�res d'affichage </h3> 	
	<xsl:for-each select="./critere[@type='affichage']">
		<input type="checkbox" name="select__{text()}" /> <xsl:value-of select="."/>
		<xsl:if test="@version"> (<xsl:value-of select="@version"/>) </xsl:if>
		<xsl:if test="not(position() = last())"> <br /> </xsl:if>
	</xsl:for-each>

</body>
</html>

<hr />
</xsl:for-each>

</xsl:template>
</xsl:stylesheet>