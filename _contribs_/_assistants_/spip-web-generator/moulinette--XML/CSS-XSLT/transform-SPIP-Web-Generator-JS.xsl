<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output 
method="text"
media-type="text/javascript" />

<xsl:template match="/">///////////////////////////////////////////////////////////////////////////
//   Copyright (C) 2006  Fabrice Gangler
//   www.drop-zone-city.com
//
//    fichier _SPIP-Boucle-Generator_DATA.js
//    pour l'assitant SPIP Web generator
//    g�n�rer � partir du fichier syntax-SPIP.xml
//     - spip_version : <xsl:value-of select="spip-syntaxe/@spip_version"/>
//     - spip_version_base : <xsl:value-of select="spip-syntaxe/@spip_version_base"/>
//     - version_syntaxe : <xsl:value-of select="spip-syntaxe/@version_syntaxe"/>
//     - maj : <xsl:value-of select="spip-syntaxe/@maj"/>
//
///////////////////////////////////////////////////////////////////////////


// SPIP R�ference - BOUCLES / BALISES    
///////////////////////////////////////////////////////////////////////////

var tab_BOUCLE = new Array ;

<xsl:for-each select="spip-syntaxe/les-boucles/boucle[not(@traitement='autre')]">
tab_BOUCLE [<xsl:number value="position()-1" />] = new Array();
tab_BOUCLE [<xsl:number value="position()-1" />]['NAME'] = '<xsl:value-of select="@type"/>' ;
tab_BOUCLE [<xsl:number value="position()-1" />]['Selection'] = new Array (<xsl:for-each select="./critere[@type='selection']">'<xsl:value-of select="."/>'<xsl:if test="not(position() = last())">, </xsl:if></xsl:for-each>) ;
tab_BOUCLE [<xsl:number value="position()-1" />]['Affichage'] = new Array (<xsl:for-each select="./critere[@type='affichage']">'<xsl:value-of select="."/>'<xsl:if test="not(position() = last())">, </xsl:if></xsl:for-each>) ;
tab_BOUCLE [<xsl:number value="position()-1" />]['Balise'] = new Array (<xsl:for-each select="./balise">'<xsl:value-of select="."/>'<xsl:if test="not(position() = last())">, </xsl:if></xsl:for-each>) ;
</xsl:for-each>

tab_BOUCLE ['FORMULAIRE'] = new Array();
tab_BOUCLE ['FORMULAIRE']['Balise'] = new Array (<xsl:for-each select="spip-syntaxe/les-balises/balise[@type='formulaire']">'<xsl:value-of select="."/>'<xsl:if test="not(position() = last())">, </xsl:if></xsl:for-each>) ;

tab_BOUCLE ['SITE_SPIP'] = new Array();
tab_BOUCLE ['SITE_SPIP']['Balise'] = new Array (<xsl:for-each select="spip-syntaxe/les-balises/balise[not(@type='formulaire')]">'<xsl:value-of select="."/>'<xsl:if test="not(position() = last())">, </xsl:if></xsl:for-each>) ;

</xsl:template>
</xsl:stylesheet>