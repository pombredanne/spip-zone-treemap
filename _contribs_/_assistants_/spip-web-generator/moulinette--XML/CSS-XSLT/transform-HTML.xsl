<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output 
	method="html"
	encoding="ISO-8859-1"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	indent="yes" />

<xsl:template match="/">
	<html>
	<head>
		<title> Syntaxe SPIP</title>
	</head>
	<body>
		<h1> Syntaxe SPIP </h1>

	
	<xsl:if test="spip-syntaxe/documentation">
		Documentation :
		<xsl:for-each select="spip-syntaxe/documentation">	 
			<a href="{@url}"><xsl:value-of select="@lang"/></a>  | 
		</xsl:for-each>
		<hr />
	</xsl:if>


		<h2> BOUCLES </h2>
		<xsl:apply-templates select="spip-syntaxe/les-boucles/boucle" />
		

		<h2> FILTRES </h2>	
		<xsl:for-each select="spip-syntaxe/les-filtres/documentation">	 
			Documentation <a href="{@url}"> (<xsl:value-of select="@lang"/>)</a> - 
			<hr />
		</xsl:for-each> 	
		<ul>
		<xsl:for-each select="spip-syntaxe/les-filtres/filtre">	 
			<li> <xsl:value-of select="."/>	 </li>
		</xsl:for-each> 
		</ul>

	
		<h2> Site multilingue et internationalisation les squelettes</h2>
		<xsl:apply-templates select="spip-syntaxe/multilingue" />


		<h2> Autres balises </h2>
		<ul>	
		<xsl:for-each select="spip-syntaxe/les-balises">

			<li> <strong> <xsl:value-of select="@type"/> </strong> 

				<xsl:if test="./documentation">
					<br /> Documentation :
					<xsl:for-each select="./documentation">	 
						<a href="{@url}"><xsl:value-of select="@lang"/></a>  | 
					</xsl:for-each>
				</xsl:if>

				<ul>
				<xsl:for-each select="./balise">
					<li> <xsl:value-of select="."/>	 </li>
				</xsl:for-each> 
				</ul>
			</li>
		</xsl:for-each>	
		</ul>

	</body>
	</html>
</xsl:template>

	
<xsl:template match="spip-syntaxe/les-boucles/boucle">
	<h3> Boucle <xsl:value-of select="@type"/>	 </h3>
	<xsl:if test="./documentation">
		<xsl:call-template name="documentation"/>. 
	</xsl:if>


	<ul>
		<li> 	<strong> Balise(s) </strong>
			<ul>
				<li>  Extrait de la base de donn�e
					<ul>	
					<xsl:for-each select="./balise[@type='db']">		
						<li> 
							<xsl:value-of select="."/> 
							<xsl:if test="@version">
								(<xsl:value-of select="@version"/>)
							</xsl:if>
						</li> 
					</xsl:for-each> 
					</ul>
				</li>
				<li>  Calcul� par SPIP
					<ul>		
					<xsl:for-each select="./balise[@type='spip']">		
						<li> 
							<xsl:value-of select="."/> 
							<xsl:if test="@version">
								(<xsl:value-of select="@version"/>)
							</xsl:if>
						</li> 
					</xsl:for-each> 
					</ul>
				</li>
			</ul>
		</li>
	Nouveaux squelettes standards
		<li> <strong> Crit�re(s)  </strong>
			<ul>
				<li>  de s�lection
					<ul>	
					<xsl:for-each select="./critere[@type='selection']">		
						<li> <xsl:value-of select="."/>  </li> 
					</xsl:for-each> 
					</ul>
				</li>
				<li>  d'affichage
					<ul>		
					<xsl:for-each select="./critere[@type='affichage']">		
						<li> <xsl:value-of select="current()" /> </li> 
					</xsl:for-each> 
					</ul>
				</li>
			</ul>

		</li>
	</ul>
</xsl:template>


<xsl:template match="spip-syntaxe/multilingue">

	<xsl:if test="./documentation">
		<xsl:call-template name="documentation"/>. 
	</xsl:if>

	<ul>
		<li> 	<strong> Balise(s) </strong>
			<ul>
			<xsl:for-each select="./les-balises/balise">		
				<li> <xsl:value-of select="."/>  </li> 
			</xsl:for-each> 
			</ul>
		</li>

		<li> 	<strong> Chaine(s) d'internationalisation </strong>
			<ul>
			<xsl:for-each select="./les-chaines/chaine">		
				<li> <xsl:value-of select="."/>  </li> 
			</xsl:for-each> 
			</ul>
		</li>
	</ul>
</xsl:template>


<xsl:template name="documentation">
		Documentation :
		<xsl:for-each select="./documentation">	 
			<a href="{@url}"><xsl:value-of select="@lang"/> </a>  | 
		</xsl:for-each>
</xsl:template>


</xsl:stylesheet>