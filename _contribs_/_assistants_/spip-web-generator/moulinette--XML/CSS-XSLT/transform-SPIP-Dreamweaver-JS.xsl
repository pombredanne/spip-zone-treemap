<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output 
	method="text"
	media-type="text/javascript" />
<xsl:template match="/">///////////////////////////////////////////////////////////////////////////
//   Copyright (C) 2006  Fabrice Gangler
//   www.drop-zone-city.com
//
//   	 fichier SPIP_reference.js 
//   	 pour Assitant SPIP Dreamweaver  
//   	 g�n�rer � partir du fichier syntax-SPIP.xml
//	     - spip_version : <xsl:value-of select="spip-syntaxe/@spip_version"/>
//	     - spip_version_base : <xsl:value-of select="spip-syntaxe/@spip_version_base"/>
//	     - version_syntaxe : <xsl:value-of select="spip-syntaxe/@version_syntaxe"/>
//	     - maj : <xsl:value-of select="spip-syntaxe/@maj"/>
//
///////////////////////////////////////////////////////////////////////////


// SPIP R�ference - BOUCLES / BALISES    
///////////////////////////////////////////////////////////////////////////

var tab_selection = new Array ;
var tab_affichage = new Array ;
var tab_balise = new Array ;

// TAB_SELECTION 
<xsl:for-each select="spip-syntaxe/les-boucles/boucle[not(@traitement='autre')]">
tab_selection ['<xsl:value-of select="@type"/>'] =  new Array (<xsl:for-each select="./critere[@type='selection']">'<xsl:value-of select="."/>'<xsl:if test="not(position() = last())">, </xsl:if></xsl:for-each>) ;     </xsl:for-each>


// TAB_AFFICHAGE 
<xsl:for-each select="spip-syntaxe/les-boucles/boucle[not(@traitement='autre')]">
tab_affichage ['<xsl:value-of select="@type"/>'] = new Array (<xsl:for-each select="./critere[@type='affichage']">'<xsl:value-of select="."/>'<xsl:if test="not(position() = last())">, </xsl:if></xsl:for-each>) ;   </xsl:for-each>


// TAB_BALISE
<xsl:for-each select="spip-syntaxe/les-boucles/boucle[not(@traitement='autre')]">
tab_balise ['<xsl:value-of select="@type"/>'] = new Array (<xsl:for-each select="./balise">'<xsl:value-of select="."/>'<xsl:if test="not(position() = last())">, </xsl:if></xsl:for-each>) ;           </xsl:for-each>


</xsl:template>
</xsl:stylesheet>