;;; spip-mode.el --- spip-mode

;; Copyright (C) 2005 Pierre Andrews

;; Authors: Pierre Andrews (mortimer.pa@free.fr)
;; Keywords: spip, cms, derived mode
;; Version: 0.5

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; Mode pour la coloration spip. Ceci est une extension du mode
;; html-mode de emacs.
;; installer le dossier dans votre repertoire site-lisp

;; puis ajouter dans votre fichier .emacs:

;; (add-to-list 'load-path
;;              "/Applications/Emacs.app/Contents/Resources/site-lisp/spip-mode/")
;; (require 'spip-mode)
;; pour que spip-mode se lance tout seul quand vous ouvrez un fichier html
;; (add-to-list 'auto-mode-alist '("\\.[sj]?html?\\'" . spip-mode))
;; pour que spip-mode se lance avec la coloration syntaxique.
;; (add-hook 'spip-mode-hook 'turn-on-font-lock)

;; si vous avez les modes (mmm, css et php) necessaires, vous pouvez aussi installer ceci:
;; (require 'mmm-mode)
;; (setq mmm-global-mode 'maybe)
;; (add-to-list 'mmm-mode-ext-classes-alist '(spip-mode nil html-js))
;; (add-to-list 'mmm-mode-ext-classes-alist '(spip-mode nil embedded-css))
;; (mmm-add-group
;;  'fancy-html
;;  '(
;;    (html-php-tagged
;; 	:submode php-mode
;; 	:face mmm-code-submode-face
;; 	:front "<[?]php"
;; 	:back "[?]>")
;;    (html-css-attribute
;; 	:submode css-mode
;; 	:face mmm-declaration-submode-face
;; 	:front "style=\""
;; 	:back "\"")))
;; (add-to-list 'mmm-mode-ext-classes-alist '(spip-mode nil fancy-html))

;; Il est conseill� de configurer quelques variables:
;; M-x customize-group
;; spip

;; BUGS:

;;    1. indentation
;;    2. menu dans xemacs,
;;    3. bug de coloration des balises imbriqu�es.

;; TODO:
;;     * ajouter des fonctions pour ajouter du code facilement :
;;           o insertion balise code optionnel
;;           o ajout d'un filtre
;;           o fermeture de la derni�re boucle ouverte
;;     * compl�tions dans le buffer
;;           o des balise
;;           o des crit�res
;;           o des filtres
;;     * fonctions de localisation des squelettes :
;;           o voir la traduction d'une cha�ne,
;;           o transformer une cha�ne en cha�ne localis�e,
;;     * uploader sur le site
;;     * tester la page avec browse-url
;;     * trouver comment faire des boutons
;;     * documentation en fran�ais.


;; REVISIONS:

;; 26/05/05 0.5
;; - compl�t� et branch� les fonctions de d�couverte, spip-mode
;;   d�couvre les noms de balises, filtres etc. pour la compl�tion
;;   automatiquement si spip-racine-locale est customiz�.
;; - ajout� la fonction pour mettre un inclure (raccourcis: C-c ; i)
;;   la variable spip-racine-locale doit �tre r�gl�e pour avoir la
;;   completion des fichiers
;; - ajout� une fonction pour creer un squelette (C-c ; n)
;; - ajout� une fonction pour envoyer une partie d'un squelette dans un inclure (C-c ; k)
;;   la variable spip-repertoire-squelettes-local doit �tre r�gl�e pour que ca marche bien
;; - divis� en fichiers:
;;     o spip-outils qui contient des ptts outils
;;     o spip-decouvre pour la d�couverte de plein de chose dans les fichiers spip
;;     o spip-hil pour la coloration
;;     o spip-mode reste le fichier principal
;; - spip-regexp contient maintenant la plupart des expressions
;;   r�guli�re (pour la coloration entre autre). Calcul� avec sregex
;;   (j'ai enfin capt� le truc des backquote!!)
;; - corrig� plein de truc dans les regexps, maintenant les boucles
;;   avec des "" sont bien color�es (un keep oubli�) et les crit�res
;;   et param�tres qui contiennent des balises (avec param�tres) sont
;;   maintenant bien color�s aussi.
;; - n�toy� et ajout� de la doc.

;; 15/03/05
;; - corrig� un bug dans l'expression r�guli�re pour etendre les
;;   balises. Il va falloir sortir les expressions dans des
;;   variables. voir spip-regexp.el

;; 25/02/05 0.4
;; - des raccourcis customizable (ala ecb) pour les fonctions
;;   interactives. gr�ce au group spip-raccourcis
;; - fonction spip-etendre-balise pour rendre une balise
;;   �tendue. Ajoute [] autour de la selection et ()
;; autour de la premi�re balise dans la selection et ajoute les
;; filtres sp�cifi�s apr�s la balise. Si on passe un argument, cela
;; etend la derni�re balise dans la region.

;; 22/02/05 0.3
;; - ajout de fonctions pour inserer des balises,
;; - ajout de la completion pour les demandes dans le mini-buffer,
;; - ajout de l'historique pour les demandes dans le mini-buffer,
;; - ajout d'un menu spip.

;; 21/02/05 0.2
;; -ajout de fonctions pour inserer des boucles.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; custom

(condition-case nil					;;menu support, standard in emacs19
    (require 'auc-menu)			   
  (error (require 'easymenu)))		  
(require 'outline)

(require 'spip-hil)
(require 'spip-decouvre)
(require 'spip-regexp)

(defgroup spip nil
  "Customization for spip"
  :prefix "spip-")

;; historiques et completions

(defcustom spip-racine-locale ""
  "Chemin vers la racine locale du site spip sur lequel on travail.

Ce chemin pointe vers la racine d'installation des sources de
spip. Il n'y a pas besoin qu'un serveur web ou php tourne en
local.

Cette variable est n�cessaire si on veut que spip-mode d�couvre
les noms de tables etc... correctement pour la compl�tion. Si
cette variable n'est pas fix�e, alors spip-mode utilise des
valeurs par d�faut et les informations sp�cifiques au groupe
spip-perso.
"
  :group 'spip
  :type 'string
  )

(defcustom spip-repertoire-squelettes-local "squelettes"
  "Chemin vers le r�p�rtoire local des squelettes du site spip sur lequel on travail.

Ce r�pertoire est le repertoire des squelettes html. spip-mode
mettra les fichiers php dans le repertoire spip-racine-locale.

Cette variable est nescessaire si on veut que spip-mode puisse
cr�er des nouveaux squelettes automatiquement."
  :group 'spip
  :type 'string
  )

(defcustom spip-extension "php3"
  "Extensions de la distribution spip."
  :group 'spip
  :type '(choice :tag "extension"
				 (string :tag "Distribution de base (php3)" :value "php3")
				 (string :tag "Distribution php" :value "php"))
  )


(defvar spip-boucle-noms-hist nil
  "Historique des noms de boucle.")

(defvar spip-boucle-types-hist nil
  "Historique des types de boucle.")

(defvar spip-boucle-types-defauts '("ARTICLES"
									"AUTEURS"
									"BREVES"
									"DOCUMENTS"
									"FORUMS"
									"GROUPES_MOTS"
									"HIERARCHIE"
									"MOTS"
									"RUBRIQUES" 
									"SIGNATURES"
									"SITES" 
									"SYNDIC_ARTICLES"
									)
  "liste des types de boucles de la distribution.")

(defvar spip-boucle-types-list nil
  "Liste des types de boucle connus.")

(defgroup spip-perso nil
  "Configuration des filtres, crit�res et balises perso si on n'a pas les fichiers mes_fonctions etc en local."
  :prefix "spip-perso"
  :group 'spip)


(defcustom spip-perso-boucle-types nil 
  "Liste des types de boucle possible avec spip. 

Les types par d�faut n'ont pas � �tre ajout�s ici. Seules les
tables ajout�es � la main devraient �tre mises ici."
  :group 'spip-perso
  :type '(repeat (string :tag "Type"))
  :set (lambda (symbol value)
		 (set symbol value)
		 (if (sequencep value)
			 (set 'spip-boucle-types-list (append value spip-boucle-types-defauts))
		   (set 'spip-boucle-types-list spip-boucle-types-defauts)
		   )
		 )
  )

(defvar spip-criteres-hist nil
  "Historique des criteres de boucle.")

(defvar spip-criteres-defauts '("ad_email"
								"age"
								"age_relatif"
								"branche"
								"date"
								"debut_"
								"doublons"
								"exclus"
								"extension"
								"id_article"
								"id_auteur"
								"id_breve"
								"id_document"
								"id_enfant"
								"id_forum"
								"id_groupe"
								"id_mot"
								"id_parent"
								"id_rubrique"
								"id_secteur"
								"id_signature"
								"id_syndic"
								"id_syndic_article"
								"inverse"
								"lang"
								"meme_parent"
								"mode"
								"moderation"
								"nom_email"
								"origine_traduction"
								"par "
								"par hasard"
								"par num "
								"par points"
								"plat"
								"racine"
								"recherche"
								"syndication"
								"titre"
								"titre_mot"
								"traduction"
								"type"
								"type_mot"
								"inverse"
								"fragment"
								"pagination"
								)
  "Liste des criteres de boucle possibles dans la distribution.")

(defvar spip-criteres-list nil
  "Liste des criteres de boucle connus.")

(defcustom spip-perso-criteres nil 
  "Liste des criteres de boucle possibles avec spip. 

Les criteres par d�faut n'ont pas � �tre ajout�s ici. Seuls les
crit�res ajout�s � la main devraient �tre mis ici."
  :group 'spip-perso
  :type '(repeat (string :tag "Criteres"))
  :set (function (lambda (symbol value)
                   (set symbol value)
				   (if (sequencep value)
					   (set 'spip-criteres-list (append value spip-criteres-defauts))
					 (set 'spip-criteres-list spip-criteres-defauts)
					 )
				   )
				 )
  )


;; fonction d'ajouts
;;demande des parametres d'une boucle (criteres, nom, type)

;;nom

(defun spip-ask-nom ()
  "Demande le nom d'une boucle. Fait les verifications de validit� du nom. 
Si c'est un nom qui ne contient pas que des chiffres et qui ne commence pas par _,
 on ajoute un _ avant."
  (let (
		(nom (read-from-minibuffer "Nom: " nil nil nil 'spip-boucle-noms-hist))
		)
	(cond ((string-match "^[0-9]+$" nom) nom)
		  ((string-match "^_\\(\\w\\|_\\)+$" nom) nom)
		  ((string-match "^\\(\\w\\|_\\)+$" nom) (concat "_" nom))
		  (t (error "'%s' n'est pas un nom de boucle valide" nom))
		  )
	)
  )

;;type

(defun spip-ask-type ()
  "Demande le type de la table sur laquelle faire la boucle."
  (spip-completing-read "Type" spip-boucle-types-list nil 'spip-boucle-types-hist)
  ;;ici on ne devrait pas laisser les gens mettre n'importe quoi, mais on est oblig� pour l'instant � cause des boucles recursive.
  )

;;criteres
(defun spip-ask-criteres ()
  "Demande une liste de criteres pour une boucle. 
On sarrete si le critere entr� est une cha�ne vide."
  (let (
		(criteres "")
		(new-crit)
		)
	(while (setq new-crit (spip-ask-critere))
	  (setq criteres (concat criteres " " new-crit))
	  )
	criteres
	)
  )

(defun spip-ask-critere ()
  "Demande un critere.
Ajoute les {} autours s'ils n'y sont pas d�j�."
  (let (
		(crit (spip-completing-read "Critere" spip-criteres-list nil 'spip-criteres-hist))
		)
	(cond ((string= crit "") nil)
		  ((string-match "{[^}]*}" crit) crit)
		  (t (concat "{" crit "}"))
		  )
	)
  )

(defun spip-insert-boucle (condi)
  "Ajoute une boucle. Si un param�tre lui est donn�,
 alors on ajoute aussi les parties conditionnelles avant et apres." 
  (interactive "P")
  (let (
 		(nom-boucle (spip-ask-nom))
 		(type-boucle (spip-ask-type))
 		(criteres (spip-ask-criteres))
 		)
	(if condi
		(insert (concat "<B" nom-boucle ">\n"))
	  )
	(insert (concat "<BOUCLE" nom-boucle "(" type-boucle ")" criteres ">\n"))
	(insert "</BOUCLE" nom-boucle ">")		
	(if condi
		(progn
		  (insert (concat "\n</B" nom-boucle ">"))
		  (insert (concat "\m<//B" nom-boucle ">"))
		  )
	  )
	)
  )			  


;;pour ajouter une boucle simple autour d'une region
(defun spip-insert-boucle-region (condi beg end)
  "ajouter une boucle autour du code selectionn�. Si un param�tre lui est donn�,
 alors on ajoute aussi les parties conditionnelles avant et apres."
  (interactive "P\nr")
  (let (
		(to (make-marker))
		(nom-boucle (spip-ask-nom))
 		(type-boucle (spip-ask-type))
 		(criteres (spip-ask-criteres))
		indent-region-function)
    (set-marker to
				(save-excursion
				  (goto-char end)
				  (if (and (bolp)
						   (not (= beg end)))
					  (point)
					(end-of-line)
					(1+ (point)))))
	(goto-char beg)
    (beginning-of-line)	       
	(if condi
		(insert (concat "<B" nom-boucle ">\n"))
	  )
	(insert (concat "<BOUCLE" nom-boucle "(" type-boucle ")" criteres ">\n"))
    (indent-region (point) to nil)
    (goto-char to)
    (insert "\n</BOUCLE" nom-boucle ">")
	(if condi
		(progn
		  (insert (concat "\n</B" nom-boucle ">"))
		  (insert (concat "\n<//B" nom-boucle ">"))
		  )
	  )
	)
  )

;; Balise

(defvar spip-filtres-defauts '("PtoBR"
							   "abs_url"
							   "affdate"
							   "affdate_base"
							   "affdate_court"
							   "affdate_heure"
							   "affdate_jourcourt"
							   "affdate_mois_annee"
							   "afficher_enclosures"
							   "afficher_tags"
							   "agenda_affiche"
							   "agenda_connu"
							   "agenda_memo"
							   "aligner"
							   "aligner_droite"
							   "aligner_gauche"
							   "alterner"
							   "annee"
							   "antispam"
							   "attribut_html"
							   "barre_textarea"
							   "boutonne"
							   "calcul_bornes_pagination"
							   "calcul_pagination"
							   "centrer"
							   "choixsiegal"
							   "choixsivide"
							   "concat"
							   "corriger_caracteres"
							   "corriger_entites_html"
							   "corriger_toutes_entites_html"
							   "couleur_extraire"
							   "date_anneemois"
							   "date_anneemoisjour"
							   "date_debut_semaine"
							   "date_fin_semaine"
							   "date_ical"
							   "date_interface"
							   "date_iso"
							   "date_relative"
							   "direction_css"
							   "div"
							   "echapper_tags"
							   "email_valide"
							   "enclosure2microformat"
							   "entites_html"
							   "entites_unicode"
							   "env_to_attributs"
							   "env_to_params"
							   "extra"
							   "extraire_attribut"
							   "extraire_balise"
							   "extraire_date"
							   "extraire_multi"
							   "extraire_tag"
							   "extraire_tags"
							   "extraire_trad"
							   "f_jQuery"
							   "fichier"
							   "filtrer_entites"
							   "filtrer_ical"
							   "form_hidden"
							   "hauteur"
							   "heures"
							   "heures_minutes"
							   "image_filtrer"
							   "image_typo"
							   "in_any"
							   "inserer_attribut"
							   "jour"
							   "journum"
							   "justifier"
							   "largeur"
							   "liens_absolus"
							   "liens_ouvrants"
							   "lignes_longues"
							   "majuscules"
							   "match"
							   "microformat2enclosure"
							   "minutes"
							   "modulo"
							   "moins"
							   "mois"
							   "mult"
							   "multi_trad"
							   "nom_jour"
							   "nom_mois"
							   "normaliser_date"
							   "paragrapher"
							   "plus"
							   "post_autobr"
							   "recup_date"
							   "recup_heure"
							   "recuperer_numero"
							   "reduire_image"
							   "replace"
							   "resolve_path"
							   "saison"
							   "secondes"
							   "sinon"
							   "sinon{}"
							   "spip_version"
							   "style_align"
							   "suivre_lien"
							   "supprimer_caracteres_illegaux"
							   "supprimer_numero"
							   "supprimer_tags"
							   "table_valeur"
							   "tags2dcsubject"
							   "taille_en_octets"
							   "tester_config"
							   "texte_backend"
							   "texte_script"
							   "textebrut"
							   "traduire_nom_langue"
							   "traiter_doublons_documents"
							   "unique"
							   "url_absolue"
							   "url_reponse_forum"
							   "url_rss_forum"
							   "url_var_recherche"
							   "valeur_numerique"
							   "valeurs_image_trans"
							   "vide"
							   "vider_attribut"
							   "vider_date"
							   "vider_url"
							   "filtrer"
							   )
  "Liste des filtres existant dans la distribution.")

(defvar spip-filtres-list nil
  "Liste des filtres connus.")

(defvar spip-filtres-hist nil
  "historique des filtres utilis�es.")


(defcustom spip-perso-filtres nil
  "Liste des filtres ajout� � spip.

 Les filtres par d�faut n'ont pas � �tre ajout�s ici. Seuls les
 filtres ajout�s � la main devraient �tre mis ici."
  :group 'spip-perso
  :type '(repeat (string :tag "Filtre"))
  :set (function (lambda (symbol value)
                   (set symbol value)
				   (if (sequencep value)
					   (set 'spip-filtres-list (append value spip-filtres-defauts))
					 (set 'spip-filtres-list spip-filtres-defauts)
					 )
				   )
				 )
  )

(defvar spip-balises-defauts '("ANCRE_PAGINATION"
							   "BIO"
							   "CHAPO"
							   "CHARSET"
							   "DATE"
							   "DATE_MODIF"
							   "DATE_NOUVEAUTES"
							   "DATE_REDAC"
							   "DESCRIPTIF"
							   "DESCRIPTIF_SITE_SPIP"
							   "EMAIL"
							   "EMAIL_WEBMASTER"
							   "EMBED_DOCUMENT"
							   "EXPOSE"
							   "EXPOSER"
							   "FORMULAIRE_ADMIN"
							   "FORMULAIRE_ECRIRE_AUTEUR"
							   "FORMULAIRE_FORUM"
							   "FORMULAIRE_INSCRIPTION"
							   "FORMULAIRE_RECHERCHE"
							   "FORMULAIRE_SIGNATURE"
							   "FORMULAIRE_SITE"
							   "HAUTEUR"
							   "ID_ARTICLE"
							   "ID_AUTEUR"
							   "ID_BREVE"
							   "ID_DOCUMENT"
							   "ID_FORUM"
							   "ID_GROUPE"
							   "ID_MOT"
							   "ID_RUBRIQUE"
							   "ID_SECTEUR"
							   "ID_SIGNATURE"
							   "ID_SYNDIC"
							   "ID_SYNDIC_ARTICLE"
							   "INTRODUCTION"
							   "IP"
							   "LANG"
							   "LANG_DIR"
							   "LANG_LEFT"
							   "LANG_RIGHT"
							   "LARGEUR"
							   "LESAUTEURS"
							   "LOGIN_PRIVE"
							   "LOGIN_PUBLIC"
							   "LOGO_ARTICLE"
							   "LOGO_ARTICLE_NORMAL"
							   "LOGO_ARTICLE_RUBRIQUE"
							   "LOGO_ARTICLE_SURVOL"
							   "LOGO_AUTEUR"
							   "LOGO_AUTEUR_NORMAL"
							   "LOGO_AUTEUR_SURVOL"
							   "LOGO_BREVE"
							   "LOGO_BREVE_RUBRIQUE"
							   "LOGO_DOCUMENT"
							   "LOGO_MOT"
							   "LOGO_RUBRIQUE"
							   "LOGO_RUBRIQUE_NORMAL"
							   "LOGO_RUBRIQUE_SURVOL"
							   "LOGO_SITE"
							   "LOGO_SITE_SPIP"
							   "MENU_LANG"
							   "MENU_LANG_ECRIRE"
							   "MESSAGE"
							   "NOM"
							   "NOM_SITE"
							   "NOM_SITE_SPIP"
							   "NOTES"
							   "PARAMETRES_FORUM"
							   "PGP"
							   "POINTS"
							   "POPULARITE"
							   "POPULARITE_ABSOLUE"
							   "POPULARITE_MAX"
							   "POPULARITE_SITE"
							   "PS"
							   "PUCE"
							   "RANG"
							   "RECHERCHE"
							   "SOUSTITRE"
							   "SURTITRE"
							   "SPIP_CRON"
							   "TAILLE"
							   "TEXTE"
							   "TITRE"
							   "TYPE"
							   "TYPE_DOCUMENT"
							   "URL_ARTICLE"
							   "URL_AUTEUR"
							   "URL_BREVE"
							   "URL_DOCUMENT"
							   "URL_LOGOUT"
							   "URL_MOT"
							   "URL_RUBRIQUE"
							   "URL_SITE"
							   "URL_SITE_SPIP"
							   "URL_SYNDIC"
							   "VISITES"
							   "GRAND_TOTAL"
							   "PAGINATION"
							   "CHEMIN"
							   "ENV"
							   "CONFIG"
							   "EVAL"
							   "REM"
							   "HTTP_HEADER"
							   "CACHE"
							   "INSERT_HEAD"
							   "INCLUDE"
							   "INCLURE"
							   "MODELE"
							   "SET"
							   "GET"
							   "PIPELINE"
							   "TOTAL_UNIQUE"
							   "FIN_SURLIGNE"
							   "EXTRA"
							   "DEBUT_SURLIGNE"
							   "SPIP_VERSION"
							   )
  "Liste des balises existantes dans la distribution.")

(defvar spip-balises-hist nil
  "historique des balises utilis�es.")

(defvar spip-balises-list nil
  "Liste des balises connus.")

(defcustom spip-perso-balises nil 
  "Liste des balises ajout�s � spip.

 Les balises par d�faut n'ont pas � �tre ajout�s ici. Seuls les
 balises ajout�s � la main devraient �tre mis ici."
  :group 'spip-perso
  :type '(repeat (string :tag "Balise"))
  :set (function (lambda (symbol value)
                   (set symbol value)
				   (if (sequencep value)
					   (set 'spip-balises-list (append value spip-balises-defauts))
					 (set 'spip-balises-list spip-balises-defauts)
					 )
				   )
				 )
  )

(defun spip-ask-filtres ()
  "Demande une liste de filtres pour une balise �tendue. 
On s'arrete si le filtre entr� est une cha�ne vide."
  (let (
		(filtres "")
		(new-filtres)
		)
	(while (setq new-filtres (spip-ask-filtre))
	  (setq filtres (concat filtres "|" new-filtres))
	  )
	filtres
	)
  )

(defun spip-ask-filtre ()
  "Demande un filtre."
  (let (
		(filtre (spip-completing-read "Filtre" spip-filtres-list nil 'spip-filtres-hist))
		)
	(if (string= filtre "") 
		nil
	  filtre
	  )
	)
  )

(defun spip-ask-balise ()
  "demande un nom de balise"
  (let (
		(balise (spip-completing-read "Balise" spip-balises-list nil 'spip-balises-hist))
		)
	(cond ((string-match "^#\\(\\w\\|_\\)+\\*?\\({[^}]*}\\)?$" balise) balise)
		  ((string-match "^\\(\\w\\|_\\)+\\*?\\({[^}]*}\\)?$" balise) (concat "#" balise))
		  (t (error "'%s' n'est pas un nom de balise valide" balise))
		  )
	)
  )

(defun spip-insert-balise (etendue)
  "insert une balise. 

Si on l'appelle avec C-u, alors on insert une balise �tendue."
  (interactive "P")
  (let (
		(balise (spip-ask-balise))
		(avant (if etendue (read-from-minibuffer "Code Avant: ") nil))
		(apres (if etendue (read-from-minibuffer "Code Apres: ") nil))
		(filtres (if etendue (spip-ask-filtres) nil))
		)
	(if etendue (insert (concat "[" avant "(")))
	(insert balise)
	(if etendue (insert (concat filtres ")" apres "]")))
	)
  )

(defun spip-etendre-balise (beg end condi)
  "�tend une balise simple dans la selection.

La balise la plus � droite dans la selection est �tendue, le
reste devient du code optionel.  Si on l'appel avec C-u, alors la
balise la plus � gauche est �tendue."
  (interactive "r\nP")
  (let ( (to (make-marker)) (from (make-marker))
		 (debut-bal (make-marker))  (fin-bal (make-marker))
		 (filtres "")
		 (test nil)
		 )
	(if condi
		(progn
		  (set-marker to
 					  (progn
 						(goto-char end)
 						(point)))
		  (set 'test (search-backward-regexp spip-reg-balise beg t))
		  )
	  (set-marker from
				  (progn
					(goto-char beg)
					(point)))
	  (set 'test (search-forward-regexp spip-reg-balise end t))
	  )
	(if test		
		(progn 		 
		  (if condi 
			  (progn
				(set-marker debut-bal (point))
				(set-marker fin-bal (match-end 0))
				(set-marker from
							(save-excursion
							  (goto-char beg)
							  (point)))
				)
			(set-marker fin-bal (point))
			(set-marker debut-bal (match-beginning 0))
			(set-marker to
						(save-excursion
						  (goto-char end)
						  (point)))
			)
		  (goto-char to)
		  (insert "]")
		  (goto-char fin-bal)
		  (set 'filtres (spip-ask-filtres))
		  (insert (concat filtres ")"))
		  (goto-char debut-bal)
		  (insert "(")
		  ;;(goto-char (+ (+ 1 (length filtres)) to))
		  (goto-char from)
		  (insert "[")
		  )
	  )
	)
  )

(defvar spip-fichier-php-hist nil
  "Historique des fond html cherch�s.")

(defun spip-insert-inclure ()
  "ins�re la balise <INCLURE{fond=...}>"
  (interactive)
  (let
	  ( 
	   (fichier (spip-completing-read "Fond" (spip-list-php-file ".*" nil t) nil 'spip-fichier-php-hist))
	   (criteres (spip-ask-criteres))
	  )
	(insert (concat "<INCLURE{fond=" (car (split-string fichier "\\.")) "}" criteres ">"))
	)
)


;; nouveau squelette
(defvar spip-nouveau-squelette-hook nil
  "fonction � appliquer apres avoir creer le fichier html d'un squelette.")

(defun spip-nouveau-squelette (fond delais)
  "cr�e une nouvelle paire de fichiers php+html pour faire un nouveau squelette."
  (interactive "sNom du Fond: \nsDelais: ")
	(if (not (file-exists-p (spip-construit-chemin fond)))
		(progn
		;;   ;; fichier php
;; 			  (find-file (spip-construit-chemin fond))
;; 			  (erase-buffer)
;; 			  (insert "<?php \n\n")
;; 			  (insert " $fond = \"")
;; 			  (insert fond)
;; 			  (insert "\";\n")
;; 			  (insert " $delais = ")
;; 			  (insert delais)
;; 			  (insert ";\n\n")
;; 			  (insert (concat "include('inc-public." spip-extension "');\n"))
;; 			  (insert "\n?>\n")
;; 			  (save-buffer)
;; 			  (kill-buffer nil)
			  ;; fichier html
			  (find-file (spip-construit-chemin fond (concat "/" spip-repertoire-squelettes-local) "html"))
			  (erase-buffer)
 			  (insert (concat "#CACHE{" delais "}"))
			  (insert "\n")
			  (run-hooks spip-nouveau-squelette-hook)
			  (save-buffer)
			  )
	  (message "Un squelette de ce nom existe d�j�.")
	  )
	)

;; envoyer la r�gion dans un nouveau squelette que l'on inclus

(defun spip-transfert-inclure (fond delais beg end)
  "remplacer la r�gion par un <INCLURE()> d'un nouveau squelette qui contient la r�gion."
  (interactive "sNom du Fond: \nsDelais: \nr")

  (kill-region beg end)
  (insert (concat "<INCLURE('" fond "." spip-extension "')" (spip-ask-criteres) ">"))
  (spip-nouveau-squelette fond delais)
  (yank)
)


;; fonction d'indentation. Malheureusement, elle n'est pas complete. Les parties avant et apres la boucle principale ne sont pas indente.

(defun spip-look-indent-back ()
  (if (bobp)
	  '(nil 0)
	(cond
	 ((looking-at (concat "^[ \t]*" spip-reg-fermeture-boucle))
	  (forward-line -1)
	  (if (< (- (current-indentation) default-tab-width) 0)
		  '(nil 0)
		(list nil (- (current-indentation) default-tab-width))
		)
	  )
	 ((looking-at (concat "^[ \t]*" spip-reg-ouverture-boucle))
	  (forward-line -1)
	  (list nil (+ (current-indentation) default-tab-width))
	  )
	 ((looking-at (concat "^[ \t]*" spip-reg-ouverture-boucle-opt))
	  (forward-line -1)
	  (list t (+ (current-indentation) default-tab-width))
	  )
	 (t 
	  (forward-line -1)
	  (spip-look-indent-back)
	  )
	 )	
	)
  )

(defun spip-indent-rec ()
  "indent"
  (interactive)
  (beginning-of-line)
  (let (cur-indent)
	(save-excursion
	  (setq cur-indent (spip-look-indent-back))
	  )
	(cond ((car cur-indent)
			 (cond ((looking-at (concat "^[ \t]*" spip-reg-ouverture-boucle))
					(indent-line-to (if (< (- (car (cdr cur-indent)) default-tab-width) 0)
										0
									  (- (car (cdr cur-indent)) default-tab-width)
									  )
									)
					)
				   ((looking-at (concat "^[ \t]*" spip-reg-ouverture-boucle-opt))
					(indent-line-to (if (< (- (car (cdr cur-indent)) default-tab-width) 0)
										0
									  (- (car (cdr cur-indent)) default-tab-width)
									  )
									)
					)
				   (t 
					(indent-line-to (car (cdr cur-indent)))	  
					(message "ouvop not ouv")
					)
				   )
			 )
		  ((looking-at (concat "^[ \t]*" spip-reg-ouverture-boucle))
		   (indent-line-to (if (< (- (car (cdr cur-indent)) default-tab-width) 0)
							   0
							 (- (car (cdr cur-indent)) default-tab-width)
									)
						   )
		   )
		  ((looking-at (concat "^[ \t]*" spip-reg-ouverture-boucle-opt))
		   (indent-line-to (if (< (- (car (cdr cur-indent)) default-tab-width) 0)
							   0
							 (- (car (cdr cur-indent)) default-tab-width)
							 )
						   )
		   )
		  ((looking-at (concat "^[ \t]*\\(?:" spip-reg-fermeture-boucle "\\|" spip-reg-fermeture-boucle-opt "\\|" spip-reg-fermeture-boucle-alt "\\)"))
		   (indent-line-to (if (< (- (car (cdr cur-indent)) default-tab-width) 0)
							   0
							 (- (car (cdr cur-indent)) default-tab-width)
							 )
						   )
		   )
			(t 
			 (indent-line-to (car (cdr cur-indent)))
			 (message "not ouv-opt")
			 )
			)
	)
  )


(defun spip-update-criteres-list ()
  "recherche les nom de crit�res dans les fichiers php."
  (interactive)
  (mapcar (lambda (crit) 
 			(if (not (member crit spip-criteres-list))
 				(set 'spip-criteres-list (append spip-criteres-list (list crit)))
 			  )
 			)
 		  (spip-decouvre-criteres)
 		  )
  )

(defun spip-update-balises-list ()
  "recherche les nom de balises dans les fichiers php."
   (interactive)
   (mapcar (lambda (bal) 
			 (if (not (member bal spip-balises-list))
 				(set 'spip-balises-list (append spip-balises-list (list bal)))
 			  )
 			)
 		  (spip-decouvre-balises)
 		  )
 )

(defun spip-update-boucles-list ()
    "recherche les nom de boucles dans les fichiers php."
   (interactive)
   (mapcar (lambda (bou) 
 			(if (not (member bou spip-boucle-types-list))
 				(set 'spip-boucle-types-list (append spip-boucle-types-list (list bou)))
 			  )
 			)
 		  (spip-decouvre-boucles)
 		  )
 )

(defun spip-update-filtres-list ()
    "recherche les nom de filtres dans les fichiers php."
   (interactive)
   (mapcar (lambda (filt) 
 			(if (not (member filt spip-filtres-list))
 				(set 'spip-filtres-list (append spip-filtres-list (list filt)))
 			  )
 			)
 		  (spip-decouvre-filtres)
 		  )
   )


;; menu
(defvar spip-mode-menu-list '("SPIP"
							  ["Nouveau Squelette" spip-nouveau-squelette t]
							  ["Ajouter Boucle" spip-insert-boucle t]
							  ["Ajouter Boucle Etendue" (spip-insert-boucle t) t]
							  ["Entourer d'une Boucle" spip-insert-boucle-region t]
							  ["Ajouter une Balise" spip-insert-balise t]
							  ["Ajouter une Balise Etendue" (spip-insert-balise t) t]
							  ["Etendre la Balise" spip-etendre-balise t]
							  ["Inclure un fichier" spip-insert-inclure t]
							  ["Transferer dans un inclure" spip-transfert-inclure t]
							  )
  )

(define-derived-mode spip-mode html-mode "SPIP"
  "Mode d'�dition pour les squelettes SPIP.

 On peut facilement ins�rer un boucle, une balise, etc...
la coloration syntaxique permet d'avoir une meilleure vue des squelettes.

\\{spip-mode-map}
"
  (font-lock-add-keywords
   nil
   spip-font-lock-keywords)
;;  (make-local-variable 'indent-line-function)
  (make-local-variable 'comment-start)
  (make-local-variable 'comment-end)
  (make-local-variable 'comment-column)
  (make-local-variable 'comment-start-skip)
  (setq ;;indent-line-function 'spip-indent-line
		comment-start "<!-- "
		comment-end " -->"
		comment-start-skip "<!--[ \t]*"
		comment-column 0)
  (spip-update-filtres-list)
  (spip-update-balises-list)
  (spip-update-boucles-list)
  (spip-update-criteres-list)
  (easy-menu-add spip-mode-menu-list)	; for XEmacs
  (easy-menu-define spip-mode-menu spip-mode-map
	"Spip Menu." spip-mode-menu-list)
  )

(defgroup spip-raccourcis nil
  "Configuration des touches."
  :prefix "spip-raccourcis"
  :group 'spip)

;; key definitions in customize. Code comming from ecb: http://ecb.sourceforge.net/
(defun ecb-copy-list (list)
  "Return a copy of a list, which may be a dotted list.
The elements of the list are not copied, just the list structure itself."
  (if (consp list)
      (let ((res nil))
		(while (consp list) (push (pop list) res))
		(prog1 (nreverse res) (setcdr res list)))
    (car list)))

(defcustom spip-raccourcis-table
  '("C-c ;" . ((t "n" spip-nouveau-squelette)
			   (t "b" spip-insert-boucle)
               (t "r" spip-insert-boucle-region)
               (t "t" spip-insert-balise)
               (t "x" spip-etendre-balise)
               (t "i" spip-insert-inclure)
               (t "k" spip-transfert-inclure)
			   )
	)

  "raccourcis clavier pour emacs."
  :group 'spip-raccourcis
  :type '(cons (choice :tag "Touche de pr�fixe"
                       (const :tag "Sans touche pr�fixe" :value nil)
                       (string :tag "Touche Pr�fixe" :value "C-c ;"))
               (repeat :tag "Raccourcis"
                       (list :tag "D�finition de Raccourcis"
                             (boolean :tag "o Utiliser le pr�fixe" :value t)
                             (string :tag "o Touches")
                             (function :tag "o Fonction ou expression lambda"
                                       :value nil))))
  :set (function (lambda (symbol value)
                   (set symbol value)
                   ;; make a mode-map and save it
                   (setq spip-mode-map
                         (let ((km (make-sparse-keymap))
                               (val-list (ecb-copy-list (cdr value)))
                               keq-string)
                           (dolist (elem val-list)
                             (setq key-string (concat (if (nth 0 elem) (car value))
                                                      " " (nth 1 elem)))
                             (define-key km (read-kbd-macro key-string) (nth 2 elem)))
						   (easy-menu-define spip-mode-menu km
							 "Spip Menu." spip-mode-menu-list)
                           km))
                   )
				 )
  )

(provide 'spip-mode)

