Auteur: Pierre Andrews (aka Mortimer) mortimer.pa@free.fr
Description:
Mode d'�dition pour Emacs ajoutant des raccourcis et la coloration syntaxique pour l'�criture facile de vos squelettes.
Regles de commit: voir le fichier _REGLES_DE_COMMIT.txt
Listes des fichiers:
spip-decouvre.el fonctions et customizations pour la d�couverte des filtres, balises etc de spip dans les fichiers de la distribution
spip-hil.el d�finition des "faces" pour la coloration syntaxique
spip-mode.el  le fichier principal 
spip-outils.el quelques fonctions "outils"
spip-regexp.el d�finition des expressions r�guli�res pour parser les squelettes
spip-sregex.el fonction sregex de emacs pour XEmacs (fichier venant de la distrib Emacs)

Page de la contrib: www.spip-contrib.net/spikini/SpipMode


