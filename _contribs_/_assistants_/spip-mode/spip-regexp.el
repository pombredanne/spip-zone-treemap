;;; spip-regexp.el --- spip-mode

;; Copyright (C) 2005 Pierre Andrews

;; Authors: Pierre Andrews (mortimer.pa@free.fr)
;; Keywords: spip, cms, derived mode
;; Version: 0.2

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;;construction des regexps. 
;; on contruit ici les regexp pour la coloration syntaxique
;; etc... Attention, cela utilise sregex qui n'est pas toujours
;; fournis avec XEmacs

(require 'spip-outils)
(spip-require 'sregex)

(defconst spip-reg-mot '(1+ (or wordchar "_")))

(defconst spip-reg-whitespace '(0+ (syntax ?-)))
(defconst spip-reg-nom-boucle `(or 
					   (1+ (char (?0 . ?9)))
					   (sequence "_"
								 ,spip-reg-mot
								 )
					   )
  )
(defconst spip-reg-criteres  `(0+ "{"
								  (1+ (not-char "}"))
								  (opt "{"
									   (1+ (not-char "}"))									   
									   "}")
								  "}"
								  ,spip-reg-whitespace
								  )
  )

(defconst spip-reg-nom-balise `(sequence
					   (opt
						,spip-reg-nom-boucle
						":"		   
						)
					   ,spip-reg-mot
					   (opt "*")
					   )
  )


(defconst spip-reg-param-balise '(opt "{"
									  (1+ (not-char "}"))
									  (opt "{"
										   (1+ (not-char "}"))									   
										   "}")
									  "}"
									  )
  )

;;des filtres

(defconst spip-reg-filtre
  `(sequence "|"
			 (or ,spip-reg-mot
				 "=="
				 "!="
				 "<>"
				 "?"
				 ">"
				 ">="
				 "<"
				 "<="
				 "!="
				 "==="
				 "!=="
				 )
			 ,spip-reg-param-balise
			 )
)

;;balise spip
(defconst spip-reg-balise 
  (sregex "#"
		  spip-reg-nom-balise
		  spip-reg-param-balise
		  )
  )





;;balise etendue
(defconst spip-reg-balise-etendue  
  (sregex "(#"
		  spip-reg-nom-balise
		  spip-reg-param-balise
		  `(group (0+ 
			 (opt "|")
			 ,spip-reg-filtre)
				  )
		  ")"
		  )
  )
 		   

;;ouverture boucle
(defconst spip-reg-ouverture-boucle (sregex "<BOUCLE"
								   spip-reg-nom-boucle
								   spip-reg-whitespace
								   "("
								   '(group (1+ (not-char ")")))
								   ")"
								   spip-reg-whitespace
								   `(group ,spip-reg-criteres)
								   ">"
								   )
  )

(defconst spip-reg-ouverture-boucle-opt (sregex 
								"<B"
								spip-reg-nom-boucle
								">"
								)
  )

;; fermeture boucle
(defconst spip-reg-fermeture-boucle  (sregex 
							 "</BOUCLE"
							 spip-reg-nom-boucle
							 ">"
							 )
  )

(defconst spip-reg-fermeture-boucle-opt  (sregex 
								 "</B"
								 spip-reg-nom-boucle
								 ">"
								 )
  )

(defconst spip-reg-fermeture-boucle-alt  (sregex 
								 "<//B"
								 spip-reg-nom-boucle
								 ">"
								 )
  )


;;inclure
(defconst spip-reg-inclure (sregex "<INCLU"
						  '(or "D" "R")
						  "E"
			 			  '(or
 							(sequence 
							 (0+ (syntax ?-))
 							 "("
							 (group (1+ (not-char ")")))
 							 ")"
 							 )
							(group 
							 "{fond="
							 (1+ (not-char "}"))
							 "}"
							 )
 							)
						  spip-reg-whitespace
						  `(group ,spip-reg-criteres)
						  ">"
						  )
  )


;;traduction
(defconst spip-reg-traduction  (sregex "<:"
							  `(opt
								,spip-reg-mot
								":"
								)
							  spip-reg-mot
							  '(group (opt 
									   "|"
									   (1+ (not-char ?:))
									   )
									  )
							  ":>"
							  )
  )

(provide 'spip-regexp)