;;; spip-decouvre.el --- spip-mode

;; Copyright (C) 2005 Pierre Andrews

;; Authors: Pierre Andrews (mortimer.pa@free.fr)
;; Keywords: spip, cms, derived mode
;; Version: 0.2

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:
;; fonctions pour d�couvrir les d�finitions des �l�ments de SPIP dans
;; les fichiers sources de la distribution. Utilis� pour fournir les
;; completions.

(defgroup spip-decouvre nil
  "Groupe pour la configuration des fonctions de decouverte des filtres, boucles et crit�res existants. Vous ne devriez pas � avoir � modifier ces expressions par vous m�me.

Si vous savez ce que vous faites, vous pouvez configurer ici les
noms de filtres etc.. que spip-mode va d�couvrire
automatiquement. Ainsi, si certaines fonctions dans mes_fonctions
doivent �tre ignor�es, il faut modifier l'expression r�guli�re en
cons�quence."
  :group 'spip
  :prefix "spip-decouvre"
  )

(defcustom spip-decouvre-filtres-regexp "^\\s-*function\\s-*\\([^(]+\\)\\s-*("
  "Expression r�guli�re pour extraire les d�finitions de filtres."
  :group 'spip-decouvre
  :type 'regexp
  )

(defcustom spip-decouvre-filtres-numeros-regexp 1 
  "Num�ros du groupe contenant le nom du filtre."
  :group 'spip-decouvre
  :type 'number
)

(defcustom spip-decouvre-filtres-ignore-regexp "^\\(boucle_.*\\|balise_.*\\|critere_.*\\|.*_dist\\)$"
  "Regexp sp�cifiant les fonctions � ne pas prendre comme filtres."
  :group 'spip-decouvre
  :type 'regexp
  )

(defcustom spip-decouvre-criteres-regexp "^\\s-*function\\s-*critere_\\([^(]+\\)\\(?:_dist\\)?\\s-*("
  "Expression r�guli�re pour extraire les d�finitions de crit�res des fonctions php."
  :group 'spip-decouvre
  :type 'regexp
  )

(defcustom spip-decouvre-criteres-numeros-regexp 1 
  "Num�ros du groupe contenant le nom de crit�re."
  :group 'spip-decouvre
  :type 'number
)

(defcustom spip-decouvre-criteres-ignore-regexp nil
  "Regexp sp�cifiant les fonctions � ne pas prendre comme critere."
  :group 'spip-decouvre
  :type 'regexp)

(defcustom spip-decouvre-balises-regexp "^\\s-*function\\s-*balise_\\([^(]+\\)\\(?:_dist\\)?\\s-*("
  "Expression r�guli�re pour extraire les d�finitions de balise."
  :group 'spip-decouvre
  :type 'regexp
  )

(defcustom spip-decouvre-balises-numeros-regexp 1 
  "Num�ros du groupe contenant le nom de balise."
  :group 'spip-decouvre
)

(defcustom spip-decouvre-balises-ignore-regexp nil
  "Regexp sp�cifiant les fonctions � ne pas prendre comme balise."
  :group 'spip-decouvre
  :type 'regexp)

(defcustom spip-decouvre-boucles-regexp "^\\s-*function\\s-*boucle_\\([^(]+?\\)\\(?:_dist\\)?\\s-*("
  "Expression r�guli�re pour extraire les d�finitions de boucles des definitions de fonctions"
  :group 'spip-decouvre
  :type 'regexp
  )

(defcustom spip-decouvre-boucles-numeros-regexp 1 
  "Num�ros du groupe contenant le nom de boucle."
  :group 'spip-decouvre
  :type 'number
)

(defcustom spip-decouvre-boucles-ignore-regexp nil
  "Regexp sp�cifiant les fonctions � ne pas prendre comme boucle."
  :group 'spip-decouvre
  :type 'regexp)


(defcustom spip-decouvre-nom-colonne-regexp "\"\\([a-zA-Z_]+\\)\"\\s-*=>"
  "Expression r�guli�re pour extraire les d�finitions des declarations de tables"
  :group 'spip-decouvre
  :type 'regexp
  )

(defcustom spip-decouvre-nom-colonne-numeros-regexp 1 
  "Num�ros du groupe contenant le nom de colonne."
  :group 'spip-decouvre
  :type 'number
)

(defcustom spip-decouvre-nom-colonne-ignore-regexp "pass"
  "Regexp sp�cifiant les fonctions � ne pas prendre comme colonne."
  :group 'spip-decouvre
  :type 'regexp)


(defcustom spip-decouvre-nom-table-regexp "tables_principales\\['\\(?:spip_\\)?\\([a-zA-Z_]+?\\)'\\]"
  "Expression r�guli�re pour extraire les d�finitions des declarations de tables"
  :group 'spip-decouvre
  :type 'regexp
  )

(defcustom spip-decouvre-nom-table-numeros-regexp 1 
  "Num�ros du groupe contenant le nom de table."
  :group 'spip-decouvre
  :type 'number
)

(defcustom spip-decouvre-nom-table-ignore-regexp nil
  "Regexp sp�cifiant les fonctions � ne pas prendre comme nom de table."
  :group 'spip-decouvre
  :type 'regexp)


(require 'spip-outils)
(defun spip-decouvre-filtres ()
  "Trouve les filtres d�clar�s dans les fichiers php."
  (let ((filtres nil)
		(fichiers)
		)
	;;TODO rendre cela customizable
	;; on cherche dans les fichiers mes_fonctions et dans inc_filtres
	(set 'fichiers (spip-list-php-file "^mes_fonctions.*"))
	(set 'fichiers (append fichiers (list (spip-construit-chemin "inc_filtres" "/ecrire"))))
	(mapcar (lambda (fichier)
			  (set 'filtres (append filtres (spip-decouvre-generique fichier "*spip-filtres-perso-list-temp-buffer*" spip-decouvre-filtres-regexp spip-decouvre-filtres-numeros-regexp spip-decouvre-filtres-ignore-regexp))
				   )
			  )
			fichiers
			)
	filtres
	)
  )

(defun spip-decouvre-criteres ()
  "Trouve les crit�res d�clar�s dans les fichiers php."
  (let ((criteres nil)
		(new-criteres nil)
		(fichiers)
		)
	;; on cherche dans inc_serialbase le nom des colonnes
	;;TOTO rendre cela customizable
	(set 'criteres (append criteres (spip-decouvre-generique (spip-construit-chemin "inc_serialbase" "/ecrire") "*spip-criteres-list-temp-buffer*" spip-decouvre-nom-colonne-regexp spip-decouvre-nom-colonne-numeros-regexp spip-decouvre-nom-colonne-ignore-regexp))
		 )
	;; on ajoute le crit�re par
	(mapcar (lambda (crit)
			  (set 'new-criteres (append new-criteres (list (concat "par " crit))))
			  (set 'new-criteres (append new-criteres (list crit)))
			  )
			criteres
			)
	;;TODO rendre cela customizable
	;; on cherche dans les fichiers mes_fonctions et inc-criteres pour des criteres
	(set 'fichiers (spip-list-php-file "^mes_fonctions.*"))
	(set 'fichiers (append fichiers (list (spip-construit-chemin "inc-criteres"))))
	(mapcar (lambda (fichier)
			  (set 'new-criteres (append new-criteres (spip-decouvre-generique fichier "*spip-criteres-list-temp-buffer*" spip-decouvre-criteres-regexp spip-decouvre-criteres-numeros-regexp spip-decouvre-criteres-ignore-regexp))
				   )
			  )
			fichiers
			)
	new-criteres
	)
  )

(defun spip-decouvre-balises ()
  "Trouve les balises d�clar�es dans les fichiers php."
  (let ((balises nil)
		(new-balises nil)
		(fichiers)
		)
	;; on cherche dans inc_serialbase le nom des colonnes
	;;TOTO rendre cela customizable
	(set 'balises (append balises (spip-decouvre-generique (spip-construit-chemin "inc_serialbase" "/ecrire") "*spip-balises-list-temp-buffer*" spip-decouvre-nom-colonne-regexp spip-decouvre-nom-colonne-numeros-regexp spip-decouvre-nom-colonne-ignore-regexp))
		 )
	;;TODO rendre cela customizable
	;; on cherche dans les fichiers mes_fonctions et inc-balises pour des balises
	(set 'fichiers (spip-list-php-file "^mes_fonctions.*"))
	(set 'fichiers (append fichiers (list (spip-construit-chemin "inc-criteres"))))
	(mapcar (lambda (fichier)
			  (set 'balises (append balises (spip-decouvre-generique fichier "*spip-balises-list-temp-buffer*" spip-decouvre-balises-regexp  spip-decouvre-balises-numeros-regexp spip-decouvre-balises-ignore-regexp))
				   )
			  )
			fichiers
			)
	(mapcar (lambda (bal)
			  (set 'new-balises (append new-balises (list (upcase bal))))
			  )
			balises
			)
	new-balises
	)
  )

(defun spip-decouvre-boucles ()
  "Trouve les boucles d�clar�es dans les fichiers php."
  (let ((boucles nil)
		(new-boucles nil)
		(fichiers)
		)
	;; on cherche dans inc_serialbase le nom des tables
	;;TOTO rendre cela customizable
	(set 'boucles (append boucles (spip-decouvre-generique (spip-construit-chemin "inc_serialbase" "/ecrire") "*spip-boucles-list-temp-buffer*" spip-decouvre-nom-table-regexp spip-decouvre-nom-table-numeros-regexp spip-decouvre-nom-table-ignore-regexp))
		 )
	;;TODO rendre cela customizable
	;; on cherche dans les fichiers mes_fonctions et inc-boucles pour des boucles
 	(set 'fichiers (spip-list-php-file "^mes_fonctions.*"))
 	(set 'fichiers (append fichiers (list (spip-construit-chemin "inc-boucles"))))
 	(mapcar (lambda (fichier)
 			  (set 'boucles (append boucles (spip-decouvre-generique fichier "*spip-boucles-list-temp-buffer*" spip-decouvre-boucles-regexp spip-decouvre-boucles-numeros-regexp spip-decouvre-boucles-ignore-regexp))
 				   )
 			  )
 			fichiers
 			)
	(mapcar (lambda (boucle)
			  (set 'new-boucles (append new-boucles (list (upcase boucle))))
			  )
			boucles
			)
	new-boucles
	)
  )

;; (defun spip-debug-decouvre ()
;;   "petite fonction pour tester la d�couverte"
;;   (interactive)
;;   (mapcar
;;    (lambda (comp)
;; 	 (insert (concat comp "\n"))) (spip-decouvre-boucles))
;;   )


(provide 'spip-decouvre)