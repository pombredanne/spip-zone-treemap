;;; spip-hil.el --- spip-mode

;; Copyright (C) 2005 Pierre Andrews

;; Authors: Pierre Andrews (mortimer.pa@free.fr)
;; Keywords: spip, cms, derived mode
;; Version: 0.2

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:
;; d�finition de la coloration syntaxique des squelettes spip.

;;faces
(defgroup spip-font-lock-faces nil
  "Customization pour les couleurs de la coloration syntaxique de squelttes spip."
  :prefix "spip-font-lock-"
  :group 'spip
  :group (if (featurep 'xemacs)
             'font-lock-faces
           'font-lock-highlighting-faces))

(defface spip-font-lock-boucle-face
  '((((class grayscale) (background light)) (:foreground "DimGray" :italic t))
    (((class grayscale) (background dark)) (:foreground "LightGray" :italic t))
    (((class color) (background light)) (:foreground "RoyalBlue4"))
    (((class color) (background dark)) (:foreground "steel blue"))
	)
  "Font Lock mode face used to highlight loops."
  :group 'spip-font-lock-faces)

(defface spip-font-lock-type-boucle-face
  '((((class grayscale) (background light)) (:foreground "DimGray" :bold t))
    (((class grayscale) (background dark)) (:foreground "LightGray" :bold t))
    (((class color) (background light)) (:foreground "DarkGoldenrod4"))
    (((class color) (background dark)) (:foreground "DarkGoldenrod1"))
    )
  "Font Lock mode face used to highlight loops table type."
  :group 'spip-font-lock-faces)

(defface spip-font-lock-critere-boucle-face
  '((((class grayscale) (background light)) (:foreground "DimGray" :bold t))
    (((class grayscale) (background dark)) (:foreground "LightGray" :bold t))
    (((class color) (background light)) (:foreground "RoyalBlue1"))
    (((class color) (background dark)) (:foreground "RoyalBlue1"))
    (t (:bold t)))
  "Font Lock mode face used to highlight loops criterion."
  :group 'spip-font-lock-faces)

(defface spip-font-lock-balise-face
  '((((class grayscale) (background light)) (:foreground "DimGray" :bold t))
    (((class grayscale) (background dark)) (:foreground "LightGray" :bold t))
    (((class color) (background light)) (:foreground "Blue"))
    (((class color) (background dark)) (:foreground "medium sea green"))
    (t (:bold t)))
  "Font Lock mode face used to highlight spip tags."
  :group 'spip-font-lock-faces)

(defface spip-font-lock-option-face
  '((((class grayscale) (background light)) (:underline t))
    (((class grayscale) (background dark)) (:underline t))
    (((class color) (background light)) (:underline t))
    (((class color) (background dark)) (:underline t))
    )
  "Font Lock mode face used to highlight extended spip tags optional parts."
  :group 'spip-font-lock-faces)

(defface spip-font-lock-filtre-face
  '((((class grayscale) (background light)) (:foreground "DimGray" :bold t))
    (((class grayscale) (background dark)) (:foreground "LightGray" :bold t))
    (((class color) (background light)) (:foreground "forest green"))
    (((class color) (background dark)) (:foreground "yellow green"))
    )
  "Font Lock mode face used to highlight loops tags filtres."
  :group 'spip-font-lock-faces)

(defface spip-font-lock-traduction-face
  '((((class grayscale) (background light)) (:foreground "DimGray" :bold t))
    (((class grayscale) (background dark)) (:foreground "LightGray" :bold t))
    (((class color) (background light)) (:foreground "darkgrey"))
    (((class color) (background dark)) (:foreground "grey"))
    )
  "Font Lock mode face used to highlight localisation strings."
  :group 'spip-font-lock-faces)

;; define new faces
(defvar spip-font-lock-boucle-face    'spip-font-lock-boucle-face
  "Face name to use for loop.")

(defvar spip-font-lock-type-boucle-face    'spip-font-lock-type-boucle-face
  "Face name to use for loop criterion type.")

(defvar spip-font-lock-critere-boucle-face    'spip-font-lock-critere-boucle-face
  "Face name to use for loop criterion.")

(defvar spip-font-lock-balise-face    'spip-font-lock-balise-face
  "Face name to use for tags.")

(defvar spip-font-lock-option-face    'spip-font-lock-option-face
  "Face name to use for tag's optional parts.")

(defvar spip-font-lock-filtre-face    'spip-font-lock-filtre-face
  "Face name to use for tag filtres.")

(defvar spip-font-lock-traduction-face    'spip-font-lock-traduction-face
  "Face name to use for localisation strings.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'spip-regexp)


(defconst spip-font-lock-keywords-1
  (list
   ;; les balises
   `( ,(concat "\\[\\([^(]*\\)" spip-reg-balise-etendue "\\([^]]*\\)\\]")
	 (0 spip-font-lock-balise-face keep)
	 (1 spip-font-lock-option-face prepend t)
	 (2 spip-font-lock-filtre-face t t)
	 (3 spip-font-lock-option-face prepend t)
	 )
   `(,spip-reg-balise . spip-font-lock-balise-face)
   `(,(concat "\\(?:" spip-reg-ouverture-boucle-opt "\\|" spip-reg-fermeture-boucle-opt "\\|" spip-reg-fermeture-boucle-alt  "\\|" spip-reg-fermeture-boucle "\\)") . spip-font-lock-boucle-face)
   ;; les boucles
   `(,spip-reg-ouverture-boucle  
	 (0 spip-font-lock-boucle-face keep)
	 (1 spip-font-lock-type-boucle-face t t)
	 (2 spip-font-lock-critere-boucle-face t)
	 )
   ;; les inclures
   `(,spip-reg-inclure
	 (0 spip-font-lock-boucle-face keep)
	 (1 spip-font-lock-type-boucle-face t)
	 (2 spip-font-lock-type-boucle-face t)
	 (3 spip-font-lock-critere-boucle-face t t)
	 )
   )
  "coloration minimale."
  )

(defconst spip-font-lock-keywords-2
  (append spip-font-lock-keywords-1
		  (list
		   ;; les traductions
		   `(,spip-reg-traduction
			 (0 spip-font-lock-traduction-face keep)
			 (1 spip-font-lock-filtre-face t t)
			 )
		   )
		  )
  "coloration des cha�nes localis�es aussi."
  )


(defvar spip-font-lock-keywords spip-font-lock-keywords-2
  "coloration par d�faut SPIP mode")

(provide 'spip-hil)