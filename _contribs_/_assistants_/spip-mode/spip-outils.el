;;; spip-outils.el --- spip-mode

;; Copyright (C) 2005 Pierre Andrews

;; Authors: Pierre Andrews (mortimer.pa@free.fr)
;; Keywords: spip, cms, derived mode
;; Version: 0.2

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:
;; outils divers pour spip-mode

;; pris de l'extension JDEE: http://jdee.sunsite.dk
(defun spip-require (feature)
   "Require FEATURE, either pre-installed or from the distribution.
 That is, first try to load the FEATURE library. Then try to load the
spip-mode-FEATURE library from the spip-mode's distribution.
 Signal an error if FEATURE can't be found."
   (condition-case nil
      ;; If the library if available, use it.
       (require feature)
     (error
      (require feature (format "spip-%s" feature)))))


(defun spip-decouvre-generique (fichier nom-temp regexp num &optional exclure)
  "Retourne une liste simple avec les cha�nes match�es pas une regexp dans un fichier. 

FICHIER est le chemin complet du fichier, 
NOM-TEMP est un nom de
buffer temporaire o� charger le fichier pendant la recherche,
REGEXP est l'expression r�guli�re � utiliser pour trouver ce
qu'on cherche,
NUM sp�cifie le groupe de REGEXP que l'on veut
extraire, 
Si le groupe NUM match� par REGEXP est aussi
match� par EXCLURE, alors il ne sera pas retourn�."
  (let ((filtres nil)
		(temp-buf nil)
		(exclu (if exclure exclure "^$"))
		)
	(if (file-exists-p fichier)
		(progn
		  (set 'temp-buf (get-buffer-create (concat "*" nom-temp "*")))
		  (unwind-protect
			  (save-excursion
				(set-buffer temp-buf)
				(erase-buffer)
				(insert-file-contents fichier)
				(goto-char (point-min))
				(while (re-search-forward regexp (point-max) t)
				  (if (stringp (match-string num))
					  (if (not (string-match exclu (match-string num)))
						  (set 'filtres (append filtres (list (match-string num))))
						)
					)
				  )
				)
			(kill-buffer temp-buf)
			)
		  )
	  )
	filtres
	)
  )

(defun spip-construit-chemin (fichier &optional repertoire extension)
  "construit un chemin spip � partir de l'extension sp�cifi� et de la racine d�clar�e par spip-racine-locale.

On peut sp�cifier le REPERTOIRE � partir de cette racine o� le
fichier est cens� se trouver.  
On peut aussi sp�cifier une autre EXTENSION que spip-extension."
  (let (
		(ext (if extension 
				 (concat "." extension) 
			   (concat "." spip-extension)
			   )
			 )
		)
	(concat spip-racine-locale repertoire "/" fichier ext)
	)
  )

(defun spip-list-php-file (fichier &optional repertoire not-full extension)
  "liste les fichiers php dans un repertoire spip.

On liste les fichiers d'extension spip-extension matchant l'expression
  r�guli�re FICHIER � la racine spip-racine-locale ou dans le
  REPERTOIRE en dessous des cette racine.

NOT-FULL si non-nil sp�cifie que l'on veut juste le nom du fichier et
  pas le chemin complet,
EXTENSION sp�cifie que l'on veut une autre extension que spip-extension."
  (let (
		(ext (if extension 
				 (concat "." extension) 
			   (concat "." spip-extension)
			   )
			 )
		)
	(directory-files (concat spip-racine-locale repertoire "/") (not not-full) (concat fichier ext))
	)
  )

(defun spip-completing-read (prompt completion-list match hist)
  "Petit raccourci pour faire un prompt."
  (completing-read (concat prompt ": ")
				   (mapcar
					(lambda (comp)
					  (cons comp 't)) completion-list)
				   nil match nil hist)
  )

(provide 'spip-outils)