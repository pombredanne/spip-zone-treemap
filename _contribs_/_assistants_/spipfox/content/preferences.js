/*
* Copyright (C) 2005 Pierre Andrews

* Authors: Pierre Andrews (mortimer.pa@free.fr)
* Keywords: spip, cms, firefox, extension, shotcuts
* Version: 0.1

* This file is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.

* This file is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with GNU Emacs; see the file COPYING.  If not, write to
* the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
* Boston, MA 02111-1307, USA.
*/

var SpipFoxPreferences = {

    changePopup: function(event) {
		//reinit the popup
		event.target.style.display = '';
		while(event.target.hasChildNodes()) {
			event.target.removeChild(event.target.firstChild);
		}
		var properties = this.propertiesForCurrentCell(event);
		if ((properties != null) && this.cellHasProperty(properties, "type")) {
			this.showTypeMenu(event.target);
		} else if ((properties != null) && this.cellHasProperty(properties, "avant")) {
			this.showOtherMenu(event.target);
		} else {
			//hide the popup if we do not need it.
			event.target.style.display='none';
		}
	},

	//cell property bit from http://xullite.blogspot.com/2005/06/context-menu-for-xul-trees.html
	cellHasProperty: function(properties,name) {
		for (count = 0;  count < properties.Count(); ++count) {
			var property = properties.GetElementAt(count);			
			if(name == property.QueryInterface(Components.interfaces.nsIAtom)) {
				return true;
			}
		}
		return false;
	},

	propertiesForCurrentCell: function(event) {

		var treeElement = event.rangeParent;  // returns the reference to tree xul element
		var row = new Object();
		var col = new Object();
		var childElement = new Object();
		if(treeElement != null) {
			treeElement.treeBoxObject.getCellAt(event.clientX, event.clientY, row, col, childElement);
			var arrayComp = Components.classes['@mozilla.org/supports-array;1'].createInstance();
			var properties = arrayComp.QueryInterface(Components.interfaces.nsISupportsArray);
			
			treeElement.view.getCellProperties(row.value, col.value, properties);
			
			return properties;
		} else {
			return null;
		}
	},	

	showTypeMenu: function(popupMenu) {
		var menuitem = document.createElement("menuitem");
		menuitem.setAttribute("label","clipboard");
		menuitem.setAttribute("id","tree-menu-clipboard");
		popupMenu.appendChild(menuitem);
	},

	showOtherMenu: function(popupMenu) {
		var menuitem = document.createElement("menuitem");
		menuitem.setAttribute("label","BOOM");
		menuitem.setAttribute("id","tree-menu-BOOM");
		popupMenu.appendChild(menuitem);
	}

}
