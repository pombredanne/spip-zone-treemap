/*
* Copyright (C) 2005 Pierre Andrews

* Authors: Pierre Andrews (mortimer.pa@free.fr)
* Keywords: spip, cms, firefox, extension, shotcuts
* Version: 0.1

* This file is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2, or (at your option)
* any later version.

* This file is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with GNU Emacs; see the file COPYING.  If not, write to
* the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
* Boston, MA 02111-1307, USA.
*/

var SpipFox = {

  onLoad: function() {
    // initialization code
	  this.initialized = true;
  },

  getTarget: function() {
	  return document.commandDispatcher.focusedElement;
  },
  
  //http://xulplanet.com/tutorials/xultu/clipboard.html
  getClipboardContent: function() {
	  var clip = Components.classes["@mozilla.org/widget/clipboard;1"].
	  getService(Components.interfaces.nsIClipboard);
	  if (!clip) return false;
	  
	  var trans = Components.classes["@mozilla.org/widget/transferable;1"].
	  createInstance(Components.interfaces.nsITransferable);
	  if (!trans) return false;
	  trans.addDataFlavor("text/unicode");

	  clip.getData(trans,clip.kGlobalClipboard);
	  
	  var str = new Object();
	  var strLength = new Object();
	  
	  try{
		  trans.getTransferData("text/unicode",str,strLength);
		  
		  if (str) str = str.value.QueryInterface(Components.interfaces.nsISupportsString);
		  if (str) pastetext = str.data.substring(0,strLength.value / 2);

		  return pastetext;
	  }catch (e){
		  //alert("No text in the clipboard, please copy something first.");
	  }
	  return '';
  },
    
  //from http://www.massless.org/mozedit/
  mozWrap:	function (txtarea, lft, rgt) {
	  var selLength = txtarea.textLength;
	  var selStart = txtarea.selectionStart;
	  var selEnd = txtarea.selectionEnd;
	  if (selEnd==1 || selEnd==2) selEnd=selLength;
	  var s1 = (txtarea.value).substring(0,selStart);
	  var s2 = (txtarea.value).substring(selStart, selEnd)
	  var s3 = (txtarea.value).substring(selEnd, selLength);
	  txtarea.value = s1 + lft + s2 + rgt + s3;
  },

  //insert at cursor:
  insertAtCursor: function(txtarea, text) {
	  if (txtarea.selectionStart || txtarea.selectionStart == '0') {
		  var startPos = txtarea.selectionStart;
		  var endPos = txtarea.selectionEnd;
		  txtarea.value = txtarea.value.substring(0, startPos)
		  + text 
		  + txtarea.value.substring(endPos, txtarea.value.length);
	  } else {
		  txtarea.value += text;
	  }
  },

  wrapTarget: function(bef,aft) {
	  this.mozWrap(this.getTarget(),bef,aft);

  },

  pasteTarget: function(bef,aft) {
	  this.insertAtCursor(this.getTarget(),bef+this.getClipboardContent()+aft);

  },

  //======================================================================
  // Les fonctions du menu
  //======================================================================

  
  wrapGenerique: function(bfr,aftr) {
	  this.wrapTarget(bfr,aftr);
  },

  clipboardGenerique: function(bfr,aftr) {
	  this.pasteTarget(bfr,aftr);
  },

  lien: function(event) {
	  this.wrapTarget('[','->'+this.getClipboardContent()+']');
  },

  preferences: function(event) {
	  window.openDialog('chrome://spipfox/content/preferences.xul', 'spipfox_preferences',
						'chrome,modal,centerscreen');
  }

}
	
window.addEventListener("load", function(e) { SpipFox.onLoad(e); }, false); 
