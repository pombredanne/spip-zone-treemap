<?php

// Este es un archivo para SPIP -- This is a SPIP module file  --  Ceci est un fichier module de SPIP

$GLOBALS['i18n_spiplistes_es'] = array(

//_
'_aide' => 'SPIP-Listes te permite enviar un mensajes colectivos a las personas inscritas. Puedes utilizar una plantilla, escribir un mensaje o
prepararlo en HTML. 
<br><br> Las personas inscritas pueden modificar sus opciones en l&iacute;nea: darse de baja, listas de novedades que les interesan y formato en el que quieren recibir sus mensajes (HTML/texto).
<br><br>Los mensajes ser&aacute;n traducidos autom&aacute;ticamente al formato texto para las personas que lo soliciten o que tengan configurada la recepci&oacute;n de correos en modo texto.<br><br><b>Nota:</b><br>El env&iacute;o de mensajes puede tardar unos minutos: los env&iacute;os salen cuando se visita la parte p&uacute;blica de la web. Puedes forzar ese env&iacute;o cliqueando en el enlace "Seguimiento de env&iacute;os".',

// A
'abo_1_lettre' => 'Inscribirse para recibir novedades de ',
'abonnement' => 'Inscripci&oacute;n',
'abonnement_mod'=>'Modificar tu inscripci&oacute;n',
'abonnement_bouton'=>'Modificar',
'abonnement_cdt'=>'<a href=\'http://bloog.net\'>SPIP-Listes</a>' ,
'abonnement_change_format'=>'Puedes cambiar las listas y el formato en el que recibes las novedades y los mensajes, o darte de baja. ',
'abonnement_mail'=>'Para modificar tu inscripci&oacute;n, cliquea en este enlace',
'abonnement_modifie'=>'Los cambios se han guardado',
'abonnement_nouveau_format'=>'Tu nuevo formato para recibir nuestras novedades e informaciones: ',
'abonnement_titre_mail'=>'Modificar tu inscripci&oacute;n',
'abonnement_texte_mail'=>'Escribe la direcci&oacute;n de correo electr&oacute;nico con la que est&aacute;s inscrito(a). 
Te enviaremos un correo electr&oacute;nico con el enlace a la p&aacute;gina en la que podr&aacute;s hacer los cambios.',
'abonnement_mail_passcookie'=>'(este es un mensaje autom&aacute;tico)

En el siguiente enlace:

@adresse_site@/abonnement.php3?d=@cookie@

podr&aacute;s cambiar el formato de recepci&oacute;n de los mensajes y novedades de
"@nom_site_spip@" (@adresse_site@)
o darte de baja.

Un saludo
  
    ',

'abonner' => 'inscribirse',
'actualiser' => 'Actualizar esta p&aacute;gina',
'adresse' => 'Direcci&oacute;n de correo electr&oacute;nico para responder al mensaje (por defecto, la direcci&oacute;n del/la webmestre/webmistress ser&aacute; utilizada como direcci&oacute;n de respuesta) :',
'adresses_importees' => 'Adresses import&eacute;es ***FIXME ***',
'aff_encours' => 'Mensajes a enviar',
'aff_envoye' => 'Mensajes enviados "manualmente"',
'aff_lettre_auto' => 'Mensajes enviados a las listas de forma programada',
'aff_redac' => 'Mensajes en curso de redacci&oacute;n',
'alerte_edit' => 'Atenci&oacute;n: este mensaje puede ser modificado por l@s administradore(a)s y lo recibir&aacute;n todas las personas inscritas en la lista (puedes seleccionarla en el siguiente paso).<br />No utilices los mensajes colectivos nada m&aacute;s que para acontecimientos importantes de la vida del sitio.',
'alerte_modif' => '<b>Despu&eacute;s de \'visualizar el mensaje\',<br />podr&aacute;s modificar su contenido</b>',
'annuler_envoi' => 'Cancelar el env&iacute;o',

// B


//C
'Cette_liste_est' => 'Lista ',
'charger_le_patron' => 'Visualizar el mensaje',
'charger_patron' => 'Elegir una plantilla',
'Configuration' => 'Configuraci&oacute;n',
'courriers' => 'Mensajes',

//D
'definir_squel' => 'Elegir la plantilla y el formato a previsualizar',
'definir_squel_choix' => 'Al redactar un nuevo mensaje, SPIP-Listes te permite seleccionar una plantilla. Cliqueadno en el bot&oacute;n, ver&aacute;s el contenido del mensaje en \'una de las plantillas del directorio <b>/patrons</b> (situado en la ra&iacute;z de tu sitio Spip). <p><b>Puedes personalizar estas plantillas a tu gusto.</b></p> <UL><li>Las plantillas pueden hacerse con HTML cl&aacute;sico</li>
<li>La plantilla tambi&eacute;n puede contener bucles SPIP</li>
<li>Despu&eacute;s de preparar el mensaje, puedes volver a editarlo antes de enviarlo (para modificar su contenido)</li>
</ul><p>La funci&oacute;n "elegir una plantilla" permite utilizar plantillas HTML personalizadas pata tus mensajes o crear boletines de informaci&oacute;n tem&aacute;ticos ya que el contenido lo pueden gestionar los bucles de SPIP.</p><p>Atenci&oacuten: ce squelette ne doit pas contenir de balises body, head ou html mais juste du code HTML ou des boucles Spip.</p>',
'definir_squel_texte' => 'Si dispones de acceso por FTP, puedes a&ntilde;adir esqueletos SPIP (u otras plantillas) en el directorio /patrons (en la ra&iacute;z de vuestro sitio Spip).',
'devenir_redac'=>'pasar a redactor(a) de este sitio',
'devenir_abonne'=>'Bolet&iacute;n de inscripci&oacute;n',

'desabonnement_valid'=>'La siguiente direcci&oacuten no est&aacute; inscrita para recibir nuestras novedades e informaciones' ,
'pass_recevoir_mail'=>'Hemos enviado un correo electr&oacute;nico para que puedas cambiar tu inscripci&oacute;n.',
'desabonnement_confirm'=>'Vas a darte de baja para recibir las novedades de este sitio',
'date_depuis'=>'depuis @delai@', //FIX ME

//E
'email' => 'Mensaje',
'envoi' => 'Env&iacute;o :',
'envoi_nouv' => 'Env&iacute;o de novedades',
'envoi_program' => 'Env&iacute;o programado',
'envoi_texte' => 'Si has terminado tu mensaje, puedes enviarlo ya con una de estas dos opciones:',
'exporter' => 'Exportar la lista de inscripciones',

//F
'faq' => 'FAQ',
'form_forum_identifiant_confirm'=>'Tu inscripci&oacute;n est&aacute; guardada, recibir&aacute;s un mensaje de confirmaci&oacute;n.',
'forum' => 'Foro',
'ferme' => 'Este foro se ha cerrado',
'form_forum_identifiants' => 'Confirmaci&0acute;n',
'format' => 'Formato',

//H
'Historique_des_envois' => 'Env&iacute;os',

//I
'info_auto' => 'SPIP-Listes para SPIP puede enviar peri&oacute;dicamente a l@s inscrit@s, la \'informaci&oacute;n de las &uacute;ltimas novedades del sitio (art&iacute;culos, breves, mensajes en los foros, sitios sindicados,... recientemente publicados).',
'info_heberg' => 'En algunos servidores est&aacute; desactivado el \'env&iacute;o autom&aacute;tico de mensajes. En ese caso, las siguientes opciones de SPIP-Listes para SPIP no funcionar&aacute;n',
'info_nouv' => 'Tienes activado el env&iacute;o de novedades',
'info_nouv_texte' => 'Pr&oacute;simo env&iacute;o de novedades en @proch@ d&iacute;as',
'inscription_mail_forum' => 'Estos son tus identificadores para conectarte a @nom_site_spip@ (@adresse_site@)',
'inscription_mail_redac' => 'Estos son tus identificadores para conectarte a @nom_site_spip@ (@adresse_site@) et l\'interface de r�daction (@adresse_site@/ecrire)',

'inscription_visiteurs' => 'Inscripci&oacute;n como visitante',

'inscription_redacteurs' =>'Inscripci&oacute;n como redactor(a)',
'import_export' => 'Import / Export *** FIXME ***',

//J
'jours' => 'd&iacute;a(s)',

//L
'lettre_d_information' => 'Informaci&oacute;n',
'lieu' => 'Localizaci&oacute;n',
'Listes_de_diffusion' => 'Listas',
'login' => 'Conexi&oacute;n',
'logout' => 'Desconexi&oacute;n',
'lot_suivant' => 'Enviar siguiente lote',

//M
'mail_format' => 'Est&aacute;s inscrit@ para recibir los novedades de @nom_site_spip@ en formato',
'mail_non' => 'No est&aacute;s inscrit@ para recibir las novedades de @nom_site_spip@',
'membres_avec_messages_connecte' => 'Tienes @nombres@ nuevo(s) mensaje(s)',
'membres_groupes' => 'Grupos de usuari@s',
'membres_liste' => 'Inscripciones',
'membres_messages_deconnecte' => 'Con&eacute;ctate para verificar los mensajes privados',
'membres_profil' => 'Perfil',
'membres_sans_messages_connecte' => 'No hay nuevos mensajes',
'message' => 'Mensaje: ',
'message_arch' => 'Mensaje guardado','message_date' => 'Enviarlo ',
'messages_auto' => 'Mensajes autom&aacute;ticos',
'messages_auto_texte' => '<p>Por defecto la plantilla \'nouveautes.html\' permite \'enviar autom&aacute;ticamente la lista de los art&iacute;culos y breves publicados en el sitio desde el &uacute;ltimo env&iacute;o. </p><p>puedes personalizar el mensaje incluyendo una \'direcci&oacute;n, \'un logo o \'una imagen de fondo para los t&iacute;tulos de las secciones editando el archivo <b>"nouveautes.html"</b> (situado en el directorio \'/patrons\' en la ra&iacute;z de tu sitio Spip).</p>',
'message_redac' => 'En curso de redacci&oacute;n y pendiente de ser enviado',
'message_sujet' => 'Mensaje: ',
'message_type' => 'Correo electr&oacute;nico',
'messages_auto_envoye' => 'Novedades enviadas',
'message_en_cours' => 'Mensaje para enviar',
'messages' => 'Mensajes',
'Messages_automatiques' => 'Programaci&oacute;n de env&iacute;os a las listas',
'messages_derniers' => '&uacute;ltimos mensajes',
'messages_forum_clos' => 'Foro desactivado',
'messages_non_lus_grand' => 'No hay mensajes nuevos',
'messages_nouveaux' => 'Nuevos mensajes',
'messages_pas_nouveaux' => 'No hay mensajes nuevos',
'messages_repondre' => 'Responder',
'messages_voir_dernier' => 'Ver el &uacute;ltimo mensaje',
'moderateurs' => 'Moderadora(e)(s)',
'modifier' => 'Cambiar',

//N
'nom' => 'Nombre de usuari@',
'nouveaux_messages' => 'Nuevos mensajes',
'Nouveau_courrier' => 'Nuevo mensaje',
'Nouvelle_liste_de_diffusion' => 'Nueva lista',

//P
'par_date' => 'Por fecha de inscripci&oacute;n',
'Patrons' => 'Plantillas',
'patron_disponibles' => 'Plantillas disponibles',
'poster' => 'Env&iacute;ar un mensaje',
'prochain_envoi_prevu' => 'Pr&oacute;ximo env&iacute;o hoy',

//R
'recherche' => 'Buscar',
'revenir_haut' => 'Ir al principio de la p&aacute;gina',
'reponse' => 'Respuesta al mensaje',
'retour' => 'Direcci&oacute;n de correo electr&oacute;nico de la lista (para las respuestas)',

//S
'suivi' => 'Seguimiento de inscrit@s',
'Suivi_des_abonnements' => 'Inscripciones',
'sujet_nouveau' => 'Nuevo mensaje',
'sujet_auteur' => 'Autor(a)',
'sujet_visites' => 'Visitas',
'sujets' => 'Temas',
'sujets_aucun' => 'Por ahora no hay mensajes en este foro',
'site' => 'Sitio web',
'sujet_clos_titre' => 'Tema cerrado',
'sujet_clos_texte' => 'Este tema est&aacute; cerrado, no puedes env&iacute;ar mensajes.',
 
 //T
'texte_boite_en_cours' => 'SPIP-Listes tiene mensajes para enviar. <p> Puedes forzar su env&iacute;o entrando en "Revisar tareas pendientes".</p> <p>Este recuadro desaparecer&aacute; cuando acabe el env&iacute;o.</p>',
'texte_lettre_information' => 'Informaci&oacute;n de ',
'Tous_les' => 'cada',

//V
'voir' => 'vista previa',

// ====================== spip_listes.php3 ======================

'abon' => 'INSCRIPCIONES',
'abon_ajouter' => 'A&Ntilde;ADIR INSCRIPCIONES &nbsp; ',
'abonees' => 'Todas las inscripciones',
'abonne_listes' => 'Este contacto est&aacute; inscrito/a en las siguientes listas',
'abonnement_simple' => '<b>Inscripci&oacute;n simple: </b><br><i>Las personas inscritas s&oacute;lo recibir&aacute;n un mensaje de confirmaci&oacute;n</i>',
'abonnement_code_acces' => '<b>Inscripci&oacute;n con identificadores: </b><br><i>Las personas inscritas recibir&aacute;n un login y una contrase&ntilde;a que les permitir&aacute; acceder a la parte privada del sitio. </i>',
'abonnement_newsletter' => '<b>Inscripci&oacute;n en las listas</b>',
'acces_a_la_page' => 'No tienes acceso a est&aacute; p&aacute;gina.',
'adresse_deja_inclus' => 'Est&aacute; direcci&oacute;n de correo ya est&aacute; inscrita',
'autorisation_inscription' => 'La bloOgletter vient d\'activer l\'autorisation de s\'inscrire aux visiteurs du site',


'choisir' => 'Seleccionar',
'choisir_cette' => 'Enviarlo a esta lista',
'confirme_envoi' => 'Para confirmar su env&iacute;o ve hasta el final.',

'date_act' => 'Datos actualizados',
'date_ref' => 'Fecha de referencia',
'desabo' => 'baja',
'desabonnes' => 'Sin inscripci&oacute;n en listas',
'desole' => 'Desolado/a',
'destinataire' => 'destinatari@',
'destinataires' => 'Destinatari@s',

'efface' => 'ha sido borrado/a de la base de datos\"',
'efface_base' => 'Ha sido borrado/a de las listas y de la base de datos',
'email_adresse' => 'Direcci&oacute;n de prueba',
'email_collec' => 'Mensaje colectivo',
'email_test' => 'Env&iacute;ar un correo de prueba',
'email_test_liste' => 'Enviarlo a una lista',
'email_tester' => 'Enviarlo a esta direcci&oacute;n',
'env_maint' => 'Enviar ahora',
'env_esquel' => 'Env&iacute;o programado con la plantilla ',
'envoyer' => 'enviar el correo',
'envoyer_a' => 'Enviar a',
'erreur' => 'Error',
'erreur_import' => 'El archivo de importaci&oacute;n tiene un error en la l&iacute;nea ',

'format_date' => 'd-m-Y',

'html' => 'HTML',

'importer' => 'Importar inscripciones',
'importer_fichier' => 'Importar un archivo',
'importer_fichier_txt' => '<p><b>Las inscripciones a importar deben estar en un fichero de texto que s&oacute;lo tenga una direcci&oacute;n por l&iacute;nea</b></p>',
'importer_preciser' => '<p>Selecciona las listas y el formato para las nuevas inscripciones</p>',
'inconnu' => 'no est&aacute; inscrit@ en la lista',

'liste_diff_publiques' => 'Listas p&uacute;blicas<br><i>En la parte p&uacute;blica del sitio se propone la inscripci&oacute;n a estas listas.</i>',
'liste_sans_titre' => 'Lista sin t&iacute;tulo',
'listes_internes' => 'Listes de diffusion internes<br /><i>Au moment de l\'envoi d\'un message, ces listes sont propos�es parmi les destinataires</i>',
'listes_poubelle' => 'Listas en la papelera',
'lock' => 'Bloqueo activo: ',

'mail_a_envoyer' => 'N&uacute;mero de correos a enviar: ',
'mail_tache_courante' => 'Correos enviados en este lote: ',
'messages_auto_envoye' => 'Env&iacute;os autom&aacute;ticos a las listas ya realizados',
'message_en_cours' => 'Mensaje NO enviado',
'message_presque_envoye' =>'Mensaje preparado para ser enviado.',
'mode_inscription' => 'Determinar el modo de inscripci&oacute;n de los/las visitantes',
'modif_envoi' => 'Puedes modificarlo o enviarlo con las opciones del final.',
'modifier_liste' => 'Modificar esta lista:',

'nb_abonnes' => 'En las listas: ',
'nb_inscrits' => 'En el sitio:  ',
'nb_listes' => 'Entre todas las listas: ',
'non_program' => 'No hay env&iacute;os programados para esta lista.',
'nouvelle_abonne' => 'La siguiente inscripci&oacute;n ha sido a&ntilde;adida a la lista',

'pas_acces' => 'No tienes acceso permitido a esta p&aacute;gina.',
'plus_abonne' => ' No est&aacute;s inscrit@ en esta lista ',
'prochain_envoi_aujd' => 'Pr&oacute;ximo env&iacute;o hoy',
'prochain_envoi_prevu' => 'Pr&oacute;ximo env&iacute;o en ',
'prog_env' => 'Programar un env&iacute;o',
'prog_env_non' => 'No programar el env&iacute;o',
'program' => 'Programaci&oacute;n de env&iacute;os',

'remplir_tout' => 'Tous les champs doivent �tre remplis',
'repartition' => 'Distribuci&oacute;n',
'retour_link' => 'Volver',

'sans_envoi' => 'La direcci&oacute;n de correo que has escrito no corresponde con ninguna inscripci&oacute;n, <br />No puede hacerse el env&iacute;o, int&eacute;ntalo otra vez<br /><br />',
'squel' => 'Plantilla: &nbsp;',
'statut_interne' => 'Interna',
'statut_publique' => 'P&uacute;blica',
'suivi_envois' => 'Revisar tareas pendientes',
'supprime_contact' => 'Suprimir esta inscripci&oacute;n definitivamente',
'supprime_contact_base' => 'Borrarlo definitivamente de la base de datos',

'tableau_bord' => 'Cuadro resumen',
'texte' => 'texto',
'toutes' => 'Todas las inscripciones de este sitio',
'txt_inscription' => 'Texto para la inscripci&oacute;n: ',
'txt_abonnement' => '(Indica aqu&iacute; el texto para la inscripci&oacute;n en esta lista, se ver&aacute; en la zona p&ucaute;blica si la lista est&aacute; activa)',

'une_inscription' => 'Se ha encontrado una inscripci&oacute;n',

'val_texte' => 'Texto',
'version' => 'versi&oacute;n',
'voir_historique' => 'Ver todos los env&iacute;os',


// ====================== inscription-listes.php3 / abonnement.php3 ======================

'abo_listes' => '<b>Listas de novedades de </b>',
'acces_refuse' => 'No tienes acceso a este sitio',
'confirmation_format' => ' en formato ',
'confirmation_liste_unique_1' => 'Est&aacute;s inscrit@ en las listas de este sitio',
'confirmation_liste_unique_2' =>'Has seleccionado recibir los mensajes de la lista:',
'confirmation_listes_multiples_1' => 'Est&aacute;s inscri@ en las listas de este sitio ',
'confirmation_listes_multiples_2' => 'Has seleccionado recibir los mensajes de las siguientes listas:',

'erreur_adresse' => 'Error, la direcci&oacute;n que has escrito no es v&aacute;lida',

'infos_liste' => 'Informaci&oacute;n sobre esta lista',


// ====================== spip-meleuse.php3 ======================

'contacts' => 'N&uacute;mero de inscripciones',
'contacts_lot' => 'Env&iacute;os en este lote',
'editeur' => 'Enviado por: ',
'envoi_en_cours' => 'Realiz&aacute;ndose el env&iacute;o',
'envoi_tous' => 'Env&iacute a todas las inscripciones del sitio',
'envoi_listes' => 'Env&iacute;o a todas las inscripciones de la lista: ',
'envoi_erreur' => 'Error : SPIP-listes no encuentra destinatari@ para este mensaje',
'email_reponse' => 'Direcci&oacute;n de respuesta: ',
'envoi_annule' => 'Env&iacute;o cancelado',
'envoi_fini' => 'Env&iacute;os acabados',
'erreur_destinataire' => 'Error de destinatari@: no enviado',
'erreur_sans_destinataire' => 'Error: no hay destinatari@ para este mensaje',
'erreur_mail' => 'Erreur : envoi du mail impossible (v�rifier si mail() de php est disponible) ***FIXME***',

'forcer_lot' => 'Enviar el siguiente lote',

'non_courrier' => 'No quedan correos por enviar',
'non_html' => 'Puede que tu aplicaci&oacute;n para correo electr&oacute;nico no pueda mostrar la versi&oacute;n gr&aacute;fica (HTML) de este mensaje',

'sans_adresse' => 'Correo no enviado -> No hay direcci&oacute;n de respuesta',



// ====================== inc_import_patron.php3 ======================

'confirmer' => 'Confirmar',

'lettre_info' => 'Informaci&oacute;n de ',

'patron_erreur' => 'Puede que tu mensaje tenga menos de diez car&aacute;cteres o por las opciones que tienes seleccionadas haya alg&uacute;n error en el contenido mensaje.',


// ====================== listes.html ======================

'abonees_titre' => 'Inscripciones',


// ====================== inc-presentation.php3 ======================

'listes_emails' => 'Listas de novedades<br />y mensajes colectivos',


// ====================== mes-options.php3 ======================

'bonjour' => 'Hola,',

'inscription_response' => 'Est&aacute;s inscrit@ en la lista de ',
'inscription_responses' => 'Est&aacute;s inscrit@ en las listas de ',
'inscription_liste' => 'Has seleccionado recibir los mensajes de la lista: ',
'inscription_listes' => 'Has seleccionado recibir los mensajes de las listas: ',
'inscription_format' => ' en formato ',

'options' => 'radio|brut|Formato:|Html,Texto,No recibir novedades|html,texte,non',

);

?>
