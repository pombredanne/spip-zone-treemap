<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => '7h3 4ddr355 &17;@var_email@&g7; w111 b3 4dd3d 70 7h3 1157 &17;@liste@&g7; 0nc3 17 h45 b33n ch3ck3d. P13453 r3p1y 70 7h3 m3554g3 wh1ch h45 ju57 b33n 53n7 70 y0u.',


// C
'confirm' => '4 c0nf1rm4710n r3qu357 h45 b33n 53n7 70 &17;@var_email@&g7;.',


// D
'deja' => '&17;@var_email@&g7;: Y0u 4r3 41r34dy 5ubcr1b3d 70 7h3 &17;@liste@&g7; 1157.',
'desabo' => '5ub5cr1p710n c4nc3113d.',


// F
'fermer' => 'c1053',


// I
'inscription' => '5ub5cr1p710n 70 &17;@liste@&g7;',


// M
'mail_removed' => '

7h3 4ddr355 &17;@var_email@&g7; h45 b33n r3m0v3d fr0m 7h3 @liste@ 1157.

1f 7h3r3 15 4 pr0b13m, 0r 1f y0u d1d n07 r3qu357 7h15 c4nc3114710n
p13453 wr173 70 &17;@responsable@&g7;.

G00dby3 4nd 7h4nk5.
',
'merci' => '7h4nk y0u.',


// P
'pasabo' => '7h3 4ddr355 &17;@var_email@&g7; 15 n07 5ub5cr1b3d 7h3 &17;@liste@&g7; 1157.',
'patientez' => 'P13453 w417...',


// Q
'quitter' => 'C4nc3114710n',


// S
'subject_removed' => 'Y0ur 4ddr355 h45 b33n r3m0v3d fr0m 7h3 @liste@ 1157.',


// T
'titrefenetre' => '5ub5cr1b3',


// V
'veuillez' => 'P13453 g1v3 y0ur 4ddr355.',
'votreemail' => 'Y0ur 3m411:'

);


?>
