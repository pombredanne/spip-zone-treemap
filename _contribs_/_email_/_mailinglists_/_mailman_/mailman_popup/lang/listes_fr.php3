<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'L\'adresse &lt;@var_email@&gt; sera ajout&eacute;e &agrave; la liste &lt;@liste@&gt; apr&egrave;s v&eacute;rification. Merci de r&eacute;pondre au message qui vient de vous &ecirc;tre envoy&eacute;.',


// C
'confirm' => 'Une demande de confirmation a &eacute;t&eacute; adress&eacute;e &agrave; &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;&nbsp;: vous &ecirc;tes d&eacute;j&agrave; inscrit(e) &agrave; la liste &lt;@liste@&gt;.',
'desabo' => 'D&eacute;sabonnement effectu&eacute;.',


// F
'fermer' => 'fermer',


// I
'inscription' => 'Inscription &agrave; &lt;@liste@&gt;',


// M
'mail_removed' => '

L\'adresse <@var_email@> a &eacute;t&eacute; supprim&eacute;e de la liste @liste@.

En cas de probl&egrave;me, ou si vous n\'avez pas demand&eacute; ce d&eacute;sabonnement,
veuillez &eacute;crire &agrave; <@responsable@>.

Au revoir, et merci.
',
'merci' => 'Merci.',


// P
'pasabo' => 'L\'adresse &lt;@var_email@&gt; n\'est pas abonn&eacute;e &agrave; la liste &lt;@liste@&gt;.',
'patientez' => 'Veuillez patienter...',


// Q
'quitter' => 'D&eacute;sabonnement',


// S
'subject_removed' => 'Votre adresse a &eacute;t&eacute; supprim&eacute;e de la liste @liste@.',


// T
'titrefenetre' => 'Inscription',


// V
'veuillez' => 'Veuillez pr&eacute;ciser votre adresse.',
'votreemail' => 'Votre email&nbsp;:'

);


?>
