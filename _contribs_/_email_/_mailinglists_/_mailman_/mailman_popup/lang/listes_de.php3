<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'Die Adresse &lt;@var_email@&gt; wird der Liste &lt;@liste@&gt; nach einer &Uuml;berpr&uuml;fung hinzugef&uuml;gt. Bitte beantworten Sie die Mail, welche Ihnen zur &Uuml;berpr&uuml;fung Ihrer Adresse geschickt wurde.',


// C
'confirm' => 'Die Bitte um Best&auml;tigung wurde an &lt;@var_email@&gt; geschickt.',


// D
'deja' => '&lt;@var_email@&gt;&nbsp;: Sie sind bereits Mitglied der Liste &lt;@liste@&gt;.',
'desabo' => 'Abmeldung erfolgreich.',


// F
'fermer' => 'schlie&szlig;en',


// I
'inscription' => 'Abonnieren von &lt;@liste@&gt;',


// M
'mail_removed' => '

Die Adresse <@var_email@> wurde von der Liste @liste@ gl&ouml;scht.

Falls ein Problem auftritt oder Sie die Liste nicht abbestellt haben, schreiben Sie bitte an <@responsable@>.

Vielen Dank und auf Wiedersehen!
',
'merci' => 'Danke.',


// P
'pasabo' => 'Die Adresse &lt;@var_email@&gt; geh&ouml;rt nicht  zu den Empf&auml;ngern der Liste &lt;@liste@&gt;.',
'patientez' => 'Bitte warten...',


// Q
'quitter' => 'Abbestellen',


// S
'subject_removed' => 'Ihre Adresse wurde aus der Liste @liste@ gel&ouml;scht.',


// T
'titrefenetre' => 'Abonnieren',


// V
'veuillez' => 'Bitte geben Sie Ihre Adresse an.',
'votreemail' => 'Ihre E-Mail Adresse:'

);


?>
