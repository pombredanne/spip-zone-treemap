<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'The address &lt;@var_email@&gt; will be added to the list &lt;@liste@&gt; once it has been checked. Please reply to the message which has just been sent to you.',


// C
'confirm' => 'A confirmation request has been sent to &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;: You are already subcribed to the &lt;@liste@&gt; list.',
'desabo' => 'Subscription cancelled.',


// F
'fermer' => 'close',


// I
'inscription' => 'Subscription to &lt;@liste@&gt;',


// M
'mail_removed' => '

The address &lt;@var_email@&gt; has been removed from the @liste@ list.

If there is a problem, or if you did not request this cancellation
please write to &lt;@responsable@&gt;.

Goodbye and thanks.
',
'merci' => 'Thank you.',


// P
'pasabo' => 'The address &lt;@var_email@&gt; is not subscribed the &lt;@liste@&gt; list.',
'patientez' => 'Please wait...',


// Q
'quitter' => 'Cancellation',


// S
'subject_removed' => 'Your address has been removed from the @liste@ list.',


// T
'titrefenetre' => 'Subscribe',


// V
'veuillez' => 'Please give your address.',
'votreemail' => 'Your email:'

);


?>
