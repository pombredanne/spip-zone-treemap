<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'Osoite &lt;@var_email@&gt; lis&auml;t&auml;&auml;n listaan &lt;@liste@&gt; kunhan se on varmennettu. Vastaa viestiin, joka on juuri l&auml;hetetty sinulle.',


// C
'confirm' => 'Varmennuspyynt&ouml; on l&auml;hetetty osoitteseen &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;: olet jo listan &lt;@liste@&gt; tilaajana.',
'desabo' => 'Tilaus peruttu.',


// F
'fermer' => 'sulje',


// I
'inscription' => 'Tilaus listaan &lt;@liste@&gt;',


// M
'mail_removed' => '

Osoite &lt;@var_email@&gt; on poistettu listalta @liste@.

Jos sinulla on ongelmia, tai et pyyt&auml;nyt peruutusta niin ota yhteys osoitteseen
&lt;@responsable@&gt;.

Kiitos ja n&auml;kemiin.
',
'merci' => 'Kiitos.',


// P
'pasabo' => 'Osoite &lt;@var_email@&gt; ei ole &lt;@liste@&gt; listan tilaaja.',
'patientez' => 'Odota...',


// Q
'quitter' => 'Peruutus',


// S
'subject_removed' => 'Osoitteesi on poistettu listalta @liste@.',


// T
'titrefenetre' => 'Tilaa',


// V
'veuillez' => 'Anna osoitteesi.',
'votreemail' => 'Sinun s&auml;hk&ouml;posti:'

);


?>
