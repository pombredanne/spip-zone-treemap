<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'Ladrs &lt;@var_email@&gt; i sar azout&eacute; andan lo list &lt;@liste@&gt; kank zot lora v&eacute;rifi&eacute; ali. M&egrave;rsi r&eacute;ponn lo mod&eacute;kri ni la fin anvway&eacute; azot.',


// C
'confirm' => 'N&eacute;na in domann lakofirmasyon sak la fin d&egrave;t anvway&eacute; si ladr&egrave;s &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;&nbsp;: ou l&eacute; d&eacute;za inskri ast&egrave;r si lo &lt;@liste@&gt;.',
'desabo' => 'Lo d&eacute;zabonman la finn mars&eacute; ast&egrave;r.',


// F
'fermer' => 'Rof&egrave;rm',


// I
'inscription' => 'Inskrisyon si &lt;@liste@&gt;',


// M
'mail_removed' => ' 

Ladrs <@var_email@> la finn d&egrave;t d&eacute;gr&eacute;n&eacute; si lo list @liste@.

Si n&eacute;na in larlik ou si ou la pa ryin dmand&eacute; pou lo d&eacute;zabonman,
m&egrave;rsi &eacute;kri <@responsable@>.

M&egrave;rsi aou, nartrouv&eacute;.
',
'merci' => 'M&egrave;rsi aou.',


// P
'pasabo' => 'Ladr&egrave;s &lt;@var_email@&gt;  li l&eacute; pa abon&eacute; si lo list &lt;@liste@&gt;.',
'patientez' => 'M&egrave;si &eacute;sp&egrave;r...',


// Q
'quitter' => 'D&eacute;zabonman',


// S
'subject_removed' => 'Out ladr&egrave;s la finn d&egrave;t d&eacute;gr&eacute;n&eacute; si lo list @liste@.',


// T
'titrefenetre' => 'Inskri aou',


// V
'veuillez' => 'M&egrave;rsi pr&eacute;siz&eacute; out ladr&egrave;s.',
'votreemail' => 'Out lim&egrave;l&nbsp;:'

);


?>
