<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'La direcci&oacute;n &lt;@var_email@&gt; ser&aacute; agregada a la lista &lt;@liste@&gt; luego de ser verificada. Se ruega responder al mensaje que acaba de ser enviado.',


// C
'confirm' => 'Un pedido de confirmaci&oacute;n fue enviado a  &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;: Ya est&aacute;s inscrita o inscrito a la lista &lt;@liste@&gt;.',
'desabo' => 'Desuscripci&oacute;n efectuada.',


// F
'fermer' => 'cerrar',


// I
'inscription' => 'Inscripci&oacute;n a &lt;@liste@&gt;',


// M
'mail_removed' => '

Se suprimi&oacute; la direcci&oacute;n <@var_email@> de la lista @liste@.

En caso de problema, o si no fuiste t&uacute; quien pidi&oacute; esta cancelaci&oacute;n de suscripci&oacute;n,
no dudes en contactar <@responsable@>.

Hasta luego, y gracias.
',
'merci' => 'Gracias.',


// P
'pasabo' => 'La direcci&oacute;n &lt;@var_email@&gt; no est&aacute; suscrita a la lista &lt;@liste@&gt;.',
'patientez' => 'Un momento, por favor...',


// Q
'quitter' => 'Desuscripci&oacute;n',


// S
'subject_removed' => 'Se suprimi&oacute; tu direcci&oacute;n de la lista @liste@.',


// T
'titrefenetre' => 'Inscripci&oacute;n',


// V
'veuillez' => 'Por favor indica tu direcci&oacute;n.',
'votreemail' => 'Tu e-milio:'

);


?>
