<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'La adreso &lt;@var_email@&gt; estos aldonita al la listo, post kontrolo. Bonvolu respondi al la mesa&#285;o ke &#309;us estis sendita al vi.',


// C
'confirm' => 'Peto pri konfirmado estis sendita al &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;&nbsp;: vi jam estas registrigita en la listo &lt;@liste@&gt;.',
'desabo' => 'Malabonita.',


// F
'fermer' => 'fermi',


// I
'inscription' => 'Ali&#285;o al &lt;@liste@&gt;',


// M
'mail_removed' => '

La adreso <@var_email@> estis forigita de la listo @liste@.

Se vis havas problemon, a&#365; se vi ne petis tiun malabonon,
bonvolu skribi al <@responsable@>.

&#284;is la revido, kaj dankon.
',
'merci' => 'Dankon.',


// P
'pasabo' => 'La adreso &lt;@var_email@&gt; ne estas abonita en la listo &lt;@liste@&gt;.',
'patientez' => 'Bonvolu atendi...',


// Q
'quitter' => 'Malabono',


// S
'subject_removed' => 'Via adreso estis forigita de la listo @liste@.',


// T
'titrefenetre' => 'Registrado',


// V
'veuillez' => 'Bonvolu precizigi vian adreson.',
'votreemail' => 'Via retpo&#349;tadreso&nbsp;:'

);


?>
