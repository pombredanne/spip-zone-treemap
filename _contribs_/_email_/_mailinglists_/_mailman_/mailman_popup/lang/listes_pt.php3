<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'O endere&ccedil;o &lt;@var_email@&gt; ser&aacute; acrescentado &agrave; lista &lt;@liste@&gt; ap&oacute;s confirma&ccedil;&atilde;o. Por favor responda &agrave; mensagem que acaba de lhe ser enviada.',


// C
'confirm' => 'Um pedido de confirma&ccedil;&atilde;o foi dirigido a &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;&nbsp;: j&aacute; est&aacute; inscrito(a) na lista &lt;@liste@&gt;.',
'desabo' => 'Fim da assinatura efectuado.',


// F
'fermer' => 'fechar',


// I
'inscription' => 'Inscri&ccedil;&atilde;o a &lt;@liste@&gt;',


// M
'mail_removed' => '

O endere&ccedil;o <@var_email@> foi retirado da lista @liste@.

Se houver problema, ou se n&atilde;o pediu esse fim de assinatura,
favor escrever a <@responsable@>.

Adeus, e obrigado.
',
'merci' => 'Obrigado',


// P
'pasabo' => 'O endere&ccedil;o &lt;@var_email@&gt; n&atilde;o assinou a lista  &lt;@liste@&gt;.',
'patientez' => 'Aguarde, por favor...',


// Q
'quitter' => 'Fim da assinatura',


// S
'subject_removed' => 'O seu endere&ccedil;o foi retirado da lista @liste@.',


// T
'titrefenetre' => 'Inscri&ccedil;&atilde;o',


// V
'veuillez' => 'Favor indicar o seu endere&ccedil;o.',
'votreemail' => 'O seu email&nbsp;:'

);


?>
