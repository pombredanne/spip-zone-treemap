<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'D\'Adress &lt;@var_email@&gt; g&euml;tt op d\'L&euml;scht &lt;@liste@&gt; no enger Kontroll abonn&eacute;iert. &Auml;ntwert weg op de Message deen Iech gesch&eacute;ckt ginn ass.',


// C
'confirm' => 'Eng Konfirmatiouns-Demande ass un d\'Adress &lt;@var_email@&gt; gesch&eacute;ckt ginn.',


// D
'deja' => '&lt;@var_email@&gt;&nbsp;: dir sidd schon un der L&euml;scht &lt;@liste@&gt; abonn&eacute;iert.',
'desabo' => 'Annul&eacute;iert.',


// F
'fermer' => 'zoumaachen',


// I
'inscription' => 'Aschreiwung op d\'L&euml;scht &lt;@liste@&gt;',


// M
'mail_removed' => '

D\'Adress <@var_email@> ass vun der L&euml;scht @liste@ gel&auml;scht ginn.

Wann dat e Problem ass oder wann dier d&euml;s Annulatioun n&euml;t gefrot hut,
schreiwt weg un <@responsable@>.

Au revoir a merci.
',
'merci' => 'Merci.',


// P
'pasabo' => 'D\'Adress &lt;@var_email@&gt; ass n&euml;t un der L&euml;scht &lt;@liste@&gt; abonn&eacute;iert.',
'patientez' => 'Waart weg...',


// Q
'quitter' => 'Ofmelden',


// S
'subject_removed' => '&Auml;r Adress ass vun der L&euml;scht @liste@ gel&auml;scht ginn.',


// T
'titrefenetre' => 'Aschreiwen',


// V
'veuillez' => 'Gidd weg &auml;r Adress un.',
'votreemail' => '&Auml;r Email-Adress:'

);


?>
