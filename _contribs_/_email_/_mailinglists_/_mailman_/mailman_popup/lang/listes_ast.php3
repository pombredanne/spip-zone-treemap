<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'La direci&oacute;n &lt;@var_email@&gt; va axuntase a la llista &lt;@liste@&gt; llueu de verificase. Por favor, responde al corr&eacute;u que vamos unviate de secute.',


// C
'confirm' => 'Unvi&oacute;se un corr&eacute;u de confirmaci&oacute;n a &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;&nbsp;: ya ta apunt&aacute;u(a) a la llista &lt;@liste@&gt;.',
'desabo' => 'Fechu el desapuntamientu.',


// F
'fermer' => 'pesllar',


// I
'inscription' => 'Apuntase a &lt;@liste@&gt;',


// M
'mail_removed' => '

La direci&oacute;n <@var_email@> quitose de la llista @liste@.

En casu de problemes, o si non pediste que se quitara,
escribe a <@responsable@>.

Al&oacute;n, y gracies.
',
'merci' => 'Gracies.',


// P
'pasabo' => 'La direci&oacute;n &lt;@var_email@&gt; nun ta apuntada la llista &lt;@liste@&gt;.',
'patientez' => 'Un moment&iacute;n...',


// Q
'quitter' => 'Borrase',


// S
'subject_removed' => 'la to direci&oacute;n borrose de la llista @liste@.',


// T
'titrefenetre' => 'Apuntase',


// V
'veuillez' => 'Tienes que poner la to direci&oacute;n.',
'votreemail' => 'El to email:'

);


?>
