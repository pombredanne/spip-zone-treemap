<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'Adressen &lt;@var_email@&gt; vil bli lagt til epostlisten &lt;@liste@&gt; s&aring; fort den er kontrollert. Vennligst svar p&aring; den eposten som er sendt deg.',


// C
'confirm' => 'En bekreftselsesforesp&oslash;rsel er sendt til &lt;@var_email@&gt;.',


// D
'deja' => '<MODIF> &lt;@var_email@&gt;: Du har allerede meldt deg p&aring; epostlisten &lt;@liste@&gt;.',
'desabo' => 'P&aring;melding avbrutt.',


// F
'fermer' => 'steng',


// I
'inscription' => 'P&aring;melding til &lt;@liste@&gt;',


// M
'mail_removed' => '

Adressen &lt;@var_email@&gt; er fjernet fra epostlisten @liste@.

Hvis du er et problem eller hvis du ikke har bedt om &aring; meldes av
vennligst skriv til &lt;@responsable@&gt;.

Takk, ha en fin dag.
',
'merci' => 'Takk.',


// P
'pasabo' => 'Adressen &lt;@var_email@&gt; er ikke p&aring;meldt epostlisten &lt;@liste@&gt;.',


// Q
'quitter' => 'Avmelding',


// S
'subject_removed' => 'Adressen din er meldt av epostlisten @liste@.',


// T
'titrefenetre' => '<NEW> Inscription',


// V
'veuillez' => 'Oppgi din adresse.',
'votreemail' => 'Din epost:'

);


?>
