<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'L\'adre&ccedil;a &lt;@var_email@&gt; ser&agrave; afegida a la llista &lt;@liste@&gt; despr&eacute;s de la verificaci&oacute;. Gr&agrave;cies per respondre al missatge que acaba de ser-vos enviat.',


// C
'confirm' => 'Una petici&oacute; de confirmaci&oacute; ha sigut dirigida cap a &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;&nbsp;: Ja est&agrave; subscrit(a) a la llista &lt;@liste@&gt;.',
'desabo' => 'Desubscripci&oacute; efectuada',


// F
'fermer' => 'tancar',


// I
'inscription' => 'Inscripci&oacute; a &lt;@liste@&gt;',


// M
'mail_removed' => ' 

L\'adre&ccedil;a <@var_email@> ha estat suprimida de la llista @liste@.

En cas de problemes, o si no ha  demanat aquesta desubscripci&oacute;,
si us plau escrigau a <@responsable@>.

Fins aviat i gr&agrave;cies.
',
'merci' => 'Gr&agrave;cies.',


// P
'pasabo' => 'L\'adre&ccedil;a &lt;@var_email@&gt; no est&agrave; pas abonada a la llista &lt;@liste@&gt;.',
'patientez' => 'Esperi, si us plau...',


// Q
'quitter' => 'Desubscripci&oacute;',


// S
'subject_removed' => 'La vostre adre&ccedil;a ha estat suprimida de la llista @liste@.',


// T
'titrefenetre' => 'Inscripci&oacute;',


// V
'veuillez' => 'Si us plau indiqueu la vostre adre&ccedil;a.',
'votreemail' => 'El vostre email&nbsp;:'

);


?>
