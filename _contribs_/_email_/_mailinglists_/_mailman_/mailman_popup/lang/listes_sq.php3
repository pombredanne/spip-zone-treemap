<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'Adresa &lt;@var_email@&gt; do t&euml; shtohet n&euml; list&euml;n &lt;@liste@&gt; sapo t&euml; shqyrtohet. Ju lutemi, p&euml;rgjigjiuni mesazhit t&euml; konfirmimit q&euml; do ju d&euml;rgohet me email.',


// C
'confirm' => 'K&euml;rkesa p&euml;r konfirmim u d&euml;rgua n&euml; adres&euml;n  &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;&nbsp;: kjo adres&euml; ishte e pranishme n&euml; list&euml;n &lt;@liste@&gt;.',
'desabo' => 'Fshirja nga lista u krye.',


// F
'fermer' => 'mbyll',


// I
'inscription' => 'Regjistrim n&euml; list&euml;n &lt;@liste@&gt;',


// M
'mail_removed' => '

Adresa <@var_email@> u fshi nga lista @liste@.

N&euml; rast se ka probleme apo n&euml;se kjo fshirje ishte e pad&euml;shiruar,
shkruani tek <@responsable@>.

Faleminderit.
',
'merci' => 'Faleminderit.',


// P
'pasabo' => 'Adresa &lt;@var_email@&gt; nuk &euml;sht&euml; e pranishme n&euml; list&euml;n &lt;@liste@&gt;.',
'patientez' => 'Ju lutem prisni...',


// Q
'quitter' => 'Fshirje nga lista',


// S
'subject_removed' => 'Adresa juaj &euml;sht&euml; fshir&euml; nga lista @liste@.',


// T
'titrefenetre' => 'Regjistrim',


// V
'veuillez' => 'Vendosni adres&euml;n tuaj email.',
'votreemail' => 'Adresa email:'

);


?>
