<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'L\'adrei&ccedil;a &lt;@var_email@&gt; s\'apondr&agrave; a la lista &lt;@liste@&gt; apr&egrave;s verificacion. Merc&eacute; de resp&ograve;ndre au messatge que ven&egrave;m de vos mandar.',


// C
'confirm' => 'Una demanda de confirmacion s\'es adrei&ccedil;ada a &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;&nbsp;: ja siatz inscrich(a) a la lista &lt;@liste@&gt;.',
'desabo' => 'Desabonament fach.',


// F
'fermer' => 'barrar',


// I
'inscription' => 'Inscripcion a &lt;@liste@&gt;',


// M
'mail_removed' => '

L\'adrei&ccedil;a <@var_email@> es estada suprimida de la lista @liste@.

En cas de probl&egrave;ma, &ograve; se non demandat minga d\'abonament,
vorgatz escriure a <@responsable@>.

Adieu-siatz e merc&eacute;.
',
'merci' => 'Merc&eacute;.',


// P
'pasabo' => 'L\'adrei&ccedil;a &lt;@var_email@&gt; non es abonada a la lista &lt;@liste@&gt;.',
'patientez' => 'Merc&eacute; de pacientar...',


// Q
'quitter' => 'Desabonament',


// S
'subject_removed' => 'S\'es suprimit la v&ograve;stra adrei&ccedil;a de la lista @liste@.',


// T
'titrefenetre' => 'Inscripcion',


// V
'veuillez' => 'Vorgatz precisar la v&ograve;stra adrei&ccedil;a.',
'votreemail' => 'Lo v&ograve;stre e-mail:'

);


?>
