<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => '&lt;@var_email@&gt; adresi denetlemeden sonra &lt;@liste@&gt; listesine eklenecek. Size g&ouml;nderilen iletiyi yan&#305;tlad&#305;&#287;&#305;n&#305;z i&ccedil;in te&#351;ekk&uuml;r ederiz.',


// C
'confirm' => '&lt;@var_email@&gt; adresine bir kay&#305;t onay iletisi g&ouml;nderildi.',


// D
'deja' => '&lt;@var_email@&gt;&nbsp;: siz zaten &lt;@liste@&gt; listesine kay&#305;tl&#305;s&#305;n&#305;z.',
'desabo' => 'Abonelik iptal edildi.',


// F
'fermer' => 'kapat',


// I
'inscription' => '&lt;@liste@&gt; listesine kay&#305;t ',


// M
'mail_removed' => '

 <@var_email@> adresi @liste@ listesinden silinmi&#351;.

Problem olursa veya abonelik iptalini siz istemediyseniz,
l&uuml;tfen <@responsable@> adresine yaz&#305;n&#305;z.  

Te&#351;ekk&uuml;rler. G&ouml;r&uuml;&#351;mek &uuml;zere.
',
'merci' => 'Te&#351;ekk&uuml;rler.',


// P
'pasabo' => '&lt;@var_email@&gt; adresi  &lt;@liste@&gt; listesine abone de&#287;il.',
'patientez' => 'L&uuml;tfen bekleyiniz...',


// Q
'quitter' => 'Abonelik iptali',


// S
'subject_removed' => 'Adresiniz @liste@ listesinden silinmi&#351;.',


// T
'titrefenetre' => 'Kay&#305;t',


// V
'veuillez' => 'L&uuml;tfen adresinizi belirtiniz.',
'votreemail' => 'E-posta adresiniz&nbsp;:'

);


?>
