<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'L\'indirizzo &lt;@var_email@&gt; sar&agrave; aggiunto all\'elenco &lt;@liste@&gt; dopo essere stato verificato. Rispondere al messaggio di conferma che vi verr&agrave; inviato, per favore.',


// C
'confirm' => 'La richiesta di conferma &egrave; stata inviata a &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;&nbsp;: indirizzo gi&agrave; presente nella mailing list &lt;@liste@&gt;.',
'desabo' => 'La cancellazione dalla lista &egrave; stata effettuata.',


// F
'fermer' => 'chiudi',


// I
'inscription' => 'Iscrizione a &lt;@liste@&gt;',


// M
'mail_removed' => '

L\'indirizzo <@var_email@> &egrave; stato cancellato dalla lista @liste@.

In caso di problemi, oppure se tale cancellazione non &egrave; stata richiesta,
scrivere a <@responsable@>.

Grazie.
',
'merci' => 'Grazie.',


// P
'pasabo' => 'L\'indirizzo &lt;@var_email@&gt; non risulta abbonato alla lista &lt;@liste@&gt;.',
'patientez' => 'Attendere prego...',


// Q
'quitter' => 'Cancellazione dalla lista',


// S
'subject_removed' => 'Il suo indirizzo &egrave; stato cancellato dalla lista @liste@.',


// T
'titrefenetre' => 'Iscrizione',


// V
'veuillez' => 'Specificare il proprio indirizzo email.',
'votreemail' => 'Indirizzo email:'

);


?>
