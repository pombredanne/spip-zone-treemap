<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'Adres &lt;@var_email@&gt; zostanie dodany do listy &lt;@liste@&gt; po koniecznej weryfikacji. Prosimy o odpowied&#378; na wiadomo&#347;&#263;, kt&oacute;ra w&#322;a&#347;nie zosta&#322;a do Ciebie wys&#322;ana.',


// C
'confirm' => 'Zapytanie o potwierdzenie zosta&#322;o zaadresowane do &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;&nbsp;: jeste&#347; ju&#380; zapisany(a) na list&#281; &lt;@liste@&gt;.',
'desabo' => 'Adres usuni&#281;ty z powodzeniem.',


// F
'fermer' => 'zamknij',


// I
'inscription' => 'Zapis na list&#281; &lt;@liste@&gt;',


// M
'mail_removed' => '

Adres <@var_email@> zosta&#322; usuni&#281;ty z listy @liste@.

W przypadku jakich&#347; problem&oacute;w, lub je&#347;li nie prosi&#322;e&#347; o usuni&#281;cie,
prosimy o kontakt z odpowiedzialnym <@responsable@>.

Dzi&#281;kujemy, mi&#322;ego dnia.
',
'merci' => 'Dzi&#281;kujemy.',


// P
'pasabo' => 'Adresu &lt;@var_email@&gt; nie ma na li&#347;cie &lt;@liste@&gt;.',
'patientez' => 'Prosimy o chwil&#281; cierpliwo&#347;ci...',


// Q
'quitter' => 'Usuni&#281;cie',


// S
'subject_removed' => 'Tw&oacute;j adres zosta&#322; usuni&#281;ty z listy @liste@.',


// T
'titrefenetre' => 'Zapisz si&#281;',


// V
'veuillez' => 'Prosimy o podanie adresu.',
'votreemail' => 'Tw&oacute;j e-mail&nbsp;:'

);


?>
