<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'nyik&#596; &lt;@var_email@&amp;gt ; na g&#596;na &#598;i&#598;&egrave;m&#603; &#596; &lt;@liste@&amp;gt, nu mi v&#596; l&#603; kp&#596;n fo &#596;. Mi nu mi na yi gb&egrave; nu w&#603;n &eacute; s&#603; &#598;o mi &eacute; ',


// C
'confirm' => 'Y&eacute; &#598;al&#596;wemam&#603; bo  s&#603;  &#598;o &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;: Mi ko na nyik&#596; mi t&#596;n &lt;@liste@&gt;.',
'desabo' => '&#598;i&#598;et&#596;n sin agun &#596; m&#603; &eacute; ko s&#596; gb&egrave;',


// F
'fermer' => 'Su',


// I
'inscription' => 'nyik&#596; ni na &#598;o &lt;@liste@&gt;',


// M
'mail_removed' => 'nyik&#596; <@var_email@> sunsun sin agun &#596; m&#603; @liste@.Nu n&#598;&eacute; j&#596; &#596; , kabi nu mi ma by&#596; &#598;i&#598;et&#596;n si m&#603; a ,Mi k&#603;nklin bo wlan <@ga&#770;n@>.O &#598;ab&#596;, mi ku&#598;&eacute;wu.',
'merci' => 'mi kudewu.',


// P
'pasabo' => 'nyik&#596; &lt;@var_emailu@&gt; &eacute; lo agun &#596; m&#603; a  &lt;@liste@&gt;.',
'patientez' => 'Mi k&#603;nklin bo kuhun ',


// Q
'quitter' => '&#598;i&#598;et&#596;nsim&#603; ',


// S
'subject_removed' => 'Y&eacute; sunsun nyik&#596; mit&#596;n sin agun &#596; m&#603; @liste@.',


// T
'titrefenetre' => 'nyik&#596; ni na ',


// V
'veuillez' => 'Mi k&#603;nkl&#603;n mi l&#603; v&#596; t&#603;&#598;&#603; nyik&#596; mi t&#596;n ji ',
'votreemail' => 'e-mailu mi t&#596;n '

);


?>
