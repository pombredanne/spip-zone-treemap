<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'Het adres &lt;@var_email@&gt; zal na verificatie aan de lijst &lt;@liste@&gt; worden toegevoegd. Gelieve het bericht te beantwoorden dat je werd toegestuurd.',


// C
'confirm' => 'Een vraag voor bevestiging is gestuurd naar &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;&nbsp;: je bent al ingeschreven op de lijst &lt;@liste@&gt;.',
'desabo' => 'Uitschrijving is voltooid.',


// F
'fermer' => 'sluiten',


// I
'inscription' => 'Inschrijving bij &lt;@liste@&gt;',


// M
'mail_removed' => '

Het adres &lt;@var_email@&gt; is van de lijst @liste@ geschrapt.

Bij problemen of indien je de uitschrijving niet hebt gevraagd, kan je &lt;@responsable@&gt; contacteren.

Bedankt en tot ziens.
',
'merci' => 'Bedankt.',


// P
'pasabo' => 'Het adres &lt;@var_email@&gt; is niet ingeschreven op de lijst &lt;@liste@&gt;.',
'patientez' => 'Even geduld...',


// Q
'quitter' => 'Uitschrijving',


// S
'subject_removed' => 'Je adres is geschrapt van de lijst @liste@.',


// T
'titrefenetre' => 'Inschrijving',


// V
'veuillez' => 'Gelieve je adres op te geven.',
'votreemail' => 'Je e-mail&nbsp;:'

);


?>
