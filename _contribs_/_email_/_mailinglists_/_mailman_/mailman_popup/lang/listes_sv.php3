<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'Adressen &lt;@var_email@&gt; kommer att l&auml;ggas till listan &lt;@liste@&gt; n&auml;r den har blivit kontrollerad. Var god och svara p&aring; meddelandet som just skickats till dig.',


// C
'confirm' => 'En beg&auml;ran om bekr&auml;ftelse har skickats till &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;: Du prenumerar redan p&aring; &lt;@liste@&gt; listan.',
'desabo' => 'Prenumeration avbruten.',


// F
'fermer' => 'st&auml;ng',


// I
'inscription' => 'Prenumerera p&aring; &lt;@liste@&gt;',


// M
'mail_removed' => '

Adressen &lt;@var_email@&gt; har tagits bort som prenumerant fr&aring;n @liste@ listan.
Om det orsakar problem, eller om du inte beg&auml;rt detta: skriv till &lt;@responsable@&gt;.
Hejd&aring; och tack.
',
'merci' => 'Tack',


// P
'pasabo' => 'Adressen &lt;@var_email@&gt; &auml;r inte anm&auml;ld som prenumerant p&aring; &lt;@liste@&gt; listan.',
'patientez' => 'Var god dr&ouml;j...',


// Q
'quitter' => 'Avssluta',


// S
'subject_removed' => 'Din adress har tagits bort fr&aring;n @liste@ listan.',


// T
'titrefenetre' => 'Prenumerera',


// V
'veuillez' => 'Var god ange din adress.',
'votreemail' => 'Din epost:'

);


?>
