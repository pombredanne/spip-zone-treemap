<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => '<NEW> L\'adresse &lt;@var_email@&gt; sera ajout&eacute;e &agrave; la liste &lt;@liste@&gt; apr&egrave;s v&eacute;rification. Merci de r&eacute;pondre au message qui vient de vous &ecirc;tre envoy&eacute;.',


// C
'confirm' => '<NEW> Une demande de confirmation a &eacute;t&eacute; adress&eacute;e &agrave; &lt;@var_email@&gt;.',


// D
'deja' => '<NEW> &lt;@var_email@&gt;&nbsp;: vous &ecirc;tes d&eacute;j&agrave; inscrit(e) &agrave; la liste &lt;@liste@&gt;.',
'desabo' => '<NEW> D&eacute;sabonnement effectu&eacute;.',


// F
'fermer' => '<NEW> fermer',


// I
'inscription' => '<NEW> Inscription &agrave; &lt;@liste@&gt;',


// M
'mail_removed' => '<NEW> 

L\'adresse <@var_email@> a &eacute;t&eacute; supprim&eacute;e de la liste @liste@.

En cas de probl&egrave;me, ou si vous n\'avez pas demand&eacute; ce d&eacute;sabonnement,
veuillez &eacute;crire &agrave; <@responsable@>.

Au revoir, et merci.
',
'merci' => '<NEW> Merci.',


// P
'pasabo' => '<NEW> L\'adresse &lt;@var_email@&gt; n\'est pas abonn&eacute;e &agrave; la liste &lt;@liste@&gt;.',


// Q
'quitter' => '<NEW> D&eacute;sabonnement',


// S
'subject_removed' => '<NEW> Votre adresse a &eacute;t&eacute; supprim&eacute;e de la liste @liste@.',


// T
'titrefenetre' => '<NEW> Inscription',


// V
'veuillez' => '<NEW> Veuillez pr&eacute;ciser votre adresse.',
'votreemail' => '<NEW> Votre email&nbsp;:'

);


?>
