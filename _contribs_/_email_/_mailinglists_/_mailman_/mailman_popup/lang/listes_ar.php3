<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => '&#1587;&#1578;&#1578;&#1605; &#1575;&#1590;&#1575;&#1601;&#1577; &#1575;&#1604;&#1593;&#1606;&#1608;&#1575;&#1606; &lt;@var_email@&gt; &#1575;&#1604;&#1609; &#1575;&#1604;&#1602;&#1575;&#1574;&#1605;&#1577; &#1575;&#1604;&#1576;&#1585;&#1610;&#1583;&#1610;&#1577; &lt;@liste@&gt; &#1576;&#1593;&#1583; &#1575;&#1604;&#1578;&#1583;&#1602;&#1610;&#1602;. &#1575;&#1604;&#1585;&#1580;&#1575;&#1569; &#1575;&#1604;&#1585;&#1583; &#1593;&#1604;&#1609; &#1575;&#1604;&#1585;&#1587;&#1575;&#1604;&#1577; &#1575;&#1604;&#1578;&#1610; &#1575;&#1587;&#1578;&#1604;&#1605;&#1578;&#1607;&#1575;.',


// C
'confirm' => '&#1578;&#1605; &#1575;&#1585;&#1587;&#1575;&#1604; &#1591;&#1604;&#1576; &#1578;&#1579;&#1576;&#1610;&#1578; &#1575;&#1604;&#1609; &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;: &#1575;&#1606;&#1578; &#1605;&#1587;&#1580;&#1604; &#1605;&#1587;&#1576;&#1602;&#1575;&#1611; &#1601;&#1610; &#1575;&#1604;&#1602;&#1575;&#1574;&#1605;&#1577; &#1575;&#1604;&#1576;&#1585;&#1610;&#1583;&#1610;&#1577; &lt;@liste@&gt;.',
'desabo' => '&#1578;&#1605; &#1575;&#1604;&#1594;&#1575;&#1569; &#1575;&#1604;&#1575;&#1588;&#1578;&#1585;&#1575;&#1603;.',


// F
'fermer' => '&#1575;&#1602;&#1601;&#1575;&#1604;',


// I
'inscription' => '&#1575;&#1604;&#1578;&#1587;&#1580;&#1610;&#1604; &#1601;&#1610; &lt;@liste@&gt;',


// M
'mail_removed' => '

&#1578;&#1605; &#1581;&#1584;&#1601; &#1575;&#1604;&#1593;&#1606;&#1608;&#1575;&#1606; &lt;@var_email@&gt; &#1605;&#1606; &#1575;&#1604;&#1602;&#1575;&#1574;&#1605;&#1577; &#1575;&#1604;&#1576;&#1585;&#1610;&#1583;&#1610;&#1577; @liste@.

&#1601;&#1610; &#1581;&#1575;&#1604; &#1581;&#1589;&#1608;&#1604; &#1605;&#1588;&#1603;&#1604;&#1577;&#1548; &#1575;&#1608; &#1575;&#1584;&#1575; &#1604;&#1605; &#1578;&#1603;&#1606; &#1602;&#1583; &#1591;&#1604;&#1576;&#1578; &#1575;&#1604;&#1594;&#1575;&#1569; &#1575;&#1604;&#1575;&#1588;&#1578;&#1585;&#1575;&#1603;&#1548;
&#1575;&#1604;&#1585;&#1580;&#1575;&#1569; &#1575;&#1604;&#1603;&#1578;&#1575;&#1576;&#1577; &#1575;&#1604;&#1609; &lt;@responsable@&gt;.

&#1588;&#1603;&#1585;&#1575;&#1611; &#1608;&#1575;&#1604;&#1587;&#1604;&#1575;&#1605;.
',
'merci' => '&#1588;&#1603;&#1585;&#1575;&#1611;.',


// P
'pasabo' => '&#1575;&#1604;&#1593;&#1606;&#1608;&#1575;&#1606; &lt;@var_email@&gt; &#1604;&#1610;&#1587; &#1605;&#1588;&#1578;&#1585;&#1603;&#1575;&#1611; &#1601;&#1610; &#1575;&#1604;&#1602;&#1575;&#1574;&#1605;&#1577; &#1575;&#1604;&#1576;&#1585;&#1610;&#1583;&#1610;&#1577; &lt;@liste@&gt;. ',
'patientez' => '&#1575;&#1604;&#1585;&#1580;&#1575;&#1569; &#1575;&#1604;&#1575;&#1606;&#1578;&#1592;&#1575;&#1585;...',


// Q
'quitter' => '&#1575;&#1604;&#1594;&#1575;&#1569; &#1575;&#1604;&#1575;&#1588;&#1578;&#1585;&#1575;&#1603;',


// S
'subject_removed' => '&#1578;&#1605; &#1581;&#1584;&#1601; &#1593;&#1606;&#1608;&#1575;&#1606;&#1603; &#1605;&#1606; &#1575;&#1604;&#1602;&#1575;&#1574;&#1605;&#1577; &#1575;&#1604;&#1576;&#1585;&#1610;&#1583;&#1610;&#1577; @liste@.',


// T
'titrefenetre' => '&#1578;&#1587;&#1580;&#1610;&#1604;',


// V
'veuillez' => '&#1575;&#1604;&#1585;&#1580;&#1575;&#1569; &#1575;&#1583;&#1582;&#1575;&#1604; &#1593;&#1606;&#1608;&#1575;&#1606;&#1603;.',
'votreemail' => '&#1593;&#1606;&#1608;&#1575;&#1606;&#1603; &#1575;&#1604;&#1576;&#1585;&#1610;&#1583;&#1610;:'

);


?>
