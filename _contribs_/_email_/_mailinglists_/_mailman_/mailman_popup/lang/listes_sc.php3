<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'S\'indiritzu &lt;@var_email@&gt; at a esser annantu a s\'elencu &lt;@liste@&gt; pustis verificadu. Risponde a su messazu de cunfirma chi t\'at a esser imbiadu, pro praghere.',


// C
'confirm' => 'Sa dimanda de cunfirma est istada imbiada a &lt;@var_email@&gt;.',


// D
'deja' => '<MODIF>&lt;@var_email@&gt;&nbsp;: ses zai marcadu in sa lista &lt;@liste@&gt;.',
'desabo' => 'Sa cantzellatzione dae sa lista est istada fata.',


// F
'fermer' => 'cunza',


// I
'inscription' => 'Iscritzione a &lt;@liste@&gt;',


// M
'mail_removed' => '

S\'indiritzu <@var_email@> est istadu cantzelladu dae sa  lista @liste@.

In caso di problemi, oppure se tale cancellazione non &egrave; stata richiesta,
scrivere a <@responsable@>.

Grazie.
',
'merci' => 'Gr&agrave;tzias.',


// P
'pasabo' => 'S\'indiritzu &lt;@var_email@&gt; non resurtat abonadu a sa lista &lt;@liste@&gt;.',
'patientez' => '<MODIF>Iseta pro praghere...',


// Q
'quitter' => 'Cantzellatzione dae sa lista',


// S
'subject_removed' => 'S\'indiritzu tuo nch\'est istadu cantzelladu dae sa lista @liste@.',


// T
'titrefenetre' => '<MODIF>Iscritzione',


// V
'veuillez' => 'Pone s\'indiritzu tuo de posta eletr&ograve;nica.',
'votreemail' => 'Indiritzu de post.e.:'

);


?>
