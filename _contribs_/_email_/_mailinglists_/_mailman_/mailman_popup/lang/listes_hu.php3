<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'A c&iacute;m &lt;@var_email@&gt; ker&uuml;l a &lt;@liste@&gt; list&aacute;ba ellen&#337;rz&eacute;s ut&aacute;n. El&#337;re k&ouml;sz&ouml;juk, hogy v&aacute;laszol a most k&uuml;ld&ouml;tt &uuml;zenetre.',


// C
'confirm' => 'Egy meger&#337;s&iacute;t&eacute;si k&eacute;relem lett k&uuml;ldve ennek: &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;&nbsp;: m&aacute;r szerepel a list&aacute;ban &lt;@liste@&gt;.',
'desabo' => 'Ki&iacute;ratkoz&aacute;s megt&ouml;rt&eacute;nt.',


// F
'fermer' => 'bez&aacute;r&aacute;s',


// I
'inscription' => 'Be&iacute;ratkoz&aacute;s a list&aacute;ban &lt;@liste@&gt;',


// M
'mail_removed' => '

A cim <@var_email@> lett t&ouml;r&ouml;lve a list&aacute;b&oacute;l @liste@.

En cas de probl&egrave;me, ou si vous n\'avez pas demand&eacute; ce d&eacute;sabonnement,
veuillez &eacute;crire &agrave; <@responsable@>.

Au revoir, et merci.
',
'merci' => 'K&ouml;sz&ouml;nj&uuml;k.',


// P
'pasabo' => 'A c&iacute;m &lt;@var_email@&gt; nem szerepel a list&aacute;ban &lt;@liste@&gt;.',
'patientez' => 'Egy kis t&uuml;relmet k&eacute;r&uuml;nk...',


// Q
'quitter' => 'Ki&iacute;ratkoz&aacute;s',


// S
'subject_removed' => 'Az &Ouml;n c&iacute;me kiker&uuml;lt a list&aacute;b&oacute;l @liste@.',


// T
'titrefenetre' => 'Be&iacute;ratkoz&aacute;s',


// V
'veuillez' => 'Legyen sz&iacute;ves pontos&iacute;tani az &Ouml;n c&iacute;m&eacute;t.',
'votreemail' => 'Az &Ouml;n e-mailje&nbsp;:'

);


?>
