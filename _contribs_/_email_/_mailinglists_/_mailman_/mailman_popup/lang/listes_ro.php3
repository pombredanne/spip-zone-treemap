<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'Adresa &lt;@var_email@&gt; va fi ad&#259;ugat&#259; la lista &lt;@liste@&gt; dup&#259; verificare. V&#259; rug&#259;m s&#259; r&#259;spunde&#355;i la mesajul care tocmai v-a fost trimis.',


// C
'confirm' => 'O cerere de confirmare a fost adresat&#259; la &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;&nbsp;: sunte&#355;i deja &icirc;nscris(&#259;) pe lista &lt;@liste@&gt;.',
'desabo' => 'Dezabonare efectuat&#259;.',


// F
'fermer' => '&icirc;nchide&#355;i',


// I
'inscription' => '&Icirc;nscriere la &lt;@liste@&gt;',


// M
'mail_removed' => '

Adresa <@var_email@> a fost &#351;tears&#259; de pe lista @liste@.

&Icirc;n caz de probleme, sau dac&#259; nu a&#355;i cerut aceast&#259; dezabonare,
v&#259; rug&#259;m s&#259; v&#259; adresa&#355;i la <@responsable@>.

La revedere &#351;i v&#259; mul&#355;umim.
',
'merci' => 'Mul&#355;umesc.',


// P
'pasabo' => 'Adresa &lt;@var_email@&gt; nu este abonat&#259; la lista &lt;@liste@&gt;.',
'patientez' => 'V&#259; rug&#259;m s&#259; a&#351;tepta&#355;i un moment...',


// Q
'quitter' => 'Dezabonare',


// S
'subject_removed' => 'Adresa dumneavoastr&#259; a fost &#351;tears&#259; de pe lista @liste@.',


// T
'titrefenetre' => '&Icirc;nscriere',


// V
'veuillez' => 'V&#259; rug&#259;m s&#259; preciza&#355;i adresa dumneavoastr&#259;.',
'votreemail' => 'Email-ul dumneavoastr&#259;&nbsp;:'

);


?>
