<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'L\'adre&ccedil;a &lt;@var_email@&gt; ser&agrave; ajustada a la tiera &lt;@liste@&gt; apr&egrave;s verificacion. Merc&eacute; de resp&ograve;ndre au messatge que vene de vos &egrave;stre mandat.',


// C
'confirm' => 'Une demanda de confirmacion es estada mandada a &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;&nbsp;: si&egrave;tz ja inscrich a la tiera &lt;@liste@&gt;.',
'desabo' => 'Desabonament efetuat.',


// F
'fermer' => 'plegar',


// I
'inscription' => 'Inscripcion a &lt;@liste@&gt;',


// M
'mail_removed' => '

L\'adre&ccedil;a &lt;@var_email@&gt; es estada suprimida de la tiera @liste@.

En cas de problema, &ograve; s\'avetz pas demandat aquel desabonament,
vorgatz escriure a &lt;@responsable@&gt;.

A ben l&egrave;u, e merc&eacute;.
',
'merci' => 'Merc&eacute;.',


// P
'pasabo' => 'L\'adre&ccedil;a &lt;@var_email@&gt; es pas abonada a la tiera &lt;@liste@&gt;.',
'patientez' => 'Vorgatz esperar...',


// Q
'quitter' => 'Desabonament',


// S
'subject_removed' => 'La v&ograve;stre adre&ccedil;a es estada suprimida de la tiera @liste@.',


// T
'titrefenetre' => 'Inscripcion',


// V
'veuillez' => 'Vorgatz precisar la v&ograve;stre adre&ccedil;a.',
'votreemail' => 'Lo v&ograve;stre e-mail&nbsp;:'

);


?>
