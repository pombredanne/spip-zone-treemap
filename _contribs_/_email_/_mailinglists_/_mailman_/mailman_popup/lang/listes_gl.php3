<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'O enderezo &lt;@var_email@&gt; ser&aacute; engadido &aacute; lista &lt;@liste@&gt; despois da verificaci&oacute;n. Agradeceremos que responda &aacute; mensaxe que acaba de vos ser enviada.',


// C
'confirm' => 'Unha solicitude de confirmaci&oacute;n foi dirixida a &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;&nbsp;: xa est&aacute; subscrito/a(e) &aacute; lista &lt;@liste@&gt;.',
'desabo' => 'Desubscrici&oacute;n efectuada.',


// F
'fermer' => 'pechar',


// I
'inscription' => 'Subscrici&oacute;n a &lt;@liste@&gt;',


// M
'mail_removed' => '

O enderezo <@var_email@> foi suprimido da lista @liste@.

En caso de problema, ou se vostede non ten solicitado a desubscrici&oacute;n,
escr&iacute;balle a  <@responsable@>.

Deica logo, e gracias.
',
'merci' => 'Gracias.',


// P
'pasabo' => 'O enderezo &lt;@var_email@&gt; non est&aacute; subscrito &aacute; lista &lt;@liste@&gt;.',
'patientez' => 'Te&ntilde;a un pouco de paciencia...',


// Q
'quitter' => 'Desubscrici&oacute;n',


// S
'subject_removed' => 'O seu enderezo foi suprimido da lista',


// T
'titrefenetre' => 'Inscrici&oacute;n',


// V
'veuillez' => 'Precise o seu enderezo',
'votreemail' => 'O seu correo&nbsp;:'

);


?>
