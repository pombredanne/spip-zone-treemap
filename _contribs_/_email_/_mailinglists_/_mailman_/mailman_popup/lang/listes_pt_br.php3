<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajoutee' => 'O endere&ccedil;o &lt;@var_email@&gt; ser&aacute; inclu&iacute;do na lista &lt;@liste@&gt; ap&oacute;s verifica&ccedil;&atilde;o. Por favor, responda &agrave; mensagem que acabou de lhe ser enviada.',


// C
'confirm' => 'Um pedido de confirma&ccedil;&atilde;o foi enviado para o endere&ccedil;o &lt;@var_email@&gt;.',


// D
'deja' => '&lt;@var_email@&gt;: voc&ecirc; j&aacute; est&aacute; inscrito(a) na lista &lt;@liste@&gt;.',
'desabo' => 'Cancelamento efetuado.',


// F
'fermer' => 'fechar',


// I
'inscription' => 'Inscri&ccedil;&atilde;o na &lt;@liste@&gt;',


// M
'mail_removed' => '

O endere&ccedil;o <@var_email@> foi removido da lista @liste@.

Em caso de problemas, ou se voc&ecirc; n&atilde;o pediu este cancelamento,
por favor, escreva para <@responsable@>.

Adeus e obrigado.
',
'merci' => 'Obrigado.',


// P
'pasabo' => 'O endere&ccedil;o &lt;@var_email@&gt; n&atilde;o est&aacute; cadastrado na lista &lt;@liste@&gt;.',
'patientez' => 'Por favor, aguarde...',


// Q
'quitter' => 'Desinscri&ccedil;&atilde;o',


// S
'subject_removed' => 'O seu endere&ccedil;o foi suprimido da lista  @liste@.',


// T
'titrefenetre' => 'Inscri&ccedil;&atilde;o',


// V
'veuillez' => 'Por favor, confirme o seu endere&ccedil;o.',
'votreemail' => 'Seu e-mail:'

);


?>
