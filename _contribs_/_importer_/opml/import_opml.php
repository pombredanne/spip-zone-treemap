<?php

include('ecrire/inc_version.php3');
include_ecrire('inc_sites.php3');
include_ecrire('inc_filtres.php3');

#$opml = 'http://spip.blog.ouvaton.org/opml.php3';
$opml = 'http://spip.blog.ouvaton.org/export.opml';

$f = recuperer_page($opml);

#echo htmlspecialchars($f);

function inserer_site($s) {

    $r = 1; ## numero de la rubrique racine (secteur) ou enregistrer les sites

    $t = spip_query("SELECT * FROM spip_syndic WHERE url_syndic='".addslashes($s['url_syndic'])."'");
    if (spip_fetch_array($t)) {
        echo $s['nom_site'].' existe deja ('.$s['url_syndic'].') !<br />';
        return false;
    }

    $query = "INSERT INTO spip_syndic
    (nom_site, url_site, url_syndic, descriptif,
    statut, syndication, date, id_rubrique, id_secteur)
    VALUES (
    '".addslashes($s['nom_site'])."',
    '".addslashes($s['url_site'])."',
    '".addslashes($s['url_syndic'])."',
    '".addslashes($s['descriptif'])."',
    'prop', 'oui', NOW(), $r, $r
    )";
    
#    spip_query($query);
    echo "$query<br />\n";
    return true;
}


// dechiffrer le OPML
$sites = array();
if (preg_match_all(',<outline.*>,Uims', $f, $regs, PREG_SET_ORDER))
foreach ($regs as $reg) {
    if (extraire_attribut($reg[0], 'type') == 'rss') {
        $s = array();
        $s['nom_site'] = extraire_attribut($reg[0], 'title');
        $s['descriptif'] = extraire_attribut($reg[0], 'description');
        $s['url_syndic'] = extraire_attribut($reg[0], 'xmlUrl');
        $s['url_site'] = extraire_attribut($reg[0], 'htmlurl');
        if (!$s['url_site']) $s['url_site'] = dirname($s['url_syndic']).'/';
        $sites[] = $s;
    }
}

// Les inserer dans la base (pour de faux)
foreach ($sites as $s)
    inserer_site($s);

?>
