<?php

// � Pierre Lazuly & Ph Riviere 2003-2011.
// licence GNU/GPL
// Documentation : http://www.spip-contrib.net/Le-bouton-memo

$debug = false;

//
// Demarrage
//

unset ($methode);
if (@file_exists('shim_auth.php')) {
	// SHIM
	require('shim_auth.php');
	$methode = 'shim';
} else if (@file_exists('inc_version.php')) {
	// on est dans ecrire
	include('inc.php');
	$methode = 'ecrire';
} else if (@file_exists('ecrire/inc_version.php')) {
	// on est dans la racine de spip
	include('ecrire/inc_version.php');
	include_spip('inc/session');
	include_spip('inc/presentation');
	include_spip('inc/minipres');
	include_spip('base/abstract_sql');
	include_spip('inc/vieilles_defs');
	verifier_visiteur();
	$methode = 'spip';
} else {
	echo "mal install&eacute;.";
	exit;
}

# compat. SPIP 1.9
if (!function_exists('recuperer_page')) include_ecrire('inc_distant');

if ($debug) {
	$GLOBALS['mysql_profile'] = true;
	$GLOBALS['mysql_debug'] = true;
}

// nom du site
if (!$shim) $shim=lire_meta('nom_site');

// version de la base spip = avant 1.722, on utilise le ps pour l'url, et le soustitre pour le nom du site
$version_installee = lire_meta('version_installee');
if ($version_installee < 1.722) {
	$v_url_site = 'ps';
	$v_nom_site = 'soustitre';
} else {
	$v_url_site = 'url_site';
	$v_nom_site = 'nom_site';
}

// multicompatible
function sql_quote_x($x) {
	if (function_exists('sql_quote'))
		return sql_quote($x);
	else
		return "'".addslashes($x)."'";
}

include_spip('inc/filtres');
include_spip('inc/charsets');
include_spip('inc/sites');

function javascript2unicode($texte) {	// fonctionne avec le jajascript safari
	while (ereg("%u([0-9A-F][0-9A-F][0-9A-F][0-9A-F])", $texte, $regs))
		$texte = str_replace($regs[0],"&#x".$regs[1].";", $texte);
	return $texte;
}

//
//
//


//
// Configuration ?
//
if (!$url=$_REQUEST['url'] OR (lire_meta("memo_active") != "oui")) {
	configuration_memo();
	exit;
}


//
// Recuperer la page
//

// rubrique 'import'
if ($rub = intval($_GET['rub'])) {
	$resq =spip_query("SELECT * FROM spip_rubriques WHERE id_rubrique=$rub");
	$res = spip_fetch_array($resq);
	if (!$res) die ("La rubrique '$rub' n'existe pas, veuillez reconfigurer le bouton m&eacute;mo.");
}
$lang = $res['lang'];
$auto_id_rubrique=$rub;
$auto_id_auteur=$auteur_session['id_auteur'];

// lire la page et la transcoder
$url = ereg_replace(" ", "%20", $url);

# test version de SPIP : a partir de 1.8pr2 recuperer_page() gere le charset
if (function_exists('init_mb_string'))
	$lapage = recuperer_page($url,true);

else {
	$lapage = recuperer_page($url);
	if (preg_match(',<[^>]*charset=[\'" ]*([a-z0-9_-]+),i', $lapage, $regs))
		$charset = trim($regs[1]);
	else
		$charset = 'iso-8859-1';
	$lapage = importer_charset($lapage, $charset);
}

// PRETRAITEMENTS
$lapage = str_replace("\n\r", "\r", $lapage); // echapper au greedyness de preg_replace
$lapage = str_replace("\n", "\r", $lapage);

// virer les commentaires html (qui cachent souvent css et jajascript)
$lapage = preg_replace("/<!--.*-->/Uims", "", $lapage);

$lapage = preg_replace("/<(script|style)\b.+?<\/\\1>/i", "", $lapage);
// <pre>...</pre>
if (preg_match_all('@<pre>.*</pre>@Uims', $lapage, $regs, PREG_SET_ORDER)) {
	foreach ($regs as $reg) {
		$lapage = str_replace($reg[0], str_replace("\r", "<br>", $reg[0]), $lapage);
	}
}
// itals
$lapage = preg_replace(",<(i|em)( [^>\r]*)?".">(.+)</\\1>,Uims", "{\\3}", $lapage);

// gras (pas de {{ pour eviter tout conflit avec {)
$lapage = preg_replace(",<(b|h[4-6])( [^>]*)?".">(.+)</\\1>,Uims", "@@b@@\\3@@/b@@", $lapage);

// entites
include_spip('inc/charsets');
$lapage = html2unicode($lapage, true /* secure */);

// liens
$lapage = preg_replace(",<a[ \t\n\r][^<>]*href=[^<>]*(http[^<>'\"]*)[^<>]*>(.*?)<\/a>,uims", "[\\2->\\1]", $lapage);
// intertitres
$lapage = preg_replace(",<(h[1-3])( [^>]*)?".">(.+)</\\1>,Uims", "\r{{{ \\3 }}}\r", $lapage);
// tableaux
$lapage = preg_replace(",<tr( [^>]*)?".">,Uims", "<br>\r", $lapage);
$lapage = preg_replace(",<t[hd]( [^>]*)?".">,Uims", " | ", $lapage);


// POST TRAITEMENT
$lapage = str_replace("\r", "\n", $lapage);

// SUPPRIME LES TAGS
if (eregi("<title.*>(.*)</title>", $lapage, $regs))
	$titre = textebrut($regs[1]);
$lapage = textebrut($lapage);

// Suite tableaux
$lapage = preg_replace(",\n[| ]+\n,", "", $lapage);
$lapage = preg_replace(",\n[|].+?[|].+?[|].+,", "\\0|\r", $lapage);

// retablir les gras
$lapage = preg_replace(",@@b@@(.*)@@/b@@,Uims","<b>\\1</b>",$lapage);
$lapage = preg_replace(",@@/?b@@,"," ",$lapage);

$lapage = preg_split("/\r\n|\n\r|\n|\r/", $lapage);


// TEST VALIDITE
if (sizeof($lapage) < 10) {
	echo install_debut_html("Erreur de lecture...");
	echo "<p><b>burps !</b> Je n'ai pas r&eacute;ussi &agrave; lire la page demand&eacute;e (ou le r&eacute;sultat fait moins de 10&nbsp;lignes)&nbsp;:<hr>\n<pre>\n".nl2br(join("\n",$lapage))."</pre>\n";
	echo install_fin_html();
	exit;
} 

// ANALYSER
if (ereg("THE_TITLE:",$lapage[1]))
	$titre=ereg_replace("THE_TITLE:(.*)","\\1",$lapage[1]);
$titre=trim($titre);
if ($titre=='') $titre="(Sans titre)";

// charset titre : safari et mozilla ne passent pas les valeurs dans le meme charset ;(
// il faut donc se baser sur "le plus probable", a la louche...
if ($t = $_REQUEST['t']) {
	$marqueurs = array(224, 233, 232, 249, 244, 249, 234, 231);
	while (list(,$marqueur) = each($marqueurs)) {
		if (strpos(chr($marqueur), $t))
			$proba['iso-8859-1'] ++;
		if (strpos(unicode2charset(chr($marqueur), 'utf-8', true), $t))
			$proba['iso-8859-1'] --;
	} 

	if ($proba['iso-8859-1']>0)
		$titre = javascript2unicode(charset2unicode($t, 'iso-8859-1', true));  // titre passe par la bookmarklet (safari only)
	else
		$titre = javascript2unicode(charset2unicode($t, 'utf-8', true));

	// si une valeur entre parenthese : c'est le nom_site
	if (ereg("^(.+)\((.+)\)[ \r\n]*$", $titre, $regs)) {
		$titre = $regs[1];
		$nom_site = $regs[2];
	}
}

unset($texte);
unset($inbox);
while (list($key,$lig) = each($lapage)) 
{
	$lig=trim($lig);
	if (strlen($lig)>200) $inbox=1;
	$lig = ereg_replace("[-_]{10,}", "\n------", $lig);
	if ($inbox) {
		$texte.=$lig."\n";
		if ((strlen($lig)>200)AND(strlen($lig)<980))
			$texte.="\n";
	} else
		$chapo .= $lig."\n";
}


unset($id_article);
$query="SELECT id_article,statut FROM spip_articles WHERE $v_url_site=".sql_quote_x($url)." AND statut!='poubelle'";
$result=spip_query($query);
if ($row = spip_fetch_array($result)) $id_article=$row['id_article'];

if ($row['statut'] == 'publie') {
	if (! (($auteur_session['statut']=='0minirezo') AND (lire_meta('memo_statut_article') == 'publie'))) {
		die ("Article d&eacute;j&agrave; t&eacute;l&eacute;charg&eacute; et publi&eacute;... on n'y revient pas");
	}
}

if (!$id_article) {
	$forums_publics = substr(lire_meta('forums_publics'),0,3);
	if (function_exists('sql_insert')) {
		$id_secteur = array_pop(sql_fetsel('id_secteur', 'spip_rubriques', 'id_rubrique='._q($auto_id_rubrique)));
		
		$id_article = sql_insert('spip_articles',
			'(id_rubrique, id_secteur, titre, statut, date, accepter_forum, lang)',
			"($auto_id_rubrique, $id_secteur, 'temp', 'prop', NOW(), '$forums_publics', '$lang')");
	} else {
		// vieux spip
		spip_query("INSERT INTO spip_articles (id_rubrique, titre, statut, date, accepter_forum, lang) VALUES ($auto_id_rubrique, 'temp', 'prop', NOW(), '$forums_publics', '$lang')");
		$id_article = spip_insert_id();
	}
	if (!$id_article) die ('erreur insertion');

	spip_query("DELETE FROM spip_auteurs_articles WHERE id_article = $id_article");
	spip_query("INSERT INTO spip_auteurs_articles (id_auteur, id_article) VALUES ($auto_id_auteur, $id_article)");
}


// ENTRER DANS LA BASE

	$titre = corriger_caracteres($titre);
	$nom_site = corriger_caracteres($nom_site);
	$url_site = corriger_caracteres($url);
	$chapo = corriger_caracteres($chapo);
	$texte = corriger_caracteres($texte);

	if (function_exists('sql_updateq')) {
		sql_updateq('spip_articles', array('surtitre' => $surtitre, 'titre' => $titre, $v_nom_site => $nom_site, 'id_rubrique' => $auto_id_rubrique, $v_url_site => $url_site, 'chapo' => $chapo, 'texte' => $texte, 'ps' => $ps), "id_article=$id_article");
	} else {
		$query = "UPDATE spip_articles SET surtitre=".sql_quote_x($surtitre).", titre=".sql_quote_x($titre).", $v_nom_site=".sql_quote_x($nom_site).", id_rubrique=".sql_quote_x($auto_id_rubrique).", $v_url_site=".sql_quote_x($url_site).", chapo=".sql_quote_x($chapo).", texte=".sql_quote_x($texte).", ps=".sql_quote_x($ps)." WHERE id_article=$id_article";
		$result = spip_query($query);
	}

	// statut article
	$statut_article = lire_meta('memo_statut_article');
	if (($statut_article == 'publie') AND ($auteur_session['statut'] == '0minirezo')) {
		spip_query ("UPDATE spip_articles SET statut='publie' WHERE id_article=$id_article");
		}
	
$url_spip = lire_meta("adresse_site");

if (!$debug)
@Header("Location: $url_spip/ecrire/?exec=articles&id_article=$id_article");

exit(1);


function configuration_memo() {
	global $shim, $SCRIPT_URI;

	$laius = str_replace(' ', '%20', 'Memoriser cette page sous le titre...');

	include_spip("inc/meta");
	echo install_debut_html("Configuration m&eacute;mo ($shim)");

	// admin ?
	if ($GLOBALS['auteur_session']['statut'] != '0minirezo') {
		echo "<p>Il faut vous <a href='spip.php?page=login&url=memo.php'>enregistrer</a> en tant qu'administrateur pour configurer ce 'bouton memo'.";
		install_fin_html();
		exit;
	}

	// recevoir les donn&eacute;es ou les lire dans meta.
	$memo = Array('memo_active'=>'non', 'memo_statut'=>'0minirezo', 'memo_statut_article' => 'prop', 'memo_rubrique'=>0);
	$memo_help = Array('memo_active' => '"oui" pour activer le syst&egrave;me',
		'memo_statut' => "'0minirezo' ou '1comite' pour savoir qui peut m&eacute;moriser (vide = tout le monde) [pas encore impl&eacute;ment&eacute;]",
		'memo_statut_article' => "'publie' si on veut publier les articles illico (admins seulement)",
		'memo_rubrique' => "id_rubrique o&ugrave; tombent les pages m&eacute;moris&eacute;es (non bloquant)");
	$post = $_REQUEST;
	reset ($post);
	while (list($var,$val) = each($post))
		if (ereg("^memo_", $var)) {
			if ($memo[$var] = $val)
				ecrire_meta($var,$val);
			else {
				unset($memo[$var]);
				ecrire_meta($var,$val);
				effacer_meta($var);
			}
		}
	ecrire_metas();
	$metas = $GLOBALS['meta'];
	reset ($metas);
	while (list($var,$val) = each($metas))
		if (ereg("^memo_", $var)) {
			$memo[$var] = $val;
		}

	if ($_SERVER['SCRIPT_URI'])
		$base_href = $_SERVER['SCRIPT_URI'];
	else
		$base_href = lire_meta('adresse_site') . '/memo.php';

	if ($memo['memo_active'] == 'oui' AND $memo['memo_rubrique']) {
		$bookmarklet1 = "javascript:void(location.href='$base_href?url='+escape(location.href)+'&rub=".$memo['memo_rubrique']."')";
		$bookmarklet2 = "javascript:if(t=prompt('$laius',document.title))%7Bvoid(location.href='$base_href?url='+escape(location.href)+'&rub=".$memo['memo_rubrique']."&t='+escape(t));%7D";

		echo "<p><b><a href=\"".$bookmarklet2."\">[+$shim]</a></b>&nbsp;: ";
		echo "installez ce lien (par glisser-d&eacute;poser) dans votre barre de bookmarks&nbsp;; vous pourrez l'utiliser pour m&eacute;moriser n'importe quelle page web, directement dans votre base SPIP.<br /><a href='http://www.spip-contrib.net/article317.html'>Voir la documentation graphique...</a></p>";
	}

	// afficher config + formulaire config
	echo "<br><hr><form method='get' action='memo.php'>\n";
	reset($memo);
	while (list($var,$val) = each($memo)) {
		if (ereg("^memo_", $var)) {
			$valt = entites_html($val);
			echo "<p><b>$var</b> : <input type='text' name='$var' value='$valt'><br><small>$memo_help[$var]</small>\n";
		}
	}
	echo "<br><div align='right'><input type='submit' name='ok' value='ok'></div></form>\n";

		
	if ($methode == 'shim') echo "<p><hr>[<a href='/shim/'>shim</a>]</p>";
	echo install_fin_html();
}

?>
