﻿<?php
//
// Configurateur de mots clefs et de rubriques bas'e sur
// Configurateur Squelette Epona - 2004 Nov 10 - Marc Lebas.
// Realisation : RealET : real3t@gmail.com
//

function refus($msg) {
  echo "<b><font color=red>$msg</font></b><br>";
  echo "Action non commencée; rectifier les conditions initiales avant de reprendre<br>";
}

function abandon($msg) {
  echo "<b><font color=red>$msg</font></b><br>";
  echo "Action abandonnée en cours d'éxécution<br>";
  echo "Rectifier le problème et rétablir des conditions initiales avant de reprendre<br>";
}

function avertir($msg) { echo "<font color=orange>$msg</font><br>"; }

//
// Fonctions pour mot-clés
//
function id_groupe($titre) {
	$result = spip_query("SELECT id_groupe FROM spip_groupes_mots WHERE titre='$titre'");
	if ($row = spip_fetch_array($result)) return $row['id_groupe'];
	return 0;
}

function id_mot($titre) {
	$result = spip_query("SELECT id_mot FROM spip_mots WHERE titre='$titre'");
	if ($row = spip_fetch_array($result)) return $row['id_mot'];
	return 0;
}

function id_rubrique($titre) {
	$result = spip_query("SELECT id_rubrique FROM spip_rubriques WHERE titre='$titre'");
	if ($row = spip_fetch_array($result)) return $row['id_rubrique'];
	return 0;
}

function create_groupe($groupe, $descriptif='', $texte='', $unseul='non', $obligatoire='non', $articles='oui', $breves='non', $rubriques='non', $syndic='non', $minirezo='oui', $comite='oui', $forum='non') {
	$groupe = importer_charset($groupe,'utf-8');
	$id_groupe=id_groupe($groupe);
	$texte = importer_charset($texte,'utf-8');
	$descriptif = importer_charset($descriptif,'utf-8');
	if ($id_groupe == 0) {
		//Création groupe + mots clé
		$query = "INSERT INTO spip_groupes_mots SET titre='$groupe', descriptif='$descriptif', texte='$texte', unseul='$unseul', obligatoire='$obligatoire',
			articles='$articles', breves='$breves', rubriques='$rubriques', syndic='$syndic',
			minirezo='$minirezo', comite='$comite', forum='$forum'";
		spip_query($query);
		$id_groupe = spip_insert_id();
	} else {
		// Mise à jour
		spip_query("UPDATE spip_groupes_mots SET descriptif='$descriptif', texte='$texte', unseul='$unseul', obligatoire='$obligatoire',
			articles='$articles', breves='$breves', rubriques='$rubriques', syndic='$syndic',
			minirezo='$minirezo', comite='$comite', forum='$forum' WHERE id_groupe=$id_groupe");
	}
	echo "<h2>Groupe: $groupe (<a href='mots_type.php3?id_groupe=$id_groupe'>$id_groupe</a>)</h2>";
	return $id_groupe;
}

function create_mot($groupe, $mot, $descriptif='', $texte='') {
	$groupe = importer_charset($groupe,'utf-8');
	$id_groupe=id_groupe($groupe);
	$mot = importer_charset($mot,'utf-8');
	$texte = importer_charset($texte,'utf-8');
	$descriptif = importer_charset($descriptif,'utf-8');
	if ($id_groupe != 0) {
		$id_mot=id_mot($mot);
		if ($id_mot == 0 ) {
			spip_query("INSERT INTO spip_mots (type, titre, id_groupe, descriptif, texte) VALUES ('$groupe', '$mot', '$id_groupe', '$descriptif', '$texte')");
			$id_mot = spip_insert_id();
		} else {
			// Mise à jour
			spip_query("UPDATE spip_mots SET type='$groupe', id_groupe='$id_groupe', descriptif='$descriptif', texte='$texte' WHERE id_mot=$id_mot");
		}
	}
	echo "<li>Mot: $mot (<a href='mots_edit.php3?id_mot=$id_mot&redirect=mots_tous.php3'>$id_mot</a>)</li>";
	return $id_mot;
}

function create_rubrique($titre, $id_parent='0', $descriptif='') {
	$id_rubrique = id_rubrique($titre);
	if ($id_rubrique==0) {
		$titre = importer_charset($titre,'utf-8');
		$descriptif = importer_charset($descriptif,'utf-8');
		$query="INSERT INTO spip_rubriques (titre, id_parent, descriptif) VALUES ('$titre', '$id_parent', '$descriptif')";
		spip_query($query);
		$id_rubrique = spip_insert_id();
	}
	echo "<li>Rubrique: $titre (<a href='naviguer.php3?coll=$id_rubrique'>$id_rubrique</a>)</li>";
	return $id_rubrique;
}

function create_rubrique_mot($rubrique, $mot) {
	$id_rubrique = id_rubrique($rubrique);
	$id_mot=id_mot($mot);
	if ($id_rubrique!=0 && $id_mot!=0) {
		$query="SELECT count(*) as nb_rub_mot FROM spip_mots_rubriques WHERE id_mot='$id_mot' AND id_rubrique='$id_rubrique'";
		$result=spip_query($query);
		if ($row = spip_fetch_array($result)) {
			if ($row['nb_rub_mot']==0) {
				$query="INSERT INTO spip_mots_rubriques (id_mot, id_rubrique) VALUES ('$id_mot', '$id_rubrique')";
				spip_query($query);
			}
		}
	}
	echo "<li>Liaison entre Rubrique (<a href='naviguer.php3?coll=$id_rubrique'>$id_rubrique</a>) et Mot (<a href='./ecrire/mots_edit.php3?id_mot=$id_mot&redirect=mots_tous.php3'>$id_mot</a>)</li>";
	return TRUE;
}

function config_site() {
	// Autorisations dates antérieures et gestion avancée des mots clé
	spip_query("REPLACE spip_meta (nom, valeur) VALUES ('config_precise_groupes', 'oui')");
	spip_query("REPLACE spip_meta (nom, valeur) VALUES ('articles_redac', 'oui')");
	// Création rubriques
	create_rubrique('000. Racine', '0', "Vous trouverez dans cette rubrique:\n\n-* Les Éditos\n-* Des articles concernant le site lui-même\n");
	create_rubrique('900. Agenda', '0');

## -------------------------------------------->

	create_groupe("CategorieAgenda", "Détermine la liste des éléments présentés en liste déroulante dans l\'Agenda du site", "Il est {{impératif}} pour le bon fonctionnement de l\'Agenda de sélectionner au moins un mot clef.\n\nUn événement de l\'Agenda peut avoir un ou {{plusieurs}} mot clefs ratachés.", 'non', 'non', 'oui', 'non', 'non', 'non', 'oui', 'oui', 'non');
		create_mot("CategorieAgenda", "Autres", "", "");
		create_mot("CategorieAgenda", "Freeware", "Logiciel gratuit, mais dont le source n\'est pas rendu public :(", "");
		create_mot("CategorieAgenda", "Open-Source", "Pour tout ce qui tourne autour de l\'Open-Source", "");

	create_groupe("_Specialisation", "Spécialisation d’un article ", "Un mot clef pris dans ce groupe permettra de modifier\n\n-* le comportement d’un article particulier\n", 'non', 'non', 'oui', 'non', 'non', 'non', 'oui', 'oui', 'non');
		create_mot("_Specialisation", "ALaUne", "Article qui doit rester à la une du site (soit dans quoi de neuf, soit dans la liste des articles en ModeNews)", "");
		create_mot("_Specialisation", "DevoilerIdentite", "Mettre ce mot clef à un article dont on veut afficher le nom du ou des auteurs (au sens de SPIP)", "");
		create_mot("_Specialisation", "EDITO", "Sert à dire que l\'article est un éditorial.", "{{Attention}} : le site utilisera l\'article le plus récent ayant ce mot clef pour l\'afficher en tant qu\'éditorial.\n\n[*Conséquence*] : ne changez pas le contenu d\'un éditorial par le nouvel éditorial, créez un nouvel article éditorial!");
		create_mot("_Specialisation", "Gallerie", "Si l\'article contient une galerie de photos (portofolio).", "Pour faire un portofolio, il faut attacher à un article des documents de type images (.jpeg, .gif et .png).\n\nAvec ce mot clef, ces images seront automatiquement affichées sous forme de vignettes avec la possibilité de cliquer sur une image pour en voir une version plus grande et pourvoir de nouveau cliquer pour avoir dans une nouvelle fenêtre l\'image de la taille originale.\n\n[*Attention*]: surveillez l\'espace occupé sur votre hébergement de site pour ne pas le dépasser avec trop d\'images...");
		create_mot("_Specialisation", "GraverSonNom", "Un article avec ce mot clef permettra aux visiteurs de laisser leur nom sur le site en tant que bulle d\'aide sur l\'image (Logo du mot) et de faire parvenir un texte aux administrateurs", "Il faut pour que ça fonctionne:\n\n-* un article\n-* un forum modéré a posteriori\n-* ce mot mot clef attaché à cet article\n-* un logo à ce mot clef\n\nÀ partir de là, l\'article permet aux visiteurs de «graver leur nom» dans le site. Leur nom aparaitra en bulle d\'aide sur une image (le logo de ce mot clef).");
		create_mot("_Specialisation", "Livre d\'Or", "Pour empécher que l\'on puisse répondre à un forum", "Ce mot clef appliqué à un article ayant un forum fait que ce forum n\'a qu\'un niveau (pas possible de répondre à une intervention, seulement d\'en rajouter)");
		create_mot("_Specialisation", "MENURACINE", "Doit s\'afficher en dessous de Accueil", "Pour dire que l\'article s\'affiche en dessous de Accueil dans le menu de gauche avant les rubriques du site");
		create_mot("_Specialisation", "MENURACINEBAS", "Pour dire que l\'article s\'affiche au dessus de Plan", "Permet de placer dans le menu de gauche un (ou plusieurs) article(s) en bas de menu, avant le plan du site.");
		create_mot("_Specialisation", "PasdeSiteDansForums", "Pour que les sites référencés n\'apparaissent pas dans un forum (mesure anti SPAM)", "Pour décourager ceux qui utiliseraient vos forums pour faire de la pub pour leurs site (généralement, des sonneries de téléphone)");

	create_groupe("_Specialisation_Rubrique", "Spécialisation d’une rubrique", "Un mot clef pris dans ce groupe permettra de modifier\n\n-* le comportement d’une rubrique et de ses articles\n", 'non', 'non', 'non', 'non', 'oui', 'non', 'oui', 'oui', 'non');
		create_mot("_Specialisation_Rubrique", "Agenda", "Pour dire qu\'une rubrique est dans l\'Agenda", "Il est impératif de mettre ce mot clef pour la rubrique à la racine ayant cette caractéristique, mais aussi pour ses sous-rubriques.");
		create_mot("_Specialisation_Rubrique", "Citations", "Rubrique destinée à recevoir de courtes citations (une par article) affichées en haut à droite des pages du site de manière alléatoire (une nouvelle citation toutes les heures)", "Créer un article par citation avec :\n\n-* La citation dans le corps du texte (entourée de guillemets si nécessaires)\n-* L\'auteur dans le sous-titre\n-* Le titre de l\'article sert d\'accroche pour le lecteur\n");
		create_mot("_Specialisation_Rubrique", "DessousBreves", "Pour placer une rubrique et ses articles qui sont placés sous les brèves (dans la colonne de droite du site)", "[*Attention*] : une rubrique qui a ce mot clef ne doit pas avoir de sous-rubrique !\n\nLe titre de la rubrique sera affiché sur la droite et la liste de ses articles en dessous.\n\nSeuls les articles sont clicables pour accéder à leur contenu.");
		create_mot("_Specialisation_Rubrique", "MenuHaut", "Pour qu\'un secteur soit dans un menu horizontal en haut du site", "Affecter ce mot clef aux secteurs (rubriques rattachées à la racine du site) qui doivent être dans le menu horizontal en haut du site.");
		create_mot("_Specialisation_Rubrique", "NewsLetter", "Mettre ce mot clef à une rubrique dont le contenu doit servir de newsLetter.\n\nMettre ci-dessous l\'URL de désabonnement.", "maito:");
		create_mot("_Specialisation_Rubrique", "PasDansMenu", "Pour interdire que la rubrique (et ses sous-rubriques) soi(en)t dans le menu de gauche", "");
		create_mot("_Specialisation_Rubrique", "PasDansPlan", "Permet de masquer une rubrique, et tout son contenu (y compris les sous-rubriques) du plan du site", "À affecter aux rubriques qui ne doivent pas être affichées dans le plan du site");
		create_mot("_Specialisation_Rubrique", "SecteurPasDansQuoiDeNeuf", "Pour interdire que les articles d\'un secteur entier soit dans «Quoi de Neuf» sur la page d\'accueil", "Un secteur, c\'est une rubrique rattachée à la racine du site et toutes ses sous-rubriques");

	create_groupe("_Specialisation_Rubrique_ou_Article", "Spécialisation d\'une rubrique ou d\'un article", "Un mot clef pris dans ce groupe permettra de modifier\n\n-* le comportement d\'une rubrique et de ses articles\n-* le comportement d\'un article particulier", 'non', 'non', 'oui', 'non', 'oui', 'non', 'oui', 'oui', 'non');
		create_mot("_Specialisation_Rubrique_ou_Article", "2Cols", "À mettre pour n\'avoir que 2 colonnes au lieu de 3 sur l\'article ou la rubrique", "D\'habitude, le site s\'affiche avec 3 colonnes : le menu de gauche, le menu de droite, et le texte au centre.\n\nAvec ce paramètre, la colonne de droite se retrouve en bas de la colonne de gauche et le texte occupe l\'espace libéré.");
		create_mot("_Specialisation_Rubrique_ou_Article", "PasDansQuoiDeNeuf", "Pour interdire que l\'article ou la rubrique soit dans «Quoi de Neuf» sur la page d\'accueil", "À mettre soit:\n\n-* pour un article précis\n-* pour une rubrique particulière\n\nRemarque : si elle a des sous rubriques, il faut aussi le faire pour chacunes de celles-ci si on veut les exclure aussi...");
		create_mot("_Specialisation_Rubrique_ou_Article", "Sommaire", "Pour dire que les articles de cette rubrique ont un sommaire ou que l\'article a un sommaire", "Un sommaire automatique sera placé en début d\'article.\n\nCe sommaire sera bati à partir des titres et sous-titres du texte de l\'article.");


## <--------------------------------------------

	// Liaison entre rubrique et mot clé
	create_rubrique_mot('000. Racine', 'SecteurPasDansQuoiDeNeuf');
	create_rubrique_mot('000. Racine', 'MENURACINE');
	create_rubrique_mot('000. Racine', 'PasDansMenu');
	create_rubrique_mot('900. Agenda', 'Agenda');
	create_rubrique_mot('900. Agenda', 'SecteurPasDansQuoiDeNeuf');
	create_rubrique_mot('900. Agenda', 'PasDansMenu');
	return TRUE;
}

//
// Main
//
if (!file_exists("inc.php3")) {
	refus("inc.php3 non trouvé. Le fichier postconfig.php3 a-t-il été placé dans le répertoire ecrire ?");
	exit;
}

include ("inc.php3");
include_ecrire ("inc_presentation.php3");
include_ecrire ("inc_lang.php3");
include_ecrire ("inc_charsets.php3");

if (($connect_statut != "0minirezo")) {
	install_debut_html(_T('info_acces_refuse'));
	install_fin_html();
	exit;
}

install_debut_html("Configurateur site");
config_site();

install_fin_html();
?>
