<?php

	chdir('..');

	include 'ecrire/inc_version.php';

	$s = spip_query("SELECT * FROM spip_articles WHERE statut = 'prop'");
	
	while ($t = spip_fetch_array($s)) {
		$c = notes_automatiques($t['texte']);
		if ($c) {
			spip_query("UPDATE spip_articles SET texte='".addslashes($c)."' WHERE id_article=".$t['id_article']);
			echo "Article corrige : <a href='../ecrire/?exec=articles&amp;id_article=".$t['id_article']."'>".$t['titre']."</a><br />\n";
		}
	}

	function notes_automatiques($texte) {

		// Attrapper les notes
		$regexp = ', *\[\[(.*?)\]\],msS';
		if ($s = preg_match_all($regexp, $texte, $matches, PREG_SET_ORDER)
		AND $s==1
		AND preg_match(",^ *<>(.*),s", $matches[0][1], $r)) {
			$lesnotes = $r[1];
			$letexte = trim(str_replace($matches[0][0], '', $texte));

			$num = 0;
			while (($a = strpos($lesnotes, '('.(++$num).')')) !== false
			AND ($b = strpos($letexte, '('.($num).')')) !== false
			) {
				if (!isset($debut))
					$debut = trim(substr($lesnotes, 0, $a));

				$lanote = substr($lesnotes,$a+strlen('('.$num.')'));

				$lanote = preg_replace(
				',[(]'.($num+1).'[)].*,s', '',$lanote
				);

				$lanote = trim($lanote);
				$lanote = (strlen($lanote) ? '[['.$lanote.' ]]' : '');

				$letexte = substr($letexte,0,$b)
					. $lanote
					. substr($letexte,$b+strlen('('.$num.')'));
			}

			if (isset($debut)) {
				return (strlen($debut)?"\n\n[[<>$debut]]":'') . $letexte;
			}
		}
	}

?>