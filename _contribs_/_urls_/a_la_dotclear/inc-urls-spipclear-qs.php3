<?php


/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2005                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/


/*

Ce jeu d'URLs est une variante de inc-urls-spipclear, qui ajoute
le prefixe './?' aux adresses, ce qui permet de l'utiliser en
mode "Query-String", sans .htaccess ;

	<http://mon-site-spip/?Rubrique/YYYY/MM>

Attention : le mode 'spipclear-qs' est moins fonctionnel que le mode 'spipclear' ou
'spipclear2'. Si vous pouvez utiliser le .htaccess, ces deux derniers modes sont
preferables au mode 'spipclear-qs'.

*/

if (!defined('_terminaison_urls_spipclear'))
	define ('_terminaison_urls_spipclear', '');

define ('_debut_urls_spipclear', './?');

include('inc-urls-spipclear.php3');

?>
