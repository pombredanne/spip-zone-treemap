<?php

function url_path($adresse = '') {
	static $_url_path = array();
	if($adresse == '')
		$adresse = $GLOBALS['meta']['adresse_site'];
	if(!$_url_path[$adresse])
		$_url_path[$adresse] = parse_url($adresse, PHP_URL_PATH).'/';
	return $_url_path[$adresse];
}

function balise_SQUELETTE($p) {
	$code = addslashes($p->descr['sourcefile']);
	$p->code = "'".url_path()."'.'$code'" . 

	$p->interdire_scripts = false;
	return $p;
}

function balise_CHEMIN($p) {
	$p->code = interprete_argument_balise(1,$p);
	$url_path = interprete_argument_balise(2,$p);
	$p->code = "'".($url_path!="'rel'"?url_path():'')."'.find_in_path(". $p->code .")";

	return $p;
}

function corrige_src($img){
	return preg_replace(",src=\'"._DIR_VAR.",", "src='".url_path()._DIR_VAR, $img);
}
?>