<?php

// This is a SPIP-forum module file  --  Ceci est un fichier module de SPIP-forum

$GLOBALS['i18n_trackbacks_fr'] = array(

//A
'article_nopost' => 'Pas d\'article pour cet identifiant.',

//B
'bouton_radio_articles_tous_sauf_trackback_desactive' => '&agrave; tous les articles, sauf ceux dont les trackbacks sont d&eacute;sactiv&eacute;.',
'breve_nopost' => 'Pas de br&egrave;ve pour cet identifiant.',

//D
'deja_recu_article' => 'Nous avons d&eacute;j&agrave; re&ccedil;u un ping de cette adresse pour cet article',
'deja_recu_breve' => 'Nous avons d&eacute;j&agrave; re&ccedil;u un ping de cette adresse pour cette br&egrave;ve',

//E
'erreur_insertion' => 'Une erreur s\'est produite lors de l\'insertion du trackback.',

//I
'icone_envoi' => 'Envoyer un trackback',
'icone_envoi_cet_article' => 'Envoyer un trackback depuis cet article',
'icone_suivi' => 'Trackbacks re&ccedil;us',
'icone_suivi_article' => 'Trackbacks re&ccedil;us pour l\'article',
'icone_suivi_breve' => 'Trackbacks re&ccedil;us pour la br&egrave;ve',
'icone_suivi_cet_article' => 'Trackbacks re&ccedil;us pour cet article',
'icone_suivi_cette_breve' => 'Trackbacks re&ccedil;us pour cette br&egrave;ve',
'icone_suivi_general' => 'Trackbacks re&ccedil;us sur tout le site',
'info_activer_trackback' => 'Pour activer les trackbacks, veuillez choisir leur mode de mod&eacute;ration par d&eacute;faut&nbsp;:',
'info_desactiver_trackback' => 'D&eacute;sactiver les trackbacks',
'info_fonctionnement' => 'Fonctionnement des trackbacks',
'info_mode_fonctionnement_defaut' => 'Mode de fonctionnement par d&eacute;faut des trackbacks',
'info_pas_de_trackback' => 'Pas de trackback',
'info_rss' => 'TrackBack item for this blog',

//N
'noid' => 'Pas d\'ID',
'notrackbacks' => 'Les trackbacks ne sont pas autoris&eacute;s pour cet objet ou pour ce weblog.',

//U
'url_requise' => 'le param&egrave;tre URL est requis.',

);

?>
