<?php

// This is a SPIP-forum module file  --  Ceci est un fichier module de SPIP-forum

$GLOBALS['i18n_trackbacks_fr'] = array(

//A
'article_nopost' => 'No post for this ID.',

//B
'bouton_radio_articles_tous_sauf_trackback_desactive' => 'to all articles, except those with disabled trackbacks.',
'breve_nopost' => 'No post for this ID.',

//D
'deja_recu_article' => 'We already have a ping from that URI for this post.',
'deja_recu_breve' => 'We already have a ping from that URI for this post.',

//E
'erreur_insertion' => 'An error occured while inserting the trackback.',

//I
'icone_envoi' => 'Send a trackback',
'icone_envoi_cet_article' => 'Send a trackback from the current article',
'icone_suivi' => 'Received trackbacks',
'icone_suivi_article' => 'Received trackbacks re&ccedil;us for the article',
'icone_suivi_breve' => 'Received trackbacks for the new',
'icone_suivi_cet_article' => 'Received trackbacks for this article',
'icone_suivi_cette_breve' => 'received trackbacks for this new',
'icone_suivi_general' => 'Received trackbacks on the whole site',
'info_activer_trackback' => 'To enable trackback functionnalities, please choose their default mode of moderation:',
'info_desactiver_trackback' => 'Disable the use of trackbacks',
'info_fonctionnement' => 'Operation mode of trackbacks',
'info_mode_fonctionnement_defaut' => 'Default operation mode of trackbacks',
'info_pas_de_trackback' => 'no trackback',
'info_rss' => 'TrackBack item for this blog',

//N
'noid' => 'No ID.',
'notrackbacks' => 'Trackbacks are not allowed for this post or weblog.',

//U
'url_requise' => 'URL parameter is requiered.',

);

?>
