<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'agrandir_vignette' => 'Cliquez sur les vignettes pour agrandir',
'artisan' => 'Nos artisans',
'arianne' => 'Vous �tes ici : ',
'accueil' => 'Accueil',
'ancien_facture' => 'Vos anciennes factures',
'ajout_ok' => 'L\'article &agrave; &eacute;t&eacute; ajout&eacute; &agrave; votre panier',
'acheter' => 'Commander',
'achat' => 'achat',
'aucunes_cde' => 'Vous n\'avez aucun article en attente dans votre panier',
'adresse_livraison' => 'Adresse de livraison diff&eacute;rente :',

//C
'compte_menu' => 'Gestion compte',
'compte_acces' => 'Mon compte',
'catalogue' => 'Notre catalogue',
'coordonnees' => 'Vos coordonn&eacute;es',
'cde_en_cour' => 'Votre commande en cours',


//D
'deconect' => 'D&eacute;connection',
'dans_panier' => 'articles dans votre panier',

//E
'enlever' => 'Enlever la ligne',

//F
'facture' => 'Votre facture',
'frais_livraison' => 'Frais de livraison',
'factures_en_cours' => 'Etat de vos derni�res factures',
'form_pet_votre_non' => 'Votre nom',
'form_pet_votre_prenom' => 'Votre pr&eacute;nom',
'form_pet_votre_adresse' => 'Votre adresse',
'form_pet_cp' => 'Code Postal',
'form_pet_ville' => 'Ville',
'form_pet_pays' => 'Pays',
'forum_vous_enregistrer' => 'Pour acheter sur le site, vous devez vous inscrire en remplissant ce formulaire. Vos identifiants vous seront envoy&eacute; par mail.',
'forum_vous_inscrire' => '',

//G
'gestion_client' => 'Gestion des clients',
'gestion_commande' => 'Gestion des commandes',

//L
'livrer' => 'Livraison des commandes : ',

//M
'monnaie' => '&euro;',
'message_non_enregistre' => 'Vous devez vous enregistrer pour acc&eacute;der &agrave; cette partie du site.',
'modif_adresse' => 'Modifier mes coordonn&eacute;es',

//N
'num_commande_courant' => 'Num&eacute;ro de commande en cours',
'num_fact' => 'Num&eacute;ro de facture',
'nb_client' => 'Nb de client : ',
'nb_cde_en_cour' => 'En cours : ',
'nb_cde_attente_livraison' => 'Attente livraison :',
'nb_cde_livree' => 'Livr&eacute;es :',

//P
'panier' => 'Voir le d&eacute;tail de votre panier',
'panier_vide' => 'Votre panier est vide',
'prix' => 'prix',
'pass_bla'=>'pour acheter sur le site, vous devez vous inscrire en remplissant ce formulaire.',
'paiement' => 'Gestion des paiements diff&eacute;r&eacute;s : ',
'pass_forum_bla' => 'Pour acheter sur le site, vous devez vous inscrire en remplissant ce formulaire.',
'pass_vousinscrire' => 'Vous inscrire sur ce site',

//R
'retour_accueil' => 'Retour &agrave; l\'accueil',
'recap_commande' => 'R&eacute;capitulatif de votre commande',

//T
'titre_dernier_parus' => 'Derniers articles parus',
'type_paiement' => 'Type de paiement',
'tableau_bord' => 'Tableau de bord',
'total'=> 'Total commande'

);

?>
