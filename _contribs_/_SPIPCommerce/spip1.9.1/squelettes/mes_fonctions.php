<?php
/************************************
		ESSAIS BALISE ID_SESSION
		pour retourner l'id_auteur d'une personne identifi�e parcourant le site
************************************/
function calcul_ID_SESSION() {
   global $auteur_session;
   $id_session=$auteur_session['id_auteur'];
   return $id_session;
}

function balise_ID_SESSION($p) {
   $p->code = "calcul_ID_SESSION()";
   $p->statut = 'html';
   return $p;
}	
/**********************************
//  utilisation de colonne supl�mentaires dans la table auteur
**********************************/
include_spip ('base/serial'); include_spip('inc/indexation');
global $tables_principales;
$tables_principales['spip_auteurs']['field']['prenom']= "TEXT NOT NULL";
$tables_principales['spip_auteurs']['field']['adresse']= "TEXT NOT NULL";
$tables_principales['spip_auteurs']['field']['cp']= "TEXT NOT NULL";
$tables_principales['spip_auteurs']['field']['ville']= "TEXT NOT NULL";
$tables_principales['spip_auteurs']['field']['pays']= "TEXT NOT NULL";
	
//puis la table commandes

$tables_principales['commandes']= array(
 'field' => array(
   "id_cde" => "int(11)",
   "id_commande" => "bigint(21)",
   "id_auteur" => "bigint(21)",
   "date" => "date",
   "id_article" => "bigint(21)",
   "quantite" => "bigint(21)",
   "valide" => "varchar(3)"
   
 ),
 'key' => array("PRIMARY KEY" => "id_cde")
);

//et enfin, la table factures

$tables_principales['factures']= array(
 'field' => array(
   "id_facture" => "INT" ,
"id_auteur" => "BIGINT( 21 )"  ,
"date" => "DATE"  ,
"commande" => "LONGBLOB" ,
"frais_envoi" => "DECIMAL" ,
"total" => "DECIMAL" ,
"etat" => "TEXT" ,
"statut_cde" => "TEXT" ,
"adresse_livraison" => "TEXT" ,
 ),
 'key' => array("PRIMARY KEY" => "id_facture")
);


/********************************************
	 fonction colissimo
********************************************************************/
 				 function colissimo ($poid){
					if($poid<='0.5'){$result="5.1";}
					if(($poid>='0.5') && ($poid<'1')){$result="6.2";}
					if(($poid>='1') && ($poid<'2')){$result="7";}
					if(($poid>='2') && ($poid<'3')){$result="8";}
					if(($poid>='3') && ($poid<'5')){$result="10";}
					if(($poid>='5') && ($poid<'7')){$result="11";}
					if(($poid>='7') && ($poid<'10')){$result="13";}
					if(($poid>='10') && ($poid<'15')){$result="15";}
					if(($poid>='15') && ($poid<'30')){$result="20";}
				return $result;
				}

/********************************************
	fonction suppression enregistrement lignes commande
********************************************************************/

function dell_commandes()
{
spip_query("DELETE FROM `commandes` WHERE valide='oui';");
 
}
/********************************************
	fonction modif adresse compte
********************************************************************/

function modif_adresse($id_auteur,$nom_modif,$prenom_modif,$adresse_modif,$cp_modif,$ville_modif,$pays_modif,$mail_modif)
{
spip_query("UPDATE spip_auteurs SET nom='$nom_modif', prenom='$prenom_modif', adresse='$adresse_modif', cp='$cp_modif', ville='$ville_modif',
					pays='$pays_modif' ,email='$mail_modif' WHERE id_auteur='$id_auteur' ;");
	
}

/********************************************
	fonction modif statut commande
********************************************************************/

function modif_statut($id_facture)
{
spip_query("UPDATE factures SET statut_cde='livr�e' WHERE id_facture='$id_facture' ;");
	
}
/********************************************
	fonction modif statut paiement
********************************************************************/

function modif_paiement($id_facture,$type_paiement)
{
spip_query("UPDATE factures SET etat='$type_paiement' WHERE id_facture='$id_facture' ;");
	
}
/********************************************
	fonction ajout_article
********************************************************************/

function ajout_article($id_commande,$id_auteur_session,$idarticlecde,$nbarticle)
{
$date_jour=date("Y-m-d");

spip_query("INSERT INTO commandes (id_commande,id_auteur,date,id_article, quantite, valide)
              VALUES ('$id_commande','$id_auteur_session','$date_jour','$idarticlecde','$nbarticle', 'non')");  
}
/********************************************
	fonction sup_article
********************************************************************/

function sup_article($id_cde)
{
spip_query("DELETE FROM commandes WHERE id_cde = '$id_cde' ;");	
}
/********************************************
	fonction de dessirielisation
********************************************************************/	
function retrouve_tableau($commande)
{
$ligne="<p>";
$commande=unserialize($commande);
$number=count($commande)-1;
	while ($number>=0){
		$ligne.=($commande[$number][qte]);
		$ligne.="&nbsp;X&nbsp;";
		$ligne.=($commande[$number][titre]);
		$ligne.="&nbsp;�&nbsp;";
		$ligne.=($commande[$number][prix]);
		$ligne.="�<br />";
	$number--;
	}
	$ligne.="</p>";
return $ligne;
}
/********************************************
	fonction de dessirielisation avec tag php pour envoie par mail
********************************************************************/	
function retrouve_tableau2($commande)
{
$ligne="<p>";
$commande=unserialize($commande);
$number=count($commande)-1;
	while ($number>=0){
		$ligne.=($commande[$number][qte]);
		$ligne.=($commande[$number][titre]);
		$ligne.=" � ";
		$ligne.=($commande[$number][prix]);
		$ligne.="�\n";
	$number--;
	}
	$ligne.="</p>";
return $ligne;
}
		
/*
 *   +----------------------------------+
 *    Nom du Filtre : meta_keywords
 *   +----------------------------------+
 *    Date : 3 mai 2005
 *    Auteur :  Georges Cubas
 *    contact : http://www.sourisverte.net/auteur.php3?id_auteur=1
 *   +-------------------------------------+
 *    Fonctions de ce filtre :
 *   rend le texte entr� en param�tre compatible avec le metatag "keywords"
 *    - ne repetes pas les mots,
 *    - classe les mot par nombre d'ocurence d�croissant
 *    - ne prend pas en compte les mot de moins de 4 lettres : le la les �...
 *   +-------------------------------------+
 * reportez-vous au forum de l'article :
 * http://www.spip-contrib.net/article.php3?id_article=945
 */

//d�but filtre meta_keywords

function meta_keywords($texte) {
	// met tous les mots (suite de carateres a-z_����������������) de plus de trois lettres ({4,}) dans $tableau[0]
	if (preg_match_all("/[a-z_������������������]{4,}/",strtolower($texte), $tableau) > 0)
	{
		$tableau = array_count_values($tableau[0]); // tri les mots par leurs fr�quences
		arsort($tableau);

		$result = "";
		foreach($tableau as $key=>$value) // cr�e la liste de mots cl�s
			$result .= "$key ";
		return trim($result); // renvoit la liste sans l'espace � la fin
	}
	return ""; // si il n'y a pas de mot de plus de 3 lettres renvoit rien
}
//fin filtre meta_keywords

	?>
