<?php 
  
// *****************  
// FORMATER LA DATE 
// *****************

function formateDate($date)
{
// on en fait un tableau 
$tableau = explode("-",$date);

// on formate 
$annee = $tableau[0];
$mois = $tableau[1];
$jour = $tableau[2];

// affichage 
echo "$jour/$mois/$annee";
}


// **************************
// CONTROLER LES FORMULAIRES 
// **************************

// Verifier la longueur de la chaine 
 
function check_long($var , $taille_min , $taille_max)
{	  
	if (!isset($var)) return FALSE; 
	
	elseif (strlen($var) < $taille_min) return FALSE;
	
	elseif (strlen($var) > $taille_max) return FALSE;
	
	else return TRUE;
}


// Verifier si elle ne contient pas de caracteres interdits

function check_char($var)
{	
	if (ereg("[^A-Za-z0-9_]", $var)) return FALSE; // si elle contient autre chose que des lettres de A � Z ou des chiffres de 0 a 9 ou un under-score

	else return TRUE;
}


// Verifier si c'est une URL 

function check_url($var)
{	
	if (ereg("^http:/{2}", $var)) return TRUE; // si elle commence par http://... 	

	else return FALSE;
}

// Verifier le mot de passe 

function check_mdp($var1, $var2)
{
	if ($var1 == $var2) return TRUE; 
	
	else return FALSE;
}

// Verifier si une base avec le meme nom existe 

function check_base($var)
{
$baseExiste = "non";

$requete = mysql_query("SHOW DATABASES;");

	while($listeBases = mysql_fetch_row($requete)) 
	{
		if(strtolower($var) == strtolower($listeBases[0])) $baseExiste = "oui";
	}
	
	// Verifier si le nom ne correspond pas a un repertoire par defaut de 'sites'
	if($var == "logs" | $var == "images")
	{
		$baseExiste = "oui";	
	}
	
if ($baseExiste == "oui") return FALSE; 

else return TRUE;
}


// ***********************************************************************************
// SYNCHRO - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
// ***********************************************************************************


// Chemin relatif du dossier des sites 
$GLOBALS['cheminCopie'] = "sites/"; 

// CREER LES REPERTOIRES 
function copierDossierSynchro($nomDossierCopie)
{
if(is_dir($GLOBALS['cheminCopie'] . $GLOBALS['dossierSite'] . $nomDossierCopie))
	{
	$contenuLog =  "\r\rDossier '$nomDossierCopie' trouv�\r";
	fwrite($GLOBALS['log'], $contenuLog);
	}
else 
	{
	if(mkdir($GLOBALS['cheminCopie'] . $GLOBALS['dossierSite'] . $nomDossierCopie, 0777)) 
		{
		$contenuLog = "\r\rCopie du dossier '$nomDossierCopie' effectu�e</b>\r";
		fwrite($GLOBALS['log'], $contenuLog);
		}
	else
		{
		$contenuLog = "\r\r!!! La copie du dossier '$nomDossierCopie' a �chou�e\r";
		fwrite($GLOBALS['log'], $contenuLog);
		}
	}
}

// COPIER LES FICHIERS
function copierFichierSynchro($NomfichierCopie)
{
if (!copy($GLOBALS['dossierModele'] . $NomfichierCopie, $GLOBALS['cheminCopie'] . $GLOBALS['dossierSite'] . $NomfichierCopie))
	{
	$contenuLog = "\r\r!!! La copie du fichier '$NomfichierCopie' a �chou�e\r";
	fwrite($GLOBALS['log'], $contenuLog);
	}
}

// COPIER LES FICHIERS DU MODELE DANS LE REPERTOIRE DU SITE (4 niveaux de repertoires max)

// NIVEAU 1 ------------------------------------

function synchro()
{
$chemin_0= "";

$contenuLog =  "\r\r -> ".$GLOBALS['dossierModele'].$chemin_0."\r" ;
fwrite($GLOBALS['log'], $contenuLog);
$contenu= opendir($GLOBALS['dossierModele'] . $chemin_0);
	while (($nomFichier = readdir($contenu)) !==false)
	{
	// ne pas lire ".", "..", "CACHE", "ecrire"
	if ($nomFichier !="." && $nomFichier !=".." && $nomFichier !="CACHE" && $nomFichier !="ecrire")
		{
		if (is_file($GLOBALS['dossierModele'] . $chemin_0 . $nomFichier)) // voir si c'est un ficher
			{
			$contenuLog =  " - ".$GLOBALS['dossierModele']."$nomFichier -> ".$GLOBALS['cheminCopie'].$GLOBALS['dossierSite'].$chemin_0.$nomFichier."\r";
			fwrite($GLOBALS['log'], $contenuLog);
			copierFichierSynchro($chemin_0.$nomFichier);
			}
		else // c'est un dossier
			{
			copierDossierSynchro($chemin_0.$nomFichier);
			synchro_1($nomFichier,$chemin_0);
			}
		}
	}
	$contenuLog = "\r";
	fwrite($GLOBALS['log'], $contenuLog);
	closedir($contenu);
	return true;
}

// NIVEAU 2 ------------------------------------

function synchro_1($nomFichier,$chemin_0)
{
$chemin_1 = $chemin_0 . $nomFichier . "/"; // creation du chemin

$contenuLog =  "\r\r - -> ".$GLOBALS['dossierModele'].$chemin_1."\r" ;
fwrite($GLOBALS['log'], $contenuLog);
$contenu= opendir($GLOBALS['dossierModele'] . $chemin_1);
	while (($nomFichier = readdir($contenu)) !==false)
	{
	if ($nomFichier !="." && $nomFichier !="..") // ne pas lire ".",".."
		{
		if (is_file($GLOBALS['dossierModele'] . $chemin_1 . $nomFichier)) // voir si c'est un ficher
			{
			$contenuLog =  " - - ".$GLOBALS['dossierModele'].$chemin_1.$nomFichier." -> ".$GLOBALS['cheminCopie'].$GLOBALS['dossierSite'].$chemin_1.$nomFichier."\r";
			fwrite($GLOBALS['log'], $contenuLog);
			copierFichierSynchro($chemin_1.$nomFichier);
			}
		else // c'est un dossier
			{
			copierDossierSynchro($chemin_1.$nomFichier);
			synchro_2($nomFichier,$chemin_1);
			}
		}
	}
	$contenuLog =  "\r";
	fwrite($GLOBALS['log'], $contenuLog);
	closedir($contenu);
}

// NIVEAU 3 ------------------------------------

function synchro_2($nomFichier,$chemin_1)
{
$chemin_2 = $chemin_1 . $nomFichier . "/"; // creation du chemin

$contenuLog =  "\r\r - - -> ".$GLOBALS['dossierModele'].$chemin_2."\r" ;
fwrite($GLOBALS['log'], $contenuLog);
$contenu= opendir($GLOBALS['dossierModele'] . $chemin_2);
	while (($nomFichier = readdir($contenu)) !==false)
	{
	
	if ($nomFichier !="." && $nomFichier !="..") 
		{
		if (is_file($GLOBALS['dossierModele'] . $chemin_2.$nomFichier)) // voir si c'est un ficher
			{
			$contenuLog =  " - - - ".$GLOBALS['dossierModele'].$chemin_2.$nomFichier." -> ".$GLOBALS['cheminCopie'].$GLOBALS['dossierSite'].$chemin_2.$nomFichier."\r";
			fwrite($GLOBALS['log'], $contenuLog);
			copierFichierSynchro($chemin_2.$nomFichier);
			}
		else // c'est un dossier
			{
			copierDossierSynchro($chemin_2.$nomFichier);
			synchro_3($nomFichier,$chemin_2);
			}
		}
	}
	$contenuLog =  "\r";
	fwrite($GLOBALS['log'], $contenuLog);
	closedir($contenu);
}


// NIVEAU 4 ------------------------------------

function synchro_3($nomFichier,$chemin_2)
{
$chemin_3 = $chemin_2 . $nomFichier . "/"; // creation du chemin

$contenuLog =  "\r\r - - - -> ".$GLOBALS['dossierModele'].$chemin_3."\r" ;
fwrite($GLOBALS['log'], $contenuLog);
$contenu= opendir($GLOBALS['dossierModele'] . $chemin_3);
	while (($nomFichier = readdir($contenu)) !==false)
	{
	if ($nomFichier !="." && $nomFichier !="..") // ne pas lire ".",".."
		{
		if (is_file($GLOBALS['dossierModele'] . $chemin_3.$nomFichier)) // voir si c'est un ficher
			{
			$contenuLog =  " - - - - ".$GLOBALS['dossierModele'].$chemin_3.$nomFichier." -> ".$GLOBALS['cheminCopie'].$GLOBALS['dossierSite'].$chemin_3.$nomFichier."\r";
			fwrite($GLOBALS['log'], $contenuLog);
			copierFichierSynchro($chemin_3.$nomFichier);
			}
		else // c'est un dossier
			{
			$contenuLog =  "!!!!! $chemin_3$nomFichier : le chemin est trop long !!!!!\r";
			fwrite($GLOBALS['log'], $contenuLog);
			}
		}
	}
	$contenuLog =  "\r";
	fwrite($GLOBALS['log'], $contenuLog);
	closedir($contenu);
}


?>