<?php
$niveauPage= "";

include($niveauPage . "inc_verif.php"); // Verification de l'ID de l'inscrit 

include($niveauPage . "inc_fonctions.php");

// Connection 
include($niveauPage . "inc_connection.php");

// Recuperation des variables dans le POST 
if(isset($_POST['Nom']))
	$Nom = $_POST['Nom'];
else
	$Nom = "";
if(isset($_POST['Adresse']))
	$Adresse = $_POST['Adresse'];
else
	$Adresse = "http://";
if(isset($_POST['Description'])) {
	$Description = $_POST['Description'];
	// Traitement de la description
	$Description= addslashes($Description);
} else
	$Description = "";

if(isset($_POST['Submit']))
	$Submit = $_POST['Submit'];
else
	$Submit = "";


// VERIFICATION DES INFOS ENVOYEES  



// liste des erreurs

$debutErreur = "<tr><td colspan='2' class='edf7'><b><font color='FF0000'>";
$finErreur = "</font></b></td></tr>";

$erreur = array(
$debutErreur . "Attention, tous les champs sont obligatoires !" . $finErreur,
$debutErreur . "Le nom de votre site doit �tre compos� de 3 � 30 caract�res" . $finErreur,
$debutErreur . "L'adresse du site doit commencer par \"HTTP://\" et �tre compos�e de 10 � 100 caract�res" . $finErreur,
$debutErreur . "La description de votre site doit �tre compos�e de 10 � 100 caract�res" . $finErreur,
);


$reponse = "";

if ($Submit)
{
	if($Nom && $Adresse && $Description)
	{
		// verifier le nom du site
		if(!check_long($Nom, 3, 30)) 
		{
			$reponse = $erreur[1];
		}
		// verifier si "Adresse" est une URL 
		elseif(!check_url($Adresse) | !check_long($Adresse, 10, 100)) 
		{
			$reponse = $erreur[2];
		}
		// verifier la description
		elseif(!check_long($Description, 10, 100)) 
		{
			$reponse = $erreur[3];
		}
		// sinon, c'est ok pour enregistrement
		else
		{
			$reponse = "ok";
		}
	}
}

?>
<html>
<head>
<title>Multispip - Enregistrement</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css_edf.css" rel="stylesheet" type="text/css">
</head>
<body>
<form name="formulaire" method="post" action="enregistrement.php">
<table width="600" border="0" cellpadding="1" cellspacing="0" bgcolor="#003366">

  <tr>
    <td><table width="100%" border="0" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="1">
                <tr> 
                  <td bgcolor="#003366"><table width="100%" border="0" cellpadding="3" cellspacing="0" bgcolor="#003366">
                      <tr> 
                        <td class="edf7"><strong><font color="#FFFFFF">Administration 
                          - Enregister un site existant </font></strong></td>
                      </tr>
                    </table></td>
                </tr>
                <tr> 
                  <td><img src="images/dot.gif" width="500" height="5"></td>
                </tr>
                <tr>
                  <td class="edf7">
<?php
if ($reponse == "ok")
{
// Connection 
mysql_connect($hote,$utilisateurBase,$mdpBase);
mysql_select_db($base);

// Recuperation de la date 

$date= date("Y-m-d");

$Description= mysql_escape_string($Description);
$Nom= mysql_escape_string($Nom);
$Adresse = mysql_escape_string($Adresse);
settype($idUser, 'integer');

// requetes 
$inserer= "INSERT INTO sites 
(IdSite, NomSite, UrlSite, DescriptionSite, dateEnregistrement, IdInscrit, multispip) 
VALUES ('', '$Nom', '$Adresse', '$Description', '$date', '$idUser', 'non')";

mysql_query($inserer) or die("la requete a echou�e parce que : ". mysql_error());
echo "<p align=\"center\"><br><b><i>Le site � �t� ajout�</i></b><br></p><script language=\"JavaScript\">document.location.href=\"accueil_inscrits.php\";</script>";
} else { 
	if ($reponse) 
		{
		// Reponse si erreur
		echo "$reponse";
		}
// Afficher le formulaire de saisie 
?>
			<table width="100%" border="0" cellpadding="1" cellspacing="0">
                      <tr> 
                        <td class="edf7"><em>Nom du site</em></td>
                        <td align="right" class="edf7"><input name="Nom" type="text" id="Nom" value="<?php echo $Nom; ?>"></td>
                      </tr>
                      <tr> 
                        <td colspan="2"><img src="images/dot.gif" width="500" height="5"></td>
                      </tr>
                      <tr> 
                        <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td background="images/pointill_bleu.gif"><img src="images/dot.gif" width="2" height="1"></td>
                            </tr>
                          </table></td>
                      </tr>
                      <tr> 
                        <td colspan="2"><img src="images/dot.gif" width="500" height="5"></td>
                      </tr>
                      <tr> 
                        <td class="edf7"><em>Adresse du site</em></td>
                        <td align="right"><input name="Adresse" type="text" id="Adresse" value="<?php if(isset($Adresse)) echo $Adresse; else echo "http://";?>"></td>
                      </tr>
                      <tr> 
                        <td colspan="2"><img src="images/dot.gif" width="500" height="5"></td>
                      </tr>
                      <tr> 
                        <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td background="images/pointill_bleu.gif"><img src="images/dot.gif" width="2" height="1"></td>
                            </tr>
                          </table></td>
                      </tr>
                      <tr> 
                        <td colspan="2"><img src="images/dot.gif" width="500" height="5"></td>
                      </tr>
                      <tr> 
                        <td valign="top" class="edf7"><em>Description du site&nbsp;&nbsp;</em></td>
                        <td align="right"><textarea name="Description" id="textarea"><?php echo $Description; ?></textarea></td>
                      </tr>
                      <tr> 
                        <td colspan="2"><img src="images/dot.gif" width="500" height="5"></td>
                      </tr>
                      <tr> 
                        <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td background="images/pointill_bleu.gif"><img src="images/dot.gif" width="2" height="1"></td>
                            </tr>
                          </table></td>
                      </tr>
                      <tr> 
                        <td colspan="2"><img src="images/dot.gif" width="500" height="5"></td>
                      </tr>
                      <tr> 
                        <td>&nbsp;</td>
                        <td align="right"> <input type="submit" name="Submit" value="  Envoyer  "> 
                        </td>
                      </tr>
                    </table>
<?php } ?>
		</td>
                </tr>
                <tr> 
                  <td><img src="images/dot.gif" width="500" height="5"></td>
                </tr>
                <tr> 
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td bgcolor="#003366"><img src="images/dot.gif" width="2" height="1"></td>
                      </tr>
                    </table></td>
                </tr>
                <tr> 
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr> 
                        <td><table width="100%" border="0" cellspacing="0" cellpadding="1">
                            <tr> 
                              <td bgcolor="#999999"><table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
                                  <tr> 
                                    <td align="center" bgcolor="#999999" class="edf8"><a href="accueil_inscrits.php" class="edf8"><font color="#FFFFFF">Retourner 
                                      &agrave; l'administration</font></a></td>
                                  </tr>
                                </table></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>

</table>
</form>
</body>
</html>
