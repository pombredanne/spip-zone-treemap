<?php

include( $contexte_multispip . $niveauPage . "inc_fonctions.php"); 
include($niveauPage . "inc_connection.php");

?>
<table width="600" border="0" cellpadding="1" cellspacing="0" bgcolor="#003366">
  <tr>
    <td><table width="100%" border="0" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr> 
                <td bgcolor="#003366"><table width="100%" border="0" cellpadding="3" cellspacing="0" bgcolor="#003366">
                    <tr> 
                      <td class="edf7"><strong><font color="#FFFFFF">Liste des 
                        sites cr&eacute;&eacute;s par des membres de <?php echo $nom_site; ?> 
                        </font></strong></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td><img src="<?php echo $contexte_multispip ?>images/dot.gif" width="500" height="5"></td>
              </tr>
              <tr> 
                <td> 
                  <?php include( $contexte_multispip . "listeSites.php"); ?>
                </td>
              </tr>
              <tr> 
                <td align="center" class="edf6"><em>Cliquez sur le nom pour visiter 
                  le site. <br>
                  Cliquez sur le nom de son cr&eacute;ateur pour consulter les 
                  informations le concernant.</em></td>
              </tr>
              <tr> 
                <td><img src="<?php echo $contexte_multispip ?>images/dot.gif" width="500" height="10"></td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td bgcolor="#003366"><img src="<?php echo $contexte_multispip ?>images/dot.gif" width="2" height="1"></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="3">
                    <tr> 
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="1">
                          <tr> 
                            <td bgcolor="#999999"><table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
                                <tr> 
                                  <td align="center" bgcolor="#999999"><a href="<?php echo $contexte_multispip ?>login.php" class="edf8"><font color="#FFFFFF">Cr&eacute;er 
                                    votre site</font></a></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table></td>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="1">
                          <tr> 
                            <td bgcolor="#999999"> <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
                                <tr> 
                                  <td align="center" bgcolor="#999999"><a href="<?php echo $contexte_multispip ?>login.php" class="edf8"><font color="#FFFFFF">Administrer 
                                    vos sites</font></a></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>

