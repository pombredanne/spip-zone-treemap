<?php 
$niveauPage= "";

include($niveauPage . "inc_verif.php"); // Verification de l'ID de l'inscrit 

include($niveauPage . "inc_fonctions.php");
?>
<html>
<head>
<title>Multispip - Administration</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="css_edf.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="600" border="0" cellpadding="1" cellspacing="0" bgcolor="#003366">
  <tr>
    <td><table width="100%" border="0" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr> 
                <td bgcolor="#003366"><table width="100%" border="0" cellpadding="3" cellspacing="0" bgcolor="#003366">
                    <tr> 
                      <td class="edf7"><strong><font color="#FFFFFF">Administration</font></strong></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td><img src="images/dot.gif" width="500" height="5"></td>
              </tr>
              <tr> 
                <td class="edf7"><em>Vos sites</em></td>
              </tr>
              <tr> 
                <td><img src="images/dot.gif" width="500" height="5"></td>
              </tr>
              <tr> 
                <td><?php include("listeSites.php"); ?></td>
              </tr>
              <tr>
                <td align="right" class="edf6"><img src="images/dot.gif" width="500" height="5"></td>
              </tr>
              <tr> 
                <td class="edf11"><em>Si vous souhaitez r&eacute;ferencer un site 
                  existant : <a href="enregistrement.php"><b>cliquez ici</b></a> 
                  </em></td>
              </tr>
              <tr> 
                <td><img src="images/dot.gif" width="500" height="10"></td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td bgcolor="#003366"><img src="images/dot.gif" width="2" height="1"></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="3">
                    <tr> 
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="1">
                          <tr> 
                            <td bgcolor="#999999"> <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
                                <tr> 
                                  <td align="center" bgcolor="#999999"><a href="creerSite/choisirModele.php" class="edf8"><font color="#FFFFFF">Cr&eacute;er 
                                    votre site</font></a></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table></td>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="1">
                          <tr> 
                            <td bgcolor="#999999"> <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
                                <tr> 
                                  <td align="center" bgcolor="#999999"><a href="suppression.php" class="edf8"><font color="#FFFFFF">Supprimer 
                                    un de vos sites</font></a></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table></td>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="1">
                          <tr> 
                            <td bgcolor="#999999"> <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
                                <tr> 
                                  <td align="center" bgcolor="#999999"><a href="index.php" class="edf8"><font color="#FFFFFF">Quitter</font></a></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
<span class="edf11"><img src="images/dot.gif" width="500" height="5"><br><em>Acc&eacute;der au <a href="synchro.php">module de synchronisation</a> (r&eacute;serv&eacute; aux administrateurs)</em></span> 
</body>
</html>
