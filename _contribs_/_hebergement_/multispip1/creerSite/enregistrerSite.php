<?php 
$niveauPage= "../";

// Verification de l'ID de l'inscrit
include($niveauPage . "inc_verif.php"); 

// Recuperer le nom du modele de site dans la session 
$modeleSite = $_SESSION['modeleSite'];
// Recuperer le nom du site dans la session 
$nomSite = $_SESSION['nomSite'];
// Recuperer le dossier du site dans la session

include($niveauPage . "inc_fonctions.php");
include("inc_config.php");
include("fonctions_creer.php"); 

// Connection 
include($niveauPage . "inc_connection.php");
mysql_connect($hote,$utilisateurBase,$mdpBase);

// construction de l'adresse d�finitive du site 
$Adresse = $AdresseSites . $nomSite . "/";

// VERIFICATION DES INFOS ENVOYEES  
$debutErreur = "<tr><td colspan='2' class='edf7'><b><font color='FF0000'>";
$finErreur = "</font></b></td></tr>";

// liste des erreurs
$erreur = array(
$debutErreur . "Attention, tous les champs sont obligatoires !" . $finErreur,
$debutErreur . "Le nom de votre site doit �tre compos� de 3 � 50 caract�res" . $finErreur,
$debutErreur . "La description de votre site doit �tre compos�e de 10 � 100 caract�res" . $finErreur,
$debutErreur . "Votre login doit �tre au moins compos� de 5 caract�res" . $finErreur,
$debutErreur . "Votre mot de passe doit �tre au moins compos� de 6 caract�res" . $finErreur
);

// reponse Par defaut
$reponse = "";

// Recuperer les infos dans le POST 
if(isset($_POST['Submit']))
	$Submit = $_POST['Submit'];
else
	$Submit = "";
if(isset($_POST['Nom']))
	$Nom = $_POST['Nom'];
else	
	$Nom = "";
if(isset($_POST['Description']))
	$Description = $_POST['Description'];
else
	$Description = "";
if(isset($_POST['loginAdmin']))
	$loginAdmin = $_POST['loginAdmin'];
else
	$loginAdmin = "";
if(isset($_POST['mdpAdmin']))
	$mdpAdmin = $_POST['mdpAdmin'];
else
	$mdpAdmin = "";
// Verification 
if ($Submit)
{
	if($Nom && $Description && $loginAdmin && $mdpAdmin)
	{
		// verifier le nom du site
		if(!check_long($Nom, 3, 50)) 
		{
			$reponse = $erreur[1];
		}
		// verifier la description
		elseif(!check_long($Description, 10, 100)) 
		{
			$reponse = $erreur[2];
		}
		// verifier le login d'acces au nouveau site 
		elseif(!check_long($loginAdmin, 5, 20)) 
		{
			$reponse = $erreur[3];
		}
		// verifier le mot de passe d'acces au nouveau site
		elseif(!check_long($mdpAdmin, 6, 20)) 
		{
			$reponse = $erreur[4];
		}
		// sinon, c'est ok pour enregistrement
		else
		{
			$reponse = "ok";
		}
	}
}

?>
<html>
<head>
<title>Enregistrement</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css_edf.css" rel="stylesheet" type="text/css">
</head>
<body>
<form name="form1" method="post" action="enregistrerSite.php">
<table width="600" border="0" cellpadding="1" cellspacing="0" bgcolor="#003366">
  <tr> 
    <td><table width="100%" border="0" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><table width="100%" border="0" cellspacing="0" cellpadding="1">
                <tr> 
                  <td colspan="2" bgcolor="#003366"><table width="100%" border="0" cellpadding="3" cellspacing="0" bgcolor="#003366">
                      <tr> 
                        <td class="edf7"><strong><font color="#FFFFFF">Cr&eacute;er 
                          votre site - Personalisation</font></strong></td>
                      </tr>
                    </table></td>
                </tr>
                <tr> 
                  <td colspan="2"><img src="../images/dot.gif" width="500" height="5"></td>
                </tr>
                <?php
			
// ---------------- affich� apr�s envois du formulaire d'inscription du nouveau site  ------------------

if ($reponse == "ok") // verification
{
// Mise a jour de la base du nouveau site
enregistrerNouveauSite($base,$Nom,$Adresse,$Description,$idUser,$loginAdmin,$mdpAdmin,$nomSite,$modeleSite);

// on efface toutes les donn�es de la session
session_destroy(); 

?>
                <tr> 
                  <td colspan="2" class="edf7">Votre site a �t� ajout� avec succ�s 
                    dans la liste des sites de <?php echo $nom_site; ?></td>
                </tr>
                <tr> 
                  <td colspan="2" class="edf7"><img src="../images/dot.gif" width="500" height="5"></td>
                </tr>
                <tr> 
                  <td colspan="2" class="edf7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td background="../images/pointill_bleu.gif"><img src="images/dot.gif" width="2" height="1"></td>
                      </tr>
                    </table></td>
                </tr>
                <tr> 
                  <td colspan="2" class="edf7"><img src="../images/dot.gif" width="500" height="5"></td>
                </tr>
                <tr> 
                  <td colspan="2" class="edf7"><p class="edf8"><em><strong>L'adresse 
                      de votre site est : <a href="<?php echo $Adresse ?>" target="_blank"><?php echo $Adresse ?></a></strong></em></p>
                    <p class="edf8">Vous pouvez d&egrave;s maintenant vous rendre 
                      dans le <a href="<?php echo $Adresse . "/ecrire/"; ?>" target="_blank">module 
                      d'administration</a> de votre site.<em><br>
                      <br>
                      Votre login est : <b><?php echo $loginAdmin; ?></b><br>
                      Votre mot de passe est : <?php echo $mdpAdmin; ?></em></p></td>
                </tr>
                <tr> 
                  <td colspan="2" class="edf7"><img src="../images/dot.gif" width="500" height="5"></td>
                </tr>
                <tr> 
                  <td colspan="2" class="edf5"> <p><em>Conservez pr&eacute;cieusement 
                      ces identifiants, ils vous seront n&eacute;cessaires pour 
                      administrer votre site. </em> </td>
                </tr>
                <tr> 
                  <td colspan="2"><img src="../images/dot.gif" width="500" height="10"></td>
                </tr>
                <tr> 
                  <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td bgcolor="#003366"><img src="../images/dot.gif" width="2" height="1"></td>
                      </tr>
                    </table></td>
                </tr>
                <tr> 
                  <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr> 
                        <td><table width="100%" border="0" cellspacing="0" cellpadding="1">
                            <tr> 
                              <td bgcolor="#999999"> <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
                                  <tr> 
			  <td align="center" bgcolor="#999999"><strong><a href="<?php echo $adresseMultispip; ?>index.php" class="edf8"><font color="#FFFFFF">Retourner &agrave; l'accueil</font></a></strong></td>
                                  </tr>
                                </table></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table></td>
                </tr>
                <?php 
} 
// ---------------------------- affich� avant l'envois du formulaire ----------------------------------------
else
{ 
// Si erreur : r�ponse suite � l'envois du formulaire
echo "$reponse"; 
?>
                
                    
                <tr> 
                  <td class="edf7"> <strong> Choisissez le nom complet de votre 
                    site </strong></td>
                  <td align="right" valign="bottom" class="edf7"> <input name="Nom" type="text" id="Nom2" value="<?php echo $Nom; ?>" size="30" maxlength="50"> 
                  </td>
                </tr>
                <tr> 
                  <td colspan="2" class="edf5"> Vous pouvez choisir un nom avec 
                    des espaces. (exemple : Le site du projet bidule) </td>
                </tr>
                <tr> 
                  <td colspan="2" class="edf7"><img src="../images/dot.gif" width="500" height="5"></td>
                </tr>
                <tr> 
                  <td colspan="2" class="edf7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td background="../images/pointill_bleu.gif"><img src="images/dot.gif" width="2" height="1"></td>
                      </tr>
                    </table></td>
                </tr>
                <tr> 
                  <td colspan="2" class="edf7"><img src="../images/dot.gif" width="500" height="5"></td>
                </tr>
                <tr> 
                  <td class="edf7">Description du site ( minimum 10 caracteres )</td>
                  <td align="right" class="edf7"> <textarea name="Description" id="textarea"><?php echo $Description; ?></textarea> 
                  </td>
                </tr>
                <tr> 
                  <td colspan="2" class="edf7"><img src="../images/dot.gif" width="500" height="5"></td>
                </tr>
                <tr> 
                  <td colspan="2" class="edf7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td background="../images/pointill_bleu.gif"><img src="images/dot.gif" width="2" height="1"></td>
                      </tr>
                    </table></td>
                </tr>
                <tr> 
                  <td colspan="2" class="edf7"><img src="../images/dot.gif" width="500" height="5"></td>
                </tr>
                <tr> 
                  <td colspan="2" class="edf7"><table width="100%" border="0" cellpadding="1" cellspacing="0">
                      <tr> 
                        <td rowspan="3" class="edf7"><strong><em>Choisissez un 
                          login et un mot de passe<br>
                          </em></strong><span class="edf4">Ils vous permettrons 
                          d'acc&eacute;der au module d'administration de votre 
                          site.</span></td>
                        <td align="right" nowrap class="edf7">Login : </td>
                        <td align="left" class="edf7"><input name="loginAdmin" type="text" id="loginAdmin3" value="<?php echo $loginAdmin; ?>" size="10" maxlength="10"></td>
                      </tr>
                      <tr> 
                        <td colspan="2" class="edf7"><img src="../images/dot.gif" width="100" height="5"></td>
                      </tr>
                      <tr> 
                        <td align="right" nowrap class="edf7">Mot de passe : </td>
                        <td align="left" class="edf7"><input name="mdpAdmin" type="password" id="mdpAdmin4" value="<?php echo $mdpAdmin; ?>" size="8" maxlength="8"></td>
                      </tr>
                    </table></td>
                </tr>
                <tr> 
                  <td colspan="2" class="edf7"><img src="../images/dot.gif" width="500" height="5"></td>
                </tr>
                <tr> 
                  <td colspan="2" class="edf7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td background="../images/pointill_bleu.gif"><img src="images/dot.gif" width="2" height="1"></td>
                      </tr>
                    </table></td>
                </tr>
                <tr> 
                  <td colspan="2" class="edf7"><img src="../images/dot.gif" width="500" height="5"></td>
                </tr>
                <tr align="right"> 
                  <td colspan="2" class="edf7"> <input type="submit" name="Submit" value="Envoyer"> 
                  </td>
                </tr>
                <tr> 
                  <td colspan="2" class="edf7"><img src="../images/dot.gif" width="500" height="5"></td>
                </tr>
                <?php 
}
?>
              </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</form>
</body>
</html>
