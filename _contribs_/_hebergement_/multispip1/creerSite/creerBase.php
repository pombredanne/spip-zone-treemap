<?php
// Inclure les fonctions communes 
$niveauPage= "../";

// Verification de l'ID de l'inscrit
include($niveauPage . "inc_verif.php"); 

// Recuperer le nom du modele de site dans la session 
$modeleSite = $_SESSION['modeleSite'];

// Recuperer le nom du site dans le POST
if (!isset($_POST['nomSite']))  // pour �viter des 'notices' php
	$nomSite = ""; 
else
	$nomSite = $_POST['nomSite'];

include($niveauPage . "inc_fonctions.php");
include("fonctions_creer.php"); 
include("inc_config.php");

// Connection 
include($niveauPage . "inc_connection.php");
mysql_connect($hote,$utilisateurBase,$mdpBase);

// requetes
$creerBase= "CREATE DATABASE ".mysql_escape_string($nomSite);

$droits_base="GRANT ALL PRIVILEGES ON `".mysql_escape_string($nomSite)."`.* TO `".mysql_escape_string($utilisateurBase)."`@`".$hote."` WITH GRANT OPTION ;";
// faire en plus un reload ?

// TESTS SUR LE NOM DU SITE 

$validation = "";

if($nomSite)
{
	if(check_long($nomSite, 3, 15) && check_char($nomSite) && check_base($nomSite)) 
	{
		$validation = "ok";
	}
	elseif(!check_base($nomSite))
	{
	$validation = "erreur2"; // La base existe deja 
	}
	else 
	{
		$validation = "erreur1"; // Probleme de caract�re ou de longueur
	}
}


?>
<html>
<head>
<title>Multispip - C&eacute;er une base</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css_edf.css" rel="stylesheet" type="text/css">
</head>

<body>
<form name="form1" method="post" action="creerBase.php">
<table width="600" border="0" cellpadding="1" cellspacing="0" bgcolor="#003366">
  <tr> 
    <td><table width="100%" border="0" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><table width="100%" border="0" cellspacing="0" cellpadding="1">
                <tr> 
                  <td bgcolor="#003366"><table width="100%" border="0" cellpadding="3" cellspacing="0" bgcolor="#003366">
                      <tr> 
                        <td class="edf7"><strong><font color="#FFFFFF">Cr&eacute;er 
                          votre site - Choix de l'URL</font></strong></td>
                      </tr>
                    </table></td>
                </tr>
                <tr> 
                  <td><img src="../images/dot.gif" width="500" height="5"></td>
                </tr>
                <tr> 
                  <td>
				<?php if(!isset($nomSite) | $validation != "ok")
				{ 				
				  ?>
				  <table border="0" cellpadding="1" cellspacing="0">
                      <tr valign="middle"> 
                        <td class="edf7"> <p><em><strong>Choisir le nom court 
                            de votre site :</strong></em></p></td>
                         <!-- Champs de saisie du nom du site --> 
                        <td class="edf7"><input name="nomSite" type="text" id="nomSite" value="<?php echo $nomSite ?>"> 
                          <input type="submit" name="creer" value="cr�er le site"> 
                        </td>
                      </tr>
                     
                    </table>
				<?php 
				} 
				?>
					
					</td>
                </tr>
                <tr> 
                  <td><img src="../images/dot.gif" width="500" height="10"></td>
                </tr>
                <tr> 
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td background="../images/pointill_bleu.gif"><img src="images/dot.gif" width="2" height="1"></td>
                      </tr>
                    </table></td>
                </tr>
                <tr> 
                  <td align="center" class="edf4">
				  <?php
				  	// Alerte si le nom n'est pas valide 
					if ($validation == "erreur1")
					{
						echo "<font color='#FF0000'><b>Ce nom n'est pas valide.</b> Verifiez qu'il ne contient ni espaces, ni caract�res speciaux et qu'il est constitu� de 3 � 15 caract�res.</font>";
					}
					elseif ($validation == "erreur2")
					{
						echo "<font color='#FF0000'><b>Ce nom n'est pas disponible.</b> Veuillez en choisir un different ... </font>";
					}
				  	elseif(isset($nomSite) && $validation == "ok")
					{
						// Alerte confirmant que le nom est accept� 
						echo "<font color='#FF0000'><b>Le nom que vous avez choisi est valid� !<b></font>";
					}
					else
					{ 
					?> 
                    <p>Ce nom apparaitra dans l'adresse du site <br>
					 (Exemple : <?php echo "$AdresseSites"; ?><i>projetMachin</i>)
                      <br>
                      <em>Il ne doit contenir ni espaces ni caract&egrave;res accentu&eacute;s.</em></p></td>
					  <?php 
					  } 
					  ?>
                </tr>
                <tr> 
                  <td><img src="../images/dot.gif" width="500" height="10"></td>
                </tr>
                <tr> 
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td bgcolor="#003366"><img src="../images/dot.gif" width="2" height="1"></td>
                      </tr>
                    </table></td>
                </tr>
                <tr> 
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr> 
                        <td><table width="100%" border="0" cellspacing="0" cellpadding="1">
                            <tr> 
                              <td bgcolor="#999999"> <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
                                  <tr> 
                                    <td align="center" bgcolor="#999999" class="edf8"> 
<?php
       
// *************** Page affich�e apres choix du nom du site ************************
      
      
      
if (isset($nomSite) && $validation == "ok") 
	{
         sleep(5);
		// cr�er la base enregistrer le nom de la base dans la session et aller � la page suivante 
		mysql_query($creerBase) or die("la requete de creation a echou�e parce que : ". mysql_error());
		mysql_query($droits_base) or die("la requete de permissions a echou�e parce que : ". mysql_error());

//$droits_base="GRANT ALL PRIVILEGES ON `".$nomSite."`.* TO `".$utilisateurBase."@".$hote." WITH GRANT OPTION ;"

		session_register('nomSite');
		$_SESSION['nomSite'] = $nomSite;
        sleep(5);
		
?>
                                      
 <script language="JavaScript">document.location.href="remplirBase.php";</script>
 <a href="remplirBase.php" class="edf8"><font color="#FFFFFF">Etape suivante &gt;&gt;&gt;</font></a> 

<?php

	} 
else 
	{ 
?>
                                      <font color="#FFFFFF">Vous avez s&eacute;l&eacute;ctionn&eacute; 
                                      le mod&egrave;le : <?php echo "$modeleSite" ?> 
<?php 
	} 
?>
                                      </font></td>
                                  </tr>
                                </table></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table></td>
                </tr>
                <!-- fin nav bas -->
              </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</form>

</body>
</html>
