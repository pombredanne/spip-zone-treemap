<?php 

// Inclure les fonctions communes 
$niveauPage= "../";

// Verification de l'ID de l'inscrit
include($niveauPage . "inc_verif.php"); 

// Recuperer les variables 
if (!isset($_POST['ChoixModeleSite']))  // pour �viter des 'notices' php
	$ChoixModeleSite = ""; 
else
	$ChoixModeleSite = $_POST['ChoixModeleSite'];

include($niveauPage . "inc_fonctions.php");
include("inc_config.php");

?>

<html>
<head>
<title>Multispip - Choisir le mod�le de site</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css_edf.css" rel="stylesheet" type="text/css">
</head>

<body>

<table width="600" border="0" cellpadding="1" cellspacing="0" bgcolor="#003366">
  <tr>
    <td><table width="100%" border="0" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr> 
                <td bgcolor="#003366"><table width="100%" border="0" cellpadding="3" cellspacing="0" bgcolor="#003366">
                    <tr> 
                      <td class="edf7"><strong><font color="#FFFFFF">Cr&eacute;er votre site </font></strong></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td><img src="../images/dot.gif" width="500" height="5"></td>
              </tr>
             
              <tr>
                <td><table border="0" cellpadding="1" cellspacing="0">
                    <tr valign="middle"> 
                      <td class="edf7"> 
                        <p><em>Choisir un mod&egrave;le de site : &nbsp;</em></p></td>
                        
                        <!-- Choix du mod�le du site --> 
			<form name="formulaire" method="post" action="choisirModele.php">
                      <td class="edf7"> 
                          <select name="ChoixModeleSite" id="modeleSite" onChange="javascript:document.formulaire.submit()">
                            <option <?php if(!isset($modeleSite)){echo " selected";} ?> value="">Choisir</option>
                        <?php 
			// Liste des modeles 
			for ($i = 0; $i < count($listeModeles); $i++)
			{
			echo "<option value=\"$listeModeles[$i]\"";
			if($ChoixModeleSite==$listeModeles[$i])
				{
				echo " selected";
				} 
			echo ">$NomsModeles[$i]</option>";
			}
			?>
                          </select>
                        </td>
						</form>
                    </tr>
                    
<?php 
              
// ****************** Affichage apres choix du modele *******************
				
if($ChoixModeleSite) 
{

// Enregistrer le nom du modele dans la session

$_SESSION['modeleSite'] = $ChoixModeleSite;
session_register('modeleSite');
sleep(5);

// Afficher le lien vers le mod�le 
 ?>
         	<!-- debut nav bas  -->
                  </table></td>
              </tr>
              <tr> 
                <td><img src="../images/dot.gif" width="500" height="10"></td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td bgcolor="#003366"><img src="../images/dot.gif" width="2" height="1"></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td><table width="100%" border="0" cellspacing="0" cellpadding="3">
                    <tr> 
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="1">
                          <tr> 
                            <td bgcolor="#999999"><table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
                                <tr> 
                                  <td align="center" bgcolor="#999999"><?php echo "<a href='../modeles/images/".$_POST['ChoixModeleSite'].".jpg' target='_blank' class='edf8'>" ?> <font color="#FFFFFF">Voir le mod&egrave;le du site</font></a></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table></td>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="1">
                          <tr> 
                            <td bgcolor="#999999"> <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
                                <tr> 
                                  <td align="center" bgcolor="#999999"><a href="creerBase.php" class="edf8"><font color="#FFFFFF">Etape suivante &gt;&gt;&gt;</font></a></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
<?php 
}
?>
              <!-- fin nav bas -->
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
