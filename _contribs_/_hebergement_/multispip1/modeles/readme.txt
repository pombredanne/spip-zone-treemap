Les sites spip contenant les modeles (squelettes) sont ici avec leurs bases (dumps).

Si vous souhaitez modifier les mod�les, modifier l'aspect de sites tels qu'ils s'installent (plus ou moins d'articles par defaut...) ou mettre une autre version de Spip par exemple, il faut alors installer ces 2 spips (donc installer les dumps et modifier les fichiers ecrire/inc-config.php3 avec les coordonnees de votre base).

N'oubliez pas de refaire des dumps au cas ou vous auriez modifi� le contenu ou la structure (changement de version de spip par ex) de la base.

Starcrouz dec 2004