# phpMyAdmin SQL Dump
# version 2.5.3
# http://www.phpmyadmin.net
#
# Host: localhost
# Generation Time: Jul 20, 2004 at 04:11 PM
# Server version: 4.0.15
# PHP Version: 4.3.3
# 
# Database : `mire_multispip`
# 

# --------------------------------------------------------

#
# Table structure for table `spip_articles`
#

CREATE TABLE `spip_articles` (
  `id_article` bigint(21) NOT NULL auto_increment,
  `surtitre` text NOT NULL,
  `titre` text NOT NULL,
  `soustitre` text NOT NULL,
  `id_rubrique` bigint(21) NOT NULL default '0',
  `descriptif` text NOT NULL,
  `chapo` mediumtext NOT NULL,
  `texte` longblob NOT NULL,
  `ps` mediumtext NOT NULL,
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `statut` varchar(10) NOT NULL default '0',
  `id_secteur` bigint(21) NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  `export` varchar(10) default 'oui',
  `date_redac` datetime NOT NULL default '0000-00-00 00:00:00',
  `visites` int(11) NOT NULL default '0',
  `referers` int(11) NOT NULL default '0',
  `popularite` double(16,4) NOT NULL default '0.0000',
  `accepter_forum` char(3) NOT NULL default '',
  `auteur_modif` bigint(21) NOT NULL default '0',
  `date_modif` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id_article`),
  KEY `id_rubrique` (`id_rubrique`),
  KEY `id_secteur` (`id_secteur`),
  KEY `statut` (`statut`,`date`)
) TYPE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=1 ;

#
# Dumping data for table `spip_articles`
#


# --------------------------------------------------------

#
# Table structure for table `spip_auteurs`
#

CREATE TABLE `spip_auteurs` (
  `id_auteur` bigint(21) NOT NULL auto_increment,
  `nom` text NOT NULL,
  `bio` text NOT NULL,
  `email` tinytext NOT NULL,
  `nom_site` tinytext NOT NULL,
  `url_site` text NOT NULL,
  `login` varchar(255) binary NOT NULL default '',
  `pass` tinytext NOT NULL,
  `statut` varchar(255) NOT NULL default '',
  `maj` timestamp(14) NOT NULL,
  `pgp` blob NOT NULL,
  `htpass` tinyblob NOT NULL,
  `en_ligne` datetime NOT NULL default '0000-00-00 00:00:00',
  `imessage` char(3) NOT NULL default '',
  `messagerie` char(3) NOT NULL default '',
  `alea_actuel` tinytext NOT NULL,
  `alea_futur` tinytext NOT NULL,
  `prefs` tinytext NOT NULL,
  `cookie_oubli` tinytext NOT NULL,
  `source` varchar(10) NOT NULL default 'spip',
  `lang` varchar(10) NOT NULL default '',
  PRIMARY KEY  (`id_auteur`),
  KEY `login` (`login`),
  KEY `statut` (`statut`),
  KEY `en_ligne` (`en_ligne`)
) TYPE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=2 ;

#
# Dumping data for table `spip_auteurs`
#

INSERT INTO `spip_auteurs` VALUES (1, 'admin_multispip', '', 'stephane.rouilly@edf.fr', '', '', 'admin_multispip', '5c9d61d3e84cd66c693d4cced51fe241', '0minirezo', 20040720101739, '', 0x2431414b7465712e2e65646363, '2004-07-20 10:17:39', '', '', '18254200773fe80f96a381b0.75349783', '109368688140fcd45094c0e2.92687896', 'a:3:{s:7:"couleur";d:6;s:7:"display";d:2;s:7:"options";s:8:"avancees";}', '', 'spip', 'fr');

# --------------------------------------------------------

#
# Table structure for table `spip_auteurs_articles`
#

CREATE TABLE `spip_auteurs_articles` (
  `id_auteur` bigint(21) NOT NULL default '0',
  `id_article` bigint(21) NOT NULL default '0',
  KEY `id_auteur` (`id_auteur`),
  KEY `id_article` (`id_article`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_auteurs_articles`
#


# --------------------------------------------------------

#
# Table structure for table `spip_auteurs_messages`
#

CREATE TABLE `spip_auteurs_messages` (
  `id_auteur` bigint(21) NOT NULL default '0',
  `id_message` bigint(21) NOT NULL default '0',
  `vu` char(3) NOT NULL default '',
  KEY `id_auteur` (`id_auteur`),
  KEY `id_message` (`id_message`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_auteurs_messages`
#


# --------------------------------------------------------

#
# Table structure for table `spip_auteurs_rubriques`
#

CREATE TABLE `spip_auteurs_rubriques` (
  `id_auteur` bigint(21) NOT NULL default '0',
  `id_rubrique` bigint(21) NOT NULL default '0',
  KEY `id_auteur` (`id_auteur`),
  KEY `id_rubrique` (`id_rubrique`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_auteurs_rubriques`
#


# --------------------------------------------------------

#
# Table structure for table `spip_breves`
#

CREATE TABLE `spip_breves` (
  `id_breve` bigint(21) NOT NULL auto_increment,
  `date_heure` datetime NOT NULL default '0000-00-00 00:00:00',
  `titre` text NOT NULL,
  `texte` longblob NOT NULL,
  `lien_titre` text NOT NULL,
  `lien_url` text NOT NULL,
  `statut` varchar(6) NOT NULL default '',
  `id_rubrique` bigint(21) NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`id_breve`),
  KEY `id_rubrique` (`id_rubrique`)
) TYPE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=1 ;

#
# Dumping data for table `spip_breves`
#


# --------------------------------------------------------

#
# Table structure for table `spip_documents`
#

CREATE TABLE `spip_documents` (
  `id_document` bigint(21) NOT NULL auto_increment,
  `id_vignette` bigint(21) NOT NULL default '0',
  `id_type` bigint(21) NOT NULL default '0',
  `titre` text NOT NULL,
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `descriptif` text NOT NULL,
  `fichier` varchar(255) NOT NULL default '',
  `taille` int(11) NOT NULL default '0',
  `largeur` int(11) NOT NULL default '0',
  `hauteur` int(11) NOT NULL default '0',
  `mode` enum('vignette','document') NOT NULL default 'vignette',
  `inclus` char(3) default 'non',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`id_document`),
  KEY `id_vignette` (`id_vignette`),
  KEY `mode` (`mode`),
  KEY `id_type` (`id_type`)
) TYPE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=1 ;

#
# Dumping data for table `spip_documents`
#


# --------------------------------------------------------

#
# Table structure for table `spip_documents_articles`
#

CREATE TABLE `spip_documents_articles` (
  `id_document` bigint(21) NOT NULL default '0',
  `id_article` bigint(21) NOT NULL default '0',
  KEY `id_document` (`id_document`),
  KEY `id_article` (`id_article`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_documents_articles`
#


# --------------------------------------------------------

#
# Table structure for table `spip_documents_breves`
#

CREATE TABLE `spip_documents_breves` (
  `id_document` bigint(21) NOT NULL default '0',
  `id_breve` bigint(21) NOT NULL default '0',
  KEY `id_document` (`id_document`),
  KEY `id_breve` (`id_breve`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_documents_breves`
#


# --------------------------------------------------------

#
# Table structure for table `spip_documents_rubriques`
#

CREATE TABLE `spip_documents_rubriques` (
  `id_document` bigint(21) NOT NULL default '0',
  `id_rubrique` bigint(21) NOT NULL default '0',
  KEY `id_document` (`id_document`),
  KEY `id_rubrique` (`id_rubrique`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_documents_rubriques`
#


# --------------------------------------------------------

#
# Table structure for table `spip_forum`
#

CREATE TABLE `spip_forum` (
  `id_forum` bigint(21) NOT NULL auto_increment,
  `id_parent` bigint(21) NOT NULL default '0',
  `id_rubrique` bigint(21) NOT NULL default '0',
  `id_article` bigint(21) NOT NULL default '0',
  `id_breve` bigint(21) NOT NULL default '0',
  `date_heure` datetime NOT NULL default '0000-00-00 00:00:00',
  `titre` text NOT NULL,
  `texte` mediumtext NOT NULL,
  `auteur` text NOT NULL,
  `email_auteur` text NOT NULL,
  `nom_site` text NOT NULL,
  `url_site` text NOT NULL,
  `statut` varchar(8) NOT NULL default '',
  `ip` varchar(16) default NULL,
  `maj` timestamp(14) NOT NULL,
  `id_auteur` bigint(20) NOT NULL default '0',
  `id_message` bigint(21) NOT NULL default '0',
  `id_syndic` bigint(21) NOT NULL default '0',
  PRIMARY KEY  (`id_forum`),
  KEY `id_parent` (`id_parent`),
  KEY `id_rubrique` (`id_rubrique`),
  KEY `id_article` (`id_article`),
  KEY `id_breve` (`id_breve`),
  KEY `id_message` (`id_message`),
  KEY `id_syndic` (`id_syndic`),
  KEY `statut` (`statut`,`date_heure`)
) TYPE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=1 ;

#
# Dumping data for table `spip_forum`
#


# --------------------------------------------------------

#
# Table structure for table `spip_forum_cache`
#

CREATE TABLE `spip_forum_cache` (
  `id_forum` bigint(21) NOT NULL default '0',
  `id_rubrique` bigint(21) NOT NULL default '0',
  `id_article` bigint(21) NOT NULL default '0',
  `id_breve` bigint(21) NOT NULL default '0',
  `id_syndic` bigint(21) NOT NULL default '0',
  `fichier` char(150) binary NOT NULL default '',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`fichier`,`id_forum`,`id_article`,`id_rubrique`,`id_breve`,`id_syndic`),
  KEY `id_forum` (`id_forum`),
  KEY `id_rubrique` (`id_rubrique`),
  KEY `id_article` (`id_article`),
  KEY `id_syndic` (`id_syndic`),
  KEY `id_breve` (`id_breve`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_forum_cache`
#


# --------------------------------------------------------

#
# Table structure for table `spip_groupes_mots`
#

CREATE TABLE `spip_groupes_mots` (
  `id_groupe` bigint(21) NOT NULL auto_increment,
  `titre` text NOT NULL,
  `unseul` char(3) NOT NULL default '',
  `obligatoire` char(3) NOT NULL default '',
  `articles` char(3) NOT NULL default '',
  `breves` char(3) NOT NULL default '',
  `rubriques` char(3) NOT NULL default '',
  `syndic` char(3) NOT NULL default '',
  `0minirezo` char(3) NOT NULL default '',
  `1comite` char(3) NOT NULL default '',
  `6forum` char(3) NOT NULL default '',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`id_groupe`)
) TYPE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=1 ;

#
# Dumping data for table `spip_groupes_mots`
#


# --------------------------------------------------------

#
# Table structure for table `spip_index_articles`
#

CREATE TABLE `spip_index_articles` (
  `hash` bigint(20) unsigned NOT NULL default '0',
  `points` int(10) unsigned NOT NULL default '0',
  `id_article` int(10) unsigned NOT NULL default '0',
  KEY `hash` (`hash`),
  KEY `id_article` (`id_article`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_index_articles`
#


# --------------------------------------------------------

#
# Table structure for table `spip_index_auteurs`
#

CREATE TABLE `spip_index_auteurs` (
  `hash` bigint(20) unsigned NOT NULL default '0',
  `points` int(10) unsigned NOT NULL default '0',
  `id_auteur` int(10) unsigned NOT NULL default '0',
  KEY `hash` (`hash`),
  KEY `id_auteur` (`id_auteur`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_index_auteurs`
#


# --------------------------------------------------------

#
# Table structure for table `spip_index_breves`
#

CREATE TABLE `spip_index_breves` (
  `hash` bigint(20) unsigned NOT NULL default '0',
  `points` int(10) unsigned NOT NULL default '0',
  `id_breve` int(10) unsigned NOT NULL default '0',
  KEY `hash` (`hash`),
  KEY `id_breve` (`id_breve`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_index_breves`
#


# --------------------------------------------------------

#
# Table structure for table `spip_index_dico`
#

CREATE TABLE `spip_index_dico` (
  `hash` bigint(20) unsigned NOT NULL default '0',
  `dico` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`dico`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_index_dico`
#


# --------------------------------------------------------

#
# Table structure for table `spip_index_mots`
#

CREATE TABLE `spip_index_mots` (
  `hash` bigint(20) unsigned NOT NULL default '0',
  `points` int(10) unsigned NOT NULL default '0',
  `id_mot` int(10) unsigned NOT NULL default '0',
  KEY `hash` (`hash`),
  KEY `id_mot` (`id_mot`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_index_mots`
#


# --------------------------------------------------------

#
# Table structure for table `spip_index_rubriques`
#

CREATE TABLE `spip_index_rubriques` (
  `hash` bigint(20) unsigned NOT NULL default '0',
  `points` int(10) unsigned NOT NULL default '0',
  `id_rubrique` int(10) unsigned NOT NULL default '0',
  KEY `hash` (`hash`),
  KEY `id_rubrique` (`id_rubrique`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_index_rubriques`
#


# --------------------------------------------------------

#
# Table structure for table `spip_index_syndic`
#

CREATE TABLE `spip_index_syndic` (
  `hash` bigint(20) unsigned NOT NULL default '0',
  `points` int(10) unsigned NOT NULL default '0',
  `id_syndic` int(10) unsigned NOT NULL default '0',
  KEY `hash` (`hash`),
  KEY `id_syndic` (`id_syndic`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_index_syndic`
#


# --------------------------------------------------------

#
# Table structure for table `spip_messages`
#

CREATE TABLE `spip_messages` (
  `id_message` bigint(21) NOT NULL auto_increment,
  `titre` text NOT NULL,
  `texte` longblob NOT NULL,
  `type` varchar(6) NOT NULL default '',
  `date_heure` datetime NOT NULL default '0000-00-00 00:00:00',
  `rv` char(3) NOT NULL default '',
  `statut` varchar(6) NOT NULL default '',
  `id_auteur` bigint(21) NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`id_message`),
  KEY `id_auteur` (`id_auteur`)
) TYPE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=1 ;

#
# Dumping data for table `spip_messages`
#


# --------------------------------------------------------

#
# Table structure for table `spip_meta`
#

CREATE TABLE `spip_meta` (
  `nom` varchar(255) NOT NULL default '',
  `valeur` varchar(255) default '',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`nom`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_meta`
#

INSERT INTO `spip_meta` VALUES ('version_installee', '1.604', 20031128145910);
INSERT INTO `spip_meta` VALUES ('email_webmaster', 'admin@admin.com', 20040720101542);
INSERT INTO `spip_meta` VALUES ('activer_breves', 'non', 20040720101542);
INSERT INTO `spip_meta` VALUES ('config_precise_groupes', 'non', 20031202135429);
INSERT INTO `spip_meta` VALUES ('mots_cles_forums', 'non', 20031202135429);
INSERT INTO `spip_meta` VALUES ('articles_surtitre', 'oui', 20040720101542);
INSERT INTO `spip_meta` VALUES ('articles_soustitre', 'oui', 20040720101542);
INSERT INTO `spip_meta` VALUES ('articles_descriptif', 'oui', 20040720101542);
INSERT INTO `spip_meta` VALUES ('articles_chapeau', 'oui', 20040720101542);
INSERT INTO `spip_meta` VALUES ('articles_ps', 'oui', 20040720101542);
INSERT INTO `spip_meta` VALUES ('articles_redac', 'non', 20040720101542);
INSERT INTO `spip_meta` VALUES ('articles_mots', 'non', 20040720101542);
INSERT INTO `spip_meta` VALUES ('post_dates', 'oui', 20040720101542);
INSERT INTO `spip_meta` VALUES ('creer_preview', 'non', 20031202135623);
INSERT INTO `spip_meta` VALUES ('taille_preview', '150', 20031128150039);
INSERT INTO `spip_meta` VALUES ('articles_modif', 'oui', 20031202135623);
INSERT INTO `spip_meta` VALUES ('activer_sites', 'oui', 20040720101542);
INSERT INTO `spip_meta` VALUES ('proposer_sites', '0', 20040720101616);
INSERT INTO `spip_meta` VALUES ('activer_syndic', 'oui', 20040720101542);
INSERT INTO `spip_meta` VALUES ('visiter_sites', 'non', 20031128150039);
INSERT INTO `spip_meta` VALUES ('moderation_sites', 'non', 20040720101542);
INSERT INTO `spip_meta` VALUES ('forums_publics', 'posteriori', 20031203172343);
INSERT INTO `spip_meta` VALUES ('accepter_inscriptions', 'non', 20031203172343);
INSERT INTO `spip_meta` VALUES ('prevenir_auteurs', 'non', 20031203172343);
INSERT INTO `spip_meta` VALUES ('activer_messagerie', 'non', 20031203172343);
INSERT INTO `spip_meta` VALUES ('activer_imessage', 'oui', 20031128150039);
INSERT INTO `spip_meta` VALUES ('suivi_edito', 'non', 20031203172343);
INSERT INTO `spip_meta` VALUES ('quoi_de_neuf', 'non', 20031203172343);
INSERT INTO `spip_meta` VALUES ('forum_prive_admin', 'non', 20031203172343);
INSERT INTO `spip_meta` VALUES ('activer_moteur', 'non', 20031202135623);
INSERT INTO `spip_meta` VALUES ('activer_statistiques', 'oui', 20040720101542);
INSERT INTO `spip_meta` VALUES ('activer_statistiques_ref', 'non', 20031202135623);
INSERT INTO `spip_meta` VALUES ('documents_article', 'oui', 20040720101542);
INSERT INTO `spip_meta` VALUES ('documents_rubrique', 'non', 20040720101542);
INSERT INTO `spip_meta` VALUES ('charset', 'iso-8859-1', 20031202135623);
INSERT INTO `spip_meta` VALUES ('creer_htpasswd', 'non', 20031128150039);
INSERT INTO `spip_meta` VALUES ('langue_site', 'fr', 20031202135623);
INSERT INTO `spip_meta` VALUES ('date_purge_cache', '1090310238', 20040720095718);
INSERT INTO `spip_meta` VALUES ('date_statistiques', '2004-07-20', 20040720095719);
INSERT INTO `spip_meta` VALUES ('date_stats_popularite', '1090331991', 20040720155951);
INSERT INTO `spip_meta` VALUES ('popularite_max', '', 20040720155952);
INSERT INTO `spip_meta` VALUES ('popularite_total', '', 20040720155952);
INSERT INTO `spip_meta` VALUES ('date_stats_referers', '1090331992', 20040720155952);
INSERT INTO `spip_meta` VALUES ('calculer_referers_now', 'non', 20040720160002);
INSERT INTO `spip_meta` VALUES ('langues_proposees', 'ar,cpf,da,de,en,eo,es,fr,gl,it,vi', 20040720101739);
INSERT INTO `spip_meta` VALUES ('nom_site', 'L\'actualit� des sites multispip', 20040720101542);
INSERT INTO `spip_meta` VALUES ('adresse_site', 'http://icame/sites/mire_multispip', 20040720101542);
INSERT INTO `spip_meta` VALUES ('alea_ephemere_ancien', '8989714f9ee58a39ebca5bef13c21023', 20040720101404);
INSERT INTO `spip_meta` VALUES ('alea_ephemere', 'b95aa1d13d76d188f09434708b4685ef', 20040720101404);
INSERT INTO `spip_meta` VALUES ('date_optimisation', '1072172962', 20031223104922);
INSERT INTO `spip_meta` VALUES ('gd_formats', 'jpg,png', 20031202135517);
INSERT INTO `spip_meta` VALUES ('http_proxy', '', 20031202135623);
INSERT INTO `spip_meta` VALUES ('adresse_neuf', '', 20031203172343);
INSERT INTO `spip_meta` VALUES ('jours_neuf', '', 20031203172343);

# --------------------------------------------------------

#
# Table structure for table `spip_mots`
#

CREATE TABLE `spip_mots` (
  `id_mot` bigint(21) NOT NULL auto_increment,
  `type` varchar(100) NOT NULL default '',
  `titre` text NOT NULL,
  `descriptif` text NOT NULL,
  `texte` longblob NOT NULL,
  `id_groupe` bigint(21) NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`id_mot`),
  KEY `type` (`type`)
) TYPE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=1 ;

#
# Dumping data for table `spip_mots`
#


# --------------------------------------------------------

#
# Table structure for table `spip_mots_articles`
#

CREATE TABLE `spip_mots_articles` (
  `id_mot` bigint(21) NOT NULL default '0',
  `id_article` bigint(21) NOT NULL default '0',
  KEY `id_mot` (`id_mot`),
  KEY `id_article` (`id_article`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_mots_articles`
#


# --------------------------------------------------------

#
# Table structure for table `spip_mots_breves`
#

CREATE TABLE `spip_mots_breves` (
  `id_mot` bigint(21) NOT NULL default '0',
  `id_breve` bigint(21) NOT NULL default '0',
  KEY `id_mot` (`id_mot`),
  KEY `id_breve` (`id_breve`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_mots_breves`
#


# --------------------------------------------------------

#
# Table structure for table `spip_mots_forum`
#

CREATE TABLE `spip_mots_forum` (
  `id_mot` bigint(21) NOT NULL default '0',
  `id_forum` bigint(21) NOT NULL default '0',
  KEY `id_mot` (`id_mot`),
  KEY `id_forum` (`id_forum`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_mots_forum`
#


# --------------------------------------------------------

#
# Table structure for table `spip_mots_rubriques`
#

CREATE TABLE `spip_mots_rubriques` (
  `id_mot` bigint(21) NOT NULL default '0',
  `id_rubrique` bigint(21) NOT NULL default '0',
  KEY `id_mot` (`id_mot`),
  KEY `id_rubrique` (`id_rubrique`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_mots_rubriques`
#


# --------------------------------------------------------

#
# Table structure for table `spip_mots_syndic`
#

CREATE TABLE `spip_mots_syndic` (
  `id_mot` bigint(21) NOT NULL default '0',
  `id_syndic` bigint(21) NOT NULL default '0',
  KEY `id_mot` (`id_mot`),
  KEY `id_syndic` (`id_syndic`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_mots_syndic`
#


# --------------------------------------------------------

#
# Table structure for table `spip_petitions`
#

CREATE TABLE `spip_petitions` (
  `id_article` bigint(21) NOT NULL default '0',
  `email_unique` char(3) NOT NULL default '',
  `site_obli` char(3) NOT NULL default '',
  `site_unique` char(3) NOT NULL default '',
  `message` char(3) NOT NULL default '',
  `texte` longblob NOT NULL,
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`id_article`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_petitions`
#


# --------------------------------------------------------

#
# Table structure for table `spip_referers`
#

CREATE TABLE `spip_referers` (
  `referer_md5` bigint(20) unsigned NOT NULL default '0',
  `date` date NOT NULL default '0000-00-00',
  `referer` varchar(255) NOT NULL default '',
  `visites` int(10) unsigned NOT NULL default '0',
  `visites_jour` int(10) unsigned NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`referer_md5`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_referers`
#


# --------------------------------------------------------

#
# Table structure for table `spip_referers_articles`
#

CREATE TABLE `spip_referers_articles` (
  `id_article` int(10) unsigned NOT NULL default '0',
  `referer_md5` bigint(20) unsigned NOT NULL default '0',
  `date` date NOT NULL default '0000-00-00',
  `referer` varchar(255) NOT NULL default '',
  `visites` int(10) unsigned NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`id_article`,`referer_md5`),
  KEY `referer_md5` (`referer_md5`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_referers_articles`
#


# --------------------------------------------------------

#
# Table structure for table `spip_referers_temp`
#

CREATE TABLE `spip_referers_temp` (
  `ip` int(10) unsigned NOT NULL default '0',
  `referer` varchar(255) NOT NULL default '',
  `referer_md5` bigint(20) unsigned NOT NULL default '0',
  `type` enum('article','rubrique','breve','autre') NOT NULL default 'article',
  `id_objet` int(10) unsigned NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`type`,`id_objet`,`referer_md5`,`ip`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_referers_temp`
#


# --------------------------------------------------------

#
# Table structure for table `spip_rubriques`
#

CREATE TABLE `spip_rubriques` (
  `id_rubrique` bigint(21) NOT NULL auto_increment,
  `id_parent` bigint(21) NOT NULL default '0',
  `titre` text NOT NULL,
  `descriptif` text NOT NULL,
  `texte` longblob NOT NULL,
  `id_secteur` bigint(21) NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  `export` varchar(10) default 'oui',
  `id_import` bigint(20) default '0',
  `statut` varchar(10) NOT NULL default '',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id_rubrique`),
  KEY `id_parent` (`id_parent`)
) TYPE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=3 ;

#
# Dumping data for table `spip_rubriques`
#

INSERT INTO `spip_rubriques` VALUES (1, 0, 'Multispip', 'Rubrique contenant les sites r�f�renc�s cr��s par l\'entremise de Multispip. ', '', 1, 20031205170433, 'oui', 0, 'publie', '2003-12-05 13:58:18');
INSERT INTO `spip_rubriques` VALUES (2, 0, 'Autres sites', 'Rubrique contenant les sites qui n\'ont pas �t� cr�es avec Multispip.', '', 2, 20031205170433, 'oui', 0, 'publie', '2003-12-05 16:42:05');

# --------------------------------------------------------

#
# Table structure for table `spip_signatures`
#

CREATE TABLE `spip_signatures` (
  `id_signature` bigint(21) NOT NULL auto_increment,
  `id_article` bigint(21) NOT NULL default '0',
  `date_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `nom_email` text NOT NULL,
  `ad_email` text NOT NULL,
  `nom_site` text NOT NULL,
  `url_site` text NOT NULL,
  `message` mediumtext NOT NULL,
  `statut` varchar(10) NOT NULL default '',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`id_signature`),
  KEY `id_article` (`id_article`),
  KEY `statut` (`statut`)
) TYPE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=1 ;

#
# Dumping data for table `spip_signatures`
#


# --------------------------------------------------------

#
# Table structure for table `spip_syndic`
#

CREATE TABLE `spip_syndic` (
  `id_syndic` bigint(21) NOT NULL auto_increment,
  `id_rubrique` bigint(21) NOT NULL default '0',
  `id_secteur` bigint(21) NOT NULL default '0',
  `nom_site` blob NOT NULL,
  `url_site` blob NOT NULL,
  `url_syndic` blob NOT NULL,
  `descriptif` blob NOT NULL,
  `maj` timestamp(14) NOT NULL,
  `syndication` char(3) NOT NULL default '',
  `statut` varchar(10) NOT NULL default '',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `date_syndic` datetime NOT NULL default '0000-00-00 00:00:00',
  `date_index` datetime NOT NULL default '0000-00-00 00:00:00',
  `moderation` char(3) NOT NULL default '',
  PRIMARY KEY  (`id_syndic`),
  KEY `id_rubrique` (`id_rubrique`),
  KEY `id_secteur` (`id_secteur`),
  KEY `statut` (`statut`,`date_syndic`)
) TYPE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=3 ;

#
# Dumping data for table `spip_syndic`
#


# --------------------------------------------------------

#
# Table structure for table `spip_syndic_articles`
#

CREATE TABLE `spip_syndic_articles` (
  `id_syndic_article` bigint(21) NOT NULL auto_increment,
  `id_syndic` bigint(21) NOT NULL default '0',
  `titre` text NOT NULL,
  `url` varchar(255) NOT NULL default '',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `lesauteurs` text NOT NULL,
  `maj` timestamp(14) NOT NULL,
  `statut` varchar(10) NOT NULL default '',
  `descriptif` blob NOT NULL,
  PRIMARY KEY  (`id_syndic_article`),
  KEY `id_syndic` (`id_syndic`),
  KEY `statut` (`statut`),
  KEY `url` (`url`)
) TYPE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=335 ;

#
# Dumping data for table `spip_syndic_articles`
#


# --------------------------------------------------------

#
# Table structure for table `spip_types_documents`
#

CREATE TABLE `spip_types_documents` (
  `id_type` bigint(21) NOT NULL auto_increment,
  `titre` text NOT NULL,
  `descriptif` text NOT NULL,
  `extension` varchar(10) NOT NULL default '',
  `mime_type` varchar(100) NOT NULL default '',
  `inclus` enum('non','image','embed') NOT NULL default 'non',
  `upload` enum('oui','non') NOT NULL default 'oui',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`id_type`),
  UNIQUE KEY `extension` (`extension`),
  KEY `inclus` (`inclus`)
) TYPE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=53 ;

#
# Dumping data for table `spip_types_documents`
#

INSERT INTO `spip_types_documents` VALUES (1, 'JPEG', '', 'jpg', '', 'image', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (2, 'PNG', '', 'png', '', 'image', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (3, 'GIF', '', 'gif', '', 'image', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (4, 'BMP', '', 'bmp', '', 'image', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (5, 'Photoshop', '', 'psd', '', 'image', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (6, 'TIFF', '', 'tif', '', 'image', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (7, 'AIFF', '', 'aiff', '', 'embed', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (8, 'Windows Media', '', 'asf', '', 'embed', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (9, 'Windows Media', '', 'avi', '', 'embed', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (10, 'Midi', '', 'mid', '', 'embed', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (11, 'MNG', '', 'mng', '', 'embed', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (12, 'QuickTime', '', 'mov', '', 'embed', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (13, 'MP3', '', 'mp3', '', 'embed', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (14, 'MPEG', '', 'mpg', '', 'embed', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (15, 'Ogg', '', 'ogg', '', 'embed', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (16, 'QuickTime', '', 'qt', '', 'embed', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (17, 'RealAudio', '', 'ra', '', 'embed', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (18, 'RealAudio', '', 'ram', '', 'embed', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (19, 'RealAudio', '', 'rm', '', 'embed', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (20, 'Flash', '', 'swf', '', 'embed', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (21, 'WAV', '', 'wav', '', 'embed', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (22, 'Windows Media', '', 'wmv', '', 'embed', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (23, 'Adobe Illustrator', '', 'ai', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (24, 'BZip', '', 'bz2', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (25, 'C source', '', 'c', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (26, 'Debian', '', 'deb', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (27, 'Word', '', 'doc', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (28, 'DjVu', '', 'djvu', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (29, 'LaTeX DVI', '', 'dvi', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (30, 'PostScript', '', 'eps', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (31, 'GZ', '', 'gz', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (32, 'C header', '', 'h', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (33, 'HTML', '', 'html', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (34, 'Pascal', '', 'pas', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (35, 'PDF', '', 'pdf', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (36, 'PowerPoint', '', 'ppt', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (37, 'PostScript', '', 'ps', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (38, 'RedHat/Mandrake/SuSE', '', 'rpm', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (39, 'RTF', '', 'rtf', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (40, 'StarOffice', '', 'sdd', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (41, 'StarOffice', '', 'sdw', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (42, 'Stuffit', '', 'sit', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (43, 'OpenOffice Calc', '', 'sxc', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (44, 'OpenOffice Impress', '', 'sxi', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (45, 'OpenOffice', '', 'sxw', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (46, 'LaTeX', '', 'tex', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (47, 'TGZ', '', 'tgz', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (48, 'texte', '', 'txt', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (49, 'GIMP multi-layer', '', 'xcf', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (50, 'Excel', '', 'xls', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (51, 'XML', '', 'xml', '', 'non', 'oui', 20031128145910);
INSERT INTO `spip_types_documents` VALUES (52, 'Zip', '', 'zip', '', 'non', 'oui', 20031128145910);

# --------------------------------------------------------

#
# Table structure for table `spip_visites`
#

CREATE TABLE `spip_visites` (
  `date` date NOT NULL default '0000-00-00',
  `visites` int(10) unsigned NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`date`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_visites`
#


# --------------------------------------------------------

#
# Table structure for table `spip_visites_articles`
#

CREATE TABLE `spip_visites_articles` (
  `date` date NOT NULL default '0000-00-00',
  `id_article` int(10) unsigned NOT NULL default '0',
  `visites` int(10) unsigned NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`date`,`id_article`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_visites_articles`
#


# --------------------------------------------------------

#
# Table structure for table `spip_visites_temp`
#

CREATE TABLE `spip_visites_temp` (
  `ip` int(10) unsigned NOT NULL default '0',
  `type` enum('article','rubrique','breve','autre') NOT NULL default 'article',
  `id_objet` int(10) unsigned NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`type`,`id_objet`,`ip`)
) TYPE=MyISAM PACK_KEYS=1;

#
# Dumping data for table `spip_visites_temp`
#

INSERT INTO `spip_visites_temp` VALUES (2187469846, 'rubrique', 1, 20040720120147);
