-- MySQL dump 9.09
--
-- Host: sedna.neofutur.net    Database: biospip4
---------------------------------------------------------
-- Server version	4.0.15'-Max'-log

--
-- Table structure for table `spip_ajax_fonc`
--

CREATE TABLE `spip_ajax_fonc` (
  `id_ajax_fonc` bigint(21) NOT NULL auto_increment,
  `id_auteur` bigint(21) NOT NULL default '0',
  `variables` text NOT NULL,
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `hash` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id_ajax_fonc`),
  KEY `hash` (`hash`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_ajax_fonc`
--


--
-- Table structure for table `spip_articles`
--

CREATE TABLE `spip_articles` (
  `id_article` bigint(21) NOT NULL auto_increment,
  `surtitre` text NOT NULL,
  `titre` text NOT NULL,
  `soustitre` text NOT NULL,
  `id_rubrique` bigint(21) NOT NULL default '0',
  `descriptif` text NOT NULL,
  `chapo` mediumtext NOT NULL,
  `texte` longblob NOT NULL,
  `ps` mediumtext NOT NULL,
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `statut` varchar(10) NOT NULL default '0',
  `id_secteur` bigint(21) NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  `export` varchar(10) default 'oui',
  `date_redac` datetime NOT NULL default '0000-00-00 00:00:00',
  `visites` int(11) NOT NULL default '0',
  `referers` int(11) NOT NULL default '0',
  `popularite` double NOT NULL default '0',
  `accepter_forum` char(3) NOT NULL default '',
  `auteur_modif` bigint(21) NOT NULL default '0',
  `date_modif` datetime NOT NULL default '0000-00-00 00:00:00',
  `lang` varchar(10) NOT NULL default '',
  `langue_choisie` char(3) default 'non',
  `id_trad` bigint(21) NOT NULL default '0',
  `extra` longblob,
  `idx` enum('','1','non','oui','idx') NOT NULL default '',
  `id_version` int(10) unsigned NOT NULL default '0',
  `nom_site` tinytext NOT NULL,
  `url_site` varchar(255) NOT NULL default '',
  `url_propre` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id_article`),
  KEY `id_rubrique` (`id_rubrique`),
  KEY `id_secteur` (`id_secteur`),
  KEY `id_trad` (`id_trad`),
  KEY `lang` (`lang`),
  KEY `statut` (`statut`,`date`),
  KEY `url_site` (`url_site`),
  KEY `date_modif` (`date_modif`),
  KEY `idx` (`idx`),
  KEY `url_propre` (`url_propre`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_articles`
--

INSERT INTO `spip_articles` VALUES (1,'test 2','article test 2','test 2',3,'article test 2','article test 2','article test 2','','2005-12-12 00:41:11','publie',3,20051212004310,'oui','0000-00-00 00:00:00',0,0,0,'pos',1,'2005-12-12 00:40:55','fr','non',0,NULL,'oui',0,'','','');
INSERT INTO `spip_articles` VALUES (2,'2bis','Article test 2bis','2bis',3,'Article test 2bis','Article test 2bis','Article test 2bis','Article test 2bis','2005-12-12 00:43:05','publie',3,20051212004329,'oui','0000-00-00 00:00:00',1,0,0.666066637500768,'pos',1,'2005-12-12 00:42:24','fr','non',0,NULL,'oui',0,'','','');
INSERT INTO `spip_articles` VALUES (3,'test 1','Article test 1','test 1',1,'Article test 1','Article test 1','Article test 1','Article test 1','2005-12-12 00:44:10','publie',1,20051212004513,'oui','0000-00-00 00:00:00',1,0,0.666066637500768,'pos',1,'2005-12-12 00:43:57','fr','non',0,NULL,'oui',0,'','','');
INSERT INTO `spip_articles` VALUES (4,'test 1.1','Article test 1.1','test 1.1',2,'Article test 1.1','Article test 1.1','Article test 1.1','Article test 1.1','2005-12-12 00:45:20','publie',1,20051212004524,'oui','0000-00-00 00:00:00',4,0,2.66426655000307,'pos',1,'2005-12-12 00:45:08','fr','non',0,NULL,'oui',0,'','','');

--
-- Table structure for table `spip_auteurs`
--

CREATE TABLE `spip_auteurs` (
  `id_auteur` bigint(21) NOT NULL auto_increment,
  `nom` text NOT NULL,
  `bio` text NOT NULL,
  `email` tinytext NOT NULL,
  `nom_site` tinytext NOT NULL,
  `url_site` text NOT NULL,
  `login` varchar(255) binary NOT NULL default '',
  `pass` tinytext NOT NULL,
  `low_sec` tinytext NOT NULL,
  `statut` varchar(255) NOT NULL default '',
  `maj` timestamp(14) NOT NULL,
  `pgp` blob NOT NULL,
  `htpass` tinyblob NOT NULL,
  `en_ligne` datetime NOT NULL default '0000-00-00 00:00:00',
  `imessage` char(3) NOT NULL default '',
  `messagerie` char(3) NOT NULL default '',
  `alea_actuel` tinytext NOT NULL,
  `alea_futur` tinytext NOT NULL,
  `prefs` tinytext NOT NULL,
  `cookie_oubli` tinytext NOT NULL,
  `source` varchar(10) NOT NULL default 'spip',
  `lang` varchar(10) NOT NULL default '',
  `idx` enum('','1','non','oui','idx') NOT NULL default '',
  `url_propre` varchar(255) NOT NULL default '',
  `extra` longblob,
  PRIMARY KEY  (`id_auteur`),
  KEY `login` (`login`),
  KEY `statut` (`statut`),
  KEY `lang` (`lang`),
  KEY `idx` (`idx`),
  KEY `en_ligne` (`en_ligne`),
  KEY `url_propre` (`url_propre`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_auteurs`
--

INSERT INTO `spip_auteurs` VALUES (1,'neofutur','','spiptest@waisse.org','','','neofutur','d8fb0171bf95179a8c1bd1279b3bb784','','0minirezo',20051212145138,'','$1$HM5pq56a$nVuAe63SUwnLO36twstUI0','2005-12-12 14:51:38','','','2113424005439d7e465f54c2.22412231','1177939555439d8054deff29.84416510','a:4:{s:3:\"cnx\";s:5:\"perma\";s:7:\"couleur\";d:6;s:7:\"display\";d:2;s:7:\"options\";s:8:\"avancees\";}','','spip','','oui','',NULL);

--
-- Table structure for table `spip_auteurs_articles`
--

CREATE TABLE `spip_auteurs_articles` (
  `id_auteur` bigint(21) NOT NULL default '0',
  `id_article` bigint(21) NOT NULL default '0',
  KEY `id_auteur` (`id_auteur`),
  KEY `id_article` (`id_article`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_auteurs_articles`
--

INSERT INTO `spip_auteurs_articles` VALUES (1,1);
INSERT INTO `spip_auteurs_articles` VALUES (1,2);
INSERT INTO `spip_auteurs_articles` VALUES (1,3);
INSERT INTO `spip_auteurs_articles` VALUES (1,4);

--
-- Table structure for table `spip_auteurs_messages`
--

CREATE TABLE `spip_auteurs_messages` (
  `id_auteur` bigint(21) NOT NULL default '0',
  `id_message` bigint(21) NOT NULL default '0',
  `vu` char(3) NOT NULL default '',
  KEY `id_auteur` (`id_auteur`),
  KEY `id_message` (`id_message`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_auteurs_messages`
--


--
-- Table structure for table `spip_auteurs_rubriques`
--

CREATE TABLE `spip_auteurs_rubriques` (
  `id_auteur` bigint(21) NOT NULL default '0',
  `id_rubrique` bigint(21) NOT NULL default '0',
  KEY `id_auteur` (`id_auteur`),
  KEY `id_rubrique` (`id_rubrique`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_auteurs_rubriques`
--


--
-- Table structure for table `spip_breves`
--

CREATE TABLE `spip_breves` (
  `id_breve` bigint(21) NOT NULL auto_increment,
  `date_heure` datetime NOT NULL default '0000-00-00 00:00:00',
  `titre` text NOT NULL,
  `texte` longblob NOT NULL,
  `lien_titre` text NOT NULL,
  `lien_url` text NOT NULL,
  `statut` varchar(6) NOT NULL default '',
  `id_rubrique` bigint(21) NOT NULL default '0',
  `lang` varchar(10) NOT NULL default '',
  `langue_choisie` char(3) default 'non',
  `maj` timestamp(14) NOT NULL,
  `idx` enum('','1','non','oui','idx') NOT NULL default '',
  `extra` longblob,
  `url_propre` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id_breve`),
  KEY `idx` (`idx`),
  KEY `id_rubrique` (`id_rubrique`),
  KEY `url_propre` (`url_propre`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_breves`
--


--
-- Table structure for table `spip_caches`
--

CREATE TABLE `spip_caches` (
  `fichier` char(64) NOT NULL default '',
  `id` char(64) NOT NULL default '',
  `type` char(1) NOT NULL default 'i',
  `taille` int(11) NOT NULL default '0',
  PRIMARY KEY  (`fichier`,`id`),
  KEY `fichier` (`fichier`),
  KEY `id` (`id`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_caches`
--

INSERT INTO `spip_caches` VALUES ('CACHE/d/ch-log-log-ecrire%252F.f5f4b50a.gz','1134402139','t',467);
INSERT INTO `spip_caches` VALUES ('CACHE/3/325991%3B-option%3E%0A.77d91c62.gz','1134402138','t',1027);
INSERT INTO `spip_caches` VALUES ('CACHE/2/in%3Furl%3Decrire%252F.60eb9b51.gz','1134402138','t',898);
INSERT INTO `spip_caches` VALUES ('CACHE/b/BioSquelettes4-.3c36cf6f.gz','1134485043','t',326);
INSERT INTO `spip_caches` VALUES ('CACHE/6/BioSquelettes4-.f1bfe203.gz','1134405842','t',716);
INSERT INTO `spip_caches` VALUES ('CACHE/c/biospip4-sommaire.89eae099.gz','1134484110','t',326);
INSERT INTO `spip_caches` VALUES ('CACHE/5/biospip4-.1eed0707.gz','1134407968','t',716);
INSERT INTO `spip_caches` VALUES ('CACHE/7/biospip4-sommaire.da11d8c9.gz','1134404909','t',710);
INSERT INTO `spip_caches` VALUES ('CACHE/2/inc_meta-fr.7e2390ef.gz','1134487169','t',1055);
INSERT INTO `spip_caches` VALUES ('CACHE/9/inc_menu-fr.192360d4.gz','1134487169','t',264);
INSERT INTO `spip_caches` VALUES ('CACHE/8/inc_recherche_google-fr.88dc773e.gz','1134487169','t',564);
INSERT INTO `spip_caches` VALUES ('CACHE/1/biospip4-.957469f7.gz','1134487169','t',341);
INSERT INTO `spip_caches` VALUES ('CACHE/e/inc_footer-fr.3bc4b102.gz','1134487169','t',188);
INSERT INTO `spip_caches` VALUES ('CACHE/1/ticle%3Fid_article%3D4.8d93cfcb.gz','id_forum/a4','i',0);
INSERT INTO `spip_caches` VALUES ('CACHE/1/ticle%3Fid_article%3D4.8d93cfcb.gz','1134437653','t',705);
INSERT INTO `spip_caches` VALUES ('CACHE/b/inc_meta-4-fr.e545876a.gz','1134437654','t',1112);
INSERT INTO `spip_caches` VALUES ('CACHE/0/inc_menu-fr-4.a7888f8e.gz','1134437654','t',237);
INSERT INTO `spip_caches` VALUES ('CACHE/b/ticle%3Fid_article%3D4.e280d275.gz','1134437654','t',326);
INSERT INTO `spip_caches` VALUES ('CACHE/0/inc_footer-fr-4.bc3fec2b.gz','1134437654','t',334);
INSERT INTO `spip_caches` VALUES ('CACHE/a/ticle%3Fid_article%3D3.646b0be6.gz','id_forum/a3','i',0);
INSERT INTO `spip_caches` VALUES ('CACHE/a/ticle%3Fid_article%3D3.646b0be6.gz','1134434974','t',702);
INSERT INTO `spip_caches` VALUES ('CACHE/0/inc_meta-3-fr.f8f2b757.gz','1134434974','t',1113);
INSERT INTO `spip_caches` VALUES ('CACHE/0/inc_menu-fr-3.3c7af63c.gz','1134434974','t',237);
INSERT INTO `spip_caches` VALUES ('CACHE/5/ticle%3Fid_article%3D3.c734403f.gz','1134434974','t',326);
INSERT INTO `spip_caches` VALUES ('CACHE/f/inc_footer-fr-3.d52ffce6.gz','1134434974','t',334);
INSERT INTO `spip_caches` VALUES ('CACHE/b/inc_meta-fr-3.4b396ca1.gz','1134437668','t',1034);
INSERT INTO `spip_caches` VALUES ('CACHE/4/inc_menu-fr-3.38239240.gz','1134437668','t',237);
INSERT INTO `spip_caches` VALUES ('CACHE/8/ique%3Fid_rubrique%3D3.a6f7c96d.gz','1134437668','t',326);
INSERT INTO `spip_caches` VALUES ('CACHE/f/inc_footer-fr-3.b4f8d23c.gz','1134437668','t',188);
INSERT INTO `spip_caches` VALUES ('CACHE/0/inc_meta-fr-1.34d40b88.gz','1134465142','t',1033);
INSERT INTO `spip_caches` VALUES ('CACHE/6/inc_menu-fr-1.dc904a81.gz','1134465142','t',237);
INSERT INTO `spip_caches` VALUES ('CACHE/d/ique%3Fid_rubrique%3D1.1550a46a.gz','1134465143','t',326);
INSERT INTO `spip_caches` VALUES ('CACHE/a/inc_footer-fr-1.37336472.gz','1134465143','t',188);
INSERT INTO `spip_caches` VALUES ('CACHE/3/ticle%3Fid_article%3D2.c1b3dcab.gz','id_forum/a2','i',0);
INSERT INTO `spip_caches` VALUES ('CACHE/3/ticle%3Fid_article%3D2.c1b3dcab.gz','1134437678','t',706);
INSERT INTO `spip_caches` VALUES ('CACHE/7/inc_meta-2-fr.ec02bf4b.gz','1134437678','t',1115);
INSERT INTO `spip_caches` VALUES ('CACHE/0/inc_menu-fr-2.b06a2d27.gz','1134437678','t',237);
INSERT INTO `spip_caches` VALUES ('CACHE/5/ticle%3Fid_article%3D2.03a7f00a.gz','1134437678','t',326);
INSERT INTO `spip_caches` VALUES ('CACHE/2/inc_footer-fr-2.4eb070fd.gz','1134437678','t',334);
INSERT INTO `spip_caches` VALUES ('CACHE/f/inc_meta-fr-2.d7aa410a.gz','1134466220','t',1037);
INSERT INTO `spip_caches` VALUES ('CACHE/a/inc_menu-fr-2.1d8a9144.gz','1134466220','t',237);
INSERT INTO `spip_caches` VALUES ('CACHE/6/ique%3Fid_rubrique%3D2.e2c4ef3c.gz','1134466220','t',326);
INSERT INTO `spip_caches` VALUES ('CACHE/f/inc_footer-fr-2.37555143.gz','1134466220','t',188);
INSERT INTO `spip_caches` VALUES ('CACHE/0/in%3Furl%3Decrire%252F.8c020faa.gz','1134402667','t',939);
INSERT INTO `spip_caches` VALUES ('CACHE/7/325991%3B-option%3E%0A.fd8b01df.gz','1134402667','t',1026);

--
-- Table structure for table `spip_documents`
--

CREATE TABLE `spip_documents` (
  `id_document` bigint(21) NOT NULL auto_increment,
  `id_vignette` bigint(21) NOT NULL default '0',
  `id_type` bigint(21) NOT NULL default '0',
  `titre` text NOT NULL,
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `descriptif` text NOT NULL,
  `fichier` varchar(255) NOT NULL default '',
  `taille` int(11) NOT NULL default '0',
  `largeur` int(11) NOT NULL default '0',
  `hauteur` int(11) NOT NULL default '0',
  `mode` enum('vignette','document') NOT NULL default 'vignette',
  `inclus` char(3) default 'non',
  `distant` char(3) default 'non',
  `idx` enum('','1','non','oui','idx') NOT NULL default '',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`id_document`),
  KEY `id_vignette` (`id_vignette`),
  KEY `mode` (`mode`),
  KEY `id_type` (`id_type`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_documents`
--


--
-- Table structure for table `spip_documents_articles`
--

CREATE TABLE `spip_documents_articles` (
  `id_document` bigint(21) NOT NULL default '0',
  `id_article` bigint(21) NOT NULL default '0',
  KEY `id_document` (`id_document`),
  KEY `id_article` (`id_article`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_documents_articles`
--


--
-- Table structure for table `spip_documents_breves`
--

CREATE TABLE `spip_documents_breves` (
  `id_document` bigint(21) NOT NULL default '0',
  `id_breve` bigint(21) NOT NULL default '0',
  KEY `id_document` (`id_document`),
  KEY `id_breve` (`id_breve`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_documents_breves`
--


--
-- Table structure for table `spip_documents_rubriques`
--

CREATE TABLE `spip_documents_rubriques` (
  `id_document` bigint(21) NOT NULL default '0',
  `id_rubrique` bigint(21) NOT NULL default '0',
  KEY `id_document` (`id_document`),
  KEY `id_rubrique` (`id_rubrique`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_documents_rubriques`
--


--
-- Table structure for table `spip_documents_syndic`
--

CREATE TABLE `spip_documents_syndic` (
  `id_document` bigint(21) NOT NULL default '0',
  `id_syndic` bigint(21) NOT NULL default '0',
  `id_syndic_article` bigint(21) NOT NULL default '0',
  KEY `id_document` (`id_document`),
  KEY `id_syndic` (`id_syndic`),
  KEY `id_syndic_article` (`id_syndic_article`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_documents_syndic`
--


--
-- Table structure for table `spip_forum`
--

CREATE TABLE `spip_forum` (
  `id_forum` bigint(21) NOT NULL auto_increment,
  `id_parent` bigint(21) NOT NULL default '0',
  `id_thread` bigint(21) NOT NULL default '0',
  `id_rubrique` bigint(21) NOT NULL default '0',
  `id_article` bigint(21) NOT NULL default '0',
  `id_breve` bigint(21) NOT NULL default '0',
  `date_heure` datetime NOT NULL default '0000-00-00 00:00:00',
  `titre` text NOT NULL,
  `texte` mediumtext NOT NULL,
  `auteur` text NOT NULL,
  `email_auteur` text NOT NULL,
  `nom_site` text NOT NULL,
  `url_site` text NOT NULL,
  `statut` varchar(8) NOT NULL default '',
  `idx` enum('','1','non','oui','idx') NOT NULL default '',
  `ip` varchar(16) default NULL,
  `maj` timestamp(14) NOT NULL,
  `id_auteur` bigint(20) NOT NULL default '0',
  `id_message` bigint(21) NOT NULL default '0',
  `id_syndic` bigint(21) NOT NULL default '0',
  PRIMARY KEY  (`id_forum`),
  KEY `id_parent` (`id_parent`),
  KEY `id_rubrique` (`id_rubrique`),
  KEY `id_article` (`id_article`),
  KEY `id_breve` (`id_breve`),
  KEY `id_message` (`id_message`),
  KEY `id_syndic` (`id_syndic`),
  KEY `idx` (`idx`),
  KEY `statut` (`statut`,`date_heure`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_forum`
--


--
-- Table structure for table `spip_groupes_mots`
--

CREATE TABLE `spip_groupes_mots` (
  `id_groupe` bigint(21) NOT NULL auto_increment,
  `titre` text NOT NULL,
  `descriptif` text NOT NULL,
  `texte` longblob NOT NULL,
  `unseul` char(3) NOT NULL default '',
  `obligatoire` char(3) NOT NULL default '',
  `articles` char(3) NOT NULL default '',
  `breves` char(3) NOT NULL default '',
  `rubriques` char(3) NOT NULL default '',
  `syndic` char(3) NOT NULL default '',
  `minirezo` char(3) NOT NULL default '',
  `comite` char(3) NOT NULL default '',
  `forum` char(3) NOT NULL default '',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`id_groupe`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_groupes_mots`
--


--
-- Table structure for table `spip_index_articles`
--

CREATE TABLE `spip_index_articles` (
  `hash` bigint(20) unsigned NOT NULL default '0',
  `points` int(10) unsigned NOT NULL default '0',
  `id_article` int(10) unsigned NOT NULL default '0',
  KEY `hash` (`hash`),
  KEY `id_article` (`id_article`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_index_articles`
--


--
-- Table structure for table `spip_index_auteurs`
--

CREATE TABLE `spip_index_auteurs` (
  `hash` bigint(20) unsigned NOT NULL default '0',
  `points` int(10) unsigned NOT NULL default '0',
  `id_auteur` int(10) unsigned NOT NULL default '0',
  KEY `hash` (`hash`),
  KEY `id_auteur` (`id_auteur`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_index_auteurs`
--


--
-- Table structure for table `spip_index_breves`
--

CREATE TABLE `spip_index_breves` (
  `hash` bigint(20) unsigned NOT NULL default '0',
  `points` int(10) unsigned NOT NULL default '0',
  `id_breve` int(10) unsigned NOT NULL default '0',
  KEY `hash` (`hash`),
  KEY `id_breve` (`id_breve`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_index_breves`
--


--
-- Table structure for table `spip_index_dico`
--

CREATE TABLE `spip_index_dico` (
  `hash` bigint(20) unsigned NOT NULL default '0',
  `dico` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`dico`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_index_dico`
--


--
-- Table structure for table `spip_index_documents`
--

CREATE TABLE `spip_index_documents` (
  `hash` bigint(20) unsigned NOT NULL default '0',
  `points` int(10) unsigned NOT NULL default '0',
  `id_document` int(10) unsigned NOT NULL default '0',
  KEY `hash` (`hash`),
  KEY `id_document` (`id_document`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_index_documents`
--


--
-- Table structure for table `spip_index_forum`
--

CREATE TABLE `spip_index_forum` (
  `hash` bigint(20) unsigned NOT NULL default '0',
  `points` int(10) unsigned NOT NULL default '0',
  `id_forum` int(10) unsigned NOT NULL default '0',
  KEY `hash` (`hash`),
  KEY `id_forum` (`id_forum`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_index_forum`
--


--
-- Table structure for table `spip_index_mots`
--

CREATE TABLE `spip_index_mots` (
  `hash` bigint(20) unsigned NOT NULL default '0',
  `points` int(10) unsigned NOT NULL default '0',
  `id_mot` int(10) unsigned NOT NULL default '0',
  KEY `hash` (`hash`),
  KEY `id_mot` (`id_mot`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_index_mots`
--


--
-- Table structure for table `spip_index_rubriques`
--

CREATE TABLE `spip_index_rubriques` (
  `hash` bigint(20) unsigned NOT NULL default '0',
  `points` int(10) unsigned NOT NULL default '0',
  `id_rubrique` int(10) unsigned NOT NULL default '0',
  KEY `hash` (`hash`),
  KEY `id_rubrique` (`id_rubrique`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_index_rubriques`
--


--
-- Table structure for table `spip_index_signatures`
--

CREATE TABLE `spip_index_signatures` (
  `hash` bigint(20) unsigned NOT NULL default '0',
  `points` int(10) unsigned NOT NULL default '0',
  `id_signature` int(10) unsigned NOT NULL default '0',
  KEY `hash` (`hash`),
  KEY `id_signature` (`id_signature`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_index_signatures`
--


--
-- Table structure for table `spip_index_syndic`
--

CREATE TABLE `spip_index_syndic` (
  `hash` bigint(20) unsigned NOT NULL default '0',
  `points` int(10) unsigned NOT NULL default '0',
  `id_syndic` int(10) unsigned NOT NULL default '0',
  KEY `hash` (`hash`),
  KEY `id_syndic` (`id_syndic`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_index_syndic`
--


--
-- Table structure for table `spip_messages`
--

CREATE TABLE `spip_messages` (
  `id_message` bigint(21) NOT NULL auto_increment,
  `titre` text NOT NULL,
  `texte` longblob NOT NULL,
  `type` varchar(6) NOT NULL default '',
  `date_heure` datetime NOT NULL default '0000-00-00 00:00:00',
  `date_fin` datetime NOT NULL default '0000-00-00 00:00:00',
  `rv` char(3) NOT NULL default '',
  `statut` varchar(6) NOT NULL default '',
  `id_auteur` bigint(21) NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`id_message`),
  KEY `id_auteur` (`id_auteur`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_messages`
--


--
-- Table structure for table `spip_meta`
--

CREATE TABLE `spip_meta` (
  `nom` varchar(255) NOT NULL default '',
  `valeur` varchar(255) default '',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`nom`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_meta`
--

INSERT INTO `spip_meta` VALUES ('version_installee','1.904',20051212003541);
INSERT INTO `spip_meta` VALUES ('email_webmaster','spiptest@waisse.org',20051212145136);
INSERT INTO `spip_meta` VALUES ('activer_breves','oui',20051212145136);
INSERT INTO `spip_meta` VALUES ('config_precise_groupes','non',20051212145136);
INSERT INTO `spip_meta` VALUES ('mots_cles_forums','non',20051212145136);
INSERT INTO `spip_meta` VALUES ('articles_surtitre','oui',20051212145136);
INSERT INTO `spip_meta` VALUES ('articles_soustitre','oui',20051212145136);
INSERT INTO `spip_meta` VALUES ('articles_descriptif','oui',20051212145136);
INSERT INTO `spip_meta` VALUES ('articles_chapeau','oui',20051212145136);
INSERT INTO `spip_meta` VALUES ('articles_ps','oui',20051212145136);
INSERT INTO `spip_meta` VALUES ('articles_redac','non',20051212145136);
INSERT INTO `spip_meta` VALUES ('articles_mots','oui',20051212145136);
INSERT INTO `spip_meta` VALUES ('post_dates','oui',20051212145136);
INSERT INTO `spip_meta` VALUES ('articles_urlref','non',20051212145136);
INSERT INTO `spip_meta` VALUES ('creer_preview','non',20051212003612);
INSERT INTO `spip_meta` VALUES ('taille_preview','150',20051212003612);
INSERT INTO `spip_meta` VALUES ('articles_modif','non',20051212003612);
INSERT INTO `spip_meta` VALUES ('activer_sites','oui',20051212145136);
INSERT INTO `spip_meta` VALUES ('proposer_sites','0',20051212145136);
INSERT INTO `spip_meta` VALUES ('activer_syndic','oui',20051212145136);
INSERT INTO `spip_meta` VALUES ('visiter_sites','non',20051212145136);
INSERT INTO `spip_meta` VALUES ('moderation_sites','non',20051212145136);
INSERT INTO `spip_meta` VALUES ('forums_publics','posteriori',20051212003612);
INSERT INTO `spip_meta` VALUES ('accepter_inscriptions','non',20051212003612);
INSERT INTO `spip_meta` VALUES ('accepter_visiteurs','non',20051212003612);
INSERT INTO `spip_meta` VALUES ('prevenir_auteurs','non',20051212003612);
INSERT INTO `spip_meta` VALUES ('suivi_edito','non',20051212003612);
INSERT INTO `spip_meta` VALUES ('quoi_de_neuf','non',20051212003612);
INSERT INTO `spip_meta` VALUES ('forum_prive_admin','non',20051212003612);
INSERT INTO `spip_meta` VALUES ('activer_moteur','oui',20051212003612);
INSERT INTO `spip_meta` VALUES ('articles_versions','non',20051212003612);
INSERT INTO `spip_meta` VALUES ('articles_ortho','non',20051212003612);
INSERT INTO `spip_meta` VALUES ('preview','non',20051212003612);
INSERT INTO `spip_meta` VALUES ('activer_statistiques','oui',20051212003612);
INSERT INTO `spip_meta` VALUES ('documents_article','oui',20051212145136);
INSERT INTO `spip_meta` VALUES ('documents_rubrique','non',20051212145136);
INSERT INTO `spip_meta` VALUES ('charset','iso-8859-1',20051212003612);
INSERT INTO `spip_meta` VALUES ('creer_htpasswd','non',20051212003612);
INSERT INTO `spip_meta` VALUES ('creer_htaccess','non',20051212003612);
INSERT INTO `spip_meta` VALUES ('langue_site','fr',20051212003612);
INSERT INTO `spip_meta` VALUES ('multi_articles','non',20051212003612);
INSERT INTO `spip_meta` VALUES ('multi_rubriques','non',20051212003612);
INSERT INTO `spip_meta` VALUES ('multi_secteurs','non',20051212003612);
INSERT INTO `spip_meta` VALUES ('gerer_trad','non',20051212003612);
INSERT INTO `spip_meta` VALUES ('langues_multilingue','ar,bg,br,ca,cpf,cpf_hat,da,de,en,eo,es,eu,fa,fon,fr,gl,hu,it,ja,lb,nl,oc_auv,oc_gsc,oc_lms,oc_lnc,oc_ni,oc_ni_la,oc_prv,oc_va,pl,pt,pt_br,ro,ru,tr,vi,zh',20051212003612);
INSERT INTO `spip_meta` VALUES ('langues_proposees','ar,bg,br,ca,cpf,cpf_hat,da,de,en,eo,es,eu,fa,fon,fr,gl,hu,it,ja,lb,nl,oc_auv,oc_gsc,oc_lms,oc_lnc,oc_ni,oc_ni_la,oc_prv,oc_va,pl,pt,pt_br,ro,ru,tr,vi,zh',20051212003619);
INSERT INTO `spip_meta` VALUES ('alea_ephemere_ancien','',20051212003626);
INSERT INTO `spip_meta` VALUES ('alea_ephemere','80eb167f629a4cb826a2cff3b5064e3c',20051212003626);
INSERT INTO `spip_meta` VALUES ('alea_ephemere_date','1134344186',20051212003626);
INSERT INTO `spip_meta` VALUES ('adresse_site','http://spip172.neoskills.com/biospip4',20051212145136);
INSERT INTO `spip_meta` VALUES ('date_calcul_rubriques','1134394944',20051212144224);
INSERT INTO `spip_meta` VALUES ('date_popularites','1134394980',20051212144300);
INSERT INTO `spip_meta` VALUES ('popularite_max','2.6642665500031',20051212144300);
INSERT INTO `spip_meta` VALUES ('popularite_total','3.9963998250046',20051212144300);
INSERT INTO `spip_meta` VALUES ('date_statistiques','2005-12-12',20051212144300);
INSERT INTO `spip_meta` VALUES ('nom_site','Site de tests des squelettes en contribution sur spip-contrib',20051212145136);

--
-- Table structure for table `spip_mots`
--

CREATE TABLE `spip_mots` (
  `id_mot` bigint(21) NOT NULL auto_increment,
  `titre` text NOT NULL,
  `descriptif` text NOT NULL,
  `texte` longblob NOT NULL,
  `id_groupe` bigint(21) NOT NULL default '0',
  `type` text NOT NULL,
  `extra` longblob,
  `idx` enum('','1','non','oui','idx') NOT NULL default '',
  `url_propre` varchar(255) NOT NULL default '',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`id_mot`),
  KEY `idx` (`idx`),
  KEY `url_propre` (`url_propre`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_mots`
--


--
-- Table structure for table `spip_mots_articles`
--

CREATE TABLE `spip_mots_articles` (
  `id_mot` bigint(21) NOT NULL default '0',
  `id_article` bigint(21) NOT NULL default '0',
  KEY `id_mot` (`id_mot`),
  KEY `id_article` (`id_article`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_mots_articles`
--


--
-- Table structure for table `spip_mots_breves`
--

CREATE TABLE `spip_mots_breves` (
  `id_mot` bigint(21) NOT NULL default '0',
  `id_breve` bigint(21) NOT NULL default '0',
  KEY `id_mot` (`id_mot`),
  KEY `id_breve` (`id_breve`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_mots_breves`
--


--
-- Table structure for table `spip_mots_documents`
--

CREATE TABLE `spip_mots_documents` (
  `id_mot` bigint(21) NOT NULL default '0',
  `id_document` bigint(21) NOT NULL default '0',
  KEY `id_mot` (`id_mot`),
  KEY `id_document` (`id_document`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_mots_documents`
--


--
-- Table structure for table `spip_mots_forum`
--

CREATE TABLE `spip_mots_forum` (
  `id_mot` bigint(21) NOT NULL default '0',
  `id_forum` bigint(21) NOT NULL default '0',
  KEY `id_mot` (`id_mot`),
  KEY `id_forum` (`id_forum`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_mots_forum`
--


--
-- Table structure for table `spip_mots_rubriques`
--

CREATE TABLE `spip_mots_rubriques` (
  `id_mot` bigint(21) NOT NULL default '0',
  `id_rubrique` bigint(21) NOT NULL default '0',
  KEY `id_mot` (`id_mot`),
  KEY `id_rubrique` (`id_rubrique`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_mots_rubriques`
--


--
-- Table structure for table `spip_mots_syndic`
--

CREATE TABLE `spip_mots_syndic` (
  `id_mot` bigint(21) NOT NULL default '0',
  `id_syndic` bigint(21) NOT NULL default '0',
  KEY `id_mot` (`id_mot`),
  KEY `id_syndic` (`id_syndic`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_mots_syndic`
--


--
-- Table structure for table `spip_ortho_cache`
--

CREATE TABLE `spip_ortho_cache` (
  `lang` varchar(10) NOT NULL default '',
  `mot` varchar(255) binary NOT NULL default '',
  `ok` tinyint(4) NOT NULL default '0',
  `suggest` blob NOT NULL,
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`lang`,`mot`),
  KEY `maj` (`maj`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_ortho_cache`
--


--
-- Table structure for table `spip_ortho_dico`
--

CREATE TABLE `spip_ortho_dico` (
  `lang` varchar(10) NOT NULL default '',
  `mot` varchar(255) binary NOT NULL default '',
  `id_auteur` bigint(20) unsigned NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`lang`,`mot`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_ortho_dico`
--


--
-- Table structure for table `spip_petitions`
--

CREATE TABLE `spip_petitions` (
  `id_article` bigint(21) NOT NULL default '0',
  `email_unique` char(3) NOT NULL default '',
  `site_obli` char(3) NOT NULL default '',
  `site_unique` char(3) NOT NULL default '',
  `message` char(3) NOT NULL default '',
  `texte` longblob NOT NULL,
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`id_article`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_petitions`
--


--
-- Table structure for table `spip_referers`
--

CREATE TABLE `spip_referers` (
  `referer_md5` bigint(20) unsigned NOT NULL default '0',
  `date` date NOT NULL default '0000-00-00',
  `referer` varchar(255) NOT NULL default '',
  `visites` int(10) unsigned NOT NULL default '0',
  `visites_jour` int(10) unsigned NOT NULL default '0',
  `visites_veille` int(10) unsigned NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`referer_md5`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_referers`
--

INSERT INTO `spip_referers` VALUES (969164552488845912,'2005-12-12','http://spip172.neoskills.com/biospip4/ecrire/install.php3',1,1,0,20051212013051);
INSERT INTO `spip_referers` VALUES (451413010724205980,'2005-12-12','http://spip172.neoskills.com/biospip4/spip_login.php3?url=ecrire%2Findex.php3',1,1,0,20051212013051);
INSERT INTO `spip_referers` VALUES (747425511498614417,'2005-12-12','http://spip172.neoskills.com/biospip4/spip_login.php3?var_echec_cookie=oui&url=ecrire%2Findex.php3',1,1,0,20051212013051);
INSERT INTO `spip_referers` VALUES (919162548655021161,'2005-12-12','http://www.spip-contrib.net/ecrire/articles.php3?id_article=1182',1,1,0,20051212143343);

--
-- Table structure for table `spip_referers_articles`
--

CREATE TABLE `spip_referers_articles` (
  `id_article` int(10) unsigned NOT NULL default '0',
  `referer_md5` bigint(20) unsigned NOT NULL default '0',
  `date` date NOT NULL default '0000-00-00',
  `referer` varchar(255) NOT NULL default '',
  `visites` int(10) unsigned NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`id_article`,`referer_md5`),
  KEY `referer_md5` (`referer_md5`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_referers_articles`
--


--
-- Table structure for table `spip_rubriques`
--

CREATE TABLE `spip_rubriques` (
  `id_rubrique` bigint(21) NOT NULL auto_increment,
  `id_parent` bigint(21) NOT NULL default '0',
  `titre` text NOT NULL,
  `descriptif` text NOT NULL,
  `texte` longblob NOT NULL,
  `id_secteur` bigint(21) NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  `export` varchar(10) default 'oui',
  `id_import` bigint(20) default '0',
  `statut` varchar(10) NOT NULL default '',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `lang` varchar(10) NOT NULL default '',
  `langue_choisie` char(3) default 'non',
  `idx` enum('','1','non','oui','idx') NOT NULL default '',
  `extra` longblob,
  `url_propre` varchar(255) NOT NULL default '',
  `statut_tmp` varchar(10) NOT NULL default '',
  `date_tmp` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id_rubrique`),
  KEY `lang` (`lang`),
  KEY `idx` (`idx`),
  KEY `id_parent` (`id_parent`),
  KEY `url_propre` (`url_propre`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_rubriques`
--

INSERT INTO `spip_rubriques` VALUES (1,0,'Rubrique test 1','','Rubrique test 1',1,20051212144224,'oui',0,'publie','2005-12-12 00:45:20','fr','non','oui',NULL,'','publie','2005-12-12 00:45:20');
INSERT INTO `spip_rubriques` VALUES (2,1,'sous rubrique test 1.1','sous rubrique test 1.1','sous rubrique test 1.1',1,20051212144224,'oui',0,'publie','2005-12-12 00:45:20','fr','non','oui',NULL,'','publie','2005-12-12 00:45:20');
INSERT INTO `spip_rubriques` VALUES (3,0,'Rubrique test 2','Rubrique test 2','Rubrique test 2',3,20051212144224,'oui',0,'publie','2005-12-12 00:43:05','fr','non','oui',NULL,'','publie','2005-12-12 00:43:05');

--
-- Table structure for table `spip_signatures`
--

CREATE TABLE `spip_signatures` (
  `id_signature` bigint(21) NOT NULL auto_increment,
  `id_article` bigint(21) NOT NULL default '0',
  `date_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `nom_email` text NOT NULL,
  `ad_email` text NOT NULL,
  `nom_site` text NOT NULL,
  `url_site` text NOT NULL,
  `message` mediumtext NOT NULL,
  `statut` varchar(10) NOT NULL default '',
  `idx` enum('','1','non','oui','idx') NOT NULL default '',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`id_signature`),
  KEY `id_article` (`id_article`),
  KEY `idx` (`idx`),
  KEY `statut` (`statut`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_signatures`
--


--
-- Table structure for table `spip_syndic`
--

CREATE TABLE `spip_syndic` (
  `id_syndic` bigint(21) NOT NULL auto_increment,
  `id_rubrique` bigint(21) NOT NULL default '0',
  `id_secteur` bigint(21) NOT NULL default '0',
  `nom_site` blob NOT NULL,
  `url_site` blob NOT NULL,
  `url_syndic` blob NOT NULL,
  `descriptif` blob NOT NULL,
  `url_propre` varchar(255) NOT NULL default '',
  `idx` enum('','1','non','oui','idx') NOT NULL default '',
  `maj` timestamp(14) NOT NULL,
  `syndication` char(3) NOT NULL default '',
  `statut` varchar(10) NOT NULL default '',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `date_syndic` datetime NOT NULL default '0000-00-00 00:00:00',
  `date_index` datetime NOT NULL default '0000-00-00 00:00:00',
  `extra` longblob,
  `moderation` char(3) default 'non',
  `miroir` char(3) default 'non',
  `oubli` char(3) default 'non',
  `resume` char(3) default 'oui',
  PRIMARY KEY  (`id_syndic`),
  KEY `id_rubrique` (`id_rubrique`),
  KEY `id_secteur` (`id_secteur`),
  KEY `idx` (`idx`),
  KEY `statut` (`statut`,`date_syndic`),
  KEY `url_propre` (`url_propre`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_syndic`
--


--
-- Table structure for table `spip_syndic_articles`
--

CREATE TABLE `spip_syndic_articles` (
  `id_syndic_article` bigint(21) NOT NULL auto_increment,
  `id_syndic` bigint(21) NOT NULL default '0',
  `titre` text NOT NULL,
  `url` varchar(255) NOT NULL default '',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `lesauteurs` text NOT NULL,
  `maj` timestamp(14) NOT NULL,
  `statut` varchar(10) NOT NULL default '',
  `descriptif` blob NOT NULL,
  `lang` varchar(10) NOT NULL default '',
  `url_source` tinytext NOT NULL,
  `source` tinytext NOT NULL,
  `tags` text NOT NULL,
  PRIMARY KEY  (`id_syndic_article`),
  KEY `id_syndic` (`id_syndic`),
  KEY `statut` (`statut`),
  KEY `url` (`url`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_syndic_articles`
--


--
-- Table structure for table `spip_types_documents`
--

CREATE TABLE `spip_types_documents` (
  `id_type` bigint(21) NOT NULL auto_increment,
  `titre` text NOT NULL,
  `descriptif` text NOT NULL,
  `extension` varchar(10) NOT NULL default '',
  `mime_type` varchar(100) NOT NULL default '',
  `inclus` enum('non','image','embed') NOT NULL default 'non',
  `upload` enum('oui','non') NOT NULL default 'oui',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`id_type`),
  UNIQUE KEY `extension` (`extension`),
  KEY `inclus` (`inclus`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_types_documents`
--

INSERT INTO `spip_types_documents` VALUES (1,'JPG','','jpg','image/jpeg','image','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (2,'PNG','','png','image/png','image','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (3,'GIF','','gif','image/gif','image','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (4,'BMP','','bmp','image/x-ms-bmp','image','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (5,'Photoshop','','psd','image/x-photoshop','image','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (6,'TIFF','','tif','image/tiff','image','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (7,'AIFF','','aiff','audio/x-aiff','embed','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (8,'Windows Media','','asf','video/x-ms-asf','embed','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (9,'Windows Media','','avi','video/x-msvideo','embed','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (10,'Midi','','mid','audio/midi','embed','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (11,'MNG','','mng','video/x-mng','embed','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (12,'QuickTime','','mov','video/quicktime','embed','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (13,'MP3','','mp3','audio/mpeg','embed','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (14,'MPEG','','mpg','video/mpeg','embed','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (15,'Ogg','','ogg','application/ogg','embed','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (16,'QuickTime','','qt','video/quicktime','embed','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (17,'RealAudio','','ra','audio/x-pn-realaudio','embed','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (18,'RealAudio','','ram','audio/x-pn-realaudio','embed','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (19,'RealAudio','','rm','audio/x-pn-realaudio','embed','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (20,'Flash','','swf','application/x-shockwave-flash','embed','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (21,'WAV','','wav','audio/x-wav','embed','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (22,'Windows Media','','wmv','video/x-ms-wmv','embed','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (23,'Adobe Illustrator','','ai','application/illustrator','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (24,'BZip','','bz2','application/x-bzip2','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (25,'Binary Data','','bin','application/octet-stream','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (26,'C source','','c','text/x-csrc','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (27,'Cascading Style Sheet','','css','text/css','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (28,'Debian','','deb','application/x-debian-package','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (29,'Word','','doc','application/msword','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (30,'DjVu','','djvu','image/vnd.djvu','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (31,'LaTeX DVI','','dvi','application/x-dvi','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (32,'PostScript','','eps','application/postscript','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (33,'GZ','','gz','application/x-gzip','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (34,'C header','','h','text/x-chdr','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (35,'HTML','','html','text/html','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (36,'Pascal','','pas','text/x-pascal','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (37,'PDF','','pdf','application/pdf','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (38,'Portable Game Notation','','pgn','application/x-chess-pgn','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (39,'PowerPoint','','ppt','application/vnd.ms-powerpoint','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (40,'PostScript','','ps','application/postscript','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (41,'RedHat/Mandrake/SuSE','','rpm','application/x-redhat-package-manager','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (42,'RTF','','rtf','application/rtf','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (43,'StarOffice','','sdd','application/vnd.stardivision.impress','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (44,'StarOffice','','sdw','application/vnd.stardivision.writer','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (45,'Stuffit','','sit','application/x-stuffit','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (46,'OpenOffice Calc','','sxc','application/vnd.sun.xml.calc','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (47,'OpenOffice Impress','','sxi','application/vnd.sun.xml.impress','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (48,'OpenOffice','','sxw','application/vnd.sun.xml.writer','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (49,'LaTeX','','tex','text/x-tex','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (50,'BitTorrent','','torrent','application/x-bittorrent','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (51,'TGZ','','tgz','application/x-gtar','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (52,'texte','','txt','text/plain','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (53,'GIMP multi-layer','','xcf','application/x-xcf','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (54,'Excel','','xls','application/vnd.ms-excel','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (55,'XML','','xml','application/xml','non','oui',20051212003541);
INSERT INTO `spip_types_documents` VALUES (56,'Zip','','zip','application/zip','non','oui',20051212003541);

--
-- Table structure for table `spip_versions`
--

CREATE TABLE `spip_versions` (
  `id_article` bigint(21) NOT NULL default '0',
  `id_version` int(10) unsigned NOT NULL default '0',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `id_auteur` bigint(21) NOT NULL default '0',
  `titre_version` text NOT NULL,
  `permanent` char(3) NOT NULL default '',
  `champs` text NOT NULL,
  PRIMARY KEY  (`id_article`,`id_version`),
  KEY `date` (`id_article`,`date`),
  KEY `id_auteur` (`id_auteur`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_versions`
--


--
-- Table structure for table `spip_versions_fragments`
--

CREATE TABLE `spip_versions_fragments` (
  `id_fragment` int(10) unsigned NOT NULL default '0',
  `version_min` int(10) unsigned NOT NULL default '0',
  `version_max` int(10) unsigned NOT NULL default '0',
  `id_article` bigint(21) NOT NULL default '0',
  `compress` tinyint(4) NOT NULL default '0',
  `fragment` longblob NOT NULL,
  PRIMARY KEY  (`id_article`,`id_fragment`,`version_min`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_versions_fragments`
--


--
-- Table structure for table `spip_visites`
--

CREATE TABLE `spip_visites` (
  `date` date NOT NULL default '0000-00-00',
  `visites` int(10) unsigned NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`date`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_visites`
--

INSERT INTO `spip_visites` VALUES ('2005-12-12',6,20051212143343);

--
-- Table structure for table `spip_visites_articles`
--

CREATE TABLE `spip_visites_articles` (
  `date` date NOT NULL default '0000-00-00',
  `id_article` int(10) unsigned NOT NULL default '0',
  `visites` int(10) unsigned NOT NULL default '0',
  `maj` timestamp(14) NOT NULL,
  PRIMARY KEY  (`date`,`id_article`)
) TYPE=MyISAM;

--
-- Dumping data for table `spip_visites_articles`
--

INSERT INTO `spip_visites_articles` VALUES ('2005-12-12',4,4,20051212091221);
INSERT INTO `spip_visites_articles` VALUES ('2005-12-12',3,1,20051212013051);
INSERT INTO `spip_visites_articles` VALUES ('2005-12-12',2,1,20051212091221);

