# phpMyAdmin MySQL-Dump
# version 2.3.0
# http://phpwizard.net/phpMyAdmin/
# http://www.phpmyadmin.net/ (download page)
#
# Host: localhost
# Generation Time: Dec 23, 2003 at 06:15 PM
# Server version: 3.22.32
# PHP Version: 4.1.2
# Database : MultiSpip
# --------------------------------------------------------

#
# Table structure for table inscrits
#

CREATE TABLE inscrits (
   IdInscrit int(11) NOT NULL auto_increment,
   NomInscrit varchar(32),
   PrenomInscrit varchar(32),
   EmailInscrit varchar(64),
   MdpInscrit varchar(8),
   idAgent varchar(5),
   ValidInscrit varchar(10) DEFAULT 'nok' NOT NULL,
   PRIMARY KEY (IdInscrit)
);
# --------------------------------------------------------

#
# Table structure for table sites
#

CREATE TABLE sites (
   IdSite int(11) NOT NULL auto_increment,
   NomSite varchar(48),
   UrlSite varchar(100),
   DescriptionSite varchar(255),
   dateEnregistrement date DEFAULT '0000-00-00',
   IdInscrit int(11) DEFAULT '0' NOT NULL,
   multispip varchar(10),
   modele varchar(120),
   dossier varchar(50),
   synchro varchar(10),
   PRIMARY KEY (IdSite)
);

# --------------------------------------------------------

#
# Table structure for table `personnes`
#

CREATE TABLE `personnes` (
  `per_id` int(11) NOT NULL auto_increment,
  `per_nom` varchar(255) NOT NULL default '',
  `per_prenom` varchar(255) NOT NULL default '',
  `per_mail` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`per_id`)
) TYPE=MyISAM AUTO_INCREMENT=2 ;

#
# Dumping data for table `personnes`
#

INSERT INTO `personnes` VALUES (1, 'DOE', 'John', 'john.doe@mail.com');