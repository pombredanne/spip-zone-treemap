/*
 *  VirtualSPIP, un projet libre (c) Nat33, Thus0, Fil etc
 *  Distribue sous licence GNU/GPL
 */

Description :
----------------

Acc�der � des sites spip virtuels via l'URL suivante
	http://[SITE].virtualspip.example.com/



Configuration du DNS :
-------------------------

1�) Vous g�rez votre domaine example.com
Dans le DNS, on va g�rer tout *.virtual.example.com :

*.virtual          A       217.24.84.2

2�) Vous utilisez un service de DNS dynamique (par exemple http://www.dyndns.com) :
Dans l'interface de configuration, dans "My Host", cr�er un nouvel h�te "Static DNS
et cocher la case "Enable Wildcard".

Par exemple : virtualspip.homedns.org



Configuration de apache :
----------------------------

Cr�er le r�pertoire /var/www/virtualspip o� sera install� SPIP

Configurer apache :

<VirtualHost *:80>
  ServerName	virtualspip.example.com
  ServerAlias	*.virtualspip.example.com

  DocumentRoot	/var/www/virtualspip
  DirectoryIndex index.htm index.html index.php index.php3

  <Directory /var/www/virtualspip>
    Options Indexes
  </Directory>

  ErrorLog	/var/log/apache2/virtualspip-error.log
  LogLevel	warn
  CustomLog	/var/log/apache2/virtualspip-access.log combined
</Virtualhost>



Installation de SPIP :
-------------------------

Rq : il faut une version disposant du patch : http://trac.rezo.net/trac/spip/changeset/5576

ou utiliser le patch suivant sur les 2 fichiers suivants :

1�) ecrire/inc_version.php3

27a28
> define('_DIR_RACINE', _DIR_RESTREINT ? '' : '../');
29,33d29
< define('_FILE_CONNECT_INS', (_DIR_RESTREINT . "inc_connect"));
< define('_FILE_CONNECT',
<       (@file_exists(_FILE_CONNECT_INS . _EXTENSION_PHP) ?
<               (_FILE_CONNECT_INS . _EXTENSION_PHP)
<        : false));
353a350,355
> // Le fichier de connexion a la base de donnees
> define_once('_FILE_CONNECT_INS', (_DIR_RESTREINT . "inc_connect"));
> define_once('_FILE_CONNECT',
>       (@file_exists(_FILE_CONNECT_INS . _EXTENSION_PHP) ?
>               (_FILE_CONNECT_INS . _EXTENSION_PHP)
>        : false));

2�) ecrire/inc_auth.php3 :

19c19
< include_ecrire ("inc_connect.php3");
---
> include_ecrire (_FILE_CONNECT);



Modification de ecrire/mes_options.php3 :
--------------------------------------------

Rajouter la ligne suivante dans votre fichier ecrire/mes_options.php3 :
	<?php include(dirname(__FILE__).'/virtualspip.php'); ?>



Modification de ecrire/mes_definitions.php :
-----------------------------------------------

Cr�er un r�pertoire o� seront stock�s les sites SPIP virtuels, par exemple : /var/www/virtualspip/site

Modifier les variables :
	$virtual_domain : nom de domaine (par ex: virtualspip.example.com)
	$virtual_dir_racine : nom du r�pertoire pr�c�demment cr�� (par ex: 'site/')



Utilisation de VirtualSPIP :
-------------------------------

Vous pouvez d�finir des :
	options par d�faut pour tous les sites SPIP virtuels dans le fichier ecrire/mes_options.php3,
	ou pour un site particulier dans le fichier site/<site>/mes_options.php3



Exemple : site toto.virtualspip.example.com :
------------------------------------------------

Le site toto.virtualspip.example.com d�pose ses fichiers dans
	site/toto/
				IMG/
				data/
				mes_options.php3 (facultatif)
				CACHE/
				upload/

        (les r�pertoires sont � cr�er, mais SPIP pourrait le faire tout seul   
        si site/toto/ est 777)

et ses donn�es sont dans les tables toto_***** de la base principale.



D�monstration :
------------------

dossier squelettes pour chaque site :
	http://machin.mini.rezo.net/article.php3?id_article=1
	http://machin.mini.rezo.net/site/machin/squelettes/


des types d'urls diff�rents d'un site � l'autre :
	http://chouette.mini.rezo.net/gotcha.html
	http://machin.mini.rezo.net/Fantastique



TODO :
-------

- g�rer la personnalisation (puce.gif, etc)
- l'installation
- la mise � jour automatique de la base en cas de changement de version de spip
- etc
