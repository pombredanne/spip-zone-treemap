<?php

  define('_DIR_RESTREINT_ABS', 'ecrire/');
  define('_DIR_RESTREINT', (!@is_dir(_DIR_RESTREINT_ABS) ? "" : _DIR_RESTREINT_ABS));
  define('_DIR_RACINE', _DIR_RESTREINT ? '' : '../');
  include('mes_definitions.php');

  if (!is_dir($virtual_dir_site)) {
    echo "Creation du site <b>$virtual_site</b><br/><br/>";

    if (@mkdir($virtual_dir_site, 0755)) {
      echo '... cr�ation du r�pertoire _DIR_SITE<br/>';
    }
    if (@mkdir(_DIR_IMG, 0755)) {
      echo '... cr�ation du r�pertoire _DIR_IMG<br/>';
    }
    if (@mkdir(_DIR_CACHE, 0755)) {
      echo '... cr�ation du r�pertoire _DIR_CACHE<br/>';
    }
    if (@mkdir(_DIR_SESSIONS, 0755)) { 
      echo '... cr�ation du r�pertoire _DIR_SESSIONS<br/>';
    }
    if (@mkdir(_DIR_TRANSFERT, 0755)) {
      echo '... cr�ation du r�pertoire _DIR_TRANSFERT<br/>';
    }

    echo "<br/>Vous pouvez maintenant <a href=\"http://" . $_SERVER['HTTP_HOST'] . '/ecrire/>configurer</a> votre site ...';

} else {

  echo "Le site <b>$virtual_site</b> existe d�j� !<br/><br/>";

  if (@file_exists(_FILE_CONNECT)) {
    echo " -> <a href=\"http://".$_SERVER['HTTP_HOST']."\">http://".$_SERVER['HTTP_HOST']."</a>";
  } else {
    echo "... mais n'est pas encore  <a href=\"http://".$_SERVER['HTTP_HOST'].'/ecrire/\">configur�</a> ...';
  }

}

?>
