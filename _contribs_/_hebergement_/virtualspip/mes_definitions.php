<?php

   // Nom du domaine de virtualspip
   $virtual_domain = ".virtualspip.homedns.org";

   // Repertoire ou sont stock�s les sites SPIP virtuels
   $virtual_dir_racine = 'site/';

   // Site SPIP
   preg_match(',^([a-z:0-9:\-:\_]+)'.$virtual_domain.'$,', $_SERVER['HTTP_HOST'], $r);
   $virtual_site = $r[1];
   
   // Redefinition de la racine
   $virtual_dir_site = _DIR_RACINE . $virtual_dir_racine . $virtual_site . '/';

   // Redefinition des repertoires
   define('_FILE_CONNECT_INS', $virtual_dir_site.'inc_connect');

   define('_DIR_IMG', $virtual_dir_site.'IMG/');
   define('_DIR_DOC', _DIR_IMG);
   define('_DIR_IMG_ICONES', _DIR_RACINE.'IMG/icones/');
   define('_DIR_IMG_ICONES_BARRE', _DIR_RACINE.'IMG/icones_barre/');
   define('_DIR_CACHE', $virtual_dir_site.'CACHE/');
   define('_DIR_SESSIONS', $virtual_dir_site.'data/');
   define('_DIR_TRANSFERT', $virtual_dir_site.'upload');
   
   // fichier virtuel mes_options.php3
   define('_VIRTUAL_FILE_OPTIONS', $virtual_dir_site.'mes_options.php3');
    
?>
