<?php

  include('mes_definitions.php');

  if (!isset($virtual_site))
    die ("domaine inconnu !");
  
  if (!is_dir($virtual_dir_site)) {
  
    echo "<H1>Virtual SPIP</H1>";
    echo "Le site <b>$site</b> n'est pas encore ouvert !<br/><br/>";
    echo "Pour initialiser ce site, allez sur <a href=\"" . _DIR_RACINE . "ecrire/creer_site.php\">cette page</a> ";
    exit;
    
  } else {
  
    // ci-dessous, on joue sur le prefixe des tables ;
    // on pourrait modifier (au choix, ou aussi) le nom de la base de donnees
    $table_prefix  = $site;
    $cookie_prefix = $site;
    $dossier_squelettes = $virtual_dir_site.'squelettes/';

    // fichier mes_options
    if (@file_exists(_VIRTUAL_FILE_OPTIONS)) {
      include(_VIRTUAL_FILE_OPTIONS);
    }

  }

?>
