<?php

/***************************************************************************\
 *  inc-rhisome.php3                                                       *
 *  Jeu de filtres pour mettre en place une navigation non hierarchique    *
 *  ("rhisomique") dans SPIP                                               *
 *                                                                         *
 *  Version: 0.1                                                           *
 *                                                                         *
 *  Explications ici:                                                      *
 *  http://www.spip-contrib.net/ecrire/articles.php3?id_article=1072       *
 *                                                                         *
 *  Copyright (c) 2005 Francois Schreuer                                   *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 \***************************************************************************/


/***************** Configuration *******************/

// Nombre maximal de liens d'un meme type a renvoyer par le filtre
define('_RHIZOME_MAX_RETOUR', '7');

// Les groupes de mots-cles concernes par le rhizome (on ne cherchera a faire des liens
// que vers les mots-cles appartenant a ces groupes)
# define('_RHIZOME_GROUPES', '4,6'); // separes par des virgules
# !!! parametrage marche pas encore, allez a la ligne 120 changer ca a la main !!!

// Chaines html entourant chaque item renvoye
define('_RHIZOME_DEBUT_HTML', '<li>');
define('_RHIZOME_FIN_HTML', '</li>');
define('_RHIZOME_DEBUT_HTML_GENERAL', '<ul>');
define('_RHIZOME_FIN_HTML_GENERAL', '</ul>');

// Seuil de l'indice total en dessous duquel la liaison ne se fait pas
// Augmenter la valeur pour diminuer le nombre de resultats
define('_RHIZOME_MIN_INDICE_TOTAL', '0.05');

// Nombre minimal d'articles que deux objets ont en commun en dessous duquel on ne renvoie pas de resultat
define('_RHIZOME_MIN_INTERSECTION', '3');

// Les quatre (x2) parametres suivants permettent de configurer le filtre indice_interet_propre
// Mettre 0 pour annuler un sous-indice (et donc donner plus d'importance proportionnelle aux autres
// Exemple pour le premier parametre: duree en jours pendant laquelle on estime que les articles site sont utiles
// Sur un site dont le contenu est rapidement perime, mettre une petite valeur (30 ou 60 par exemple)
// Sur un site ou la date du contenu n'a pas d'importance, mettre une grande valeur ou neutraliser
// cette partie de l'indice en le mettant � 0
define('_RHIZOME_ARTICLE_DUREE', '365');
	// Choisir la fonction de calcul du sous-indice
	// Valeurs possibles:
	//    log : fonction logarithmique (avantage les valeurs eleves)
	//    1er : fonction du 1er degre (totalement continue, moins discriminante)
	define('_RHIZOME_ARTICLE_DUREE_MODE', '1er');
// Nombre de messages de forums donnant lieu au maximum de points sur ce critere
define('_RHIZOME_ARTICLE_FORUMS', '10');
	// Idem supra
	define('_RHIZOME_ARTICLE_FORUMS_MODE', '1er');
// Longueur d'un article donnant lieu au maximum des points sur ce critere
define('_RHIZOME_ARTICLE_LONGUEUR', '40000');
	// Idem supra
	define('_RHIZOME_ARTICLE_LONGUEUR_MODE', '1er');
// Popularite d'un article donnant lieu au maximum des points sur ce critere
define('_RHIZOME_ARTICLE_POPULARITE', '100');
	// Idem supra
	define('_RHIZOME_ARTICLE_POPULARIT_MODE', '1er');
// NB: Si les quatre criteres sont annules, l'indice renverra systematiquement la valeur 1

/************* Fin de la configuration *************/


//
// Renvoie la liste des objets lies a un objet
//
// Parametres :
//
//   $type         : le type d'objet pass� en variable principale (par defaut : 'mot')
//                   valeurs possibles : 'mot'
//                   valeurs possibles a terme : 'mot', 'auteur', 'article'
//
//   $mode         : le type d'objet que le filtre retournera
//                   valeurs possibles : les memes que pour $type
//
// Il y a donc theoriquement neuf usages possibles du filtre rhizome :
//
//  Le seul usage deja operationnel est :
//     [(#ID_MOT|rhizome{'mot','mot'})] alias [(#ID_MOT|rhizome)]
//
//  Les 8 suivants devraient l'etre sous peu :
//     [(#ID_MOT|rhizome{'mot','auteur'})]
//     [(#ID_MOT|rhizome{'mot','article'})]
//     [(#ID_AUTEUR|rhizome{'auteur','mot'})]
//     [(#ID_AUTEUR|rhizome{'auteur','auteur'})]
//     [(#ID_AUTEUR|rhizome{'auteur','article'})]
//     [(#ID_ARTICLE|rhizome{'article','mot'})]
//     [(#ID_ARTICLE|rhizome{'article','auteur'})]
//     [(#ID_ARTICLE|rhizome{'article','article'})]
//
function rhizome ($id, $type='', $mode='', $debut_html='', $fin_html='', $debut_html_general='', $fin_html_general='')
{

	// On verifie les variables
	if(empty($type)) $type = 'mot';
	if(empty($mode)) $mode = $type;
	if(empty($debut_html)) $debut_html = _RHIZOME_DEBUT_HTML;
	if(empty($fin_html)) $fin_html = _RHIZOME_FIN_HTML;
	if(empty($debut_html_general)) $debut_html_general = _RHIZOME_DEBUT_HTML_GENERAL;
	if(empty($fin_html_general)) $fin_html_general = _RHIZOME_FIN_HTML_GENERAL;


	switch($type)
	{

		case'mot':
			// On initialise le compteur du tableau
			$i = 0;
			// On passe en revue tous les mots-cles du site appartenant aux groupes ok
			// TODO - Parametrer les groupes
			$q = spip_query("SELECT id_mot,titre FROM spip_mots WHERE id_mot!= '$id' AND id_groupe IN (4,6)");
			while ($r =  spip_fetch_array($q))
			{
				$id_mot_courant = $r['id_mot'];
				// Pour chaque mot, on compte le nombre d'articles qu'il a en commun avec le mot courant
				// NB: on ne se limite pas aux articles publies pour avoir une masse critique plus importante
				//     Pour limiter aux articles publies, il suffit d'ajouter: AND articles.statut='publie'
				$intersection = spip_fetch_array(spip_query("SELECT
					COUNT(articles.id_article) AS compteur
					FROM
						spip_mots_articles AS mots_articles_2,
						spip_mots_articles AS mots_articles_3,
						spip_articles AS articles
					WHERE
						(articles.id_article = mots_articles_2.id_article
						AND mots_articles_2.id_mot = '$id_mot_courant')
						AND (articles.id_article = mots_articles_3.id_article
						AND mots_articles_3.id_mot = '$id')
					"));
				// Si le nombre d'article est inferieur au minimum requis, on ne va pas plus loin
				if($intersection['compteur'] >= _RHIZOME_MIN_INTERSECTION)
				{
					// Le nombre de mots appartenant a un mot OU a l'autre
					$unoulautre = spip_fetch_array(spip_query("SELECT
						COUNT(distinct id_article) AS compteur
						FROM spip_mots_articles
						WHERE id_mot in ($id_mot_courant,$id)
					"));
					// On calcule l'indice de pertinence en comparant l'intersection des deux mots � leur union
					// NB: signification de la division de $unoulautre['compteur'] par 5 :
					// on considere que la liaisons entre deux mots ayant 20% d'articles en commun est pertinente a 100%
					$pertinence = calculer_indice($intersection['compteur'], $unoulautre['compteur'] / 5);
					// On recupere l'indice d'interet propre du mot en question
					$interet = indice_interet_propre($id_mot_courant,$type);
					// On attenue un peu la portee de l'indice d'interet
					$interet = ($interet / 3) + 0.667;
					// On pondere l'indice de pertinence par l'indice d'interet (et on arrondit)
					$indice_total = round($interet * $pertinence,3);
					// Si la valeur de l'indice total depasse le seuil requis,
					// on stocke le tout dans un tableau
					if($indice_total > _RHIZOME_MIN_INDICE_TOTAL) {
						$tableau[$i]['indice_total'] = $indice_total;
						$tableau[$i]['titre'] = $r['titre'];
						$tableau[$i]['id_mot'] = $r['id_mot'];
						$tableau[$i]['intersection'] = $intersection['compteur'];
						$tableau[$i]['union'] = $unoulautre['compteur'];
						$tableau[$i]['indice_interet'] = $interet;
						$tableau[$i]['indice_pertinence'] = $pertinence;
						$i++;
					}
				}
			}

			// On trie le tableau
			if(count($tableau) > 1)
				$tableau = tri_multi($tableau, 'indice_total', SORT_DESC);

			// On genere l'affichage html a partir du tableau
			for($j=0; $j != count($tableau); $j++) {
				if($j < _RHIZOME_MAX_RETOUR) {
					$retour .= $debut_html.'<a href="'.
						generer_url_mot($tableau[$j]['id_mot']).'" title="Pertinence de la liaison&nbsp;: '.
						ceil($tableau[$j]['indice_total']*100).'&nbsp;%">'.
						$tableau[$j]['titre'].'</a>'.$fin_html;
				}
			}
		break;
	}

	$retour = trim($retour);
	if ($retour)
		return $debut_html_general.$retour.$fin_html_general;
	else
		return '';
}

function indice_interet_propre($id,$type='') {
	if(empty($type)) $type = 'article';
	switch($type) {
		case'article':
			$r = spip_fetch_array(spip_query("SELECT popularite,
				UNIX_TIMESTAMP(date) AS date,
				UNIX_TIMESTAMP(date_redac) AS date_redac,
				UNIX_TIMESTAMP(date_modif) AS date_modif,
				LENGTH(texte) AS longueur
				FROM spip_articles
				WHERE statut='publie'
				AND id_article='$id'"));
			// On compte le nombre de message de forums lies a cet article
			$nb_forum = spip_fetch_array(spip_query("SELECT COUNT(*) AS forums
				FROM spip_forum WHERE statut='public' AND id_article='$id'"));

			// Si elle est definie, on se base sur la date de publication anterieure
			/*
			if($r['date_modif'] != '0000-00-00 00:00:00')
				$date = $r['date_modif'];
			// Sinon, on prend la date normale
			else
			*/
			$date = $r['date'];
			// On finit par recuperer l'age en jours de l'article
			$age = ceil((time() - $date) / (24*60*60));

			// Construction de l'indice
			// On commence par creer des sous-indices ayant chacun une valeur comprise entre 0 et 1
			// Les indices sont calcules par la fonction calculer_indice() ci-dessous
			// Le troisieme parametre de la fonction calculer_indice definit la fonction de calcul
				// Age
				$age = _RHIZOME_ARTICLE_DUREE - $age; // On inverse (plus l'age est petit, mieux c'est)
				$indice_age = calculer_indice($age,
					_RHIZOME_ARTICLE_DUREE,
					_RHIZOME_ARTICLE_DUREE_MODE);

				// Nb de forums
				$indice_forum = calculer_indice($nb_forum['forums'],
					_RHIZOME_ARTICLE_FORUMS,
					_RHIZOME_ARTICLE_FORUMS_MODE);

				// Longueur du texte
				$indice_longueur = calculer_indice($r['longueur'],
					_RHIZOME_ARTICLE_LONGUEUR,
					_RHIZOME_ARTICLE_LONGUEUR_MODE);

				// Popularite relative
				// TODO: ponderer par la popularite totale du site (ce sera plus pertinent
				// mais dans la mesure ou la valeur de l'indice n'est utilisee que de maniere
				// relative, ca n'affectera pas la pertinence du filtre rhizome
				$indice_popularite = calculer_indice($r['popularite'],
					_RHIZOME_ARTICLE_POPULARITE,
					_RHIZOME_ARTICLE_POPULARITE_MODE);

			// On agrege les valeurs des differents sous-indices
			// Valeur brute, simple somme des sous-indices
			$indice = $indice_age + $indice_forum + $indice_longueur + $indice_popularite;
			// Calcul du nombre de criteres actifs
			$nb_criteres_utilises = 0;
			if(_RHIZOME_ARTICLE_DUREE > 0) $nb_criteres_utilises++;
			if(_RHIZOME_ARTICLE_FORUMS > 0) $nb_criteres_utilises++;
			if(_RHIZOME_ARTICLE_LONGUEUR > 0) $nb_criteres_utilises++;
			if(_RHIZOME_ARTICLE_POPULARITE > 0) $nb_criteres_utilises++;
			// On evite une division par 0 : si aucun critere n'est actif, l'indice vaut 1
			if($nb_criteres_utilises == 0)
				$indice = '1';
			else
			// Ponderation de la valeur brute de l'indice par le nombre de criteres utilises
			// NB: chacune a ici la meme ponderation - peut-etre qu'il faudra changer ca
			// On en profite pour arrondir a trois decimales
				$indice = round($indice / $nb_criteres_utilises, 3);

		break;

		// TODO: configuration fine comme pour case'article'
		case'mot':
			// On compte le nombre d'articles lies a ce mot
			$nb_article = spip_fetch_array(spip_query("SELECT
				COUNT(*) AS compteur
				FROM spip_mots_articles
				WHERE id_mot='$id'"));
			// On compte le nombre de sites syndiques lies a ce mot
			$nb_syndic = spip_fetch_array(spip_query("SELECT
				COUNT(*)AS compteur
				FROM spip_mots_syndic WHERE id_mot='$id'"));
			// On construit l'indice
			$indice_article = calculer_indice($nb_article['compteur'],20);
			$indice_syndic = calculer_indice($nb_syndic['compteur'],10);
			$indice = round(($indice_article * 3 / 4) + ($indice_syndic / 4) , 3);
		break;

		case'auteur':
			$nb_article = spip_query("SELECT COUNT(*) FROM spip_auteurs_articles WHERE id_auteur='$id'");
			$indice_article = calculer_indice($nb_article,50);
			$indice = $indice_article;
		break;

	}

	// Par construction, l'indice ne peut pas etre superieur a 1, mais on ne sait jamais
	if($indice > 1) $indice = 1;

	// Et on renvoie l'indice
	return $indice;
}


//
// Calculer un indice dont la valeur est comprise entre 0 et 1
//   Par defaut, simple fonction du premier degre
//   NB: Possibilite d'utiliser une fonction logarithmique en passant 'log' en troisieme argument
//
function calculer_indice($val, $max, $mode='') {
	// Si $max est egal a zero, l'indice le sera aussi
	if($max == 0)
		return 0;
	// Sinon, on calcule sa valeur entre 0 et 1
	else {
		if($val < 1) $val = 1;
		if($val > $max) $val = $max;
		if($mode=='log')
			return log($val,$max);
		else
			return $val/$max;
	}
}


//
// Trier le tableau
//
function tri_multi($tableau, $col, $direction = SORT_DESC) {
	foreach($tableau as $k=>$v){
		$cle[$k] = $v[$col];
	}
	array_multisort($cle, $direction, $tableau);
	return $tableau;
}

?>
