<?php

	#
	# MODE D'EMPLOI: Jeter ce fichier a la racine du site et le naviguer
	# puis comparer a http://www.useit.com/alertbox/traffic_logs.html
	#
	# (c) Fil 2006 distribue sous licence GNU/GPL
	#

	function graphe_truc($quoi, $chose) {

		if (!$chose) return;

		$width = 200; $height = 200;

		$image = imagecreatetruecolor($width, $height);

		$s = spip_query($chose);

		while ($t = spip_fetch_array($s)) {

			if ($quoi == 'popularite') $val = $t['v'];
			else $val = log($t['v']);

			// init
			if (!$i++) {
				$n = $width / log(spip_num_rows($s));
				$m = $height / $val;
			}

			$x = intval($n * log($i));
			$y = $height-intval($m * $val);

			imagesetpixel($image, $x, $y, 255*255);
		}

		return $image;
	}

	include('ecrire/inc_version.php');

	$choses = array (
	'popularite' => 'SELECT popularite AS v FROM spip_articles ORDER BY v DESC'
	,'visites' => 'SELECT visites AS v FROM spip_articles ORDER BY v DESC'
	,'referers' => 'SELECT visites AS v FROM spip_referers ORDER BY v DESC'
	#, 'referers_a' => 'SELECT visites AS v FROM spip_referers_articles ORDER BY v DESC'
	);

	if ($choses[$_GET['quoi']]) {
		$image = graphe_truc($_GET['quoi'], $choses[$_GET['quoi']]);
		header('Content-Type: image/png');
		imagepng($image);
	} else {
		header('Content-Type: text/html');
		foreach ($choses as $quoi => $rien)
			echo "<div>$quoi : <img src='".parametre_url(self(),'quoi',$quoi)."'></div>";

		echo "<div>cf. <a href='http://www.useit.com/alertbox/traffic_logs.html'>Traffic log patterns</a>";
	}

?>