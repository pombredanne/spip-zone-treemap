<?php
	$message = $this->GetMessage();
	$user = $this->GetUser();
	$multi = $GLOBALS['multi'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>


<head>
<title><?php echo $this->GetWakkaName().":".$this->GetPageTag(); ?></title>
<?php
if ($this->GetMethod() != 'show' || isset($_REQUEST["time"]))
	echo "<meta name=\"robots\" content=\"noindex, nofollow\"/>\n";
?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<meta name="keywords" content="<?php echo $this->GetConfigValue("meta_keywords") ?>" />
<meta name="description" content="<?php echo  $this->GetConfigValue("meta_description") ?>" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php if($multi) echo '/'; ?>wakka.basic.css" />
<style type="text/css" media="all"> @import "<?php if($multi) echo '/'; ?>wakka.css";</style>
<style type="text/css" media="all"> @import "<?php if($multi) echo '/'; ?>spip_style.css";</style>
<style type="text/css" media="print"> @import "<?php if($multi) echo '/'; ?>wakka.print.css";</style>
<script type="text/javascript">
function fKeyDown()	{
	if (event.keyCode == 9) {
		event.returnValue= false;
		document.selection.createRange().text = String.fromCharCode(9) } }
</script>
</head>


<body <?php echo (!$user || ($user["doubleclickedit"] != 'N')) && ($this->GetMethod() == "show") ? "ondblclick=\"document.location='".$this->href("edit")."';\" " : "" ?>
<?php echo $message ? "onLoad=\"alert('".$message."');\" " : "" ?> >


<h1 class="wiki_name"><?php echo $this->config["wakka_name"] ?></h1>


<h1 class="page_name">
<a href="<?php echo $this->config["base_url"] ?>RechercheTexte&amp;phrase=<?php echo urlencode($this->GetPageTag()); ?>">
<?php echo $this->GetPageTag(); ?>
</a>
</h1>


<div class="header">
<?php echo $this->ComposeLinkToPage($this->config["root_page"]); ?> ::
<?php echo $this->Format("DerniersChangements")." :: \n"; ?>
Vous &ecirc;tes <?php
echo $this->Format($this->GetUserName());
if ($user = $this->GetUser()) {
	echo " (<a href=\"../".
		parametre_url(
			parametre_url(SPIP_COOKIE,
				'logout_public', $user['login']
			),
			'url', $this->config["base_url"].$this->GetPageTag()
		)
	."\">D&eacute;connexion</a>)\n";
}
/*else if ($GLOBALS['login']) {
	include("spikini_login.php3");
	echo "</div>";
	exit;*/
else {
/*	echo " (<a
href=\"".$this->config["base_url"].$this->GetPageTag().($this->config["rewrite_mode"]?'?':'&')."login=oui&url=".urlencode($this->config['base_url'].$this->GetPageTag())."\">Connexion</a>)\n";*/
	echo " (<a href=\"../".parametre_url(SPIP_LOGIN,'url', $this->config["base_url"].$this->GetPageTag()).
		"\">Connexion</a>)\n";
}
?>
</div>


