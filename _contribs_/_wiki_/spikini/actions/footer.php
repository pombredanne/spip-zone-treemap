

<div class="footer">
<?php
//echo  $this->FormOpen("", "RechercheTexte", "get");
echo  $this->HasAccess("write") ? "<a href=\"".$this->href("edit")."\" title=\"Cliquez pour &eacute;diter cette page.\">&Eacute;diter cette page</a> ::\n" : "";
echo  $this->GetPageTime() ? "<a href=\"".$this->href("revisions")."\" title=\"Cliquez pour voir les derni&egrave;res modifications sur cette page.\">".$this->GetPageTime()."</a> ::\n" : "";
	// if this page exists
	if ($this->page)
	{
		// if owner is current user
		if ($this->UserIsOwner())
		{
			echo 
			"Propri&eacute;taire&nbsp;: vous :: \n",
			"<a href=\"",$this->href("acls")."\" title=\"Cliquez pour &eacute;diter les permissions de cette page.\">&Eacute;diter permissions</a> :: \n",
			"<a href=\"",$this->href("deletepage")."\">Supprimer</a> :: \n";
		}
		else
		{
			if ($owner = $this->GetPageOwner())
			{
				echo "Propri&eacute;taire : ",$this->Format($owner);
			}
			else
			{
				echo "Pas de propri&eacute;taire ";
				echo ($this->GetUser() ? "(<a href=\"".$this->href("claim")."\">Appropriation</a>)" : "");
			}
			echo " :: \n";
		}
	}
?>
<!--
<a href="<?php echo $this->href("referrers") ?>" title="Cliquez pour voir les URLs faisant r&eacute;f&eacute;rence &agrave; cette page.">Referrers</a>
-->
</div>

<div class="copyright">
Fonctionne avec <a href="http://www.uzine.net/spip_contrib/spikini/">Spikini</a>, une modif de <a href="http://www.wikini.net/wakka.php?wiki=PagePrincipale">WikiNi</a>
</div>

<?php
	if ($this->GetConfigValue("debug")=="yes")
	{
		echo "<span class=\"debug\"><b>Query log :</b><br />\n";
		$t_SQL=0;
        foreach ($this->queryLog as $query)
		{
			echo $query["query"]." (".round($query["time"],4).")<br />\n";
			$t_SQL = $t_SQL + $query["time"];
		}
		echo "</span>\n";

		echo "<span class=\"debug\">".round($t_SQL, 4)." s (total SQL time)</span><br />\n";
		
		list($g2_usec, $g2_sec) = explode(" ",microtime());
		define ("t_end", (float)$g2_usec + (float)$g2_sec);
		echo "<span class=\"debug\"><b>".round(t_end-t_start, 4)." s (total time)</b></span><br />\n";

		echo "<span class=\"debug\">SQL time represent : ".round((($t_SQL/(t_end-t_start))*100),2)."% of total time</span>\n";
	}
?> 


</body>
</html>
