<?php

// Inclure les fichiers spip de base
//
include 'ecrire/inc_version.php';

function convertir_charset($x) {
	return importer_charset($x, 'iso-8859-1');
}

#securite
if(!defined('SPIKINI')
OR $auteur_session['statut']<>'0minirezo')
	die('Erreur, rien a faire ici.');

include($configfile);
define ('wikini', $wakkaConfig['table_prefix']);
if (!wikini) die('erreur de lecture de la configuration '.$configfile);
if (!($_POST['importer'] == 'oui')) {

	if (!$id_rubrique=$_REQUEST['id_rubrique'])
		$id_rubrique = '??';
	die ("Importer ce spikini dans une rubrique de SPIP :
	<form action='./' method='post'>
	<input type='hidden' name='importer' value='oui' />
	<br/>id_rubrique: <input type='text' size='4' name='id_rubrique' value='$id_rubrique' />
	<br/>statut: <input type='text' size='8' name='statut' value='publie' />
	<br/><input type='submit' value='ok'/>
	</form>");
}

# importer dans la rubrique N avec le statut S
define ('import_statut', _request('statut'));
define ('import_id_rubrique', intval(_request('id_rubrique')));

$s = spip_query("SELECT * FROM spip_rubriques WHERE id_rubrique=".import_id_rubrique);
if (!$t = spip_fetch_array($s))
	die("la rubrique ".import_id_rubrique." n'existe pas");

define ('import_id_secteur', $t['id_secteur']);
define ('import_lang', $t['lang']);


###

# charger SPIP
include_spip('base/abstract_sql');
include_spip('inc/modifier');
include_spip('inc/charsets');
include_spip('inc/texte');
include_once 'spikini/formatters/wakka.php';

$titre = typo($t['titre']);

echo "<h3>F&#233;licitations. Le contenu de ce wiki a &#233;t&#233; import&#233; dans <a href='../ecrire/?exec=naviguer&amp;id_rubrique=".import_id_rubrique."'>la rubrique ".import_id_rubrique." ($titre)</a></h3>\n";

echo "<p>Vous pouvez maintenant utiliser cette rubrique en mode wiki</p>\n";
echo "<p>Pour desactiver ce wiki supprimez le fichier <b>$configfile</b></p>\n";


#$where="WHERE tag='PagePrincipale'"; #accelerer, pour tests

# insertion initiale des articles (pour avoir les liens)
# & nettoyage de la table des versions
$tags = array();
$s = spip_query("select * from ".wikini."pages $where ORDER BY time ASC");
while ($t = spip_fetch_array($s)) {
	if (!in_array($t['tag'], $tags)) {
		$id_article = inserer_version_spikini($t, true);
		$tags[] = $t['tag'];
	}
}


$tags = array();
$s = spip_query("select * from ".wikini."pages $where  order by time ASC");
while ($t = spip_fetch_array($s)) {
	if (in_array($t['tag'], $tags)) {
		// eviter de multiplier les latest (bug wikini)
		if ($t['user'] <> 'WikiNiInstaller')
			inserer_version_spikini($t);
	} else {
		$tags[] = $t['tag']; // la premiere version est deja la
	}
}


function inserer_version_spikini($spik, $purger_versions = false) {

	$s = spip_query("SELECT id_article FROM spip_articles
	WHERE id_rubrique=".import_id_rubrique."
	AND titre="._q($spik['tag']));

	if ($t = spip_fetch_array($s)) {
		$id_article = $t['id_article'];
	} else {
		$id_article = spip_abstract_insert('spip_articles',
		'(id_rubrique, id_secteur, lang, statut)',
		'('.import_id_rubrique.','.import_id_secteur.','._q(import_lang).','._q(import_statut).')');
	}

	// entrer la nouvelle revision
	$body = convertir_charset(
		($spik['latest'] == 'Y')
		? analyser_raccourcis($spik['body'])
		: $spik['body']
		)
		;

	$r = modifier_contenu('article', $id_article,
		array('champs' => array('titre', 'texte', 'date')),
		array(
			'titre' => convertir_charset($spik['tag']),
			'texte' => $body,
			'date' => $spik['time']
		)
	);

	if ($purger_versions) {
		spip_query("DELETE FROM spip_versions WHERE id_article=$id_article");
		spip_query("DELETE FROM spip_versions_fragments WHERE id_article=$id_article");
	} else {

		// regler la date de la revision a la date de son entree dans spikini
		// et son auteur a celui du modificateur de l'epoque
		$auteur = substr(convertir_charset($spik['user']),0, 23);
		spip_query("UPDATE spip_versions SET date="._q($spik['time']).", id_auteur="._q($auteur)." WHERE id_article=$id_article ORDER BY id_version DESC LIMIT 1");
	}

	echo "<br />importe <b>".$spik['tag']." ($spik[time], $purger_versions)</b> dans l'article $id_article\n";

	return $id_article;
}


// Traiter les LiensWiki
function lien_wiki($lien) {
	static $articles;

	if (!isset($articles[$lien])) {
		$s = spip_query("SELECT id_article FROM spip_articles WHERE id_rubrique=".import_id_rubrique." AND titre="._q($lien));
		if ($t = spip_fetch_array($s)) {
			$articles[$lien] = $t['id_article'];
			echo "<br />remplace $lien par $id_article;\n";
		} else
			$articles[$lien] = $lien;
	}

	return $articles[$lien];
}

function analyser_raccourcis($text) {


	$text = str_replace('[*[', '[[', $text);
	$text = str_replace(']*]', ']]', $text);

	list($text, $les_echap) = echappe_tags($text);


	if (preg_match_all("/\[\[(\S*)(\s+(.+))?\]\]/",
	$text, $r, PREG_SET_ORDER)) {
		foreach ($r as $regs) {
			$lien = lien_wiki($regs[1]);
			$text = preg_replace(',\b'.preg_quote($lien,',').'\b,',
				'[' . $r[3] .'->'.$lien.']', $text);

		}
	}


	if (preg_match_all("/\b[A-Z][a-z]+[A-Z,0-9][A-Z,a-z,0-9]*\b/s",
	$text, $r, PREG_SET_ORDER)) {
		foreach ($r as $regs) {
			$lien = lien_wiki($regs[0]);
			$text = preg_replace(',\b'.preg_quote($regs[0],',').'\b,',
				' [->'.$lien.'] ', $text);
		}
	}

	$text = echappe_tags_retour($text, $les_echap);

	return $text;
}



exit;

?>
