<?php

//
// Recuperer le sous-repertoire d'installation
//
if (preg_match(',/([^/]*)/[^/]*$,', __FILE__, $r)) 
	define('SPIKINI', $r[1].'/');
else 
	define('SPIKINI', 'spikini/');

chdir ('../');


// Inclure les fichiers spip de base
//
include 'ecrire/inc_version.php';
include_spip('inc/texte');

spip_connect();

{
	define('SPIP_LOGIN', 'spip.php?page=login');
	define('SPIP_COOKIE', 'spip.php?action=cookie');
}


if ($auteur_session) {
	$cs= lire_meta('charset');
	if($cs!="iso-8859-1") {
		include_spip('inc/charsets');
		$auteur_session['nom']= charset2unicode($auteur_session['nom']);
	}
}

// Hacks pour ne pas trop perturber wakka.php
unset($HTTP_SERVER_VARS['HTTP_ACCEPT_ENCODING']);
$_SERVER['PHP_SELF'] = str_replace('/index.php', '/wakka.php', $_SERVER['PHP_SELF']);
$_SERVER['PHP_SELF'] = str_replace('/multi.php', '/wakka.php', $_SERVER['PHP_SELF']);


?>
