function changestyle(id_couche, element, style) {
	if (!(layer = findObj(id_couche))) return;

	layer.style[element] = style;
}

function decalerCouche(id_couche) {
	if (!(layer = findObj(id_couche))) return;
	if (bug_offsetwidth && ( parseInt(layer.style.left) > 0)) {
		demilargeur = Math.floor( layer.offsetWidth / 2 );
		if (demilargeur == 0) demilargeur = 100; // bug offsetwidth MSIE, on fixe une valeur arbitraire
		gauche = parseInt(layer.style.left)
		  - demilargeur
		  + Math.floor(largeur_icone / 2);
		  		  
		if (gauche < 0) gauche = 0;

		layer.style.left = gauche+"px";
	}

}	

var accepter_change_statut;
	
function selec_statut(id, type, decal, puce, statut) {

	if (!accepter_change_statut) {
	  accepter_change_statut = confirm('confirm_changer_statut')
	}

	if (accepter_change_statut) {
		changestyle ('statutdecal'+type+id, 'marginLeft', decal+'px');
		cacher ('statutdecal'+type+id);

		findObj('imgstatut'+type+id).src = puce;
		frames['iframe_action'].location.href = 
  'iframe_action.php3?action=statut_'+ type +'&id='+id+'&statut='+statut;

	}
}
function changeclass(objet, myClass)
{
		objet.className = myClass;
}
function changesurvol(iddiv, myClass)
{
		document.getElementById(iddiv).className = myClass;
}
function setActiveStyleSheet(title) {
   var i, a, main;
   for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
	 if(a.getAttribute("rel").indexOf("style") != -1
		&& a.getAttribute("title")) {
	   a.disabled = true;
	   if(a.getAttribute("title") == title) a.disabled = false;
	 }
   }
}

function setvisibility (objet, statut) {
	element = findObj(objet);
	if (element.style.visibility != statut) element.style.visibility = statut;
}

function montrer(objet) {
	setvisibility(objet, 'visible');
}
function cacher(objet) {
	setvisibility(objet, 'hidden');
}

