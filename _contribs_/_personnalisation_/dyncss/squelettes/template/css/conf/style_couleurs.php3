<?
// ------------ ATTENTION VOUS RENTRER ICI DANS LE CHOIX DES COULEURS - CERTAINES COULEURS SONT UTILISEES POUR LE TEXTE MAIS S'AFFICHENT AUSSI SUR UNE COULEUR DE FOND. FAITES VOS CHOIX ET TESTER EN RECHARGEANT REGULIEREMENT LA PAGE  www.votrenomdedomaine/spipstyle-site.php3 .

// ------------ couleur des liens internet
$coul_lien['defaut']= "#003399";
$coul_survol['defaut']= "#808080";
$coul_visite['defaut']= "#336699";

// ------------ couleur de reference du site
$coul_primo['defaut']= "#4F7196";

// ------------ couleur secondaire par ex pour separer des zones via un filet ou pour des items d'infomations ou encore pour les notes
$coul_secundo['defaut']= "#669999";

// couleur pour les titres des rubriques, articles, breves, auteur etc...
$coul_tertio['defaut']= "#993333";

// par ex pour d'autres items : titre de bloc d'infos dans une page, descriptifs , chapeau, introduction
$coul_quatro['defaut']= "#993333";

// ------------  couleur pour lien, site, courriel
$coul_cinquo['defaut']= "#339999";

// ------------ par ex pour le texte bandeau haut ou bas sur fond de la couleur principale
$coul_navigatio['defaut']="#FFFFCC";

// ------------  couleur neutre, ici le blanc
$coul_neutre['defaut']= "#FFFFFF";

// ------------  couleur pour fond titre forum
$coul_fondtitreforum['defaut']= "#FFCC99";

// ------------  couleur pour texte courant, biblio des auteurs et dans les forums
$coul_texte['defaut']= "#000000";

// ------------ couleur en fond de textes de navigation
$coul_fondtextenavig['defaut']= "#808080";

// ------------  couleur de navigation texte, utilise aussi pour date, statistiques et fond de certaines colonnes
$coul_textenavig['defaut']= "#696969";

// ------------  couleur pour fond ponctuel d'un bloc d'info (titre ou texte)
$coul_fondponctuel['defaut']= "#DCDCDC";

// ------------ ATTENTION VOUS RENTRER ICI DANS LE CHOIX DES COULEURS - CERTAINES COULEURS SONT UTILISEES POUR LE TEXTE MAIS S'AFFICHENT AUSSI SUR UNE COULEUR DE FOND. FAITES VOS CHOIX ET TESTER EN RECHARGEANT REGULIEREMENT LA PAGE  www.votrenomdedomaine/spipstyle-site.php3 .

// ------------ couleur des liens internet
$coul_lien['couleur_perso']= "#00000";
$coul_survol['couleur_perso']= "#CCCCCC";
$coul_visite['couleur_perso']= "#666666";

// ------------ couleur de reference du site
$coul_primo['couleur_perso']= "#006699";

// ------------ couleur secondaire par ex pour separer des zones via un filet ou pour des items d'infomations ou encore pour les notes
$coul_secundo['couleur_perso']= "#003399";

// couleur pour les titres des rubriques, articles, breves, auteur etc...
$coul_tertio['couleur_perso']= "#669999";

// par ex pour d'autres items : titre de bloc d'infos dans une page, descriptifs , chapeau, introduction
$coul_quatro['couleur_perso']= "#F4A460";

// ------------  couleur pour lien, site, courriel
$coul_cinquo['couleur_perso']= "#9999CC";

// ------------ par ex pour le texte bandeau haut ou bas sur fond de la couleur principale
$coul_navigatio['couleur_perso']="#ffcc66";

// ------------  couleur neutre, ici le blanc
$coul_neutre['couleur_perso']= "#FFFFFF";

// ------------  couleur pour fond titre forum
$coul_fondtitreforum['couleur_perso']= "#FFCC99";

// ------------  couleur pour texte courant, biblio des auteurs et dans les forums
$coul_texte['couleur_perso']= "#000000";

// ------------ couleur en fond de textes de navigation
$coul_fondtextenavig['couleur_perso']= "#808080"; 

// ------------  couleur de navigation texte, utilise aussi pour date, statistiques et fond de certaines colonnes
$coul_textenavig['couleur_perso']= "#696969";

// ------------  couleur pour fond ponctuel d'un bloc d'info (titre ou texte)
$coul_fondponctuel['couleur_perso']= "#DCDCDC";

?>