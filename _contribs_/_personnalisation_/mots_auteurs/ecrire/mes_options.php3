<?
//courcy creation de la boucle mots_auteur 
$spip_mots_auteurs = array(
		"id_auteur"	=> "bigint(21) NOT NULL",
		"id_mot"	  => "bigint(21) NOT NULL");
		
		
$spip_mots_auteurs_key = array(
		"PRIMARY KEY"	=> "id_auteur",
		"KEY"					=> "id_mot"
		); 

$tables_auxiliaires['mots_auteurs']= array('field' => &$spip_mots_auteurs,'key' => &$spip_mots_auteurs_key);
$tables_principales['mots_auteurs']= array('field' => &$spip_mots_auteurs,'key' => &$spip_mots_auteurs_key);
$tables_relations['auteurs']['id_mot']='mots_auteurs';
$tables_relations['mots']['id_auteur']='mots_auteurs';

function boucle_MOTS_AUTEURS($id_boucle, &$boucles) {
	$boucle = &$boucles[$id_boucle];
	$boucle->from[] =  "spip_mots_auteurs AS " . $boucle->type_requete;
	return calculer_boucle($id_boucle, $boucles); 
}

?>