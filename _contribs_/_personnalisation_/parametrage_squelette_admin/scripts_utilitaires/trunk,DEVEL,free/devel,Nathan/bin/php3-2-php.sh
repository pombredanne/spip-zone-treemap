#!/bin/sh

touch=":"
from=".php3"
to=".php"

while [ $# -gt 0 ]
do
    case "$#x$1" in
	1x-f) break;;
	1x-t) break;;
	1x-s) break;;
	2x-s) break;;
	*x--touch) touch=touch;;
	*x-f) from="$2"; shift;;
	*x-t) to="$2"; shift;;
	*x-s) from="$2"; to="$3"; shift; shift;;
	*x-x) xxx="${from}"; from="${to}"; to="${xxx}";;
	* ) break;;
    esac
    shift
done

frommod="`echo "${from}" | sed -e 's/./[&]/g'`"
tomod="`echo "${to}" | sed -e 's/[/]/\\\\&/g'`"

for file
do
    if sed -e "s/${frommod}/${tomod}/g" "${file}" > "$$-file"
    then
	$touch -r "${file}" "$$-file"
	mv "$$-file" "${file}"
    fi
done

rename "${from}" "${to}" "$@"
