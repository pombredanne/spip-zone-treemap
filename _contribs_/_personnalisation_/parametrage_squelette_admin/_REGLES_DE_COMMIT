R�gles d'intervention
=====================

   1. Une seule personne autoris�e � la fois dans chaque zone de d�veloppement
      pour �viter de trop se marcher sur les pieds (les conflits se r�gleront �
      l'int�gration).

   2. Cette personne est nomm�e en fin de nom du r�pertoire.  sauf pour
      "*free*" qui signifie que l'on peut modifier le r�pertoire de sa propre
      initiative.

      Un cas particulier est le *r�pertoire racine du projet*: si quelqu'un
      veut adapter la contribution � une nouvelle version de Spip, il doit
      cr�er uniquement les r�pertoires suivants:

          * pour_SPIP_xxx/
                o trunk,DEVEL,free/
                      + adaptation,commiteur

      et travailler dans ce dernier.

   3. Changer un nom de r�pertoire est une d�cision coll�giale.


Parcours d'un d�veloppement
===========================

   1. On commence par proposer un d�veloppement de sa propre initiative
      [trunk,DEVEL,free/develx,Blake].

   2. Une fois termin� et fonctionnel, la main passe � l'int�grateur (une autre
      personne si possible) qui fusionne les modifications dans la branche de
      d�veloppement principale.  Il v�rifie que �a marche et que �a ne casse
      pas d'autres fonctionnalit�s.

   3. Apr�s un certain nombre d'int�grations dans la branche de d�veloppement
      principale, on d�cide de d�poser une nouvelle version stable.


Parcours d'une correction
=========================

   1. Si un bug est d�couvert dans la version xx, on le r�sout dans une branche
      de d�veloppement [trunk,DEVEL,free/MAINT,xx-xx+1,Joe].

   2. Apr�s correction, on d�pose la nouvelle version xx+1.

   3. Si on le d�cide, on peut r�percuter cette correction sur les versions
      suivant xx.

   4. Une autre personne si possible int�gre ensuite cette correction � la
      branche de d�veloppement principale afin de b�n�ficier de cette
      correction dans les futures versions.
