<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2005                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

include ("inc.php3");

include_ecrire ("inc_config.php3");
include_ecrire ("inc_presentation.php3"); // Pr�t pour Spip 1.9 (en 1.8.2e le test en
					  // entr�e emp�chera une seconde lecture)

debut_page(_T('confskel:titre_page_config_squelette'), "administration", "configuration");

if ($connect_statut != '0minirezo' OR !$connect_toutes_rubriques) {
	echo _T('avis_non_acces_page');
	fin_page();
	exit;
}

init_config();
if ($changer_config == 'oui') {
  if ($f = find_in_path("appliquer_modifs_config-squelette.php3"))
    include_local($f);
}

lire_metas();

echo "<br><br><br>";
gros_titre(_T('confskel:titre_page_config_squelette'));

barre_onglets("configuration", "squelette");

debut_gauche();

debut_droite();

	debut_boite_info();
	echo "<div class='verdana2' align='justify'>";
	echo http_img_pack("warning.gif", addslashes(_T('avis_attention')), "width='48' height='48' align='$spip_lang_right' style='padding-$spip_lang_left: 10px;'");
	echo "<p align='center'>";
	echo "<font size='+1'>\n";
	echo "<B>"._T('avis_attention')."</B><br>";
	echo "Cette page a �t� fournie par l'auteur du squelette d'affichage et\n";
	echo "<font color='red'>ne fait pas partie de la distribution standard de SPIP</font>.\n";
	echo "</font>\n";
	echo "</p>\n";
	echo "</div>";
	fin_boite_info();
	echo "<p>";

echo "<form action='config-squelette.php3' method='post'>";
echo "<input type='hidden' name='changer_config' value='oui'>";

if ($f = find_in_path("subconfig-squelette.php3"))
  include_local($f);

echo "</form>";

fin_page();

?>
