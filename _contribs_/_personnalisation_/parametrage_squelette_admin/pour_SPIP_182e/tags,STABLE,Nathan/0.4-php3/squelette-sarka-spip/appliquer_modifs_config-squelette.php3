<?php

include_ecrire("inc_skelvars.php3");

$fichier_skel_vars = find_in_path("variables_globales_squelette.php3");

if( $fichier_skel_vars && is_writable($fichier_skel_vars) ) {
  include_local($fichier_skel_vars);

  $var_skel_prefixe = 'sarka';

  $liste_vars_skel	= array(
				'rubriques_sommaire' => 'haut',
				'nombre_articles_recents' => '10'
				);

  $modif_vars = false;
  
  $unset_str = '___UNSET___';

  reset($liste_vars_skel);
  while (list($nom, $valeur) = each($liste_vars_skel)) {
    // Traitement des variables non encore positionn�es dans
    // variables_globales_squelette.php3 (affectation de la valeur par d�faut):
    if (lire_skelvar($var_skel_prefixe,$nom,$unset_str) == $unset_str) {
      ecrire_skelvar($var_skel_prefixe,$nom,$valeur);
      $modif_vars = true;
    }
    // Traitement des variables modifi�es par la page de configuration:
    $oldval = lire_skelvar($var_skel_prefixe,$nom,$unset_str);
    $newval = lire_form_skelvar($var_skel_prefixe,$nom,$oldval);
    if( $newval != $oldval ) {
      ecrire_skelvar($var_skel_prefixe,$nom,$newval);
      $modif_vars = true;
    }
  }

  if( $modif_vars ) {
    // Une nouvelle variable a �t� cr��e ou une variable a �t� modifi�e ==> il
    // faut r��crire le fichier variables_globales_squelette.php3

    $s = "\n";

    reset($liste_vars_skel);
    while (list($nom,) = each($liste_vars_skel)) {
      $s .= chaine_pour_ecrire_skelvar($var_skel_prefixe,$nom,lire_skelvar($var_skel_prefixe,$nom,$unset_str.$unset_str));
    }
    $s .= "\n";

    $ok = ecrire_fichier ($fichier_skel_vars,
			  '<'.'?php

if (defined("_VAR_GLOB_SKEL")) return;
define("_VAR_GLOB_SKEL", "1");
' 
			  . $s . '?'.">\n");
    if (!$ok && $GLOBALS['connect_statut'] == '0minirezo')
      echo "<h4 font color=red>"._T('texte_inc_skel_1')." <a href='../spip_test_dirs.php3'>"._T('texte_inc_skel_2')."</a> "._T('texte_inc_skel_3')."&nbsp;</h4>\n";
  }
 } else {
  echo "<h4 font color=red>"._T('fichier')." ".$fichier_skel_vars." "._T("n'est pas inscriptible")."&nbsp;</h4>\n";
 }

// echo "xxx Contenu du fichier $fichier_skel_vars:<pre>\n";
// $fd = fopen ($fichier_skel_vars, "r");
// while (!feof ($fd)) {
//   $buffer = fgets($fd, 4096);
//   if( !(strstr($buffer,"<"."?php") || strstr($buffer,"?".">")) ) {
//     if( strstr($buffer,"GLOBALS") && strstr($buffer,"sarka") ) {
//       echo "$buffer";
//     }
//   }
// }
// fclose ($fd);
// echo "</pre>\n";

?>
