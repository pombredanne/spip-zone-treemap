<?php

$skelvars = find_in_path("variables_globales_squelette.php3");

if( $skelvars && is_writable($skelvars) ) {
  include_local($skelvars);

  $var_skel_prefixe = 'sarka';

  $liste_vars_skel	= array(
				'rubriques_sommaire' => 'haut',
				'nombre_articles_recents' => '10'
				);

  $modif_vars = false;

  reset($liste_vars_skel);
  while (list($nom, $valeur) = each($liste_vars_skel)) {
    // Traitement des variables non encore positionn�es dans
    // variables_globales_squelette.php3 (affectation de la valeur par d�faut):
    if (!isset($GLOBALS[$var_skel_prefixe][$nom])) {
      $GLOBALS[$var_skel_prefixe][$nom] = $valeur;
      $modif_vars = true;
    }
    // Traitement des variables modifi�es par la page de configuration:
    if( isset($GLOBALS[$var_skel_prefixe."/".$nom]) && $GLOBALS[$var_skel_prefixe."/".$nom] != $GLOBALS[$var_skel_prefixe][$nom] ) {
      $GLOBALS[$var_skel_prefixe][$nom] = $GLOBALS[$var_skel_prefixe."/".$nom];
    $modif_vars = true;
    }
  }

  if( $modif_vars ) {
    // Une nouvelle variable a �t� cr��e ou une variable a �t� modifi�e ==> il
    // faut r��crire le fichier variables_globales_squelette.php3

    $s = "\n";

    reset($liste_vars_skel);
    while (list($nom,) = each($liste_vars_skel)) {
      $nom = addslashes($nom);
      $valeur = ereg_replace("([\\\\'])", "\\\\1", $GLOBALS[$var_skel_prefixe][$nom]);
      $s .= "\$GLOBALS['".$var_skel_prefixe."']['$nom'] = '$valeur';\n";
    }
    $s .= "\n";

    $ok = ecrire_fichier ($skelvars,
			  '<'.'?php

if (defined("_VAR_GLOB_SKEL")) return;
define("_VAR_GLOB_SKEL", "1");
' 
			  . $s . '?'.">\n");
    if (!$ok && $GLOBALS['connect_statut'] == '0minirezo')
      echo "<h4 font color=red>"._T('texte_inc_skel_1')." <a href='../spip_test_dirs.php3'>"._T('texte_inc_skel_2')."</a> "._T('texte_inc_skel_3')."&nbsp;</h4>\n";
  }
 } else {
  echo "<h4 font color=red>"._T('fichier')." ".$skelvars." "._T("n'est pas inscriptible")."&nbsp;</h4>\n";
 }

// echo "xxx Contenu du fichier $skelvars:<pre>\n";
// $fd = fopen ($skelvars, "r");
// while (!feof ($fd)) {
//   $buffer = fgets($fd, 4096);
//   if( !(strstr($buffer,"<"."?php") || strstr($buffer,"?".">")) ) {
//     if( strstr($buffer,"GLOBALS") && strstr($buffer,"sarka") ) {
//       echo "$buffer";
//     }
//   }
// }
// fclose ($fd);
// echo "</pre>\n";

?>
