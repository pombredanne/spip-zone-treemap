<?php

$GLOBALS['dossier_squelettes'] = "squelette-sarka-spip";
// $GLOBALS['dossier_squelettes'] = "squelettes/epona";

if ($f = find_in_path("variables_globales_squelette.php"))
	include_local ($f);

//  XXX REVUE de balise_SKELVAR � faire par Mortimer XXX

function balise_SKELVAR($p) {
  // Trois param�tres: $module $nom $defaut
  if( !$p->param || count($p->param[0]) != 4 || $p->param[0][0] ) {
    // Si il y a moins de param�tres qu'il n'en faut, c'est une erreur
    spip_log ("erreur balise SKELVAR"
	      ." nbparams=".count($p->param[0])
	      ." p->param=[".serialize($p->param)."]");
    echo "<p><font color=red>*** err SKELVAR err ***</font></p>\n";
    array_shift( $p->param );
    $p->code = "1";
    $p->status = "html";
    return $p;
  }
  $module =  calculer_liste($p->param[0][1],
			    $p->descr,
			    $p->boucles,
			    $p->id_boucle);
  $nom = calculer_liste($p->param[0][2],
			$p->descr,
			$p->boucles,
			$p->id_boucle);
  $defaut = calculer_liste($p->param[0][3],
			   $p->descr,
			   $p->boucles,
			   $p->id_boucle);
  // autres filtres
  array_shift($p->param);

  // Int�grer ici la table de "traduction":
  //      ($module,$nom) ==> ($modulex,$nomx)
  // permettant d'"accrocher" les param�tres d'un module � un autre.
  // Ici, cela signifie que le param�tre $nom du module $module est �
  // remplacer par le param�tre $nomx du module $modulex.

  // J'imagine que cette table serait d�finie dans ce fichier m�me
  // mais on peut imaginer que ce soit une possibilit� �tendue du
  // param�trage des squelettes: au lieu de simplement mettre une
  // valeur pour un param�tre, on pourrait s�lectionner un autre
  // squelette/module/jeu de param�tres et un param�tre dans la liste.
  // Dans ce cas, il faudra peut-�tre "typer" les param�tres: couleur,
  // dimension, position, ...

  // Cette balise peut permettre de traiter les jeux de param�tres que l'on
  // nommeraient alors $module.  Cela demandera de la normalisation
  // des noms (typage?).

  if( eval("!\$GLOBALS[$module][$nom]") ) {
    $p->code = $defaut;
  } else {
    $p->code = "\$GLOBALS[$module][$nom]";
  }
  $p->statut = 'php';

  return $p;
}

?>
