<?php

//
// Ce fichier ne sera execute qu'une fois
if (defined("_ECRIRE_INC_SKELVAR")) return;
define("_ECRIRE_INC_SKELVAR", "1");

function lire_skelvar($module,$nom,$defaut) {
  return sinon($GLOBALS['skel_params'][$module][$nom],$defaut);
}

function form_skelvar_name($module,$nom) {
  return "skel_params/".$module."/".$nom;
}

function lire_form_skelvar($module,$nom,$defaut) {
  return sinon($GLOBALS[form_skelvar_name($module,$nom)],$defaut);
}

function ecrire_skelvar($module,$nom,$valeur) {
  $GLOBALS['skel_params'][$module][$nom] = $valeur;
}

function chaine_pour_ecrire_skelvar($module,$nom,$valeur) {
  return
    "\$GLOBALS['skel_params']" .
    "['" . addslashes($module) . "']" .
    "['" . addslashes($nom) . "']" .
    " = '" . addslashes($valeur) . "';\n";
}

// On force lire_skelvars() si le cache n'a pas ete utilise
if (!isset($GLOBALS['skel_params']))
  if ($f = find_in_path("variables_globales_squelette.php3"))
    include_local ($f);

?>
