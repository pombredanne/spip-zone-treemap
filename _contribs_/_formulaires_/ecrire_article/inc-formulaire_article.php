<?php

if (!defined("_ECRIRE_INC_VERSION")) return;	#securite

include_ecrire ("inc_date.php3");
include_ecrire ("inc_texte.php3");

// Le contexte indique dans quelle rubrique le visiteur peut proposer l article
global $balise_FORMULAIRE_ARTICLE_collecte;
$balise_FORMULAIRE_ARTICLE_collecte = array('id_rubrique');

function balise_FORMULAIRE_ARTICLE_stat($args, $filtres) {

	// Pas d'id_rubrique, ni en contexte, ni en parametre ? Erreur de squelette
	if (!$args[0] && !$args[1])
		return erreur_squelette(
			_T('zbug_champ_hors_motif',
				array ('champ' => '#FORMULAIRE_ARTICLE',
					'motif' => 'RUBRIQUES')), '');

	if ($args[1])
	    return (array($args[1]));
	else
	    return (array($args[0]));
}

function balise_FORMULAIRE_ARTICLE_dyn($id_rubrique) {
	global $REMOTE_ADDR, $afficher_texte, $_COOKIE, $_POST;

	if ($GLOBALS["auteur_session"]) {
		$id_auteur_session = $GLOBALS['auteur_session']['id_auteur'];
		$statut_auteur=$GLOBALS["auteur_session"]["statut"];
		$nom_auteur_session = $GLOBALS['auteur_session']['nom'];
		$email_auteur_session = $GLOBALS['auteur_session']['email'];
	} else {
		$nom_auteur_session= _request('nom_auteur_session');
		$email_auteur_session= _request('email_auteur_session');
	}

	//TODO : utiliser un meta ?
	$articles_publics = $GLOBALS['articles_publics'];
	// statut de l'article, ou formulaire de login en fonction de la configuration choisie
	if ((($articles_publics == "abo") && (!$GLOBALS["auteur_session"]))
	||(($articles_publics == "redac") && (($statut_auteur != "1comite") && ($statut_auteur != "0minirezo")))) {
		return array('formulaire_login_forum', 0, array());
	}
	elseif ($statut_auteur != "0minirezo") {
		$statut= addslashes(_request('statut'));
		//TODO : gerer les admins restreints
		$id_rubrique= intval(_request('id_rubrique'));
	}
	else{
		$statut= "prop";
	}
	
	//TODO : voir comment on passe url 
	//=> filtre ?
	// url de reference
	if (!$url) {
		$url = new Link();
		$url = $url->getUrl();
		}


	//TODO : voir comment trouver le bon document_root : chez altern
	//$document_root="/var/alternc/dns/o/nomdedomaine/";
	$document_root=$_SERVER['DOCUMENT_ROOT'];

	//Pour gestion logo
	// Recuperer les variables d'upload
	if (!is_array($_FILES))
		$_FILES = array();
	foreach ($_FILES as $id => $file) {
		if ($file['error'] == 4 /* UPLOAD_ERR_NO_FILE */)
			unset ($_FILES[$id]);
	}
	
	
	$surtitre= addslashes(_request('surtitre'));	
	$titre= addslashes(_request('titre'));
	$soustitre= addslashes(_request('soustitre'));
	$descriptif= addslashes(_request('descriptif'));
	$chapo= addslashes(_request('chapo'));
	$texte= addslashes(_request('texte'));
	$ps= addslashes(_request('ps'));
	$lien_titre= addslashes(_request('lien_titre'));
	$lien_url= addslashes(_request('lien_url'));
	$annee= addslashes(_request('annee'));
	$mois= addslashes(_request('mois'));
	$jour= addslashes(_request('jour'));
	
	$lang = _request('var_lang');	
	$nom = 'changer_lang';
	lang_dselect();
	$langues = liste_options_langues($nom, $lang);
	
	// retourver le secteur et la langue de la rubrique
	$s = spip_query("SELECT id_secteur, lang FROM spip_rubriques WHERE id_rubrique = '$id_rubrique' ");
	if ($r = spip_fetch_array($s)) {
		$id_secteur = $r["id_secteur"];
		$lang = $r["lang"];
	}
	
	$previsualiser= _request('previsualiser');
	$valider= _request('valider');
	
	$previsu = '';
	$bouton= '';
	
	if($valider){
		$time=time();
		$date_redac=date('Y-m-d H:i:s',$time);

		$formulaire_date = format_mysql_date($annee, $mois, $jour);
		
		// ajouter l'article (sans auteur) dans la base
		//statut: 'prop' ou 'publie'
		spip_query("INSERT INTO spip_articles (surtitre, titre, soustitre, id_rubrique, descriptif, chapo, texte, ps, statut, id_secteur, accepter_forum, auteur_modif, date_modif, lang, date, date_redac, id_version, nom_site, url_site) VALUES ('$surtitre', '$titre', '$soustitre', '$id_rubrique', '$descriptif', '$chapo', '$texte', '$ps', '$statut', '$id_secteur', 'pos', '$id_auteur_session', '$formulaire_date', '$lang',  '$formulaire_date', '$date_redac', '1', '$lien_titre', '$lien_url')");
		// aller chercher l'identifiant de l'article cr��
		$id_article=mysql_insert_id();
		// ajouter l'auteur
		if($id_auteur_session != 0){
			spip_query("INSERT INTO spip_auteurs_articles (id_auteur, id_article) VALUES ($id_auteur_session, $id_article)");
		}
		//recuperer logos
		if(_request("fic_logo1")) 
			rename($document_root."/"._request("fic_logo1"), $document_root."/IMG/arton".$id_article.strrchr(_request("fic_logo1"),'.'));
		if(_request("fic_logo2")) 
			rename($document_root."/"._request("fic_logo2"), $document_root."/IMG/artoff".$id_article.strrchr(_request("fic_logo2"),'.'));
		
		if (strlen($titre) < 3){$erreur .= _T('forum_attention_trois_caracteres');}
		if(!$erreur){$bouton= _T('form_prop_confirmer_envoi');}
		
		if ($statut=='prop'){
			include_ecrire("inc_mail.php3");
			envoyer_mail_proposition($id_article);
		}
		elseif ($statut=='publie'){
			//invalider cache article
			//quand on fera les modifs
			//suivre_invalideur("id='id_article/mId'");
			//invalider cache rubrique
			suivre_invalideur("id='id_rubrique/$id_rubrique'");
		}
		//TODO : recaculer rubrique ?
		
		return  _T('form_prop_enregistre');
	}
	else{
		if($previsualiser){
			$imglogo1="";
			$imglogo2="";
			if ($_FILES['logo1']['name']){
				$ext1=strrchr($_FILES['logo1']['name'],'.');
				$myFile1 = $document_root."/IMG/logo1".$id_auteur_session.$ext1;
				if (file_exists($myFile1)) @unlink($myFile1);	
				//spip_log($_FILES['logo1']['tmp_name']."=>".$myFile1);
				rename($_FILES['logo1']['tmp_name'], $myFile1);
				$imglogo1="IMG/logo1".$id_auteur_session.$ext1;
			}
			if ($_FILES['logo2']['name']){
				$ext2=strrchr($_FILES['logo2']['name'],'.');
				$myFile2 = $document_root."/IMG/logo2".$id_auteur_session.$ext2;
				if (file_exists($myFile2)) @unlink($myFile2);	
				//spip_log($_FILES['logo2']['tmp_name']."=>".$myFile2);
				rename($_FILES['logo2']['tmp_name'], $myFile2);
				$imglogo2="IMG/logo2".$id_auteur_session.$ext2;
			}
	
			if (strlen($titre) < 3){$erreur .= _T('forum_attention_trois_caracteres');}
			if(!$erreur){$bouton= _T('form_prop_confirmer_envoi');}
	
			if ($imglogo1=="")$imglogo1=_request("fic_logo1");
			if ($imglogo2=="")$imglogo2=_request("fic_logo2");
	//		$img_logo1="<img src=\"".$imglogo1."\" />";
	//		$img_logo2="<img src=\"".$imglogo2."\" />";
			$img_logo="<img src=\"".$imglogo1."\" alt='".addslashes($titre)."' width=\"105\" height=\"105\" onmouseover=\"this.src='".$imglogo2."'\" onmouseout=\"this.src='".$imglogo1."'\" style=\"border-width: 0px;\" class=\"spip_logos\" />";
			$input_logo1="<input name=\"fic_logo1\" type=\"hidden\" value=\"".$imglogo1."\" />";
			$input_logo2="<input name=\"fic_logo2\" type=\"hidden\" value=\"".$imglogo2."\" />";
			
			$previsu = inclure_balise_dynamique(
				array(
					'formulaire_signature_previsu',
					0,
					array(
						'surtitre' => interdire_scripts(typo($surtitre)),
						'titre' => interdire_scripts(typo($titre)),
						'soustitre' => interdire_scripts(typo($soustitre)),
						'descriptif' => propre($descriptif),
						'chapo' => propre($chapo),
						'texte' => propre($texte),
						'ps' => propre($ps),
						'lien_titre' => $lien_titre,
						'lien_url' => $lien_url,
						'erreur' => $erreur,
						'bouton' => $bouton,
						'logo' => $img_logo,
					)
				), false);
				//Ouch ...
				$previsu = preg_replace("@<(/?)f(orm[>[:space:]])@ism",
				"<\\1no-f\\2", $previsu);
		}

		return array('formulaire_signature', 0,
		array(
					'logo1'=>formlogo('logo1'),
					'logo2'=>formlogo('logo2'),
					'imglogo1'=>$input_logo1,
					'imglogo2'=>$input_logo2,
					'annee'=>$annee,
					'mois'=>$mois,
					'jour'=>$jour,
					'url' =>  $url,
					'langues' => $langues,
					'previsu' => $previsu,
					'surtitre' => $surtitre,
					'titre' => interdire_scripts(typo($titre)),
					'soustitre' => $soustitre,
					'descriptif' => $descriptif,
					'chapo' => $chapo,
					'texte' => $texte,
					'ps' => $ps,
					'lien_titre' => $lien_titre,
					'lien_url' => $lien_url,
					'id_rubrique' => $id_rubrique,
					'id_secteur' => $id_secteur,
					'id_auteur_session' => $id_auteur_session,
					'nom_auteur_session' => $nom_auteur_session,
					'email_auteur_session' => $email_auteur_session
			));
	}

}

function barre_article($texte,$name='texte')
{
	include_ecrire('inc_layer.php3');

	if (!$GLOBALS['browser_barre'])
		return "<textarea name='texte' rows='12' class='forml' cols='40'>$texte</textarea>";
	static $num_formulaire = 0;
	$num_formulaire++;
	include_ecrire('inc_barre.php3');
	return afficher_barre("document.getElementById('formulaire_$num_formulaire')", true) .
	  "<textarea name='$name' rows='12' class='forml' cols='40'
				id='formulaire_$num_formulaire'
				onselect='storeCaret(this);'
				onclick='storeCaret(this);'
				onkeyup='storeCaret(this);'
				ondbclick='storeCaret(this);'>$texte</textarea>";
}

function logoauteur($id_auteur, $formats = array ('gif', 'jpg', 'png')) {
	reset($formats);
	while (list(, $format) = each($formats)) {
		$d = _DIR_IMG . "auton$id_auteur.$format";
		if (@file_exists($d)) return $d;
	}
	return  '';
}
//TODO : ameliorer un peu, voir comment utiliser inc_logo 
//(en faisant une fonction return au lieu des echo ...)
function formlogo($name='image')
{
		$returned.="\n<input id='$name' name='$name' type='file' class='forml' SIZE=15>";
		return $returned;	
}

?>
