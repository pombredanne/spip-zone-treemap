<?php

if (!defined("_ECRIRE_INC_VERSION")) return;


// #FORMULAIRE_CONNEXION
function formulaires_connexion_charger_dist($listes='') {

	$email = sinon(
		$GLOBALS['visiteur_session']['session_email'],
		$GLOBALS['visiteur_session']['email']
	);
	$nom = sinon(
		$GLOBALS['visiteur_session']['session_nom'],
		$GLOBALS['visiteur_session']['nom']
	);

	$v = array(
		'email' => $email,
		'nom' => $nom
	);

	if (!$email)
		return $v;

	$confirme = (
		$GLOBALS['visiteur_session']['statut']
		OR
		($GLOBALS['visiteur_session']['email_confirme'] == $email)
	);

	if (!$confirme
	AND $GLOBALS['visiteur_session']['email_envoye'] == $email) {

		if (strlen(_request('p'))
		AND _request('p') == $GLOBALS['visiteur_session']['email_cookie']) {
			// ok bravo tu viens de confirmer
			session_set('email_confirme', $email);
			$v['email_ok'] = _L('Bravo !');
			$confirme = true;
		}
		else
			// indiquer qu'on attend la reponse
			$v['email_attente'] = _L('email en attente de confirmation');
	}

	if ($confirme) {
		$v['email_confirme'] = ' ';
	}

	return $v;
}

// verifier le contenu et couiner si c'est pas ok ;
// si c'est OK traiter() sera appele.
function formulaires_connexion_verifier_dist() {
	$erreurs = array();

	if (strlen($email = ""._request('session_email'))) {
		include_spip('inc/filtres');
		if (!email_valide($email))
			$erreurs['email'] = 'Adresse invalide';
	}

	return $erreurs;
}

// traiter les donnees postees
function formulaires_connexion_traiter_dist() {
	// traiter les _request() et batir une reponse

	$email = $GLOBALS['visiteur_session']['session_email'];
	$confirme = $GLOBALS['visiteur_session']['email_confirme'] == $email;

	// Si l'email n'est pas encore confirme, il y a tout le
	// processus de confirmation a etablir
	if (!$confirme) {
		if (
		// si on vient deja d'envoyer l'email a cette adresse
		// ne pas le renvoyer
		$GLOBALS['visiteur_session']['email_envoye'] !== $email
		
		// si c'est un email de spip_auteurs, demander le mot de pass
		AND !$reponse = verifier_email_spip_auteurs($email)

		) {
			// envoi de l'email
			if (envoyer_mail_confirmation($email))
				$reponse = _L('email envoy&#233; pour confirmation');
			else
				$reponse = _L('envoi de l\'email : erreur');
		}
	}

	// renvoyer array($editable, 'reponse')
	return array(true,$reponse);
}

function cle_secrete_confirmer($email) {
	return substr(md5(uniqid('')), 0,8);
}

function envoyer_mail_confirmation($email) {
	$p = cle_secrete_confirmer($email);
	session_set('email_cookie', $p);

	$subject = _L('Confirmation de votre adresse email');
	
	$texte = _L('  Vous avez demand&#233; &#224; vous inscrire sur le site
    @site@

  Veuillez suivre le lien ci-dessous pour confirmer votre
  adresse email :

    @url@

  Merci !
', array('site' => $GLOBALS['meta']['nom_site'],
	'url' => url_absolue(parametre_url(nettoyer_uri(), 'p', $p, '&'))
	));

	$envoyer_mail = charger_fonction('envoyer_mail', 'inc');
	$ok = $envoyer_mail($email, $subject, $texte);

	session_set('email_envoye', $email);

	return $ok;
}

function verifier_email_spip_auteurs($email) {
	include_spip('base/abstract_sql');
	if ($r = sql_fetsel('nom,login,statut', 'spip_auteurs', array('email='.sql_quote($email)))) {
		if ($r['statut'] == '5poubelle')
			return _L("Vous n'&#234;tes pas autoris&#233; &#224; vous connecter &#224; ce site.");
		else
			return _L("Pour cet email une connexion &#224; SPIP est requise")
			." ... <a href='"
			. generer_url_public('login', 'url='.urlencode(self('&')))
			."'>connexion</a>";
	}
}