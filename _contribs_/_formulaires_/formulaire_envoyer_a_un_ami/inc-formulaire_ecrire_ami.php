<?php

if (!defined("_ECRIRE_INC_VERSION")) return; #securite

global $balise_FORMULAIRE_ECRIRE_AMI_collecte;
$balise_FORMULAIRE_ECRIRE_AMI_collecte = array('id_breve', 'id_article');

	include_ecrire('inc_texte.php');
	include_ecrire("inc_mail.php");
	include_ecrire("inc_filtres.php");

function balise_FORMULAIRE_ECRIRE_AMI_stat($args, $filtres) {

	// Pas d'id_auteur ni d'id_article ? Erreur de squelette
	if (!$args[0] AND !$args[1])
		return erreur_squelette(
			_T('zbug_champ_hors_motif',
				array ('champ' => '#FORMULAIRE_ECRIRE_AMI',
					'motif' => 'BREVES/ARTICLES')), '');

	// OK
	return $args;
}

function balise_FORMULAIRE_ECRIRE_AMI_dyn($id_article) {
	$puce = $GLOBALS['puce'.$GLOBALS['spip_lang_rtl']];
	$destinataire = _request('email_destinataire');
	$sujet = _request('sujet_message_auteur');
	$adres = _request('email_message_auteur');
	$texte = _request('texte_message_auteur');
	$urlevent = _request('urlevent');
	$nom_expediteur = _request('nom_expediteur');
	$confirmation = _request('confirmation');
	
	if ($confirmation == "yes"){
		$mailconfirm = _T('confirmation_a').$adres;
	}
	else {
		$mailconfirm = _T('confirmation_sans');
	}
	$sujetko = $texte && !$sujet;
	$mailko = $texte && !email_valide($adres);
	$destmailko = $texte && !email_valide($destinataire);
	$nomko = $texte && !$nom_expediteur;

	$texte3 = htmlspecialchars($texte);
	$texte3 .= "\n\n"._T('consulter_event')."\n$urlevent";
	
	if ($sujetko OR $mailko OR $destmailko OR $nomko){
		$erreur = ' ';
	}

	// doit-on envoyer le mail ?

	$validable = $texte && (!$mailko) && (!$destmailko) && (!$sujetko) && (!$nomko);
	if ($validable
	AND $id == _request('num_formulaire_ecrire_ami')
	AND _request('confirmer'.$id)) {
		$texte2 = ""._T('bonjour')."\n\n$nom_expediteur ($adres) "._T('pense_a_vous_event')." "._T('avec_message')."\n\n";
		$texte2 .= $texte;
		$texte2 .= "\n\n"._T('consulter_event')."\n$urlevent";
		$texte2 .= "\n\n"._T('soin_de_vous')."\n\nSklunk.net";
		$texte2 .= "\n\n-- "._T('envoi_via_le_site')." ".supprimer_tags(extraire_multi(lire_meta('nom_site')))." (".lire_meta('adresse_site')."/) --\n";
		$sujet2 = lire_meta('nom_site');
		$sujet2 .= $sujet;
		envoyer_mail($destinataire, $sujet2, $texte2, $adres,
				"X-Originating-IP: ".$GLOBALS['REMOTE_ADDR']);
		if ($confirmation == "yes"){
			$textconfirm = ""._T('confirmation_texte')."\n$destinataire";
			$textconfirm .= "\n\n"._T('soin_de_vous')."\n\nSklunk.net";
			$textconfirm .= "\n\n-- "._T('envoi_via_le_site')." ".supprimer_tags(extraire_multi(lire_meta('nom_site')))." (".lire_meta('adresse_site')."/) --\n";
			$sujetconfirm = lire_meta('nom_site')." - "._T('confirmation_sujet');
			envoyer_mail($adres, $sujetconfirm, $textconfirm, $adres,
				"X-Originating-IP: ".$GLOBALS['REMOTE_ADDR']);
			}
		else {
			return $erreur;
		}
		return _T('form_prop_message_envoye')."<br />cliquez <a href=\"$urlevent\">ici</a> si vous souhaitez envoyer un nouveau mail..." ;
	}

	return
		array('formulaire_ecrire_ami', 0,
			array(
			'id' => $id,
			'mailko' => $mailko ? $puce : '',
			'mail' => $adres,
			'nom_expediteur' => $nom_expediteur,
			'auteur' => $auteur,
			'destinataire' => $destinataire,
			'sujetko' => $sujetko ? $puce : '',
			'destmailko' => $destmailko ? $puce : '',
			'nomko' => $nomko ? $puce : '',
			'erreur' => $erreur,
			'sujetfinal' => $sujet,
			'sujet' => $sujet,
			'textefinal' => $texte3,
			'texte' => $texte,
			'valide' => ($validable ? $id : ''),
			'mailconfirm' => $mailconfirm,
			'bouton' => (_T('form_prop_envoyer')),
			'boutonconfirmation' => ($validable ? _T('form_prop_confirmer_envoi') : '')
			)
		);
}
?>