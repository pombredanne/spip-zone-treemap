<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'messagedefinitif' => 'Voici le message qui va &ecirc;tre envoy&eacute; &agrave; votre correspondant :',
'emmetteur_nom' => 'Entrez votre nom',
'destinataire_email' => 'Entrez l\'email du destinataire',
'form_pet_email_dest' => 'Veuillez indiquer une adresse valide pour le destinataire',
'form_pet_nom' => 'Veillez indiquer votre nom',
'recevoir_confirmation' => 'Recevoir une confirmation'
);


?>