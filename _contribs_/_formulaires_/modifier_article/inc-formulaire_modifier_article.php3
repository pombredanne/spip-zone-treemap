<?php

if (!defined("_ECRIRE_INC_VERSION")) return;	#securite

include_ecrire ("inc_presentation.php3");


global $balise_FORMULAIRE_MODIFIER_ARTICLE_collecte;
$balise_FORMULAIRE_MODIFIER_ARTICLE_collecte = array('id_article','id_rubrique','statut');

function balise_FORMULAIRE_MODIFIER_ARTICLE_stat($args, $filtres) {

	// Pas d'id_rubrique ? Erreur de squelette
	if (!$args[0] && !$args[1])
		return erreur_squelette(
			_T('zbug_champ_hors_motif',
				array ('champ' => '#FORMULAIRE_MODIFIER_ARTICLE',
					'motif' => 'ARTICLES')), '');

	if ($args[3])
	    return (array($args[3],$args[4],$args[5]));
	else
	    return (array($args[0],$args[1],$args[2]));
}

function balise_FORMULAIRE_MODIFIER_ARTICLE_dyn($id_article, $id_rubrique, $statut_article) {

$message='';
$gerer_surtitre = (lire_meta("articles_surtitre")=='oui');
$gerer_soustitre = (lire_meta("articles_soustitre")=='oui');
$gerer_descriptif = (lire_meta("articles_descriptif")=='oui');
$gerer_urlref = (lire_meta("articles_urlref")=='oui');
$gerer_chapeau = (lire_meta("articles_chapeau")=='oui');
$gerer_ps = (lire_meta("articles_ps")=='oui');
$gerer_redac = (lire_meta("articles_redac")=='oui');
$gerer_mots = (lire_meta("articles_mots")=='oui');
$gerer_modif = (lire_meta("articles_modif")=='oui');
$gerer_lang = (lire_meta("multi_articles")=='oui');

	if (!$GLOBALS["auteur_session"]) 
		return array('formulaire_afficher_article', 0, 
					array(
						'id_article' => $id_article,
						'message' => $message,
						'gerer_surtitre' => $gerer_surtitre?' ':'',
						'gerer_soustitre' => $gerer_soustitre?' ':'',
						'gerer_descriptif' => $gerer_descriptif?' ':'',
						'gerer_urlref' => $gerer_urlref?' ':'',
						'gerer_chapeau' => $gerer_chapeau?' ':'',
						'gerer_ps' => $gerer_ps?' ':'',
						'gerer_lang' => $gerer_lang?' ':'',
						'gerer_redac' => $gerer_redac?' ':'',
						'gerer_mots' => $gerer_mots?' ':'',
						'gerer_modif' => $gerer_modif?' ':''					
				));
	
	$auteur_statut=$GLOBALS["auteur_session"]["statut"];
	$id_auteur_session = $GLOBALS['auteur_session']['id_auteur'];

	global $gestion_droits,$droit_vue,$droit_modif;
	include_ecrire("inc_auth.php3");

	$query = "SELECT * FROM spip_auteurs_articles WHERE id_article=".$id_article." AND id_auteur=".$id_auteur_session;
	$result_auteur = spip_query($query);
	$flag_auteur = (spip_num_rows($result_auteur) > 0);

	$flag_modif= ((acces_rubrique($id_rubrique) && ($auteur_statut=='0minirezo'))
	OR ($flag_auteur AND ($statut_article == 'prepa' OR $statut_article == 'prop' OR $statut_article == 'poubelle')));
	
	$form_prefixe= 'fma'.$id_article.'_';
	$valider= (_request($form_prefixe.'valider')=='oui');
	$editer= (_request($form_prefixe.'editer')=='oui');

	if ($flag_modif){ 
		$url_edit = new Link();
		$url_edit->addVar($form_prefixe.'editer','oui');
		$url_edit = $url_edit->getUrl();
		$url = new Link();
		$url->delVar($form_prefixe.'editer');
//		$url->addVar('var_mode','calcul');
		$url->addVar($form_prefixe.'valider','oui');
		$fma_form=$url->getForm('POST','','multipart/form-data');
	}
	if (($flag_modif)&&($valider)){
		$surtitre= addslashes(stripslashes(_request($form_prefixe.'surtitre')));	
		$titre= addslashes(stripslashes(_request($form_prefixe.'titre')));
		$soustitre= addslashes(stripslashes(_request($form_prefixe.'soustitre')));
		$descriptif= addslashes(stripslashes(_request($form_prefixe.'descriptif')));
		$chapo= addslashes(stripslashes(_request($form_prefixe.'chapo')));
		$texte= addslashes(stripslashes(_request($form_prefixe.'texte')));
		$ps= addslashes(stripslashes(_request($form_prefixe.'ps')));
		$nom_site= addslashes(stripslashes(_request($form_prefixe.'lien_titre')));
		$url_site= addslashes(stripslashes(_request($form_prefixe.'lien_url')));
		$annee= addslashes(stripslashes(_request($form_prefixe.'annee_date')));
		$mois= addslashes(stripslashes(_request($form_prefixe.'mois_date')));
		$jour= addslashes(stripslashes(_request($form_prefixe.'jour_date')));
		$date = format_mysql_date($annee, $mois, $jour);
		$annee_redac= addslashes(stripslashes(_request($form_prefixe.'annee_date_redac')));
		$mois_redac= addslashes(stripslashes(_request($form_prefixe.'mois_date_redac')));
		$jour_redac= addslashes(stripslashes(_request($form_prefixe.'jour_date_redac')));
		$date_redac = format_mysql_date($annee_redac, $mois_redac, $jour_redac);
		$lang = addslashes(stripslashes(_request($form_prefixe.'lang')));	
		// int�grer � la base de donn�es
		$time=time();
		$date_redac=date('Y-m-d H:i:s',$time);
		
		// modifier l'article
		$query="update `spip_articles` SET";
		$query.=" `titre`='".$titre."',";
		if ($gerer_surtitre) $query.=" `surtitre`='".$surtitre."',";
		if ($gerer_soustitre) $query.=" `soustitre`='".$soustitre."',";
		//$query.=" `id_rubrique`=".$id_rubrique.",";
		if ($gerer_descriptif) $query.=" `descriptif`='".$descriptif."',";
		if ($gerer_chapeau) $query.=" `chapo`='".$chapo."',";
		$query.=" `texte`='".$texte."',";
		if ($gerer_ps) $query.=" `ps`='".$ps."',";
		if ($gerer_lang) $query.=" `lang`='".$lang."',";
		$query.=" `date`='".$date."',";
		if ($gerer_redac) $query.=" `date_redac`='".$date_redac."','";
		if ($gerer_urlref) {
			$query.=" `nom_site`='".$nom_site."','";
			$query.=" `url_site`='".$url_site."';";
		}
		$query.=" `date_modif`=now()";
		$query.=" where `id_article`=".$id_article;

		spip_query($query);
		
		//$document_root="/var/alternc/dns/o/nomdedomaine";
		$document_root=$_SERVER['DOCUMENT_ROOT'];
		if (!is_array($_FILES))
			$_FILES = array();
		foreach ($_FILES as $id => $file) {
			if ($file['error'] == 4 /* UPLOAD_ERR_NO_FILE */)
				unset ($_FILES[$id]);
		}
		$imglogo1="";
		$imglogo2="";
		if ($_FILES['logo1']['name']){
			$ext1=strrchr($_FILES['logo1']['name'],'.');
			$myFile1 = $document_root."/IMG/arton".$id_auteur_session.$ext1;
			if (file_exists($myFile1)) @unlink($myFile1);	
			rename($_FILES['logo1']['tmp_name'], $myFile1);
		}
		if ($_FILES['logo2']['name']){
			$ext2=strrchr($_FILES['logo2']['name'],'.');
			$myFile2 = $document_root."/IMG/artoff".$id_auteur_session.$ext2;
			if (file_exists($myFile2)) @unlink($myFile2);	
			rename($_FILES['logo2']['tmp_name'], $myFile2);
		}
				
//		include_ecrire("inc_mail.php3");
//		envoyer_mail_proposition($id_article);
		$message.=_T('form_prop_enregistre');
		
	}
	
	if (($flag_modif)&&($editer)){
		return array('formulaire_modifier_article', 0,
			array('id_article' => $id_article,
					'message' => $message,
					'fma_form' => $fma_form,
					'form_prefixe' => $form_prefixe,
					'gerer_surtitre' => $gerer_surtitre?' ':'',
					'gerer_soustitre' => $gerer_soustitre?' ':'',
					'gerer_descriptif' => $gerer_descriptif?' ':'',
					'gerer_urlref' => $gerer_urlref?' ':'',
					'gerer_chapeau' => $gerer_chapeau?' ':'',
					'gerer_ps' => $gerer_ps?' ':'',
					'gerer_lang' => $gerer_lang?' ':'',
					'gerer_redac' => $gerer_redac?' ':'',
					'gerer_mots' => $gerer_mots?' ':'',
					'gerer_modif' => $gerer_modif?' ':''					
			));
	}

	return array('formulaire_afficher_article', 0, 
					array('id_article' => $id_article,
						'message' => $message,
						'url_modifier_article' => $url_edit,
						'gerer_surtitre' => $gerer_surtitre?' ':'',
						'gerer_soustitre' => $gerer_soustitre?' ':'',
						'gerer_descriptif' => $gerer_descriptif?' ':'',
						'gerer_urlref' => $gerer_urlref?' ':'',
						'gerer_chapeau' => $gerer_chapeau?' ':'',
						'gerer_ps' => $gerer_ps?' ':'',
						'gerer_lang' => $gerer_lang?' ':'',
						'gerer_redac' => $gerer_redac?' ':'',
						'gerer_mots' => $gerer_mots?' ':'',
						'gerer_modif' => $gerer_modif?' ':''
		));

}
?>