<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2005                                                *
 *  Damien Nouvel, Arnaud Ventre                                           *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/


//
// Ce fichier ne sera execute qu'une fois
if (defined("_ECRIRE_INC_AJAX")) return;
define("_ECRIRE_INC_AJAX", "1");

// Fonction qui cr�e l'appel javascript ajax sur un �v�nement javascript
// Prend en param�tre :
//   - le fichier php3 qui renvoie le code HTML � afficher
//   - un tableau des ids des champs � r�cup�rer qui seront pass�s comme variables du formulaire au formulaire pointant vers le fichier php3
//   - l'id de la div dans laquelle afficher le r�sultat (le code HTML recu)
//   - un d�lai entre l'�v�nement et l'appel � ajax
//   - un code javascript � ex�cuter juste au retour de l'appel (avant d'affecter la div)
function formUpdateAjaxDelay($updateFile, $fieldIds, $divId, $delay, $onCallFunction, $onResultFunction, $onUpdateFunction){
  $sortie .= "DelayFunction.delay(newAjaxPrevisualisation('$updateFile', ";
	$sortie .= "'$divId', ";
	$sortie .= "{onCallFunction: '".str_replace("'", "\\'", $onCallFunction)."', ";
	$sortie .= "onResultFunction: '".str_replace("'", "\\'", $onResultFunction)."', ";
	$sortie .= "onUpdateFunction: '".str_replace("'", "\\'", $onUpdateFunction)."',";
	foreach($fieldIds as $field)
		$fields_sortie[] = "\\'$field\\'";
	$sortie .= "formVarsEval: 'Array(".implode(',', $fields_sortie).")'} ";
	$sortie .= "),$delay);";
	return $sortie;
}

?>
