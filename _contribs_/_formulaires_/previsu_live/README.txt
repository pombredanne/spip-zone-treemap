{{{Contrib:}}}
prévisualisation "en direct" du formulaire forum avec validation en 1 seule passe.

Auteur(s):
Pierre Andrews,
à partir des fichiers de base SPIP et de la contrib de previsu des articles: 
http://www.spip-contrib.net/ecrire/articles.php3?id_article=1140

{{{Installation:}}}

1- placer tous les fichiers dans votre repertoire squelettes,
3- vider le cache,
4- et voilà.

{{{Les fichiers:}}}

ajax.js							le fichier qui s'oqp de l'envoi ajax, etc... on pourrait le rendre encore plus générique et l'utiliser un peu partout,
formulaire_forum.html			le squelette du formulaire
inc-formulaire_forum.php3		la logique du formulaire. Les modifications ont été faites dans la fonction afficher_barre
inc_ajax_previsu.php3			fournis un raccourcis pour construire l'appel js
valide_previsu.php3				appeler au départ par ajax, crée le fichier .lock antispam et renvoi le hash et le alea pour la validation
forum_preview.php3				fichier qui s'oqp de formater le texte pour la previsualisation.

{{{TODO}}}

- extraire le js du php et le placer dans un fichier externe, ça sera plus propre,
- extraire le css
- internationaliser (1 chaine à ajouter),
- utiliser le squelette previsu pour formater la previsu,
- verifier la validité de la protextion antispam par ajax
- commenter un peu le code.

{{{Personalisation}}}

La previsualisation est placée dans un élément d'id "[forum_previsu(#ENV{id})]".

Les chaînes d'erreur et le message d'attente sont placés dans une un élément d'id: ["attente_(#ENV{id})"]

Vous pouvez les mettre où vous voulez dans le squelette de formulaire.

{{{BUG}}}

- quand on efface tout, la previsu reste affiché
- même quand ajax n'est pas permis, on voit "Previsualisation:"
- la chaîne d'attente n'est pas affiché correctement, on voit le dernier message d'erreur s'il y en a eu un.
