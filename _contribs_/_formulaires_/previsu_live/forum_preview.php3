<?php

/*
  Copyright (C) 2005 Pierre Andrews


  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

chdir('..');
include('ecrire/inc_version.php3');
include_ecrire('inc_texte.php3');
include_ecrire('inc_filtres.php3');
include_ecrire('inc_charsets.php3');
include('inc-public-global.php3');

$charset = lire_meta("charset");
@header('Content-type: text/html; charset=$charset');

echo charset2unicode(safehtml(@propre($_POST['texte'])),'AUTO',true);

?>
