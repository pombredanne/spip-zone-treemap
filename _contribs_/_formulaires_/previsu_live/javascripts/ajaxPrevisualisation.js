/*
  Copyright (C) 2006

  addaptation depuis les fichiers fournis par la contrib:
  http://www.spip-contrib.net/ecrire/articles.php3?id_article=1140

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

var DelayFunction =  {
	currentTimeout: null,

	// D�clencher une fonction apr�s n secondes ou annuler un appel pr�c�dent � la fonction
	delay: function(callbackFunction, delay){
		if(this.currentTimeout)
			clearTimeout(this.currentTimeout);
		if(callbackFunction && delay)
			this.currentTimeout = setTimeout(callbackFunction, delay*1000);
	}
}

	function newAjaxPrevisualisation(formUrl, callbackObjectId, options) {
		new AjaxPrevisualisation(formUrl, callbackObjectId, options);
	}

var AjaxPrevisualisation = Class.create();

AjaxPrevisualisation.prototype = {


	callbackObject: null,
	formURL: '',
	options: null,


	setOptions: function(options) {
		this.options = {
			formVarsEval: null,
			callbackObjectId: '',
			onCallFunction: null,
			onResultFunction: null,
			onUpdateFunction: null,
			delay: 3
		}.extend(options || {});
	},


	initialize: function(formUrl, callbackObjectId, options) {
		this.formURL = formUrl;
		this.callbackObjectId = callbackObjectId;
		this.setOptions(options);
		this.formUpdateField();
	},



	// Valider un formulaire et obtenir le r�sultat comme requ�te HTTP
	// non bloquante, avec fonction de rappel pendant l'appel, � la
	// r�ception du r�sultat et � l'�criture dans un �l�ment identifi�
	formUpdateField: function(){

		var envoiValide = true;
		if(this.options.onCallFunction)
			eval(this.options.onCallFunction);

		if(envoiValide) {
			var postRequest = new Array();
			if(eval('formVars = '+this.options.formVarsEval+';'))
				for(var i = 0; i < formVars.length; i++)
					if($(formVars[i]) != null)
						postRequest.push(Form.Element.serialize(formVars[i]));
			var myAjax = new Ajax.Request(this.formURL, {method: 'post', parameters: postRequest.join('&'), onComplete: this.aLaFin.bind(this)});
			
		}
	},

	aLaFin: function(request) {
		if(request.responseText.length>0) {
			if(this.options.onResultFunction)
				eval(this.options.onResultFunction);
			$(this.callbackObjectId).innerHTML = request.responseText;
			if(this.options.onUpdateFunction)
				eval(this.options.onUpdateFunction);
		}
	}
	
}