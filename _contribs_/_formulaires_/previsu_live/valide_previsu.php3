<?php

/*
  Copyright (C) 2005 Pierre Andrews

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

chdir('..');
if(file_exists('ecrire/inc_utils.php3')) include('ecrire/inc_utils.php3');
else include('ecrire/inc_version.php3');
include_ecrire('inc_admin.php3');
include_ecrire('inc_filtres.php3');
include_ecrire('inc_session.php3');

$charset = lire_meta("charset");
@header('Content-type: text/html; charset=$charset');

global $afficher_texte;

if ($afficher_texte != 'non') {

  // Une securite qui nous protege contre :
  // - les doubles validations de forums (derapages humains ou des brouteurs)
  // - les abus visant a mettre des forums malgre nous sur un article (??)
  // On installe un fichier temporaire dans _DIR_SESSIONS (et pas _DIR_CACHE
  // afin de ne pas bugguer quand on vide le cache)
  // Le lock est leve au moment de l'insertion en base (inc-messforum.php3)
  // Ce systeme n'est pas fonctionnel pour les forums sans previsu (notamment
  // si $afficher_texte = 'non')
  
  while ($alea = time() + @mt_rand() // astuce : mt_rand pour autoriser les hits simultanes
		 AND 
		 @file_exists($f = _DIR_SESSIONS."forum_$alea.lck")) {}
  spip_touch ($f);
  
  // et maintenant on purge les locks de forums ouverts depuis > 4 h
  if ($dh = @opendir(_DIR_SESSIONS)) {
	while (($file = @readdir($dh)) !== false) {
	  if (preg_match('/^forum_([0-9]+)\.lck$/', $file) 
		  AND (time()-@filemtime(_DIR_SESSIONS.$file) > 4*3600)) {
		@unlink(_DIR_SESSIONS.$file);
	  }
	}
  }

  $chaine_verifie = "ajout_forum";
  foreach(array('forum_id_rubrique', 'forum_id_forum', 'forum_id_article', 'forum_id_breve', 'forum_id_syndic') as $var) {
	if(isset($_REQUEST[$var])) 
	  $chaine_verifie .= " ".$_REQUEST[$var];
	else
	  $chaine_verifie .= " 0";
  }
  $chaine_verifie .= " $alea";

    $hash = calculer_action_auteur($chaine_verifie);
  
  // preparer les parametres du forum en input hidden
  $formulaire = '';
  foreach (array('alea', 'hash') as $var) {
	if ($val = entites_html($$var)) {
	  $formulaire .=
		"<input type='hidden' name='$var' value=\"$val\" />\n";
	}
  }

  echo $formulaire;

}
?>
