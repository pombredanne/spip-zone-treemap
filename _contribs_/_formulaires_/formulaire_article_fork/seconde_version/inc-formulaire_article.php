<?php

if (!defined("_ECRIRE_INC_VERSION")) return;	#securite

include_ecrire ("inc_date.php");
include_ecrire ("inc_texte.php");

// Le contexte indique dans quelle rubrique le visiteur peut proposer l article
global $balise_FORMULAIRE_ARTICLE_collecte;
$balise_FORMULAIRE_ARTICLE_collecte = array('id_rubrique');

function balise_FORMULAIRE_ARTICLE_stat($args, $filtres) {

	// Pas d'id_rubrique ? Erreur de squelette
	if (!$args[0])
		return erreur_squelette(
			_T('zbug_champ_hors_motif',
				array ('champ' => '#FORMULAIRE_ARTICLE',
					'motif' => 'RUBRIQUES')), '');

	// Verifier que les visisteurs sont autorises a proposer un article
	return ($args);
}

function balise_FORMULAIRE_ARTICLE_dyn($surtitre=' ', $titre=' ', $soustitre=' ',  $descriptif=' ', $chapeau=' ', $texte=' ', $ps=' ', $id_rubrique=1, $lien_titre=' ', $lien_url=' ', $var_lang=' ', $mot_cle=' ', $pays=' ', $ville=' ', $structure=' ', $id_auteur_session = 0, $nom_auteur_session =' ', $email_auteur_session=' ', $date=' ', $date_redac=' ') {
	global $REMOTE_ADDR, $afficher_texte, $_COOKIE, $_POST;

	$auteur_statut=$GLOBALS["auteur_session"]["statut"];

	$articles_publics = $GLOBALS['articles_publics'];
	
	// url de reference
	if (!$url) {
		$url = new Link();
		$url = $url->getUrl();
		}
	
	
	$surtitre= stripslashes(_request('surtitre'));	
	$titre= stripslashes(_request('titre'));
	$soustitre= stripslashes(_request('soustitre'));
	$descriptif= stripslashes(_request('descriptif'));
	$chapo= stripslashes(_request('chapo'));
	$texte= stripslashes(_request('texte'));
	$ps= stripslashes(_request('ps'));
	$lien_titre= stripslashes(_request('lien_titre'));
	$lien_url= stripslashes(_request('lien_url'));
	$var_lang = _request('var_lang');
	$jour_d = _request('jour_d');
	$mois_d = _request('mois_d');
	$annee_d = _request('annee_d');
	$heure_d = _request('heure_d');
	$minute_d = _request('minute_d');
	$jour_f = _request('jour_f');
	$mois_f = _request('mois_f');
	$annee_f = _request('annee_f');
	$heure_f = _request('heure_f');
	$minute_f = _request('minute_f');
	$date_aujd = date('Y-m-d H:i:s');

	if ($GLOBALS["auteur_session"]) {
		$id_auteur_session = $GLOBALS['auteur_session']['id_auteur'];
		$nom_auteur_session = $GLOBALS['auteur_session']['nom'];
		$email_auteur_session = $GLOBALS['auteur_session']['email'];
	} else {
		$nom_auteur_session= _request('nom_auteur_session');
		$email_auteur_session= _request('email_auteur_session');
	}
	
// 	$id_article = (_request('id_article')+1);
	
	$mot_cle= _request('mot_cle');
	$pays = _request('pays');
	$ville = _request('ville');
	$structure = _request('structure');
	$id_rubrique= _request('id_rubrique');


//Cr�ation du formulaire de l'heure de debut
	$heure_debut = afficher_heure($heure_d, "name='heure_d' size='1' class='fondl'", true);
	$minute_debut = afficher_minute($minute_d, "name='minute_d' size='1' class='fondl'", true);

//Cr�ation du formulaire de l'heure de fin
	$heure_fin = afficher_heure($heure_f, "name='heure_f' size='1' class='fondl'", true);
	$minute_fin = afficher_minute($minute_f, "name='minute_f' size='1' class='fondl'", true);

//Creation des deux dates qui servent a l'insertion dans la base
	$date = format_mysql_date($annee_d, $mois_d, $jour_d, $heure_d, $minute_d);
	$date_redac= format_mysql_date($annee_f, $mois_f, $jour_f, $heure_f, $minute_f);

//Creation du champ de langue
	$lang = _request('var_lang');
	$nom = 'changer_lang';
	lang_dselect();
	$langues = liste_options_langues($nom, $lang);
	
	// retrouver le secteur et la langue de la rubrique
	$s = spip_query("SELECT id_secteur, lang FROM spip_rubriques WHERE id_rubrique = '$id_rubrique' ");
	if ($r = spip_fetch_array($s)) {
		$id_secteur = $r["id_secteur"];
		$lang = $r["lang"];
		}
	
	$previsualiser= _request('previsualiser');
	$valider= _request('valider');
	
	$previsu = '';
	$bouton= '';

	// statut de l'article, et formulaire de login en fonction de la configuration choisie
	if (($articles_publics == "abo") && (!$GLOBALS["auteur_session"])) {
		return array('formulaire_login_article', 0, array());
	}
	elseif ($articles_publics == "pos") {
		$statut= "publie";
	}
	else{
		$statut= "publie";
	}

	if($valider)
		{
		// int�grer � la base de donn�es
		$surtitre= addslashes($surtitre);
		$titre= addslashes($titre);
		$soustitre= addslashes($soustitre);
		$descriptif= addslashes($descriptif);
		$chapo= addslashes($chapo);
		$texte= addslashes($texte);
		$ps= addslashes($ps);
		$lien_titre= addslashes($lien_titre);
		$lien_url= addslashes($lien_url);
		
		// ajouter l'article (sans auteur) dans la base: statut: 'prop' ou 'publie'
		spip_query("INSERT INTO spip_articles (surtitre, titre, soustitre, id_rubrique, descriptif, chapo, texte, ps, statut, id_secteur, accepter_forum, auteur_modif, date, date_modif, lang, date_redac, id_version, nom_site, url_site) VALUES ('$surtitre', '$titre', '$soustitre', '$id_rubrique', '$descriptif', '$chapo', '$texte', '$ps', '$statut', '$id_secteur', 'pos', '$id_auteur_session', '$date', '$date', '$var_lang', '$date_redac', '1', '$lien_titre', '$lien_url')");

		$s = spip_query("SELECT id_article FROM spip_articles WHERE date = '$date' ORDER BY id_article DESC ");
		if ($r = spip_fetch_array($s)){
			$id_article = $r["id_article"];
		}

		spip_query("INSERT INTO spip_mots_articles (id_mot, id_article) VALUES ($mot_cle, $id_article)");
		
		spip_query("INSERT INTO spip_mots_articles (id_mot, id_article) VALUES ($pays, $id_article)");

		spip_query("INSERT INTO spip_mots_articles (id_mot, id_article) VALUES ($ville, $id_article)");

		spip_query("INSERT INTO spip_mots_articles (id_mot, id_article) VALUES ($structure, $id_article)");

		// ajouter l'auteur
		if($id_auteur_session != 0){
		spip_query("INSERT INTO spip_auteurs_articles (id_auteur, id_article) VALUES ($id_auteur_session, $id_article)");
		}
		return  _T('form_prop_enregistre');
		}

	else{
		if($previsualiser)
		{

		if ($date>$date_redac){$erreur .= _T('forum_attention_dates_incoherentes');}
		else if ($date_redac<$date_aujd){$erreur .= _T('forum_attention_dates_incoherentes');}
		else if (strlen($titre) < 3){$erreur .= _T('forum_attention_trois_caracteres');}
		if(!$erreur){$bouton= _T('form_prop_confirmer_envoi');}

		$previsu = inclure_balise_dynamique(
			array(
				'formulaire_article_previsu',
				0,
			array(
				'date' => $date,
				'date_redac' => $date_redac,
				'surtitre' => propre_article($surtitre),
				'titre' => propre_article($titre),
				'soustitre' => propre_article($soustitre),
				'descriptif' => propre($descriptif),
				'chapo' => propre_article($chapo),
				'texte' => propre($texte),
				'ps' => propre($ps),
				'jour_d' => $jour_d,
				'lien_titre' => propre_article($lien_titre),
				'lien_url' => $lien_url,
				'erreur' => $erreur,
				'bouton' => $bouton,
				'mot_cle' => $mot_cle,
				'pays' => $pays,
				'ville' => $ville,
				'structure' => $structure
				)
			), false);
				$previsu = preg_replace("@<(/?)f(orm[>[:space:]])@ism",
				"<\\1no-f\\2", $previsu);
		}

		return array('formulaire_article', 0,
		array(
				'url' =>  $url,
				'langues' => $langues,
				'jour_d' => $jour_d,
				'mois_d' => $mois_d,
				'annee_d' => $annee_d,
				'heure_debut' => $heure_debut,
				'minute_debut' => $minute_debut,
				'jour_f' => $jour_f,
				'mois_f' => $mois_f,
				'annee_f' => $annee_f,
				'heure_fin' => $heure_fin,
				'minute_fin' => $minute_fin,
				'previsu' => $previsu,
				'surtitre' => propre_article($surtitre),
				'titre' => propre_article($titre),
				'soustitre' => propre_article($soustitre),
				'descriptif' => propre_article($descriptif),
				'chapo' => propre_article($chapo),
				'texte' => $texte,
				'ps' => $ps,
				'lien_titre' => propre_article($lien_titre),
				'lien_url' => $lien_url,
				'id_rubrique' => $id_rubrique,
				'id_secteur' => $id_secteur,
				'id_auteur_session' => $id_auteur_session,
				'nom_auteur_session' => $nom_auteur_session,
				'email_auteur_session' => $email_auteur_session,
				'mot_cle' => $mot_cle,
				'pays' => $pays,
				'ville' => $ville,
				'structure' => $structure
			));
	}
}

function propre_article ($texte) {
	return ereg_replace('"','&nbsp;', $texte);
}

?>