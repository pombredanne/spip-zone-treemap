<?php

if (!defined("_ECRIRE_INC_VERSION")) return;	#securite

include_ecrire ("inc_date.php3");
include_ecrire ("inc_texte.php3");

// Le contexte indique dans quelle rubrique le visiteur peut proposer l article
global $balise_FORMULAIRE_ARTICLE_collecte;
$balise_FORMULAIRE_ARTICLE_collecte = array('id_rubrique');

function balise_FORMULAIRE_ARTICLE_stat($args, $filtres) {

	// Pas d'id_rubrique ? Erreur de squelette
	if (!$args[0])
		return erreur_squelette(
			_T('zbug_champ_hors_motif',
				array ('champ' => '#FORMULAIRE_ARTICLE',
					'motif' => 'RUBRIQUES')), '');

	// Verifier que les visisteurs sont autorises a proposer un article
	return ($args);
}

function balise_FORMULAIRE_ARTICLE_dyn($surtitre=' ', $titre=' ', $soustitre=' ',  $descriptif=' ', $chapeau=' ', $texte=' ', $ps=' ', $id_rubrique=1, $lien_titre=' ', $lien_url=' ', $var_lang=' ', $jour_debut=' ', $mois_debut=' ', $annee_debut=' ', $heure_debut=' ', $minute_debut=' ', $jour_fin=' ', $mois_fin=' ', $annee_fin=' ',  $heure_fin=' ', $minute_fin=' ', $mot_cle=' ', $id_auteur_session = 0, $nom_auteur_session =' ', $email_auteur_session=' ', $date=' ', $date_redac=' ') {
	global $REMOTE_ADDR, $afficher_texte, $_COOKIE, $_POST;

	$auteur_statut=$GLOBALS["auteur_session"]["statut"];

	$articles_publics = $GLOBALS['articles_publics'];
	
	// url de reference
	if (!$url) {
		$url = new Link();
		$url = $url->getUrl();
		}
	
	
	$surtitre= stripslashes(_request('surtitre'));	
	$titre= stripslashes(_request('titre'));
	$soustitre= stripslashes(_request('soustitre'));
	$descriptif= stripslashes(_request('descriptif'));
	$chapo= stripslashes(_request('chapo'));
	$texte= stripslashes(_request('texte'));
	$ps= stripslashes(_request('ps'));
	$lien_titre= stripslashes(_request('lien_titre'));
	$lien_url= stripslashes(_request('lien_url'));
	$var_lang = _request('var_lang');

	if ($GLOBALS["auteur_session"]) {
		$id_auteur_session = $GLOBALS['auteur_session']['id_auteur'];
		$nom_auteur_session = $GLOBALS['auteur_session']['nom'];
		$email_auteur_session = $GLOBALS['auteur_session']['email'];
	} else {
		$nom_auteur_session= _request('nom_auteur_session');
		$email_auteur_session= _request('email_auteur_session');
	}
	
	$id_article = (_request('id_article')+1);
	
	$mot_cle= _request('mot_cle');
	$id_rubrique= _request('id_rubrique');


//Cr�ation de la date de debut a la fois dans le formulaire et les donnees pour l'insertion dans la base
	$jour_debut = afficher_jour(date('d'), "name='jour_debut' size='1' class='fondl'", true);
	$j_debut = _request('jour_debut');

	$mois_debut = afficher_mois(date('m'), "name='mois_debut' size='1' class='fondl'", true);
	$m_debut = _request('mois_debut');

	$annee_debut = afficher_annee(date('Y'), "name='annee_debut' size='1' class='fondl'", 2006);
	$y_debut = _request('annee_debut');

	$heure_debut = afficher_heure(date('H'), "name='heure_debut' size='1' class='fondl'", true);
	$h_debut = _request('heure_debut');

	$minute_debut = afficher_minute(date('i'), "name='minute_debut' size='1' class='fondl'", true);
	$i_debut = _request('minute_debut');

//Cr�ation de la date de fin a la fois dans le formulaire et les donnees pour l'insertion dans la base
	$jour_fin = afficher_jour(date('d'), "name='jour_fin' size='1' class='fondl'", true);
	$j_fin = _request('jour_fin');

	$mois_fin = afficher_mois(date('m'), "name='mois_fin' size='1' class='fondl'", true);
	$m_fin = _request('mois_fin');

	$annee_fin = afficher_annee(date('Y'), "name='annee_fin' size='1' class='fondl'", 2006);
	$y_fin = _request('annee_fin');

	$heure_fin = afficher_heure((date('H')+1), "name='heure_fin' size='1' class='fondl'", true);
	$h_fin = _request('heure_fin');

	$minute_fin = afficher_minute(date('i'), "name='minute_fin' size='1' class='fondl'", true);
	$i_fin = _request('minute_fin');

//Cr�ation des deux dates qui servent a l'insertion dans la base
	$date = format_mysql_date($y_debut, $m_debut, $j_debut, $h_debut, $i_debut);
	$date_redac= format_mysql_date($y_fin, $m_fin, $j_fin, $h_fin, $i_fin);

	$lang = _request('var_lang');
	$nom = 'changer_lang';
	lang_dselect();
	$langues = liste_options_langues($nom, $lang);
	
	// retourver le secteur et la langue de la rubrique
	$s = spip_query("SELECT id_secteur, lang FROM spip_rubriques WHERE id_rubrique = '$id_rubrique' ");
	if ($r = spip_fetch_array($s)) {
		$id_secteur = $r["id_secteur"];
		$lang = $r["lang"];
		}
	
	$previsualiser= _request('previsualiser');
	$valider= _request('valider');
	
	$previsu = '';
	$bouton= '';

	// statut de l'article, et formulaire de login en fonction de la configuration choisie
	if (($articles_publics == "abo") && (!$GLOBALS["auteur_session"])) {
		return array('formulaire_login_article', 0, array());
	}
	elseif ($articles_publics == "pos") {
		$statut= "publie";
	}
	else{
		$statut= "publie";
	}

	if($valider)

		
		{
		// int�grer � la base de donn�es
		$surtitre= addslashes($surtitre);
		$titre= addslashes($titre);
		$soustitre= addslashes($soustitre);
		$descriptif= addslashes($descriptif);
		$chapo= addslashes($chapo);
		$texte= addslashes($texte);
		$ps= addslashes($ps);
		$lien_titre= addslashes($lien_titre);
		$lien_url= addslashes($lien_url);
		$var_lang = $var_lang;

		
		// ajouter l'article (sans auteur) dans la base: statut: 'prop' ou 'publie'
		spip_query("INSERT INTO spip_articles (surtitre, titre, soustitre, id_rubrique, descriptif, chapo, texte, ps, statut, id_secteur, accepter_forum, auteur_modif, date, lang, date_redac, id_version, nom_site, url_site) VALUES ('$surtitre', '$titre', '$soustitre', '$id_rubrique', '$descriptif', '$chapo', '$texte', '$ps', '$statut', '$id_secteur', 'pos', '$id_auteur_session', '$date', '$var_lang', '$date_redac', '1', '$lien_titre', '$lien_url')");

		spip_query("INSERT INTO spip_mots_articles (id_mot, id_article) VALUES ($mot_cle, $id_article)");

		// ajouter l'auteur
		if($id_auteur_session != 0){
		spip_query("INSERT INTO spip_auteurs_articles (id_auteur, id_article) VALUES ($id_auteur_session, $id_article)");
		}
		include_ecrire("inc_mail.php3");
		envoyer_mail_proposition($id_article);
		return  _T('form_prop_enregistre');
		}

	else{
		if($previsualiser)
		{

		if ($date>$date_redac){$erreur .= _T('forum_attention_dates_incoherentes');}
		else if (strlen($titre) < 3){$erreur .= _T('forum_attention_trois_caracteres');}
		if(!$erreur){$bouton= _T('form_prop_confirmer_envoi');}

		$previsu = inclure_balise_dynamique(
			array(
				'formulaire_article_previsu',
				0,
				array(
					'date' => $date,
					'date_redac' => $date_redac,
					'surtitre' => interdire_scripts(typo($surtitre)),
					'titre' => interdire_scripts(typo($titre)),
					'soustitre' => interdire_scripts(typo($soustitre)),
					'descriptif' => propre($descriptif),
					'chapo' => propre($chapo),
					'texte' => propre($texte),
					'ps' => propre($ps),
					'lien_titre' => $lien_titre,
					'lien_url' => $lien_url,
					'erreur' => $erreur,
					'bouton' => $bouton,
					'lang' => $var_lang,
					'mot_cle' => $mot_cle
				)
			), false);
				$previsu = preg_replace("@<(/?)f(orm[>[:space:]])@ism",
				"<\\1no-f\\2", $previsu);
		}

		return array('formulaire_article', 0,
		array(
					
					'url' =>  $url,
					'langues' => $langues,
					'lang' => $var_lang,
					'jour_debut' => $jour_debut,
					'mois_debut' => $mois_debut,
					'annee_debut' => $annee_debut,
					'heure_debut' => $heure_debut,
					'minute_debut' => $minute_debut,
					'jour_fin' => $jour_fin,
					'mois_fin' => $mois_fin,
					'annee_fin' => $annee_fin,
					'heure_fin' => $heure_fin,
					'minute_fin' => $minute_fin,
					'previsu' => $previsu,
					'surtitre' => $surtitre,
					'titre' => interdire_scripts(typo($titre)),
					'soustitre' => $soustitre,
					'descriptif' => $descriptif,
					'chapo' => $chapo,
					'texte' => $texte,
					'ps' => $ps,
					'lien_titre' => $lien_titre,
					'lien_url' => $lien_url,
					'id_rubrique' => $id_rubrique,
					'id_secteur' => $id_secteur,
					'id_auteur_session' => $id_auteur_session,
					'nom_auteur_session' => $nom_auteur_session,
					'email_auteur_session' => $email_auteur_session,
					'mot_cle' => $mot_cle
			));
	}
}

function barre_article($texte)
{
	include_ecrire('inc_layer.php3');

	if (!$GLOBALS['browser_barre'])
		return "<textarea name='texte' rows='12' class='forml' cols='40'>$texte</textarea>";
	static $num_formulaire = 0;
	$num_formulaire++;
	include_ecrire('inc_barre.php3');
	return afficher_barre("document.getElementById('formulaire_$num_formulaire')", true) .
	  "
	<textarea name='texte' rows='12' class='forml' cols='40' id='formulaire_$num_formulaire' onselect='storeCaret(this);' onclick='storeCaret(this);' onkeyup='storeCaret(this);' ondbclick='storeCaret(this);'>$texte</textarea>";
}

function logoauteur($id_auteur, $formats = array ('gif', 'jpg', 'png')) {
	reset($formats);
	while (list(, $format) = each($formats)) {
		$d = _DIR_IMG . "auton$id_auteur.$format";
		if (@file_exists($d)) return $d;
	}
	return  '';
}

?>