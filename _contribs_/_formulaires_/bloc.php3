<?php

// Appel spip
if (!function_exists('find_in_path')) {
	define('_SPIP_PAGE',1); # ne pas mourir si on passe le $fond
	include ('ecrire/inc_version.php');
}
// Reglage du $fond
if (isset($contexte_inclus['bloc']))
	$fond = $contexte_inclus['bloc'];
else if (isset($_GET["bloc"]))
	$fond = $_GET["bloc"];
else
	$fond = '404';

// Reglage du $delais
// par defaut : la valeur existante (inclusion) ou sinon SPIP fera son reglage
if (isset($contexte_inclus['delais']))
	$delais = $contexte_inclus['delais'];

// Securite 
if (strstr($fond, '/')) {
	die ("Faut pas se gener");
}
if (!find_in_path("$fond.html")) {
	spip_log("page.php: find_in_path ne trouve pas le squelette $bloc");
	$fond = '404';
}
include ("inc-public.php");

?>
