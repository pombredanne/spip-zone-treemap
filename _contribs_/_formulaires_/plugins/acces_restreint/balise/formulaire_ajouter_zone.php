<?php
if (!defined("_ECRIRE_INC_VERSION")) return;	#securite

include_spip ("inc/meta");
include_spip ("inc/session");
include_spip('base/abstract_sql');

function balise_FORMULAIRE_AJOUTER_ZONE ($p) {
	$p = calculer_balise_dynamique($p,'FORMULAIRE_AJOUTER_ZONE', array('id_article'));
	return $p;
}

function balise_FORMULAIRE_AJOUTER_ZONE_dyn($id_article) {

	//verifier si la personne en question a le droit de creer un acces prive (s'il est l'auteur de l'article)
	$s = spip_query("SELECT id_auteur FROM spip_auteurs_articles WHERE id_article = '$id_article' ");
	if ($r = spip_fetch_array($s)) {
		$id_auteur = $r["id_auteur"];
		if (($GLOBALS["auteur_session"]["id_auteur"]) != $id_auteur) {
			return 'vous n etes pas auteur';
		}
	}

	//on recupere les infos de l'article necessaires
	$art = spip_query("SELECT * FROM spip_articles WHERE id_article = '$id_article' ");
	$artinfos = spip_fetch_array($art);
	
	$id_rub_orig = $artinfos["id_rubrique"];
	$id_secteur = $artinfos["id_secteur"];
	$titre = $artinfos["titre"];
	$lang = $artinfos["lang"];
	
	// Si on est deja dans une zone on recup l'id_zone pour le supprimer plus tard
	$zone = spip_query("SELECT id_zone FROM spip_zones_rubriques WHERE id_rubrique = '$id_rub_orig'");
	$z = spip_fetch_array($zone);
	$id_zone = $z['id_zone'];
	
	// Si on est deja dans une zone on aura besoin de la rubrique parente de la rubrique
	$rub_parente = spip_query("SELECT * FROM spip_rubriques WHERE id_rubrique = '$id_rub_orig' ");
	$a = spip_fetch_array($rub_parente);
	$id_rub_parente = $a['id_parent'];	
	
	//recuperer les donnees qui nous interessent
	$creer_zone = _request('creer_zone'); // true ou false = checkbox
	$supprimer_zone = _request('supprimer_zone'); // true ou false = checkbox

	$valider= _request('valider');
	
	if ($valider){
		if ($creer_zone){
		
		//creer la bonne rubrique avec le meme nom que l'article
		spip_query("INSERT INTO spip_rubriques (id_parent,titre,id_secteur,lang) VALUES ('$id_rub_orig', "._q($titre).", '$id_secteur', '$lang')");
		spip_log("Creation rubrique $titre", "ajouter_zone");
		
		//on r�cup�re l'id de la rubrique
		$id_rub = spip_insert_id();
		
		//on d�place l'article
		spip_query("UPDATE spip_articles SET id_rubrique='$id_rub' WHERE id_article='$id_article'");
		spip_log("Deplacement article $id_article $titre", "ajouter_zone");
		
		// on cree une zone "publique" qui a le meme nom que l'article (pas privee sinon les admin y auraient pas acces)
		spip_query("INSERT INTO spip_zones (titre,publique) VALUES ("._q($titre).",'oui')");
		spip_log("Creation zone $titre", "ajouter_zone");
		
		// on recupere l'id de la zone en question
		$id_zone_creee = spip_insert_id();
		
		// on applique cette zone a la rubrique
		spip_query("INSERT INTO spip_zones_rubriques (id_zone,id_rubrique) VALUES ('$id_zone_creee','$id_rub')");
		
		// on ajoute l'auteur egalement
		spip_query("INSERT INTO spip_zones_auteurs (id_zone,id_auteur) VALUES ('$id_zone_creee','$id_auteur')");
		
		//invalider le cache afin de prendre en consideration les changements
	
		include_spip('inc/invalideur');
		suivre_invalideur("0",true);
		spip_log('invalider', 'ajouter_zone');

		$invalider = true;	
		}
	
		else if ($supprimer_zone){
	
		// suppression des references a la zone dans les 3 tables zones
		spip_query("DELETE FROM spip_zones_auteurs WHERE id_zone = '$id_zone'");
		spip_query("DELETE FROM spip_zones_rubriques WHERE id_zone = '$id_zone'");
		spip_query("DELETE FROM spip_zones WHERE id_zone = '$id_zone'");
		spip_log("Suppression zone $id_zone", "ajouter_zone");

		//on d�place l'article vers la rubrique parente
		spip_query("UPDATE spip_articles SET id_rubrique='$id_rub_parente' WHERE id_article='$id_article'");
		spip_log("Deplacement article $id_article $titre vers $id_rub_parente", "ajouter_zone");
		
		// on supprime definitivement l'ancienne rubrique
		spip_query("DELETE FROM spip_rubriques WHERE id_rubrique = '$id_rub_orig'");
		spip_log("Suppression definitive de la rubrique $id_rubrique", "ajouter_zone");

		$invalider = true;
		}
		
		else {
			if (is_array(_request('add_user'))){
				spip_query("DELETE FROM spip_zones_auteurs WHERE id_zone = '$id_zone'");
				foreach (_request('add_user') as $adduser){
					if ($adduser = intval($adduser)) {
						spip_query("INSERT INTO  spip_zones_auteurs (id_zone,id_auteur) VALUES ('$id_zone','$adduser')");
						$invalider = true;
						spip_log("ajouter utilisateur $adduser a la zone $id_zone");
					}
				}	
				spip_query("INSERT INTO spip_zones_auteurs (id_zone,id_auteur) VALUES ('$id_zone','$id_auteur')");
			}
		}
		if ($invalider){
			//invalider le cache afin de prendre en consideration les changements
			include_spip('inc/invalideur');
			suivre_invalideur("0",true);
			spip_log('invalider', 'ajouter_zone');
		}
		return header("Location: ".generer_url_article($id_article)."");
	}
	
	return array('formulaires/ajouter_zone', 0,
		array(
				'article' => $id_article,
				'rubrique' => $id_rub_orig,
				'titre' => $titre,
				'erreur' => $erreur
		));

}
?>