<?php

if (!defined("_ECRIRE_INC_VERSION")) return;	#securite

include_ecrire ("inc_documents.php3");
include_ecrire ("inc_date.php3");

global $balise_FORMULAIRE_DOCUMENTS_RUBRIQUE_collecte;
$balise_FORMULAIRE_DOCUMENTS_RUBRIQUE_collecte = array('id_rubrique');

function balise_FORMULAIRE_DOCUMENTS_RUBRIQUE_stat($args, $filtres) {

	// Pas d'id ? Erreur de squelette
	if (!$args[0])
		return erreur_squelette(
			_T('zbug_champ_hors_motif',
				array ('champ' => '#FORMULAIRE_DOCUMENT',
					'motif' => 'RUBRIQUES')), '');

	if ($args[1])
	    return (array($args[1]));
	else
	    return (array($args[0]));
}

function balise_FORMULAIRE_DOCUMENTS_RUBRIQUE_dyn($id_rubrique) {
	if(lire_meta("documents_rubrique")!='oui') 
		return '';
	if (!$GLOBALS["auteur_session"]) 
		return array('formulaire_documents_rubrique', 0,array('id_rubrique' => $id_rubrique));
	
	$auteur_statut=$GLOBALS["auteur_session"]["statut"];
	$id_auteur_session = $GLOBALS['auteur_session']['id_auteur'];
	include_ecrire("inc_auth.php3");
	$flag_modif=(($auteur_statut == "0minirezo") && acces_rubrique($id_rubrique));
	
	if ($flag_modif){
		maj_documents($id_rubrique, "rubrique");
		$redirect_url = new Link();
		$redirect_url->addVar('modifier_rubrique', 'oui');
		$redirect_url = $redirect_url->getUrl();

		return array('formulaire_portfolio_rubrique', 0,
			array(
					'id_rubrique' => $id_rubrique,
					'id_auteur_session' => $id_auteur_session,
					'redirect_url' => $redirect_url
			));
	}
	else
		return array('formulaire_documents_rubrique', 0,array('id_rubrique' => $id_rubrique));
	
}

?>
