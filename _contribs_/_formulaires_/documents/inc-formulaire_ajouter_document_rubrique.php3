<?php

if (!defined("_ECRIRE_INC_VERSION")) return;	#securite

global $balise_FORMULAIRE_AJOUTER_DOCUMENT_RUBRIQUE_collecte;
$balise_FORMULAIRE_AJOUTER_DOCUMENT_RUBRIQUE_collecte = array('id_rubrique');

function balise_FORMULAIRE_AJOUTER_DOCUMENT_RUBRIQUE_stat($args, $filtres) {

	// Pas d'id_rubrique ? Erreur de squelette
	if (!$args[0] && !$args[1])
		return erreur_squelette(
			_T('zbug_champ_hors_motif',
				array ('champ' => '#FORMULAIRE_AJOUTER_DOCUMENT_RUBRIQUE',
					'motif' => 'RUBRIQUES')), '');

	if ($args[1])
	    return (array($args[1]));
	else
	    return (array($args[0]));
}

function balise_FORMULAIRE_AJOUTER_DOCUMENT_RUBRIQUE_dyn($id_rubrique) {

	if(lire_meta("documents_rubrique")!='oui') 
		return '';
	if (!$GLOBALS["auteur_session"]) 
		return array('formulaire_ajouter_document_login', 0, array());
	
	$auteur_statut=$GLOBALS["auteur_session"]["statut"];
	$id_auteur_session = $GLOBALS['auteur_session']['id_auteur'];
	include_ecrire("inc_auth.php3");
	$flag_modif=(($auteur_statut == "0minirezo") && acces_rubrique($id_rubrique));
	
	if ($flag_modif){
		$redirect_url = new Link();
		$redirect_url = $redirect_url->getUrl();
		return array('formulaire_ajouter_document', 0,
			array(
						'id_chose' => $id_rubrique,
						'type' => 'rubrique',
						'id_auteur_session' => $id_auteur_session,
						'redirect_url' => $redirect_url,
				));
	}
	else return '';
}

?>
