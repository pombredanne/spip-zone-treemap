<?php

if (!defined("_ECRIRE_INC_VERSION")) return;	#securite

include_ecrire ("inc_presentation.php3");


global $balise_FORMULAIRE_AJOUTER_DOCUMENT_ARTICLE_collecte;
$balise_FORMULAIRE_AJOUTER_DOCUMENT_ARTICLE_collecte = array('id_article','id_rubrique','statut');

function balise_FORMULAIRE_AJOUTER_DOCUMENT_ARTICLE_stat($args, $filtres) {

	// Pas d'id_rubrique ? Erreur de squelette
	if (!$args[0] && !$args[1])
		return erreur_squelette(
			_T('zbug_champ_hors_motif',
				array ('champ' => '#FORMULAIRE_AJOUTER_DOCUMENT_ARTICLE',
					'motif' => 'ARTICLES')), '');

	if ($args[3])
	    return (array($args[3],$args[4],$args[5]));
	else
	    return (array($args[0],$args[1],$args[2]));
}

function balise_FORMULAIRE_AJOUTER_DOCUMENT_ARTICLE_dyn($id_article,$id_rubrique,$statut_article) {

	if(lire_meta("documents_article")!='oui') 
		return '';
	if (!$GLOBALS["auteur_session"]) 
		return array('formulaire_ajouter_document_login', 0, array());
	
	$auteur_statut=$GLOBALS["auteur_session"]["statut"];
	$id_auteur_session = $GLOBALS['auteur_session']['id_auteur'];

	include_ecrire("inc_auth.php3");

	$query = "SELECT * FROM spip_auteurs_articles WHERE id_article=".$id_article." AND id_auteur=".$id_auteur_session;
	$result_auteur = spip_query($query);
	$flag_auteur = (spip_num_rows($result_auteur) > 0);

	$flag_modif= ((acces_rubrique($id_rubrique) && ($auteur_statut=='0minirezo'))
	OR ($flag_auteur AND ($statut_article == 'prepa' OR $statut_article == 'prop' OR $statut_article == 'poubelle')));
	
	if ($flag_modif){
		$redirect_url = new Link();
		$redirect_url = $redirect_url->getUrl();
		return array('formulaire_ajouter_document', 0,
			array(
						'id_chose' => $id_article,
						'type' => 'article',
						'id_auteur_session' => $id_auteur_session,
						'redirect_url' => $redirect_url,
				));
	}
	else return '';
}

?>
