<?php

/***************************************************************************\
*  SPIP, Systeme de publication pour l'internet                           *
*                                                                         *
*  Copyright (c) 2001-2005                                                *
*  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
*                                                                         *
*  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
*  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

include ("inc.php3");

$pack = 20;	// nb de forums affiches par page
$mode_affichage_defaut="html"; // html-htmlmfleche-rss
$texte_div_invisible="";
$compteurMarginTop=0;

//
// Debut de la colonne de gauche
//

function debut_gauche_ben($rubrique = "asuivre") {
	global $connect_statut, $cookie_admin;
	global $options, $spip_display;
	global $connect_id_auteur;
	global $spip_ecran;
	global $flag_3_colonnes, $flag_centre_large;
	global $spip_lang_rtl;

	$flag_3_colonnes = false;
	$largeur=400 ; 

	// Ecran panoramique ?
	if ($spip_ecran == "large") {
		$largeur_ecran = 974;
		
		// Si edition de texte, formulaires larges
		if (ereg('((articles|breves|rubriques)_edit|forum_envoi)\.php3', $GLOBALS['REQUEST_URI'])) {
			$flag_centre_large = true;
		}
		
		

	}
	else {
		$largeur_ecran = 750;
	}

	echo "<br><table width='$largeur_ecran' cellpadding=0 cellspacing=0 border=0>
		<tr><td width='$largeur' class='colonne_etroite' valign='top' class='serif' $rspan>\n";
		
	if ($spip_display == 4) echo "<!-- ";

}

function get_texte_forum($prefixe,$row,$mode_affichage) {
	global $texte_div_invisible ;
	global $compteurMarginTop ;
	$id_forum = $row['id_forum'];
	$forum_id_article = $row['id_article'];
	$forum_date_heure = $row['date_heure'];
	$forum_titre = echapper_tags($row['titre']);
	$forum_texte = echapper_tags($row['texte']);
	$forum_id_auteur = $row["id_auteur"];
	$forum_nom_auteur = $row["nom"];
	$forum_titre_article = echapper_tags($row['titreArticle']);


	if ($mode_affichage=="html") {
		$texte_forum = "";
		$texte_forum .="<br><span onMouseOver=\"changestyle('".$prefixe."_invisible_".$id_forum."', 'display', 'block');\"   onMouseOut=\"changestyle('".$prefixe."_invisible_".$id_forum."', 'display', 'none');\">";
		$texte_forum .="<span class='arial2'>".date_relative($forum_date_heure)."</span> ";
		$texte_forum .= "/ <A HREF='auteurs_edit.php3?id_auteur=$forum_id_auteur'>$forum_nom_auteur</A>";
		$texte_forum .= "/ <a href='articles.php3?id_article=$forum_id_article#$id_forum'>$forum_titre_article </A>";
		$texte_forum .="</span>";

		// le texte du message n'est pas affiche
		$compteurMarginTop +=1;
		$texte_div_invisible.="<div style='display:none;margin-top:".$compteurMarginTop."em' id=\"".$prefixe."_invisible_".$id_forum."\" ><b>".typo($forum_titre)."</b><P align='justify'>".propre($forum_texte)."</div>";
	}
	elseif ($mode_affichage=="htmlfleche") {
		$texte_forum = "<br>";
		$texte_forum .=bouton_block_invisible("forum$id_forum");
		$texte_forum .="<span class='arial2'>".date_relative($forum_date_heure)."</span>";
		$texte_forum .= " / <A HREF='auteurs_edit.php3?id_auteur=$forum_id_auteur'>$forum_nom_auteur</A>";
		$texte_forum .= " / <a href='articles.php3?id_article=$forum_id_article#$id_forum'>$forum_titre_article </A>";
		$texte_forum .= debut_block_invisible("forum$id_forum");
		$texte_forum .= debut_cadre_thread_forum("", true, "", typo($forum_titre))."<br />$avant<B>$pref </B>" .	"<P align='justify'>".propre($forum_texte);
		$texte_forum .= fin_cadre_thread_forum(true).fin_block();
	}
	elseif ($mode_affichage=="rss")
	{
		$texte_forum = "";
		$texte_forum .= "<item>\n";
		$texte_forum .= "<title>".htmlentities(typo($forum_titre))."</title>\n";
		$adresse_site = lire_meta("adresse_site");
		$texte_forum .= "<link>".$adresse_site."/articles.php3?id_article=$forum_id_article#$id_forum</link>";
		$texte_forum .= "<dc:date>$forum_date_heure</dc:date>";
		$texte_forum .= "<description>".texte_backend(couper(propre($forum_texte),300))."</description>";
		$texte_forum .= "</item>";

	}
	return $texte_forum;
}




function get_forums ( $prefixe,$whereArticles,$pack ,$mode_affichage) {

	global $pack;

	$texte_forums="";
	$sqlTexte="
	SELECT
	f.id_forum,
	f.id_article,
	f.date_heure,
	f.titre,
	f.texte,
	f.id_auteur,
	auteurs.nom,
	a.titre as titreArticle
	FROM
	spip_forum as f ,
	spip_articles as a ,
	spip_auteurs as auteurs
	WHERE
	f.id_article=a.id_article AND
	auteurs.id_auteur=f.id_auteur AND
	". $whereArticles . " ORDER BY f.date_heure DESC LIMIT 0, $pack" ;

	$result_forum=spip_query($sqlTexte);

	while ($row = spip_fetch_array($result_forum)) {
		$texte_forums .= get_texte_forum($prefixe,$row,$mode_affichage);
	}
	return $texte_forums ;
}









if (!$mode_affichage)
$mode_affichage="htmlfleche";
//
// Debut de la page de controle
//

$query_forum = "f.statut IN ('prive') AND f.texte!='' and f.id_article > 0";


//liste des articles propos�s de l'auteur connect�.
$result_mesArticlesProp = spip_query("
select articles.id_article from
spip_articles as articles,
spip_auteurs_articles AS lien
WHERE
articles.id_article=lien.id_article AND
lien.id_auteur=$connect_id_auteur AND
articles.statut='prop'
ORDER BY articles.date DESC
");


while ($row = spip_fetch_array($result_mesArticlesProp)) {
	$tous_id[] =$row['id_article'];
}
$queryMesArticlesProp="";
if ($tous_id)
$queryMesArticlesProp.="f.id_article in (".join($tous_id,',').")";


$controle ="";
if ($mode_affichage=="htmlfleche" or $mode_affichage=="html") {

	$controle .="<div class='serif2'>";
	$controle.= "<hr>Tous les commentaires de mes articles propos&eacute;s<br>";
}
else if ($mode_affichage=="rss") { 

	include("inc_meta.php3");
	include ("inc_version.php3");
	lire_metas();
	$nom_site = lire_meta("nom_site");
	$adresse_site = lire_meta("adresse_site");
	$langue_site = lire_meta("langue_site");
	$charset = lire_meta("charset");
	$controle .= "<?xml version=\"1.0\" encoding=\"$charset\" >
	<rss version=\"2.0\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\">
	<channel>
	<title>".texte_backend($nom_site)."</title>
	<link>$adresse_site/</link>
	<description>".texte_backend($nom_site)."</description>
	<language>$langue_site</language>
	<image>
	<title>".texte_backend($nom_site)."</title>
	<url>$adresse_site/images/logo.png</url>
	<link>$adresse_site/</link>
	<description></description>
	</image>
	";
}

$controle.= get_forums("mes",$queryMesArticlesProp." and ".$query_forum,$pack,$mode_affichage);

if ($listeArticle) {
	if (!ereg('^([0-9]*,)*([0-9]*)$', $listeArticle))
	die ("erreur $listeArticle");
	$query_forum_liste = "f.statut IN ('prive') AND f.texte!='' and f.id_article in ($listeArticle)";

	if ($mode_affichage=="htmlfleche" or $mode_affichage=="html") {

		$controle.="<hr>Tous les commentaires des articles s&eacute;lectionn&eacute;s<br>";
	}
	$controle.=get_forums("liste",$query_forum_liste,$pack,$mode_affichage);
}

if ($mode_affichage=="htmlfleche" or $mode_affichage=="html") {
	$controle.="<hr>Tous les commentaires<br>";
}
$controle.=get_forums("tout",$query_forum,$pack,$mode_affichage);



if ($mode_affichage=="htmlfleche" or $mode_affichage=="html") {
	debut_page(_T('titre_page_forum_suivi'), "redacteurs", "forum-controle");
	echo "<br><h1><a href=\"_test.php3?mode_affichage=html&bonjour=oui&set_ecran=large\">CLIQUER ICI </a></h1><br><br>";
	debut_gauche_ben();
	debut_boite_info();
	echo "<FONT FACE='Verdana,Arial,Sans,sans-serif' SIZE=2>";
	echo $texte_div_invisible;
	echo "</FONT>";
	fin_boite_info();
	debut_droite();
	echo $controle;
	fin_page();
}
else if ($mode_affichage=="rss") {
	echo $controle;
}


?>
