# phpMyAdmin MySQL-Dump
# version 2.3.2
# http://www.phpmyadmin.net/ (download page)
#
# Serveur: localhost
# G�n�r� le : Mardi 26 Juillet 2005 � 15:21
# Version du serveur: 4.00.21
# Version de PHP: 4.3.6
# Base de donn�es: `tw1`
# --------------------------------------------------------

#
# Structure de la table `a_droit`
#

DROP TABLE IF EXISTS `a_droit`;
CREATE TABLE `a_droit` (
  `dr_statut_spip` varchar(50) NOT NULL default '-',
  `dr_statut_act` varchar(50) NOT NULL default '-',
  `dr_action` varchar(50) NOT NULL default '-',
  `dr_objet` varchar(50) NOT NULL default '-',
  `dr_droit` varchar(50) NOT NULL default '-',
  `dr_condition` varchar(255) NOT NULL default '-',
  `dr_post_process` varchar(255) NOT NULL default '-',
  `a_droit_id` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`a_droit_id`)
) TYPE=MyISAM;

#
# Contenu de la table `a_droit`
#

INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'preparer', 'article', 'O', '', '', 1);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'preparer', 'breve', 'O', '', '', 2);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'preparer', 'rubrique', 'O', '', '', 3);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'preparer', 'site', 'O', '', '', 4);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'preparer', 'mot', 'O', '', '', 5);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'preparer', 'forum', 'O', '', '', 6);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'soumettre', 'article', 'O', '', '', 8);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'soumettre', 'breve', 'O', '', '', 9);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'soumettre', 'rubrique', 'N', '', '', 10);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'soumettre', 'site', 'O', '', '', 11);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'soumettre', 'mot', 'O', '', '', 12);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'soumettre', 'forum', 'O', '', '', 13);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'publier', 'article', 'N', '', '', 22);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'publier', 'breve', 'N', '', '', 23);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'publier', 'rubrique', 'N', '', '', 24);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'publier', 'site', 'N', '', '', 25);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'publier', 'mot', 'N', '', '', 26);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'publier', 'forum', 'N', '', '', 27);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'effacer', 'article', 'N', '', '', 29);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'effacer', 'breve', 'N', '', '', 30);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'effacer', 'rubrique', 'N', '', '', 31);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'effacer', 'site', 'N', '', '', 32);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'effacer', 'mot', 'N', '', '', 33);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'effacer', 'forum', 'N', '', '', 34);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'modifier', 'article', 'O', '', '', 36);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'modifier', 'breve', 'O', '', '', 37);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'modifier', 'rubrique', 'O', '', '', 38);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'modifier', 'site', 'O', '', '', 39);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'modifier', 'mot', 'O', '', '', 40);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'modifier', 'forum', 'O', '', '', 41);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'changer statut', 'article', 'N', '', '', 43);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'changer statut', 'breve', 'N', '', '', 44);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'changer statut', 'rubrique', 'N', '', '', 45);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'changer statut', 'site', 'N', '', '', 46);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'changer statut', 'mot', 'N', '', '', 47);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'changer statut', 'forum', 'N', '', '', 48);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'preparer', 'breve', 'O', '', '', 100);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'preparer', 'rubrique', 'O', '', '', 101);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'preparer', 'site', 'O', '', '', 102);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'preparer', 'mot', 'O', '', '', 103);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'preparer', 'forum', 'O', '', '', 104);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'soumettre', 'article', 'O', '', '', 106);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'soumettre', 'breve', 'O', '', '', 107);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'soumettre', 'rubrique', 'O', '', '', 108);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'soumettre', 'site', 'O', '', '', 109);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'soumettre', 'mot', 'O', '', '', 110);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'soumettre', 'forum', 'O', '', '', 111);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'publier', 'article', 'O', '', '', 120);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'publier', 'breve', 'O', '', '', 121);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'publier', 'rubrique', 'O', '', '', 122);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'publier', 'site', 'O', '', '', 123);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'publier', 'mot', 'O', '', '', 124);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'publier', 'forum', 'O', '', '', 125);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'effacer', 'article', 'O', '', '', 127);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'effacer', 'breve', 'O', '', '', 128);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'effacer', 'rubrique', 'O', '', '', 129);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'effacer', 'site', 'O', '', '', 130);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'effacer', 'mot', 'O', '', '', 131);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'effacer', 'forum', 'O', '', '', 132);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'modifier', 'article', 'O', '', '', 134);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'modifier', 'breve', 'O', '', '', 135);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'modifier', 'rubrique', 'O', '', '', 136);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'modifier', 'site', 'O', '', '', 137);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'modifier', 'mot', 'O', '', '', 138);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'modifier', 'forum', 'O', '', '', 139);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'changer statut', 'article', 'O', '', '', 141);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'changer statut', 'breve', 'O', '', '', 142);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'changer statut', 'rubrique', 'O', '', '', 143);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'changer statut', 'site', 'O', '', '', 144);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'changer statut', 'mot', 'N', '', '', 145);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'changer statut', 'forum', 'O', '', '', 146);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'changer statut', 'langue', 'N', '', '', 148);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'effacer', 'langue', 'N', '', '', 149);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'modifier', 'langue', 'O', '', '', 150);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'preparer', 'langue', 'N', '', '', 151);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'publier', 'langue', 'N', '', '', 152);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'soumettre', 'langue', 'N', '', '', 153);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'changer statut', 'langue', 'N', '', '', 155);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'effacer', 'langue', 'N', '', '', 156);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'modifier', 'langue', 'N', '', '', 157);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'preparer', 'langue', 'N', '', '', 158);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'publier', 'langue', 'N', '', '', 159);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'soumettre', 'langue', 'N', '', '', 160);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'changer statut', 'auteur', 'O', '', '', 169);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'effacer', 'auteur', 'O', '', '', 170);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'modifier', 'auteur', 'O', '', '', 171);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'preparer', 'auteur', 'N', '', '', 172);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'publier', 'auteur', 'O', '', '', 173);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'soumettre', 'auteur', 'N', '', '', 174);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'changer statut', 'auteur', 'N', '', '', 176);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'effacer', 'auteur', 'N', '', '', 177);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'modifier', 'auteur', 'N', '', '', 178);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'preparer', 'auteur', 'N', '', '', 179);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'publier', 'auteur', 'N', '', '', 180);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'soumettre', 'auteur', 'N', '', '', 181);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'changer statut', 'trad', 'N', '', '', 190);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'effacer', 'trad', 'N', '', '', 191);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'modifier', 'trad', 'N', '', '', 192);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'preparer', 'trad', 'N', '', '', 193);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'publier', 'trad', 'N', '', '', 194);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('0minirezo', 'administrateur', 'soumettre', 'trad', 'N', '', '', 195);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'changer statut', 'trad', 'N', '', '', 197);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'effacer', 'trad', 'N', '', '', 198);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'modifier', 'trad', 'N', '', '', 199);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'preparer', 'trad', 'N', '', '', 200);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'publier', 'trad', 'N', '', '', 201);
INSERT INTO `a_droit` (`dr_statut_spip`, `dr_statut_act`, `dr_action`, `dr_objet`, `dr_droit`, `dr_condition`, `dr_post_process`, `a_droit_id`) VALUES ('1comite', 'redacteur', 'soumettre', 'trad', 'N', '', '', 202);