<?
// ---------------------------------------------
//	Fonction pour lire l'int�gralit� des droits d'un utilisateur
//  Retourne un bool�en
//	Updated Alain 23/06/05
//  Updated Alain 19/07/05
// ---------------------------------------------

function auteur_droit($action, $objet, $objet_id, $statut_auteur='auto', $id_auteur='auto') {
	global $auteur_session;
	global $display;
	
	if ($id_auteur == 'auto') 	 {$id_auteur = $auteur_session['id_auteur'];}
	if ($statut_auteur =='auto') {$statut_auteur = $auteur_session['statut'];}
	
	if ($display) {print "check si $statut_auteur ($id_auteur) peut $action  $objet($objet_id)<br><br>";}
	
	// ---------------------------------------------
	//		DROITS G�N�RAUX
	//      statut =>? action sur objet
	// ---------------------------------------------
	$qgd = "select * from a_droit
			where dr_statut_spip = '$statut_auteur'
			and dr_objet = '$objet'
			and dr_action = '$action'
			";
			
	$rgd = mysql_query($qgd);
	$tgd = mysql_num_rows($rgd);
	
	if ($tgd == 1) {
		$obj = mysql_fetch_object($rgd);
		$resultat			= $obj -> dr_droit;
	}
	else { // au cas o�
		$resultat = 'N';
	}
		
	// ---------------------------------------------
	//		DROITS PARTICULIERS RESTREIGNANT LES DROITS GENERAUX
	//      ssi $resultat = O
	// ---------------------------------------------
	
	
	
	// ---------------------------------------------
	//		restriction admin restreint
	// 		pour admin
	//      pourrait �tre pour r�dacteur, ??
	// ---------------------------------------------
	
	// sur rubrique
	if ($resultat == 'O' 
		and $objet == 'rubrique' 
		and ($statut_auteur == '0minirezo')) {
		
		// is retriction in spip_auteurs_rubriques ?
		$qaurub = "select * from spip_auteurs_rubriques
				where id_auteur = '$id_auteur'
				";
		$raurub = mysql_query($qaurub);
		$taurub = mysql_numrows($raurub);
		
		if ($taurub > 0) {
			// is rubrique comptabile ??
			$qrub = "select * from spip_auteurs_rubriques
				where id_auteur = '$id_auteur'
				and id_rubrique = '$objet_id'
				";
			$rrub = mysql_query($qrub);
			$trub = mysql_numrows($rrub);
			
				if ($trub == 0) {
					$resultat = 'N';
				}
				else {
					// no restriction specific
				}
			}
		}
		else {
			// no restriction global
		}
	// ----------------------------------------------
	
	// sur article associ�
	if ($resultat == 'O' 
		and $objet == 'article' 
		and ($statut_auteur == '0minirezo')) {
		
		// is retriction in spip_auteurs_rubriques ?
		$qaurub = "select * from spip_auteurs_rubriques
				where id_auteur = '$id_auteur'
				";
		$raurub = mysql_query($qaurub);
		$taurub = mysql_numrows($raurub);
		
		
		if ($taurub > 0) {
			// is article compatible with rubrique ??
			$qrub = "select * from spip_auteurs_rubriques as sarub, spip_articles as sart
				where sarub.id_auteur = '$id_auteur'
				and sart.id_rubrique = sarub.id_rubrique
				and sart.id_article = '$objet_id'
				";
			$rrub = mysql_query($qrub);
			$trub = mysql_numrows($rrub);
			
				if ($trub == 0) {
					$resultat = 'N';
				}
				else {
					// no restriction specific
				}
		}
		
		else {
			// no restriction global
		}
			
	}
	// ----------------------------------------------
	// booleanify resultat
	if ($display) {print "resultat = $resultat<br>";}

	
	if ($resultat == 'N') {$resultat_bool = false;}
	else {$resultat_bool = true;}
	
	return $resultat_bool; 
}



// ---------------------------------------------
//		function auteur_statut
//      raccourci
// ---------------------------------------------
function auteur_statut ($any_statut, $id_auteur='auto') {
	global $auteur_session;
	global $display;
	
	$statut_cts = array(
					'administrateur' => '0minirezo',
					'redacteur' => '1comite'
					);
	
	if ($id_auteur == 'auto') {$id_auteur = $auteur_session['id_auteur'];}
	$statut_auteur_spip = $auteur_session['statut'];
	$any_statut_spip = $statut_cts["$any_statut"];
	
	if ($display) {print "sp = $statut_auteur_spip / as = $any_statut / asc = $any_statut_spip<br><br>";}
	
	if ($any_statut == $statut_auteur_spip or $any_statut_spip == $statut_auteur_spip) {
		$resultat = true;
	}
	else {
		$resultat = false;
	}
	return $resultat;
}

// ---------------------------------------------
//		function auteur_self
//      raccourci
// ---------------------------------------------
function auteur_self () {
	global $auteur_session, $id_auteur;
	global $display;

	if ($id_auteur == $auteur_session['id_auteur']) {
		$resultat = true;
	}
	else {
		$resultat = false;
	}
	return $resultat;
}

// ---------------------------------------------
//		lit les condition specifiques
// ---------------------------------------------
function auteur_condition($action, $objet, $objet_id, $statut_auteur='auto', $id_auteur='auto') {
	global $auteur_session;
	global $display;
	
	if ($id_auteur == 'auto') 	 {$id_auteur = $auteur_session['id_auteur'];}
	if ($statut_auteur =='auto') {$statut_auteur = $auteur_session['statut'];}
	
	if ($display) {print "check condition $statut_auteur ($id_auteur) pour $action  $objet($objet_id)<br><br>";}
	
	// ---------------------------------------------
	//		condition
	//      statut =>? action sur objet
	// ---------------------------------------------
	$qgd = "select * from a_droit
			where dr_statut_spip = '$statut_auteur'
			and dr_objet = '$objet'
			and dr_action = '$action'
			";
			
	$rgd = mysql_query($qgd);
	$tgd = mysql_num_rows($rgd);
	
	if ($tgd == 1) {
		$obj = mysql_fetch_object($rgd);
		$resultat			= $obj -> dr_condition;
	}
	else { // au cas o�
		$resultat = 'N';
	}
	return $resultat;
}
?>