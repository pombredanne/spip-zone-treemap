<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// langue / language = fr

$GLOBALS[$GLOBALS['idx_lang']] = array(


// A
'ajouter' => 'Ajouter',


// F
'fichier_a_creer' => 'Fichier(s) &agrave; cr&eacute;er',
'fichier_modifiables' => 'Fichier(s) modifiables',


// L
'langue_encours' => 'Langue en cours : ',
'liste_module' => '<ul>
<li><a href="?module=public">Public</a></li>
<li><a href="?module=local">Local</a></li>
<li><a href="?module=adminlangue">adminlangue</a></li>
</ul>',


// M
'master' => 'Master',
'modules_a_travailler' => 'Modules &agrave; travailler',


// P
'panneau_multilingue' => 'le panneau multilingue',


// T
'texte_aide_saisie_texte' => 'Texte',
'texte_aide_saisie_variable' => 'Variable, sans <: :>',
'texte_allerbasdepage' => 'Aller en bas de page pour Enregistrer',
'texte_bt_validation' => 'Enregistrer',
'texte_choixmodule' => 'Choisissez d\'abord un module &agrave; travailler',
'texte_fichier_non_existant' => '<br>Fichier master non existant, afin de cr&eacute;ez ce fichier de langue veuillez au pr&eacute;alable cr&eacute;er le fichier de langue de la langue master',
'texte_langue_possible' => 'Les langues possibles sont g&eacute;r&eacute;es dans',
'texte_pas_droit_ecriture' => '<br>Vous n\'avez pas les droits en &eacute;criture sur ce fichier.<br> Veuillez r&eacute;glez les droits sur le fichier gr&acirc;ce &agrave; votre logiciel FTP en mode 777  puis recharger la page',
'texte_pas_droit_lecture' => 'Vous n\'avez pas les droits en lecture sur ce fichier.<br> Veuillez r&eacute;glez les droits sur le fichier gr&acirc;ce &agrave; votre logiciel FTP en mode 777 puis recharger la page',
'texte_pour_effacer' => 'Pour effacer une entr&eacute;e du master, mettez son texte &agrave; vide',
'texte_retour_master' => 'L\'ajout de "cha&icirc;ne de langue" se fait &agrave; partir du master, alias le fichier dans la langue par d&eacute;faut du site',
'texte_sauvegarde' => 'Par s&eacute;curit&eacute;, le fichier de la traduction initiale a &eacute;t&eacute; sauvegard&eacute; :'


);

?>