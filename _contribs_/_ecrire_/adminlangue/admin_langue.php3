<?php
// ---------------------------------------------
//	Admin_langue
// 	v 0.94 - last modification: 23/12/05
//  simeray@tektonika.com
// ---------------------------------------------

include ("inc.php3");
include_ecrire ("inc_config.php3");
lire_metas();

// nombre de boites de saisie vides dans les formulaires, pour les ajouts
$addition_volume = 5;

// affichage, dans la page, d'infos de debug
$display_debug = false;

// ????
$writedb = false;

// ---------------------------------------------
//		INIT STRINGS
// ---------------------------------------------
// la langue "master" = celle de refenrece
$master_lang = $meta['langue_site'];

// fyi:
	// $spip_lang 					<=> langue de l'admin;
	// $meta['langues_proposees'] 	<=> multilinguisme
	// $meta['langues_utilisees'] 	<=> langues possible du site

// default if needed
	//if (!$module) $module = "local";

// liste des modules a gerer : les existants dans le repertoire des langues
// et local "de force"
// on monte un tableau avec des noms de modules comme cle et comme valuer, le
// repertoire ou ils sont situes
$modules= array();
foreach(glob(_DIR_LANG . '*_' . $master_lang . '.php3') as $fichier) {
	$m= preg_replace(':' . _DIR_LANG . '(.*)_' . $master_lang . '.php3:',
		'$1', $fichier);
	$modules[$m]= _DIR_LANG;
}

// on cherche local_* dans le path, et s'il est introuvable, on le force
// dans dir_lang
if(!$modules['local']) {
	if($f=find_in_path('local_' . $master_lang . '.php3')) {
		$modules['local']= dirname($f).'/';
	} else {
		$modules['local']= _DIR_LANG;
	}
}

// et si on demande un nouveau module, on le case de force
if($module && !$modules[$module]) {
	$modules[$module]= _DIR_LANG;
}

// ---------------------------------------------
//		DISPLAY SPIP HEADERS
// ---------------------------------------------
debut_page(_T('module_fichier_langue').": $module", "administration", "langues");
echo "<br><br><br>";
gros_titre(_T('module_fichier_langue').": $module");
if ($connect_statut != '0minirezo' OR !$connect_toutes_rubriques) {
	echo _T('avis_non_acces_page');
	fin_page();
	exit;
}
barre_onglets("config_lang", "fichiers");


// ---------------------------------------------
//		DEFINE FILES
// ---------------------------------------------
if ($display_debug) {
	print "ENTRY<br>module = $module / target lang = $target_lang<br><br>";
	if ($Submit) {print "FORM SUBMITED<br>";}
	else {print "FORM not submited<br>";}
}

if ($module and $target_lang) {
	
	$target_file 			 = $module . "_" . "$target_lang.php3";
	$master_file 			 =  $module . "_" . "$master_lang.php3";
	
	$target_file_full 		 = $modules[$module] . $target_file;
	$target_file_full_backup = $target_file_full . '_bak';
	$master_file_full 		 = $modules[$module] . $master_file;
	
	$new_file_full 			 = $target_file_full; // could be != if needed
	
	if(@is_readable($target_file_full) and $display_debug) {print "target found<br>";}
	if(@is_readable($master_file_full) and $display_debug) {print "master found<br>";}
}


// ---------------------------------------------
//		TREAT
// ---------------------------------------------
if ($Submit) {
	
	if ($display_debug) {print "TREAT : Enregistrement du fichier langue : $target_file<br><br>";}
	
	if ($mode == 'work'){
		// ---------------------------------------------
		//		backup init spip files if needed
		// ---------------------------------------------
		if(@!is_readable($target_file_full_backup)) {
			copy ($target_file_full, $target_file_full_backup);
			print "<p class='arial2'>Par s&eacute;curit&eacute;, le fichier de la traduction initiale a &eacute;t&eacute; sauvegard&eacute; : 
			$target_file_full_backup</p>";
		}
	}


	// initialiser la liste avec le post
	$target_table = $_POST['target_table'];

	// ---------------------------------------------
	//		GET ADDITION
	// ---------------------------------------------
	// puis y ajouter les nouvelles entr�es
	for ($i = 0; $i < $addition_volume; $i++) {
		$new_value_item = $new_value[$i]; 
		$new_trad_item = $new_trad[$i];
		
		if ($display_debug) {print "nvi : $new_value_item / nti $new_trad_item<br>";}
		
		if ($new_value_item != '' and $new_trad_item != '') {
			$new_value_item = filter_value_entry($new_value_item);
			$target_table[$new_value_item] = $new_trad_item;
		}
	}
	@ksort($target_table);
	
	// ---------------------------------------------
	//		PREPARE AND FORMAT
	//      empty $trad <=> delete from file
	// ---------------------------------------------
	$ctr = 0;
	while (list($item, $trad) = each ($target_table)) {
		$trad = filter_trad_entry($trad);
		if ($trad != '') {$any_line[$ctr] = "'$item' => '$trad',";$ctr++;}
		if ($display_debug) {print $any_line[$ctr-1] . "<br>";}
	}

	// remove last ,
	$any_line[$ctr - 1] = substr($any_line[$ctr - 1], 0, -1);
	

	// ---------------------------------------------
	//		WRITE RESULTING FILE
	// ---------------------------------------------
	$f = fopen($new_file_full,"w");
			
	// ---------------------------------------------
	//		write header
	// ---------------------------------------------
	wf($f, "<?php");
	wf($f, "");
	wf($f, "// This is a SPIP language file  --  Ceci est un fichier langue de SPIP");
	wf($f, "// langue / language = $target_lang");
	wf($f, "");
	wf($f, "\$GLOBALS[\$GLOBALS['idx_lang']] = array(");
	
	// ---------------------------------------------
	//		write body
	// ---------------------------------------------
	$prev_letter = '';
	for ($i = 0; $i < count($any_line); $i++) {
		$first_letter = substr($any_line[$i], 1,1);
		
		if ($first_letter != $prev_letter) {
			$first_cap_letter = strtoupper($first_letter);
			wf($f, "");
			wf($f, "");
			wf($f, "// $first_cap_letter");
			$prev_letter = $first_letter;
		}
		
		wf($f, $any_line[$i]);
	}
	wf($f, "");
	
	// ---------------------------------------------
	//		write footer
	// ---------------------------------------------
	wf($f, "");
	wf($f, ");");
	wf($f, "");
	wf($f, "?>");

	$mode = 'work'; // end of create mode, switch to work mode
}



// ---------------------------------------------
//		FOR MODULE
// ---------------------------------------------

if ($module) {
	// ---------------------------------------------
	//		get existing lang files name in /ecrire/lang
	// ---------------------------------------------
	$dir_lang = opendir($modules[$module]);
	$ctr = 0;
	while (($lang_file = readdir($dir_lang)) != '') {
		// Eviter ".", "..", ".htaccess", etc.
		if ($lang_file[0] == '.') continue;
		if ($lang_file == 'CVS') continue;
	
		$lang_file_name = $modules[$module].$lang_file;
		if (is_file($lang_file_name)) {
			$lang_file_table[$ctr] = $lang_file;
			$ctr++;
		}
	}
	closedir($dir_lang);
	
	if ($display_debug) {print_r($lang_file_table);}
	
	// ---------------------------------------------
	//		get possible lang
	// ---------------------------------------------
	init_codes_langues();
	$langues = $GLOBALS['codes_langues'];
	
	
	$lang_install = explode(',', $GLOBALS['all_langs']);
	$lang_author = explode(',', lire_meta('langues_multilingue'));
	
	if ($display_debug) {
		print "<br><br>INSTALLED<br>";
		print_r($lang_install);
		
		print "<br><br>AUTHORIZED<br>";
		print_r($lang_author);
		
		print count($lang_author);
	}

	// ---------------------------------------------
	//		prepare interface
	// ---------------------------------------------
	$available_file = array();
	$file_to_create = array();
	
	$ctrok = $ctrnotok = 0;
	for ($i = 0; $i < count($lang_author); $i++) {
		$str_to_search = $module . '_' . $lang_author[$i] . '.php3';
		
		if (@in_array($str_to_search, $lang_file_table)) {
			$available_file[$ctrok] = $lang_author[$i];
			$ctrok++;
		}
		else {
			$file_to_create[$ctrnotok] = $lang_author[$i];
			$ctrnotok++;
		}
	}
	
	// ---------------------------------------------
	//		show resulting choice
	// ---------------------------------------------
	for ($i = 0; $i < count($available_file); $i++) {
		$any_lang_lib 	= $langues[$available_file[$i]];
		$any_lang_code 	= $available_file[$i];
		
		// bold master lang
		if ($any_lang_code == $master_lang) {$any_lang_lib = "<strong>$any_lang_lib</strong>";}
		$bloc_lang_work .= "<a href=\"?module=$module&target_lang=$any_lang_code&mode=work\">$any_lang_lib</a>, "; 
	}
	
	for ($i = 0; $i < count($file_to_create); $i++) {
		$any_lang_lib 	= $langues[$file_to_create[$i]];
		$any_lang_code 	= $file_to_create[$i];
		
		// bold master lang
		if ($any_lang_code == $master_lang) {$any_lang_lib = "<strong>$any_lang_lib</strong>";}
		$bloc_lang_create .= "<a href=\"?module=$module&target_lang=$any_lang_code&mode=create\">$any_lang_lib</a>, ";
	}
}


// ----------------------------------

// ---------------------------------------------
//		display bloc gauche (module)
// ---------------------------------------------
debut_gauche();
echo debut_cadre_relief();

print "Modules &agrave; travailler<ul>";
foreach($modules as $m => $dir) {
	print "<li><a href=\"?module=$m\">$m</a>\n";
}
print "</ul>";
print "<form name='formcreer' method='get' action=''>\n";
print "<input type='hidden' name='mode' value='create'>\n";
print "<input type='hidden' name='target_lang' value='$master_lang'>\n";
print "<input type='text' size='8' name='module'>\n";
print "<input type='submit' name='creer' value='Nouveau module'>\n";
print "</form>\n";

if ($display_debug) {
	print "Mode = $mode<br>module = $module<br>";
	print "<br>Master lang: $master_lang<br>";
	print "Target lang: $target_lang<br>";
	print "DIR TO WRITE = " . _DIR_LANG . "<br>";
	print "FILE TO READ = $target_file_full <br>FILE TO WRITE = $new_file_full<br><br>";
}

if ($target_lang) {
	$target_lang_lib = $langues[$target_lang];
	print "<p>Langue en cours : $target_lang_lib</p>";

}

echo fin_cadre_relief();

// ------------------



// ---------------------------------------------
//		FORM, FOR ANY MODULE AND LANG
// ---------------------------------------------
debut_droite();
	if ($module) {
		print "<span class='arial2'><strong>Fichier(s) modifiables</strong> : $bloc_lang_work</span>";
		print "<p class='arial2'><strong>Fichier(s) &agrave; cr&eacute;er</strong> : $bloc_lang_create. <br>";
		print "<i>Les langues possibles sont g&eacute;r&eacute;es dans 
		<a href=\"config-multilang.php3\">le panneau multilingue</a></i></p>";
		if ($master_lang == $target_lang) {
			print "<p class='arial2'>Pour effacer une entr&eacute;e du master, mettez son texte &agrave; vide</p>";
		}
	}
	else {
		print "<p class='arial2'>Choisissez d'abord un module &agrave; travailler</p>";
	}

if ($module and $target_lang) {
	// ---------------------------------------------
	//		get initial data
	// ---------------------------------------------
	if ($display_debug) {print "master = $master_file<br>";}
	
	$protect_idx_lang = $GLOBALS['idx_lang'];
	
	$GLOBALS['idx_lang'] = 'i18n' . '_' . $module . '_' . $master_lang;

	// Cas particulier : chercher local_* dans le path, et pas seulement
	// dans INC_LANG
	
	if ($f = (find_in_path('local_'.$lang.'.php3'))) {
	}

	include($master_file_full);
	$master_table = $GLOBALS[$GLOBALS['idx_lang']];
	@ksort($master_table);
	

	if ($mode == 'work') {
		if ($display_debug) {print "master = $master_file / target = $target_file<br><br>";}
		$GLOBALS['idx_lang'] = 'i18n' . '_' . $module . '_' . $target_lang;
		include($target_file_full);
		$target_table = $GLOBALS[$GLOBALS['idx_lang']];
		@ksort($target_table);
	}
	else {
		$target_table = array();
	}
	
	//reset idex_lang
	$GLOBALS['idx_lang'] = $protect_idx_lang;
	
	$table_color = '#CCCCCC';
	$table_color = '';
	$index_color = $couleur_foncee;
	$normal_color = '';
	$altern_color = '#EEEEEE';
	
	$switch_color= false;
	print "<form name=\"form1\" method=\"post\" action=\"\">\r";

	if (count($master_table) > 0) { // do not display update form if target file is empty
		print "<table border=\"0\" cellspacing=\"1\" cellpadding=\"2\" bgcolor=\"$table_color\" width=\"95%\">\r";
		
		@reset($master_table);
		@reset($target_table);
		$first_letter = '';
		while (list($value_to_change, $master_to_change) = @each ($master_table)) {
			
			$cell_color = ($switch_color) ? $normal_color : $altern_color;
			$style_area = "font-family:arial;font-size:12px; background:$cell_color";
			
			$first_letter = substr($value_to_change, 0,1);
				
				if ($first_letter != $prev_letter) {
					$first_cap_letter = strtoupper($first_letter);
					print "<tr class='verdana3' style=\"background-color: $couleur_foncee; color:white; padding: 3px; font-weight:bold;\">";
					print "<td style=\"text-align:right;\">$first_cap_letter</td>";
					print  "<td style=\"text-align:right;\">Master</td>";
					print "<td style=\"text-align:right;\"><a href='#save_lang_file' style='color:white; text-decoration:underline'>Aller en bas de page pour Enregistrer</a></td>";
					print "</tr>";
					$prev_letter = $first_letter;
				}
			
			$master_to_change = stripslashes($master_to_change);
			$target_to_change = $target_table["$value_to_change"];
			
			if ($target_to_change == '') {$style_master_2 = "color:darkred; font-weight:bold; text-align:right;";}
			else {$style_master_2 = "color:black; font-weight:normal; text-align:right;";}
			
			
			print "<tr bgcolor=\"$cell_color\">\r";
				print "<td style=\"text-align:right;\" width=\"20%\" class='verdana2' valign=\"top\">\r";	
				print "<strong><:$value_to_change:></strong>";
				print "</td>\r";
				
				print "<td valign=\"top\" style=\"$style_master_2\" width=\"30%\" class='arial2'>\r";	
				print " $master_to_change";
				print "</td>";
				
				print "<td>";
				//print "<input type=\"text\" name=\"trad_from_file[$value_to_change]\" value=\"$trad_to_change\" size=\"40\"
				//style=\"font-size:10px; background:$cell_color\">";
				print "<textarea name=\"target_table[$value_to_change]\" cols=\"45\" rows=\"1\" wrap=\"soft\"
				style=\"$style_area\">$target_to_change</textarea>";
				print "</td>\r";
				//print "<input type=\"hidden\" name=\"value_to_change\" value=\"$value_to_change\">";
			print "</tr>\r";
			$switch_color = !$switch_color;
		}
		
		// -----------------------------	
		print "<tr style=\"background-color: $couleur_foncee;text-align=\"right\">\r";
		print "<td>&nbsp;</td>";
		print "<td>&nbsp;</td>";
		print "<td align='right' valign='bottom' >";
		
		print  "<input id='save_lang_file' type=\"submit\" name=\"Submit\" value=\"Enregistrer\">";	
		//print  "<a href=\"javascript:window.close();\" class=\"boutonBOform\">Fermer cette fen&ecirc;tre</a>";
		print "</td>\r";
		print "</tr>\r";	
		print "</table>";
		
		print "<p>&nbsp;</p>";
	} // end master file empty
	
	// ---------------------------------------------
	//		ADDITION ZONE
	// 		only if master = target ??
	// ---------------------------------------------
	if ($master_lang == $target_lang) {
		print "<table border=\"0\" cellspacing=\"1\" cellpadding=\"2\" bgcolor=\"$table_color\" width=\"95%\">\r";
		
		print "<tr class='verdana3' style=\"background-color: $couleur_foncee; color:white; padding: 3px; font-weight:bold;\">\r";
		print "<td style=\"text-align:right;\">Ajouter</td>";
		print "<td>&nbsp;</td>";
		print "</tr>";
		
		print "<tr class='verdana2' style=\"background-color: $couleur_foncee; color:white; padding: 3px; font-weight:bold;\">\r";
		print "<td style=\"text-align:right;\">Variable, sans <: :></td>";
		print "<td>Texte</td>";
		print "</tr>";
		
		$switch_color= false;
		for ($i = 0; $i < $addition_volume; $i++) {
			$cell_color = ($switch_color) ? $normal_color : $altern_color;
			$style_area = "font-family:arial;font-size:12px; background:$cell_color";
	
			print "<tr bgcolor=\"$cell_color\">\r";
			
					
			print "<td valign=\"top\">";
					print "<input type=\"text\" name=\"new_value[$i]\" value=\"\" size=\"40\"
					style=\"font-size:10px; background:$cell_color\">";
			print "</td>";	
			
			print "<td>\r";
					print "<textarea name=\"new_trad[$i]\" cols=\"45\" rows=\"1\" wrap=\"soft\"
					style=\"$style_area\"></textarea>";
			print "</td>\r";
			$switch_color = !$switch_color;
		}
		
		print "</tr>";
		
		print "<tr style=\"background-color: $couleur_foncee;text-align=\"right\">\r";
		print "<td>&nbsp;</td>";
		print "<td align='right' valign='bottom' >";
		
		print  "<input id='save_lang_file' type=\"submit\" name=\"Submit\" value=\"Enregistrer\">";	
		//print  "<a href=\"javascript:window.close();\" class=\"boutonBOform\">Fermer cette fen&ecirc;tre</a>";
		print "</td>\r";
		print "</tr>\r";
		print "</table>\r";
	}
	else {
		$link_master = "<a href=\"?module=$module&target_lang=$master_lang&mode=work\"><strong>master</strong></a>";
		print "<p class='verdana2'>L'ajout de cha&icirc;ne de langue se fait &agrave; partir du $link_master, alias le fichier dans la langue par d&eacute;faut du site</p>";
	} // end if addition
		
	print "</form>\r";
}
else {
	//print "ici, texte d'explication compl&eacute;mentaire possible...<br>";
} // end if module & target_lang
fin_page();

// ===============================================================
//		FUNCTION ZONE	
// ===============================================================

// ---------------------------------------------
//	Write File, line by line
// ---------------------------------------------
function wf($myFile, $any_string, $end_line="\n") {
	if (substr($any_string, -2) != "?>") {
		$any_string = $any_string . $end_line;
	} // append end_line except ending php file
	if ($myFile != "") { fputs($myFile, "$any_string"); }
	else { print "!! Error - File to write is undefined<br>\r"; }
}

// ---------------------------------------------
//		filter_value_entry
// ---------------------------------------------
function filter_value_entry($any_string) {
	$rstr =  "";
	$rstr = trim($any_string);
	$rstr = ereg_replace('<|>|:', '', $rstr);
	$rstr = ereg_replace(' +', ' ', $rstr);
	$rstr = ereg_replace(' |-', '_', $rstr);
	return $rstr;
}


// ---------------------------------------------
//		filter_trad_entry
// ---------------------------------------------
function filter_trad_entry($any_string) {
	$rstr =  '';
	if (!get_magic_quotes_runtime() and !get_magic_quotes_gpc()) {
		$rstr = ereg_replace("'", "\\'", $any_string); // slash only single quote
	} 
	else {$rstr = $any_string;}
	//$rstr = strip_tags($rstr, "<b><i><strong><em><br>"); // hum, anytime...
	$rstr = htmlentities($rstr);
	$rstr = str_replace('&quot;', '"', $rstr);
	$rstr = str_replace('&amp;', '&', $rstr);
	$rstr = str_replace('&lt;', '<', $rstr);
	$rstr = str_replace('&gt;', '>', $rstr);
	$rstr = eregi_replace('<b>', '<strong>', $rstr);
	$rstr = eregi_replace('</b>', '</strong>', $rstr);
	$rstr = eregi_replace('<i>', '<em>', $rstr);
	$rstr = eregi_replace('</i>', '</em>', $rstr);
	
	return $rstr;
}
?>