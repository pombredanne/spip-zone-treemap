<?php
// ---------------------------------------------
//	Admin_langue.php3
//  spip addition to manage language files
$adml_version =  "v 0.94a2 - Updated Alain 3/01/06";
//  simeray@tektonika.com
// ---------------------------------------------
$time_start = getmicrotime();


include ("inc.php3");
include_ecrire ("inc_config.php3");
lire_metas();

// ---- user options -------------
$addition_volume 	= 5;		// nb field for add
$file_backup_option = true;		// to backup init lang files

// ---- dev options --------------
$display_debug 		= false;	// to debug
$display_debug_full = false;	// to debug deeper
$writedb 			= false;	// to write file or not


// ---------------------------------------------
//		calc lang file_root
// ---------------------------------------------
$lang_file_root = str_replace(substr(strrchr($_SERVER['SCRIPT_FILENAME'], '/'), 1), "", $_SERVER['SCRIPT_FILENAME']) . 'lang';

if ($display_debug) {
	print "admin_langue $adml_version<br><br>";
	//print "lang_file_root = $lang_file_root<br>";
}
// ---------------------------------------------
//		INIT STRINGS
// ---------------------------------------------
$master_lang = $meta['langue_site'];

// fyi:
	// $spip_lang 					<=> langue de l'admin;
	// $meta['langues_proposees'] 	<=> multilinguisme
	// $meta['langues_utilisees'] 	<=> langues possible du site

// default if needed
	//if (!$module) $module = "local";

// ---------------------------------------------
//		DISPLAY SPIP HEADERS
// ---------------------------------------------
debut_page(_T('module_fichier_langue').": $module", "administration", "langues");
echo "<br><br><br>";
gros_titre(_T('module_fichier_langue').": $module");
if ($connect_statut != '0minirezo' OR !$connect_toutes_rubriques) {
	echo _T('avis_non_acces_page');
	fin_page();
	exit;
}
barre_onglets("config_lang", "fichiers");


// ---------------------------------------------
//		DEFINE FILES
// ---------------------------------------------
if ($display_debug) {
	print "ENTRY<br>module = $module / target lang = $target_lang<br><br>";
	if ($Submit) {print "FORM SUBMITED<br>";}
	else {print "FORM not submited<br>";}
}

if ($module and $target_lang) {
	
	$target_file 			 =  $module . "_" . "$target_lang.php3";
	$master_file 			 =  $module . "_" . "$master_lang.php3";
	
	$target_file_full 		 = "$lang_file_root/$target_file";
	$target_file_full_backup = $target_file_full . '_bak';
	$master_file_full 		 = "$lang_file_root/$master_file";
	
	$new_file_full 			 = $target_file_full; // could be != if needed
	
	// ---------------------------------------------
	//	checking
	//  	read master
	//		write target
	// ---------------------------------------------
	if(@file_exists($target_file_full) and !@is_writable($target_file_full)) {
		print _T('adminlangue:texte_pas_droit_ecriture');
		exit;
	}
	
	if(@file_exists($master_file_full)) {
		if  (!@is_readable($master_file_full)) {
			print _T('adminlangue:texte_pas_droit_lecture');
			exit;
		}
	}
	else {
		if ($master_lang != $target_lang) {
			$link_redo = "?module=$module&target_lang=$master_lang&mode=create";
			print _T('adminlangue:texte_fichier_non_existant');
			exit;
		}
	}
	
		
}


// ---------------------------------------------
//		TREAT
// ---------------------------------------------
if ($Submit) {
	$target_table = $_POST['target_table'];
	
	if ($display_debug) {print "<H1>TREAT : Enregistrement du fichier langue : $target_file</H1><br>";}
	
	if ($mode == 'work'){
		// ---------------------------------------------
		//		backup init spip files if needed
		// ---------------------------------------------
		if(@!is_readable($target_file_full_backup) and $file_backup_option) {
			copy ($target_file_full, $target_file_full_backup);
			print "<p class='arial2'>" . _T('adminlangue:texte_sauvegarde'). " $target_file" . "_bak</p>";	
		}
	}
	// ---------------------------------------------
	//		GET ADDITION
	// ---------------------------------------------
	for ($i = 0; $i < $addition_volume; $i++) {
		$new_value_item = $new_value[$i]; 
		$new_trad_item = $new_trad[$i];
	
		if ($new_value_item != '' and $new_trad_item != '') {
			if ($display_debug) {print "nvi : $new_value_item / nti $new_trad_item<br>";}
			$new_value_item = filter_value_entry($new_value_item);
			$target_table[$new_value_item] = $new_trad_item;
		}
	}
	@ksort($target_table);
	
	// ---------------------------------------------
	//		PREPARE AND FORMAT
	//      empty $trad <=> delete from file
	// ---------------------------------------------
	
	$ctr = 0;
	

	if ($display_debug_full) {
		print "<pre>";
		print "<H1>target table</H1>";
		print_r($target_table);
		print "</pre><hr>";
	}
	
	
	while (list($item, $trad) = each ($target_table)) {
		$trad = filter_trad_entry($trad);
		if ($trad != '') {
			$any_line[$ctr] = "'$item' => '$trad',";
			if ($display_debug) {print $any_line[$ctr] . "<br>";}
			$ctr++;
		}
	}
	
	// remove last ,
	$any_line[$ctr - 1] = substr($any_line[$ctr - 1], 0, -1);
	

	// ---------------------------------------------
	//		WRITE RESULTING FILE
	// ---------------------------------------------
	$f = fopen($new_file_full,"w");
	
	// lock it
	flock ($f, LOCK_EX); 
			
	// ---------------------------------------------
	//		write header
	// ---------------------------------------------
	wf($f, "<?php");
	wf($f, "");
	wf($f, "// This is a SPIP language file  --  Ceci est un fichier langue de SPIP");
	wf($f, "// langue / language = $target_lang");
	wf($f, "");
	wf($f, "\$GLOBALS[\$GLOBALS['idx_lang']] = array(");
	
	// ---------------------------------------------
	//		write body
	// ---------------------------------------------
	$prev_letter = '';
	for ($i = 0; $i < count($any_line); $i++) {
		$first_letter = substr($any_line[$i], 1,1);
		
		if ($first_letter != $prev_letter) {
			$first_cap_letter = strtoupper($first_letter);
			wf($f, "");
			wf($f, "");
			wf($f, "// $first_cap_letter");
			$prev_letter = $first_letter;
		}
		
		wf($f, $any_line[$i]);
	}
	wf($f, "");
	
	// ---------------------------------------------
	//		write footer
	// ---------------------------------------------
	wf($f, "");
	wf($f, ");");
	wf($f, "");
	wf($f, "?>");
	
	flock ($f, LOCK_UN);
	fclose ($f);

	$mode = 'work'; // end of create mode, switch to work mode
} // ================ end treatment ==========================





// ---------------------------------------------
//		FOR MODULE
// ---------------------------------------------

if ($module) {
	// ---------------------------------------------
	//		get existing lang files name in /ecrire/lang
	// ---------------------------------------------
	$dir_lang = opendir($lang_file_root);
	$ctr = 0;
	while (($lang_file = readdir($dir_lang)) != '') {
		// Eviter ".", "..", ".htaccess", etc.
		if ($lang_file[0] == '.') continue;
		if ($lang_file == 'CVS') continue;
	
		$lang_file_name = "$lang_file_root/$lang_file";
		if (is_file($lang_file_name)) {
			$lang_file_table[$ctr] = $lang_file;
			$ctr++;
		}
	}
	closedir($dir_lang);
	
	//if ($display_debug) {print_r($lang_file_table);}
	
	// ---------------------------------------------
	//		get possible lang
	// ---------------------------------------------
	init_codes_langues();
	$langues = $GLOBALS['codes_langues'];
	
	
	$lang_install = explode(',', $GLOBALS['all_langs']);
	$lang_author = explode(',', lire_meta('langues_multilingue'));
	
	if ($display_debug) {
		//print "<br><br>INSTALLED<br>";
		//print_r($lang_install);
		
		//print "<br><br>AUTHORIZED<br>";
		//print_r($lang_author);
		
		//print count($lang_author);
	}
	// ---------------------------------------------
	//		prepare interface
	// ---------------------------------------------
	$available_file = array();
	$file_to_create = array();
	
	$ctrok = $ctrnotok = 0;
	for ($i = 0; $i < count($lang_author); $i++) {
		$str_to_search = $module . '_' . $lang_author[$i] . '.php3';
		
		if (@in_array($str_to_search, $lang_file_table)) {
			$available_file[$ctrok] = $lang_author[$i];
			$ctrok++;
		}
		else {
			$file_to_create[$ctrnotok] = $lang_author[$i];
			$ctrnotok++;
		}
	}
	
	// ---------------------------------------------
	//		show resulting choice
	// ---------------------------------------------
	for ($i = 0; $i < count($available_file); $i++) {
		$any_lang_lib 	= $langues[$available_file[$i]];
		$any_lang_code 	= $available_file[$i];
		
		// bold master lang
		if ($any_lang_code == $master_lang) {$any_lang_lib = "<strong>$any_lang_lib</strong>";}
		$bloc_lang_work .= "<a href=\"?module=$module&target_lang=$any_lang_code&mode=work\">$any_lang_lib</a>, "; 
	}
	

	for ($i = 0; $i < count($file_to_create); $i++) {
		$any_lang_lib 	= $langues[$file_to_create[$i]];
		$any_lang_code 	= $file_to_create[$i];
		
		// bold master lang
		if ($any_lang_code == $master_lang) {$any_lang_lib = "<strong>$any_lang_lib</strong>";}
		$bloc_lang_create .= "<a href=\"?module=$module&target_lang=$any_lang_code&mode=create\">$any_lang_lib</a>, ";
	}
	
	// remove last , and add .
	$bloc_lang_work = substr(trim($bloc_lang_work), 0, -1) . '.';
	$bloc_lang_create = substr(trim($bloc_lang_create), 0, -1) . '.';
}


// ----------------------------------
if ($display_debug) {
	print "Mode = $mode<br>module = $module<br>";
	print "<br>Master lang: $master_lang<br>";
	print "Target lang: $target_lang<br>";
	print "DIR TO WRITE = $lang_file_root<br>";
	print "FILE TO READ = $target_file_full <br>FILE TO WRITE = $new_file_full<br><br>";
}

// ---------------------------------------------
//		display bloc gauche (module)
// ---------------------------------------------
debut_gauche();
echo debut_cadre_relief();

print _T('adminlangue:modules_a_travailler');
print stripslashes(_T('adminlangue:liste_module')); // stripslashes <=> double security against magic quote




if ($target_lang) {
	$target_lang_lib = $langues[$target_lang];
	print "<p>"._T('adminlangue:langue_encours').$target_lang_lib."</p>";

}

echo fin_cadre_relief();

// ------------------



// ---------------------------------------------
//		FORM, FOR ANY MODULE AND LANG
// ---------------------------------------------
debut_droite();
	if ($module) {
		print "<span class='arial2'><strong>"._T('adminlangue:fichier_modifiables')."</strong> : $bloc_lang_work</span>";
		print "<p class='arial2'><strong>"._T('adminlangue:fichier_a_creer')."</strong> : $bloc_lang_create<br>";
		print "<em>"._T('adminlangue:texte_langue_possible')." 
		<a href=\"config-multilang.php3\">"._T('adminlangue:panneau_multilingue')."</a></em></p>";
		if ($master_lang == $target_lang) {
			print "<p class='arial2'>"._T('adminlangue:texte_pour_effacer')."</p>";
		}
	}
	else {
		print "<p class='arial2'>"._T('adminlangue:texte_choixmodule')."</p>";
	}

if ($module and $target_lang) {

	// ---------------------------------------------
	//		get initial data
	//      include file => current lang table = $GLOBALS[$GLOBALS['idx_lang']]
	//      copy $GLOBALS[$GLOBALS['idx_lang']] in new table to keep it active
	// ---------------------------------------------
	if ($display_debug) {print "master_file = $master_file<br>";}
	
	$protect_idx_lang = $GLOBALS['idx_lang'];
	$protect_main_lang_table = $GLOBALS[$GLOBALS['idx_lang']];
	
	//unset($master_table);
	//unset($target_table);
	if ($display_debug) {print "PROTECT: $protect_idx_lang<br>";}
	
	$GLOBALS['idx_lang'] = 'i18n' . '_' . $module . '_' . $master_lang;
	
	if ($display_debug) {
		print "Global master table to load :  ";
		print 'i18n' . '_' . $module . '_' . $master_lang . "<br>";
	}
	
	$GLOBALS['idx_lang'] = 'i18n' . '_' . $module . '_' . $master_lang;
	@include("lang/$master_file");
	$master_table = $GLOBALS[$GLOBALS['idx_lang']];
	@ksort($master_table);
	
	if ($mode == 'work') {
		if ($display_debug) {print "master_file = $master_file / target_file = $target_file<br><br>";}
		$GLOBALS['idx_lang'] = 'i18n' . '_' . $module . '_' . $target_lang;
		@include_lang($target_file);
		$target_table = $GLOBALS[$GLOBALS['idx_lang']];
		@ksort($target_table);
	}
	else {
		$target_table = array();
	}
	
	//reset idx_lang in global
	$GLOBALS['idx_lang'] = $protect_idx_lang;
	$GLOBALS[$GLOBALS['idx_lang']] = $protect_main_lang_table;
	
	if ($display_debug) {print "RESET PROTECTED : $protect_idx_lang<br>";}
	
	if ($display_debug_full) {
		print "<pre>";
		print "<H1>master table</H1>";
		print_r($master_table);
		print "<H1>target table</H1>";
		print_r($target_table);
		print "</pre>";
	}
	
	$table_color = '#CCCCCC';
	$table_color = '';
	$index_color = $couleur_foncee;
	$normal_color = '';
	$altern_color = '#EEEEEE';
	
	$switch_color= false;
	print "<form name=\"form1\" method=\"post\" action=\"\">\r";
		
	if (count($master_table) > 0) { // do not display update form if target file is empty
		print "<table border=\"0\" cellspacing=\"1\" cellpadding=\"2\" bgcolor=\"$table_color\" width=\"95%\">\r";
		
		@reset($master_table);
		@reset($target_table);
		$first_letter = '';
		while (list($value_to_change, $master_to_change) = @each ($master_table)) {
			
			$cell_color = ($switch_color) ? $normal_color : $altern_color;
			$style_area = "font-family:arial;font-size:12px; background:$cell_color";
			
			$first_letter = substr($value_to_change, 0,1);
				
				if ($first_letter != $prev_letter) {
					$first_cap_letter = strtoupper($first_letter);
					print "<tr class='verdana3' style=\"background-color: $couleur_foncee; color:white; padding: 3px; font-weight:bold;\">";
					print "<td style=\"text-align:right;\">$first_cap_letter</td>";
					print  "<td style=\"text-align:right;\">"._T('adminlangue:master')."</td>";
					print "<td style=\"text-align:right;\"><a href='#save_lang_file' style='color:white; text-decoration:underline'>"._T('adminlangue:texte_allerbasdepage')."</a></td>";
					print "</tr>";
					$prev_letter = $first_letter;
				}
			
			$master_to_change = stripslashes($master_to_change);
			$target_to_change = $target_table["$value_to_change"];
			
			if ($target_to_change == '') {$style_master_2 = "color:darkred; font-weight:bold; text-align:right;";}
			else {$style_master_2 = "color:black; font-weight:normal; text-align:right;";}
			
			
			print "<tr bgcolor=\"$cell_color\">\r";
				print "<td style=\"text-align:right;\" width=\"20%\" class='verdana2' valign=\"top\">\r";	
				print "<strong><:$value_to_change:></strong>";
				print "</td>\r";
				
				print "<td valign=\"top\" style=\"$style_master_2\" width=\"30%\" class='arial2'>\r";	
				print " $master_to_change";
				print "</td>";
				
				print "<td>";
				//print "<input type=\"text\" name=\"trad_from_file[$value_to_change]\" value=\"$trad_to_change\" size=\"40\"
				//style=\"font-size:10px; background:$cell_color\">";
				print "<textarea name=\"target_table[$value_to_change]\" cols=\"45\" rows=\"1\" wrap=\"soft\"
				style=\"$style_area\">$target_to_change</textarea>";
				print "</td>\r";
				//print "<input type=\"hidden\" name=\"value_to_change\" value=\"$value_to_change\">";
			print "</tr>\r";
			$switch_color = !$switch_color;
		}
		
		// -----------------------------	
		print "<tr style=\"background-color: $couleur_foncee;text-align=\"right\">\r";
		print "<td>&nbsp;</td>";
		print "<td>&nbsp;</td>";
		print "<td align='right' valign='bottom' >";
		
		print  "<input id='save_lang_file' type=\"submit\" name=\"Submit\" value=\""._T('adminlangue:texte_bt_validation')."\">";	
		//print  "<a href=\"javascript:window.close();\" class=\"boutonBOform\">Fermer cette fen&ecirc;tre</a>";
		print "</td>\r";
		print "</tr>\r";	
		print "</table>";
		
		print "<p>&nbsp;</p>";
	} // end master file empty
	
	// ---------------------------------------------
	//		ADDITION ZONE
	// 		only if master = target ??
	// ---------------------------------------------
	if ($master_lang == $target_lang) {
		print "<table border=\"0\" cellspacing=\"1\" cellpadding=\"2\" bgcolor=\"$table_color\" width=\"95%\">\r";
		
		print "<tr class='verdana3' style=\"background-color: $couleur_foncee; color:white; padding: 3px; font-weight:bold;\">\r";
		print "<td style=\"text-align:right;\">"._T('adminlangue:ajouter')."</td>";
		print "<td>&nbsp;</td>";
		print "</tr>";
		
		print "<tr class='verdana2' style=\"background-color: $couleur_foncee; color:white; padding: 3px; font-weight:bold;\">\r";
		print "<td style=\"text-align:right;\">"._T('adminlangue:texte_aide_saisie_variable')."</td>";
		print "<td>"._T('adminlangue:texte_aide_saisie_texte')."</td>";
		print "</tr>";
		
		$switch_color= false;
		for ($i = 0; $i < $addition_volume; $i++) {
			$cell_color = ($switch_color) ? $normal_color : $altern_color;
			$style_area = "font-family:arial;font-size:12px; background:$cell_color";
	
			print "<tr bgcolor=\"$cell_color\">\r";
			
					
			print "<td valign=\"top\">";
					print "<input type=\"text\" name=\"new_value[$i]\" value=\"\" size=\"40\"
					style=\"font-size:10px; background:$cell_color\">";
			print "</td>";	
			
			print "<td>\r";
					print "<textarea name=\"new_trad[$i]\" cols=\"45\" rows=\"1\" wrap=\"soft\"
					style=\"$style_area\"></textarea>";
			print "</td>\r";
			$switch_color = !$switch_color;
		}
		
		print "</tr>";
		
		print "<tr style=\"background-color: $couleur_foncee;text-align=\"right\">\r";
		print "<td>&nbsp;</td>";
		print "<td align='right' valign='bottom' >";
		
		print  "<input id='save_lang_file' type=\"submit\" name=\"Submit\" value=\""._T('adminlangue:texte_bt_validation')."\">";	
		//print  "<a href=\"javascript:window.close();\" class=\"boutonBOform\">Fermer cette fen&ecirc;tre</a>";
		print "</td>\r";
		print "</tr>\r";
		print "</table>\r";
	}
	
		
	print "</form>\r";
}
else {
	//print "ici, texte d'explication compl&eacute;mentaire possible...<br>";
} // end if module & target_lang
fin_page();

// ===============================================================
//		FUNCTION ZONE	
// ===============================================================

// ---------------------------------------------
//	Write File, line by line
// ---------------------------------------------
function wf($myFile, $any_string, $end_line="\n") {
	global $display_debug_full;
	
	if ($display_debug_full) {print "-> write string : $any_string<br>";}
	
	if (substr($any_string, -2) != "?>") {$any_string = $any_string . $end_line;} // append end_line except ending php file
	if ($myFile != "") {fputs($myFile, "$any_string");}
	else {print "!! Error - File to write is undefined<br>\r";}
	
}

// ---------------------------------------------
//		filter_value_entry
// ---------------------------------------------
function filter_value_entry($any_string) {
	$rstr =  "";
	$rstr = trim($any_string);
	$rstr = ereg_replace('<|>|:', '', $rstr);
	$rstr = ereg_replace(' +', ' ', $rstr);
	$rstr = ereg_replace(' |-', '_', $rstr);
	return $rstr;
}


// ---------------------------------------------
//		filter_trad_entry
// ---------------------------------------------
function filter_trad_entry($any_string) {
	$rstr =  '';
	$any_string = stripslashes($any_string);
	$rstr = ereg_replace("'", "\\'", $any_string); // slash only single quote
	$rstr = htmlentities($rstr);
	$rstr = str_replace('&quot;', '"', $rstr);
	$rstr = str_replace('&amp;', '&', $rstr);
	$rstr = str_replace('&lt;', '<', $rstr);
	$rstr = str_replace('&gt;', '>', $rstr);
	$rstr = eregi_replace('<b>', '<strong>', $rstr);
	$rstr = eregi_replace('</b>', '</strong>', $rstr);
	$rstr = eregi_replace('<i>', '<em>', $rstr);
	$rstr = eregi_replace('</i>', '</em>', $rstr);
	
	return $rstr;
}

// ---------------------------------------------
//		light trad // obso now
// ---------------------------------------------
function _LT($text, $args = '') {
	return traduire_chaine($text, $args);
}


// ---------------------------------------------
//	get time in microseconds, and format in seconds
// ---------------------------------------------
function getmicrotime(){
	// get micro time and format in second
	list($sec, $usec) = explode(" ",microtime());
	return ((float)$usec + (float)$sec);
}

// ---------------------------------------------
//	calc duration from 2 timestamps
// ---------------------------------------------
function exetime($t_start,$t_end) {
	// evaluate exe time in second
	$e_time = $t_end - $t_start;
	$e_time = sprintf("%01.4f",$e_time);
	return $e_time;
}

// ---------------------------------------------
//	combine both previous to deliver a formated result
// ---------------------------------------------
function display_exe_time ($time_start, $comment) {
	$time_end = getmicrotime();
	$duration = exetime($time_start,$time_end);
	print "$comment : $duration sec</i><br><br>";
}

//display_exe_time ($time_start, 'after includes<br>');
?>
