This is my second wordpress style. Inspiration taken from Michael Heilemann's 'Scandanavia' (http://binarybonsai.com/) (the menu CSS was cribbed from there and modified) and from Kevin Husted's Swizcore site (http://swizcore.com/SS)
(Oh and Panic's site too)

If you want to change the color of the headers they're in the 'images' directory. Shouldn't be a big deal for someone with a passing familiarity with Photoshop. 

28 Nov 2004

Gary Rogers (gary.rogers@dmin.net) http://dmin.net/wordpress
