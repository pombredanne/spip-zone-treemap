VerySimple
    Wordpress Theme
    by Gary Rogers (gary.rogers@dmin.net)
    
Shouldn't have any issues with PNGs in this theme, there are no images. Inspired by Apple.com's layout. This is a very basic theme that hopes to achieve more with less. It could look really splashy if you threw in white backgrounded images in your posts.