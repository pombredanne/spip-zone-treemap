<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2005                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/


include ("inc.php3");
include_ecrire ("inc_rubriques.php3");
include_ecrire ("inc_logos.php3");
include_ecrire ("inc_mots.php3");
include_ecrire ("inc_documents.php3");
include_ecrire ("inc_abstract_sql.php3");

//
// Gerer les modifications...
//

$id_parent = intval($id_parent);
$id_rubrique = intval(33);
$flag_mots = lire_meta("articles_mots");
$flag_editable = ($connect_statut == '0minirezo' AND (acces_rubrique($id_parent) OR acces_rubrique($id_rubrique))); // id_parent necessaire en cas de creation de sous-rubrique


if (!$titre) {
	if ($modifier_rubrique == "oui") calculer_rubriques();
}
else {
	// creation, le cas echeant
	if ($new == 'oui' AND $flag_editable AND !$id_rubrique) {
		$id_rubrique = spip_abstract_insert("spip_rubriques", 
			"(titre, id_parent)",
			"('"._T('item_nouvelle_rubrique')."', '$id_parent')");

		// Modifier le lien de base pour qu'il prenne en compte le nouvel id
		unset($_POST['id_parent']);
		$_POST['id_rubrique'] = $id_rubrique;
		$clean_link = new Link();
	}

	// si c'est une rubrique-secteur contenant des breves, ne deplacer
	// que si $confirme_deplace == 'oui', et changer l'id_rubrique des
	// breves en question
	if ($GLOBALS['confirme_deplace'] == 'oui'
	AND $id_parent > 0) {
		list($id_secteur) = spip_fetch_array(spip_query(
			"SELECT id_secteur FROM spip_rubriques
			WHERE id_rubrique=$id_parent"));
		if ($id_secteur)
			spip_query("UPDATE spip_breves
				SET id_rubrique=$id_secteur
				WHERE id_rubrique=$id_rubrique");
	}

	if ($flag_editable) {

		if ($champs_extra) {
			include_ecrire("inc_extra.php3");
			$champs_extra = ", extra = '".addslashes(extra_recup_saisie("rubriques"))."'";
		} 
		spip_query("UPDATE spip_rubriques SET " .
(acces_rubrique($id_parent) ? "id_parent='$id_parent'," : "") . "
titre='" . addslashes($titre) ."',
descriptif='" . addslashes($descriptif) . "',
texte='" . addslashes($texte) . "'
$champs_extra
WHERE id_rubrique=$id_rubrique");
	}

	calculer_rubriques();
	calculer_langues_rubriques();

	// invalider et reindexer
	if ($invalider_caches) {
		include_ecrire ("inc_invalideur.php3");
		suivre_invalideur("id='id_rubrique/$id_rubrique'");
	}
	if (lire_meta('activer_moteur') == 'oui') {
		include_ecrire ("inc_index.php3");
		marquer_indexer('rubrique', $id_rubrique);
	}
 }

//
// Appliquer le changement de langue
//
if ($changer_lang AND $id_rubrique>0 AND lire_meta('multi_rubriques') == 'oui' AND (lire_meta('multi_secteurs') == 'non' OR $id_parent == 0) AND $flag_editable) {
	if ($changer_lang != "herit")
		spip_query("UPDATE spip_rubriques SET lang='".addslashes($changer_lang)."', langue_choisie='oui' WHERE id_rubrique=$id_rubrique");
	else {
		if ($id_parent == 0)
			$langue_parent = lire_meta('langue_site');
		else {
			$row = spip_fetch_array(spip_query("SELECT lang FROM spip_rubriques WHERE id_rubrique=$id_parent"));
			$langue_parent = $row['lang'];
		}
		spip_query("UPDATE spip_rubriques SET lang='".addslashes($langue_parent)."', langue_choisie='non' WHERE id_rubrique=$id_rubrique");
	}
	calculer_langues_rubriques();
}


///// debut de la page
debut_page($titre_page, "documents", "rubriques");


changer_typo('', 'rubrique'.$id_rubrique);


debut_gauche();


debut_droite();



if ($id_rubrique ==  0) $ze_logo = "racine-site-24.gif";
else if ($id_parent == 0) $ze_logo = "secteur-24.gif";
else $ze_logo = "rubrique-24.gif";






//echo "<div align='$spip_lang_left'>";


//////////  Vos articles en cours de redaction
/////////////////////////

echo "<P>";



//////////  Les articles publies
/////////////////////////

afficher_articles(_T('Article en preparation'),
	"WHERE statut='prepa' ORDER BY titre ASC", true);
afficher_articles(_T('Article propos�'),
	"WHERE statut='prop' ORDER BY titre ASC", true);
afficher_articles('article publi�',
	"WHERE statut='publie' ORDER BY titre ASC", true);
afficher_articles('article poubelle',
	"WHERE statut='poubelle' ORDER BY titre ASC", true);
afficher_articles('article refus�',
	"WHERE statut='refuse' ORDER BY titre ASC", true);



fin_page();

?>
