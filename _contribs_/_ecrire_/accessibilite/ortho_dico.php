<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2005                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/


include("inc_lab.php");

include_spip("ecrire.php");
include_spip("ortho.php");
include_spip("layer.php"); // Pour $browser_name

global $spip_display;

global $supp_mot, $supp_lang;
if (($mot = strval($supp_mot)) && ($lang = strval($supp_lang))) {
    supprimer_dico_ortho($mot, $lang); 
}

debut_html();
print "<h1>Suppression de mot du dictionnaire</h1>";
print "<p>attention, la supression se fait sans confirmation</p>";

$query = "SELECT * FROM spip_ortho_dico ORDER BY mot";
$result = spip_query($query);

while ($row = spip_fetch_array($result)) {
	$lang = $row["lang"];
	$mot = $row["mot"];
	print "Supprimer <a href=\"ortho_dico.php?supp_mot=$mot&supp_lang=$lang\">$mot $lang</a><br>";
}

print "<p>retour au <a href=\".\">sommaire</a></p>";

fin_html();
