<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2005                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/


include("inc_lab.php");

include_spip("ecrire.php");
include_spip("ortho.php");
include_spip("layer.php"); // Pour $browser_name

global $spip_display;
global $id_article;

// actuellemnt on est en franco-francais
if (!$lang_article) $lang_article = lire_meta('langue_site');

//print $id_article;
//print $mot_corr;
//print $lang_article;

//
// Fonctions d'affichage HTML
//

function panneau_ortho_oo($ortho_result) {
	global $id_suggest;
	global $id_article;

	$id_suggest = array();
	$i = 1;

	$mauvais = $ortho_result['mauvais'];
	$bons = $ortho_result['bons'];
	if (!count($mauvais) && !count($bons)) return;
	ksort($mauvais);


	// Mots mal orthographies :
	// liste des suggestions plus lien pour ajouter au dico
	foreach ($mauvais as $mot => $suggest) {
		$id = $id_suggest[$mot];
		$mot_html = afficher_ortho($mot);
		echo "<div class='suggest-inactif' id='suggest$id'>";
		echo "<span class='ortho'>Mot incorrect : $mot_html</span>\n";
		echo "<div class='detail'>\n";
		if (is_array($suggest) && count($suggest)) {
			echo "<ul>\n";
			$i = 0;
			foreach ($suggest as $sug) {
				if (++$i > 12) {
					echo "<li><i>(...)</i></li>\n";
					break;
				}
				echo "<li><a href=articles_ortho.php?id_article=$id_article&mot_corr=$mot_html&mot_modif=".typo(afficher_ortho($sug)).">".typo(afficher_ortho($sug))."</li>\n";
			}
			echo "</ul>\n";
		}
		else {
			echo "<i>"._T('ortho_aucune_suggestion')."</i>";
		}
		echo "<br />";
//		$link = new Link;
//		$link->delVar('supp_ortho');
//		$link->addVar('ajout_ortho', $mot);
//		icone_horizontale(_T('ortho_ajouter_ce_mot'), $link->getUrl(), "ortho-24.gif", "creer.gif");
		echo "<a href=\"articles_ortho?id_article=$id_article&ajout_ortho=$mot\">Ajouter $mot au dictionnaire</a>";
		echo "</div>\n";
		echo "</div>\n\n";
	}
	print "<p><form action=\"articles_ortho.php?id_article=$id_article&mot_corr=$mot_html\" method=\"get\">Remplacer par :  
		<input name=\"mot_modif\" type=\"text\"> 
		<input type=\"hidden\" name=\"id_article\" value=\"$id_article\"> 
		<input type=\"hidden\" name=\"mot_corr\" value=\"$mot_html\">
		<input value=\"envoyer\" type=\"submit\"></form></p>";
	print "<p><a href=\"articles_edit.php3?id_article=$id_article\">Modifier l'article </a></p>";
}



$ortho = preparer_ortho($mot_corr, $lang_article);
$result_ortho = corriger_ortho($ortho, $lang_article);

if (is_array($result_ortho)) {
	$mots = $result_ortho['mauvais'];
	if ($erreur = $result_ortho['erreur']) {
		echo "<b>"._T('ortho_trop_de_fautes').aide('corrortho')."</b><p>\n";
		echo "<b>"._T('ortho_trop_de_fautes2')."</b><p>";
	}
	else {
		echo "<b>On peut soit s�lectionner un mot propos�, soit en proposer un nouveau, soit ajouter l'original au dico, ou encore aller r��diter l'article </b><p>\n";
	}

	panneau_ortho_oo($result_ortho);
}
else {
	$erreur = $result_ortho;
	echo "<b>"._T('ortho_dico_absent').aide('corrortho')." (";
	echo traduire_nom_langue($lang_article);
	echo "). ";
	echo _T('ortho_verif_impossible')."</b>";
}
