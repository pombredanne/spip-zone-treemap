<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2005                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/


include("inc_lab.php");

include_spip("ecrire.php");
include_spip("ortho.php");
include_spip("layer.php"); // Pour $browser_name

//charset_texte('utf-8');
global $spip_display;

// Modifier l'article avec un mot � corriger
global $mot_corr, $mot_modif;
if (($mot_c = strval($mot_corr)) &&  ($mot_m = strval($mot_modif))) {
    modif_article_ortho($id_article,$mot_c,$mot_modif);
}

//
// Lire l'article
//

$query = "SELECT * FROM spip_articles WHERE id_article='$id_article'";
$result = spip_query($query);

if ($row = spip_fetch_array($result)) {
	$id_article = $row["id_article"];
	$surtitre = $row["surtitre"];
	$titre = $row["titre"];
	$soustitre = $row["soustitre"];
	$id_rubrique = $row["id_rubrique"];
	$descriptif = $row["descriptif"];
	$nom_site = $row["nom_site"];
	$url_site = $row["url_site"];
	$chapo = $row["chapo"];
	$texte = $row["texte"];
	$ps = $row["ps"];
	$date = $row["date"];
	$statut_article = $row["statut"];
	$maj = $row["maj"];
	$date_redac = $row["date_redac"];
	$visites = $row["visites"];
	$referers = $row["referers"];
	$extra = $row["extra"];
	$id_trad = $row["id_trad"];
	$lang_article = $row["lang"];
}
if (!$lang_article) $lang_article = lire_meta('langue_site');
changer_typo($lang_article); # pour l'affichage du texte

// pour l'affichage du virtuel
unset($virtuel);
if (substr($chapo, 0, 1) == '=') {
	$virtuel = substr($chapo, 1);
	$chapo = "";
}

$champs = array('surtitre', 'titre', 'soustitre', 'descriptif', 'chapo', 'texte', 'ps');
$echap = array();
$ortho = "";

//
// Affichage HTML
//

// Gros hack IE pour le "position: fixed"
$code_ie = "<!--[if IE]>
<style type=\"text/css\" media=\"screen\">
	body {
		height: 100%; margin: 0px; padding: 0px;
		overflow: hidden;
	}
	.ortho-content {
		position: absolute; $spip_lang_left: 0px;
		height: 100%; margin: 0px; padding: 0px;
		width: 72%;
		overflow-y: auto;
	}
	#ortho-fixed {
		position: absolute; $spip_lang_right: 0px; width: 25%;
		height: 100%; margin: 0px; padding: 0px;
		overflow: hidden;
	}
	.ortho-padding {
		padding: 12px;
	}
</style>
<script type=\"text/javascript\">
	onload = function() { document.getElementById('ortho-content').focus(); }
</script>
<![endif]-->";

debut_html(_T('ortho_orthographe').' &laquo;'.$titre.'&raquo;', $code_ie);


// Ajouts et suppressions de mots par l'utilisateur
gerer_dico_ortho($lang_article);

//
// Panneau de droite
//
echo "<div id='ortho-fixed'>";
echo "<div class='ortho-padding serif'>";

debut_cadre_enfonce();

foreach ($champs as $champ) {
	$ortho .= $$champ." ";
}
$ortho = preparer_ortho($ortho, $lang_article);
$result_ortho = corriger_ortho($ortho, $lang_article);
if (is_array($result_ortho)) {
	$mots = $result_ortho['mauvais'];
	if ($erreur = $result_ortho['erreur']) {
		echo "<b>"._T('ortho_trop_de_fautes').aide('corrortho')."</b><p>\n";
		echo "<b>"._T('ortho_trop_de_fautes2')."</b><p>";
	}
	else {
		if ($spip_display!=4) {
		    echo "<b>"._T('ortho_mode_demploi').aide('corrortho')."</b><p>\n";
		} else { 
		    echo "Les mots errones sont repris par des liens, attention, les liens ext�rieurs sont tjs actifs";
		} 
	}

	if ($spip_display!=4) {panneau_ortho($result_ortho);}
}
else {
	$erreur = $result_ortho;
	echo "<b>"._T('ortho_dico_absent').aide('corrortho')." (";
	echo traduire_nom_langue($lang_article);
	echo "). ";
	echo _T('ortho_verif_impossible')."</b>";
}

fin_cadre_enfonce();

echo "</div>";
echo "</div>";


//
// Colonne de gauche : textes de l'article
//
echo "<div class='ortho-content' id='ortho-content'>";
echo "<div class='ortho-padding serif'>";

// Traitement des champs : soulignement des mots mal orthographies
foreach ($champs as $champ) {
	switch ($champ) {
	case 'texte':
	case 'chapo':
	case 'descriptif':
	case 'ps':
		// Mettre de cote les <code>, <cadre>, etc.
		list($$champ, $echap) = echappe_html($$champ, "ORTHO");
		$$champ = propre($$champ);
		break;
	default:
		$echap = "";
		$$champ = typo($$champ);
		break;
	}
	// On passe en UTF-8 juste pour la correction
	$$champ = preparer_ortho($$champ, $lang_article);
	if (is_array($result_ortho))
		$$champ = souligner_ortho($$champ, $lang_article, $result_ortho);
	// Et on repasse dans le charset original pour remettre les echappements
	$$champ = afficher_ortho($$champ);
	if ($echap)
		$$champ = echappe_retour($$champ, $echap, "ORTHO");
}
// Traitement identique pour les notes de bas de page
if ($les_notes) {
	$les_notes = preparer_ortho($les_notes, $lang_article);
	if (is_array($result_ortho))
		$les_notes = souligner_ortho($les_notes, $lang_article, $result_ortho);
	$les_notes = afficher_ortho($les_notes);
}

debut_cadre_relief();

if ($surtitre) {
	echo "<span $dir_lang><font face='arial,helvetica' size='3'><b>";
	echo $surtitre;
	echo "</b></font></span>\n";
}
gros_titre($titre);

if ($soustitre) {
	echo "<span $dir_lang><font face='arial,helvetica' size='3'><b>";
	echo $soustitre;
	echo "</b></font></span>\n";
}

if ($descriptif OR $url_site OR $nom_site) {
	echo "<p><div align='$spip_lang_left' style='padding: 5px; border: 1px dashed #aaaaaa; background-color: #e4e4e4;' $dir_lang>";
	echo "<font size='2' face='Verdana,Arial,Sans,sans-serif'>";
	$texte_case = ($descriptif) ? "{{"._T('info_descriptif')."}} $descriptif\n\n" : '';
	$texte_case .= ($nom_site.$url_site) ? "{{"._T('info_urlref')."}} [".$nom_site."->".$url_site."]" : '';
	echo $descriptif;
	echo "</font>";
	echo "</div>";
}


// Corps de l'article

echo "\n\n<div align='justify'>";

if ($virtuel) {
	debut_boite_info();
	echo _T('info_renvoi_article')." ".propre("<center>[->$virtuel]</center>");
	fin_boite_info();
}
else {
	echo "<div $dir_lang><b>";
	echo $chapo;
	echo "</b></div>\n\n";

	echo "<div $dir_lang>";
	echo $texte;
	echo "</div>";

	if ($ps) {
		echo debut_cadre_enfonce();
		echo "<div $dir_lang><font size='2' face='Verdana,Arial,Sans,sans-serif'>";
		echo "<b>"._T('info_ps')."</b> ";
		echo $ps;
		echo "</font></div>";
		echo fin_cadre_enfonce();
	}

	if ($les_notes) {
		echo debut_cadre_relief();
		echo "<div $dir_lang><font size='2'>";
		echo "<b>"._T('info_notes')."&nbsp;:</b> ".$les_notes;
		echo "</font></div>";
		echo fin_cadre_relief();
	}

	if ($champs_extra AND $extra) {
		include_spip("extra.php");
		extra_affichage($extra, "articles");
	}
}


echo "</div>";


fin_cadre_relief();

html_background();
echo "</div>";
echo "</div>";

fin_html();

?>
