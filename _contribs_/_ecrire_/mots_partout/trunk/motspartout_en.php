<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
									   'titre_page' => 'Keywords assignation',
									   'ajouter' => 'add',
									   'enlever' => 'remove',
									   'voir' => 'see',
									   'limite' => 'limitation',
									   'aucune' => 'none',
									   'action' => 'Action',
									   'stricte' => 'strict',
									   'select' => 'select',
									   'sans' => 'exclude',
									   'pas_de_documents' => 'There is no such object',
									   'choses' => 'Add keywords on:',
									   'dejamotgroupe' => 'There is already a word from this group (@groupe@) on the object @chose@.',
									   'ATTENTION' => 'WARNING'
									   );

?>
