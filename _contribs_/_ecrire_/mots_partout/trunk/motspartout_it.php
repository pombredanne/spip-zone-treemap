<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
									   'titre_page' => 'Aggiungere parole chiave',
									   'ajouter' => 'aggiungere',
									   'enlever' => 'cancella',
									   'voir' => 'vedere',
									   'limite' => 'limitazione',
									   'aucune' => 'nessuna',
									   'action' => 'Azione',
									   'stricte' => 'rigorosa',
									   'select' => 'selezione',
									   'sans' => 'escluda',
									   'pas_de_documents' => 'Non ce niente con questi caratteristiche',
									   'choses' => 'Aggiungere parole chiave sopra:',
									   'dejamotgroupe' => 'Ce gia un parole chiave de questo gruppo (@groupe@) sul oggetto @chose@.',
									   'ATTENTION' => 'ATTENZIONE'
									   );

?>
