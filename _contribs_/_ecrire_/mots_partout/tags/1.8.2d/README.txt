Auteur: Pierre Andrews (aka Mortimer) mortimer.pa@free.fr
Licence: GPL

Description:

[fr] Page de l'interface priv�e pour pouvoir mettre des mots clefs sur
n'importe quelle "objet" spip. Pour plus d'info, voir la contrib sur
spip-contrib.net. L'interface etait destin� � l'origine � l'ajout de
mots clefs sur les documents et s'inspire d'iPhoto. 


[en] Interface for the private part of a SPIP web site to put tags on
any "object". The interface was primarilly intended for using keywords
on documents and is inspired from iPhoto.

Listes des fichiers:

[fr]
README.txt vous y etes,
mots_partout.php nouvelle page pour l'interface,
motspartout_en.php fichier de localisation en anglais,
motspartout_fr.php fichier de localisation en francais,
motspartout_it.php fichier de localisation en italien,
_REGLES_DE_COMMIT.txt r�gles pour faire �voluer cette contrib,
TODO.txt ce qu'il reste � faire,
BUG.txt les bugs connus.

[en]
README.txt you are here <-,
mots_partout.php the main interface,
motspartout_en.php localisation in english,
motspartout_fr.php localisation in french,
motspartout_it.php localisation en italian,
_REGLES_DE_COMMIT.txt rules of contribution on this project,
TODO.txt what remains to do,
BUG.txt the known bugs.

Page de la contrib:
http://spip-contrib.net/ecrire/articles.php3?id_article=905


