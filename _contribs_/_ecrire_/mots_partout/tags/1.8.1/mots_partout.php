<?php 


//	  mots_partout.php
//    Fichier cr�� pour SPIP avec un bout de code emprunt� � celui ci.
//    Distribu� sans garantie sous licence GPL.
//
//    Copyright (C) 2003  Pierre ANDREWS
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

include ("inc.php3");
include_ecrire ("inc_version.php3");
include_ecrire ("inc_documents.php3");
include_ecrire ("inc_abstract_sql.php3");
include_ecrire ("inc_objet.php3");


/***********************************************************************
* D�finition des choses sur lesquels on peut vouloir mettre des mots clefs
***********************************************************************/


$choses_possibles['articles'] = array(
									  'titre_chose' => 'public:articles',
										  'id_chose' => 'id_article',
									  'table_principale' => 'spip_articles',
									  'table_auth' => 'spip_auteurs_articles',
									  'tables_limite' => array(
															   'articles' => array(
																				   'table' => 'spip_articles',
																				   'nom_id' => 'id_article'),
															   'rubriques' => array(
																					'table' => 'spip_articles',
																					'nom_id' =>  'id_rubrique'),
															   'documents' => array(
																					'table' => 'spip_documents_articles',
																					'nom_id' =>  'id_document'),
															   'auteurs' => array(
																				  'table' => 'spip_auteurs_articles',
																				  'nom_id' => 'id_auteur')
															   )
									  );


$choses_possibles['documents'] = array(
									   'titre_chose' => 'info_documents',
												  'id_chose' => 'id_document',
									   'table_principale' => 'spip_documents',
									   'tables_limite' => array(
																'articles' => array(
																					'table' => 'spip_documents_articles',
																					'nom_id' => 'id_article'),
																'rubriques' => array(
																					 'table' => 'spip_documents_rubriques',
																					 'nom_id' =>  'id_rubrique'),
																'documents' => array(
																					 'table' => 'spip_documents',
																					 'nom_id' =>  'id_document')
																)
									   );


/***********************************************************************
 * function
 ***********************************************************************/
  
function verifier_admin() {
  global $connect_statut, $connect_toutes_rubriques;
  return (($connect_statut == '0minirezo') AND $connect_toutes_rubriques);
}

function verifier_auteur($table, $id_chose, $id) {
  global $connect_id_auteur;
  $select = array('id_auteur');
  
  $from =  array($table);
  
  $where = array("id_auteur = $connect_id_auteur", "$id_chose = $id");
  
  $result = spip_abstract_select($select,$from,$where);
  
  if (spip_abstract_count($result) > 0) {
	spip_abstract_free($result);
	return true;
  }
  spip_abstract_free($result);
  return false;
}


function calcul_numeros($array, $search, $total) {
  if(is_array($array))
	$tt = count(array_keys($array,$search));
  else
	return 0;

  if($tt == 0) return 0;
  if($tt < $total) return 1;
  return 2;
}

function md_afficher_liste($largeurs, $table, $styles = '') {
	global $couleur_claire;
	global $browser_name;
	global $spip_display;
	global $spip_lang_left;

	if (!is_array($table)) return;
	reset($table);
	echo "\n";
	if ($spip_display != 4) {
		while (list(,$t) = each($table)) {
			if (eregi("msie", $browser_name)) $msover = " onMouseOver=\"changeclass(this,'tr_liste_over');\" onMouseOut=\"changeclass(this,'tr_liste');\"";
			echo "<tr class='tr_liste'$msover>";
			reset($largeurs);
			if ($styles) reset($styles);
			while (list($texte, $sel) = each($t)) {
				$style = $largeur = "";
				list(, $largeur) = each($largeurs);
				if ($styles) list(,$style) = each($styles);
				if (!trim($texte)) $texte .= "&nbsp;";
				echo "<td";
				if ($largeur) echo " width=\"$largeur\"";
				if ($style)  echo ' class="'.$style[$sel].'"';
				echo ">$texte</td>";
			}
			echo "</tr>\n";
		}
	} else {
		echo "<ul style='text-align: $spip_lang_left;'>";
		while (list(, $t) = each($table)) {
			echo "<li>";
			reset($largeurs);
			if ($styles) reset($styles);
			while (list(, $texte) = each($t)) {
				$style = $largeur = "";
				list(, $largeur) = each($largeurs);
				
				if (!$largeur) {
					echo $texte." ";
				}
			}
			echo "</li>\n";
		}
		echo "</ul>";
	}
	echo "\n";
}


function find_tables($nom, $tables) {
  $toret = array();
  foreach($tables as $t => $dec) {
	if(ereg($nom,$t)) {
	  $toret[] = $t;
	}
  }
  return $toret;
}

function calcul_in($mots) {
  for($i=0; $i < count($mots); $i++) {
	if($i > 0) $to_ret .= ',';
	$to_ret .= $mots[$i];
  }

  return $to_ret;
}

//======================================================================

function afficher_horizontal_document_assoc($id_document,$with_check) {
	global $connect_id_auteur, $connect_statut;
	global $couleur_foncee, $couleur_claire, $couleur_lien_off, $couleur_lien;
	global $clean_link;

	$document = fetch_document($id_document);

	$id_vignette = $document->get('id_vignette');
	$id_type = $document->get('id_type');
	$titre = $document->get('titre');
	$descriptif = $document->get('descriptif');
	$fichier = generer_url_document($id_document);
	$largeur = $document->get('largeur');
	$hauteur = $document->get('hauteur');
	$taille = $document->get('taille');
	$date = $document->get('date');
	$mode = $document->get('mode');

	if (!$titre) {
		$titre_aff = ereg_replace("^[^\/]*\/[^\/]*\/","",$fichier);
	} else {
		$titre_aff = $titre;
	}

	$select =  array();
	$select[] = 'extension';
	$select[] = 'inclus';
	$select[] = 'titre';

	$from = array();
	$from[] = 'spip_types_documents';
	
	$where = array();
	$where[] = "id_type=$id_type";

	if ($type = @spip_abstract_fetsel($select,$from,$where))	{
		$type_extension = $type['extension'];
		$type_inclus = $type['inclus'];
		$type_titre = $type['titre'];
	}

	debut_cadre_enfonce("doc-24.gif");
	echo "<div style='padding: 2px; background-color: #aaaaaa; text-align: left; color: black;'>";

	if($with_check) {
	  echo "<input type='checkbox' name='id_choses[]' value='$id_document' />";
	}
	echo "<font size=1 face='arial,helvetica,sans-serif'>"._T('info_document')." - </font> <b><font size=2>".typo($titre_aff)."</font></b>";
	echo "</div>\n";


	//
	// Recuperer la vignette
	//
	if ($id_vignette) $vignette = fetch_document($id_vignette);
	if ($vignette) {
		$fichier_vignette = generer_url_document($id_vignette);
		$largeur_vignette = $vignette->get('largeur');
		$hauteur_vignette = $vignette->get('hauteur');
		$taille_vignette = $vignette->get('taille');
	}

	echo "<p></p><div style='border: 1px dashed #666666; padding: 5px; background-color: #f0f0f0;'>";
	if ($fichier_vignette) {
		// Afficher la vignette
		echo "<div align='left'>\n";
		echo "<div align='center'>";
		$block = "doc_vignette $id_document";
		echo texte_vignette_document($largeur_vignette, $hauteur_vignette, $fichier_vignette, "$fichier");
		echo "</div>";

		echo "</div>\n";
	}
	else {
		// Pas de vignette : afficher la vignette par defaut
		echo "<div align='center'>\n";
		$block = "doc_vignette $id_document";
		list($icone, $largeur_icone, $hauteur_icone) = vignette_par_defaut($type_extension);
		if ($icone) {
			echo "<a href='$fichier'><img src='$icone' border=0 width='$largeur_icone' align='top' height='$hauteur_icone'></a>\n";
		}
		echo "</div>\n";
	}
	echo "</div>";
/*
	$block = "document $id_document ";

	//
	// Boite d'edition du document
	//
	echo debut_block_visible($block);

	echo "<p></p><div style='border: 1px solid #666666; padding: 0px; background-color: #f0f0f0;'>";

	echo "<div style='padding: 5px;'>";

	if ($type_titre)
		echo "$type_titre";
	else
		echo "Document ".majuscules($type_extension);
	echo " : <a href='$fichier'>".taille_en_octets($taille)."</a>";

	echo "</div>";
	echo "</div>";
	echo fin_block();
*/
	fin_cadre_enfonce();
}

function afficher_pagination($count,$actu) {
  $i = 0;
  while($i++ < $actu)
	echo "<button name='page' value='$i'>$i</button>";
   echo $i;
  while($i++ < $count/30)
	echo "<button name='page' value='$i'>$i</button>";
}

function afficher_liste_documents($choses) {
  echo "<table>";						  
  $i=0;
  foreach($choses as $id_chose) {
	$i++;
	echo "<td width='33%'>";
	afficher_horizontal_document_assoc($id_chose,true);
	echo "</td>";
	if($i > 30) {
	  echo "</tr>";
	  afficher_pagination(count($choses),$HTTP_GET_VARS['page']);
	  break;
	} else
	if ($i==3)
	  {
		echo "</tr><TR>";
		$i=0;
	  }
  }
  echo '</table>';
}

//======================================================================

function afficher_liste_articles($choses) {
  echo "<div style='height: 12px;'></div>";
  echo "<div class='liste'>";
  bandeau_titre_boite2($titre_table, "article-24.gif");
  
  echo afficher_liste_debut_tableau();
  
  $from = array('spip_articles as articles');
  $select= array();
  $select[] = 'id_article';
  $select[] = 'titre';
  $select[] = 'id_rubrique';
  $select[] = 'date';
  $select[] = 'statut';
  $select[] = 'lang';
  $select[] = 'descriptif';
  $where = array('articles.id_article IN ('.calcul_in($choses).')');
  
  $result = spip_abstract_select($select,$from,$where);
  $i = 0;
  while ($row = spip_abstract_fetch($result)) {
	$i++;
	$vals = '';
	
	$id_article = $row['id_article'];
	$tous_id[] = $id_article;
	$titre = $row['titre'];
	$id_rubrique = $row['id_rubrique'];
	$date = $row['date'];
	$statut = $row['statut'];
	if ($lang = $row['lang']) changer_typo($lang);
	$descriptif = $row['descriptif'];
	if ($descriptif) $descriptif = ' title="'.attribut_html(typo($descriptif)).'"';
	
	$vals[] = "<input type='checkbox' name='id_choses[]' value='$id_article' id='id_chose$i'/>";
	
	// Le titre (et la langue)
	$s = "<div>";
	
	$s .= "<a href=\"articles.php3?id_article=$id_article\"$descriptif$dir_lang style=\"display:block;\">";
	
	if ($spip_display != 1 AND $spip_display != 4 AND lire_meta('image_process') != "non") {
	  include_ecrire("inc_logos.php3");
	  $logo = decrire_logo("arton$id_article");
	  if ($logo) {
		$fichier = $logo[0];
		$taille = $logo[1];
		$taille_x = $logo[3];
		$taille_y = $logo[4];
		$taille = image_ratio($taille_x, $taille_y, 26, 20);
		$w = $taille[0];
		$h = $taille[1];
		$fid = $logo[2];
		$hash = calculer_action_auteur ("reduire $w $h");
		
		$s.= "<div style='float: $spip_lang_right; margin-top: -2px; margin-bottom: -2px;'>
<img src='../spip_image_reduite.php3?img="._DIR_IMG."$fichier&taille_x=$w&taille_y=$h&hash=$hash&hash_id_auteur=$connect_id_auteur' alt='$fichier' width='$w' height='$h' border='0'></div>";
		
	  }
	}
	
	$s .= typo($titre);
	if ($afficher_langue AND $lang != $langue_defaut)
	  $s .= " <font size='1' color='#666666'$dir_lang>(".traduire_nom_langue($lang).")</font>";
	$s .= "</a>";
	$s .= "</div>";
	
	$vals[] = $s;
	
	// La date
	$s = affdate_jourcourt($date);
	$vals[] = $s;
	
	// Le numero (moche)
	if ($options == "avancees") {
	  $vals[] = "<b>"._T('info_numero_abbreviation')."$id_article</b>";
	}
	
	
	$table[] = $vals;
  }
  spip_free_result($result);
  
  if ($options == "avancees") { // Afficher le numero (JMB)
	if ($afficher_auteurs) {
	  $largeurs = array(11, '', 80, 100, 35);
	  $styles = array('', 'arial2', 'arial1', 'arial1', 'arial1');
	} else {
	  $largeurs = array(11, '', 100, 35);
	  $styles = array('', 'arial2', 'arial1', 'arial1');
	}
  } else {
	if ($afficher_auteurs) {
	  $largeurs = array(11, '', 100, 100);
	  $styles = array('', 'arial2', 'arial1', 'arial1');
	} else {
	  $largeurs = array(11, '', 100);
	  $styles = array('', 'arial2', 'arial1');
	}
  }
  afficher_liste($largeurs, $table, $styles);
  
  echo afficher_liste_fin_tableau();
}

//======================================================================

function afficher_liste_defaut($choses) {
  echo '<table>';
  $i = 0;
  foreach($choses as $id_chose) {
	$i++;
	echo "<td><tr><input type='checkbox' name='id_choses[]' value='$id_chose' id='id_chose$i'/></tr><tr> <label for='id_chose$i'>$id_chose</label></tr></td>";
  }
  echo '</table>';
}

/***********************************************************************
 * r�cuperation de la chose sur laquelle on travaille
 ***********************************************************************/

$nom_chose = $HTTP_GET_VARS['nom_chose'];
if(!isset($choses_possibles[$nom_chose])) {
  list($nom_chose,) = each($choses_possibles);
  reset($choses_possibles);
}
$id_chose = $choses_possibles[$nom_chose]['id_chose'];
$table_principale = $choses_possibles[$nom_chose]['table_principale'];
$table_auth = $choses_possibles[$nom_chose]['table_auth'];
$tables_limite = $choses_possibles[$nom_chose]['tables_limite'];

/***********************************************************************
 * action
 ***********************************************************************/
$mots = $HTTP_GET_VARS['id_mots'];
$sans_mots = $HTTP_GET_VARS['sans_mots'];
$choses = $HTTP_GET_VARS['id_choses'];

if($HTTP_GET_VARS['bouton'] == 'ajouter' && count($mots) && count($choses)) {
  foreach($mots as $m) {	
	$from = array('spip_mots');
	$select = array('id_groupe');
	$where = array("id_mot = $m");
	$res = spip_abstract_select($select,$from,$where);
	$unseul = false;
	$id_groupe = 0;
	$titre_groupe = '';
	if($row = spip_abstract_fetch($res)) {
	  spip_abstract_free($res);
	  $from = array('spip_groupes_mots');
	  $select = array('unseul','titre');
	  $id_groupe = $row['id_groupe'];
	  $where = array("id_groupe = $id_groupe");
	  $res = spip_abstract_select($select,$from,$where);
	  if($row = spip_abstract_fetch($res)) {
		$unseul = ($row['unseul'] == 'oui');
		$titre_groupe = $row['titre'];
	  }
	}
	spip_abstract_free($res);
	foreach($choses as $d) {
	  if($unseul) {
		$from = array("spip_mots_$nom_chose",'spip_mots');
		$select = array("count('id_mot') as cnt");
		$where = array("id_groupe = $id_groupe","spip_mots_$nom_chose.id_mot = spip_mots.id_mot","$id_chose = $d");
		$group = $id_chose;
		$res = spip_abstract_select($select,$from,$where,$group);
		if($row = spip_abstract_fetch($res)) {	
		  if($row['cnt'] > 0) {
			$warnings[] = array(_T('motspartout:dejamotgroupe',array('groupe' => $titre_groupe, 'chose' => $d)));
			continue; 
		  }
		}
		spip_abstract_free($res);
	  }
	  spip_abstract_insert("spip_mots_$nom_chose","(id_mot,$id_chose)","($m,$d)");
	}
  }
} else if ($HTTP_GET_VARS['bouton'] == 'enlever' && count($mots) && count($choses)) {
    foreach($mots as $m) {
	  foreach($choses as $d) {
		spip_query("DELETE FROM spip_mots_$nom_chose WHERE id_mot=$m AND $id_chose=$d");
	  }
	}
}

/**********************************************************************
* recherche des choses.
***********************************************************************/

if(count($choses) <= 0) {
  $select = array();
  $select[] = "DISTINCT main.$id_chose";
  
  $from = array();
  $where = array();
  $group = '';
  $order = '';
  
  if(isset($limit) && $limit != 'rien') {
	$table_lim = $tables_limite[$limit]['table'];
	$nom_id_lim = $tables_limite[$limit]['nom_id'];
	
	$from[] = "$table_lim as main";
	$where[] = "main.$nom_id_lim IN ($id_limit)"; 
	if(count($mots) > 0) {
	  $from[] = "spip_mots_$nom_chose as table_temp";
	  $where[] = "table_temp.id_mot IN (".calcul_in($mots).')';
	  $where[] = "table_temp.$id_chose = main.$id_chose";
	  if($HTTP_GET_VARS['strict']) {
		$select[] = 'count(id_mot) as tot';
		$group = "main.$id_chose";
		$order = 'tot DESC';
	  }
	}
  } else if(count($mots) > 0) {
	  $from[] = "spip_mots_$nom_chose as main";
	  $where[] = "main.id_mot IN (".calcul_in($mots).')';
	  if($HTTP_GET_VARS['strict']) {
		$select[] = 'count(id_mot) as tot';
		$group = "main.$id_chose";
		$order = 'tot DESC';
	  }
  } else {
	$from[] = "$table_principale as main"; 
  }

  
  $res=spip_abstract_select($select,$from,$where,$group,$order);
  
  $choses = array();
  $avec_sans = (count($sans_mots) > 0);
  if($avec_sans) $in_sans = calcul_in($sans_mots);
  while ($row = spip_abstract_fetch($res)) {
	if(!isset($table_auth) ||
	   (isset($table_auth) &&
		(verifier_admin() ||
		 verifier_auteur($table_auth,$id_chose,$row[$id_chose])
		 )
		)
	   ) {
	  if($avec_sans) {
		$test = spip_abstract_select(array($id_chose),array("spip_mots_$nom_chose"),array("id_mot IN ($in_sans)","$id_chose = ".$row[$id_chose]));
		if(spip_abstract_count($test) > 0) {
		  continue;
		}
		spip_abstract_free($test);
	  }
	  if(count($mots) > 0 && $HTTP_GET_VARS['strict']) {
		if($row['tot'] >= count($mots)) {
		  $choses[] = $row[$id_chose];
		} else {
		  break;
		}
	  } else {
		$choses[] = $row[$id_chose];
	  }
	}
  }
  spip_abstract_free($res);
}

if(count($choses) > 0) {
  $select = array();
  $from = array();
  $where = array();
  $show_mots = array();
  $from[] = "spip_mots_$nom_chose";
  $select[] = "spip_mots_$nom_chose.id_mot";
  $where[] = "spip_mots_$nom_chose.$id_chose IN (".calcul_in($choses).')';
  $res=spip_abstract_select($select,$from,$where);
  while ($row = spip_abstract_fetch($res)) {
	$show_mots[] = $row['id_mot'];
  }
  spip_abstract_free($res);
} 

/***********************************************************************
 * affichage
 ***********************************************************************/

debut_page('&laquo; '._T('motspartout:titre_page').' &raquo;', 'documents', 'mots');

echo '<br><br><center>';
gros_titre(_T('motspartout:titre_page'));
echo '</center>';

//Colonne de gauche
debut_gauche();

// choix de la chose sur laquelle on veut ajouter des mots
debut_cadre_enfonce('',false,'',_T('motspartout:choses'));
echo  '<form action="mots_partout.php">
<div class=\'liste\'>
<table border=0 cellspacing=0 cellpadding=3 width=\"100%\">
<tr class=\'tr_liste\'>
<td><select name="nom_chose">';
foreach($choses_possibles as $cho => $m) {
  echo "<option value=\"$cho\"".(($cho == $nom_chose)?'selected':'').'>'._T($m['titre_chose']).'</option>';
}
echo '</select></td><td><button type=\'submit\'>';
echo _T('bouton_valider');
echo '</button></td></tr>
</table></div></form>';
fin_cadre_enfonce();

echo '<form action="mots_partout.php" >';

// les actions et limitations possibles.
debut_cadre_enfonce('',false,'',_T('motspartout:action'));

echo '<div class=\'liste\'>
<table border=0 cellspacing=0 cellpadding=3 width=\"100%\">
<tr class=\'tr_liste\'>
<td><input type=\'radio\' value=\'ajouter\' name="bouton" id=\'ajouter\'><br><label for=\'ajouter\'>'.
_T('motspartout:ajouter').
'</label></td>
<td ><input type=\'radio\' value=\'enlever\' name="bouton" id=\'enlever\'><br><label for=\'enlever\'>'.
_T('motspartout:enlever').
'</label></td>
<td>
<input type=\'radio\' value=\'voir\' name="bouton" id=\'voir\' checked><br><label for=\'voir\'>'.
_T('motspartout:voir').
'</label></td>
</tr>
<tr class=\'tr_liste\'><td>'.
_T('motspartout:limite').
':</td><td><select name="limit">
<option value="rien" selected="true">'.
_T('motspartout:aucune').
'</option>';

foreach($tables_limite as $t => $m) {
  echo "<option value=\"$t\"".(($t == $limit)?'selected':'').">$t</option>";
}

echo '</select></td>';
echo "<td><input type='text' size='3' name='id_limit' value='$id_limit'></td>";
?>

</tr>
<tr class='tr_liste'>
<td>
<input type='checkbox' id='strict' name='strict'/><label for='strict'>
<?php echo _T('motspartout:stricte'); ?>
</label></td>
<td></td>
<td><button type='submit'>
<?php echo _T('bouton_valider'); ?>
</button></td>
</tr>
</table>
</div>

<?php

fin_cadre_enfonce();

// affichage de mots clefs.
$select = array('*');
$from = array('spip_groupes_mots');
$order = 'titre';
$result_groupes = spip_abstract_select($select,$from,'','',$order);

while ($row_groupes = spip_abstract_fetch($result_groupes)) {
  $id_groupe = $row_groupes['id_groupe'];
  $titre_groupe = typo($row_groupes['titre']);
  $unseul = $row_groupes['unseul'];
  $acces_admin =  $row_groupes['0minirezo'];
  $acces_redacteur = $row_groupes['1comite'];

  if($row_groupes[$nom_chose] == 'oui' && (($GLOBALS['connect_statut'] == '1comite' AND $acces_redacteur == 'oui') OR ($GLOBALS['connect_statut'] == '0minirezo' AND $acces_admin == 'oui'))) {
	// Afficher le titre du groupe
	debut_cadre_enfonce("groupe-mot-24.gif", false, '', $titre_groupe);
	
	//
	// Afficher les mots-cles du groupe
	//
	$query = "SELECT * FROM spip_mots WHERE id_groupe = '$id_groupe' ORDER BY titre";
	$result = spip_query($query);
	$table = '';
	
	if (spip_abstract_count($result) > 0) {
	  echo "<div class='liste'>";
	  echo "<table border=0 cellspacing=0 cellpadding=3 width=\"100%\">";
	  $i =0;
	  $table[] = array(
					   ' ' => 0,
					   _T('motspartout:select') => 0,
					   _T('motspartout:sans') => 0
					   );
	  while ($row = spip_abstract_fetch($result)) {
		$i++;
		$vals = '';
		
		$id_mot = $row['id_mot'];
		$titre_mot = $row['titre'];
		
		$s = typo($titre_mot);
		
		$vals["<label for='id_mot$i'>$s</label>"] = calcul_numeros($show_mots,$id_mot,count($choses));
		
		if($unseul == 'oui') 
		  $vals["<input type='radio' name='id_mots[]' id='id_mot$i' value='$id_mot'>"] = calcul_numeros($show_mots,$id_mot,count($choses));
		else
		  $vals["<input type='checkbox' name='id_mots[]' id='id_mot$i' value='$id_mot'>"] = calcul_numeros($show_mots,$id_mot,count($choses));
		
		$vals["<input type='checkbox' name='sans_mots[]' id='sans_mot$i' value='$id_mot'>"] = calcul_numeros($show_mots,$id_mot,count($choses));
		$table[] = $vals;
	  }
	  
  }
	$largeurs = array(40, 10, 10);
	$styles = array(
					array('arial11',
						  'diff-deplace',
						  'diff-ajoute'),
					array('arial1',
						  'diff-para-deplace',
						  'diff-para-ajoute'),
					array('arial1',
						  'diff-para-deplace',
						  'diff-para-ajoute')
					);
	md_afficher_liste($largeurs, $table, $styles);
	
	echo "</table>";
	echo "</div>";
	spip_abstract_free($result);
	
	fin_cadre_enfonce();
  }
}
spip_abstract_free($result_groupes);

//Milieu

debut_droite();


if(count($warnings) > 0) {
  debut_cadre_relief('',false,'',_T('motspartout:ATTENTION'));
  echo '<div class="liste"><table border=0 cellspacing=0 cellpadding=3 width=\"100%\">';
  $largeurs = array('100%');
  $styles = array( 'arial11');
  afficher_liste($largeurs, $warnings, $styles);
  echo '</table>';
  echo '</div>';
  fin_cadre_enfonce();
}

// Affichage de toutes les choses (on pourrait imaginer faire une pagination l�)
debut_cadre_relief('',false,'document', _T('portfolio'));
if(count($choses) > 0) {
  $function = "afficher_liste_$nom_chose";
  if(function_exists($function)) 
	$function($choses);
  else
	afficher_liste_defaut($choses);
} else {
  echo _T('motspartout:pas_de_documents').'.';
}
fin_cadre_relief();
echo '<input type="hidden" name="nom_chose" value="'.$HTTP_GET_VARS['nom_chose'].'"></form>';
fin_page();
?>
