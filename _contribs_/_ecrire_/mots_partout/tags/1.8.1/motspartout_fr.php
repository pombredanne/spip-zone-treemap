<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
									   'titre_page' => 'Assignation de mots clefs',
									   'ajouter' => 'ajouter',
									   'enlever' => 'enlever',
									   'voir' => 'voir',
									   'limite' => 'limitation',
									   'aucune' => 'aucune',
									   'action' => 'Action',
									   'stricte' => 'stricte',
									   'select' => 'selection',
									   'sans' => 'sans',
									   'pas_de_documents' => 'Il n&#39;y a aucun objet avec ces charactéristiques',
									   'choses' => 'Ajouter des mots clefs sur:',
									   'dejamotgroupe' => 'il y a déjà un mot de ce groupe (@groupe@) sur l&#39;objet @chose@.',
									   'ATTENTION' => 'ATTENTION'
									   );

?>
