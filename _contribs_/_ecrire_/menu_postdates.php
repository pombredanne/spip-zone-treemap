function menu_postdates() {

	$ret = "<div id='valider_statut_date'>
		<small>"
		._T('info_mise_en_ligne')
		. " &nbsp;";

	$ret .= "<select name='date_posterieure' class='fondl'>\n";
	$ret .= "<option value=''>"._L('imm&#233;diate')."</option>\n";

	$t = time();
	for($j=1; $j<10; $j++) {
		$d = date('Y-m-d', $t+24*3600*$j);
		$ret .= "<option value='".$d."'>" . (($j == 1)? 'demain' : affdate_court($d)) . "</option>\n";
	}

	$ret .= "</select>\n";

	$ret .=
		"</small></div><script type='text/javascript'><!--
		jQuery('#valider_statut_date').hide();
		//--></script>\n";

	$js = " if (options[selectedIndex].value == 'publie')"
		. " jQuery('#valider_statut_date:hidden').slideDown();"
		. "else jQuery('#valider_statut_date').hide();";

	return array($ret, $js);
}


