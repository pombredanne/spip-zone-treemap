<?php

include ("inc_version.php3");

include_ecrire ("inc_admin.php3");
include_ecrire ("inc_texte.php3");
include_ecrire ("inc_presentation.php3");
// Ajout Nicolas Steinmetz <nsteinmetz@gmail.com> - Adaptation du script a la version 1.9 alpha
if(file_exists('inc_mnipres.php'))
	{
	include_ecrire ("inc_minipres.php");
	}

//$charset_import = 'windows-1256';		// arabe
$charset_import = 'iso-8859-1';			// iso-latin (occidental)


/*
 * REMARQUE IMPORTANTE : SECURITE
 * Ce systeme de reparation doit pouvoir fonctionner meme si
 * la base de donnees n'est pas chargee : on n'appelle donc pas
 * inc_auth ; seule l'authentification ftp est exigee
 *
 */

// include_ecrire ("inc_auth.php3");
$connect_statut = '0minirezo';


function my_charset_utf8($texte) {
	global $charset_import;

	static $bloc;
	$bloc ++;
	spip_log ("conversion du bloc numero $bloc");

	for ($i=128; $i<=255; $i++) {
		$texte = str_replace(chr($i), "=@@".chr($i), $texte);
	}
	for ($i=128; $i<=255; $i++) {
		$t = charset2unicode(chr($i),$charset_import,true);
		$s = unicode2charset($t, 'utf-8');
		spip_log("conversion '$i' '$t' '$s'");

		// transcoder les caracteres "bruts"
		$texte = str_replace("=@@".chr($i), $s, $texte);

		// transcoder les &amp;#xxxx;
		if (ereg('^&#', $t))
			$texte = str_replace(ereg_replace('^&', '&amp;', $t), $s, $texte);

	}
	return $texte;
}

function convertir_dump() {
	include_ecrire('inc_charsets.php3');

	if (file_exists('data/dump.xml'))
		$file = 'data/dump.xml';

	if (file_exists('data/dump.xml.gz'))
		$filegz = 'data/dump.xml.gz';

	if (!$file AND !$filegz) return "Aucun dump disponible.";

	$out = fopen("data/dump-utf8.xml", "wb");
	if (!$out) return "Erreur fichier";

	if ($file) {
		$in = fopen($file, "r");
		while ($text = fread($in, 640000)) {

			if (!$zap) {
				$text = str_replace('<?'.'xml version="1.0" encoding="'.$charset_import.'"', 
				'<?'.'xml version="1.0" encoding="utf-8"', $text);
				$zap = true;
			}

			$text = my_charset_utf8($text);
			fwrite($out, $text);
			@set_time_limit(90);
		}
		fclose($out);
		spip_log("Conversion terminee");
	}

	else if ($filegz) {
		$in = gzopen($filegz, "r");
		while ($text = gzread($in, 640000)) {

			if (!$zap) {
				$text = str_replace('<?'.'xml version="1.0" encoding="'.$charset_import.'"', 
				'<?'.'xml version="1.0" encoding="utf-8"', $text);
				$zap = true;
			}

			$text = my_charset_utf8($text);
			fwrite($out, $text);
			@set_time_limit(90);
		}
		fclose($out);
		spip_log("Conversion terminee");
	}

	return _L("OK. Vous pouvez maintenant <a href='import_all.php3?archive=dump-utf8.xml'>importer</a> le nouveau fichier.");
}


$action = _L('Convertir le charset de la sauvegarde');


$message = _L('Une fois que vous avez export&eacute; votre base en '.$charset_import.' dans
le fichier <tt>ecrire/data/dump.xml</tt> (ou <tt>ecrire/data/dump.xml</tt>), cette page
vous permet de la convertir en utf-8. Si la conversion fonctionne normalement, vous pourrez
ensuite r&eacute;importer la base <tt>ecrire/data/dump-utf8.xml</tt>.
<br>N.B.: vous pouvez suivre la progression de la conversion dans <tt>ecrire/data/spip.log</tt>');

debut_admin($action, $message);


install_debut_html($action);


	debut_cadre_relief();
	if ($error = convertir_dump())
		echo "<br><br>$error";
	fin_cadre_relief();
	echo "<br>";

install_fin_html();

fin_admin($action);


?>
