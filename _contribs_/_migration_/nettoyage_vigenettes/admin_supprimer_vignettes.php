<?php

### OUTIL DE MIGRATION VERS LE NOUVEAU SYSTEME DE "VUES REDUITES" 
### REMPLACANT LES ANCIENNES "VIGNETTES AUTOMATIQUES"
### Auteur: fil@rezo.net

### Regles de commit : quiconque peut améliorer l'interface et debuguer

### TODO: permettre de cocher les vignettes a supprimer et "zou".

### Droits d'auteurs: (C) Philippe Riviere 2005 - Licence GNU/GPL


### USAGE: installer dans ecrire/ puis appeler avec son navigateur

	include('inc.php3');
	include_ecrire('inc_documents.php3');

	if ($connect_statut!='0minirezo') die("minirezo");

	debut_page();
	
	gros_titre("Suppression des vieilles vignettes");

	
	debut_gauche();

	echo "Utilisez cette page pour supprimer vos anciennes 'vignettes automatiques' ; SPIP permet desormais d'utiliser des 'vues reduites' beaucoup plus performantes. Ne conservez que les vignettes que vous avez ajoutees vous-meme.<p />
	NOTE: seules les documents ayant une vignette qui peut avoir ete faite automatiquement (d'apres leur nom de fichier) sont listes ici.<p />
	Apres usage supprimez ce fichier ecrire/admin_supprimer_vignettes.php.
	";

	debut_droite();

	$s = spip_query("SELECT * FROM spip_documents
		WHERE id_vignette>0 AND id_type IN (1,2,3)");

echo "<table border=1>";
echo "<tr><td>Voici la vignette actuelle (cliquez-la pour voir le document). <td>Voici ce que vous obtiendrez (version reduite) si vous supprimez la vignette ; si c'est mieux, ou equivalent, n'hesitez pas a supprimer la vignette.</tr>\n";

	while ($t = spip_fetch_array($s)) {
		$v = spip_fetch_array(spip_query("SELECT * FROM spip_documents WHERE id_document=".$t['id_vignette']));

		$base = preg_replace(",\.(jpg|gif|png)$,", "", $t['fichier']);

		// Candidats a suppression
		if (strpos('-'.$v['fichier'], "$base-s.")
		OR strpos('-'.$v['fichier'], "$base-prv.")) {
			echo "<tr>";

			echo "<td>";
			// Affiche la vignette
			echo texte_vignette_document($v['largeur'], $v['hauteur'], $v['fichier'], $t['fichier']);

			if ($supprimer == $t['id_document']) {
				// supprimer la reference a la vignette et sa description
				// (on laisse son fichier au cas ou...)
				spip_query("UPDATE spip_documents SET id_vignette=0 WHERE id_document=".$t['id_document']);
				spip_query("DELETE FROM spip_documents WHERE id_document=".$v['id_document']);
				echo "<br>supprimee !";
			}
			else
				echo "<br><a href='admin_supprimer_vignettes.php?supprimer=".$t[id_document]."'>ok, supprimer</a>";

			echo "<td>";
			// Affiche la version reduite
			echo prive_lien_image_reduite ($t['largeur'], $t['hauteur'], $t['fichier']);

			echo "</td></tr>";
		}
	}


	echo "</table>";

	fin_page();

?>