<?php
function affiche_diff_champ($id_article, $champ, $id_version, $format='complet') {
	include_spip('inc/suivi_versions');
	$textes = revision_comparee($id_article, $id_version, $format);
	return propre_diff($textes[$champ]);
}
?>