<?
$forcer_lang = true;
/////////////////////////////////////
// BOUCLE LANGUES
/////////////////////////////////////
$langues = array(
	"abbrev" => "char(2)",
	"nom" => "varchar(20)",
	"url_lang" => "varchar(255)"
);
$langues_key = array(
	"PRIMARY KEY"	=> "abbrev"
);
$GLOBALS['tables_principales']['langues'] =
	array('field' => &$langues, 'key' => &$langues_key);
//$GLOBALS['$table_primary']['langues'] ="abbrev";

function boucle_LANGUES($id_boucle, &$boucles) {
	$boucle = &$boucles[$id_boucle];
	$separateur=join('', $boucle->separateur);
	$code=<<<CODE
	\$SP++;
	\$code=array();
	\$cible = new Link();
	\$cible->delVar('lang');
	\$cible = \$cible->getUrl();
	\$site = lire_meta('adresse_site');
	\$post = (\$site ? \$site : '..') . '/spip_cookie.php3?url='.urlencode(\$cible);
	
	\$l= split(',', lire_meta('langues_multilingue'));
	foreach(\$l as \$k) {
		\$mUrl=\$post.'&var_lang='.\$k;
		\$Pile[\$SP] = array('abbrev' => \$k,
							 'nom' => traduire_nom_langue(\$k),
							 'url_lang' => \$mUrl);
		\$code[]=$boucle->return;
	}
	\$t0= join('$separateur', \$code);
CODE;

	return $code;
}
?>