<?php

/*
*	Voici un plugin spip pour avoir des alias. 
*
*    Copyright (C) 2005  Pierre ANDREWS
*
*    This program is free software; you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation; either version 2 of the License, or any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software
*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
* Ce fichier inclus le fichier generer precedemment pour qu'il soit connue par spip.
*/

include('config.php');

$file = $plug_pose_racine_site . '/ecrire/data/plugin_alias_genere.php';
if (!$GLOBALS['included_files'][$file]++)
  if (file_exists($file)) include($file);


?>
