<?php

include('config.php');

$GLOBALS['pose_version'] = 1.0;

/*
*	Voici un plugin spip pour avoir des alias. Ce fichier g�n�r� les nouvelles balises.
*   url: http://www.spip-contrib.net/
*
*    Copyright (C) 2005  Pierre ANDREWS
*
*    This program is free software; you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation; either version 2 of the License, or any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software
*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


/*
* Gestion des tags xml ouvrants.
*/
function pose_debut($parser, $name, $attrs) {
  global $pose_state; //�tat actuel (qu'elle �tait la derni�re balise)
  global $pose_texte; //le texte en m�moire
  global $pose_special; //cas sp�cial ou un jeu par d�faut est sp�cifie
  global $to_ret_Pose; //code g�n�rer par l'alias actuel
  global $pose_defaut; //le jeu par d�faut (premi�re rencontre)
  global $pose_version; //la version du plugin pour la compatibilit�
  global $pose_boucles; //tableau des boucles pour cet alias
  global $pose_contrainte_boucles; //y a t il des contraintes de boucles sur cet alias
  global $pose_nom_alias;  //le nom de l'alias
  global $pose_bool_jeux;

  switch($name) {
	case 'ALIASES':
	  //balises englobante
	  if($pose_state != '') die('fichier mal form� (&lt;'.$name.'&gt; ligne '.xml_get_current_line_number($parser).')');//rien avant!!
	  //v�rification de la version du fichier
	  if($attrs['VERSION_FICHIER'] && $pose_version > $attrs['VERSION_FICHIER']) {
		echo "La version $pose_version du plugin n'est pas compatible avec la version du fichier: ".$attrs['VERSION_FICHIER'];
	  }
	  echo "<a href=\"".$attrs['DOC']."\">".$attrs['DESCRIPTION']."</a></p>";
	  break;
	case 'JEUX':
	  if(($pose_state != 'ALIASES') && ($pose_state != 'JEUXc') && ($pose_state != 'ALIASc')) die('fichier mal form� (&lt;'.$name.'&gt; ligne '.xml_get_current_line_number($parser).')'); //seulement apr�s un autre alias ou au d�but d'une connection
	  echo '<p>jeux globaux<i>'.$attrs['DESCRIPTION']."</i>";	  
	  break;
	case 'ALIAS':
	  //balise pour un alias
	  if(($pose_state != 'ALIASES') && ($pose_state != 'JEUXc') && ($pose_state != 'ALIASc')) die('fichier mal form� (&lt;'.$name.'&gt; ligne '.xml_get_current_line_number($parser).')'); //seulement apr�s un autre alias ou au d�but d'une connection
	  $pose_nom_alias = strtoupper($attrs['NOM']);
	  echo '<p>balise #POSE_'.$pose_nom_alias.':<i>'.$attrs['DESCRIPTION']."</i>";
	  if($attrs['BOUCLES']) {
		//il y a des contraintes sur les boucles applicables
		$pose_contrainte_boucles = true;
		$pose_boucles = split(',',$attrs['BOUCLES']);
		//intiatilisation du tableau jeux pour cette boucle.
		foreach($pose_boucles as $boucle) {
		  $to_ret_Pose .= "\n\t\$jeux['$boucle'] = array();";
		  $to_ret_Pose .= "\n\t\$jeux['$boucle']['defaut'] = array();";
		}
		//ajout des contraintes de boucles aux contraintes qui existent d�j�
		$to_ret_Pose .= "\n\t\$boucles .= ',".$attrs['BOUCLES']."';";
		echo '<br><small>pour les boucles:'.$attrs['BOUCLES'].'</small> </p><ol>';
	  } else {
		//pas de contraintes, on initialise le tableau pour toutes les boucles
		$to_ret_Pose .= "\n\t\$jeux['toutes'] = array();";
		$to_ret_Pose .= "\n\t\$jeux['toutes']['defaut'] = array();";
		$pose_boucles = array('toutes');
		echo '<br><small>pour toutes les boucles</small> </p><ol>';
	  }
	  //sp�cification de la balise par d�faut
	  if($attrs['DEFAUT']) {
		$pose_defaut = $attrs['DEFAUT'];
	  } else {
		$pose_defaut = 'defaut';
	  }
	  break;
	case 'POSER':
	  //le texte � poser � la place de l'alias
	  if(($pose_state != 'PARAMETREc') && ($pose_state != 'JEUc') && ($pose_state != 'ALIAS')) die('fichier mal form� (&lt;'.$name.'&gt; ligne '.xml_get_current_line_number($parser).')'); //seulement dans une balise alias
	  if($pose_state == 'PARAMETREc') $to_ret_Pose .= ';'; //finir le dernier param�tre qui se balade en dehors d'un jeu
	  //un texte diff�rent pour chaque boucle
	  if($pose_contrainte_boucles) {
		foreach($pose_boucles as $boucle) {
		  $to_ret_Pose .= "\n\t\$texte['$boucle'] = ";
		}
	  } else {
		$to_ret_Pose .= "\n\t\$texte['toutes'] = ";
	  }
	  break;
	case 'JEU':
	  //un jeu de param
	  if(($pose_state != 'POSERc') && ($pose_state != 'JEUc') && ($pose_state != 'JEUX') && ($pose_state != 'PARAMETREc') && ($pose_state != 'ALIAS')) die('fichier mal form� (&lt;'.$name.'&gt; ligne '.xml_get_current_line_number($parser).')');
	  echo '<li>jeu:'.$attrs['NOM'].':<i>'.$attrs['DESCRIPTION'].'</i><ol>';
	  if($pose_bool_jeux || ($pose_state == 'JEUX')) {
		$to_ret_Pose .= "\n\n\$GLOBALS['pose_jeux_globaux']['".$attrs['NOM']."'] = ";
		$pose_bool_jeux = true;
	  } else {
		if($pose_defaut == 'defaut') $pose_defaut = $attrs['NOM']; //si c'est le premier qu'on trouve, c'est celui par d�faut
		if($pose_state == 'PARAMETREc') $to_ret_Pose .= ';'; //fermer le param�tre qui se balade en dehors d'un jeu
	  if($pose_contrainte_boucles) {
		//un nouveau tableau pour toutes les boucles auxquelles s'applique ce jeu
		foreach($pose_boucles as $boucle) {	  
		  $to_ret_Pose .= "\n\t\$jeux['$boucle']['".$attrs['NOM']."'] = ";
		}
	  } else {
		$to_ret_Pose .= "\n\t\$jeux['toutes']['".$attrs['NOM']."'] = ";
	  }
	  }
	  $to_ret_Pose .= "array(";
	  break;
	case 'PARAMETRE':
	  // un param�tre
	  if(($pose_state != 'POSERc') && ($pose_state != 'JEU') && ($pose_state != 'JEUc') && ($pose_state != 'PARAMETREc') && ($pose_state != 'ALIAS')) die('fichier mal form� (&lt;'.$name.'&gt; ligne '.xml_get_current_line_number($parser).')'); // � peut pr�t partout
	  echo '<li>'.$attrs['NOM'].'</li>';
	  switch($pose_state) {
		case 'PARAMETREc':
		  //un param apr�s un param
		  if($pose_special) {
			// si on se trouve en dehors d'un jeu
			$to_ret_Pose .= ';';
			if($pose_contrainte_boucles) {
			  //on l'ajoute � tous les tableaux du jeu par d�faut
			  foreach($pose_boucles as $boucle) {	  
				$to_ret_Pose .= "\n\t\$jeux['$boucle']['defaut']['".$attrs['NOM']."'] = ";
			  }
			} else {
			  $to_ret_Pose .= "\n\t\$jeux['toutes']['defaut']['".$attrs['NOM']."'] = ";
			}
			$to_ret_Pose .= "array(";
			break;
		  } else
			//c'est la continuation d'un tableau de jeu
			$to_ret_Pose .= ',';
		case 'JEU':
		  //on se trouve dans un jeu
		  $to_ret_Pose .= "\n\t\t'".$attrs['NOM']."' => array(";
		  break;
		default:
		  //les autres cas
		  if($pose_contrainte_boucles) {
			foreach($pose_boucles as $boucle) {	  
			  //on g�n�r� les tableaux du jeu d�faut
			  $to_ret_Pose .= "\n\t\$jeux['$boucle']['defaut']['".$attrs['NOM']."'] = ";
			}
		  } else {
			$to_ret_Pose .= "\n\t\$jeux['toutes']['defaut']['".$attrs['NOM']."'] = ";
			}
		  $to_ret_Pose .= "array(";
		  $pose_special = true;
		  break;
	  }
	  break;
	case 'VALEUR':
	  //une valeur de param�tre
	  if(($pose_state != 'VERIFICATIONc') && ($pose_state != 'PARAMETRE')) die('fichier mal form� (&lt;'.$name.'&gt; ligne '.xml_get_current_line_number($parser).')'); //seulement dans un param�tre
	  if($pose_state == 'VERIFICATIONc') $to_ret_Pose .= ','; //on continue le tableau apr�s une balise v�rification
	  $pose_texte = '';
	  $to_ret_Pose .= "\n\t\t\t'valeur' => ";
	  break;
	case 'VERIFICATION':
	  //une v�rification de param�tre
	  if(($pose_state != 'VALEURc') && ($pose_state != 'PARAMETRE')) die('fichier mal form� (&lt;'.$name.'&gt; ligne '.xml_get_current_line_number($parser).')');
	  if($pose_state == 'VALEURc') $to_ret_Pose .= ','; //on se trouve apr�s une valeur, on continue le tableau
	  $pose_texte = '';
	  $to_ret_Pose .= "\n\t\t\t'verification' => ";
	  break;
  }
  $pose_state = $name; //nouvel �tat
}

/*
* Gestion des tags xml fermants.
*/
function pose_fin($parser, $name) {
  global $pose_state;
  global $pose_texte;
  global $pose_defaut;
  global $to_ret_Pose;
  global $pose_boucles;
  global $pose_nom_alias;  
  global $pose_contrainte_boucles;
  global $pose_bool_jeux;
  
  global $pose_jeux;
  global $pose_aliases; //tous les alias rencontres. au cas ou on rencontre plusieurs fois le m�me chose.
  
  switch($name) {
	case 'JEUX':
	  $pose_bool_jeux = true;
	  $pose_jeux .= $to_ret_Pose;
	  $to_ret_Pose = '';
	  break;
	case 'ALIAS':
	  //on ferme une balise �tat
	  if(($pose_state != 'PARAMETREc') && ($pose_state != 'JEUc') && ($pose_state != 'POSERc')) die('fichier mal form� (&lt;'.$name.'&gt; ligne '.xml_get_current_line_number($parser).')');
	  echo '</ol>';
	  $pose_texte='';
	  if($pose_state == 'PARAMETREc') $to_ret_Pose .= ';'; //on fini ce param qui se balade tout seul hors d'un jeu
	  if($pose_contrainte_boucles) {
		//d�faut pour toutes les boucles diff�rentes.
		foreach($pose_boucles as $boucle) {
		  $to_ret_Pose .= "\n\t\$defaut['$boucle'] = '$pose_defaut';";
		} 
	  } else {
		  $to_ret_Pose .= "\n\t\$defaut['toutes'] = '$pose_defaut';";
	  }
	  $pose_boucles = '';
	  //test si un alias existe d�j� dans ceux qu'on conna�t
	  if($pose_aliases[$pose_nom_alias]) {
		$pose_aliases[$pose_nom_alias] .= $to_ret_Pose;
	  } else {
		$pose_aliases[$pose_nom_alias] = $to_ret_Pose;
	  }
	  $to_ret_Pose = '';
	  break;
	case 'POSER':
	  //on a fini de lire le texte � poser
	  if($pose_state != 'POSER') die('fichier mal form� (&lt;'.$name.'&gt; ligne '.xml_get_current_line_number($parser).')');
	  $to_ret_Pose .= "'$pose_texte';";
	  $pose_texte='';
	  break;
	case 'JEU':	  
	  //on finit le tableau d'un jeu.
	  if($pose_state == 'PARAMETREc'){
		echo '</ol></li>';
		$to_ret_Pose .= "\n\t);";
	  } else if($pose_state == 'JEU') {
		echo 'voir jeux globaux</ol></li>';
		$to_ret_Pose = substr($to_ret_Pose,0,-6);
		$to_ret_Pose .= 'false;';
	  } else die('fichier mal form� (&lt;'.$name.'&gt; ligne '.xml_get_current_line_number($parser).')');
	  break;
	case 'PARAMETRE':
	  //on finit le tableau valeur,v�rification d'un param
	  if(($pose_state != 'VERIFICATIONc') && ($pose_state != 'VALEURc')) die('fichier mal form� (&lt;'.$name.'&gt; ligne '.xml_get_current_line_number($parser).')');
	  $to_ret_Pose .= "\n\t\t)";	  
	  break;
	case 'VALEUR':
	  //on a fini de lire le texte de la valeur par d�faut
	  if($pose_state != 'VALEUR') die('fichier mal form� (&lt;'.$name.'&gt; ligne '.xml_get_current_line_number($parser).')');
	  $to_ret_Pose .= "'$pose_texte'";
	  $pose_texte = '';
	  break;
	case 'VERIFICATION':
	  //on a fini de lire la regexp de v�rification du param
	  if($pose_state != 'VERIFICATION') die('fichier mal form� (&lt;'.$name.'&gt; ligne '.xml_get_current_line_number($parser).')');
	  $to_ret_Pose .= "'$pose_texte'";
	  $pose_texte = '';
	  break;
  }
  $pose_state = $name."c";
}

/*
* Gestion du texte [CDATA[ dans le xml
*/
function pose_dutexte($parser, $data) {
  global $pose_texte;
  
  $pose_texte .= trim($data);
}

/*
* parse un fichier d'alias. appel du parser
*/
function parser_fichier_pose($fichier) {
  
  global $pose_state;
	
  $pose_state ='';

  $xml_parser = xml_parser_create();
  xml_set_element_handler($xml_parser, "pose_debut", "pose_fin");
  xml_set_character_data_handler($xml_parser, "pose_dutexte");
  if (!($fp = fopen($fichier, "r"))) {
	die("could not open XML input");
  }
  
  while ($data = fread($fp, 4096)) {
	if (!xml_parse($xml_parser, $data, feof($fp))) {
	  die(sprintf("XML error: %s at line %d",
				  xml_error_string(xml_get_error_code($xml_parser)),
				  xml_get_current_line_number($xml_parser)));
	}
  }
  xml_parser_free($xml_parser);
 
}


/*
* Liste tous les fichier "alias....xml" dans le r�pertoire ecrire/plugins et ses sous r�pertoire (fonction emprunt� de la gestion des plugins)
*/
function fichiers_alias($dir) {

  $fichiers = array();
  $d = opendir($dir);
  
  while ($f = readdir($d)) {
	if (is_file("$dir/$f") AND $f != 'remove.txt') {
	  if (ereg("^alias.*\.xml$", $f)) {
		$fichiers[] = "$dir/$f";
	  }
	} else if (is_dir("$dir/$f") AND $f != '.' AND $f != '..') {
	  $fichiers_dir = fichiers_alias("$dir/$f");
	  while (list(,$f2) = each ($fichiers_dir))
		$fichiers[] = $f2;
	}
  }	
	
  closedir($d);
  
  sort($fichiers);
  return $fichiers;
}

/*G�n�ration du fichier de plugins*/
$fichiers = array();
$fichiers = fichiers_alias($plug_pose_dossier_alias);

//d�but du fichier g�n�r�: inclusion des fonctions de g�n�ration du code de la balise
$alias = "<"."?php\n\n";
$alias .= "if(defined('_PLUGINS_ALIAS_GENERE')) return;\n";
$alias .= "define('_PLUGINS_ALIAS_GENERE', '1');\n\n";
$alias .= "if(@file_exists('inc_version.php')) \$chemin = './'; else \$chemin = './ecrire/';\n";
$alias .= "include(\$chemin.'plugins/plug_pose/inc_alias.php');";

//on parcourt tous les fichiers trouv�s
foreach($fichiers as $nom_fichier) {
  echo "<p><b>$nom_fichier:</b><br/>";
  parser_fichier_pose($nom_fichier);
}

global $pose_aliases;
global $pose_jeux;

//pour tous les alias trouv�, on g�n�re la fonction balise_
foreach($pose_aliases as $nom_alias => $texte_alias) {
  $alias .= "\n\nfunction balise_POSE_".$nom_alias."(\$p) {\n\t\$jeux = array();\n\t\$boucles='';\n";
  $alias .= $texte_alias;
  $alias .= "\n\n\treturn pose_calcul(\$p,\$texte,\$jeux,\$defaut,\$boucles);\n}";
;
}

$alias .= $pose_jeux;

$alias .= "\n?".">";

include($plug_pose_racine_site . '/ecrire/inc_flock.php');

//�criture du fichier
$ok = ecrire_fichier($plug_pose_racine_site . '/ecrire/data/plugin_alias_genere.php', $alias);
if($ok)
  echo '<br>DONE';
else
  echo '<br>ERROR';

?>
