Auteur: Pierre Andrews (aka Mortimer) mortimer.pa@free.fr
Licence: GPL

Description:

[fr] Balise pour faire des raccourcis (alias) et des petites macro.

[en] SPIP Tag for aliases and macros

Listes des fichiers:

[fr]
README.txt vous y etes,
calculer_pose.php fichier qui cr�e le code pour les nouvelles
balises. A appeler individuellement 
config.php fichier de configuration des chemins, 
inc_alias.php fichier avec les fonctions
outils,
plug_pose.php fichier qui importe le fichier g�n�r� par
calculer_pose. A importer dans mes_fonctions,
_REGLES_DE_COMMIT.txt r�gles pour faire �voluer cette contrib,
TODO.txt ce qu'il reste � faire,
BUG.txt les bugs � corriger.
librairies  r�pertoire avec des librairies d'alias
		   alias_exif.xml exemple d'alias simple pour des balises EXIF
		   alias_outils.xml collection de jeux basiques
		   alias_titre.xml alias basique pour les titres d'article

[en]
README.txt you are here <-,
calculer_pose.php file that generates the TAG code for the aliases, call that at the begining,
config.php configurations
inc_alias.php tool functions
plug_pose.php file that import the file generated, include this in mes_fonctions
_REGLES_DE_COMMIT.txt rules of contribution on this project,
TODO.txt what remains to do,
BUG.txt the bugs to correct.
librairies  collection of aliases
		   alias_exif.xml example for EXIF tags
		   alias_outils.xml simple "jeux"
		   alias_titre.xml example for article titles.


Page de la contrib:
http://spip-contrib.net/ecrire/articles.php3?id_article=817



