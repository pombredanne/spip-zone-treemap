<?php

/*
*	Voici un plugin spip pour avoir des alias. 
*
*    Copyright (C) 2005  Pierre ANDREWS
*
*    This program is free software; you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation; either version 2 of the License, or any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software
*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


if(defined('_INCLUDE_ALIAS')) return;
define('_INCLUDE_ALIAS', '1');

/*
* Applique la verification sur les parametres
*/
function verifier_param($par,$verification) {
  if($verification)
	return preg_match($verification,$par);
  return true;
}

/*
* Place le parametre dans la chaine, en faisant les verifications.
*/
function remplacer_param($nom_param,$par,$texte,$verification) {
  if(verifier_param($par,$verification)) {
	$texte = str_replace("@$nom_param@",$par,$texte);
  } else 
	erreur_squelette(_L("le param�tre @$nom_param@ = '".htmlspecialchars($par)."' ne correspond pas � la contrainte: '$verification'"),
					 $p->id_boucle);
  return $texte;
}

function bon_tableau($tableaux,$type_requete) {
  if($tableaux[$type_requete]) {
	return $tableaux[$type_requete];
  } else {
	return $tableaux['toutes'];
  }
}

/*
* Calcul la chaine a placer en remplacant les parametres.
* Calcul ensuite le code php a retourner.
*/
function pose_calcul($p,$textes,$jeuxs,$defauts,$boucles) {
  global $pose_jeux_globaux;
  if($boucles && !ereg($p->type_requete,strtolower($boucles))) {   
	erreur_squelette(_L('la balise '.$p->nom_champ.' n\'est pas permise dans cette boucle'),
					 $p->id_boucle);	
  } else {
	$list = param_balise($p);
	$params = '';
	
	$texte = bon_tableau($textes,$p->type_requete);
	$jeux = bon_tableau($jeuxs,$p->type_requete);
	$defaut = bon_tableau($defauts,$p->type_requete);

	if(strpos($list,';')) {
	  $list = split(';',$list);
	  $nom_jeu = $list[0];
	  if(!isset($jeux[$nom_jeu])) {
		$nom_jeu = entites_html($_GET[addslashes($nom_jeu)]);
		if(!$nom_jeu || !isset($jeux[$nom_jeu]))
		  $nom_jeu = $defaut;

	  }	  
	  $list = $list[1];
	} else {
	  $nom_jeu = $defaut;
	}
	$jeu = $jeux[$nom_jeu];
	  if(!$jeu)
		$jeu = $pose_jeux_globaux[$nom_jeu];


	$params = split(',',$list);

	if($list && count($jeu)) {
	  list($nom_param,$val_param) = each($jeu);
	  while(list(,$par) = each($params)) {
		if($par) {
		  $jeu[$nom_param]['valeur'] = $par;
		  $texte = remplacer_param($nom_param,$par,$texte,$val_param['verification']);
		}
		list($nom_param,$val_param) = each($jeu);
	  }
	} else if($list){
	  while(list(,$par) = each($params)) {
		if(preg_match('/@(.+)@/Ui',$texte,$matches)) {
		  if($par) {
		  	$jeu[$matches[1]]['valeur'] = $par;
			$texte = remplacer_param($matches[1],$par,$texte,'');
		  }
		}
	  }
	}

	while(preg_match('/@(.+)@/Ui',$texte,$matches)) {
	  $val_param = $jeu[$matches[1]];
	  $texte = remplacer_param($matches[1],$val_param['valeur'],$texte,$val_param['verification']);
	}
	
	//	$texte = preg_replace('/@.+@/Ui','',$texte);
	
	$p->code = calculer_liste(parser_champs_etendus(" ".$texte,array()),$p->descr,$p->boucles,$p->id_boucle);
	
	$p->type = 'php';
	return $p;

  }

}

?>
