<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
$GLOBALS[$GLOBALS['idx_lang']] = array(
'module_titre'=>'Zugangsbeschr&auml;nkungen organisieren',
'module_info'=>'Dieses Modul ermöglicht, Zugangsbeschr&auml;nkungen zu Rubriken zu organisieren.  Wenn Sie den Zugang zu einer Rubrik vergeben, wird die Rubrik automatisch f&uuml;r andere Benutzer unzug&auml;nglich.',
// BLOC GROUPES
'select' => '',
'select_vide' => '-- Gruppenliste --',
'nom' => 'NOM',
'description' => 'Beschreibung',
'actif' => 'Verf&uuml;gbar?',
'creer' => 'Erzeugen',
'modifier' => 'Ver&auml;ndern',
// BLOC MEMBRES
'membres' => 'Gruppenmitglieder',
'choisir' => 'Sie m&uuml;ssen eine Gruppe w&auml;hlen',
'ajouter' => 'Hinzuf&uuml;gen',
'accepter' => 'In die Gruppe aufnehmen',
// BLOC RUBRIQUES
'rubriques_autorisees' => 'erlaubte Rubriken',
'rubriques_autorisees_info' => '<b>Zugangsbeschr&auml;nkte Rubriken sind in <font color="red">rot</font></b>',
'autoriser' => 'Den Zugang zur Rubrik erlauben',
'autoriser_info' => 'F&uuml;r diese gruppe zug&auml;ngliche Rubriken...  Unterrubriken sind automatisch eingeschlossen.',
'acces_double' => 'Diese Gruppe hat schon einen Zugang zu dieser Rubrik!',
'acces_rubrique_par' =>'Die Rubrike ist auch f&uuml;r die folgende Gruppe(n) zu g&auml;nglich:',
// MESSAGES ESPACE PUBLIQUE
'non_connecte' => 'Ihr Profil gibt Ihnen keinen Zugang zu diesem Teil der Website',
'bloque_rubrique' => '<br />Sie geh&ouml;ren nicht zur Gruppe, die zu dieser Rubrik Zugang hat.',
'bloque_article' => '<br />Sie geh&ouml;ren nicht zu einer Gruppe, die zu Artikeln dieser Rubrik Zugang hat.',

// modif clem MESSAGE SS-GROUPES 
'groupes' => 'Gruppen',
'titre_groupes' => 'Gruppen und Rubriken mit beschr&auml;nktem Zugang<br />',
'titre_page_groupe' => 'Gruppenname',
'organisation' => 'Gruppen organisieren',
'groupes_rubriques' => 'Gruppenrubriken',
'auteurs' => 'Autoren',
'ss_groupes' => 'Untergruppen',
'statuts' => 'Statute',
'du_groupe' => 'der Gruppe zugeh&ouml;rig',
'ajouter_auteur' => 'Einen Autor hinzuf&uuml;gen',
'ajouter_ss_groupe' => 'Eine Untergruppe hinzuf&uuml;gen',
'ajouter_statut' => 'Ein Statut hinzuf&uuml;gen',
'auteurs_groupe' => 'Dieser Gruppe zugeh&ouml;rige Autoren',
'ss_groupes_groupe' => 'Untergruppen zu dieser Gruppe<br />( Alle Benutzer der Untergruppe eingeschlossen )',
'statut_groupe' => 'Statut dieser Gruppe <br />( Alle Benutzer mit diesem Statut geh&ouml;ren zur Gruppe )',
'retirer_groupe' => 'Die Untergruppe l&ouml;schen',
'retirer_statut' => 'Das Statut l&ouml;schen',
'erreur_inclusion_recurrente' => '<span style="color: #f00;">Fehler! Eine Gruppe kann nicht in eine Untergruppe integriert werden.</span>',
'arborescence_groupes' => 'Untergruppenansicht',
'creation_table' => 'Tabelle erzeugen',
'installation' => 'Gruppenorganisation JPK installieren : <br />',
'install_ok' => 'Installation OK',
'install_pas_ok' => 'Es gab Fehler bei der Installation!',
'duplicata_nom' => 'Dieser Gruppenname existiert schon: W&auml;hlen Sie bitte einen anderen!'

);
?>