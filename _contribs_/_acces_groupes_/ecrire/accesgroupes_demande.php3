<?php
/*
Gestion par groupes
par Coyote et cy_altern
==============================================
Formulaire pour demander � rejoindre un groupe
==============================================
*/
$msg_text = '';
$alerte = 0;

// DETECTION DE L'EXTENSION (.php3 OU .php)
$ext = "php";
if (substr($_SERVER['PHP_SELF'],-1) == 3)  $ext = "php3";
include ("inc.$ext");
include_once("mes_options-jpk.$ext");

$id_auteur = (isset($_POST['auteur'])?$_POST['auteur']:floor($connect_id_auteur));

    // AUTEUR Suppression...
    if (isset($_GET['del_auteur'])){
      $groupe = $_GET['id_groupe'];
      $sql = "DELETE FROM $Tjpk_groupes_auteurs WHERE id_grpacces=$groupe AND id_auteur=$del_auteur";
      $result = spip_query($sql);
    }
	
// DEBUT PAGE ==================================================================
debut_page(_T('accesgroupes:module_titre'));
debut_gauche();

if ($id_auteur) {
    debut_boite_info();
    echo "<CENTER>";
    echo "<FONT FACE='Verdana,Arial,Sans,sans-serif' SIZE=1><B>"._T('titre_cadre_numero_auteur')."&nbsp;:</B></FONT>";
    echo "<BR><FONT FACE='Verdana,Arial,Sans,sans-serif' SIZE=6><B>$id_auteur</B></FONT>";
    echo "</CENTER>";
    fin_boite_info();
}

debut_droite();
debut_cadre_relief("rubrique-24.gif", false, "", _T("accesgroupes:module_titre"));
debut_boite_info();
echo "<form action=\"".$_SERVER['PHP_SELF']."\" method=\"post\">";
echo "<SELECT NAME='id_rubrique' style='font-size: 90%; width:80%; font-face:verdana,arial,helvetica,sans-serif; max-height: 24px;' SIZE=1>\n";

if ($connect_toutes_rubriques) {
  echo "<OPTION".mySel("0",$id_parent). http_style_background('racine-site-12.gif',  "$spip_lang_left no-repeat; background-color:$couleur_foncee; padding-$spip_lang_left: 16px; font-weight:bold; color:white") .'>'._T('info_racine_site')."\n";
} else {
	echo "<OPTION".mySel("0",$id_parent).">"._T('info_non_deplacer')."\n";
}

if (lire_meta('multi_rubriques') == 'oui') echo " [".traduire_nom_langue(lire_meta('langue_site'))."]";

// si le parent ne fait pas partie des rubriques restreintes, modif impossible
if (acces_rubrique($id_parent)) {
	AccesGroupes_enfant(0);
}
echo "</SELECT>\n";
echo "<input type=\"submit\" value=\""._T('voir')."\" CLASS=\"fondo\">";
echo "</form>";
fin_boite_info();

include("../".$dossier_squelettes."formulaire_groupes.html");
fin_cadre_relief();

debut_cadre_relief("statuts-24.png", false, "", _T("accesgroupes:msg_demande_acces2"));
$sql="SELECT spip_jpk_groupes.* FROM spip_jpk_groupes_auteurs, spip_jpk_groupes 
WHERE spip_jpk_groupes_auteurs.id_grpacces = spip_jpk_groupes.id_grpacces AND 
	  spip_jpk_groupes_auteurs.id_auteur=$id_auteur";
$result= spip_query($sql);
echo mysql_error();
echo "<table BORDER=0 CELLSPACING=0 CELLPADDING=3 WIDTH=100% BACKGROUND=''>";
//echo "<tr><th>"._T('accesgroupes:select_vide')."</th><th>&nbsp;</th></tr>";
while($row=spip_fetch_array($result)){
echo "<tr><td>".$row['nom']."</td><td><a href=\"".$_SERVER['PHP_SELF']."?id_groupe=".$row['id_grpacces']."&del_auteur=$id_auteur\" >"._T('accesgroupes:supprimer')."&nbsp;".http_img_pack('croix-rouge.gif', "X", "width='7' height='7' border='0' align='middle'") ."</a></td></tr>";
}
echo "</table>";
fin_cadre_relief();
echo "&nbsp;<p>";
fin_page();
?>