<?php
// FONCTIONS pour ACCESGROUPE

// pour r�cup�ration des noms des tables accesgroupes
include_once("inc_config_accesgroupes.php3");

// cr�ation de la boucle ACCESGROUPES
$GLOBALS['tables_principales']['spip_accesgroupes'] = array('field' => array(), 'key' => array());
$GLOBALS['table_des_tables']['accesgroupes'] = 'accesgroupes';

function boucle_ACCESGROUPES($id_boucle, &$boucles) {
				 global $Tspip_rubriques, $Tspip_breves, $Tspip_articles;
				 $boucle = &$boucles[$id_boucle];
			// si on est pas dans un squelette rubrique, trouver le id_rubrique
				 if (!$GLOBALS['id_rubrique']) {
				 	// cas du squelette article
						if ($GLOBALS['id_article']) {
							 $crit_champ = 'id_article';
							 $crit_table = $Tspip_articles;
							 $crit_id = $GLOBALS['id_article'];
						}
				 // cas du squelette breve
						else {
								 $crit_champ = 'id_breve';
								 $crit_table = $Tspip_breves;
								 $crit_id = $GLOBALS['id_breve'];
						}
					  $sql222 = "SELECT id_rubrique FROM $crit_table WHERE $crit_champ = $crit_id LIMIT 1";
					  $result222 = spip_query($sql222);
						$row222 = spip_fetch_array($result222);
						$id_rub = $row222['id_rubrique'];
				 }
				 else {
				 			$id_rub = $GLOBALS['id_rubrique'];
				 }
	$code = <<<CODE
						 if (verif_acces($id_rub, 'public') == 1 OR verif_acces($id_rub, 'public') == 2) {
						 		return '&nbsp;';
						 }
CODE;
	return $code;
}
/* pour info : syntaxe possible dans $code = <<<CODE
				\$toto = '$txt';
				return \$toto;
*/


// le filtre qui permet d'ajouter une img aux #TITRE des rubriques/articles/breves � acc�s restreint
function accesgroupes_visualise($texte, $id_rub = 0, $image = 'cadenas-24.gif') {
				 if (verif_acces($id_rub, 'public') == 1 OR verif_acces($id_rub, 'public') == 2) {
				 		return "<img src=\"ecrire/img_pack/".$image."\" alt=\""._T('accesgroupes:bloque_rubrique')."\" style=\"border: none; vertical-align: baseline;\"> ".$texte;
				 }
				 else {
				 			return $texte;
				 }
}

// le crit�re qui permet de ne pas afficher les rubriques � acc�s restreint
function critere_accesgroupes_invisible($id_boucle, &$boucles, $crit) {
				 global $Tjpk_groupes_acces;
				 $boucle = &$boucles[$id_boucle];
      // construit le tableau de toutes les rubriques � acces interdit (dans la partie publique) pour l'auteur en cours
				 $sql1 = "SELECT id_rubrique FROM $Tjpk_groupes_acces WHERE prive_public != 1";
         $result1 = spip_query($sql1);
         $Trub_interdites = array();
         while ($rows = spip_fetch_array($result1)) {
        			 if (verif_acces($rows['id_rubrique'], 'public') == 1 OR verif_acces($rows['id_rubrique'], 'public') == 2) {
        				  if (!in_array($rows['id_rubrique'], $Trub_interdites)) {
        				 		 $Trub_interdites[] = $rows['id_rubrique'];
        				  }
        			 }
         }
				 foreach ($Trub_interdites as $id_rub_ec) {
				 				 $boucle->where[] = ' id_rubrique != '.$id_rub_ec;
				 }
}


// d�termine si une rubrique $rub est restreinte ou non (en fct de la provenance $prive_public : prive | public)
function verif_acces($rub, $prive_public){
   		global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
      
      $aut = $GLOBALS['auteur_session']['login'];
      // v�rifie si la rubrique courante est priv�e
      do{ // boucle tant que la rubrique n'est pas la racine du site et que le retour est vide
         if (RubPrive($rub, $prive_public)) {
            if ($aut <> "") {
               $aut = IdAut($aut); // cherche l'id_auteur
               if (GrpAcces($aut, $rub)){
                 $retour = 3; // acc�s restreint : autoris�
               }
							 else {
                 $retour = 2; // acc�s restreint : non autoris�
               }
            }
						else {
               $retour = 1; // acc�s restreint : non connect�
            }
         }
				 else {
            $retour = 0; // acc�s libre - v�rifier la rubrique parente
         }
         $sql = "SELECT id_parent FROM $Tspip_rubriques WHERE id_rubrique = $rub LIMIT 1"; // recherche la rubrique parente
         $result = spip_query($sql);
         if ($row = spip_fetch_array($result)) {
            $rub = $row['id_parent'];
         }
      }
			while ($rub > 0 && $retour == 0 );
      
      return $retour;
}


// retourne l'id_auteur � partir du login $aut
function IdAut($aut){
   		global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
      
      $sql = "SELECT id_auteur, statut FROM $Tspip_auteurs WHERE login='" .addslashes($aut) ."' LIMIT 1";
      $result = spip_query($sql);
      if ($result){
         if ($row = spip_fetch_array($result)){
            $aut = $row['id_auteur'];
         }
      }
      return $aut;
}

// v�rifie si la rubrique $rub est restreinte, en fct de la provenance $prive_public (prive | public)
function RubPrive($rub, $prive_public){
   		global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
      $sql = "SELECT count(*) FROM $Tjpk_groupes_acces, $Tjpk_groupes 
					 	  WHERE $Tjpk_groupes_acces.id_rubrique = $rub 
							AND $Tjpk_groupes.actif = 1
							AND $Tjpk_groupes_acces.id_grpacces = $Tjpk_groupes.id_grpacces";
			$prive_public == 'prive' ? $sql .= ' AND '.$Tjpk_groupes_acces.'.prive_public < 2' : $sql .= ' AND '.$Tjpk_groupes_acces.'.prive_public != 1';
      $result = spip_query($sql);
//print '<br>$sql = '.$sql.'<br>mysql_error() = '.mysql_error().'<br>';			
      if ($row = spip_fetch_array($result)) {
      	 $prive = $row['0'];
      }
      if ($prive > 0) {
         return true;
      }
			else {
         return false;
      }
}

// v�rifie si l'auteur $aut est autoris� � acc�der � la rubrique restreinte $rub
function GrpAcces($aut,$rub){
      global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
      
      $acces = 0;
		// les admins restreints ont acc�s sans limitation � leur rubrique
		  $sql507 = "SELECT COUNT(*) AS est_admin FROM $Tspip_auteurs_rubriques WHERE id_auteur = $aut AND id_rubrique = $rub LIMIT 1";
			$result507 = spip_query($sql507);
			$row507 = spip_fetch_array($result507);
			if ($row507['est_admin'] > 0) {
				 return TRUE;
			}
			$sql = "SELECT count(*) AS NbAcces 
					 	  FROM $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes
        			WHERE $Tjpk_groupes_acces.id_grpacces = $Tjpk_groupes_auteurs.id_grpacces 
							AND $Tjpk_groupes_auteurs.id_grpacces = $Tjpk_groupes.id_grpacces 
              AND $Tjpk_groupes_auteurs.id_auteur = $aut  
              AND $Tjpk_groupes_auteurs.dde_acces = 0 
              AND $Tjpk_groupes_acces.id_rubrique = $rub 
              AND $Tjpk_groupes.actif = 1";
      $result = spip_query($sql);
      if ($row = spip_fetch_array($result)){
         $acces = $row['NbAcces'];
      }
      if ($acces > 0){
         return TRUE; // acc�s autoris�
      }
			else {
         // si pas d'acc�s direct pour l'auteur => test si les groupes auxquels il appartient ont un droit d'acces
  				 $sql201 = "SELECT id_grpacces FROM $Tjpk_groupes_auteurs WHERE id_auteur = $aut AND dde_acces = 0";
  				 $result201 = spip_query($sql201);
  				 while ($row = spip_fetch_array($result201)) {
  							 if (ssGrpAcces($row['id_grpacces'], $rub) == TRUE) {
								 		return TRUE;
								 }
  				 }
  			// si pas d'acc�s direct ou par groupes => test des groupes dans lesquels l'utilisateur est inclu par son statut
					 $sql202 = "SELECT statut FROM $Tspip_auteurs WHERE id_auteur = $aut LIMIT 1";
           $result202 = spip_query($sql202);
           if ($result202){
              if ($row = spip_fetch_array($result202)){
    					   $sp_statut = $row['statut'];
              }
    			 }
					 $sql204 = "SELECT id_grpacces FROM $Tjpk_groupes_auteurs WHERE sp_statut = '$sp_statut'";					 
  				 $result204 = spip_query($sql204);
  				 while ($row = spip_fetch_array($result204)) {
  							 if (ssGrpAcces($row['id_grpacces'], $rub) == TRUE) {
  									  return TRUE;
  								 }
  				 }
 					 return FALSE;
      }
}

// test des acc�s par ss-groupe, r�cursivement dans toute l'ascendance du groupe test�
function ssGrpAcces($id_grpe, $rub) {
      	 global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;

         // si pas d'acc�s direct pour l'auteur => test si les groupes auxquels il appartient sont exon�r�s par prive_public
				 $sql202 = "SELECT COUNT(*) AS NbAcces FROM $Tjpk_groupes_acces WHERE id_grpacces = $id_grpe AND id_rubrique = $rub";
				 $result202 = spip_query($sql202);
				 if ($row = spip_fetch_array($result202)) {
				 		if ($row['NbAcces'] > 0) {
							 return TRUE;
						}
  				  else {
  				 		// test des groupes de l'ascendance du groupe test�
								 $sql203 = "SELECT id_grpacces FROM $Tjpk_groupes_auteurs WHERE id_ss_groupe = $id_grpe";
  							 $result203 = spip_query($sql203);
  							 while ($row = spip_fetch_array($result203)) {
  										 $id_at = $row['id_grpacces'];
  										 if (ssGrpAcces($id_at, $rub) == TRUE) {
  											  return TRUE;
  										 }
  							 }
  				  }
				 }
				 return FALSE;
}

// d�termine si une rubrique � acc�s restreint est contr�l�e par (au moins) un groupe autorisant les demandes d'acc�s
function existe_demande_acces($rub) {
				 global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
		// d�terminer si c'est la rubrique en cours qui est restreinte
		 		 $sql333 = "SELECT COUNT(*) as nb_rub FROM $Tjpk_groupes_acces WHERE id_rubrique = $rub";
				 $result333 = spip_query($sql333);
				 $row333 = spip_fetch_array($result333);
				 $existe_rub = $row333['nb_rub'];
		// si c'est la rubrique en cours qui est contr�l�e, tester si le groupe autorise les demandes d'acc�s
				 if ($existe_rub > 0) {
    				 $sql303 = "SELECT COUNT(*) AS nb_demande_acces
    				 				 	  FROM $Tjpk_groupes_acces, $Tjpk_groupes
    				 				 	  WHERE $Tjpk_groupes_acces.id_grpacces = $Tjpk_groupes.id_grpacces
    										AND $Tjpk_groupes_acces.id_rubrique = $rub
    										AND $Tjpk_groupes.demande_acces = 1";
    				 $result303 = spip_query($sql303);
    				 $rows303 = spip_fetch_array($result303);
    				 if ($rows303['nb_demande_acces'] > 0) {
    				 		return TRUE;
    				 }
    				 else {
									return FALSE;
    				 }
    		 }
		// sinon tester si c'est son parent qui est la rubrique restreinte (r�cursivement)
				 else {
				 			$sql374 = "SELECT id_parent FROM $Tspip_rubriques WHERE id_rubrique = $rub LIMIT 1";
							$result374 = spip_query($sql374);
							$row374 = spip_fetch_array($result374);
							$id_parent = $row374['id_parent'];
							if (existe_demande_acces($id_parent) == TRUE) {
								 return TRUE;
							}
				 }
}

// trouve la rubrique restreinte dans l'ascendance d'une rubrique
function trouve_parent_restreint($rub, $prive_public, $retour = '') {
				 global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
				 while ($rub != 0 AND $retour == '') {
							 if (RubPrive($rub, $prive_public)) {
							 		$retour = $rub;
							 }
							 else {
      				 			$sql374 = "SELECT id_parent FROM $Tspip_rubriques WHERE id_rubrique = $rub LIMIT 1";
      							$result374 = spip_query($sql374);
      							$row374 = spip_fetch_array($result374);
      							$id_parent = $row374['id_parent'];
							 			$rub = $id_parent;
							 }
				 }
				 return $retour;
}

// FIN - Acces groupes


?>