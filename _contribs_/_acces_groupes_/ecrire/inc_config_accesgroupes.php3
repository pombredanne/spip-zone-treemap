<?php
// fichier de config pour utiliser un pr�fixe pour les tables SPIP et/ou jpk_xxx pour la gestion des groupes

// pour ne pas avoir � configurer ce fichier on utilise le inc_version.php3 pour assurer la connexion � la base de donn�es
	 include_once ("inc_version.php3");


		$prefix_tables_SPIP = $table_prefix;	 // $table_prefix d�finie dans ecrire/inc_version.php3 (qui appelle mes_options.php3 s'il existe)
		$prefix_tables_jpk = $table_prefix."_accesgroupes";		 // pour la version originale : $prefix_tables_jpk = "jpk"


		$Tspip_rubriques = $prefix_tables_SPIP."_rubriques";
		$Tspip_articles = $prefix_tables_SPIP."_articles";
		$Tspip_breves = $prefix_tables_SPIP."_breves";
		$Tspip_auteurs = $prefix_tables_SPIP."_auteurs";
		$Tspip_auteurs_rubriques = $prefix_tables_SPIP."_auteurs_rubriques";
		
		$Tjpk_groupes = $prefix_tables_jpk."_groupes";
		$Tjpk_groupes_auteurs = $prefix_tables_jpk."_auteurs";
		$Tjpk_groupes_acces = $prefix_tables_jpk."_acces";
		
		$Tspip_messages = $prefix_tables_SPIP."_messages";
		$Tspip_auteurs_messages = $prefix_tables_SPIP."_auteurs_messages";
		
		
?>