<?php
/*
  Gestion des acces auw rubriques et articles par groupes d'utilisateurs
  Contrib de Coyote, alias JPK ou Jean-Pierre KUNTZ
  V0.1 - 16 juillet 2005
       version initiale avec gestion multilingue
  v0.2 - 02 ao�t 2005
       maj pour compatibilit� avec MySQL 3.23
       la table jpk_auteurs_groupes devient jpk_groupes_auteurs
       correction affichage des titres de rubriques typo()
  v0.3 - 05 ao�t 2005
       ajout d'un bouton pour suppression d'un groupe
       transforme tous les mysql_query() en spip_query()
       Correction du test d�acc�s, utilisation du login au lieu de l�id_auteur
	v0.7 - juillet 2006 cy_altern 
			 restrictions s�par�es possibles pour public/priv� 
			 + correction bogue suppression/modification 
			 + message demande d'inscription au groupe
			 + passage en BOUCLE_xx (ACCESGROUPES) du contr�le d'acc�s dans les squelettes
*/

$msg_text = '';
$alerte = 0;

// modifs clem pour utilisation d'un pr�fixe au noms de tables SPIP et/ou _jpk
include_once("inc_config_accesgroupes.php3");

// DETECTION DE L'EXTENSION (.php3 OU .php)
    $ext = "php";
    if (substr($_SERVER['PHP_SELF'],-1) == 3)  $ext = "php3";
    include ("inc.$ext"); 

/*     INITIALISATION DES TABLES DE JPK_GROUPES
*   d�finition et test existence des tables jpk_ 
*   + cr�ation de la table si pas encore install�e. 
*   PAS de DROP donc pas de risque de RAZ des contenus existants
*/

// existe t'il des tables version 0.6 ?
		$sql6880 = "SHOW TABLES LIKE '".$prefix_tables_SPIP."_jpk_groupes'";
		@$result6880 = spip_query($sql6880);
// si pas de tables v0.6, cr�er les tables
		if (! @$data6880 =  spip_num_rows($result6880) AND spip_num_rows($result6880) < 1) {
    		$sql6881 = "SHOW TABLES LIKE '$Tjpk_groupes'";
    		@$result6881 = spip_query($sql6881);
        if (! @$data6881 =  spip_num_rows($result6881) AND spip_num_rows($result6881) < 1) {
    // cr�ation de la table si elle n'existe pas
    			 $sql_create1 = "CREATE TABLE $Tjpk_groupes (
                                    id_grpacces bigint(20) NOT NULL auto_increment,
                                    nom varchar(30) NOT NULL default '',
                                    description varchar(250) default NULL,
                                    actif smallint(1) NOT NULL default '0',
                                    proprio bigint(21) NOT NULL default '0',
    																demande_acces tinyint(4) NOT NULL default '0',
                            PRIMARY KEY  (id_grpacces),
                            UNIQUE KEY nom (nom) )";
    			 @spip_query($sql_create1);
    			 if (mysql_error() != '') {
    			 			$msg_text .= "<br />".mysql_error()." "._T('accesgroupes:creation_table')." ".$Tjpk_groupes."\r\n";
    						$alerte = 1;
    			 }
    			 else {
    					 $msg_text .= "<br />"._T('accesgroupes:creation_table')." ".$Tjpk_groupes."\r\n";
    			 }
    		}
    		
    		$sql6882 = "SHOW TABLES LIKE '$Tjpk_groupes_auteurs'";
    		$result6882 = spip_query($sql6882);
        if (! @$data6882 =  spip_num_rows($result6882) AND spip_num_rows($result6882) < 1) {
    // cr�ation de la table si elle n'existe pas
    			 $sql_create2 = "CREATE TABLE $Tjpk_groupes_auteurs (
                                id_grpacces bigint(21) NOT NULL default '0',
                                id_auteur bigint(21) NOT NULL default '0',
                                id_ss_groupe bigint(21) NOT NULL default '0',
                                sp_statut varchar(255) NOT NULL default '',
                                dde_acces smallint(1) NOT NULL default '1',
                                proprio bigint(21) NOT NULL default '0',
                             UNIQUE KEY id_grp (id_grpacces,id_auteur,id_ss_groupe,sp_statut) )";
    			 @spip_query($sql_create2);
    			 if (mysql_error() != '') {
    					 $msg_text .= "<br />".mysql_error()."  "._T('accesgroupes:creation_table')." ".$Tjpk_groupes_auteurs."\r\n";
    					 $alerte = 1;
    			 }
    			 else {
    					 $msg_text .= "<br />"._T('accesgroupes:creation_table')." ".$Tjpk_groupes_auteurs."\r\n";
    			 }
    		}
    		
    		
    		$sql6883 = "SHOW TABLES LIKE '$Tjpk_groupes_acces'";
    		$result6883 = spip_query($sql6883);
        if (! @$data6883 =  spip_num_rows($result6883) AND spip_num_rows($result6883) < 1) {
    // cr�ation de la table si elle n'existe pas
    			 $sql_create3 ="CREATE TABLE $Tjpk_groupes_acces (
                                id_grpacces bigint(21) NOT NULL default '0',
                                id_rubrique bigint(21) NOT NULL default '0',
                                id_article bigint(21) default NULL,
                                dtdb date default NULL,
                                dtfn date default NULL,
                                proprio bigint(21) NOT NULL default '0',
    														prive_public SMALLINT(6) NOT NULL default '0',
    														
                              KEY id_grpacces (id_grpacces),
                              KEY id_rubrique (id_rubrique),
                              KEY id_article (id_article) )";
    			@spip_query($sql_create3);
    			if (mysql_error() != '' ) {
    			 			$msg_text .= "<br />".mysql_error()." "._T('accesgroupes:creation_table')." ".$Tjpk_groupes_acces."\r\n";
    						$alerte = 1;
    			 }
    			 else {
    					 $msg_text .= "<br />"._T('accesgroupes:creation_table')." ".$Tjpk_groupes_acces."\r\n";
    			 }
    		}
		}
// si il existe des tables v0.6, lancement du patch pour passer de v0.61	� v0.7
		else {
    // �tape 1 renommer les tables prefixspip_jpk_xxx en prefixspip_accesgroupe_groupes, prefixspip_accesgroupe_acces, prefixspip_accesgroupe_auteurs  	       
    		 spip_query("ALTER TABLE ".$prefix_tables_SPIP."_jpk_groupes RENAME ".$Tjpk_groupes);
    		 spip_query("ALTER TABLE ".$prefix_tables_SPIP."_jpk_groupes_acces RENAME ".$Tjpk_groupes_acces);
    		 spip_query("ALTER TABLE ".$prefix_tables_SPIP."_jpk_groupes_auteurs RENAME ".$Tjpk_groupes_auteurs);		
    		 if (mysql_error() != '') {
    		 		$alerte = 1;
    				$msg_text .= _T('accesgroupes:erreur_patch0.7_etape1');
    		 }
    
    // �tape 2 : ajout des champs suppl�mentaires
      	$sql701 = "SHOW COLUMNS FROM $Tjpk_groupes_acces";
    		$result701 = spip_query($sql701);
        $col_names = array();
      	if ($sql701) {
         		while ($row701 = spip_fetch_array($result701)) {
         					$col_names[]=$row701[0];
         		}
      	}
        if (!in_array('prive_public', $col_names)) {
      		  spip_query("ALTER TABLE $Tjpk_groupes_acces ADD prive_public smallint(6) NOT NULL");
      		  if (mysql_error() != '') {  
      				 $alerte = 1;
    					 $msg_text .= _T('accesgroupes:erreur_patch0.7_etape2');
      	 		}
      	}
    		
      	$sql702 = "SHOW COLUMNS FROM $Tjpk_groupes";
    		$result702 = spip_query($sql702);
        $col_names = array();
      	if ($sql702) {
         		while ($row702 = spip_fetch_array($result702)) {
         					$col_names[]=$row702[0];
         		}
      	}
        if (!in_array('demande_acces', $col_names)) {
      		  spip_query("ALTER TABLE $Tjpk_groupes ADD demande_acces tinyint(4) NOT NULL default '0'");
      		  if (mysql_error() != '') {  
      				 $alerte = 1;
    					 $msg_text .= _T('accesgroupes:erreur_patch0.7_etape2');
      	 		}
      	}
    		
    // �tape 3 transformation des champs intitul�s id_groupe en id_grpacces
     	  if (!in_array('id_grpacces', $col_names)) {
     	      spip_query("ALTER TABLE $Tjpk_groupes CHANGE id_groupe id_grpacces BIGINT( 20 ) NOT NULL AUTO_INCREMENT ");
     	    	spip_query("ALTER TABLE $Tjpk_groupes_acces CHANGE id_groupe id_grpacces BIGINT( 21 ) DEFAULT '0' NOT NULL ");
     	    	spip_query("ALTER TABLE $Tjpk_groupes_auteurs CHANGE id_groupe id_grpacces BIGINT( 21 ) DEFAULT '0' NOT NULL ");   
     	    	if (mysql_error() != '') {
     	         $alerte = 1;
     	         $msg_text .= '<br>'._T('accesgroupes:erreur_patch0.7_etape3');
    					 $msg_text .= mysql_error();
     	    	}
     	  }
				if ($msg_txt == '') {
					 $msg_text .= _T('accesgroupes:OK_patch0.7');
				}
    // fin patch
	  }	  

		if ($msg_text != '') {
			 $msg_text = _T('accesgroupes:installation').($alerte != 1 ? _T('accesgroupes:install_ok') : _T('accesgroupes:install_pas_ok') ).'<br />'.$msg_text;
		}
		
// FIN INITIALISATION TABLES JPK_GROUPES
		
		
/*  LES FONCTIONS   */

// affichage de la liste des rubriques disponibles pour l'utilisateur 
//      rubriques priv�es en rouge + vert + jaune + bleu
$Trub_grpe_ec_parent = array();   // ce tableau sera utilis� pour la limitation des possibilit�s de type d'acc�s pour les rubriques d�ja restreintes par le groupe en cours (prive+public/prive/public) => cf tableau en dessous du select des rubriques � restreindre
function enfant($leparent, $prive_public_parent = 10){
				global $Trub_grpe_ec_parent;
				global $groupe;
        global $connect_toutes_rubriques;
        global $i;
        global $couleur_claire, $spip_lang_left;
        global $browser_name, $browser_version;
    		global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
				global $prive_public_ec;

        $i++;
        $prive_public_ec = $prive_public_parent;
				
				$query="SELECT * FROM $Tspip_rubriques WHERE id_parent = '$leparent' ORDER BY 0+titre,titre";
        $result=spip_query($query);
        while($row = spip_fetch_array($result)){
              $my_rubrique = $row['id_rubrique'];
              $titre = typo($row['titre']);
              $statut_rubrique = $row['statut'];
              $lang_rub = $row['lang'];
              $langue_choisie_rub = $row['langue_choisie'];
              $style = "";
              $espace = "";
              $prive = $row['prive'];
      
              if (eregi("mozilla", $browser_name)) {
                  $style .= "padding-$spip_lang_left: ".($i*16)."px;";
              } 
    					else {
                    for ($count = 0; $count <= $i; $count ++) {
  											$espace .= "&nbsp;&nbsp;&nbsp;&nbsp;";
  									}
        			}
              if ($i == 1) {
                  $style .= "background: $couleur_claire url("._DIR_IMG_PACK."secteur-12.gif) no-repeat ".(($i - 1) * 16)."px 0px; ";
              }
							else {
							 		 $style .= "background: $couleur_claire url("._DIR_IMG_PACK."rubrique-12.gif) no-repeat ".(($i * 16) - 12)."px 2px; ";
							}
           // affiche en rouge/vert/jaune/bleu les rubriques priv�es : prive_public = 0 => prive+public | 1 => prive | 2 => public 
							$prive_public_ec = trouve_prive_public($my_rubrique, $groupe, $prive_public_ec);
    					switch ($prive_public_ec) {
    								 case 0 :		// priv� + public
    								 			$style .= "color: #f00;";
    								 break;
    								 case 1 :		// priv� seul
    								 			$style .= "color: #093;";
    								 break;
    								 case 2 :		// public seul
    								 			$style .= "color: #f90;";
    								 break;
    								 case 3 :		// autres groupes : priv�+public 
    								 			$style .= "color: #00f;";
    								 break;
										 case 4 :		// autres groupes :  priv� 
    								 			$style .= "color: #00f;";
    								 break;
										 case 5 :		// autres groupes : public 
    								 			$style .= "color: #00f;";
    								 break;
    								 default :	// rubrique sans restriction
    								 			$style .= "color: #000;";
    								 break;
    					}
              if ($prive_public_ec <= 5) {
						 		 $style .= "font-weight:bold; ";
					    }
              if ($statut_rubrique != 'publie') {
  							 $titre = "($titre , non publi&eacute;e)";
  						}
              if (lire_meta('multi_rubriques') == 'oui' AND $langue_choisie_rub == "oui") {
  							 $titre = $titre." [".traduire_nom_langue($lang_rub)."]";
  						}
              $selec_rub = "selec_rub";
              if ($browser_name == "MSIE" AND floor($browser_version) == "5") {
  							 $selec_rub = ""; // Bug de MSIE MacOs 9.0
  						}
              if (acces_rubrique($my_rubrique)) {
                  echo "<option".mySel($my_rubrique,$id_parent)." class='$selec_rub' style=\"$style\">$espace".supprimer_tags($titre);
//echo " i = $i my_rubrique = $my_rubrique prive_public_ec = $prive_public_ec prive_public_parent = $prive_public_parent";
									if ($prive_public_ec >= 3 AND $prive_public_ec <= 5) {
										 echo " (";
										 echo ($prive_public_ec == 3 ? _T('accesgroupes:prive_public') : ($prive_public_ec == 4 ? _T('accesgroupes:prive_seul') : _T('accesgroupes:public_seul') ));
										 echo ")";
									}
									echo "</option>\n";  
									$prive_public_parent >= 3 ? $prive_public_parent_ec = $prive_public_parent - 3 : $prive_public_parent_ec = $prive_public_parent;
									if ($prive_public_ec < 3) {
  									 echo "<script language=\"JavaScript\" type=\"text/javascript\">
  										  	    Tacces_rub.push([\"$my_rubrique\", \"$prive_public_ec\", \"$prive_public_parent\"]);</script>";
							// remplissage du tableau des rubriques restreintes par le groupe en cours : permet d'avoir le prive_public_parent de chaque rubrique restreinte par le groupe en cours
								     $Trub_grpe_ec_parent[$my_rubrique] = $prive_public_parent; 
  								}
									elseif ($prive_public_ec <=5 AND $prive_public_ec > 2) {
												 echo "<script language=\"JavaScript\" type=\"text/javascript\">
  										  	    Tacces_rub.push([\"$my_rubrique\", \"".($prive_public_ec - 3)."\", \"$prive_public_parent_ec\"]);</script>";
									}
  
              }
              enfant($my_rubrique, $prive_public_ec);
        }
        $i = $i - 1;
				$i == 1 ? $prive_public_ec = 10 : $prive_public_ec = $prive_public_parent;
}

//  fct pour renvoyer le code num�rique du type d'acc�s � une rubrique restreinte
//	si la rubrique est contr�l�e par le groupe $id_grpacces retourne : 0 => prive + public, 1 => priv�, 2 => public
//	si la  rubrique est contr�l�e par un autre groupe retourne : 3 =>  prive + public, 4 => priv�, 5 => public
function trouve_prive_public($id_rub, $id_grpacces, $prive_public_parent = 10) {
				global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
				$prive_public_rub = $prive_public_parent;  // prive_public = 10 => ind�termin�
        $sql2 = "SELECT prive_public, id_rubrique, id_grpacces, proprio FROM $Tjpk_groupes_acces WHERE id_rubrique = $id_rub";
        $result2 = spip_query($sql2);
    		while ($row2 = spip_fetch_array($result2)){
    					$id_rubrique_ec = $row2['id_rubrique'];
    					$id_groupe_ec = $row2['id_grpacces'];
    					$prive_public_ec = $row2['prive_public'];
							if ($id_groupe_ec != $id_grpacces) {
								 $prive_public_ec += 3;
							}
							if ($prive_public_ec < $prive_public_rub) {
								 $prive_public_rub = $prive_public_ec;
							}
    	  }
    		return $prive_public_rub;
}	 
//print '<br>trouve_prive_public(27, 1) = '.trouve_prive_public(27, 1);


// les fonctions n�cessaires pour la gestions des ss-groupes et statuts inclus dans les groupes

// fct de v�rification que le groupe_pere n'est pas inclu dans le groupe_fils ou l'un de ses ascendants (r�cursivement)
// renvoie FALSE si groupe_pere ou l'un de ses ascendants contient groupe_fils 
function verifie_inclusions_groupe($id_groupe_fils, $id_groupe_pere) {
				 global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
				 
				 $sql111 = "SELECT COUNT(*) AS inclusions FROM $Tjpk_groupes_auteurs WHERE id_grpacces = $id_groupe_fils AND id_ss_groupe = $id_groupe_pere LIMIT 1";
				 $result111 = spip_query($sql111);
				 $row = spip_fetch_array($result111);
				 if ($row['inclusions'] > 0){
				  	 return FALSE;
				 }
			// le p�re n'est pas inclu dans le fils => on teste toute l'ascendance du fils par r�currence avant d'�tre OK
				 else {
    				 $sql110 = "SELECT id_grpacces FROM $Tjpk_groupes_auteurs WHERE id_ss_groupe = $id_groupe_pere";
    				 $result110 = spip_query($sql110);
    				 while ($row = spip_fetch_array($result110)) {
    				 			 $id_groupe_ec = $row["id_groupe"];
									 if (verifie_inclusions_groupe($id_groupe_fils, $id_groupe_ec) == FALSE) {
									 		return FALSE;
									 }
    				 }
				 }				 
			// pas d'inclusion d'un parent donc OK
				 return TRUE; 				 
}

// fct pour construire le tableau de la descendance d'un groupe (r�cursivement)
function descendance_groupe($groupe_pere, $Tdescendance = array()) {
				 global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
  			 $sql121 = "SELECT $Tjpk_groupes_auteurs.id_ss_groupe, $Tjpk_groupes.nom 
				 				 	  FROM $Tjpk_groupes_auteurs, $Tjpk_groupes
										WHERE $Tjpk_groupes_auteurs.id_grpacces = $groupe_pere 
										AND $Tjpk_groupes_auteurs.id_ss_groupe = $Tjpk_groupes.id_grpacces
										AND $Tjpk_groupes.actif = 1 
										AND $Tjpk_groupes_auteurs.id_ss_groupe != 0
										ORDER BY $Tjpk_groupes.nom";
  			 $result121 = spip_query($sql121);
				 while ($row = spip_fetch_array($result121)) {
				 			 $id_descendant_ec = $row["id_ss_groupe"];
							 $nom_descendant_ec = $row["nom"];
							 $Tdescendance[$groupe_pere][] = array('id' => $id_descendant_ec, 'nom' => $nom_descendant_ec);
							 $Tdescendance = descendance_groupe($id_descendant_ec, $Tdescendance);
				 }
				 return $Tdescendance;
}

// fct pour afficher le tableau $Tdesc de la descendance du groupe $id_grpe (r�cursivement)
function affiche_descendance($id_grpe, $Tdesc, $a_afficher = "") {
				 global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
				 if (is_array($Tdesc[$id_grpe]) AND count($Tdesc[$id_grpe]) > 0) {
				 		$a_afficher .= "<div style='margin-left: 10px; font-size: 10px; padding: 2px;'>";
						foreach ($Tdesc[$id_grpe] as $a => $Td) {
									 $a_afficher .= "<img src='img_pack/sous-groupe.png' alt='|_' style='vertical-align:top;'>";
        					 $a_afficher .= "<img src='img_pack/groupe-12.png' alt='|_' style='vertical-align:top;'> <a href=\"$PHP_SELF?groupe=".$Td['id']."\">".$Td['nom']."</a><br />";
									 $a_afficher = affiche_descendance($Td['id'], $Tdesc, $a_afficher);
    			  }
				 		$a_afficher .= "</div>";
				 }
				 return $a_afficher;
}

// fct pour cr�er le tableau des rubriques g�r�es par les groupes
function affiche_groupes_rubriques() {
				 global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
         $a_afficher =  "<table CELLPADDING=2 CELLSPACING=0 WIDTH='100%' class='arial2' style='border: 1px solid #aaaaaa;'>\n";
         $a_afficher .= "<tr><th colspan=\"2\">"._T('accesgroupes:groupes_rubriques')."</th></tr>";
      	 $sql301 = "SELECT nom, id_grpacces FROM $Tjpk_groupes GROUP BY id_grpacces";
				 $result301 = spip_query($sql301);
      	 while ($row301 = spip_fetch_array($result301)) {
      	 			 $id_grpe_ec = $row301['id_grpacces'];
      				 $nom_grpe_ec = $row301['nom'];
               $a_afficher .= "<tr style='background-color: #eeeeee;'>";
               $a_afficher .= "<td class='verdana11' style='border-top: 1px solid #cccccc; width: 14px; vertical-align:top;'>";
      				 if (est_admin_restreint() == TRUE AND est_proprio($id_grpe_ec) == TRUE) {
							 		$a_afficher .= "<img src='img_pack/admin-12.gif' alt='|_' style='vertical-align:top;'>";
							 }
							 $a_afficher .= "<img src='img_pack/groupe-12.png' alt='|_' style='vertical-align:top;'></td>";
      				 $a_afficher .= "<td style='border-top: 1px solid #cccccc;'><a href=\"$PHP_SELF?groupe=".$id_grpe_ec."\">".$nom_grpe_ec."</a><br />";
      				 $a_afficher .= "<div style='margin-left: 20px; font-size: 10px; padding: 2px;'>";
      				 $sql302 = "SELECT $Tspip_rubriques.titre, $Tspip_rubriques.id_rubrique
      				 				 	  FROM $Tspip_rubriques, $Tjpk_groupes_acces
      										WHERE $Tjpk_groupes_acces.id_grpacces = $id_grpe_ec
      										AND $Tjpk_groupes_acces.id_rubrique = $Tspip_rubriques.id_rubrique";
      				 if ($result302 = spip_query($sql302)) {
      				 		while ($row302 = spip_fetch_array($result302)) {
      				 					$id_rub_ec = $row302['id_rubrique'];
      									$nom_rub_ec = $row302['titre'];
      									$a_afficher .= "<img src='img_pack/sous-groupe.png' alt='|_' style='vertical-align:top;'>";
      									$a_afficher .= " <a href=\"naviguer.php3?id_rubrique=".$id_rub_ec."\"><img src='img_pack/rubrique-12.gif' alt='|_' style='vertical-align:top; border: 0px;'>".$nom_rub_ec."</a><br />";
      				 		}
      				 }
      				 $a_afficher .= "</div>\r\n</td>";
               $a_afficher .= "</tr>";
      	 }
         $a_afficher .= "</table>";
				 return $a_afficher;
}

// fct pour cr�er et retourner l'array contenant les rubriques d'un admin restreint
function cree_Trub_admin () {
				  global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
          
					$id_utilisateur = trouve_id_utilisateur();
					$sql501= "SELECT  id_rubrique
									 	FROM $Tspip_auteurs_rubriques
										WHERE id_auteur = '$id_utilisateur'";
					$result501 = spip_query($sql501);
					$Trubriques_autorises = array();
					while ($row501 = spip_fetch_array($result501)) {
								$Trubriques_autorises[] = $row501['id_rubrique'];
					}
					return $Trubriques_autorises;
}

// fct pour trouver l'id de l'utilisateur en cours
function trouve_id_utilisateur() {
				  global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
					
				  $login_utilisateur = $GLOBALS['auteur_session']['login'];

					$sql502 = "SELECT id_auteur FROM $Tspip_auteurs WHERE login = '$login_utilisateur' LIMIT 1";
					$result502 = spip_query($sql502);
					$row502 = spip_fetch_array($result502);
					$id_utilisateur = $row502['id_auteur'];
					return $id_utilisateur;				 
}

// fct pour d�terminer si l'utilisateur en cours est admin restreint
function est_admin_restreint() {
				 $Trub_restreint = cree_Trub_admin();
				 if (count($Trub_restreint) > 0) {
				 		return TRUE;
				 }
				 else {
				 			return $Trub_restreint;
				 }
}

// fct pour d�terminer si l'utilisateur est proprio du groupe en cours (donc peut le modifier)
function est_proprio($id_groupe) {
				 global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
				 $sql505 = "SELECT proprio FROM $Tjpk_groupes WHERE id_grpacces = $id_groupe LIMIT 1";
				 $result505 = spip_query($sql505);
				 $row505 = spip_fetch_array($result505);
				 $id_proprio = $row505['proprio'];
				 if ($id_proprio == trouve_id_utilisateur()) {
				 		return TRUE;
				 }
				 else {
				 			return FALSE;
				 }
}

// fct pour d�terminer si l'utilisateur est proprio de l'acc�s sur une rubrique
function est_proprio_acces($id_rubrique) {
				 global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
				 $sql506 = "SELECT proprio FROM $Tjpk_groupes_acces WHERE id_rubrique = $id_rubrique LIMIT 1";
				 $result506 = spip_query($sql506);
				 $row506 = spip_fetch_array($result506);
				 $id_proprio_acces = $row506['proprio'];
				 if ($id_proprio_acces == trouve_id_utilisateur()) {
				 		return TRUE;
				 }
				 else {
				 			return FALSE;
				 }
}

// fct pour d�terminer le statut d'un utilisateur
function trouve_statut($id_util) {
				 global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
				 $sql506 = "SELECT statut FROM $Tspip_auteurs WHERE id_auteur = $id_util LIMIT 1";
				 $result506 = spip_query($sql506);
				 $row506 = spip_fetch_array($result506);
				 return $row506['statut'];
}

// fct pour traiter les noms de groupes en doublons retourne FALSE si le nom existe d�ja
function verifie_duplicata_groupes($nom_at) {
				 global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
				 $sql507 = "SELECT nom FROM $Tjpk_groupes";
				 if ($result507 = spip_query($sql507)) {
    				 while ($row507 = spip_fetch_array($result507)) {
    				 			 if ($row507['nom'] == $nom_at) {
    							 		return FALSE;
    							 }
    				 }
				 }
				 return TRUE;
}

function debug(){
         if(mysql_errno() > 0){
           echo mysql_errno().": ".mysql_error();
         }
}

function rub_reinit(){
    		global $Tspip_rubriques, $Tspip_auteurs, $Tspip_auteurs_rubriques, $Tjpk_groupes_acces, $Tjpk_groupes_auteurs, $Tjpk_groupes;
   // lib�re les rubriques qui n'ont plus de restriction
   // ok avec mysql 3.23
        $sql = "SELECT $Tspip_rubriques.* 
						    FROM $Tspip_rubriques
        				LEFT JOIN $Tjpk_groupes_acces
        				ON $Tspip_rubriques.id_rubrique = $Tjpk_groupes_acces.id_rubrique 
								AND $Tspip_rubriques.prive = 1
        				WHERE $Tjpk_groupes_acces.id_rubrique IS NULL";
        $result=spip_query($sql);
        debug($result);
        while ($row = spip_fetch_array($result)){
              $sql1 = "UPDATE $Tspip_rubriques SET prive=0 WHERE id_rubrique = \"".$row['id_rubrique']."\"";
              $result1=spip_query($sql1);
              debug($result1);
        }
}

    
// DEBUT DE TRAITEMENT des DONNEES RENVOYEES PAR LES FORMULAIRES : AJOUT - MODIFICATION - SUPPRESSION

isset($_GET['groupe'])? $groupe = $_GET['groupe'] : $groupe = 0;
//isset($_POST['groupe'])? $groupe = $_POST['groupe'] : $groupe = $_GET['groupe'];
isset($_POST['groupe'])? $groupe = $_POST['groupe'] : (isset($_GET['groupe'])? $groupe = $_GET['groupe'] : $groupe = 0);

// modif clem pour traiter les admins restreints 
$id_util_restreint = 0;
if (est_admin_restreint() == TRUE) {
	 $id_util_restreint = trouve_id_utilisateur();
	 $Trub_restreint =  cree_Trub_admin ();
}	 

//$msg_text .= 'est_admin_restreint = '.est_admin_restreint().' $Trub_restreint = '.print_r($Trub_restreint).'<br>$id_util_restreint ='.$id_util_restreint.' trouve_id_utilisateur() = '.trouve_id_utilisateur();
//$msg_text .= '<br>$_REQUEST = '.print_r($_REQUEST);
//$msg_text . = '<br>$GLOBALS[auteur_session]= '.$GLOBALS['auteur_session'];

// d�sactive toutes les fcts de modifs du groupe si admin restreint + pas proprio !!! ATTENTION ce if est extra long !!!
if ($id_util_restreint == 0 OR ($id_util_restreint != 0 AND est_proprio($groupe) == TRUE)) {
        
    // AUTEUR ======================================================================
    // AUTEUR Ajout...
    if (isset($_POST['add_auteur'])){
    	$auteur = $_POST['auteur'];
    	$sql = "INSERT INTO $Tjpk_groupes_auteurs(id_grpacces,id_auteur, dde_acces, proprio) VALUES($groupe, $auteur, 0, $id_util_restreint)";
      $result = spip_query($sql);
    }
    
    // AUTEUR Modif...
    if (isset($_GET['mod_auteur'])){
      $sql = "UPDATE $Tjpk_groupes_auteurs SET dde_acces = 0, proprio = $id_util_restreint WHERE id_grpacces = $groupe AND id_auteur = $mod_auteur";
      $result = spip_query($sql);
    }
    
    // AUTEUR Suppression...
    if (isset($_GET['del_auteur'])){
      $sql = "DELETE FROM $Tjpk_groupes_auteurs WHERE id_grpacces=$groupe AND id_auteur=$del_auteur";
      $result = spip_query($sql);
    }
    
    // SS-GROUPES ==================================================================
    // modif clem pour gestion des ss-groupes int�gr�s dans les groupes
    // SS-GROUPES Ajout...
    if (isset($_POST['add_ss_groupe'])){
      // v�rification que le sous-groupe � cr�er n'est pas dans l'ascendance du groupe en cours 
    	// "never trust user" : en principe ce cas n'est pas possible mais un hack du POST est si vite arriv�...
      	if (verifie_inclusions_groupe($_POST['add_ss_groupe'], $groupe) != FALSE) {
        	$sql = "INSERT INTO $Tjpk_groupes_auteurs(id_grpacces,id_ss_groupe,dde_acces, proprio) VALUES($groupe,{$_POST['ss_groupe']}, 0, $id_util_restreint)";
          $result = spip_query($sql);
      	}
    		else {
    				 echo _T('accesgroupes:erreur_inclusion_recurrente');
    		}
    }
    
    // SS-GROUPES Modif...
    if (isset($_GET['mod_ss_groupe'])){
      $sql = "UPDATE $Tjpk_groupes_auteurs SET dde_acces = 0, proprio = $id_util_restreint WHERE id_grpacces = $groupe AND id_ss_groupe = {$_GET['mod_ss_groupe']}";
      $result = spip_query($sql);
    }
    
    // SS-GROUPES Suppression...
    if (isset($_GET['del_ss_groupe'])){
      $sql = "DELETE FROM $Tjpk_groupes_auteurs WHERE id_grpacces=$groupe AND id_ss_groupe = {$_GET['del_ss_groupe']}";
      $result = spip_query($sql);
    }
    
    // gestion des statuts int�gr�s dans les groupes
    // STATUTS Ajout...
    if (isset($_POST['add_statut'])){
        	$sp_statut = $_POST['sp_statut'];
    			$sql = "INSERT INTO $Tjpk_groupes_auteurs(id_grpacces,sp_statut,dde_acces, proprio) VALUES ($groupe,'$sp_statut',0, $id_util_restreint)";
          $result = spip_query($sql);
    }
    
    // STATUTS Modif...
    if (isset($_GET['mod_statut'])){
      $mod_statut = $_GET['mod_statut'];
    	$sql = "UPDATE $Tjpk_groupes_auteurs SET dde_acces = 0, proprio = $id_util_restreint WHERE id_grpacces = $groupe AND sp_statut = '$mod_statut'";
      $result = spip_query($sql);
    }
    
    // STATUTS Suppression...
    if (isset($_GET['del_statut'])){
      $del_statut = $_GET['del_statut'];
    	$sql = "DELETE FROM $Tjpk_groupes_auteurs WHERE id_grpacces = $groupe AND sp_statut = '$del_statut'";
      $result = spip_query($sql);
    }
    

    
    // GROUPE ======================================================================
    // GROUPE Modification
    if (isset($_POST['mod_groupe'])){
				$_POST['actif'] == 1 ? $actif = 1 : $actif = 0;
				$_POST['demandes_acces'] == 1 ? $demande_acces = 1 : $demande_acces = 0;
        $sql = "UPDATE $Tjpk_groupes
         		    SET nom='".$_POST['nom']."',
               			description = '".$_POST['description']."',
               			actif = $actif,
    					 			proprio = $id_util_restreint,
							 			demande_acces = $demande_acces
         			  WHERE id_grpacces=".$_POST['groupe'];
        $result = spip_query($sql);
        debug($result);
    }
} // fin du IF admin restreint + proprio
    // GROUPE Ajout (actif est mis � 1 syst�matiquement lors de la cr�ation d'un groupe)
    if (isset($_POST['add_groupe'])){
      // modif clem pour �viter les duplicatas de noms de groupes
				if (verifie_duplicata_groupes($_POST['nom']) == FALSE) {
					 $msg_text = '<h2 style="color: #f00;">'.$_POST['nom'].' : '._T('accesgroupes:duplicata_nom').'</h2>';
				}
				else {
              $sql = "INSERT INTO $Tjpk_groupes(nom, description, actif, proprio, demande_acces)
                   	 				 VALUES(\"".$_POST['nom']."\",\"".$_POST['description']."\", 1, $id_util_restreint, ".$_POST['demandes_acces'].")
              ";
              $result = spip_query($sql);
              debug($result);
              $groupe = mysql_insert_id();
				}
    }
// limitation admins restreints
if ($id_util_restreint == 0 OR ($id_util_restreint != 0 AND est_proprio($groupe) == TRUE)) {
    // GROUPE Suppression
    if ( (isset($_POST['del_groupe_all']) AND isset($_POST['groupe']) AND $_POST['groupe'] != '') OR (isset($_GET['del_groupe']) AND $_GET['del_groupe'] != '') ) {
        isset($_GET['del_groupe']) ? $id_grpe_asupr = $_GET['del_groupe'] : $id_grpe_asupr = $_POST['groupe'];
				$sql111 = "DELETE FROM $Tjpk_groupes_acces WHERE id_grpacces = ".$id_grpe_asupr;
        spip_query($sql111);
        if (mysql_error() != '') {
					 $alerte = 1;
					 $msg_text .= _T('accesgroupes:erreur_supr_rubriques').$id_grpe_asupr.' : '.mysql_error();
				}
				else {
              $sql112 = "DELETE FROM $Tjpk_groupes_auteurs WHERE id_grpacces = ".$id_grpe_asupr;
              spip_query($sql112);
              if (mysql_error() != '') {
  						 $alerte = 1;
							 $msg_text .= _T('accesgroupes:erreur_supr_auteurs').$id_grpe_asupr.' : '.mysql_error();
  					}
  					else {
                $sql113 = "DELETE FROM $Tjpk_groupes_auteurs WHERE id_ss_groupe = ".$id_grpe_asupr;
								spip_query($sql113);
								if (mysql_error() != '') {
									 $alerte = 1;
									 $msg_text .= _T('accesgroupes:erreur_supr_ssgrpes').$id_grpe_asupr.' : '.mysql_error();
								}
								else {
    								$sql114 = "DELETE FROM $Tjpk_groupes WHERE id_grpacces = ".$id_grpe_asupr;
                    spip_query($sql114);
										if (mysql_error() != '') {
											 $alerte = 1;
											 $msg_text .= _T('accesgroupes:erreur_supr_groupe').$id_grpe_asupr.' : '.mysql_error();
										}
										else {
												 $sql115 = "OPTIMIZE TABLE $Tjpk_groupes";
												 spip_query($sql115);
												 $sql116 = "OPTIMIZE TABLE $Tjpk_groupes_auteurs";
												 spip_query($sql116);
												 $sql117 = "OPTIMIZE TABLE $Tjpk_groupes_acces";
												 spip_query($sql117);
												 $msg_text .= _T('accesgroupes:supr_groupe_ok').$id_grpe_asupr;
										}
								}
  					}
				}
    }
}  // fin du IF admin restreint + proprio


// ACCES  ======================================================================
// ACCES AJOUT
if (isset($_POST['add_rub']) && $_POST['id_parent'] > 0){
  // si admin restreint, modif autoris�es si proprio de l'acc�s
    $acces_groupe_ec = $_POST['groupe'];
	  $acces_id_parent = $_POST['id_parent'];
	  $acces_prive_public = $_POST['prive_public'];
  if ($id_admin_restreint == 0 OR  ($id_admin_restreint !== 0 AND est_proprio_acces($id_parent) == TRUE)) {
        $sql_verif = "SELECT count(*) AS RubAcces
          				 	  FROM $Tjpk_groupes_acces
         							WHERE id_grpacces = \"".$acces_groupe_ec."\" 
											AND id_rubrique = \"".$acces_id_parent."\" ";
        $result = spip_query($sql_verif);
      // cr�ation nvl acc�s
				$nb_verif = spip_fetch_array($result);
				if ($nb_verif['RubAcces'] <= 0) {
  				 $sql601 = "INSERT INTO $Tjpk_groupes_acces(id_grpacces, id_rubrique, id_article, dtdb, dtfn, proprio, prive_public)
                   VALUES(\"".$acces_groupe_ec."\", \"".$acces_id_parent."\", \"\",now(),now(), $id_util_restreint, \"".$acces_prive_public."\")";
           $result601 = spip_query($sql601);
        }
				else {
						$sql604 = "UPDATE $Tjpk_groupes_acces 
						 				SET prive_public = $acces_prive_public
								 	  WHERE id_grpacces = $acces_groupe_ec 
										AND id_rubrique = $acces_id_parent
										LIMIT 1" ;
						$result604 = spip_query($sql604); 
				}
		}
}
// ACCES MODIFICATION
if (isset($_POST['modif_id_groupe']) AND isset($_POST['modif_id_rubrique'])){
  // si admin restreint, modif autoris�es si proprio de l'acc�s
//         	 $msg_text = "<h2>"._T('accesgroupes:acces_double')."</h2>";
   $modif_prive_public = $_POST['modif_prive_public'];
   $modif_id_rubrique = $_POST['modif_id_rubrique'];
   $modif_id_groupe = $_POST['modif_id_groupe'];
   if ($id_admin_restreint == 0 OR  ($id_admin_restreint !== 0 AND est_proprio_acces($id_parent) == TRUE)) {
  		 $sql602 = "UPDATE $Tjpk_groupes_acces 
  		 				SET prive_public = $modif_prive_public
  				 	  WHERE id_grpacces = $modif_id_groupe 
  						AND id_rubrique = $modif_id_rubrique
  						LIMIT 1" ;
  		 $result602 = spip_query($sql602);
	 }
}

if (isset($_GET['del_rub'])){
 // si admin restreint, efface autoris� si proprio de l'acc�s
  $id_parent_del = $_GET['del_rub'];
  if ($id_admin_restreint == 0 OR  ($id_admin_restreint !== 0 AND est_proprio_acces($del_rub) == TRUE)) {
      $sql = "DELETE FROM $Tjpk_groupes_acces WHERE id_grpacces = $groupe AND id_rubrique = \"".$id_parent_del."\"";
      $result = spip_query($sql);
      debug($result);
	}
}
//rub_reinit();





// DEBUT PAGE ==================================================================
debut_page(_T('accesgroupes:module_titre'));

// SECURITE 
if ($connect_statut != "0minirezo") {
    echo "\r\n<H3><FONT COLOR = red>"._T('avis_non_acces_page')."</FONT></H3>";
    fin_page();
    exit;
}

// test existence du groupe pour ne pas afficher la page admin s'il n'existe pas dans la base alors qu'il est envoy� par $_POST ou $_GET
if ($groupe != 0) {
	 $sql91 = "SELECT COUNT(*) AS verif_grpe FROM $Tjpk_groupes WHERE id_grpacces = $groupe LIMIT 1";
	 $result91 = spip_query($sql91);
	 if ($row91 = spip_fetch_array($result91)) {
	 		$row91['verif_grpe'] != 1 ? $groupe = 0 : $groupe = $groupe;
	 }
}



// =============================================================================
// GAUCHE
// =============================================================================
debut_gauche();
debut_boite_info();
			echo "<b>"._T('accesgroupes:module_titre')."</b><br />"._T('accesgroupes:module_info');
			if (isset($msg) AND $msg != '') {
				 echo $msg;
			}
fin_boite_info();


$sql = "SELECT * FROM $Tjpk_groupes";
$result = spip_query($sql);

debut_cadre_relief('groupe-24.png');
echo "\r\n<form action=\"$PHP_SELF\" name=\"frm_groupe\" method=\"post\">";
echo "\r\n<br />"._T('accesgroupes:select').": <select name=\"groupe\" size=\"1\" onchange='submit()';>";
echo "\r\n<option value=\"0\">"._T('accesgroupes:select_vide')."</option>";
while ($row = spip_fetch_array($result)){
		 echo "<option value=".$row['id_grpacces'].($groupe== $row['id_grpacces'] ? ' selected':'').">".typo($row['nom'])."</option>";
}
echo "</select>";

$sql_grp  = "SELECT * FROM $Tjpk_groupes WHERE id_grpacces=\"$groupe\"";
$result_grp = spip_query($sql_grp );
if ($row = spip_fetch_array($result_grp))
   $nom = $row['nom'];
   $desc = $row['description'];
   $actif = $row['actif'];
	 $prive_public = $row['prive_public'];
	 $demande_acces = $row['demande_acces'];

echo "\r\n<table style=\"width: 100%;\"><tr><td class='serif2'>";
echo bouton_block_invisible('groupeinfo')._T('accesgroupes:creer');
if ($groupe > 0 AND ($id_util_restreint == 0 OR ($id_util_restreint != 0 AND est_proprio($groupe) == TRUE)) ) {
	 echo '/'._T('accesgroupes:modifier')." : ";
}
// echo "$desc";
echo "\r\n</td></tr></table>\n";

echo debut_block_invisible('groupeinfo');
echo "\r\n<input type=\"hidden\" name=\"id_grpacces\" value=\"$groupe\" />";
echo "\r\n<table width=\"100%\">";
echo "\r\n<tr><td colspan=\"2\" style=\"font-weight: bold;\">"._T('accesgroupes:nom')." : </td></tr>";
echo "\r\n<tr><td colspan=\"2\"><input type=\"text\" name=\"nom\" value=\"$nom\" size=\"18\" />\r\n</td>\r\n</tr>";			
echo "\r\n<tr><td colspan=\"2\" style=\"padding-top: 7px; font-weight: bold;\">"._T('accesgroupes:description')." : </td></tr>";
echo "\r\n<tr><td colspan=\"2\"><textarea name=\"description\" rows=\"2\" cols=\"15\">$desc</textarea></td>\r\n</tr>";
echo "\r\n<tr><td  style=\"padding-top: 3px;\"><input type=\"submit\" name=\"add_groupe\" value=\""._T('accesgroupes:creer')."\" class='fondo' style='font-size:10px;' />";
echo "\r\n</td>\r\n</tr>";
echo "\r\n<tr><td colspan=\"2\" style=\"padding-top: 7px; font-weight: bold;\">"._T('accesgroupes:autoriser_demandes');
echo " <span style=\"font-size: 10px; font-weight: normal;\">"._T('accesgroupes:help_inscriptions')."</span>\r\n</td></tr>";
echo "\r\n<tr><td colspan=\"2\">";
echo _T('accesgroupes:oui')."<input name=\"demandes_acces\" value=\"1\" type=\"radio\" ".($demande_acces == 1 ? "checked=\"checked\"" : "")."\">";
echo "&nbsp;&nbsp;<input name=\"demandes_acces\" value=\"0\" type=\"radio\" ".($demande_acces == 0 ? "checked=\"checked\"" : "")."\">"._T('accesgroupes:non');
echo "\r\n</td>\r\n</tr>";

// admins restreints interdits si pas proprios			
if ($groupe > 0 AND ($id_util_restreint == 0 OR ($id_util_restreint != 0 AND est_proprio($groupe) == TRUE))) {
	  
		echo "\r\n<tr><td colspan=\"2\" style=\"padding-top: 7px; font-weight: bold;\">"._T('accesgroupes:etat_groupe')." : </td></tr>";
	  echo "\r\n<tr><td colspan=\"2\">"._T('accesgroupes:actif');
    echo "\r\n<input type=\"radio\" id=\"actif\" name=\"actif\" value=\"1\" ".(($actif != 0)? 'checked="checked" ' : '')."/>";
    echo "\r\n&nbsp;&nbsp;<input type=\"radio\" id=\"inactif\" name=\"actif\" value=\"0\" ".(($actif == 0)? 'checked="checked" ' : '')."/>";
    echo "\r\n"._T('accesgroupes:inactif');
		echo "\r\n</td>\r\n</tr>";
}
// admins restreints interdits si pas proprios			
if ($groupe > 0 AND ($id_util_restreint == 0 OR ($id_util_restreint != 0 AND est_proprio($groupe) == TRUE))){
    echo "\r\n<tr style=\"padding-top: 7px; font-weight: bold;\"><td>";
    echo "\r\n<input type=\"submit\" name=\"mod_groupe\" value=\""._T('accesgroupes:modifier')."\" class='fondo' style='font-size:10px;' />";   			
    echo "\r\n</td>\r\n<td>";
    echo "\r\n<input type=\"button\" onclick=\"bascule_effacer();\" id=\"del_groupe\" name=\"del_groupe\" value=\""._T('accesgroupes:supprimer')."\" CLASS='fondo' STYLE='font-size:10px;' />";
    echo "\r\n<input type=\"submit\" id=\"del_groupe_all\" name=\"del_groupe_all\" value=\""._T('accesgroupes:supprimer_tout')."\" CLASS='fondo' STYLE='font-size:10px; display: none;' />";
		echo "\r\n<script language=\"JavaScript\">
				 function bascule_effacer() {
				 					if (document.getElementById('del_groupe') && document.getElementById('del_groupe_all')) {
										 document.getElementById('del_groupe').style.display = 'none';
										 document.getElementById('del_groupe_all').style.display = 'inline';
										 document.getElementById('help_del_groupe').style.display = 'inline';
										 document.getElementById('inactif').checked = 'checked';
									}
				 }";
	 echo "\r\n</script>";
	 echo "\r\n</td></tr>";
} // fin limitations admins restreints
echo "\r\n<tr><td colspan=\"2\" style=\"background-color: #eeeeee; border: 1px solid #cccccc; font-size: 10px;\">";
echo "\r\n<span id=\"help_del_groupe\" style=\"display: none;\">"._T('accesgroupes:help_supprimer')."</span>";
echo "\r\n</td>\r\n</tr>\r\n</table>";
echo fin_block();
fin_cadre_relief();
echo "\r\n</form>";


// afficher la r�partition des rubriques par groupes
if ($groupe > 0){
	 debut_cadre_relief('rubrique-24.gif');
	 echo affiche_groupes_rubriques();	 
	 fin_cadre_relief();
}

// afficher l'arborescence des groupes

debut_raccourcis();
					
      echo "\r\n<table CELLPADDING=2 CELLSPACING=0 class='arial2' style='border: 1px solid #aaaaaa; width: 100%;'>\n";
      echo "\r\n<tr style='background-color: #fff;'><th colspan=\"2\">"._T('accesgroupes:arborescence_groupes')."</th><th colspan=\"2\">&nbsp;</th></tr>";
			$sql102 = "SELECT id_grpacces, nom, actif
								 FROM $Tjpk_groupes
								 GROUP BY nom";
			$result102 = spip_query($sql102);
      while ($row = spip_fetch_array($result102)){
      			 $id_ec = $row['id_grpacces'];
      			 $nom_ec = $row['nom'];
      			 echo "\r\n<tr style='background-color: #eeeeee;'>";
             echo "\r\n<td class='verdana11' style='border-top: 1px solid #cccccc; width: 14px; vertical-align:top;'>";
						 echo "\r\n<img src='img_pack/groupe-12.png' alt='|_'></td>";
             echo "\r\n<td style='border-top: 1px solid #cccccc;'><a href=\"$PHP_SELF?groupe=".$id_ec."\">".$nom_ec."</a><br />";
      			 echo affiche_descendance($id_ec, descendance_groupe($id_ec));
      			 echo "\r\n</td>";
             echo "\r\n<td style='border-top: 1px solid #cccccc; text-align : right; padding-right: 20px; vertical-align: middle;'>";
		  // supprimer rapide pour les admins pas restreints
				if ($id_util_restreint == 0 AND $groupe != 0) {
						 echo "\r\n<a style=\"vertical-align: bottom;\" href=\"".$PHP_SELF."?del_groupe=".$id_ec."&groupe=".$groupe."\">". http_img_pack('croix-rouge.gif', _T('accesgroupes:suppression'), "width='7' height='7' border='0' style='vertical-align: middle;'")."</a>";
				}
				else {
						 echo "&nbsp;";
				}					 
						 echo "\r\n</td>";
             echo "\r\n</tr>";
      }




      echo "\r\n</table>";
	
fin_raccourcis();


// =============================================================================
// DROITE
// =============================================================================
debut_droite();

if ($msg_text != '') {
	 $tete_msg = '<span style="color: '.($alerte != 1 ? '#6c3' : '#f00').'; text-align: center; font-weight: bold; font-size: 1.2em;">';
	 $queue_msg = '</span>';
	 $msg_text = $tete_msg.$msg_text.$queue_msg;
}
if ($groupe <= 0){
    // affichage des groupes existants et des rubriques qu'ils contr�lent
    gros_titre(_T('accesgroupes:titre_groupes'));
    echo "\r\n<br />";
		if ($msg_text != '') {
			  debut_cadre_trait_couleur("fiche-perso-24.gif", false, "", _T('accesgroupes:titre_msg_text'));
			  debut_cadre_couleur();
				echo $msg_text;
				fin_cadre_couleur();
			  fin_cadre_trait_couleur();
		}
    debut_cadre_trait_couleur("auteur-24.gif", false, "", _T('accesgroupes:membres'));
    gros_titre(_T('accesgroupes:choisir'));
    fin_cadre_trait_couleur();
    echo "\r\n<br />";
    
    debut_cadre_trait_couleur("rubrique-24.gif", false, "", _T('accesgroupes:organisation'));
    echo affiche_groupes_rubriques();
    fin_cadre_trait_couleur();
}
else {
      gros_titre(_T('accesgroupes:titre_page_groupe').' : '.$nom.' (n&deg; '.$groupe.')');
      if ($desc != "") {
    			echo "<div align='$spip_lang_left' style='margin-top: 10px; padding: 5px; border: 1px dashed #aaa; font-family: Verdana,Arial,Sans,sans-serif; font-size: 10px;'>";
    			echo "$desc";
    			echo "</div>\r\n<br />";
			}
  		if ($msg_text != '') {
  			 debut_cadre_trait_couleur("fiche-perso-24.gif", false, "", _T('accesgroupes:titre_msg_text'));
  			 debut_cadre_couleur();
				 echo $msg_text;
				 fin_cadre_couleur();
  			 fin_cadre_trait_couleur();
  		}
			debut_cadre_trait_couleur("auteur-24.gif", false, "", _T('accesgroupes:auteurs')._T('accesgroupes:du_groupe'));
			
// admins restreints interdits si pas proprios			
   if ($id_util_restreint == 0 OR ($id_util_restreint != 0 AND est_proprio($groupe) == TRUE)) {
	 		debut_cadre_couleur();
		  $suffixe_action_form = ($groupe AND $groupe != '') ? "?groupe=".$groupe : "";
			echo "\r\n<form action=\"$PHP_SELF".$suffixe_action_form."\" name=\"frm_auteur\" method=\"post\">";
			echo "\r\n<input type=\"hidden\" name=\"groupe\" value=\"$groupe\" />";
      echo _T('accesgroupes:ajouter_auteur');
			echo "\r\n<select name='auteur' size='1' style='width:150px;' class='fondl'>";
      $group = false;
      $group2 = false;
			
      $sql1 = "SELECT $Tspip_auteurs.*
        			 FROM $Tspip_auteurs
      				 LEFT JOIN $Tjpk_groupes_auteurs
      				 ON $Tspip_auteurs.id_auteur =  $Tjpk_groupes_auteurs.id_auteur 
      				 AND $Tjpk_groupes_auteurs.id_grpacces = \"$groupe\"
      	 			 WHERE $Tjpk_groupes_auteurs.id_auteur IS NULL
      	 			 ";
      $sql1 .= " AND statut!='5poubelle' 
      				 	 AND statut!='nouveau' 
      					 ORDER BY $Tspip_auteurs.statut, $Tspip_auteurs.nom, $Tspip_auteurs.id_auteur";
      $result1 = spip_query($sql1);
      if (mysql_errno() > 0) {
				 echo mysql_errno().": ".mysql_error();
			}
      while ($row = spip_fetch_array($result1)) {
            $id_auteur = $row["id_auteur"];
            $nom = $row["nom"];
            $email = $row["email"];
            $statut = $row["statut"];
						
            $statut = str_replace("0minirezo", _T('info_administrateurs'), $statut);
            $statut = str_replace("1comite", _T('info_redacteurs'), $statut);
            $statut = str_replace("6visiteur", _T('info_visiteurs'), $statut);
  
            $premiere = strtoupper(substr(trim($nom), 0, 1));
  
            if ($connect_statut != '0minirezo')
                if ($p = strpos($email, '@'))
                    $email = substr($email, 0, $p).'@...';
            if ($email)
                $email = " ($email)";
  
            if ($statut != $statut_old) {
                echo "\r\n<option value=\"x\"> </option>";
                echo "\r\n<option value=\"x\" style='background-color: $couleur_claire;'> $statut</option>";
            }
  
            if ($premiere != $premiere_old AND ($statut != _T('info_administrateurs') OR !$premiere_old)) {
                echo "\r\n<option value=\"x\">";
            }
  
            $texte_option = supprimer_tags(couper(typo("$nom$email"), 40));
            echo "\r\n<option value=\"$id_auteur\">&nbsp;&nbsp;&nbsp;&nbsp;$texte_option</option>";
            $statut_old = $statut;
            $premiere_old = $premiere;
      }

      echo "\r\n</select>";
			echo "\r\n<input type=\"submit\" name=\"add_auteur\" value=\""._T('accesgroupes:ajouter')."\"  class='fondo'/>";
			fin_cadre_couleur();		
   } // fin restriction admin restreint
	 
     // tableau des auteurs ayant acc�s
			echo "\r\n<table CELLPADDING=2 CELLSPACING=0 class='arial2' style='width: 100%; border: 1px solid #aaaaaa;'>\n";
      echo "\r\n<tr><th colspan=\"2\">"._T('accesgroupes:auteurs_groupe')."</th><th colspan=\"2\">&nbsp;</th></tr>";
      $sql2 = "SELECT $Tspip_auteurs.id_auteur, $Tspip_auteurs.nom, $Tspip_auteurs.statut, $Tjpk_groupes_auteurs.dde_acces
        			 FROM $Tjpk_groupes_auteurs, $Tspip_auteurs
       				 WHERE $Tjpk_groupes_auteurs.id_auteur = $Tspip_auteurs.id_auteur 
      				 AND $Tjpk_groupes_auteurs.id_grpacces = $groupe
							 AND $Tjpk_groupes_auteurs.dde_acces = 0";
      $result2 = spip_query($sql2);
      while ($row = spip_fetch_array($result2)){
           echo "\r\n<tr style='background-color: #eeeeee;'>";
           echo "\r\n<td class='verdana11' style='border-top: 1px solid #cccccc; width: 14px; vertical-align:top;'>";
  				 $statut_util_ec = trouve_statut($row['id_auteur']);
					 $statut_util_ec == '0minirezo' ?  $ico_statut = 'admin-12.gif' : ($statut_util_ec == '1comite' ? $ico_statut = 'redac-12.gif' : $ico_statut = 'visit-12.gif');
					 echo "\r\n<img src='img_pack/".$ico_statut."' alt='|_' style='vertical-align:top;'></td>";
           echo "\r\n<td style='border-top: 1px solid #cccccc;'><a href=\"auteurs_edit.php3?id_auteur=".$row['id_auteur']."\">".$row['nom']."</a></td>";
           echo "\r\n<td style='border-top: 1px solid #cccccc; text-align : right; padding-right: 20px;'>";
				// admins restreints interdits de modifs des membres du groupe si pas proprios
					 if ($id_util_restreint == 0 OR ($id_util_restreint != 0 AND est_proprio($groupe) == TRUE)) {
    					 echo "\r\n<a href=\"".$PHP_SELF."?del_auteur=".$row['id_auteur']."&groupe=$groupe\"> "._T('lien_retirer_auteur')." &nbsp;  ".http_img_pack('croix-rouge.gif', "X", "width='7' height='7' border='0' style='vertical-align: middle;'")."</a>";
               echo ($row['dde_acces']==1) ? "&nbsp;|&nbsp;<a href=\"".$PHP_SELF."?mod_auteur=".$row['id_auteur']."&groupe=".$groupe."\"> "._T('accesgroupes:accepter')."</a>" : "";
    				}
    				else {
    						 echo $row['id_auteur'];
    				}
					 echo "\r\n</td>";
           echo "\r\n<td style='border-top: 1px solid #cccccc;'><a href=\"mailto:$email\"></a></td>";
           echo "\r\n</tr>";
      }
      echo "</table>";
			
     // tableau des auteurs en attente d'une demande d'acc�s
      $sql2 = "SELECT $Tspip_auteurs.id_auteur, $Tspip_auteurs.nom, $Tspip_auteurs.statut, $Tjpk_groupes_auteurs.dde_acces
        			 FROM $Tjpk_groupes_auteurs, $Tspip_auteurs
       				 WHERE $Tjpk_groupes_auteurs.id_auteur = $Tspip_auteurs.id_auteur 
      				 AND $Tjpk_groupes_auteurs.id_grpacces = $groupe
							 AND $Tjpk_groupes_auteurs.dde_acces = 1";
      $result2 = spip_query($sql2);
			if (spip_num_rows($result2) > 0) {
          echo "\r\n<br /><table CELLPADDING=2 CELLSPACING=0 class='arial2' style='width: 100%; border: 1px solid #aaaaaa;'>\n";
          echo "\r\n<tr><th colspan=\"4\">"._T('accesgroupes:auteurs_en_attente')."</th></tr>";
          while ($row = spip_fetch_array($result2)){
               echo "\r\n<tr style='background-color: #eeeeee;'>";
               echo "\r\n<td class='verdana11' style='border-top: 1px solid #cccccc; width: 14px; vertical-align:top;'>";
      				 $statut_util_ec = trouve_statut($row['id_auteur']);
    					 $statut_util_ec == '0minirezo' ?  $ico_statut = 'admin-12.gif' : ($statut_util_ec == '1comite' ? $ico_statut = 'redac-12.gif' : $ico_statut = 'visit-12.gif');
    					 echo "\r\n<img src='img_pack/".$ico_statut."' alt='|_' style='vertical-align:top;'></td>";
               echo "\r\n<td style='border-top: 1px solid #cccccc;'><a href=\"auteurs_edit.php3?id_auteur=".$row['id_auteur']."\">".$row['nom']."</a></td>";
               echo "\r\n<td style='border-top: 1px solid #cccccc; text-align : right; padding-right: 20px;'>";
    			 if ($id_util_restreint == 0 OR ($id_util_restreint != 0 AND est_proprio($groupe) == TRUE)) {
    					 echo "\r\n<a href=\"".$PHP_SELF."?del_auteur=".$row['id_auteur']."&groupe=$groupe\"> "._T('lien_retirer_auteur')." &nbsp;  ".http_img_pack('croix-rouge.gif', "X", "width='7' height='7' border='0' style='vertical-align: middle;'")."</a>";
               echo ($row['dde_acces']==1) ? "&nbsp;|&nbsp;<a href=\"".$PHP_SELF."?mod_auteur=".$row['id_auteur']."&groupe=".$groupe."\"> "._T('accesgroupes:accepter')."</a>" : "";
    				}
    				else {
    						 echo $row['id_auteur'];
    				}
    					 echo "\r\n</td>";
               echo "\r\n<td style='border-top: 1px solid #cccccc;'><a href=\"mailto:$email\"></a></td>";
               echo "\r\n</tr>";
          }
          echo "</table>";
			}
fin_cadre_trait_couleur();


// inclure/g�rer des ss-groupes ou des statuts dans les groupes
// d�but des sous-groupes
      echo "\r\n<br />";
debut_cadre_trait_couleur("groupe-24.png", false, "", _T('accesgroupes:ss_groupes')._T('accesgroupes:du_groupe'));

// admins restreints interdits si pas proprios
   if ($id_util_restreint == 0 OR ($id_util_restreint != 0 AND est_proprio($groupe) == TRUE)) {
debut_cadre_couleur();
			echo _T('accesgroupes:ajouter_ss_groupe');
      echo "\r\n<select name='ss_groupe' SIZE='1' style='width:150px;' class='fondl'>\r\n";
			
			$sql101 = "SELECT id_grpacces, nom FROM $Tjpk_groupes WHERE id_grpacces != $groupe AND actif = 1 ORDER BY nom";
			$result101 = spip_query($sql101);
			

      echo "\n<option value=\"x\"> </option>\r\n";
      while ($row = spip_fetch_array($result101)) {
            $id_ss_groupe = $row["id_grpacces"];
            $nom_ss_groupe = $row["nom"];
  			// ne pas afficher les groupes d�ja inclus dans le groupe en cours (et leurs descendants)
						if (verifie_inclusions_groupe($groupe, $id_ss_groupe) == FALSE) {
							 continue;
						}
				// ne pas afficher les groupes appartenant � l'ascendance du groupe en cours
						if (verifie_inclusions_groupe($id_ss_groupe, $groupe) == FALSE) {
							 continue;
						}
            $nom_ss_grpe = strtoupper(substr(trim($nom_ss_groupe), 0, 1));
            echo "\n<option value=\"$id_ss_groupe\" style='background-color: $couleur_claire;'> $nom_ss_groupe</option>\r\n";
      }

      echo "</select>\r\n";
			
			echo "<input type=\"submit\" name=\"add_ss_groupe\" value=\""._T('accesgroupes:ajouter')."\"  CLASS='fondo'/>";
fin_cadre_couleur();
   } // fin limitation admin restreint
			echo "\r\n<table CELLPADDING=2 CELLSPACING=0 class='arial2' style='width: 100%; border: 1px solid #aaa;'>\n";
      echo "\r\n<tr><th colspan='3'>"._T('accesgroupes:ss_groupes_groupe')."</th></tr>";
			$sql102 = "SELECT $Tjpk_groupes_auteurs.id_ss_groupe, $Tjpk_groupes_auteurs.dde_acces, $Tjpk_groupes.nom 
								 FROM $Tjpk_groupes_auteurs, $Tjpk_groupes
								 WHERE $Tjpk_groupes_auteurs.id_grpacces = $groupe 
								 AND $Tjpk_groupes.actif = 1 
								 AND $Tjpk_groupes_auteurs.id_ss_groupe = $Tjpk_groupes.id_grpacces 
								 ORDER BY $Tjpk_groupes.nom";
			$result102 = spip_query($sql102);
      while ($row = spip_fetch_array($result102)){
      			 $id_ec = $row['id_ss_groupe'];
      			 $nom_ec = $row['nom'];
      			 echo "\r\n<tr style='background-color: #eeeeee;'>";
             echo "\r\n<td class='verdana11' style='border-top: 1px solid #cccccc; width: 14px; vertical-align:top;'>";
						 echo "\r\n<img src='img_pack/groupe-12.png' alt='|_' style='vertical-align:top;'></td>";
             echo "\r\n<td style='border-top: 1px solid #cccccc;'><a href=\"$PHP_SELF?groupe=".$id_ec."\">".$nom_ec."</a><br />";
      			 echo affiche_descendance($id_ec, descendance_groupe($id_ec));
      			 echo "</td>";
             echo "\r\n<td style='border-top: 1px solid #cccccc; text-align : right; padding-right: 20px;'>";
		    if ($id_util_restreint == 0 OR ($id_util_restreint != 0 AND est_proprio($groupe) == TRUE)) {
						 echo "\r\n<a href=\"".$PHP_SELF."?del_ss_groupe=".$row['id_ss_groupe']."&groupe=".$groupe."\">"._T('accesgroupes:retirer_groupe')."&nbsp;". http_img_pack('croix-rouge.gif', "X", "width='7' height='7' border='0' style='vertical-align: middle;'")."</a>";
             echo $row['dde_acces'] == 1 ? "&nbsp;|&nbsp;<a href=\"".$PHP_SELF."?mod_ss_groupe=".$row['id_ss_groupe']."&groupe=".$groupe."\">"._T('accesgroupes:accepter')."</a>" : "";
				}
				else {
						 echo "&nbsp;";
				}					 
						 echo "</td>";
             echo "</tr>";
      }
      echo "\r\n</table>";
fin_cadre_trait_couleur();

// d�but des statuts			
      echo "\r\n<br />";
debut_cadre_trait_couleur("statuts-24.png", false, "", _T('accesgroupes:statuts')._T('accesgroupes:du_groupe'));
			$Tstatuts = array("0minirezo" => "Administrateurs", "1comite" => "R&eacute;dacteurs", "6forum" => "Visiteurs");
   if ($id_util_restreint == 0 OR ($id_util_restreint != 0 AND est_proprio($groupe) == TRUE)) {
debut_cadre_couleur();
			echo _T('accesgroupes:ajouter_statut');
      echo "\r\n<select name='sp_statut' SIZE='1' STYLE='width:150px;' CLASS='fondl'>\r\n";
			$sql105 = "SELECT sp_statut FROM $Tjpk_groupes_auteurs WHERE id_grpacces = $groupe AND sp_statut != '' GROUP BY sp_statut";
			$result105 = spip_query($sql105);
			while ($row = spip_fetch_array($result105)) {
						$Tresult105[] = $row['sp_statut'];
			}
      echo "\r\n<option value=\"x\"> </option>\r\n";
      foreach ($Tstatuts as $statut_ec => $nom_stat) {
							if (!in_array($statut_ec, $Tresult105)) {
  							 echo "\r\n<option value=\"$statut_ec\" style='background-color: $couleur_claire;'> $nom_stat</option>\r\n";
							}
      }
      echo "\r\n</select>\r\n";
			echo "\r\n<input type=\"submit\" name=\"add_statut\" value=\""._T('accesgroupes:ajouter')."\"  CLASS='fondo'/>";
fin_cadre_couleur();
   } // fin limitation admin restreint
      $sql104 = "SELECT $Tjpk_groupes_auteurs.sp_statut, $Tjpk_groupes_auteurs.dde_acces 
      				 FROM $Tjpk_groupes_auteurs, $Tjpk_groupes
      				 WHERE $Tjpk_groupes_auteurs.id_grpacces = $groupe 
      				 AND $Tjpk_groupes.actif = 1 
      				 AND $Tjpk_groupes.id_grpacces = $Tjpk_groupes_auteurs.id_grpacces
      				 AND $Tjpk_groupes_auteurs.sp_statut != ''
      				 GROUP BY $Tjpk_groupes_auteurs.sp_statut";
      $result104 = spip_query($sql104);
      
      echo "\r\n<table CELLPADDING=2 CELLSPACING=0 class='arial2' style='width: 100%; border: 1px solid #aaaaaa;'>\n";
      echo "\r\n<tr><th colspan=\"3\">"._T('accesgroupes:statut_groupe')."</th></tr>";
      while ($row = spip_fetch_array($result104)) {
      			 $statut_ec = $row['sp_statut'];
      			 $nom_statut = $Tstatuts[$statut_ec];
  					 $statut_ec == '0minirezo' ?  $ico_statut = 'admin-12.gif' : ($statut_ec == '1comite' ? $ico_statut = 'redac-12.gif' : $ico_statut = 'visit-12.gif');
						 
      			 echo "\r\n<tr style='background-color: #eeeeee;'>";
             echo "\r\n<td class='verdana11' style='border-top: 1px solid #cccccc; width: 14px; vertical-align:top;'>";
						 echo "\r\n<img src='img_pack/".$ico_statut."' alt='|_' style='vertical-align:top;'></td>";
						 echo "\r\n<td style='border-top: 1px solid #cccccc;'>".$nom_statut."</td>";			 
             echo "\r\n<td style='border-top: 1px solid #cccccc; text-align : right; padding-right: 20px;'>";
			 if ($id_util_restreint == 0 OR ($id_util_restreint != 0 AND est_proprio($groupe) == TRUE)) {
						 echo "\r\n<a href=\"".$PHP_SELF."?del_statut=".$statut_ec."&groupe=".$groupe."\">"._T('accesgroupes:retirer_statut')."&nbsp;".http_img_pack('croix-rouge.gif', "X", "width='7' height='7' border='0' style='vertical-align: middle;'")."</a>";
             echo ($row['dde_acces']== 1) ? "&nbsp;|&nbsp;<a href=\"".$PHP_SELF."?mod_statut=".$statut_ec."&groupe=".$groupe."\">"._T('accesgroupes:accepter')."</a>" : "";
			 }
			 else {
			 			echo "&nbsp;";
			 }
						 echo "\r\n</td>";
             echo "\r\n</tr>";
      }
      echo "\r\n</table>";
fin_cadre_trait_couleur();

	 		echo "\r\n</form>";

      echo "<br />";

			echo "\r\n<script language=\"JavaScript\" type=\"text/javascript\">
					  				Tacces_rub = new Array();
								// fct pour s�lectionner le radio correspondant au type d'acc�s restreint (priv�+public/priv�/public) de la rubrique s�lectionn�e dans la liste d�roulante
								// et limiter le choix du type de restriction en fct du type d'acc�s du parent par masquage des radios interdits
										function select_rubrique_acces() {
														 document.getElementById('span_prive_public_1').style.display = 'inline';
														 document.getElementById('span_prive_public_2').style.display = 'inline';
														 for (i in Tacces_rub) {
														 		 if (document.getElementById('id_parent').value == Tacces_rub[i][0]) {
																 		document.getElementById('prive_public_' + Tacces_rub[i][1]).checked = 'checked';
																		if (Tacces_rub[i][2] == 0) {
																			 document.getElementById('span_prive_public_1').style.display = 'none';
																			 document.getElementById('span_prive_public_2').style.display = 'none';
																		} 
																		if (Tacces_rub[i][2] == 1) {
																			 document.getElementById('span_prive_public_2').style.display = 'none';
																		}
																		if (Tacces_rub[i][2] == 2) {
																			 document.getElementById('span_prive_public_1').style.display = 'none';
																		}
																		return true;
																 }
														 }
													// si la rubrique n'appartient pas � Tacces_rub, s�lectionner prive+public par d�faut
														 document.getElementById('prive_public_0').checked = 'checked';
										}
						</script>";

debut_cadre_trait_couleur("rubrique-24.gif", false, "", _T('accesgroupes:rubriques_restreintes'));
      
			echo "<div style=\"font-weight: bold; margin-bottom: 5px; font-size: 10px;\">";
			echo _T('accesgroupes:rubriques_autorisees_info');
			echo "<span style=\"color: #f00; background-color: #c0cad4; padding: 2px;\">";
			echo _T('accesgroupes:prive_public');
			echo "</span> <span style=\"color: #093; background-color: #c0cad4; padding: 2px;\">";
			echo _T('accesgroupes:prive_seul');
			echo "</span> <span style=\"color: #f90; background-color: #c0cad4; padding: 2px;\">";
			echo _T('accesgroupes:public_seul');
			echo "</span></div>";
			echo "<div style=\"font-weight: bold; margin-bottom: 5px; font-size: 10px;\">";
			echo _T('accesgroupes:prive_public_autres');
			echo "<span style=\"color: #00f; background-color: #c0cad4; padding: 2px; font-size: 10px;\">";
			echo _T('accesgroupes:prive_public_tous');
			echo "</span></div>";
			
      echo "\r\n<form action=\"$PHP_SELF\" name=\"frm_rubrique\" method=\"post\" style=\"margin-bottom: 0px;\">\n";
			
      echo "\r\n<select id='id_parent' onChange=\"select_rubrique_acces();\" name='id_parent' style='background-color:#fff; font-size:90%; font-face:verdana,arial,helvetica,sans-serif; max-height: 24px; margin-top: 2px;' class='forml' size='1'>\n";
      if ($connect_toutes_rubriques) {
        echo "\r\n<option ".mySel("0",$id_parent).http_style_background('racine-site-12.gif',  "$spip_lang_left no-repeat; background-color:$couleur_foncee; padding-$spip_lang_left: 16px; font-weight:bold; color:white") .'>'._T('info_racine_site')."\n";
      } 
      else {
					 $Trub_autorise = cree_Trub_admin();
					 if (count($Trub_autorise) > 0) {
    					 enfant(0);
					 }
					 else {
					 			echo "\r\n<option ".mySel("0",$id_parent). http_style_background('racine-site-12.gif',  "$spip_lang_left no-repeat; background-color:$couleur_foncee; padding-$spip_lang_left: 16px; font-weight:bold; color:white");" \">"._T('info_non_deplacer')."\n";
					 }
      }
      
      if (lire_meta('multi_rubriques') == 'oui') {
				 echo " [".traduire_nom_langue(lire_meta('langue_site'))."]";
			}
      
      // si le parent ne fait pas partie des rubriques restreintes, modif impossible
      if (acces_rubrique($id_parent)) {
          enfant(0);
      }
			
      echo "</select>";

    	echo "\r\n<span style=\"font-size: 10px; float: right; text-align: left;\">";
		 	echo "\r\n<span id=\"span_prive_public_2\"><input id=\"prive_public_2\" style=\"text-align: right; vertical-align: bottom;\" type=\"radio\" name=\"prive_public\" value=\"2\"".($prive_public_ec == 2 ? " checked=\"checked\"" : "")."  />"._T('accesgroupes:public')."</span>";
		 	echo "\r\n<span id=\"span_prive_public_1\"><br /><input id=\"prive_public_1\" style=\"text-align: right; vertical-align: bottom;\" type=\"radio\" name=\"prive_public\" value=\"1\"".($prive_public_ec == 1 ? " checked=\"checked\"" : "")."  />"._T('accesgroupes:prive')."</span>";
		 	echo "\r\n<span id=\"span_prive_public_0\"><br /><input id=\"prive_public_0\" style=\"text-align: right; vertical-align: bottom;\" type=\"radio\" name=\"prive_public\" value=\"0\"".(($prive_public_ec == 0 OR $prive_public_ec == '') ? " checked=\"checked\"" : "")."  />"._T('accesgroupes:les_2')."</span>";
			echo "\r\n</span>";
			echo "\r\n<span style=\"float: right;\">"._T('accesgroupes:portee_acces')."</span>";
      echo "\r\n<input type=\"submit\" name=\"add_rub\" value=\""._T('accesgroupes:autoriser')."\"  class='fondo'/> \n";
			echo "\r\n<br /><span style=\"font-size: 9px;\">("._T('accesgroupes:help_portee_acces').")</span>";
      echo "\r\n<input type=\"hidden\" name=\"groupe\" value=\"$groupe\" />\n";
      echo "\r\n</form>\n";
			echo "\r\n<script language=\"JavaScript\" type=\"text/javascript\">select_rubrique_acces();</script>";			

      $sql603 = "SELECT $Tspip_rubriques.*, $Tjpk_groupes_acces.prive_public
        				 FROM $Tjpk_groupes_acces, $Tspip_rubriques
        				 WHERE $Tspip_rubriques.id_rubrique = $Tjpk_groupes_acces.id_rubrique 
              	 AND $Tjpk_groupes_acces.id_grpacces = \"$groupe\"";
      $result603 = spip_query($sql603);
      debug($result603);
      echo "\r\n<table CELLPADDING=2 CELLSPACING=0 class='arial2' style='border: 1px solid #aaaaaa; width: 100%; clear: right; margin-top: 0px;'>\n";
      echo "\r\n<tr><th colspan='5'>"._T('accesgroupes:autoriser_info')."</tr>";
			echo "\r\n<tr style=\"background-color: #fff; border-top: solid 1px #ccc;\"><td style=\"font-size: 10px; border-top: solid 1px #ccc;\"></td><td style=\" border-top: solid 1px #ccc;\">"._T('accesgroupes:nom')."</td><td colspan=\"2\" style=\" border-top: solid 1px #ccc;\">"._T('accesgroupes:portee_acces')."</td><td  style=\"margin-left: 20px; padding-right: 20px; border-top: solid 1px #ccc;\" >"._T('accesgroupes:suppression')."</td></tr>";
      while ($row = spip_fetch_array($result603)){
      			$prive_public_ec = $row['prive_public'];
						$modif_id_rubrique = $row['id_rubrique'];
						echo "\r\n<tr style='background-color: #eeeeee;'>";
						echo "\r\n<td class='verdana11' style='border-top: 1px solid #cccccc; width: 14px; vertical-align:middle;'>";
						echo "\r\n<img src='img_pack/rubrique-12.gif' alt='|_'></td>";
						echo "\r\n<td style='border-top: 1px solid #cccccc;'><a href=\"naviguer.php3?id_rubrique=".$row['id_rubrique']."\">".typo($row['titre'])."</a></td>";
						echo "\r\n<td style='border-top: 1px solid #cccccc; text-align: right;'>";
    		 		if ($id_util_restreint == 0 OR ($id_util_restreint != 0 AND est_proprio_acces($row['id_rubrique']) == TRUE)) {
          		  echo "\r\n<form action=\"$PHP_SELF?groupe=$groupe\" name=\"form_modif_rubrique_".$modif_id_rubrique."\" method=\"post\">\n";
    						echo "\r\n<input  style=\"font-size: 75%;\" type=\"submit\" name=\"submit_modif_rub\" value=\""._T('accesgroupes:modifier')."\"  CLASS='fondo'/></td>\n";
    						echo "\r\n<td style=\"border-top: 1px solid #cccccc;\">";
						 // selon le prive/public de l'ascendance de la rubrique, limiter les possibilit�s de prive/public pour la rubrique
								if ($Trub_grpe_ec_parent[$row['id_rubrique']] > 1) { 
									 echo "\r\n<input style=\"text-align: right; vertical-align: bottom;\" type=\"radio\" name=\"modif_prive_public\" value=\"2\"".($prive_public_ec == 2 ? " checked=\"checked\"" : "")."  />"._T('accesgroupes:public');
    						}
								if ($Trub_grpe_ec_parent[$row['id_rubrique']] > 0 AND $Trub_grpe_ec_parent[$row['id_rubrique']] != 2) {
									 echo "\r\n<br /><input style=\"text-align: right; vertical-align: bottom;\" type=\"radio\" name=\"modif_prive_public\" value=\"1\"".($prive_public_ec == 1 ? " checked=\"checked\"" : "")."  />"._T('accesgroupes:prive');
    						}
								echo "\r\n<br /><input style=\"text-align: right; vertical-align: bottom;\" type=\"radio\" name=\"modif_prive_public\" value=\"0\"".(($prive_public_ec == 0 OR $prive_public_ec == '') ? " checked=\"checked\"" : "")."  />"._T('accesgroupes:les_2');
    						echo "\r\n<input type=\"hidden\" name=\"modif_id_rubrique\" value=\"$modif_id_rubrique\" />\n";
    						echo "\r\n<input type=\"hidden\" name=\"modif_id_groupe\" value=\"$groupe\" /></td>";
          			echo "\r\n</form>\n";
    						echo "\r\n<td style=\"border-top: 1px solid #cccccc; padding-right: 10px;\" ><a href=\"".$PHP_SELF."?del_rub=".$row['id_rubrique']."&groupe=".$groupe."\">"._T('supprimer')."&nbsp;". http_img_pack('croix-rouge.gif', "X", "width='7' height='7' border='0' style='vertical-align: middle;'") ."</a></td>\n";
    
    			  }
    			  else{
    			 		  echo "\r\n</td>";
    			  }
    				echo "\r\n</tr>";
      }
      echo "\r\n</table>";

fin_cadre_trait_couleur();
}

// liste des groupes ayant acc�s � la rubrique courante...
$sql_rub_grp = "SELECT $Tjpk_groupes.*
  					 	  FROM $Tjpk_groupes_acces, $Tjpk_groupes
  							WHERE $Tjpk_groupes_acces.id_grpacces = $Tjpk_groupes.id_grpacces 
								AND $Tjpk_groupes_acces.id_rubrique = \"$id_parent\" 
								AND $Tjpk_groupes_acces.id_grpacces <> $groupe";
$result_rub_grp = spip_query($sql_rub_grp);
if ($id_parent > 0 && spip_num_rows($result_rub_grp) > 0 ){
    debut_cadre_relief("mot-cle-24.gif");
    echo gros_titre(_T('accesgroupes:acces_rubrique_par'));
    echo "\r\n<ul>";
    while ($row=spip_fetch_array($result_rub_grp)){
    			echo "\r\n<li>".typo($row['nom'])."</li>";
    }
    echo "\r\n</ul>";
    fin_cadre_relief();
}
fin_page();
?>