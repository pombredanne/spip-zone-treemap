<?php
// Zktx language file for SPIP --  Fichier langue Zktx pour SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_squelettes_/zktx/lang/

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pass_erreur_404' => 'Le document que vous recherchez n\'existe plus ou a &eacute;t&eacute; d&eacute;plac&eacute;.',
	'pass_erreur_401' => 'Une authentification est n&eacute;cessaire pour acc&eacute;der &agrave; la page.',
	'pass_erreur_503' => 'Service temporairement indisponible ou en maintenance.'
);

?>