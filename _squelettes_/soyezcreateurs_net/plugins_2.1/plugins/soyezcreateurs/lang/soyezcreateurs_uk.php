<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de http://trad.spip.net/tradlang_module/soyezcreateurs?lang_cible=uk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accessibilite_menu' => 'Перейти до меню',
	'accessibilite_onglets' => 'Aller aux onglets', # NEW
	'accessibilite_recherche' => 'Перейти до пошуку',
	'accessibilite_texte' => 'Перейти до тексту',
	'accueil' => 'головна',
	'accueil_menu' => 'Головна сторінка',
	'accueil_site' => 'Revenir à l\'Accueil du site', # NEW
	'agenda' => 'Події',
	'agenda_exporter_ical' => 'Експорт в ICAL',
	'agenda_fsd' => 'З @dдня_l@ @dдня@ @місяця_l@ @року@ о @годині@:@хвилин@ au @fдня_l@ @fдня@ @fмісяця_l@ @fроку@ à @fгодин@:@fхвилин@',
	'agenda_fsd_notime' => 'З @dдня_l@ @dдня@ @dмісяця_l@ @dроку@ до @fдня_l@ @fдня@ @fмісяця_l@ @fроку@',
	'agenda_proposer_evenement' => 'Proposer un événement', # NEW
	'agenda_proposer_evenement_explication' => 'Vous pouvez proposer un événement à l\'administrateur du site qui jugera bon de le publier ou non. Les événements les plus détaillés possibles ont le plus de chances d\'être validé.', # NEW
	'agenda_proposer_evenement_title' => 'Vous pouvez proposer un événement à l\'administrateur du site', # NEW
	'agenda_sd' => 'Le @djour_l@ @djour@ @dmois_l@ @dannee@ de @dheure@:@dminutes@ à @fheure@:@fminutes@', # NEW
	'agenda_sd_h' => 'Le @djour_l@ @djour@ @dmois_l@ @dannee@ à @dheure@:@dminutes@', # NEW
	'agenda_sd_notime' => 'Le @djour_l@ @djour@ @dmois_l@ @dannee@', # NEW
	'agenda_sinscrire' => 'Зареєструватись на подію',
	'agendamoisde' => 'Події місяця',
	'aidesc' => 'Довідка SoyezCréateurs',
	'alaune' => 'На першій сторінці',
	'annuaire' => 'Annuaire', # NEW
	'annuaire_consulter' => 'Consulter l\'annuaire', # NEW
	'annuaire_local' => 'Annuaire local', # NEW
	'annuaire_vide' => 'L\'annuaire est vide.', # NEW
	'archives' => 'Архів',
	'archives_title' => 'Переглянути архів статей',
	'articlesconnexes' => 'Відповідні статті ( за темою) ',
	'articlesordreantichrono' => 'Статті у зворотному хронологічному порядку',
	'auteur' => 'Автор',
	'auteurs_liste' => 'Список авторів',
	'auteurs_site' => 'Автори сайту',

	// C
	'carte_monde' => 'Карта світу',
	'cfg_affichage_affiche' => 'Афішувати, показати, відкрити',
	'cfg_affichage_impression' => 'Повний дисплей',
	'cfg_affichage_impression_ex' => 'Перегляд посилання у верхній частині сторінки для відображення всього вмісту гілки (у тому числі елементів) для друку',
	'cfg_affichage_impression_label' => 'Показ повної рубрики',
	'cfg_affichage_masquer' => 'Замаскувати ( приховати)',
	'cfg_affichage_nouveautes_ex' => 'Афішування ( показ)  блоку "Що нового " у вихідних даних по кожній рубриці, за списком нових статей поточної гілки',
	'cfg_affichage_nouveautes_label' => 'Афішування ( показ) новин за галузями ',
	'cfg_affichage_plan_ex' => 'Це посилання відправляє вас на сторінку зі списком всіх елементів з сайту, перерахованих у зворотному хронологічному порядку ', # MODIF
	'cfg_affichage_plan_label' => 'Афішувати (показати ) посилання на план у зворотному хронологічному порядку у картриджі   Що нового?',
	'cfg_affichage_zone_bandeau' => 'Показ ( Дисплей ) у верхній банер',
	'cfg_affichage_zone_principale' => 'Показ ( Дисплей ) в головному меню',
	'cfg_affichage_zone_secondaire' => 'Показ ( Дисплей)  у зоні вторинного меню',
	'cfg_affichagelistedocuments' => ' Афішування ( Показ) переліку документів, що додаються',
	'cfg_affichagelistedocuments_cartouche' => 'У нижній частині картриджа статті та теми',
	'cfg_affichagelistedocuments_enbas' => 'У нижній частині статей та рубрик',
	'cfg_affichagelistedocuments_label' => 'Зона ( область ) афішування ( показу) ',
	'cfg_affichagelistedocuments_listeravecarticles' => 'Список документів, які додаються до статті в списках статей (центральних)',
	'cfg_affichagelistedocuments_masquervus' => 'Ne pas afficher les documents insérés dans le contenu', # NEW
	'cfg_affichagelistedocuments_noicon' => 'Не показувати іконку  документу в списках',
	'cfg_affichagelistedocuments_nulpart' => 'Ніде (виключити інші варіанти!)',
	'cfg_affichagelistedocuments_redirectsiunique' => 'Перенаправлення на документ, якщо він єдиний і немає тексту в статті',
	'cfg_afficher_titre_invisible' => 'Невидимий: текст спалюється в банері',
	'cfg_afficher_titre_label' => 'Афішування ( Показ )  назви сайту',
	'cfg_afficher_titre_visible' => 'Видимий: банер без спаленого тексту',
	'cfg_age_goodies' => 'Âge maximum des goodies avant archivage', # NEW
	'cfg_agenda_futurseul' => 'Події в майбутньому',
	'cfg_agenda_ical_masque' => 'Відображення іконки ICAL в подіях',
	'cfg_agenda_ical_masque_label' => 'Приховати  ( замаскувати)',
	'cfg_agenda_inscription' => 'Реєстрація на події',
	'cfg_agenda_inscription_label' => 'Дозволити відвідувачам (зареєстрованим) записатись на події',
	'cfg_agendascolaire' => 'Шкільний щоденник',
	'cfg_agendathematique' => 'Афішування ( Показ )  тематичного щоденника ',
	'cfg_agendathematique_label' => 'Тематичний щоденник (тематичний план рубрик та розділів щоденника)',
	'cfg_alaune_label' => 'Кількість перших сторінок',
	'cfg_annuel' => 'Річний',
	'cfg_anonymatglobal' => 'Глобальна анонімність',
	'cfg_anonymatglobal_ex' => 'Дозволяє приховати ім\'я автора статті',
	'cfg_anonymatglobal_label' => 'Глобальна анонімність (можуть бути відновлені стаття за статтею  з ключовим словом "Розкрити відомості про особу" ',
	'cfg_apparence_header' => 'Вигляд верхньої частини сторінки',
	'cfg_bandeau_annonce' => 'Афішування ( показ) банеру оголошення',
	'cfg_bandeau_contact' => 'Афішування ( показ) банеру контакту',
	'cfg_bandeau_page' => 'У банер заголовку сторінки',
	'cfg_banniere_degrade' => 'Фон втрачає чіткість',
	'cfg_banniere_lentilles' => 'Ефект лінзи',
	'cfg_banniere_metal' => 'Матовий метал',
	'cfg_bas_principale' => 'Вниз головного меню навігації',
	'cfg_bulle_aide_label' => '"Хмарка з поясненням" відображається спочатку в браузері',
	'cfg_cadre_vignettes' => 'Налаштування мініатюр ( режим порталу)',
	'cfg_cadreinfo' => 'Рамки інформації',
	'cfg_cadrestexte' => 'Рамки змісту',
	'cfg_cartouche' => 'Рамки "Що нового "," Зміст "..',
	'cfg_choix_banniere_label' => 'Вибір банера (якщо немає логотипу огляду  для сайту )',
	'cfg_code_pub_google' => 'Код Google  для банерів',
	'cfg_code_recherche_google' => 'Код Google  для  пошуку та кнопки  спонсорного програмного  забезпечення ',
	'cfg_code_stats_google' => 'Код Google  для статистики з Google Analytics (формат: UA-XXXXX-X)',
	'cfg_colonne_date' => 'Афішування ( Показ )  поруч з датою оновлення сайту',
	'cfg_colonne_extra' => 'Вторинна колонка (вище цитати)',
	'cfg_colonne_nav' => 'Головна колонка( вище головної сторінки)',
	'cfg_colonne_nav_liens' => 'Внизу зони головного меню',
	'cfg_colonne_sec' => 'Афішування (показ ) внизу  зони  вторинного меню',
	'cfg_connexion_public' => 'Посилання зв\'язку в громадських ...',
	'cfg_connexion_public_ex' => 'Афішувати ( показати ) посилання підключення громадськості для дозволу  використовувати пучки для громадськості, минаючи інтерфейс запису ',
	'cfg_connexion_public_label' => 'Афішування ( показ) посилання зв\'язку',
	'cfg_contenu' => 'Зміст',
	'cfg_couleurs_css' => 'Налаштування ( конфігурація)кольорів CSS ',
	'cfg_couleurs_css_contenu' => 'Конфігурація кольорів CSS змісту',
	'cfg_couleurs_css_lettre' => ' Конфігурація кольорів Розсилання',
	'cfg_datepublication' => 'Відображення дати публікаці',
	'cfg_datepublication_ex' => '(Якщо дата вимкнено, вона може бути відновлена ключовим словом "DevoilerDate" "Відкриття дати")',
	'cfg_datepublication_label' => 'Афішувати ( показати)  дату публікації статей',
	'cfg_datepublicationgoodies_ex' => 'En mode Portail communiquant', # NEW
	'cfg_datepublicationgoodies_label' => 'Affichage de la date de publication des <b>Goodies</b>', # NEW
	'cfg_debut_contenu' => 'Au début du contenu (flottant en haut à droite)', # NEW
	'cfg_decalage_bas_label' => 'Зрушення до низу логотипа, якщо він присутній в заголовку (у пікселях)',
	'cfg_decalage_droite_label' => 'Перехід на право від логотипу, якщо він присутній в заголовку (у пікселях)',
	'cfg_em' => 'ет </ B>', # MODIF
	'cfg_entete_page' => 'Заголовок сторінки',
	'cfg_ephemeride' => 'Ефемерід',
	'cfg_ephemeride_label' => ' Афішування ( показ ) ефемеріду',
	'cfg_evenements_label' => 'Афішування ( показ, перегляд ) подій',
	'cfg_exemple' => 'Приклад: <strong> напівжирним шрифтом </ STRONG> та <em> курсивом </ EM>',
	'cfg_exemple_rendu' => 'Приклад візуалізації',
	'cfg_explicationslogin' => 'Пояснення до Логін сторінки з обмеженим доступом',
	'cfg_feedflare' => 'FeedFlare',
	'cfg_fin_contenu' => 'À la fin du contenu, en bas', # NEW
	'cfg_fond' => 'Фон',
	'cfg_fond_degrade' => 'Змінний',
	'cfg_footer' => 'Нижній колонтитул',
	'cfg_formulaire_recherche' => 'Формуляр пошуку',
	'cfg_futur_seul' => 'Тільки в майбутньому',
	'cfg_geolocalisation_label' => 'Афішувати ( показати ) геолокаціі нижче пошуку',
	'cfg_globale' => 'Глобальна для всього сайту',
	'cfg_goodies_label' => 'Nombre de goodies affichées par rubrique (mode portail communiquant)', # NEW
	'cfg_googleplusun_explication' => '<a href="http://www.google.com/webmasters/+1/button/index.html">Google +1</a> : service de Google pour indiquer aux autres son intérêt pour une page.', # NEW
	'cfg_googleplusun_info' => 'Google +1', # NEW
	'cfg_googleplusun_non' => 'Ne pas afficher le badge Google +1', # NEW
	'cfg_googleplusun_oui' => 'Afficher le badge Google +1', # NEW
	'cfg_haut_footer' => 'Афішування ( показ, дисплей )  у верхній частині нижнього колонтитула',
	'cfg_hauteur' => 'Висота',
	'cfg_hauteur_videoaccueil' => 'Висота відео головної сторінки (0 для автоматичної висоти)',
	'cfg_header' => 'Тема ',
	'cfg_homeagenda_ex' => 'Concerne les événements affichés en mode portail au centre de la page', # NEW
	'cfg_homeagenda_label' => 'Affichage des événements sur la page d\'accueil', # NEW
	'cfg_identifiant_addthis' => 'Id <a href="http://www.addthis.com/"> AddThis </ A>',
	'cfg_indexation' => 'Індексування',
	'cfg_indexation_info' => 'Індексація сайту пошуковими системами',
	'cfg_indexation_non' => 'Не індексувати сайт в пошукових системах',
	'cfg_indexation_oui' => 'Індексувати сайт в пошукових системах',
	'cfg_inf_rainette_ville' => 'Дозволяє відображати погодні умови і прогнози для французького міста, завдяки вільному коду: а href = "http://www.weather.com/outlook/weatherbycountry/france?from=enhsearch_reg"> <a "weather.com ® </ A>.',
	'cfg_info' => 'Конфігурація скелету SoyezCreateurs',
	'cfg_largeur' => 'Ширина',
	'cfg_largeur_colonnes_centrales' => 'Ширина кожної з трьох центральних колонок',
	'cfg_largeur_conteneur' => 'Ширина контейнера',
	'cfg_largeur_max_img' => 'Максимальна ширина зображення (великі зображення будуть автоматично зменшені)',
	'cfg_largeur_menu_p' => 'Ширина головного меню',
	'cfg_largeur_menu_sec' => 'Ширина підменю (якщо відділити від основного меню',
	'cfg_largeur_ombre' => 'Ширина тіні на стороні (симетричній!)',
	'cfg_largeur_splickrbox' => 'Largeur de la colonne contenant le Splickrbox', # NEW
	'cfg_largeur_videoaccueil' => 'Ширина відео головної сторінки  (0 для автоматичної ширини)',
	'cfg_largeurs_colonnes' => 'Ширина стовпців',
	'cfg_layout_accueil' => 'Головна сторінка',
	'cfg_layout_agencement' => 'Композиція',
	'cfg_layout_defaut' => 'За замовчуванням',
	'cfg_layout_gala' => 'Композиція із стовпців на сторінці',
	'cfg_layout_gala_explications' => 'Схема сайту. <br /> Для статтей та нарисів, установка ( регулювання)  може бути зроблена індивідуально шляхом присвоєння  ключових слів до них. </ P>',
	'cfg_layout_gala_intro' => 'На підставі <p> <a href="http://blog.html.it/layoutgala/" hreflang="en"> Layout Gala </ A> </ P>',
	'cfg_lien_clic' => 'Натиснути на посилання',
	'cfg_lien_non_visite' => 'Невідвідане посилання',
	'cfg_lien_survol' => 'Посилання огляд',
	'cfg_lien_visite' => 'Відвідане посилання',
	'cfg_locale_menu' => 'Розташування для кожного сектора у верхньому меню',
	'cfg_localement' => 'Тільки локально в поточній рубриці',
	'cfg_logo' => 'Логотип',
	'cfg_logolocal' => 'Успадкування логотипу рубрик',
	'cfg_logolocal_herite' => 'Успадкований логотип ( від предків) ',
	'cfg_logolocal_label' => 'Спосіб',
	'cfg_logolocal_local' => 'Логотип специфічний для кожної категорії',
	'cfg_maj' => 'Дата поновлення',
	'cfg_marges_colonnes_centrales' => 'Ширина поля по обидві сторони від центральних блоків.', # MODIF
	'cfg_masque' => 'Замаскувати ( приховати ) піктограму RSS  на сторінках (Сайт + щоденник)',
	'cfg_max_agenda_label' => 'Максимальна кількість елементів, афішованих ( показаних) під щоденником',
	'cfg_menu_actif' => 'Активне меню',
	'cfg_menu_affichagelogo_label' => 'Affichage du logo', # NEW
	'cfg_menu_clic' => 'Меню натиснути',
	'cfg_menu_footer_liens' => 'У колонтитул',
	'cfg_menu_haut' => 'Меню верхнє та нижнє',
	'cfg_menu_haut_affichagelogo_deroulant' => 'Menus déroulants', # NEW
	'cfg_menu_haut_affichagelogo_deroulant_non' => 'N\'afficher que le texte', # NEW
	'cfg_menu_haut_affichagelogo_logoonly' => 'Affichage du logo seul', # NEW
	'cfg_menu_haut_affichagelogo_logoplustexte' => 'Affichage du logo et du titre de la rubrique', # NEW
	'cfg_menu_inactif' => 'Неактивне меню',
	'cfg_menu_survol' => 'Огляд меню',
	'cfg_menuderoulant' => 'Меню, що випадає ( розкривається)', # MODIF
	'cfg_menuderoulant_deroule' => 'Розгорнутий (-те) ',
	'cfg_menuderoulant_label' => 'Афішування ( показ, дисплей) ',
	'cfg_menuderoulant_replie' => 'Складений (- не )',
	'cfg_menuderoulantgauche' => 'Menu de gauche', # NEW
	'cfg_miniagenda' => 'Міні-щоденник',
	'cfg_miniagenda_label' => 'Афішування ( показ) міні-щоденника',
	'cfg_mode_affichage' => 'Вибір засобу афішування ( показу, перегляду) сайту',
	'cfg_mode_affichage_bandeau' => 'Дисплей ( афішування, показ)  у верхній банер',
	'cfg_mode_affichage_citations_label' => 'Засіб відображення цитат ( якщо вони є) ',
	'cfg_mode_affichage_deepmenu_arborescent' => 'Toute l\'arborescence', # NEW
	'cfg_mode_affichage_deepmenu_deroulant' => 'Seulement le premier niveau', # NEW
	'cfg_mode_affichage_deepmenu_label' => 'Profondeur du menu du haut', # NEW
	'cfg_mode_affichage_deepmenu_rien' => 'Pas de menu déroulant', # NEW
	'cfg_mode_affichage_dessus_contenu' => 'Дисплеї ( афішування, показ) над змістом, такої ж ширини',
	'cfg_mode_affichage_haut_label' => 'Режим ( засіб) відображення головного меню (якщо є)',
	'cfg_mode_affichage_label' => 'Режим афішування ( дисплея)  головної сторінки сайту',
	'cfg_mode_affichage_logo_label' => 'Засіб афішування ( дисплея )  логотипу  (якщо є)',
	'cfg_mode_bandeau_contact_label' => 'Засіб афішування ( дисплея) банеру зв\'язку',
	'cfg_mode_communiquant' => 'Афішування ( перегляд) порталу головної сторінки типу зв\'язку',
	'cfg_mode_edito' => 'Афішування ( показ) передової',
	'cfg_mode_evenementiel' => 'Афішування ( показ) порталу головної сторінки типу подій',
	'cfg_mode_international' => 'Афіщування ( показ) порталу головної сторінки міжнародного типу',
	'cfg_mode_mairie' => 'Affichage d\'un portail d\'accueil de type mairie', # NEW
	'cfg_mode_nouveautes' => 'Афішування ( показ) у формі виписки    із останніх новин',
	'cfg_mode_portail' => 'Афішування ( показ) порталу головної сторінки',
	'cfg_mode_portail_actu' => 'Афішування ( показ) порталу головної сторінки орієнтованих Новини',
	'cfg_nav' => 'Навігація',
	'cfg_nav_p' => 'Головне меню ( навігація)',
	'cfg_nav_sec' => 'Вторинна навігація',
	'cfg_navigationtransversale_label' => 'Навігація поперечна',
	'cfg_navigationtransversalelocale' => 'Навігація місцева поперечна',
	'cfg_nombres_quoideneuf' => 'Кількість " Що нового?"',
	'cfg_nombres_quoideneuf_lettres' => 'Les nombres quoi de neuf pour la NewsLetter', # NEW
	'cfg_non_secondaire' => 'Ні, афішувати ( показати) його ( її) внизу  вторинної навігації',
	'cfg_numero_cnil' => 'Афішування ( дисплей) номера CNIL',
	'cfg_numero_cnil_label' => 'Номер регістрації CNIL',
	'cfg_numero_xiti' => ' Nº реєстрації Xiti',
	'cfg_ordre_quoideneuf_date' => 'По зворотній даті публікації',
	'cfg_ordre_quoideneuf_date_modif' => 'По зворотній даті модифікації ( зміни)',
	'cfg_ordre_quoideneuf_label' => 'Класифікація',
	'cfg_page' => 'Сторінка',
	'cfg_parametres_addthis' => 'Настройки AddThis ',
	'cfg_parametres_google' => 'Настройки Google ',
	'cfg_parametres_xiti' => 'Настройки XiTi',
	'cfg_pas_affichage_rss' => 'Не показувати (за винятком логотипу RSS)',
	'cfg_pas_datepublication' => 'Відключення дати публікації',
	'cfg_pas_ephemeride' => 'Ефемерід не відображається',
	'cfg_pied' => 'Нижній колонтитул',
	'cfg_police_nav' => 'Пункт ( police) навігації',
	'cfg_police_texte' => 'Шрифт тексту',
	'cfg_polices_affichage' => 'Налаштування ( конфігурація) відображення шрифтів',
	'cfg_polices_tailles' => 'Настроювання розміру шрифту',
	'cfg_position_cartouche' => 'Position du cartouche de navigation', # NEW
	'cfg_position_flux_rss' => 'Потік RSS',
	'cfg_position_flux_rss_label' => 'Позиція іконки потоку RSS',
	'cfg_position_liens_nav' => 'Загальні навігаційні посилання (положення і дисплей)',
	'cfg_position_liens_nav_label' => 'Позиція',
	'cfg_position_nuage' => 'Position du nuage de tags', # NEW
	'cfg_position_panier' => 'Позиція кошика',
	'cfg_position_panier_label' => 'Афішування (Дисплей ) у верхній частині колонки:',
	'cfg_px' => 'пікселів </ B>', # MODIF
	'cfg_quoideneuf_label' => 'Кількість "Що нового?"',
	'cfg_rainette_ville' => 'Код міста (погоди)',
	'cfg_realisation' => 'Реалізація',
	'cfg_redirection_ancien_site' => 'Перенаправлення старого сайту',
	'cfg_rss_label' => 'Кількість RSS',
	'cfg_rss_label_ex' => 'Кількість  синдикацій відображених потоком RSS у вторинній навігації',
	'cfg_scolaire' => 'Школи (з вересня по грудень наступного року)',
	'cfg_sousmenulocaux' => 'Місцеві підменю',
	'cfg_sousmenulocaux_ex' => 'Змінює  головне меню, щоб  при бажанні не показувати під-рубрики, за винятком для поточної філії ( гілки )',
	'cfg_sousmenulocaux_label' => 'Афішування ( показ )під-рубрик в головному меню',
	'cfg_sponsor' => 'Вам сподобався цей кістяк, ви можете його проявляти, використовуючи даний ідентифікатор для реклами і пошуку: @ @ ID (відповідні скелети завантажити на <HREF = "http://www.pyrat.net/squelettes/noisettes/ додаткових / ads_column.html "> ads_column.html </ A> і <a href="http://www.pyrat.net/squelettes/noisettes/navigation/ads_search.html"> ads_search.html </> і встановити у вашому скелети файл).',
	'cfg_systematiquement' => 'Систематично',
	'cfg_taille_logos_max' => 'Максимальний розмір логотипів меню і панелі інструментів', # MODIF
	'cfg_taille_logos_max_contenu_automatique' => 'Taille maximum des logos des contenus automatiques centraux', # NEW
	'cfg_taille_logos_max_listes_sites' => 'Taille maximum des logos des sites affichés en liste dans le contenu central', # NEW
	'cfg_taille_logos_max_menufooter' => 'Taille maximum des logos du menu de pied de page', # NEW
	'cfg_taille_logos_max_menuprincipal' => 'Taille maximum des logos du menu principal', # NEW
	'cfg_taille_logos_max_outils' => 'Taille maximum des logos de la boite à outil', # NEW
	'cfg_texte_bandeau_annonce' => 'Текст банера оголошення у верхній частині сайту',
	'cfg_texte_bandeau_label' => 'Текст банера контакту сайта',
	'cfg_texte_central' => 'Центральний текст',
	'cfg_texte_explicationslogin' => 'Пояснювальний текст ( ярлики друкарської помилки допускаються)',
	'cfg_texte_police_choisie' => 'Цей текст відображається в обраному шрифті',
	'cfg_textesiplanvide' => 'Текст, якщо план порожній',
	'cfg_textesiplanvide_label' => 'Текст в плані сайту, якщо план порожній від змісту',
	'cfg_titraille' => 'Назва',
	'cfg_titraille_ssniveaux' => 'Titraille',
	'cfg_tous_evenements' => 'Всі події',
	'cfg_type_agenda_label' => 'Календар Типовий',
	'cfg_url_feedflare' => 'URL FeedFlare',
	'cfg_url_redirection' => 'Адреса старого сайту (Перенаправлено на розділ 301, якщо існує)',
	'cfg_url_xiti' => 'URL вашого маркера Xiti',
	'cfg_vignettes' => 'Ескізи режимного порталу',
	'cfg_vignettes_choix_auto' => 'Ескізи розраховані автоматично ',
	'cfg_vignettes_choix_graphiste' => 'Ескізи вільні (зроблені дизайнером)',
	'cfg_vignettesauto' => 'Ескізи:',
	'cfg_zone_forum' => 'Налаштування ( конфігурація)  текстів форумів',
	'cfg_zone_forum_invite' => 'Запрошує прийняти участь у Форумі',
	'cfg_zone_forum_laisser_vide' => 'Залиште порожнім, щоб використовувати текст за замовчуванням.',
	'cfg_zone_forum_titre' => 'Заголовок в розділі "форум"',
	'cfg_zone_recherche_label' => 'Зона афішування ( показу) формуляру пошуку',
	'chapitre_complet' => 'Ціла глава для друку',
	'chapitre_complet_title' => 'Переглянути весь вміст рубрики :',
	'chercheravecgoogle' => 'З Alt <a href=\'http://www.google.com/\'> <IMG SRC "http://www.google.com/logos/Logo_25wht.gif \'= = Назва" Google "= стилі" Google "= \'вертикального вирівнювання: середній, "ширина = \'75\' висота = \'32 \'/> </ A> пошук в Інтернеті!',
	'clever_uns' => 'Відмова від підписки здійснена',
	'clevermail' => 'CleverMail',
	'commencer' => 'Для початку',
	'configurersc' => 'Налаштувати ( конфігурувати) SoyezCréateurs',
	'connexion' => 'Підключення',
	'connexiontitle' => 'Доступ до зарезервованого простору',
	'copyright_cnil' => ' Декларація CNIL N º',
	'copyright_icra' => 'Мічені завдяки  <a href="http://www.icra.org/sitelabel"> ICRA </ A>',
	'copyright_realisation' => 'Реалізація:',
	'copyright_spip' => 'Скелет <a href=\'http://www.pyrat.net/\' title=\'Відвідати сайт  творця цього скелету\'> SoyezCreateurs </ A> на харчування від <HREF = \'http://www.spip. чистої "назва = \'/ Відвідати сайт SPIP, програмне забезпечення управління змістом веб- вільної ліцензії "GPL SPIP </ A>',

	// D
	'deconnexion' => 'Роз\'єднання',
	'deconnexiontitle' => 'Ви виходите ( роз\'єднуєтесь)',
	'deposer_intention' => 'Надіслати наміри прохання ',
	'derniereactualites' => 'Останні новини',
	'dernieremaj' => 'Останнє оновлення сайту:',
	'derniersarticlespublies' => 'Останні опубліковані статті',
	'descriptif' => 'Опис',
	'docatelecharger' => 'Документи для завантаження',

	// E
	'ecoute' => 'Прослуховування : ',
	'editos' => 'Передові статті',
	'erreur' => 'Помилка!',
	'erreur_documentexistepas' => 'Помилка: цей документ не існує!',
	'erreur_excuses_404' => 'Nous sommes désolés...', # NEW
	'erreur_excuses_404_explications' => '<p>La page que vous cherchez n\'a pu être trouvée parce que :</p><ol><li>elle n\'était plus d\'actualité,</li><li>le lien vers la page que vous cherchiez est obsolète,</li><li>la page recherchée a été déplacée.</li></ol><p>Merci de commencer votre recherche sur la <a href="@urlsite@">Page d\'accueil : @nomsite@</a>, ou de chercher un mot clé avec le formulaire ci-dessous.</p>', # NEW

	// F
	'forum' => 'Форум',
	'forum_enreponse' => 'У відповідь на статтю:',
	'forum_enreponse_breve' => 'У відповідь на коротку новину:',
	'forum_enreponse_message' => 'У відповідь на  повідомлення:',
	'forum_prenom' => 'Хто ви?',
	'forum_repondre' => 'Додати ваше свідчення',
	'forum_vosreponses' => 'Ваші свідчення',
	'forum_votre_prenom' => 'Ваше ім\'я :',

	// L
	'layout_accueil_title' => 'Специфічне розташування на головній сторінці',
	'layout_defaut_title' => 'Розташування за замовчуванням  для сайту (статті та рубрики)',
	'liresuitede' => 'Читати наступне:',
	'liresuiteeditorial' => 'Читати продовження передової статті',
	'liresuitesyndic' => 'Читати продовження на початковому сайті',
	'listezones' => 'Список інтерактивних зон',

	// M
	'memerubrique' => 'У цій же рубриці...',
	'mentions_legales_obligatoires' => 'Юридичні вимоги обов\'язкові ([CNIL | Національна комісія з інформатики і свободи-> http://www.cnil.fr/] і [LCEN | Закон про впевненість в економіці Цифровий> http://www.legifrance.gouv. COM / WAspad / UnTexteDeJorf? numjo ECOX0200175L =]). Все, що потрібно знати-> http://www.juriblogsphere.net/2009/12/04/les-informations-legales-a-faire-figurer-sur-un-site-internet/]. [Розшифровка правових зобов\'язань-> http://www.maitre-eolas.fr/post/2008/03/24/905-blogueurs-et-responsabilite-reloaded].',
	'menu_deplier' => 'розкрити ( розгорнути):',
	'menu_picalt' => 'Натиснути ( "клікнути" ) , щоб ',
	'menu_replier' => 'знову скласти: ',
	'mot' => 'Слово',
	'mots' => 'Слова',
	'mots_title' => 'Ключові слова сайту',
	'motsgroupe' => 'Група слів',
	'multimedia' => 'Слухати',
	'multimedia_title' => 'Слухати або дивитися уривок (відкривається у новому вікні)',

	// N
	'newsletter' => 'Новини сайту',
	'newsletter_recevoir' => 'Отримувати новини сайту',

	// O
	'ordreantichronologique' => 'зворотний хронологічний порядок',
	'ou_sommes_nous' => 'Як нас знайти на карті!',

	// P
	'par' => 'по',
	'participez' => 'Візьміть участь у житті сайту!',
	'plan_menu' => 'План сайту Web',
	'plus_loin' => 'Див.також',
	'plus_loin_title' => 'Інформаційний сайт',
	'precisezrecherche' => 'Звузьте область пошуку',
	'publiele' => 'Опубліковано',

	// Q
	'quoideneuf' => 'Що нового?',

	// R
	'recherche_infructueuse' => 'Невдалий пошук.',
	'recherche_label' => 'Пошук по сайту.',
	'recherche_title' => 'Будь ласка, введіть ваш пошук',
	'recherche_title_ok' => 'Почати пошук',
	'recherche_total' => 'Загальна кількість знайдених елементів',
	'recherche_value' => 'Пошук?',
	'retouraccueil' => 'Повернутися на головну сторінку',
	'retoursommaire' => 'Назад Головна',
	'retourtop' => 'Повернутися до початку',
	'rubrique_securisee' => 'Accés réservé ouvert', # NEW

	// S
	'savoirplus' => 'Дізнатися більше ...',
	'savoirpluscritere' => 'Детальніше про критерій',
	'sedna' => 'Седна (RSS агрегатор)',
	'sommaire' => 'зміст',
	'soyezcreateurs' => 'Будьте творчими особистостями',
	'soyezcreateurs_couleurs' => 'Кольори',
	'soyezcreateurs_google' => 'Забезпечення  списком літератури',
	'soyezcreateurs_layout' => 'Позиціонування ( розміщення)',
	'syndiquer_agenda' => 'Об\'єднати щоденник ',

	// T
	'themes' => 'Теми',
	'tousarticlesantichrono' => 'Всі статты в зворотному хронологічному порядку',
	'tout' => 'Все',
	'toutleplan' => 'Весь сайт на одній сторінці',

	// V
	'voirarticlespar' => 'Див. статті',
	'voirdetailannee' => 'Див. деталь року',
	'voirdetailmois' => 'Див. деталь місяця...',
	'voirimage' => 'Перегляд зображень тільки',
	'voirle' => 'Див. дату ...',
	'voirsitespar' => 'Список сайтів з',
	'vous_souhaitez_etre_tenu_au_courant' => 'Vous souhaitez être tenu au courant ?', # NEW

	// W
	'wdcalendar_all_day' => 'Toute la journée', # NEW
	'wdcalendar_apr' => 'Avr', # NEW
	'wdcalendar_aug' => 'Aou', # NEW
	'wdcalendar_cancel' => 'Annuler', # NEW
	'wdcalendar_current_date' => 'Cliquez pour voir la date courante', # NEW
	'wdcalendar_day' => 'Jour', # NEW
	'wdcalendar_day_letter' => 'j', # NEW
	'wdcalendar_day_plural' => 'jours', # NEW
	'wdcalendar_dec' => 'Dec', # NEW
	'wdcalendar_event' => 'Evenement', # NEW
	'wdcalendar_feb' => 'Fév', # NEW
	'wdcalendar_fri' => 'Ven', # NEW
	'wdcalendar_jan' => 'Jan', # NEW
	'wdcalendar_jul' => 'Juil', # NEW
	'wdcalendar_jun' => 'Juin', # NEW
	'wdcalendar_loading_data' => 'Chargement des donnée...', # NEW
	'wdcalendar_loading_fail' => 'Désoler, nous ne pouvons charger vos données, merci d\'essayer plus tard', # NEW
	'wdcalendar_location' => 'Lieu', # NEW
	'wdcalendar_mar' => 'Mar', # NEW
	'wdcalendar_may' => 'Mai', # NEW
	'wdcalendar_mon' => 'Lun', # NEW
	'wdcalendar_month' => 'Mois', # NEW
	'wdcalendar_next_month' => 'Mois Suiv', # NEW
	'wdcalendar_nov' => 'Nov', # NEW
	'wdcalendar_oct' => 'Oct', # NEW
	'wdcalendar_others' => 'Autres', # NEW
	'wdcalendar_participant' => 'Participant', # NEW
	'wdcalendar_prev_month' => 'Mois Prec', # NEW
	'wdcalendar_refresh' => 'Actualiser', # NEW
	'wdcalendar_sat' => 'Sam', # NEW
	'wdcalendar_see_detail' => 'Voir détails', # NEW
	'wdcalendar_sep' => 'Sep', # NEW
	'wdcalendar_sun' => 'Dim', # NEW
	'wdcalendar_thu' => 'Jeu', # NEW
	'wdcalendar_time' => 'Heure', # NEW
	'wdcalendar_today' => 'Aujourd\'hui', # NEW
	'wdcalendar_tue' => 'Mar', # NEW
	'wdcalendar_undefined' => 'Indéfinie', # NEW
	'wdcalendar_wed' => 'Mer', # NEW
	'wdcalendar_week' => 'Semaine', # NEW
	'wdcalendar_what' => 'Sujet' # NEW
);

?>
