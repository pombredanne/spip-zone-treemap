<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Module: public
// Langue: fr

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'derniers_articles' => 'Derniers billets',
	'articles_rubrique' => 'Dans cette catégorie',
	'articles_auteur' => 'Billets de cet auteur',
	'articles' => 'Billets',
);

?>