<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Module: paquet-svpskel
// Langue: fr

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'svpskel_description' => 'Ce plugin est un module optionnel de SVP. 
_ Il fournit un jeu de squelettes Z simple mais complet pour afficher l\'ensemble des objets SVP.
Son but est avant tout de permettre une vérification aisée de toutes les informations collectées par SVP sur les objets dépôt, plugin et paquet.',
	'svpskel_slogan' => 'Jeu de squelettes Z pour SVP',
);
?>