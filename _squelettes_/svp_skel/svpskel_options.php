<?php

// Liste des pages publiques d'objet supportees par le squelette (depot, plugin).
// Permet d'afficher le bouton voir en ligne dans la page d'edition de l'objet
define('_SVP_PAGES_OBJET_PUBLIQUES', 'depot:plugin');

?>
