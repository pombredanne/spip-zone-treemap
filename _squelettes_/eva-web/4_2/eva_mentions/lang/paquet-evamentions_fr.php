<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

    'evamentions_slogan' => 'Gérer la page des mentions légales du site',
    'evamentions_description' => 'Le contenu des mentions légales sera affiché en page mentions (et éventuellement dans le pied de page).',
    
);

?>
