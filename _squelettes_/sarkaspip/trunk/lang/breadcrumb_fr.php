<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Module: sarkaspip
// Langue: fr

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'abonnement_newsletter' => 'Abonnement',
	'agenda' => 'Agenda',
	'galerie' => 'Galerie',
	'mot' => 'Mots-clés',
	'herbier' => 'Sites favoris',
	'site' => 'Sur le web',
	'404' => 'Page introuvable',
	'plan' => 'Plan du site',
	'recherche' => 'Recherche',
);
?>